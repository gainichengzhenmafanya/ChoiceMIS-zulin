package com.choice.common.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import com.choice.framework.util.DataSourceSwitch;
import com.opensymphony.xwork2.ActionContext;

/**
 * DAO层AOP拦截器，实现记录用户操作过的所有方法和参数，并实现DAO层缓存
 * 
 * @author Administrator
 * 
 */
@Component
@Aspect
public class MapperInterceptor {
	ActionContext context = ActionContext.getContext();  
	HttpServletRequest request;  
	HttpServletResponse response;
	
	@Pointcut("execution(* com.choice.*.service..*(..))")
	public void pointCut() {
	}

	/*@After("pointCut()")
	public void after(JoinPoint joinPoint) {
		System.out.println("after aspect executed");
	}*/

	@Before("pointCut()")
	public void before(JoinPoint joinPoint) {
		try {
			// 企业编号
			String companyId = (String)SysContext.getSession().getAttribute("pk_group");
			// 根据企业编号，动态切换数据源
			DataSourceSwitch.setDataSourceType("compJdbcTemplate_" + companyId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
