package com.choice.common.servlet;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;

import com.alibaba.druid.pool.DruidDataSource;
import com.choice.framework.shiro.tools.SpringUtils;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DataSources;

/**
 * usedfor：动态加载数据源
 * Created by javahao on 2017/7/13.
 * auth：JavaHao
 */
public class Jdbconfig {
    @Value("${jdbc.driver}")
    private String driver;
    @Value("${jdbc.url}")
    private String url;
    @Value("${jdbc.username}")
    private String username;
    @Value("${jdbc.password}")
    private String password;

    private Map<Object, Object> targetDataSources = new HashMap<Object, Object>();

    public static final String DATASOURCE_PREFIX="compJdbcTemplate_";

    private Connection connect = null;

    public Jdbconfig init(){
        try {
            Class.forName(driver);
            connect = DriverManager.getConnection(url,username,password);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this;
    }

    public Connection getConnection(){
        return connect;
    }
    /***
     * 获取当前数据源链接
     * @return
     * @throws SQLException
     */
    public Connection getCurrentConnetion() throws SQLException{
    	Object daSource = targetDataSources.get(DataSourceSwitch.getDataSourceType());
    	if(daSource == null)
    		return null;
    	DataSource conn = (DataSource)targetDataSources.get(DataSourceSwitch.getDataSourceType());
    	return conn.getConnection();
    }
    /**
     * 加载动态数据源
     * @param pkGroup 集团ID
     */
    public Map<Object,Object> loadDataSources(String pkGroup){
        if(targetDataSources.size()>0){
            if(!StringUtils.isBlank(pkGroup)){
                Map<Object,Object> result = new HashMap<Object,Object>();
                result.put(pkGroup,targetDataSources.get(DATASOURCE_PREFIX+pkGroup));
                return result;
            }
            return targetDataSources;
        }
        targetDataSources.putAll(loads(pkGroup));
        return targetDataSources;
    }
    /**
     * 加载动态数据源
     * @param pkGroup 集团ID
     */
    private Map<Object,Object> loads(String pkGroup){
        Map<Object, Object> dataSources = new HashMap<Object, Object>();
        String sql = "SELECT PK_GROUP, VCODE, VCOMPNAME, DBIP, DBPORT, DBNAME, DBUSERNAME, DBPASSWORD, VAPPID, VSECRET FROM COMPANY"+
                (StringUtils.isBlank(pkGroup)?"":(" WHERE PK_GROUP = '"+pkGroup+"'"));
        Statement sta = null;
        ResultSet rs = null;
        try{
        	sta = connect.createStatement();
        	rs = sta.executeQuery(sql);
            while(rs.next()) {
                DruidDataSource ds = new DruidDataSource();
                ds.setUrl("jdbc:mysql://" + rs.getString("DBIP") + ":" + rs.getString("DBPORT") + "/" + rs.getString("DBNAME"));
                ds.setUsername(rs.getString("DBUSERNAME"));
                ds.setPassword(rs.getString("DBPASSWORD"));
                ds.setInitialSize(10);
                ds.setMinIdle(10);
                ds.setMaxActive(50);
                ds.setMaxWait(60000);
                ds.setTimeBetweenEvictionRunsMillis(60000);
                ds.setMinEvictableIdleTimeMillis(300000);
                ds.setValidationQuery("SELECT 'x'");
                ds.setTestWhileIdle(true);
                ds.setTestOnBorrow(true);
                ds.setTestOnReturn(true);
                ds.setPoolPreparedStatements(false);
                ds.setMaxPoolPreparedStatementPerConnectionSize(20);
                ds.setFilters("wall,stat");
                dataSources.put(DATASOURCE_PREFIX + rs.getString("PK_GROUP"), ds);
            }
            
            dataSources.put(DATASOURCE_PREFIX + "defaultDataBase", SpringUtils.getBean("defaultDataBase"));
            
        } catch (SQLException e) {
            e.printStackTrace();
        }catch (Exception ex) {
            ex.printStackTrace();
        } finally {
        	if(rs != null){
        		try {
					rs.close();
				} catch (Exception e) {
				}
        	}
        	if(sta != null)
        	{
        		try {
					sta.close();
				} catch (Exception e) {
				}
        		}
        }
        return dataSources;
    }

    /**
     * 判断是否包含数据源
     * @param pkGroup 企业ID
     * @return 返回结果
     */
    public boolean contains(String pkGroup){
        if(StringUtils.isBlank(pkGroup))
            return false;
        if(targetDataSources.get(DATASOURCE_PREFIX+pkGroup)!=null)
            return true;
        //将不存在的数据源重新加载入动态数据源切换列表
        Map<Object, Object> dataSources = new HashMap<Object, Object>();
        dataSources.putAll(loads(pkGroup));
        if(dataSources.size()>0){
            targetDataSources.putAll(dataSources);
            SpringUtils.getBean(DataSources.class).setTargetDataSources(targetDataSources);
            return true;
        }
        return false;
    }
}
