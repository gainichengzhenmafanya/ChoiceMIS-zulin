package com.choice.common.servlet;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ImageShowServlet
 */
public class ImageShowServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static String filesrc = LoadCompInfoServlet.filesrc;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ImageShowServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setHeader("Pragma", "No-cache");  
        response.setHeader("Cache-Control", "no-cache");  
        response.setDateHeader("Expires", 0);   
		OutputStream os = response.getOutputStream(); 
		String pk_group=request.getSession().getAttribute("pk_group").toString();
		String imgname = (null == request.getParameter("imgname") || "".equals(request.getParameter("imgname").toString()))?".PNG":request.getParameter("imgname").toString();
		String filepath=filesrc+pk_group+"/scrimg/"+imgname;
		if(null != request.getParameter("filepath") && !"".equals(request.getParameter("filepath").toString())){
			//如果有filepath参数，就直接传入file参数
			filepath = filesrc+request.getParameter("filepath").toString();
		}
		File file = new File(filepath);
        FileInputStream fips = new FileInputStream(file);  
        byte[] btImg = readStream(fips);  
        os.write(btImg);  
        os.flush();  
	}
	
	    /** 
	     * 读取管道中的流数据 
	     */  
	    public byte[] readStream(InputStream inStream) {  
	        ByteArrayOutputStream bops = new ByteArrayOutputStream();  
	        int data = -1;  
	        try {  
	            while((data = inStream.read()) != -1){  
	                bops.write(data);  
	            }  
	            return bops.toByteArray();  
	        }catch(Exception e){  
	            return null;  
	        }  
	    }  

}
