package com.choice.common.servlet;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import com.choice.framework.domain.system.Account;
import com.choice.framework.util.DataSourceSwitch;

/**
 * 登陆验证
 * @author wangkai
 *2015-8-10
 */

@Service
public class LoginValidate {

	/**
	 * 登录验证
	 * @param account
	 * @param session
	 * @return
	 */
	public String validateLogin(Account account,HttpSession session){
		//从库中查询是否存在有效的企业号，如果没有返回无效
		session.setAttribute("pk_group", account.getPk_group());
		DataSourceSwitch.setDataSourceType("compJdbcTemplate_" + account.getPk_group());
		return account.getPk_group();
	} 
}
