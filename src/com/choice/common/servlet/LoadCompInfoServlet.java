package com.choice.common.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.choice.framework.util.ForResourceFiles;

public class LoadCompInfoServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	public static String url = "";
	public static String username = "";
	public static String password = "";
	public static String driver = "";
	public static String initialSize = "";
	public static String minIdle = "";
	public static String maxActive = "";
	public static String maxWait = "";
	public static String timeBetweenEvictionRunsMillis = "";
	public static String minEvictableIdleTimeMillis = "";
	public static String validationQuery = "";
	public static String testWhileIdle = "";
	public static String testOnBorrow = "";
	public static String testOnReturn = "";
	public static String poolPreparedStatements = "";
	public static String maxOpenPreparedStatements = "";
	public static String removeAbandoned = "";
	public static String removeAbandonedTimeout = "";
	public static String logAbandoned = "";
	public static String logFilters = "";
	public static Map<String,String> groupmap=new HashMap<String,String>();
	public static final String filesrc=ForResourceFiles.getValByKey("config-boh.properties","filesrc");
	public static final String curProName=ForResourceFiles.getValByKey("config-boh.properties","curProName");
	// 设置动态连接数据库
	public static Map<Object, Object> targetDataSources = new HashMap<Object, Object>();
	public static Map<Object, Object> targetCompany = new HashMap<Object, Object>();//存储企业信息

	static {
		//获取数据库连接文件
				InputStream inputStream = LoadCompInfoServlet.class.getClassLoader().getResourceAsStream("jdbc.properties");
				Properties properties = new Properties();
				try {
					properties.load(inputStream);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				//设置连接数据库连接参数
				driver = properties.getProperty("jdbc.driver_all");
				url = properties.getProperty("jdbc.url_all");
				username =properties.getProperty("jdbc.username_all");
				password =properties.getProperty("jdbc.password_all");	
				initialSize = properties.getProperty("pool.initialSize");
				minIdle = properties.getProperty("pool.minIdle");
				maxActive = properties.getProperty("pool.maxActive");
				maxWait = properties.getProperty("pool.maxWait");
				timeBetweenEvictionRunsMillis = properties.getProperty("pool.timeBetweenEvictionRunsMillis");
				minEvictableIdleTimeMillis = properties.getProperty("pool.minEvictableIdleTimeMillis");
				validationQuery = properties.getProperty("pool.validationQuery");
				testWhileIdle = properties.getProperty("pool.testWhileIdle");
				testOnBorrow = properties.getProperty("pool.testOnBorrow");
				testOnReturn = properties.getProperty("pool.testOnReturn");
				poolPreparedStatements = properties.getProperty("pool.poolPreparedStatements");
				maxOpenPreparedStatements = properties.getProperty("pool.maxOpenPreparedStatements");
				removeAbandoned = properties.getProperty("pool.removeAbandoned");
				removeAbandonedTimeout = properties.getProperty("pool.removeAbandonedTimeout");
				logAbandoned = properties.getProperty("pool.logAbandoned");
				logFilters = properties.getProperty("pool.logFilters");
	}

	public void init() throws ServletException {
		super.init();
		/*ServletContext servletContext = this.getServletContext();
		
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		DataSources jdbcTemplate = (DataSources)ctx.getBean("dataSource");
		
//		WeChatUtil.setDynamicDataSource(jdbcTemplate);
		
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			// 连接数据库
			conn = new JdbcConnection().addConnection(url, username, password, driver);
			st = conn.createStatement();
			String sql = "SELECT PK_GROUP, VCODE, VCOMPNAME, DBIP, DBPORT, DBNAME, DBUSERNAME, DBPASSWORD, VAPPID, VSECRET FROM COMPANY";
			rs = st.executeQuery(sql);
			while (rs.next()) {
				//此处由 pk_group 改为 vcode
				//groupmap.put(rs.getString("PK_GROUP"),rs.getString("PK_GROUP"));
				groupmap.put(rs.getString("VCODE"),rs.getString("VCODE"));
				Company company = new Company();
				company.setPk_group(rs.getString("PK_GROUP"));
				company.setVcode(rs.getString("VCODE"));
				company.setVcompName(rs.getString("VCOMPNAME"));
				company.setDbIp(rs.getString("DBIP"));
				company.setDbPort(rs.getString("DBPORT"));
				company.setDbUrl("jdbc:mysql://" + rs.getString("DBIP") + ":" + rs.getString("DBPORT") + "/" + rs.getString("DBNAME"));
				//company.setDbUrl("jdbc:oracle:thin:@" + rs.getString("DBIP") + ":" + rs.getString("DBPORT") + ":" + rs.getString("DBNAME"));
				company.setDbUserName(rs.getString("DBUSERNAME"));
				company.setDbPassword(rs.getString("DBPASSWORD"));
				company.setAppId(rs.getString("VAPPID"));
				company.setSecret(rs.getString("VSECRET"));
				//此处由 PK_GROUP 改为 VCODE
				//targetCompany.put(rs.getString("PK_GROUP"), company);
				targetCompany.put(rs.getString("VCODE"), company);
//				MemCachedUtil.setObject("compInfo_" + rs.getString("PK_GROUP"), company);
				
				DruidDataSource jdbcDataSource = new DruidDataSource();
				jdbcDataSource.setInitialSize(Integer.valueOf(initialSize));
				jdbcDataSource.setMaxActive(Integer.valueOf(maxActive));
				jdbcDataSource.setMinIdle(Integer.valueOf(minIdle));
				jdbcDataSource.setMaxWait(Long.valueOf(maxWait));
				jdbcDataSource.setTimeBetweenEvictionRunsMillis(Long.valueOf(timeBetweenEvictionRunsMillis));
				jdbcDataSource.setMinEvictableIdleTimeMillis(Long.valueOf(minEvictableIdleTimeMillis));
				jdbcDataSource.setValidationQuery(validationQuery);
				jdbcDataSource.setTestWhileIdle(Boolean.valueOf(testWhileIdle));
				jdbcDataSource.setTestOnBorrow(Boolean.valueOf(testOnBorrow));
				jdbcDataSource.setTestOnReturn(Boolean.valueOf(testOnReturn));
				jdbcDataSource.setPoolPreparedStatements(Boolean.valueOf(poolPreparedStatements));
				jdbcDataSource.setMaxOpenPreparedStatements(Integer.valueOf(maxOpenPreparedStatements));
				jdbcDataSource.setRemoveAbandoned(Boolean.valueOf(removeAbandoned));
				jdbcDataSource.setRemoveAbandonedTimeout(Integer.valueOf(removeAbandonedTimeout));
				jdbcDataSource.setLogAbandoned(Boolean.valueOf(logAbandoned));
				jdbcDataSource.setDriverClassName(driver);
				jdbcDataSource.setUrl(company.getDbUrl());
				jdbcDataSource.setUsername(company.getDbUserName());
				jdbcDataSource.setPassword(company.getDbPassword());
				//jdbcTemplate.setDataSource(jdbcDataSource);
				//此处由 PK_GROUP 改为 VCODE
				System.out.println("@@@@@@@@@@@@@@@@@@@@@load company:" + rs.getString("VCODE"));
				//此处由 PK_GROUP 改为 VCODE
				targetDataSources.put("compJdbcTemplate_" + rs.getString("VCODE"), jdbcDataSource);
				
//				WeChatUtil.getJdbcTemplateMap().put("compJdbcTemplate_" + rs.getString("PK_GROUP"), jdbcTemplate);
				//MemCachedUtil.setObject("compJdbcTemplate_" + rs.getString("PK_GROUP"), jdbcTemplate);
			}
			rs.close();
			
			jdbcTemplate.setTargetDataSources(targetDataSources);
			jdbcTemplate.afterPropertiesSet();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != st) {
					st.close();
				}
				if (null != conn) {
					conn.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}*/
	}

	public void doGet(HttpServletRequest httpservletrequest,
			HttpServletResponse httpservletresponse) throws ServletException,
			IOException {
		super.doGet(httpservletrequest, httpservletresponse);
	}

	public void destory() {
		super.destroy();
	}
}
