package com.choice.common.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.alibaba.druid.pool.DruidDataSource;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DataSources;

public class ActionFilter implements Filter {

	public void destroy() {
	}

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest hreq = (HttpServletRequest) req;

		SysContext.setRequest((HttpServletRequest) req);
		SysContext.setResponse((HttpServletResponse) res);

		try {
			String companyId = hreq.getParameter("pk_group");
			if (StringUtils.isBlank(companyId)) {//非登陆状态
				companyId = (String) hreq.getSession().getAttribute("pk_group");
			} else {//登陆状态
				//切换成大写
				companyId = companyId.toUpperCase();
				hreq.getSession().setAttribute("pk_group", companyId);
			}
			String pk_group = LoadCompInfoServlet.groupmap.get(companyId);// 从内存中取，如果取的到，就进行设置数据源
			if (null == pk_group || "".equals(pk_group)) {
				if (!this.checkPkGroup(companyId, hreq.getSession())) {// 如果false说明库中也无此企业号，返回企业号无效
					companyId = "0";
				}
			}
			DataSourceSwitch.setDataSourceType("compJdbcTemplate_" + companyId);
			pk_group = null;
			// Filter 只是链式处理，请求依然转发到目的地址。
			chain.doFilter(req, res);
			companyId = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void init(FilterConfig config) throws ServletException {
	}

	/**
	 * 库中查询是否含有企业号，如果有返回true，并添加到内存map中，如果没有返回false
	 * 
	 * @param account
	 * @param session
	 * @return
	 */
	private boolean checkPkGroup(String pk_group, HttpSession session) {
		boolean flag = true;
		ServletContext servletContext = session.getServletContext();
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		DataSources jdbcTemplate = (DataSources) ctx.getBean("dataSource");
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			// 设置动态连接数据库
			// 连接数据库
			conn = new JdbcConnection().addConnection(LoadCompInfoServlet.url, LoadCompInfoServlet.username,
					LoadCompInfoServlet.password, LoadCompInfoServlet.driver);
			st = conn.createStatement();
			//vcode企业号 dr启用状态 dbcreated数据库是否已创建 expiredate激活时间
			String sql = "SELECT PK_GROUP, VCODE, VCOMPNAME, DBIP, DBPORT, DBNAME, DBUSERNAME, "
					+ "DBPASSWORD, VAPPID, VSECRET FROM COMPANY WHERE upper(VCODE) = '" + pk_group + "' "
					+ "AND dr='0' AND dbcreated='1' AND EXPIREDATE >= DATE_FORMAT(NOW(),'%Y-%m-%d')";
			rs = st.executeQuery(sql);
			if (rs.next()) {
				// 此处由 PK_GROUP 改为 VCODE
				// LoadCompInfoServlet.groupmap.put(rs.getString("PK_GROUP"),rs.getString("PK_GROUP"));
				String vcode = rs.getString("VCODE");
				if(StringUtils.isBlank(vcode)){
					throw new NullPointerException("tanent.COMPANY表中没有该vcode");
				}
				vcode = vcode.toUpperCase();
				LoadCompInfoServlet.groupmap.put(vcode, vcode);
				Company company = new Company();
				company.setPk_group(rs.getString("PK_GROUP"));
				company.setVcode(vcode);
				company.setVcompName(rs.getString("VCOMPNAME"));
				company.setDbIp(rs.getString("DBIP"));
				company.setDbPort(rs.getString("DBPORT"));
				company.setDbUrl("jdbc:mysql://" + rs.getString("DBIP") + ":" + rs.getString("DBPORT") + "/"
						+ rs.getString("DBNAME"));
				// company.setDbUrl("jdbc:oracle:thin:@" + rs.getString("DBIP")
				// + ":" + rs.getString("DBPORT") + ":" +
				// rs.getString("DBNAME"));
				company.setDbUserName(rs.getString("DBUSERNAME"));
				company.setDbPassword(rs.getString("DBPASSWORD"));
				company.setAppId(rs.getString("VAPPID"));
				company.setSecret(rs.getString("VSECRET"));
				LoadCompInfoServlet.targetCompany.put(vcode, company);
				// MemCachedUtil.setObject("compInfo_" +
				// rs.getString("PK_GROUP"), company);

				DruidDataSource jdbcDataSource = new DruidDataSource();
				jdbcDataSource.setInitialSize(Integer.valueOf(LoadCompInfoServlet.initialSize));
				jdbcDataSource.setMaxActive(Integer.valueOf(LoadCompInfoServlet.maxActive));
				jdbcDataSource.setMinIdle(Integer.valueOf(LoadCompInfoServlet.minIdle));
				jdbcDataSource.setMaxWait(Long.valueOf(LoadCompInfoServlet.maxWait));
				jdbcDataSource.setTimeBetweenEvictionRunsMillis(Long.valueOf(LoadCompInfoServlet.timeBetweenEvictionRunsMillis));
				jdbcDataSource.setMinEvictableIdleTimeMillis(Long.valueOf(LoadCompInfoServlet.minEvictableIdleTimeMillis));
				jdbcDataSource.setValidationQuery(LoadCompInfoServlet.validationQuery);
				jdbcDataSource.setTestWhileIdle(Boolean.valueOf(LoadCompInfoServlet.testWhileIdle));
				jdbcDataSource.setTestOnBorrow(Boolean.valueOf(LoadCompInfoServlet.testOnBorrow));
				jdbcDataSource.setTestOnReturn(Boolean.valueOf(LoadCompInfoServlet.testOnReturn));
				jdbcDataSource.setPoolPreparedStatements(Boolean.valueOf(LoadCompInfoServlet.poolPreparedStatements));
				jdbcDataSource.setMaxOpenPreparedStatements(Integer.valueOf(LoadCompInfoServlet.maxOpenPreparedStatements));
				jdbcDataSource.setRemoveAbandoned(Boolean.valueOf(LoadCompInfoServlet.removeAbandoned));
				jdbcDataSource.setRemoveAbandonedTimeout(Integer.valueOf(LoadCompInfoServlet.removeAbandonedTimeout));
				jdbcDataSource.setLogAbandoned(Boolean.valueOf(LoadCompInfoServlet.logAbandoned));
				jdbcDataSource.setFilters(LoadCompInfoServlet.logFilters);
				jdbcDataSource.setDriverClassName(LoadCompInfoServlet.driver);
				jdbcDataSource.setUrl(company.getDbUrl());
				jdbcDataSource.setUsername(company.getDbUserName());
				jdbcDataSource.setPassword(company.getDbPassword());
				// 此处由 PK_GROUP 改为 VCODE
				// System.out.println("@@@@@@@@@@@@@@@@@@@@@load company:" +
				// rs.getString("PK_GROUP"));
				System.out.println("@@@@@@@@@@@@@@@@@@@@@load company:" + vcode);
				LoadCompInfoServlet.targetDataSources.put("compJdbcTemplate_" + vcode, jdbcDataSource);
			} else {
				flag = false;
			}
			rs.close();

			jdbcTemplate.setTargetDataSources(LoadCompInfoServlet.targetDataSources);
			jdbcTemplate.afterPropertiesSet();
		} catch (Exception e) {
			e.printStackTrace();
			flag = false;
		} finally {
			try {
				if (null != st) {
					st.close();
				}
				if (null != conn) {
					conn.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return flag;
	}

}