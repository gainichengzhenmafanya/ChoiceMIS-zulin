package com.choice.framework.domain.system;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.choice.orientationSys.constants.StringConstant;
import com.choice.scm.domain.Positn;


public class Account implements Serializable {
	
	private static final long serialVersionUID = -5875698656374182743L;
	
    private String id;

    private String name;
    
    private String password;

	private String state;

    private String deleteFlag;
    
    private User user;
    
    //新加入  分店
    private Positn positn;
    
    private String isCustom;	//是否是客服人员
    
    private String names;
    
    private String pk_group;
    
    private String isRegist;//是否注册
    
    private String regist;
    private String code;
    private String sex;
	private Date birthday;
	private String positncode;
	private String ySail;
	private int days;
	private String dates;
	private String userNums;
	private String choice;
	private List roleList;
    
    /*
     * 构造方法
     * 删除标识（TRUE：已删除；FALSE：未删除）
     */
    public Account(){
    	deleteFlag = StringConstant.FALSE;
    }
    
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Positn getPositn() {
		return positn;
	}

	public void setPositn(Positn positn) {
		this.positn = positn;
	}

	public String getIsCustom() {
		return isCustom;
	}

	public void setIsCustom(String isCustom) {
		this.isCustom = isCustom;
	}

	public String getNames() {
		return names;
	}

	public void setNames(String names) {
		this.names = names;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getPk_group() {
		return pk_group;
	}

	public void setPk_group(String pk_group) {
		this.pk_group = pk_group;
	}

	public String getIsRegist() {
		return isRegist;
	}

	public void setIsRegist(String isRegist) {
		this.isRegist = isRegist;
	}

	public String getRegist() {
		return regist;
	}

	public void setRegist(String regist) {
		this.regist = regist;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getPositncode() {
		return positncode;
	}

	public void setPositncode(String positncode) {
		this.positncode = positncode;
	}

	public String getySail() {
		return ySail;
	}

	public void setySail(String ySail) {
		this.ySail = ySail;
	}

	public int getDays() {
		return days;
	}

	public void setDays(int days) {
		this.days = days;
	}

	public String getDates() {
		return dates;
	}

	public void setDates(String dates) {
		this.dates = dates;
	}

	public String getUserNums() {
		return userNums;
	}

	public void setUserNums(String userNums) {
		this.userNums = userNums;
	}

	public String getChoice() {
		return choice;
	}

	public void setChoice(String choice) {
		this.choice = choice;
	}

	public List getRoleList() {
		return roleList;
	}

	public void setRoleList(List roleList) {
		this.roleList = roleList;
	}

}
