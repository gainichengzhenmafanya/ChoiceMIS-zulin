package com.choice.framework.web.interceptor;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.redis.RedisConfig;
import com.choice.framework.util.CacheUtil;
import com.choice.framework.vo.AccountCache;

public class AuthorityInterceptor extends HandlerInterceptorAdapter {
//	@Autowired
//	private RedisOpsForString<AccountCache> redisDao;
	
	@Override 
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, 
			Object handler) throws Exception {
		String path = request.getServletPath();
		String accountId = (String)(request.getSession().getAttribute("accountId"));
		//手机系统
		if(null != path && path.contains("WAP.do")){
			return true;
		}
		//微信取号
		if((null != path && "waitSeat".equals(path.split("/")[1])) ||(null != path && "actmExec".equals(path.split("/")[1]))){
			return true;
		}
		
		if((null != path && "wapFirmMis".equals(path.split("/")[1]))||(null != path && "orderMeal".equals(path.split("/")[1]))||(null != path && "wapTele".equals(path.split("/")[1]))){
			return true;
		}
//		if(null != path && "/login/loginOut.do".equals(path)){
//				response.sendRedirect(request.getContextPath()+ "/view/login_.jsp");
//				return false;
//		}
		if(null != path && !"/login/loginIn.do".equals(path)){
			if((null == accountId) || ("".equals(accountId))){
				response.sendRedirect(request.getContextPath()+ "/view/out.jsp");
				return false;
			}
		}
		return true;
	}
	
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, 
			Object handler, ModelAndView modelAndView) throws Exception {
		
		String path = request.getServletPath();
		//手机系统
		if(null != path && path.contains("WAP.do")){
			return;
		}
		//微信取号
		if(null != path && "waitSeat".equals(path.split("/")[1]) ||(null != path && "actmExec".equals(path.split("/")[1]))){
			return;
		}
		
		if((null != path && "orderMeal".equals(path.split("/")[1]))){
			return;
		}
		if((null != path && "wapFirmMis".equals(path.split("/")[1]))||(null != path && (null != path && "wapTele".equals(path.split("/")[1])))){
			return;
		}
		
		String accountId = (String)(request.getSession().getAttribute("accountId"));
		HashMap<String,Boolean> operateMap  = new HashMap<String, Boolean>();
		
		//判断是否存在账号名称，并且路径是否符合“/controllerMapping/methodMapping.do”的形式
		if((null != accountId) && (!"".equals(accountId))
			&& (null != path.split("/")  && path.split("/").length > 1)){
			
			AccountCache accountCache = null;
			//如果开启集群模式，从redis中取值
			String opencluster = RedisConfig.getString("opencluster");
			if(null != opencluster && "1".equals(opencluster)){
				try{
					//accountCache = redisDao.get(StringConstant.ACCOUNT_CACHE + accountId);
					accountCache = (AccountCache) request.getSession().getAttribute(StringConstant.ACCOUNT_CACHE);
				}catch(Exception e){
					e.printStackTrace();
				}
			}else{
				//获取账号的缓存信息
				CacheUtil cacheUtil = CacheUtil.getInstance();
				accountCache = (AccountCache)cacheUtil.get(StringConstant.ACCOUNT_CACHE, accountId);
			}
			
			String moduleMapping = path.split("/")[1];
			if(null!=accountCache){
				HashMap<String,HashMap<String,Boolean>> moduleOperateMap = accountCache.getModuleOperateMap();
				
				//获取账号请求访问模块的操作集合
				if(moduleOperateMap.containsKey(moduleMapping))
					operateMap = moduleOperateMap.get(moduleMapping);
			}
		}else if(null != path && "/login/loginIn.do".equals(path)){
			return;
		}else{
			response.sendRedirect(request.getContextPath()+ "/view/login.jsp");
		}
		
		if(null!=modelAndView)
			modelAndView.getModel().put("operateMap", operateMap);
		
	}
	
//	@Override
//	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, 
//			Object handler, Exception ex) throws Exception {
//		
//	}

}
