package com.choice.framework.web.controller.system;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.system.AccountSupplyConstants;
import com.choice.framework.domain.system.AccountSupply;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.service.system.AccountSupplyService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.Supply;
import com.choice.scm.service.GrpTypService;
import com.choice.scm.service.SupplyService;


@Controller
@RequestMapping(value = "accountSupply")
public class AccountSupplyController {
	
	@Autowired
	private AccountSupplyService accountSupplyService;
	@Autowired
	private SupplyService supplyService;
	@Autowired
	private GrpTypService grpTypService;
	@Autowired
	private LogsMapper logsMapper;	
	
	/**
	 * 查询物资 上
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addSupplyBatch")
	public ModelAndView addSupplyBatch(ModelMap modelMap, String accountId) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		AccountSupply accountSupply = new AccountSupply();
		accountSupply.setAccountId(accountId);
		List<AccountSupply> accountSupplyList = accountSupplyService.findAccountSupply(accountSupply);
//		modelMap.put("defaultName", (null==defaultName||"".equals(defaultName))?defaultName:URLDecoder.decode(defaultName, "UTF-8"));
		String supplysId = "";
		String supplysDes = "";
		for (int i=0;i<accountSupplyList.size();i++) {
			if (i!=0){
				supplysId += ",";
				supplysDes += ",";
			}
			supplysId += accountSupplyList.get(i).getSp_code();
			supplysDes += accountSupplyList.get(i).getSp_des();
		}
		
		modelMap.put("defaultCode", supplysId);
		modelMap.put("defaultName", supplysDes);
		modelMap.put("accountId", accountId);
		return new ModelAndView(AccountSupplyConstants.ADD_SUPPLYBATCH, modelMap);
	}
	
	/**
	 * 查询物资 下左
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectNSupply")
	public ModelAndView selectNSupply(ModelMap modelMap, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));//大类
		modelMap.put("grpList", grpTypService.findAllGrpA(acct,null));//中类
		modelMap.put("typList", grpTypService.findAllTypA(acct,null));//小类
		return new ModelAndView(AccountSupplyConstants.SELECT_NSUPPLY, modelMap);
	}
	
	/**
	 * 查询物资 下右
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectTableNSupply")
	public ModelAndView selectTableNSupply(ModelMap modelMap, Supply supply, String level, String code, Page page, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("supplyList", supplyService.findAllSupplyByLeftGrpTyp(supply, level, code, page));
		modelMap.put("pageobj", page);
		modelMap.put("code", code);
		modelMap.put("level", level);
		modelMap.put("supply", supply);//查询记忆
		return new ModelAndView(AccountSupplyConstants.SELECT_TABLENSUPPLY, modelMap);
	}
	
	/**
	 * 查询账户具有的物资
	 * @param accountSupply
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findSupplyByAccountId")
	@ResponseBody
	public Object findSupplyByAccountId(AccountSupply accountSupply, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,"查询账户具有的分店",session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return accountSupplyService.findAccountSupply(accountSupply);
	}
	
	/**
	 * 修改账户具有的物资
	 * @param accountSupply
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateAccounSupply")
	@ResponseBody
	public void updateAccountFirm(AccountSupply accountSupply, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改账户具有的物资权限",session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		
		accountSupplyService.saveAccountSupply(accountSupply);
	}
	
}

