package com.choice.framework.web.controller.system;

import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.constants.system.AccountPositnConstants;
import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.service.system.AccountService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.MD5;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Positn;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.PositnService;

@Controller
@RequestMapping("accountPositn")
public class AccountPositnController {

	@Autowired
	PositnService positnService;
	@Autowired
	CodeDesService codeDesService;
	@Autowired
	AccountPositnService accountPositnService;
	@Autowired
	AccountService accountService;
	@Autowired
    LogsMapper logsMapper;
		
	/**
	 * 模糊查询分店和仓位
	 * @param modelMap
	 * @param positn
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/list")
	public ModelAndView findPositn(ModelMap modelMap) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(AccountPositnConstants.MANAGER_POSITN_ACCOUNT,modelMap);
	}
	/**
	 * 模糊查询分店和仓位
	 * @param modelMap
	 * @param positn
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/listPositnAccount")
	public ModelAndView listPositnAccount(ModelMap modelMap,Positn positn,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		CodeDes code = new CodeDes();
		Page p = new Page();
		p.setPageSize(Integer.MAX_VALUE);
		code.setTyp(String.valueOf(CodeDes.S_SYQ));
		modelMap.put("listMod", codeDesService.findCodeDes(code, p));
		code.setTyp(String.valueOf(CodeDes.S_AREA));
		modelMap.put("listArea", codeDesService.findCodeDes(code, p));
		modelMap.put("listPositn", positnService.findPositn(positn,page));
		modelMap.put("queryPositn", positn);
		modelMap.put("pageobj", page);
		return new ModelAndView(AccountPositnConstants.LIST_ACCOUNT_POSITN,modelMap);
	}
	/**
	 * 根据分店 查询   所有账号
	 * @param accountPositn
	 * @return
	 */
	@RequestMapping(value = "/tableFromPositn")
	public ModelAndView findAccountFromUser1(ModelMap modelMap, AccountPositn accountPositn) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("listAccount", accountPositnService.findAccountPositn(accountPositn));
		if(null!=accountPositn && null!=accountPositn.getPositn() && !"".equals(accountPositn.getPositn().getCode())){
			modelMap.put("positnId", accountPositn.getPositn().getCode());
		}
		modelMap.put("SEX_RADIO", StringConstant.SEX_RADIO);
		modelMap.put("ACCOUNT_STATE", StringConstant.ACCOUNT_STATE);
		return new ModelAndView(AccountPositnConstants.TABLE_ACCOUNT_BY_POSITN, modelMap);
	}
	/**
	 * 添加账号跳转
	 * @param modelMap
	 * @param positnId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/add")
	public ModelAndView add(ModelMap modelMap, String positnId) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("positnId", positnId);
		modelMap.put("ACCOUNT_STATE", StringConstant.ACCOUNT_STATE);
		return new ModelAndView(AccountPositnConstants.ADD_ACCOUNT, modelMap);
	}
	/**
	 * 修改账号密码跳转
	 * @param modelMap
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updatePasssWord")
	public ModelAndView updatePasssWord(ModelMap modelMap, String id,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null==id || "".equals(id)){
			id=(String)session.getAttribute("accountId");
		}
		modelMap.put("accountPositn", accountPositnService.findAccountById(id));
		return new ModelAndView(AccountPositnConstants.UPDATE_ACCOUNT_PASSWORD, modelMap);
	}
	/**
	 * 修改账号基本信息跳转
	 * @param modelMap
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update")
	public ModelAndView update(ModelMap modelMap, String id) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("accountPositn", accountPositnService.findAccountById(id));
		modelMap.put("ACCOUNT_STATE", StringConstant.ACCOUNT_STATE);
		return new ModelAndView(AccountPositnConstants.UPDATE_ACCOUNT, modelMap);
	}
	/**
	 * 查询用户名   是否存在
	 * @param name
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/nameIsExist", method=RequestMethod.POST)
	@ResponseBody
	public Object ajaxSaveByAdd(@RequestParam String name) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return accountPositnService.nameIsExist(name);
		 
	}
	/**
	 * 保存账号
	 * @param modelMap
	 * @param accountPositn
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveByAdd")
	public ModelAndView saveByAdd(ModelMap modelMap, AccountPositn accountPositn,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		accountPositnService.saveAccount(accountPositn);
		
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增账号:"+accountPositn.getName(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
				
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	/**
	 * 保存修改
	 * @param modelMap
	 * @param accountPositn
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveByUpdate")
	public ModelAndView saveByUpdate(ModelMap modelMap, String action, AccountPositn accountPositn,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if("resetPwd".equals(action)){
			AccountPositn ap=accountPositnService.findAccountById(accountPositn.getId());
			accountPositn.setPassword(MD5.md5(ap.getName()+StringConstant.INIT_PASSWORD));
		}
		accountPositnService.updateAccount(accountPositn);
		
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改账号:"+accountPositn.getName(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	/**
	 * 删除账号
	 * @param modelMap
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete")
	public ModelAndView delete(ModelMap modelMap,String ids,HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		accountService.deleteAccount(ids);
		
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,"删除账号",session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
}
