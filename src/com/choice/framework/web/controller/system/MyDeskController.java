package com.choice.framework.web.controller.system;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.domain.system.AccountFirm;
import com.choice.framework.domain.system.Role;
import com.choice.framework.service.system.AccountFirmService;
import com.choice.framework.service.system.RoleService;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.constants.myDesk.MyDeskMisbohConstants;
import com.choice.misboh.domain.BaseRecord.OtherStockDataO;
import com.choice.misboh.domain.inventory.Chkstoref;
import com.choice.misboh.domain.lossmanage.PostionLoss;
import com.choice.misboh.domain.myDesk.GuideConfig;
import com.choice.misboh.domain.store.Store;
import com.choice.misboh.service.chkstom.ChkstomMisService;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.misboh.service.inout.ChkinmMisService;
import com.choice.misboh.service.inout.ChkoutMisService;
import com.choice.misboh.service.myDesk.MyDeskMisbohService;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.ReadProperties;
import com.choice.scm.constants.MainInfoConstants;
import com.choice.scm.domain.Announcement;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.Chkoutm;
import com.choice.scm.domain.Chkstom;
import com.choice.scm.domain.Condition;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.ScheduleD;
import com.choice.scm.domain.Spprice;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.domain.WorkBench;
import com.choice.scm.service.AnnouncementService;
import com.choice.scm.service.MainInfoService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.SppriceService;
import com.choice.scm.util.ArrayUtil;

@Controller
@RequestMapping("myDesk")
public class MyDeskController {

	@Autowired
	private MainInfoService mainInfoService;
	@Autowired	
	private AnnouncementService announcementService;
	@Autowired
	private SppriceService sppriceService;
	@Autowired
	private RoleService roleService;
	@Autowired
	private AccountFirmService accountFirmService;
	@Autowired
	private MyDeskMisbohService myDeskMisbohService;
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	@Autowired
	private ChkoutMisService chkoutMisService;
	@Autowired
	private ChkstomMisService chkstomMisService;
	@Autowired
	private ChkinmMisService chkinmMisService;
	@Autowired
	private PositnService positnService;
	
	/**
	 * 查询用户定义的所需信息
	 */
	@RequestMapping(value = "/list")
	public ModelAndView findTableMain(ModelMap modelMap,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		String accountId = (String)(session.getAttribute("accountId"));
		int trLength = 10;
		Date startDate = DateFormat.getDateBefore(new Date(), "day", -1, 3);
		String startDateString = DateFormat.getStringByDate(startDate,"yyyy-MM-dd");
		Date yesterDay = DateFormat.getDateBefore(new Date(), "day", -1, 1);
		String yesterDayString = DateFormat.getStringByDate(yesterDay, "yyyy-MM-dd");
		//所有菜单的list信息
		Map<String,Object> conditionMap = new HashMap<String, Object>();
		conditionMap.put("acct", acct);
		conditionMap.put("topN", trLength);
		conditionMap.put("maded", startDate);
		conditionMap.put("today", DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		conditionMap.put("yesterday", yesterDay);
		
		
		//查询未审核单据
		List<Map<String,Object>> unCheckedList = null;//未审核信息
		List<Announcement> announcementList = null;//公告信息
		List<Supply> supplyListOver= null;//库存超过上限
		List<Supply> supplyListLow= null;//库存不足下限
		List<Spprice> sppriceList = null;//将到期物资
		List<Spprice> sppriceLIST = null;
		List<Map<String,Object>> stackBillSumList = null;//进货单据汇总
		List<Map<String,Object>> outBillSumList = null;//出库单单据汇总
		List<Map<String,Object>> firmAmtList = new ArrayList<Map<String,Object>>();//各门店昨日营业额
		List<Map<String,Object>> uploadList = new ArrayList<Map<String, Object>>();//各门店上传情况
		List<Map<String,Object>> storeYyeList = new ArrayList<Map<String, Object>>();//门店营业额
		List<Map<String,Object>> yiBiaoPan = new ArrayList<Map<String, Object>>();//营业额增长率、人均、单客增长率
		
		//查询用户自定义菜单信息
		WorkBench workBench = mainInfoService.findWorkBench(accountId);
		if(null!= workBench && null != workBench.getMenu() && !"".equals(workBench.getMenu())){
			String[] menuArray = workBench.getMenu().split(",");
			for(String menu:menuArray){
				if(menu.equals("m_1_1")){//未审核信息
					unCheckedList = mainInfoService.findUnChecked(conditionMap);
					modelMap.put("today", unCheckedList.get(3).get("count"));
					modelMap.put("yesterday", unCheckedList.get(4).get("count"));
					unCheckedList = unCheckedList.subList(0, 3);
					
				}else if(menu.equals("m_1_2")){//查询公告信息
					announcementList = announcementService.findAllAnnouncement();
				}else if(menu.equals("m_2_1")){//库存超过上限
					supplyListOver = mainInfoService.findSupplyOver(conditionMap);
				}else if(menu.equals("m_2_2")){//库存不足下限
					supplyListLow = mainInfoService.findSupplyLow(conditionMap);
				}else if(menu.equals("m_2_3")){//进货单据汇总
					/*Map<String,Object> condition = new HashMap<String, Object>();
					condition.put("startDate", DateFormat.formatDate(DateFormat.getDateBefore(new Date(), "day", -1, 3), "yyyy-MM-dd"));
					condition.put("endDate", DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
					stackBillSumList = mainInfoService.findStackBillSum(condition);*/
				}else if(menu.equals("m_2_4")){//出库单据汇总
					/*Map<String,Object> condition = new HashMap<String, Object>();
					condition.put("startDate", DateFormat.formatDate(DateFormat.getDateBefore(new Date(), "day", -1, 3), "yyyy-MM-dd"));
					condition.put("endDate", DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
					outBillSumList = mainInfoService.findOutBillSum(condition);*/
				}else if(menu.equals("m_2_5")){//将到期物资报价
					Spprice spprice = new Spprice();
					spprice.setAcct(acct);
					spprice.setEdat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
					Page page = new Page();
//					page.setPageSize(Integer.MAX_VALUE);
					page.setPageSize(10);
					sppriceList = sppriceService.findSppriceByEdatMain(spprice,page);
					if(sppriceList.size()>trLength){
						sppriceList.subList(0, trLength);
					}
					
					//计算物资报价剩余日期
					sppriceLIST = new ArrayList<Spprice>();
					for(Spprice sp:sppriceList){
						if(sp.getDeltaT()!=null && sp.getDeltaT()>=0){
							Date date = sp.getEdat();
							Date now = new Date();
							long deltaT = (date.getTime()-now.getTime())/86400000+1;
							sp.setDeltaT((int)deltaT);
							sppriceLIST.add(sp);
						}
					}
				}else if(menu.equals("m_2_7")){//各门店昨日营业额
					DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
					String firmids = "";
					AccountFirm accountFirm = new AccountFirm();
					accountFirm.setAccountId(accountId);
					List<AccountFirm> list = accountFirmService.findAccountFirm(accountFirm);
					if(list.size()>0){
						for(AccountFirm afirm : list)
							firmids += afirm.getFirmId()+",";
						firmids = firmids.substring(0,firmids.length()-1);
					}
					Condition condition = new Condition();
					condition.setFirmid(firmids);
					DataSourceSwitch.setDataSourceType(DataSourceInstances.TELE);//选择数据源
					condition.setBdat(DateFormat.getDateBefore(new Date(),"day",-1,1));
					condition.setEdat(DateFormat.getDateBefore(new Date(),"day",-1,1));
					condition.setPage(10);
//					firmAmtList = mainInfoService.firmAmtList(condition);
				}else if(menu.equals("m_2_11")){//各门店昨日未上传数据
					DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
					String firmids = "";
					AccountFirm accountFirm = new AccountFirm();
					accountFirm.setAccountId(accountId);
					List<AccountFirm> list = accountFirmService.findAccountFirm(accountFirm);
					if(list.size()>0){
						for(AccountFirm afirm : list)
							firmids += afirm.getFirmId()+",";
						firmids = firmids.substring(0,firmids.length()-1);
					}
					Condition condition = new Condition();
					condition.setFirmid(firmids);
					DataSourceSwitch.setDataSourceType(DataSourceInstances.TELE);//选择数据源
					condition.setBdat(DateFormat.getDateBefore(new Date(),"day",-1,1));
					condition.setEdat(DateFormat.getDateBefore(new Date(),"day",-1,1));
//					uploadList = mainInfoService.getUnUploadFirm(condition);
				}else if(menu.equals("m_2_12")){//门店营业额
					DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
					storeYyeList = mainInfoService.findStoreYye();
				}else if(menu.equals("m_66_1")||menu.equals("m_66_2")){//营业额增长率、人均、单客增长率 总部boh  中餐
					DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
					yiBiaoPan = mainInfoService.getYiBiaoPan();
					modelMap.put("y1",yiBiaoPan.get(0).get("ZQAMTPER"));
					modelMap.put("y2",yiBiaoPan.get(0).get("ZHAMT"));
					modelMap.put("y3",yiBiaoPan.get(0).get("PAXPER"));
					modelMap.put("y4",yiBiaoPan.get(0).get("PAXRJ"));
				}else if(menu.equals("m_76_1")||menu.equals("m_76_2")){//营业额增长率、人均、单客增长率 总部boh 快餐
					DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
					yiBiaoPan = mainInfoService.getYiBiaoPanKcBoh();
					modelMap.put("ky1",yiBiaoPan.get(0).get("ZQAMTPER"));
					modelMap.put("ky2",yiBiaoPan.get(0).get("ZHAMT"));
					modelMap.put("ky3",yiBiaoPan.get(0).get("PAXPER"));
					modelMap.put("ky4",yiBiaoPan.get(0).get("PAXRJ"));
				}else if(menu.equals("m_33_1") || menu.equals("m_33_2") || menu.equals("m_33_3") || menu.equals("m_33_4")){
					DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
					//1.判断有没有分配所属分店，没有分配直接返回
					Positn thePositn = (Positn)session.getAttribute("accountPositn");
					if (null != thePositn) {
						String positnCode = thePositn.getCode();
						//2.判断在cboh_store_3ch 里有没有此分店
						int con = myDeskMisbohService.findStoreByVcode(positnCode);
						if(con != 1){
							modelMap.put("accountStore", "N");
						}
						if(menu.equals("m_33_1")){//misboh查询当天要报货的类别
							ScheduleD scheduleD = new ScheduleD();
							scheduleD.setOrderDate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
							scheduleD.setPositnCode(positnCode);
							List<ScheduleD> detailsList = myDeskMisbohService.findSchedulesByPositn(scheduleD);
							modelMap.put("scheduleList", detailsList);
						}else if(menu.equals("m_33_2")){//misboh查询当天 要验货的物资
							//1.查询今日直配到货数量
							Dis dis = new Dis();
							dis.setFirmCode(positnCode);
							dis.setInd(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
							List<Dis> direList = myDeskMisbohService.findDireCountGroupbyDeliver(dis);
							modelMap.put("direList", direList);
							//2.查询统配到货数量
							List<SupplyAcct> outList = myDeskMisbohService.findOutCount(dis);
							modelMap.put("outList", outList);
							//3.查询调拨到货数量
							List<SupplyAcct> dbList = myDeskMisbohService.findDbCountGroupByPositn(dis);
							modelMap.put("dbList", dbList);
						}else if(menu.equals("m_33_4")){//我的提醒
							Date edate = DateFormat.formatDate(new Date(), "yyyy-MM-dd");
							Date bdate = DateFormat.getDateBefore(edate, "day", -1, 2);
							modelMap.put("unCheckBdate", DateFormat.getStringByDate(bdate, "yyyy-MM-dd"));
							modelMap.put("unCheckEdate", DateFormat.getStringByDate(edate, "yyyy-MM-dd"));
							//近三天有多少报货单未审核
							HashMap<String, Object> chkstomMap = new HashMap<String, Object>();
							Chkstom chkstom = new Chkstom();
							chkstom.setAcct(acct);
							chkstom.setbMaded(bdate);
							chkstom.seteMaded(edate);
							chkstom.setFirm(positnCode);
							chkstomMap.put("chkstom", chkstom);
							Page page = new Page();
							chkstomMisService.findByKey(chkstomMap,page);
							modelMap.put("chkstomCount", page.getCount());
							//近三天有多少入库单未审核
							Chkinm chkinm = new Chkinm();
							chkinm.setInout("in");
							chkinm.setPositn(thePositn);
							chkinm.setMaded(bdate);
							chkinmMisService.findAllChkinm("unCheck",chkinm,page,edate,session.getAttribute("locale").toString());
							modelMap.put("chkinmCount", page.getCount());
							//近三天多少直发单未审核
							chkinm.setInout("zf");
							chkinmMisService.findAllChkinm("unCheck",chkinm,page,edate,session.getAttribute("locale").toString());
							modelMap.put("chkinmzfCount", page.getCount());
							//近三天有多少出库单未审核
							Chkoutm chkoutm = new Chkoutm();
							chkoutm.setAcct(acct);
							Positn positn = new Positn();
							positn.setUcode(positnCode);
							List<Positn> list = positnService.findPositnSuperNOPage(positn);//查询该分店的档口
							List<String> listPositn = new ArrayList<String>();
							for(Positn posi:list){
								listPositn.add(posi.getCode());
							}
							listPositn.add(thePositn.getCode());//加上分店本身
							chkoutm.setListPositn(listPositn);
							chkoutMisService.findChkoutm(chkoutm, bdate, edate, page,session.getAttribute("locale").toString(),"ck");// 不查调拨到其他门店的
							modelMap.put("chkoutmCount", page.getCount());
							//近三天调拨出库未审核的
							chkoutMisService.findChkoutm(chkoutm, bdate, edate, page,session.getAttribute("locale").toString(),"db");// 不查调拨到其他门店的
							modelMap.put("chkoutmdbCount", page.getCount());
						}else if(menu.equals("m_33_3")){//每日指引
							//查询我的桌面配置
							List<GuideConfig> list = myDeskMisbohService.findGuideConfig();
							for(GuideConfig gc : list){
								Dis dis = new Dis();
								dis.setFirmCode(positnCode);
								dis.setInd(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
								if("1".equals(gc.getPk_guideconfig())){//1.直配验货
									int count = myDeskMisbohService.findDireCount(dis);
									if(count == 0){
										gc.setHint("0");
										gc.setHintClass("greenhint");
									}else{
										gc.setHint(count+"");
										gc.setHintClass("redhint");
									}
								}else if("2".equals(gc.getPk_guideconfig())){//2.统配验货
									int count = myDeskMisbohService.findOutCount1(dis);
									if(count == 0){
										gc.setHint("0");
										gc.setHintClass("greenhint");
									}else{
										gc.setHint(count+"");
										gc.setHintClass("redhint");
									}
								}else if("3".equals(gc.getPk_guideconfig())){//3.调拨验货
									int count = myDeskMisbohService.findDbCount(dis);
									if(count == 0){
										gc.setHint("0");
										gc.setHintClass("greenhint");
									}else{
										gc.setHint(count+"");
										gc.setHintClass("redhint");
									}
								}else if("4".equals(gc.getPk_guideconfig())){//4.成本核减
									int count = myDeskMisbohService.findCostitemspcodeCount(dis);
									if(count == 0){
										gc.setHint("未做");
										gc.setHintClass("redhint");
									}else{
										gc.setHint("已做");
										gc.setHintClass("greenhint");
									}
								}else if("5".equals(gc.getPk_guideconfig())){//5.水电气录入
									//将编码放入实体类
									Store store = new Store();
									store.setVcode(positnCode);
									//返回根据当前门店编码获取的当前门店主键
									String pk_store = commonMISBOHService.getStore(store).getPk_store();
									OtherStockDataO oo = new OtherStockDataO();
									oo.setPk_store(pk_store);
									oo.setWorkdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
									if(pk_store == null){
										gc.setHint("水电气未录入");
										gc.setHintClass("redhint");
									}else{
										List<OtherStockDataO> ooList = myDeskMisbohService.findOtherStockdataCount(oo);
										if(ooList.size() > 0){
											for(OtherStockDataO o : ooList){
												if(1 == o.getType()){//水
													gc.setHint("电表未录入");
													gc.setHintClass("redhint");
												}else{
													gc.setHint("水表未录入");
													gc.setHintClass("redhint");
													break;
												}
												if(2 == o.getType()){//电
													gc.setHint("气表未录入");
													gc.setHintClass("redhint");
												}else{
													gc.setHint("电表未录入");
													gc.setHintClass("redhint");
													break;
												}
												if(3 == o.getType()){//气
													gc.setHint("水电气已录入");
													gc.setHintClass("greenhint");
												}else{
													gc.setHint("气表未录入");
													gc.setHintClass("redhint");
													break;
												}
											}
										}else{
											gc.setHint("水电气未录入");
											gc.setHintClass("redhint");
										}
									}
								}else if("6".equals(gc.getPk_guideconfig())){//6.成品损耗
									PostionLoss pl = new PostionLoss();
									pl.setDept(positnCode);
									pl.setWorkdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
									pl.setTyp(1);//成品
									pl.setState(2);//已确认
									int count = myDeskMisbohService.findPostionLossCount(pl);
									if(count == 0){
										gc.setHint("未录入");
										gc.setHintClass("redhint");
									}else{
										gc.setHint("已录入");
										gc.setHintClass("greenhint");
									}
								}else if("7".equals(gc.getPk_guideconfig())){//7.半成品损耗
									PostionLoss pl = new PostionLoss();
									pl.setDept(positnCode);
									pl.setWorkdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
									pl.setTyp(2);//半成品
									pl.setState(2);//已确认
									int count = myDeskMisbohService.findPostionLossCount(pl);
									if(count == 0){
										gc.setHint("未录入");
										gc.setHintClass("redhint");
									}else{
										gc.setHint("已录入");
										gc.setHintClass("greenhint");
									}
								}else if("8".equals(gc.getPk_guideconfig())){//8.原料损耗
									PostionLoss pl = new PostionLoss();
									pl.setDept(positnCode);
									pl.setWorkdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
									pl.setTyp(3);//物资
									pl.setState(2);//已确认
									int count = myDeskMisbohService.findPostionLossCount(pl);
									if(count == 0){
										gc.setHint("未录入");
										gc.setHintClass("redhint");
									}else{
										gc.setHint("已录入");
										gc.setHintClass("greenhint");
									}
								}else if("9".equals(gc.getPk_guideconfig())){//9.盘点
									Chkstoref cf = new Chkstoref();
									cf.setDept(positnCode);
									cf.setWorkDate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
									cf.setState("1");//已审核
									int count = myDeskMisbohService.findChkstorefCount(cf);
									if(count == 0){
										gc.setHint("未做");
										gc.setHintClass("redhint");
									}else{
										gc.setHint("已做");
										gc.setHintClass("greenhint");
									}
								}else if("10".equals(gc.getPk_guideconfig())){//10.千元用量报货
									int count = myDeskMisbohService.findChkstomCount(dis);
									if(count == 0){
										gc.setHint("未做");
										gc.setHintClass("redhint");
									}else{
										gc.setHint("已做");
										gc.setHintClass("greenhint");
									}
								}else if("11".equals(gc.getPk_guideconfig())){//11.日结
									Date date = myDeskMisbohService.findWorkdateByPositn(positnCode);
									if(date == null){
										gc.setHint("未日结");
										gc.setHintClass("redhint");
									}else{
										date = DateFormat.formatDate(date, "yyyy-MM-dd");
										Date nowDate = DateFormat.formatDate(new Date(), "yyyy-MM-dd");
										Calendar calendar = Calendar.getInstance();
										calendar.setTime(nowDate);
										calendar.add(Calendar.DATE,1);//把日期往后增加一天.整数往后推,负数往前移动
										Date nextDate=calendar.getTime(); //这个时间就是日期往后推一天的结果 
										nextDate = DateFormat.formatDate(nextDate, "yyyy-MM-dd");
										if(date.getTime() == nextDate.getTime()){
											gc.setHint("已日结");
											gc.setHintClass("greenhint");
											modelMap.put("nowDate", DateFormat.getStringByDate(nowDate, "yyyy-MM-dd"));
											modelMap.put("nextDate", DateFormat.getStringByDate(date, "yyyy-MM-dd"));
										}else{
											gc.setHint("未日结");
											gc.setHintClass("redhint");
										}
									}
								}else if("12".equals(gc.getPk_guideconfig())){//12.员工餐出库
									Chkoutm chkoutm = new Chkoutm();
									chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
									Positn positn = new Positn();
									positn.setCode(positnCode);
									chkoutm.setPositn(positn);
									chkoutm.setTyp(DDT.YGCCK);
									chkoutm.setChecby("c");
									Date date = DateFormat.formatDate(new Date(), "yyyy-MM-dd");
									List<Chkoutm> listCk = chkoutMisService.findChkoutm(chkoutm, date, date, null,session.getAttribute("locale").toString(),"ck");// 不查调拨到其他门店的
									if(listCk.size() == 0){
										gc.setHint("未做");
										gc.setHintClass("redhint");
									}else{
										gc.setHint("已做");
										gc.setHintClass("greenhint");
									}
								}
							}
							modelMap.put("guideConfigList", list);
						}
					}else{//没有设置所属分店要提示
						modelMap.put("accountPositn", "N");
					}
				}
			}
		}
		
		modelMap.put("currDate", DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		modelMap.put("startDate", startDateString);
		modelMap.put("yesterDay", yesterDayString);
		modelMap.put("menu",null==workBench?"":workBench.getMenu());
		modelMap.put("unCheckedList",unCheckedList);
		modelMap.put("announcementList",announcementList);
		modelMap.put("supplyListOver",supplyListOver);
		modelMap.put("supplyListLow",supplyListLow);
		modelMap.put("sppriceList",sppriceLIST);
		modelMap.put("stackBillSumList", stackBillSumList);
		modelMap.put("outBillSumList", outBillSumList);
		modelMap.put("firmAmtList",firmAmtList);
		modelMap.put("uploadList",uploadList);
		modelMap.put("storeYyeList",storeYyeList);
		ReadProperties rp = new ReadProperties();
		String EXT = rp.getStrByParam("project_ext");
		if("_xwy".equals(EXT))
			return new ModelAndView(MainInfoConstants.MAIN+EXT,modelMap);
		else
			return new ModelAndView(MainInfoConstants.MAIN,modelMap);
	}
	
	/**
	 * 用户工作台配置界面
	 */
	@RequestMapping(value = "/open")
	public ModelAndView openTableMain(ModelMap modelMap,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId = (String)(session.getAttribute("accountId"));
		//根据账号查询角色中的  menu字段，确定显示哪些模块
		List<Role> roleList=roleService.findRoleMainTableList(accountId);
		String allMenu = roleList.get(0).getMenu();
		for(Role role:roleList){
			allMenu = ArrayUtil.getSumString(allMenu, role.getMenu(), ",");
		}
		//查询用户自定义菜单信息
		WorkBench workBench = mainInfoService.findWorkBench(accountId);
		
		if(null!=workBench){
			String showMenu = ArrayUtil.getMixedString(allMenu, workBench.getMenu(), ",");
			modelMap.put("menu",showMenu);
		}
		modelMap.put("roleMenu",allMenu);
		return new ModelAndView(MyDeskMisbohConstants.MYDESKMANAGE,modelMap);
	}
	
	/**
	 * 保存用户配置的工作台
	 */
	@RequestMapping(value = "/saveWorkbench") 
	@ResponseBody
	public String saveWorkbench(ModelMap modelMap,String menu,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId = (String)(session.getAttribute("accountId"));
		WorkBench workbench = mainInfoService.findWorkBench(accountId);
		
		if(null != workbench){
			workbench.setAccountid(accountId);
			workbench.setMenu(menu);
			workbench.setId(workbench.getId());
			mainInfoService.updateWorkBench(workbench);
		}else{
			workbench = new WorkBench();
			workbench.setAccountid(accountId);
			workbench.setMenu(menu);
			workbench.setId(CodeHelper.createUUID());
			mainInfoService.saveWorkBench(workbench);
		}
		
		return "ok";
	}
	
	/**
	 * 跳转到常用操作选择页面
	 */
	@RequestMapping(value = "/addUsedTag") 
	public ModelAndView addUsedTag(ModelMap modelMap,String menu,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId = (String)(session.getAttribute("accountId"));
		Role role = new Role();
		role.setId(accountId);
		//查询角色现有的操作列表
		modelMap.put("roleOperateList",myDeskMisbohService.findRoleOperateList(role));
		String tagIdList = "";
		if(mainInfoService.findWorkBench(accountId)!=null ){
			tagIdList = mainInfoService.findWorkBench(accountId).getTagpage();
		}
		modelMap.put("tagIdList", tagIdList);
		return new ModelAndView(MyDeskMisbohConstants.LISTMODULE,modelMap);
	}
	/**
	 * 保存常用操作id
	 */
	@RequestMapping(value = "/saveUsedModel") 
	@ResponseBody
	public String saveUsedModel(ModelMap modelMap,HttpSession session,String roleOperateId) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId = (String)(session.getAttribute("accountId"));
		WorkBench workBench = new WorkBench();
		workBench.setAccountid(accountId);
		workBench.setTagpage(roleOperateId);
		//查询角色现有的操作列表
		return mainInfoService.saveUsedModel(workBench);
	}
	
	/**
	 * 日结
	 */
	@RequestMapping(value = "/endOfDay") 
	@ResponseBody
	public int endOfDay(ModelMap modelMap,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			String positnCode = thePositn.getCode();
			Date date = myDeskMisbohService.findWorkdateByPositn(positnCode);
			if(date == null){
				date = new Date();
			}
			date = DateFormat.formatDate(date, "yyyy-MM-dd");
			Date nowDate = DateFormat.formatDate(date, "yyyy-MM-dd");
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(nowDate);
			calendar.add(Calendar.DATE,1);//把日期往后增加一天.整数往后推,负数往前移动
			Date nextDate=calendar.getTime(); //这个时间就是日期往后推一天的结果 
			nextDate = DateFormat.formatDate(nextDate, "yyyy-MM-dd");
			myDeskMisbohService.updateWorkDateByPositn(positnCode,nextDate);
			return 1;
		}
		return 0;
	}
	
	
	
	
	//查询图标
	@RequestMapping("/getXml")
	public void getXmlForGroupSales(HttpServletResponse response,HttpSession session,String id,String days) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId = (String)(session.getAttribute("accountId"));
		String firmids = "";
		AccountFirm accountFirm = new AccountFirm();
		accountFirm.setAccountId(accountId);
		List<AccountFirm> list = accountFirmService.findAccountFirm(accountFirm);
		if(list.size()>0){
			for(AccountFirm afirm : list)
				firmids += afirm.getFirmId()+",";
			firmids = firmids.substring(0,firmids.length()-1);
		}
		String acct = session.getAttribute("ChoiceAcct").toString();
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("acct", acct);
		map.put("topN", 10);
		
		if(id.equals("m_2_1")){
			mainInfoService.findXmlForSupplyOver(response, map).output();
		}else if(id.equals("m_2_2")){
			mainInfoService.findXmlForSupplyLow(response, map).output();
		}else if(id.equals("m_2_3")){
			if(null==days){
				days="30";
			}
			map.put("endDate", DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			map.put("startDate", DateFormat.formatDate(DateFormat.getDateBefore(new Date(), "day", -1, Integer.valueOf(days)), "yyyy-MM-dd"));
			mainInfoService.findXmlForPurchaseInSum(response,map).output();
		}else if(id.equals("m_2_4")){
			if(null==days){
				days="30";
			}
			map.put("endDate", DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			map.put("startDate", DateFormat.formatDate(DateFormat.getDateBefore(new Date(), "day", -1, Integer.valueOf(days)), "yyyy-MM-dd"));
			mainInfoService.findXmlForOutBillSum(response,map).output();
		}else if(id.equals("m_2_5")){
			
		}else if(id.equals("m_2_6")){
			mainInfoService.findXmlForOutTop10(response).output();
		}else if(id.equals("m_2_8")){
			DataSourceSwitch.setDataSourceType(DataSourceInstances.TELE);//选择数据源
			Condition condition = new Condition();
			condition.setFirmid(firmids);
			condition.setBdat(DateFormat.getDateBefore(new Date(),"day",-1,1));
			condition.setEdat(DateFormat.getDateBefore(new Date(),"day",-1,1));
			mainInfoService.findModAmt(response, condition).output();
		}else if(id.equals("m_2_9")){
			DataSourceSwitch.setDataSourceType(DataSourceInstances.TELE);//选择数据源
			Condition condition = new Condition();
			condition.setFirmid(firmids);
			condition.setBdat(DateFormat.getFirstDayOfCurrMonth());
			condition.setEdat(DateFormat.getEndDayOfCurrMonth(new Date()));
	//		Condition conBefore = new Condition();
	//		conBefore.setBdat(DateFormat.getDateByString("2013-01-01", "yyyy-MM-dd"));
	//		conBefore.setEdat(DateFormat.getDateByString("2013-01-31", "yyyy-MM-dd"));
			mainInfoService.findModLineAmt(response,condition).output();
		}else if(id.equals("m_2_10")){
			DataSourceSwitch.setDataSourceType(DataSourceInstances.TELE);//选择数据源
			Condition condition = new Condition();
			condition.setFirmid(firmids);
			condition.setBdat(DateFormat.getDateBefore(new Date(),"day",-1,1));
			condition.setEdat(DateFormat.getDateBefore(new Date(),"day",-1,1));
			condition.setDetailMod(1);
			mainInfoService.findCaiPinLeiBiePaiHang(response,condition).output();
		}else if(id.equals("m_2_13")){
			DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
			mainInfoService.findLbtjList(response).output();
		}else if(id.equals("m_2_14")){
			DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
			mainInfoService.findSczbList(response).output();
		}else if(id.equals("m_66_3")){//昨日营业额
			DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
			mainInfoService.findModMultiAmtList(response).output();
		}else if(id.equals("m_66_4")){//本月营业额走势图
			DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
	 		mainInfoService.jituanyingyeeyuefenxi(response).output();
		}else if(id.equals("m_66_5")){//菜品类别排行
			DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
	 		mainInfoService.findCaiPinLeiBiePaiHang(response).output();
		}else if(id.equals("m_76_4")){//本月营业额走势图 快餐
			DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
	 		mainInfoService.findTotalMoneyQushiByDays(response).output();
		}else if(id.equals("m_76_5")){//菜品类别排行 快餐
			DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
			mainInfoService.findCplbzbList(response).output();
		}
	}
	
	/**
	 * 查询全部的 超上限物资
	 */
	@RequestMapping(value = "/overLimit")
	public ModelAndView supplyOverLimit(ModelMap modelMap,Page page,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		Supply supply = new Supply();
		supply.setAcct(acct);
		List<Supply> announcementList= mainInfoService.findAllSupplyOver(supply, page);
		modelMap.put("pageobj", page);
		modelMap.put("supplyList",announcementList);
		return new ModelAndView(MainInfoConstants.OVER_LIMIT,modelMap);
	}
	
	/**
	 * 查询全部的 不足上限物资
	 */
	@RequestMapping(value = "/lessLimit")
	public ModelAndView supplyLessLimit(ModelMap modelMap,Page page,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		Supply supply = new Supply();
		supply.setAcct(acct);
		List<Supply> announcementList= mainInfoService.findAllSupplyLow(supply, page);
		modelMap.put("pageobj", page);
		modelMap.put("supplyList",announcementList);
		return new ModelAndView(MainInfoConstants.LESS_LIMIT,modelMap);
	}
	
}
