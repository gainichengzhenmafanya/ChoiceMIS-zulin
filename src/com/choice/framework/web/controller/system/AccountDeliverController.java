package com.choice.framework.web.controller.system;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.system.AccountDeliverConstants;
import com.choice.framework.domain.system.AccountDeliver;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.service.system.AccountDeliverService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.Deliver;
import com.choice.scm.service.DeliverService;


@Controller
@RequestMapping(value = "accountDeliver")
public class AccountDeliverController {
	
	@Autowired
	private AccountDeliverService accountDeliverService;
	@Autowired
	private DeliverService deliverService;
	@Autowired
	private LogsMapper logsMapper;
	
	/**
	 * 查询供应商 上
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addDeliverBatch")
	public ModelAndView addDeliverBatch(ModelMap modelMap, String accountId) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		AccountDeliver accountDeliver = new AccountDeliver();
		accountDeliver.setAccountId(accountId);
		List<AccountDeliver> accountDeliverList = accountDeliverService.findAccountDeliver(accountDeliver);
//		modelMap.put("defaultName", (null==defaultName||"".equals(defaultName))?defaultName:URLDecoder.decode(defaultName, "UTF-8"));
		String deliversId = "";
		String deliversDes = "";
		for (int i=0;i<accountDeliverList.size();i++) {
			if (i!=0){
				deliversId += ",";
				deliversDes += ",";
			}
			deliversId += accountDeliverList.get(i).getDeliverId();
			deliversDes += accountDeliverList.get(i).getDeliverDes();
		}
		
		modelMap.put("defaultCode", deliversId);
		modelMap.put("defaultName", deliversDes);
		modelMap.put("accountId", accountId);
		return new ModelAndView(AccountDeliverConstants.ADD_DELIVERBATCH, modelMap);
	}
	
	/**
	 * 查询供应商 下左
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectNDeliver")
	public ModelAndView selectNDeliver(ModelMap modelMap) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("delivers", deliverService.findDeliverType());
		return new ModelAndView(AccountDeliverConstants.SELECT_NDELIVER, modelMap);
	}
	
	/**
	 * 查询供应商 下右
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectTableNDeliver")
	public ModelAndView selectTableNDeliver(ModelMap modelMap, Deliver deliver, Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("deliverList", deliverService.findAllDelivers(deliver));
		modelMap.put("pageobj", page);
		modelMap.put("deliver", deliver);//查询记忆
		return new ModelAndView(AccountDeliverConstants.SELECT_TABLENDELIVER, modelMap);
	}
	
	/**
	 * 查询账户具有的供应商
	 * @param accountDeliver
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findDeliverByAccountId")
	@ResponseBody
	public Object findDeliverByAccountId(AccountDeliver accountDeliver, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,"查询账户具有的分店",session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return accountDeliverService.findAccountDeliver(accountDeliver);
	}
	
	/**
	 * 修改账户具有的供应商
	 * @param accountDeliver
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateAccounDeliver")
	@ResponseBody
	public void updateAccountFirm(AccountDeliver accountDeliver, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改账户具有的供应商权限",session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		
		accountDeliverService.saveAccountDeliver(accountDeliver);
	}
	
}

