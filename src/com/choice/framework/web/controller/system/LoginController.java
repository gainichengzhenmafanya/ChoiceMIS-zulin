package com.choice.framework.web.controller.system;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.common.servlet.LoadCompInfoServlet;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.constants.system.LoginConstants;
import com.choice.framework.domain.system.Account;
import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.domain.system.Module;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.redis.RedisConfig;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.service.system.LoginService;
import com.choice.framework.service.system.ModuleService;
import com.choice.framework.util.CacheUtil;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.framework.util.ProgramConstants;
import com.choice.framework.vo.AccountCache;
import com.choice.misboh.domain.costReduction.MisSupply;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.WorkBench;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.MainInfoService;
import com.choice.scm.service.PositnService;

@Controller
@RequestMapping(value = "login")
@SessionAttributes({"accountId","accountName","ChoiceAcct","locale","crmFirmId","crmFirmDes","ip","accountPositn","misSupply_cache"})
public class LoginController {
	
	@Autowired
	private LoginService loginService;
	@Autowired
	private LogsMapper logsMapper;
	@Autowired
	private AcctService acctService;
	@Autowired
	private MainInfoService mainInfoService;
	@Autowired
	private ModuleService moduleService;
	@Autowired
	private AccountPositnService accountPositnService;//查询当前登录用户的所属分店 wjf
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	@Autowired
	private PositnService positnService;
	
	/**
	 * 账号登录
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/loginIn")
	public ModelAndView loginIn(HttpServletResponse response,HttpSession session,HttpServletRequest request, ModelMap modelMap, Account account, 
			String remember,String locale, String userInfo) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//根据输入的企业号查询是否已加载到内存中,以及进行企业号的库中认证
		String info = account.getPk_group();
		if(null != info){
			info = info.toUpperCase();
		}
		//String groupId = account.getPk_group().toUpperCase();
		String pk_groups = LoadCompInfoServlet.groupmap.get(info);
		if(StringUtils.isBlank(info) || "0".equals(info) || StringUtils.isBlank(pk_groups)){ //说明企业号无效
			info = "无效企业号!";
		}else{
			// 根据企业编号，动态切换数据源
//			DataSourceSwitch.setDataSourceType("compJdbcTemplate_" + account.getPk_group());
			info = loginService.validateLogin(account,session);
		}
		//将企业号存入session
		session.setAttribute("pk_group", pk_groups);
		//System.out.println("=============================企业号为:" + session.getAttribute("pk_group") + "=====================================");
		pk_groups = null;
		if(info.equals(StringConstant.TRUE)){
			AccountCache accountCache = loginService.loadAuthorityInfo(account, session);
			modelMap.put("accountCache", accountCache);
			modelMap.put("accountId", accountCache.getAccount().getId());
			
			//查询当前用户所属分店wjf2014.11.21
			AccountPositn accountPositn=accountPositnService.findAccountById(accountCache.getAccount().getId());
			if(null != accountPositn.getPositn()){
				modelMap.put("accountPositn", accountPositn.getPositn());
				//查找该门店使用物资
				String positnCode = accountPositn.getPositn().getCode();
			    MisSupply s = new MisSupply();
				s.setAcct("1");
				s.setSp_position(positnCode);
				s.setOrderBy("length(s.sp_init)");
				List<MisSupply> listSupply = commonMISBOHService.findAllSupply(s);
				String opencluster = RedisConfig.getString("opencluster");
				if(null != opencluster && "1".equals(opencluster)){
					modelMap.put("misSupply_cache", listSupply);
				} else {
					String pk_group = account.getPk_group();
					//如果有缓存 清缓存
					CacheUtil cacheUtil = CacheUtil.getInstance();
					@SuppressWarnings("unchecked")
					List<MisSupply> misSupply_cache = (List<MisSupply>)cacheUtil.get("misSupply_cache", pk_group+positnCode);	
					if(null != misSupply_cache){
						cacheUtil.flush("misSupply_cache", pk_group+positnCode);
					}
					//将物资放到缓存
					cacheUtil.put("misSupply_cache", pk_group+positnCode, listSupply);
				}
			}
		    
			modelMap.put("account", account);
			modelMap.put("accountName", account.getName());
			modelMap.put("locale", locale);
			
			//判断是否自动登录
			Cookie rememberCookie = new Cookie("remember", remember);
			rememberCookie.setMaxAge(10 * 24 * 60 * 60);
			rememberCookie.setPath("/");
			response.addCookie(rememberCookie);
			if (StringConstant.TRUE.equals(remember)) {
				Cookie pk_group = new Cookie("pk_group",account.getPk_group());
				Cookie nameCookie = new Cookie("accountName", account.getName());
				Cookie pwCookie = new Cookie("password", account.getPassword());
				Cookie localeCookie = new Cookie("locale",locale);
				pk_group.setMaxAge(10 * 24 * 60 * 60);
				pk_group.setPath("/");
				nameCookie.setMaxAge(10 * 24 * 60 * 60);
				nameCookie.setPath("/");
				pwCookie.setMaxAge(10 * 24 * 60 * 60);
				pwCookie.setPath("/");
				localeCookie.setMaxAge(10 * 24 * 60 * 60);
				localeCookie.setPath("/");
				response.addCookie(pk_group);
				response.addCookie(nameCookie);
				response.addCookie(pwCookie);
				response.addCookie(localeCookie);
			} else {
				Cookie pk_group = new Cookie("pk_group",account.getPk_group());
				Cookie nameCookie = new Cookie("accountName", account.getName());
				Cookie pwCookie = new Cookie("password", "");
				Cookie localeCookie = new Cookie("locale","");
				pk_group.setMaxAge(10 * 24 * 60 * 60);
				pk_group.setPath("/");
				nameCookie.setMaxAge(10 * 24 * 60 * 60);
				nameCookie.setPath("/");
				pwCookie.setMaxAge(0);
				pwCookie.setPath("/");
				localeCookie.setMaxAge(0);
				localeCookie.setPath("/");
				response.addCookie(pk_group);
				response.addCookie(nameCookie);
				response.addCookie(pwCookie);
				response.addCookie(localeCookie);
			}
			//当前帐套     当前会计月
			Map<String,Object> map = acctService.findAcctMainInfo();
			modelMap.put("mainInfo",map);
			if(!"Y".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH,"ynCenterMonthEnd"))){//如果启用门店月结
				if(null != accountPositn.getPositn()){
					Positn positn = positnService.findPositnByCode(accountPositn.getPositn());
					String monthh = "1";
					if(null != positn && null != positn.getMonthh() && !"".equals(positn.getMonthh())){
						monthh = positn.getMonthh();
					}
					map.put("month", monthh);
				}
			}
			modelMap.put("sysDate", DateFormat.getStringByDate(new Date(), "yyyy-MM-dd hh:mm:ss"));
			modelMap.put("ChoiceAcct","1");
			//获取当前登录用户定义的常用操作
			WorkBench workBench = mainInfoService.findWorkBench(accountCache.getAccount().getId());
			List<Module> tagList = new ArrayList<Module>();
			if(null!=workBench && null != workBench.getTagpage()){
				String ids = "'"+workBench.getTagpage().replace(",", "','")+"'";
				tagList = moduleService.findModuleByIds(ids);
			}
			modelMap.put("tagList",tagList);
			//加入登陆日志
			String ip=Util.getIpAddr(request);
			modelMap.put("ip", ip);
			Logs logs=new Logs(Util.getUUID(),accountCache.getAccount().getId(),new Date(),LoginConstants.LOGIN_EVENTS,LoginConstants.LOGIN_CONTENTS,ip,ProgramConstants.OVERALL);
			logsMapper.addLogs(logs);
			try {
				//版本号  
				String sbohVersion = ForResourceFiles.getValByKey("config-bsboh-version.properties","sbohVersion");
				modelMap.put("version", sbohVersion);
			} catch (Exception e) {
				String version = ForResourceFiles.getValByKey("config.properties","version");
				modelMap.put("version",version);
			}
			return new ModelAndView(LoginConstants.MAIN, modelMap);
		}
		else{
			Cookie pk_group = new Cookie("pk_group",account.getPk_group());
			Cookie nameCookie = new Cookie("accountName", account.getName());
			nameCookie.setMaxAge(10 * 24 * 60 * 60);
			nameCookie.setPath("/");
			pk_group.setMaxAge(10 * 24 * 60 * 60);
			pk_group.setPath("/");
			response.addCookie(pk_group);
			response.addCookie(nameCookie);
			modelMap.put("info", info);
			modelMap.put("loginOut", "loginOut");
			return new ModelAndView(LoginConstants.LOGIN, modelMap);
		}
		
	}
	
	/**
	 * 账号登出
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/loginOut")
	public ModelAndView loginOut(HttpServletResponse response,HttpSession session, ModelMap modelMap) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
	//	Cookie nameCookie = new Cookie("accountName", "");
		Cookie pwCookie = new Cookie("password", "");
		Cookie localeCookie = new Cookie("locale","");
		Cookie rememberCookie = new Cookie("remember", "F");
	//	nameCookie.setMaxAge(-1);
	//	nameCookie.setPath("/");
		pwCookie.setMaxAge(-1);
		pwCookie.setPath("/");
		localeCookie.setMaxAge(-1);
		localeCookie.setPath("/");
		rememberCookie.setMaxAge(-1);
		rememberCookie.setPath("/");
		
	//	response.addCookie(nameCookie);
		response.addCookie(pwCookie);
		response.addCookie(localeCookie);
		response.addCookie(rememberCookie);

		//session失效
		session.invalidate();
		modelMap.put("loginOut", "loginOut");
		return new ModelAndView(LoginConstants.LOGIN, modelMap);
	}
	
	/**
	 * 账号登出
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/loginNull")
	public ModelAndView loginNull(HttpServletResponse response,HttpSession session, ModelMap modelMap) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return null;
	}
	
	/**
	 * 注册跳转
	 */
	@RequestMapping(value = "/regist")
	public ModelAndView regist(HttpServletResponse response, ModelMap modelMap) throws Exception{
		String regist = loginService.findRegist();
		modelMap.put("regist", regist);
		return new ModelAndView("regist", modelMap);
	}
	/**
	 * 注册保存
	 */
	@RequestMapping(value = "/saveRegist")
	@ResponseBody
	public String saveRegist(HttpServletResponse response,HttpSession session, ModelMap modelMap,String registCode) throws Exception{
		return loginService.saveRegist(registCode,session);
	}
}

