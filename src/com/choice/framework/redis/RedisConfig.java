package com.choice.framework.redis;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/***
 * Redis 配置文件读取
 * @author prolong
 *
 */
public class RedisConfig {
	
	private static final Logger LOG = LoggerFactory.getLogger(RedisConfig.class);

	private static Configuration config;
	
	private RedisConfig(){}
	
	private static final String filePath = "/redis.properties";
	
	private static Configuration getConfig(){
		if(config == null)
			syncInit();
		return config;
	}
	
	private static synchronized void syncInit(){
		if(config != null) return;
		PropertiesConfiguration proConf = new PropertiesConfiguration();
		try {
			proConf.load(RedisConfig.class.getResource(filePath));
			config = proConf;
		} catch (ConfigurationException e) {
			config = null;
			LOG.error(e.getMessage(), e);
		}
	}
	
	public static String getString(String key) {
		return getConfig().getString(key);
	}
	
	public static String getString(String key, String defaultValue){
		return getConfig().getString(key, defaultValue);
	}
	
	public static int getInt(String key) {
		return getConfig().getInt(key);
	}
	
	public static int getInt(String key, int defaultValue) {
		return getConfig().getInt(key, defaultValue);
	}
	
	public static boolean getBoolean(String key){
		return getConfig().getBoolean(key);
	}
	
	public static boolean getBoolean(String key, boolean defaultValue) {
		return getConfig().getBoolean(key, defaultValue);
	}
	
}
