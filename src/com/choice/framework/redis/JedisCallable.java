package com.choice.framework.redis;

import java.util.List;
import java.util.concurrent.Callable;

import redis.clients.jedis.Jedis;

public class JedisCallable implements Callable<List<byte[]>> {
    
    /** jedis 客户端 **/
    private Jedis jedis = null;
    
    /** keys **/
    private byte[][] keys = null;;

    public JedisCallable(Jedis j, byte[]... keys) {
        this.jedis = j;
        this.keys = keys;
    }

    @Override
    public List<byte[]> call() throws Exception {
        return jedis.mget(keys);
    }

}
