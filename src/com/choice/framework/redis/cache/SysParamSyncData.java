package com.choice.framework.redis.cache;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SysParamSyncData implements Runnable {
	
	private static final Logger LOG = LoggerFactory.getLogger(MemCacheFactory.class);

	private List<SysParam> paramList;
	
	public SysParamSyncData (List<SysParam> paramList){
		this.paramList = paramList;
	}
	
	@Override
	public void run() {
		String name = Thread.currentThread().getName();
		boolean setRes = MemCacheFactory.setAll(paramList);
		LOG.info(name + "--" + ", 同步结果[{}]", setRes);
	}

}
