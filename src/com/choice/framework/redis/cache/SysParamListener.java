package com.choice.framework.redis.cache;

import java.util.List;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.choice.framework.exception.CRUDException;
import com.choice.scm.service.SysParamService;

public class SysParamListener implements ServletContextListener {

	private SysParamSyncData paramSyncData;
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		
		WebApplicationContext webApplicationContext= WebApplicationContextUtils.getWebApplicationContext(arg0.getServletContext());
		SysParamService sysParamService=(SysParamService) webApplicationContext.getBean("sysParamService");
		
		try {
			List<SysParam> out = sysParamService.getAll();
			if(out != null && !out.isEmpty()){
				paramSyncData = new SysParamSyncData(out);
				Thread paramThread = new Thread(paramSyncData);
				paramThread.setName("启动--同步系统参数线程");
				paramThread.start();
			}
		} catch (CRUDException e) {
			e.printStackTrace();
		}
	}
	

}
