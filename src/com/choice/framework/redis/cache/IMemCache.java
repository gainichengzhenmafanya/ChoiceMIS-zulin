package com.choice.framework.redis.cache;

import java.util.List;

public interface IMemCache {
	
	/***
	 * 根据域头, 获取对应的缓存信息
	 * @param fieldHeader
	 * @return
	 */
	List<SysParam> getAllParams(String fieldHeader);
	
	/***
	 * 根据组合key获取value信息
	 * @param key
	 * @return
	 */
	String getParamValueByKey(String ... key);
	
	/***
	 * 根据实体设置参数缓存
	 * @param param
	 * @return
	 */
	boolean setParam(SysParam param);

	/***
	 * 根据组合key设置参数缓存
	 * @param param
	 * @return
	 */
	boolean setParam(String ... param);
	
	/***
	 * 设置List实体参数缓存
	 * @param list
	 * @return
	 */
	boolean setParams(List<SysParam> list);
}
