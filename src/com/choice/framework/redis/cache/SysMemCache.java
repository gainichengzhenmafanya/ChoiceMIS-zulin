package com.choice.framework.redis.cache;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.choice.framework.redis.JedisServer;
import com.choice.framework.redis.MyShardedJedis;

/***
 * 系统参数缓存实体
 * @author prolong
 *
 */
public class SysMemCache implements IMemCache {
	
	private static final Logger LOG = LoggerFactory.getLogger(SysMemCache.class);
	
	private static final String KEY = "_SYS";
	
	private JedisServer jedisServer;
	
	private static ThreadLocal<SysMemCache> sysMemCache;
	
	private SysMemCache(){
		jedisServer = JedisServer.getJedisServer();
	}
	
	/***
	 * 获取系统参数缓存实体
	 * @return
	 */
	public static SysMemCache getInstance (){
		if(sysMemCache == null) sysMemCache = new SysMemCacheThreadLocal();
		SysMemCache scmCache = sysMemCache.get();
		if(scmCache == null) {
			scmCache = new SysMemCache();
		}
		return scmCache;
	}

	/***
	 * 获取系统参数全部参数
	 */
	@Override
	public List<SysParam> getAllParams(String fieldHeader) {
		MyShardedJedis jedis = jedisServer.getJedis();
		List<SysParam> scmParamList = null;
		try {
			Map<String, String> scmParams = jedis.hgetAll(KEY);
			if (scmParams == null || scmParams.isEmpty()) {
				return null;
			}
			scmParamList = new ArrayList<SysParam>();
			for (Map.Entry<String, String> map : scmParams.entrySet()) {
				SysParam param = new SysParam();
				param.setParamCode(map.getKey());
				param.setParamValue(map.getValue());
				scmParamList.add(param);
			}
			return scmParamList;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e );
			return null;
		} finally {
			jedisServer.returnJedis(jedis);
		}
	}

	/***
	 * 根据key获取缓存value
	 */
	@Override
	public String getParamValueByKey(String... key) {
		if(key == null|| StringUtils.isEmpty(key[0])) return null;
		MyShardedJedis jedis = jedisServer.getJedis();
		try {
			return jedis.hget(KEY, key[0]);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return null;
		}finally{
			jedisServer.returnJedis(jedis);
		}
	}

	/***
	 * 设置缓存参数
	 */
	@Override
	public boolean setParam(SysParam param) {
		if(param == null) return false;
		MyShardedJedis jedis = jedisServer.getJedis();
		long setRes = -1;
		try {
			setRes = jedis.hset(KEY, param.getParamCode(), param.getParamValue());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		} finally{
			jedisServer.returnJedis(jedis);
		}
		return setRes > -1;
	}
	
	/***
	 * 设置缓存参数
	 */
	@Override
	public boolean setParam(String ... params) {
		if(params == null || params.length != 2) return false;
		MyShardedJedis jedis = jedisServer.getJedis();
		long setRes = -1;
		try {
			setRes = jedis.hset(KEY, params[0], params[1]);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		} finally{
			jedisServer.returnJedis(jedis);
		}
		return setRes > -1;
	}
	
	/***
	 * 设置缓存参数
	 * @param paramMap
	 * @return
	 */
	public boolean setParams(Map<String, String> paramMap) {
		if(paramMap == null) return false;
		MyShardedJedis jedis = jedisServer.getJedis();
		try {
			for(Map.Entry<String, String> map: paramMap.entrySet()){
				long res = jedis.hset(KEY, map.getKey(), map.getValue());
				if(res < 0) {
					LOG.error("scm setParams failed , paramCode is [{}], paramValue is [{}]", map.getKey(), map.getValue());
				}
			}
			return true;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return false;
		} finally{
			jedisServer.returnJedis(jedis);
		}
	}
	
	/***
	 * 设置缓存参数
	 */
	@Override
	public boolean setParams(List<SysParam> paramList) {
		if(paramList == null ||paramList.isEmpty()) return false;
		MyShardedJedis jedis = jedisServer.getJedis();
		try {
			for(SysParam demo: paramList){
				long res = jedis.hset(KEY, demo.getParamCode(), demo.getParamValue());
				if(res < 0) {
					LOG.warn("scm setParams failed , paramCode is [{}], paramValue is [{}]", demo.getParamCode(), demo.getParamValue());
				}
			}
			return true;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return false;
		} finally{
			jedisServer.returnJedis(jedis);
		}
	}
	
	private static class SysMemCacheThreadLocal extends ThreadLocal<SysMemCache>{
		@Override
		protected synchronized SysMemCache initialValue() {
			return new SysMemCache();
		}
		
	}
	
	
}
