package com.choice.framework.redis.cache;

import static com.choice.assistant.constants.system.SysParamConstants.SBOH;
import static com.choice.assistant.constants.system.SysParamConstants.SYS;
import static com.choice.assistant.constants.system.SysParamConstants.USER;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/***
 * MemCacheFactory 缓存工厂
 * @author prolong
 *
 */
public class MemCacheFactory {
	
	private static final Logger LOG = LoggerFactory.getLogger(MemCacheFactory.class);
	
	/***
	 * 获取系统参数缓存
	 * @return
	 */
	public static SysMemCache getSysCache() {
		return SysMemCache.getInstance();
	}
	
	/***
	 * 获取企业参数缓存
	 * @return
	 */
	public static SbohMemCache getSbohCache(){
		return SbohMemCache.getInstance();
	}
	
	/***
	 * 获取企业参数缓存
	 * @return
	 */
	public static ScmMemCache getScmCache(){
		return ScmMemCache.getInstance();
	}
	
	/***
	 * 获取用户参数缓存
	 * @return
	 */
	public static UserMemCache getUserCache(){
		return UserMemCache.getInstance();
	}
	
	/***
	 * 获取缓存中所有参数(系统参数、企业参数、用户参数)
	 * @param vCode
	 * @param userName
	 * @return
	 */
	public static LinkedList<SysParam> getAll(String vCode, String userName){
		
		LinkedList<SysParam> linkedList = new LinkedList<SysParam>();
		
		// 获取系统参数
		List<SysParam> sysParams = getSysCache().getAllParams(null);
		if(LOG.isTraceEnabled())
			LOG.trace("RedisMemCache getAll method , getSysMemCache size is [{}]", (sysParams == null || sysParams.isEmpty())?0:sysParams.size());
		if(sysParams != null && !sysParams.isEmpty())
			linkedList.addAll(sysParams);
		
		// 获取企业参数
		List<SysParam> scmParams = getScmCache().getAllParams(vCode);
		if(LOG.isTraceEnabled())
			LOG.trace("RedisMemCache getAll method , getScmMemCache size is [{}]", (scmParams == null || scmParams.isEmpty())?0:scmParams.size());
		if(scmParams != null && !scmParams.isEmpty())
			linkedList.addAll(scmParams);
		
		// 门店参数
		List<SysParam> sbohParams = getSbohCache().getAllParams(vCode);
		if(LOG.isTraceEnabled())
			LOG.trace("RedisMemCache getAll method , getSbohMemCache size is [{}]", (sbohParams == null || sbohParams.isEmpty())?0:sbohParams.size());
		if(sbohParams != null && !sbohParams.isEmpty())
			linkedList.addAll(sbohParams);
		
		// 获取用户参数
		List<SysParam> userParams = getUserCache().getAllParams(userName);
		if(LOG.isTraceEnabled())
			LOG.trace("RedisMemCache getAll method , getUserMemCache size is [{}]", (userParams ==null || userParams.isEmpty())? 0:userParams.size());
		if(userParams != null && !userParams.isEmpty())
			linkedList.addAll(userParams);
		
		return linkedList;
	}
	
	/***
	 * 设置缓存中所有参数(系统参数、企业参数、用户参数)
	 * @param list
	 * @return
	 */
	public static boolean setAll(List<SysParam> list){
		if(LOG.isTraceEnabled())
			LOG.trace("RedisMemCache setAll method start , size is [{}]", (list ==null || list.isEmpty())?0:list.size());
		
		if(list == null || list.isEmpty()) 
			return true;
		
		try {
			for(SysParam param : list) {
				if(param == null) continue;
				// 设置系统参数
				if(SYS.equals(param.getType())){
					if(!getSysCache().setParam(param)){
						LOG.error("getScmCache setParam failed , paramCode is [{}], paramValue is [{}] ", param.getParamCode(), param.getParamValue());
						return false;
					};
				}
				// 设置门店参数
				else if (SBOH.equals(param.getType())){
					if(!getSbohCache().setParam(param)){
						LOG.error("getSbohCache setparam failed, paramCode is [{}], paramValue is [{}]", param.getParamCode(), param.getParamValue());
						return false;
					}
				}
				// 设置用户参数
				else if (USER.equals(param.getType())){
					if(!getUserCache().setParam(param)) {
						LOG.error("getUserCache setParam failed, paramCode is [{}], paramValue is [{}]", param.getParamCode(), param.getParamValue());
						return false;
					}
				}
				else {
					LOG.error("setAll failed for paramInformation error, param type is wrong, paramCode is [{}], paramValue is [{}], paramType is [{}]", new Object[]{param.getParamCode(), param.getParamValue(), param.getType()});
					throw new InternalError("参数信息错误, 参数类型不正确.");
				}
			}
		} catch (InternalError e) {
			LOG.error(e.getMessage(), e);
			return false;
		}
		
		return true;
	}

}
