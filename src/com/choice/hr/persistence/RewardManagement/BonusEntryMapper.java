package com.choice.hr.persistence.RewardManagement;

import java.util.List;
import java.util.Map;

import com.choice.hr.domain.RewardManagement.BonusEntry;

/**
 * 描述：奖惩管理——奖金录入
 * @author 马振
 * 创建时间：2015-6-9 上午10:15:32
 */
public interface BonusEntryMapper {

	/**
	 * 描述：左侧树
	 * @author 马振
	 * 创建时间：2015-6-9 上午11:02:55
	 * @param bonusEntry
	 * @return
	 */
	public List<Map<String, Object>> treeBonusEntry(BonusEntry bonusEntry);
	
	/**
	 * 描述：奖金录入列表
	 * @author 马振
	 * 创建时间：2015-6-9 上午11:03:08
	 * @param bonusEntry
	 * @return
	 */
	public List<BonusEntry> listBonusEntry(BonusEntry bonusEntry);
	
	/**
	 * 描述：获取需要录入奖金的员工
	 * @author 马振
	 * 创建时间：2015-6-9 下午2:51:34
	 * @param bonusEntry
	 * @return
	 */
	public List<BonusEntry> getEmpBonus(BonusEntry bonusEntry);
	
	/**
	 * 描述：新增录入奖金
	 * @author 马振
	 * 创建时间：2015-6-10 上午9:44:49
	 * @param bonusEntry
	 */
	public void addBonusEntry(List<BonusEntry> list);
}