package com.choice.hr.persistence.RewardManagement;

import java.util.List;

import com.choice.hr.domain.RewardManagement.RewardPunishment;

/**
 * 描述：奖惩登记
 * @author 马振
 * 创建时间：2015-6-5 下午3:21:48
 */
public interface RewardPunishmentMapper {

	/**
	 * 描述：查询奖惩登记
	 * @author 马振
	 * 创建时间：2015-6-5 下午3:22:06
	 * @param RewardPunishment
	 * @return
	 */
	public List<RewardPunishment> listRewardPunishment(RewardPunishment rewardPunishment);
	
	/**
	 * 描述：根据主键查询
	 * @author 马振
	 * 创建时间：2015-6-5 下午3:22:26
	 * @param RewardPunishment
	 * @return
	 */
	public RewardPunishment getByPk(RewardPunishment rewardPunishment);
	
	/**
	 * 描述：新增奖惩登记
	 * @author 马振
	 * 创建时间：2015-6-5 下午3:22:48
	 * @param rewardPunishment
	 */
	public void saveRewardPunishment(RewardPunishment rewardPunishment);
	
	/**
	 * 描述：修改奖惩登记
	 * @author 马振
	 * 创建时间：2015-6-5 下午3:23:15
	 * @param rewardPunishment
	 */
	public void updateRewardPunishment(RewardPunishment rewardPunishment);
	
	/**
	 * 描述：删除奖惩登记
	 * @author 马振
	 * 创建时间：2015-6-5 下午3:23:38
	 * @param rewardPunishment
	 */
	public void deleteRewardPunishment(RewardPunishment rewardPunishment);
}