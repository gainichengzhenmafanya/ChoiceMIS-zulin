package com.choice.hr.persistence.ScheduleAttendance;

import java.util.List;

import com.choice.hr.domain.ScheduleAttendance.ClassTableSetting;

/**
 * 描述：排班考勤——班表设定
 * @author 马振
 * 创建时间：2015-6-16 下午5:40:09
 */
public interface ClassTableSettingMapper {

	/**
	 * 描述：根据门店和日期查询班表设定
	 * @author 马振
	 * 创建时间：2015-6-17 上午9:24:41
	 * @param classTableSetting
	 * @return
	 */
	public List<ClassTableSetting> selectTableSetting(ClassTableSetting classTableSetting);
	
	/**
	 * 描述：添加新数据
	 * @author 马振
	 * 创建时间：2015-6-16 下午5:41:18
	 * @param listClassTableSetting
	 */
	public void saveData(List<ClassTableSetting> listClassTableSetting);
	
	/**
	 * 描述：删除数据
	 * @author 马振
	 * 创建时间：2015-6-16 下午6:19:58
	 * @param classTableSetting
	 */
	public void deleteData(ClassTableSetting classTableSetting);
	
	/**
	 * 描述：excel表格导出查询的数据
	 * @author 马振
	 * 创建时间：2015-6-19 下午2:42:56
	 * @param classTableSetting
	 * @return
	 */
	public List<ClassTableSetting> getListData(ClassTableSetting classTableSetting);
}