package com.choice.hr.persistence.ScheduleAttendance;

import java.util.List;
import java.util.Map;

import com.choice.hr.domain.ScheduleAttendance.CrewScheduling;

/**
 * 描述：排班考勤——排班查询
 * @author 马振
 * 创建时间：2015-5-28 下午1:08:00
 */
public interface QuerySchedulingMapper {

	/**
	 * 描述：排班查询左侧树
	 * @author 马振
	 * 创建时间：2015-5-28 下午1:08:24
	 * @param crewScheduling
	 * @return
	 */
	public List<Map<String, Object>> treeQueryScheduling(CrewScheduling crewScheduling);
	
	/**
	 * 描述：排班查询列表
	 * @author 马振
	 * 创建时间：2015-5-28 下午1:40:38
	 * @param crewScheduling
	 * @return
	 */
	public List<Map<String, Object>> listQuery(CrewScheduling crewScheduling);
	
	/**
	 * 描述：查询一段时间内的员工主键
	 * @author 马振
	 * 创建时间：2015-5-28 下午2:00:41
	 * @param crewScheduling
	 * @return
	 */
	public List<CrewScheduling> findPk(CrewScheduling crewScheduling);
}