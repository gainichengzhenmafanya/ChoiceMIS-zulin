package com.choice.hr.persistence.ScheduleAttendance;

import java.util.List;
import java.util.Map;

import com.choice.hr.domain.ScheduleAttendance.EmployeeScheduling;

/**
 * 描述：排班考勤——员工排班
 * @author 马振
 * 创建时间：2015-5-27 下午6:36:22
 */
public interface EmployeeSchedulingMapper {

	/**
	 * 描述：员工排班左侧树
	 * @author 马振
	 * 创建时间：2015-5-28 上午9:00:47
	 * @param employeeScheduling
	 * @return
	 */
	public List<Map<String, Object>> treeEmployeeScheduling(EmployeeScheduling employeeScheduling);
	
	/**
	 * 描述：员工排班列表
	 * @author 马振
	 * 创建时间：2015-5-28 上午9:01:02
	 * @param employeeScheduling
	 * @return
	 */
	public List<EmployeeScheduling> listEmployeeScheduling(EmployeeScheduling employeeScheduling);
	
	/**
	 * 描述：根据班组主键查询内容
	 * @author 马振
	 * 创建时间：2015-5-28 下午12:33:11
	 * @param team
	 * @return
	 */
	public EmployeeScheduling findDeptByTeam(EmployeeScheduling employeeScheduling);
	
	/**
	 * 描述：保存员工排班
	 * @author 马振
	 * 创建时间：2015-5-28 上午9:01:15
	 * @param employeeScheduling
	 */
	public void saveData(EmployeeScheduling employeeScheduling);
	
	/**
	 * 描述：删除员工排班
	 * @author 马振
	 * 创建时间：2015-5-28 上午9:01:28
	 * @param employeeScheduling
	 */
	public void deleteEmployee(EmployeeScheduling employeeScheduling);
}