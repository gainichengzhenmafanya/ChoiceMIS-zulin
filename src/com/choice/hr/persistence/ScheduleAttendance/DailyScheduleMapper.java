package com.choice.hr.persistence.ScheduleAttendance;

import java.util.List;

import com.choice.hr.domain.ScheduleAttendance.CrewScheduling;
import com.choice.hr.domain.ScheduleAttendance.Shift;

/**
 * 描述：排班考勤——每日排班
 * @author 马振
 * 创建时间：2015-6-11 下午4:24:05
 */
public interface DailyScheduleMapper {

	/**
	 * 描述：查询部门
	 * @author 马振
	 * 创建时间：2015-6-11 下午4:26:55
	 * @param crewScheduling
	 * @return
	 */
	public List<CrewScheduling> getDept(CrewScheduling crewScheduling);
	
	/**
	 * 描述：根据部门查询班组
	 * @author 马振
	 * 创建时间：2015-6-11 下午4:27:36
	 * @param shift
	 * @return
	 */
	public List<Shift> getTeamByDept(Shift shift);
	
	/**
	 * 描述：查询员工班次
	 * @author 马振
	 * 创建时间：2015-6-11 下午4:29:25
	 * @param crewScheduling
	 * @return
	 */
	public List<CrewScheduling> getEmpShift(CrewScheduling crewScheduling);
}