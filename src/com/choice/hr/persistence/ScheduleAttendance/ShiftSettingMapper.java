package com.choice.hr.persistence.ScheduleAttendance;

import java.util.List;

import com.choice.hr.domain.ScheduleAttendance.Shift;

/**
 * 描述：排班考勤——班次设置
 * @author 马振
 * 创建时间：2015-5-26 下午1:31:32
 */
public interface ShiftSettingMapper {

	/**
	 * 描述：班次列表
	 * @author 马振
	 * 创建时间：2015-5-26 下午1:32:39
	 * @param shift
	 * @return
	 */
	public List<Shift> listShiftSetting(Shift shift);
	
	/**
	 * 描述：获取最大编码+1
	 * @author 马振
	 * 创建时间：2015-5-26 下午1:32:51
	 * @param shift
	 * @return
	 */
	public Shift getMaxVcodeAndIsortno(Shift shift);
	
	/**
	 * 描述：新增数据
	 * @author 马振
	 * 创建时间：2015-5-26 下午1:33:05
	 * @param shift
	 */
	public void addShift(Shift shift);
	
	/**
	 * 描述：根据主键查询
	 * @author 马振
	 * 创建时间：2015-5-26 下午1:33:37
	 * @param shift
	 * @return
	 */
	public Shift findShiftByPk(Shift shift);
	
	/**
	 * 描述：修改数据
	 * @author 马振
	 * 创建时间：2015-5-26 下午1:33:50
	 * @param shift
	 */
	public void updateShift(Shift shift);
	
	/**
	 * 描述：删除班次
	 * @author 马振
	 * 创建时间：2015-5-26 下午1:34:10
	 * @param shift
	 */
	public void deleteShift(Shift shift);
	
	/**
	 * 描述：检验编码是否已存在
	 * @author 马振
	 * 创建时间：2015-5-26 下午1:34:29
	 * @param shift
	 * @return
	 */
	public String checkVcode(Shift shift);
}