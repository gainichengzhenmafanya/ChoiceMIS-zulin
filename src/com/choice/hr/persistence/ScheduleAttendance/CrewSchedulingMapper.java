package com.choice.hr.persistence.ScheduleAttendance;

import java.util.List;
import java.util.Map;

import com.choice.hr.domain.ScheduleAttendance.CrewScheduling;

/**
 * 描述：排班考勤——班组排班
 * @author 马振
 * 创建时间：2015-5-26 下午4:23:27
 */
public interface CrewSchedulingMapper {

	/**
	 * 描述：班组排班左侧树
	 * @author 马振
	 * 创建时间：2015-5-26 下午4:24:14
	 * @param crewScheduling
	 * @return
	 */
	public List<Map<String, Object>> treeCrewScheduling(CrewScheduling crewScheduling);
	
	/**
	 * 描述：班组排班列表
	 * @author 马振
	 * 创建时间：2015-5-26 下午4:24:53
	 * @param crewScheduling
	 * @return
	 */
	public List<CrewScheduling> listCrewScheduling(CrewScheduling crewScheduling);
	
	/**
	 * 描述：保存班组排班
	 * @author 马振
	 * 创建时间：2015-5-27 上午9:57:18
	 * @param crewScheduling
	 */
	public void saveData(CrewScheduling crewScheduling);
	
	/**
	 * 描述：删除班组排班
	 * @author 马振
	 * 创建时间：2015-5-27 上午9:57:43
	 * @param crewScheduling
	 */
	public void deleteCrew(CrewScheduling crewScheduling);
}