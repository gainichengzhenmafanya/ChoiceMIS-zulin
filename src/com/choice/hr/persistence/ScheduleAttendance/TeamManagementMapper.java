package com.choice.hr.persistence.ScheduleAttendance;

import java.util.List;
import java.util.Map;

import com.choice.hr.domain.PersonnelManagement.Employee;
import com.choice.hr.domain.ScheduleAttendance.Team;

/**
 * 描述：排班考勤
 * @author 马振
 * 创建时间：2015-5-25 下午1:48:26
 */
public interface TeamManagementMapper {

	/**
	 * 描述：左侧树
	 * @author 马振
	 * 创建时间：2015-5-25 下午1:48:56
	 * @param hrDept
	 * @return
	 */
	public List<Map<String, Object>> treeTeamManagement(Team team);
	
	/**
	 * 描述：班组列表
	 * @author 马振
	 * 创建时间：2015-5-25 下午2:10:27
	 * @param team
	 * @return
	 */
	public List<Team> listTeamManagement(Team team);
	
	/**
	 * 描述：获取最大编码和排序+1
	 * @author 马振
	 * 创建时间：2015-5-25 下午4:26:16
	 * @param team
	 * @return
	 */
	public Team getMaxVcodeAndIsortno(Team team);
	
	/**
	 * 描述：新增数据
	 * @author 马振
	 * 创建时间：2015-5-25 下午4:26:26
	 * @param team
	 */
	public void addTeam(Team team);
	
	/**
	 * 描述：根据主键查询数据字典
	 * @author 马振
	 * 创建时间：2015-5-25 下午4:26:36
	 * @param team
	 * @return
	 */
	public Team findTeamByPk(Team team);
	
	/**
	 * 描述：修改数据
	 * @author 马振
	 * 创建时间：2015-5-25 下午4:26:48
	 * @param team
	 */
	public void updateTeam(Team team);
	
	/**
	 * 描述：删除班组
	 * @author 马振
	 * 创建时间：2015-5-25 下午4:26:57
	 * @param team
	 */
	public void deleteTeam(Team team);
	
	/**
	 * 描述：检验编码是否已存在
	 * @author 马振
	 * 创建时间：2015-5-22 下午3:37:15
	 * @param Team
	 * @return
	 */
	public String checkVcode(Team team);
	
	/**
	 * 描述：检验排序是否已存在
	 * @author 马振
	 * 创建时间：2015-5-22 下午3:37:15
	 * @param Team
	 * @return
	 */
	public String checkIsortno(Team team);
	
	/**
	 * 描述：为员工分配班组
	 * @author 马振
	 * 创建时间：2015-5-27 下午6:03:31
	 * @param employee
	 * @return
	 */
	public void addTeamToEmployee(Employee employee);
	
	/**
	 * 描述：修改员工排班中员工所在班组
	 * @author 马振
	 * 创建时间：2015-6-10 下午3:14:21
	 * @param employee
	 */
	public void updateTeamInEmpsch(Employee employee);
}