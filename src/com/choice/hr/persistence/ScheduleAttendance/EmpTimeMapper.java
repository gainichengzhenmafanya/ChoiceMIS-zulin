package com.choice.hr.persistence.ScheduleAttendance;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.choice.hr.domain.ScheduleAttendance.EmpTime;

public interface EmpTimeMapper {

	/**
	 * 执行考勤数据保存
	 * @author 文清泉
	 * @param 2015年5月26日 下午2:55:38
	 * @param empTime
	 * @param scode
	 */
	void saveEmpTime(EmpTime empTime);
	
	/**
	 * 获取未计算工时考勤数据
	 * @author 文清泉
	 * @param scode 
	 * @param 2015年6月23日 上午10:57:36
	 * @return
	 */
	List<EmpTime> findEmpTime(@Param(value="scode")String scode);

	/**
	 * 避免重复上传造成数据重复
	 * @author 文清泉
	 * @param 2015年6月23日 上午10:59:13
	 * @param empTime
	 */
	void delEmpTime(EmpTime empTime);

	/**
	 * 计算考勤工时修改数据库
	 * @author 文清泉
	 * @param 2015年6月23日 下午5:31:37
	 * @param scode
	 * @param dat
	 */
	void updateWorkAttendance(@Param(value="scode")String scode, @Param(value="dat")String dat);

	/**
	 * 计算考勤工时 修改已计算过数据
	 * @author 文清泉
	 * @param 2015年6月23日 下午5:39:29
	 * @param scode
	 * @param dat
	 * @param iscal 
	 */
	void updateEmpTime(@Param(value="scode")String scode, @Param(value="dat")String dat, @Param(value="iscal")int iscal);

	/**
	 * 根据考勤号获取员工主键
	 * @author 文清泉
	 * @param 2015年6月23日 下午5:47:25
	 * @param code
	 * @return
	 */
	String findEmployeeBy(@Param(value="scode")String scode,@Param(value="code")String code);
}
