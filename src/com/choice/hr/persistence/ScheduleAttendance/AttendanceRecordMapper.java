package com.choice.hr.persistence.ScheduleAttendance;

import java.util.List;
import java.util.Map;

import com.choice.hr.domain.ScheduleAttendance.AttendanceRecord;
import com.choice.hr.domain.ScheduleAttendance.WorkAttendance;

/**
 * 描述：考勤记录
 * @author 马振
 * 创建时间：2015-5-28 下午5:20:28
 */
public interface AttendanceRecordMapper {

	/**
	 * 描述：左侧树
	 * @author 马振
	 * 创建时间：2015-6-2 上午9:08:22
	 * @param empTime
	 * @return
	 */
	public List<Map<String, Object>> treeAttendanceRecord(AttendanceRecord attendanceRecord);

	/**
	 * 描述：查询考勤记录
	 * @author 马振
	 * 创建时间：2015-6-2 上午10:09:21
	 * @param attendanceRecord
	 * @return
	 */
	public List<AttendanceRecord> listAttendanceRecord(AttendanceRecord attendanceRecord);
	
	/**
	 * 描述：考勤汇总
	 * @author 马振
	 * 创建时间：2015-6-2 下午1:49:30
	 * @param attendanceRecord
	 * @return
	 */
	public List<AttendanceRecord> listCount(AttendanceRecord attendanceRecord);

	/**
	 * 员工考勤 工时
	 * @author 文清泉
	 * @param 2015年6月19日 上午10:55:46
	 * @param workAttendance
	 * @param pk_store
	 * @param code
	 * @return
	 */
	public List<WorkAttendance> listWorkAttendance(WorkAttendance workAttendance);
	
	
	/**
	 * 保存员工考勤数据
	 * @author 文清泉
	 * @param 2015年6月19日 下午1:55:36
	 * @param wa
	 */
	public void saveWorkAttendance(WorkAttendance wa);

	/**
	 * 删除员工考勤数据  -  确保每天没人只有一个记录
	 * @author 文清泉
	 * @param 2015年6月19日 下午1:55:46
	 * @param workAttendance
	 */
	public void delWorkAttendance(WorkAttendance workAttendance);
	
	/**
	 * 描述：根据门店和日期统计一天的实际工时和理论工时
	 * @author 马振
	 * 创建时间：2015-6-25 上午9:12:30
	 * @param workAttendance
	 * @return
	 */
	public WorkAttendance getWorkHour(WorkAttendance workAttendance);
}