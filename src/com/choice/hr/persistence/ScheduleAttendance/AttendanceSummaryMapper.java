package com.choice.hr.persistence.ScheduleAttendance;

import java.util.List;

import com.choice.hr.domain.ScheduleAttendance.AttendanceSummary;

/**
 * 描述：排班考勤——考勤工时汇总
 * @author 马振
 * 创建时间：2015-6-24 下午1:50:23
 */
public interface AttendanceSummaryMapper {

	/**
	 * 描述：考勤工时汇总列表
	 * @author 马振
	 * 创建时间：2015-6-24 下午1:50:41
	 * @param attendanceSummary
	 * @return
	 */
	public List<AttendanceSummary> listAttendanceSummary(AttendanceSummary attendanceSummary);
}
