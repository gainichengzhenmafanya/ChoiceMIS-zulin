package com.choice.hr.persistence.PersonnelManagement;

import java.util.List;
import java.util.Map;

import com.choice.hr.domain.PersonnelManagement.HrDept;

/**
 * 描述：人力资源——部门接口类
 * @author 马振
 * 创建时间：2015-5-11 下午12:32:20
 */
public interface OrganizationStructureMapper {
	
	/**
	 * 描述：人力资源组织结构
	 * @author 马振
	 * 创建时间：2015-5-11 上午10:22:58
	 * @param pk_store
	 * @return
	 */
	public List<Map<String, Object>> personnelStructureTree(HrDept hrDept);
	
	/**
	 * 描述：查询人力部门信息
	 * @author 马振
	 * 创建时间：2015-5-11 下午12:30:05
	 * @param hrDept
	 * @return
	 */
	public List<HrDept> listHrDept(HrDept hrDept);
	
	/**
	 * 描述：新增人力资源部门
	 * @author 马振
	 * 创建时间：2015-5-11 下午3:11:40
	 * @param hrDept
	 * @return
	 */
	public void saveHrDept(HrDept hrDept);
	
	/**
	 * 描述：修改人力资源部门
	 * @author 马振
	 * 创建时间：2015-5-11 下午4:06:07
	 * @param hrDept
	 */
	public void updateHrDept(HrDept hrDept);
	
	/**
	 * 描述：启用、停用
	 * @author 马振
	 * 创建时间：2015-5-11 下午4:48:50
	 * @param hrDept
	 */
	public void updateEnableState(HrDept hrDept);
}
