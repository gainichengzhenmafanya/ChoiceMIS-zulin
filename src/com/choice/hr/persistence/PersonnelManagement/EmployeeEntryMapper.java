package com.choice.hr.persistence.PersonnelManagement;

import org.apache.ibatis.annotations.Param;

import com.choice.hr.domain.PersonnelManagement.Employee;

/**
 * 人事管理---入职
 * @author 文清泉
 * @param 2015年5月15日 下午5:35:25
 */
public interface EmployeeEntryMapper {

	/**
	 * 获取门店员工卡号
	 * @author 文清泉
	 * @param 2015年5月15日 下午5:43:28
	 * @param code
	 * @return
	 */
	public String findEmployeeCard(@Param(value="scode")String code);

	/**
	 * 保存入职记录
	 * @author 文清泉
	 * @param 2015年5月16日 上午9:36:42
	 * @param employee
	 */
	public int saveEmployeeEntry(Employee employee);

	/**
	 * 检测黑名单
	 * @author 文清泉
	 * @param 2015年5月16日 下午4:40:39
	 * @param vidcard
	 * @return
	 */
	public String checkBlackList(@Param(value="vidcard")String vidcard);

}
