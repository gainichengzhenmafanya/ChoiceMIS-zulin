package com.choice.hr.persistence.PersonnelManagement;

import java.util.List;
import java.util.Map;

import com.choice.hr.domain.DataDictionary.DataTyp;
import com.choice.hr.domain.PersonnelManagement.Employee;
import com.choice.hr.domain.PersonnelManagement.Family;
import com.choice.hr.domain.PersonnelManagement.HrDept;


/**
 * 描述：人事管理—人员列表接口类
 * @author 马振
 * 创建时间：2015-5-11 上午9:00:38
 */
public interface StaffListMapper {

	/**
	 * 描述：人员列表左侧树
	 * @author 马振
	 * 创建时间：2015-5-12 上午9:23:14
	 * @param employee
	 * @return
	 */
	public List<Map<String, Object>> staffListTree(HrDept hrDept);
	
	/**
	 * 描述：人员列表信息
	 * @author 马振
	 * 创建时间：2015-5-12 上午9:24:05
	 * @param employee
	 * @return
	 */
	public List<Employee> listEmployee(Employee employee);
	
	/**
	 * 描述：根据班组查询员工
	 * @author 马振
	 * 创建时间：2015-5-28 上午10:57:30
	 * @param employee
	 * @return
	 */
	public List<Employee> findByTeam(Employee employee);
	
	/**
	 * 描述：根据类别查询数据字典表中的数据
	 * @author 马振
	 * 创建时间：2015-5-14 下午4:28:23
	 * @param vtyp
	 * @return
	 */
	public List<DataTyp> listDataTyp(DataTyp dataTyp);
	
	/**
	 * 描述：根据主键查询数据字典
	 * @author 马振
	 * 创建时间：2015-5-16 下午4:17:08
	 * @param dataTyp
	 * @return
	 */
	public DataTyp findDataTypByPk(DataTyp dataTyp);
	
	/**
	 * 描述：查询员工的家庭成员
	 * @author 马振
	 * 创建时间：2015-5-15 上午9:50:44
	 * @param family
	 * @return
	 */
	public List<Family> listFamilyByEmp(Family family);
	
	/**
	 * 描述：修改人员信息
	 * @author 马振
	 * 创建时间：2015-5-14 下午2:48:59
	 * @param employee
	 */
	public void updateEmployee(Employee employee);
	
	/**
	 * 描述：新增家庭成员
	 * @author 马振
	 * 创建时间：2015-5-15 上午9:51:20
	 * @param family
	 */
	public void saveFamily(Family family);
	
	/**
	 * 描述：修改家庭成员信息
	 * @author 马振
	 * 创建时间：2015-5-15 下午3:23:05
	 * @param family
	 */
	public void updateFamily(Family family);
	
	/**
	 * 描述：员工转正
	 * @author 马振
	 * 创建时间：2015-5-19 下午2:49:27
	 * @param employee
	 */
	public void employeePositive(Employee employee);
}
