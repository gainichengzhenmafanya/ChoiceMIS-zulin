package com.choice.hr.persistence.DataDictionary;

import java.util.List;

import com.choice.hr.domain.DataDictionary.DataTyp;

/**
 * 描述：数据字典
 * @author 马振
 * 创建时间：2015-5-22 下午2:12:57
 */
public interface DataDictionaryMapper {

	/**
	 * 描述：根据类别查询数据字典表中的数据
	 * @author 马振
	 * 创建时间：2015-5-22 下午2:13:33
	 * @param dataTyp
	 * @return
	 */
	public List<DataTyp> listDataTyp(DataTyp dataTyp);

	/**
	 * 描述：获取一种类型下最大编码和排序+1
	 * @author 马振
	 * 创建时间：2015-5-22 下午4:14:10
	 * @param dataTyp
	 * @return
	 */
	public DataTyp getMaxVcodeAndIsortno(DataTyp dataTyp);
	
	/**
	 * 描述：新增数据
	 * @author 马振
	 * 创建时间：2015-5-22 下午3:48:29
	 * @param dataTyp
	 */
	public void addDataTyp(DataTyp dataTyp);
	
	/**
	 * 描述：根据主键查询数据字典
	 * @author 马振
	 * 创建时间：2015-5-22 下午2:13:44
	 * @param dataTyp
	 * @return
	 */
	public DataTyp findDataTypByPk(DataTyp dataTyp);
	
	/**
	 * 描述：修改数据
	 * @author 马振
	 * 创建时间：2015-5-22 下午2:17:21
	 * @param dataTyp
	 */
	public void updateDataTyp(DataTyp dataTyp);
	
	/**
	 * 描述：删除数据字典数据
	 * @author 马振
	 * 创建时间：2015-5-22 下午2:18:44
	 * @param dataTyp
	 */
	public void deleteDataTyp(DataTyp dataTyp);
	
	/**
	 * 描述：检验同类型下的编码是否已存在
	 * @author 马振
	 * 创建时间：2015-5-22 下午3:37:15
	 * @param dataTyp
	 * @return
	 */
	public String checkVcode(DataTyp dataTyp);
	
	/**
	 * 描述：检验同类型下的排序是否已存在
	 * @author 马振
	 * 创建时间：2015-5-22 下午3:37:15
	 * @param dataTyp
	 * @return
	 */
	public String checkIsortno(DataTyp dataTyp);
}