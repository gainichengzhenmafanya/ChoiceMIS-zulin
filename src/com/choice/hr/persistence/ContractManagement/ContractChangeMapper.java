package com.choice.hr.persistence.ContractManagement;

import java.util.List;

import com.choice.hr.domain.ContractManagement.Contract;
import com.choice.hr.domain.ContractManagement.ContractRegistration;

/**
 * 合同变更记录查询
 * @author 文清泉
 * @param 2015年5月19日 上午10:23:39
 */
public interface ContractChangeMapper {

	/**
	 * 读取员工合同登记记录
	 * @author 文清泉
	 * @param 2015年5月21日 上午9:39:49
	 * @param contract
	 * @return
	 */
	List<ContractRegistration> listContractChangeController(Contract contract);

}
