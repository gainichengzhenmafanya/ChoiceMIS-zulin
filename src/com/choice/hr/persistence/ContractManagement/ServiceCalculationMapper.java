package com.choice.hr.persistence.ContractManagement;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.hr.domain.ContractManagement.Contract;
import com.choice.hr.domain.ContractManagement.ServiceFormula;
import com.choice.hr.domain.PersonnelManagement.Employee;

/**
 * 合同工龄计算
 * @author 文清泉
 * @param 2015年5月21日 下午2:14:57
 */
public interface ServiceCalculationMapper {

	/**
	 * 执行删除功能
	 * @author 文清泉
	 * @param 2015年5月22日 下午2:31:45
	 * @param serviceFormula
	 */
	void delServiceCalculation(ServiceFormula serviceFormula);

	/**
	 * 执行保存
	 * @author 文清泉
	 * @param 2015年5月22日 下午2:31:52
	 * @param serviceFormula
	 */
	void saveServiceCalculation(ServiceFormula serviceFormula);

	/**
	 * 查询计算公式
	 * @author 文清泉
	 * @param 2015年5月22日 下午2:41:51
	 * @param serviceFormula
	 * @return
	 */
	List<ServiceFormula> listCalculationMode(ServiceFormula serviceFormula);

	/**
	 * 根据员工显示合同信息
	 * @author 文清泉
	 * @param 2015年5月22日 下午4:11:27
	 * @param contract
	 * @return
	 */
	List<Employee> listServiceCalculation(Contract contract);

	/**
	 * 获取当前门店的合同类型计算公式
	 * @author 文清泉
	 * @param 2015年6月5日 上午11:27:43
	 * @param pk_store
	 * @param pk_employee 
	 * @return
	 */
	List<Map<String, Object>> getCalConTyp(@Param(value="pk_store")String pk_store,@Param(value="pk_employee")String pk_employee);

	/**
	 * 获取需要计算工龄员工信息
	 * @author 文清泉
	 * @param 2015年6月5日 上午11:28:11
	 * @param pk_store
	 * @param pk_emp
	 * @return
	 */
	List<Map<String, Object>> getCalEmplyee(@Param(value="pk_store")String pk_store,@Param(value="pk_employee")String pk_employee);

	/**
	 * 保存计算后工龄天数
	 * @author 文清泉
	 * @param 2015年6月5日 上午11:54:31
	 * @param pk_emp
	 * @param dat
	 */
	void updateEmployeeVgl(@Param(value="pk_employee")String pk_emp,@Param(value="vgl")String vgl);
}
