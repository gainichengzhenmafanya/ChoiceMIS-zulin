package com.choice.hr.persistence.ContractManagement;

import java.util.List;

import com.choice.hr.domain.ContractManagement.Contract;
import com.choice.hr.domain.ContractManagement.ContractRegistration;

/**
 * 合同登记
 * @author 文清泉
 * @param 2015年5月19日 上午10:24:10
 */
public interface ContractRegistrationMapper {

	/**
	 * 获取知道员工合同登记情况
	 * @author 文清泉
	 * @param 2015年5月19日 下午1:33:58
	 * @param reg
	 * @return
	 */
	List<ContractRegistration> listContractRegistration(ContractRegistration reg);

	/**
	 * 保存 合同登记
	 * @author 文清泉
	 * @param 2015年5月19日 下午1:58:12
	 * @param reg
	 */
	void saveContractRegistration(ContractRegistration reg);
	
	/**
	 * 合同登记  删除  --- 确保修改时使用
	 * @author 文清泉
	 * @param 2015年5月19日 下午2:03:01
	 * @param reg
	 */
	void delContractRegistration(ContractRegistration reg);
	
	/**
	 * 根据选择员工 获取员工合同汇总信息
	 * @author 文清泉
	 * @param 2015年5月19日 下午2:44:36
	 * @param reg
	 * @return
	 */
	Contract findContract(ContractRegistration reg);

	/**
	 * 根据主键读取合同登记信息
	 * @author 文清泉
	 * @param 2015年5月19日 下午6:15:02
	 * @param reg
	 * @return
	 */
	ContractRegistration listContractRegistrationBy(ContractRegistration reg);
	/**
	 * 获取员工是否首次合同签定
	 * @author 文清泉
	 * @param reg 
	 * @param 2015年6月5日 下午2:58:33
	 * @param pk_EMPLOYEE
	 * @return
	 */
	int findContractByEmployee(ContractRegistration reg);

	/**
	 * 修改员工记录中合同信息
	 * @author 文清泉
	 * @param 2015年6月5日 下午3:01:18
	 * @param reg
	 */
	void updateEmployee(ContractRegistration reg);

	/**
	 * 修改合同签定类型
	 * @author 文清泉
	 * @param 2015年6月5日 下午3:07:09
	 * @param reg
	 */
	void updateEmployeeByContractTyp(ContractRegistration reg);
}
