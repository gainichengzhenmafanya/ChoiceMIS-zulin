package com.choice.hr.persistence.LeaveManagement;

import java.util.List;

import com.choice.hr.domain.LeaveManagement.LeaveRegistration;

/**
 * 描述：休假登记
 * @author 马振
 * 创建时间：2015-6-2 下午5:34:28
 */
/**
 * 描述：年休假
 * @author 马振
 * 创建时间：2015-6-4 上午10:42:40
 */
public interface AnnualLeaveMapper {

	/**
	 * 描述：根据日期查询员工年休假表
	 * @author 马振
	 * 创建时间：2015-6-4 上午10:43:06
	 * @param leaveRegistration
	 * @return
	 */
	public List<LeaveRegistration> listAnnualLeave(LeaveRegistration leaveRegistration);
	
	/**
	 * 描述：根据主键查询
	 * @author 马振
	 * 创建时间：2015-6-4 上午10:43:37
	 * @param leaveRegistration
	 * @return
	 */
	public LeaveRegistration getByPk(LeaveRegistration leaveRegistration);
	
	/**
	 * 描述：新增员工年休假
	 * @author 马振
	 * 创建时间：2015-6-4 上午10:43:54
	 * @param leaveRegistration
	 */
	public void saveAnnualLeave(LeaveRegistration leaveRegistration);
	
	/**
	 * 描述：修改员工年休假
	 * @author 马振
	 * 创建时间：2015-6-4 上午10:44:21
	 * @param leaveRegistration
	 */
	public void updateAnnualLeave(LeaveRegistration leaveRegistration);
	
	/**
	 * 描述：删除员工年休假
	 * @author 马振
	 * 创建时间：2015-6-4 上午10:55:21
	 * @param leaveRegistration
	 */
	public void deleteAnnualLeave(List<String> id);
}