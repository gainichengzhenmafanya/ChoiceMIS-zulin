package com.choice.hr.persistence.LeaveManagement;

import java.util.List;
import java.util.Map;

import com.choice.hr.domain.LeaveManagement.LeaveRegistration;

/**
 * 描述：休假管理——休假查询
 * @author 马振
 * 创建时间：2015-6-5 上午10:15:03
 */
public interface LeaveInquiryMapper {

	/**
	 * 描述：休假查询左侧树
	 * @author 马振
	 * 创建时间：2015-6-5 上午10:15:24
	 * @param team
	 * @return
	 */
	public List<Map<String, Object>> treeLeaveInquiry(LeaveRegistration leaveRegistration);
	
	/**
	 * 描述：休假查询列表
	 * @author 马振
	 * 创建时间：2015-6-5 上午10:24:12
	 * @param leaveRegistration
	 * @return
	 */
	public List<LeaveRegistration> listLeaveInquiry(LeaveRegistration leaveRegistration);
}