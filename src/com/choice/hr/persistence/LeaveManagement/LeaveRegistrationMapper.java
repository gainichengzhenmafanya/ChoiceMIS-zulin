package com.choice.hr.persistence.LeaveManagement;

import java.util.List;

import com.choice.hr.domain.LeaveManagement.LeaveRegistration;

/**
 * 描述：休假登记
 * @author 马振
 * 创建时间：2015-6-2 下午5:34:28
 */
public interface LeaveRegistrationMapper {

	/**
	 * 描述：根据日期查询员工休假登记表
	 * @author 马振
	 * 创建时间：2015-6-3 上午8:49:16
	 * @param leaveRegistration
	 * @return
	 */
	public List<LeaveRegistration> listLeaveRegistration(LeaveRegistration leaveRegistration);
	
	/**
	 * 描述：根据主键查询
	 * @author 马振
	 * 创建时间：2015-6-3 下午6:54:18
	 * @param leaveRegistration
	 * @return
	 */
	public LeaveRegistration getByPk(LeaveRegistration leaveRegistration);
	
	/**
	 * 描述：新增员工休假登记
	 * @author 马振
	 * 创建时间：2015-6-3 下午1:08:34
	 * @param leaveRegistration
	 */
	public void saveEmployeeLeave(LeaveRegistration leaveRegistration);
	
	/**
	 * 描述：修改员工登记
	 * @author 马振
	 * 创建时间：2015-6-3 下午3:17:04
	 * @param leaveRegistration
	 */
	public void updateLeave(LeaveRegistration leaveRegistration);
	
	/**
	 * 描述：删除员工休假登记
	 * @author 马振
	 * 创建时间：2015-6-3 下午3:17:28
	 * @param leaveRegistration
	 */
	public void deleteLeave(LeaveRegistration leaveRegistration);
}
