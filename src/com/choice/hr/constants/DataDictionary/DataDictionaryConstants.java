package com.choice.hr.constants.DataDictionary;

/**
 * 描述：数据字典维护
 * @author 马振
 * 创建时间：2015-5-22 下午2:05:24
 */
public class DataDictionaryConstants {

	/**
	 * 数据字典列表
	 */
	public static final String LISTDATADICTIONARY = "/hr/dataDictionary/listDataDictionary";
	
	/**
	 * 新增数据字典
	 */
	public static final String ADDDATADICTIONARY = "/hr/dataDictionary/addDataDictionary";
	
	/**
	 * 修改数据字典
	 */
	public static final String UPDATEDATADICTIONARY = "/hr/dataDictionary/updateDataDictionary";
}
