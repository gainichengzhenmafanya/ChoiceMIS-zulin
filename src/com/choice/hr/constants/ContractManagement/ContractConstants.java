package com.choice.hr.constants.ContractManagement;

/**
 * 合同管理
 * @author 文清泉
 * @param 2015年5月18日 下午7:00:27
 */
public class ContractConstants {

	/**
	 * 合同管理列表显示
	 */
	public static final String LISTCONTRACTREGISTRATION = "/hr/ContractManagement/listContractRegistration";
	
	/**
	 * 保存 修改页面跳转
	 */
	public static final String SAVECONTRACTREGISTRATION = "/hr/ContractManagement/saveContractRegistration";
	
	/**
	 * 合同变更记录查询
	 */
	public static final String LISTCONTRACTCHANGE = "/hr/ContractManagement/listContractChange";

	
	/**
	 * 合同工龄计算主界面
	 */
	public static final String MAINSERVICECALCULATION = "/hr/ContractManagement/ServiceCalculation/MainServiceCalculation";
	
	/**
	 * 合同工龄计算 左侧树结构
	 */
	public static final String TREESERVICECALCULATION = "/hr/ContractManagement/ServiceCalculation/treeServiceCalculation";
	
	/**
	 * 合同工龄计算 右侧数据列表
	 */
	public static final String LISTSERVICECALCULATION = "/hr/ContractManagement/ServiceCalculation/listServiceCalculation";
	
	/**
	 * 工龄模式设置
	 */
	public static final String LISTCALCULATIONMODE = "/hr/ContractManagement/ServiceCalculation/listCalculationMode";
	
	/**
	 * 合同工龄计算公式编辑
	 */
	public static final String SAVECALCULATIONMODE = "/hr/ContractManagement/ServiceCalculation/saveCalculationMode";
}
