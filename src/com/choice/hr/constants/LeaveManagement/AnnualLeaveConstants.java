package com.choice.hr.constants.LeaveManagement;

/**
 * 描述：休假管理——年休假
 * @author 马振
 * 创建时间：2015-6-4 上午11:10:42
 */
public class AnnualLeaveConstants {

	/**
	 * 年休假
	 */
	public static final String LIST_ANNUALLEAVE = "/hr/leverManagement/annualLeave/listAnnualLeave";
	
	/**
	 * 新增、修改年休假
	 */
	public static final String ADD_DATA = "/hr/leverManagement/annualLeave/addAnnualLeave";
}
