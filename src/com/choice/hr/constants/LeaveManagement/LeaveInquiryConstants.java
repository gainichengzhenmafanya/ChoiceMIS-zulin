package com.choice.hr.constants.LeaveManagement;

/**
 * 描述：休假管理——休假查询
 * @author 马振
 * 创建时间：2015-6-5 上午10:40:15
 */
public class LeaveInquiryConstants {

	/**
	 * 休假查询左侧树列表
	 */
	public static final String TREE_LEAVEINQUIRY = "/hr/leverManagement/leaveInquiry/treeLeaveInquiry";
	
	/**
	 * 休假查询列表
	 */
	public static final String LIST_LEAVEINQUIRY = "/hr/leverManagement/leaveInquiry/listLeaveInquiry";
}
