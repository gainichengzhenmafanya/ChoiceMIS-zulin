package com.choice.hr.constants.LeaveManagement;

/**
 * 描述：休假管理——休假登记
 * @author 马振
 * 创建时间：2015-6-2 下午6:03:17
 */
public class LeaveRegistrationConstants {

	/**
	 * 休假登记列表
	 */
	public static final String LIST_LEAVEREGISTRATION = "/hr/leverManagement/leverReginstration/listLeaveRegistration";
	
	/**
	 * 新增登记
	 */
	public static final String ADD_DATA = "/hr/leverManagement/leverReginstration/addLeaveRegistration";
}
