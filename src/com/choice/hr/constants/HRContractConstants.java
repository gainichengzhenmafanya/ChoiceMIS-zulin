package com.choice.hr.constants;

/**
 * 人事公共界面调整
 * @author 文清泉
 * @param 2015年5月20日 下午5:45:22
 */
public class HRContractConstants {

	public static final String HRACTION_DONE = "hr/hrdone";
	
	public static final String HRERROR_DONE = "share/errorDone";
}
