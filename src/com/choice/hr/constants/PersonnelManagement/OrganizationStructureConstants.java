package com.choice.hr.constants.PersonnelManagement;

/**
 * 描述：人力资源——部门
 * @author 马振
 * 创建时间：2015-5-11 下午12:37:49
 */
public class OrganizationStructureConstants {
	
	/**
	 * 组织结构——左侧树
	 */
	public static final String TREE_ORGANIZATIONSTRUCTURE = "/hr/personnelManagement/organizationStructure/treeOrganizationStructure";
	
	/**
	 * 人力部门详细页面
	 */
	public static final String HRDEPTDETAIL = "/hr/personnelManagement/organizationStructure/hrDeptDetail";
	 
	/**
	 * 人力部门详细页面
	 */
	public static final String CHOOSEHRDEPT = "/hr/personnelManagement/organizationStructure/chooseHrDept";
}
