package com.choice.hr.constants.PersonnelManagement;

/**
 * 描述：人事管理
 * @author 马振
 * 创建时间：2015-5-11 上午10:30:54
 */
public class StaffListConstants {
	
	/**
	 * 人员列表左侧树
	 */
	public static final String TREE_STAFFLIST = "/hr/personnelManagement/staffList/treeEmployee";
	
	/**
	 * 人员列表选择左侧树
	 */
	public static final String TREE_LISTEMPLOYEE = "/hr/personnelManagement/listSelectEmployee/treeSelectEmployee";
	
	/**
	 * 人员列表信息
	 */
	public static final String LIST_EMPLOYEE = "/hr/personnelManagement/staffList/listEmployee";

	/**
	 * 人员列表信息
	 */
	public static final String LIST_EMPLOYEE_NEW = "/hr/personnelManagement/staffList/listEmployee_new";
	
	/**
	 * 人员列表选择信息
	 */
	public static final String LIST_SELECTEMPLOYEE = "/hr/personnelManagement/listSelectEmployee/listSelectEmployee";
	
	/**
	 * 上传图片
	 */
	public static final String UPLOADPICTURE = "/hr/personnelManagement/staffList/uploadPicture";
	
	/**
	 * 查询民族
	 */
	public static final String CHOOSENATION = "/hr/personnelManagement/staffList/chooseNation";
	
	/**
	 * 人员管理——档案
	 */
	public static final String EMPLOYEE_ARCHIVES = "/hr/personnelManagement/staffList/employeeArchives";
	
	/**
	 * 人员管理 -- 入职操作
	 */
	public static final String EMPLOYEE_ENTRY = "/hr/personnelManagement/staffList/editEmployeeEntry";
	
	/**
	 * 新增家庭页面
	 */
	public static final String ADD_FAMILY = "/hr/personnelManagement/staffList/addFamily";
	
	/**
	 * 转正页面
	 */
	public static final String EMPLOYEE_POSITIVE = "/hr/personnelManagement/positive/empoyeePositive";
}
