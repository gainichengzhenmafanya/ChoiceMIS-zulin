package com.choice.hr.constants.RewardManagement;

/**
 * 描述：奖惩登记
 * @author 马振
 * 创建时间：2015-6-5 下午3:56:17
 */
public class RewardPunishmentConstants {

	/**
	 * 奖惩登记列表
	 */
	public static final String LIST_REWARDPUNISHMENT = "/hr/rewardManagement/rewardPunishment/listRewardPunishment";
	
	/**
	 * 新增奖惩登记
	 */
	public static final String ADD_REWARDPUNISHMENT = "/hr/rewardManagement/rewardPunishment/addRewardPunishment";
}
