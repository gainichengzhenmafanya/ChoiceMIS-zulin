package com.choice.hr.constants.RewardManagement;

/**
 * 描述：
 * @author 马振
 * 创建时间：2015-6-9 上午11:33:02
 */
public class BonusEntryConstants {

	/**
	 * 左侧树
	 */
	public static final String TREE_BONUSENTRY = "/hr/rewardManagement/bonusEntry/treeBonusEntry";
	
	/**
	 * 奖金录入列表
	 */
	public static final String LIST_BONUSENTRY = "/hr/rewardManagement/bonusEntry/listBonusEntry";
	
	/**
	 * 获取需要录入奖金的员工
	 */
	public static final String LIST_EMP = "/hr/rewardManagement/bonusEntry/listEmp";
}
