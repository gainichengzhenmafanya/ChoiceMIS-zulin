package com.choice.hr.constants.ScheduleAttendance;

/**
 * 描述：排班考勤——班次设置
 * @author 马振
 * 创建时间：2015-5-26 下午1:45:21
 */
public class ShiftSettingConstants {

	/**
	 * 班次列表
	 */
	public static final String LIST_SHIFT = "/hr/ScheduleAttendance/shiftSetting/listShift";
	
	/**
	 * 新增班次
	 */
	public static final String ADD_SHIFT = "/hr/ScheduleAttendance/shiftSetting/addShift";
	
	/**
	 * 修改班次
	 */
	public static final String UPDATE_SHIFT = "/hr/ScheduleAttendance/shiftSetting/updateShift";
	
	/**
	 * 班次弹出框
	 */
	public static final String CHOOSESHIFT = "/hr/ScheduleAttendance/shiftSetting/toChooseShift";
}
