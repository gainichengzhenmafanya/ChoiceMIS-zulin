package com.choice.hr.constants.ScheduleAttendance;

/**
 * 描述：考勤记录
 * @author 马振
 * 创建时间：2015-6-2 上午10:20:19
 */
public class AttendanceRecordConstants {

	/**
	 * 左侧树
	 */
	public static final String TREE_ATTENDANCERECORD = "/hr/ScheduleAttendance/attendanceRecord/treeAttendanceRecord";
	
	/**
	 * 考勤记录列表
	 */
	public static final String LIST_ATTENDANCERECORD = "/hr/ScheduleAttendance/attendanceRecord/listAttendanceRecord";
	
	/**
	 * 考勤汇总列表
	 */
	public static final String LIST_COUNT = "/hr/ScheduleAttendance/attendanceRecord/listAttendanceCount";
	
	/**
	 * 考勤工时调整计算 
	 */
	public static final String LISTWORKATTENDANCE = "/hr/ScheduleAttendance/WorkAttendance/listWorkAttendance";
}
