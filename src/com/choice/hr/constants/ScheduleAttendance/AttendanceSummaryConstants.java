package com.choice.hr.constants.ScheduleAttendance;

/**
 * 描述：排班考勤——考勤工时汇总
 * @author 马振
 * 创建时间：2015-6-24 下午2:00:59
 */
public class AttendanceSummaryConstants {

	/**
	 * 考勤工时汇总列表
	 */
	public static final String LIST_ATTENDANCESUMMARY = "/hr/ScheduleAttendance/attendanceRecord/listAttendanceSummary";
	
	/**
	 * 查询理论工时
	 */
	public static final String LIST_THEORY = "/hr/ScheduleAttendance/attendanceRecord/listTheory";
}
