package com.choice.hr.constants.ScheduleAttendance;

/**
 * 描述：班组
 * @author 马振
 * 创建时间：2015-5-25 下午2:12:03
 */
public class TeamManagementConstants {

	/**
	 * 班组左侧树
	 */
	public static final String TREE_TEAMMANAGEMENT = "/hr/ScheduleAttendance/teamManagement/treeTeamManagement";
	
	/**
	 * 班组列表
	 */
	public static final String LIST_TEAMMANAGEMENT = "/hr/ScheduleAttendance/teamManagement/listTeamManagement";
	
	/**
	 * 新增班组
	 */
	public static final String ADDTEAM = "/hr/ScheduleAttendance/teamManagement/addTeamManagement";
	
	/**
	 * 修改班组
	 */
	public static final String UPDATETEAM = "/hr/ScheduleAttendance/teamManagement/updateTeamManagement";
}
