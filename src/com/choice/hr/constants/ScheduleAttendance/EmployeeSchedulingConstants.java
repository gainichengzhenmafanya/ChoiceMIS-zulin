package com.choice.hr.constants.ScheduleAttendance;

/**
 * 描述：员工排班路径
 * @author 马振
 * 创建时间：2015-5-28 上午8:52:26
 */
public class EmployeeSchedulingConstants {

	/**
	 * 班组排班左侧树
	 */
	public static final String TREE_EMPLOYEESCHEDULING = "/hr/ScheduleAttendance/employeeScheduling/treeEmployeeScheduling"; 
	
	/**
	 * 班组排班列表
	 */
	public static final String LIST_EMPLOYEESCHEDULING = "/hr/ScheduleAttendance/employeeScheduling/listEmployeeScheduling"; 
}
