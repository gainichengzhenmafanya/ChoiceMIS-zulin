package com.choice.hr.constants.ScheduleAttendance;

/**
 * 描述：班考勤——班表设定
 * @author 马振
 * 创建时间：2015-6-16 上午9:09:42
 */
public class ClassTableSettingConstants {

	/**
	 * 班表设定
	 */
	public static final String LIST_CLASSTABLESETTING = "/hr/ScheduleAttendance/classTableSetting/listClassTableSetting";
	
	/**
	 * 设置人数
	 */
	public static final String ADD_PEOPLENUMBER = "/hr/ScheduleAttendance/classTableSetting/addPeopleNumber";
	
	/**
	 * 复制(选择日期)
	 */
	public static final String SELECTDATE = "/hr/ScheduleAttendance/classTableSetting/selectDate";
}
