package com.choice.hr.constants.ScheduleAttendance;

/**
 * 描述：班组排班路径
 * @author 马振
 * 创建时间：2015-5-26 下午4:34:03
 */
public class CrewSchedulingConstants {

	/**
	 * 班组排班左侧树
	 */
	public static final String TREE_CREWSCHEDULING = "/hr/ScheduleAttendance/crewscheduling/treeCrewScheduling"; 
	
	/**
	 * 班组排班列表
	 */
	public static final String LIST_CREWSCHEDULING = "/hr/ScheduleAttendance/crewscheduling/listCrewScheduling"; 
}
