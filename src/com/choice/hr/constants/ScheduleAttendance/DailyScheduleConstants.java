package com.choice.hr.constants.ScheduleAttendance;

/**
 * 描述：排班考勤——每日排班
 * @author 马振
 * 创建时间：2015-6-11 下午5:26:40
 */
public class DailyScheduleConstants {

	/**
	 * 每日排班查询
	 */
	public static final String LIST_DAILYSCHEDULE = "/hr/ScheduleAttendance/dailySchedule/listDailySchedule";
}
