package com.choice.hr.constants.ScheduleAttendance;

/**
 * 描述：排班查询路径
 * @author 马振
 * 创建时间：2015-5-28 下午1:13:48
 */
public class QuerySchedulingConstants {

	/**
	 * 排班查询左侧树
	 */
	public static final String TREE_QUERYSCHEDULING = "/hr/ScheduleAttendance/queryScheduling/treeQueryScheduling"; 
	
	/**
	 * 排班查询列表
	 */
	public static final String LIST_QUERYSCHEDULING = "/hr/ScheduleAttendance/queryScheduling/listQueryScheduling"; 
}
