package com.choice.hr.domain.ContractManagement;

import java.util.Date;

import com.choice.misboh.commonutil.DateJudge;


/**
 * 合同等级类型
 * @author 文清泉
 * @param 2015年5月18日 下午3:41:23
 */
public class ContractRegistration {

	/**
	 * 主键
	 */
	private String PK_CONTRACTREG;
	/**
	 * 员工工号
	 */
	private String VEMPNO;
	/**
	 * 合同起始
	 */
	private String VBEINDATE;
	/**
	 * 合同结束
	 */
	private String VENDDATE;
	/**
	 * 执行日期
	 */
	private String VEXECUTIVEDATE;
	/**
	 * 期限
	 */
	private String VTIMELIMIT;      
	/**
	 * 合同类型
	 */
	private String VTYP;            
	/**
	 * 违约金
	 */
	private String DBREACHCONTRACT;
	/**
	 * 备注
	 */
	private String VMEMO;
	/**
	 * 录入人
	 */
	private String VINPUTBY;
	/**
	 * 录入时间
	 */
	private String VINPUTTIME;
	/**
	 * 员工主键
	 */
	private String PK_EMPLOYEE;
	/**
	 * 合同状态 ----------不使用--------------------------
	 */
	private String STATUS;
	/**
	 * 有效
	 */
	private String VEFFECTIVE;
	
	/**
	 * 岗位
	 */
	private String POST;
	
	/**
	 * 部门
	 */
	private String DEPT;
	
	/**
	 * 门店编码
	 */
	private String VSCODE;
	/**
	 * 合同类型  
	 */
	private String VTYPDESC;
	
	/**
	 * 系统当前时间
	 */
	private String VSYSDATE;
	/**
	 * 员工姓名
	 */
	private String VENAME;

	public String getVSCODE() {
		return VSCODE;
	}
	public void setVSCODE(String vSCODE) {
		VSCODE = vSCODE;
	}
	public String getPOST() {
		return POST;
	}
	public void setPOST(String pOST) {
		POST = pOST;
	}
	public String getDEPT() {
		return DEPT;
	}
	public String getVTYPDESC() {
		return VTYPDESC;
	}
	public void setVTYPDESC(String vTYPDESC) {
		VTYPDESC = vTYPDESC;
	}
	public void setDEPT(String dEPT) {
		DEPT = dEPT;
	}
	public String getPK_CONTRACTREG() {
		return PK_CONTRACTREG;
	}
	public void setPK_CONTRACTREG(String pK_CONTRACTREG) {
		PK_CONTRACTREG = pK_CONTRACTREG;
	}
	public String getVEMPNO() {
		return VEMPNO;
	}
	public void setVEMPNO(String vEMPNO) {
		VEMPNO = vEMPNO;
	}
	public String getVBEINDATE() {
		if(VBEINDATE == null  || VBEINDATE.equals("")){
			VBEINDATE = DateJudge.YYYY_MM_DD.format(new Date());
		}
		return VBEINDATE;
	}
	public void setVBEINDATE(String vBEINDATE) {
		VBEINDATE = vBEINDATE;
	}
	public String getVENDDATE() {
		if(VENDDATE == null  || VENDDATE.equals("")){
			VENDDATE = DateJudge.YYYY_MM_DD.format(new Date());
		}
		return VENDDATE;
	}
	public void setVENDDATE(String vENDDATE) {
		VENDDATE = vENDDATE;
	}
	public String getVEXECUTIVEDATE() {
		if(VEXECUTIVEDATE == null  || VEXECUTIVEDATE.equals("")){
			VEXECUTIVEDATE = DateJudge.YYYY_MM_DD.format(new Date());
		}
		return VEXECUTIVEDATE;
	}
	public void setVEXECUTIVEDATE(String vEXECUTIVEDATE) {
		VEXECUTIVEDATE = vEXECUTIVEDATE;
	}
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getVEFFECTIVE() {
		return VEFFECTIVE;
	}
	public void setVEFFECTIVE(String vEFFECTIVE) {
		VEFFECTIVE = vEFFECTIVE;
	}
	public String getVTIMELIMIT() {
		return VTIMELIMIT;
	}
	public void setVTIMELIMIT(String vTIMELIMIT) {
		VTIMELIMIT = vTIMELIMIT;
	}
	public String getVTYP() {
		return VTYP;
	}
	public void setVTYP(String vTYP) {
		VTYP = vTYP;
	}
	public String getDBREACHCONTRACT() {
		return DBREACHCONTRACT;
	}
	public void setDBREACHCONTRACT(String dBREACHCONTRACT) {
		DBREACHCONTRACT = dBREACHCONTRACT;
	}
	public String getVMEMO() {
		return VMEMO;
	}
	public void setVMEMO(String vMEMO) {
		VMEMO = vMEMO;
	}
	public String getVINPUTBY() {
		return VINPUTBY;
	}
	public void setVINPUTBY(String vINPUTBY) {
		VINPUTBY = vINPUTBY;
	}
	public String getVINPUTTIME() {
		return VINPUTTIME;
	}
	public void setVINPUTTIME(String vINPUTTIME) {
		VINPUTTIME = vINPUTTIME;
	}
	public String getPK_EMPLOYEE() {
		return PK_EMPLOYEE;
	}
	public void setPK_EMPLOYEE(String pK_EMPLOYEE) {
		PK_EMPLOYEE = pK_EMPLOYEE;
	}
	public String getVSYSDATE() {
		return VSYSDATE;
	}
	public void setVSYSDATE(String vSYSDATE) {
		VSYSDATE = vSYSDATE;
	}
	public String getVENAME() {
		return VENAME;
	}
	public void setVENAME(String vENAME) {
		VENAME = vENAME;
	}
	
}
