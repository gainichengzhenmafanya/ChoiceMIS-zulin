package com.choice.hr.domain.ContractManagement;

/**
 * 合同工龄计算模式公式
 * @author 文清泉
 * @param 2015年5月22日 下午2:11:15
 */
public class ServiceFormula {
	/**
	 * 主键
	 */
	private String PK_SERVICEFORMULA;
	/**
	 * 计算开始时间字段
	 */
	private String BFIELD;
	private String BFIELDDES;
	/**
	 * 计算起始时间
	 */
	private String BCDAT;
	/**
	 * 合同类型
	 */
	private String PK_CONTENTTYP;
	private String CONTENTTYPCODE;
	private String CONTENTTYPDESC;
	
	/**
	 * 增减天数
	 */
	private String SERVICEDAY;
	/**
	 * 计算模式
	 */
	private String PK_MODE;
	/**
	 * 员工编码
	 */
	private String PK_EMPLOYEE;
	/**
	 * 店铺编码
	 */
	private String PK_STORE;
	public String getPK_EMPLOYEE() {
		return PK_EMPLOYEE;
	}
	public void setPK_EMPLOYEE(String pK_EMPLOYEE) {
		PK_EMPLOYEE = pK_EMPLOYEE;
	}
	public String getPK_STORE() {
		return PK_STORE;
	}
	public void setPK_STORE(String pK_STORE) {
		PK_STORE = pK_STORE;
	}
	public String getPK_SERVICEFORMULA() {
		return PK_SERVICEFORMULA;
	}
	public void setPK_SERVICEFORMULA(String pK_SERVICEFORMULA) {
		PK_SERVICEFORMULA = pK_SERVICEFORMULA;
	}
	public String getBFIELD() {
		return BFIELD;
	}
	public void setBFIELD(String bFIELD) {
		BFIELD = bFIELD;
	}
	public String getBCDAT() {
		return BCDAT;
	}
	public void setBCDAT(String bCDAT) {
		BCDAT = bCDAT;
	}
	public String getPK_CONTENTTYP() {
		return PK_CONTENTTYP;
	}
	public void setPK_CONTENTTYP(String pK_CONTENTTYP) {
		PK_CONTENTTYP = pK_CONTENTTYP;
	}
	public String getSERVICEDAY() {
		return SERVICEDAY;
	}
	public void setSERVICEDAY(String sERVICEDAY) {
		SERVICEDAY = sERVICEDAY;
	}
	public String getBFIELDDES() {
		return BFIELDDES;
	}
	public void setBFIELDDES(String bFIELDDES) {
		BFIELDDES = bFIELDDES;
	}
	public String getCONTENTTYPCODE() {
		return CONTENTTYPCODE;
	}
	public void setCONTENTTYPCODE(String cONTENTTYPCODE) {
		CONTENTTYPCODE = cONTENTTYPCODE;
	}
	public String getCONTENTTYPDESC() {
		return CONTENTTYPDESC;
	}
	public void setCONTENTTYPDESC(String cONTENTTYPDESC) {
		CONTENTTYPDESC = cONTENTTYPDESC;
	}
	public String getPK_MODE() {
		return PK_MODE;
	}
	public void setPK_MODE(String pK_MODE) {
		PK_MODE = pK_MODE;
	}
	
}
