package com.choice.hr.domain.ContractManagement;

import java.util.List;

import com.choice.misboh.commonutil.FormatDouble;

/**
 * 合同管理 --- 表头显示数据实体类
 * @author 文清泉
 * @param 2015年5月19日 下午2:04:53
 */
public class Contract {
	private String empno;// 员工卡号
	private String ename;// 员工姓名
	private String esta;// 员工状态
	private String bfirst;// 首签开始日期
	private String efirst;// 首签结束日期
	private String bsign;// 末签开始日期
	private String esign;// 末签结束日期
	private String timeLimit;//累计期限
	private String signCnt;// 签定次数
	private String pk_employee;//员工主键
	private String vdeptdes;//部门名称
	private String vjrdat;// 入职日期
	private String vstation;// 岗位
	private String isDat;// 日期选择（0  到期日期   1 签定日期）
	private String isSta;//是否有效（2  未执行  1 执行  0 过期）
	private String vtyp;//合同类型
	private String pk_hrdept;//部门
	private List<ContractRegistration> listContractRegistration;//合同记录集合
	private String sysDate;// 系统当前时间
	private String pk_store;//店铺编码
	private Integer moneyLength;//界面金额显示位数
	private String moneypattern;//界面金额显示位数 --- 标签使用
	private String execdat;// 执行日期
	private String vagl;//合同年龄
	public String getExecdat() {
		return execdat;
	}
	public void setExecdat(String execdat) {
		this.execdat = execdat;
	}
	public String getEmpno() {
		return empno;
	}
	public void setEmpno(String empno) {
		this.empno = empno;
	}
	public String getVagl() {
		return vagl;
	}
	public void setVagl(String vagl) {
		this.vagl = vagl;
	}
	public String getEname() {
		return ename;
	}
	public String getVdeptdes() {
		return vdeptdes;
	}
	public void setVdeptdes(String vdeptdes) {
		this.vdeptdes = vdeptdes;
	}
	
	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
	public String getSysDate() {
		return sysDate;
	}
	public void setSysDate(String sysDate) {
		this.sysDate = sysDate;
	}
	public String getMoneypattern() {
		return moneypattern;
	}
	public void setMoneypattern(String moneypattern) {
		this.moneypattern = moneypattern;
	}
	public String getIsDat() {
		return isDat;
	}
	public void setIsDat(String isDat) {
		this.isDat = isDat;
	}
	public Integer getMoneyLength() {
		moneyLength = FormatDouble.getMoneyBit();
		return moneyLength;
	}
	public void setMoneyLength(Integer moneyLength) {
		this.moneyLength = moneyLength;
	}
	public String getIsSta() {
		return isSta;
	}
	public void setIsSta(String isSta) {
		this.isSta = isSta;
	}
	public String getVtyp() {
		return vtyp;
	}
	public void setVtyp(String vtyp) {
		this.vtyp = vtyp;
	}
	public String getPk_hrdept() {
		return pk_hrdept;
	}
	public void setPk_hrdept(String pk_hrdept) {
		this.pk_hrdept = pk_hrdept;
	}
	public List<ContractRegistration> getListContractRegistration() {
		return listContractRegistration;
	}
	public void setListContractRegistration(
			List<ContractRegistration> listContractRegistration) {
		this.listContractRegistration = listContractRegistration;
	}
	public String getVjrdat() {
		return vjrdat;
	}
	public void setVjrdat(String vjrdat) {
		this.vjrdat = vjrdat;
	}
	public String getVstation() {
		return vstation;
	}
	public void setVstation(String vstation) {
		this.vstation = vstation;
	}
	public void setEname(String ename) {
		this.ename = ename;
	}
	public String getEsta() {
		return esta;
	}
	public void setEsta(String esta) {
		this.esta = esta;
	}
	public String getBfirst() {
		return bfirst;
	}
	public void setBfirst(String bfirst) {
		this.bfirst = bfirst;
	}
	public String getEfirst() {
		return efirst;
	}
	public void setEfirst(String efirst) {
		this.efirst = efirst;
	}
	public String getBsign() {
		return bsign;
	}
	public void setBsign(String bsign) {
		this.bsign = bsign;
	}
	public String getEsign() {
		return esign;
	}
	public void setEsign(String esign) {
		this.esign = esign;
	}
	public String getTimeLimit() {
		return timeLimit;
	}
	public void setTimeLimit(String timeLimit) {
		this.timeLimit = timeLimit;
	}
	public String getSignCnt() {
		return signCnt;
	}
	public void setSignCnt(String signCnt) {
		this.signCnt = signCnt;
	}
	public String getPk_employee() {
		return pk_employee;
	}
	public void setPk_employee(String pk_employee) {
		this.pk_employee = pk_employee;
	}
	
}
