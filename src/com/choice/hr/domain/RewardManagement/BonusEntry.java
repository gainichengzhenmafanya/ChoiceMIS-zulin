package com.choice.hr.domain.RewardManagement;

import java.util.List;

/**
 * 描述：奖惩管理——奖金录入
 * @author 马振
 * 创建时间：2015-6-9 上午10:47:47
 */
public class BonusEntry {

	private String pk_bonusentry;	//主键
	private String pk_employee;		//员工主键
	private String pk_store;		//门店主键
	private String pk_hrdept;		//部门主键
	private String vstation;		//任职状态
	private Double nfullaward;		//全勤奖
	private Double nlastaward;		//上月岗位奖
	private Double nthisaward;		//本月岗位奖
	private String vdismissreasons;	//驳回原因
	private String nauditstatus;	//审核状态:0已审核，1未审核
	private String vapprovalperson;	//审核人
	private String dworkdate;		//审核日期
	
	private String enablestate;		//启用状态
	private String vcode;
	private String vname;
	private String vinit;			
	
	private List<BonusEntry> list;
	
	public String getPk_bonusentry() {
		return pk_bonusentry;
	}
	public void setPk_bonusentry(String pk_bonusentry) {
		this.pk_bonusentry = pk_bonusentry;
	}
	public String getPk_employee() {
		return pk_employee;
	}
	public void setPk_employee(String pk_employee) {
		this.pk_employee = pk_employee;
	}
	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
	public String getPk_hrdept() {
		return pk_hrdept;
	}
	public void setPk_hrdept(String pk_hrdept) {
		this.pk_hrdept = pk_hrdept;
	}
	public String getVstation() {
		return vstation;
	}
	public void setVstation(String vstation) {
		this.vstation = vstation;
	}
	public Double getNfullaward() {
		return nfullaward;
	}
	public void setNfullaward(Double nfullaward) {
		this.nfullaward = nfullaward;
	}
	public Double getNlastaward() {
		return nlastaward;
	}
	public void setNlastaward(Double nlastaward) {
		this.nlastaward = nlastaward;
	}
	public Double getNthisaward() {
		return nthisaward;
	}
	public void setNthisaward(Double nthisaward) {
		this.nthisaward = nthisaward;
	}
	public String getVdismissreasons() {
		return vdismissreasons;
	}
	public void setVdismissreasons(String vdismissreasons) {
		this.vdismissreasons = vdismissreasons;
	}
	public String getNauditstatus() {
		return nauditstatus;
	}
	public void setNauditstatus(String nauditstatus) {
		this.nauditstatus = nauditstatus;
	}
	public String getVapprovalperson() {
		return vapprovalperson;
	}
	public void setVapprovalperson(String vapprovalperson) {
		this.vapprovalperson = vapprovalperson;
	}
	public String getDworkdate() {
		return dworkdate;
	}
	public void setDworkdate(String dworkdate) {
		this.dworkdate = dworkdate;
	}
	public String getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(String enablestate) {
		this.enablestate = enablestate;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getVinit() {
		return vinit;
	}
	public void setVinit(String vinit) {
		this.vinit = vinit;
	}
	public List<BonusEntry> getList() {
		return list;
	}
	public void setList(List<BonusEntry> list) {
		this.list = list;
	}
}