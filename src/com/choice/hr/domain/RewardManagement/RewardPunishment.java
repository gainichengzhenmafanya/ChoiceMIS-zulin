package com.choice.hr.domain.RewardManagement;

/**
 * 描述：奖惩管理——奖惩登记
 * @author 马振
 * 创建时间：2015-6-5 下午2:51:34
 */
public class RewardPunishment {
	
	private String pk_rewardpunishment;	//主键
	private String pk_employee;			//员工主键
	private String pk_store;			//门店主键
	private String pk_hrdept;			//部门主键
	private String pk_status;			//员工状态主键
	private String pk_repunityp;		//奖惩类型主键
	private String dworkdate;			//奖惩日期
	private String vsummary;			//奖惩摘要
	private Double namount;				//奖惩金额
	private String vmemo;				//备注
	
	private String vbdat;				//开始日期
	private String vedat;				//结束日期
	
	private String flag;				//新增、修改、删除标识
	
	public String getPk_rewardpunishment() {
		return pk_rewardpunishment;
	}

	public void setPk_rewardpunishment(String pk_rewardpunishment) {
		this.pk_rewardpunishment = pk_rewardpunishment;
	}

	public String getPk_employee() {
		return pk_employee;
	}

	public void setPk_employee(String pk_employee) {
		this.pk_employee = pk_employee;
	}

	public String getPk_store() {
		return pk_store;
	}

	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}

	public String getPk_hrdept() {
		return pk_hrdept;
	}

	public void setPk_hrdept(String pk_hrdept) {
		this.pk_hrdept = pk_hrdept;
	}

	public String getPk_status() {
		return pk_status;
	}

	public void setPk_status(String pk_status) {
		this.pk_status = pk_status;
	}

	public String getPk_repunityp() {
		return pk_repunityp;
	}

	public void setPk_repunityp(String pk_repunityp) {
		this.pk_repunityp = pk_repunityp;
	}

	public String getDworkdate() {
		return dworkdate;
	}

	public void setDworkdate(String dworkdate) {
		this.dworkdate = dworkdate;
	}

	public String getVsummary() {
		return vsummary;
	}

	public void setVsummary(String vsummary) {
		this.vsummary = vsummary;
	}

	public Double getNamount() {
		return namount;
	}

	public void setNamount(Double namount) {
		this.namount = namount;
	}

	public String getVmemo() {
		return vmemo;
	}

	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getVbdat() {
		return vbdat;
	}

	public void setVbdat(String vbdat) {
		this.vbdat = vbdat;
	}

	public String getVedat() {
		return vedat;
	}

	public void setVedat(String vedat) {
		this.vedat = vedat;
	}
}