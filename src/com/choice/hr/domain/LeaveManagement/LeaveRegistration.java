package com.choice.hr.domain.LeaveManagement;

/**
 * 描述：休假登记、年休假
 * @author 马振
 * 创建时间：2015-6-4 上午10:56:09
 */
public class LeaveRegistration {
	
	private String pk_leaveregistration;//主键
	private String pk_store;			//门店主键
	private String pk_employee;			//员工主键
	private String pk_hrdept;			//部门主键
	private String pk_status;			//员工状态主键
	private String pk_leavetyp;			//休假类型
	private String vempno;				//员工号
	private String vname;				//员工姓名
	private String vbdat;				//休假期自
	private String vedat;				//休假期至
	private String vnum;				//休假天数
	private String vsickdate;			//销假日期
	private String vleavereason;		//休假原因
	private String vmemo;				//备注
	
	//年休假特有字段
	private String pk_annualleave;		//年休假主键
	private String vyears;				//休假年度
	private String ncandays;			//可休天数
	private String nrepairdays;			//已休天数
	private String nworktime;			//工作年限
	
	//用于存储查询字段
	private String vcode;				//编码
	private String vinit;				//缩写
	private String enablestate;			//启用状态
	
	private String vleavetyp;			//离职类型
	private String vstatus;				//员工状态
	private String vstation;			//任职岗位
	
	private String flag;				//新增、修改、删除标识
	
	public String getPk_leaveregistration() {
		return pk_leaveregistration;
	}
	public void setPk_leaveregistration(String pk_leaveregistration) {
		this.pk_leaveregistration = pk_leaveregistration;
	}
	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
	public String getPk_hrdept() {
		return pk_hrdept;
	}
	public void setPk_hrdept(String pk_hrdept) {
		this.pk_hrdept = pk_hrdept;
	}
	public String getPk_status() {
		return pk_status;
	}
	public void setPk_status(String pk_status) {
		this.pk_status = pk_status;
	}
	public String getVempno() {
		return vempno;
	}
	public void setVempno(String vempno) {
		this.vempno = vempno;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getVbdat() {
		return vbdat;
	}
	public void setVbdat(String vbdat) {
		this.vbdat = vbdat;
	}
	public String getVedat() {
		return vedat;
	}
	public void setVedat(String vedat) {
		this.vedat = vedat;
	}
	public String getPk_leavetyp() {
		return pk_leavetyp;
	}
	public void setPk_leavetyp(String pk_leavetyp) {
		this.pk_leavetyp = pk_leavetyp;
	}
	public String getVnum() {
		return vnum;
	}
	public void setVnum(String vnum) {
		this.vnum = vnum;
	}
	public String getVsickdate() {
		return vsickdate;
	}
	public void setVsickdate(String vsickdate) {
		this.vsickdate = vsickdate;
	}
	public String getVleavereason() {
		return vleavereason;
	}
	public void setVleavereason(String vleavereason) {
		this.vleavereason = vleavereason;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public String getPk_employee() {
		return pk_employee;
	}
	public void setPk_employee(String pk_employee) {
		this.pk_employee = pk_employee;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getVyears() {
		return vyears;
	}
	public void setVyears(String vyears) {
		this.vyears = vyears;
	}
	public String getNcandays() {
		return ncandays;
	}
	public void setNcandays(String ncandays) {
		this.ncandays = ncandays;
	}
	public String getNrepairdays() {
		return nrepairdays;
	}
	public void setNrepairdays(String nrepairdays) {
		this.nrepairdays = nrepairdays;
	}
	public String getPk_annualleave() {
		return pk_annualleave;
	}
	public void setPk_annualleave(String pk_annualleave) {
		this.pk_annualleave = pk_annualleave;
	}
	public String getNworktime() {
		return nworktime;
	}
	public void setNworktime(String nworktime) {
		this.nworktime = nworktime;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVinit() {
		return vinit;
	}
	public void setVinit(String vinit) {
		this.vinit = vinit;
	}
	public String getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(String enablestate) {
		this.enablestate = enablestate;
	}
	public String getVleavetyp() {
		return vleavetyp;
	}
	public void setVleavetyp(String vleavetyp) {
		this.vleavetyp = vleavetyp;
	}
	public String getVstatus() {
		return vstatus;
	}
	public void setVstatus(String vstatus) {
		this.vstatus = vstatus;
	}
	public String getVstation() {
		return vstation;
	}
	public void setVstation(String vstation) {
		this.vstation = vstation;
	}
}