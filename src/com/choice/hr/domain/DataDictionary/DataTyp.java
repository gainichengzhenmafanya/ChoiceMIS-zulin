package com.choice.hr.domain.DataDictionary;

/**
 * 描述：数据字典表
 * @author 马振
 * 创建时间：2015-5-14 下午4:25:50
 */
public class DataTyp {
	private String  pk_datatyp;		//主键
	private String  pk_store;		//门店主键
	private String  vcode;			//编码
	private String  vname;			//名称
	private String  vinit;			//缩写
	private Integer isortno;		//排序列
	private String 	vtyp;			//类别
	private String  vmemo;			//备注
	
	private String  flag;			//新增或修改的标识
	
	public String getPk_datatyp() {
		return pk_datatyp;
	}
	public void setPk_datatyp(String pk_datatyp) {
		this.pk_datatyp = pk_datatyp;
	}
	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getVinit() {
		return vinit;
	}
	public void setVinit(String vinit) {
		this.vinit = vinit;
	}
	public Integer getIsortno() {
		return isortno;
	}
	public void setIsortno(Integer isortno) {
		this.isortno = isortno;
	}
	public String getVtyp() {
		return vtyp;
	}
	public void setVtyp(String vtyp) {
		this.vtyp = vtyp;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
}