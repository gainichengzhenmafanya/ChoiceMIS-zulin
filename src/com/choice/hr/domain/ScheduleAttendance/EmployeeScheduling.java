package com.choice.hr.domain.ScheduleAttendance;

import java.util.List;

/**
 * 描述：员工排班
 * @author 马振
 * 创建时间：2015-5-27 下午6:34:23
 */
public class EmployeeScheduling {
	private String pk_employeescheduling;	//主键
	private String pk_store;			//门店主键
	private String pk_hrdept;			//部门主键
	private String pk_team;				//班组主键
	private String pk_shift;			//班次主键
	private String pk_employee;			//员工主键
	private String vcode;				//班次编码
	private String vname;				//班次名称
	private String vinit;				//缩写
	private String dworkdate;			//日期
	private String vbdat;				//开始日期
	private String vedat;				//结束日期
	private String enablestate;			//启用状态
	
	private List<EmployeeScheduling> listEmployeeScheduling;
	
	public String getPk_employeescheduling() {
		return pk_employeescheduling;
	}
	public void setPk_employeescheduling(String pk_employeescheduling) {
		this.pk_employeescheduling = pk_employeescheduling;
	}
	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
	public String getPk_hrdept() {
		return pk_hrdept;
	}
	public void setPk_hrdept(String pk_hrdept) {
		this.pk_hrdept = pk_hrdept;
	}
	public String getPk_team() {
		return pk_team;
	}
	public void setPk_team(String pk_team) {
		this.pk_team = pk_team;
	}
	public String getPk_shift() {
		return pk_shift;
	}
	public void setPk_shift(String pk_shift) {
		this.pk_shift = pk_shift;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getDworkdate() {
		return dworkdate;
	}
	public void setDworkdate(String dworkdate) {
		this.dworkdate = dworkdate;
	}
	public String getVinit() {
		return vinit;
	}
	public void setVinit(String vinit) {
		this.vinit = vinit;
	}
	public String getVbdat() {
		return vbdat;
	}
	public void setVbdat(String vbdat) {
		this.vbdat = vbdat;
	}
	public String getVedat() {
		return vedat;
	}
	public void setVedat(String vedat) {
		this.vedat = vedat;
	}
	public String getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(String enablestate) {
		this.enablestate = enablestate;
	}
	public List<EmployeeScheduling> getListEmployeeScheduling() {
		return listEmployeeScheduling;
	}
	public void setListEmployeeScheduling(
			List<EmployeeScheduling> listEmployeeScheduling) {
		this.listEmployeeScheduling = listEmployeeScheduling;
	}
	public String getPk_employee() {
		return pk_employee;
	}
	public void setPk_employee(String pk_employee) {
		this.pk_employee = pk_employee;
	}
}