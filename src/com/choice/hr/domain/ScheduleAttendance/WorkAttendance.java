package com.choice.hr.domain.ScheduleAttendance;

import java.util.List;

/**
 * 考勤工时
 * @author 文清泉
 * @param 2015年6月19日 上午10:42:12
 */
public class WorkAttendance {
	private String PK_STORE;//门店主键
	private String VSCODE;//门店名称
	private String VECODE;// 员工编号
	private String PK_EMPLOYEE;//员工主键
	private Double ATTENDANCE;//员工工时(实际)
	private String DAT;//考勤日期
	private Integer STA;//状态  1 正常  0 异常
	private String VKQCARDNO;//考勤卡号
	private String VNAME;//员工姓名
	private String VDCODE;//员工状态编码
	private String VTYP;//员工状态
	private List<WorkAttendance> workAttendanceList;// 保存考勤集合
	private String PK_WA;
	private Integer SHOWLENGTH;//界面数字显示长度
	
	private String flag;	//标识
	
	private Double TOTALHOUR;	//理论工时
	
	public String getPK_WA() {
		return PK_WA;
	}
	public void setPK_WA(String pK_WA) {
		PK_WA = pK_WA;
	}
	public List<WorkAttendance> getWorkAttendanceList() {
		return workAttendanceList;
	}
	public void setWorkAttendanceList(List<WorkAttendance> workAttendanceList) {
		this.workAttendanceList = workAttendanceList;
	}
	public String getPK_STORE() {
		return PK_STORE;
	}
	public void setPK_STORE(String pK_STORE) {
		PK_STORE = pK_STORE;
	}
	public String getVSCODE() {
		return VSCODE;
	}
	public void setVSCODE(String vSCODE) {
		VSCODE = vSCODE;
	}
	public Integer getSHOWLENGTH() {
		return SHOWLENGTH;
	}
	public void setSHOWLENGTH(Integer sHOWLENGTH) {
		SHOWLENGTH = sHOWLENGTH;
	}
	public String getVECODE() {
		return VECODE;
	}
	public void setVECODE(String vECODE) {
		VECODE = vECODE;
	}
	public String getPK_EMPLOYEE() {
		return PK_EMPLOYEE;
	}
	public String getVDCODE() {
		return VDCODE;
	}
	public void setVDCODE(String vDCODE) {
		VDCODE = vDCODE;
	}
	public String getVTYP() {
		return VTYP;
	}
	public void setVTYP(String vTYP) {
		VTYP = vTYP;
	}
	public void setPK_EMPLOYEE(String pK_EMPLOYEE) {
		PK_EMPLOYEE = pK_EMPLOYEE;
	}
	public Double getATTENDANCE() {
		return ATTENDANCE;
	}
	public void setATTENDANCE(Double aTTENDANCE) {
		ATTENDANCE = aTTENDANCE;
	}
	public String getDAT() {
		return DAT;
	}
	public void setDAT(String dAT) {
		DAT = dAT;
	}
	public Integer getSTA() {
		return STA;
	}
	public void setSTA(Integer sTA) {
		STA = sTA;
	}
	public String getVKQCARDNO() {
		return VKQCARDNO;
	}
	public void setVKQCARDNO(String vKQCARDNO) {
		VKQCARDNO = vKQCARDNO;
	}
	public String getVNAME() {
		return VNAME;
	}
	public void setVNAME(String vNAME) {
		VNAME = vNAME;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public Double getTOTALHOUR() {
		return TOTALHOUR;
	}
	public void setTOTALHOUR(Double tOTALHOUR) {
		TOTALHOUR = tOTALHOUR;
	}
}