package com.choice.hr.domain.ScheduleAttendance;

/**
 * 描述：考勤记录
 * @author 马振
 * 创建时间：2015-6-2 上午9:34:40
 */
public class AttendanceRecord {

	private String pk_attendancerecord;	//主键
	private String vcode;
	private String vname;
	private String vinit;
	private String enablestate;
	private String vempno;				//员工号
	private String vename;				//员工名称
	private String dworkdate;			//打卡日期
	private String vtime;				//打卡时间
	private String vkqcardno;			//考勤号
	private String vpunchstate;			//打卡状态：0上班签到、1下班签退、2外出、3外出返回、4加班签到、5加班签退
	private String pk_store;			//门店主键
	private String pk_hrdept;			//部门主键
	private String vstation;			//岗位
	
	private String bdat;				//开始日期
	private String edat;				//结束日期
	
	private String sbqd;				//上班签到
	private String xbqt;				//下班签退
	private String wc;					//外出
	private String wcfh;				//外出返回
	private String jbqd;				//加班签到
	private String jbqt;				//加班签退
	
	private String flag;				//考勤记录或汇总标识
	
	public String getPk_attendancerecord() {
		return pk_attendancerecord;
	}
	public void setPk_attendancerecord(String pk_attendancerecord) {
		this.pk_attendancerecord = pk_attendancerecord;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getVinit() {
		return vinit;
	}
	public void setVinit(String vinit) {
		this.vinit = vinit;
	}
	public String getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(String enablestate) {
		this.enablestate = enablestate;
	}
	public String getVename() {
		return vename;
	}
	public void setVename(String vename) {
		this.vename = vename;
	}
	public String getDworkdate() {
		return dworkdate;
	}
	public void setDworkdate(String dworkdate) {
		this.dworkdate = dworkdate;
	}
	public String getVtime() {
		return vtime;
	}
	public void setVtime(String vtime) {
		this.vtime = vtime;
	}
	public String getVkqcardno() {
		return vkqcardno;
	}
	public void setVkqcardno(String vkqcardno) {
		this.vkqcardno = vkqcardno;
	}
	public String getVpunchstate() {
		return vpunchstate;
	}
	public void setVpunchstate(String vpunchstate) {
		this.vpunchstate = vpunchstate;
	}
	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
	public String getPk_hrdept() {
		return pk_hrdept;
	}
	public void setPk_hrdept(String pk_hrdept) {
		this.pk_hrdept = pk_hrdept;
	}
	public String getBdat() {
		return bdat;
	}
	public void setBdat(String bdat) {
		this.bdat = bdat;
	}
	public String getEdat() {
		return edat;
	}
	public void setEdat(String edat) {
		this.edat = edat;
	}
	public String getSbqd() {
		return sbqd;
	}
	public void setSbqd(String sbqd) {
		this.sbqd = sbqd;
	}
	public String getXbqt() {
		return xbqt;
	}
	public void setXbqt(String xbqt) {
		this.xbqt = xbqt;
	}
	public String getWc() {
		return wc;
	}
	public void setWc(String wc) {
		this.wc = wc;
	}
	public String getWcfh() {
		return wcfh;
	}
	public void setWcfh(String wcfh) {
		this.wcfh = wcfh;
	}
	public String getJbqd() {
		return jbqd;
	}
	public void setJbqd(String jbqd) {
		this.jbqd = jbqd;
	}
	public String getJbqt() {
		return jbqt;
	}
	public void setJbqt(String jbqt) {
		this.jbqt = jbqt;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getVstation() {
		return vstation;
	}
	public void setVstation(String vstation) {
		this.vstation = vstation;
	}
	public String getVempno() {
		return vempno;
	}
	public void setVempno(String vempno) {
		this.vempno = vempno;
	}
}