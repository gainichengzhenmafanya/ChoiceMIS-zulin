package com.choice.hr.domain.ScheduleAttendance;

import java.util.List;

/**
 * 描述：班组排班
 * @author 马振
 * 创建时间：2015-5-26 下午4:15:31
 */
public class CrewScheduling {
	private String pk_crewscheduling;	//主键
	private String pk_store;			//门店主键
	private String pk_hrdept;			//部门主键
	private String pk_team;				//班组主键
	private String pk_shift;			//班次主键
	private String vcode;				//班次编码
	private String vname;				//班次名称
	private String vinit;				//缩写
	private String dworkdate;			//日期
	private String vbdat;				//开始日期
	private String vedat;				//结束日期
	private String enablestate;			//启用状态
	
	private String pk_employee;			//员工主键
	private String vempno;				//员工编码
	private String vempname;			//员工姓名
	private String vstation;			//所在岗位
	private String vkqcardno;			//考勤号
	
	private String vbegintime1;			//开始时间
	private Integer size;				//长度(list的长度)
	private List<CrewScheduling> listCrew;
	private List<Shift> listShift;
	
	public String getPk_crewscheduling() {
		return pk_crewscheduling;
	}
	public void setPk_crewscheduling(String pk_crewscheduling) {
		this.pk_crewscheduling = pk_crewscheduling;
	}
	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
	public String getPk_hrdept() {
		return pk_hrdept;
	}
	public void setPk_hrdept(String pk_hrdept) {
		this.pk_hrdept = pk_hrdept;
	}
	public String getPk_team() {
		return pk_team;
	}
	public void setPk_team(String pk_team) {
		this.pk_team = pk_team;
	}
	public String getPk_shift() {
		return pk_shift;
	}
	public void setPk_shift(String pk_shift) {
		this.pk_shift = pk_shift;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getDworkdate() {
		return dworkdate;
	}
	public void setDworkdate(String dworkdate) {
		this.dworkdate = dworkdate;
	}
	public String getVinit() {
		return vinit;
	}
	public void setVinit(String vinit) {
		this.vinit = vinit;
	}
	public String getVbdat() {
		return vbdat;
	}
	public void setVbdat(String vbdat) {
		this.vbdat = vbdat;
	}
	public String getVedat() {
		return vedat;
	}
	public void setVedat(String vedat) {
		this.vedat = vedat;
	}
	public String getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(String enablestate) {
		this.enablestate = enablestate;
	}
	public List<CrewScheduling> getListCrew() {
		return listCrew;
	}
	public void setListCrew(List<CrewScheduling> listCrew) {
		this.listCrew = listCrew;
	}
	public String getPk_employee() {
		return pk_employee;
	}
	public void setPk_employee(String pk_employee) {
		this.pk_employee = pk_employee;
	}
	public String getVempno() {
		return vempno;
	}
	public void setVempno(String vempno) {
		this.vempno = vempno;
	}
	public String getVempname() {
		return vempname;
	}
	public void setVempname(String vempname) {
		this.vempname = vempname;
	}
	public String getVstation() {
		return vstation;
	}
	public void setVstation(String vstation) {
		this.vstation = vstation;
	}
	public String getVkqcardno() {
		return vkqcardno;
	}
	public void setVkqcardno(String vkqcardno) {
		this.vkqcardno = vkqcardno;
	}
	public List<Shift> getListShift() {
		return listShift;
	}
	public void setListShift(List<Shift> listShift) {
		this.listShift = listShift;
	}
	public String getVbegintime1() {
		return vbegintime1;
	}
	public void setVbegintime1(String vbegintime1) {
		this.vbegintime1 = vbegintime1;
	}
	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}
}