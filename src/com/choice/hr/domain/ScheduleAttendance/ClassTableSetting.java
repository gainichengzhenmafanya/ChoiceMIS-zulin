package com.choice.hr.domain.ScheduleAttendance;

import java.util.List;

/**
 * 描述：排班考勤——班表设定
 * @author 马振
 * 创建时间：2015-6-16 上午9:30:39
 */
public class ClassTableSetting {
	
	private String pk_tablesetting;	//主键
	
	private String pk_store;		//门店主键
	
	private String dworkdate;		//日期

	private String dworkdate1;		//日期1(复制时用到)
	
	private String workstation;		//工作站
	
	private String vbegintime;		//开始时间

	private String vendtime;		//结束时间

	private String totalhour;		//工时合计
	
	private Integer npeople;		//人数
	
	private String td;				//用于存储单元格id
	
	private List<String> listTime;	//存储1~24数字
	
	private List<String> listInt;	//存储1~48个数字
	
	private List<ClassTableSetting> listClassTableSetting;
	
	public String getPk_tablesetting() {
		return pk_tablesetting;
	}

	public void setPk_tablesetting(String pk_tablesetting) {
		this.pk_tablesetting = pk_tablesetting;
	}

	public String getPk_store() {
		return pk_store;
	}

	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}

	public String getDworkdate() {
		return dworkdate;
	}

	public void setDworkdate(String dworkdate) {
		this.dworkdate = dworkdate;
	}

	public String getDworkdate1() {
		return dworkdate1;
	}

	public void setDworkdate1(String dworkdate1) {
		this.dworkdate1 = dworkdate1;
	}

	public String getWorkstation() {
		return workstation;
	}

	public void setWorkstation(String workstation) {
		this.workstation = workstation;
	}

	public String getVbegintime() {
		return vbegintime;
	}

	public void setVbegintime(String vbegintime) {
		this.vbegintime = vbegintime;
	}

	public String getVendtime() {
		return vendtime;
	}

	public void setVendtime(String vendtime) {
		this.vendtime = vendtime;
	}

	public List<String> getListTime() {
		return listTime;
	}

	public String getTotalhour() {
		return totalhour;
	}

	public void setTotalhour(String totalhour) {
		this.totalhour = totalhour;
	}

	public Integer getNpeople() {
		return npeople;
	}

	public void setNpeople(Integer npeople) {
		this.npeople = npeople;
	}

	public String getTd() {
		return td;
	}

	public void setTd(String td) {
		this.td = td;
	}

	public void setListTime(List<String> listTime) {
		this.listTime = listTime;
	}

	public List<String> getListInt() {
		return listInt;
	}

	public void setListInt(List<String> listInt) {
		this.listInt = listInt;
	}

	public List<ClassTableSetting> getListClassTableSetting() {
		return listClassTableSetting;
	}

	public void setListClassTableSetting(
			List<ClassTableSetting> listClassTableSetting) {
		this.listClassTableSetting = listClassTableSetting;
	}
}