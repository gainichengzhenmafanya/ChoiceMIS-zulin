package com.choice.hr.domain.ScheduleAttendance;

/**
 * 描述：班组
 * @author 马振
 * 创建时间：2015-5-25 下午1:35:37
 */
public class Team {
	private String  pk_team;		//主键
	private String  pk_store;		//门店主键
	private String  pk_hrdept;		//人力资源部门主键
	private String  vcode;			//编码
	private String  vname;			//名称
	private String  vinit;			//缩写
	private String  enablestate;	//启用状态
	private Integer isortno;		//排序列
	private String  vmemo;			//备注
	
	private String  flag;			//增删改的标识
	
	public String getPk_team() {
		return pk_team;
	}
	public void setPk_team(String pk_team) {
		this.pk_team = pk_team;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getVinit() {
		return vinit;
	}
	public void setVinit(String vinit) {
		this.vinit = vinit;
	}
	public Integer getIsortno() {
		return isortno;
	}
	public void setIsortno(Integer isortno) {
		this.isortno = isortno;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
	public String getPk_hrdept() {
		return pk_hrdept;
	}
	public void setPk_hrdept(String pk_hrdept) {
		this.pk_hrdept = pk_hrdept;
	}
	public String getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(String enablestate) {
		this.enablestate = enablestate;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
}