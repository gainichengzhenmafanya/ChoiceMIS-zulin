package com.choice.hr.domain.ScheduleAttendance;

/**
 * 描述：排班考勤——考勤工时汇总
 * @author 马振
 * 创建时间：2015-6-24 下午1:41:52
 */
public class AttendanceSummary {

	private String dworkdate;	//营业日
	private String attendance;	//实际工时
	private String totalhour;	//理论工时
	private String cha;			//工时差异
	private String vbdat;		//开始日期
	private String vedat;		//结束日期
	private String pk_store;	//门店主键
	
	public String getDworkdate() {
		return dworkdate;
	}
	public void setDworkdate(String dworkdate) {
		this.dworkdate = dworkdate;
	}
	public String getAttendance() {
		return attendance;
	}
	public void setAttendance(String attendance) {
		this.attendance = attendance;
	}
	public String getTotalhour() {
		return totalhour;
	}
	public void setTotalhour(String totalhour) {
		this.totalhour = totalhour;
	}
	public String getCha() {
		return cha;
	}
	public void setCha(String cha) {
		this.cha = cha;
	}
	public String getVbdat() {
		return vbdat;
	}
	public void setVbdat(String vbdat) {
		this.vbdat = vbdat;
	}
	public String getVedat() {
		return vedat;
	}
	public void setVedat(String vedat) {
		this.vedat = vedat;
	}
	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
}