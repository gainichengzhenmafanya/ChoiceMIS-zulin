package com.choice.hr.domain.ScheduleAttendance;

/**
 * 描述：班次
 * @author 马振
 * 创建时间：2015-5-26 下午1:28:46
 */
public class Shift {
	private String  pk_shift;		//主键
	private String  pk_store;		//门店主键
	private String  vcode;			//编码
	private String  vname;			//名称
	private String  vbegintime1;	//开始时间1
	private String  vendtime1;		//结束时间1
	private String  vbegintime2;	//开始时间2
	private String  vendtime2;		//结束时间2
	private String  vmemo;			//备注
	
	private String  flag;			//增删改的标识
	
	private String pk_team;			//班组
	
	private String dworkdate;
	private String pk_hrdept;

	public String getPk_shift() {
		return pk_shift;
	}

	public void setPk_shift(String pk_shift) {
		this.pk_shift = pk_shift;
	}

	public String getPk_store() {
		return pk_store;
	}

	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}

	public String getVcode() {
		return vcode;
	}

	public void setVcode(String vcode) {
		this.vcode = vcode;
	}

	public String getVname() {
		return vname;
	}

	public void setVname(String vname) {
		this.vname = vname;
	}

	public String getVbegintime1() {
		return vbegintime1;
	}

	public void setVbegintime1(String vbegintime1) {
		this.vbegintime1 = vbegintime1;
	}

	public String getVendtime1() {
		return vendtime1;
	}

	public void setVendtime1(String vendtime1) {
		this.vendtime1 = vendtime1;
	}

	public String getVbegintime2() {
		return vbegintime2;
	}

	public void setVbegintime2(String vbegintime2) {
		this.vbegintime2 = vbegintime2;
	}

	public String getVendtime2() {
		return vendtime2;
	}

	public void setVendtime2(String vendtime2) {
		this.vendtime2 = vendtime2;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getVmemo() {
		return vmemo;
	}

	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}

	public String getDworkdate() {
		return dworkdate;
	}

	public void setDworkdate(String dworkdate) {
		this.dworkdate = dworkdate;
	}

	public String getPk_hrdept() {
		return pk_hrdept;
	}

	public void setPk_hrdept(String pk_hrdept) {
		this.pk_hrdept = pk_hrdept;
	}

	public String getPk_team() {
		return pk_team;
	}

	public void setPk_team(String pk_team) {
		this.pk_team = pk_team;
	}
}