package com.choice.hr.domain.PersonnelManagement;

/**
 * 描述：家庭
 * @author 马振
 * 创建时间：2015-5-15 上午9:35:47
 */
public class Family {

	private String pk_family;
	private String pk_employee;
	private String pk_store;
	private String vmembers;		//成员
	private String vrelationship;	//关系
	private String vsex;			//性别
	private String vbirthday;		//出生日期
	private String vpost;			//职务
	private String vpubliclist;		//政治面貌
	private String vworkunit;		//工作单位
	private String vphone;			//联系电话
	private String vaddress;		//联系地址
	private String flag;			//新增或修改的标识
	
	public String getPk_family() {
		return pk_family;
	}
	public void setPk_family(String pk_family) {
		this.pk_family = pk_family;
	}
	public String getPk_employee() {
		return pk_employee;
	}
	public void setPk_employee(String pk_employee) {
		this.pk_employee = pk_employee;
	}
	public String getVmembers() {
		return vmembers;
	}
	public void setVmembers(String vmembers) {
		this.vmembers = vmembers;
	}
	public String getVrelationship() {
		return vrelationship;
	}
	public void setVrelationship(String vrelationship) {
		this.vrelationship = vrelationship;
	}
	public String getVsex() {
		return vsex;
	}
	public void setVsex(String vsex) {
		this.vsex = vsex;
	}
	public String getVbirthday() {
		return vbirthday;
	}
	public void setVbirthday(String vbirthday) {
		this.vbirthday = vbirthday;
	}
	public String getVpost() {
		return vpost;
	}
	public void setVpost(String vpost) {
		this.vpost = vpost;
	}
	public String getVpubliclist() {
		return vpubliclist;
	}
	public void setVpubliclist(String vpubliclist) {
		this.vpubliclist = vpubliclist;
	}
	public String getVworkunit() {
		return vworkunit;
	}
	public void setVworkunit(String vworkunit) {
		this.vworkunit = vworkunit;
	}
	public String getVphone() {
		return vphone;
	}
	public void setVphone(String vphone) {
		this.vphone = vphone;
	}
	public String getVaddress() {
		return vaddress;
	}
	public void setVaddress(String vaddress) {
		this.vaddress = vaddress;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
}
