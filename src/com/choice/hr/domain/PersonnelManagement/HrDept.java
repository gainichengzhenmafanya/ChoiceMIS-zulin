package com.choice.hr.domain.PersonnelManagement;

/**
 * 描述：人力资源——部门
 * @author 马振
 * 创建时间：2015-5-11 上午10:34:11
 */
public class HrDept {
	private String  pk_hrdept;		//主键
	private String  vcode;			//编码
	private String  vname;			//名称
	private String  vinit;			//缩写
	private Integer isortno;		//排序列
	private Integer enablestate;	//启用状态
	private String  vmemo;			//备注
	private String  pk_store;		//门店主键
	private Integer iproportion;	//分成比例（10% 输入10）
	private Integer istatistic;		//部门属性（统计类型）
	private String  pk_brand;		//自定义项
	private String  pk_brand1;		//自定义项1
	private String  pk_brand2;		//自定义项2
	private String  pk_brand3;		//自定义项3
	private String  pk_brand4;		//自定义项4
	private String  pk_brand5;		//自定义项5
	private String  pk_brand6;		//自定义项6
	
	public String getPk_hrdept() {
		return pk_hrdept;
	}
	public void setPk_hrdept(String pk_hrdept) {
		this.pk_hrdept = pk_hrdept;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getVinit() {
		return vinit;
	}
	public void setVinit(String vinit) {
		this.vinit = vinit;
	}
	public Integer getIsortno() {
		return isortno;
	}
	public void setIsortno(Integer isortno) {
		this.isortno = isortno;
	}
	public Integer getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(Integer enablestate) {
		this.enablestate = enablestate;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public Integer getIproportion() {
		return iproportion;
	}
	public void setIproportion(Integer iproportion) {
		this.iproportion = iproportion;
	}
	public Integer getIstatistic() {
		return istatistic;
	}
	public void setIstatistic(Integer istatistic) {
		this.istatistic = istatistic;
	}
	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
	public String getPk_brand() {
		return pk_brand;
	}
	public void setPk_brand(String pk_brand) {
		this.pk_brand = pk_brand;
	}
	public String getPk_brand1() {
		return pk_brand1;
	}
	public void setPk_brand1(String pk_brand1) {
		this.pk_brand1 = pk_brand1;
	}
	public String getPk_brand2() {
		return pk_brand2;
	}
	public void setPk_brand2(String pk_brand2) {
		this.pk_brand2 = pk_brand2;
	}
	public String getPk_brand3() {
		return pk_brand3;
	}
	public void setPk_brand3(String pk_brand3) {
		this.pk_brand3 = pk_brand3;
	}
	public String getPk_brand4() {
		return pk_brand4;
	}
	public void setPk_brand4(String pk_brand4) {
		this.pk_brand4 = pk_brand4;
	}
	public String getPk_brand5() {
		return pk_brand5;
	}
	public void setPk_brand5(String pk_brand5) {
		this.pk_brand5 = pk_brand5;
	}
	public String getPk_brand6() {
		return pk_brand6;
	}
	public void setPk_brand6(String pk_brand6) {
		this.pk_brand6 = pk_brand6;
	}
}