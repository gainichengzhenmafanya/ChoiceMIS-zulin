package com.choice.hr.domain.PersonnelManagement;

/**
 * 描述：人员信息
 * @author 马振
 * 创建时间：2015-5-12 上午9:26:08
 */
public class Employee {
	private String pk_employee;	//主键
	private String vacct;			//账套号
	private String vempno;			//员工编号
	private String vkqcardno;		//考勤编号
	private String vcode;			//编码
	private String vname;			//姓名
	private String vcnname;		//中文姓名
	private String vsex;			//性别：0男、1女
	private Integer vage;			//年龄
	private String vinit;			//缩写	
	private String vmarriage;		//婚姻状况：0已婚、1未婚、2离异
	private String vidcard;		//身份证号
	private String vjrdat;			//入职日期 
	private String vplace;			//专业
	private String vstatus;		//人事状态：1试用、2在职、3离职、4退休、5删除
	private String vstatusname;	//人事状态名称
	private String vstatuscode;	//人事状态编号
	private String vtele1;			//电话1	
	private String vtele2;			//电话2	
	private String vmobil;			//手机
	private String vaddress;		//联系地址
	private String vyfirm;			//单位住宿
	private String vbirthdat;		//出生日期 
	private String djrtim;   		//入职时间
	private String vjremp;			//入职审核人 
	private String vlr;			//入职申报审核日期 
	private String vdays;   		//入职全天/半天：0全天、1半天
	private String vwkdat;			//转正日期 
	private String vwkemp;			//转正审核人 
	private String dwktim;			//转正时间
	private String vlvfdat;		//暂住证有效期自 
	private String vyjsz;   		//计生证
	private String vywgz;  		//业绩锁定
	private String vyzzz;   		//暂住证
	private String vlvtodat;		//暂住证有效期至 
	private String vwagetyp;		//社保帐号 
	private String vbankno;		//银行卡号 
	private String vyholiday;   	//享受年假
	private String vdormfdat;		//宿舍入住时间 
	private String vdormtdat;		//宿舍离住时间 
	private String vlastpxdat;		//最后培训日期 
	private String vlastwgdat;		//最后转正日期 
	private String nmedamt;		//医疗保险金额 
	private String ndockamt;		//工伤保险金额 
	private String vmedno;			//医保帐号 
	private String nroomamt;		//公积金 
	private String vyold;   		//是否养老保险
	private String noldamt;		//养老保险金额 
	private String vyjkz;   		//健康证
	private String vjkcardno;		//健康证编号 
	private String vjkfdat;		//健康证有效期自 
	private String vjktdat;		//健康证有效期至 
	private String nholiday; 		//年休假天数  
	private String nholdaycnt; 	//年假天数  
	private String vidvaldat;		//身份证有效期至 
	private String dchktim;   		//审核时间
	private String vtechnic;		//外语等级
	private String vpostcode;		//邮编 
	private String vpackfdat;		//合同期自 
	private String vpacktdat;		//合同期至 
	private String vfirstpackdat;	//首次签合同日期 
	private String vpacknote;		//合同续签通知日 
	private String vpackexedat;	//合同执行日期 
	private String vschool;		//毕业学校 
	private String vgzdept;		//工资部门 
	private String nchgwgcnt;		//工资调整次数  
	private String ngzwage;		//基本工资 
	private String vwage;			//工资标准 
	private String vstopwage;   	//停发工资
	private String vytax;   		//个税锁定
	private String ntaxamt;		//个税锁定金额 
	private String vymulacct;   	//工资拆分
	private String vytim;   		//计时工
	private String nsavettl;		//储蓄累计 
	private String nzforegift;		//押金累扣 
	private String nggss;			//公共设施分摊费 
	private String nforegift;		//押金标准 
	private String nsavemth;		//每月储蓄 
	private String vbxflag;		//保险标志 
	private String vinvalid;		//保险作废日期
	private String vwgid;			//班组状态 
	private String vbmxy;   		//是否签署保密协议
	private String vypdjb;			//应聘等级表/特殊岗位应聘等级表 
	private String vcphoto;   		//是否留存照片
	private String vcardfj;   		//是否保障身份证复印件
	private String vdbs;			//担保书 
	private String vfzxh;			//服装型号 
	private String nfzsl; 			//服装数量  
	private String vseason;		//服装季节 
	private String vfzks;			//服装款式 
	private String vfzgw;			//服装岗位 
	private String vhr;   			//兼职人事管理：Y是、N否
	private String vsdg;   		//兼职水电工：Y是、N否
	private String vyblack;   		//黑名单
	private String vbackmemo;		//黑名单描述 
	private String vdeldat;		//删除日期 
	private String vdelemp;		//删除操作人 
	private String vleftdat;		//离职日期 
	private String vleftrsn;		//离职原因
	private String vchestno;		//更衣柜号 
	private String vmemo;			//备注 
	private String vgl;			//工龄
	private String vglbt;			//工龄补贴
	private String vhtjsgl;		//是否合同计算工龄：Y是、N否
	private String pk_team;		//班组
	
	private String vworktyp;		//用工形式
	private String vpubliclist;	//政治面貌
	private String vhouse;			//户口类型
	private String vbxsta;			//保险状态
	private String vschoolage;		//学历
	private String vdegree;		//学位
	private String vdeptgrp;		//部门分组
	private String vnation;		//国籍
	private String vhome;			//籍贯
	private String vbanktyp;		//银行卡类型
	private String vjkarea;		//健康证办理地区
	private String vtaxno;			//税率类型 
	private String vpacktyp;		//合同类型
	private String vdutyid;		//工资及别
	private String vcomfrom;		//用工来源
	private String vposition;		//职位
	private String vinsurance;		//保险种类
	private String bstation;		//是否主要职位:0是  1否，默认0
	private String vstationcode;	//职位编码
	private String vstation;		//职位描述
	private String vdutylvl;		//人事级别名称
	private String vtrial;			//试用期
	private String vpositivereason;//转正理由
	private String vcalendartyp;	//公司日历类别
	private String pk_group;		//集团
	private String vcardcode;		//银行卡号
	private String vcardname;		//银行卡名称
	private String vkhr;			//开户人
	private String pk_positivereason;
	private String pk_nation;      //民族
	private String pk_schoolage;   //学历
	private String pk_degree;      //学位
	private String pk_publiclist;  //政治面貌
	private String pk_house;      	//户口类型
	private String pk_worktyp;    	//用工形式
    private String pk_home;      	//籍贯
	private String pk_banktyp;    	//银行卡类型
	private String pk_jkarea;      //健康证办理地区
	private String pk_taxno;      	//税率类型 
	private String pk_packtyp;    	//合同类型
	private String pk_dutyid;      //工资及别
	private String pk_comfrom;    	//用工来源
	private String pk_bxsta;      	//保险状态
	private String pk_dutylvl;    	//人事级别
	private String pk_technic;    	//外语等级
	private String pk_station;    	//任职岗位
	private String pk_status;    	//人事状态
	private String pk_marriage;    //婚姻状况
	private String pk_sex;    		//性别
	private String pk_store;		//门店主键
	private String vscode;			//门店编码
	private String vscodedes;		//门店名称
	private String pk_hrdept;		//部门主键
	private String vdeptcode;		//部门编码
	private String vdeptdes;		//部门名称
	private String pk_brand;		//品牌
	private String pk_area;		//区域
	private String pk_market;		//市场
	private String pk_dept;		//部门
	
	
	private String flag;			//判断是新增或修改的标识
	private String buttontyp;		//按钮类型(档案、转正或其他)
	
	public String getPk_employee() {
		return pk_employee;
	}
	public void setPk_employee(String pk_employee) {
		this.pk_employee = pk_employee;
	}
	public String getVacct() {
		return vacct;
	}
	public void setVacct(String vacct) {
		this.vacct = vacct;
	}
	public String getVempno() {
		return vempno;
	}
	public void setVempno(String vempno) {
		this.vempno = vempno;
	}
	public String getVkqcardno() {
		return vkqcardno;
	}
	public void setVkqcardno(String vkqcardno) {
		this.vkqcardno = vkqcardno;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getVsex() {
		return vsex;
	}
	public void setVsex(String vsex) {
		this.vsex = vsex;
	}
	public Integer getVage() {
		return vage;
	}
	public void setVage(Integer vage) {
		this.vage = vage;
	}
	public String getVinit() {
		return vinit;
	}
	public void setVinit(String vinit) {
		this.vinit = vinit;
	}
	public String getVmarriage() {
		return vmarriage;
	}
	public void setVmarriage(String vmarriage) {
		this.vmarriage = vmarriage;
	}
	public String getVidcard() {
		return vidcard;
	}
	public void setVidcard(String vidcard) {
		this.vidcard = vidcard;
	}
	public String getVplace() {
		return vplace;
	}
	public void setVplace(String vplace) {
		this.vplace = vplace;
	}
	public String getVstatus() {
		return vstatus;
	}
	public void setVstatus(String vstatus) {
		this.vstatus = vstatus;
	}
	public String getVtele1() {
		return vtele1;
	}
	public void setVtele1(String vtele1) {
		this.vtele1 = vtele1;
	}
	public String getVtele2() {
		return vtele2;
	}
	public void setVtele2(String vtele2) {
		this.vtele2 = vtele2;
	}
	public String getVmobil() {
		return vmobil;
	}
	public void setVmobil(String vmobil) {
		this.vmobil = vmobil;
	}
	public String getVaddress() {
		return vaddress;
	}
	public void setVaddress(String vaddress) {
		this.vaddress = vaddress;
	}
	public String getVyfirm() {
		return vyfirm;
	}
	public void setVyfirm(String vyfirm) {
		this.vyfirm = vyfirm;
	}
	public String getVbirthdat() {
		return vbirthdat;
	}
	public void setVbirthdat(String vbirthdat) {
		this.vbirthdat = vbirthdat;
	}
	public String getDjrtim() {
		return djrtim;
	}
	public void setDjrtim(String djrtim) {
		this.djrtim = djrtim;
	}
	public String getVjrdat() {
		return vjrdat;
	}
	public void setVjrdat(String vjrdat) {
		this.vjrdat = vjrdat;
	}
	public String getVjremp() {
		return vjremp;
	}
	public void setVjremp(String vjremp) {
		this.vjremp = vjremp;
	}
	public String getVlr() {
		return vlr;
	}
	public void setVlr(String vlr) {
		this.vlr = vlr;
	}
	public String getVdays() {
		return vdays;
	}
	public void setVdays(String vdays) {
		this.vdays = vdays;
	}
	public String getVwkdat() {
		return vwkdat;
	}
	public void setVwkdat(String vwkdat) {
		this.vwkdat = vwkdat;
	}
	public String getVwkemp() {
		return vwkemp;
	}
	public void setVwkemp(String vwkemp) {
		this.vwkemp = vwkemp;
	}
	public String getDwktim() {
		return dwktim;
	}
	public void setDwktim(String dwktim) {
		this.dwktim = dwktim;
	}
	public String getVlvfdat() {
		return vlvfdat;
	}
	public void setVlvfdat(String vlvfdat) {
		this.vlvfdat = vlvfdat;
	}
	public String getVyjsz() {
		return vyjsz;
	}
	public void setVyjsz(String vyjsz) {
		this.vyjsz = vyjsz;
	}
	public String getVywgz() {
		return vywgz;
	}
	public void setVywgz(String vywgz) {
		this.vywgz = vywgz;
	}
	public String getVyzzz() {
		return vyzzz;
	}
	public void setVyzzz(String vyzzz) {
		this.vyzzz = vyzzz;
	}
	public String getVlvtodat() {
		return vlvtodat;
	}
	public void setVlvtodat(String vlvtodat) {
		this.vlvtodat = vlvtodat;
	}
	public String getVwagetyp() {
		return vwagetyp;
	}
	public void setVwagetyp(String vwagetyp) {
		this.vwagetyp = vwagetyp;
	}
	public String getVbankno() {
		return vbankno;
	}
	public void setVbankno(String vbankno) {
		this.vbankno = vbankno;
	}
	public String getVyholiday() {
		return vyholiday;
	}
	public void setVyholiday(String vyholiday) {
		this.vyholiday = vyholiday;
	}
	public String getVdormfdat() {
		return vdormfdat;
	}
	public void setVdormfdat(String vdormfdat) {
		this.vdormfdat = vdormfdat;
	}
	public String getVdormtdat() {
		return vdormtdat;
	}
	public void setVdormtdat(String vdormtdat) {
		this.vdormtdat = vdormtdat;
	}
	public String getVlastpxdat() {
		return vlastpxdat;
	}
	public void setVlastpxdat(String vlastpxdat) {
		this.vlastpxdat = vlastpxdat;
	}
	public String getVlastwgdat() {
		return vlastwgdat;
	}
	public void setVlastwgdat(String vlastwgdat) {
		this.vlastwgdat = vlastwgdat;
	}
	public String getNmedamt() {
		return nmedamt;
	}
	public void setNmedamt(String nmedamt) {
		this.nmedamt = nmedamt;
	}
	public String getNdockamt() {
		return ndockamt;
	}
	public void setNdockamt(String ndockamt) {
		this.ndockamt = ndockamt;
	}
	public String getVmedno() {
		return vmedno;
	}
	public void setVmedno(String vmedno) {
		this.vmedno = vmedno;
	}
	public String getNroomamt() {
		return nroomamt;
	}
	public void setNroomamt(String nroomamt) {
		this.nroomamt = nroomamt;
	}
	public String getVyold() {
		return vyold;
	}
	public void setVyold(String vyold) {
		this.vyold = vyold;
	}
	public String getNoldamt() {
		return noldamt;
	}
	public void setNoldamt(String noldamt) {
		this.noldamt = noldamt;
	}
	public String getVyjkz() {
		return vyjkz;
	}
	public void setVyjkz(String vyjkz) {
		this.vyjkz = vyjkz;
	}
	public String getVjkcardno() {
		return vjkcardno;
	}
	public void setVjkcardno(String vjkcardno) {
		this.vjkcardno = vjkcardno;
	}
	public String getVjkfdat() {
		return vjkfdat;
	}
	public void setVjkfdat(String vjkfdat) {
		this.vjkfdat = vjkfdat;
	}
	public String getVjktdat() {
		return vjktdat;
	}
	public void setVjktdat(String vjktdat) {
		this.vjktdat = vjktdat;
	}
	public String getNholiday() {
		return nholiday;
	}
	public void setNholiday(String nholiday) {
		this.nholiday = nholiday;
	}
	public String getNholdaycnt() {
		return nholdaycnt;
	}
	public void setNholdaycnt(String nholdaycnt) {
		this.nholdaycnt = nholdaycnt;
	}
	public String getVidvaldat() {
		return vidvaldat;
	}
	public void setVidvaldat(String vidvaldat) {
		this.vidvaldat = vidvaldat;
	}
	public String getDchktim() {
		return dchktim;
	}
	public void setDchktim(String dchktim) {
		this.dchktim = dchktim;
	}
	public String getVtechnic() {
		return vtechnic;
	}
	public void setVtechnic(String vtechnic) {
		this.vtechnic = vtechnic;
	}
	public String getVpostcode() {
		return vpostcode;
	}
	public void setVpostcode(String vpostcode) {
		this.vpostcode = vpostcode;
	}
	public String getVpackfdat() {
		return vpackfdat;
	}
	public void setVpackfdat(String vpackfdat) {
		this.vpackfdat = vpackfdat;
	}
	public String getVpacktdat() {
		return vpacktdat;
	}
	public void setVpacktdat(String vpacktdat) {
		this.vpacktdat = vpacktdat;
	}
	public String getVfirstpackdat() {
		return vfirstpackdat;
	}
	public void setVfirstpackdat(String vfirstpackdat) {
		this.vfirstpackdat = vfirstpackdat;
	}
	public String getVpacknote() {
		return vpacknote;
	}
	public void setVpacknote(String vpacknote) {
		this.vpacknote = vpacknote;
	}
	public String getVpackexedat() {
		return vpackexedat;
	}
	public void setVpackexedat(String vpackexedat) {
		this.vpackexedat = vpackexedat;
	}
	public String getVschool() {
		return vschool;
	}
	public void setVschool(String vschool) {
		this.vschool = vschool;
	}
	public String getVgzdept() {
		return vgzdept;
	}
	public void setVgzdept(String vgzdept) {
		this.vgzdept = vgzdept;
	}
	public String getNchgwgcnt() {
		return nchgwgcnt;
	}
	public void setNchgwgcnt(String nchgwgcnt) {
		this.nchgwgcnt = nchgwgcnt;
	}
	public String getNgzwage() {
		return ngzwage;
	}
	public void setNgzwage(String ngzwage) {
		this.ngzwage = ngzwage;
	}
	public String getVwage() {
		return vwage;
	}
	public void setVwage(String vwage) {
		this.vwage = vwage;
	}
	public String getVstopwage() {
		return vstopwage;
	}
	public void setVstopwage(String vstopwage) {
		this.vstopwage = vstopwage;
	}
	public String getVytax() {
		return vytax;
	}
	public void setVytax(String vytax) {
		this.vytax = vytax;
	}
	public String getNtaxamt() {
		return ntaxamt;
	}
	public void setNtaxamt(String ntaxamt) {
		this.ntaxamt = ntaxamt;
	}
	public String getVymulacct() {
		return vymulacct;
	}
	public void setVymulacct(String vymulacct) {
		this.vymulacct = vymulacct;
	}
	public String getVytim() {
		return vytim;
	}
	public void setVytim(String vytim) {
		this.vytim = vytim;
	}
	public String getNsavettl() {
		return nsavettl;
	}
	public void setNsavettl(String nsavettl) {
		this.nsavettl = nsavettl;
	}
	public String getNzforegift() {
		return nzforegift;
	}
	public void setNzforegift(String nzforegift) {
		this.nzforegift = nzforegift;
	}
	public String getNggss() {
		return nggss;
	}
	public void setNggss(String nggss) {
		this.nggss = nggss;
	}
	public String getNforegift() {
		return nforegift;
	}
	public void setNforegift(String nforegift) {
		this.nforegift = nforegift;
	}
	public String getNsavemth() {
		return nsavemth;
	}
	public void setNsavemth(String nsavemth) {
		this.nsavemth = nsavemth;
	}
	public String getVbxflag() {
		return vbxflag;
	}
	public void setVbxflag(String vbxflag) {
		this.vbxflag = vbxflag;
	}
	public String getVinvalid() {
		return vinvalid;
	}
	public void setVinvalid(String vinvalid) {
		this.vinvalid = vinvalid;
	}
	public String getVwgid() {
		return vwgid;
	}
	public void setVwgid(String vwgid) {
		this.vwgid = vwgid;
	}
	public String getVbmxy() {
		return vbmxy;
	}
	public void setVbmxy(String vbmxy) {
		this.vbmxy = vbmxy;
	}
	public String getVypdjb() {
		return vypdjb;
	}
	public void setVypdjb(String vypdjb) {
		this.vypdjb = vypdjb;
	}
	public String getVcphoto() {
		return vcphoto;
	}
	public void setVcphoto(String vcphoto) {
		this.vcphoto = vcphoto;
	}
	public String getVcardfj() {
		return vcardfj;
	}
	public void setVcardfj(String vcardfj) {
		this.vcardfj = vcardfj;
	}
	public String getVdbs() {
		return vdbs;
	}
	public void setVdbs(String vdbs) {
		this.vdbs = vdbs;
	}
	public String getVfzxh() {
		return vfzxh;
	}
	public void setVfzxh(String vfzxh) {
		this.vfzxh = vfzxh;
	}
	public String getNfzsl() {
		return nfzsl;
	}
	public void setNfzsl(String nfzsl) {
		this.nfzsl = nfzsl;
	}
	public String getVseason() {
		return vseason;
	}
	public void setVseason(String vseason) {
		this.vseason = vseason;
	}
	public String getVfzks() {
		return vfzks;
	}
	public void setVfzks(String vfzks) {
		this.vfzks = vfzks;
	}
	public String getVfzgw() {
		return vfzgw;
	}
	public void setVfzgw(String vfzgw) {
		this.vfzgw = vfzgw;
	}
	public String getVhr() {
		return vhr;
	}
	public void setVhr(String vhr) {
		this.vhr = vhr;
	}
	public String getVsdg() {
		return vsdg;
	}
	public void setVsdg(String vsdg) {
		this.vsdg = vsdg;
	}
	public String getVyblack() {
		return vyblack;
	}
	public void setVyblack(String vyblack) {
		this.vyblack = vyblack;
	}
	public String getVbackmemo() {
		return vbackmemo;
	}
	public void setVbackmemo(String vbackmemo) {
		this.vbackmemo = vbackmemo;
	}
	public String getVdeldat() {
		return vdeldat;
	}
	public void setVdeldat(String vdeldat) {
		this.vdeldat = vdeldat;
	}
	public String getVdelemp() {
		return vdelemp;
	}
	public void setVdelemp(String vdelemp) {
		this.vdelemp = vdelemp;
	}
	public String getVleftdat() {
		return vleftdat;
	}
	public void setVleftdat(String vleftdat) {
		this.vleftdat = vleftdat;
	}
	public String getVleftrsn() {
		return vleftrsn;
	}
	public void setVleftrsn(String vleftrsn) {
		this.vleftrsn = vleftrsn;
	}
	public String getVchestno() {
		return vchestno;
	}
	public void setVchestno(String vchestno) {
		this.vchestno = vchestno;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public String getVgl() {
		return vgl;
	}
	public void setVgl(String vgl) {
		this.vgl = vgl;
	}
	public String getVglbt() {
		return vglbt;
	}
	public void setVglbt(String vglbt) {
		this.vglbt = vglbt;
	}
	public String getVhtjsgl() {
		return vhtjsgl;
	}
	public void setVhtjsgl(String vhtjsgl) {
		this.vhtjsgl = vhtjsgl;
	}
	public String getVworktyp() {
		return vworktyp;
	}
	public void setVworktyp(String vworktyp) {
		this.vworktyp = vworktyp;
	}
	public String getVpubliclist() {
		return vpubliclist;
	}
	public void setVpubliclist(String vpubliclist) {
		this.vpubliclist = vpubliclist;
	}
	public String getVhouse() {
		return vhouse;
	}
	public void setVhouse(String vhouse) {
		this.vhouse = vhouse;
	}
	public String getVbxsta() {
		return vbxsta;
	}
	public void setVbxsta(String vbxsta) {
		this.vbxsta = vbxsta;
	}
	public String getVschoolage() {
		return vschoolage;
	}
	public void setVschoolage(String vschoolage) {
		this.vschoolage = vschoolage;
	}
	public String getVdegree() {
		return vdegree;
	}
	public void setVdegree(String vdegree) {
		this.vdegree = vdegree;
	}
	public String getVdeptgrp() {
		return vdeptgrp;
	}
	public void setVdeptgrp(String vdeptgrp) {
		this.vdeptgrp = vdeptgrp;
	}
	public String getVnation() {
		return vnation;
	}
	public void setVnation(String vnation) {
		this.vnation = vnation;
	}
	public String getVhome() {
		return vhome;
	}
	public void setVhome(String vhome) {
		this.vhome = vhome;
	}
	public String getVbanktyp() {
		return vbanktyp;
	}
	public void setVbanktyp(String vbanktyp) {
		this.vbanktyp = vbanktyp;
	}
	public String getVjkarea() {
		return vjkarea;
	}
	public void setVjkarea(String vjkarea) {
		this.vjkarea = vjkarea;
	}
	public String getVtaxno() {
		return vtaxno;
	}
	public void setVtaxno(String vtaxno) {
		this.vtaxno = vtaxno;
	}
	public String getVpacktyp() {
		return vpacktyp;
	}
	public void setVpacktyp(String vpacktyp) {
		this.vpacktyp = vpacktyp;
	}
	public String getVdutyid() {
		return vdutyid;
	}
	public void setVdutyid(String vdutyid) {
		this.vdutyid = vdutyid;
	}
	public String getVcomfrom() {
		return vcomfrom;
	}
	public void setVcomfrom(String vcomfrom) {
		this.vcomfrom = vcomfrom;
	}
	public String getVposition() {
		return vposition;
	}
	public void setVposition(String vposition) {
		this.vposition = vposition;
	}
	public String getVinsurance() {
		return vinsurance;
	}
	public void setVinsurance(String vinsurance) {
		this.vinsurance = vinsurance;
	}
	public String getVstation() {
		return vstation;
	}
	public void setVstation(String vstation) {
		this.vstation = vstation;
	}
	public String getVdutylvl() {
		return vdutylvl;
	}
	public void setVdutylvl(String vdutylvl) {
		this.vdutylvl = vdutylvl;
	}
	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
	public String getVscode() {
		return vscode;
	}
	public void setVscode(String vscode) {
		this.vscode = vscode;
	}
	public String getVscodedes() {
		return vscodedes;
	}
	public void setVscodedes(String vscodedes) {
		this.vscodedes = vscodedes;
	}
	public String getPk_hrdept() {
		return pk_hrdept;
	}
	public void setPk_hrdept(String pk_hrdept) {
		this.pk_hrdept = pk_hrdept;
	}
	public String getVdeptcode() {
		return vdeptcode;
	}
	public void setVdeptcode(String vdeptcode) {
		this.vdeptcode = vdeptcode;
	}
	public String getVdeptdes() {
		return vdeptdes;
	}
	public void setVdeptdes(String vdeptdes) {
		this.vdeptdes = vdeptdes;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getPk_nation() {
		return pk_nation;
	}
	public void setPk_nation(String pk_nation) {
		this.pk_nation = pk_nation;
	}
	public String getPk_schoolage() {
		return pk_schoolage;
	}
	public void setPk_schoolage(String pk_schoolage) {
		this.pk_schoolage = pk_schoolage;
	}
	public String getPk_degree() {
		return pk_degree;
	}
	public void setPk_degree(String pk_degree) {
		this.pk_degree = pk_degree;
	}
	public String getPk_publiclist() {
		return pk_publiclist;
	}
	public void setPk_publiclist(String pk_publiclist) {
		this.pk_publiclist = pk_publiclist;
	}
	public String getPk_house() {
		return pk_house;
	}
	public void setPk_house(String pk_house) {
		this.pk_house = pk_house;
	}
	public String getPk_worktyp() {
		return pk_worktyp;
	}
	public void setPk_worktyp(String pk_worktyp) {
		this.pk_worktyp = pk_worktyp;
	}
	public String getPk_home() {
		return pk_home;
	}
	public void setPk_home(String pk_home) {
		this.pk_home = pk_home;
	}
	public String getPk_banktyp() {
		return pk_banktyp;
	}
	public void setPk_banktyp(String pk_banktyp) {
		this.pk_banktyp = pk_banktyp;
	}
	public String getPk_jkarea() {
		return pk_jkarea;
	}
	public void setPk_jkarea(String pk_jkarea) {
		this.pk_jkarea = pk_jkarea;
	}
	public String getPk_taxno() {
		return pk_taxno;
	}
	public void setPk_taxno(String pk_taxno) {
		this.pk_taxno = pk_taxno;
	}
	public String getPk_packtyp() {
		return pk_packtyp;
	}
	public void setPk_packtyp(String pk_packtyp) {
		this.pk_packtyp = pk_packtyp;
	}
	public String getPk_dutyid() {
		return pk_dutyid;
	}
	public void setPk_dutyid(String pk_dutyid) {
		this.pk_dutyid = pk_dutyid;
	}
	public String getPk_comfrom() {
		return pk_comfrom;
	}
	public void setPk_comfrom(String pk_comfrom) {
		this.pk_comfrom = pk_comfrom;
	}
	public String getPk_bxsta() {
		return pk_bxsta;
	}
	public void setPk_bxsta(String pk_bxsta) {
		this.pk_bxsta = pk_bxsta;
	}
	public String getPk_dutylvl() {
		return pk_dutylvl;
	}
	public void setPk_dutylvl(String pk_dutylvl) {
		this.pk_dutylvl = pk_dutylvl;
	}
	public String getPk_technic() {
		return pk_technic;
	}
	public void setPk_technic(String pk_technic) {
		this.pk_technic = pk_technic;
	}
	public String getPk_station() {
		return pk_station;
	}
	public void setPk_station(String pk_station) {
		this.pk_station = pk_station;
	}
	public String getPk_status() {
		return pk_status;
	}
	public void setPk_status(String pk_status) {
		this.pk_status = pk_status;
	}
	public String getPk_marriage() {
		return pk_marriage;
	}
	public void setPk_marriage(String pk_marriage) {
		this.pk_marriage = pk_marriage;
	}
	public String getPk_sex() {
		return pk_sex;
	}
	public void setPk_sex(String pk_sex) {
		this.pk_sex = pk_sex;
	}
	public String getVtrial() {
		return vtrial;
	}
	public void setVtrial(String vtrial) {
		this.vtrial = vtrial;
	}
	public String getVpositivereason() {
		return vpositivereason;
	}
	public void setVpositivereason(String vpositivereason) {
		this.vpositivereason = vpositivereason;
	}
	public String getPk_positivereason() {
		return pk_positivereason;
	}
	public void setPk_positivereason(String pk_positivereason) {
		this.pk_positivereason = pk_positivereason;
	}
	public String getButtontyp() {
		return buttontyp;
	}
	public void setButtontyp(String buttontyp) {
		this.buttontyp = buttontyp;
	}
	public String getVstatuscode() {
		return vstatuscode;
	}
	public void setVstatuscode(String vstatuscode) {
		this.vstatuscode = vstatuscode;
	}
	public String getPk_team() {
		return pk_team;
	}
	public void setPk_team(String pk_team) {
		this.pk_team = pk_team;
	}
	public String getVstatusname() {
		return vstatusname;
	}
	public void setVstatusname(String vstatusname) {
		this.vstatusname = vstatusname;
	}
	public String getVcalendartyp() {
		return vcalendartyp;
	}
	public void setVcalendartyp(String vcalendartyp) {
		this.vcalendartyp = vcalendartyp;
	}
	public String getPk_group() {
		return pk_group;
	}
	public void setPk_group(String pk_group) {
		this.pk_group = pk_group;
	}
	public String getVcardcode() {
		return vcardcode;
	}
	public void setVcardcode(String vcardcode) {
		this.vcardcode = vcardcode;
	}
	public String getVcardname() {
		return vcardname;
	}
	public void setVcardname(String vcardname) {
		this.vcardname = vcardname;
	}
	public String getVkhr() {
		return vkhr;
	}
	public void setVkhr(String vkhr) {
		this.vkhr = vkhr;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVcnname() {
		return vcnname;
	}
	public void setVcnname(String vcnname) {
		this.vcnname = vcnname;
	}
	public String getBstation() {
		return bstation;
	}
	public void setBstation(String bstation) {
		this.bstation = bstation;
	}
	public String getVstationcode() {
		return vstationcode;
	}
	public void setVstationcode(String vstationcode) {
		this.vstationcode = vstationcode;
	}
	public String getPk_brand() {
		return pk_brand;
	}
	public void setPk_brand(String pk_brand) {
		this.pk_brand = pk_brand;
	}
	public String getPk_area() {
		return pk_area;
	}
	public void setPk_area(String pk_area) {
		this.pk_area = pk_area;
	}
	public String getPk_market() {
		return pk_market;
	}
	public void setPk_market(String pk_market) {
		this.pk_market = pk_market;
	}
	public String getPk_dept() {
		return pk_dept;
	}
	public void setPk_dept(String pk_dept) {
		this.pk_dept = pk_dept;
	}
}