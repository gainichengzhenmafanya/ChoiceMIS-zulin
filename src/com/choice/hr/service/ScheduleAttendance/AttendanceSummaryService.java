package com.choice.hr.service.ScheduleAttendance;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.hr.domain.ScheduleAttendance.AttendanceSummary;
import com.choice.hr.persistence.ScheduleAttendance.AttendanceSummaryMapper;

/**
 * 描述：排班考勤——考勤工时汇总
 * @author 马振
 * 创建时间：2015-6-24 下午1:52:04
 */
@Service
public class AttendanceSummaryService {

	private static Logger log = Logger.getLogger(AttendanceSummaryService.class);
	
	@Autowired
	private AttendanceSummaryMapper attendanceSummaryMapper;

	/**
	 * 描述：考勤工时汇总查询
	 * @author 马振
	 * 创建时间：2015-6-24 下午1:53:46
	 * @param attendanceSummary
	 * @return
	 * @throws CRUDException
	 */
	public List<AttendanceSummary> listAttendanceSummary(AttendanceSummary attendanceSummary) throws CRUDException {
		try{
			return attendanceSummaryMapper.listAttendanceSummary(attendanceSummary);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}