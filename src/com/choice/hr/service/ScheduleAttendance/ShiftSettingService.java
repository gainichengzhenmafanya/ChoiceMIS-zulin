package com.choice.hr.service.ScheduleAttendance;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.hr.domain.ScheduleAttendance.Shift;
import com.choice.hr.persistence.ScheduleAttendance.ShiftSettingMapper;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.service.common.CommonMISBOHService;

/**
 * 描述：排班考勤——班次设置
 * @author 马振
 * 创建时间：2015-5-12 上午9:07:25
 */
@Service
public class ShiftSettingService {
	
	private static Logger log = Logger.getLogger(ShiftSettingService.class);
	
	@Autowired
	private ShiftSettingMapper shiftSettingMapper;
	
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	
	/**
	 * 描述：班次列表
	 * @author 马振
	 * 创建时间：2015-5-26 下午1:41:32
	 * @param shift
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<Shift> listShiftSetting(Shift shift, HttpSession session) throws CRUDException {
		try{
			shift.setPk_store(FormatDouble.StringCodeReplace(commonMISBOHService.getPkStore(session).getPk_store()));
			return shiftSettingMapper.listShiftSetting(shift);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：获取最大编码+1
	 * @author 马振
	 * 创建时间：2015-5-26 下午1:41:12
	 * @param shift
	 * @return
	 * @throws CRUDException
	 */
	public Shift getMaxVcodeAndIsortno(Shift shift) throws CRUDException {
		try{
			return shiftSettingMapper.getMaxVcodeAndIsortno(shift);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：新增班次
	 * @author 马振
	 * 创建时间：2015-5-26 下午1:40:52
	 * @param shift
	 * @param session
	 * @throws CRUDException
	 */
	public void addshift(Shift shift, HttpSession session) throws CRUDException {
		try{
			shift.setPk_shift(CodeHelper.createUUID().substring(0,20).toUpperCase());
			shift.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
			shiftSettingMapper.addShift(shift);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：根据主键查班次
	 * @author 马振
	 * 创建时间：2015-5-26 下午1:40:14
	 * @param shift
	 * @return
	 * @throws CRUDException
	 */
	public Shift findShiftByPk(Shift shift) throws CRUDException {
		try{
			shift.setPk_shift(FormatDouble.StringCodeReplace(shift.getPk_shift()));
			return shiftSettingMapper.findShiftByPk(shift);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：修改班次
	 * @author 马振
	 * 创建时间：2015-5-26 下午1:39:52
	 * @param shift
	 * @throws CRUDException
	 */
	public void updateshift(Shift shift) throws CRUDException {
		try{
			shiftSettingMapper.updateShift(shift);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：删除班次
	 * @author 马振
	 * 创建时间：2015-5-26 下午1:39:30
	 * @param shift
	 * @throws CRUDException
	 */
	public void deleteshift(Shift shift) throws CRUDException {
		try{
			String[] pk_shifts = shift.getPk_shift().split(",");
			
			for (int i = 0; i < pk_shifts.length; i++) {
				shift.setPk_shift(pk_shifts[i]);
				shiftSettingMapper.deleteShift(shift);
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：检验编码是否已存在
	 * @author 马振
	 * 创建时间：2015-5-26 下午1:39:11
	 * @param shift
	 * @return
	 * @throws CRUDException
	 */
	public String checkVcode(Shift shift) throws CRUDException {
		try{
			return shiftSettingMapper.checkVcode(shift);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}