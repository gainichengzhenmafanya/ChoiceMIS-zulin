package com.choice.hr.service.ScheduleAttendance;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.util.CodeHelper;
import com.choice.hr.domain.ScheduleAttendance.EmpTime;
import com.choice.hr.domain.ScheduleAttendance.WorkAttendance;
import com.choice.hr.persistence.ScheduleAttendance.AttendanceRecordMapper;
import com.choice.hr.persistence.ScheduleAttendance.EmpTimeMapper;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.commonutil.DateJudge;

@Service
public class EmpTimeService {
	@Autowired
	private EmpTimeMapper empTimeMapper;

	@Autowired
	private AttendanceRecordMapper attendanceRecordMapper;
	
	/**
	 * 员工工时计算 --- 考勤上传实时计算
	 * @author 文清泉
	 * @param 2015年6月23日 下午1:31:54
	 * @throws ParseException 
	 */
	public void calEmpTime(String scode){
		List<EmpTime> list = empTimeMapper.findEmpTime(scode);
		//循环所有打卡记录
		Date btime = null;
		Date etime = null;
		String jsbtime ="";
		String jsetime ="";
		// 超出时间范围
		long timeout = 31*60*1000;
		Map<String,List<String>> timeMap = new HashMap<String,List<String>>();
		List<String> belist = new ArrayList<String>();
		String oldEmp = null;
		for (EmpTime empTime : list) {
			if(oldEmp == null || oldEmp.equals("")){
				oldEmp = empTime.getDwEnrollNumber()+"###"+empTime.getClickTime().substring(0,10)+"###"+empTime.getScode();
			}
			//记录考勤时间
			Date clickTime = null;
			try {
				clickTime = DateJudge.YYYY_MM_DD_HH_mm_ss.parse(empTime.getClickTime());
			} catch (ParseException e) {
				continue;
			}
			// 判断是否上班打卡
			if(DDT.CHECKIN.equals(empTime.getDwInOutMode()+"")){
				//验证是否第一次加载
				if(btime==null || btime.equals("") || DateJudge.YYYY_MM_DD.format(btime).equals(DateJudge.YYYY_MM_DD.format(clickTime))){
					btime = clickTime;
					jsbtime = DateJudge.YYYY_MM_DD_HH_mm_ss.format(btime);
					//时间差是否超过设定时间
				}else if((clickTime.getTime() - btime.getTime()) < timeout){
					//判断时间是否大于 查找到本次考勤最小时间
					if(btime.getTime() > clickTime.getTime()){
						btime = clickTime;
					}
					jsbtime = DateJudge.YYYY_MM_DD_HH_mm_ss.format(btime);
				}else{
					jsbtime = DateJudge.YYYY_MM_DD_HH_mm_ss.format(btime);
					belist.add("b"+jsbtime);
					btime = clickTime;
					jsbtime ="";
				}
			}else if(DDT.CHECKOUT.equals(empTime.getDwInOutMode()+"")){
				//验证是否第一次加载
				if(etime==null || etime.equals("") || DateJudge.YYYY_MM_DD.format(btime).equals(DateJudge.YYYY_MM_DD.format(clickTime))){
					etime = clickTime;
					jsetime = DateJudge.YYYY_MM_DD_HH_mm_ss.format(etime);
					//时间差是否超过设定时间
				}else if((clickTime.getTime() - etime.getTime()) < timeout){
					//判断时间是否大于 查找到本次考勤最大时间
					if(etime.getTime() < clickTime.getTime()){
						etime = clickTime;
					}
					jsetime = DateJudge.YYYY_MM_DD_HH_mm_ss.format(etime);
				}else{
					belist.add("e"+jsetime);
					etime = clickTime;
					jsetime ="";
				}
			}
			// 判断填充LIST集合中
			if(!oldEmp.equals(empTime.getDwEnrollNumber()+"-"+empTime.getClickTime().substring(0,10)+"-"+empTime.getScode())){
				if(jsbtime != null && !jsbtime.equals("")){
					belist.add("b"+jsbtime);
					jsbtime ="";
				}
				if(jsetime != null && !jsetime.equals("")){
					belist.add("e"+jsetime);
					jsetime ="";
				}
				timeMap.put(oldEmp,belist);
				oldEmp = empTime.getDwEnrollNumber()+"###"+empTime.getClickTime().substring(0,10)+"###"+empTime.getScode();
				belist = new ArrayList<String>();
			}
		}
		
		//计算每人每天工时
		if(timeMap!=null){
			for (String key : timeMap.keySet()) {
				belist = timeMap.get(key);
				WorkAttendance wa = new WorkAttendance();
				String[] split = key.split("###");
				wa.setVSCODE(split[2]);
				wa.setDAT(split[1]);
				wa.setVKQCARDNO(split[0]);
				long tx = 0;
				for (int i =0;i<belist.size();i++) {
					String bdat = belist.get(i);
					if(bdat.indexOf("b")!=-1){
						continue;
					}
					String edat = null;
					if((i+1) < belist.size()){
						i++;
						edat = belist.get(i);
					}
					
					if(edat !=null && edat.indexOf("e")==-1){
						i --;
						edat ="";
					}
					if(edat==null || edat.equals("")){
						wa.setSTA(1);
					}else{
						wa.setSTA(0);
						try {
							btime = DateJudge.YYYY_MM_DD_HH_mm_ss.parse(bdat);
							etime = DateJudge.YYYY_MM_DD_HH_mm_ss.parse(edat);
							tx += etime.getTime() - btime.getTime();
						} catch (ParseException e) {
						}
					}
				}
				double hourtx = (tx / 60 );
				double mintx = (tx % 60 )/60.000;
				double txgs=  hourtx+mintx;
				wa.setATTENDANCE(txgs);
				wa.setPK_WA(CodeHelper.createUUID().substring(0, 20).toUpperCase());
				wa.setPK_EMPLOYEE(empTimeMapper.findEmployeeBy(split[2],split[0]));
				attendanceRecordMapper.delWorkAttendance(wa);
				attendanceRecordMapper.saveWorkAttendance(wa);
				empTimeMapper.updateWorkAttendance(split[2],split[1]);
				empTimeMapper.updateEmpTime(split[2],split[1],1);
			}
		}
	}
	public void updateEmpTime(String code, String dat, int sta) {
		empTimeMapper.updateEmpTime(code,dat,sta);
	}
}
