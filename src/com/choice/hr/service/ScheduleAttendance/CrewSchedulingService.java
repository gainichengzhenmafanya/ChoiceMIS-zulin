package com.choice.hr.service.ScheduleAttendance;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.hr.domain.ScheduleAttendance.CrewScheduling;
import com.choice.hr.persistence.ScheduleAttendance.CrewSchedulingMapper;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.service.common.CommonMISBOHService;

/**
 * 描述：排班考勤——班组排班
 * @author 马振
 * 创建时间：2015-5-26 下午4:27:39
 */
@Service
public class CrewSchedulingService {
	
	private static Logger log = Logger.getLogger(CrewSchedulingService.class);
	
	@Autowired
	private CrewSchedulingMapper crewSchedulingMapper;
	
	@Autowired
	private CommonMISBOHService commonMISBOHService; 
	
	/**
	 * 描述：班组排班左侧树
	 * @author 马振
	 * 创建时间：2015-5-26 下午4:29:17
	 * @param crewScheduling
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> treeCrewScheduling(CrewScheduling crewScheduling, HttpSession session) throws CRUDException {
		try{
			crewScheduling.setPk_store(FormatDouble.StringCodeReplace(commonMISBOHService.getPkStore(session).getPk_store()));
			
			return crewSchedulingMapper.treeCrewScheduling(crewScheduling);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：班组排班列表
	 * @author 马振
	 * 创建时间：2015-5-26 下午4:30:03
	 * @param crewScheduling
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<CrewScheduling> listCrewScheduling(CrewScheduling crewScheduling, HttpSession session) throws CRUDException {
		try{
			//为门店主键加''
			crewScheduling.setPk_store(FormatDouble.StringCodeReplace(commonMISBOHService.getPkStore(session).getPk_store()));
			crewScheduling.setPk_hrdept(FormatDouble.StringCodeReplace(crewScheduling.getPk_hrdept()));
			crewScheduling.setPk_team(FormatDouble.StringCodeReplace(crewScheduling.getPk_team()));
			
			//获取一段时间内一班组下的数据
			List<CrewScheduling> listCrewScheduling = crewSchedulingMapper.listCrewScheduling(crewScheduling);
			
			//创建新的list
			List<CrewScheduling> list = new ArrayList<CrewScheduling>();
			
			if (crewScheduling.getVbdat() != null && crewScheduling.getVedat() != null) {
				
				//获取这段时间内所有日期
				List<String> listDate = DateJudge.getDateList(crewScheduling.getVbdat(),crewScheduling.getVedat());
					
				for (int i = 0; i < listDate.size(); i++) {
					CrewScheduling crew = new CrewScheduling();
					crew.setDworkdate(listDate.get(i));
					crew.setPk_team(crewScheduling.getPk_team());
					crew.setPk_hrdept(crewScheduling.getPk_hrdept());
					crew.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
						
					//循环获得的数据，当日期与获得数据中的日期相同时，将此日期的班次放到crew中
					for (int k = 0; k < listCrewScheduling.size(); k++) {
						if (listDate.get(i).equals(listCrewScheduling.get(k).getDworkdate())) {
							crew.setPk_shift(listCrewScheduling.get(k).getPk_shift());
							crew.setVcode(listCrewScheduling.get(k).getVcode());
							crew.setVname(listCrewScheduling.get(k).getVname());
						}
					}
						
					//将每一个日期及班组、部门、门店主键放入新的list中
					list.add(crew);
				}
			} else {
				list = listCrewScheduling;
			}
			
			return list;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：保存班组排班
	 * @author 马振
	 * 创建时间：2015-5-27 上午9:58:39
	 * @param crewScheduling
	 * @param session
	 * @throws CRUDException
	 */
	public void saveData(CrewScheduling crewScheduling, HttpSession session) throws CRUDException {
		try{
			crewScheduling.setPk_store(FormatDouble.StringCodeReplace(commonMISBOHService.getPkStore(session).getPk_store()));
			crewScheduling.setPk_team(FormatDouble.removeQuotes(crewScheduling.getPk_team()));
			
			for (int i = 0; i < crewScheduling.getListCrew().size(); i++) {
				CrewScheduling crew = crewScheduling.getListCrew().get(i);
				crew.setPk_crewscheduling(CodeHelper.createUUID().substring(0, 20).toUpperCase());
				crew.setPk_store(FormatDouble.removeQuotes(crew.getPk_store()));
				crew.setPk_team(FormatDouble.removeQuotes(crew.getPk_team()));
				crew.setPk_hrdept(FormatDouble.removeQuotes(crew.getPk_hrdept()));

				//保存数据前先删除同一班组、这一天已存在的数据
				crewScheduling.setDworkdate(crew.getDworkdate());
				crewSchedulingMapper.deleteCrew(crewScheduling);
				
				//判断班组是否分配班次，只有分配了的才保存
				if (ValueUtil.IsNotEmpty(crewScheduling.getListCrew().get(i).getPk_shift())) {
					crewSchedulingMapper.saveData(crewScheduling.getListCrew().get(i));
				}
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：删除班组排班
	 * @author 马振
	 * 创建时间：2015-6-10 下午3:40:30
	 * @param crewScheduling
	 * @param session
	 * @throws CRUDException
	 */
	public void deleteCrew(CrewScheduling crewScheduling, HttpSession session) throws CRUDException {
		try{
			String dworkdates[] = crewScheduling.getDworkdate().split(",");
			for (int i = 0; i < dworkdates.length; i++) {
				crewScheduling.setDworkdate(dworkdates[i]);
				crewSchedulingMapper.deleteCrew(crewScheduling);
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}