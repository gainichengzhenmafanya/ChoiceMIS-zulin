package com.choice.hr.service.ScheduleAttendance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.hr.domain.DataDictionary.DataTyp;
import com.choice.hr.domain.ScheduleAttendance.ClassTableSetting;
import com.choice.hr.persistence.ScheduleAttendance.ClassTableSettingMapper;
import com.choice.hr.service.PersonnelManagement.StaffListService;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.domain.reportMis.ReportObject;

/**
 * 描述：排班考勤——班表设定
 * @author 马振
 * 创建时间：2015-6-16 上午9:14:12
 */
@Service
public class ClassTableSettingService {

	private static Logger log = Logger.getLogger(ClassTableSettingService.class);
	
	@Autowired
	private ClassTableSettingMapper classTableSettingMapper;
	
	private List<String> listTime = null;
	
	private List<String> listInt = null;

	@Autowired
	private StaffListService staffListService;
	
	@Autowired
	private ReportObject<Map<String,Object>> reportObject;

	/**
	 * 描述：查询班表设定
	 * @author 马振
	 * 创建时间：2015-6-16 上午9:20:52
	 * @return
	 * @throws CRUDException
	 */
	public List<ClassTableSetting> listClassTableSetting(ClassTableSetting classTableSetting) throws CRUDException {
		try{
			List<ClassTableSetting> listClassTableSetting = classTableSettingMapper.selectTableSetting(classTableSetting);
			for (int i = 0; i < listClassTableSetting.size(); i++) {
				ClassTableSetting classTable = listClassTableSetting.get(i);
				classTable.setVendtime((DateJudge.hourBetween(classTable.getVbegintime(),classTable.getVendtime()) * 2) + "");
			}
			return listClassTableSetting;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：获取表头时间
	 * @author 马振
	 * 创建时间：2015-6-17 上午9:40:07
	 * @param classTableSetting
	 * @return
	 * @throws CRUDException
	 */
	public ClassTableSetting listTrTime(ClassTableSetting classTableSetting) throws CRUDException {
		try{
			listTime = new ArrayList<String>();
			listInt = new ArrayList<String>();
			
			//存储1~24的数字
			for (int i = 0; i < 24; i++) {
				listTime.add(FormatDouble.formatStringLength("0",i + "",2));
			}
			
			//存储1~48的数字
			for (int i = 0; i < 48; i++) {
				if (i % 2 == 0) {
					listInt.add(FormatDouble.formatStringLength("0",(i / 2) + "",2) + ":00");
				} else {
					listInt.add(FormatDouble.formatStringLength("0",((i - 1) / 2) + "",2) + ":30");
				}
			}
			
			classTableSetting.setListTime(listTime);
			classTableSetting.setListInt(listInt);
			
			return classTableSetting;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：保存数据
	 * @author 马振
	 * 创建时间：2015-6-16 下午5:21:18
	 * @param classTableSetting
	 * @throws CRUDException
	 */
	public void saveData(ClassTableSetting classTableSetting) throws CRUDException {
		try{
			//根据日期和门店删除数据
			classTableSettingMapper.deleteData(classTableSetting);
			
			//添加主键
			for (int i = 0; i < classTableSetting.getListClassTableSetting().size(); i++) {
				classTableSetting.getListClassTableSetting().get(i).setPk_tablesetting(CodeHelper.createUUID().substring(0,20).toUpperCase().toString());
			}
			
			classTableSettingMapper.saveData(classTableSetting.getListClassTableSetting());
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：保存复制的数据
	 * @author 马振
	 * 创建时间：2015-6-23 上午9:15:55
	 * @param classTableSetting
	 * @throws CRUDException
	 */
	public void copyData(ClassTableSetting classTableSetting) throws CRUDException {
		try{
			List<ClassTableSetting> listClassTableSetting = classTableSettingMapper.selectTableSetting(classTableSetting);
			
			//添加主键
			for (int i = 0; i < listClassTableSetting.size(); i++) {
				listClassTableSetting.get(i).setPk_tablesetting(CodeHelper.createUUID().substring(0,20).toUpperCase().toString());
				listClassTableSetting.get(i).setPk_store(classTableSetting.getPk_store());
				listClassTableSetting.get(i).setDworkdate(classTableSetting.getDworkdate1());
			}
			
			classTableSettingMapper.saveData(listClassTableSetting);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：删除数据
	 * @author 马振
	 * 创建时间：2015-6-18 下午5:17:47
	 * @param classTableSetting
	 * @throws CRUDException
	 */
	public void deleteData(ClassTableSetting classTableSetting) throws CRUDException {
		try{
			//根据日期、门店、工作站、开始时间删除数据
			classTableSettingMapper.deleteData(classTableSetting);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：将查处的数据放入map中
	 * @author 马振
	 * 创建时间：2015-6-19 下午1:03:08
	 * @param classTableSetting
	 * @return
	 * @throws CRUDException
	 */
	public Object getListData(ClassTableSetting classTableSetting) throws CRUDException {
		try{
			List<ClassTableSetting> listClassTableSetting = classTableSettingMapper.selectTableSetting(classTableSetting);
			List<Map<String,Object>> listResult = new ArrayList<Map<String,Object>>();
			DataTyp dataTyp = new DataTyp();
			
			//工作站
			dataTyp.setVtyp(DDT.DATATYPE_TYP25);
			dataTyp.setPk_store(classTableSetting.getPk_store());
			
			List<DataTyp> listDataTyp = staffListService.listDataTyp(dataTyp);
			
			//循环工作站
			for (int i = 0; i < listDataTyp.size(); i++) {
				Map<String,Object> map = new HashMap<String, Object>();
				map.put("workstation", listDataTyp.get(i).getVname());
				
				//循环班表设定中的数据
				for (int k = 0; k < listClassTableSetting.size(); k++) {
					ClassTableSetting classTable = listClassTableSetting.get(k);
					
					//当两个工作站相等时，放入map中
					if (listDataTyp.get(i).getPk_datatyp().equals(classTable.getWorkstation())) {
						map.put("vbegintime" + classTable.getVbegintime().replace(":", ""), classTable.getVbegintime());
						map.put("vendtime" + classTable.getVbegintime().replace(":", ""), FormatDouble.formatDoubleLength(DateJudge.hourBetween(classTable.getVbegintime(),classTable.getVendtime()) * 2 + "",0));
						map.put("totalhour", classTable.getTotalhour());
						map.put("npeople" + classTable.getVbegintime().replace(":", ""), classTable.getNpeople());
						map.put("dworkdate", classTable.getDworkdate());
					}
				}
				listResult.add(map);
			}
			
			reportObject.setRows(listResult);
			return reportObject;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}