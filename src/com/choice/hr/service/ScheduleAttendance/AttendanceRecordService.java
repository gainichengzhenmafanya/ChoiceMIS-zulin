package com.choice.hr.service.ScheduleAttendance;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.hr.domain.ScheduleAttendance.AttendanceRecord;
import com.choice.hr.domain.ScheduleAttendance.WorkAttendance;
import com.choice.hr.persistence.ScheduleAttendance.AttendanceRecordMapper;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.service.common.CommonMISBOHService;

/**
 * 描述：考勤记录
 * @author 马振
 * 创建时间：2015-6-2 上午9:30:56
 */
@Service
public class AttendanceRecordService {
	
	private static Logger log = Logger.getLogger(AttendanceRecordService.class);
	
	@Autowired
	private AttendanceRecordMapper attendanceRecordMapper;
	
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	
	/**
	 * 描述：左侧树
	 * @author 马振
	 * 创建时间：2015-6-2 上午9:48:00
	 * @param attendanceRecord
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> treeAttendanceRecord(AttendanceRecord attendanceRecord, HttpSession session) throws CRUDException {
		try{
			attendanceRecord.setPk_store(FormatDouble.StringCodeReplace(commonMISBOHService.getPkStore(session).getPk_store()));
			return attendanceRecordMapper.treeAttendanceRecord(attendanceRecord);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询考勤记录
	 * @author 马振
	 * 创建时间：2015-6-2 上午10:10:08
	 * @param attendanceRecord
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<AttendanceRecord> listAttendanceRecord(AttendanceRecord attendanceRecord, HttpSession session) throws CRUDException {
		try{
			attendanceRecord.setPk_store(FormatDouble.StringCodeReplace(commonMISBOHService.getPkStore(session).getPk_store()));
			return attendanceRecordMapper.listAttendanceRecord(attendanceRecord);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：考勤汇总
	 * @author 马振
	 * 创建时间：2015-6-2 下午1:50:23
	 * @param attendanceRecord
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<AttendanceRecord> listCount(AttendanceRecord attendanceRecord, HttpSession session) throws CRUDException {
		try{
			attendanceRecord.setPk_store(FormatDouble.StringCodeReplace(commonMISBOHService.getPkStore(session).getPk_store()));
			return attendanceRecordMapper.listCount(attendanceRecord);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 员工考勤工时
	 * @author 文清泉
	 * @param 2015年6月19日 上午10:55:02
	 * @param workAttendance 
	 * @return
	 */
	public List<WorkAttendance> listWorkAttendance(
			WorkAttendance workAttendance) {
		if(workAttendance.getVNAME()!=null && !workAttendance.getVNAME().equals("")){
			workAttendance.setVNAME("%"+workAttendance.getVNAME()+"%");
		}
		if(workAttendance.getVKQCARDNO()!=null && !workAttendance.getVKQCARDNO().equals("")){
			workAttendance.setVKQCARDNO("%"+workAttendance.getVKQCARDNO()+"%");
		}
		return attendanceRecordMapper.listWorkAttendance(workAttendance);
	}

	public int saveWorkAttendance(WorkAttendance workAttendance)  throws CRUDException {
		try{
			for (WorkAttendance wa : workAttendance.getWorkAttendanceList()) {
				wa.setPK_STORE(workAttendance.getPK_STORE());
				wa.setVSCODE(workAttendance.getVSCODE());
				wa.setDAT(workAttendance.getDAT());
				if(wa.getPK_WA() == null || wa.getPK_WA().equals("")){
					wa.setPK_WA(CodeHelper.createUUID().substring(0, 20).toUpperCase());
				}
				attendanceRecordMapper.delWorkAttendance(wa);
				attendanceRecordMapper.saveWorkAttendance(wa);
			}
			return  1;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：根据门店和日期统计一天的实际工时和理论工时
	 * @author 马振
	 * 创建时间：2015-6-25 上午9:14:14
	 * @param workAttendance
	 * @return
	 * @throws CRUDException
	 */
	public WorkAttendance getWorkHour(WorkAttendance workAttendance) throws CRUDException {
		try{
			return attendanceRecordMapper.getWorkHour(workAttendance);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}