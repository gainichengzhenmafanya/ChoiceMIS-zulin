package com.choice.hr.service.ScheduleAttendance;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.hr.domain.ScheduleAttendance.EmployeeScheduling;
import com.choice.hr.persistence.ScheduleAttendance.EmployeeSchedulingMapper;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.service.common.CommonMISBOHService;

/**
 * 描述：排班考勤——员工排班
 * @author 马振
 * 创建时间：2015-5-28 上午8:55:08
 */
@Service
public class EmployeeSchedulingService {
	
	private static Logger log = Logger.getLogger(EmployeeSchedulingService.class);
	
	@Autowired
	private EmployeeSchedulingMapper employeeSchedulingMapper;
	
	@Autowired
	private CommonMISBOHService commonMISBOHService; 
	
	/**
	 * 描述：员工排班左侧树
	 * @author 马振
	 * 创建时间：2015-5-28 上午8:56:51
	 * @param EmployeeScheduling
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> treeEmployeeScheduling(EmployeeScheduling EmployeeScheduling, HttpSession session) throws CRUDException {
		try{
			EmployeeScheduling.setPk_store(FormatDouble.StringCodeReplace(commonMISBOHService.getPkStore(session).getPk_store()));
			
			return employeeSchedulingMapper.treeEmployeeScheduling(EmployeeScheduling);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：员工排班列表
	 * @author 马振
	 * 创建时间：2015-5-28 上午8:57:12
	 * @param EmployeeScheduling
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<EmployeeScheduling> listEmployeeScheduling(EmployeeScheduling EmployeeScheduling, HttpSession session) throws CRUDException {
		try{
			//为各主键加''
			EmployeeScheduling.setPk_store(FormatDouble.StringCodeReplace(commonMISBOHService.getPkStore(session).getPk_store()));
			EmployeeScheduling.setPk_hrdept(FormatDouble.StringCodeReplace(EmployeeScheduling.getPk_hrdept()));
			EmployeeScheduling.setPk_team(FormatDouble.StringCodeReplace(EmployeeScheduling.getPk_team()));
			EmployeeScheduling.setPk_employee(FormatDouble.StringCodeReplace(EmployeeScheduling.getPk_employee()));
			
			//获取一段时间内一班组下的数据
			List<EmployeeScheduling> listEmployeeScheduling = employeeSchedulingMapper.listEmployeeScheduling(EmployeeScheduling);
			
			//创建新的list
			List<EmployeeScheduling> list = new ArrayList<EmployeeScheduling>();
			
			if (EmployeeScheduling.getVbdat() != null && EmployeeScheduling.getVedat() != null) {
				
				//获取这段时间内所有日期
				List<String> listDate = DateJudge.getDateList(EmployeeScheduling.getVbdat(),EmployeeScheduling.getVedat());
					
				for (int i = 0; i < listDate.size(); i++) {
					EmployeeScheduling Employee = new EmployeeScheduling();
					Employee.setDworkdate(listDate.get(i));
					Employee.setPk_team(EmployeeScheduling.getPk_team());
					Employee.setPk_hrdept(EmployeeScheduling.getPk_hrdept());
					Employee.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
						
					//循环获得的数据，当日期与获得数据中的日期相同时，将此日期的班次放到Employee中
					for (int k = 0; k < listEmployeeScheduling.size(); k++) {
						if (listDate.get(i).equals(listEmployeeScheduling.get(k).getDworkdate())) {
							Employee.setPk_shift(listEmployeeScheduling.get(k).getPk_shift());
							Employee.setVcode(listEmployeeScheduling.get(k).getVcode());
							Employee.setVname(listEmployeeScheduling.get(k).getVname());
						}
					}
						
					//将每一个日期及班组、部门、门店主键放入新的list中
					list.add(Employee);
				}
			} else {
				list = listEmployeeScheduling;
			}
			
			return list;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：保存员工排班
	 * @author 马振
	 * 创建时间：2015-5-28 上午8:57:30
	 * @param EmployeeScheduling
	 * @param session
	 * @throws CRUDException
	 */
	public void saveData(EmployeeScheduling employeeScheduling, HttpSession session) throws CRUDException {
		try{
			for (int i = 0; i < employeeScheduling.getListEmployeeScheduling().size(); i++) {
				EmployeeScheduling employee = employeeScheduling.getListEmployeeScheduling().get(i);
				
				employee.setPk_employeescheduling(CodeHelper.createUUID().substring(0, 20).toUpperCase());
				employee.setPk_store(FormatDouble.removeQuotes(employee.getPk_store()));
				employee.setPk_hrdept(employeeSchedulingMapper.findDeptByTeam(employee).getPk_hrdept());
				employee.setPk_team(FormatDouble.removeQuotes(employee.getPk_team()));
				employee.setPk_employee(FormatDouble.removeQuotes(employee.getPk_employee()));
				employee.setDworkdate(employee.getDworkdate());

				//保存数据前先删除同一班组、这一天已存在的数据
				employeeSchedulingMapper.deleteEmployee(employee);
				
				//判断班组是否分配班次，只有分配了的才保存
				if (ValueUtil.IsNotEmpty(employee.getPk_shift())) {
					employeeSchedulingMapper.saveData(employee);
				}
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：删除员工排班
	 * @author 马振
	 * 创建时间：2015-6-10 下午3:24:03
	 * @param employeeScheduling
	 * @param session
	 * @throws CRUDException
	 */
	public void deleteEmployee(EmployeeScheduling employeeScheduling, HttpSession session) throws CRUDException {
		try{
			String dworkdates[] = employeeScheduling.getDworkdate().split(",");
			for (int i = 0; i < dworkdates.length; i++) {
				employeeScheduling.setDworkdate(dworkdates[i]);
				employeeSchedulingMapper.deleteEmployee(employeeScheduling);
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}