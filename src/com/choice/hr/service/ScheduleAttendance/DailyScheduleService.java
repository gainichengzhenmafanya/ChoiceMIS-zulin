package com.choice.hr.service.ScheduleAttendance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.hr.domain.ScheduleAttendance.CrewScheduling;
import com.choice.hr.domain.ScheduleAttendance.Shift;
import com.choice.hr.persistence.ScheduleAttendance.DailyScheduleMapper;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.domain.reportMis.ReportObject;

/**
 * 描述：排班考勤——每日排班
 * @author 马振
 * 创建时间：2015-6-11 下午4:39:50
 */
@Service
public class DailyScheduleService {
	
	private static Logger log = Logger.getLogger(DailyScheduleService.class);
	
	@Autowired
	private ReportObject<Map<String,Object>> reportObject;
	
	@Autowired
	private DailyScheduleMapper dailyScheduleMapper;

	/**
	 * 描述：根据部门查询班组
	 * @author 马振
	 * 创建时间：2015-6-11 下午4:43:07
	 * @param shift
	 * @return
	 * @throws CRUDException
	 */
	public List<CrewScheduling> getTeamByDept(Shift shift) throws CRUDException {
		try{
			CrewScheduling crewScheduling = new CrewScheduling();
			crewScheduling.setDworkdate(shift.getDworkdate());
			crewScheduling.setPk_store(shift.getPk_store());
			List<CrewScheduling> list = dailyScheduleMapper.getDept(crewScheduling);
			
			//循环出每一个部门主键，根据部门主键查询班组
			for (int i = 0; i < list.size(); i++) {
				List<Shift> listShift = new ArrayList<Shift>();
				shift.setPk_hrdept(list.get(i).getPk_hrdept());
				
				if (0 != dailyScheduleMapper.getTeamByDept(shift).size()) {
					
					//循环根据部门主键查询出的班组
					for (int k = 0; k < dailyScheduleMapper.getTeamByDept(shift).size(); k++) {
						
						//判断班组中的开始时间是否为空，只有不为空时才放入listShift中
						if (ValueUtil.IsNotEmpty(dailyScheduleMapper.getTeamByDept(shift).get(k).getVbegintime1())) {
							listShift.add(dailyScheduleMapper.getTeamByDept(shift).get(k));
						}
					}
				}
				
				//将listShift及其长度放入list中
				list.get(i).setListShift(listShift);
				list.get(i).setSize(listShift.size());
			}
			
			return list;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询员工班次
	 * @author 马振
	 * 创建时间：2015-6-11 下午5:14:45
	 * @param crewScheduling
	 * @return
	 * @throws CRUDException
	 */
	public List<CrewScheduling> getEmpShift(CrewScheduling crewScheduling) throws CRUDException {
		try{
			return dailyScheduleMapper.getEmpShift(crewScheduling);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：表格导出查询的数据
	 * @author 马振
	 * 创建时间：2015-6-12 下午4:04:28
	 * @param crewScheduling
	 * @return
	 * @throws CRUDException
	 */
	public Object getEmpMap(Shift shift) throws CRUDException {
		try{
			String employee = "";	//员工名称
			String empno = "";		//员工编号
			String station = "";	//岗位
			String kqcardno = "";	//考勤号
			String pk_hrdept = "";	//部门
			String pk_team = "";	//班组
			String pk_shift = "";	//班次
			List<CrewScheduling> getTeamByDept = this.getTeamByDept(shift);
			List<Map<String,Object>> listResult = new ArrayList<Map<String,Object>>();
			
			CrewScheduling crewScheduling = new CrewScheduling();
			crewScheduling.setDworkdate(shift.getDworkdate());
			crewScheduling.setPk_store(shift.getPk_store());
			List<CrewScheduling> getEmpShift = dailyScheduleMapper.getEmpShift(crewScheduling);
			
			for (int i = 0; i < getTeamByDept.size(); i++) {
				for (int k = 0; k < getTeamByDept.get(i).getListShift().size(); k++) {
					Map<String,Object> map = new HashMap<String, Object>();
					map.put("vname", getTeamByDept.get(i).getVname());
					map.put("vshiftname", getTeamByDept.get(i).getListShift().get(k).getVname());
					map.put("dworkdate", getTeamByDept.get(i).getListShift().get(k).getDworkdate());
					map.put("size", getTeamByDept.get(i).getListShift().size());
					map.put("pkHrdept", getTeamByDept.get(i).getPk_hrdept());
					map.put("pkTeam", getTeamByDept.get(i).getListShift().get(k).getPk_team());
					map.put("pkShift", getTeamByDept.get(i).getListShift().get(k).getPk_shift());
					
					for (int j = 0; j < getEmpShift.size(); j++) {
						employee = employee + getEmpShift.get(j).getVname() + ",";
						empno = empno + getEmpShift.get(j).getVempno() + ",";
						station = station + getEmpShift.get(j).getVstation() + ",";
						kqcardno = kqcardno + getEmpShift.get(j).getVkqcardno() + ",";
						pk_hrdept = pk_hrdept + getEmpShift.get(j).getPk_hrdept() + ",";
						pk_team = pk_team + getEmpShift.get(j).getPk_team() + ",";
						pk_shift = pk_shift + getEmpShift.get(j).getPk_shift() + ",";
					}
					
					map.put("employee", employee);
					map.put("empno", empno);
					map.put("station", station);
					map.put("kqcardno", kqcardno);
					map.put("pk_hrdept", pk_hrdept);
					map.put("pk_team", pk_team);
					map.put("pk_shift", pk_shift);
					listResult.add(map);
				}
			}
			
			reportObject.setRows(listResult);
			return reportObject;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}