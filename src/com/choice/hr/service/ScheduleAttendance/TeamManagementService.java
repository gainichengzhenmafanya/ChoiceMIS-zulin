package com.choice.hr.service.ScheduleAttendance;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.hr.domain.PersonnelManagement.Employee;
import com.choice.hr.domain.ScheduleAttendance.Team;
import com.choice.hr.persistence.ScheduleAttendance.TeamManagementMapper;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.service.common.CommonMISBOHService;

/**
 * 描述：排班考勤——班组管理
 * @author 马振
 * 创建时间：2015-5-12 上午9:07:25
 */
@Service
public class TeamManagementService {
	
	private static Logger log = Logger.getLogger(TeamManagementService.class);
	
	@Autowired
	private TeamManagementMapper teamManagementMapper;
	
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	
	/**
	 * 描述：左侧树
	 * @author 马振
	 * 创建时间：2015-5-25 下午1:53:23
	 * @param hrDept
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> treeTeamManagement(Team team, HttpSession session) throws CRUDException {
		try{
			team.setPk_store(FormatDouble.StringCodeReplace(commonMISBOHService.getPkStore(session).getPk_store()));
			return teamManagementMapper.treeTeamManagement(team);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：班组列表
	 * @author 马振
	 * 创建时间：2015-5-25 下午2:19:14
	 * @param team
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<Team> listTeamManagement(Team team, HttpSession session) throws CRUDException {
		try{
			team.setPk_store(FormatDouble.StringCodeReplace(commonMISBOHService.getPkStore(session).getPk_store()));
			team.setPk_hrdept(FormatDouble.StringCodeReplace(team.getPk_hrdept()));
			return teamManagementMapper.listTeamManagement(team);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：获取最大编码和排序
	 * @author 马振
	 * 创建时间：2015-5-25 下午4:29:24
	 * @param team
	 * @return
	 * @throws CRUDException
	 */
	public Team getMaxVcodeAndIsortno(Team team) throws CRUDException {
		try{
			return teamManagementMapper.getMaxVcodeAndIsortno(team);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：新增班组
	 * @author 马振
	 * 创建时间：2015-5-25 下午4:29:36
	 * @param team
	 * @throws CRUDException
	 */
	public void addTeam(Team team, HttpSession session) throws CRUDException {
		try{
			team.setPk_team(CodeHelper.createUUID().substring(0,20).toUpperCase());
			team.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
			if (team.getPk_hrdept().length() > 20) {
				team.setPk_hrdept(team.getPk_hrdept().substring(1,21));
			}
			teamManagementMapper.addTeam(team);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：根据主键查班组
	 * @author 马振
	 * 创建时间：2015-5-25 下午4:29:48
	 * @param team
	 * @return
	 * @throws CRUDException
	 */
	public Team findTeamByPk(Team team) throws CRUDException {
		try{
			team.setPk_team(FormatDouble.StringCodeReplace(team.getPk_team()));
			return teamManagementMapper.findTeamByPk(team);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：修改班组
	 * @author 马振
	 * 创建时间：2015-5-25 下午4:30:01
	 * @param team
	 * @throws CRUDException
	 */
	public void updateTeam(Team team) throws CRUDException {
		try{
			teamManagementMapper.updateTeam(team);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：删除班组
	 * @author 马振
	 * 创建时间：2015-5-25 下午4:30:11
	 * @param team
	 * @throws CRUDException
	 */
	public void deleteTeam(Team team) throws CRUDException {
		try{
			String[] pk_teams = team.getPk_team().split(",");
			
			for (int i = 0; i < pk_teams.length; i++) {
				team.setPk_team(pk_teams[i]);
				teamManagementMapper.deleteTeam(team);
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：检验编码是否已存在
	 * @author 马振
	 * 创建时间：2015-5-25 下午4:30:29
	 * @param team
	 * @return
	 * @throws CRUDException
	 */
	public String checkVcode(Team team) throws CRUDException {
		try{
			return teamManagementMapper.checkVcode(team);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：检验排序是否已存在
	 * @author 马振
	 * 创建时间：2015-5-25 下午4:30:43
	 * @param team
	 * @return
	 * @throws CRUDException
	 */
	public String checkIsortno(Team team) throws CRUDException {
		try{
			return teamManagementMapper.checkIsortno(team);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：为员工分配班组,如果员工排班中有了员工和与之对应的班组，则修改
	 * @author 马振
	 * 创建时间：2015-5-27 下午6:02:49
	 * @param employee
	 * @return
	 * @throws CRUDException
	 */
	public void addTeamToEmployee(Employee employee) throws CRUDException {
		try{
			String[] pk_employees = employee.getPk_employee().split(",");
			
			for (int i = 0; i < pk_employees.length; i++) {
				employee.setPk_employee(pk_employees[i]);
				teamManagementMapper.addTeamToEmployee(employee);
				teamManagementMapper.updateTeamInEmpsch(employee);
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}