package com.choice.hr.service.ScheduleAttendance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.hr.domain.ScheduleAttendance.CrewScheduling;
import com.choice.hr.persistence.ScheduleAttendance.QuerySchedulingMapper;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.service.common.CommonMISBOHService;

/**
 * 描述：排班考勤——排班查询
 * @author 马振
 * 创建时间：2015-5-28 下午1:10:18
 */
@Service
public class QuerySchedulingService {
	
	private static Logger log = Logger.getLogger(QuerySchedulingService.class);
	
	@Autowired
	private QuerySchedulingMapper querySchedulingMapper;
	
	@Autowired
	private CommonMISBOHService commonMISBOHService; 
	
	/**
	 * 描述：排班查询左侧树
	 * @author 马振
	 * 创建时间：2015-5-28 下午1:11:14
	 * @param crewScheduling
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> treeQueryScheduling(CrewScheduling crewScheduling, HttpSession session) throws CRUDException {
		try{
			crewScheduling.setPk_store(FormatDouble.StringCodeReplace(commonMISBOHService.getPkStore(session).getPk_store()));
			
			return querySchedulingMapper.treeQueryScheduling(crewScheduling);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：排班查询列表
	 * @author 马振
	 * 创建时间：2015-5-28 下午1:41:29
	 * @param crewScheduling
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> listQuery(CrewScheduling crewScheduling, HttpSession session) throws CRUDException {
		try{
			crewScheduling.setPk_store(FormatDouble.StringCodeReplace(commonMISBOHService.getPkStore(session).getPk_store()));

			List<Map<String, Object>> listMapQuery = new ArrayList<Map<String,Object>>();

			//若开始日期为空，则赋值当日
			if (ValueUtil.IsEmpty(crewScheduling.getVbdat())) {
				crewScheduling.setVbdat(DateJudge.getNowDate());
			}
			
			//若结束日期为空，则赋值当日
			if (ValueUtil.IsEmpty(crewScheduling.getVedat())) {
				crewScheduling.setVedat(DateJudge.getNowDate());
			}

			//获取这段时间内所有日期
			List<String> listDate = this.listDate(crewScheduling);
			
			//查询所选时间段内的员工主键
			List<CrewScheduling> findPk = querySchedulingMapper.findPk(crewScheduling);
			
				
			for (int k = 0; k < findPk.size(); k++) {
				Map<String, Object> map = new HashMap<String, Object>();
				
				crewScheduling.setPk_employee(findPk.get(k).getPk_employee());
				
				//根据员工主键查询所选时间段内的数据（此时间段内分配了班次的数据）
				List<Map<String, Object>> listQuery = querySchedulingMapper.listQuery(crewScheduling);
				
				for (int j = 0; j < listQuery.size(); j++) {
					
					//循环这个时间段
					for (int i = 0; i < listDate.size(); i++) {
						
						//根据员工主键查询所选时间段内的数据中的日期与这个时间段(查询条件)的日期相同，则以日期作为key把班次作为value放入map中，否则value为空
						if (listQuery.get(j).get("DWORKDATE").equals(listDate.get(i))) {
							map.put(listDate.get(i), listQuery.get(j).get("VNAME"));
						} else {
							map.put(listDate.get(i), "");
						}
					}
					
				}

				map.put("VSTATION", listQuery.get(0).get("VSTATION"));
				map.put("VKQCARDNO", listQuery.get(0).get("VKQCARDNO"));
				map.put("VEMPNAME", listQuery.get(0).get("VEMPNAME"));
				
				listMapQuery.add(map);
			}
			return listMapQuery;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：获取所选时间段内所有日期
	 * @author 马振
	 * 创建时间：2015-5-28 下午2:39:44
	 * @param crewScheduling
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<String> listDate(CrewScheduling crewScheduling) throws CRUDException {
		try{
			//获取这段时间内所有日期
			return DateJudge.getDateList(crewScheduling.getVbdat(),crewScheduling.getVedat());
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}