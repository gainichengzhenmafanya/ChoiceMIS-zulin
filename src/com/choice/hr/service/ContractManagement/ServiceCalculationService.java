package com.choice.hr.service.ContractManagement;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.hr.domain.ContractManagement.Contract;
import com.choice.hr.domain.ContractManagement.ServiceFormula;
import com.choice.hr.domain.PersonnelManagement.Employee;
import com.choice.hr.persistence.ContractManagement.ServiceCalculationMapper;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.commonutil.TypeConversionUtil;
import com.choice.misboh.commonutil.ValueUtil;

/**
 * 工龄计算 ---service
 * @author 文清泉
 * @param 2015年5月21日 下午2:14:29
 */
@Service
public class ServiceCalculationService {

	@Autowired
	public ServiceCalculationMapper serviceCalculationMapper;

	private static Logger log = Logger.getLogger(ServiceCalculationService.class);
	
	/**
	 * 编辑计算公式
	 * @author 文清泉
	 * @param 2015年5月22日 下午2:41:06
	 * @param serviceFormula
	 * @return
	 * @throws CRUDException
	 */
	public int saveServiceCalculation(ServiceFormula serviceFormula)throws CRUDException {
		try{
			//先确认当前主键是否已存在数据  --- 修改时使用
			serviceCalculationMapper.delServiceCalculation(serviceFormula);
			serviceCalculationMapper.saveServiceCalculation(serviceFormula);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
		return 1;
	}
	
	/**
	 * 查询计算公式
	 * @author 文清泉
	 * @param 2015年5月22日 下午2:41:20
	 * @param serviceFormula
	 * @return
	 */
	public List<ServiceFormula> listCalculationMode(ServiceFormula serviceFormula) {
		return serviceCalculationMapper.listCalculationMode(serviceFormula);
	}

	/**
	 * 删除计算公式
	 * @author 文清泉
	 * @param 2015年5月22日 下午3:46:50
	 * @param serviceFormula
	 * @return
	 * @throws CRUDException
	 */
	public String delCalculationModeRet(ServiceFormula serviceFormula) throws CRUDException{
		try{
			//先确认当前主键是否已存在数据  --- 修改时使用
			serviceCalculationMapper.delServiceCalculation(serviceFormula);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
		return "1";
	}
	
	/**
	 * 主键查询
	 * @author 文清泉
	 * @param 2015年5月22日 下午3:46:44
	 * @param serviceFormula
	 * @return
	 */
	public ServiceFormula listCalculationModeBy(ServiceFormula serviceFormula) {
		List<ServiceFormula> list = listCalculationMode(serviceFormula);
		if(list!=null && list.size()>0){
			return list.get(0);
		}
		return null;
	}

	/**
	 * 
	 * @author 文清泉
	 * @param 2015年5月22日 下午3:46:38
	 * @param contract
	 * @return
	 */
	public List<Employee> listServiceCalculation(Contract contract) {
		return serviceCalculationMapper.listServiceCalculation(contract);
	}

	/**
	 * 合同计算模式
	 * @author 文清泉
	 * @param 2015年6月5日 上午10:32:44
	 * @param pk_store 门店 
	 * @param pk_employee 需要计算员工
	 * @return
	 */
	public String calCalculationMode(String pk_store, String pk_employee)  throws CRUDException {
		try{
			String pk_emp = FormatDouble.StringCodeReplace(pk_employee);
			List<Map<String,Object>> empList = serviceCalculationMapper.getCalEmplyee(pk_store,pk_emp);
			List<Map<String,Object>> conTypList = serviceCalculationMapper.getCalConTyp(pk_store,pk_emp);
			//转行合同类型MAP对象
			Map<String,Object[]> conTypMap = TypeConversionUtil.listMapToMap(conTypList,"PK_CONTENTTYP",new String[]{"BFIELD","SERVICEDAY","BCDAT"});
			String sysdat = DateJudge.YYYY_MM_DD.format(new Date());
			for (Map<String, Object> empMap : empList) {
				Object[] obj = conTypMap.get(empMap.get("PK_PACKTYP"));
				if(obj!=null){
					// 工龄计算字段
					Object BFIELD = empMap.get(obj[0]);
					// 增减天数
					Object SERVICEDAY = obj[1];
					Integer vgl = 0;
					// 记录日期差异天数
					try {
						vgl = DateJudge.daysBetween(BFIELD.toString(),sysdat);
					} catch (ParseException e) {
					}
					vgl += ValueUtil.getIntValue(SERVICEDAY);
					// 修改数据库工龄天数
					serviceCalculationMapper.updateEmployeeVgl(pk_emp,vgl.toString());
				}else{
					continue;
				}
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
		return "1";
	}
}
