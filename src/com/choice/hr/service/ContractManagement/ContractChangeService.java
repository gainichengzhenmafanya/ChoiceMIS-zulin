package com.choice.hr.service.ContractManagement;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.hr.domain.ContractManagement.Contract;
import com.choice.hr.domain.ContractManagement.ContractRegistration;
import com.choice.hr.persistence.ContractManagement.ContractChangeMapper;

/**
 * 合同变更记录
 * @author 文清泉
 * @param 2015年5月19日 上午10:24:24
 */
@Service
public class ContractChangeService {

	@Autowired
	private ContractChangeMapper contractChangeMapper;

	/**
	 * 获取员工登记记录
	 * @author 文清泉
	 * @param 2015年5月21日 上午9:36:35
	 * @param contract
	 * @return
	 */
	public List<ContractRegistration> listContractChangeController(
			Contract contract) {
		return contractChangeMapper.listContractChangeController(contract);
	}
}
