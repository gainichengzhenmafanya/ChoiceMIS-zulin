package com.choice.hr.service.ContractManagement;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.hr.domain.ContractManagement.Contract;
import com.choice.hr.domain.ContractManagement.ContractRegistration;
import com.choice.hr.persistence.ContractManagement.ContractRegistrationMapper;
import com.choice.misboh.commonutil.DateJudge;

/**
 * 合同登记
 * @author 文清泉
 * @param 2015年5月18日 下午4:14:33
 */
@Service
public class ContractRegistrationService {
	
	@Autowired
	private ContractRegistrationMapper contractRegistrationMapper;
	
	private static Logger log = Logger.getLogger(ContractRegistrationService.class);
	/**
	 * 获取合同登记记录
	 * @author 文清泉
	 * @param 2015年5月19日 下午2:47:02
	 * @param reg
	 * @return
	 */
	public List<ContractRegistration>  listContractRegistration(ContractRegistration reg) {
		reg.setVSYSDATE(DateJudge.YYYY_MM_DD.format(new Date()));
		return contractRegistrationMapper.listContractRegistration(reg);
	}

	/**
	 * 获取合同登记表头数据
	 * @author 文清泉
	 * @param 2015年5月19日 下午2:46:55
	 * @param reg
	 * @return
	 */
	public Contract findContract(ContractRegistration reg) {
		return contractRegistrationMapper.findContract(reg);
	}

	/**
	 * 保存登记记录
	 * @author 文清泉
	 * @param 2015年5月19日 下午3:55:29
	 * @param reg
	 * @return
	 */
	public int saveContractRegistration(ContractRegistration reg)throws CRUDException{
		try{
			//先确认当前主键是否已存在数据  --- 修改时使用
			contractRegistrationMapper.delContractRegistration(reg);
			int count = contractRegistrationMapper.findContractByEmployee(reg);
			if(count <= 0){
				contractRegistrationMapper.updateEmployee(reg);
			}else{
				contractRegistrationMapper.updateEmployeeByContractTyp(reg);
			}
			contractRegistrationMapper.saveContractRegistration(reg);
			
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
		return 1;
	}

	/**
	 * 根据逐渐年获取 合同登记信息
	 * @author 文清泉
	 * @param 2015年5月19日 下午6:14:30
	 * @param reg
	 * @return
	 */
	public ContractRegistration listContractRegistrationBy(
			ContractRegistration reg) {
		return contractRegistrationMapper.listContractRegistrationBy(reg);
	}

	/**
	 * 删除合同登记记录
	 * @author 文清泉
	 * @param 2015年5月19日 下午6:56:49
	 * @param reg
	 * @return
	 */
	public String delContractReg(ContractRegistration reg)throws CRUDException {
		try{
			//先确认当前主键是否已存在数据  --- 修改时使用
			contractRegistrationMapper.delContractRegistration(reg);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
		return "1";
	}

}
