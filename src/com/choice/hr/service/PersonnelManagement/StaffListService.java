package com.choice.hr.service.PersonnelManagement;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.hr.domain.DataDictionary.DataTyp;
import com.choice.hr.domain.PersonnelManagement.Employee;
import com.choice.hr.domain.PersonnelManagement.Family;
import com.choice.hr.domain.PersonnelManagement.HrDept;
import com.choice.hr.persistence.PersonnelManagement.StaffListMapper;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.service.common.CommonMISBOHService;

/**
 * 描述：人员管理——人员列表
 * @author 马振
 * 创建时间：2015-5-12 上午9:07:25
 */
@Service
public class StaffListService {
	
	private static Logger log = Logger.getLogger(StaffListService.class);
	
	@Autowired
	private StaffListMapper staffListMapper;
	
	@Autowired
	private CommonMISBOHService commonMISBOHService; 
	
	/**
	 * 描述：人员列表左侧树
	 * @author 马振
	 * 创建时间：2015-5-12 上午9:19:52
	 * @param employee
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> staffListTree(HrDept hrDept, HttpSession session) throws CRUDException {
		try{
			
			hrDept.setPk_store(FormatDouble.StringCodeReplace(commonMISBOHService.getPkStore(session).getPk_store()));
			
			//如果启用状态为空，则选已启用的数据
			if (ValueUtil.IsEmpty(hrDept.getEnablestate())) {
				hrDept.setEnablestate(2);
			}
			
			return staffListMapper.staffListTree(hrDept);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询人力部门信息
	 * @author 马振
	 * 创建时间：2015-5-11 下午12:42:06
	 * @param hrDept
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<Employee> listEmployee(Employee employee, HttpSession session) throws CRUDException {
		try{
			//为门店主键加''
			employee.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
			
			//如果vempno不为空，则为其加''
			if (ValueUtil.IsNotEmpty(employee.getVempno())) {
				employee.setVempno(FormatDouble.StringCodeReplace(employee.getVempno()));
			}
			
			//如果vname不为空，则为其加''
			if (ValueUtil.IsNotEmpty(employee.getVname())) {
				employee.setVname(FormatDouble.StringCodeReplace(employee.getVname()));
			}
			
			//如果vinit不为空，则为其加''
			if (ValueUtil.IsNotEmpty(employee.getVinit())) {
				employee.setVinit(FormatDouble.StringCodeReplace(employee.getVinit()));
			}
			
			//如果pk_hrdept不为空，则为其加''
			if (ValueUtil.IsNotEmpty(employee.getPk_hrdept())) {
				employee.setPk_hrdept(FormatDouble.StringCodeReplace(employee.getPk_hrdept()));
			}
			
			//如果人事列表传过来的人事员工主键为undefined或null，则赋值为空
			if ("undefined".equals(employee.getPk_employee()) || null == employee.getPk_employee()) {
				employee.setPk_employee("");
			} else {
				//为人员主键加''
				employee.setPk_employee(FormatDouble.StringCodeReplace(employee.getPk_employee()));
			}
			
			//如果employee中vstatus为空，则没有员工状态的查询条件，默认为试用、在职、退休；如果是0，则查询全部，默认为空
			if ("".equals(employee.getVstatuscode())) {
				employee.setVstatuscode("1,2,3");
			} else if ("0".equals(employee.getVstatuscode())) {
				employee.setVstatuscode("");
			} else {
				FormatDouble.StringCodeReplace(employee.getVstatuscode());
			}
			
			return staffListMapper.listEmployee(employee);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：根据班组查询员工
	 * @author 马振
	 * 创建时间：2015-5-28 上午10:58:19
	 * @param employee
	 * @return
	 * @throws CRUDException
	 */
	public List<Employee> findByTeam(Employee employee) throws CRUDException {
		try{
			return staffListMapper.findByTeam(employee);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：根据类别查询数据字典表中的数据(vtyp为必输)
	 * @author 马振
	 * 创建时间：2015-5-14 下午4:32:07
	 * @param type
	 * @throws CRUDException
	 */
	public List<DataTyp> listDataTyp(DataTyp dataTyp) throws CRUDException {
		try{
			return staffListMapper.listDataTyp(dataTyp);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询员工家庭成员
	 * @author 马振
	 * 创建时间：2015-5-15 上午9:52:21
	 * @param family
	 * @return
	 * @throws CRUDException
	 */
	public List<Family> listFamilyByEmp(Employee employee,HttpSession session) throws CRUDException {
		try{
			Family family = new Family();//创建家庭对象，用于存储人员主键
			
			//为门店主键加''
			family.setPk_store(FormatDouble.StringCodeReplace(commonMISBOHService.getPkStore(session).getPk_store()));
			
			//为人员主键加''
			family.setPk_employee(FormatDouble.StringCodeReplace(employee.getPk_employee()));
			return staffListMapper.listFamilyByEmp(family);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：若是新增根据主键查询家庭成员
	 * @author 马振
	 * 创建时间：2015-5-15 下午3:41:38
	 * @param employee
	 * @return
	 * @throws CRUDException
	 */
	public Family findFamilyByPk(Employee employee,HttpSession session) throws CRUDException {
		try{
			Family family = new Family();//创建家庭对象，用于存储人员主键
			
			//为门店主键加''
			family.setPk_store(FormatDouble.StringCodeReplace(commonMISBOHService.getPkStore(session).getPk_store()));
			
			//为家庭成员主键加''
			family.setPk_family(FormatDouble.StringCodeReplace(employee.getPk_employee()));
			
			return staffListMapper.listFamilyByEmp(family).get(0);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：修改人员信息
	 * @author 马振
	 * 创建时间：2015-5-14 下午2:47:52
	 * @param employee
	 * @param session
	 * @throws CRUDException
	 */
	public void updateEmployee(Employee employee,HttpSession session) throws CRUDException {
		try{
			DataTyp dataTyp = new DataTyp();
			
			//民族
//			dataTyp.setPk_datatyp(FormatDouble.StringCodeReplace(employee.getPk_nation()));
//			employee.setVnation(staffListMapper.findDataTypByPk(dataTyp).getVname());

			//学历
			dataTyp.setPk_datatyp(FormatDouble.StringCodeReplace(employee.getPk_schoolage()));
			employee.setVschoolage(staffListMapper.findDataTypByPk(dataTyp).getVname());

			//学位
			if (ValueUtil.IsNotEmpty(employee.getPk_degree())) {
				dataTyp.setPk_datatyp(FormatDouble.StringCodeReplace(employee.getPk_degree()));
				employee.setVdegree(staffListMapper.findDataTypByPk(dataTyp).getVname());
			}
			
			//政治面貌
			dataTyp.setPk_datatyp(FormatDouble.StringCodeReplace(employee.getPk_publiclist()));
			employee.setVpubliclist(staffListMapper.findDataTypByPk(dataTyp).getVname());
			
			//户口类型
			dataTyp.setPk_datatyp(FormatDouble.StringCodeReplace(employee.getPk_house()));
			employee.setVhouse(staffListMapper.findDataTypByPk(dataTyp).getVname());
			
			//籍贯
			dataTyp.setPk_datatyp(FormatDouble.StringCodeReplace(employee.getPk_home()));
			employee.setVhome(staffListMapper.findDataTypByPk(dataTyp).getVname());
			
			//银行卡类型
//			dataTyp.setPk_datatyp(FormatDouble.StringCodeReplace(employee.getPk_banktyp()));
//			employee.setVbanktyp(staffListMapper.findDataTypByPk(dataTyp).getVname());
			
			//健康证办理地区
//			dataTyp.setPk_datatyp(FormatDouble.StringCodeReplace(employee.getPk_jkarea()));
//			employee.setVjkarea(staffListMapper.findDataTypByPk(dataTyp).getVname());
			
			//税率类型
			dataTyp.setPk_datatyp(FormatDouble.StringCodeReplace(employee.getPk_taxno()));
			employee.setVtaxno(staffListMapper.findDataTypByPk(dataTyp).getVname());
			
			//合同类型
//			dataTyp.setPk_datatyp(FormatDouble.StringCodeReplace(employee.getPk_packtyp()));
//			employee.setVpacktyp(staffListMapper.findDataTypByPk(dataTyp).getVname());
			
			//工资级别
			dataTyp.setPk_datatyp(FormatDouble.StringCodeReplace(employee.getPk_dutyid()));
			employee.setVdutyid(staffListMapper.findDataTypByPk(dataTyp).getVname());
			
			//用工来源
			dataTyp.setPk_datatyp(FormatDouble.StringCodeReplace(employee.getPk_comfrom()));
			employee.setVcomfrom(staffListMapper.findDataTypByPk(dataTyp).getVname());
			
			//保险状态
			dataTyp.setPk_datatyp(FormatDouble.StringCodeReplace(employee.getPk_bxsta()));
			employee.setVbxsta(staffListMapper.findDataTypByPk(dataTyp).getVname());
			
			//岗位级别
			dataTyp.setPk_datatyp(FormatDouble.StringCodeReplace(employee.getPk_dutylvl()));
			employee.setVdutylvl(staffListMapper.findDataTypByPk(dataTyp).getVname());
			
			//外语等级
			dataTyp.setPk_datatyp(FormatDouble.StringCodeReplace(employee.getPk_technic()));
			employee.setVtechnic(staffListMapper.findDataTypByPk(dataTyp).getVname());
			
			//任职岗位
			dataTyp.setPk_datatyp(FormatDouble.StringCodeReplace(employee.getPk_station()));
			employee.setVstation(staffListMapper.findDataTypByPk(dataTyp).getVname());
			
			//员工状态
//			dataTyp.setPk_datatyp(FormatDouble.StringCodeReplace(employee.getPk_status()));
//			employee.setVstatus(staffListMapper.findDataTypByPk(dataTyp).getVname());
			
			//婚姻状况
			dataTyp.setPk_datatyp(FormatDouble.StringCodeReplace(employee.getPk_marriage()));
			employee.setVmarriage(staffListMapper.findDataTypByPk(dataTyp).getVname());
			
			//性别
			dataTyp.setPk_datatyp(FormatDouble.StringCodeReplace(employee.getPk_sex()));
			employee.setVsex(staffListMapper.findDataTypByPk(dataTyp).getVname());
			
			staffListMapper.updateEmployee(employee);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：新增家庭成员
	 * @author 马振
	 * 创建时间：2015-5-15 上午9:53:48
	 * @param family
	 * @param session
	 * @throws CRUDException
	 */
	public void saveFamily(Family family,HttpSession session) throws CRUDException {
		try{
			
			//保存新增
			if ("add".equals(family.getFlag())) {
				family.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
				family.setPk_family(CodeHelper.createUUID().substring(0, 20).toUpperCase());
				staffListMapper.saveFamily(family);
			}
			
			//保存修改
			if ("update".equals(family.getFlag())) {
				staffListMapper.updateFamily(family);
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：员工转正
	 * @author 马振
	 * 创建时间：2015-5-19 下午2:51:23
	 * @param employee
	 * @param session
	 * @throws CRUDException
	 */
	public void employeePositive(Employee employee,HttpSession session) throws CRUDException {
		try{		
			DataTyp dataTyp = new DataTyp();
			dataTyp.setVtyp(DDT.DATATYPE_TYP18);
			dataTyp.setVcode("2");
			employee.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
			dataTyp = staffListMapper.findDataTypByPk(dataTyp);
			employee.setPk_status(dataTyp.getPk_datatyp());
			employee.setVstatus(dataTyp.getVname());
			employee.setVstatuscode("2");
			staffListMapper.employeePositive(employee);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}