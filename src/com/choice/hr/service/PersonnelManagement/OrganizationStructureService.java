package com.choice.hr.service.PersonnelManagement;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.hr.domain.PersonnelManagement.HrDept;
import com.choice.hr.persistence.PersonnelManagement.OrganizationStructureMapper;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.service.common.CommonMISBOHService;

/**
 * 描述：人事管理—人员列表
 * @author 马振
 * 创建时间：2015-5-11 上午9:05:46
 */
@Service
public class OrganizationStructureService {

	private static Logger log = Logger.getLogger(OrganizationStructureService.class);
	
	@Autowired
	private OrganizationStructureMapper organizationStructureMapper;
	
	@Autowired
	private CommonMISBOHService commonMISBOHService; 
	
	
	/**
	 * 描述：人力资源组织结构
	 * @author 马振
	 * 创建时间：2015-5-11 上午10:24:21
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> listPersonnelStructureTree(HrDept hrDept, HttpSession session) throws CRUDException {
		try{
			
			//获取门店主键并加上''，放入部门实体类中
			hrDept.setPk_store(FormatDouble.StringCodeReplace(commonMISBOHService.getPkStore(session).getPk_store()));
			
			//如果启用状态为空，则选已启用的数据
			if (ValueUtil.IsEmpty(hrDept.getEnablestate())) {
				hrDept.setEnablestate(2);
			}
			
			return organizationStructureMapper.personnelStructureTree(hrDept);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询人力部门信息
	 * @author 马振
	 * 创建时间：2015-5-11 下午12:42:06
	 * @param hrDept
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<HrDept> listHrDept(HrDept hrDept,HttpSession session) throws CRUDException {
		try{
			//为门店主键加''
			hrDept.setPk_store(FormatDouble.StringCodeReplace(commonMISBOHService.getPkStore(session).getPk_store()));
			
			//如果pk_hrdept不为空，则为其加''
			if (ValueUtil.IsNotEmpty(hrDept.getPk_hrdept())) {
				hrDept.setPk_hrdept(FormatDouble.StringCodeReplace(hrDept.getPk_hrdept()));
			}
			
			return organizationStructureMapper.listHrDept(hrDept);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：保存新增的部门
	 * @author 马振
	 * 创建时间：2015-5-11 下午3:09:08
	 * @param hrDept
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public void saveHrDept(HrDept hrDept,HttpSession session) throws CRUDException {
		try{
			hrDept.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
			hrDept.setPk_hrdept(CodeHelper.createUUID().substring(0, 20).toUpperCase());
			
			organizationStructureMapper.saveHrDept(hrDept);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：修改人力资源部门
	 * @author 马振
	 * 创建时间：2015-5-11 下午4:06:43
	 * @param hrDept
	 * @param session
	 * @throws CRUDException
	 */
	public void updateHrDept(HrDept hrDept,HttpSession session) throws CRUDException {
		try{
			//如果pk_hrdept不为空，则为其加''
			if (ValueUtil.IsNotEmpty(hrDept.getPk_hrdept())) {
				hrDept.setPk_hrdept(FormatDouble.StringCodeReplace(hrDept.getPk_hrdept()));
			}
			organizationStructureMapper.updateHrDept(hrDept);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：启用、停用
	 * @author 马振
	 * 创建时间：2015-5-11 下午4:49:55
	 * @param hrDept
	 * @param session
	 * @throws CRUDException
	 */
	public void updateEnableState(HrDept hrDept,HttpSession session) throws CRUDException {
		try{
			//如果pk_hrdept不为空，则为其加''
			if (ValueUtil.IsNotEmpty(hrDept.getPk_hrdept())) {
				hrDept.setPk_hrdept(FormatDouble.StringCodeReplace(hrDept.getPk_hrdept()));
			}
			organizationStructureMapper.updateEnableState(hrDept);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}