package com.choice.hr.service.PersonnelManagement;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.hr.domain.PersonnelManagement.Employee;
import com.choice.hr.persistence.PersonnelManagement.EmployeeEntryMapper;
/**
 * 员工入职
 * @author 文清泉
 * @param 2015年5月15日 下午5:34:37
 */
@Service
public class EmployeeEntryService {
	private static Logger log = Logger.getLogger(EmployeeEntryService.class);
	@Autowired
	private EmployeeEntryMapper employeeEntryMapper;
	
	
	/**
	 * 获取门店最大员工编号
	 * @author 文清泉
	 * @param 2015年5月15日 下午5:41:37
	 * @param code 门店编码
	 * @return
	 */
	public String findEmployeeCard(String code) {
		String empno = employeeEntryMapper.findEmployeeCard(code);
		if(empno == null ||  empno.equals("0")){
			empno = code+"0001";
		}else{
			if(empno.indexOf(code)!=-1){
				String no = (Integer.parseInt(empno.substring(empno.indexOf(code)+code.length(),empno.length()))+1)+"";
				empno = "";
				for(int k=0;k<(4-no.length());k++){
					empno += "0";
				}
				empno = code +empno+no;
			}else{
				empno = code+"0001";
			}
		}
		return empno;
	}
	
	/**
	 * 保存入职记录
	 * @author 文清泉
	 * @param 2015年5月15日 下午6:25:29
	 * @param employee
	 * @return
	 */
	public int saveEmployeeEntry(Employee employee) throws CRUDException {
		try{
			employeeEntryMapper.saveEmployeeEntry(employee);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
		return 1;
	}

	public String checkBlackList(String vidcard) {
		return employeeEntryMapper.checkBlackList(vidcard);
	}
}
