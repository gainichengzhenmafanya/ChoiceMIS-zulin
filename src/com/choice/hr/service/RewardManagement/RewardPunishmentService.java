package com.choice.hr.service.RewardManagement;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.hr.domain.DataDictionary.DataTyp;
import com.choice.hr.domain.RewardManagement.RewardPunishment;
import com.choice.hr.persistence.PersonnelManagement.StaffListMapper;
import com.choice.hr.persistence.RewardManagement.RewardPunishmentMapper;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.service.common.CommonMISBOHService;

/**
 * 描述：奖惩管理——奖惩登记
 * @author 马振
 * 创建时间：2015-6-5 下午3:28:02
 */
@Service
public class RewardPunishmentService {
	
	private static Logger log = Logger.getLogger(RewardPunishmentService.class);
	
	@Autowired
	private RewardPunishmentMapper rewardPunishmentMapper;
	
	@Autowired
	private StaffListMapper staffListMapper;
	
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	
	/**
	 * 描述：根据日期查询奖惩登记
	 * @author 马振
	 * 创建时间：2015-6-5 下午3:28:54
	 * @param RewardPunishment
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<RewardPunishment> listRewardPunishment(RewardPunishment rewardPunishment, HttpSession session) throws CRUDException {
		try{
			rewardPunishment.setPk_store(FormatDouble.StringCodeReplace(commonMISBOHService.getPkStore(session).getPk_store()));
			return rewardPunishmentMapper.listRewardPunishment(rewardPunishment);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：根据主键查询
	 * @author 马振
	 * 创建时间：2015-6-5 下午3:29:17
	 * @param RewardPunishment
	 * @return
	 * @throws CRUDException
	 */
	public RewardPunishment getByPk(RewardPunishment rewardPunishment) throws CRUDException {
		try{
			return rewardPunishmentMapper.getByPk(rewardPunishment);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：新增员工奖惩登记
	 * @author 马振
	 * 创建时间：2015-6-5 下午3:30:36
	 * @param rewardPunishment
	 * @param session
	 * @throws CRUDException
	 */
	public void saveRewardPunishment(RewardPunishment rewardPunishment, HttpSession session) throws CRUDException {
		try{
			rewardPunishment.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
			rewardPunishment.setPk_rewardpunishment(CodeHelper.createUUID().substring(0, 20).toUpperCase());
			
			DataTyp dataTyp = new DataTyp();
			dataTyp.setVtyp(DDT.DATATYPE_TYP24);					//奖惩类型
			dataTyp.setVcode(rewardPunishment.getPk_repunityp());	//奖惩编码
			
			//根据类型(奖惩类型为24)和编码查询数据字典中的主键(即所选奖惩主键)，
			rewardPunishment.setPk_repunityp(staffListMapper.findDataTypByPk(dataTyp).getPk_datatyp());
			rewardPunishmentMapper.saveRewardPunishment(rewardPunishment);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：修改奖惩登记
	 * @author 马振
	 * 创建时间：2015-6-5 下午3:31:12
	 * @param rewardPunishment
	 * @param session
	 * @throws CRUDException
	 */
	public void updateRewardPunishment(RewardPunishment rewardPunishment, HttpSession session) throws CRUDException {
		try{
			DataTyp dataTyp = new DataTyp();
			dataTyp.setVtyp(DDT.DATATYPE_TYP24);					//奖惩类型
			dataTyp.setVcode(rewardPunishment.getPk_repunityp());	//奖惩编码
			
			//根据类型(奖惩类型为24)和编码查询数据字典中的主键(即所选奖惩主键)，
			rewardPunishment.setPk_repunityp(staffListMapper.findDataTypByPk(dataTyp).getPk_datatyp());
			
			rewardPunishmentMapper.updateRewardPunishment(rewardPunishment);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：删除奖惩登记
	 * @author 马振
	 * 创建时间：2015-6-5 下午3:32:02
	 * @param rewardPunishment
	 * @param session
	 * @throws CRUDException
	 */
	public void deleteRewardPunishment(RewardPunishment rewardPunishment, HttpSession session) throws CRUDException {
		try{
			String[] pk_RewardPunishments = rewardPunishment.getPk_rewardpunishment().split(",");
			
			for (int i = 0; i < pk_RewardPunishments.length; i++) {
				rewardPunishment.setPk_rewardpunishment(pk_RewardPunishments[i]);
				rewardPunishmentMapper.deleteRewardPunishment(rewardPunishment);
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}