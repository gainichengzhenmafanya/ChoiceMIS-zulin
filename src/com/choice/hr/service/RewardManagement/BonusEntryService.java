package com.choice.hr.service.RewardManagement;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.hr.domain.RewardManagement.BonusEntry;
import com.choice.hr.persistence.RewardManagement.BonusEntryMapper;
import com.choice.misboh.service.common.CommonMISBOHService;

/**
 * 描述：奖惩管理——奖金录入
 * @author 马振
 * 创建时间：2015-6-9 上午11:23:56
 */
@Service
public class BonusEntryService {
	
	private static Logger log = Logger.getLogger(BonusEntryService.class);
	
	@Autowired
	private BonusEntryMapper bonusEntryMapper;
	
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	
	/**
	 * 描述：左侧树
	 * @author 马振
	 * 创建时间：2015-6-9 上午11:26:53
	 * @param bonusEntry
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> treeBonusEntry(BonusEntry bonusEntry, HttpSession session) throws CRUDException {
		try{
			bonusEntry.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
			return bonusEntryMapper.treeBonusEntry(bonusEntry);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：奖金录入列表
	 * @author 马振
	 * 创建时间：2015-6-9 上午11:26:27
	 * @param bonusEntry
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<BonusEntry> listBonusEntry(BonusEntry bonusEntry, HttpSession session) throws CRUDException {
		try{
			bonusEntry.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
			return bonusEntryMapper.listBonusEntry(bonusEntry);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：获取需要录入奖金的员工
	 * @author 马振
	 * 创建时间：2015-6-9 下午2:53:25
	 * @param bonusEntry
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<BonusEntry> getEmpBonus(BonusEntry bonusEntry, HttpSession session) throws CRUDException {
		try{
			bonusEntry.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
			return bonusEntryMapper.getEmpBonus(bonusEntry);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：新增录入奖金
	 * @author 马振
	 * 创建时间：2015-6-10 上午9:51:26
	 * @param bonusEntry
	 * @throws CRUDException
	 */
	public void addBonusEntry(BonusEntry bonusEntry) throws CRUDException {
		try{
			List<BonusEntry> listBonus = new ArrayList<BonusEntry>();
			
			for (int i = 0; i < bonusEntry.getList().size(); i++) {
				
				//只有全勤奖或本月岗位奖不为0时才可添加
				if (bonusEntry.getList().get(i).getNfullaward() != 0 || bonusEntry.getList().get(i).getNthisaward() != 0) {
					bonusEntry.getList().get(i).setPk_bonusentry(CodeHelper.createUUID().substring(0, 20).toString().toUpperCase());
					listBonus.add(bonusEntry.getList().get(i));
				}
			}
			
			bonusEntryMapper.addBonusEntry(listBonus);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}