package com.choice.hr.service.LeaveManagement;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.hr.domain.LeaveManagement.LeaveRegistration;
import com.choice.hr.persistence.LeaveManagement.AnnualLeaveMapper;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.service.common.CommonMISBOHService;

/**
 * 描述：休假管理——年休假
 * @author 马振
 * 创建时间：2015-6-4 上午11:00:55
 */
@Service
public class AnnualLeaveService {
	
	private static Logger log = Logger.getLogger(AnnualLeaveService.class);
	
	@Autowired
	private AnnualLeaveMapper annualLeaveMapper;
	
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	
	/**
	 * 描述：根据日期查询年休假
	 * @author 马振
	 * 创建时间：2015-6-4 上午11:02:28
	 * @param leaveRegistration
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<LeaveRegistration> listAnnualLeave(LeaveRegistration leaveRegistration, HttpSession session) throws CRUDException {
		try{
			leaveRegistration.setPk_store(FormatDouble.StringCodeReplace(commonMISBOHService.getPkStore(session).getPk_store()));
			return annualLeaveMapper.listAnnualLeave(leaveRegistration);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：根据主键查询
	 * @author 马振
	 * 创建时间：2015-6-4 上午11:02:48
	 * @param leaveRegistration
	 * @return
	 * @throws CRUDException
	 */
	public LeaveRegistration getByPk(LeaveRegistration leaveRegistration) throws CRUDException {
		try{
			return annualLeaveMapper.getByPk(leaveRegistration);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：新增员工年休假
	 * @author 马振
	 * 创建时间：2015-6-4 上午11:03:18
	 * @param leaveRegistration
	 * @param session
	 * @throws CRUDException
	 */
	public void saveAnnualLeave(LeaveRegistration leaveRegistration, HttpSession session) throws CRUDException {
		try{
			leaveRegistration.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
			leaveRegistration.setPk_annualleave(CodeHelper.createUUID().substring(0, 20).toUpperCase());
			annualLeaveMapper.saveAnnualLeave(leaveRegistration);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：修改员工年休假
	 * @author 马振
	 * 创建时间：2015-6-4 上午11:03:53
	 * @param leaveRegistration
	 * @param session
	 * @throws CRUDException
	 */
	public void updateAnnualLeave(LeaveRegistration leaveRegistration, HttpSession session) throws CRUDException {
		try{
			annualLeaveMapper.updateAnnualLeave(leaveRegistration);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：删除员工年休假
	 * @author 马振
	 * 创建时间：2015-6-4 上午11:06:06
	 * @param leaveRegistration
	 * @param session
	 * @throws CRUDException
	 */
	public void deleteAnnualLeave(LeaveRegistration leaveRegistration, HttpSession session) throws CRUDException {
		try{
			List<String> idList = Arrays.asList(leaveRegistration.getPk_annualleave().split(","));

			annualLeaveMapper.deleteAnnualLeave(idList);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}