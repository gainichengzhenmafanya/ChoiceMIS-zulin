package com.choice.hr.service.LeaveManagement;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.hr.domain.LeaveManagement.LeaveRegistration;
import com.choice.hr.persistence.LeaveManagement.LeaveInquiryMapper;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.service.common.CommonMISBOHService;

/**
 * 描述：休假管理——休假查询
 * @author 马振
 * 创建时间：2015-6-5 上午10:17:29
 */
@Service
public class LeaveInquiryService {
	
	private static Logger log = Logger.getLogger(LeaveInquiryService.class);
	
	@Autowired
	private LeaveInquiryMapper leaveInquiryMapper;
	
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	
	/**
	 * 描述：左侧树
	 * @author 马振
	 * 创建时间：2015-6-5 上午10:17:48
	 * @param team
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> treeLeaveInquiry(LeaveRegistration leaveRegistration, HttpSession session) throws CRUDException {
		try{
			leaveRegistration.setPk_store(FormatDouble.StringCodeReplace(commonMISBOHService.getPkStore(session).getPk_store()));
			return leaveInquiryMapper.treeLeaveInquiry(leaveRegistration);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：休假查询列表
	 * @author 马振
	 * 创建时间：2015-6-5 上午10:26:14
	 * @param leaveRegistration
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<LeaveRegistration> listLeaveInquiry(LeaveRegistration leaveRegistration, HttpSession session) throws CRUDException {
		try{
			leaveRegistration.setPk_store(FormatDouble.StringCodeReplace(commonMISBOHService.getPkStore(session).getPk_store()));
			return leaveInquiryMapper.listLeaveInquiry(leaveRegistration);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}