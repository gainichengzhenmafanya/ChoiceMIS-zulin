package com.choice.hr.service.LeaveManagement;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.hr.domain.LeaveManagement.LeaveRegistration;
import com.choice.hr.persistence.LeaveManagement.LeaveRegistrationMapper;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.service.common.CommonMISBOHService;

/**
 * 描述：休假管理——休假登记
 * @author 马振
 * 创建时间：2015-5-12 上午9:07:25
 */
@Service
public class LeaveRegistrationService {
	
	private static Logger log = Logger.getLogger(LeaveRegistrationService.class);
	
	@Autowired
	private LeaveRegistrationMapper leaveRegistrationMapper;
	
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	
	/**
	 * 描述：根据日期查询员工休假
	 * @author 马振
	 * 创建时间：2015-6-3 上午8:54:04
	 * @param leaveRegistration
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<LeaveRegistration> listLeaveRegistration(LeaveRegistration leaveRegistration, HttpSession session) throws CRUDException {
		try{
			leaveRegistration.setPk_store(FormatDouble.StringCodeReplace(commonMISBOHService.getPkStore(session).getPk_store()));
			return leaveRegistrationMapper.listLeaveRegistration(leaveRegistration);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：根据主键查询
	 * @author 马振
	 * 创建时间：2015-6-3 下午6:55:16
	 * @param leaveRegistration
	 * @return
	 * @throws CRUDException
	 */
	public LeaveRegistration getByPk(LeaveRegistration leaveRegistration) throws CRUDException {
		try{
			return leaveRegistrationMapper.getByPk(leaveRegistration);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：新增员工休假登记
	 * @author 马振
	 * 创建时间：2015-6-3 下午1:09:57
	 * @param leaveRegistration
	 * @param session
	 * @throws CRUDException
	 */
	public void saveEmployeeLeave(LeaveRegistration leaveRegistration, HttpSession session) throws CRUDException {
		try{
			leaveRegistration.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
			leaveRegistration.setPk_leaveregistration(CodeHelper.createUUID().substring(0, 20).toUpperCase());
			leaveRegistrationMapper.saveEmployeeLeave(leaveRegistration);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：修改员工登记
	 * @author 马振
	 * 创建时间：2015-6-3 下午3:31:23
	 * @param leaveRegistration
	 * @param session
	 * @throws CRUDException
	 */
	public void updateLeave(LeaveRegistration leaveRegistration, HttpSession session) throws CRUDException {
		try{
			leaveRegistrationMapper.updateLeave(leaveRegistration);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：删除员工休假登记
	 * @author 马振
	 * 创建时间：2015-6-3 下午3:32:01
	 * @param leaveRegistration
	 * @param session
	 * @throws CRUDException
	 */
	public void deleteLeave(LeaveRegistration leaveRegistration, HttpSession session) throws CRUDException {
		try{
			String[] pk_leaveregistrations = leaveRegistration.getPk_leaveregistration().split(",");
			
			for (int i = 0; i < pk_leaveregistrations.length; i++) {
				leaveRegistration.setPk_leaveregistration(pk_leaveregistrations[i]);
				leaveRegistrationMapper.deleteLeave(leaveRegistration);
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}