package com.choice.hr.service.DataDictionary;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.hr.domain.DataDictionary.DataTyp;
import com.choice.hr.persistence.DataDictionary.DataDictionaryMapper;
import com.choice.misboh.commonutil.FormatDouble;

/**
 * 描述：数据字典维护
 * @author 马振
 * 创建时间：2015-5-22 下午2:20:18
 */
@Service
public class DataDictionaryService {
	
	private static Logger log = Logger.getLogger(DataDictionaryService.class);
	
	@Autowired
	private DataDictionaryMapper dataDictionaryMapper;
	
	/**
	 * 描述：根据类别查询数据字典表中的数据(vtyp为必输)
	 * @author 马振
	 * 创建时间：2015-5-22 下午2:22:08
	 * @param dataTyp
	 * @return
	 * @throws CRUDException
	 */
	public List<DataTyp> listDataTyp(DataTyp dataTyp) throws CRUDException {
		try{
			return dataDictionaryMapper.listDataTyp(dataTyp);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：获取一种类型下最大编码和排序+1
	 * @author 马振
	 * 创建时间：2015-5-22 下午4:14:51
	 * @param dataTyp
	 * @return
	 * @throws CRUDException
	 */
	public DataTyp getMaxVcodeAndIsortno(DataTyp dataTyp) throws CRUDException {
		try{
			return dataDictionaryMapper.getMaxVcodeAndIsortno(dataTyp);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：新增数据
	 * @author 马振
	 * 创建时间：2015-5-22 下午3:49:13
	 * @param dataTyp
	 * @throws CRUDException
	 */
	public void addDataTyp(DataTyp dataTyp) throws CRUDException {
		try{
			dataTyp.setPk_datatyp(CodeHelper.createUUID().substring(0,20).toUpperCase());
			dataDictionaryMapper.addDataTyp(dataTyp);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：根据主键查询数据字典数据
	 * @author 马振
	 * 创建时间：2015-5-22 下午2:22:29
	 * @param dataTyp
	 * @return
	 * @throws CRUDException
	 */
	public DataTyp findDataTypByPk(DataTyp dataTyp) throws CRUDException {
		try{
			dataTyp.setPk_datatyp(FormatDouble.StringCodeReplace(dataTyp.getPk_datatyp()));
			return dataDictionaryMapper.findDataTypByPk(dataTyp);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：修改数据字典数据
	 * @author 马振
	 * 创建时间：2015-5-22 下午2:23:30
	 * @param dataTyp
	 * @throws CRUDException
	 */
	public void updateDataTyp(DataTyp dataTyp) throws CRUDException {
		try{
			dataDictionaryMapper.updateDataTyp(dataTyp);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：删除数据字典数据
	 * @author 马振
	 * 创建时间：2015-5-22 下午2:23:53
	 * @param dataTyp
	 * @throws CRUDException
	 */
	public void deleteDataTyp(DataTyp dataTyp) throws CRUDException {
		try{
			String[] pk_dataTyps = dataTyp.getPk_datatyp().split(",");
			
			for (int i = 0; i < pk_dataTyps.length; i++) {
				dataTyp.setPk_datatyp(pk_dataTyps[i]);
				dataDictionaryMapper.deleteDataTyp(dataTyp);
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：检验同类型的编码是否已存在
	 * @author 马振
	 * 创建时间：2015-5-22 下午3:36:37
	 * @param dataTyp
	 * @return
	 * @throws CRUDException
	 */
	public String checkVcode(DataTyp dataTyp) throws CRUDException {
		try{
			return dataDictionaryMapper.checkVcode(dataTyp);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：检验同类型的排序是否已存在
	 * @author 马振
	 * 创建时间：2015-5-22 下午4:36:11
	 * @param dataTyp
	 * @return
	 * @throws CRUDException
	 */
	public String checkIsortno(DataTyp dataTyp) throws CRUDException {
		try{
			return dataDictionaryMapper.checkIsortno(dataTyp);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}