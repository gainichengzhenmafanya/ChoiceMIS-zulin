package com.choice.hr.web.controller.DataDictionary;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.hr.constants.DataDictionary.DataDictionaryConstants;
import com.choice.hr.domain.DataDictionary.DataTyp;
import com.choice.hr.service.DataDictionary.DataDictionaryService;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.orientationSys.constants.StringConstant;

/**
 * 描述：数据字典维护
 * @author 马振
 * 创建时间：2015-5-22 下午1:58:58
 */
@Controller
@RequestMapping(value="dataDictionary")
public class DataDictionaryController {
	
	@Autowired
	private DataDictionaryService dataDictionaryService;

	@Autowired
	private CommonMISBOHService commonMISBOHService; 
	
	/**
	 * 描述：数据字典列表
	 * @author 马振
	 * 创建时间：2015-5-22 下午2:03:15
	 * @param modelMap
	 * @param hrDept
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/listData")
	public ModelAndView listData(ModelMap modelMap, DataTyp dataTyp, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源

		dataTyp.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		modelMap.put("vtyp", dataTyp.getVtyp());
		modelMap.put("listDataTyp", dataDictionaryService.listDataTyp(dataTyp));
		return new ModelAndView(DataDictionaryConstants.LISTDATADICTIONARY, modelMap);
	}
	
	/**
	 * 描述：新增数据
	 * @author 马振
	 * 创建时间：2015-5-22 下午3:11:58
	 * @param modelMap
	 * @param dataTyp
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/addData")
	public ModelAndView addData(ModelMap modelMap, DataTyp dataTyp, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("pk_store",commonMISBOHService.getPkStore(session).getPk_store());
		modelMap.put("vtyp", dataTyp.getVtyp());
		modelMap.put("dataTyp", dataDictionaryService.getMaxVcodeAndIsortno(dataTyp));
		return new ModelAndView(DataDictionaryConstants.ADDDATADICTIONARY, modelMap);
	}
	
	/**
	 * 描述：修改数据
	 * @author 马振
	 * 创建时间：2015-5-22 下午3:14:54
	 * @param modelMap
	 * @param dataTyp
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/updateData")
	public ModelAndView updateData(ModelMap modelMap, DataTyp dataTyp, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("dataTyp", dataDictionaryService.findDataTypByPk(dataTyp));
		return new ModelAndView(DataDictionaryConstants.UPDATEDATADICTIONARY, modelMap);
	}
	
	/**
	 * 描述：保存数据
	 * @author 马振
	 * 创建时间：2015-5-22 下午3:33:47
	 * @param modelMap
	 * @param dataTyp
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/saveData")
	public ModelAndView saveData(ModelMap modelMap, DataTyp dataTyp, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源

		//若dataTyp中的flag为add，则是新增
		if ("add".equals(dataTyp.getFlag())) {
			dataDictionaryService.addDataTyp(dataTyp);
		}
		
		//若dataTyp中的flag为update，则是修改
		if ("update".equals(dataTyp.getFlag())) {
			dataDictionaryService.updateDataTyp(dataTyp);
		}
		
		//若dataTyp中的flag为delete，则是删除
		if ("delete".equals(dataTyp.getFlag())) {
			dataDictionaryService.deleteDataTyp(dataTyp);
		}
		
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 描述：检验同类型中编码是否已存在
	 * @author 马振
	 * 创建时间：2015-5-22 下午3:32:36
	 * @param modelMap
	 * @param dataTyp
	 * @param session
	 * @return
	 */
	@SuppressWarnings("unused")
	@RequestMapping(value = "/checkVcode")
    @ResponseBody
    private String checkVcode(ModelMap modelMap,DataTyp dataTyp,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
    	return dataDictionaryService.checkVcode(dataTyp);
    }
	
	/**
	 * 描述：检验同类型中排序是否已存在
	 * @author 马振
	 * 创建时间：2015-5-22 下午4:38:54
	 * @param modelMap
	 * @param dataTyp
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@SuppressWarnings("unused")
	@RequestMapping(value = "/checkIsortno")
    @ResponseBody
    private String checkIsortno(ModelMap modelMap,DataTyp dataTyp,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
    	return dataDictionaryService.checkIsortno(dataTyp);
    }
}