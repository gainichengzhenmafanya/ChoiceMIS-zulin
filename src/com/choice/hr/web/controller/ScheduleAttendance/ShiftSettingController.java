package com.choice.hr.web.controller.ScheduleAttendance;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.hr.constants.ScheduleAttendance.ShiftSettingConstants;
import com.choice.hr.domain.ScheduleAttendance.Shift;
import com.choice.hr.service.ScheduleAttendance.ShiftSettingService;
import com.choice.misboh.domain.common.CommonMethod;
import com.choice.orientationSys.constants.StringConstant;
import com.choice.orientationSys.util.Page;

/**
 * 描述：排班考勤——班次设置
 * @author 马振
 * 创建时间：2015-5-12 上午9:07:25
 */
@Controller
@RequestMapping(value = "shiftSetting")
public class ShiftSettingController {
	
	@Autowired
	private ShiftSettingService shiftSettingService;
	
	/**
	 * 描述：班次列表
	 * @author 马振
	 * 创建时间：2015-5-26 下午4:58:00
	 * @param modelMap
	 * @param shift
	 * @param commonMethod
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/listShiftSetting")
	public ModelAndView listShiftSetting(ModelMap modelMap, Shift shift,CommonMethod commonMethod, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("listShift", shiftSettingService.listShiftSetting(shift, session));
		return new ModelAndView(ShiftSettingConstants.LIST_SHIFT, modelMap);
	}
	
	/**
	 * 描述：跳转到新增页面
	 * @author 马振
	 * 创建时间：2015-5-26 下午5:01:04
	 * @param modelMap
	 * @param shift
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/addData")
	public ModelAndView addData(ModelMap modelMap, Shift shift, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("shift", shiftSettingService.getMaxVcodeAndIsortno(shift));
		return new ModelAndView(ShiftSettingConstants.ADD_SHIFT, modelMap);
	}
	
	/**
	 * 描述：跳转到修改页面
	 * @author 马振
	 * 创建时间：2015-5-26 下午5:02:29
	 * @param modelMap
	 * @param shift
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/updateData")
	public ModelAndView updateData(ModelMap modelMap, Shift shift, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("shift", shiftSettingService.findShiftByPk(shift));
		return new ModelAndView(ShiftSettingConstants.UPDATE_SHIFT, modelMap);
	}
	
	/**
	 * 描述：保存编辑的数据
	 * @author 马振
	 * 创建时间：2015-5-26 下午5:01:47
	 * @param modelMap
	 * @param shift
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/saveData")
	public ModelAndView saveData(ModelMap modelMap, Shift shift, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源

		//若shift中的flag为add，则是新增
		if ("add".equals(shift.getFlag())) {
			shiftSettingService.addshift(shift,session);
		}
		
		//若shift中的flag为update，则是修改
		if ("update".equals(shift.getFlag())) {
			shiftSettingService.updateshift(shift);
		}
		
		//若shift中的flag为delete，则是删除
		if ("delete".equals(shift.getFlag())) {
			shiftSettingService.deleteshift(shift);
		}
		
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	

	/**
	 * 描述：班次弹出框
	 * @author 马振
	 * 创建时间：2015-5-27 上午8:42:24
	 * @param modelMap
	 * @param session
	 * @param pk_store
	 * @param single
	 * @param callBack
	 * @param domId
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toChooseShift")
	public ModelAndView toChooseNation(ModelMap modelMap,HttpSession session,Shift shift,String single,String callBack,String domId,Page page) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("listShift", shiftSettingService.listShiftSetting(shift,session));
		modelMap.put("pageobj", page);
		modelMap.put("callBack", callBack);
		modelMap.put("domId", domId);
		modelMap.put("single", single);
		modelMap.put("pk_shift", shift.getPk_shift());
		return new ModelAndView(ShiftSettingConstants.CHOOSESHIFT, modelMap);
	}
	
	/**
	 * 描述：检验编码是否已存在
	 * @author 马振
	 * 创建时间：2015-5-26 下午5:01:28
	 * @param modelMap
	 * @param shift
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/checkVcode")
    @ResponseBody
    private String checkVcode(ModelMap modelMap,Shift shift,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
    	return shiftSettingService.checkVcode(shift);
    }
}