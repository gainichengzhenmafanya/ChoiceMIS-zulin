package com.choice.hr.web.controller.ScheduleAttendance;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.hr.constants.ScheduleAttendance.AttendanceSummaryConstants;
import com.choice.hr.domain.DataDictionary.DataTyp;
import com.choice.hr.domain.ScheduleAttendance.AttendanceSummary;
import com.choice.hr.domain.ScheduleAttendance.ClassTableSetting;
import com.choice.hr.service.PersonnelManagement.StaffListService;
import com.choice.hr.service.ScheduleAttendance.AttendanceSummaryService;
import com.choice.hr.service.ScheduleAttendance.ClassTableSettingService;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.service.common.CommonMISBOHService;

/**
 * 描述：排班考勤——考勤工时汇总
 * @author 马振
 * 创建时间：2015-6-24 上午11:26:15
 */
@Controller
@RequestMapping(value = "attendanceSummary")
public class AttendanceSummaryController {
	
	@Autowired
	private AttendanceSummaryService attendanceSummaryService;
	
	@Autowired
	private CommonMISBOHService commonMISBOHService; 

	@Autowired
	private ClassTableSettingService classTableSettingService;
	
	@Autowired
	private StaffListService staffListService;
	
	/**
	 * 描述：考勤工时汇总列表
	 * @author 马振
	 * 创建时间：2015-6-24 上午11:27:44
	 * @param modelMap
	 * @param crewScheduling
	 * @param shift
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/listAttendanceSummary")
	public ModelAndView listAttendanceSummary(ModelMap modelMap, AttendanceSummary attendanceSummary, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		//若开始日期为空，则赋值当前日期
		if (ValueUtil.IsEmpty(attendanceSummary.getVbdat())) {
			attendanceSummary.setVbdat(DateJudge.getNowDate());
		}
		
		//若结束日期为空，则赋值当前日期
		if (ValueUtil.IsEmpty(attendanceSummary.getVedat())) {
			attendanceSummary.setVedat(DateJudge.getNowDate());
		}
		
		//若门店主键为空则，赋值当前登录门店
		if (ValueUtil.IsEmpty(attendanceSummary.getPk_store())) {
			attendanceSummary.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		}
		
		modelMap.put("listAttendanceSummary", attendanceSummaryService.listAttendanceSummary(attendanceSummary));
		modelMap.put("vbdat", attendanceSummary.getVbdat());
		modelMap.put("vedat", attendanceSummary.getVedat());
		modelMap.put("pk_store", commonMISBOHService.getPkStore(session).getPk_store());
		return new ModelAndView(AttendanceSummaryConstants.LIST_ATTENDANCESUMMARY, modelMap);
	}
	
	/**
	 * 描述：查询理论工时
	 * @author 马振
	 * 创建时间：2015-6-24 下午3:08:28
	 * @param modelMap
	 * @param classTableSetting
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/listClassTableSetting")
	public ModelAndView listClassTableSetting(ModelMap modelMap,ClassTableSetting classTableSetting, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		//若日期为空，则赋值当前日期
		if (ValueUtil.IsEmpty(classTableSetting.getDworkdate())) {
			classTableSetting.setDworkdate(DateJudge.getNowDate());
		}
		
		//若门店为空，则赋值当前登陆门店
		if (ValueUtil.IsEmpty(classTableSetting.getPk_store())) {
			classTableSetting.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		}
		
		DataTyp dataTyp = new DataTyp();
	
		//工作站
		dataTyp.setVtyp(DDT.DATATYPE_TYP25);
		dataTyp.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		modelMap.put("listDataTyp", staffListService.listDataTyp(dataTyp));
		modelMap.put("listTime", classTableSettingService.listTrTime(classTableSetting).getListTime());
		modelMap.put("listInt", classTableSettingService.listTrTime(classTableSetting).getListInt());
		modelMap.put("listClassTableSetting", classTableSettingService.listClassTableSetting(classTableSetting));
		modelMap.put("flag", DateJudge.timeCompare(DateJudge.getNowDate(), classTableSetting.getDworkdate()));
		modelMap.put("dworkdate", classTableSetting.getDworkdate());
		modelMap.put("pk_store", classTableSetting.getPk_store());
		return new ModelAndView(AttendanceSummaryConstants.LIST_THEORY, modelMap);
	}
}