package com.choice.hr.web.controller.ScheduleAttendance;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.hr.constants.ScheduleAttendance.AttendanceRecordConstants;
import com.choice.hr.domain.ScheduleAttendance.AttendanceRecord;
import com.choice.hr.domain.ScheduleAttendance.WorkAttendance;
import com.choice.hr.service.ScheduleAttendance.AttendanceRecordService;
import com.choice.hr.service.ScheduleAttendance.EmpTimeService;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.domain.store.Store;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.scm.domain.Positn;
import com.choice.scm.service.PositnService;

/**
 * 描述：考勤记录
 * @author 马振
 * 创建时间：2015-6-2 上午10:10:28
 */
@Controller
@RequestMapping(value = "attendanceRecord")
public class AttendanceRecordController {
	
	@Autowired
	private AttendanceRecordService attendanceRecordService;
	
    private Store store;
    
    private Positn positn;
    
    /**
     * 获取公共方法SERVICE
     */
    @Autowired
	private CommonMISBOHService commonMISBOHService;
    
    /**
     * 获取仓位公共方法
     */
    @Autowired
    private PositnService positnService;
	@Autowired
	private EmpTimeService empTimeService;
    
	/**
	 * 描述：考勤记录左侧树
	 * @author 马振
	 * 创建时间：2015-6-2 上午10:17:32
	 * @param modelMap
	 * @param attendanceRecord
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/treeAttendanceRecord")
	public ModelAndView treeAttendanceRecord(ModelMap modelMap, AttendanceRecord attendanceRecord, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("treeAttendanceRecord", attendanceRecordService.treeAttendanceRecord(attendanceRecord,session));
		modelMap.put("pk_id", attendanceRecord.getPk_hrdept());
		modelMap.put("flag", attendanceRecord.getFlag());
		return new ModelAndView(AttendanceRecordConstants.TREE_ATTENDANCERECORD, modelMap);
	}
	
	/**
	 * 描述：考勤记录列表
	 * @author 马振
	 * 创建时间：2015-6-2 上午10:19:00
	 * @param modelMap
	 * @param attendanceRecord
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/listAttendanceRecord")
	public ModelAndView listAttendanceRecord(ModelMap modelMap, AttendanceRecord attendanceRecord, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String url = "";	//跳转路径
		String flag = "";
		
		//若开始日期为空，则赋值当前日期
		if (ValueUtil.IsEmpty(attendanceRecord.getBdat())) {
			attendanceRecord.setBdat(DateJudge.getNowDate());
		}
		
		//若结束日期为空，则赋值当前日期
		if (ValueUtil.IsEmpty(attendanceRecord.getEdat())) {
			attendanceRecord.setEdat(DateJudge.getNowDate());
		}
		
		//创建list接收对象
		List<AttendanceRecord> list = new ArrayList<AttendanceRecord>();
		
		if ("record".equals(attendanceRecord.getFlag())) {
			list = attendanceRecordService.listAttendanceRecord(attendanceRecord, session);
			url = AttendanceRecordConstants.LIST_ATTENDANCERECORD;
			flag = "record";
		}
		
		if ("count".equals(attendanceRecord.getFlag())) {
			list = attendanceRecordService.listCount(attendanceRecord, session);
			url = AttendanceRecordConstants.LIST_COUNT;
			flag = "count";
		}
		
		modelMap.put("listAttendance", list);
		modelMap.put("flag", flag);
		modelMap.put("bdat", attendanceRecord.getBdat());
		modelMap.put("edat", attendanceRecord.getEdat());
		modelMap.put("pk_hrdept", attendanceRecord.getPk_hrdept());
		return new ModelAndView(url, modelMap);
	}
	
	
	/**
	 * 员工考勤工时
	 * @author 文清泉
	 * @param 2015年6月19日 上午10:51:52
	 * @param modelMap
	 * @param workAttendance
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/listWorkAttendance")
	public ModelAndView listWorkAttendance(ModelMap modelMap, WorkAttendance workAttendance, HttpSession session) throws CRUDException{
		setPositn(session);
		workAttendance.setPK_STORE(store.getPk_store());
		workAttendance.setVSCODE(positn.getCode());
		workAttendance.setVTYP(DDT.DATATYPE_TYP18);
		workAttendance.setVDCODE("'"+DDT.HR_SY+"','"+DDT.HR_ZZ+"'");
		if(workAttendance.getDAT() == null || workAttendance.getDAT().equals("")){
			workAttendance.setDAT(DateJudge.YYYY_MM_DD.format(new Date()));
		}
		String VNAME = workAttendance.getVNAME();
		String VKQCARDNO = workAttendance.getVKQCARDNO();
		List<WorkAttendance> list = attendanceRecordService.listWorkAttendance(workAttendance);
		workAttendance.setVNAME(VNAME);
		workAttendance.setVKQCARDNO(VKQCARDNO);
		workAttendance.setSHOWLENGTH(FormatDouble.MoneyBit);
		String pattern = "#.";
		for (int k = 0;k<FormatDouble.getMoneyBit();k++) {
			pattern +="0";
		}
		modelMap.put("list", list);
		modelMap.put("spattern", pattern);
		// 验证是否查询历史数据  历史数据不可编辑
		if(DateJudge.timeCompare(workAttendance.getDAT(),DateJudge.YYYY_MM_DD.format(new Date())) && !DateJudge.YYYY_MM_DD.format(new Date()).equals(workAttendance.getDAT())){
			modelMap.put("edit", 0);
		}else{
			modelMap.put("edit", 1);
		}
		
		modelMap.put("getWorkHour", attendanceRecordService.getWorkHour(workAttendance));
		modelMap.put("flag", workAttendance.getFlag());
		return new ModelAndView(AttendanceRecordConstants.LISTWORKATTENDANCE, modelMap);
	}
	
	/**
	 * 员工工时考勤保存
	 * @author 文清泉
	 * @param 2015年6月19日 下午1:15:01
	 * @param modelMap
	 * @param workAttendance
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/saveWorkAttendance")
	public ModelAndView saveWorkAttendance(ModelMap modelMap, WorkAttendance workAttendance, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		setPositn(session);
		workAttendance.setPK_STORE(store.getPk_store());
		workAttendance.setVSCODE(positn.getCode());
		attendanceRecordService.saveWorkAttendance(workAttendance);
		return new ModelAndView(AttendanceRecordConstants.LISTWORKATTENDANCE, modelMap);
	}
	
	/**
	 * 员工工时考勤计算
	 * @author 文清泉
	 * @param 2015年6月19日 下午1:15:01
	 * @param modelMap
	 * @param workAttendance
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/calWorkAttendance")
	public ModelAndView calWorkAttendance(ModelMap modelMap, WorkAttendance workAttendance, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		setPositn(session);
		empTimeService.updateEmpTime(positn.getCode(),workAttendance.getDAT(),0);
		empTimeService.calEmpTime(positn.getCode());
		return new ModelAndView(AttendanceRecordConstants.LISTWORKATTENDANCE, modelMap);
	}
	
    /**
     * 读取SESSION数据
     * @author 文清泉
     * @param 2015年1月1日 上午10:59:13
     * @param session
     */
    public void setPositn(HttpSession session){
    	Positn thePositn = (Positn)session.getAttribute("accountPositn");
		try {
		    store = new Store();
	    	store.setVcode(thePositn.getCode());
	    	store = commonMISBOHService.getStore(store);
	    	positn = positnService.findPositnByCode(thePositn);
		} catch (CRUDException e) {
		    e.printStackTrace();
		}
    }
}