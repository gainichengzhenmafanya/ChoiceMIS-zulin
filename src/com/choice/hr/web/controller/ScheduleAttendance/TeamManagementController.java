package com.choice.hr.web.controller.ScheduleAttendance;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.hr.constants.ScheduleAttendance.TeamManagementConstants;
import com.choice.hr.domain.PersonnelManagement.Employee;
import com.choice.hr.domain.ScheduleAttendance.Team;
import com.choice.hr.service.ScheduleAttendance.TeamManagementService;
import com.choice.misboh.domain.common.CommonMethod;
import com.choice.orientationSys.constants.StringConstant;

/**
 * 描述：排班考勤
 * @author 马振
 * 创建时间：2015-5-25 下午1:24:56
 */
@Controller
@RequestMapping(value = "scheduleAttendance")
public class TeamManagementController {
	
	@Autowired
	private TeamManagementService teamManagementService;
	
	/**
	 * 描述：班组左侧树
	 * @author 马振
	 * 创建时间：2015-5-25 下午2:17:00
	 * @param modelMap
	 * @param team
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/treeTeamManagement")
	public ModelAndView treeTeamManagement(ModelMap modelMap, Team team, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("treeTeamManagement", teamManagementService.treeTeamManagement(team,session));
		modelMap.put("pk_id", team.getPk_hrdept());
		return new ModelAndView(TeamManagementConstants.TREE_TEAMMANAGEMENT, modelMap);
	}
	
	/**
	 * 描述：班组列表显示
	 * @author 马振
	 * 创建时间：2015-5-25 下午2:23:27
	 * @param modelMap
	 * @param team
	 * @param commonMethod
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/listTeamManagement")
	public ModelAndView listEmployee(ModelMap modelMap, Team team,CommonMethod commonMethod, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("listTeam", teamManagementService.listTeamManagement(team, session));
		modelMap.put("pk_hrdept", team.getPk_hrdept());
		return new ModelAndView(TeamManagementConstants.LIST_TEAMMANAGEMENT, modelMap);
	}
	
	/**
	 * 描述：新增班组
	 * @author 马振
	 * 创建时间：2015-5-25 下午4:16:26
	 * @param modelMap
	 * @param team
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/addData")
	public ModelAndView addData(ModelMap modelMap, Team team, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("pk_hrdept", team.getPk_hrdept());
		modelMap.put("team", teamManagementService.getMaxVcodeAndIsortno(team));
		return new ModelAndView(TeamManagementConstants.ADDTEAM, modelMap);
	}
	
	/**
	 * 描述：修改数据
	 * @author 马振
	 * 创建时间：2015-5-22 下午3:14:54
	 * @param modelMap
	 * @param Team
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/updateData")
	public ModelAndView updateData(ModelMap modelMap, Team Team, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("team", teamManagementService.findTeamByPk(Team));
		return new ModelAndView(TeamManagementConstants.UPDATETEAM, modelMap);
	}
	
	/**
	 * 描述：保存数据
	 * @author 马振
	 * 创建时间：2015-5-22 下午3:33:47
	 * @param modelMap
	 * @param Team
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/saveData")
	public ModelAndView saveData(ModelMap modelMap, Team Team, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源

		//若Team中的flag为add，则是新增
		if ("add".equals(Team.getFlag())) {
			teamManagementService.addTeam(Team,session);
		}
		
		//若Team中的flag为update，则是修改
		if ("update".equals(Team.getFlag())) {
			teamManagementService.updateTeam(Team);
		}
		
		//若Team中的flag为delete，则是删除
		if ("delete".equals(Team.getFlag())) {
			teamManagementService.deleteTeam(Team);
		}
		
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 描述：检验同类型中编码是否已存在
	 * @author 马振
	 * 创建时间：2015-5-22 下午3:32:36
	 * @param modelMap
	 * @param Team
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/checkVcode")
    @ResponseBody
    private String checkVcode(ModelMap modelMap,Team Team,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
    	return teamManagementService.checkVcode(Team);
    }
	
	/**
	 * 描述：检验同类型中排序是否已存在
	 * @author 马振
	 * 创建时间：2015-5-22 下午4:38:54
	 * @param modelMap
	 * @param Team
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/checkIsortno")
    @ResponseBody
    private String checkIsortno(ModelMap modelMap,Team Team,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
    	return teamManagementService.checkIsortno(Team);
    }
	
	/**
	 * 描述：将为员工分配班组
	 * @author 马振
	 * 创建时间：2015-5-27 下午6:01:17
	 * @param modelMap
	 * @param employee
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/addTeamToEmployee")
    @ResponseBody
    private void addTeamToEmployee(ModelMap modelMap,Employee employee,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
    	teamManagementService.addTeamToEmployee(employee);
    }
}