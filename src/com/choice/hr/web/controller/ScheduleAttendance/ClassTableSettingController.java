package com.choice.hr.web.controller.ScheduleAttendance;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.hr.constants.ScheduleAttendance.ClassTableSettingConstants;
import com.choice.hr.domain.DataDictionary.DataTyp;
import com.choice.hr.domain.ScheduleAttendance.ClassTableSetting;
import com.choice.hr.service.PersonnelManagement.StaffListService;
import com.choice.hr.service.ScheduleAttendance.ClassTableSettingService;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.MisUtil;
import com.choice.misboh.commonutil.ReadReport;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.commonutil.excel.GenerateExcel;
import com.choice.misboh.constants.reportMis.MisStringConstant;
import com.choice.misboh.domain.reportMis.ReportObject;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.orientationSys.constants.StringConstant;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;

/**
 * 描述：排班考勤——班表设定
 * @author 马振
 * 创建时间：2015-6-16 上午9:04:32
 */
@Controller
@RequestMapping(value = "classTableSetting")
public class ClassTableSettingController {
	
	@Autowired
	private ClassTableSettingService classTableSettingService;
	
	@Autowired
	private StaffListService staffListService;
	
	@Autowired
	private CommonMISBOHService commonMISBOHService; 

	@Autowired
	protected DictColumns dictColumns;

	@Autowired
	protected DictColumnsService dictColumnsService;
	
	/**
	 * 描述：跳转到班表设置页面
	 * @author 马振
	 * 创建时间：2015-6-16 下午3:01:27
	 * @param modelMap
	 * @param classTableSetting
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/listClassTableSetting")
	public ModelAndView listClassTableSetting(ModelMap modelMap,ClassTableSetting classTableSetting, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		//若日期为空，则赋值当前日期
		if (ValueUtil.IsEmpty(classTableSetting.getDworkdate())) {
			classTableSetting.setDworkdate(DateJudge.getNowDate());
		}
		
		//若门店为空，则赋值当前登陆门店
		if (ValueUtil.IsEmpty(classTableSetting.getPk_store())) {
			classTableSetting.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		}
		
		DataTyp dataTyp = new DataTyp();
	
		//工作站
		dataTyp.setVtyp(DDT.DATATYPE_TYP25);
		dataTyp.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		modelMap.put("listDataTyp", staffListService.listDataTyp(dataTyp));
		modelMap.put("listTime", classTableSettingService.listTrTime(classTableSetting).getListTime());
		modelMap.put("listInt", classTableSettingService.listTrTime(classTableSetting).getListInt());
		modelMap.put("listClassTableSetting", classTableSettingService.listClassTableSetting(classTableSetting));
		modelMap.put("flag", DateJudge.timeCompare(DateJudge.getNowDate(), classTableSetting.getDworkdate()));
		modelMap.put("dworkdate", classTableSetting.getDworkdate());
		modelMap.put("pk_store", classTableSetting.getPk_store());
		return new ModelAndView(ClassTableSettingConstants.LIST_CLASSTABLESETTING, modelMap);
	}
	
	/**
	 * 描述：设置人数
	 * @author 马振
	 * 创建时间：2015-6-16 下午3:01:52
	 * @param modelMap
	 * @param classTableSetting
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/addPeopleNumber")
	public ModelAndView addPeopleNumber(ModelMap modelMap,ClassTableSetting classTableSetting, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("totalhour", classTableSetting.getTotalhour());
		modelMap.put("td", classTableSetting.getTd());
		return new ModelAndView(ClassTableSettingConstants.ADD_PEOPLENUMBER, modelMap);
	}
	
	/**
	 * 描述：保存数据
	 * @author 马振
	 * 创建时间：2015-6-16 下午5:14:15
	 * @param modelMap
	 * @param classTableSetting
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/saveData")
	public ModelAndView saveData(ModelMap modelMap,ClassTableSetting classTableSetting, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		classTableSettingService.saveData(classTableSetting);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 描述：ajax根据门店和日期获取数据
	 * @author 马振
	 * 创建时间：2015-6-18 上午10:18:51
	 * @param modelMap
	 * @param classTableSetting
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/getTableSetting")
    @ResponseBody
    private List<ClassTableSetting> getTableSetting(ModelMap modelMap,ClassTableSetting classTableSetting,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
    	//若日期为空，则赋值当前日期
    	if (ValueUtil.IsEmpty(classTableSetting.getDworkdate())) {
    		classTableSetting.setDworkdate(DateJudge.getNowDate());
    	}
    			
    	//若门店为空，则赋值当前登陆门店
    	if (ValueUtil.IsEmpty(classTableSetting.getPk_store())) {
    		classTableSetting.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
    	}
    	
    	return classTableSettingService.listClassTableSetting(classTableSetting);
    }
	
	/**
	 * 描述：跳转到选择日期页面
	 * @author 马振
	 * 创建时间：2015-6-18 下午2:48:05
	 * @param modelMap
	 * @param classTableSetting
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/selectDate")
	public ModelAndView selectDate(ModelMap modelMap,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("dworkdate", DateJudge.getNowDate());
		return new ModelAndView(ClassTableSettingConstants.SELECTDATE, modelMap);
	}
	
	/**
	 * 描述：保存复制的数据(所选日期没数据时的方法)
	 * @author 马振
	 * 创建时间：2015-6-23 上午9:15:06
	 * @param modelMap
	 * @param classTableSetting
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/copy")
	public ModelAndView copy(ModelMap modelMap,ClassTableSetting classTableSetting, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		classTableSettingService.copyData(classTableSetting);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 描述：根据日期、门店、工作站、开始时间删除数据
	 * @author 马振
	 * 创建时间：2015-6-18 下午5:13:57
	 * @param modelMap
	 * @param classTableSetting
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/deleteData")
	public ModelAndView deleteData(ModelMap modelMap,ClassTableSetting classTableSetting, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		classTableSettingService.deleteData(classTableSetting);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 描述：生成文件，反馈文件输出流
	 * @author 马振
	 * 创建时间：2015-6-19 上午10:29:17
	 * @param fileName
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/createExcel")
	public void exportReport(HttpServletResponse response,HttpServletRequest request,HttpSession session,ClassTableSetting classTableSetting,String reportName,String headers) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		classTableSetting.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		
		//设置分页，查询所有数据
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ReadReport.getReportName(MisStringConstant.class, reportName));
		
		//获取并执行查询方法，获取查询结果
		ReportObject<Map<String,Object>> result = MisUtil.execMethod(classTableSettingService, reportName, classTableSetting);
		if(null != result.getFooter())
			result.getRows().addAll(result.getFooter());
		if(null != headers)
			new GenerateExcel<Map<String, Object>>().creatReport(ReadReport.getReportNameCN(MisStringConstant.class, reportName),request,response,
					result.getRows(),ReadReport.getReportNameCN(MisStringConstant.class, reportName),
					null,headers, ReadReport.getReportExcel(MisStringConstant.class, reportName));
		else
			new GenerateExcel<Map<String, Object>>().creatReport(ReadReport.getReportNameCN(MisStringConstant.class, reportName),request,response, result.getRows(),
					ReadReport.getReportNameCN(MisStringConstant.class, reportName), 
					dictColumnsService.listDictColumnsByAccount(dictColumns, 
							ReadReport.getDefaultHeader(MisStringConstant.class, reportName)),
					headers, ReadReport.getReportExcel(MisStringConstant.class, reportName));
	}
}