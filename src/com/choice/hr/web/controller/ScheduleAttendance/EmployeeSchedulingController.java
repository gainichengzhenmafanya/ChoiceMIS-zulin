package com.choice.hr.web.controller.ScheduleAttendance;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.hr.constants.ScheduleAttendance.EmployeeSchedulingConstants;
import com.choice.hr.domain.ScheduleAttendance.EmployeeScheduling;
import com.choice.hr.service.ScheduleAttendance.EmployeeSchedulingService;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.orientationSys.constants.StringConstant;

/**
 * 描述：排班考勤——员工排班
 * @author 马振
 * 创建时间：2015-5-28 上午8:48:53
 */
@Controller
@RequestMapping(value = "employeeScheduling")
public class EmployeeSchedulingController {
	
	@Autowired
	private EmployeeSchedulingService employeeSchedulingService;
	
	/**
	 * 描述：员工排班左侧树
	 * @author 马振
	 * 创建时间：2015-5-28 上午8:54:05
	 * @param modelMap
	 * @param EmployeeScheduling
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/treeEmployeeScheduling")
	public ModelAndView treeEmployeeScheduling(ModelMap modelMap, EmployeeScheduling EmployeeScheduling, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("treeEmployeeScheduling", employeeSchedulingService.treeEmployeeScheduling(EmployeeScheduling, session));
		return new ModelAndView(EmployeeSchedulingConstants.TREE_EMPLOYEESCHEDULING, modelMap);
	}
	
	/**
	 * 描述：员工排班列表
	 * @author 马振
	 * 创建时间：2015-5-28 上午8:54:26
	 * @param modelMap
	 * @param EmployeeScheduling
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/listEmployeeScheduling")
	public ModelAndView listEmployeeScheduling(ModelMap modelMap, EmployeeScheduling EmployeeScheduling, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源

		//若session中存在vbdat，则赋值为session中的日期；否则判断EmployeeScheduling中的开始日期是否为空，若为空则赋值当前日期
		if (ValueUtil.IsNotEmpty(session.getAttribute("vbdat"))) {
			EmployeeScheduling.setVbdat(session.getAttribute("vbdat").toString());
		} else {
			if (ValueUtil.IsEmpty(EmployeeScheduling.getVbdat())) {
				EmployeeScheduling.setVbdat(DateJudge.getNowDate());
			}
		}

		//若session中存在vedat，则赋值为session中的日期；否则判断EmployeeScheduling中的结束日期是否为空，若为空则赋值当前日期
		if (ValueUtil.IsNotEmpty(session.getAttribute("vedat"))) {
			EmployeeScheduling.setVedat(session.getAttribute("vedat").toString());
		} else {
			if (ValueUtil.IsEmpty(EmployeeScheduling.getVedat())) {
				EmployeeScheduling.setVedat(DateJudge.getNowDate());
			}
		}
		
		if (ValueUtil.IsNotEmpty(session.getAttribute("pk_employee"))) {
			EmployeeScheduling.setPk_employee(session.getAttribute("pk_employee").toString());
		}

		if (ValueUtil.IsNotEmpty(session.getAttribute("pk_hrdept"))) {
			EmployeeScheduling.setPk_hrdept(session.getAttribute("pk_hrdept").toString());
		}

		if (ValueUtil.IsNotEmpty(session.getAttribute("pk_team"))) {
			EmployeeScheduling.setPk_team(session.getAttribute("pk_team").toString());
		}
		
		session.removeAttribute("pk_employee");
		session.removeAttribute("pk_hrdept");
		session.removeAttribute("pk_team");
		session.removeAttribute("vbdat");
		session.removeAttribute("vedat");
		
		modelMap.put("listShift", employeeSchedulingService.listEmployeeScheduling(EmployeeScheduling, session));
		modelMap.put("count", employeeSchedulingService.listEmployeeScheduling(EmployeeScheduling, session).size());
		modelMap.put("pk_hrdept", EmployeeScheduling.getPk_hrdept());
		modelMap.put("pk_team", EmployeeScheduling.getPk_team());
		modelMap.put("pk_employee", EmployeeScheduling.getPk_employee());
		modelMap.put("pk_store", EmployeeScheduling.getPk_store());
		modelMap.put("vbdat", EmployeeScheduling.getVbdat());
		modelMap.put("vedat", EmployeeScheduling.getVedat());
		modelMap.put("vcode", EmployeeScheduling.getVcode());
		return new ModelAndView(EmployeeSchedulingConstants.LIST_EMPLOYEESCHEDULING, modelMap);
	}
	
	/**
	 * 描述：保存员工排班
	 * @author 马振
	 * 创建时间：2015-5-28 上午8:54:40
	 * @param modelMap
	 * @param EmployeeScheduling
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/saveData")
	public ModelAndView saveData(ModelMap modelMap, EmployeeScheduling EmployeeScheduling, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		session.setAttribute("pk_employee", EmployeeScheduling.getPk_employee());
		session.setAttribute("pk_team", EmployeeScheduling.getPk_team());
		session.setAttribute("pk_hrdept", EmployeeScheduling.getPk_hrdept());
		session.setAttribute("vbdat", EmployeeScheduling.getVbdat());
		session.setAttribute("vedat", EmployeeScheduling.getVedat());
		employeeSchedulingService.saveData(EmployeeScheduling, session);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 描述：删除员工排班
	 * @author 马振
	 * 创建时间：2015-6-10 下午3:27:03
	 * @param modelMap
	 * @param employeeScheduling
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/deleteData")
	public ModelAndView deleteData(ModelMap modelMap, EmployeeScheduling employeeScheduling, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		employeeSchedulingService.deleteEmployee(employeeScheduling, session);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
}