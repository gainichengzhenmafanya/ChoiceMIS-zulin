package com.choice.hr.web.controller.ScheduleAttendance;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.hr.constants.ScheduleAttendance.CrewSchedulingConstants;
import com.choice.hr.domain.ScheduleAttendance.CrewScheduling;
import com.choice.hr.service.ScheduleAttendance.CrewSchedulingService;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.orientationSys.constants.StringConstant;

/**
 * 描述：排班考勤——班组排班
 * @author 马振
 * 创建时间：2015-5-26 下午3:53:17
 */
@Controller
@RequestMapping(value = "crewScheduling")
public class CrewSchedulingController {
	
	@Autowired
	private CrewSchedulingService crewSchedulingService;
	
	/**
	 * 描述：班组排班左侧树
	 * @author 马振
	 * 创建时间：2015-5-26 下午4:17:22
	 * @param modelMap
	 * @param crewScheduling
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/treeCrewScheduling")
	public ModelAndView treeCrewScheduling(ModelMap modelMap, CrewScheduling crewScheduling, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("treeCrewScheduling", crewSchedulingService.treeCrewScheduling(crewScheduling, session));
		return new ModelAndView(CrewSchedulingConstants.TREE_CREWSCHEDULING, modelMap);
	}
	
	/**
	 * 描述：班组排班列表
	 * @author 马振
	 * 创建时间：2015-5-26 下午4:33:03
	 * @param modelMap
	 * @param crewScheduling
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/listCrewScheduling")
	public ModelAndView listCrewScheduling(ModelMap modelMap, CrewScheduling crewScheduling, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		//若开始日期为空，则赋值当前日期
		if (ValueUtil.IsEmpty(crewScheduling.getVbdat())) {
			crewScheduling.setVbdat(DateJudge.getNowDate());
		}
				
		//若结束日期为空，则赋值当前日期
		if (ValueUtil.IsEmpty(crewScheduling.getVedat())) {
			crewScheduling.setVedat(DateJudge.getNowDate());
		}
		
		if (ValueUtil.IsNotEmpty(session.getAttribute("pk_team"))) {
			crewScheduling.setPk_team(session.getAttribute("pk_team").toString());
		}
		
		session.removeAttribute("pk_team");
		
		modelMap.put("listShift", crewSchedulingService.listCrewScheduling(crewScheduling, session));
		modelMap.put("count", crewSchedulingService.listCrewScheduling(crewScheduling, session).size());
		modelMap.put("pk_hrdept", crewScheduling.getPk_hrdept());
		modelMap.put("pk_team", crewScheduling.getPk_team());
		modelMap.put("pk_store", crewScheduling.getPk_store());
		modelMap.put("vbdat", crewScheduling.getVbdat());
		modelMap.put("vedat", crewScheduling.getVedat());
		modelMap.put("vcode", crewScheduling.getVcode());
		return new ModelAndView(CrewSchedulingConstants.LIST_CREWSCHEDULING, modelMap);
	}
	
	/**
	 * 描述：保存班次排班
	 * @author 马振
	 * 创建时间：2015-5-27 上午9:48:42
	 * @param modelMap
	 * @param crewScheduling
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/saveData")
	public ModelAndView saveData(ModelMap modelMap, CrewScheduling crewScheduling, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		crewSchedulingService.saveData(crewScheduling, session);
		session.setAttribute("pk_team", crewScheduling.getPk_team());
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 描述：删除班组排班
	 * @author 马振
	 * 创建时间：2015-6-10 下午3:41:43
	 * @param modelMap
	 * @param crewScheduling
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/deleteData")
	public ModelAndView deleteData(ModelMap modelMap, CrewScheduling crewScheduling, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		crewSchedulingService.deleteCrew(crewScheduling, session);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
}