package com.choice.hr.web.controller.ScheduleAttendance;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.hr.constants.ScheduleAttendance.QuerySchedulingConstants;
import com.choice.hr.domain.ScheduleAttendance.CrewScheduling;
import com.choice.hr.service.ScheduleAttendance.QuerySchedulingService;

/**
 * 描述：排班考勤——排班查询
 * @author 马振
 * 创建时间：2015-5-28 下午1:11:55
 */
@Controller
@RequestMapping(value = "queryScheduling")
public class QuerySchedulingController {
	
	@Autowired
	private QuerySchedulingService querySchedulingService;
	
	/**
	 * 描述：排班查询左侧树
	 * @author 马振
	 * 创建时间：2015-5-28 下午1:12:59
	 * @param modelMap
	 * @param crewScheduling
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/treeQueryScheduling")
	public ModelAndView treeCrewScheduling(ModelMap modelMap, CrewScheduling crewScheduling, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("treeQueryScheduling", querySchedulingService.treeQueryScheduling(crewScheduling, session));
		return new ModelAndView(QuerySchedulingConstants.TREE_QUERYSCHEDULING, modelMap);
	}
	
	/**
	 * 描述：排班查询列表
	 * @author 马振
	 * 创建时间：2015-5-28 下午2:31:04
	 * @param modelMap
	 * @param crewScheduling
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/listQueryScheduling")
	public ModelAndView listCrewScheduling(ModelMap modelMap, CrewScheduling crewScheduling, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("listShift", querySchedulingService.listQuery(crewScheduling, session));
		modelMap.put("listDate", querySchedulingService.listDate(crewScheduling));
		modelMap.put("count", querySchedulingService.listDate(crewScheduling).size());
		modelMap.put("pk_hrdept", crewScheduling.getPk_hrdept());
		modelMap.put("pk_team", crewScheduling.getPk_team());
		modelMap.put("pk_store", crewScheduling.getPk_store());
		modelMap.put("vbdat", crewScheduling.getVbdat());
		modelMap.put("vedat", crewScheduling.getVedat());
		modelMap.put("vcode", crewScheduling.getVcode());
		return new ModelAndView(QuerySchedulingConstants.LIST_QUERYSCHEDULING, modelMap);
	}
}