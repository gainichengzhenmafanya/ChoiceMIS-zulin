package com.choice.hr.web.controller.ScheduleAttendance;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.hr.constants.ScheduleAttendance.DailyScheduleConstants;
import com.choice.hr.domain.ScheduleAttendance.CrewScheduling;
import com.choice.hr.domain.ScheduleAttendance.Shift;
import com.choice.hr.service.ScheduleAttendance.DailyScheduleService;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.MisUtil;
import com.choice.misboh.commonutil.ReadReport;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.commonutil.excel.GenerateExcel;
import com.choice.misboh.constants.reportMis.MisStringConstant;
import com.choice.misboh.domain.reportMis.ReportObject;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;

/**
 * 描述：排班考勤——每日排班
 * @author 马振
 * 创建时间：2015-6-11 下午5:16:41
 */
@Controller
@RequestMapping(value = "dailySchedule")
public class DailyScheduleController {
	
	@Autowired
	private DailyScheduleService dailyScheduleService;
	
	@Autowired
	private CommonMISBOHService commonMISBOHService; 

	@Autowired
	protected DictColumns dictColumns;

	@Autowired
	protected DictColumnsService dictColumnsService;
	
	/**
	 * 描述：每日排班列表
	 * @author 马振
	 * 创建时间：2015-6-12 上午11:27:03
	 * @param modelMap
	 * @param crewScheduling
	 * @param shift
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/listDailySchedule")
	public ModelAndView listDailySchedule(ModelMap modelMap, CrewScheduling crewScheduling,Shift shift, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		if (ValueUtil.IsEmpty(crewScheduling.getDworkdate())) {
			crewScheduling.setDworkdate(DateJudge.getNowDate());
			shift.setDworkdate(DateJudge.getNowDate());
		}
		
		if (ValueUtil.IsEmpty(crewScheduling.getPk_store())) {
			crewScheduling.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
			shift.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		}
		
		modelMap.put("getTeamByDept", dailyScheduleService.getTeamByDept(shift));
		modelMap.put("getEmpShift", dailyScheduleService.getEmpShift(crewScheduling));
		modelMap.put("pk_store", commonMISBOHService.getPkStore(session).getPk_store());
		modelMap.put("dworkdate", shift.getDworkdate());
		return new ModelAndView(DailyScheduleConstants.LIST_DAILYSCHEDULE, modelMap);
	}
	
	/**
	 * 描述：excel表格导出
	 * @author 马振
	 * 创建时间：2015-6-12 下午2:16:38
	 * @param response
	 * @param request
	 * @param session
	 * @param publicEntity
	 * @param reportName
	 * @param headers
	 * @throws CRUDException
	 */
	@RequestMapping("/exportReport")
	public void exportReport(HttpServletResponse response,HttpServletRequest request,HttpSession session,Shift shift,String reportName,String headers) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		shift.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		
		//设置分页，查询所有数据
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ReadReport.getReportName(MisStringConstant.class, reportName));
		
		//获取并执行查询方法，获取查询结果
		ReportObject<Map<String,Object>> result = MisUtil.execMethod(dailyScheduleService, reportName, shift);
		if(null != result.getFooter())
			result.getRows().addAll(result.getFooter());
		if(null != headers)
			new GenerateExcel<Map<String, Object>>().creatReportCrewSchedule(ReadReport.getReportNameCN(MisStringConstant.class, reportName),request,response,
					result.getRows(),ReadReport.getReportNameCN(MisStringConstant.class, reportName),
					null,headers, ReadReport.getReportExcel(MisStringConstant.class, reportName));
		else
			new GenerateExcel<Map<String, Object>>().creatReportCrewSchedule(ReadReport.getReportNameCN(MisStringConstant.class, reportName),request,response, 
					result.getRows(),ReadReport.getReportNameCN(MisStringConstant.class, reportName), 
					dictColumnsService.listDictColumnsByAccount(dictColumns,ReadReport.getDefaultHeader(MisStringConstant.class, reportName)),
					headers, ReadReport.getReportExcel(MisStringConstant.class, reportName));
	}
}