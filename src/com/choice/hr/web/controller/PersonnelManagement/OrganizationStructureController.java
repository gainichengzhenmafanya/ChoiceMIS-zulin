package com.choice.hr.web.controller.PersonnelManagement;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.hr.constants.PersonnelManagement.OrganizationStructureConstants;
import com.choice.hr.domain.PersonnelManagement.HrDept;
import com.choice.hr.service.PersonnelManagement.OrganizationStructureService;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.orientationSys.util.Page;

/**
 * 描述：组织结构
 * @author 马振
 * 创建时间：2015-5-11 上午9:06:38
 */
@Controller
@RequestMapping(value="organizationStructure")
public class OrganizationStructureController {
	
	@Autowired
	private OrganizationStructureService organizationStructureService;

	@Autowired
	private CommonMISBOHService commonMISBOHService; 
	
	/**
	 * 描述：人力资源组织结构
	 * @author 马振
	 * 创建时间：2015-5-11 上午10:28:09
	 * @param modelMap
	 * @param marsaleclass
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/treeOrganizationStructure")
	public ModelAndView treePersonnelStructure(ModelMap modelMap, HrDept hrDept, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<Map<String,Object>> listMarsaleclassTree = organizationStructureService.listPersonnelStructureTree(hrDept,session);
		modelMap.put("listPersonnelStructureTree", listMarsaleclassTree);
		modelMap.put("pk_id", hrDept.getPk_hrdept());
		return new ModelAndView(OrganizationStructureConstants.TREE_ORGANIZATIONSTRUCTURE, modelMap);
	}
	
	/**
	 * 描述：人力资源部门
	 * @author 马振
	 * 创建时间：2015-5-11 上午11:33:58
	 * @param modelMap
	 * @param hrDept
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/hrDeptDetail")
	public ModelAndView listHrDept(ModelMap modelMap, HrDept hrDept, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("pk_store", commonMISBOHService.getPkStore(session).getPk_store());
		
		//如果部门主键不为空则有数据传递，否则无数据直接跳转页面
		if (ValueUtil.IsNotEmpty(hrDept.getPk_hrdept())) {
			modelMap.put("listHrDept", organizationStructureService.listHrDept(hrDept,session).get(0));
		}
		return new ModelAndView(OrganizationStructureConstants.HRDEPTDETAIL, modelMap);
	}
	
	/**
	 * 描述：保存新增或修改的人力资源部门信息
	 * @author 马振
	 * 创建时间：2015-5-11 下午3:07:14
	 * @param modelMap
	 * @param hrDept
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/saveHrDept")
	public ModelAndView saveHrDept(ModelMap modelMap, HrDept hrDept, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		//如果部门主键不为空则说明是修改，否则为新增
		if (ValueUtil.IsNotEmpty(hrDept.getPk_hrdept())) {
			organizationStructureService.updateHrDept(hrDept,session);
		}else{
			organizationStructureService.saveHrDept(hrDept,session);
		}
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 描述：启用、停用
	 * @author 马振
	 * 创建时间：2015-5-11 下午4:53:22
	 * @param modelMap
	 * @param hrDept
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/enableOrDisable")
	public ModelAndView updateEnableState(ModelMap modelMap, HrDept hrDept, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		organizationStructureService.updateEnableState(hrDept,session);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 描述：部门选择
	 * @author 马振
	 * 创建时间：2015-5-15 下午6:10:56
	 * @param modelMap
	 * @param session
	 * @param pk_store
	 * @param single
	 * @param callBack
	 * @param domId
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toChooseHrDept")
	public ModelAndView toChooseHrDept(ModelMap modelMap,HttpSession session,String pk_store, String single,String callBack,String domId,Page page) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		HrDept hrDept = new HrDept();
		hrDept.setPk_store(pk_store);
		
		modelMap.put("listHrDept", organizationStructureService.listHrDept(hrDept, session));
		modelMap.put("pageobj", page);
		modelMap.put("callBack", callBack);
		modelMap.put("domId", domId);
		modelMap.put("single", single);
		modelMap.put("pk_store", pk_store);
		return new ModelAndView(OrganizationStructureConstants.CHOOSEHRDEPT, modelMap);
	}
}