package com.choice.hr.web.controller.PersonnelManagement;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.hr.constants.PersonnelManagement.StaffListConstants;
import com.choice.hr.domain.ContractManagement.ContractRegistration;
import com.choice.hr.domain.DataDictionary.DataTyp;
import com.choice.hr.domain.PersonnelManagement.Employee;
import com.choice.hr.domain.PersonnelManagement.Family;
import com.choice.hr.domain.PersonnelManagement.HrDept;
import com.choice.hr.service.ContractManagement.ContractRegistrationService;
import com.choice.hr.service.PersonnelManagement.StaffListService;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.ItemPath;
import com.choice.misboh.commonutil.ReadReport;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.constants.reportMis.MisStringConstant;
import com.choice.misboh.domain.common.CommonMethod;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;

/**
 * 描述：人员管理——人员列表
 * @author 马振
 * 创建时间：2015-5-12 上午9:03:17
 */
@Controller
@RequestMapping(value="staffList")
public class StaffListController {
	
	@Autowired
	private LogsMapper logsMapper;
	
	@Autowired
	protected DictColumnsService dictColumnsService;
	
	@Autowired
	protected DictColumns dictColumns;
	
	@Autowired
	private StaffListService staffListService;
	
	@Autowired
	private ContractRegistrationService registrationService;

	@Autowired
	private CommonMISBOHService commonMISBOHService; 
	
	private String imgType = "PNG";//限定图片格式
	private String path = ItemPath.getItemRootPath() + "/scrimg/";
	private static final int BUFFER_SIZE = 100 * 150;
	
	/**
	 * 描述：人员列表树
	 * @author 马振
	 * 创建时间：2015-5-12 上午9:03:58
	 * @param modelMap
	 * @param hrDept
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/treeStaffList")
	public ModelAndView treePersonnelStructure(ModelMap modelMap, Employee employee, HrDept hrDept, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		List<Map<String,Object>> listMarsaleclassTree = staffListService.staffListTree(hrDept,session);
//		modelMap.put("listPersonnelStructureTree", listMarsaleclassTree);
//		modelMap.put("pk_id", hrDept.getPk_hrdept());
//		return new ModelAndView(StaffListConstants.TREE_STAFFLIST, modelMap);
		
		modelMap.put("listEmployee", staffListService.listEmployee(employee, session));
		return new ModelAndView(StaffListConstants.LIST_EMPLOYEE_NEW, modelMap);
	}
	
	/**
	 * 描述：人员列表信息
	 * @author 马振
	 * 创建时间：2015-5-12 上午9:20:38
	 * @param modelMap
	 * @param employee
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/listEmployee")
	public ModelAndView listEmployee(ModelMap modelMap, Employee employee,CommonMethod commonMethod, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		List<Employee> listEmployee = staffListService.listEmployee(employee, session);
		
		modelMap.put("listEmployee", listEmployee);
		dictColumns.setTableName(ReadReport.getReportName(MisStringConstant.class,"employee"));
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		
		//判断session中是否存在flag的对象，如果不存在则说明有字段显示，否则没有
		if (null == session.getAttribute("flag")) {
			modelMap.put("listColumns", dictColumnsService.listDictColumnsByAccount(dictColumns, ReadReport.getDefaultHeader(MisStringConstant.class, "employee")));
		}
		
		//移除session中的flag对象
		session.removeAttribute("flag");
		
		String url = StaffListConstants.LIST_EMPLOYEE;
		
		//如果type为select，则是跳转到列表选择页面
		if ("select".equals(commonMethod.getQuerysql())) {
			url = StaffListConstants.LIST_SELECTEMPLOYEE;
			modelMap.put("commonMethod", commonMethod);
			
			for (int i = 0; i < listEmployee.size(); i++) {
				for (int k = 0; k < staffListService.findByTeam(employee).size(); k++) {
					if (listEmployee.get(i).getPk_employee().equals(staffListService.findByTeam(employee).get(k).getPk_employee())) {
						listEmployee.get(i).setFlag("true");
					}
				}
			}
		}
		
		return new ModelAndView(url, modelMap);
	}
	
	/**
	 * 描述：人员列表弹出框
	 * @author 马振
	 * 创建时间：2015-5-19 上午10:04:13
	 * @param modelMap
	 * @param session
	 * @param pk_store
	 * @param single
	 * @param callBack
	 * @param domId
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toChooseEmployee")
	public ModelAndView toChooseEmployee(ModelMap modelMap,HttpSession session,CommonMethod commonMethod) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		HrDept hrDept = new HrDept();
		hrDept.setVcode(commonMethod.getVcode());
		hrDept.setVname(commonMethod.getVname());
		hrDept.setVinit(commonMethod.getVinit());
		
		modelMap.put("listPersonnelStructureTree", staffListService.staffListTree(hrDept,session));
		modelMap.put("pk_team", commonMethod.getPk_team());
		modelMap.put("pk_id", hrDept.getPk_hrdept());
		modelMap.put("callBack", commonMethod.getCallBack());
		modelMap.put("domId", commonMethod.getDomId());
		modelMap.put("single", commonMethod.getSingle());
		modelMap.put("pk_store", commonMethod.getPk_store());
		return new ModelAndView(StaffListConstants.TREE_LISTEMPLOYEE, modelMap);
	}
	
	/**
	 * 描述：调转到列选择页面
	 * @author 马振
	 * 创建时间：2015-5-12 下午3:27:03
	 * @param modelMap
	 * @param session
	 * @param reportName
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toColumnsChoose")
	public ModelAndView toColumnsChoose(ModelMap modelMap,HttpSession session,String reportName)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ReadReport.getReportName(MisStringConstant.class,reportName));
		String[] objBean = this.getClass().getAnnotation(RequestMapping.class).value();
		if(objBean.length >= 1)
			modelMap.put("objBean", objBean[0]);
		modelMap.put("tableName", ReadReport.getReportName(MisStringConstant.class,reportName));
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ReadReport.getDefaultHeader(MisStringConstant.class, reportName)));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(MisStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	
	/**
	 * 描述：恢复默认显示字段
	 * @author 马振
	 * 创建时间：2015-5-19 下午6:47:55
	 * @param modelMap
	 * @param dictColumns
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/recoverDefaultColumns")
	public ModelAndView recoverDefaultColumns(ModelMap modelMap,String tableName,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(tableName);
		dictColumnsService.deleteColumnsChoose(dictColumns);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 描述：保存现有的字段
	 * @author 马振
	 * 创建时间：2015-5-12 下午6:01:28
	 * @param modelMap
	 * @param dictColumns
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/saveColumnsChoose")
	public ModelAndView saveColumnsChoose(ModelMap modelMap,DictColumns dictColumns,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		
		//判断是否有字段需要显示，若有则在session中创建一个flag对象；没有，则不需要创建
		if (0 == dictColumns.getId().length()) {
			session.setAttribute("flag", "false");
		}
		dictColumnsService.saveColumnsChoose(dictColumns);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 描述：查询民族
	 * @author 马振
	 * 创建时间：2015-5-16 下午6:35:10
	 * @param modelMap
	 * @param dictColumns
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toChooseNation")
	public ModelAndView toChooseNation(ModelMap modelMap,HttpSession session,String pk_store, String single,String callBack,String domId,Page page) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		DataTyp dataTyp = new DataTyp();//创建数据字典对象
		dataTyp.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		dataTyp.setVtyp(DDT.DATATYPE_TYP1);
		modelMap.put("listNation", staffListService.listDataTyp(dataTyp));
		modelMap.put("pageobj", page);
		modelMap.put("callBack", callBack);
		modelMap.put("domId", domId);
		modelMap.put("single", single);
		modelMap.put("pk_store", pk_store);
		return new ModelAndView(StaffListConstants.CHOOSENATION, modelMap);
	}
	
	/**
	 * 描述：跳转到人事管理——档案页面
	 * @author 马振
	 * 创建时间：2015-5-13 上午9:18:47
	 * @param modelMap
	 * @param employee
	 * @param page
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/findEmployeeByPk")
	public ModelAndView findEmployeeByPk(ModelMap modelMap, Employee employee, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String url = ""; //跳转页面
		DataTyp dataTyp = new DataTyp();//创建数据字典对象
		dataTyp.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		
		//学历
		dataTyp.setVtyp(DDT.DATATYPE_TYP2);
		modelMap.put("vschoolage", staffListService.listDataTyp(dataTyp));
		
		//学位
		dataTyp.setVtyp(DDT.DATATYPE_TYP3);
		modelMap.put("vdegree", staffListService.listDataTyp(dataTyp));
		
		//政治面貌
		dataTyp.setVtyp(DDT.DATATYPE_TYP4);
		modelMap.put("vpubliclist", staffListService.listDataTyp(dataTyp));
		
		//户口类型
		dataTyp.setVtyp(DDT.DATATYPE_TYP5);
		modelMap.put("vhouse", staffListService.listDataTyp(dataTyp));
		
		//用工形式
		dataTyp.setVtyp(DDT.DATATYPE_TYP6);
		modelMap.put("vworktyp", staffListService.listDataTyp(dataTyp));
		
		//籍贯
		dataTyp.setVtyp(DDT.DATATYPE_TYP7);
		modelMap.put("vhome", staffListService.listDataTyp(dataTyp));
		
		//银行卡类型
		dataTyp.setVtyp(DDT.DATATYPE_TYP8);
		modelMap.put("vbanktyp", staffListService.listDataTyp(dataTyp));
		
		//健康证办理地区
		dataTyp.setVtyp(DDT.DATATYPE_TYP9);
		modelMap.put("vjkarea", staffListService.listDataTyp(dataTyp));
		
		//税率类型
		dataTyp.setVtyp(DDT.DATATYPE_TYP10);
		modelMap.put("vtaxno", staffListService.listDataTyp(dataTyp));

		//合同类型
		dataTyp.setVtyp(DDT.DATATYPE_TYP11);
		modelMap.put("vpacktyp", staffListService.listDataTyp(dataTyp));
		
		//工资级别
		dataTyp.setVtyp(DDT.DATATYPE_TYP12);
		modelMap.put("vdutyid", staffListService.listDataTyp(dataTyp));
		
		//用工来源
		dataTyp.setVtyp(DDT.DATATYPE_TYP13);
		modelMap.put("vcomfrom", staffListService.listDataTyp(dataTyp));
		
		//保险状态
		dataTyp.setVtyp(DDT.DATATYPE_TYP14);
		modelMap.put("vbxsta", staffListService.listDataTyp(dataTyp));
		
		//岗位级别
		dataTyp.setVtyp(DDT.DATATYPE_TYP15);
		modelMap.put("vdutylvl", staffListService.listDataTyp(dataTyp));
		
		//外语等级
		dataTyp.setVtyp(DDT.DATATYPE_TYP16);
		modelMap.put("vtechnic", staffListService.listDataTyp(dataTyp));
		
		//在职岗位
		dataTyp.setVtyp(DDT.DATATYPE_TYP17);
		modelMap.put("vstation", staffListService.listDataTyp(dataTyp));
		
		//员工状态
		dataTyp.setVtyp(DDT.DATATYPE_TYP18);
		modelMap.put("vstatus", staffListService.listDataTyp(dataTyp));
		
		//试用期
		dataTyp.setVtyp(DDT.DATATYPE_TYP19 + "");
		modelMap.put("vtrial", staffListService.listDataTyp(dataTyp));
		
		//婚姻状况
		dataTyp.setVtyp(DDT.DATATYPE_TYP20);
		modelMap.put("vmarriage", staffListService.listDataTyp(dataTyp));
				
		//员工状态
		dataTyp.setVtyp(DDT.DATATYPE_TYP21);
		modelMap.put("vsex", staffListService.listDataTyp(dataTyp));
		
		//转正原因
		dataTyp.setVtyp(DDT.DATATYPE_TYP22);
		modelMap.put("positiveReason", staffListService.listDataTyp(dataTyp));
		
		//判断session中是否存在tabs_flag的对象，如果存在则传递
		if (null != session.getAttribute("flag")) {
			modelMap.put("tabs_flag", session.getAttribute("tabs_flag"));
		}
		
		employee.setFlag("add");
		path = session.getServletContext().getRealPath("/scrimg")+"\\";
		modelMap.put("image", findImage(path, employee.getPk_employee()));
		modelMap.put("listFamily", staffListService.listFamilyByEmp(employee,session));	//家庭
		
		ContractRegistration cr = new ContractRegistration();	
		cr.setPK_EMPLOYEE(employee.getPk_employee());
		modelMap.put("listContract", registrationService.listContractRegistration(cr));	//合同
		
		if (0 == staffListService.listEmployee(employee, session).size()) {
			modelMap.put("employee", null);
		} else {
			modelMap.put("employee", staffListService.listEmployee(employee, session).get(0));
		}
		
		if (ValueUtil.IsNotEmpty(session.getAttribute("picButton"))) {
			employee.setButtontyp(session.getAttribute("picButton").toString());
		}
		
		//跳转到档案页面
		if ("archives".equals(employee.getButtontyp())) {
			
			session.removeAttribute("picButton");
			
			//获取年龄
			int age = staffListService.listEmployee(employee, session).get(0).getVage();
			
			//获取出生日期(当前系统日期-年龄)
			modelMap.put("birthday", DateJudge.getDateAfterYear(DateJudge.getNowDate(), -age));
			url = StaffListConstants.EMPLOYEE_ARCHIVES;
		}
		
		//跳转到转正页面
		if ("positive".equals(employee.getButtontyp())) {
			
			//获取入职日期
			String vjrdat = staffListService.listEmployee(employee, session).get(0).getVjrdat();
			
			//获取试用时间
			String vtrial = staffListService.listEmployee(employee, session).get(0).getVtrial();
			modelMap.put("positiveDate", DateJudge.getDateAfterMonth(vjrdat, Integer.valueOf(vtrial)));
			url = StaffListConstants.EMPLOYEE_POSITIVE;
		}
		return new ModelAndView(url, modelMap);
	}
	
	/**
	 * 描述：跳转到新增图片页面
	 * @author 马振
	 * 创建时间：2015-5-17 下午2:50:41
	 * @param modelMap
	 * @param employee
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/uploadPicture")
	public ModelAndView uploadPicture(ModelMap modelMap, String pk_employee, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("pk_employee", pk_employee);
		return new ModelAndView(StaffListConstants.UPLOADPICTURE, modelMap);
	}
	
	/**
	 * 描述：保存图片
	 * @author 马振
	 * 创建时间：2015-5-17 下午3:13:48
	 * @param request
	 * @param name
	 * @param chunk
	 * @param chunks
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveImage", method=RequestMethod.POST)
	@ResponseBody
	public String saveImage(HttpServletRequest request,String pk_employee, String name, int chunk, int chunks,HttpSession session) throws Exception {
		try {
			if(name.length() == name.getBytes().length){
				
				//如果相等说明不含汉字字符，直接上传
				path = request.getSession().getServletContext().getRealPath("/scrimg") + "\\";
				path = path + pk_employee + ".PNG";
				File uploadPath = new File(path);
				
				// 文件已存在（上传了同名的文件）
				if (chunk == 0 && uploadPath.exists()) {
					uploadPath.delete();
					uploadPath = new File(path);
				}
				
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
				MultipartFile file = multipartRequest.getFile("upload");    
				copy(file, uploadPath);
				session.setAttribute("picButton","archives");
				DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
				Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),"上传场景图片","成功上传了场景图片"+name.substring(0, name.indexOf("."))+".PNG",session.getAttribute("ip").toString(),ProgramConstants.BOH);
				logsMapper.addLogs(logd);
				return "success";
			}else{
				//否则含有汉字，不进行上传
				return "error";
			}
		} catch (Exception e) {
			return "";
		}	
	}
	
	/**
	 * 描述：图片复制
	 * @author 马振
	 * 创建时间：2015-5-17 下午3:18:44
	 * @param src
	 * @param dst
	 */
	private void copy(MultipartFile src, File dst) {
		InputStream in = null;
		OutputStream out = null;
		try {
			if (dst.exists()) {
				out = new BufferedOutputStream(new FileOutputStream(dst, true),
						BUFFER_SIZE);
			} else {
				out = new BufferedOutputStream(new FileOutputStream(dst),
						BUFFER_SIZE);
			}
			in = new BufferedInputStream(src.getInputStream(), BUFFER_SIZE);

			byte[] buffer = new byte[BUFFER_SIZE];
			int len = 0;
			while ((len = in.read(buffer)) > 0) {
				out.write(buffer, 0, len);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != in) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (null != out) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * 描述：获取指定目录下的图片
	 * @author 马振
	 * 创建时间：2015-5-17 下午3:47:54
	 * @param path
	 * @param value
	 * @return
	 */
	public String findImage(String path, String value) {
		
		String vData = "";
		File file = new File(path);
		for (File f : file.listFiles()) {
			int  idex = f.getName().indexOf(".PNG");
			String filename = "";
			if(idex == -1){
				filename = f.getName().substring(0,f.getName().indexOf("."));
				continue;
			}else{
				filename = f.getName().substring(0,idex);
			}

			String lowerCase = f.getName()
					.substring( f.getName().indexOf(".PNG")+1,f.getName().length()).toLowerCase();
						
			if(!imgType.toLowerCase().equals(lowerCase)){
				continue;
			}
			
			if (value != null) {
				int len = value.length();
				
				if (filename.length()>=len&&(filename.indexOf(value.toLowerCase())!= -1||filename.indexOf(value.toUpperCase())!= -1)) {
					vData = f.getName().substring(0, f.getName().indexOf(".PNG"));
				}
			} else {
				vData = f.getName().substring(0, f.getName().indexOf(".PNG"));
			}
		}

		return vData;
	}	
	
	/**
	 * 描述：保存修改的人员信息
	 * @author 马振
	 * 创建时间：2015-5-14 下午2:45:56
	 * @param modelMap
	 * @param employee
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/saveEmployee")
	public ModelAndView saveEmployee(ModelMap modelMap, Employee employee, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		staffListService.updateEmployee(employee, session);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 描述：根据传递的type跳转页面
	 * @author 马振
	 * 创建时间：2015-5-15 上午8:52:42
	 * @param modelMap
	 * @param type
	 * @param pk_employee
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/addData")
	public ModelAndView addData(ModelMap modelMap, String type, String flag, String pk_employee, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		String url = "";//跳转页面的路径
		DataTyp dataTyp = new DataTyp();
		Employee employee = new Employee();
		employee.setPk_employee(pk_employee);
		employee.setFlag(flag);

		dataTyp.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		
		//政治面貌
		dataTyp.setVtyp("4");
		modelMap.put("vpubliclist", staffListService.listDataTyp(dataTyp));
		
		//在职岗位
		dataTyp.setVtyp("17");
		modelMap.put("vstation", staffListService.listDataTyp(dataTyp));
		
		//如果type为family，则跳转到新增家庭页面
		if ("family".equals(type)) {
			if ("add".equals(flag)) {
				modelMap.put("family", new Family());
			}
			if ("update".equals(flag)) {
				modelMap.put("family", staffListService.findFamilyByPk(employee,session));
			}
			url = StaffListConstants.ADD_FAMILY;
		}
		modelMap.put("pk_employee", pk_employee);
		modelMap.put("flag", flag);
		return new ModelAndView(url, modelMap);
	}
	
	/**
	 * 描述：保存新增或修改的家庭成员
	 * @author 马振
	 * 创建时间：2015-5-15 上午11:35:12
	 * @param modelMap
	 * @param employee
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/saveFamily")
	public ModelAndView saveFamily(ModelMap modelMap, Family family, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		staffListService.saveFamily(family,session);
		session.setAttribute("tabs_flag", "family");
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 描述：员工转正
	 * @author 马振
	 * 创建时间：2015-5-19 下午2:52:28
	 * @param modelMap
	 * @param employee
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/employeePositive")
	public ModelAndView employeePositive(ModelMap modelMap, Employee employee, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		staffListService.employeePositive(employee, session);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
}