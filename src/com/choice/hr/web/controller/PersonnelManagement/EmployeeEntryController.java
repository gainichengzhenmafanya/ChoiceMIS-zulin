package com.choice.hr.web.controller.PersonnelManagement;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.hr.constants.PersonnelManagement.StaffListConstants;
import com.choice.hr.domain.DataDictionary.DataTyp;
import com.choice.hr.domain.PersonnelManagement.Employee;
import com.choice.hr.service.PersonnelManagement.EmployeeEntryService;
import com.choice.hr.service.PersonnelManagement.StaffListService;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.orientationSys.constants.StringConstant;
import com.choice.scm.domain.Positn;

/**
 * 员工入职管理 --- 人员档案
 * @author 文清泉
 * @param 2015年5月15日 上午9:26:16
 */
@Controller
@RequestMapping(value="employeeEntry")
public class EmployeeEntryController {
	
	@Autowired
	private EmployeeEntryService employeeEntryService;
	
	@Autowired
	private StaffListService staffListService;
	
	@Autowired
	private CommonMISBOHService commonMISBOHService;
    
    private Positn positn;

	@RequestMapping(value = "/listEmployeeEntry")
	public ModelAndView listEmployeeEntry(ModelMap modelMap, HttpSession session) throws CRUDException{
		setPositn(session);
		DataTyp dataTyp = new DataTyp();//创建数据字典对象
		dataTyp.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		
		//工资级别
		dataTyp.setVtyp(DDT.DATATYPE_TYP12);
		modelMap.put("pk_dutyid", staffListService.listDataTyp(dataTyp));
		
		//岗位级别
		dataTyp.setVtyp(DDT.DATATYPE_TYP15);
		modelMap.put("pk_dutylvl", staffListService.listDataTyp(dataTyp));
		
		//在职岗位
		dataTyp.setVtyp(DDT.DATATYPE_TYP17);
		modelMap.put("pk_station", staffListService.listDataTyp(dataTyp));
		// 试用期
		dataTyp.setVtyp(DDT.DATATYPE_TYP19);
		modelMap.put("vtrial", staffListService.listDataTyp(dataTyp));
		
		String empno = employeeEntryService.findEmployeeCard(positn.getCode());
		Employee emp = new Employee();
		emp.setVempno(empno);
		emp.setVjrdat(DateJudge.YYYY_MM_DD.format(new Date()));
		emp.setVwkdat(DateJudge.YYYY_MM_DD.format(new Date()));
		emp.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		modelMap.put("employee", emp);
		return new ModelAndView(StaffListConstants.EMPLOYEE_ENTRY, modelMap);
	}
	
	/**
	 * 保存入职记录
	 * @author 文清泉
	 * @param 2015年5月15日 下午4:54:17
	 * @param modelMap
	 * @param session
	 * @param employee
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/saveEmployeeEntry")
	public ModelAndView saveEmployeeEntry(ModelMap modelMap, HttpSession session,Employee employee) throws CRUDException{
		setPositn(session);
		DataTyp dataTyp = new DataTyp();//创建数据字典对象
		dataTyp.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		
		//工资级别
		dataTyp.setVtyp(DDT.DATATYPE_TYP12);
		modelMap.put("vdutyid", staffListService.listDataTyp(dataTyp));
		
		//岗位级别
		dataTyp.setVtyp(DDT.DATATYPE_TYP15);
		modelMap.put("vdutylvl", staffListService.listDataTyp(dataTyp));
		
		//在职岗位
		dataTyp.setVtyp(DDT.DATATYPE_TYP17);
		modelMap.put("vstation", staffListService.listDataTyp(dataTyp));

		// 试用期
		dataTyp.setVtyp(DDT.DATATYPE_TYP19);
		modelMap.put("vtrial", staffListService.listDataTyp(dataTyp));
		employee.setVscode(positn.getCode());
		employee.setVscodedes(positn.getDes());
		employee.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		//获取员工状态
		dataTyp.setVtyp(DDT.DATATYPE_TYP18);
		List<DataTyp> data = staffListService.listDataTyp(dataTyp);
		for(DataTyp typ :data){
			if(typ.getVcode().equals("1")){
				employee.setVstatus(typ.getVname());
				employee.setVstatuscode(typ.getVcode());
				employee.setPk_status(typ.getPk_datatyp());
				break;
			}
		}
		employee.setDjrtim(DateJudge.YYYY_MM_DD_HH_mm_ss.format(new Date()));
		employee.setVjremp(session.getAttribute("accountName").toString());
		employee.setVlr(DateJudge.YYYY_MM_DD.format(new Date()));
		if(employee.getPk_employee() == null || employee.getPk_employee().equals("")){
			employee.setPk_employee(CodeHelper.createUUID().substring(0, 20).toUpperCase());
		}
		
		int sta = employeeEntryService.saveEmployeeEntry(employee);
		
		if(sta == 1){
			return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
		}else{
			return new ModelAndView(StringConstant.ERROR_DONE, modelMap);
		}
	}
	
	
    /**
     * 读取SESSION数据 ,设定访问数据源
     * @author 文清泉
     * @param 2015年1月1日 上午10:59:13
     * @param session
     */
    public void setPositn(HttpSession session){
    	positn = (Positn)session.getAttribute("accountPositn");
    }
    
    /**
     * 检测是否存在黑名单中
     * @author 文清泉
     * @param 2015年5月16日 下午4:37:07
     * @param modelMap
     * @param employee
     * @param session
     * @return
     */
	@RequestMapping(value = "/checkBlackList")
    @ResponseBody
    private String checkBlackList(ModelMap modelMap,Employee employee,HttpSession session) {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
    	setPositn(session);
    	return employeeEntryService.checkBlackList(employee.getVidcard());
    }
}