package com.choice.hr.web.controller.RewardManagement;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.hr.constants.RewardManagement.BonusEntryConstants;
import com.choice.hr.domain.RewardManagement.BonusEntry;
import com.choice.hr.service.RewardManagement.BonusEntryService;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.orientationSys.constants.StringConstant;

/**
 * 描述：奖惩管理——奖金录入
 * @author 马振
 * 创建时间：2015-6-9 上午11:28:50
 */
@Controller
@RequestMapping(value = "bonusEntry")
public class BonusEntryController {
	
	@Autowired
	private BonusEntryService bonusEntryService;
	
	/**
	 * 描述：左侧树
	 * @author 马振
	 * 创建时间：2015-6-9 上午11:31:23
	 * @param modelMap
	 * @param bonusEntry
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/treeBonusEntry")
	public ModelAndView treeBonusEntry(ModelMap modelMap, BonusEntry bonusEntry, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("treeBonusEntry", bonusEntryService.treeBonusEntry(bonusEntry,session));
		modelMap.put("pk_id", bonusEntry.getPk_hrdept());
		return new ModelAndView(BonusEntryConstants.TREE_BONUSENTRY, modelMap);
	}
	
	/**
	 * 描述：奖金录入列表
	 * @author 马振
	 * 创建时间：2015-6-9 上午11:31:45
	 * @param modelMap
	 * @param bonusEntry
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/listBonusEntry")
	public ModelAndView listBonusEntry(ModelMap modelMap, BonusEntry bonusEntry, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("listBonusEntry", bonusEntryService.listBonusEntry(bonusEntry, session));
		modelMap.put("pk_hrdept", bonusEntry.getPk_hrdept());
		modelMap.put("dworkdate", bonusEntry.getDworkdate() == null ? DateJudge.getNowDate() : bonusEntry.getDworkdate());
		return new ModelAndView(BonusEntryConstants.LIST_BONUSENTRY, modelMap);
	}
	
	/**
	 * 描述：获取需要录入奖金的员工
	 * @author 马振
	 * 创建时间：2015-6-9 下午3:00:50
	 * @param modelMap
	 * @param bonusEntry
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/getEmp")
	public ModelAndView getEmp(ModelMap modelMap, BonusEntry bonusEntry, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("getEmpBonus", bonusEntryService.getEmpBonus(bonusEntry, session));
		modelMap.put("dworkdate", DateJudge.getNowDate());
		return new ModelAndView(BonusEntryConstants.LIST_EMP, modelMap);
	}
	
	/**
	 * 描述：新增录入奖金
	 * @author 马振
	 * 创建时间：2015-6-10 上午9:57:08
	 * @param modelMap
	 * @param bonusEntry
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/addBonusEntry")
	public ModelAndView addBonusEntry(ModelMap modelMap, BonusEntry bonusEntry, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		bonusEntryService.addBonusEntry(bonusEntry);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
}