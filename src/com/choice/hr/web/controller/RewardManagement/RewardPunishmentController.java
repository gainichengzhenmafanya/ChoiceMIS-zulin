package com.choice.hr.web.controller.RewardManagement;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.hr.constants.RewardManagement.RewardPunishmentConstants;
import com.choice.hr.domain.DataDictionary.DataTyp;
import com.choice.hr.domain.PersonnelManagement.Employee;
import com.choice.hr.domain.RewardManagement.RewardPunishment;
import com.choice.hr.service.PersonnelManagement.StaffListService;
import com.choice.hr.service.RewardManagement.RewardPunishmentService;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.orientationSys.constants.StringConstant;

/**
 * 描述：奖惩管理——奖惩登记
 * @author 马振
 * 创建时间：2015-6-5 下午3:53:08
 */
@Controller
@RequestMapping(value = "rewardPunishment")
public class RewardPunishmentController {
	
	@Autowired
	private StaffListService staffListService;
	
	@Autowired
	private RewardPunishmentService rewardPunishmentService;
	
	@Autowired
	private CommonMISBOHService commonMISBOHService; 
	
	/**
	 * 描述：奖惩登记显示
	 * @author 马振
	 * 创建时间：2015-6-5 下午3:55:42
	 * @param modelMap
	 * @param rewardPunishment
	 * @param employee
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/listRewardPunishment")
	public ModelAndView listRewardPunishment(ModelMap modelMap, RewardPunishment rewardPunishment,Employee employee, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (ValueUtil.IsNotEmpty(session.getAttribute("employee.pk_employee"))) {
			rewardPunishment.setPk_employee(session.getAttribute("employee.pk_employee").toString());
			employee.setPk_employee(session.getAttribute("employee.pk_employee").toString());
		}
		
		//若员工主键不为空，则查询员工数据
		if (ValueUtil.IsNotEmpty(rewardPunishment.getPk_employee())) {
			modelMap.put("employee", staffListService.listEmployee(employee, session).get(0));
		}

		modelMap.put("listRewardPunishment",rewardPunishmentService.listRewardPunishment(rewardPunishment, session));
		return new ModelAndView(RewardPunishmentConstants.LIST_REWARDPUNISHMENT, modelMap);
	}
	
	/**
	 * 描述：跳转到新增、修改页
	 * @author 马振
	 * 创建时间：2015-6-5 下午4:02:01
	 * @param modelMap
	 * @param rewardPunishment
	 * @param employee
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/addData")
	public ModelAndView addData(ModelMap modelMap, RewardPunishment rewardPunishment,Employee employee,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		DataTyp dataTyp = new DataTyp();//创建数据字典对象
		dataTyp.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		RewardPunishment leave = new RewardPunishment();
		
		//休假类型
		dataTyp.setVtyp(DDT.DATATYPE_TYP24);
		modelMap.put("leaveType", staffListService.listDataTyp(dataTyp));
		
		//若员工主键不为空，则查询员工数据
		if (ValueUtil.IsNotEmpty(rewardPunishment.getPk_employee())) {
			modelMap.put("employee", staffListService.listEmployee(employee, session).get(0));
		}
		
		//主键和员工主键不为空时，为修改
		if (ValueUtil.IsNotEmpty(rewardPunishment.getPk_rewardpunishment()) && ValueUtil.IsNotEmpty(rewardPunishment.getPk_employee())) {
			leave = rewardPunishmentService.getByPk(rewardPunishment);
		}
		
		modelMap.put("flag", rewardPunishment.getFlag());
		modelMap.put("leave", leave);
		modelMap.put("dworkdate", rewardPunishment.getDworkdate() == null ? DateJudge.getNowDate() : rewardPunishment.getDworkdate());
		return new ModelAndView(RewardPunishmentConstants.ADD_REWARDPUNISHMENT, modelMap);
	}
	
	/**
	 * 描述：保存数据
	 * @author 马振
	 * 创建时间：2015-6-5 下午4:05:01
	 * @param modelMap
	 * @param rewardPunishment
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/saveData")
	public ModelAndView saveData(ModelMap modelMap, RewardPunishment rewardPunishment,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		if ("add".equals(rewardPunishment.getFlag())) {
			rewardPunishmentService.saveRewardPunishment(rewardPunishment, session);
		}
		
		if ("update".equals(rewardPunishment.getFlag())) {
			rewardPunishmentService.updateRewardPunishment(rewardPunishment, session);
		}
		
		if ("delete".equals(rewardPunishment.getFlag())) {
			rewardPunishmentService.deleteRewardPunishment(rewardPunishment, session);
		}
		
		session.setAttribute("employee.pk_employee", rewardPunishment.getPk_employee());
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
}