package com.choice.hr.web.controller.ContractManagement;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.hr.constants.HRContractConstants;
import com.choice.hr.constants.ContractManagement.ContractConstants;
import com.choice.hr.domain.ContractManagement.Contract;
import com.choice.hr.domain.ContractManagement.ServiceFormula;
import com.choice.hr.domain.DataDictionary.DataTyp;
import com.choice.hr.domain.PersonnelManagement.Employee;
import com.choice.hr.domain.PersonnelManagement.HrDept;
import com.choice.hr.service.ContractManagement.ServiceCalculationService;
import com.choice.hr.service.PersonnelManagement.StaffListService;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.ReadReport;
import com.choice.misboh.constants.reportMis.MisStringConstant;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;

/**
 * 工龄计算
 * @author 文清泉
 * @param 2015年5月21日 下午2:13:30
 */
@Controller
@RequestMapping(value="serviceCalculation")
public class ServiceCalculationController {

	@Autowired
	private ServiceCalculationService serviceCalculationService;
	@Autowired
	private StaffListService staffListService;
	@Autowired
	protected DictColumnsService dictColumnsService;
	
	@Autowired
	protected DictColumns dictColumns;
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	/**
	 * 合同工龄计算主界面
	 * @author 文清泉
	 * @param 2015年5月21日 下午2:43:45
	 * @param modelMap
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/findServiceCalculation")
	public ModelAndView findServiceCalculation(ModelMap modelMap,HttpSession session){
		return  new ModelAndView(ContractConstants.MAINSERVICECALCULATION, modelMap);
	}
	@RequestMapping(value = "/treeServiceCalculation")
	public ModelAndView treeServiceCalculation(ModelMap modelMap, HrDept hrDept,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<Map<String,Object>> listMarsaleclassTree = staffListService.staffListTree(hrDept,session);
		modelMap.put("listPersonnelStructureTree", listMarsaleclassTree);
		modelMap.put("pk_id", hrDept.getPk_hrdept());
		return  new ModelAndView(ContractConstants.TREESERVICECALCULATION, modelMap);
	}
	
	/**
	 * 合同工龄计算界面展示
	 * @author 文清泉
	 * @param 2015年5月22日 下午3:44:30
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping(value = "/listServiceCalculation")
	public ModelAndView listServiceCalculation(ModelMap modelMap,HttpSession session,Contract contract) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setTableName(ReadReport.getReportName(MisStringConstant.class,"ServiceCalculation"));
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		String pk_store = commonMISBOHService.getPkStore(session).getPk_store();
		contract.setPk_store(pk_store);
		if(pk_store.equals(contract.getPk_hrdept())){
			contract.setPk_hrdept("-1");
		}
		modelMap.put("listColumns", dictColumnsService.listDictColumnsByAccount(dictColumns, ReadReport.getDefaultHeader(MisStringConstant.class, "ServiceCalculation")));
		List<Employee> listEmployee = serviceCalculationService.listServiceCalculation(contract);
		modelMap.put("listEmployee", listEmployee);
		return  new ModelAndView(ContractConstants.LISTSERVICECALCULATION, modelMap);
	}
	/**
	 * 工龄模版计算
	 * @author 文清泉
	 * @param 2015年5月22日 下午3:07:09
	 * @param modelMap
	 * @param session
	 * @param serviceFormula
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping(value = "/listCalculationMode")
	public ModelAndView listCalculationMode(ModelMap modelMap,HttpSession session,ServiceFormula serviceFormula) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String pk_store = commonMISBOHService.getPkStore(session).getPk_store();
		serviceFormula.setPK_STORE(pk_store);
		List<ServiceFormula> listServiceFormula = serviceCalculationService.listCalculationMode(serviceFormula);
		modelMap.put("listServiceFormula", listServiceFormula);
		return  new ModelAndView(ContractConstants.LISTCALCULATIONMODE, modelMap);
	}
	
	/**
	 * 编辑界面调整
	 * @author 文清泉
	 * @param 2015年5月22日 下午2:28:07
	 * @param modelMap
	 * @param session
	 * @param serviceFormula
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/saveCalculationModeRet")
	public ModelAndView saveCalculationModeRet(ModelMap modelMap,HttpSession session,ServiceFormula serviceFormula) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String pk_store = commonMISBOHService.getPkStore(session).getPk_store();
		serviceFormula.setPK_STORE(pk_store);
		DataTyp dataTyp = new DataTyp();//创建数据字典对象
		dataTyp.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		
		//合同类型
		dataTyp.setVtyp(DDT.DATATYPE_TYP11);
		modelMap.put("PK_CONTENTTYP", staffListService.listDataTyp(dataTyp));
		
		if(serviceFormula.getBCDAT() == null){
			serviceFormula.setBCDAT(DateJudge.YYYY_MM_DD.format(new Date()));
		}
		if(serviceFormula.getPK_SERVICEFORMULA() != null && !serviceFormula.getPK_SERVICEFORMULA().equals("")){
			serviceFormula = serviceCalculationService.listCalculationModeBy(serviceFormula);
		}
		modelMap.put("serviceFormula", serviceFormula);
		
		return  new ModelAndView(ContractConstants.SAVECALCULATIONMODE, modelMap);
	}
	/**
	 * 保存
	 * @author 文清泉
	 * @param 2015年5月22日 下午2:28:29
	 * @param modelMap
	 * @param session
	 * @param serviceFormula
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/saveServiceCalculation")
	public ModelAndView saveServiceCalculation(ModelMap modelMap,HttpSession session,ServiceFormula serviceFormula) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		DataTyp dataTyp = new DataTyp();//创建数据字典对象
		dataTyp.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		
		//合同类型
		dataTyp.setVtyp(DDT.DATATYPE_TYP11);
		modelMap.put("PK_CONTENTTYP", staffListService.listDataTyp(dataTyp));
		
		if(serviceFormula.getPK_SERVICEFORMULA() == null || serviceFormula.getPK_SERVICEFORMULA().equals("")){
			serviceFormula.setPK_SERVICEFORMULA(CodeHelper.createUUID().substring(0, 20).toUpperCase());
		}
		String pk_store = commonMISBOHService.getPkStore(session).getPk_store();
		serviceFormula.setPK_STORE(pk_store);
		
		int sta = serviceCalculationService.saveServiceCalculation(serviceFormula);
		
		if(serviceFormula.getBCDAT() == null){
			serviceFormula.setBCDAT(DateJudge.YYYY_MM_DD.format(new Date()));
		}
		modelMap.put("serviceFormula", serviceFormula);
		if(sta == 1){
			return new ModelAndView(HRContractConstants.HRACTION_DONE, modelMap);
		}else{
			return new ModelAndView(HRContractConstants.HRERROR_DONE, modelMap);
		}
	}
	 /**
     * 删除工龄公式
     * @author 文清泉
     * @param 2015年5月16日 下午4:37:07
     * @param modelMap
     * @param employee
     * @param session
     * @return
     */
	@RequestMapping(value = "/delCalculationModeRet")
    @ResponseBody
    public String delCalculationModeRet(ModelMap modelMap,ServiceFormula serviceFormula,HttpSession session)throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
    	return serviceCalculationService.delCalculationModeRet(serviceFormula);
    }
	
	/**
	 * 合同工龄计算
	 * @author 文清泉
	 * @param 2015年6月5日 下午12:02:18
	 * @param modelMap
	 * @param pk_employee
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/calCalculationMode")
	public ModelAndView calCalculationMode(ModelMap modelMap,ServiceFormula serviceFormula,HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		try {
			String pk_store = commonMISBOHService.getPkStore(session).getPk_store();
			serviceCalculationService.calCalculationMode(pk_store,serviceFormula.getPK_EMPLOYEE());
			return new ModelAndView(HRContractConstants.HRACTION_DONE, modelMap);
		} catch (Exception e) {
			return new ModelAndView(HRContractConstants.HRERROR_DONE, modelMap);
		}
	}
}