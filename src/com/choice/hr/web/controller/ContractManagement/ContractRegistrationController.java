package com.choice.hr.web.controller.ContractManagement;

import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.hr.constants.HRContractConstants;
import com.choice.hr.constants.ContractManagement.ContractConstants;
import com.choice.hr.domain.ContractManagement.Contract;
import com.choice.hr.domain.ContractManagement.ContractRegistration;
import com.choice.hr.domain.DataDictionary.DataTyp;
import com.choice.hr.service.ContractManagement.ContractRegistrationService;
import com.choice.hr.service.PersonnelManagement.StaffListService;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.commonutil.ReadReport;
import com.choice.misboh.constants.reportMis.MisStringConstant;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.scm.domain.Positn;

/**
 * 合同登记
 * @author 文清泉
 * @param 2015年5月18日 下午3:48:35
 */
@Controller
@RequestMapping(value="contractRegistration")
public class ContractRegistrationController{

	@Autowired
	private ContractRegistrationService registrationService;
	
	@Autowired
	protected DictColumnsService dictColumnsService;
	
	@Autowired
	protected DictColumns dictColumns;
	
	@Autowired
	private StaffListService staffListService;
	
    private Positn positn;
    
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	/**
	 * 合同登记列表显示
	 * @author 文清泉
	 * @param 2015年5月19日 上午9:26:12
	 * @param modelMap
	 * @param reg
	 * @param session
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping(value = "/listContractRegistration")
	public Object listContractRegistration(ModelMap modelMap, ContractRegistration reg, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		reg.setPK_CONTRACTREG("");
		dictColumns.setTableName(ReadReport.getReportName(MisStringConstant.class,"ContractRegistration"));
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		String pk_store = commonMISBOHService.getPkStore(session).getPk_store();
		modelMap.put("pk_store", pk_store);
		modelMap.put("listColumns", dictColumnsService.listDictColumnsByAccount(dictColumns, ReadReport.getDefaultHeader(MisStringConstant.class, "ContractRegistration")));
//			reg.setPK_EMPLOYEE("A4C6C5502C254791B72E");
		Contract contract = new Contract();
		if(reg.getPK_EMPLOYEE() == null || reg.getPK_EMPLOYEE().equals("")){
			modelMap.put("listContract", new ArrayList<ContractRegistration>());
		}else{
			contract = registrationService.findContract(reg);
			modelMap.put("listContract", registrationService.listContractRegistration(reg));
		}
		String pattern = "#.";
		for (int k = 0;k<FormatDouble.getMoneyBit();k++) {
			pattern +="0";
		}
		contract.setMoneypattern(pattern);
		modelMap.put("Contract",contract);
		return  new ModelAndView(ContractConstants.LISTCONTRACTREGISTRATION, modelMap);
	}
	
	/**
	 * 保存界面调整
	 * @author 文清泉
	 * @param 2015年5月19日 上午9:26:31
	 * @param modelMap
	 * @param reg
	 * @param session
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping(value = "/saveContractRegistrationRet")
	public ModelAndView saveContractRegistrationRet(ModelMap modelMap, ContractRegistration reg, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		DataTyp dataTyp = new DataTyp();//创建数据字典对象
		dataTyp.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		
		//查询月份
		dataTyp.setVtyp(DDT.DATATYPE_TYP19);
		modelMap.put("VTIMELIMIT", staffListService.listDataTyp(dataTyp));
		
		//合同类型
		dataTyp.setVtyp(DDT.DATATYPE_TYP11);
		modelMap.put("VTYP", staffListService.listDataTyp(dataTyp));
		
		modelMap.put("Contract",registrationService.findContract(reg));
		
		if(reg.getPK_CONTRACTREG() != null && !reg.getPK_CONTRACTREG().equals("")){
				reg =  registrationService.listContractRegistrationBy(reg);
		}
		if(reg.getVBEINDATE() == null){
			reg.setVBEINDATE(DateJudge.YYYY_MM_DD.format(new Date()));
		}
		if(reg.getVENDDATE() == null){
			reg.setVENDDATE(DateJudge.YYYY_MM_DD.format(new Date()));
		}
		if(reg.getVEXECUTIVEDATE() == null){
			reg.setVEXECUTIVEDATE(DateJudge.YYYY_MM_DD.format(new Date()));
		}
		modelMap.put("reg", reg);
		return  new ModelAndView(ContractConstants.SAVECONTRACTREGISTRATION, modelMap);
	}
	
	/**
	 * 保存
	 * @author 文清泉
	 * @param 2015年5月19日 下午3:48:09
	 * @param modelMap
	 * @param reg
	 * @param session
	 * @return
	 */
	@RequestMapping(value="/saveContractRegistration")
	public ModelAndView saveContractRegistration(ModelMap modelMap, ContractRegistration reg, HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		setPositn(session);
		DataTyp dataTyp = new DataTyp();//创建数据字典对象
		//查询月份
		dataTyp.setVtyp(DDT.DATATYPE_TYP19);
		modelMap.put("VTIMELIMIT", staffListService.listDataTyp(dataTyp));
		
		//合同类型
		dataTyp.setVtyp(DDT.DATATYPE_TYP11);
		modelMap.put("VTYP", staffListService.listDataTyp(dataTyp));
		
		modelMap.put("Contract",registrationService.findContract(reg));
		reg.setVINPUTBY(session.getAttribute("accountName").toString());
		reg.setVINPUTTIME(DateJudge.YYYY_MM_DD.format(new Date()));
		reg.setVSCODE(positn.getCode());
		if(reg.getPK_CONTRACTREG() == null || reg.getPK_CONTRACTREG().equals("")){
			reg.setPK_CONTRACTREG(CodeHelper.createUUID().substring(0, 20).toUpperCase());
		}
		int sta = registrationService.saveContractRegistration(reg);
		modelMap.put("reg",reg);
		modelMap.put("pk_employee", reg.getPK_EMPLOYEE());
		if(sta == 1){
			return new ModelAndView(HRContractConstants.HRACTION_DONE, modelMap);
		}else{
			return new ModelAndView(HRContractConstants.HRERROR_DONE, modelMap);
		}
	}
	
	/**
	 * 合同登记选择列调整
	 * @author 文清泉
	 * @param 2015年5月19日 上午9:26:49
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toColumnsChoose")
	public ModelAndView toColumnsChoose(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String reportName="ContractRegistration";
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ReadReport.getReportName(MisStringConstant.class,reportName));
		String[] objBean = this.getClass().getAnnotation(RequestMapping.class).value();
		if(objBean.length >= 1)
		modelMap.put("objBean", objBean[0]);
		modelMap.put("tableName", ReadReport.getReportName(MisStringConstant.class,reportName));
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ReadReport.getDefaultHeader(MisStringConstant.class, reportName)));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(MisStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	
	/**
	 * 保存列选择
	 * @author 文清泉
	 * @param 2015年5月19日 上午9:28:25
	 * @param modelMap
	 * @param dictColumns
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/saveColumnsChoose")
	public ModelAndView saveColumnsChoose(ModelMap modelMap,DictColumns dictColumns,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		
		//判断是否有字段需要显示，若有则在session中创建一个flag对象；没有，则不需要创建
		if (0 == dictColumns.getId().length()) {
			session.setAttribute("flag", "false");
		}
		dictColumnsService.saveColumnsChoose(dictColumns);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
    /**
     * 读取SESSION数据 ,设定访问数据源
     * @author 文清泉
     * @param 2015年1月1日 上午10:59:13
     * @param session
     */
    public void setPositn(HttpSession session){
	    positn = (Positn)session.getAttribute("accountPositn");
    }
    
    /**
     * 删除登记记录
     * @author 文清泉
     * @param 2015年5月16日 下午4:37:07
     * @param modelMap
     * @param employee
     * @param session
     * @return
     */
	@SuppressWarnings("unused")
	@RequestMapping(value = "/delContractReg")
    @ResponseBody
    private String delContractReg(ModelMap modelMap,ContractRegistration reg,HttpSession session)throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
    	setPositn(session);
    	return registrationService.delContractReg(reg);
    }
}
