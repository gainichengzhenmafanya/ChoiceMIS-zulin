package com.choice.hr.web.controller.ContractManagement;

import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.hr.constants.ContractManagement.ContractConstants;
import com.choice.hr.domain.ContractManagement.Contract;
import com.choice.hr.domain.DataDictionary.DataTyp;
import com.choice.hr.service.ContractManagement.ContractChangeService;
import com.choice.hr.service.PersonnelManagement.StaffListService;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.commonutil.ReadReport;
import com.choice.misboh.constants.reportMis.MisStringConstant;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;

/**
 * 合同变更记录
 * @author 文清泉
 * @param 2015年5月19日 上午10:25:42
 */
@Controller
@RequestMapping(value="changeController")
public class ContractChangeController {
	@Autowired
	protected DictColumnsService dictColumnsService;
	
	@Autowired
	protected DictColumns dictColumns;
	
	@Autowired
	private ContractChangeService contractChangeService;
	
	@Autowired
	private StaffListService staffListService;

	@Autowired
	private CommonMISBOHService commonMISBOHService; 
	
	/**
	 * 合同变更记录
	 * @author 文清泉
	 * @param 2015年5月19日 上午10:28:00
	 * @param modelMap
	 * @param contract
	 * @param session
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping(value = "/listContractChange")
	public ModelAndView listContractChangeController(ModelMap modelMap, Contract contract, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setTableName(ReadReport.getReportName(MisStringConstant.class,"ContractChange"));
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		DataTyp dataTyp = new DataTyp();//创建数据字典对象
		dataTyp.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		
		//合同类型
		dataTyp.setVtyp(DDT.DATATYPE_TYP11);
		modelMap.put("VTYP", staffListService.listDataTyp(dataTyp));
		
		//员工状态
		dataTyp.setVtyp(DDT.DATATYPE_TYP18);
		modelMap.put("esta", staffListService.listDataTyp(dataTyp));
		
		modelMap.put("listColumns", dictColumnsService.listDictColumnsByAccount(dictColumns, ReadReport.getDefaultHeader(MisStringConstant.class, "ContractChange")));
		contract.setSysDate(DateJudge.YYYY_MM_DD.format(new Date()));
		String pattern = "#.";
		for (int k = 0;k<FormatDouble.getMoneyBit();k++) {
			pattern +="0";
		}
		contract.setMoneypattern(pattern);
		if(contract.getBfirst()== null){
			contract.setBfirst(DateJudge.YYYY_MM_DD.format(new Date()));
		}
		if(contract.getEfirst()== null){
			contract.setEfirst(DateJudge.YYYY_MM_DD.format(new Date()));
		}
		contract.setListContractRegistration(contractChangeService.listContractChangeController(contract));
		modelMap.put("contract", contract);
		return  new ModelAndView(ContractConstants.LISTCONTRACTCHANGE, modelMap);
	
	} 
	
	/**
	 * 合同登记选择列调整
	 * @author 文清泉
	 * @param 2015年5月19日 上午9:26:49
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toColumnsChoose")
	public ModelAndView toColumnsChoose(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String reportName="ContractChange";
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ReadReport.getReportName(MisStringConstant.class,reportName));
		String[] objBean = this.getClass().getAnnotation(RequestMapping.class).value();
		if(objBean.length >= 1)
			modelMap.put("objBean", objBean[0]);
		modelMap.put("tableName", ReadReport.getReportName(MisStringConstant.class,reportName));
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ReadReport.getDefaultHeader(MisStringConstant.class, reportName)));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(MisStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	
	/**
	 * 保存列选择
	 * @author 文清泉
	 * @param 2015年5月19日 上午9:28:25
	 * @param modelMap
	 * @param dictColumns
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/saveColumnsChoose")
	public ModelAndView saveColumnsChoose(ModelMap modelMap,DictColumns dictColumns,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		
		//判断是否有字段需要显示，若有则在session中创建一个flag对象；没有，则不需要创建
		if (0 == dictColumns.getId().length()) {
			session.setAttribute("flag", "false");
		}
		dictColumnsService.saveColumnsChoose(dictColumns);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
}
