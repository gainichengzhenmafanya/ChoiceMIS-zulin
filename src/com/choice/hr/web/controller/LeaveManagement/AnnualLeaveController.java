package com.choice.hr.web.controller.LeaveManagement;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.hr.constants.LeaveManagement.AnnualLeaveConstants;
import com.choice.hr.domain.DataDictionary.DataTyp;
import com.choice.hr.domain.LeaveManagement.LeaveRegistration;
import com.choice.hr.domain.PersonnelManagement.Employee;
import com.choice.hr.service.LeaveManagement.AnnualLeaveService;
import com.choice.hr.service.PersonnelManagement.StaffListService;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.orientationSys.constants.StringConstant;

/**
 * 描述：休假管理——年休假
 * @author 马振
 * 创建时间：2015-6-4 上午11:08:12
 */
@Controller
@RequestMapping(value = "annualLeave")
public class AnnualLeaveController {
	
	@Autowired
	private StaffListService staffListService;
	
	@Autowired
	private AnnualLeaveService annualLeaveService;

	@Autowired
	private CommonMISBOHService commonMISBOHService; 
	
	/**
	 * 描述：年休假显示
	 * @author 马振
	 * 创建时间：2015-6-4 上午11:09:02
	 * @param modelMap
	 * @param leaveRegistration
	 * @param employee
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/listAnnualLeave")
	public ModelAndView listLeaveRegistration(ModelMap modelMap, LeaveRegistration leaveRegistration,Employee employee, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (ValueUtil.IsNotEmpty(session.getAttribute("employee.pk_employee"))) {
			leaveRegistration.setPk_employee(session.getAttribute("employee.pk_employee").toString());
			employee.setPk_employee(session.getAttribute("employee.pk_employee").toString());
		}
		
		//若员工主键不为空，则查询员工数据
		if (ValueUtil.IsNotEmpty(leaveRegistration.getPk_employee())) {
			modelMap.put("employee", staffListService.listEmployee(employee, session).get(0));
		}

		modelMap.put("vbdat", leaveRegistration.getVbdat());
		modelMap.put("vedat", leaveRegistration.getVedat());
		modelMap.put("listLeave",annualLeaveService.listAnnualLeave(leaveRegistration, session));
		return new ModelAndView(AnnualLeaveConstants.LIST_ANNUALLEAVE, modelMap);
	}
	
	/**
	 * 描述：跳转到年休假编辑页面
	 * @author 马振
	 * 创建时间：2015-6-4 上午11:09:20
	 * @param modelMap
	 * @param leaveRegistration
	 * @param employee
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/addData")
	public ModelAndView addData(ModelMap modelMap, LeaveRegistration leaveRegistration,Employee employee,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		DataTyp dataTyp = new DataTyp();//创建数据字典对象
		dataTyp.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		LeaveRegistration leave = new LeaveRegistration();
		
		//休假类型
		dataTyp.setVtyp(DDT.DATATYPE_TYP23);
		dataTyp.setVcode("6");
		modelMap.put("leaveType", staffListService.listDataTyp(dataTyp));
		
		//若员工主键不为空，则查询员工数据
		if (ValueUtil.IsNotEmpty(leaveRegistration.getPk_employee())) {
			modelMap.put("employee", staffListService.listEmployee(employee, session).get(0));
		}
		
		//主键和员工主键不为空时，为修改
		if (ValueUtil.IsNotEmpty(leaveRegistration.getPk_annualleave()) && ValueUtil.IsNotEmpty(leaveRegistration.getPk_employee())) {
			leave = annualLeaveService.getByPk(leaveRegistration);
			leaveRegistration.setVbdat(leave.getVbdat());
			leaveRegistration.setVedat(leave.getVedat());
			leaveRegistration.setVsickdate(leave.getVsickdate());
		}
		
		modelMap.put("flag", leaveRegistration.getFlag());
		modelMap.put("leave", leave);
		modelMap.put("vbdat", leaveRegistration.getVbdat() == null ? DateJudge.getNowDate() : leaveRegistration.getVbdat());
		modelMap.put("vedat", leaveRegistration.getVedat() == null ? DateJudge.getNowDate() : leaveRegistration.getVedat());
		modelMap.put("vyears", leaveRegistration.getVyears() == null ? DateJudge.getNowDate().substring(0, 4) : leaveRegistration.getVyears());
		modelMap.put("vsickdate", leaveRegistration.getVsickdate() == null ? DateJudge.getNowDate() : leaveRegistration.getVsickdate());
		return new ModelAndView(AnnualLeaveConstants.ADD_DATA, modelMap);
	}
	
	/**
	 * 描述：保存年休假
	 * @author 马振
	 * 创建时间：2015-6-4 上午11:09:51
	 * @param modelMap
	 * @param leaveRegistration
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/saveData")
	public ModelAndView saveData(ModelMap modelMap, LeaveRegistration leaveRegistration,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		if ("add".equals(leaveRegistration.getFlag())) {
			annualLeaveService.saveAnnualLeave(leaveRegistration, session);
		}
		
		if ("update".equals(leaveRegistration.getFlag())) {
			annualLeaveService.updateAnnualLeave(leaveRegistration, session);
		}
		
		if ("delete".equals(leaveRegistration.getFlag())) {
			annualLeaveService.deleteAnnualLeave(leaveRegistration, session);
		}
		
		session.setAttribute("employee.pk_employee", leaveRegistration.getPk_employee());
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
}