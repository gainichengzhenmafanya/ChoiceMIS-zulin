package com.choice.hr.web.controller.LeaveManagement;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.hr.constants.LeaveManagement.LeaveInquiryConstants;
import com.choice.hr.domain.DataDictionary.DataTyp;
import com.choice.hr.domain.LeaveManagement.LeaveRegistration;
import com.choice.hr.service.LeaveManagement.LeaveInquiryService;
import com.choice.hr.service.PersonnelManagement.StaffListService;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.domain.common.CommonMethod;
import com.choice.misboh.service.common.CommonMISBOHService;

/**
 * 描述：休假管理——休假查询
 * @author 马振
 * 创建时间：2015-6-5 上午10:29:16
 */
@Controller
@RequestMapping(value = "leaveInquiry")
public class LeaveInquiryController {
	
	@Autowired
	private LeaveInquiryService leaveInquiryService;
	
	@Autowired
	private StaffListService staffListService;

	@Autowired
	private CommonMISBOHService commonMISBOHService; 
	
	/**
	 * 描述：左侧树
	 * @author 马振
	 * 创建时间：2015-6-5 上午10:30:39
	 * @param modelMap
	 * @param team
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/treeLeaveInquiry")
	public ModelAndView treeTeamManagement(ModelMap modelMap, LeaveRegistration leaveRegistration, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("treeLeaveInquiry", leaveInquiryService.treeLeaveInquiry(leaveRegistration,session));
		modelMap.put("pk_id", leaveRegistration.getPk_hrdept());
		return new ModelAndView(LeaveInquiryConstants.TREE_LEAVEINQUIRY, modelMap);
	}
	
	/**
	 * 描述：休假查询列表
	 * @author 马振
	 * 创建时间：2015-6-5 上午10:43:50
	 * @param modelMap
	 * @param leaveRegistration
	 * @param commonMethod
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/listLeaveInquiry")
	public ModelAndView listEmployee(ModelMap modelMap, LeaveRegistration leaveRegistration,CommonMethod commonMethod, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		DataTyp dataTyp = new DataTyp();
		dataTyp.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		
		//休假类型
		dataTyp.setVtyp(DDT.DATATYPE_TYP23);
		modelMap.put("leaveType", staffListService.listDataTyp(dataTyp));
		
		modelMap.put("vbdat", leaveRegistration.getVbdat() == null ? DateJudge.getNowDate() : leaveRegistration.getVbdat());
		modelMap.put("vedat", leaveRegistration.getVedat() == null ? DateJudge.getNowDate() : leaveRegistration.getVedat());
		modelMap.put("pk_hrdept", leaveRegistration.getPk_hrdept());
		
		modelMap.put("listLeaveInquiry", leaveInquiryService.listLeaveInquiry(leaveRegistration, session));
		return new ModelAndView(LeaveInquiryConstants.LIST_LEAVEINQUIRY, modelMap);
	}
}