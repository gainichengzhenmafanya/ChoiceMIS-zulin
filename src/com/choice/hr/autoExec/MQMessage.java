package com.choice.hr.autoExec;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import com.choice.framework.util.ForResourceFiles;

public class MQMessage {
	private static AttendanceReading Reading;
//	public static final String BROKER_URL = "tcp://wxzf.choicesoft.com.cn:61616";//MQ发送地址
	@SuppressWarnings("static-access")
	public MQMessage(AttendanceReading attendanceReading) {
		this.setReading(attendanceReading);
	}
	/**
	 * 解析门店上传考勤数据
	 * @author 文清泉
	 * @param 2015年5月26日 下午2:20:49
	 */
	public void execRead() {
		try {
			getMqMessage("BSBOH-KQ");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public static void getMqMessage(String queueName) throws Exception {
		QueueConnection connection = null;
		QueueSession session = null;
		String   sbohVersion=ForResourceFiles.getValByKey("config-bsboh.properties","mqurl");
		try {
			QueueConnectionFactory factory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_USER,
					ActiveMQConnection.DEFAULT_PASSWORD,sbohVersion);
			connection = factory.createQueueConnection();
			connection.start();
			session = connection.createQueueSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);
			Queue queue = session.createQueue(queueName);
			QueueReceiver receiver = session.createReceiver(queue);

			receiver.setMessageListener(new MessageListener() {
				@Override
				public void onMessage(Message msg) {
					if(msg != null){
							try {
								TextMessage mess = (TextMessage)msg;
								String str = mess.getText();
								getReading().execUpdateData(str);
							} catch (JMSException e) {
								e.printStackTrace();
							}
					}
				}
			});
			Thread.sleep(1000*5);
			session.commit();
		} catch (Exception e) {
			throw e;
		} finally {
			if(session != null){
				session.close();
			}
			if(connection != null){
				connection.close();
			}
		}
	}
	public static AttendanceReading getReading() {
		return Reading;
	}
	public static void setReading(AttendanceReading reading) {
		Reading = reading;
	}
}
