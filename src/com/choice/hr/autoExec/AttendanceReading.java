package com.choice.hr.autoExec;

import java.util.List;
import java.util.TimerTask;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;

import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ForResourceFiles;
import com.choice.hr.domain.ScheduleAttendance.EmpTime;
import com.choice.hr.persistence.ScheduleAttendance.EmpTimeMapper;
import com.choice.hr.service.ScheduleAttendance.EmpTimeService;

/**
 * 执行总部消息解析
 * @author 文清泉
 * @param 2015年5月26日 下午2:20:03
 */
public class AttendanceReading  extends TimerTask {
	
	@Autowired
	private EmpTimeMapper empTimeMapper;
	@Autowired
	private EmpTimeService empTimeService;
	
	@Override
	public void run() {
		String ismq = ForResourceFiles.getValByKey("config-bsboh.properties","ismq");
		if(ismq != null && ismq.equals("1")){
			new MQMessage(this).execRead();
		}
	}
	
	/**
	 * 解析MQ获取内容
	 * @author 文清泉
	 * @param 2015年5月26日 下午2:41:12
	 * @param str MQ读取内容
	 */
	public void execUpdateData(String str) {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		JSONArray jsonArray = JSONArray.fromObject(str);
		@SuppressWarnings("unchecked")
		List<EmpTime> listEmp =  (List<EmpTime>) JSONArray.toCollection(jsonArray, EmpTime.class);
		if(listEmp!=null){
			for (EmpTime empTime : listEmp) {
				empTimeMapper.delEmpTime(empTime);
				empTimeMapper.saveEmpTime(empTime);
			}
			empTimeService.calEmpTime(null);
		}
	}
}
