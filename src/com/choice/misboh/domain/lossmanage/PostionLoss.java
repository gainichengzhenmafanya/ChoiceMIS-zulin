package com.choice.misboh.domain.lossmanage;

import java.util.List;

/**
 * 损耗记录表
 * 方法描述： 作者： wangchao 
 */
public class PostionLoss {
	private String pk_postionloss;//主键
	private String dept;//部门
	private String pcode;//编码
	private double cnt;//数量
	private double cnt1;//参考数量
	private Integer typ;//类型 1.菜品报损2.半成品报损3.原料报损
	private String workdate;//营业日
	private String inputby;//录入人
	private String inputtime;//录入时间
	private String checkpeople;//审核人
	private String checktime;//审核时间
	private Integer state;//状态
	private String batchno;//生成出库单单号
	private String causesid;
	private String unit;
	private String unit1;
	private String pname;//名称
	private String memo;
	private String inputuser;//录入人
	private String chkeckuser;//审核人
	private List<PostionLoss> listPostionLoss;
	private List<String> listStr;//主键集合
	private String pks;
	private String acct;
	private String scode;
	private double unitper;
	
	public double getUnitper() {
		return unitper;
	}
	public void setUnitper(double unitper) {
		this.unitper = unitper;
	}
	public String getUnit1() {
		return unit1;
	}
	public void setUnit1(String unit1) {
		this.unit1 = unit1;
	}
	public String getScode() {
		return scode;
	}
	public void setScode(String scode) {
		this.scode = scode;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getPks() {
		return pks;
	}
	public void setPks(String pks) {
		this.pks = pks;
	}
	public List<String> getListStr() {
		return listStr;
	}
	public void setListStr(List<String> listStr) {
		this.listStr = listStr;
	}
	public String getPk_postionloss() {
		return pk_postionloss;
	}
	public void setPk_postionloss(String pk_postionloss) {
		this.pk_postionloss = pk_postionloss;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getPcode() {
		return pcode;
	}
	public void setPcode(String pcode) {
		this.pcode = pcode;
	}
	public double getCnt() {
		return cnt;
	}
	public void setCnt(double cnt) {
		this.cnt = cnt;
	}
	public double getCnt1() {
		return cnt1;
	}
	public void setCnt1(double cnt1) {
		this.cnt1 = cnt1;
	}
	public Integer getTyp() {
		return typ;
	}
	public void setTyp(Integer typ) {
		this.typ = typ;
	}
	public String getWorkdate() {
		return workdate;
	}
	public void setWorkdate(String workdate) {
		this.workdate = workdate;
	}
	public String getInputby() {
		return inputby;
	}
	public void setInputby(String inputby) {
		this.inputby = inputby;
	}
	public String getInputtime() {
		return inputtime;
	}
	public void setInputtime(String inputtime) {
		this.inputtime = inputtime;
	}
	public String getCheckpeople() {
		return checkpeople;
	}
	public void setCheckpeople(String checkpeople) {
		this.checkpeople = checkpeople;
	}
	public String getChecktime() {
		return checktime;
	}
	public void setChecktime(String checktime) {
		this.checktime = checktime;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public String getBatchno() {
		return batchno;
	}
	public void setBatchno(String batchno) {
		this.batchno = batchno;
	}
	public String getCausesid() {
		return causesid;
	}
	public void setCausesid(String causesid) {
		this.causesid = causesid;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public List<PostionLoss> getListPostionLoss() {
		return listPostionLoss;
	}
	public void setListPostionLoss(List<PostionLoss> listPostionLoss) {
		this.listPostionLoss = listPostionLoss;
	}
	public String getInputuser() {
		return inputuser;
	}
	public void setInputuser(String inputuser) {
		this.inputuser = inputuser;
	}
	public String getChkeckuser() {
		return chkeckuser;
	}
	public void setChkeckuser(String chkeckuser) {
		this.chkeckuser = chkeckuser;
	}
	
}
