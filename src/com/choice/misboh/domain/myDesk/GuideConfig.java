package com.choice.misboh.domain.myDesk;

/***
 * 我的桌面向导指引
 * @author wjf
 *
 */
public class GuideConfig {
	
	private String pk_guideconfig;
	private String module_id;//跳转路径
	private int sortno;//排序
	private int sta;//状态
	private String scode;//门店编码  暂未用
	private String color;//颜色
	private String name;//按钮名字
	private String url;//链接地址
	private String hint;//提示
	private String hintClass;//提示样式
	
	public String getHintClass() {
		return hintClass;
	}
	public void setHintClass(String hintClass) {
		this.hintClass = hintClass;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getHint() {
		return hint;
	}
	public void setHint(String hint) {
		this.hint = hint;
	}
	public String getPk_guideconfig() {
		return pk_guideconfig;
	}
	public void setPk_guideconfig(String pk_guideconfig) {
		this.pk_guideconfig = pk_guideconfig;
	}
	public String getModule_id() {
		return module_id;
	}
	public void setModule_id(String module_id) {
		this.module_id = module_id;
	}
	public int getSortno() {
		return sortno;
	}
	public void setSortno(int sortno) {
		this.sortno = sortno;
	}
	public int getSta() {
		return sta;
	}
	public void setSta(int sta) {
		this.sta = sta;
	}
	public String getScode() {
		return scode;
	}
	public void setScode(String scode) {
		this.scode = scode;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
