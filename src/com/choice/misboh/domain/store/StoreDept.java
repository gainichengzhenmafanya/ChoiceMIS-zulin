package com.choice.misboh.domain.store;

import java.util.List;

/**
 * @author 王超
 *
 */
public class StoreDept {
	private String pk_storedept;//门店部门主键
	private String pk_groupdept;//集团部门主键
	private String vcode;//编码
	private String vname;//名称
	private String vinit;//缩写
	private Integer isortno;//排序列
	private String enablestate;//启用状态
	private String vmemo;//备注
	private String pk_store;//门店主键
	private String iproportion; //档口分成
	private String istatistic;//部门属性
	
	private List<StoreDept> storeDepts; //门店部门
	
	public String getPk_storedept() {
		return pk_storedept;
	}
	public void setPk_storedept(String pk_storedept) {
		this.pk_storedept = pk_storedept;
	}
	public String getPk_groupdept() {
		return pk_groupdept;
	}
	public void setPk_groupdept(String pk_groupdept) {
		this.pk_groupdept = pk_groupdept;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getVinit() {
		return vinit;
	}
	public void setVinit(String vinit) {
		this.vinit = vinit;
	}
	public Integer getIsortno() {
		return isortno;
	}
	public void setIsortno(Integer isortno) {
		this.isortno = isortno;
	}
	public String getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(String enablestate) {
		this.enablestate = enablestate;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
	public List<StoreDept> getStoreDepts() {
		return storeDepts;
	}
	public void setStoreDepts(List<StoreDept> storeDepts) {
		this.storeDepts = storeDepts;
	}
	public String getIproportion() {
		return iproportion;
	}
	public void setIproportion(String iproportion) {
		this.iproportion = iproportion;
	}
	public String getIstatistic() {
		return istatistic;
	}
	public void setIstatistic(String istatistic) {
		this.istatistic = istatistic;
	}
	
	
	
}
