package com.choice.misboh.domain.store;

/**
 * 门店定义
 * @author ZGL
 *
 */
/**
 * @author Iceleaf
 *
 */
/**
 * @author Iceleaf
 *
 */
public class Store {
	private  String pk_store;
	private  String vcode;
	private  String vname;
	private  String vname2;
	private  String vname3;
	private  String vname4;
	private  String vname5;
	private  String vname6;
	private  Integer enablestate;
	private  String pk_brandid;
	private  String pk_marketid;
	private  String pk_businesstypeid;
	private  String pk_bohid;
	private  String pk_governorid;
	private  String pk_juridicalid;
	private  String vcity;
	private  String vcity2;
	private  String vcity3;
	private  String vcity4;
	private  String vcity5;
	private  String vcity6;
	private  String vmail;
	private  String vtel;
	private  String vaddress;
	private  String vaddress2;
	private  String vaddress3;
	private  String vaddress4;
	private  String vaddress5;
	private  String vaddress6;
	private  String vpost;
	private  String vstore_mgr;
	private  String vstore_mgr2;
	private  String vstore_mgr3;
	private  String vstore_mgr4;
	private  String vstore_mgr5;
	private  String vstore_mgr6;
	private  String vinit;
	private  String pk_storetypeid;
	private  String vdata_v;
	private  String tdata_vdate;
	private  String vbreakfaststw;
	private  String vbreakfastetw;
	private  String vbreakfaststwe;
	private  String vbreakfastetwe;
	private  String vluncheonstw;
	private  String vluncheonetw;
	private  String vluncheonstwe;
	private  String vluncheonetwe;
	private  String vdinnerstw;
	private  String vdinneretw;
	private  String vdinnerstwe;
	private  String vdinneretwe;
	private  String vscreentimeout;
	private  Integer ilimitquantity;
	private  String vclosetimeoutc;
	private  Integer iclosetimeout;
	private  String tstime;
	private  String tetime;
	private  String vreceiptheader;
	private  String vreceiptheader2;
	private  String vreceiptheader3;
	private  String vreceiptheader4;
	private  String vreceiptheader5;
	private  String vreceiptheader6;
	private  String vreceiptfooter;
	private  String vreceiptfooter2;
	private  String vreceiptfooter3;
	private  String vreceiptfooter4;
	private  String vreceiptfooter5;
	private  String vreceiptfooter6;
	private  Double dhalolimit;
	private  Double dsalelimit;
	private  Double dskimlimit;
	private  Integer ivoidcount;
	private  Double dreserve;
	private  String iinstallt;
	private  Integer iinstalls;
	private  String vistable;
	private  String vipdes;
	private  String pk_storeposid;
	private  Integer iport = 867;
	private  Integer iparapay;
	private  String vymdes;
	private  String vstoredese;
	private  String vstoredese2;
	private  String vstoredese3;
	private  String vstoredese4;
	private  String vstoredese5;
	private  String vstoredese6;
	private  String topentim;
	private  String tclosetim;
	private  Integer idelauditdays;
	private  Integer ispaces;
	private  Integer itblcnt;
	private  Integer iempcnt;
	private  String vycardnoline;
	private  String vfdpath;
	private  String vfapath;
	private  String vyresv;
	private  String visuse;
	private  String vyarea;
	private  String vbpkgamt;
	private  String vychknet;
	private  String vmemo;
	private  String vmemo2;
	private  String vmemo3;
	private  String vmemo4;
	private  String vmemo5;
	private  String vmemo6;
	private  String pk_org;
	private  String pk_group;
	private  String creator;
	private  String creationtime;
	private  String modifier;
	private  String modifiedtime;
	private  String vdef1;
	private  String vdef2;
	private  String vdef3;
	private  String vdef4;
	private  String vdef5;
	private  String vdef6;
	private  String vdef7;
	private  String vdef8;
	private  String vdef9;
	private  String vdef10;
	private  String vdef11;
	private  String vdef12;
	private  String vdef13;
	private  String vdef14;
	private  String vdef15;
	private  String vdef16;
	private  String vdef17;
	private  String vdef18;
	private  String vdef19;
	private  String vdef20;
	private  String pk_org_v;
	private  String vprnver;
	private  String vfirmprnver;
	private  String vprndownver;
	private  String dprndowntim;
	private  String vpayver;
	private  String vfirmpaynver;
	private  String vpaydownver;
	private  String dpaydowntim;
	private  String vprgid;
	private  String vprgvouno;
	private  String vprgver;
	private  String vdownver;
	private  String vfirmver;
	private  String ddowntim;
	private  String vplantyp = "PAX";
	private  String vnighteonstw;
	private  String vnighteonetw;
	private  String vnighteonstwe;
	private  String vnighteonetwe;
	private  String bisbreakfast;
	private  String bislunch;
	private  String bisdinner;
	private  String bisnight;
	private  String innercode;
	private  String pid;
	private  Integer maxpeople;
	private  Integer maxodd;
	private  String tsendlasttim;
	private  String vdept;
	private  String vpricecode;
	private  String vpricever;
	private  String vfirmpricever;
	private  String vpricedownver;
	private  String dpricedowntim;
	private  Integer dr = 0;
	private  String ts;
	private String pk_id;
	private String pk_sourcestore;

	private String pk_airdittype;//菜品BOM  成本卡类型 编码 code
	private String bomname;//菜品BOM  成本卡类型 名称 des
	private String pk_actstr;
	//套餐模式：1：老模式，2新模式，默认新模式
	private Integer itcoperationmode =2;
	//是否选中服务费
	private String fwfflag;
	//服务费类型
	private String pk_servicefeeset;
	//是否选中税金
	private String taxsetFlag;
	//税金类型
	private String pk_taxset;
	
	
	//早茶
	private String morningTea;
	//早茶开始时间
	private String morningTeastW;
	//早茶结束时间
	private String morningTeaetW;
	//周末早茶开始时间
	private String morningTeastWe;
	//周末早茶结束时间
	private String morningTeaetWe;
	//下午茶
	private String afternoonTea;
	//下午茶开始时间
	private String afternoonTeastW;
	//下午茶结束时间
	private String afternoonTeaetW;
	//周末下午茶开始时间
	private String afternoonTeastWe;
	//周末下午茶结束时间
	private String afternoonTeaetWe;
	
	//是否有台位设置
	private String haveSiteFlag;
	//盘点限制
	private String vInventoryContr;
	//是否可录入金额
	private String vInAmounts;
	//是否添加本日单据
	private String VADDToday;
	//是否虚拟物料Y/N
	private String vxnsupply;
	
	private String vfoodsign;//餐饮类型，1：中餐  2：快餐   3：webpos
	
	private Integer iisupvalid;//数据是否上传校验，0：否 1：是
	private Integer idaylimit;//天数限制
	
	private String visuseban ;//是否使用禁盘
	private String visfillreceipt ;//是否允许直接填写入库单
	private String visfillreprotlist ;//是否允许直接填写报货单
	private String visuseschedule ;//是否允许不适用配送班表
	private String visuseprediction ;//是否允许使用预估
	private String vischeck ;//日结是否控制盘点
	
	private String pk_city;  //城市主键
	
	private String pospassword; //pos密码
	private String pospwdmd5;   //pos密码md5算法
	private String pwdtime;     //pos密码设置时间
	
	private String vismodifyestimate;//门店是否允许修改预估值
	private String vistest; //是否测试门店
	
	private String copyitem; //定义存储要复制的项目的字符串
	
	private String visactualmoney;//录入实收金额 （0：不录入 1：录入 2：盲交）
	
	private String vaddsychactm;//新增时是否同步区域门店活动，Y为是，N为否，默认为N
	
	private String idList;          //账号所能查询到的分店                权限范围
	private String tele;            //订餐电话
	private String stlye;           //菜系
	private String businfo;         //公交信息
	private String seat;            //商家卡座
	private String equip;           //包间设施
	private String bigPic;          //分店大图
	private String smallpic;        //分店小图
	private String remark;          //备注
	private String feast;			//是否有婚宴服务
	private String meeting;			//是否有会议服务
	private String pk_area;  		//隶属城市地区
	private String pk_street;		//城市街道
	private Integer sortNum;		//排序号
	private String position;  		//经纬度
	private String longitude;       //经度
	private String latitude;        //纬度
	private String lunchEndTime;	//午餐最晚预定时间,订桌
	private String dinnerEndTime;	//晚餐最晚预定时间,订桌
	private String lunchStart;		//午餐自提开始时间,订餐
	private String lunchEnd;		//午餐自提结束时间,订餐
	private String dinnerStart;		//晚餐自提开始时间,订餐
	private String dinnerEnd;		//晚餐自提结束时间,订餐
	private String createTime;		//外卖菜品制作时间
	private String netshow;			//是否在网站前端显示，即门店是否开放     Y显示  N不显示
	private String ordershow;		//是否在订桌订餐页面选择门店是显示   Y显示  N不显示
	
	public String getVfoodsign() {
		return vfoodsign;
	}
	public void setVfoodsign(String vfoodsign) {
		this.vfoodsign = vfoodsign;
	}
	public String getHaveSiteFlag() {
		return haveSiteFlag;
	}
	public void setHaveSiteFlag(String haveSiteFlag) {
		this.haveSiteFlag = haveSiteFlag;
	}
	public String getTaxsetFlag() {
		return taxsetFlag;
	}
	public void setTaxsetFlag(String taxsetFlag) {
		this.taxsetFlag = taxsetFlag;
	}
	public String getPk_taxset() {
		return pk_taxset;
	}
	public void setPk_taxset(String pk_taxset) {
		this.pk_taxset = pk_taxset;
	}
	public String getFwfflag() {
		return fwfflag;
	}
	public void setFwfflag(String fwfflag) {
		this.fwfflag = fwfflag;
	}
	public String getPk_servicefeeset() {
		return pk_servicefeeset;
	}
	public void setPk_servicefeeset(String pk_servicefeeset) {
		this.pk_servicefeeset = pk_servicefeeset;
	}
	/**
	 * 属性pk_store的Getter方法.属性名：门店主键
	 * 创建日期:
	 * @return String
	 */
	
	public String getPk_sourcestore() {
		return pk_sourcestore;
	}
	public void setPk_sourcestore(String pk_sourcestore) {
		this.pk_sourcestore = pk_sourcestore;
	} 	  
	public String getPk_store () {
		return pk_store;
	}   
	/**
	 * 属性pk_store的Setter方法.属性名：门店主键
	 * 创建日期:
	 * @param Pk_store String
	 */
	public void setPk_store (String Pk_store ) {
		this.pk_store = Pk_store;
	} 	  
	/**
	 * 属性vcode的Getter方法.属性名：门店编码
	 * 创建日期:
	 * @return String
	 */
	public String getVcode () {
		return vcode;
	}   
	/**
	 * 属性vcode的Setter方法.属性名：门店编码
	 * 创建日期:
	 * @param Vcode String
	 */
	public void setVcode (String Vcode ) {
		this.vcode = Vcode;
	} 	  
	/**
	 * 属性vname的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVname () {
		return vname;
	}   
	/**
	 * 属性vname的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vname String
	 */
	public void setVname (String Vname ) {
		this.vname = Vname;
	} 	  
	/**
	 * 属性vname2的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVname2 () {
		return vname2;
	}   
	/**
	 * 属性vname2的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vname2 String
	 */
	public void setVname2 (String Vname2 ) {
		this.vname2 = Vname2;
	} 	  
	/**
	 * 属性vname3的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVname3 () {
		return vname3;
	}   
	/**
	 * 属性vname3的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vname3 String
	 */
	public void setVname3 (String Vname3 ) {
		this.vname3 = Vname3;
	} 	  
	/**
	 * 属性vname4的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVname4 () {
		return vname4;
	}   
	/**
	 * 属性vname4的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vname4 String
	 */
	public void setVname4 (String Vname4 ) {
		this.vname4 = Vname4;
	} 	  
	/**
	 * 属性vname5的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVname5 () {
		return vname5;
	}   
	/**
	 * 属性vname5的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vname5 String
	 */
	public void setVname5 (String Vname5 ) {
		this.vname5 = Vname5;
	} 	  
	/**
	 * 属性vname6的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVname6 () {
		return vname6;
	}   
	/**
	 * 属性vname6的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vname6 String
	 */
	public void setVname6 (String Vname6 ) {
		this.vname6 = Vname6;
	} 	  
	/**
	 * 属性enablestate的Getter方法.属性名：启用状态
	 * 创建日期:
	 * @return Integer
	 */
	public Integer getEnablestate () {
		return enablestate;
	}   
	/**
	 * 属性enablestate的Setter方法.属性名：启用状态
	 * 创建日期:
	 * @param Enablestate Integer
	 */
	public void setEnablestate (Integer Enablestate ) {
		this.enablestate = Enablestate;
	} 	  
	/**
	 * 属性pk_brandid的Getter方法.属性名：所属品牌
	 * 创建日期:
	 * @return String
	 */
	public String getPk_brandid () {
		return pk_brandid;
	}   
	/**
	 * 属性pk_brandid的Setter方法.属性名：所属品牌
	 * 创建日期:
	 * @param Pk_brandid String
	 */
	public void setPk_brandid (String Pk_brandid ) {
		this.pk_brandid = Pk_brandid;
	} 	  
	/**
	 * 属性pk_marketid的Getter方法.属性名：所属市场
	 * 创建日期:
	 * @return String
	 */
	public String getPk_marketid () {
		return pk_marketid;
	}   
	/**
	 * 属性pk_marketid的Setter方法.属性名：所属市场
	 * 创建日期:
	 * @param Pk_marketid String
	 */
	public void setPk_marketid (String Pk_marketid ) {
		this.pk_marketid = Pk_marketid;
	} 	  
	/**
	 * 属性pk_businesstypeid的Getter方法.属性名：所属商圈
	 * 创建日期:
	 * @return String
	 */
	public String getPk_businesstypeid () {
		return pk_businesstypeid;
	}   
	/**
	 * 属性pk_businesstypeid的Setter方法.属性名：所属商圈
	 * 创建日期:
	 * @param Pk_businesstypeid String
	 */
	public void setPk_businesstypeid (String Pk_businesstypeid ) {
		this.pk_businesstypeid = Pk_businesstypeid;
	} 	  
	/**
	 * 属性pk_bohid的Getter方法.属性名：所属运营区
	 * 创建日期:
	 * @return String
	 */
	public String getPk_bohid () {
		return pk_bohid;
	}   
	/**
	 * 属性pk_bohid的Setter方法.属性名：所属运营区
	 * 创建日期:
	 * @param Pk_bohid String
	 */
	public void setPk_bohid (String Pk_bohid ) {
		this.pk_bohid = Pk_bohid;
	} 	  
	/**
	 * 属性pk_governorid的Getter方法.属性名：所属督导区
	 * 创建日期:
	 * @return String
	 */
	public String getPk_governorid () {
		return pk_governorid;
	}   
	/**
	 * 属性pk_governorid的Setter方法.属性名：所属督导区
	 * 创建日期:
	 * @param Pk_governorid String
	 */
	public void setPk_governorid (String Pk_governorid ) {
		this.pk_governorid = Pk_governorid;
	} 	  
	/**
	 * 属性pk_juridicalid的Getter方法.属性名：所属法人
	 * 创建日期:
	 * @return String
	 */
	public String getPk_juridicalid () {
		return pk_juridicalid;
	}   
	/**
	 * 属性pk_juridicalid的Setter方法.属性名：所属法人
	 * 创建日期:
	 * @param Pk_juridicalid String
	 */
	public void setPk_juridicalid (String Pk_juridicalid ) {
		this.pk_juridicalid = Pk_juridicalid;
	} 	  
	/**
	 * 属性vcity的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVcity () {
		return vcity;
	}   
	/**
	 * 属性vcity的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vcity String
	 */
	public void setVcity (String Vcity ) {
		this.vcity = Vcity;
	} 	  
	/**
	 * 属性vcity2的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVcity2 () {
		return vcity2;
	}   
	/**
	 * 属性vcity2的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vcity2 String
	 */
	public void setVcity2 (String Vcity2 ) {
		this.vcity2 = Vcity2;
	} 	  
	/**
	 * 属性vcity3的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVcity3 () {
		return vcity3;
	}   
	/**
	 * 属性vcity3的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vcity3 String
	 */
	public void setVcity3 (String Vcity3 ) {
		this.vcity3 = Vcity3;
	} 	  
	/**
	 * 属性vcity4的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVcity4 () {
		return vcity4;
	}   
	/**
	 * 属性vcity4的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vcity4 String
	 */
	public void setVcity4 (String Vcity4 ) {
		this.vcity4 = Vcity4;
	} 	  
	/**
	 * 属性vcity5的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVcity5 () {
		return vcity5;
	}   
	/**
	 * 属性vcity5的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vcity5 String
	 */
	public void setVcity5 (String Vcity5 ) {
		this.vcity5 = Vcity5;
	} 	  
	/**
	 * 属性vcity6的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVcity6 () {
		return vcity6;
	}   
	/**
	 * 属性vcity6的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vcity6 String
	 */
	public void setVcity6 (String Vcity6 ) {
		this.vcity6 = Vcity6;
	} 	  
	/**
	 * 属性vmail的Getter方法.属性名：邮箱
	 * 创建日期:
	 * @return String
	 */
	public String getVmail () {
		return vmail;
	}   
	/**
	 * 属性vmail的Setter方法.属性名：邮箱
	 * 创建日期:
	 * @param Vmail String
	 */
	public void setVmail (String Vmail ) {
		this.vmail = Vmail;
	} 	  
	/**
	 * 属性vtel的Getter方法.属性名：电话
	 * 创建日期:
	 * @return String
	 */
	public String getVtel () {
		return vtel;
	}   
	/**
	 * 属性vtel的Setter方法.属性名：电话
	 * 创建日期:
	 * @param Vtel String
	 */
	public void setVtel (String Vtel ) {
		this.vtel = Vtel;
	} 	  
	/**
	 * 属性vaddress的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVaddress () {
		return vaddress;
	}   
	/**
	 * 属性vaddress的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vaddress String
	 */
	public void setVaddress (String Vaddress ) {
		this.vaddress = Vaddress;
	} 	  
	/**
	 * 属性vaddress2的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVaddress2 () {
		return vaddress2;
	}   
	/**
	 * 属性vaddress2的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vaddress2 String
	 */
	public void setVaddress2 (String Vaddress2 ) {
		this.vaddress2 = Vaddress2;
	} 	  
	/**
	 * 属性vaddress3的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVaddress3 () {
		return vaddress3;
	}   
	/**
	 * 属性vaddress3的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vaddress3 String
	 */
	public void setVaddress3 (String Vaddress3 ) {
		this.vaddress3 = Vaddress3;
	} 	  
	/**
	 * 属性vaddress4的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVaddress4 () {
		return vaddress4;
	}   
	/**
	 * 属性vaddress4的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vaddress4 String
	 */
	public void setVaddress4 (String Vaddress4 ) {
		this.vaddress4 = Vaddress4;
	} 	  
	/**
	 * 属性vaddress5的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVaddress5 () {
		return vaddress5;
	}   
	/**
	 * 属性vaddress5的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vaddress5 String
	 */
	public void setVaddress5 (String Vaddress5 ) {
		this.vaddress5 = Vaddress5;
	} 	  
	/**
	 * 属性vaddress6的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVaddress6 () {
		return vaddress6;
	}   
	/**
	 * 属性vaddress6的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vaddress6 String
	 */
	public void setVaddress6 (String Vaddress6 ) {
		this.vaddress6 = Vaddress6;
	} 	  
	/**
	 * 属性vpost的Getter方法.属性名：邮编
	 * 创建日期:
	 * @return String
	 */
	public String getVpost () {
		return vpost;
	}   
	/**
	 * 属性vpost的Setter方法.属性名：邮编
	 * 创建日期:
	 * @param Vpost String
	 */
	public void setVpost (String Vpost ) {
		this.vpost = Vpost;
	} 	  
	/**
	 * 属性vstore_mgr的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVstore_mgr () {
		return vstore_mgr;
	}   
	/**
	 * 属性vstore_mgr的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vstore_mgr String
	 */
	public void setVstore_mgr (String Vstore_mgr ) {
		this.vstore_mgr = Vstore_mgr;
	} 	  
	/**
	 * 属性vstore_mgr2的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVstore_mgr2 () {
		return vstore_mgr2;
	}   
	/**
	 * 属性vstore_mgr2的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vstore_mgr2 String
	 */
	public void setVstore_mgr2 (String Vstore_mgr2 ) {
		this.vstore_mgr2 = Vstore_mgr2;
	} 	  
	/**
	 * 属性vstore_mgr3的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVstore_mgr3 () {
		return vstore_mgr3;
	}   
	/**
	 * 属性vstore_mgr3的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vstore_mgr3 String
	 */
	public void setVstore_mgr3 (String Vstore_mgr3 ) {
		this.vstore_mgr3 = Vstore_mgr3;
	} 	  
	/**
	 * 属性vstore_mgr4的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVstore_mgr4 () {
		return vstore_mgr4;
	}   
	/**
	 * 属性vstore_mgr4的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vstore_mgr4 String
	 */
	public void setVstore_mgr4 (String Vstore_mgr4 ) {
		this.vstore_mgr4 = Vstore_mgr4;
	} 	  
	/**
	 * 属性vstore_mgr5的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVstore_mgr5 () {
		return vstore_mgr5;
	}   
	/**
	 * 属性vstore_mgr5的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vstore_mgr5 String
	 */
	public void setVstore_mgr5 (String Vstore_mgr5 ) {
		this.vstore_mgr5 = Vstore_mgr5;
	} 	  
	/**
	 * 属性vstore_mgr6的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVstore_mgr6 () {
		return vstore_mgr6;
	}   
	/**
	 * 属性vstore_mgr6的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vstore_mgr6 String
	 */
	public void setVstore_mgr6 (String Vstore_mgr6 ) {
		this.vstore_mgr6 = Vstore_mgr6;
	} 	  
	/**
	 * 属性vinit的Getter方法.属性名：缩写
	 * 创建日期:
	 * @return String
	 */
	public String getVinit () {
		return vinit;
	}   
	/**
	 * 属性vinit的Setter方法.属性名：缩写
	 * 创建日期:
	 * @param Vinit String
	 */
	public void setVinit (String Vinit ) {
		this.vinit = Vinit;
	} 	  
	/**
	 * 属性pk_storetypeid的Getter方法.属性名：餐厅类型
	 * 创建日期:
	 * @return String
	 */
	public String getPk_storetypeid () {
		return pk_storetypeid;
	}   
	/**
	 * 属性pk_storetypeid的Setter方法.属性名：餐厅类型
	 * 创建日期:
	 * @param Pk_storetypeid String
	 */
	public void setPk_storetypeid (String Pk_storetypeid ) {
		this.pk_storetypeid = Pk_storetypeid;
	} 	  
	/**
	 * 属性vdata_v的Getter方法.属性名：数据版本号
	 * 创建日期:
	 * @return String
	 */
	public String getVdata_v () {
		return vdata_v;
	}   
	/**
	 * 属性vdata_v的Setter方法.属性名：数据版本号
	 * 创建日期:
	 * @param Vdata_v String
	 */
	public void setVdata_v (String Vdata_v ) {
		this.vdata_v = Vdata_v;
	} 	  
	/**
	 * 属性tdata_vdate的Getter方法.属性名：数据版本发布时间
	 * 创建日期:
	 * @return String
	 */
	public String getTdata_vdate () {
		return tdata_vdate;
	}   
	/**
	 * 属性tdata_vdate的Setter方法.属性名：数据版本发布时间
	 * 创建日期:
	 * @param Tdata_vdate String
	 */
	public void setTdata_vdate (String Tdata_vdate ) {
		this.tdata_vdate = Tdata_vdate;
	} 	  
	/**
	 * 属性vbreakfaststw的Getter方法.属性名：早餐开始时间
	 * 创建日期:
	 * @return String
	 */
	public String getVbreakfaststw () {
		return vbreakfaststw;
	}   
	/**
	 * 属性vbreakfaststw的Setter方法.属性名：早餐开始时间
	 * 创建日期:
	 * @param Vbreakfaststw String
	 */
	public void setVbreakfaststw (String Vbreakfaststw ) {
		this.vbreakfaststw = Vbreakfaststw;
	} 	  
	/**
	 * 属性vbreakfastetw的Getter方法.属性名：早餐结束时间
	 * 创建日期:
	 * @return String
	 */
	public String getVbreakfastetw () {
		return vbreakfastetw;
	}   
	/**
	 * 属性vbreakfastetw的Setter方法.属性名：早餐结束时间
	 * 创建日期:
	 * @param Vbreakfastetw String
	 */
	public void setVbreakfastetw (String Vbreakfastetw ) {
		this.vbreakfastetw = Vbreakfastetw;
	} 	  
	/**
	 * 属性vbreakfaststwe的Getter方法.属性名：周末早餐开始时间
	 * 创建日期:
	 * @return String
	 */
	public String getVbreakfaststwe () {
		return vbreakfaststwe;
	}   
	/**
	 * 属性vbreakfaststwe的Setter方法.属性名：周末早餐开始时间
	 * 创建日期:
	 * @param Vbreakfaststwe String
	 */
	public void setVbreakfaststwe (String Vbreakfaststwe ) {
		this.vbreakfaststwe = Vbreakfaststwe;
	} 	  
	/**
	 * 属性vbreakfastetwe的Getter方法.属性名：周末早餐结束时间
	 * 创建日期:
	 * @return String
	 */
	public String getVbreakfastetwe () {
		return vbreakfastetwe;
	}   
	/**
	 * 属性vbreakfastetwe的Setter方法.属性名：周末早餐结束时间
	 * 创建日期:
	 * @param Vbreakfastetwe String
	 */
	public void setVbreakfastetwe (String Vbreakfastetwe ) {
		this.vbreakfastetwe = Vbreakfastetwe;
	} 	  
	/**
	 * 属性vluncheonstw的Getter方法.属性名：午餐开始时间
	 * 创建日期:
	 * @return String
	 */
	public String getVluncheonstw () {
		return vluncheonstw;
	}   
	/**
	 * 属性vluncheonstw的Setter方法.属性名：午餐开始时间
	 * 创建日期:
	 * @param Vluncheonstw String
	 */
	public void setVluncheonstw (String Vluncheonstw ) {
		this.vluncheonstw = Vluncheonstw;
	} 	  
	/**
	 * 属性vluncheonetw的Getter方法.属性名：午餐结束时间
	 * 创建日期:
	 * @return String
	 */
	public String getVluncheonetw () {
		return vluncheonetw;
	}   
	/**
	 * 属性vluncheonetw的Setter方法.属性名：午餐结束时间
	 * 创建日期:
	 * @param Vluncheonetw String
	 */
	public void setVluncheonetw (String Vluncheonetw ) {
		this.vluncheonetw = Vluncheonetw;
	} 	  
	/**
	 * 属性vluncheonstwe的Getter方法.属性名：周末午餐开始时间
	 * 创建日期:
	 * @return String
	 */
	public String getVluncheonstwe () {
		return vluncheonstwe;
	}   
	/**
	 * 属性vluncheonstwe的Setter方法.属性名：周末午餐开始时间
	 * 创建日期:
	 * @param Vluncheonstwe String
	 */
	public void setVluncheonstwe (String Vluncheonstwe ) {
		this.vluncheonstwe = Vluncheonstwe;
	} 	  
	/**
	 * 属性vluncheonetwe的Getter方法.属性名：周末午餐结束时间
	 * 创建日期:
	 * @return String
	 */
	public String getVluncheonetwe () {
		return vluncheonetwe;
	}   
	/**
	 * 属性vluncheonetwe的Setter方法.属性名：周末午餐结束时间
	 * 创建日期:
	 * @param Vluncheonetwe String
	 */
	public void setVluncheonetwe (String Vluncheonetwe ) {
		this.vluncheonetwe = Vluncheonetwe;
	} 	  
	/**
	 * 属性vdinnerstw的Getter方法.属性名：晚餐开始时间
	 * 创建日期:
	 * @return String
	 */
	public String getVdinnerstw () {
		return vdinnerstw;
	}   
	/**
	 * 属性vdinnerstw的Setter方法.属性名：晚餐开始时间
	 * 创建日期:
	 * @param Vdinnerstw String
	 */
	public void setVdinnerstw (String Vdinnerstw ) {
		this.vdinnerstw = Vdinnerstw;
	} 	  
	/**
	 * 属性vdinneretw的Getter方法.属性名：晚餐结束时间
	 * 创建日期:
	 * @return String
	 */
	public String getVdinneretw () {
		return vdinneretw;
	}   
	/**
	 * 属性vdinneretw的Setter方法.属性名：晚餐结束时间
	 * 创建日期:
	 * @param Vdinneretw String
	 */
	public void setVdinneretw (String Vdinneretw ) {
		this.vdinneretw = Vdinneretw;
	} 	  
	/**
	 * 属性vdinnerstwe的Getter方法.属性名：周末晚餐开始时间
	 * 创建日期:
	 * @return String
	 */
	public String getVdinnerstwe () {
		return vdinnerstwe;
	}   
	/**
	 * 属性vdinnerstwe的Setter方法.属性名：周末晚餐开始时间
	 * 创建日期:
	 * @param Vdinnerstwe String
	 */
	public void setVdinnerstwe (String Vdinnerstwe ) {
		this.vdinnerstwe = Vdinnerstwe;
	} 	  
	/**
	 * 属性vdinneretwe的Getter方法.属性名：周末晚餐结束时间
	 * 创建日期:
	 * @return String
	 */
	public String getVdinneretwe () {
		return vdinneretwe;
	}   
	/**
	 * 属性vdinneretwe的Setter方法.属性名：周末晚餐结束时间
	 * 创建日期:
	 * @param Vdinneretwe String
	 */
	public void setVdinneretwe (String Vdinneretwe ) {
		this.vdinneretwe = Vdinneretwe;
	} 	  
	/**
	 * 属性vscreentimeout的Getter方法.属性名：自动锁定（秒）
	 * 创建日期:
	 * @return String
	 */
	public String getVscreentimeout () {
		return vscreentimeout;
	}   
	/**
	 * 属性vscreentimeout的Setter方法.属性名：自动锁定（秒）
	 * 创建日期:
	 * @param Vscreentimeout String
	 */
	public void setVscreentimeout (String Vscreentimeout ) {
		this.vscreentimeout = Vscreentimeout;
	} 	  
	/**
	 * 属性ilimitquantity的Getter方法.属性名：点菜数量上限
	 * 创建日期:
	 * @return Integer
	 */
	public Integer getIlimitquantity () {
		return ilimitquantity;
	}   
	/**
	 * 属性ilimitquantity的Setter方法.属性名：点菜数量上限
	 * 创建日期:
	 * @param Ilimitquantity Integer
	 */
	public void setIlimitquantity (Integer Ilimitquantity ) {
		this.ilimitquantity = Ilimitquantity;
	} 	  
	/**
	 * 属性vclosetimeoutc的Getter方法.属性名：封台计时超时颜色
	 * 创建日期:
	 * @return String
	 */
	public String getVclosetimeoutc () {
		return vclosetimeoutc;
	}   
	/**
	 * 属性vclosetimeoutc的Setter方法.属性名：封台计时超时颜色
	 * 创建日期:
	 * @param Vclosetimeoutc String
	 */
	public void setVclosetimeoutc (String Vclosetimeoutc ) {
		this.vclosetimeoutc = Vclosetimeoutc;
	} 	  
	/**
	 * 属性iclosetimeout的Getter方法.属性名：封台计时超时（秒）
	 * 创建日期:
	 * @return Integer
	 */
	public Integer getIclosetimeout () {
		return iclosetimeout;
	}   
	/**
	 * 属性iclosetimeout的Setter方法.属性名：封台计时超时（秒）
	 * 创建日期:
	 * @param Iclosetimeout Integer
	 */
	public void setIclosetimeout (Integer Iclosetimeout ) {
		this.iclosetimeout = Iclosetimeout;
	} 	  
	/**
	 * 属性tstime的Getter方法.属性名：门店开业时间
	 * 创建日期:
	 * @return String
	 */
	public String getTstime () {
		return tstime;
	}   
	/**
	 * 属性tstime的Setter方法.属性名：门店开业时间
	 * 创建日期:
	 * @param Tstime String
	 */
	public void setTstime (String Tstime ) {
		this.tstime = Tstime;
	} 	  
	/**
	 * 属性tetime的Getter方法.属性名：门店停业时间
	 * 创建日期:
	 * @return String
	 */
	public String getTetime () {
		return tetime;
	}   
	/**
	 * 属性tetime的Setter方法.属性名：门店停业时间
	 * 创建日期:
	 * @param Tetime String
	 */
	public void setTetime (String Tetime ) {
		this.tetime = Tetime;
	} 	  
	/**
	 * 属性vreceiptheader的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVreceiptheader () {
		return vreceiptheader;
	}   
	/**
	 * 属性vreceiptheader的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vreceiptheader String
	 */
	public void setVreceiptheader (String Vreceiptheader ) {
		this.vreceiptheader = Vreceiptheader;
	} 	  
	/**
	 * 属性vreceiptheader2的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVreceiptheader2 () {
		return vreceiptheader2;
	}   
	/**
	 * 属性vreceiptheader2的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vreceiptheader2 String
	 */
	public void setVreceiptheader2 (String Vreceiptheader2 ) {
		this.vreceiptheader2 = Vreceiptheader2;
	} 	  
	/**
	 * 属性vreceiptheader3的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVreceiptheader3 () {
		return vreceiptheader3;
	}   
	/**
	 * 属性vreceiptheader3的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vreceiptheader3 String
	 */
	public void setVreceiptheader3 (String Vreceiptheader3 ) {
		this.vreceiptheader3 = Vreceiptheader3;
	} 	  
	/**
	 * 属性vreceiptheader4的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVreceiptheader4 () {
		return vreceiptheader4;
	}   
	/**
	 * 属性vreceiptheader4的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vreceiptheader4 String
	 */
	public void setVreceiptheader4 (String Vreceiptheader4 ) {
		this.vreceiptheader4 = Vreceiptheader4;
	} 	  
	/**
	 * 属性vreceiptheader5的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVreceiptheader5 () {
		return vreceiptheader5;
	}   
	/**
	 * 属性vreceiptheader5的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vreceiptheader5 String
	 */
	public void setVreceiptheader5 (String Vreceiptheader5 ) {
		this.vreceiptheader5 = Vreceiptheader5;
	} 	  
	/**
	 * 属性vreceiptheader6的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVreceiptheader6 () {
		return vreceiptheader6;
	}   
	/**
	 * 属性vreceiptheader6的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vreceiptheader6 String
	 */
	public void setVreceiptheader6 (String Vreceiptheader6 ) {
		this.vreceiptheader6 = Vreceiptheader6;
	} 	  
	/**
	 * 属性vreceiptfooter的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVreceiptfooter () {
		return vreceiptfooter;
	}   
	/**
	 * 属性vreceiptfooter的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vreceiptfooter String
	 */
	public void setVreceiptfooter (String Vreceiptfooter ) {
		this.vreceiptfooter = Vreceiptfooter;
	} 	  
	/**
	 * 属性vreceiptfooter2的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVreceiptfooter2 () {
		return vreceiptfooter2;
	}   
	/**
	 * 属性vreceiptfooter2的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vreceiptfooter2 String
	 */
	public void setVreceiptfooter2 (String Vreceiptfooter2 ) {
		this.vreceiptfooter2 = Vreceiptfooter2;
	} 	  
	/**
	 * 属性vreceiptfooter3的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVreceiptfooter3 () {
		return vreceiptfooter3;
	}   
	/**
	 * 属性vreceiptfooter3的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vreceiptfooter3 String
	 */
	public void setVreceiptfooter3 (String Vreceiptfooter3 ) {
		this.vreceiptfooter3 = Vreceiptfooter3;
	} 	  
	/**
	 * 属性vreceiptfooter4的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVreceiptfooter4 () {
		return vreceiptfooter4;
	}   
	/**
	 * 属性vreceiptfooter4的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vreceiptfooter4 String
	 */
	public void setVreceiptfooter4 (String Vreceiptfooter4 ) {
		this.vreceiptfooter4 = Vreceiptfooter4;
	} 	  
	/**
	 * 属性vreceiptfooter5的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVreceiptfooter5 () {
		return vreceiptfooter5;
	}   
	/**
	 * 属性vreceiptfooter5的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vreceiptfooter5 String
	 */
	public void setVreceiptfooter5 (String Vreceiptfooter5 ) {
		this.vreceiptfooter5 = Vreceiptfooter5;
	} 	  
	/**
	 * 属性vreceiptfooter6的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVreceiptfooter6 () {
		return vreceiptfooter6;
	}   
	/**
	 * 属性vreceiptfooter6的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vreceiptfooter6 String
	 */
	public void setVreceiptfooter6 (String Vreceiptfooter6 ) {
		this.vreceiptfooter6 = Vreceiptfooter6;
	} 	  
	/**
	 * 属性dhalolimit的Getter方法.属性名：点菜金额上限（元）
	 * 创建日期:
	 * @return Double
	 */
	public Double getDhalolimit () {
		return dhalolimit;
	}   
	/**
	 * 属性dhalolimit的Setter方法.属性名：点菜金额上限（元）
	 * 创建日期:
	 * @param Dhalolimit Double
	 */
	public void setDhalolimit (Double Dhalolimit ) {
		this.dhalolimit = Dhalolimit;
	} 	  
	/**
	 * 属性dsalelimit的Getter方法.属性名：收款上限（元）
	 * 创建日期:
	 * @return Double
	 */
	public Double getDsalelimit () {
		return dsalelimit;
	}   
	/**
	 * 属性dsalelimit的Setter方法.属性名：收款上限（元）
	 * 创建日期:
	 * @param Dsalelimit Double
	 */
	public void setDsalelimit (Double Dsalelimit ) {
		this.dsalelimit = Dsalelimit;
	} 	  
	/**
	 * 属性dskimlimit的Getter方法.属性名：抽大钞上限（元）
	 * 创建日期:
	 * @return Double
	 */
	public Double getDskimlimit () {
		return dskimlimit;
	}   
	/**
	 * 属性dskimlimit的Setter方法.属性名：抽大钞上限（元）
	 * 创建日期:
	 * @param Dskimlimit Double
	 */
	public void setDskimlimit (Double Dskimlimit ) {
		this.dskimlimit = Dskimlimit;
	} 	  
	/**
	 * 属性ivoidcount的Getter方法.属性名：删除菜品上限
	 * 创建日期:
	 * @return Integer
	 */
	public Integer getIvoidcount () {
		return ivoidcount;
	}   
	/**
	 * 属性ivoidcount的Setter方法.属性名：删除菜品上限
	 * 创建日期:
	 * @param Ivoidcount Integer
	 */
	public void setIvoidcount (Integer Ivoidcount ) {
		this.ivoidcount = Ivoidcount;
	} 	  
	/**
	 * 属性dreserve的Getter方法.属性名：默认零找金（元）
	 * 创建日期:
	 * @return Double
	 */
	public Double getDreserve () {
		return dreserve;
	}   
	/**
	 * 属性dreserve的Setter方法.属性名：默认零找金（元）
	 * 创建日期:
	 * @param Dreserve Double
	 */
	public void setDreserve (Double Dreserve ) {
		this.dreserve = Dreserve;
	} 	  
	/**
	 * 属性iinstallt的Getter方法.属性名：安装时间
	 * 创建日期:
	 * @return String
	 */
	public String getIinstallt () {
		return iinstallt;
	}   
	/**
	 * 属性iinstallt的Setter方法.属性名：安装时间
	 * 创建日期:
	 * @param Iinstallt String
	 */
	public void setIinstallt (String Iinstallt ) {
		this.iinstallt = Iinstallt;
	} 	  
	/**
	 * 属性iinstalls的Getter方法.属性名：安装状态
	 * 创建日期:
	 * @return Integer
	 */
	public Integer getIinstalls () {
		return iinstalls;
	}   
	/**
	 * 属性iinstalls的Setter方法.属性名：安装状态
	 * 创建日期:
	 * @param Iinstalls Integer
	 */
	public void setIinstalls (Integer Iinstalls ) {
		this.iinstalls = Iinstalls;
	} 	  
	/**
	 * 属性vistable的Getter方法.属性名：是否启用台位管理
	 * 创建日期:
	 * @return String
	 */
	public String getVistable () {
		return vistable;
	}   
	/**
	 * 属性vistable的Setter方法.属性名：是否启用台位管理
	 * 创建日期:
	 * @param Vistable String
	 */
	public void setVistable (String Vistable ) {
		this.vistable = Vistable;
	} 	  
	/**
	 * 属性vipdes的Getter方法.属性名：IP地址
	 * 创建日期:
	 * @return String
	 */
	public String getVipdes () {
		return vipdes;
	}   
	/**
	 * 属性vipdes的Setter方法.属性名：IP地址
	 * 创建日期:
	 * @param Vipdes String
	 */
	public void setVipdes (String Vipdes ) {
		this.vipdes = Vipdes;
	} 	  
	/**
	 * 属性pk_storeposid的Getter方法.属性名：门店营业点
	 * 创建日期:
	 * @return String
	 */
	public String getPk_storeposid () {
		return pk_storeposid;
	}   
	/**
	 * 属性pk_storeposid的Setter方法.属性名：门店营业点
	 * 创建日期:
	 * @param Pk_storeposid String
	 */
	public void setPk_storeposid (String Pk_storeposid ) {
		this.pk_storeposid = Pk_storeposid;
	} 	  
	/**
	 * 属性iport的Getter方法.属性名：门店端口号
	 * 创建日期:
	 * @return Integer
	 */
	public Integer getIport () {
		return iport;
	}   
	/**
	 * 属性iport的Setter方法.属性名：门店端口号
	 * 创建日期:
	 * @param Iport Integer
	 */
	public void setIport (Integer Iport ) {
		this.iport = Iport;
	} 	  
	/**
	 * 属性iparapay的Getter方法.属性名：清机发票比率（%）
	 * 创建日期:
	 * @return Integer
	 */
	public Integer getIparapay () {
		return iparapay;
	}   
	/**
	 * 属性iparapay的Setter方法.属性名：清机发票比率（%）
	 * 创建日期:
	 * @param Iparapay Integer
	 */
	public void setIparapay (Integer Iparapay ) {
		this.iparapay = Iparapay;
	} 	  
	/**
	 * 属性vymdes的Getter方法.属性名：门店域名
	 * 创建日期:
	 * @return String
	 */
	public String getVymdes () {
		return vymdes;
	}   
	/**
	 * 属性vymdes的Setter方法.属性名：门店域名
	 * 创建日期:
	 * @param Vymdes String
	 */
	public void setVymdes (String Vymdes ) {
		this.vymdes = Vymdes;
	} 	  
	/**
	 * 属性vstoredese的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVstoredese () {
		return vstoredese;
	}   
	/**
	 * 属性vstoredese的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vstoredese String
	 */
	public void setVstoredese (String Vstoredese ) {
		this.vstoredese = Vstoredese;
	} 	  
	/**
	 * 属性vstoredese2的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVstoredese2 () {
		return vstoredese2;
	}   
	/**
	 * 属性vstoredese2的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vstoredese2 String
	 */
	public void setVstoredese2 (String Vstoredese2 ) {
		this.vstoredese2 = Vstoredese2;
	} 	  
	/**
	 * 属性vstoredese3的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVstoredese3 () {
		return vstoredese3;
	}   
	/**
	 * 属性vstoredese3的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vstoredese3 String
	 */
	public void setVstoredese3 (String Vstoredese3 ) {
		this.vstoredese3 = Vstoredese3;
	} 	  
	/**
	 * 属性vstoredese4的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVstoredese4 () {
		return vstoredese4;
	}   
	/**
	 * 属性vstoredese4的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vstoredese4 String
	 */
	public void setVstoredese4 (String Vstoredese4 ) {
		this.vstoredese4 = Vstoredese4;
	} 	  
	/**
	 * 属性vstoredese5的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVstoredese5 () {
		return vstoredese5;
	}   
	/**
	 * 属性vstoredese5的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vstoredese5 String
	 */
	public void setVstoredese5 (String Vstoredese5 ) {
		this.vstoredese5 = Vstoredese5;
	} 	  
	/**
	 * 属性vstoredese6的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVstoredese6 () {
		return vstoredese6;
	}   
	/**
	 * 属性vstoredese6的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vstoredese6 String
	 */
	public void setVstoredese6 (String Vstoredese6 ) {
		this.vstoredese6 = Vstoredese6;
	} 	  
	/**
	 * 属性topentim的Getter方法.属性名：营业开始时间
	 * 创建日期:
	 * @return String
	 */
	public String getTopentim () {
		return topentim;
	}   
	/**
	 * 属性topentim的Setter方法.属性名：营业开始时间
	 * 创建日期:
	 * @param Topentim String
	 */
	public void setTopentim (String Topentim ) {
		this.topentim = Topentim;
	} 	  
	/**
	 * 属性tclosetim的Getter方法.属性名：营业结束时间
	 * 创建日期:
	 * @return String
	 */
	public String getTclosetim () {
		return tclosetim;
	}   
	/**
	 * 属性tclosetim的Setter方法.属性名：营业结束时间
	 * 创建日期:
	 * @param Tclosetim String
	 */
	public void setTclosetim (String Tclosetim ) {
		this.tclosetim = Tclosetim;
	} 	  
	/**
	 * 属性idelauditdays的Getter方法.属性名：提前清除天数
	 * 创建日期:
	 * @return Integer
	 */
	public Integer getIdelauditdays () {
		return idelauditdays;
	}   
	/**
	 * 属性idelauditdays的Setter方法.属性名：提前清除天数
	 * 创建日期:
	 * @param Idelauditdays Integer
	 */
	public void setIdelauditdays (Integer Idelauditdays ) {
		this.idelauditdays = Idelauditdays;
	} 	  
	/**
	 * 属性ispaces的Getter方法.属性名：门店面积
	 * 创建日期:
	 * @return Integer
	 */
	public Integer getIspaces () {
		return ispaces;
	}   
	/**
	 * 属性ispaces的Setter方法.属性名：门店面积
	 * 创建日期:
	 * @param Ispaces Integer
	 */
	public void setIspaces (Integer Ispaces ) {
		this.ispaces = Ispaces;
	} 	  
	/**
	 * 属性itblcnt的Getter方法.属性名：餐位数
	 * 创建日期:
	 * @return Integer
	 */
	public Integer getItblcnt () {
		return itblcnt;
	}   
	/**
	 * 属性itblcnt的Setter方法.属性名：餐位数
	 * 创建日期:
	 * @param Itblcnt Integer
	 */
	public void setItblcnt (Integer Itblcnt ) {
		this.itblcnt = Itblcnt;
	} 	  
	/**
	 * 属性iempcnt的Getter方法.属性名：门店员工数
	 * 创建日期:
	 * @return Integer
	 */
	public Integer getIempcnt () {
		return iempcnt;
	}   
	/**
	 * 属性iempcnt的Setter方法.属性名：门店员工数
	 * 创建日期:
	 * @param Iempcnt Integer
	 */
	public void setIempcnt (Integer Iempcnt ) {
		this.iempcnt = Iempcnt;
	} 	  
	/**
	 * 属性vycardnoline的Getter方法.属性名：是否允许离线消费
	 * 创建日期:
	 * @return String
	 */
	public String getVycardnoline () {
		return vycardnoline;
	}   
	/**
	 * 属性vycardnoline的Setter方法.属性名：是否允许离线消费
	 * 创建日期:
	 * @param Vycardnoline String
	 */
	public void setVycardnoline (String Vycardnoline ) {
		this.vycardnoline = Vycardnoline;
	} 	  
	/**
	 * 属性vfdpath的Getter方法.属性名：FOODBEV路径
	 * 创建日期:
	 * @return String
	 */
	public String getVfdpath () {
		return vfdpath;
	}   
	/**
	 * 属性vfdpath的Setter方法.属性名：FOODBEV路径
	 * 创建日期:
	 * @param Vfdpath String
	 */
	public void setVfdpath (String Vfdpath ) {
		this.vfdpath = Vfdpath;
	} 	  
	/**
	 * 属性vfapath的Getter方法.属性名：FBAUDIT路径
	 * 创建日期:
	 * @return String
	 */
	public String getVfapath () {
		return vfapath;
	}   
	/**
	 * 属性vfapath的Setter方法.属性名：FBAUDIT路径
	 * 创建日期:
	 * @param Vfapath String
	 */
	public void setVfapath (String Vfapath ) {
		this.vfapath = Vfapath;
	} 	  
	/**
	 * 属性vyresv的Getter方法.属性名：历史预定查询
	 * 创建日期:
	 * @return String
	 */
	public String getVyresv () {
		return vyresv;
	}   
	/**
	 * 属性vyresv的Setter方法.属性名：历史预定查询
	 * 创建日期:
	 * @param Vyresv String
	 */
	public void setVyresv (String Vyresv ) {
		this.vyresv = Vyresv;
	} 	  
	/**
	 * 属性visuse的Getter方法.属性名：启用域名
	 * 创建日期:
	 * @return String
	 */
	public String getVisuse () {
		return visuse;
	}   
	/**
	 * 属性visuse的Setter方法.属性名：启用域名
	 * 创建日期:
	 * @param Visuse String
	 */
	public void setVisuse (String Visuse ) {
		this.visuse = Visuse;
	} 	  
	/**
	 * 属性vyarea的Getter方法.属性名：DAYTAKING表含区域
	 * 创建日期:
	 * @return String
	 */
	public String getVyarea () {
		return vyarea;
	}   
	/**
	 * 属性vyarea的Setter方法.属性名：DAYTAKING表含区域
	 * 创建日期:
	 * @param Vyarea String
	 */
	public void setVyarea (String Vyarea ) {
		this.vyarea = Vyarea;
	} 	  
	/**
	 * 属性vbpkgamt的Getter方法.属性名：不统计套餐优惠
	 * 创建日期:
	 * @return String
	 */
	public String getVbpkgamt () {
		return vbpkgamt;
	}   
	/**
	 * 属性vbpkgamt的Setter方法.属性名：不统计套餐优惠
	 * 创建日期:
	 * @param Vbpkgamt String
	 */
	public void setVbpkgamt (String Vbpkgamt ) {
		this.vbpkgamt = Vbpkgamt;
	} 	  
	/**
	 * 属性vychknet的Getter方法.属性名：忽略网络间连接检测
	 * 创建日期:
	 * @return String
	 */
	public String getVychknet () {
		return vychknet;
	}   
	/**
	 * 属性vychknet的Setter方法.属性名：忽略网络间连接检测
	 * 创建日期:
	 * @param Vychknet String
	 */
	public void setVychknet (String Vychknet ) {
		this.vychknet = Vychknet;
	} 	  
	/**
	 * 属性vmemo的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVmemo () {
		return vmemo;
	}   
	/**
	 * 属性vmemo的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vmemo String
	 */
	public void setVmemo (String Vmemo ) {
		this.vmemo = Vmemo;
	} 	  
	/**
	 * 属性vmemo2的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVmemo2 () {
		return vmemo2;
	}   
	/**
	 * 属性vmemo2的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vmemo2 String
	 */
	public void setVmemo2 (String Vmemo2 ) {
		this.vmemo2 = Vmemo2;
	} 	  
	/**
	 * 属性vmemo3的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVmemo3 () {
		return vmemo3;
	}   
	/**
	 * 属性vmemo3的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vmemo3 String
	 */
	public void setVmemo3 (String Vmemo3 ) {
		this.vmemo3 = Vmemo3;
	} 	  
	/**
	 * 属性vmemo4的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVmemo4 () {
		return vmemo4;
	}   
	/**
	 * 属性vmemo4的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vmemo4 String
	 */
	public void setVmemo4 (String Vmemo4 ) {
		this.vmemo4 = Vmemo4;
	} 	  
	/**
	 * 属性vmemo5的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVmemo5 () {
		return vmemo5;
	}   
	/**
	 * 属性vmemo5的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vmemo5 String
	 */
	public void setVmemo5 (String Vmemo5 ) {
		this.vmemo5 = Vmemo5;
	} 	  
	/**
	 * 属性vmemo6的Getter方法.属性名：$map.displayName
	 * 创建日期:
	 * @return String
	 */
	public String getVmemo6 () {
		return vmemo6;
	}   
	/**
	 * 属性vmemo6的Setter方法.属性名：$map.displayName
	 * 创建日期:
	 * @param Vmemo6 String
	 */
	public void setVmemo6 (String Vmemo6 ) {
		this.vmemo6 = Vmemo6;
	} 	  
	/**
	 * 属性pk_org的Getter方法.属性名：组织
	 * 创建日期:
	 * @return String
	 */
	public String getPk_org () {
		return pk_org;
	}   
	/**
	 * 属性pk_org的Setter方法.属性名：组织
	 * 创建日期:
	 * @param Pk_org String
	 */
	public void setPk_org (String Pk_org ) {
		this.pk_org = Pk_org;
	} 	  
	/**
	 * 属性pk_group的Getter方法.属性名：集团
	 * 创建日期:
	 * @return String
	 */
	public String getPk_group () {
		return pk_group;
	}   
	/**
	 * 属性pk_group的Setter方法.属性名：集团
	 * 创建日期:
	 * @param Pk_group String
	 */
	public void setPk_group (String Pk_group ) {
		this.pk_group = Pk_group;
	} 	  
	/**
	 * 属性creator的Getter方法.属性名：创建人
	 * 创建日期:
	 * @return String
	 */
	public String getCreator () {
		return creator;
	}   
	/**
	 * 属性creator的Setter方法.属性名：创建人
	 * 创建日期:
	 * @param Creator String
	 */
	public void setCreator (String Creator ) {
		this.creator = Creator;
	} 	  
	/**
	 * 属性creationtime的Getter方法.属性名：创建时间
	 * 创建日期:
	 * @return String
	 */
	public String getCreationtime () {
		return creationtime;
	}   
	/**
	 * 属性creationtime的Setter方法.属性名：创建时间
	 * 创建日期:
	 * @param Creationtime String
	 */
	public void setCreationtime (String Creationtime ) {
		this.creationtime = Creationtime;
	} 	  
	/**
	 * 属性modifier的Getter方法.属性名：修改人
	 * 创建日期:
	 * @return String
	 */
	public String getModifier () {
		return modifier;
	}   
	/**
	 * 属性modifier的Setter方法.属性名：修改人
	 * 创建日期:
	 * @param Modifier String
	 */
	public void setModifier (String Modifier ) {
		this.modifier = Modifier;
	} 	  
	/**
	 * 属性modifiedtime的Getter方法.属性名：修改时间
	 * 创建日期:
	 * @return String
	 */
	public String getModifiedtime () {
		return modifiedtime;
	}   
	/**
	 * 属性modifiedtime的Setter方法.属性名：修改时间
	 * 创建日期:
	 * @param Modifiedtime String
	 */
	public void setModifiedtime (String Modifiedtime ) {
		this.modifiedtime = Modifiedtime;
	} 	  
	/**
	 * 属性vdef1的Getter方法.属性名：自定义项1
	 * 创建日期:
	 * @return String
	 */
	public String getVdef1 () {
		return vdef1;
	}   
	/**
	 * 属性vdef1的Setter方法.属性名：自定义项1
	 * 创建日期:
	 * @param Vdef1 String
	 */
	public void setVdef1 (String Vdef1 ) {
		this.vdef1 = Vdef1;
	} 	  
	/**
	 * 属性vdef2的Getter方法.属性名：自定义项2
	 * 创建日期:
	 * @return String
	 */
	public String getVdef2 () {
		return vdef2;
	}   
	/**
	 * 属性vdef2的Setter方法.属性名：自定义项2
	 * 创建日期:
	 * @param Vdef2 String
	 */
	public void setVdef2 (String Vdef2 ) {
		this.vdef2 = Vdef2;
	} 	  
	/**
	 * 属性vdef3的Getter方法.属性名：自定义项3
	 * 创建日期:
	 * @return String
	 */
	public String getVdef3 () {
		return vdef3;
	}   
	/**
	 * 属性vdef3的Setter方法.属性名：自定义项3
	 * 创建日期:
	 * @param Vdef3 String
	 */
	public void setVdef3 (String Vdef3 ) {
		this.vdef3 = Vdef3;
	} 	  
	/**
	 * 属性vdef4的Getter方法.属性名：自定义项4
	 * 创建日期:
	 * @return String
	 */
	public String getVdef4 () {
		return vdef4;
	}   
	/**
	 * 属性vdef4的Setter方法.属性名：自定义项4
	 * 创建日期:
	 * @param Vdef4 String
	 */
	public void setVdef4 (String Vdef4 ) {
		this.vdef4 = Vdef4;
	} 	  
	/**
	 * 属性vdef5的Getter方法.属性名：自定义项5
	 * 创建日期:
	 * @return String
	 */
	public String getVdef5 () {
		return vdef5;
	}   
	/**
	 * 属性vdef5的Setter方法.属性名：自定义项5
	 * 创建日期:
	 * @param Vdef5 String
	 */
	public void setVdef5 (String Vdef5 ) {
		this.vdef5 = Vdef5;
	} 	  
	/**
	 * 属性vdef6的Getter方法.属性名：自定义项6
	 * 创建日期:
	 * @return String
	 */
	public String getVdef6 () {
		return vdef6;
	}   
	/**
	 * 属性vdef6的Setter方法.属性名：自定义项6
	 * 创建日期:
	 * @param Vdef6 String
	 */
	public void setVdef6 (String Vdef6 ) {
		this.vdef6 = Vdef6;
	} 	  
	/**
	 * 属性vdef7的Getter方法.属性名：自定义项7
	 * 创建日期:
	 * @return String
	 */
	public String getVdef7 () {
		return vdef7;
	}   
	/**
	 * 属性vdef7的Setter方法.属性名：自定义项7
	 * 创建日期:
	 * @param Vdef7 String
	 */
	public void setVdef7 (String Vdef7 ) {
		this.vdef7 = Vdef7;
	} 	  
	/**
	 * 属性vdef8的Getter方法.属性名：自定义项8
	 * 创建日期:
	 * @return String
	 */
	public String getVdef8 () {
		return vdef8;
	}   
	/**
	 * 属性vdef8的Setter方法.属性名：自定义项8
	 * 创建日期:
	 * @param Vdef8 String
	 */
	public void setVdef8 (String Vdef8 ) {
		this.vdef8 = Vdef8;
	} 	  
	/**
	 * 属性vdef9的Getter方法.属性名：自定义项9
	 * 创建日期:
	 * @return String
	 */
	public String getVdef9 () {
		return vdef9;
	}   
	/**
	 * 属性vdef9的Setter方法.属性名：自定义项9
	 * 创建日期:
	 * @param Vdef9 String
	 */
	public void setVdef9 (String Vdef9 ) {
		this.vdef9 = Vdef9;
	} 	  
	/**
	 * 属性vdef10的Getter方法.属性名：自定义项10
	 * 创建日期:
	 * @return String
	 */
	public String getVdef10 () {
		return vdef10;
	}   
	/**
	 * 属性vdef10的Setter方法.属性名：自定义项10
	 * 创建日期:
	 * @param Vdef10 String
	 */
	public void setVdef10 (String Vdef10 ) {
		this.vdef10 = Vdef10;
	} 	  
	/**
	 * 属性vdef11的Getter方法.属性名：自定义项11
	 * 创建日期:
	 * @return String
	 */
	public String getVdef11 () {
		return vdef11;
	}   
	/**
	 * 属性vdef11的Setter方法.属性名：自定义项11
	 * 创建日期:
	 * @param Vdef11 String
	 */
	public void setVdef11 (String Vdef11 ) {
		this.vdef11 = Vdef11;
	} 	  
	/**
	 * 属性vdef12的Getter方法.属性名：自定义项12
	 * 创建日期:
	 * @return String
	 */
	public String getVdef12 () {
		return vdef12;
	}   
	/**
	 * 属性vdef12的Setter方法.属性名：自定义项12
	 * 创建日期:
	 * @param Vdef12 String
	 */
	public void setVdef12 (String Vdef12 ) {
		this.vdef12 = Vdef12;
	} 	  
	/**
	 * 属性vdef13的Getter方法.属性名：自定义项13
	 * 创建日期:
	 * @return String
	 */
	public String getVdef13 () {
		return vdef13;
	}   
	/**
	 * 属性vdef13的Setter方法.属性名：自定义项13
	 * 创建日期:
	 * @param Vdef13 String
	 */
	public void setVdef13 (String Vdef13 ) {
		this.vdef13 = Vdef13;
	} 	  
	/**
	 * 属性vdef14的Getter方法.属性名：自定义项14
	 * 创建日期:
	 * @return String
	 */
	public String getVdef14 () {
		return vdef14;
	}   
	/**
	 * 属性vdef14的Setter方法.属性名：自定义项14
	 * 创建日期:
	 * @param Vdef14 String
	 */
	public void setVdef14 (String Vdef14 ) {
		this.vdef14 = Vdef14;
	} 	  
	/**
	 * 属性vdef15的Getter方法.属性名：自定义项15
	 * 创建日期:
	 * @return String
	 */
	public String getVdef15 () {
		return vdef15;
	}   
	/**
	 * 属性vdef15的Setter方法.属性名：自定义项15
	 * 创建日期:
	 * @param Vdef15 String
	 */
	public void setVdef15 (String Vdef15 ) {
		this.vdef15 = Vdef15;
	} 	  
	/**
	 * 属性vdef16的Getter方法.属性名：自定义项16
	 * 创建日期:
	 * @return String
	 */
	public String getVdef16 () {
		return vdef16;
	}   
	/**
	 * 属性vdef16的Setter方法.属性名：自定义项16
	 * 创建日期:
	 * @param Vdef16 String
	 */
	public void setVdef16 (String Vdef16 ) {
		this.vdef16 = Vdef16;
	} 	  
	/**
	 * 属性vdef17的Getter方法.属性名：自定义项17
	 * 创建日期:
	 * @return String
	 */
	public String getVdef17 () {
		return vdef17;
	}   
	/**
	 * 属性vdef17的Setter方法.属性名：自定义项17
	 * 创建日期:
	 * @param Vdef17 String
	 */
	public void setVdef17 (String Vdef17 ) {
		this.vdef17 = Vdef17;
	} 	  
	/**
	 * 属性vdef18的Getter方法.属性名：自定义项18
	 * 创建日期:
	 * @return String
	 */
	public String getVdef18 () {
		return vdef18;
	}   
	/**
	 * 属性vdef18的Setter方法.属性名：自定义项18
	 * 创建日期:
	 * @param Vdef18 String
	 */
	public void setVdef18 (String Vdef18 ) {
		this.vdef18 = Vdef18;
	} 	  
	/**
	 * 属性vdef19的Getter方法.属性名：自定义项19
	 * 创建日期:
	 * @return String
	 */
	public String getVdef19 () {
		return vdef19;
	}   
	/**
	 * 属性vdef19的Setter方法.属性名：自定义项19
	 * 创建日期:
	 * @param Vdef19 String
	 */
	public void setVdef19 (String Vdef19 ) {
		this.vdef19 = Vdef19;
	} 	  
	/**
	 * 属性vdef20的Getter方法.属性名：自定义项20
	 * 创建日期:
	 * @return String
	 */
	public String getVdef20 () {
		return vdef20;
	}   
	/**
	 * 属性vdef20的Setter方法.属性名：自定义项20
	 * 创建日期:
	 * @param Vdef20 String
	 */
	public void setVdef20 (String Vdef20 ) {
		this.vdef20 = Vdef20;
	} 	  
	/**
	 * 属性pk_org_v的Getter方法.属性名：组织版本信息
	 * 创建日期:
	 * @return String
	 */
	public String getPk_org_v () {
		return pk_org_v;
	}   
	/**
	 * 属性pk_org_v的Setter方法.属性名：组织版本信息
	 * 创建日期:
	 * @param Pk_org_v String
	 */
	public void setPk_org_v (String Pk_org_v ) {
		this.pk_org_v = Pk_org_v;
	} 	  
	/**
	 * 属性vprnver的Getter方法.属性名：打印方案集团版本
	 * 创建日期:
	 * @return String
	 */
	public String getVprnver () {
		return vprnver;
	}   
	/**
	 * 属性vprnver的Setter方法.属性名：打印方案集团版本
	 * 创建日期:
	 * @param Vprnver String
	 */
	public void setVprnver (String Vprnver ) {
		this.vprnver = Vprnver;
	} 	  
	/**
	 * 属性vfirmprnver的Getter方法.属性名：打印方案分店版本
	 * 创建日期:
	 * @return String
	 */
	public String getVfirmprnver () {
		return vfirmprnver;
	}   
	/**
	 * 属性vfirmprnver的Setter方法.属性名：打印方案分店版本
	 * 创建日期:
	 * @param Vfirmprnver String
	 */
	public void setVfirmprnver (String Vfirmprnver ) {
		this.vfirmprnver = Vfirmprnver;
	} 	  
	/**
	 * 属性vprndownver的Getter方法.属性名：打印方案最后同步版本
	 * 创建日期:
	 * @return String
	 */
	public String getVprndownver () {
		return vprndownver;
	}   
	/**
	 * 属性vprndownver的Setter方法.属性名：打印方案最后同步版本
	 * 创建日期:
	 * @param Vprndownver String
	 */
	public void setVprndownver (String Vprndownver ) {
		this.vprndownver = Vprndownver;
	} 	  
	/**
	 * 属性dprndowntim的Getter方法.属性名：打印方案最后同步时间
	 * 创建日期:
	 * @return String
	 */
	public String getDprndowntim () {
		return dprndowntim;
	}   
	/**
	 * 属性dprndowntim的Setter方法.属性名：打印方案最后同步时间
	 * 创建日期:
	 * @param Dprndowntim String
	 */
	public void setDprndowntim (String Dprndowntim ) {
		this.dprndowntim = Dprndowntim;
	} 	  
	/**
	 * 属性vpayver的Getter方法.属性名：支付方案集团版本
	 * 创建日期:
	 * @return String
	 */
	public String getVpayver () {
		return vpayver;
	}   
	/**
	 * 属性vpayver的Setter方法.属性名：支付方案集团版本
	 * 创建日期:
	 * @param Vpayver String
	 */
	public void setVpayver (String Vpayver ) {
		this.vpayver = Vpayver;
	} 	  
	/**
	 * 属性vfirmpaynver的Getter方法.属性名：支付方案分店版本
	 * 创建日期:
	 * @return String
	 */
	public String getVfirmpaynver () {
		return vfirmpaynver;
	}   
	/**
	 * 属性vfirmpaynver的Setter方法.属性名：支付方案分店版本
	 * 创建日期:
	 * @param Vfirmpaynver String
	 */
	public void setVfirmpaynver (String Vfirmpaynver ) {
		this.vfirmpaynver = Vfirmpaynver;
	} 	  
	/**
	 * 属性vpaydownver的Getter方法.属性名：支付方案最后同步版本
	 * 创建日期:
	 * @return String
	 */
	public String getVpaydownver () {
		return vpaydownver;
	}   
	/**
	 * 属性vpaydownver的Setter方法.属性名：支付方案最后同步版本
	 * 创建日期:
	 * @param Vpaydownver String
	 */
	public void setVpaydownver (String Vpaydownver ) {
		this.vpaydownver = Vpaydownver;
	} 	  
	/**
	 * 属性dpaydowntim的Getter方法.属性名：支付方案最后同步时间
	 * 创建日期:
	 * @return String
	 */
	public String getDpaydowntim () {
		return dpaydowntim;
	}   
	/**
	 * 属性dpaydowntim的Setter方法.属性名：支付方案最后同步时间
	 * 创建日期:
	 * @param Dpaydowntim String
	 */
	public void setDpaydowntim (String Dpaydowntim ) {
		this.dpaydowntim = Dpaydowntim;
	} 	  
	/**
	 * 属性vprgid的Getter方法.属性名：菜谱方案
	 * 创建日期:
	 * @return String
	 */
	public String getVprgid () {
		return vprgid;
	}   
	/**
	 * 属性vprgid的Setter方法.属性名：菜谱方案
	 * 创建日期:
	 * @param Vprgid String
	 */
	public void setVprgid (String Vprgid ) {
		this.vprgid = Vprgid;
	} 	  
	/**
	 * 属性vprgvouno的Getter方法.属性名：菜谱方案编号
	 * 创建日期:
	 * @return String
	 */
	public String getVprgvouno () {
		return vprgvouno;
	}   
	/**
	 * 属性vprgvouno的Setter方法.属性名：菜谱方案编号
	 * 创建日期:
	 * @param Vprgvouno String
	 */
	public void setVprgvouno (String Vprgvouno ) {
		this.vprgvouno = Vprgvouno;
	} 	  
	/**
	 * 属性vprgver的Getter方法.属性名：菜谱方案集团版本号
	 * 创建日期:
	 * @return String
	 */
	public String getVprgver () {
		return vprgver;
	}   
	/**
	 * 属性vprgver的Setter方法.属性名：菜谱方案集团版本号
	 * 创建日期:
	 * @param Vprgver String
	 */
	public void setVprgver (String Vprgver ) {
		this.vprgver = Vprgver;
	} 	  
	/**
	 * 属性vdownver的Getter方法.属性名：菜谱方案最后同步版本
	 * 创建日期:
	 * @return String
	 */
	public String getVdownver () {
		return vdownver;
	}   
	/**
	 * 属性vdownver的Setter方法.属性名：菜谱方案最后同步版本
	 * 创建日期:
	 * @param Vdownver String
	 */
	public void setVdownver (String Vdownver ) {
		this.vdownver = Vdownver;
	} 	  
	/**
	 * 属性vfirmver的Getter方法.属性名：菜谱方案分店版本
	 * 创建日期:
	 * @return String
	 */
	public String getVfirmver () {
		return vfirmver;
	}   
	/**
	 * 属性vfirmver的Setter方法.属性名：菜谱方案分店版本
	 * 创建日期:
	 * @param Vfirmver String
	 */
	public void setVfirmver (String Vfirmver ) {
		this.vfirmver = Vfirmver;
	} 	  
	/**
	 * 属性ddowntim的Getter方法.属性名：菜谱方案最后同步时间
	 * 创建日期:
	 * @return String
	 */
	public String getDdowntim () {
		return ddowntim;
	}   
	/**
	 * 属性ddowntim的Setter方法.属性名：菜谱方案最后同步时间
	 * 创建日期:
	 * @param Ddowntim String
	 */
	public void setDdowntim (String Ddowntim ) {
		this.ddowntim = Ddowntim;
	} 	  
	/**
	 * 属性vplantyp的Getter方法.属性名：预估方式
	 * 创建日期:
	 * @return String
	 */
	public String getVplantyp () {
		return vplantyp;
	}   
	/**
	 * 属性vplantyp的Setter方法.属性名：预估方式
	 * 创建日期:
	 * @param Vplantyp String
	 */
	public void setVplantyp (String Vplantyp ) {
		this.vplantyp = Vplantyp;
	} 	  
	/**
	 * 属性vnighteonstw的Getter方法.属性名：夜宵开始时间
	 * 创建日期:
	 * @return String
	 */
	public String getVnighteonstw () {
		return vnighteonstw;
	}   
	/**
	 * 属性vnighteonstw的Setter方法.属性名：夜宵开始时间
	 * 创建日期:
	 * @param Vnighteonstw String
	 */
	public void setVnighteonstw (String Vnighteonstw ) {
		this.vnighteonstw = Vnighteonstw;
	} 	  
	/**
	 * 属性vnighteonetw的Getter方法.属性名：夜宵结束时间
	 * 创建日期:
	 * @return String
	 */
	public String getVnighteonetw () {
		return vnighteonetw;
	}   
	/**
	 * 属性vnighteonetw的Setter方法.属性名：夜宵结束时间
	 * 创建日期:
	 * @param Vnighteonetw String
	 */
	public void setVnighteonetw (String Vnighteonetw ) {
		this.vnighteonetw = Vnighteonetw;
	} 	  
	/**
	 * 属性vnighteonstwe的Getter方法.属性名：周末夜宵开始时间
	 * 创建日期:
	 * @return String
	 */
	public String getVnighteonstwe () {
		return vnighteonstwe;
	}   
	/**
	 * 属性vnighteonstwe的Setter方法.属性名：周末夜宵开始时间
	 * 创建日期:
	 * @param Vnighteonstwe String
	 */
	public void setVnighteonstwe (String Vnighteonstwe ) {
		this.vnighteonstwe = Vnighteonstwe;
	} 	  
	/**
	 * 属性vnighteonetwe的Getter方法.属性名：周末夜宵结束时间
	 * 创建日期:
	 * @return String
	 */
	public String getVnighteonetwe () {
		return vnighteonetwe;
	}   
	/**
	 * 属性vnighteonetwe的Setter方法.属性名：周末夜宵结束时间
	 * 创建日期:
	 * @param Vnighteonetwe String
	 */
	public void setVnighteonetwe (String Vnighteonetwe ) {
		this.vnighteonetwe = Vnighteonetwe;
	} 	  
	/**
	 * 属性bisbreakfast的Getter方法.属性名：早餐
	 * 创建日期:
	 * @return String
	 */
	public String getBisbreakfast () {
		return bisbreakfast;
	}   
	/**
	 * 属性bisbreakfast的Setter方法.属性名：早餐
	 * 创建日期:
	 * @param Bisbreakfast String
	 */
	public void setBisbreakfast (String Bisbreakfast ) {
		this.bisbreakfast = Bisbreakfast;
	} 	  
	/**
	 * 属性bislunch的Getter方法.属性名：午餐
	 * 创建日期:
	 * @return String
	 */
	public String getBislunch () {
		return bislunch;
	}   
	/**
	 * 属性bislunch的Setter方法.属性名：午餐
	 * 创建日期:
	 * @param Bislunch String
	 */
	public void setBislunch (String Bislunch ) {
		this.bislunch = Bislunch;
	} 	  
	/**
	 * 属性bisdinner的Getter方法.属性名：晚餐
	 * 创建日期:
	 * @return String
	 */
	public String getBisdinner () {
		return bisdinner;
	}   
	/**
	 * 属性bisdinner的Setter方法.属性名：晚餐
	 * 创建日期:
	 * @param Bisdinner String
	 */
	public void setBisdinner (String Bisdinner ) {
		this.bisdinner = Bisdinner;
	} 	  
	/**
	 * 属性bisnight的Getter方法.属性名：夜宵
	 * 创建日期:
	 * @return String
	 */
	public String getBisnight () {
		return bisnight;
	}   
	/**
	 * 属性bisnight的Setter方法.属性名：夜宵
	 * 创建日期:
	 * @param Bisnight String
	 */
	public void setBisnight (String Bisnight ) {
		this.bisnight = Bisnight;
	} 	  
	/**
	 * 属性innercode的Getter方法.属性名：内部编码
	 * 创建日期:
	 * @return String
	 */
	public String getInnercode () {
		return innercode;
	}   
	/**
	 * 属性innercode的Setter方法.属性名：内部编码
	 * 创建日期:
	 * @param Innercode String
	 */
	public void setInnercode (String Innercode ) {
		this.innercode = Innercode;
	} 	  
	/**
	 * 属性pid的Getter方法.属性名：上级编码
	 * 创建日期:
	 * @return String
	 */
	public String getPid () {
		return pid;
	}   
	/**
	 * 属性pid的Setter方法.属性名：上级编码
	 * 创建日期:
	 * @param Pid String
	 */
	public void setPid (String Pid ) {
		this.pid = Pid;
	} 	  
	/**
	 * 属性maxpeople的Getter方法.属性名：最大接待人数
	 * 创建日期:
	 * @return Integer
	 */
	public Integer getMaxpeople () {
		return maxpeople;
	}   
	/**
	 * 属性maxpeople的Setter方法.属性名：最大接待人数
	 * 创建日期:
	 * @param Maxpeople Integer
	 */
	public void setMaxpeople (Integer Maxpeople ) {
		this.maxpeople = Maxpeople;
	} 	  
	/**
	 * 属性maxodd的Getter方法.属性名：最大接待单数
	 * 创建日期:
	 * @return Integer
	 */
	public Integer getMaxodd () {
		return maxodd;
	}   
	/**
	 * 属性maxodd的Setter方法.属性名：最大接待单数
	 * 创建日期:
	 * @param Maxodd Integer
	 */
	public void setMaxodd (Integer Maxodd ) {
		this.maxodd = Maxodd;
	} 	  
	/**
	 * 属性tsendlasttim的Getter方法.属性名：最后上传时间
	 * 创建日期:
	 * @return String
	 */
	public String getTsendlasttim () {
		return tsendlasttim;
	}   
	/**
	 * 属性tsendlasttim的Setter方法.属性名：最后上传时间
	 * 创建日期:
	 * @param Tsendlasttim String
	 */
	public void setTsendlasttim (String Tsendlasttim ) {
		this.tsendlasttim = Tsendlasttim;
	} 	  
	/**
	 * 属性vdept的Getter方法.属性名：部门
	 * 创建日期:
	 * @return String
	 */
	public String getVdept () {
		return vdept;
	}   
	/**
	 * 属性vdept的Setter方法.属性名：部门
	 * 创建日期:
	 * @param Vdept String
	 */
	public void setVdept (String Vdept ) {
		this.vdept = Vdept;
	} 	  
	/**
	 * 属性vpricecode的Getter方法.属性名：价格方案编码
	 * 创建日期:
	 * @return String
	 */
	public String getVpricecode () {
		return vpricecode;
	}   
	/**
	 * 属性vpricecode的Setter方法.属性名：价格方案编码
	 * 创建日期:
	 * @param Vpricecode String
	 */
	public void setVpricecode (String Vpricecode ) {
		this.vpricecode = Vpricecode;
	} 	  
	/**
	 * 属性vpricever的Getter方法.属性名：价格方案集团版本号
	 * 创建日期:
	 * @return String
	 */
	public String getVpricever () {
		return vpricever;
	}   
	/**
	 * 属性vpricever的Setter方法.属性名：价格方案集团版本号
	 * 创建日期:
	 * @param Vpricever String
	 */
	public void setVpricever (String Vpricever ) {
		this.vpricever = Vpricever;
	} 	  
	/**
	 * 属性vfirmpricever的Getter方法.属性名：价格方案门店版本
	 * 创建日期:
	 * @return String
	 */
	public String getVfirmpricever () {
		return vfirmpricever;
	}   
	/**
	 * 属性vfirmpricever的Setter方法.属性名：价格方案门店版本
	 * 创建日期:
	 * @param Vfirmpricever String
	 */
	public void setVfirmpricever (String Vfirmpricever ) {
		this.vfirmpricever = Vfirmpricever;
	} 	  
	/**
	 * 属性vpricedownver的Getter方法.属性名：价格方案最后同步版本
	 * 创建日期:
	 * @return String
	 */
	public String getVpricedownver () {
		return vpricedownver;
	}   
	/**
	 * 属性vpricedownver的Setter方法.属性名：价格方案最后同步版本
	 * 创建日期:
	 * @param Vpricedownver String
	 */
	public void setVpricedownver (String Vpricedownver ) {
		this.vpricedownver = Vpricedownver;
	} 	  
	/**
	 * 属性dpricedowntim的Getter方法.属性名：价格方案最后同步时间
	 * 创建日期:
	 * @return String
	 */
	public String getDpricedowntim () {
		return dpricedowntim;
	}   
	/**
	 * 属性dpricedowntim的Setter方法.属性名：价格方案最后同步时间
	 * 创建日期:
	 * @param Dpricedowntim String
	 */
	public void setDpricedowntim (String Dpricedowntim ) {
		this.dpricedowntim = Dpricedowntim;
	} 	  
	/**
	 * 属性dr的Getter方法.属性名：dr
	 * 创建日期:
	 * @return Integer
	 */
	public Integer getDr () {
		return dr;
	}   
	/**
	 * 属性dr的Setter方法.属性名：dr
	 * 创建日期:
	 * @param Dr Integer
	 */
	public void setDr (Integer Dr ) {
		this.dr = Dr;
	} 	  
	/**
	 * 属性ts的Getter方法.属性名：ts
	 * 创建日期:
	 * @return String
	 */
	public String getTs () {
		return ts;
	}   
	/**
	 * 属性ts的Setter方法.属性名：ts
	 * 创建日期:
	 * @param Ts String
	 */
	public void setTs (String Ts ) {
		this.ts = Ts;
	}
	public String getPk_id() {
		return pk_id;
	}
	public void setPk_id(String pk_id) {
		this.pk_id = pk_id;
	}
	public String getPk_airdittype() {
		return pk_airdittype;
	}
	public void setPk_airdittype(String pk_airdittype) {
		this.pk_airdittype = pk_airdittype;
	}
	public String getBomname() {
		return bomname;
	}
	public void setBomname(String bomname) {
		this.bomname = bomname;
	}
	/**
	 * @return itcoperationmode
	 */
	public Integer getItcoperationmode() {
		return itcoperationmode;
	}
	/**
	 * @param itcoperationmode the itcoperationmode to set
	 */
	public void setItcoperationmode(Integer itcoperationmode) {
		this.itcoperationmode = itcoperationmode;
	}
	public String getPk_actstr() {
		return pk_actstr;
	}
	public void setPk_actstr(String pk_actstr) {
		this.pk_actstr = pk_actstr;
	}
	public String getMorningTea() {
		return morningTea;
	}
	public void setMorningTea(String morningTea) {
		this.morningTea = morningTea;
	}
	public String getMorningTeastW() {
		return morningTeastW;
	}
	public void setMorningTeastW(String morningTeastW) {
		this.morningTeastW = morningTeastW;
	}
	public String getMorningTeaetW() {
		return morningTeaetW;
	}
	public void setMorningTeaetW(String morningTeaetW) {
		this.morningTeaetW = morningTeaetW;
	}
	public String getMorningTeastWe() {
		return morningTeastWe;
	}
	public void setMorningTeastWe(String morningTeastWe) {
		this.morningTeastWe = morningTeastWe;
	}
	public String getMorningTeaetWe() {
		return morningTeaetWe;
	}
	public void setMorningTeaetWe(String morningTeaetWe) {
		this.morningTeaetWe = morningTeaetWe;
	}
	public String getAfternoonTea() {
		return afternoonTea;
	}
	public void setAfternoonTea(String afternoonTea) {
		this.afternoonTea = afternoonTea;
	}
	public String getAfternoonTeastW() {
		return afternoonTeastW;
	}
	public void setAfternoonTeastW(String afternoonTeastW) {
		this.afternoonTeastW = afternoonTeastW;
	}
	public String getAfternoonTeaetW() {
		return afternoonTeaetW;
	}
	public void setAfternoonTeaetW(String afternoonTeaetW) {
		this.afternoonTeaetW = afternoonTeaetW;
	}
	public String getAfternoonTeastWe() {
		return afternoonTeastWe;
	}
	public void setAfternoonTeastWe(String afternoonTeastWe) {
		this.afternoonTeastWe = afternoonTeastWe;
	}
	public String getAfternoonTeaetWe() {
		return afternoonTeaetWe;
	}
	public void setAfternoonTeaetWe(String afternoonTeaetWe) {
		this.afternoonTeaetWe = afternoonTeaetWe;
	}
	public String getvInventoryContr() {
		return vInventoryContr;
	}
	public void setvInventoryContr(String vInventoryContr) {
		this.vInventoryContr = vInventoryContr;
	}
	public String getvInAmounts() {
		return vInAmounts;
	}
	public void setvInAmounts(String vInAmounts) {
		this.vInAmounts = vInAmounts;
	}
	public String getVADDToday() {
		return VADDToday;
	}
	public void setVADDToday(String vADDToday) {
		this.VADDToday = vADDToday;
	}
	public String getVxnsupply() {
		return vxnsupply;
	}
	public void setVxnsupply(String vxnsupply) {
		this.vxnsupply = vxnsupply;
	}
	public Integer getIisupvalid() {
		return iisupvalid;
	}
	public void setIisupvalid(Integer iisupvalid) {
		this.iisupvalid = iisupvalid;
	}
	public Integer getIdaylimit() {
		return idaylimit;
	}
	public void setIdaylimit(Integer idaylimit) {
		this.idaylimit = idaylimit;
	}
	public String getVisuseban() {
		return visuseban;
	}
	public void setVisuseban(String visuseban) {
		this.visuseban = visuseban;
	}
	public String getVisfillreceipt() {
		return visfillreceipt;
	}
	public void setVisfillreceipt(String visfillreceipt) {
		this.visfillreceipt = visfillreceipt;
	}
	public String getVisfillreprotlist() {
		return visfillreprotlist;
	}
	public void setVisfillreprotlist(String visfillreprotlist) {
		this.visfillreprotlist = visfillreprotlist;
	}
	public String getVisuseschedule() {
		return visuseschedule;
	}
	public void setVisuseschedule(String visuseschedule) {
		this.visuseschedule = visuseschedule;
	}
	public String getVisuseprediction() {
		return visuseprediction;
	}
	public void setVisuseprediction(String visuseprediction) {
		this.visuseprediction = visuseprediction;
	}
	public String getVischeck() {
		return vischeck;
	}
	public void setVischeck(String vischeck) {
		this.vischeck = vischeck;
	}
	public String getPk_city() {
		return pk_city;
	}
	public void setPk_city(String pk_city) {
		this.pk_city = pk_city;
	}
	public String getPospassword() {
		return pospassword;
	}
	public void setPospassword(String pospassword) {
		this.pospassword = pospassword;
	}
	public String getPospwdmd5() {
		return pospwdmd5;
	}
	public void setPospwdmd5(String pospwdmd5) {
		this.pospwdmd5 = pospwdmd5;
	}
	public String getPwdtime() {
		return pwdtime;
	}
	public void setPwdtime(String pwdtime) {
		this.pwdtime = pwdtime;
	}
	public String getVismodifyestimate() {
		return vismodifyestimate;
	}
	public void setVismodifyestimate(String vismodifyestimate) {
		this.vismodifyestimate = vismodifyestimate;
	}
	public String getVistest() {
		return vistest;
	}
	public void setVistest(String vistest) {
		this.vistest = vistest;
	}
	public String getCopyitem() {
		return copyitem;
	}
	public void setCopyitem(String copyitem) {
		this.copyitem = copyitem;
	}
	public String getVisactualmoney() {
		return visactualmoney;
	}
	public void setVisactualmoney(String visactualmoney) {
		this.visactualmoney = visactualmoney;
	}
	public String getVaddsychactm() {
		return vaddsychactm;
	}
	public void setVaddsychactm(String vaddsychactm) {
		this.vaddsychactm = vaddsychactm;
	}
	public String getTele() {
		return tele;
	}
	public void setTele(String tele) {
		this.tele = tele;
	}
	public String getStlye() {
		return stlye;
	}
	public void setStlye(String stlye) {
		this.stlye = stlye;
	}
	public String getBusinfo() {
		return businfo;
	}
	public void setBusinfo(String businfo) {
		this.businfo = businfo;
	}
	public String getSeat() {
		return seat;
	}
	public void setSeat(String seat) {
		this.seat = seat;
	}
	public String getEquip() {
		return equip;
	}
	public void setEquip(String equip) {
		this.equip = equip;
	}
	public String getBigPic() {
		return bigPic;
	}
	public void setBigPic(String bigPic) {
		this.bigPic = bigPic;
	}
	public String getSmallpic() {
		return smallpic;
	}
	public void setSmallpic(String smallpic) {
		this.smallpic = smallpic;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getFeast() {
		return feast;
	}
	public void setFeast(String feast) {
		this.feast = feast;
	}
	public String getMeeting() {
		return meeting;
	}
	public void setMeeting(String meeting) {
		this.meeting = meeting;
	}
	public String getPk_area() {
		return pk_area;
	}
	public void setPk_area(String pk_area) {
		this.pk_area = pk_area;
	}
	public String getPk_street() {
		return pk_street;
	}
	public void setPk_street(String pk_street) {
		this.pk_street = pk_street;
	}
	public String getIdList() {
		return idList;
	}
	public void setIdList(String idList) {
		this.idList = idList;
	}
	public Integer getSortNum() {
		return sortNum;
	}
	public void setSortNum(Integer sortNum) {
		this.sortNum = sortNum;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLunchEndTime() {
		return lunchEndTime;
	}
	public void setLunchEndTime(String lunchEndTime) {
		this.lunchEndTime = lunchEndTime;
	}
	public String getDinnerEndTime() {
		return dinnerEndTime;
	}
	public void setDinnerEndTime(String dinnerEndTime) {
		this.dinnerEndTime = dinnerEndTime;
	}
	public String getLunchStart() {
		return lunchStart;
	}
	public void setLunchStart(String lunchStart) {
		this.lunchStart = lunchStart;
	}
	public String getLunchEnd() {
		return lunchEnd;
	}
	public void setLunchEnd(String lunchEnd) {
		this.lunchEnd = lunchEnd;
	}
	public String getDinnerStart() {
		return dinnerStart;
	}
	public void setDinnerStart(String dinnerStart) {
		this.dinnerStart = dinnerStart;
	}
	public String getDinnerEnd() {
		return dinnerEnd;
	}
	public void setDinnerEnd(String dinnerEnd) {
		this.dinnerEnd = dinnerEnd;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getNetshow() {
		return netshow;
	}
	public void setNetshow(String netshow) {
		this.netshow = netshow;
	}
	public String getOrdershow() {
		return ordershow;
	}
	public void setOrdershow(String ordershow) {
		this.ordershow = ordershow;
	} 	 
}
