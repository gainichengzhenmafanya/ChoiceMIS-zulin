package com.choice.misboh.domain.BaseRecord;

import java.util.List;

/**
 * 描述：可换菜明细表
 * @author 马振
 * 创建时间：2015-4-20 下午5:21:41
 */
public class ItemPkg {
	private String vcode;
	private String pk_itempkg;			//可换菜主键
	private String vname;				//菜品名称
	private String pk_pubitem;			//菜谱主键
	private Integer ncnt;				//数量
	private Double nprice;				//单价
	private String vunit;				//单位
	private String vadditem;			//附加项
	private String pk_package;			//套餐明细主键
	private String pk_pubpack;          //套餐主表主键
	private String ts;
	private String producttc_order;		//标识
	private List<ItemPkg> listItemPkg;  //排序列
	
	private double ndiscountprice;      //折扣值
	private double npackageprice;		//套餐单价
	
	private Integer imincount;			//最小数量
	private Integer imaxcount;  		//最大数量
	
	private double nadjustprice; 		//调价 dwh 2014-04-27添加
	
	private Integer isortno;
	
	private String unitindex;			//单位序号
	private String unitcode;			//单位编码
	
	public String getPk_itempkg() {
		return pk_itempkg;
	}
	public void setPk_itempkg(String pk_itempkg) {
		this.pk_itempkg = pk_itempkg;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getPk_pubitem() {
		return pk_pubitem;
	}
	public void setPk_pubitem(String pk_pubitem) {
		this.pk_pubitem = pk_pubitem;
	}
	public Integer getNcnt() {
		return ncnt;
	}
	public void setNcnt(Integer ncnt) {
		this.ncnt = ncnt;
	}
	public Double getNprice() {
		return nprice;
	}
	public void setNprice(Double nprice) {
		this.nprice = nprice;
	}
	public String getVunit() {
		return vunit;
	}
	public void setVunit(String vunit) {
		this.vunit = vunit;
	}
	public String getVadditem() {
		return vadditem;
	}
	public void setVadditem(String vadditem) {
		this.vadditem = vadditem;
	}
	public String getPk_package() {
		return pk_package;
	}
	public void setPk_package(String pk_package) {
		this.pk_package = pk_package;
	}
	
	public String getPk_pubpack() {
		return pk_pubpack;
	}
	public void setPk_pubpack(String pk_pubpack) {
		this.pk_pubpack = pk_pubpack;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public String getProducttc_order() {
		return producttc_order;
	}
	public void setProducttc_order(String producttc_order) {
		this.producttc_order = producttc_order;
	}
	public List<ItemPkg> getListItemPkg() {
		return listItemPkg;
	}
	public void setListItemPkg(List<ItemPkg> listItemPkg) {
		this.listItemPkg = listItemPkg;
	}
	
	public double getNdiscountprice() {
		return ndiscountprice;
	}
	public void setNdiscountprice(double ndiscountprice) {
		this.ndiscountprice = ndiscountprice;
	}
	public double getNpackageprice() {
		return npackageprice;
	}
	public void setNpackageprice(double npackageprice) {
		this.npackageprice = npackageprice;
	}
	public Integer getImincount() {
		return imincount;
	}
	public void setImincount(Integer imincount) {
		this.imincount = imincount;
	}
	public Integer getImaxcount() {
		return imaxcount;
	}
	public void setImaxcount(Integer imaxcount) {
		this.imaxcount = imaxcount;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public double getNadjustprice() {
		return nadjustprice;
	}
	public void setNadjustprice(double nadjustprice) {
		this.nadjustprice = nadjustprice;
	}
	
	public Integer getIsortno() {
		return isortno;
	}
	public void setIsortno(Integer isortno) {
		this.isortno = isortno;
	}
	public String getUnitindex() {
		return unitindex;
	}
	public void setUnitindex(String unitindex) {
		this.unitindex = unitindex;
	}
	public String getUnitcode() {
		return unitcode;
	}
	public void setUnitcode(String unitcode) {
		this.unitcode = unitcode;
	}
	
}
