package com.choice.misboh.domain.BaseRecord;


/**
 * 描述：支付类型
 * @author 马振
 * 创建时间：2015-4-16 下午1:10:16
 */
public class Payment {

	private String pk_id;		//主键
	private String vcode ;		//编码
	private String vname ;		//名称
	private Integer enablestate;//启用状态
	private Integer isortno;	//排序
	private String vinit;		//缩写
	private String ivalue;		//统计码
	
	//自定义项1~20
	private String vdef1;		
	private String vdef2;
	private String vdef3;
	private String vdef4;
	private String vdef5;
	private String vdef6;
	private String vdef7;
	private String vdef8;
	private String vdef9;
	private String vdef10;
	private String vdef11;
	private String vdef12;
	private String vdef13;
	private String vdef14;
	private String vdef15;
	private String vdef16;
	private String vdef17;
	private String vdef18;
	private String vdef19;
	private String vdef20;
	
	private String vfoodsign; //餐饮类型，1为中餐和webpos用的，2为快餐用的
	private String viscy;
	private double nupmoney;
	private double ndownmoney;
	
	public String getPk_id() {
		return pk_id;
	}
	public void setPk_id(String pk_id) {
		this.pk_id = pk_id;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public Integer getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(Integer enablestate) {
		this.enablestate = enablestate;
	}
	public Integer getIsortno() {
		return isortno;
	}
	public void setIsortno(Integer isortno) {
		this.isortno = isortno;
	}
	public String getVinit() {
		return vinit;
	}
	public void setVinit(String vinit) {
		this.vinit = vinit;
	}
	public String getIvalue() {
		return ivalue;
	}
	public void setIvalue(String ivalue) {
		this.ivalue = ivalue;
	}
	public String getVdef1() {
		return vdef1;
	}
	public void setVdef1(String vdef1) {
		this.vdef1 = vdef1;
	}
	public String getVdef2() {
		return vdef2;
	}
	public void setVdef2(String vdef2) {
		this.vdef2 = vdef2;
	}
	public String getVdef3() {
		return vdef3;
	}
	public void setVdef3(String vdef3) {
		this.vdef3 = vdef3;
	}
	public String getVdef4() {
		return vdef4;
	}
	public void setVdef4(String vdef4) {
		this.vdef4 = vdef4;
	}
	public String getVdef5() {
		return vdef5;
	}
	public void setVdef5(String vdef5) {
		this.vdef5 = vdef5;
	}
	public String getVdef6() {
		return vdef6;
	}
	public void setVdef6(String vdef6) {
		this.vdef6 = vdef6;
	}
	public String getVdef7() {
		return vdef7;
	}
	public void setVdef7(String vdef7) {
		this.vdef7 = vdef7;
	}
	public String getVdef8() {
		return vdef8;
	}
	public void setVdef8(String vdef8) {
		this.vdef8 = vdef8;
	}
	public String getVdef9() {
		return vdef9;
	}
	public void setVdef9(String vdef9) {
		this.vdef9 = vdef9;
	}
	public String getVdef10() {
		return vdef10;
	}
	public void setVdef10(String vdef10) {
		this.vdef10 = vdef10;
	}
	public String getVdef11() {
		return vdef11;
	}
	public void setVdef11(String vdef11) {
		this.vdef11 = vdef11;
	}
	public String getVdef12() {
		return vdef12;
	}
	public void setVdef12(String vdef12) {
		this.vdef12 = vdef12;
	}
	public String getVdef13() {
		return vdef13;
	}
	public void setVdef13(String vdef13) {
		this.vdef13 = vdef13;
	}
	public String getVdef14() {
		return vdef14;
	}
	public void setVdef14(String vdef14) {
		this.vdef14 = vdef14;
	}
	public String getVdef15() {
		return vdef15;
	}
	public void setVdef15(String vdef15) {
		this.vdef15 = vdef15;
	}
	public String getVdef16() {
		return vdef16;
	}
	public void setVdef16(String vdef16) {
		this.vdef16 = vdef16;
	}
	public String getVdef17() {
		return vdef17;
	}
	public void setVdef17(String vdef17) {
		this.vdef17 = vdef17;
	}
	public String getVdef18() {
		return vdef18;
	}
	public void setVdef18(String vdef18) {
		this.vdef18 = vdef18;
	}
	public String getVdef19() {
		return vdef19;
	}
	public void setVdef19(String vdef19) {
		this.vdef19 = vdef19;
	}
	public String getVdef20() {
		return vdef20;
	}
	public void setVdef20(String vdef20) {
		this.vdef20 = vdef20;
	}
	public String getVfoodsign() {
		return vfoodsign;
	}
	public void setVfoodsign(String vfoodsign) {
		this.vfoodsign = vfoodsign;
	}
	public String getViscy() {
		return viscy;
	}
	public void setViscy(String viscy) {
		this.viscy = viscy;
	}
	public double getNupmoney() {
		return nupmoney;
	}
	public void setNupmoney(double nupmoney) {
		this.nupmoney = nupmoney;
	}
	public double getNdownmoney() {
		return ndownmoney;
	}
	public void setNdownmoney(double ndownmoney) {
		this.ndownmoney = ndownmoney;
	}
}
