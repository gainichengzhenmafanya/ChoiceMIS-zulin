package com.choice.misboh.domain.BaseRecord;

/**
 * 描述：营业时段设置
 * @author 马振
 * 创建时间：2015-4-23 上午10:05:51
 */
public class Interval {
	private String  pk_interval;	//主键
	private String  vmemo; 			//备注
	private String  vname;			//名称
	private String  vcode;			//编码
	private String  vinit;			//缩写
	private Integer enablestate = 2;//状态
	private Integer isortno;		//排序
	private String  vstarttime;		//开始时间
	private String  vendtime;		//结束时间
	private String vintervaltype;	//时段类型
	private String  creator;		//创建人
	private String  creationtime;	//创建时间
	private String  modifier;		//修改人
	private String  modifiedtime;	//修改时间
	public String getPk_interval() {
		return pk_interval;
	}
	public void setPk_interval(String pk_interval) {
		this.pk_interval = pk_interval;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVinit() {
		return vinit;
	}
	public void setVinit(String vinit) {
		this.vinit = vinit;
	}
	public Integer getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(Integer enablestate) {
		this.enablestate = enablestate;
	}
	public Integer getIsortno() {
		return isortno;
	}
	public void setIsortno(Integer isortno) {
		this.isortno = isortno;
	}
	public String getVstarttime() {
		return vstarttime;
	}
	public void setVstarttime(String vstarttime) {
		this.vstarttime = vstarttime;
	}
	public String getVendtime() {
		return vendtime;
	}
	public void setVendtime(String vendtime) {
		this.vendtime = vendtime;
	}
	public String getVintervaltype() {
		return vintervaltype;
	}
	public void setVintervaltype(String vintervaltype) {
		this.vintervaltype = vintervaltype;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getCreationtime() {
		return creationtime;
	}
	public void setCreationtime(String creationtime) {
		this.creationtime = creationtime;
	}
	public String getModifier() {
		return modifier;
	}
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	public String getModifiedtime() {
		return modifiedtime;
	}
	public void setModifiedtime(String modifiedtime) {
		this.modifiedtime = modifiedtime;
	}
	
}
