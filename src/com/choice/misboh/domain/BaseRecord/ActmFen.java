package com.choice.misboh.domain.BaseRecord;

/**
 * 描述：消费返积分实例
 * @author 马振
 * 创建时间：2015-4-21 下午4:47:15
 */
public class ActmFen {
	private String pk_actmfen; 		//主键
	private String pk_actm; 		//活动主键
	private Integer ifloormoney; 	//下限金额
	private Integer iceilmoney; 	//上限金额
	private String vfentyp; 		//积分类型,按比例还是按固定
	private Double ifennum; 		//比例或者积分数
	public String getPk_actmfen() {
		return pk_actmfen;
	}
	public void setPk_actmfen(String pk_actmfen) {
		this.pk_actmfen = pk_actmfen;
	}
	public String getPk_actm() {
		return pk_actm;
	}
	public void setPk_actm(String pk_actm) {
		this.pk_actm = pk_actm;
	}
	public Integer getIfloormoney() {
		return ifloormoney;
	}
	public void setIfloormoney(Integer ifloormoney) {
		this.ifloormoney = ifloormoney;
	}
	public Integer getIceilmoney() {
		return iceilmoney;
	}
	public void setIceilmoney(Integer iceilmoney) {
		this.iceilmoney = iceilmoney;
	}
	public String getVfentyp() {
		return vfentyp;
	}
	public void setVfentyp(String vfentyp) {
		this.vfentyp = vfentyp;
	}
	public Double getIfennum() {
		return ifennum;
	}
	public void setIfennum(Double ifennum) {
		this.ifennum = ifennum;
	}	
}
