package com.choice.misboh.domain.BaseRecord;

/**
 * 描述：水电气表
 * @author 马振
 * 创建时间：2015-4-2 下午2:12:27
 */
public class OtherStockDataO {
	private String 	pk_OtherStockDataO;	//主键
	private String  vcode;				//编码
	private String 	pk_store;			//门店主键
	private String 	workdate;			//日期
	private Double begincount;			//期初
	private Double endcount;			//期末
	private Double	outcount;			//用量
	private Double	incount;			//进货数量
	private Double 	price;				//单价
	private String 	ecode;				//操作人
	private String  ctime;				//操作时间
	private String  week;				//星期
	private Double  amount;				//总金额（单价   * 用量）
	private String  pk_otherstockdataf; //主表主键
	private boolean flag;				//两个日期比较后的值
	private Integer type;				//统计类型 1水/2电/3天然气/4煤气
	
	public String getPk_OtherStockDataO() {
		return pk_OtherStockDataO;
	}
	public void setPk_OtherStockDataO(String pk_OtherStockDataO) {
		this.pk_OtherStockDataO = pk_OtherStockDataO;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
	public String getWorkdate() {
		return workdate;
	}
	public void setWorkdate(String workdate) {
		this.workdate = workdate;
	}
	public Double getBegincount() {
		return begincount;
	}
	public void setBegincount(Double begincount) {
		this.begincount = begincount;
	}
	public Double getEndcount() {
		return endcount;
	}
	public void setEndcount(Double endcount) {
		this.endcount = endcount;
	}
	public Double getOutcount() {
		return outcount;
	}
	public void setOutcount(Double outcount) {
		this.outcount = outcount;
	}
	public Double getIncount() {
		return incount;
	}
	public void setIncount(Double incount) {
		this.incount = incount;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getEcode() {
		return ecode;
	}
	public void setEcode(String ecode) {
		this.ecode = ecode;
	}
	public String getCtime() {
		return ctime;
	}
	public void setCtime(String ctime) {
		this.ctime = ctime;
	}
	public String getWeek() {
		return week;
	}
	public void setWeek(String week) {
		this.week = week;
	}
	public String getPk_otherstockdataf() {
		return pk_otherstockdataf;
	}
	public void setPk_otherstockdataf(String pk_otherstockdataf) {
		this.pk_otherstockdataf = pk_otherstockdataf;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
}
