package com.choice.misboh.domain.BaseRecord;

/**
 * 描述：菜品设置
 * @author 马振
 * 创建时间：2015-4-2 上午10:01:56
 */
public class PubItem {
	private String pk_pubitem;		//主键
	private String vcode;			//编码
	private String vname;			//名称
	private String vinit;			//缩写
	private String vdese;			//别名
	private Integer enablestate = 2;//启用状态
	private String vclass;			//PAD类别
	private String vpap;			//默认图片
	private String vpicbig;			//大图
	private String vpicsmall;		//小图
	private String vunit;			//单位
	private String vgrptyp;			//大类
	private String vgrp;			//中类
	private String vtyp;			//小类
	private String pk_vgrptyp;		//大类id
	private String pk_vgrp;			//中类id
	private String pk_vtyp;			//小类id
	private String vsptyp;			//优惠类别
	private String vtypoth;			//辅助类别
	private Integer imod;			//经营模式
	private Long naidnum;			//辅助数量
	private String vquick;			//是否核心菜
	private String vspecialprice;	//明星菜
	private String vhighprice;		//自选菜
	private String vyheigh;			//统计菜
	private String vyttl;			//高档菜
	private String vplusd;			//是否只数
	private String vyrapid;			//燕鲍翅鱼类
	private String vpprntyp;		//是否总单类型
	private String vpriormth;		//是否允许修改价格
	private String vnodisc;			//是否允许打折
	private Double nrestprice;		//堂食价格
	private Double noutprice;		//外带价格
	private Double notherprice;		//其他价格
	private Integer vunitprice;		//当前销售计价单位
	private Double nsecrestprice;	//第二单位堂食价格
	private Double nsecoutprice;	//第二单位外带价格
	private Double nsecotherprice;	//第二单位其他价格
	private String nunitprice;		//菜品单位重量
	private String remindtime;		//
	private Integer iadditemnum;	//附加项数量
	private String vyprinttimely;	//是否打印即时贴
	private Long nsafestock;		//安全库存
	private Integer vpricetyp;		//套餐价格类型
	private Integer isalfoodorderid;//销售菜品排列顺序
	private Integer isortno;		//排序号
	private Integer ipntbillorderid;//打印水单菜品顺序
	private Integer ikchnptderid;	//后厨菜品打印顺序
	private String vfontcolor;		//字体颜色
	private String vbgcolor;		//背景颜色-->实际是前景顏色，就是上来显示的颜色
	private Integer ibgsizew;		//背景大小（宽）
	private Integer ibgsizeh;		//背景大小（高）
	private Integer ifontsize;		//字体大小
	private String vfonttype;		//字体类型
	private String vimgename;		//图片存放地址
	private String vmsobject;		//互斥关系
	private String units;			//第二单位id
	private Long ncost;				//理论成本
	private String dept;			//小票打印出口
	private String vhsta;			//是否按顺序显示
	private String vhsta2;			//是否按顺序显示2
	private String vhsta3;			//是否按顺序显示3
	private Integer itemno;			//显示顺序
	private Integer itemno2;		//显示顺序2
	private Integer itemno3;		//显示顺序3
	private Double price7;			//价格7
	private Double price8;			//价格8
	private Double price9;			//价格9
	private Double price10;			//价格10
	private Double price11;			//价格11
	private String vrememo;			//备注
	private String pk_material;		//物料
	
	private String creator;			//创建人
	private String creationtime;	//创建时间
	private String modifier;		//修改人
	private String modifiedtime;	//修改时间
	private String ts;
	private Integer dr;
	
	private String vmsobjec;		//
	private String materialtaxtype; //物料税率
	private String pk_marbasclass;	//物料基本分类
	private String salesSpCode;		//销售物资
	private String vreqredefine;	//是否必选附加项
	private String vistemp;  		//是否临时菜
	private String vrealbgcolor;	//背景色----》实际的背景颜色
	private String vunit3;  		//单位3
	private String vunit4;  		//单位4
	private String vunit5;  		//单位5
	private String vunit6;  		//单位6
	private String visaddprod;		//附加产品标示
	private String prodreqaddFlag;	//是否必选产品
	private String pk_dishdisctyp; 	//菜品活动类别主键
	private String dishdisctyp; 	//菜品活动类别名称
	private String vcomp; 			//是否赠送
	private String pk_brand;
	private String pk_brand1;
	private String pk_brand2;
	private String pk_brand3;
	private String pk_brand4;
	private String pk_brand5;
	private String pk_group;
	private String pk_group1;
	private String pk_group2;
	private String pk_group3;
	private String pk_group4;
	private String pk_group5;
	
	private String pk_store;
	private String deptcode;//部门编码
	private String deptname;//部门名称
	private String unitname;//单位名称
	
	public String getUnitname() {
		return unitname;
	}
	public void setUnitname(String unitname) {
		this.unitname = unitname;
	}
	public String getDeptcode() {
		return deptcode;
	}
	public void setDeptcode(String deptcode) {
		this.deptcode = deptcode;
	}
	public String getDeptname() {
		return deptname;
	}
	public void setDeptname(String deptname) {
		this.deptname = deptname;
	}
	public String getProdreqaddFlag() {
		return prodreqaddFlag;
	}
	public void setProdreqaddFlag(String prodreqaddFlag) {
		this.prodreqaddFlag = prodreqaddFlag;
	}
	public String getPk_pubitem() {
		return pk_pubitem;
	}
	public void setPk_pubitem(String pk_pubitem) {
		this.pk_pubitem = pk_pubitem;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getVinit() {
		return vinit;
	}
	public void setVinit(String vinit) {
		this.vinit = vinit;
	}
	public String getVdese() {
		return vdese;
	}
	public void setVdese(String vdese) {
		this.vdese = vdese;
	}
	public Integer getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(Integer enablestate) {
		this.enablestate = enablestate;
	}
	public String getVclass() {
		return vclass;
	}
	public void setVclass(String vclass) {
		this.vclass = vclass;
	}
	public String getVpap() {
		return vpap;
	}
	public void setVpap(String vpap) {
		this.vpap = vpap;
	}
	public String getVpicbig() {
		return vpicbig;
	}
	public void setVpicbig(String vpicbig) {
		this.vpicbig = vpicbig;
	}
	public String getVpicsmall() {
		return vpicsmall;
	}
	public void setVpicsmall(String vpicsmall) {
		this.vpicsmall = vpicsmall;
	}
	public String getVunit() {
		return vunit;
	}
	public void setVunit(String vunit) {
		this.vunit = vunit;
	}
	public String getVgrptyp() {
		return vgrptyp;
	}
	public void setVgrptyp(String vgrptyp) {
		this.vgrptyp = vgrptyp;
	}
	public String getVgrp() {
		return vgrp;
	}
	public void setVgrp(String vgrp) {
		this.vgrp = vgrp;
	}
	public String getVtyp() {
		return vtyp;
	}
	public void setVtyp(String vtyp) {
		this.vtyp = vtyp;
	}
	public String getVsptyp() {
		return vsptyp;
	}
	public void setVsptyp(String vsptyp) {
		this.vsptyp = vsptyp;
	}
	public String getVtypoth() {
		return vtypoth;
	}
	public void setVtypoth(String vtypoth) {
		this.vtypoth = vtypoth;
	}
	public Integer getImod() {
		return imod;
	}
	public void setImod(Integer imod) {
		this.imod = imod;
	}
	public Long getNaidnum() {
		return naidnum;
	}
	public void setNaidnum(Long naidnum) {
		this.naidnum = naidnum;
	}
	public String getVquick() {
		return vquick;
	}
	public void setVquick(String vquick) {
		this.vquick = vquick;
	}
	public String getVspecialprice() {
		return vspecialprice;
	}
	public void setVspecialprice(String vspecialprice) {
		this.vspecialprice = vspecialprice;
	}
	public String getVhighprice() {
		return vhighprice;
	}
	public void setVhighprice(String vhighprice) {
		this.vhighprice = vhighprice;
	}
	public String getVyheigh() {
		return vyheigh;
	}
	public void setVyheigh(String vyheigh) {
		this.vyheigh = vyheigh;
	}
	public String getVyttl() {
		return vyttl;
	}
	public void setVyttl(String vyttl) {
		this.vyttl = vyttl;
	}
	public String getVplusd() {
		return vplusd;
	}
	public void setVplusd(String vplusd) {
		this.vplusd = vplusd;
	}
	public String getVyrapid() {
		return vyrapid;
	}
	public void setVyrapid(String vyrapid) {
		this.vyrapid = vyrapid;
	}
	public String getVpprntyp() {
		return vpprntyp;
	}
	public void setVpprntyp(String vpprntyp) {
		this.vpprntyp = vpprntyp;
	}
	public String getVpriormth() {
		return vpriormth;
	}
	public void setVpriormth(String vpriormth) {
		this.vpriormth = vpriormth;
	}
	public String getVnodisc() {
		return vnodisc;
	}
	public void setVnodisc(String vnodisc) {
		this.vnodisc = vnodisc;
	}
	public Double getNrestprice() {
		return nrestprice;
	}
	public void setNrestprice(Double nrestprice) {
		this.nrestprice = nrestprice;
	}
	public Double getNoutprice() {
		return noutprice;
	}
	public void setNoutprice(Double noutprice) {
		this.noutprice = noutprice;
	}
	public Double getNotherprice() {
		return notherprice;
	}
	public void setNotherprice(Double notherprice) {
		this.notherprice = notherprice;
	}
	public Integer getVunitprice() {
		return vunitprice;
	}
	public void setVunitprice(Integer vunitprice) {
		this.vunitprice = vunitprice;
	}
	public Double getNsecrestprice() {
		return nsecrestprice;
	}
	public void setNsecrestprice(Double nsecrestprice) {
		this.nsecrestprice = nsecrestprice;
	}
	public Double getNsecoutprice() {
		return nsecoutprice;
	}
	public void setNsecoutprice(Double nsecoutprice) {
		this.nsecoutprice = nsecoutprice;
	}
	public Double getNsecotherprice() {
		return nsecotherprice;
	}
	public void setNsecotherprice(Double nsecotherprice) {
		this.nsecotherprice = nsecotherprice;
	}
	public String getNunitprice() {
		return nunitprice;
	}
	public void setNunitprice(String nunitprice) {
		this.nunitprice = nunitprice;
	}
	public String getRemindtime() {
		return remindtime;
	}
	public void setRemindtime(String remindtime) {
		this.remindtime = remindtime;
	}
	public Integer getIadditemnum() {
		return iadditemnum;
	}
	public void setIadditemnum(Integer iadditemnum) {
		this.iadditemnum = iadditemnum;
	}
	public String getVyprinttimely() {
		return vyprinttimely;
	}
	public void setVyprinttimely(String vyprinttimely) {
		this.vyprinttimely = vyprinttimely;
	}
	public Long getNsafestock() {
		return nsafestock;
	}
	public void setNsafestock(Long nsafestock) {
		this.nsafestock = nsafestock;
	}
	public Integer getVpricetyp() {
		return vpricetyp;
	}
	public void setVpricetyp(Integer vpricetyp) {
		this.vpricetyp = vpricetyp;
	}
	public Integer getIsalfoodorderid() {
		return isalfoodorderid;
	}
	public void setIsalfoodorderid(Integer isalfoodorderid) {
		this.isalfoodorderid = isalfoodorderid;
	}
	public Integer getIsortno() {
		return isortno;
	}
	public void setIsortno(Integer isortno) {
		this.isortno = isortno;
	}
	public Integer getIpntbillorderid() {
		return ipntbillorderid;
	}
	public void setIpntbillorderid(Integer ipntbillorderid) {
		this.ipntbillorderid = ipntbillorderid;
	}
	public Integer getIkchnptderid() {
		return ikchnptderid;
	}
	public void setIkchnptderid(Integer ikchnptderid) {
		this.ikchnptderid = ikchnptderid;
	}
	public String getVfontcolor() {
		return vfontcolor;
	}
	public void setVfontcolor(String vfontcolor) {
		this.vfontcolor = vfontcolor;
	}
	public String getVbgcolor() {
		return vbgcolor;
	}
	public void setVbgcolor(String vbgcolor) {
		this.vbgcolor = vbgcolor;
	}
	public Integer getIbgsizew() {
		return ibgsizew;
	}
	public void setIbgsizew(Integer ibgsizew) {
		this.ibgsizew = ibgsizew;
	}
	public Integer getIbgsizeh() {
		return ibgsizeh;
	}
	public void setIbgsizeh(Integer ibgsizeh) {
		this.ibgsizeh = ibgsizeh;
	}
	public Integer getIfontsize() {
		return ifontsize;
	}
	public void setIfontsize(Integer ifontsize) {
		this.ifontsize = ifontsize;
	}
	public String getVfonttype() {
		return vfonttype;
	}
	public void setVfonttype(String vfonttype) {
		this.vfonttype = vfonttype;
	}
	public String getVimgename() {
		return vimgename;
	}
	public void setVimgename(String vimgename) {
		this.vimgename = vimgename;
	}
	public String getVmsobject() {
		return vmsobject;
	}
	public void setVmsobject(String vmsobject) {
		this.vmsobject = vmsobject;
	}
	public String getUnits() {
		return units;
	}
	public void setUnits(String units) {
		this.units = units;
	}
	public Long getNcost() {
		return ncost;
	}
	public void setNcost(Long ncost) {
		this.ncost = ncost;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getVhsta() {
		return vhsta;
	}
	public void setVhsta(String vhsta) {
		this.vhsta = vhsta;
	}
	public String getVhsta2() {
		return vhsta2;
	}
	public void setVhsta2(String vhsta2) {
		this.vhsta2 = vhsta2;
	}
	public String getVhsta3() {
		return vhsta3;
	}
	public void setVhsta3(String vhsta3) {
		this.vhsta3 = vhsta3;
	}
	public Integer getItemno() {
		return itemno;
	}
	public void setItemno(Integer itemno) {
		this.itemno = itemno;
	}
	public Integer getItemno2() {
		return itemno2;
	}
	public void setItemno2(Integer itemno2) {
		this.itemno2 = itemno2;
	}
	public Integer getItemno3() {
		return itemno3;
	}
	public void setItemno3(Integer itemno3) {
		this.itemno3 = itemno3;
	}
	public Double getPrice7() {
		return price7;
	}
	public void setPrice7(Double price7) {
		this.price7 = price7;
	}
	public Double getPrice8() {
		return price8;
	}
	public void setPrice8(Double price8) {
		this.price8 = price8;
	}
	public Double getPrice9() {
		return price9;
	}
	public void setPrice9(Double price9) {
		this.price9 = price9;
	}
	public Double getPrice10() {
		return price10;
	}
	public void setPrice10(Double price10) {
		this.price10 = price10;
	}
	public Double getPrice11() {
		return price11;
	}
	public void setPrice11(Double price11) {
		this.price11 = price11;
	}
	public String getVrememo() {
		return vrememo;
	}
	public void setVrememo(String vrememo) {
		this.vrememo = vrememo;
	}
	public String getPk_material() {
		return pk_material;
	}
	public void setPk_material(String pk_material) {
		this.pk_material = pk_material;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getCreationtime() {
		return creationtime;
	}
	public void setCreationtime(String creationtime) {
		this.creationtime = creationtime;
	}
	public String getModifier() {
		return modifier;
	}
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	public String getModifiedtime() {
		return modifiedtime;
	}
	public void setModifiedtime(String modifiedtime) {
		this.modifiedtime = modifiedtime;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public Integer getDr() {
		return dr;
	}
	public void setDr(Integer dr) {
		this.dr = dr;
	}
	public String getVmsobjec() {
		return vmsobjec;
	}
	public void setVmsobjec(String vmsobjec) {
		this.vmsobjec = vmsobjec;
	}
	public String getPk_marbasclass() {
		return pk_marbasclass;
	}
	public void setPk_marbasclass(String pk_marbasclass) {
		this.pk_marbasclass = pk_marbasclass;
	}
	public String getMaterialtaxtype() {
		return materialtaxtype;
	}
	public void setMaterialtaxtype(String materialtaxtype) {
		this.materialtaxtype = materialtaxtype;
	}
	public String getSalesSpCode() {
		return salesSpCode;
	}
	public void setSalesSpCode(String salesSpCode) {
		this.salesSpCode = salesSpCode;
	}
	public String getVreqredefine() {
		return vreqredefine;
	}
	public void setVreqredefine(String vreqredefine) {
		this.vreqredefine = vreqredefine;
	}
	public String getVistemp() {
		return vistemp;
	}
	public void setVistemp(String vistemp) {
		this.vistemp = vistemp;
	}
	public String getVrealbgcolor() {
		return vrealbgcolor;
	}
	public void setVrealbgcolor(String vrealbgcolor) {
		this.vrealbgcolor = vrealbgcolor;
	}
	public String getVunit3() {
		return vunit3;
	}
	public void setVunit3(String vunit3) {
		this.vunit3 = vunit3;
	}
	public String getVunit4() {
		return vunit4;
	}
	public void setVunit4(String vunit4) {
		this.vunit4 = vunit4;
	}
	public String getVunit5() {
		return vunit5;
	}
	public void setVunit5(String vunit5) {
		this.vunit5 = vunit5;
	}
	public String getVunit6() {
		return vunit6;
	}
	public void setVunit6(String vunit6) {
		this.vunit6 = vunit6;
	}
	public String getVisaddprod() {
		return visaddprod;
	}
	public void setVisaddprod(String visaddprod) {
		this.visaddprod = visaddprod;
	}
	public String getPk_dishdisctyp() {
		return pk_dishdisctyp;
	}
	public void setPk_dishdisctyp(String pk_dishdisctyp) {
		this.pk_dishdisctyp = pk_dishdisctyp;
	}
	public String getDishdisctyp() {
		return dishdisctyp;
	}
	public void setDishdisctyp(String dishdisctyp) {
		this.dishdisctyp = dishdisctyp;
	}
	public String getVcomp() {
		return vcomp;
	}
	public void setVcomp(String vcomp) {
		this.vcomp = vcomp;
	}
	public String getPk_brand() {
		return pk_brand;
	}
	public void setPk_brand(String pk_brand) {
		this.pk_brand = pk_brand;
	}
	public String getPk_brand1() {
		return pk_brand1;
	}
	public void setPk_brand1(String pk_brand1) {
		this.pk_brand1 = pk_brand1;
	}
	public String getPk_brand2() {
		return pk_brand2;
	}
	public void setPk_brand2(String pk_brand2) {
		this.pk_brand2 = pk_brand2;
	}
	public String getPk_brand3() {
		return pk_brand3;
	}
	public void setPk_brand3(String pk_brand3) {
		this.pk_brand3 = pk_brand3;
	}
	public String getPk_brand4() {
		return pk_brand4;
	}
	public void setPk_brand4(String pk_brand4) {
		this.pk_brand4 = pk_brand4;
	}
	public String getPk_brand5() {
		return pk_brand5;
	}
	public void setPk_brand5(String pk_brand5) {
		this.pk_brand5 = pk_brand5;
	}
	public String getPk_group() {
		return pk_group;
	}
	public void setPk_group(String pk_group) {
		this.pk_group = pk_group;
	}
	public String getPk_group1() {
		return pk_group1;
	}
	public void setPk_group1(String pk_group1) {
		this.pk_group1 = pk_group1;
	}
	public String getPk_group2() {
		return pk_group2;
	}
	public void setPk_group2(String pk_group2) {
		this.pk_group2 = pk_group2;
	}
	public String getPk_group3() {
		return pk_group3;
	}
	public void setPk_group3(String pk_group3) {
		this.pk_group3 = pk_group3;
	}
	public String getPk_group4() {
		return pk_group4;
	}
	public void setPk_group4(String pk_group4) {
		this.pk_group4 = pk_group4;
	}
	public String getPk_group5() {
		return pk_group5;
	}
	public void setPk_group5(String pk_group5) {
		this.pk_group5 = pk_group5;
	}
	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
	public String getPk_vgrptyp() {
		return pk_vgrptyp;
	}
	public void setPk_vgrptyp(String pk_vgrptyp) {
		this.pk_vgrptyp = pk_vgrptyp;
	}
	public String getPk_vgrp() {
		return pk_vgrp;
	}
	public void setPk_vgrp(String pk_vgrp) {
		this.pk_vgrp = pk_vgrp;
	}
	public String getPk_vtyp() {
		return pk_vtyp;
	}
	public void setPk_vtyp(String pk_vtyp) {
		this.pk_vtyp = pk_vtyp;
	}
}
