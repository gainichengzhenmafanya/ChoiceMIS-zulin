package com.choice.misboh.domain.BaseRecord;

/**
 * 描述：消费返券实例
 * @author 马振
 * 创建时间：2015-4-21 下午4:30:06
 */
public class ActmVoucher {
	private String pk_actmvoucher; 	//主键
	private String pk_actm; 		//活动主键
	private String vvouchercode; 	//券编码
	private String ivouchernum; 	//券数量
	private String vmemo; 			//备注
	private String ityp;			//券类型
	private String vname;			//券名称
	private String typname;         //券类型名称
	
	public String getItyp() {
		return ityp;
	}
	public void setItyp(String ityp) {
		this.ityp = ityp;
	}
	public String getPk_actmvoucher() {
		return pk_actmvoucher;
	}
	public void setPk_actmvoucher(String pk_actmvoucher) {
		this.pk_actmvoucher = pk_actmvoucher;
	}
	public String getPk_actm() {
		return pk_actm;
	}
	public void setPk_actm(String pk_actm) {
		this.pk_actm = pk_actm;
	}
	public String getVvouchercode() {
		return vvouchercode;
	}
	public void setVvouchercode(String vvouchercode) {
		this.vvouchercode = vvouchercode;
	}
	public String getIvouchernum() {
		return ivouchernum;
	}
	public void setIvouchernum(String ivouchernum) {
		this.ivouchernum = ivouchernum;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getTypname() {
		return typname;
	}
	public void setTypname(String typname) {
		this.typname = typname;
	}
}
