package com.choice.misboh.domain.BaseRecord;

/**
 * 描述：门店POS设置
 * @author 马振
 * 创建时间：2015-4-2 下午2:12:27
 */
public class StorePos {
	private String 	pk_pos;			//主键
	private String	vmemo;			//备注
	private String  icode;			//编码
	private String 	pk_store;		//门店主键
	private String 	posip;			//posIP
	private Integer enablestate;	//启用状态
	private String 	creator;		//创建人
	private String 	creationtime;	//创建时间
	private String 	modifier;		//修改人
	private String 	modifiedtime;	//修改时间
	private String 	ts;				//
	private String  pk_operator;
	private String  pk_brand;
	private String  pk_brand1;
	private String  pk_brand2;
	private String  pk_brand3;
	private String  pk_brand4;
	private String  pk_brand5;
	private String  pk_group;
	private String  pk_group1;
	private String  pk_group2;
	private String  pk_group3;
	private String  pk_group4;
	private String  pk_group5;
	
	public String getPk_pos() {
		return pk_pos;
	}
	public void setPk_pos(String pk_pos) {
		this.pk_pos = pk_pos;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public String getIcode() {
		return icode;
	}
	public void setIcode(String icode) {
		this.icode = icode;
	}

	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
	public String getPosip() {
		return posip;
	}
	public void setPosip(String posip) {
		this.posip = posip;
	}
	public Integer getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(Integer enablestate) {
		this.enablestate = enablestate;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getCreationtime() {
		return creationtime;
	}
	public void setCreationtime(String creationtime) {
		this.creationtime = creationtime;
	}
	public String getModifier() {
		return modifier;
	}
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	public String getModifiedtime() {
		return modifiedtime;
	}
	public void setModifiedtime(String modifiedtime) {
		this.modifiedtime = modifiedtime;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public String getPk_operator() {
		return pk_operator;
	}
	public void setPk_operator(String pk_operator) {
		this.pk_operator = pk_operator;
	}
	public String getPk_brand() {
		return pk_brand;
	}
	public void setPk_brand(String pk_brand) {
		this.pk_brand = pk_brand;
	}
	public String getPk_brand1() {
		return pk_brand1;
	}
	public void setPk_brand1(String pk_brand1) {
		this.pk_brand1 = pk_brand1;
	}
	public String getPk_brand2() {
		return pk_brand2;
	}
	public void setPk_brand2(String pk_brand2) {
		this.pk_brand2 = pk_brand2;
	}
	public String getPk_brand3() {
		return pk_brand3;
	}
	public void setPk_brand3(String pk_brand3) {
		this.pk_brand3 = pk_brand3;
	}
	public String getPk_brand4() {
		return pk_brand4;
	}
	public void setPk_brand4(String pk_brand4) {
		this.pk_brand4 = pk_brand4;
	}
	public String getPk_brand5() {
		return pk_brand5;
	}
	public void setPk_brand5(String pk_brand5) {
		this.pk_brand5 = pk_brand5;
	}
	public String getPk_group() {
		return pk_group;
	}
	public void setPk_group(String pk_group) {
		this.pk_group = pk_group;
	}
	public String getPk_group1() {
		return pk_group1;
	}
	public void setPk_group1(String pk_group1) {
		this.pk_group1 = pk_group1;
	}
	public String getPk_group2() {
		return pk_group2;
	}
	public void setPk_group2(String pk_group2) {
		this.pk_group2 = pk_group2;
	}
	public String getPk_group3() {
		return pk_group3;
	}
	public void setPk_group3(String pk_group3) {
		this.pk_group3 = pk_group3;
	}
	public String getPk_group4() {
		return pk_group4;
	}
	public void setPk_group4(String pk_group4) {
		this.pk_group4 = pk_group4;
	}
	public String getPk_group5() {
		return pk_group5;
	}
	public void setPk_group5(String pk_group5) {
		this.pk_group5 = pk_group5;
	}
}
