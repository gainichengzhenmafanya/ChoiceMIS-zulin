package com.choice.misboh.domain.BaseRecord;

/**
 * 描述：台位类型
 * @author 马振
 * 创建时间：2016-2-22 下午1:29:08
 */
public class SiteType {

	private String vname5;
	private String pk_sitetype;
	private String vmemo;
	private String vname;
	private String vcode;
	private String vinit;
	private Integer enablestate = 2 ;
	private Integer isortno;
	private String creator;
	private String creationtime;
	private String modifier;
	private String modifiedtime;
	private String ts;
	
	private Integer minpax;
	private Integer maxpax;
	private Integer bzpax;
	private Integer callingno;
	
	public Integer getMinpax() {
		return minpax;
	}
	public void setMinpax(Integer minpax) {
		this.minpax = minpax;
	}
	public Integer getMaxpax() {
		return maxpax;
	}
	public void setMaxpax(Integer maxpax) {
		this.maxpax = maxpax;
	}
	public Integer getBzpax() {
		return bzpax;
	}
	public void setBzpax(Integer bzpax) {
		this.bzpax = bzpax;
	}
	public Integer getCallingno() {
		return callingno;
	}
	public void setCallingno(Integer callingno) {
		this.callingno = callingno;
	}
	public String getVname5() {
		return vname5;
	}
	public void setVname5(String vname5) {
		this.vname5 = vname5;
	}
	public String getPk_sitetype() {
		return pk_sitetype;
	}
	public void setPk_sitetype(String pk_sitetype) {
		this.pk_sitetype = pk_sitetype;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVinit() {
		return vinit;
	}
	public void setVinit(String vinit) {
		this.vinit = vinit;
	}
	public Integer getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(Integer enablestate) {
		this.enablestate = enablestate;
	}
	public Integer getIsortno() {
		return isortno;
	}
	public void setIsortno(Integer isortno) {
		this.isortno = isortno;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getCreationtime() {
		return creationtime;
	}
	public void setCreationtime(String creationtime) {
		this.creationtime = creationtime;
	}
	public String getModifier() {
		return modifier;
	}
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	public String getModifiedtime() {
		return modifiedtime;
	}
	public void setModifiedtime(String modifiedtime) {
		this.modifiedtime = modifiedtime;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
}