package com.choice.misboh.domain.BaseRecord;

/**
 * 描述：星期及时段
 * @author 马振
 * 创建时间：2015-4-21 上午10:37:59
 */
public class Wptime {
	private String pk_wptime;
	private String pk_actm;		//活动主表
	private String weekly;		//是否周次限制
	private String pertime;		//是否时段限制
	private String vbegintime1;	//时段一开始
	private String vendtime1;	//时段一结束
	private String vbegintime2;	//时段二开始
	private String vendtime2;	//时段二结束
	private String vbegintime3;	//时段三开始
	private String vendtime3;	//时段三结束
	private String vbegintime4;	//时段四开始
	private String vendtime4;	//时段四结束
	/**
	 * @return pk_wptime
	 */
	public String getPk_wptime() {
		return pk_wptime;
	}
	/**
	 * @param pk_wptime the pk_wptime to set
	 */
	public void setPk_wptime(String pk_wptime) {
		this.pk_wptime = pk_wptime;
	}
	/**
	 * @return weekly
	 */
	public String getWeekly() {
		return weekly;
	}
	/**
	 * @param weekly the weekly to set
	 */
	public void setWeekly(String weekly) {
		this.weekly = weekly;
	}
	/**
	 * @return pertime
	 */
	public String getPertime() {
		return pertime;
	}
	/**
	 * @param pertime the pertime to set
	 */
	public void setPertime(String pertime) {
		this.pertime = pertime;
	}
	/**
	 * @return vbegintime1
	 */
	public String getVbegintime1() {
		return vbegintime1;
	}
	/**
	 * @param vbegintime1 the vbegintime1 to set
	 */
	public void setVbegintime1(String vbegintime1) {
		this.vbegintime1 = vbegintime1;
	}
	/**
	 * @return vendtime1
	 */
	public String getVendtime1() {
		return vendtime1;
	}
	/**
	 * @param vendtime1 the vendtime1 to set
	 */
	public void setVendtime1(String vendtime1) {
		this.vendtime1 = vendtime1;
	}
	/**
	 * @return vbegintime2
	 */
	public String getVbegintime2() {
		return vbegintime2;
	}
	/**
	 * @param vbegintime2 the vbegintime2 to set
	 */
	public void setVbegintime2(String vbegintime2) {
		this.vbegintime2 = vbegintime2;
	}
	/**
	 * @return vendtime2
	 */
	public String getVendtime2() {
		return vendtime2;
	}
	/**
	 * @param vendtime2 the vendtime2 to set
	 */
	public void setVendtime2(String vendtime2) {
		this.vendtime2 = vendtime2;
	}
	/**
	 * @return vbegintime3
	 */
	public String getVbegintime3() {
		return vbegintime3;
	}
	/**
	 * @param vbegintime3 the vbegintime3 to set
	 */
	public void setVbegintime3(String vbegintime3) {
		this.vbegintime3 = vbegintime3;
	}
	/**
	 * @return vendtime3
	 */
	public String getVendtime3() {
		return vendtime3;
	}
	/**
	 * @param vendtime3 the vendtime3 to set
	 */
	public void setVendtime3(String vendtime3) {
		this.vendtime3 = vendtime3;
	}
	/**
	 * @return vbegintime4
	 */
	public String getVbegintime4() {
		return vbegintime4;
	}
	/**
	 * @param vbegintime4 the vbegintime4 to set
	 */
	public void setVbegintime4(String vbegintime4) {
		this.vbegintime4 = vbegintime4;
	}
	/**
	 * @return vendtime4
	 */
	public String getVendtime4() {
		return vendtime4;
	}
	/**
	 * @param vendtime4 the vendtime4 to set
	 */
	public void setVendtime4(String vendtime4) {
		this.vendtime4 = vendtime4;
	}
	/**
	 * @return pk_actm
	 */
	public String getPk_actm() {
		return pk_actm;
	}
	/**
	 * @param pk_actm the pk_actm to set
	 */
	public void setPk_actm(String pk_actm) {
		this.pk_actm = pk_actm;
	}
	
}
