package com.choice.misboh.domain.BaseRecord;

/**
 * 描述：菜品类别
 * @author 马振
 * 创建时间：2015-4-20 上午11:26:09
 */
public class Marsaleclass {

	private static final long serialVersionUID = -1832920116722425171L;
	private String pk_Marsaleclass;	//子节点id
	private String pk_father;		//父类id
	private String codeZero;		//大类编码
	private String nameZero;		//大类名称
	private String codeOne;			//中类编码
	private String nameOne;			//中类名称
	private String pk_id;			//id，用于定位
	private String pk_cplb;			//小类
	private String vismainin;		//是否主营业务收入-鱼酷需求
	
	private String vfontcolor;		//字体颜色
	private String vbgcolor;		//背景颜色
	private Integer ibgsizew;		//背景大小（宽）
	private Integer ibgsizeh;		//背景大小（高）
	private Integer ifontsize;		//字体大小
	private String vfonttype;		//字体类型
	private String vpic;		    //图片
	
	private String bserviefee;      //是否服务费
	
	private String innercode;       
	
	private String vcncode;			//中餐编码
	
	private String typeval; 		//0是一级，1是二级，2是三级
	
	/**
	 * 菜品类别编码
	 */
	private String code ;
	/**
	 * 菜品类别名称
	 */
	private String name ;
	/**
	 * 启用状态
	 */
	private Integer enablestate = 2;

	/**
	 * 排序列
	 */
	private Integer isortno;
	/**
	 * 备注
	 */
	private String vmemo;
	/**
	 * 自定义项1，以下类推
	 */
	private String vdef1;
	private String vdef2;
	private String vdef3;
	private String vdef4;
	private String vdef5;
	private String vdef6;
	private String vdef7;
	private String vdef8;
	private String vdef9;
	private String vdef10;
	private String vdef11;
	private String vdef12;
	private String vdef13;
	private String vdef14;
	private String vdef15;
	private String vdef16;
	private String vdef17;
	private String vdef18;
	private String vdef19;
	/**
	 * 自定义项20
	 */
	private String vdef20;
	
	//接收树级菜单查询时的结果
	private String orderid;//级别
	private String parentid;//父id
	private String childrenid;//自身id
	
	private String parentname;//父名称
	private String parentcode;//父编码
	
	private String vmergerefbillflg; //退单合并标识
	
	/**
	 * @return the enablestate
	 */
	public Integer getEnablestate() {
		return enablestate;
	}
	/**
	 * @param enablestate the enablestate to set
	 */
	public void setEnablestate(Integer enablestate) {
		this.enablestate = enablestate;
	}

	/**
	 * @return the isortno
	 */
	public Integer getIsortno() {
		return isortno;
	}
	/**
	 * @param isortno the isortno to set
	 */
	public void setIsortno(Integer isortno) {
		this.isortno = isortno;
	}
	/**
	 * @return the vdef1
	 */
	public String getVdef1() {
		return vdef1;
	}
	/**
	 * @param vdef1 the vdef1 to set
	 */
	public void setVdef1(String vdef1) {
		this.vdef1 = vdef1;
	}
	/**
	 * @return the vdef2
	 */
	public String getVdef2() {
		return vdef2;
	}
	/**
	 * @param vdef2 the vdef2 to set
	 */
	public void setVdef2(String vdef2) {
		this.vdef2 = vdef2;
	}
	/**
	 * @return the vdef3
	 */
	public String getVdef3() {
		return vdef3;
	}
	/**
	 * @param vdef3 the vdef3 to set
	 */
	public void setVdef3(String vdef3) {
		this.vdef3 = vdef3;
	}
	/**
	 * @return the vdef4
	 */
	public String getVdef4() {
		return vdef4;
	}
	/**
	 * @param vdef4 the vdef4 to set
	 */
	public void setVdef4(String vdef4) {
		this.vdef4 = vdef4;
	}
	/**
	 * @return the vdef5
	 */
	public String getVdef5() {
		return vdef5;
	}
	/**
	 * @param vdef5 the vdef5 to set
	 */
	public void setVdef5(String vdef5) {
		this.vdef5 = vdef5;
	}
	/**
	 * @return the vdef6
	 */
	public String getVdef6() {
		return vdef6;
	}
	/**
	 * @param vdef6 the vdef6 to set
	 */
	public void setVdef6(String vdef6) {
		this.vdef6 = vdef6;
	}
	/**
	 * @return the vdef7
	 */
	public String getVdef7() {
		return vdef7;
	}
	/**
	 * @param vdef7 the vdef7 to set
	 */
	public void setVdef7(String vdef7) {
		this.vdef7 = vdef7;
	}
	/**
	 * @return the vdef8
	 */
	public String getVdef8() {
		return vdef8;
	}
	/**
	 * @param vdef8 the vdef8 to set
	 */
	public void setVdef8(String vdef8) {
		this.vdef8 = vdef8;
	}
	/**
	 * @return the vdef9
	 */
	public String getVdef9() {
		return vdef9;
	}
	/**
	 * @param vdef9 the vdef9 to set
	 */
	public void setVdef9(String vdef9) {
		this.vdef9 = vdef9;
	}
	/**
	 * @return the vdef10
	 */
	public String getVdef10() {
		return vdef10;
	}
	/**
	 * @param vdef10 the vdef10 to set
	 */
	public void setVdef10(String vdef10) {
		this.vdef10 = vdef10;
	}
	/**
	 * @return the vdef11
	 */
	public String getVdef11() {
		return vdef11;
	}
	/**
	 * @param vdef11 the vdef11 to set
	 */
	public void setVdef11(String vdef11) {
		this.vdef11 = vdef11;
	}
	/**
	 * @return the vdef12
	 */
	public String getVdef12() {
		return vdef12;
	}
	/**
	 * @param vdef12 the vdef12 to set
	 */
	public void setVdef12(String vdef12) {
		this.vdef12 = vdef12;
	}
	/**
	 * @return the vdef13
	 */
	public String getVdef13() {
		return vdef13;
	}
	/**
	 * @param vdef13 the vdef13 to set
	 */
	public void setVdef13(String vdef13) {
		this.vdef13 = vdef13;
	}
	/**
	 * @return the vdef14
	 */
	public String getVdef14() {
		return vdef14;
	}
	/**
	 * @param vdef14 the vdef14 to set
	 */
	public void setVdef14(String vdef14) {
		this.vdef14 = vdef14;
	}
	/**
	 * @return the vdef15
	 */
	public String getVdef15() {
		return vdef15;
	}
	/**
	 * @param vdef15 the vdef15 to set
	 */
	public void setVdef15(String vdef15) {
		this.vdef15 = vdef15;
	}
	/**
	 * @return the vdef16
	 */
	public String getVdef16() {
		return vdef16;
	}
	/**
	 * @param vdef16 the vdef16 to set
	 */
	public void setVdef16(String vdef16) {
		this.vdef16 = vdef16;
	}
	/**
	 * @return the vdef17
	 */
	public String getVdef17() {
		return vdef17;
	}
	/**
	 * @param vdef17 the vdef17 to set
	 */
	public void setVdef17(String vdef17) {
		this.vdef17 = vdef17;
	}
	/**
	 * @return the vdef18
	 */
	public String getVdef18() {
		return vdef18;
	}
	/**
	 * @param vdef18 the vdef18 to set
	 */
	public void setVdef18(String vdef18) {
		this.vdef18 = vdef18;
	}
	/**
	 * @return the vdef19
	 */
	public String getVdef19() {
		return vdef19;
	}
	/**
	 * @param vdef19 the vdef19 to set
	 */
	public void setVdef19(String vdef19) {
		this.vdef19 = vdef19;
	}
	/**
	 * @return the vdef20
	 */
	public String getVdef20() {
		return vdef20;
	}
	/**
	 * @param vdef20 the vdef20 to set
	 */
	public void setVdef20(String vdef20) {
		this.vdef20 = vdef20;
	}
	
	public String getPk_Marsaleclass() {
		return pk_Marsaleclass;
	}
	public void setPk_Marsaleclass(String pk_Marsaleclass) {
		this.pk_Marsaleclass = pk_Marsaleclass;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public String getPk_father() {
		return pk_father;
	}
	public void setPk_father(String pk_father) {
		this.pk_father = pk_father;
	}
	/**
	 * @return code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code 要设置的 code
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name 要设置的 name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/**
	 * @return codeZero
	 */
	public String getCodeZero() {
		return codeZero;
	}
	/**
	 * @param codeZero 要设置的 codeZero
	 */
	public void setCodeZero(String codeZero) {
		this.codeZero = codeZero;
	}
	/**
	 * @return nameZero
	 */
	public String getNameZero() {
		return nameZero;
	}
	/**
	 * @param nameZero 要设置的 nameZero
	 */
	public void setNameZero(String nameZero) {
		this.nameZero = nameZero;
	}
	/**
	 * @return codeOne
	 */
	public String getCodeOne() {
		return codeOne;
	}
	/**
	 * @param codeOne 要设置的 codeOne
	 */
	public void setCodeOne(String codeOne) {
		this.codeOne = codeOne;
	}
	/**
	 * @return nameOne
	 */
	public String getNameOne() {
		return nameOne;
	}
	/**
	 * @param nameOne 要设置的 nameOne
	 */
	public void setNameOne(String nameOne) {
		this.nameOne = nameOne;
	}
	/**
	 * @return pk_id
	 */
	public String getPk_id() {
		return pk_id;
	}
	/**
	 * @param pk_id 要设置的 pk_id
	 */
	public void setPk_id(String pk_id) {
		this.pk_id = pk_id;
	}
	public String getPk_cplb() {
		return pk_cplb;
	}
	public void setPk_cplb(String pk_cplb) {
		this.pk_cplb = pk_cplb;
	}
	public String getVismainin() {
		return vismainin;
	}
	public void setVismainin(String vismainin) {
		this.vismainin = vismainin;
	}
	public String getOrderid() {
		return orderid;
	}
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	public String getParentid() {
		return parentid;
	}
	public void setParentid(String parentid) {
		this.parentid = parentid;
	}
	public String getChildrenid() {
		return childrenid;
	}
	public void setChildrenid(String childrenid) {
		this.childrenid = childrenid;
	}
	public String getVfontcolor() {
		return vfontcolor;
	}
	public void setVfontcolor(String vfontcolor) {
		this.vfontcolor = vfontcolor;
	}
	public String getVbgcolor() {
		return vbgcolor;
	}
	public void setVbgcolor(String vbgcolor) {
		this.vbgcolor = vbgcolor;
	}
	public Integer getIbgsizew() {
		return ibgsizew;
	}
	public void setIbgsizew(Integer ibgsizew) {
		this.ibgsizew = ibgsizew;
	}
	public Integer getIbgsizeh() {
		return ibgsizeh;
	}
	public void setIbgsizeh(Integer ibgsizeh) {
		this.ibgsizeh = ibgsizeh;
	}
	public Integer getIfontsize() {
		return ifontsize;
	}
	public void setIfontsize(Integer ifontsize) {
		this.ifontsize = ifontsize;
	}
	public String getVfonttype() {
		return vfonttype;
	}
	public void setVfonttype(String vfonttype) {
		this.vfonttype = vfonttype;
	}
	public String getVpic() {
		return vpic;
	}
	public void setVpic(String vpic) {
		this.vpic = vpic;
	}
	public String getBserviefee() {
		return bserviefee;
	}
	public void setBserviefee(String bserviefee) {
		this.bserviefee = bserviefee;
	}
	public String getParentname() {
		return parentname;
	}
	public void setParentname(String parentname) {
		this.parentname = parentname;
	}
	public String getParentcode() {
		return parentcode;
	}
	public void setParentcode(String parentcode) {
		this.parentcode = parentcode;
	}
	public String getVmergerefbillflg() {
		return vmergerefbillflg;
	}
	public void setVmergerefbillflg(String vmergerefbillflg) {
		this.vmergerefbillflg = vmergerefbillflg;
	}
	public String getInnercode() {
		return innercode;
	}
	public void setInnercode(String innercode) {
		this.innercode = innercode;
	}
	public String getVcncode() {
		return vcncode;
	}
	public void setVcncode(String vcncode) {
		this.vcncode = vcncode;
	}
	public String getTypeval() {
		return typeval;
	}
	public void setTypeval(String typeval) {
		this.typeval = typeval;
	}
	
}
