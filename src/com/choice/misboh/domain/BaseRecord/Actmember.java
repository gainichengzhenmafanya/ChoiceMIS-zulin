package com.choice.misboh.domain.BaseRecord;

/**
 * 描述：活动子表——活动会员表
 * @author 马振
 * 创建时间：2015-4-21 下午12:42:06
 */
public class Actmember {
	private String pk_actmember;
	private String pk_actm;			//活动ID
	private String pk_membertype;	//会员ID
	private String id;
	private String nam;
	public String getPk_actmember() {
		return pk_actmember;
	}
	public void setPk_actmember(String pk_actmember) {
		this.pk_actmember = pk_actmember;
	}
	public String getPk_actm() {
		return pk_actm;
	}
	public void setPk_actm(String pk_actm) {
		this.pk_actm = pk_actm;
	}
	public String getPk_membertype() {
		return pk_membertype;
	}
	public void setPk_membertype(String pk_membertype) {
		this.pk_membertype = pk_membertype;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNam() {
		return nam;
	}
	public void setNam(String nam) {
		this.nam = nam;
	}
	
	
	
	
}
