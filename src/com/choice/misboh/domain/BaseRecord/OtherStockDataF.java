package com.choice.misboh.domain.BaseRecord;

import java.util.List;

/**
 * 描述：水电气主表
 * @author 马振
 * 创建时间：2015-4-2 下午2:12:27
 */
public class OtherStockDataF {
	private String 	pk_otherstockdataf;	//主键
	private String  vname;				//编码
	private String 	pk_store;			//门店主键
	private String 	workdate;			//日期
	private Double begincount;			//期初
	private String  begindate;			//期初日期
	private Integer type;				//统计类型 1水/2电/3天然气/4煤气
	private String	unit;				//单位
	private String	scode;				//店铺
	private Double 	price;				//单价
	private String 	ecode;				//操作人
	private String  ctime;				//操作时间
	private Integer sort;				//排序
	private Double  meterrate;			//倍率
	private Integer prepayment;			//付款方式 1预付款 0后付款
	private String  vename;				//操作员名称
	private String  memo;				//备注
	private Double zcount;				//总用量
	private List<OtherStockDataO> stockDataList;//子表
	public String getPk_otherstockdataf() {
		return pk_otherstockdataf;
	}
	public void setPk_otherstockdataf(String pk_otherstockdataf) {
		this.pk_otherstockdataf = pk_otherstockdataf;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
	public String getWorkdate() {
		return workdate;
	}
	public void setWorkdate(String workdate) {
		this.workdate = workdate;
	}
	public Double getBegincount() {
		return begincount;
	}
	public void setBegincount(Double begincount) {
		this.begincount = begincount;
	}
	public String getBegindate() {
		return begindate;
	}
	public void setBegindate(String begindate) {
		this.begindate = begindate;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getScode() {
		return scode;
	}
	public void setScode(String scode) {
		this.scode = scode;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getEcode() {
		return ecode;
	}
	public void setEcode(String ecode) {
		this.ecode = ecode;
	}
	public String getCtime() {
		return ctime;
	}
	public void setCtime(String ctime) {
		this.ctime = ctime;
	}
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	public Double getMeterrate() {
		return meterrate;
	}
	public void setMeterrate(Double meterrate) {
		this.meterrate = meterrate;
	}
	public Integer getPrepayment() {
		return prepayment;
	}
	public void setPrepayment(Integer prepayment) {
		this.prepayment = prepayment;
	}
	public String getVename() {
		return vename;
	}
	public void setVename(String vename) {
		this.vename = vename;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public Double getZcount() {
		return zcount;
	}
	public void setZcount(Double zcount) {
		this.zcount = zcount;
	}
	public List<OtherStockDataO> getStockDataList() {
		return stockDataList;
	}
	public void setStockDataList(List<OtherStockDataO> stockDataList) {
		this.stockDataList = stockDataList;
	}
}
