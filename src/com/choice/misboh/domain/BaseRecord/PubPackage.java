package com.choice.misboh.domain.BaseRecord;


/**
 * 描述：套餐设置
 * @author 马振
 * 创建时间：2015-4-2 上午10:15:39
 */
public class PubPackage {
	private String pk_pubpack;		//主键
	private String vcode;			//编码
	private String vname;			//名称
	private String vpackcode;		//套餐号
	private String vinit;			//缩写
	private Integer enablestate = 2;//启用状态
	private Integer isortno;		//排序号
	private Integer imaxfood;		//最大菜品数
	private String bmodifyprice;	//是否允许修改价格
	private String bdiscount;		//是否可以打折
	private String bcancelfee;		//是否免服务费
	private String vgrptyp;			//类别id
	private String vsaleunit;		//销售单位id
	private Double nrestprice;		//堂食价格
	private Double ntheoryprice;	//理论成本
	private Double noutprice;		//外带价格
	private Double notherprice;		//其他价格
	private Double nsecrestprice;	//第二单位堂食价格
	private Double nsecoutprice;	//第二单位外带价格
	private Double nsecotherprice;	//第二单位其他价格
	private String vyprinttimely;	//是否打印即时贴
	private Integer isafestock;		//安全库存
	private Integer vpricetyp;		//套餐价格类型
	private Integer nsalfodordid;	//销售菜品排列顺序
	private Integer iptbilloid;		//打印水单
	private Integer iktchderid;		//后厨菜品打印顺序
	private String vfontcolor;		//字体颜色
	private String vbgcolor;		//背景颜色
	private Integer ibgsizew;		//背景大小（宽）
	private Integer ibgsizeh;		//背景大小（高）
	private Integer ifontsize;		//字体大小
	private String vfonttype;		//字体类型
	private String vmsobjec;		//互斥类型
	private Integer iextcount;		//附加项数量
	private Integer iprintorder;	//打印水单菜品顺序
	private Double nitemweight;		//菜品单位重量
	private String bisuseunit2;		//是否启用第二单位
	private String vunit2;			//第二单位id
	
	private String creator;			//创建人
	private String creationtime;	//创建时间
	private String modifier;		//修改人
	private String modifiedtime;	//修改时间
	private String ts;
	
	private String vsplittype;     	//套餐拆分方式（ 1.比例均摊 2.比例折扣 ）
	private String vclass;			//pad类别主键
	private Double nitemamt; 		//菜品明细合计
	private String vpicbig;			//大图
	private String vpicsmall;		//小图
	
	private String pk_brand;
	private String pk_brand1;
	private String pk_brand2;
	private String pk_brand3;
	private String pk_brand4;
	private String pk_brand5;
	private String pk_group;
	private String pk_group1;
	private String pk_group2;
	private String pk_group3;
	private String pk_group4;
	private String pk_group5;
	private String pk_store;
	
	public String getPk_pubpack() {
		return pk_pubpack;
	}
	public void setPk_pubpack(String pk_pubpack) {
		this.pk_pubpack = pk_pubpack;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getVpackcode() {
		return vpackcode;
	}
	public void setVpackcode(String vpackcode) {
		this.vpackcode = vpackcode;
	}
	public String getVinit() {
		return vinit;
	}
	public void setVinit(String vinit) {
		this.vinit = vinit;
	}
	public Integer getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(Integer enablestate) {
		this.enablestate = enablestate;
	}
	public Integer getIsortno() {
		return isortno;
	}
	public void setIsortno(Integer isortno) {
		this.isortno = isortno;
	}
	public Integer getImaxfood() {
		return imaxfood;
	}
	public void setImaxfood(Integer imaxfood) {
		this.imaxfood = imaxfood;
	}
	public String getBmodifyprice() {
		return bmodifyprice;
	}
	public void setBmodifyprice(String bmodifyprice) {
		this.bmodifyprice = bmodifyprice;
	}
	public String getBdiscount() {
		return bdiscount;
	}
	public void setBdiscount(String bdiscount) {
		this.bdiscount = bdiscount;
	}
	public String getBcancelfee() {
		return bcancelfee;
	}
	public void setBcancelfee(String bcancelfee) {
		this.bcancelfee = bcancelfee;
	}
	public String getVgrptyp() {
		return vgrptyp;
	}
	public void setVgrptyp(String vgrptyp) {
		this.vgrptyp = vgrptyp;
	}
	public String getVsaleunit() {
		return vsaleunit;
	}
	public void setVsaleunit(String vsaleunit) {
		this.vsaleunit = vsaleunit;
	}
	public Double getNrestprice() {
		return nrestprice;
	}
	public void setNrestprice(Double nrestprice) {
		this.nrestprice = nrestprice;
	}
	public Double getNtheoryprice() {
		return ntheoryprice;
	}
	public void setNtheoryprice(Double ntheoryprice) {
		this.ntheoryprice = ntheoryprice;
	}
	public Double getNoutprice() {
		return noutprice;
	}
	public void setNoutprice(Double noutprice) {
		this.noutprice = noutprice;
	}
	public Double getNotherprice() {
		return notherprice;
	}
	public void setNotherprice(Double notherprice) {
		this.notherprice = notherprice;
	}
	public Double getNsecrestprice() {
		return nsecrestprice;
	}
	public void setNsecrestprice(Double nsecrestprice) {
		this.nsecrestprice = nsecrestprice;
	}
	public Double getNsecoutprice() {
		return nsecoutprice;
	}
	public void setNsecoutprice(Double nsecoutprice) {
		this.nsecoutprice = nsecoutprice;
	}
	public Double getNsecotherprice() {
		return nsecotherprice;
	}
	public void setNsecotherprice(Double nsecotherprice) {
		this.nsecotherprice = nsecotherprice;
	}
	public String getVyprinttimely() {
		return vyprinttimely;
	}
	public void setVyprinttimely(String vyprinttimely) {
		this.vyprinttimely = vyprinttimely;
	}
	public Integer getIsafestock() {
		return isafestock;
	}
	public void setIsafestock(Integer isafestock) {
		this.isafestock = isafestock;
	}
	public Integer getVpricetyp() {
		return vpricetyp;
	}
	public void setVpricetyp(Integer vpricetyp) {
		this.vpricetyp = vpricetyp;
	}
	public Integer getNsalfodordid() {
		return nsalfodordid;
	}
	public void setNsalfodordid(Integer nsalfodordid) {
		this.nsalfodordid = nsalfodordid;
	}
	public Integer getIptbilloid() {
		return iptbilloid;
	}
	public void setIptbilloid(Integer iptbilloid) {
		this.iptbilloid = iptbilloid;
	}
	public Integer getIktchderid() {
		return iktchderid;
	}
	public void setIktchderid(Integer iktchderid) {
		this.iktchderid = iktchderid;
	}
	public String getVfontcolor() {
		return vfontcolor;
	}
	public void setVfontcolor(String vfontcolor) {
		this.vfontcolor = vfontcolor;
	}
	public String getVbgcolor() {
		return vbgcolor;
	}
	public void setVbgcolor(String vbgcolor) {
		this.vbgcolor = vbgcolor;
	}
	public Integer getIbgsizew() {
		return ibgsizew;
	}
	public void setIbgsizew(Integer ibgsizew) {
		this.ibgsizew = ibgsizew;
	}
	public Integer getIbgsizeh() {
		return ibgsizeh;
	}
	public void setIbgsizeh(Integer ibgsizeh) {
		this.ibgsizeh = ibgsizeh;
	}
	public Integer getIfontsize() {
		return ifontsize;
	}
	public void setIfontsize(Integer ifontsize) {
		this.ifontsize = ifontsize;
	}
	public String getVfonttype() {
		return vfonttype;
	}
	public void setVfonttype(String vfonttype) {
		this.vfonttype = vfonttype;
	}
	public String getVmsobjec() {
		return vmsobjec;
	}
	public void setVmsobjec(String vmsobjec) {
		this.vmsobjec = vmsobjec;
	}
	public Integer getIextcount() {
		return iextcount;
	}
	public void setIextcount(Integer iextcount) {
		this.iextcount = iextcount;
	}
	public Integer getIprintorder() {
		return iprintorder;
	}
	public void setIprintorder(Integer iprintorder) {
		this.iprintorder = iprintorder;
	}
	public Double getNitemweight() {
		return nitemweight;
	}
	public void setNitemweight(Double nitemweight) {
		this.nitemweight = nitemweight;
	}
	public String getBisuseunit2() {
		return bisuseunit2;
	}
	public void setBisuseunit2(String bisuseunit2) {
		this.bisuseunit2 = bisuseunit2;
	}
	public String getVunit2() {
		return vunit2;
	}
	public void setVunit2(String vunit2) {
		this.vunit2 = vunit2;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getCreationtime() {
		return creationtime;
	}
	public void setCreationtime(String creationtime) {
		this.creationtime = creationtime;
	}
	public String getModifier() {
		return modifier;
	}
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	public String getModifiedtime() {
		return modifiedtime;
	}
	public void setModifiedtime(String modifiedtime) {
		this.modifiedtime = modifiedtime;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public String getVsplittype() {
		return vsplittype;
	}
	public void setVsplittype(String vsplittype) {
		this.vsplittype = vsplittype;
	}
	public String getVclass() {
		return vclass;
	}
	public void setVclass(String vclass) {
		this.vclass = vclass;
	}
	public Double getNitemamt() {
		return nitemamt;
	}
	public void setNitemamt(Double nitemamt) {
		this.nitemamt = nitemamt;
	}
	public String getVpicbig() {
		return vpicbig;
	}
	public void setVpicbig(String vpicbig) {
		this.vpicbig = vpicbig;
	}
	public String getVpicsmall() {
		return vpicsmall;
	}
	public void setVpicsmall(String vpicsmall) {
		this.vpicsmall = vpicsmall;
	}
	public String getPk_brand() {
		return pk_brand;
	}
	public void setPk_brand(String pk_brand) {
		this.pk_brand = pk_brand;
	}
	public String getPk_brand1() {
		return pk_brand1;
	}
	public void setPk_brand1(String pk_brand1) {
		this.pk_brand1 = pk_brand1;
	}
	public String getPk_brand2() {
		return pk_brand2;
	}
	public void setPk_brand2(String pk_brand2) {
		this.pk_brand2 = pk_brand2;
	}
	public String getPk_brand3() {
		return pk_brand3;
	}
	public void setPk_brand3(String pk_brand3) {
		this.pk_brand3 = pk_brand3;
	}
	public String getPk_brand4() {
		return pk_brand4;
	}
	public void setPk_brand4(String pk_brand4) {
		this.pk_brand4 = pk_brand4;
	}
	public String getPk_brand5() {
		return pk_brand5;
	}
	public void setPk_brand5(String pk_brand5) {
		this.pk_brand5 = pk_brand5;
	}
	public String getPk_group() {
		return pk_group;
	}
	public void setPk_group(String pk_group) {
		this.pk_group = pk_group;
	}
	public String getPk_group1() {
		return pk_group1;
	}
	public void setPk_group1(String pk_group1) {
		this.pk_group1 = pk_group1;
	}
	public String getPk_group2() {
		return pk_group2;
	}
	public void setPk_group2(String pk_group2) {
		this.pk_group2 = pk_group2;
	}
	public String getPk_group3() {
		return pk_group3;
	}
	public void setPk_group3(String pk_group3) {
		this.pk_group3 = pk_group3;
	}
	public String getPk_group4() {
		return pk_group4;
	}
	public void setPk_group4(String pk_group4) {
		this.pk_group4 = pk_group4;
	}
	public String getPk_group5() {
		return pk_group5;
	}
	public void setPk_group5(String pk_group5) {
		this.pk_group5 = pk_group5;
	}
	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
}