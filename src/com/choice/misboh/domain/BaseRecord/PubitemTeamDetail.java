package com.choice.misboh.domain.BaseRecord;

/**
 * 菜品组明细定义
 * @author ygb
 *
 */
public class PubitemTeamDetail {
	
	private String pk_pubitemTeamDetail;
	private String pk_pubitemTeam;
	private String pk_pubitem;
	private String pubitemCode;
	private String pubitemName;
	private String vunitCode;
	private String vunitName;
	
	public String getPk_pubitemTeamDetail() {
		return pk_pubitemTeamDetail;
	}
	public void setPk_pubitemTeamDetail(String pk_pubitemTeamDetail) {
		this.pk_pubitemTeamDetail = pk_pubitemTeamDetail;
	}
	public String getPk_pubitemTeam() {
		return pk_pubitemTeam;
	}
	public void setPk_pubitemTeam(String pk_pubitemTeam) {
		this.pk_pubitemTeam = pk_pubitemTeam;
	}
	public String getPk_pubitem() {
		return pk_pubitem;
	}
	public void setPk_pubitem(String pk_pubitem) {
		this.pk_pubitem = pk_pubitem;
	}
	public String getPubitemCode() {
		return pubitemCode;
	}
	public void setPubitemCode(String pubitemCode) {
		this.pubitemCode = pubitemCode;
	}
	public String getPubitemName() {
		return pubitemName;
	}
	public void setPubitemName(String pubitemName) {
		this.pubitemName = pubitemName;
	}
	public String getVunitCode() {
		return vunitCode;
	}
	public void setVunitCode(String vunitCode) {
		this.vunitCode = vunitCode;
	}
	public String getVunitName() {
		return vunitName;
	}
	public void setVunitName(String vunitName) {
		this.vunitName = vunitName;
	}
	
}
