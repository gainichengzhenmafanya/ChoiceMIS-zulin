package com.choice.misboh.domain.BaseRecord;

/**
 * 描述：套餐类别
 * @author 马振
 * 创建时间：2015-4-20 下午6:24:44
 */
public class PackageType{
	
	private String pk_packagetype;	//主键
	private String vname;			//名称
	private String vcode;			//编码
	private Integer enablestate=2;	//启用状态
	private String vmemo;			//备注
	private String vismainin;		//是否主营业务收入-鱼酷需求
	
	public String getPk_packagetype() {
		return pk_packagetype;
	}
	public void setPk_packagetype(String pk_packagetype) {
		this.pk_packagetype = pk_packagetype;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public Integer getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(Integer enablestate) {
		this.enablestate = enablestate;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public String getVismainin() {
		return vismainin;
	}
	public void setVismainin(String vismainin) {
		this.vismainin = vismainin;
	}
	
}
