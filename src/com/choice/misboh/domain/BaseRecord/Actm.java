package com.choice.misboh.domain.BaseRecord;

/**
 * 描述：门店活动
 * @author 马振
 * 创建时间：2015-4-2 下午4:46:43
 */
public class Actm{
		private  String pk_actm;		//活动主键
		private  String vcode;			//活动编码
		private  String vname;			//活动名称
		private  Integer enablestate;	//启用状态
		private  String bsale;			//品项促销
		private  String bremit;			//账单减免
		private  String bgbond;			//消费返券
		private  String bgscore;		//返积分
		private  String nscorerate;		//积分比率
		private  String pk_acttyp;      
		private  String bisdate;		//是否启用日期
		private  String dstartdate;		//开始日期
		private  String denddate;		//结束日期
		private  String bistime;		//是否启用时间
		private  String vstarttime1;	//时段开始1
		private  String vendtime1;		//时段结束1
		private  String vstarttime2;	//时段开始2
		private  String vendtime2;		//时段结束2
		private  String vstarttime3;	//时段开始3
		private  String vendtime3;		//时段结束3
		private  String vstarttime4;	//时段开始4
		private  String vendtime4;		//时段结束4
		private  String bisamount;		//是否启用金额限制
		private  String iamount;		//金额上限
		private  String idownamount;	//金额下限
		
		private  String biscount;		//是否启用人数限制
		private  String itopnumber;		//人数上限
		private  String idownnumber;	//人数下限
		
		private  String bisstore;		//是否启用门店限制
		private  String bisticket;		//是否启用张数限制
		private  Integer iticketnum;	//张数数量
		private  String nderatenum;		//减免金额
		
		private  String bisweek;		//是否启用周次限制
		private  String bismonday;		//周一
		private  String bistuesday;		//周二
		private  String biswednesday;	//周三
		private  String bisthursday;	//周四
		private  String bisfriday;		//周五
		private  String bissaturday;	//周六
		private  String bissunday;		//周日
		
		private  String bisclasses;		//是否启用班次限制---@2014-3-14 修改为：限制条件bisclasses
		private  String bisoneclass;	//一班
		private  String bistwoclass;	//二班
		private  String bisthreeclass;	//三班
		private  String bisfourclass;	//四班
		private  String biscardtyp;		//是否启用卡型限制
		
		private  String bispubitemtyp;	//是否启用菜品类别限制--作废
		private  String bisppacktyp;	//是否启用套餐类别限制--作废
		private  String bispubitem;		//是否启用菜品限制	
		private  String bisppack;		//是否启用套餐限制
		
		private  String biseticket;		//是否启用电子券限制
		private  String vfontcolor;		//字体颜色
		private  String vbackgroud;		//背景颜色
		private  String vfont;			//字体
		private  Integer ifontsize;		//字体大小
		private  Integer ibackwidth;	//背景大小（宽）
		private  Integer ibackheight;	//背景大小（高）
		private  String vmemo;			//备注
		
		private  String bdiscount;		//是否启用减免---update--菜品折扣		
		private  Double ndiscountrate;	//折扣比率
		private  String badjust;		//是否调价
		private  Double nadjustmy;		//调价金额到
		private  Double poundage;		//手续费
		private  Double paidmoney;		//实收金额
		
		private  String tjyn;			//是否第二杯半价
		private  String operategroup;	//是否团购挂账
		private  String vouchers;		//是否抵用券
		private  String bishchd;		//是否启用互斥活动
		private  String bismdxz;		//是否启用门店限制
		private  String biscpmx;		//是否启用菜品明细设置
		private  String bistcmx;		//是否启用套餐明细设置
		private  String efzk;			//是否第二份折扣0:勾选，1不勾选
		private  String tjtype;
		private  String isauto;			//自动执行
		private  String ismember;		//会员限制
		private  String isrealupdate;	//是否更新适用门店
		private  String showinpad;		//pad是否可用
		
		private  String jmrdo;			//减免金额类型 0：金额，1：比例，2：比例输入
		private  String rdo;			//金额和产品的关系 Y：and，N：or
		private  String xztj;			//是否限制条件
		private  String bltype;			//Y:比例，N:固定
		private  String bisljxz;		//是否累计限制
		private  String pk_acttypmin;	//活动管理小类主键
		
		private String excprice;		//执行价格
		private String isvalidate;		//是否需要验证
		private String couponcode;		//验证标识，来自团购网站的编码或者代金券编码
		private String voucherback; 	//消費返券标识
		private String vfenback; 		//消費返券标识
		private String vfendisc;		//是否选中积分
		private Integer ifendiscnum;	//账单减免中的分数
		private String vvoucherdisc;	//电子券减免
		private String vvouchercode;	//电子券编码
		private String vvouchername;	//电子券名称
		private Integer ivoucherdiscnum;//电子券数量
		private Integer ityp;			//券类型
		private String jmtype; 			//减免类型,0为全单均摊,1为单菜减免(新辣道用)
		
		private String vtoprice; 		//调价到价格时是否需要会员验证
		private String vatwill; 		//任意的，标识活动是否是任意折扣或任意赠送，Y/N
		private String vproducttype; 	//是否限量，0 关 1 开 （根据日期限制） 2 每天执行，默认为0
		private String dxlsdate; 		//限量的开始日期，当限量类型为1的时候才设置
		private String visinputmoney;	//是否可以录入消费金额
		private String vthirdcode;		//第三方编码
		private String pk_store;		//门店
		private String cardtypdes;		//卡类型名称
		private String cardtyp;			//卡类型编码
		private  Integer itopamount;	//金额上限
		
		
		public String getPk_actm() {
			return pk_actm;
		}
		public void setPk_actm(String pk_actm) {
			this.pk_actm = pk_actm;
		}
		public String getVcode() {
			return vcode;
		}
		public void setVcode(String vcode) {
			this.vcode = vcode;
		}
		public String getVname() {
			return vname;
		}
		public void setVname(String vname) {
			this.vname = vname;
		}
		public Integer getEnablestate() {
			return enablestate;
		}
		public void setEnablestate(Integer enablestate) {
			this.enablestate = enablestate;
		}
		public String getBsale() {
			return bsale;
		}
		public void setBsale(String bsale) {
			this.bsale = bsale;
		}
		public String getBremit() {
			return bremit;
		}
		public void setBremit(String bremit) {
			this.bremit = bremit;
		}
		public String getBgbond() {
			return bgbond;
		}
		public void setBgbond(String bgbond) {
			this.bgbond = bgbond;
		}
		public String getBgscore() {
			return bgscore;
		}
		public void setBgscore(String bgscore) {
			this.bgscore = bgscore;
		}
		public String getNscorerate() {
			return nscorerate;
		}
		public void setNscorerate(String nscorerate) {
			this.nscorerate = nscorerate;
		}
		public String getPk_acttyp() {
			return pk_acttyp;
		}
		public void setPk_acttyp(String pk_acttyp) {
			this.pk_acttyp = pk_acttyp;
		}
		public String getBisdate() {
			return bisdate;
		}
		public void setBisdate(String bisdate) {
			this.bisdate = bisdate;
		}
		public String getDstartdate() {
			return dstartdate;
		}
		public void setDstartdate(String dstartdate) {
			this.dstartdate = dstartdate;
		}
		public String getDenddate() {
			return denddate;
		}
		public void setDenddate(String denddate) {
			this.denddate = denddate;
		}
		public String getBistime() {
			return bistime;
		}
		public void setBistime(String bistime) {
			this.bistime = bistime;
		}
		public String getVstarttime1() {
			return vstarttime1;
		}
		public void setVstarttime1(String vstarttime1) {
			this.vstarttime1 = vstarttime1;
		}
		public String getVendtime1() {
			return vendtime1;
		}
		public void setVendtime1(String vendtime1) {
			this.vendtime1 = vendtime1;
		}
		public String getVstarttime2() {
			return vstarttime2;
		}
		public void setVstarttime2(String vstarttime2) {
			this.vstarttime2 = vstarttime2;
		}
		public String getVendtime2() {
			return vendtime2;
		}
		public void setVendtime2(String vendtime2) {
			this.vendtime2 = vendtime2;
		}
		public String getVstarttime3() {
			return vstarttime3;
		}
		public void setVstarttime3(String vstarttime3) {
			this.vstarttime3 = vstarttime3;
		}
		public String getVendtime3() {
			return vendtime3;
		}
		public void setVendtime3(String vendtime3) {
			this.vendtime3 = vendtime3;
		}
		public String getVstarttime4() {
			return vstarttime4;
		}
		public void setVstarttime4(String vstarttime4) {
			this.vstarttime4 = vstarttime4;
		}
		public String getVendtime4() {
			return vendtime4;
		}
		public void setVendtime4(String vendtime4) {
			this.vendtime4 = vendtime4;
		}
		public String getBisamount() {
			return bisamount;
		}
		public void setBisamount(String bisamount) {
			this.bisamount = bisamount;
		}
		public String getIamount() {
			return iamount;
		}
		public void setIamount(String iamount) {
			this.iamount = iamount;
		}
		public String getIdownamount() {
			return idownamount;
		}
		public void setIdownamount(String idownamount) {
			this.idownamount = idownamount;
		}
		public String getBiscount() {
			return biscount;
		}
		public void setBiscount(String biscount) {
			this.biscount = biscount;
		}
		public String getItopnumber() {
			return itopnumber;
		}
		public void setItopnumber(String itopnumber) {
			this.itopnumber = itopnumber;
		}
		public String getIdownnumber() {
			return idownnumber;
		}
		public void setIdownnumber(String idownnumber) {
			this.idownnumber = idownnumber;
		}
		public String getBisstore() {
			return bisstore;
		}
		public void setBisstore(String bisstore) {
			this.bisstore = bisstore;
		}
		public String getBisticket() {
			return bisticket;
		}
		public void setBisticket(String bisticket) {
			this.bisticket = bisticket;
		}
		public Integer getIticketnum() {
			return iticketnum;
		}
		public void setIticketnum(Integer iticketnum) {
			this.iticketnum = iticketnum;
		}
		public String getNderatenum() {
			return nderatenum;
		}
		public void setNderatenum(String nderatenum) {
			this.nderatenum = nderatenum;
		}
		public String getBisweek() {
			return bisweek;
		}
		public void setBisweek(String bisweek) {
			this.bisweek = bisweek;
		}
		public String getBismonday() {
			return bismonday;
		}
		public void setBismonday(String bismonday) {
			this.bismonday = bismonday;
		}
		public String getBistuesday() {
			return bistuesday;
		}
		public void setBistuesday(String bistuesday) {
			this.bistuesday = bistuesday;
		}
		public String getBiswednesday() {
			return biswednesday;
		}
		public void setBiswednesday(String biswednesday) {
			this.biswednesday = biswednesday;
		}
		public String getBisthursday() {
			return bisthursday;
		}
		public void setBisthursday(String bisthursday) {
			this.bisthursday = bisthursday;
		}
		public String getBisfriday() {
			return bisfriday;
		}
		public void setBisfriday(String bisfriday) {
			this.bisfriday = bisfriday;
		}
		public String getBissaturday() {
			return bissaturday;
		}
		public void setBissaturday(String bissaturday) {
			this.bissaturday = bissaturday;
		}
		public String getBissunday() {
			return bissunday;
		}
		public void setBissunday(String bissunday) {
			this.bissunday = bissunday;
		}
		public String getBisclasses() {
			return bisclasses;
		}
		public void setBisclasses(String bisclasses) {
			this.bisclasses = bisclasses;
		}
		public String getBisoneclass() {
			return bisoneclass;
		}
		public void setBisoneclass(String bisoneclass) {
			this.bisoneclass = bisoneclass;
		}
		public String getBistwoclass() {
			return bistwoclass;
		}
		public void setBistwoclass(String bistwoclass) {
			this.bistwoclass = bistwoclass;
		}
		public String getBisthreeclass() {
			return bisthreeclass;
		}
		public void setBisthreeclass(String bisthreeclass) {
			this.bisthreeclass = bisthreeclass;
		}
		public String getBisfourclass() {
			return bisfourclass;
		}
		public void setBisfourclass(String bisfourclass) {
			this.bisfourclass = bisfourclass;
		}
		public String getBiscardtyp() {
			return biscardtyp;
		}
		public void setBiscardtyp(String biscardtyp) {
			this.biscardtyp = biscardtyp;
		}
		public String getBispubitemtyp() {
			return bispubitemtyp;
		}
		public void setBispubitemtyp(String bispubitemtyp) {
			this.bispubitemtyp = bispubitemtyp;
		}
		public String getBisppacktyp() {
			return bisppacktyp;
		}
		public void setBisppacktyp(String bisppacktyp) {
			this.bisppacktyp = bisppacktyp;
		}
		public String getBispubitem() {
			return bispubitem;
		}
		public void setBispubitem(String bispubitem) {
			this.bispubitem = bispubitem;
		}
		public String getBisppack() {
			return bisppack;
		}
		public void setBisppack(String bisppack) {
			this.bisppack = bisppack;
		}
		public String getBiseticket() {
			return biseticket;
		}
		public void setBiseticket(String biseticket) {
			this.biseticket = biseticket;
		}
		public String getVfontcolor() {
			return vfontcolor;
		}
		public void setVfontcolor(String vfontcolor) {
			this.vfontcolor = vfontcolor;
		}
		public String getVbackgroud() {
			return vbackgroud;
		}
		public void setVbackgroud(String vbackgroud) {
			this.vbackgroud = vbackgroud;
		}
		public String getVfont() {
			return vfont;
		}
		public void setVfont(String vfont) {
			this.vfont = vfont;
		}
		public Integer getIfontsize() {
			return ifontsize;
		}
		public void setIfontsize(Integer ifontsize) {
			this.ifontsize = ifontsize;
		}
		public Integer getIbackwidth() {
			return ibackwidth;
		}
		public void setIbackwidth(Integer ibackwidth) {
			this.ibackwidth = ibackwidth;
		}
		public Integer getIbackheight() {
			return ibackheight;
		}
		public void setIbackheight(Integer ibackheight) {
			this.ibackheight = ibackheight;
		}
		public String getVmemo() {
			return vmemo;
		}
		public void setVmemo(String vmemo) {
			this.vmemo = vmemo;
		}
		public String getBdiscount() {
			return bdiscount;
		}
		public void setBdiscount(String bdiscount) {
			this.bdiscount = bdiscount;
		}
		public Double getNdiscountrate() {
			return ndiscountrate;
		}
		public void setNdiscountrate(Double ndiscountrate) {
			this.ndiscountrate = ndiscountrate;
		}
		public String getBadjust() {
			return badjust;
		}
		public void setBadjust(String badjust) {
			this.badjust = badjust;
		}
		public Double getNadjustmy() {
			return nadjustmy;
		}
		public void setNadjustmy(Double nadjustmy) {
			this.nadjustmy = nadjustmy;
		}
		public Double getPoundage() {
			return poundage;
		}
		public void setPoundage(Double poundage) {
			this.poundage = poundage;
		}
		public Double getPaidmoney() {
			return paidmoney;
		}
		public void setPaidmoney(Double paidmoney) {
			this.paidmoney = paidmoney;
		}
		public String getTjyn() {
			return tjyn;
		}
		public void setTjyn(String tjyn) {
			this.tjyn = tjyn;
		}
		public String getOperategroup() {
			return operategroup;
		}
		public void setOperategroup(String operategroup) {
			this.operategroup = operategroup;
		}
		public String getVouchers() {
			return vouchers;
		}
		public void setVouchers(String vouchers) {
			this.vouchers = vouchers;
		}
		public String getBishchd() {
			return bishchd;
		}
		public void setBishchd(String bishchd) {
			this.bishchd = bishchd;
		}
		public String getBismdxz() {
			return bismdxz;
		}
		public void setBismdxz(String bismdxz) {
			this.bismdxz = bismdxz;
		}
		public String getBiscpmx() {
			return biscpmx;
		}
		public void setBiscpmx(String biscpmx) {
			this.biscpmx = biscpmx;
		}
		public String getBistcmx() {
			return bistcmx;
		}
		public void setBistcmx(String bistcmx) {
			this.bistcmx = bistcmx;
		}
		public String getEfzk() {
			return efzk;
		}
		public void setEfzk(String efzk) {
			this.efzk = efzk;
		}
		public String getTjtype() {
			return tjtype;
		}
		public void setTjtype(String tjtype) {
			this.tjtype = tjtype;
		}
		public String getIsauto() {
			return isauto;
		}
		public void setIsauto(String isauto) {
			this.isauto = isauto;
		}
		public String getIsmember() {
			return ismember;
		}
		public void setIsmember(String ismember) {
			this.ismember = ismember;
		}
		public String getIsrealupdate() {
			return isrealupdate;
		}
		public void setIsrealupdate(String isrealupdate) {
			this.isrealupdate = isrealupdate;
		}
		public String getShowinpad() {
			return showinpad;
		}
		public void setShowinpad(String showinpad) {
			this.showinpad = showinpad;
		}
		public String getJmrdo() {
			return jmrdo;
		}
		public void setJmrdo(String jmrdo) {
			this.jmrdo = jmrdo;
		}
		public String getRdo() {
			return rdo;
		}
		public void setRdo(String rdo) {
			this.rdo = rdo;
		}
		public String getXztj() {
			return xztj;
		}
		public void setXztj(String xztj) {
			this.xztj = xztj;
		}
		public String getBltype() {
			return bltype;
		}
		public void setBltype(String bltype) {
			this.bltype = bltype;
		}
		public String getBisljxz() {
			return bisljxz;
		}
		public void setBisljxz(String bisljxz) {
			this.bisljxz = bisljxz;
		}
		public String getPk_acttypmin() {
			return pk_acttypmin;
		}
		public void setPk_acttypmin(String pk_acttypmin) {
			this.pk_acttypmin = pk_acttypmin;
		}
		public String getExcprice() {
			return excprice;
		}
		public void setExcprice(String excprice) {
			this.excprice = excprice;
		}
		public String getIsvalidate() {
			return isvalidate;
		}
		public void setIsvalidate(String isvalidate) {
			this.isvalidate = isvalidate;
		}
		public String getCouponcode() {
			return couponcode;
		}
		public void setCouponcode(String couponcode) {
			this.couponcode = couponcode;
		}
		public String getVoucherback() {
			return voucherback;
		}
		public void setVoucherback(String voucherback) {
			this.voucherback = voucherback;
		}
		public String getVfenback() {
			return vfenback;
		}
		public void setVfenback(String vfenback) {
			this.vfenback = vfenback;
		}
		public String getVfendisc() {
			return vfendisc;
		}
		public void setVfendisc(String vfendisc) {
			this.vfendisc = vfendisc;
		}
		public Integer getIfendiscnum() {
			return ifendiscnum;
		}
		public void setIfendiscnum(Integer ifendiscnum) {
			this.ifendiscnum = ifendiscnum;
		}
		public String getVvoucherdisc() {
			return vvoucherdisc;
		}
		public void setVvoucherdisc(String vvoucherdisc) {
			this.vvoucherdisc = vvoucherdisc;
		}
		public String getVvouchercode() {
			return vvouchercode;
		}
		public void setVvouchercode(String vvouchercode) {
			this.vvouchercode = vvouchercode;
		}
		public String getVvouchername() {
			return vvouchername;
		}
		public void setVvouchername(String vvouchername) {
			this.vvouchername = vvouchername;
		}
		public Integer getIvoucherdiscnum() {
			return ivoucherdiscnum;
		}
		public void setIvoucherdiscnum(Integer ivoucherdiscnum) {
			this.ivoucherdiscnum = ivoucherdiscnum;
		}
		public Integer getItyp() {
			return ityp;
		}
		public void setItyp(Integer ityp) {
			this.ityp = ityp;
		}
		public String getJmtype() {
			return jmtype;
		}
		public void setJmtype(String jmtype) {
			this.jmtype = jmtype;
		}
		public String getVtoprice() {
			return vtoprice;
		}
		public void setVtoprice(String vtoprice) {
			this.vtoprice = vtoprice;
		}
		public String getVatwill() {
			return vatwill;
		}
		public void setVatwill(String vatwill) {
			this.vatwill = vatwill;
		}
		public String getVproducttype() {
			return vproducttype;
		}
		public void setVproducttype(String vproducttype) {
			this.vproducttype = vproducttype;
		}
		public String getDxlsdate() {
			return dxlsdate;
		}
		public void setDxlsdate(String dxlsdate) {
			this.dxlsdate = dxlsdate;
		}
		public String getVisinputmoney() {
			return visinputmoney;
		}
		public void setVisinputmoney(String visinputmoney) {
			this.visinputmoney = visinputmoney;
		}
		public String getVthirdcode() {
			return vthirdcode;
		}
		public void setVthirdcode(String vthirdcode) {
			this.vthirdcode = vthirdcode;
		}
		public String getPk_store() {
			return pk_store;
		}
		public void setPk_store(String pk_store) {
			this.pk_store = pk_store;
		}
		public String getCardtypdes() {
			return cardtypdes;
		}
		public void setCardtypdes(String cardtypdes) {
			this.cardtypdes = cardtypdes;
		}
		public String getCardtyp() {
			return cardtyp;
		}
		public void setCardtyp(String cardtyp) {
			this.cardtyp = cardtyp;
		}
		public Integer getItopamount() {
			return itopamount;
		}
		public void setItopamount(Integer itopamount) {
			this.itopamount = itopamount;
		}
}
