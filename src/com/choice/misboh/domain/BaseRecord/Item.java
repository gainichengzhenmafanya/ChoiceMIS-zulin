package com.choice.misboh.domain.BaseRecord;

/**
 * 描述：活动子表——活动菜品明细----账单减免和启用调价
 * @author 马振
 * 创建时间：2015-4-21 下午3:08:48
 */
public class Item {
	private String pk_Item;
	private String pk_actm;		//活动ID
	private String pk_pubitem;	//菜品ID
	private String vcode;		//菜品编码
	private String vname;		//菜品名称
	private String inum;		//数量
	private String vgivetyp;	//优惠方式
	private String nvalue;		//优惠值
	
	private String unitindex;	//单位序号
	private String unitcode;	//单位编码
	private String unitname;	//单位名称
	private int itypenum; 		//类别数量
	
	private String vitemtype; 	//项目标识，1为菜品，2为套餐，3为菜品活动类别
	private String jmrdo;		//减免金额类型
	private String jmttyp;		//减免类型
	private double nderatenum;  //减免金额
	
	public String getPk_Item() {
		return pk_Item;
	}
	public void setPk_Item(String pk_Item) {
		this.pk_Item = pk_Item;
	}
	public String getPk_actm() {
		return pk_actm;
	}
	public void setPk_actm(String pk_actm) {
		this.pk_actm = pk_actm;
	}
	public String getPk_pubitem() {
		return pk_pubitem;
	}
	public void setPk_pubitem(String pk_pubitem) {
		this.pk_pubitem = pk_pubitem;
	}
	public String getInum() {
		return inum;
	}
	public void setInum(String inum) {
		this.inum = inum;
	}
	public String getVgivetyp() {
		return vgivetyp;
	}
	public void setVgivetyp(String vgivetyp) {
		this.vgivetyp = vgivetyp;
	}
	public String getNvalue() {
		return nvalue;
	}
	public void setNvalue(String nvalue) {
		this.nvalue = nvalue;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getUnitindex() {
		return unitindex;
	}
	public void setUnitindex(String unitindex) {
		this.unitindex = unitindex;
	}
	public String getUnitcode() {
		return unitcode;
	}
	public void setUnitcode(String unitcode) {
		this.unitcode = unitcode;
	}
	public int getItypenum() {
		return itypenum;
	}
	public void setItypenum(int itypenum) {
		this.itypenum = itypenum;
	}
	public String getVitemtype() {
		return vitemtype;
	}
	public void setVitemtype(String vitemtype) {
		this.vitemtype = vitemtype;
	}
	public String getJmrdo() {
		return jmrdo;
	}
	public void setJmrdo(String jmrdo) {
		this.jmrdo = jmrdo;
	}
	public String getJmttyp() {
		return jmttyp;
	}
	public void setJmttyp(String jmttyp) {
		this.jmttyp = jmttyp;
	}
	public double getNderatenum() {
		return nderatenum;
	}
	public void setNderatenum(double nderatenum) {
		this.nderatenum = nderatenum;
	}
	public String getUnitname() {
		return unitname;
	}
	public void setUnitname(String unitname) {
		this.unitname = unitname;
	}
}
