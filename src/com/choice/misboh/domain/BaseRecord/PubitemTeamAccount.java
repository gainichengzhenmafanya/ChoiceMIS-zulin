package com.choice.misboh.domain.BaseRecord;

/**
 * 菜品组适用帐号定义
 * @author ygb
 *
 */
public class PubitemTeamAccount {
	
	private String pk_pubitemTeamAccount;
	private String pk_pubitemTeam;
	private String pk_account;
	private String accountCode;
	private String accountName;
	
	public String getPk_pubitemTeamAccount() {
		return pk_pubitemTeamAccount;
	}
	public void setPk_pubitemTeamAccount(String pk_pubitemTeamAccount) {
		this.pk_pubitemTeamAccount = pk_pubitemTeamAccount;
	}
	public String getPk_pubitemTeam() {
		return pk_pubitemTeam;
	}
	public void setPk_pubitemTeam(String pk_pubitemTeam) {
		this.pk_pubitemTeam = pk_pubitemTeam;
	}
	public String getPk_account() {
		return pk_account;
	}
	public void setPk_account(String pk_account) {
		this.pk_account = pk_account;
	}
	public String getAccountCode() {
		return accountCode;
	}
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
}
