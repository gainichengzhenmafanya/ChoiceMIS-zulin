package com.choice.misboh.domain.BaseRecord;

/**
 * 描述：活动子表：菜品限制明细--限制条件
 * @author 马振
 * 创建时间：2015-4-23 下午6:12:53
 */
public class Itmdtl {
	private String pk_Itmdtl;
	private String pk_actm;		//活动ID
	private String pk_pubitem;	//菜品类别ID--包括套餐
	private String vcode;
	private String vname;
	private String inum;		//数量
	private String igroups;		//分组
	private String unitindex;	//单位序号
	private String unitcode;	//单位编码
	private String unitname;	//单位名称
	private int itypenum; 		//类别数量
	private String rdo;			//金额条件与产品分数条件的关系：y(and),n(or)
	private String vgrp;		//类别
	private String vitemtype; 	//项目标识，1为菜品，2为套餐，3为菜品活动类别,4为菜品类别
	
	public String getPk_Itmdtl() {
		return pk_Itmdtl;
	}
	public void setPk_Itmdtl(String pk_Itmdtl) {
		this.pk_Itmdtl = pk_Itmdtl;
	}
	public String getPk_actm() {
		return pk_actm;
	}
	public void setPk_actm(String pk_actm) {
		this.pk_actm = pk_actm;
	}
	public String getPk_pubitem() {
		return pk_pubitem;
	}
	public void setPk_pubitem(String pk_pubitem) {
		this.pk_pubitem = pk_pubitem;
	}
	public String getInum() {
		return inum;
	}
	public void setInum(String inum) {
		this.inum = inum;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	/**
	 * @return igroups
	 */
	public String getIgroups() {
		return igroups;
	}
	/**
	 * @param igroups the igroups to set
	 */
	public void setIgroups(String igroups) {
		this.igroups = igroups;
	}
	public String getUnitindex() {
		return unitindex;
	}
	public void setUnitindex(String unitindex) {
		this.unitindex = unitindex;
	}
	public String getUnitcode() {
		return unitcode;
	}
	public void setUnitcode(String unitcode) {
		this.unitcode = unitcode;
	}
	public int getItypenum() {
		return itypenum;
	}
	public void setItypenum(int itypenum) {
		this.itypenum = itypenum;
	}
	public String getVitemtype() {
		return vitemtype;
	}
	public void setVitemtype(String vitemtype) {
		this.vitemtype = vitemtype;
	}
	public String getUnitname() {
		return unitname;
	}
	public void setUnitname(String unitname) {
		this.unitname = unitname;
	}
	public String getRdo() {
		return rdo;
	}
	public void setRdo(String rdo) {
		this.rdo = rdo;
	}
	public String getVgrp() {
		return vgrp;
	}
	public void setVgrp(String vgrp) {
		this.vgrp = vgrp;
	}
}
