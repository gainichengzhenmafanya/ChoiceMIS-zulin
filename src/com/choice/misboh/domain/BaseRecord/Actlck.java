package com.choice.misboh.domain.BaseRecord;

/**
 * 描述：活动子表——活动互斥关系--互斥条件
 * @author 马振
 * 创建时间：2015-4-21 下午12:36:19
 */
public class Actlck {
	private String pk_Actlck;
	private String pk_actm;		//活动ID
	private String pk_oldactm;	//互斥活动ID--包括支付方式数据
	private String vcode;		//活动编码
	private String vname;		//活动名称
	public String getPk_Actlck() {
		return pk_Actlck;
	}
	public void setPk_Actlck(String pk_Actlck) {
		this.pk_Actlck = pk_Actlck;
	}
	public String getPk_actm() {
		return pk_actm;
	}
	public void setPk_actm(String pk_actm) {
		this.pk_actm = pk_actm;
	}
	public String getPk_oldactm() {
		return pk_oldactm;
	}
	public void setPk_oldactm(String pk_oldactm) {
		this.pk_oldactm = pk_oldactm;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
}
