package com.choice.misboh.domain.BaseRecord;

/**
 * 描述：活动子表——门店即日期
 * @author 马振
 * 创建时间：2015-4-21 上午10:03:56
 */
public class Actstr {
	private String pk_Actstr;
	private String pk_actm;		//活动ID
	private String pk_store;	//门店ID
	private String vcode;
	private String vname;
	private String xzdate;		//是否限制日期
	private String mdbegintime1;//开始日期限制1
	private String mdendtime1;	//结束日期限制1
	private String mdbegintime2;//开始日期限制2
	private String mdendtime2;	//结束日期限制2
	private String mdbegintime3;//开始日期限制3
	private String mdendtime3;	//结束日期限制3
	private String mdbegintime4;//开始日期限制4
	private String mdendtime4;	//结束日期限制4
	private String mdbegintime5;//开始日期限制5
	private String mdendtime5;	//结束日期限制5
	private String mdbegintime6;//开始日期限制6
	private String mdendtime6;	//结束日期限制6
	private String mdbegintime7;//开始日期限制7
	private String mdendtime7;	//结束日期限制7
	private String igroups;		//分组
	public String getPk_Actstr() {
		return pk_Actstr;
	}
	public void setPk_Actstr(String pk_Actstr) {
		this.pk_Actstr = pk_Actstr;
	}
	public String getPk_actm() {
		return pk_actm;
	}
	public void setPk_actm(String pk_actm) {
		this.pk_actm = pk_actm;
	}
	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	/**
	 * @return xzdate
	 */
	public String getXzdate() {
		return xzdate;
	}
	/**
	 * @param xzdate the xzdate to set
	 */
	public void setXzdate(String xzdate) {
		this.xzdate = xzdate;
	}
	/**
	 * @return mdbegintime1
	 */
	public String getMdbegintime1() {
		return mdbegintime1;
	}
	/**
	 * @param mdbegintime1 the mdbegintime1 to set
	 */
	public void setMdbegintime1(String mdbegintime1) {
		this.mdbegintime1 = mdbegintime1;
	}
	/**
	 * @return mdendtime1
	 */
	public String getMdendtime1() {
		return mdendtime1;
	}
	/**
	 * @param mdendtime1 the mdendtime1 to set
	 */
	public void setMdendtime1(String mdendtime1) {
		this.mdendtime1 = mdendtime1;
	}
	/**
	 * @return mdbegintime2
	 */
	public String getMdbegintime2() {
		return mdbegintime2;
	}
	/**
	 * @param mdbegintime2 the mdbegintime2 to set
	 */
	public void setMdbegintime2(String mdbegintime2) {
		this.mdbegintime2 = mdbegintime2;
	}
	/**
	 * @return mdendtime2
	 */
	public String getMdendtime2() {
		return mdendtime2;
	}
	/**
	 * @param mdendtime2 the mdendtime2 to set
	 */
	public void setMdendtime2(String mdendtime2) {
		this.mdendtime2 = mdendtime2;
	}
	/**
	 * @return mdbegintime3
	 */
	public String getMdbegintime3() {
		return mdbegintime3;
	}
	/**
	 * @param mdbegintime3 the mdbegintime3 to set
	 */
	public void setMdbegintime3(String mdbegintime3) {
		this.mdbegintime3 = mdbegintime3;
	}
	/**
	 * @return mdendtime3
	 */
	public String getMdendtime3() {
		return mdendtime3;
	}
	/**
	 * @param mdendtime3 the mdendtime3 to set
	 */
	public void setMdendtime3(String mdendtime3) {
		this.mdendtime3 = mdendtime3;
	}
	/**
	 * @return mdbegintime4
	 */
	public String getMdbegintime4() {
		return mdbegintime4;
	}
	/**
	 * @param mdbegintime4 the mdbegintime4 to set
	 */
	public void setMdbegintime4(String mdbegintime4) {
		this.mdbegintime4 = mdbegintime4;
	}
	/**
	 * @return mdendtime4
	 */
	public String getMdendtime4() {
		return mdendtime4;
	}
	/**
	 * @param mdendtime4 the mdendtime4 to set
	 */
	public void setMdendtime4(String mdendtime4) {
		this.mdendtime4 = mdendtime4;
	}
	/**
	 * @return mdbegintime5
	 */
	public String getMdbegintime5() {
		return mdbegintime5;
	}
	/**
	 * @param mdbegintime5 the mdbegintime5 to set
	 */
	public void setMdbegintime5(String mdbegintime5) {
		this.mdbegintime5 = mdbegintime5;
	}
	/**
	 * @return mdendtime5
	 */
	public String getMdendtime5() {
		return mdendtime5;
	}
	/**
	 * @param mdendtime5 the mdendtime5 to set
	 */
	public void setMdendtime5(String mdendtime5) {
		this.mdendtime5 = mdendtime5;
	}
	/**
	 * @return mdbegintime6
	 */
	public String getMdbegintime6() {
		return mdbegintime6;
	}
	/**
	 * @param mdbegintime6 the mdbegintime6 to set
	 */
	public void setMdbegintime6(String mdbegintime6) {
		this.mdbegintime6 = mdbegintime6;
	}
	/**
	 * @return mdendtime6
	 */
	public String getMdendtime6() {
		return mdendtime6;
	}
	/**
	 * @param mdendtime6 the mdendtime6 to set
	 */
	public void setMdendtime6(String mdendtime6) {
		this.mdendtime6 = mdendtime6;
	}
	/**
	 * @return mdbegintime7
	 */
	public String getMdbegintime7() {
		return mdbegintime7;
	}
	/**
	 * @param mdbegintime7 the mdbegintime7 to set
	 */
	public void setMdbegintime7(String mdbegintime7) {
		this.mdbegintime7 = mdbegintime7;
	}
	/**
	 * @return mdendtime7
	 */
	public String getMdendtime7() {
		return mdendtime7;
	}
	/**
	 * @param mdendtime7 the mdendtime7 to set
	 */
	public void setMdendtime7(String mdendtime7) {
		this.mdendtime7 = mdendtime7;
	}
	/**
	 * @return igroups
	 */
	public String getIgroups() {
		return igroups;
	}
	/**
	 * @param igroups the igroups to set
	 */
	public void setIgroups(String igroups) {
		this.igroups = igroups;
	}
	
}
