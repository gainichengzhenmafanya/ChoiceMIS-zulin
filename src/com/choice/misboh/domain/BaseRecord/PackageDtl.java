package com.choice.misboh.domain.BaseRecord;

import java.util.List;

/**
 * 描述：套餐明细
 * @author 马振
 * 创建时间：2015-4-20 下午5:16:58
 */
public class PackageDtl {
	private String pk_package;			//主键
	private String pk_pubitem;			//菜品主键
	private Double icnt;				//数量
	private String vunit;				//单位
	private Double nprice;				//单价
	private Double ntotalprice;			//小计
	private Integer isortno;			//排序字段
	private Integer bchange;			//是否可换菜
	
	private Integer imaxgcnt = 1;		//可选菜数量
	
	private String vadditem;			//附加项
	private Double nre0;				//价格1
	private Double nre1;				//价格2
	private Double nre2;				//价格3
	private Double nre3;				//价格4
	private Double nre4;				//价格5
	private Double nre5;				//价格6
	private Double nre6;				//价格7
	private Double nre7;				//价格8
	private Double nre8;				//价格9
	private Double nre9;				//价格10
	private String pk_pubpack;			//套餐主表主键
	private String ts;
	private String producttc_order;		//标识


	private String vcode;           	//菜品编码
	private String vname;				//菜品名称
	private String vinit;           	//缩写

	private Double npackageprice;		//菜品在套餐里的价格
	private Double nproportion;			//折扣比例
	private Double ndiscountprice;		//折扣值
	
	private int imincount;				//最小数量
	private int imaxcount;				//最大数量
	
	private String vshowinfo; 			//显示信息 lwj 2014-04-16
	
	private String unitindex;			//单位序号
	private String unitcode; 			//单位编码

	private List<ItemPkg> itemPkgsAdd;

	private List<ItemPkg> itemPkgsEdit;
	
	public String getPk_package() {
		return pk_package;
	}
	public void setPk_package(String pk_package) {
		this.pk_package = pk_package;
	}
	public String getPk_pubitem() {
		return pk_pubitem;
	}
	public void setPk_pubitem(String pk_pubitem) {
		this.pk_pubitem = pk_pubitem;
	}
	public Double getIcnt() {
		return icnt;
	}
	public void setIcnt(Double icnt) {
		this.icnt = icnt;
	}
	public String getVunit() {
		return vunit;
	}
	public void setVunit(String vunit) {
		this.vunit = vunit;
	}
	public Double getNprice() {
		return nprice;
	}
	public void setNprice(Double nprice) {
		this.nprice = nprice;
	}
	public Double getNtotalprice() {
		return ntotalprice;
	}
	public void setNtotalprice(Double ntotalprice) {
		this.ntotalprice = ntotalprice;
	}
	public Integer getIsortno() {
		return isortno;
	}
	public void setIsortno(Integer isortno) {
		this.isortno = isortno;
	}
	public Integer getBchange() {
		return bchange;
	}
	public void setBchange(Integer bchange) {
		this.bchange = bchange;
	}
	public String getVadditem() {
		return vadditem;
	}
	public void setVadditem(String vadditem) {
		this.vadditem = vadditem;
	}
	public Double getNre0() {
		return nre0;
	}
	public void setNre0(Double nre0) {
		this.nre0 = nre0;
	}
	public Double getNre1() {
		return nre1;
	}
	public void setNre1(Double nre1) {
		this.nre1 = nre1;
	}
	public Double getNre2() {
		return nre2;
	}
	public void setNre2(Double nre2) {
		this.nre2 = nre2;
	}
	public Double getNre3() {
		return nre3;
	}
	public void setNre3(Double nre3) {
		this.nre3 = nre3;
	}
	public Double getNre4() {
		return nre4;
	}
	public void setNre4(Double nre4) {
		this.nre4 = nre4;
	}
	public Double getNre5() {
		return nre5;
	}
	public void setNre5(Double nre5) {
		this.nre5 = nre5;
	}
	public Double getNre6() {
		return nre6;
	}
	public void setNre6(Double nre6) {
		this.nre6 = nre6;
	}
	public Double getNre7() {
		return nre7;
	}
	public void setNre7(Double nre7) {
		this.nre7 = nre7;
	}
	public Double getNre8() {
		return nre8;
	}
	public void setNre8(Double nre8) {
		this.nre8 = nre8;
	}
	public Double getNre9() {
		return nre9;
	}
	public void setNre9(Double nre9) {
		this.nre9 = nre9;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public String getProducttc_order() {
		return producttc_order;
	}
	public void setProducttc_order(String producttc_order) {
		this.producttc_order = producttc_order;
	}
	public String getPk_pubpack() {
		return pk_pubpack;
	}
	public void setPk_pubpack(String pk_pubpack) {
		this.pk_pubpack = pk_pubpack;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getVinit() {
		return vinit;
	}
	public void setVinit(String vinit) {
		this.vinit = vinit;
	}

	public Double getNproportion() {
		return nproportion;
	}
	public void setNproportion(Double nproportion) {
		this.nproportion = nproportion;
	}
	public Double getNdiscountprice() {
		return ndiscountprice;
	}
	public void setNdiscountprice(Double ndiscountprice) {
		this.ndiscountprice = ndiscountprice;
	}
	/**
	 * @return the npackageprice
	 */
	public Double getNpackageprice() {
		return npackageprice;
	}
	/**
	 * @param npackageprice the npackageprice to set
	 */
	public void setNpackageprice(Double npackageprice) {
		this.npackageprice = npackageprice;
	}
	public int getImincount() {
		return imincount;
	}
	public void setImincount(int imincount) {
		this.imincount = imincount;
	}
	public Integer getImaxcount() {
		return imaxcount;
	}
	public void setImaxcount(Integer imaxcount) {
		this.imaxcount = imaxcount;
	}
	/**
	 * @return imaxgcnt
	 */
	public Integer getImaxgcnt() {
		return imaxgcnt;
	}
	/**
	 * @param imaxgcnt the imaxgcnt to set
	 */
	public void setImaxgcnt(Integer imaxgcnt) {
		this.imaxgcnt = imaxgcnt;
	}
	public String getVshowinfo() {
		return vshowinfo;
	}
	public void setVshowinfo(String vshowinfo) {
		this.vshowinfo = vshowinfo;
	}
	public String getUnitindex() {
		return unitindex;
	}
	public void setUnitindex(String unitindex) {
		this.unitindex = unitindex;
	}
	public String getUnitcode() {
		return unitcode;
	}
	public void setUnitcode(String unitcode) {
		this.unitcode = unitcode;
	}
	public void setImaxcount(int imaxcount) {
		this.imaxcount = imaxcount;
	}
	public List<ItemPkg> getItemPkgsAdd() {
		return itemPkgsAdd;
	}
	public void setItemPkgsAdd(List<ItemPkg> itemPkgsAdd) {
		this.itemPkgsAdd = itemPkgsAdd;
	}
	public List<ItemPkg> getItemPkgsEdit() {
		return itemPkgsEdit;
	}
	public void setItemPkgsEdit(List<ItemPkg> itemPkgsEdit) {
		this.itemPkgsEdit = itemPkgsEdit;
	}
}
