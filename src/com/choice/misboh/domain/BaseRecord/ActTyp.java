package com.choice.misboh.domain.BaseRecord;

/**
 * 描述：活动管理分类
 * @author 马振
 * 创建时间：2015-4-22 上午10:45:51
 */
public class ActTyp {

	private String pk_ActTyp;
	private String vcode ;			//编码
	private String vname;			//名称
	private String vinit;			//缩写
	private String vfontcolor;		//文本颜色
	private String vfont;			//字体
	private String isize;			//字号
	private String vbackgcolor;		//按钮背景颜色
	private String iwidth;			//按钮宽
	private String iheight;			//按钮高
	private String vmemo ;			//备注
	private Integer enablestate = 2;//启用状态
	
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getVfontcolor() {
		return vfontcolor;
	}
	public void setVfontcolor(String vfontcolor) {
		this.vfontcolor = vfontcolor;
	}
	public String getVfont() {
		return vfont;
	}
	public void setVfont(String vfont) {
		this.vfont = vfont;
	}
	public String getIsize() {
		return isize;
	}
	public void setIsize(String isize) {
		this.isize = isize;
	}
	public String getVbackgcolor() {
		return vbackgcolor;
	}
	public void setVbackgcolor(String vbackgcolor) {
		this.vbackgcolor = vbackgcolor;
	}
	public String getIwidth() {
		return iwidth;
	}
	public void setIwidth(String iwidth) {
		this.iwidth = iwidth;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public Integer getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(Integer enablestate) {
		this.enablestate = enablestate;
	}
	public String getPk_ActTyp() {
		return pk_ActTyp;
	}
	public void setPk_ActTyp(String pk_ActTyp) {
		this.pk_ActTyp = pk_ActTyp;
	}
	public String getVinit() {
		return vinit;
	}
	public void setVinit(String vinit) {
		this.vinit = vinit;
	}
	public String getIheight() {
		return iheight;
	}
	public void setIheight(String iheight) {
		this.iheight = iheight;
	}
}
