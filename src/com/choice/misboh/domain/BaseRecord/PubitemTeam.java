package com.choice.misboh.domain.BaseRecord;

import java.util.List;

/**
 * 菜品组定义
 * @author ygb
 *
 */
public class PubitemTeam {
	
	private String pk_pubitemTeam;
	private String vcode;
	private String vname;
	private String vinit;
	private Integer enablestate=2;
	private String vmemo;
	private String creator;
	private List<PubitemTeamDetail> pubitemTeamDetail;
	private List<PubitemTeamAccount> pubitemTeamAccount;
	
	public String getPk_pubitemTeam() {
		return pk_pubitemTeam;
	}
	public void setPk_pubitemTeam(String pk_pubitemTeam) {
		this.pk_pubitemTeam = pk_pubitemTeam;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public Integer getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(Integer enablestate) {
		this.enablestate = enablestate;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public List<PubitemTeamDetail> getPubitemTeamDetail() {
		return pubitemTeamDetail;
	}
	public void setPubitemTeamDetail(List<PubitemTeamDetail> pubitemTeamDetail) {
		this.pubitemTeamDetail = pubitemTeamDetail;
	}
	public String getVinit() {
		return vinit;
	}
	public void setVinit(String vinit) {
		this.vinit = vinit;
	}
	public List<PubitemTeamAccount> getPubitemTeamAccount() {
		return pubitemTeamAccount;
	}
	public void setPubitemTeamAccount(List<PubitemTeamAccount> pubitemTeamAccount) {
		this.pubitemTeamAccount = pubitemTeamAccount;
	}
}
