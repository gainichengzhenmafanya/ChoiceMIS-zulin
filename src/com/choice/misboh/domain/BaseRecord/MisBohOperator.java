package com.choice.misboh.domain.BaseRecord;

/**
 * 描述：门店操作员
 * @author 马振
 * 创建时间：2015-8-24 下午1:29:58
 */
public class MisBohOperator {
	
	private  String pk_operator;
	private  String vcode;
	private  String vname;
	private  String vinit;
	private  String vpassword;
	private  String pk_store;
	private  String pk_dept;
	private  String deptname;		//门店部门名称
	private  String pk_role;
	private  String rolename;		//门店角色名称
	private  Integer iretrycount;	//允许错误次数
	private  Integer enablestate = 2;
	private  Integer isortno;
	private  String vmemo;
	private  String creator;
	private  String creationtime;
	private  String modifier;
	private  String modifiedtime;
	private  String ts;
	private  String vcardno;		//卡号
	private  String vusescreen; 	//使用场景
	
	private  String iflag;			//编辑标识
	
	public String getPk_operator() {
		return pk_operator;
	}
	public void setPk_operator(String pk_operator) {
		this.pk_operator = pk_operator;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getVinit() {
		return vinit;
	}
	public void setVinit(String vinit) {
		this.vinit = vinit;
	}
	public String getVpassword() {
		return vpassword;
	}
	public void setVpassword(String vpassword) {
		this.vpassword = vpassword;
	}
	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
	public String getPk_dept() {
		return pk_dept;
	}
	public void setPk_dept(String pk_dept) {
		this.pk_dept = pk_dept;
	}
	public String getPk_role() {
		return pk_role;
	}
	public void setPk_role(String pk_role) {
		this.pk_role = pk_role;
	}
	public Integer getIretrycount() {
		return iretrycount;
	}
	public void setIretrycount(Integer iretrycount) {
		this.iretrycount = iretrycount;
	}
	public Integer getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(Integer enablestate) {
		this.enablestate = enablestate;
	}
	public Integer getIsortno() {
		return isortno;
	}
	public void setIsortno(Integer isortno) {
		this.isortno = isortno;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getCreationtime() {
		return creationtime;
	}
	public void setCreationtime(String creationtime) {
		this.creationtime = creationtime;
	}
	public String getModifier() {
		return modifier;
	}
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	public String getModifiedtime() {
		return modifiedtime;
	}
	public void setModifiedtime(String modifiedtime) {
		this.modifiedtime = modifiedtime;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public String getVcardno() {
		return vcardno;
	}
	public void setVcardno(String vcardno) {
		this.vcardno = vcardno;
	}
	public String getVusescreen() {
		return vusescreen;
	}
	public void setVusescreen(String vusescreen) {
		this.vusescreen = vusescreen;
	}
	public String getIflag() {
		return iflag;
	}
	public void setIflag(String iflag) {
		this.iflag = iflag;
	}
	public String getDeptname() {
		return deptname;
	}
	public void setDeptname(String deptname) {
		this.deptname = deptname;
	}
	public String getRolename() {
		return rolename;
	}
	public void setRolename(String rolename) {
		this.rolename = rolename;
	}
}