package com.choice.misboh.domain.publish;

import java.util.List;

/**
 * 描述：下发xml 实体类
 * @author 马振
 * 创建时间：2015-8-25 上午11:03:45
 */
public class PublishContent {
	
	private String vo;			// 目标VO全路径
	private String des;			// VO描述CheckBox后显示信息
	private String memo;		// VO备注信息
	private String executeName;	//执行方法名
	private String key;			// 生成XML的文件名
	private String fileName;	// 生成XML的文件名
	private String rootName;	// 生成XML的根元素名
	private String elementName;	// 生成XML的元素名
	private boolean isByFirm;	// 是否按分店生成XML文件
	private boolean onlyEnabled;// 是否只查询被启用的数据
	private boolean isShow = true;		// CheckBox 是否显示
	private boolean toService = false;	//需要走service层
	private String className;			//下发内容分类，类别名称
	private List<PublishContent> publishContentList;	//下发数组
	private String mapping;				//生成XML的文件名
	private boolean ismust = false;		//是否必选
	
	public String getVo() {
		return vo;
	}
	public void setVo(String vo) {
		this.vo = vo;
	}
	public String getDes() {
		return des;
	}
	public void setDes(String des) {
		this.des = des;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getRootName() {
		return rootName;
	}
	public void setRootName(String rootName) {
		this.rootName = rootName;
	}
	public String getElementName() {
		return elementName;
	}
	public void setElementName(String elementName) {
		this.elementName = elementName;
	}
	public boolean isByFirm() {
		return isByFirm;
	}
	public void setByFirm(boolean isByFirm) {
		this.isByFirm = isByFirm;
	}
	public boolean isOnlyEnabled() {
		return onlyEnabled;
	}
	public void setOnlyEnabled(boolean onlyEnabled) {
		this.onlyEnabled = onlyEnabled;
	}
	public boolean isShow() {
		return isShow;
	}
	public void setShow(boolean isShow) {
		this.isShow = isShow;
	}
	public String getExecuteName() {
		return executeName;
	}
	public void setExecuteName(String executeName) {
		this.executeName = executeName;
	}
	public String getMapping() {
		return mapping;
	}
	public void setMapping(String mapping) {
		this.mapping = mapping;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public List<PublishContent> getPublishContentList() {
		return publishContentList;
	}
	public void setPublishContentList(List<PublishContent> publishContentList) {
		this.publishContentList = publishContentList;
	}
	public boolean isToService() {
		return toService;
	}
	public void setToService(boolean toService) {
		this.toService = toService;
	}
	public boolean isIsmust() {
		return ismust;
	}
	public void setIsmust(boolean ismust) {
		this.ismust = ismust;
	}
}