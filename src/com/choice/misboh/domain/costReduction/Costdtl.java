package com.choice.misboh.domain.costReduction;


public class Costdtl {
	private String acct;
	private String item; 		//菜品的主键
	private String unit;  		//菜品的销售单位
	private String sp_code_m;	//原材料编码
	private String sp_code; 	//对应的物资编码
	private Double cnt;      	//毛用量
	private Double excnt;   	//净用量
	private String status;  	//标示
	private Double exrate;   	//出成率
	private Double cnt1;     	//参考单位用量
	private Double cnt2;     	//成本单位用量
	private String sp_exdes;	//对应净料的名称（暂不设）
	private String itcode;		//菜品编码
	private String mod;			//成本卡类型
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String spCode) {
		sp_code = spCode;
	}
	public Double getCnt() {
		return cnt;
	}
	public void setCnt(Double cnt) {
		this.cnt = cnt;
	}
	public Double getExcnt() {
		return excnt;
	}
	public void setExcnt(Double excnt) {
		this.excnt = excnt;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Double getExrate() {
		return exrate;
	}
	public void setExrate(Double exrate) {
		this.exrate = exrate;
	}
	public Double getCnt1() {
		return cnt1;
	}
	public void setCnt1(Double cnt1) {
		this.cnt1 = cnt1;
	}
	public Double getCnt2() {
		return cnt2;
	}
	public void setCnt2(Double cnt2) {
		this.cnt2 = cnt2;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getSp_code_m() {
		return sp_code_m;
	}
	public void setSp_code_m(String sp_code_m) {
		this.sp_code_m = sp_code_m;
	}
	public String getSp_exdes() {
		return sp_exdes;
	}
	public void setSp_exdes(String sp_exdes) {
		this.sp_exdes = sp_exdes;
	}
	public String getItcode() {
		return itcode;
	}
	public void setItcode(String itcode) {
		this.itcode = itcode;
	}
	public String getMod() {
		return mod;
	}
	public void setMod(String mod) {
		this.mod = mod;
	}
}
