package com.choice.misboh.domain.costReduction;

import java.util.Date;

/**
 * 描述：核减销售明细表
 * @author 马振
 * 创建时间：2015-4-14 上午10:44:30
 */
public class CostItem {
	
	public String getPr() {
		return pr;
	}
	public void setPr(String pr) {
		this.pr = pr;
	}
	private Integer rec;	//序列号
	private String acct;	//账套号
	private String firm;	//分店号
	private String firmdes;	//分店名称
	private String dat;		//核减开始日期
	private String item; 	//菜品集团编码
	private String itcode;	//菜品编码
	private String des;		//菜品名称
	private String unit;	//菜品单位
	private String typ ;	//菜品类别名称
	private String dept;	//部门----餐饮
	private Double itprice;	//价格
	private Double costmax;	//标准成本值
	private Double cnt;		//数量
	private Double num;		//菜品的辅助单位数量
	private Double amt; 	//总金额
	private Double disc;	//折扣
	private Double svc;		//服务费
	private Double tax;		//附加项金额
	private Double refund;	//免项金额
	private Double pickdisc;//套餐优惠
	private Double total; 	//折后金额
	private Double costtl;	//调料成本
	private Double costmx;	//菜品成本
	private Double costre;	//附加项成本
	private Double cost;	//合计成本
	private String tx;		//退菜标志 'A'表示正常，'B'表示退菜
	private String dept1;	//部门编码---供应链档口
	private String deptdes;	//部门名称----餐饮
	private String chkcost;	//是否核减
	private String ynunit; 	//标准单位和参考单位是否一致
	private String positn;	//分店号
	private String pmonth; // 月份
	private String yyear;  // 年份
	private String pr;     // 接受反馈值
	
	private Date bdat;
	private Date edat;
	private String ynUseDept;//是否启用档口
	
	public String getYnUseDept() {
		return ynUseDept;
	}
	public void setYnUseDept(String ynUseDept) {
		this.ynUseDept = ynUseDept;
	}
	public Date getBdat() {
		return bdat;
	}
	public void setBdat(Date bdat) {
		this.bdat = bdat;
	}
	public Date getEdat() {
		return edat;
	}
	public void setEdat(Date edat) {
		this.edat = edat;
	}
	public String getPmonth() {
		return pmonth;
	}
	public void setPmonth(String pmonth) {
		this.pmonth = pmonth;
	}
	public String getYyear() {
		return yyear;
	}
	public void setYyear(String yyear) {
		this.yyear = yyear;
	}
	public Integer getRec() {
		return rec;
	}
	public void setRec(Integer rec) {
		this.rec = rec;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public String getFirmdes() {
		return firmdes;
	}
	public void setFirmdes(String firmdes) {
		this.firmdes = firmdes;
	}
	public String getDat() {
		return dat;
	}
	public void setDat(String dat) {
		this.dat = dat;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getItcode() {
		return itcode;
	}
	public void setItcode(String itcode) {
		this.itcode = itcode;
	}
	public String getDes() {
		return des;
	}
	public void setDes(String des) {
		this.des = des;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public Double getItprice() {
		return itprice;
	}
	public void setItprice(Double itprice) {
		this.itprice = itprice;
	}
	public Double getCostmax() {
		return costmax;
	}
	public void setCostmax(Double costmax) {
		this.costmax = costmax;
	}
	public Double getCnt() {
		return cnt;
	}
	public void setCnt(Double cnt) {
		this.cnt = cnt;
	}
	public Double getNum() {
		return num;
	}
	public void setNum(Double num) {
		this.num = num;
	}
	public Double getAmt() {
		return amt;
	}
	public void setAmt(Double amt) {
		this.amt = amt;
	}
	public Double getDisc() {
		return disc;
	}
	public void setDisc(Double disc) {
		this.disc = disc;
	}
	public Double getSvc() {
		return svc;
	}
	public void setSvc(Double svc) {
		this.svc = svc;
	}
	public Double getTax() {
		return tax;
	}
	public void setTax(Double tax) {
		this.tax = tax;
	}
	public Double getRefund() {
		return refund;
	}
	public void setRefund(Double refund) {
		this.refund = refund;
	}
	public Double getPickdisc() {
		return pickdisc;
	}
	public void setPickdisc(Double pickdisc) {
		this.pickdisc = pickdisc;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public Double getCosttl() {
		return costtl;
	}
	public void setCosttl(Double costtl) {
		this.costtl = costtl;
	}
	public Double getCostmx() {
		return costmx;
	}
	public void setCostmx(Double costmx) {
		this.costmx = costmx;
	}
	public Double getCostre() {
		return costre;
	}
	public void setCostre(Double costre) {
		this.costre = costre;
	}
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	public String getTx() {
		return tx;
	}
	public void setTx(String tx) {
		this.tx = tx;
	}
	public String getDept1() {
		return dept1;
	}
	public void setDept1(String dept1) {
		this.dept1 = dept1;
	}
	public String getDeptdes() {
		return deptdes;
	}
	public void setDeptdes(String deptdes) {
		this.deptdes = deptdes;
	}
	public String getChkcost() {
		return chkcost;
	}
	public void setChkcost(String chkcost) {
		this.chkcost = chkcost;
	}
	public String getYnunit() {
		return ynunit;
	}
	public void setYnunit(String ynunit) {
		this.ynunit = ynunit;
	}
	public String getPositn() {
		return positn;
	}
	public void setPositn(String positn) {
		this.positn = positn;
	}
}
