package com.choice.misboh.domain.costReduction;

/**
 * 描述：核减物资明细表
 * @author 马振
 * 创建时间：2015-4-14 上午10:44:30
 */
public class CostItemSpcode {
	
	private Integer rec;		//序列号
	private Integer costitemrec;//菜品销售表（costitem）的序列号 - rec
	private String acct;		//账套号
	private String firm;		//分店号
	private String item;		//菜品编号
	private String des;			//菜品名称
	private String unit;		//菜品单位
	private String dept1;		//部门编码---SCM
	private String dept;		//部门编码---餐饮
	private String deptdes;		//部门编码---餐饮
	private String sp_code ;	//物资编码
	private String sp_name;		//物资名称
	private Double cnt;			//标准数量
	private Double cnt1;		//参考数量
	private Double price;	 	//价格
	private String chkcost;		//是否核减
	private String tx; 			//菜品销售表里的一个字段
	private String dat;			//核减日期
	
	private String startDate;	//核减开始日期（用于查询）
	private String endDate;		//核减结束日期（用于查询）
	
	
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getDeptdes() {
		return deptdes;
	}
	public void setDeptdes(String deptdes) {
		this.deptdes = deptdes;
	}
	public Integer getRec() {
		return rec;
	}
	public void setRec(Integer rec) {
		this.rec = rec;
	}
	public Integer getCostitemrec() {
		return costitemrec;
	}
	public void setCostitemrec(Integer costitemrec) {
		this.costitemrec = costitemrec;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getDept1() {
		return dept1;
	}
	public void setDept1(String dept1) {
		this.dept1 = dept1;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public Double getCnt() {
		return cnt;
	}
	public void setCnt(Double cnt) {
		this.cnt = cnt;
	}
	public Double getCnt1() {
		return cnt1;
	}
	public void setCnt1(Double cnt1) {
		this.cnt1 = cnt1;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getChkcost() {
		return chkcost;
	}
	public void setChkcost(String chkcost) {
		this.chkcost = chkcost;
	}
	public String getTx() {
		return tx;
	}
	public void setTx(String tx) {
		this.tx = tx;
	}
	public String getDat() {
		return dat;
	}
	public void setDat(String dat) {
		this.dat = dat;
	}
	public String getDes() {
		return des;
	}
	public void setDes(String des) {
		this.des = des;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
}
