package com.choice.misboh.domain.reportMis;

import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * 描述：报表bean
 * @author 马振
 * 创建时间：2015-4-16 下午6:18:57
 * @param <T>
 */
@Component("bohReportObject")
@Scope(value="prototype")
public class ReportObject<T> {

	private int total;
	private List<T> rows;
	private List<Map<String,Object>> footer;
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public List<T> getRows() {
		return rows;
	}
	public void setRows(List<T> rows) {
		this.rows = rows;
	}
	public List<Map<String, Object>> getFooter() {
		return footer;
	}
	public void setFooter(List<Map<String,Object>> footer) {
		this.footer = footer;
	}
}
