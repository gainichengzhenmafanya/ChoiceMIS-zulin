package com.choice.misboh.domain.reportMis;

import java.util.Date;

import com.choice.misboh.commonutil.NoUse;
import com.choice.orientationSys.util.Page;

/**
 * 描述：报表的查询条件
 * @author 马振
 * 创建时间：2015-4-16 上午10:30:54
 */
public class PublicEntity {

	@NoUse
	private Page pager; 			//分页对象
	@NoUse
	private int page; 				//请求页
	@NoUse
	private int rows; 				//每页行数
	private String sort; 			//排序字段
	private String order; 			//排序方式
	private String pk_store;		//门店主键
	private String pk_storearear;	//门店区域主键
	private String vcode;			//编码
	private String vname;			//名称
	private String vinit;			//缩写
	private String bdat;			//开始日期
	private String edat;			//结束日期
	private String startdate;
	private String enddate;
	private String sql;
	private String sqlSum;
	private String sqlMinus;
	private String sqlstr;
	private String sqlData;		//查询语句
	private String sqlStr1;		//查询语句
	private String sqlStr2;		//查询语句
	private String sqlStr3;		//查询语句
	private String sqlStr4;		//查询语句
	private String sqlStr5;		//查询语句
	private String sqlStr6;		//查询语句
	private String sqlStr7;		//查询语句
	private String sqlStr8;		//查询语句
	private String sqlAll;			//查询语句
    private String enablestate;	//启用状态
    private String vistest;		//是否测试门店
    private String firmid;			//门店id
    private String firmdes;		//门店名称
	private String pubitemdes;		//菜品名称
	private String sft;			//餐次
	private String dworkdate;		//营业日期
	private String vsavingno;		//存款单号
	private Double namount;		//金额
    private String paymentname;	//支付方式名称
    private String vsavingdate;	//存款日期
    private String typename;		//存款分类
    private String vmemo;			//备注
    private String receiptdate;	//回单日期
    private String pk_pubitem;		//菜品主键
    private String vpcode;			//菜品編碼
	private String vpname;			//菜品名称
    private String pubgrp;			//类别
    private String binterval; 		//时段开始时间
	private String einterval; 		//时段结束时间
	private String accountId;		//登陆用户主键
	private String nmoneySum;		//金额合计
    private String tcSum;			//单数合计
    private String ipeopleSum;		//人数合计
    private String fwsjSum;		//服务时间合计
    private String vecode;			//操作人编码
    private String vename;			//操作人
	private String vjename;		//授权人
	private String vbcode;			//账单号
	private String iflag;			//类型
	private int years;				//年数
	private int days;				//天数
	private int sumMod;			//汇总方式
	private int queryMod;			//查询方式
	private int detailMod;			//明细方式
	private String pubgrptyp;		//类别2
	private String vorclass;		//状态
	private String ncomp;			//招待
	private String ntmptsfr;		//挂账
	private String repayment;		//反结算
	private String tbldes;			//桌台
	private String vlastemp;		//结账员
	private String iclasses;		//流水号
	private String serial;			//流水号
	private String newyear;        //年份
	private String interval;		//隔的时间
	private String vpacktype;
	private String vpackcode;
	private String ivalue;
	
	public final static Integer normal = 1; 	//整单ok
	public final static Integer totalback = 2; 	//整单退
	public final static Integer sigleback = 3; 	//单退
	public final static Integer Discard = 4; 	//废弃
	public final static Integer Meal = 5; 		//膳食
	public final static Integer totaldisc = 6; 	//整单折扣
	public final static Integer backsite = 7; 	//返位结算
	
	public static final int BYFIRM = 0;			//按分店
	public static final int BYFOOD = 1;			//按菜品
	public static final int BYCATEGORY = 2;		//按类别
	public static final int BYDAY = 3;			//按天
	public static final int BYMONTH = 4;		//按月
	public static final int BYSEASON = 5;		//按季度
	public static final int BYYEAR = 6;			//按年
	public static final int BYWEEK = 7;			//按周
	public static final int BYWEEKDAY = 8;		//按周几
	public static final int BYHALFYEAR = 9;		//按半年
	public static final int BYAREA = 10;		//按地区
	public static final int BYMOD = 11;			//按经营模式
	public static final int DETAILBYDAY = 12;	//按天明细
	public static final int BYDIFF = 13;		//同期对比
	
	public static final int ALL = 0;			//全部
	public static final int PACKAGE = 1;		//套餐菜
	public static final int NORMAL = 2;			//零点菜
	public static final int PAXAVG = 3;			//人均消费
	public static final int INCOMESUM = 4;		//营收统计
	//消退菜类型
	public static final int RETURN = 0;			//退菜
	public static final int CANCEL = 1;			//取消
	public static final int PRESENT = 2;		//送菜
	public static final int REFUND = 3;			//免项
	
	/**
	 * 成本分析报表 使用查询条件对象
	 * @author wqq
	 */
	private String sp_code;	//物资编码
	private String firm;	//门店编码
	private String grptyp;	//物资大类
	private String grp;		//物资中类
	private String typ;		//物资小类
	private String vscode;	//门店编码
	private String code;
	private String val;
	private String idept;
	private String itemname;
	
	private String pk_acttypmin;
	private String pk_acttyp;
	private String visinputmoney;
	private String vcodemin;
	private String vnamemin;
	private String sbdat;
	private String sedat;
	private String refundTyp;
	private String status;
	
	private String stf;
	private String tag;
	private String amt;
	private String des;
	private String pk_paymode;	//支付方式
    private String vactCode;	//活动编码
	private String HolidayType;	//假日类型
	private int costCategory;	//菜品种类（全部菜品，核心菜品，明星菜品，自选菜品）
	
	//自定义报表用 wjf
	private String reportPath;
	private String positn;
	private Date week;
	
	private Date sdate;
	
	public Date getSdate() {
		return sdate;
	}
	public void setSdate(Date sdate) {
		this.sdate = sdate;
	}
	public PublicEntity(){
		pager = new Page();
	}
	public Page getPager() {
		return pager;
	}
	public void setPager(Page pager) {
		this.pager = pager;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
		pager.setNowPage(page);
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
		pager.setPageSize(rows);
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getGrptyp() {
		return grptyp;
	}
	public void setGrptyp(String grptyp) {
		this.grptyp = grptyp;
	}
	public String getGrp() {
		return grp;
	}
	public void setGrp(String grp) {
		this.grp = grp;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
	public String getPk_storearear() {
		return pk_storearear;
	}
	public void setPk_storearear(String pk_storearear) {
		this.pk_storearear = pk_storearear;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getBdat() {
		return bdat;
	}
	public void setBdat(String bdat) {
		this.bdat = bdat;
	}
	public String getEdat() {
		return edat;
	}
	public void setEdat(String edat) {
		this.edat = edat;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public String getSql() {
		return sql;
	}
	public void setSql(String sql) {
		this.sql = sql;
	}
	public String getSqlSum() {
		return sqlSum;
	}
	public void setSqlSum(String sqlSum) {
		this.sqlSum = sqlSum;
	}
	public String getSqlMinus() {
		return sqlMinus;
	}
	public void setSqlMinus(String sqlMinus) {
		this.sqlMinus = sqlMinus;
	}
	public String getSqlstr() {
		return sqlstr;
	}
	public void setSqlstr(String sqlstr) {
		this.sqlstr = sqlstr;
	}
	public String getSqlData() {
		return sqlData;
	}
	public void setSqlData(String sqlData) {
		this.sqlData = sqlData;
	}
	public String getSqlStr1() {
		return sqlStr1;
	}
	public void setSqlStr1(String sqlStr1) {
		this.sqlStr1 = sqlStr1;
	}
	public String getSqlStr2() {
		return sqlStr2;
	}
	public void setSqlStr2(String sqlStr2) {
		this.sqlStr2 = sqlStr2;
	}
	public String getSqlStr3() {
		return sqlStr3;
	}
	public void setSqlStr3(String sqlStr3) {
		this.sqlStr3 = sqlStr3;
	}
	public String getSqlStr4() {
		return sqlStr4;
	}
	public void setSqlStr4(String sqlStr4) {
		this.sqlStr4 = sqlStr4;
	}
	public String getSqlStr5() {
		return sqlStr5;
	}
	public void setSqlStr5(String sqlStr5) {
		this.sqlStr5 = sqlStr5;
	}
	public String getSqlStr6() {
		return sqlStr6;
	}
	public void setSqlStr6(String sqlStr6) {
		this.sqlStr6 = sqlStr6;
	}
	public String getSqlStr7() {
		return sqlStr7;
	}
	public void setSqlStr7(String sqlStr7) {
		this.sqlStr7 = sqlStr7;
	}
	public String getSqlStr8() {
		return sqlStr8;
	}
	public void setSqlStr8(String sqlStr8) {
		this.sqlStr8 = sqlStr8;
	}
	public String getSqlAll() {
		return sqlAll;
	}
	public void setSqlAll(String sqlAll) {
		this.sqlAll = sqlAll;
	}
	public String getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(String enablestate) {
		this.enablestate = enablestate;
	}
	public String getVistest() {
		return vistest;
	}
	public void setVistest(String vistest) {
		this.vistest = vistest;
	}
	public String getFirmdes() {
		return firmdes;
	}
	public void setFirmdes(String firmdes) {
		this.firmdes = firmdes;
	}
	public String getPubitemdes() {
		return pubitemdes;
	}
	public void setPubitemdes(String pubitemdes) {
		this.pubitemdes = pubitemdes;
	}
	public static Integer getNormal() {
		return normal;
	}
	public static Integer getTotalback() {
		return totalback;
	}
	public static Integer getSigleback() {
		return sigleback;
	}
	public static Integer getDiscard() {
		return Discard;
	}
	public static Integer getMeal() {
		return Meal;
	}
	public static Integer getTotaldisc() {
		return totaldisc;
	}
	public static Integer getBacksite() {
		return backsite;
	}
	public String getSft() {
		return sft;
	}
	public void setSft(String sft) {
		this.sft = sft;
	}
	public String getDworkdate() {
		return dworkdate;
	}
	public void setDworkdate(String dworkdate) {
		this.dworkdate = dworkdate;
	}
	public String getVsavingno() {
		return vsavingno;
	}
	public void setVsavingno(String vsavingno) {
		this.vsavingno = vsavingno;
	}
	public Double getNamount() {
		return namount;
	}
	public void setNamount(Double namount) {
		this.namount = namount;
	}
	public String getPaymentname() {
		return paymentname;
	}
	public void setPaymentname(String paymentname) {
		this.paymentname = paymentname;
	}
	public String getVsavingdate() {
		return vsavingdate;
	}
	public void setVsavingdate(String vsavingdate) {
		this.vsavingdate = vsavingdate;
	}
	public String getTypename() {
		return typename;
	}
	public void setTypename(String typename) {
		this.typename = typename;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public String getReceiptdate() {
		return receiptdate;
	}
	public void setReceiptdate(String receiptdate) {
		this.receiptdate = receiptdate;
	}
	public String getVpcode() {
		return vpcode;
	}
	public void setVpcode(String vpcode) {
		this.vpcode = vpcode;
	}
	public String getPubgrp() {
		return pubgrp;
	}
	public void setPubgrp(String pubgrp) {
		this.pubgrp = pubgrp;
	}
	public String getBinterval() {
		return binterval;
	}
	public void setBinterval(String binterval) {
		this.binterval = binterval;
	}
	public String getEinterval() {
		return einterval;
	}
	public void setEinterval(String einterval) {
		this.einterval = einterval;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getNmoneySum() {
		return nmoneySum;
	}
	public void setNmoneySum(String nmoneySum) {
		this.nmoneySum = nmoneySum;
	}
	public String getTcSum() {
		return tcSum;
	}
	public void setTcSum(String tcSum) {
		this.tcSum = tcSum;
	}
	public String getIpeopleSum() {
		return ipeopleSum;
	}
	public void setIpeopleSum(String ipeopleSum) {
		this.ipeopleSum = ipeopleSum;
	}
	public String getFwsjSum() {
		return fwsjSum;
	}
	public void setFwsjSum(String fwsjSum) {
		this.fwsjSum = fwsjSum;
	}
	public String getVename() {
		return vename;
	}
	public void setVename(String vename) {
		this.vename = vename;
	}
	public String getVjename() {
		return vjename;
	}
	public void setVjename(String vjename) {
		this.vjename = vjename;
	}
	public String getVpname() {
		return vpname;
	}
	public void setVpname(String vpname) {
		this.vpname = vpname;
	}
	public String getVbcode() {
		return vbcode;
	}
	public void setVbcode(String vbcode) {
		this.vbcode = vbcode;
	}
	public String getIflag() {
		return iflag;
	}
	public void setIflag(String iflag) {
		this.iflag = iflag;
	}
	public String getFirmid() {
		return firmid;
	}
	public void setFirmid(String firmid) {
		this.firmid = firmid;
	}
	public static int getByfirm() {
		return BYFIRM;
	}
	public static int getByfood() {
		return BYFOOD;
	}
	public static int getBycategory() {
		return BYCATEGORY;
	}
	public static int getByday() {
		return BYDAY;
	}
	public static int getBymonth() {
		return BYMONTH;
	}
	public static int getByseason() {
		return BYSEASON;
	}
	public static int getByyear() {
		return BYYEAR;
	}
	public static int getByweek() {
		return BYWEEK;
	}
	public static int getByweekday() {
		return BYWEEKDAY;
	}
	public static int getByhalfyear() {
		return BYHALFYEAR;
	}
	public static int getByarea() {
		return BYAREA;
	}
	public static int getBymod() {
		return BYMOD;
	}
	public static int getDetailbyday() {
		return DETAILBYDAY;
	}
	public static int getBydiff() {
		return BYDIFF;
	}
	public static int getAll() {
		return ALL;
	}
	public static int getPackage() {
		return PACKAGE;
	}
	public static int getPaxavg() {
		return PAXAVG;
	}
	public static int getIncomesum() {
		return INCOMESUM;
	}
	public static int getReturn() {
		return RETURN;
	}
	public static int getCancel() {
		return CANCEL;
	}
	public static int getPresent() {
		return PRESENT;
	}
	public static int getRefund() {
		return REFUND;
	}
	public int getYears() {
		return years;
	}
	public void setYears(int years) {
		this.years = years;
	}
	public int getDays() {
		return days;
	}
	public void setDays(int days) {
		this.days = days;
	}
	public int getSumMod() {
		return sumMod;
	}
	public void setSumMod(int sumMod) {
		this.sumMod = sumMod;
	}
	public int getQueryMod() {
		return queryMod;
	}
	public void setQueryMod(int queryMod) {
		this.queryMod = queryMod;
	}
	public int getDetailMod() {
		return detailMod;
	}
	public void setDetailMod(int detailMod) {
		this.detailMod = detailMod;
	}
	public String getPubgrptyp() {
		return pubgrptyp;
	}
	public void setPubgrptyp(String pubgrptyp) {
		this.pubgrptyp = pubgrptyp;
	}
	public String getVinit() {
		return vinit;
	}
	public void setVinit(String vinit) {
		this.vinit = vinit;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getVal() {
		return val;
	}
	public void setVal(String val) {
		this.val = val;
	}
	public String getVorclass() {
		return vorclass;
	}
	public void setVorclass(String vorclass) {
		this.vorclass = vorclass;
	}
	public String getRepayment() {
		return repayment;
	}
	public void setRepayment(String repayment) {
		this.repayment = repayment;
	}
	public String getNcomp() {
		return ncomp;
	}
	public void setNcomp(String ncomp) {
		this.ncomp = ncomp;
	}
	public String getNtmptsfr() {
		return ntmptsfr;
	}
	public void setNtmptsfr(String ntmptsfr) {
		this.ntmptsfr = ntmptsfr;
	}
	public String getTbldes() {
		return tbldes;
	}
	public void setTbldes(String tbldes) {
		this.tbldes = tbldes;
	}
	public String getVlastemp() {
		return vlastemp;
	}
	public void setVlastemp(String vlastemp) {
		this.vlastemp = vlastemp;
	}
	public String getIclasses() {
		return iclasses;
	}
	public void setIclasses(String iclasses) {
		this.iclasses = iclasses;
	}
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public String getNewyear() {
		return newyear;
	}
	public void setNewyear(String newyear) {
		this.newyear = newyear;
	}
	public String getInterval() {
		return interval;
	}
	public void setInterval(String interval) {
		this.interval = interval;
	}
	public String getVpackcode() {
		return vpackcode;
	}
	public void setVpackcode(String vpackcode) {
		this.vpackcode = vpackcode;
	}
	public String getVpacktype() {
		return vpacktype;
	}
	public void setVpacktype(String vpacktype) {
		this.vpacktype = vpacktype;
	}
	public String getVscode() {
		return vscode;
	}
	public void setVscode(String vscode) {
		this.vscode = vscode;
	}
	public String getIdept() {
		return idept;
	}
	public void setIdept(String idept) {
		this.idept = idept;
	}
	public String getItemname() {
		return itemname;
	}
	public void setItemname(String itemname) {
		this.itemname = itemname;
	}
	public String getPk_acttypmin() {
		return pk_acttypmin;
	}
	public void setPk_acttypmin(String pk_acttypmin) {
		this.pk_acttypmin = pk_acttypmin;
	}
	public String getPk_acttyp() {
		return pk_acttyp;
	}
	public void setPk_acttyp(String pk_acttyp) {
		this.pk_acttyp = pk_acttyp;
	}
	public String getVisinputmoney() {
		return visinputmoney;
	}
	public void setVisinputmoney(String visinputmoney) {
		this.visinputmoney = visinputmoney;
	}
	public String getVcodemin() {
		return vcodemin;
	}
	public void setVcodemin(String vcodemin) {
		this.vcodemin = vcodemin;
	}
	public String getVnamemin() {
		return vnamemin;
	}
	public void setVnamemin(String vnamemin) {
		this.vnamemin = vnamemin;
	}
	public String getSbdat() {
		return sbdat;
	}
	public void setSbdat(String sbdat) {
		this.sbdat = sbdat;
	}
	public String getSedat() {
		return sedat;
	}
	public void setSedat(String sedat) {
		this.sedat = sedat;
	}
	public String getRefundTyp() {
		return refundTyp;
	}
	public void setRefundTyp(String refundTyp) {
		this.refundTyp = refundTyp;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStf() {
		return stf;
	}
	public void setStf(String stf) {
		this.stf = stf;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getAmt() {
		return amt;
	}
	public void setAmt(String amt) {
		this.amt = amt;
	}
	public String getDes() {
		return des;
	}
	public void setDes(String des) {
		this.des = des;
	}
	public String getVecode() {
		return vecode;
	}
	public void setVecode(String vecode) {
		this.vecode = vecode;
	}
	public String getPk_pubitem() {
		return pk_pubitem;
	}
	public void setPk_pubitem(String pk_pubitem) {
		this.pk_pubitem = pk_pubitem;
	}
	public String getReportPath() {
		return reportPath;
	}
	public void setReportPath(String reportPath) {
		this.reportPath = reportPath;
	}
	public String getPositn() {
		return positn;
	}
	public void setPositn(String positn) {
		this.positn = positn;
	}
	public Date getWeek() {
		return week;
	}
	public void setWeek(Date week) {
		this.week = week;
	}
	public String getPk_paymode() {
		return pk_paymode;
	}
	public void setPk_paymode(String pk_paymode) {
		this.pk_paymode = pk_paymode;
	}
	public String getVactCode() {
		return vactCode;
	}
	public void setVactCode(String vactCode) {
		this.vactCode = vactCode;
	}
	public String getHolidayType() {
		return HolidayType;
	}
	public void setHolidayType(String holidayType) {
		HolidayType = holidayType;
	}
	public int getCostCategory() {
		return costCategory;
	}
	public void setCostCategory(int costCategory) {
		this.costCategory = costCategory;
	}
	public String getIvalue() {
		return ivalue;
	}
	public void setIvalue(String ivalue) {
		this.ivalue = ivalue;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
}