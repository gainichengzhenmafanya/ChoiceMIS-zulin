package com.choice.misboh.domain.reportMis;

import java.util.Date;

/**
 * 描述：营业报表
 * @author 马振
 * 创建时间：2015-6-26 下午5:42:46
 */
public class Business {

	private String code;		//科目代码
	private String des;			//科目名称
	private Integer firmid; 	//分店id
	private Date bdat;			//开始日期
	private double dm0;			//全天合计   一天的 
	private String db0;			//全天合计
	private double dm1;			//班次1金额
	private String db1;			//班次1比例
	private double dm2;			//班次2金额
	private String db2;			//班次2比例
	private double dm3;			//班次3金额
	private String db3;			//班次3比例
	private double dm4;			//班次4金额
	private String db4;			//班次4比例
	private double dm5;			//班次5金额
	private String db5;			//班次5比例
	private double dm6;			//班次6金额
	private String db6;			//班次6比例
	private double mm0;			//全天合计   月初到今天的
	private String mb0;			//全天合计
	private double mm1;			//班次1金额
	private String mb1;			//班次1比例
	private double mm2;			//
	private String mb2;			//
	private double mm3;			//
	private String mb3;			//
	private double mm4;			//
	private String mb4;			//
	private double mm5;			//
	private String mb5;			//
	private double mm6;			//
	private String mb6;			//
	
	private String sft;	
	private String tag;	
	private double amt;	
	
	private String sql;	
	
	public Business(){}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public Integer getFirmid() {
		return firmid;
	}

	public void setFirmid(Integer firmid) {
		this.firmid = firmid;
	}

	public Date getBdat() {
		return bdat;
	}

	public void setBdat(Date bdat) {
		this.bdat = bdat;
	}

	public double getDm0() {
		return dm0;
	}

	public void setDm0(double dm0) {
		this.dm0 = dm0;
	}

	public String getDb0() {
		return db0;
	}

	public void setDb0(String db0) {
		this.db0 = db0;
	}

	public double getDm1() {
		return dm1;
	}

	public void setDm1(double dm1) {
		this.dm1 = dm1;
	}

	public String getDb1() {
		return db1;
	}

	public void setDb1(String db1) {
		this.db1 = db1;
	}

	public double getDm2() {
		return dm2;
	}

	public void setDm2(double dm2) {
		this.dm2 = dm2;
	}

	public String getDb2() {
		return db2;
	}

	public void setDb2(String db2) {
		this.db2 = db2;
	}

	public double getDm3() {
		return dm3;
	}

	public void setDm3(double dm3) {
		this.dm3 = dm3;
	}

	public String getDb3() {
		return db3;
	}

	public void setDb3(String db3) {
		this.db3 = db3;
	}

	public double getMm0() {
		return mm0;
	}

	public void setMm0(double mm0) {
		this.mm0 = mm0;
	}

	public String getMb0() {
		return mb0;
	}

	public void setMb0(String mb0) {
		this.mb0 = mb0;
	}

	public double getMm1() {
		return mm1;
	}

	public void setMm1(double mm1) {
		this.mm1 = mm1;
	}

	public String getMb1() {
		return mb1;
	}

	public void setMb1(String mb1) {
		this.mb1 = mb1;
	}

	public double getMm2() {
		return mm2;
	}

	public void setMm2(double mm2) {
		this.mm2 = mm2;
	}

	public String getMb2() {
		return mb2;
	}

	public void setMb2(String mb2) {
		this.mb2 = mb2;
	}

	public double getMm3() {
		return mm3;
	}

	public void setMm3(double mm3) {
		this.mm3 = mm3;
	}

	public String getMb3() {
		return mb3;
	}

	public void setMb3(String mb3) {
		this.mb3 = mb3;
	}

	public String getSft() {
		return sft;
	}

	public void setSft(String sft) {
		this.sft = sft;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public double getAmt() {
		return amt;
	}

	public void setAmt(double amt) {
		this.amt = amt;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public double getDm4() {
		return dm4;
	}

	public void setDm4(double dm4) {
		this.dm4 = dm4;
	}

	public String getDb4() {
		return db4;
	}

	public void setDb4(String db4) {
		this.db4 = db4;
	}

	public double getDm5() {
		return dm5;
	}

	public void setDm5(double dm5) {
		this.dm5 = dm5;
	}

	public String getDb5() {
		return db5;
	}

	public void setDb5(String db5) {
		this.db5 = db5;
	}

	public double getDm6() {
		return dm6;
	}

	public void setDm6(double dm6) {
		this.dm6 = dm6;
	}

	public double getMm4() {
		return mm4;
	}

	public void setMm4(double mm4) {
		this.mm4 = mm4;
	}

	public String getMb4() {
		return mb4;
	}

	public void setMb4(String mb4) {
		this.mb4 = mb4;
	}

	public double getMm5() {
		return mm5;
	}

	public void setMm5(double mm5) {
		this.mm5 = mm5;
	}

	public String getMb5() {
		return mb5;
	}

	public void setMb5(String mb5) {
		this.mb5 = mb5;
	}

	public double getMm6() {
		return mm6;
	}

	public void setMm6(double mm6) {
		this.mm6 = mm6;
	}

	public String getMb6() {
		return mb6;
	}

	public void setMb6(String mb6) {
		this.mb6 = mb6;
	}

	public String getDb6() {
		return db6;
	}

	public void setDb6(String db6) {
		this.db6 = db6;
	}

}
