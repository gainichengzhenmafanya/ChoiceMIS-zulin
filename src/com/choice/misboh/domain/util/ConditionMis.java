package com.choice.misboh.domain.util;

/**
 * 查询传入参数
 * @author 王超
 *
 */
public class ConditionMis {
	
	private String supplycode;//物资编码
	private String supplydes;//物资名称
	private String supplyinit;//物资缩写
	private String supplytyp;//物资类别
	private String positncode;//仓位编码
	private String positndes;//仓位名称
	private String positninit;//仓位缩写
	private String delivercode;//供应商编码
	private String deliverdes;//供应商名称
	private String bdate;//开始日期
	private String edate;//结束日期
	private String workdate;//营业日
	private String dept;//部门
	private String storecode;//门店编码==对应BOH
	private String storename;//门店名称==对应BOH
	private String pubitecode;//菜品编码==对应BOH
	private String pubitename;//菜品名称==对应BOH
	private String pubiteinit;//菜品缩写==对应BOH
	private String acct;
	private String isOrNotBCP;//是否半成品
	public String getIsOrNotBCP() {
		return isOrNotBCP;
	}
	public void setIsOrNotBCP(String isOrNotBCP) {
		this.isOrNotBCP = isOrNotBCP;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getSupplycode() {
		return supplycode;
	}
	public void setSupplycode(String supplycode) {
		this.supplycode = supplycode;
	}
	public String getSupplydes() {
		return supplydes;
	}
	public void setSupplydes(String supplydes) {
		this.supplydes = supplydes;
	}
	public String getSupplyinit() {
		return supplyinit;
	}
	public void setSupplyinit(String supplyinit) {
		this.supplyinit = supplyinit;
	}
	public String getSupplytyp() {
		return supplytyp;
	}
	public void setSupplytyp(String supplytyp) {
		this.supplytyp = supplytyp;
	}
	public String getPositncode() {
		return positncode;
	}
	public void setPositncode(String positncode) {
		this.positncode = positncode;
	}
	public String getPositndes() {
		return positndes;
	}
	public void setPositndes(String positndes) {
		this.positndes = positndes;
	}
	public String getPositninit() {
		return positninit;
	}
	public void setPositninit(String positninit) {
		this.positninit = positninit;
	}
	public String getDelivercode() {
		return delivercode;
	}
	public void setDelivercode(String delivercode) {
		this.delivercode = delivercode;
	}
	public String getDeliverdes() {
		return deliverdes;
	}
	public void setDeliverdes(String deliverdes) {
		this.deliverdes = deliverdes;
	}
	public String getBdate() {
		return bdate;
	}
	public void setBdate(String bdate) {
		this.bdate = bdate;
	}
	public String getEdate() {
		return edate;
	}
	public void setEdate(String edate) {
		this.edate = edate;
	}
	public String getWorkdate() {
		return workdate;
	}
	public void setWorkdate(String workdate) {
		this.workdate = workdate;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getStorecode() {
		return storecode;
	}
	public void setStorecode(String storecode) {
		this.storecode = storecode;
	}
	public String getStorename() {
		return storename;
	}
	public void setStorename(String storename) {
		this.storename = storename;
	}
	public String getPubitecode() {
		return pubitecode;
	}
	public void setPubitecode(String pubitecode) {
		this.pubitecode = pubitecode;
	}
	public String getPubitename() {
		return pubitename;
	}
	public void setPubitename(String pubitename) {
		this.pubitename = pubitename;
	}
	public String getPubiteinit() {
		return pubiteinit;
	}
	public void setPubiteinit(String pubiteinit) {
		this.pubiteinit = pubiteinit;
	}
	
	
}
