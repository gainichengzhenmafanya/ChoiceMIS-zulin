package com.choice.misboh.domain.inventory;

/***
 * 盘点主表
 * @author wjf
 *
 */
public class Chkstoref {
	
	private String chkstorefid;//主键
	private String firm;//门店编码
	private String dept;//要盘点的仓位
	private String ecode;//盘点人
	private String inputDate;//盘点时间
	private String checkCode;//审核人
	private String checkDate;//审核时间
	private String workDate;//营业日
	private String state;//盘点状态  0未审核 1已审核
	private String pantyp;//盘点类型  daypan日盘 weekpan周盘 monthpan月盘
	private String beginworkdate;//盘点开始时间  没用
	private String memo;//备注   没用
	private String checkMemo;//审核备注  没用
	private Integer chkoutno;//出库单号
	private Integer chkinno;//入库单号
	
	private String ecodeName;
	private String checkCodeName;
	
	private String fromtyp;//来源模板/导入/选择
	
	public String getFromtyp() {
		return fromtyp;
	}
	public void setFromtyp(String fromtyp) {
		this.fromtyp = fromtyp;
	}
	public String getChkstorefid() {
		return chkstorefid;
	}
	public void setChkstorefid(String chkstorefid) {
		this.chkstorefid = chkstorefid;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getEcode() {
		return ecode;
	}
	public void setEcode(String ecode) {
		this.ecode = ecode;
	}
	public String getInputDate() {
		return inputDate;
	}
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	public String getCheckCode() {
		return checkCode;
	}
	public void setCheckCode(String checkCode) {
		this.checkCode = checkCode;
	}
	public String getCheckDate() {
		return checkDate;
	}
	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}
	public String getWorkDate() {
		return workDate;
	}
	public void setWorkDate(String workDate) {
		this.workDate = workDate;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPantyp() {
		return pantyp;
	}
	public void setPantyp(String pantyp) {
		this.pantyp = pantyp;
	}
	public String getBeginworkdate() {
		return beginworkdate;
	}
	public void setBeginworkdate(String beginworkdate) {
		this.beginworkdate = beginworkdate;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getCheckMemo() {
		return checkMemo;
	}
	public void setCheckMemo(String checkMemo) {
		this.checkMemo = checkMemo;
	}
	public Integer getChkoutno() {
		return chkoutno;
	}
	public void setChkoutno(Integer chkoutno) {
		this.chkoutno = chkoutno;
	}
	public Integer getChkinno() {
		return chkinno;
	}
	public void setChkinno(Integer chkinno) {
		this.chkinno = chkinno;
	}
	public String getEcodeName() {
		return ecodeName;
	}
	public void setEcodeName(String ecodeName) {
		this.ecodeName = ecodeName;
	}
	public String getCheckCodeName() {
		return checkCodeName;
	}
	public void setCheckCodeName(String checkCodeName) {
		this.checkCodeName = checkCodeName;
	}
	
}
