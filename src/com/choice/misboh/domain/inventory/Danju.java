package com.choice.misboh.domain.inventory;

import java.util.Date;

public class Danju {
	private String acct;
	private String yearr;
	private String monthh;
	private String id;
	private String typ;
	private String typDes;
	private String vouno;
	private Date maded;
	private String madet;
	private Integer chkno;
	private String madeby;
	private String checby;
	private String positn;
	private String deliver;
	private String firm;
	private String positnDes;
	private String deliverDes;
	private String firmDes;
	private double totalamt;
	private String memo;
	
	private Date bdate;
	private Date edate;
	
	private String thePositn;

	public String getAcct() {
		return acct;
	}

	public void setAcct(String acct) {
		this.acct = acct;
	}

	public String getYearr() {
		return yearr;
	}

	public void setYearr(String yearr) {
		this.yearr = yearr;
	}

	public String getMonthh() {
		return monthh;
	}

	public void setMonthh(String monthh) {
		this.monthh = monthh;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getTypDes() {
		return typDes;
	}

	public void setTypDes(String typDes) {
		this.typDes = typDes;
	}

	public String getVouno() {
		return vouno;
	}

	public void setVouno(String vouno) {
		this.vouno = vouno;
	}

	public Date getMaded() {
		return maded;
	}

	public void setMaded(Date maded) {
		this.maded = maded;
	}

	public String getMadet() {
		return madet;
	}

	public void setMadet(String madet) {
		this.madet = madet;
	}

	public Integer getChkno() {
		return chkno;
	}

	public void setChkno(Integer chkno) {
		this.chkno = chkno;
	}

	public String getMadeby() {
		return madeby;
	}

	public void setMadeby(String madeby) {
		this.madeby = madeby;
	}

	public String getChecby() {
		return checby;
	}

	public void setChecby(String checby) {
		this.checby = checby;
	}

	public String getPositn() {
		return positn;
	}

	public void setPositn(String positn) {
		this.positn = positn;
	}

	public String getDeliver() {
		return deliver;
	}

	public void setDeliver(String deliver) {
		this.deliver = deliver;
	}

	public String getFirm() {
		return firm;
	}

	public void setFirm(String firm) {
		this.firm = firm;
	}

	public String getPositnDes() {
		return positnDes;
	}

	public void setPositnDes(String positnDes) {
		this.positnDes = positnDes;
	}

	public String getDeliverDes() {
		return deliverDes;
	}

	public void setDeliverDes(String deliverDes) {
		this.deliverDes = deliverDes;
	}

	public String getFirmDes() {
		return firmDes;
	}

	public void setFirmDes(String firmDes) {
		this.firmDes = firmDes;
	}

	public double getTotalamt() {
		return totalamt;
	}

	public void setTotalamt(double totalamt) {
		this.totalamt = totalamt;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Date getBdate() {
		return bdate;
	}

	public void setBdate(Date bdate) {
		this.bdate = bdate;
	}

	public Date getEdate() {
		return edate;
	}

	public void setEdate(Date edate) {
		this.edate = edate;
	}

	public String getThePositn() {
		return thePositn;
	}

	public void setThePositn(String thePositn) {
		this.thePositn = thePositn;
	}

}