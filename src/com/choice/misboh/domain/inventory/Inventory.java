package com.choice.misboh.domain.inventory;

import java.util.Date;
import java.util.List;

import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Supply;

//盘点
public class Inventory {

	private String acct;//帐套
	private Supply supply;//物资
	private List<String> sp_codes;
	private Positn positn;//仓位
	private String firm;//所属门店 档口用
	private String yearr;//年
	private int month;//月
	private Date date;//日期
	private double cntbla;//数量
	private double cntubla;//数量2
	private double amtbla;// 金额
	private String unitper;//
	private String pantyp;   //盘点类型，日盘 周盘 月盘 
	
	private List<Inventory> inventoryList;//list 盘点

	private int startNum;//延迟加载用 wjf
    private int endNum;
    
	private String sp_code;//仓位编码
	private String sp_name;//物资名称
	private String sp_desc;//物资规格
	private String sp_init;//物资缩写
	private String sp_type;//物资类型
	private String typdes;//物资类型名称
	private double sp_price;//单价
	private double pricesale;//售价
	private String unit;//标准单位
	private String unit1;//参考单位
	private String unit2;//成本单位
	private String unit3;//采购单位
	private String unit4;//库存单位
	
	private double stockcnt;//盘点数量
	private double stockamt;//库存调整金额 
	private double cnt1;//规格1数量
	private String spec1;//规格1
	private double unitper1;//转换率1
	private double cnt2;//规格2数量
	private String spec2;//规格2
	private double unitper2;//转换率1
	private double cnt3;//规格3数量
	private String spec3;//规格3
	private double unitper3;//转换率1
	private double cnt4;//规格4数量
	private String spec4;//规格4
	private double unitper4;//转换率1
	
	private String ex;//是否半成品
	private int orderNum;//顺序号
	private String ynpd;//是否禁盘
	private String ynprint;//是否打印
	
	private String save;//已经做过盘点的标识  后台sql用
	private String end;//做过盘点 但没审核的标识 页面判断按钮状态用
	
	private String chkstorefid;//盘点主表单号
	private String onlyQuery;//仅查询
	
	private String monthpan;//是否月盘
	
	private double last_price;//最后进价
	private String disunit;//配送单位
	private double disunitper;//配送单位转换率
	
	private String startNumber;//存放当前会计月之前的月份的数量合计
	private String startNumber1;
	private String startMoney;
	private Date edate;//当前会计月最后一天
	
	private String fromtyp;//盘点来自模板/正常填/导入/选择
    
	public String getFromtyp() {
		return fromtyp;
	}
	public void setFromtyp(String fromtyp) {
		this.fromtyp = fromtyp;
	}
	public String getStartNumber() {
		return startNumber;
	}
	public void setStartNumber(String startNumber) {
		this.startNumber = startNumber;
	}
	public String getStartNumber1() {
		return startNumber1;
	}
	public void setStartNumber1(String startNumber1) {
		this.startNumber1 = startNumber1;
	}
	public String getStartMoney() {
		return startMoney;
	}
	public void setStartMoney(String startMoney) {
		this.startMoney = startMoney;
	}
	public Date getEdate() {
		return edate;
	}
	public void setEdate(Date edate) {
		this.edate = edate;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public String getSave() {
		return save;
	}
	public void setSave(String save) {
		this.save = save;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public double getStockcnt() {
		return stockcnt;
	}
	public void setStockcnt(double stockcnt) {
		this.stockcnt = stockcnt;
	}
	public double getStockamt() {
		return stockamt;
	}
	public void setStockamt(double stockamt) {
		this.stockamt = stockamt;
	}
	public int getStartNum() {
		return startNum;
	}
	public void setStartNum(int startNum) {
		this.startNum = startNum;
	}
	public int getEndNum() {
		return endNum;
	}
	public void setEndNum(int endNum) {
		this.endNum = endNum;
	}
	public List<String> getSp_codes() {
		return sp_codes;
	}

	public void setSp_codes(List<String> sp_codes) {
		this.sp_codes = sp_codes;
	}

	public String getAcct() {
		return acct;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setAcct(String acct) {
		this.acct = acct;
	}

	public Supply getSupply() {
		return supply;
	}

	public void setSupply(Supply supply) {
		this.supply = supply;
	}

	public Positn getPositn() {
		return positn;
	}

	public void setPositn(Positn positn) {
		this.positn = positn;
	}

	public String getYearr() {
		return yearr;
	}

	public void setYearr(String yearr) {
		this.yearr = yearr;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public double getCntbla() {
		return cntbla;
	}
	public void setCntbla(double cntbla) {
		this.cntbla = cntbla;
	}
	public double getCntubla() {
		return cntubla;
	}
	public void setCntubla(double cntubla) {
		this.cntubla = cntubla;
	}
	public double getAmtbla() {
		return amtbla;
	}
	public void setAmtbla(double amtbla) {
		this.amtbla = amtbla;
	}
	public String getUnitper() {
		return unitper;
	}

	public void setUnitper(String unitper) {
		this.unitper = unitper;
	}

	public List<Inventory> getInventoryList() {
		return inventoryList;
	}

	public void setInventoryList(List<Inventory> inventoryList) {
		this.inventoryList = inventoryList;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
	public String getSp_desc() {
		return sp_desc;
	}
	public void setSp_desc(String sp_desc) {
		this.sp_desc = sp_desc;
	}
	public String getSp_init() {
		return sp_init;
	}
	public void setSp_init(String sp_init) {
		this.sp_init = sp_init;
	}
	public String getSp_type() {
		return sp_type;
	}
	public void setSp_type(String sp_type) {
		this.sp_type = sp_type;
	}
	public double getSp_price() {
		return sp_price;
	}
	public void setSp_price(double sp_price) {
		this.sp_price = sp_price;
	}
	public double getPricesale() {
		return pricesale;
	}
	public void setPricesale(double pricesale) {
		this.pricesale = pricesale;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getUnit2() {
		return unit2;
	}
	public void setUnit2(String unit2) {
		this.unit2 = unit2;
	}
	public double getCnt1() {
		return cnt1;
	}
	public void setCnt1(double cnt1) {
		this.cnt1 = cnt1;
	}
	public String getSpec1() {
		return spec1;
	}
	public void setSpec1(String spec1) {
		this.spec1 = spec1;
	}
	public double getUnitper1() {
		return unitper1;
	}
	public void setUnitper1(double unitper1) {
		this.unitper1 = unitper1;
	}
	public double getCnt2() {
		return cnt2;
	}
	public void setCnt2(double cnt2) {
		this.cnt2 = cnt2;
	}
	public String getSpec2() {
		return spec2;
	}
	public void setSpec2(String spec2) {
		this.spec2 = spec2;
	}
	public double getUnitper2() {
		return unitper2;
	}
	public void setUnitper2(double unitper2) {
		this.unitper2 = unitper2;
	}
	public double getCnt3() {
		return cnt3;
	}
	public void setCnt3(double cnt3) {
		this.cnt3 = cnt3;
	}
	public String getSpec3() {
		return spec3;
	}
	public void setSpec3(String spec3) {
		this.spec3 = spec3;
	}
	public double getUnitper3() {
		return unitper3;
	}
	public void setUnitper3(double unitper3) {
		this.unitper3 = unitper3;
	}
	public double getCnt4() {
		return cnt4;
	}
	public void setCnt4(double cnt4) {
		this.cnt4 = cnt4;
	}
	public String getSpec4() {
		return spec4;
	}
	public void setSpec4(String spec4) {
		this.spec4 = spec4;
	}
	public double getUnitper4() {
		return unitper4;
	}
	public void setUnitper4(double unitper4) {
		this.unitper4 = unitper4;
	}
	public String getEx() {
		return ex;
	}
	public void setEx(String ex) {
		this.ex = ex;
	}
	public int getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(int orderNum) {
		this.orderNum = orderNum;
	}
	public String getYnpd() {
		return ynpd;
	}
	public void setYnpd(String ynpd) {
		this.ynpd = ynpd;
	}
	public String getYnprint() {
		return ynprint;
	}
	public void setYnprint(String ynprint) {
		this.ynprint = ynprint;
	}
	public String getTypdes() {
		return typdes;
	}
	public void setTypdes(String typdes) {
		this.typdes = typdes;
	}
	public String getPantyp() {
		return pantyp;
	}
	public void setPantyp(String pantyp) {
		this.pantyp = pantyp;
	}
	public String getUnit1() {
		return unit1;
	}
	public void setUnit1(String unit1) {
		this.unit1 = unit1;
	}
	public String getUnit3() {
		return unit3;
	}
	public void setUnit3(String unit3) {
		this.unit3 = unit3;
	}
	public String getUnit4() {
		return unit4;
	}
	public void setUnit4(String unit4) {
		this.unit4 = unit4;
	}
	public String getChkstorefid() {
		return chkstorefid;
	}
	public void setChkstorefid(String chkstorefid) {
		this.chkstorefid = chkstorefid;
	}
	public String getOnlyQuery() {
		return onlyQuery;
	}
	public void setOnlyQuery(String onlyQuery) {
		this.onlyQuery = onlyQuery;
	}
	public String getMonthpan() {
		return monthpan;
	}
	public void setMonthpan(String monthpan) {
		this.monthpan = monthpan;
	}
	public double getLast_price() {
		return last_price;
	}
	public void setLast_price(double last_price) {
		this.last_price = last_price;
	}
	public String getDisunit() {
		return disunit;
	}
	public void setDisunit(String disunit) {
		this.disunit = disunit;
	}
	public double getDisunitper() {
		return disunitper;
	}
	public void setDisunitper(double disunitper) {
		this.disunitper = disunitper;
	}
}
