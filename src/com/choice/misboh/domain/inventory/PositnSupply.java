package com.choice.misboh.domain.inventory;

import java.util.List;

public class PositnSupply {

	private String acct;//帐套
	private String yearr;//年份
	private String positn;//仓位编码
	private String positnName;//仓位名字
	private String sp_code;//物资编码
	private String sp_name;//物资名称
	private String sp_desc;//物资规格
	private String sp_init;//物资缩写
	private String sp_type;//物资类型
	private double sp_price;//单价
	private double unitper;//转换率
	private String unit;//标准单位
	private double inc0;//标准数量
	private double ina0;//标准金额
	private double incu0;//参考数量
	
	private double cnt1;//规格1数量
	private String spec1;//规格1
	private double unitper1;//转换率1
	private double cnt2;//规格2数量
	private String spec2;//规格2
	private double unitper2;//转换率1
	private double cnt3;//规格3数量
	private String spec3;//规格3
	private double unitper3;//转换率1
	private double cnt4;//规格4数量
	private String spec4;//规格4
	private double unitper4;//转换率1
	
	private String firm;//档口期初的时候用来保存所属门店的编码
	private String id;//保存supplybeginning用
	
	private String ynprint;//是否打印
	private String ynpd;//是否盘点  盘点用
	private int orderNum;//顺序号 盘点用
	
	private String isTemplete;//是否是模板下载 Y查分店物资属性 其他正常查
	
	private List<PositnSupply> positnSupplyList;//list 期初
	
	public List<PositnSupply> getPositnSupplyList() {
		return positnSupplyList;
	}

	public void setPositnSupplyList(List<PositnSupply> positnSupplyList) {
		this.positnSupplyList = positnSupplyList;
	}

	public String getIsTemplete() {
		return isTemplete;
	}

	public void setIsTemplete(String isTemplete) {
		this.isTemplete = isTemplete;
	}

	public String getYnpd() {
		return ynpd;
	}

	public void setYnpd(String ynpd) {
		this.ynpd = ynpd;
	}
	public String getYnprint() {
		return ynprint;
	}
	public void setYnprint(String ynprint) {
		this.ynprint = ynprint;
	}
	public int getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(int orderNum) {
		this.orderNum = orderNum;
	}

	public String getFirm() {
		return firm;
	}

	public void setFirm(String firm) {
		this.firm = firm;
	}

	public String getPositnName() {
		return positnName;
	}

	public void setPositnName(String positnName) {
		this.positnName = positnName;
	}

	public PositnSupply() {
		super();
	}

	public String getAcct() {
		return acct;
	}

	public void setAcct(String acct) {
		this.acct = acct;
	}

	public String getYearr() {
		return yearr;
	}

	public void setYearr(String yearr) {
		this.yearr = yearr;
	}

	public String getPositn() {
		return positn;
	}

	public void setPositn(String positn) {
		this.positn = positn;
	}

	public String getSp_code() {
		return sp_code;
	}

	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}

	public String getSp_name() {
		return sp_name;
	}

	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}

	public String getSp_desc() {
		return sp_desc;
	}

	public void setSp_desc(String sp_desc) {
		this.sp_desc = sp_desc;
	}

	public String getSp_type() {
		return sp_type;
	}

	public void setSp_type(String sp_type) {
		this.sp_type = sp_type;
	}

	public double getUnitper() {
		return unitper;
	}

	public void setUnitper(double unitper) {
		this.unitper = unitper;
	}

	public double getInc0() {
		return inc0;
	}

	public void setInc0(double inc0) {
		this.inc0 = inc0;
	}

	public double getIna0() {
		return ina0;
	}

	public void setIna0(double ina0) {
		this.ina0 = ina0;
	}

	public double getIncu0() {
		return incu0;
	}

	public void setIncu0(double incu0) {
		this.incu0 = incu0;
	}

	public String getSpec1() {
		return spec1;
	}

	public void setSpec1(String spec1) {
		this.spec1 = spec1;
	}

	public String getSpec2() {
		return spec2;
	}

	public void setSpec2(String spec2) {
		this.spec2 = spec2;
	}

	public String getSpec3() {
		return spec3;
	}

	public void setSpec3(String spec3) {
		this.spec3 = spec3;
	}

	public String getSpec4() {
		return spec4;
	}

	public void setSpec4(String spec4) {
		this.spec4 = spec4;
	}

	public double getUnitper1() {
		return unitper1;
	}

	public void setUnitper1(double unitper1) {
		this.unitper1 = unitper1;
	}

	public double getUnitper2() {
		return unitper2;
	}

	public void setUnitper2(double unitper2) {
		this.unitper2 = unitper2;
	}

	public double getUnitper3() {
		return unitper3;
	}

	public void setUnitper3(double unitper3) {
		this.unitper3 = unitper3;
	}

	public double getUnitper4() {
		return unitper4;
	}

	public void setUnitper4(double unitper4) {
		this.unitper4 = unitper4;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public double getCnt1() {
		return cnt1;
	}

	public void setCnt1(double cnt1) {
		this.cnt1 = cnt1;
	}

	public double getCnt2() {
		return cnt2;
	}

	public void setCnt2(double cnt2) {
		this.cnt2 = cnt2;
	}

	public double getCnt3() {
		return cnt3;
	}

	public void setCnt3(double cnt3) {
		this.cnt3 = cnt3;
	}

	public double getCnt4() {
		return cnt4;
	}

	public void setCnt4(double cnt4) {
		this.cnt4 = cnt4;
	}

	public String getSp_init() {
		return sp_init;
	}

	public void setSp_init(String sp_init) {
		this.sp_init = sp_init;
	}

	public double getSp_price() {
		return sp_price;
	}

	public void setSp_price(double sp_price) {
		this.sp_price = sp_price;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
