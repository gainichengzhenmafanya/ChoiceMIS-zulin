package com.choice.misboh.domain.inventory;


/***
 * 盘点子表
 * @author wjf
 *
 */
public class Chkstoreo {

	private String chkstoreoid;//主键
	private String chkstorefid;//主表主键
	private String sp_code;//物资编码
	private String workDate;//营业日
	private double stockCnt;//盘点合计数量
	private double price;//价格
	private double cnt1;//规格1数量
	private double cnt2;//规格2数量
	private double cnt3;//规格3数量
	private double cnt4;//规格4数量
	private double memo;//备注 没用
	private String ynpd;//是否盘点  盘点用
	private String ynprint;//是否打印
	private int orderNum;//顺序号 盘点用
	
	//报表专用
	private String firm;//门店编码
	private String dept;//要盘点的仓位
	private String ecode;//盘点人
	private String inputDate;//盘点时间
	private String checkCode;//审核人
	private String checkDate;//审核时间
	private String state;//盘点状态  0未审核 1已审核
	private String pantyp;//盘点类型  daypan日盘 weekpan周盘 monthpan月盘
	private double totalamt;//合计金额
	
	private String jmj;//九毛九专用的
	//条件
	private String bdate;
	private String edate;
	private String grptyp;
	private String grp;
	private String typ;
	private String sort;
	private String order;
	
	private String fromtyp;//盘点来源  目前只有模板template和其他
	
	public String getYnpd() {
		return ynpd;
	}

	public void setYnpd(String ynpd) {
		this.ynpd = ynpd;
	}
	public String getYnprint() {
		return ynprint;
	}
	public void setYnprint(String ynprint) {
		this.ynprint = ynprint;
	}
	public int getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(int orderNum) {
		this.orderNum = orderNum;
	}
	
	
	public String getChkstoreoid() {
		return chkstoreoid;
	}
	public void setChkstoreoid(String chkstoreoid) {
		this.chkstoreoid = chkstoreoid;
	}
	public String getChkstorefid() {
		return chkstorefid;
	}
	public void setChkstorefid(String chkstorefid) {
		this.chkstorefid = chkstorefid;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public String getWorkDate() {
		return workDate;
	}
	public void setWorkDate(String workDate) {
		this.workDate = workDate;
	}
	public double getStockCnt() {
		return stockCnt;
	}
	public void setStockCnt(double stockCnt) {
		this.stockCnt = stockCnt;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getCnt1() {
		return cnt1;
	}
	public void setCnt1(double cnt1) {
		this.cnt1 = cnt1;
	}
	public double getCnt2() {
		return cnt2;
	}
	public void setCnt2(double cnt2) {
		this.cnt2 = cnt2;
	}
	public double getCnt3() {
		return cnt3;
	}
	public void setCnt3(double cnt3) {
		this.cnt3 = cnt3;
	}
	public double getCnt4() {
		return cnt4;
	}
	public void setCnt4(double cnt4) {
		this.cnt4 = cnt4;
	}
	public double getMemo() {
		return memo;
	}
	public void setMemo(double memo) {
		this.memo = memo;
	}

	public String getFirm() {
		return firm;
	}

	public void setFirm(String firm) {
		this.firm = firm;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public String getEcode() {
		return ecode;
	}

	public void setEcode(String ecode) {
		this.ecode = ecode;
	}

	public String getInputDate() {
		return inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public String getCheckCode() {
		return checkCode;
	}

	public void setCheckCode(String checkCode) {
		this.checkCode = checkCode;
	}

	public String getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPantyp() {
		return pantyp;
	}

	public void setPantyp(String pantyp) {
		this.pantyp = pantyp;
	}

	public double getTotalamt() {
		return totalamt;
	}

	public void setTotalamt(double totalamt) {
		this.totalamt = totalamt;
	}

	public String getJmj() {
		return jmj;
	}

	public void setJmj(String jmj) {
		this.jmj = jmj;
	}

	public String getBdate() {
		return bdate;
	}

	public void setBdate(String bdate) {
		this.bdate = bdate;
	}

	public String getEdate() {
		return edate;
	}

	public void setEdate(String edate) {
		this.edate = edate;
	}

	public String getGrptyp() {
		return grptyp;
	}

	public void setGrptyp(String grptyp) {
		this.grptyp = grptyp;
	}

	public String getGrp() {
		return grp;
	}

	public void setGrp(String grp) {
		this.grp = grp;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getFromtyp() {
		return fromtyp;
	}

	public void setFromtyp(String fromtyp) {
		this.fromtyp = fromtyp;
	}
	
}
