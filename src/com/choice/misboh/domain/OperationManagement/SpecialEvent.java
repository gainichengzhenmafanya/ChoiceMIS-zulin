package com.choice.misboh.domain.OperationManagement;

import java.util.List;

/**
 * 描述：特殊事件
 * @author 马振
 * 创建时间：2015-5-20 下午6:36:54
 */
public class SpecialEvent {
	private String pk_specialevent;	//主键
	private String pk_specialitem;	//特殊事件主表主键
	private String vcode;			//主表信息编码
	private String vname;			//特殊事件主表名称
	private String pk_store;		//门店主键
	private String vcontent;		//内容(子表名称)
	private String dworkdate;		//日期
	private String vitemtyp;		//因素类型（1选择项，2填写项）
	private List<SpecialEvent> listSelectItemDtl;
	public String getPk_specialevent() {
		return pk_specialevent;
	}
	public void setPk_specialevent(String pk_specialevent) {
		this.pk_specialevent = pk_specialevent;
	}
	public String getPk_specialitem() {
		return pk_specialitem;
	}
	public void setPk_specialitem(String pk_specialitem) {
		this.pk_specialitem = pk_specialitem;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
	public String getVcontent() {
		return vcontent;
	}
	public void setVcontent(String vcontent) {
		this.vcontent = vcontent;
	}
	public String getDworkdate() {
		return dworkdate;
	}
	public void setDworkdate(String dworkdate) {
		this.dworkdate = dworkdate;
	}
	public String getVitemtyp() {
		return vitemtyp;
	}
	public void setVitemtyp(String vitemtyp) {
		this.vitemtyp = vitemtyp;
	}
	public List<SpecialEvent> getListSelectItemDtl() {
		return listSelectItemDtl;
	}
	public void setListSelectItemDtl(List<SpecialEvent> listSelectItemDtl) {
		this.listSelectItemDtl = listSelectItemDtl;
	}
}