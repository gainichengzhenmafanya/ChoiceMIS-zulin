package com.choice.misboh.domain.OperationManagement;

import java.util.List;

/**
 * 描述：门店配送范围主子表实体类
 * @author 马振
 * 创建时间：2016-3-12 下午2:59:22
 */
public class StoreRange {
	private String pk_storerange;	//主表主键
	private String pk_rangecoordi;	//子表主键
	private String pk_group;	//企业编号
	private String pk_store;	//门店主键
	private String iareaseq;	//配送区域序号
	private String vareaname;	//配送区域名称
	private String nstartprice;	//起送价
	private String ndistributfee;	//配送费
	private String vcolor;		//颜色
	private String vlng;		//经度
	private String vlat;		//维度
	private String ts;		//保存时间
	private String vedf1;		//扩展字段1
	private String vedf2;		//扩展字段2
	private String vedf3;		//扩展字段3
	private String vedf4;		//扩展字段4
	private String vedf5;		//扩展字段5
	private List<StoreRange> listRangeCoordi;
	
	public String getPk_storerange() {
		return pk_storerange;
	}
	public void setPk_storerange(String pk_storerange) {
		this.pk_storerange = pk_storerange;
	}
	public String getPk_rangecoordi() {
		return pk_rangecoordi;
	}
	public void setPk_rangecoordi(String pk_rangecoordi) {
		this.pk_rangecoordi = pk_rangecoordi;
	}
	public String getPk_group() {
		return pk_group;
	}
	public void setPk_group(String pk_group) {
		this.pk_group = pk_group;
	}
	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
	public String getIareaseq() {
		return iareaseq;
	}
	public void setIareaseq(String iareaseq) {
		this.iareaseq = iareaseq;
	}
	public String getVareaname() {
		return vareaname;
	}
	public void setVareaname(String vareaname) {
		this.vareaname = vareaname;
	}
	public String getNstartprice() {
		return nstartprice;
	}
	public void setNstartprice(String nstartprice) {
		this.nstartprice = nstartprice;
	}
	public String getNdistributfee() {
		return ndistributfee;
	}
	public void setNdistributfee(String ndistributfee) {
		this.ndistributfee = ndistributfee;
	}
	public String getVcolor() {
		return vcolor;
	}
	public void setVcolor(String vcolor) {
		this.vcolor = vcolor;
	}
	public String getVlng() {
		return vlng;
	}
	public void setVlng(String vlng) {
		this.vlng = vlng;
	}
	public String getVlat() {
		return vlat;
	}
	public void setVlat(String vlat) {
		this.vlat = vlat;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public String getVedf1() {
		return vedf1;
	}
	public void setVedf1(String vedf1) {
		this.vedf1 = vedf1;
	}
	public String getVedf2() {
		return vedf2;
	}
	public void setVedf2(String vedf2) {
		this.vedf2 = vedf2;
	}
	public String getVedf3() {
		return vedf3;
	}
	public void setVedf3(String vedf3) {
		this.vedf3 = vedf3;
	}
	public String getVedf4() {
		return vedf4;
	}
	public void setVedf4(String vedf4) {
		this.vedf4 = vedf4;
	}
	public String getVedf5() {
		return vedf5;
	}
	public void setVedf5(String vedf5) {
		this.vedf5 = vedf5;
	}
	public List<StoreRange> getListRangeCoordi() {
		return listRangeCoordi;
	}
	public void setListRangeCoordi(List<StoreRange> listRangeCoordi) {
		this.listRangeCoordi = listRangeCoordi;
	}
}