package com.choice.misboh.domain.OperationManagement;

/**
 * 描述：存款管理
 * @author 马振
 * 创建时间：2015-5-20 下午3:01:27
 */
public class CashSaving {
	private String pk_cashsaving;	//主键
	private String pk_store;		//门店主键
	private String vscode;			//门店编码
	private String vecode;			//员工编码
	private String dworkdate;		//营业日
	private String vsavingno;		//存款单号
	private Double namount;			//存款金额
	private String vcurrencytypeid;	//币种类型
	private String vsavingdate;		//存款日期
	private String vsavingtypeid;	//存入账号，存款分类
	private Double ncash;			//营业日现金合计金额
	private String ndeposit;		//存款差异
	private String vcomments;		//备注
	private String pk_paymode;		//支付方式主键
	private String vname;			//支付方式名称
	private String pk_accounttype;	//账户类型主键
	private String pk_group;		//集团
	
	private String bdat;			//开始日期
	private String edat;			//结束日期
	private String queryMod;		//查询 按回单日期还是营业日  add ygb 20160823
	
	private String flag;			//修改或保存的标识
	private String isgo;			//是否继续添加下一个存款单
	
	public String getPk_cashsaving() {
		return pk_cashsaving;
	}
	public void setPk_cashsaving(String pk_cashsaving) {
		this.pk_cashsaving = pk_cashsaving;
	}
	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
	public String getVscode() {
		return vscode;
	}
	public void setVscode(String vscode) {
		this.vscode = vscode;
	}
	public String getVecode() {
		return vecode;
	}
	public void setVecode(String vecode) {
		this.vecode = vecode;
	}
	public String getDworkdate() {
		return dworkdate;
	}
	public void setDworkdate(String dworkdate) {
		this.dworkdate = dworkdate;
	}
	public String getVsavingno() {
		return vsavingno;
	}
	public void setVsavingno(String vsavingno) {
		this.vsavingno = vsavingno;
	}
	public Double getNamount() {
		return namount;
	}
	public void setNamount(Double namount) {
		this.namount = namount;
	}
	public String getVcurrencytypeid() {
		return vcurrencytypeid;
	}
	public void setVcurrencytypeid(String vcurrencytypeid) {
		this.vcurrencytypeid = vcurrencytypeid;
	}
	public String getVsavingdate() {
		return vsavingdate;
	}
	public void setVsavingdate(String vsavingdate) {
		this.vsavingdate = vsavingdate;
	}
	public String getVsavingtypeid() {
		return vsavingtypeid;
	}
	public void setVsavingtypeid(String vsavingtypeid) {
		this.vsavingtypeid = vsavingtypeid;
	}
	public String getVcomments() {
		return vcomments;
	}
	public void setVcomments(String vcomments) {
		this.vcomments = vcomments;
	}
	public String getPk_group() {
		return pk_group;
	}
	public void setPk_group(String pk_group) {
		this.pk_group = pk_group;
	}
	public String getPk_paymode() {
		return pk_paymode;
	}
	public void setPk_paymode(String pk_paymode) {
		this.pk_paymode = pk_paymode;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getPk_accounttype() {
		return pk_accounttype;
	}
	public void setPk_accounttype(String pk_accounttype) {
		this.pk_accounttype = pk_accounttype;
	}
	public String getBdat() {
		return bdat;
	}
	public void setBdat(String bdat) {
		this.bdat = bdat;
	}
	public String getEdat() {
		return edat;
	}
	public void setEdat(String edat) {
		this.edat = edat;
	}
	public Double getNcash() {
		return ncash;
	}
	public void setNcash(Double ncash) {
		this.ncash = ncash;
	}
	public String getNdeposit() {
		return ndeposit;
	}
	public void setNdeposit(String ndeposit) {
		this.ndeposit = ndeposit;
	}
	public String getQueryMod() {
		return queryMod;
	}
	public void setQueryMod(String queryMod) {
		this.queryMod = queryMod;
	}
	public String getIsgo() {
		return isgo;
	}
	public void setIsgo(String isgo) {
		this.isgo = isgo;
	}
}