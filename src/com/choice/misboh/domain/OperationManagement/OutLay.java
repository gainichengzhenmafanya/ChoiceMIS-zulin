package com.choice.misboh.domain.OperationManagement;

import java.util.List;


/**
 * 描述：坐支明细设置
 * @author 马振
 * 创建时间：2015-5-18 上午10:26:44
 */
public class OutLay {
	private String pk_outlay;		//主键
	private String vname;			//名称
	private Integer enablestate;	//启用状态
	private Integer isortno;		//排序列
	private List<CashMx> listCashMx;
	private Double nprice;			//坐支金额
	private String vcomments;		//备注
	private String pk_store;		//门店主键
	private Integer iquantity;		//数量
	private String dworkdate;		//营业日
	
	public String getPk_outlay() {
		return pk_outlay;
	}
	public void setPk_outlay(String pk_outlay) {
		this.pk_outlay = pk_outlay;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public Integer getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(Integer enablestate) {
		this.enablestate = enablestate;
	}
	public Integer getIsortno() {
		return isortno;
	}
	public void setIsortno(Integer isortno) {
		this.isortno = isortno;
	}
	public List<CashMx> getListCashMx() {
		return listCashMx;
	}
	public void setListCashMx(List<CashMx> listCashMx) {
		this.listCashMx = listCashMx;
	}
	public Double getNprice() {
		return nprice;
	}
	public void setNprice(Double nprice) {
		this.nprice = nprice;
	}
	public String getVcomments() {
		return vcomments;
	}
	public void setVcomments(String vcomments) {
		this.vcomments = vcomments;
	}
	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
	public Integer getIquantity() {
		return iquantity;
	}
	public void setIquantity(Integer iquantity) {
		this.iquantity = iquantity;
	}
	public String getDworkdate() {
		return dworkdate;
	}
	public void setDworkdate(String dworkdate) {
		this.dworkdate = dworkdate;
	}
}