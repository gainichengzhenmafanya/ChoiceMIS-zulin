package com.choice.misboh.domain.OperationManagement;

/**
 * 描述：明细金额详细录入
 * @author 马振
 * 创建时间：2015-5-20 下午1:08:51
 */
public class CashMx {
	private String pk_cashmx;		//主键
	private String pk_store;		//门店主键
	private String vscode;			//门店编码
	private String vecode;			//员工编码
	private String dworkdate;		//营业日
	private String vitemcode;		//坐支明细主键
	private String vmxcode;
	private Integer iquantity;		//数量
	private Double nprice;			//坐支金额
	private Double namount;			
	private String vcomments;		//备注
	private String pk_group;		//集团
	
	public String getPk_cashmx() {
		return pk_cashmx;
	}
	public void setPk_cashmx(String pk_cashmx) {
		this.pk_cashmx = pk_cashmx;
	}
	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
	public String getVscode() {
		return vscode;
	}
	public void setVscode(String vscode) {
		this.vscode = vscode;
	}
	public String getVecode() {
		return vecode;
	}
	public void setVecode(String vecode) {
		this.vecode = vecode;
	}
	public String getDworkdate() {
		return dworkdate;
	}
	public void setDworkdate(String dworkdate) {
		this.dworkdate = dworkdate;
	}
	public String getVitemcode() {
		return vitemcode;
	}
	public void setVitemcode(String vitemcode) {
		this.vitemcode = vitemcode;
	}
	public String getVmxcode() {
		return vmxcode;
	}
	public void setVmxcode(String vmxcode) {
		this.vmxcode = vmxcode;
	}
	public Integer getIquantity() {
		return iquantity;
	}
	public void setIquantity(Integer iquantity) {
		this.iquantity = iquantity;
	}
	public Double getNprice() {
		return nprice;
	}
	public void setNprice(Double nprice) {
		this.nprice = nprice;
	}
	public Double getNamount() {
		return namount;
	}
	public void setNamount(Double namount) {
		this.namount = namount;
	}
	public String getVcomments() {
		return vcomments;
	}
	public void setVcomments(String vcomments) {
		this.vcomments = vcomments;
	}
	public String getPk_group() {
		return pk_group;
	}
	public void setPk_group(String pk_group) {
		this.pk_group = pk_group;
	}
}