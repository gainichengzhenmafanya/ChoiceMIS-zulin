package com.choice.misboh.domain.salesforecast;

/**
 * 营业额预估MAP对象KEY值
 * @author 文清泉
 * @param 2014年9月29日 下午2:32:17
 */
public enum SalesForecastEnum {

    HOLIDAY, //节假日
    TC,// 单数
    MONEY,// 营业额
    PEOLENUM,// 人数
    WORKDATE, //日期
    WEEKDAY,//周次
    WEEK_ONE,//周一
    WEEK_TWO,//周二
    WEEK_THREE,//周三
    WEEK_FOUR,//周四
    WEEK_FIVE,//周五
    WEEK_SIX,//周六
    WEEK_SEVEN,//周日
	
}
