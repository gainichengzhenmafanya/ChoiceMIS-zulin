package com.choice.misboh.domain.salesforecast;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 营业预估
 * @author css
 *
 */
public class SalePlan implements Serializable{   

	/**
	 * 
	 */
	private static final long serialVersionUID = -4237269811873696946L;
	private String acct;
	private List<SalePlan> SalePlanList;   //修改用
	private List<String> firmList;   //分店号list
	private String firm;//分店号
	private String firmNm;//分店名
	private String dworkdate;//字符串日期
	private Date dat;//日期
	private Date startdate;//起始日期
	private Date enddate;//结束日期
	private Date bdate;//参考日期
	private Date edate;//参考日期
	private String week;//周
	private String memo;//备注
	private double pax;// 实际
	
	private double pax1;//一班调整
	private double pax2;//二班调整
	private double pax3;//三班调整
	private double pax4;//四班调整
	
	private double pax1old;//一班预估
	private double pax2old;//二班预估
	private double pax3old;//三班预估
	private double pax4old;//四班预估
	
	private double pax1act;// 一班实际
	private double pax2act;// 二班实际
	private double pax3act;// 三班实际
	private double pax4act;// 四班实际
	
	private double lv1;// 一班预估差异率
	private double lv2;// 二班预估差异率
	private double lv3;// 三班预估差异率
	private double lv4;// 四班预估差异率
	
	private double lv1old;// 一班调整差异率
	private double lv2old;// 二班调整差异率
	private double lv3old;// 三班调整差异率
	private double lv4old;// 四班调整差异率
	
	private double total;// 合计调整
	private double totalold;// 合计预估
	private double totalact;// 合计实际
	private double totallv;// 合计预估差异率
	private double totallvold;// 合计预估差异率
	private String ynZb;//是否总部
	private String area;//区域
	
	private String dept;//档口编码
	private String deptdes;//档口名称
	
	//新添加字段=====mis用
	private Double money_y;//金额预估值
	private Double money_t;//金额调整值
	private int bills_y;//单数预估值
	private int bills_t;//单数调整值
	private int people_y;//客流预估值
	private int people_t;//客流调整值
	private Double ac_y;//单均预估
	private Double ac_t;//单均调整
	private Double percapita_y;//人均预估值
	private Double percapita_t;//人均调整
	private double last_money;//去年同期营业额
	private int last_bills;//去年同期单数
	private int last_people;//去年同期人数(客流)
	private double last_ac;//去年同期单均
	private double last_percapita;//去年同期人均
	private String isHoliday;//是否是节假日
	private String showHoliday;//节假日显示
	
	//查询和后台处理数据用
	private double money;//去年同期营业额
	private int tc;//去年同期单数
	private int peoplenum;//去年同期人数
	private double last_totalamt;//去年同期营业额合计
	private double totalamt_y;//预估营业额合计
	private double totalamt_t;//调整营业额合计
	private double last_totalpercapita;//去年同期人均合计
	private double totalpercapita_y;//预估人均合计
	private double totalpercapita_t;//调整人均合计
	private int last_totalpeople;//去年同期人数合计
	private int totalpeople_y;//预估人数合计
	private int totalpeople_t;//调整人数合计
	private int last_totaltc;//去年同期单数合计
	private int totaltc_y;//预估单数合计
	private int totaltc_t;//调整单数合计
	private double last_totalac;//去年同期单均合计
	private double totalac_y;//预估单均合计
	private double totalac_t;//调整但均合计
	//用于页面的判断
	private int trueorfalse;
	
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public int getTrueorfalse() {
		return trueorfalse;
	}
	public void setTrueorfalse(int trueorfalse) {
		this.trueorfalse = trueorfalse;
	}
	public double getMoney() {
		return money;
	}
	public void setMoney(double money) {
		this.money = money;
	}
	public int getTc() {
		return tc;
	}
	public void setTc(int tc) {
		this.tc = tc;
	}
	public int getPeoplenum() {
		return peoplenum;
	}
	public void setPeoplenum(int peoplenum) {
		this.peoplenum = peoplenum;
	}
	public Double getMoney_y() {
		return money_y;
	}
	public void setMoney_y(Double money_y) {
		this.money_y = money_y;
	}
	public Double getMoney_t() {
		return money_t;
	}
	public void setMoney_t(Double money_t) {
		this.money_t = money_t;
	}
	public int getBills_y() {
		return bills_y;
	}
	public void setBills_y(int bills_y) {
		this.bills_y = bills_y;
	}
	public int getBills_t() {
		return bills_t;
	}
	public void setBills_t(int bills_t) {
		this.bills_t = bills_t;
	}
	public int getPeople_y() {
		return people_y;
	}
	public void setPeople_y(int people_y) {
		this.people_y = people_y;
	}
	public int getPeople_t() {
		return people_t;
	}
	public void setPeople_t(int people_t) {
		this.people_t = people_t;
	}
	public Double getAc_y() {
		return ac_y;
	}
	public void setAc_y(Double ac_y) {
		this.ac_y = ac_y;
	}
	public Double getAc_t() {
		return ac_t;
	}
	public void setAc_t(Double ac_t) {
		this.ac_t = ac_t;
	}
	public Double getPercapita_y() {
		return percapita_y;
	}
	public void setPercapita_y(Double percapita_y) {
		this.percapita_y = percapita_y;
	}
	public Double getPercapita_t() {
		return percapita_t;
	}
	public void setPercapita_t(Double percapita_t) {
		this.percapita_t = percapita_t;
	}
	public String getShowHoliday() {
		return showHoliday;
	}
	public void setShowHoliday(String showHoliday) {
		this.showHoliday = showHoliday;
	}
	public String getIsHoliday() {
		return isHoliday;
	}
	public void setIsHoliday(String isHoliday) {
		this.isHoliday = isHoliday;
	}
	public double getLast_money() {
		return last_money;
	}
	public void setLast_money(double last_money) {
		this.last_money = last_money;
	}
	public int getLast_bills() {
		return last_bills;
	}
	public void setLast_bills(int last_bills) {
		this.last_bills = last_bills;
	}
	public int getLast_people() {
		return last_people;
	}
	public void setLast_people(int last_people) {
		this.last_people = last_people;
	}
	public double getLast_ac() {
		return last_ac;
	}
	public void setLast_ac(double last_ac) {
		this.last_ac = last_ac;
	}
	public double getLast_percapita() {
		return last_percapita;
	}
	public void setLast_percapita(double last_percapita) {
		this.last_percapita = last_percapita;
	}
	public double getLast_totalamt() {
		return last_totalamt;
	}
	public void setLast_totalamt(double last_totalamt) {
		this.last_totalamt = last_totalamt;
	}
	public double getTotalamt_y() {
		return totalamt_y;
	}
	public void setTotalamt_y(double totalamt_y) {
		this.totalamt_y = totalamt_y;
	}
	public double getTotalamt_t() {
		return totalamt_t;
	}
	public void setTotalamt_t(double totalamt_t) {
		this.totalamt_t = totalamt_t;
	}
	public double getLast_totalpercapita() {
		return last_totalpercapita;
	}
	public void setLast_totalpercapita(double last_totalpercapita) {
		this.last_totalpercapita = last_totalpercapita;
	}
	public double getTotalpercapita_y() {
		return totalpercapita_y;
	}
	public void setTotalpercapita_y(double totalpercapita_y) {
		this.totalpercapita_y = totalpercapita_y;
	}
	public double getTotalpercapita_t() {
		return totalpercapita_t;
	}
	public void setTotalpercapita_t(double totalpercapita_t) {
		this.totalpercapita_t = totalpercapita_t;
	}
	public int getLast_totalpeople() {
		return last_totalpeople;
	}
	public void setLast_totalpeople(int last_totalpeople) {
		this.last_totalpeople = last_totalpeople;
	}
	public int getTotalpeople_y() {
		return totalpeople_y;
	}
	public void setTotalpeople_y(int totalpeople_y) {
		this.totalpeople_y = totalpeople_y;
	}
	public int getTotalpeople_t() {
		return totalpeople_t;
	}
	public void setTotalpeople_t(int totalpeople_t) {
		this.totalpeople_t = totalpeople_t;
	}
	public int getLast_totaltc() {
		return last_totaltc;
	}
	public void setLast_totaltc(int last_totaltc) {
		this.last_totaltc = last_totaltc;
	}
	public int getTotaltc_y() {
		return totaltc_y;
	}
	public void setTotaltc_y(int totaltc_y) {
		this.totaltc_y = totaltc_y;
	}
	public int getTotaltc_t() {
		return totaltc_t;
	}
	public void setTotaltc_t(int totaltc_t) {
		this.totaltc_t = totaltc_t;
	}
	public double getLast_totalac() {
		return last_totalac;
	}
	public void setLast_totalac(double last_totalac) {
		this.last_totalac = last_totalac;
	}
	public double getTotalac_y() {
		return totalac_y;
	}
	public void setTotalac_y(double totalac_y) {
		this.totalac_y = totalac_y;
	}
	public double getTotalac_t() {
		return totalac_t;
	}
	public void setTotalac_t(double totalac_t) {
		this.totalac_t = totalac_t;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getDeptdes() {
		return deptdes;
	}
	public void setDeptdes(String deptdes) {
		this.deptdes = deptdes;
	}
	public String getYnZb() {
		return ynZb;
	}
	public void setYnZb(String ynZb) {
		this.ynZb = ynZb;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getFirmNm() {
		return firmNm;
	}
	public void setFirmNm(String firmNm) {
		this.firmNm = firmNm;
	}
	public List<String> getFirmList() {
		return firmList;
	}
	public void setFirmList(List<String> firmList) {
		this.firmList = firmList;
	}
	public double getTotalact() {
		return totalact;
	}
	public void setTotalact(double totalact) {
		this.totalact = totalact;
	}
	public double getTotallv() {
		return totallv;
	}
	public void setTotallv(double totallv) {
		this.totallv = totallv;
	}
	public double getTotallvold() {
		return totallvold;
	}
	public void setTotallvold(double totallvold) {
		this.totallvold = totallvold;
	}
	public double getPax() {
		return pax;
	}
	public void setPax(double pax) {
		this.pax = pax;
	}
	public double getPax1act() {
		return pax1act;
	}
	public void setPax1act(double pax1act) {
		this.pax1act = pax1act;
	}
	public double getPax2act() {
		return pax2act;
	}
	public void setPax2act(double pax2act) {
		this.pax2act = pax2act;
	}
	public double getPax3act() {
		return pax3act;
	}
	public void setPax3act(double pax3act) {
		this.pax3act = pax3act;
	}
	public double getPax4act() {
		return pax4act;
	}
	public void setPax4act(double pax4act) {
		this.pax4act = pax4act;
	}
	public double getLv1() {
		return lv1;
	}
	public void setLv1(double lv1) {
		this.lv1 = lv1;
	}
	public double getLv2() {
		return lv2;
	}
	public void setLv2(double lv2) {
		this.lv2 = lv2;
	}
	public double getLv3() {
		return lv3;
	}
	public void setLv3(double lv3) {
		this.lv3 = lv3;
	}
	public double getLv4() {
		return lv4;
	}
	public void setLv4(double lv4) {
		this.lv4 = lv4;
	}
	public double getLv1old() {
		return lv1old;
	}
	public void setLv1old(double lv1old) {
		this.lv1old = lv1old;
	}
	public double getLv2old() {
		return lv2old;
	}
	public void setLv2old(double lv2old) {
		this.lv2old = lv2old;
	}
	public double getLv3old() {
		return lv3old;
	}
	public void setLv3old(double lv3old) {
		this.lv3old = lv3old;
	}
	public double getLv4old() {
		return lv4old;
	}
	public void setLv4old(double lv4old) {
		this.lv4old = lv4old;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public double getTotalold() {
		return totalold;
	}
	public void setTotalold(double totalold) {
		this.totalold = totalold;
	}
	public String getDworkdate() {
		return dworkdate;
	}
	public void setDworkdate(String dworkdate) {
		this.dworkdate = dworkdate;
	}
	public Date getDat() {
		return dat;
	}
	public void setDat(Date dat) {
		this.dat = dat;
	}
	public String getWeek() {
		return week;
	}
	public void setWeek(String week) {
		this.week = week;
	}
	public List<SalePlan> getSalePlanList() {
		return SalePlanList;
	}
	public void setSalePlanList(List<SalePlan> salePlanList) {
		SalePlanList = salePlanList;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}

	public Date getStartdate() {
		return startdate;
	}
	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}
	public Date getEnddate() {
		return enddate;
	}
	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public double getPax1() {
		return pax1;
	}
	public void setPax1(double pax1) {
		this.pax1 = pax1;
	}
	public double getPax2() {
		return pax2;
	}
	public void setPax2(double pax2) {
		this.pax2 = pax2;
	}
	public double getPax3() {
		return pax3;
	}
	public void setPax3(double pax3) {
		this.pax3 = pax3;
	}
	public double getPax4() {
		return pax4;
	}
	public void setPax4(double pax4) {
		this.pax4 = pax4;
	}
	public double getPax1old() {
		return pax1old;
	}
	public void setPax1old(double pax1old) {
		this.pax1old = pax1old;
	}
	public double getPax2old() {
		return pax2old;
	}
	public void setPax2old(double pax2old) {
		this.pax2old = pax2old;
	}
	public double getPax3old() {
		return pax3old;
	}
	public void setPax3old(double pax3old) {
		this.pax3old = pax3old;
	}
	public double getPax4old() {
		return pax4old;
	}
	public void setPax4old(double pax4old) {
		this.pax4old = pax4old;
	}
	public Date getBdate() {
		return bdate;
	}
	public void setBdate(Date bdate) {
		this.bdate = bdate;
	}
	public Date getEdate() {
		return edate;
	}
	public void setEdate(Date edate) {
		this.edate = edate;
	}
	
	
}
