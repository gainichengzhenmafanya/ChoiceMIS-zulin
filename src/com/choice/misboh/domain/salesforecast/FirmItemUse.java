package com.choice.misboh.domain.salesforecast;

import java.util.Date;
import java.util.List;

/**
 * 菜品点击率
 * @author css
 *
 */
public class FirmItemUse {   

	private List<FirmItemUse> ItemUseList; //  修改用
	private List<String> firmList;   //分店号list
	private String firm;//分店号
	private String item;//菜品流水号
	private double cnt1;//工作日一班调整
	private double cnt2;//工作日二班调整
	private double cnt3;//工作日三班调整
	private double cnt4;//工作日四班调整
	private double cnt1old;//工作日一班计算
	private double cnt2old;//工作日二班计算
	private double cnt3old;//工作日三班计算
	private double cnt4old;//工作日四班计算
	private String itcode;//菜品编码
	private String itdes;//菜品名称
	private String itunit;//菜品单位
	private double hcnt1;//节假日一班调整
	private double hcnt2;//节假日二班调整
	private double hcnt3;//节假日三班调整
	private double hcnt4;//节假日四班调整
	private double hcnt1old;//节假日一班计算
	private double hcnt2old;//节假日二班计算
	private double hcnt3old;//节假日三班计算
	private double hcnt4old;//节假日四班计算
	
	private String bdat;//点击率计算开始日期
	private String edat;//点击率计算结束日期
	private double prev11;//周一早班预估值
	private double modv11;//周一早班调整值
	private double prev12;//周一中班预估值
	private double modv12;//周一中班调整值
	private double prev13;//周一晚班预估值
	private double modv13;//周一晚班调整值
	private double prev14;//周一夜班预估值
	private double modv14;//周一夜班调整值
	
	private double prev21;//周二
	private double modv21;
	private double prev22;
	private double modv22;
	private double prev23;
	private double modv23;
	private double prev24;
	private double modv24;
	
	private double prev31;//周三
	private double modv31;
	private double prev32;
	private double modv32;
	private double prev33;
	private double modv33;
	private double prev34;
	private double modv34;
	
	private double prev41;//周四
	private double modv41;
	private double prev42;
	private double modv42;
	private double prev43;
	private double modv43;
	private double prev44;
	private double modv44;

	private double prev51;//周五
	private double modv51;
	private double prev52;
	private double modv52;
	private double prev53;
	private double modv53;
	private double prev54;
	private double modv54;
	
	private double prev61;//周六
	private double modv61;
	private double prev62;
	private double modv62;
	private double prev63;
	private double modv63;
	private double prev64;
	private double modv64;
	
	private double prev71;//周日
	private double modv71;
	private double prev72;
	private double modv72;
	private double prev73;
	private double modv73;
	private double prev74;
	private double modv74;
	
	private double prev81;//节假日
	private double modv81;
	private double prev82;
	private double modv82;
	private double prev83;
	private double modv83;
	private double prev84;
	private double modv84;
	
	//需要添加的字段
	private double totlepx;//横向合计
	private double totlemx;//横向合计
	private String dept;//餐饮部门编码
	private String deptdes;//餐饮部门名称
	private String dept1;//SCM部门
	private String dept1des;//SCM部门
	
	private Date bdate;//点击率计算开始日期    日期类型  
	private Date edate;//点击率计算结束日期    日期类型  
	private Date dat;// 日期 
	private String acct;
	private String typ;//AMT 营业额    TC单数       PAX人数
	private String pax;//人数
	private String tc;//单数   
	private String amt;//营业额
	private String weekk;//周几  比如        周四  等于  5
	private String sft; //  餐次 
	private double cnt;//数量
	private String vplantyp;//预估方式
	
	public double getCnt() {
		return cnt;
	}
	public void setCnt(double cnt) {
		this.cnt = cnt;
	}
	public String getSft() {
		return sft;
	}
	public void setSft(String sft) {
		this.sft = sft;
	}
	public Date getDat() {
		return dat;
	}
	public void setDat(Date dat) {
		this.dat = dat;
	}
	public String getPax() {
		return pax;
	}
	public void setPax(String pax) {
		this.pax = pax;
	}
	public String getTc() {
		return tc;
	}
	public void setTc(String tc) {
		this.tc = tc;
	}
	public String getAmt() {
		return amt;
	}
	public void setAmt(String amt) {
		this.amt = amt;
	}
	public String getWeekk() {
		return weekk;
	}
	public void setWeekk(String weekk) {
		this.weekk = weekk;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public Date getBdate() {
		return bdate;
	}
	public void setBdate(Date bdate) {
		this.bdate = bdate;
	}
	public Date getEdate() {
		return edate;
	}
	public void setEdate(Date edate) {
		this.edate = edate;
	}
	public String getDeptdes() {
		return deptdes;
	}
	public void setDeptdes(String deptdes) {
		this.deptdes = deptdes;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getDept1() {
		return dept1;
	}
	public void setDept1(String dept1) {
		this.dept1 = dept1;
	}
	public String getDept1des() {
		return dept1des;
	}
	public void setDept1des(String dept1des) {
		this.dept1des = dept1des;
	}
	public String getBdat() {
		return bdat;
	}
	public void setBdat(String bdat) {
		this.bdat = bdat;
	}
	public String getEdat() {
		return edat;
	}
	public void setEdat(String edat) {
		this.edat = edat;
	}
	public double getPrev11() {
		return prev11;
	}
	public void setPrev11(double prev11) {
		this.prev11 = prev11;
	}
	public double getModv11() {
		return modv11;
	}
	public void setModv11(double modv11) {
		this.modv11 = modv11;
	}
	public double getPrev12() {
		return prev12;
	}
	public void setPrev12(double prev12) {
		this.prev12 = prev12;
	}
	public double getModv12() {
		return modv12;
	}
	public void setModv12(double modv12) {
		this.modv12 = modv12;
	}
	public double getPrev13() {
		return prev13;
	}
	public void setPrev13(double prev13) {
		this.prev13 = prev13;
	}
	public double getModv13() {
		return modv13;
	}
	public void setModv13(double modv13) {
		this.modv13 = modv13;
	}
	public double getPrev14() {
		return prev14;
	}
	public void setPrev14(double prev14) {
		this.prev14 = prev14;
	}
	public double getModv14() {
		return modv14;
	}
	public void setModv14(double modv14) {
		this.modv14 = modv14;
	}
	public double getPrev21() {
		return prev21;
	}
	public void setPrev21(double prev21) {
		this.prev21 = prev21;
	}
	public double getModv21() {
		return modv21;
	}
	public void setModv21(double modv21) {
		this.modv21 = modv21;
	}
	public double getPrev22() {
		return prev22;
	}
	public void setPrev22(double prev22) {
		this.prev22 = prev22;
	}
	public double getModv22() {
		return modv22;
	}
	public void setModv22(double modv22) {
		this.modv22 = modv22;
	}
	public double getPrev23() {
		return prev23;
	}
	public void setPrev23(double prev23) {
		this.prev23 = prev23;
	}
	public double getModv23() {
		return modv23;
	}
	public void setModv23(double modv23) {
		this.modv23 = modv23;
	}
	public double getPrev24() {
		return prev24;
	}
	public void setPrev24(double prev24) {
		this.prev24 = prev24;
	}
	public double getModv24() {
		return modv24;
	}
	public void setModv24(double modv24) {
		this.modv24 = modv24;
	}
	public double getPrev31() {
		return prev31;
	}
	public void setPrev31(double prev31) {
		this.prev31 = prev31;
	}
	public double getModv31() {
		return modv31;
	}
	public void setModv31(double modv31) {
		this.modv31 = modv31;
	}
	public double getPrev32() {
		return prev32;
	}
	public void setPrev32(double prev32) {
		this.prev32 = prev32;
	}
	public double getModv32() {
		return modv32;
	}
	public void setModv32(double modv32) {
		this.modv32 = modv32;
	}
	public double getPrev33() {
		return prev33;
	}
	public void setPrev33(double prev33) {
		this.prev33 = prev33;
	}
	public double getModv33() {
		return modv33;
	}
	public void setModv33(double modv33) {
		this.modv33 = modv33;
	}
	public double getPrev34() {
		return prev34;
	}
	public void setPrev34(double prev34) {
		this.prev34 = prev34;
	}
	public double getModv34() {
		return modv34;
	}
	public void setModv34(double modv34) {
		this.modv34 = modv34;
	}
	public double getPrev41() {
		return prev41;
	}
	public void setPrev41(double prev41) {
		this.prev41 = prev41;
	}
	public double getModv41() {
		return modv41;
	}
	public void setModv41(double modv41) {
		this.modv41 = modv41;
	}
	public double getPrev42() {
		return prev42;
	}
	public void setPrev42(double prev42) {
		this.prev42 = prev42;
	}
	public double getModv42() {
		return modv42;
	}
	public void setModv42(double modv42) {
		this.modv42 = modv42;
	}
	public double getPrev43() {
		return prev43;
	}
	public void setPrev43(double prev43) {
		this.prev43 = prev43;
	}
	public double getModv43() {
		return modv43;
	}
	public void setModv43(double modv43) {
		this.modv43 = modv43;
	}
	public double getPrev44() {
		return prev44;
	}
	public void setPrev44(double prev44) {
		this.prev44 = prev44;
	}
	public double getModv44() {
		return modv44;
	}
	public void setModv44(double modv44) {
		this.modv44 = modv44;
	}
	public double getPrev51() {
		return prev51;
	}
	public void setPrev51(double prev51) {
		this.prev51 = prev51;
	}
	public double getModv51() {
		return modv51;
	}
	public void setModv51(double modv51) {
		this.modv51 = modv51;
	}
	public double getPrev52() {
		return prev52;
	}
	public void setPrev52(double prev52) {
		this.prev52 = prev52;
	}
	public double getModv52() {
		return modv52;
	}
	public void setModv52(double modv52) {
		this.modv52 = modv52;
	}
	public double getPrev53() {
		return prev53;
	}
	public void setPrev53(double prev53) {
		this.prev53 = prev53;
	}
	public double getModv53() {
		return modv53;
	}
	public void setModv53(double modv53) {
		this.modv53 = modv53;
	}
	public double getPrev54() {
		return prev54;
	}
	public void setPrev54(double prev54) {
		this.prev54 = prev54;
	}
	public double getModv54() {
		return modv54;
	}
	public void setModv54(double modv54) {
		this.modv54 = modv54;
	}
	public double getPrev61() {
		return prev61;
	}
	public void setPrev61(double prev61) {
		this.prev61 = prev61;
	}
	public double getModv61() {
		return modv61;
	}
	public void setModv61(double modv61) {
		this.modv61 = modv61;
	}
	public double getPrev62() {
		return prev62;
	}
	public void setPrev62(double prev62) {
		this.prev62 = prev62;
	}
	public double getModv62() {
		return modv62;
	}
	public void setModv62(double modv62) {
		this.modv62 = modv62;
	}
	public double getPrev63() {
		return prev63;
	}
	public void setPrev63(double prev63) {
		this.prev63 = prev63;
	}
	public double getModv63() {
		return modv63;
	}
	public void setModv63(double modv63) {
		this.modv63 = modv63;
	}
	public double getPrev64() {
		return prev64;
	}
	public void setPrev64(double prev64) {
		this.prev64 = prev64;
	}
	public double getModv64() {
		return modv64;
	}
	public void setModv64(double modv64) {
		this.modv64 = modv64;
	}
	public double getPrev71() {
		return prev71;
	}
	public void setPrev71(double prev71) {
		this.prev71 = prev71;
	}
	public double getModv71() {
		return modv71;
	}
	public void setModv71(double modv71) {
		this.modv71 = modv71;
	}
	public double getPrev72() {
		return prev72;
	}
	public void setPrev72(double prev72) {
		this.prev72 = prev72;
	}
	public double getModv72() {
		return modv72;
	}
	public void setModv72(double modv72) {
		this.modv72 = modv72;
	}
	public double getPrev73() {
		return prev73;
	}
	public void setPrev73(double prev73) {
		this.prev73 = prev73;
	}
	public double getModv73() {
		return modv73;
	}
	public void setModv73(double modv73) {
		this.modv73 = modv73;
	}
	public double getPrev74() {
		return prev74;
	}
	public void setPrev74(double prev74) {
		this.prev74 = prev74;
	}
	public double getModv74() {
		return modv74;
	}
	public void setModv74(double modv74) {
		this.modv74 = modv74;
	}
	public double getPrev81() {
		return prev81;
	}
	public void setPrev81(double prev81) {
		this.prev81 = prev81;
	}
	public double getModv81() {
		return modv81;
	}
	public void setModv81(double modv81) {
		this.modv81 = modv81;
	}
	public double getPrev82() {
		return prev82;
	}
	public void setPrev82(double prev82) {
		this.prev82 = prev82;
	}
	public double getModv82() {
		return modv82;
	}
	public void setModv82(double modv82) {
		this.modv82 = modv82;
	}
	public double getPrev83() {
		return prev83;
	}
	public void setPrev83(double prev83) {
		this.prev83 = prev83;
	}
	public double getModv83() {
		return modv83;
	}
	public void setModv83(double modv83) {
		this.modv83 = modv83;
	}
	public double getPrev84() {
		return prev84;
	}
	public void setPrev84(double prev84) {
		this.prev84 = prev84;
	}
	public double getModv84() {
		return modv84;
	}
	public void setModv84(double modv84) {
		this.modv84 = modv84;
	}
	public double getTotlepx() {
		return totlepx;
	}
	public void setTotlepx(double totlepx) {
		this.totlepx = totlepx;
	}
	public double getTotlemx() {
		return totlemx;
	}
	public void setTotlemx(double totlemx) {
		this.totlemx = totlemx;
	}
	public List<String> getFirmList() {
		return firmList;
	}
	public void setFirmList(List<String> firmList) {
		this.firmList = firmList;
	}
	public List<FirmItemUse> getItemUseList() {
		return ItemUseList;
	}
	public void setItemUseList(List<FirmItemUse> itemUseList) {
		ItemUseList = itemUseList;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public double getCnt1() {
		return cnt1;
	}
	public void setCnt1(double cnt1) {
		this.cnt1 = cnt1;
	}
	public double getCnt2() {
		return cnt2;
	}
	public void setCnt2(double cnt2) {
		this.cnt2 = cnt2;
	}
	public double getCnt3() {
		return cnt3;
	}
	public void setCnt3(double cnt3) {
		this.cnt3 = cnt3;
	}
	public double getCnt4() {
		return cnt4;
	}
	public void setCnt4(double cnt4) {
		this.cnt4 = cnt4;
	}
	public double getCnt1old() {
		return cnt1old;
	}
	public void setCnt1old(double cnt1old) {
		this.cnt1old = cnt1old;
	}
	public double getCnt2old() {
		return cnt2old;
	}
	public void setCnt2old(double cnt2old) {
		this.cnt2old = cnt2old;
	}
	public double getCnt3old() {
		return cnt3old;
	}
	public void setCnt3old(double cnt3old) {
		this.cnt3old = cnt3old;
	}
	public double getCnt4old() {
		return cnt4old;
	}
	public void setCnt4old(double cnt4old) {
		this.cnt4old = cnt4old;
	}
	public String getItcode() {
		return itcode;
	}
	public void setItcode(String itcode) {
		this.itcode = itcode;
	}
	public String getItdes() {
		return itdes;
	}
	public void setItdes(String itdes) {
		this.itdes = itdes;
	}
	public String getItunit() {
		return itunit;
	}
	public void setItunit(String itunit) {
		this.itunit = itunit;
	}
	public double getHcnt1() {
		return hcnt1;
	}
	public void setHcnt1(double hcnt1) {
		this.hcnt1 = hcnt1;
	}
	public double getHcnt2() {
		return hcnt2;
	}
	public void setHcnt2(double hcnt2) {
		this.hcnt2 = hcnt2;
	}
	public double getHcnt3() {
		return hcnt3;
	}
	public void setHcnt3(double hcnt3) {
		this.hcnt3 = hcnt3;
	}
	public double getHcnt4() {
		return hcnt4;
	}
	public void setHcnt4(double hcnt4) {
		this.hcnt4 = hcnt4;
	}
	public double getHcnt1old() {
		return hcnt1old;
	}
	public void setHcnt1old(double hcnt1old) {
		this.hcnt1old = hcnt1old;
	}
	public double getHcnt2old() {
		return hcnt2old;
	}
	public void setHcnt2old(double hcnt2old) {
		this.hcnt2old = hcnt2old;
	}
	public double getHcnt3old() {
		return hcnt3old;
	}
	public void setHcnt3old(double hcnt3old) {
		this.hcnt3old = hcnt3old;
	}
	public double getHcnt4old() {
		return hcnt4old;
	}
	public void setHcnt4old(double hcnt4old) {
		this.hcnt4old = hcnt4old;
	}

	public String getVplantyp() {
		return vplantyp;
	}

	public void setVplantyp(String vplantyp) {
		this.vplantyp = vplantyp;
	}
}
