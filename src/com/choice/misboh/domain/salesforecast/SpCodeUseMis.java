package com.choice.misboh.domain.salesforecast;

import java.util.Date;
import java.util.List;

import com.choice.misboh.domain.costReduction.MisSupply;

/**
 * 物资使用
 * @author 
 *
 */
public class SpCodeUseMis {

	private String acct;//帐套
	private String yearr;//年份
	private String firm;   	//分店号
	private Date bdat;   	//开始日期/报货日期
	private Date edat;   	//结束日期/下次到货日期
	private String sp_code;	//物资编码
	private String sp_name;	//物资名称
	private String sp_desc;	//物资规格
	private String unit;	//物资单位
	private String unitper;	//单位转化率
	private String mincnt="0"; //最小申购量
	private Double sp_price=1.0;  //标准价
	private double cnt;	   	//数量
	private double cntzt;	//在途数量
	private double lv;	   	//数量
	private double amount;	   	//数量
	private double amt;	   	//金额
	private double pax;	   	//人数
	private int tc;//单数
	private Date dat;   	//日期
//	private double cnt1;	   	//数量1
	private double cntold;	//数量调整值
//	private double cnt1old;	//数量1调整值
	private String memo; 	//备注
	private List<SpCodeUseMis> spCodeUseList;
	private Double total; //预估合计
	private MisSupply misSupply;//物资
	private Double forecastcnt;//预估数量
	private Double prorequi;//采购需求
	private String grptyp;	//报货类别
	private Date ddat;   	//到货日期
	private double safetystock;//安全系数
	
	private String ynUseDept;//是否启用档口
	private String dept;//部门
	private String deptdes;
	private String typ;//预估用 报货类别
	private String vplantyp;//千人/万元/还是千次
	private String change;//是否
	
	public String getChange() {
		return change;
	}
	public void setChange(String change) {
		this.change = change;
	}
	public String getVplantyp() {
		return vplantyp;
	}
	public void setVplantyp(String vplantyp) {
		this.vplantyp = vplantyp;
	}
	public String getYnUseDept() {
		return ynUseDept;
	}
	public void setYnUseDept(String ynUseDept) {
		this.ynUseDept = ynUseDept;
	}
	public String getDeptdes() {
		return deptdes;
	}
	public void setDeptdes(String deptdes) {
		this.deptdes = deptdes;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public double getCntzt() {
		return cntzt;
	}
	public void setCntzt(double cntzt) {
		this.cntzt = cntzt;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getYearr() {
		return yearr;
	}
	public void setYearr(String yearr) {
		this.yearr = yearr;
	}
	public double getSafetystock() {
		return safetystock;
	}
	public void setSafetystock(double safetystock) {
		this.safetystock = safetystock;
	}
	public Date getDdat() {
		return ddat;
	}
	public void setDdat(Date ddat) {
		this.ddat = ddat;
	}
	public String getGrptyp() {
		return grptyp;
	}
	public void setGrptyp(String grptyp) {
		this.grptyp = grptyp;
	}
	public String getUnitper() {
		return unitper;
	}
	public void setUnitper(String unitper) {
		this.unitper = unitper;
	}
	public String getMincnt() {
		return mincnt;
	}
	public void setMincnt(String mincnt) {
		this.mincnt = mincnt;
	}
	public Double getSp_price() {
		return sp_price;
	}
	public void setSp_price(Double sp_price) {
		this.sp_price = sp_price;
	}
	public String getSp_desc() {
		return sp_desc;
	}
	public void setSp_desc(String sp_desc) {
		this.sp_desc = sp_desc;
	}
	public List<SpCodeUseMis> getSpCodeUseList() {
		return spCodeUseList;
	}
	public void setSpCodeUseList(List<SpCodeUseMis> spCodeUseList) {
		this.spCodeUseList = spCodeUseList;
	}
	public double getPax() {
		return pax;
	}
	public void setPax(double pax) {
		this.pax = pax;
	}
	public int getTc() {
		return tc;
	}
	public void setTc(int tc) {
		this.tc = tc;
	}
	public double getLv() {
		return lv;
	}
	public void setLv(double lv) {
		this.lv = lv;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getAmt() {
		return amt;
	}
	public void setAmt(double amt) {
		this.amt = amt;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public Date getBdat() {
		return bdat;
	}
	public void setBdat(Date bdat) {
		this.bdat = bdat;
	}
	public Date getEdat() {
		return edat;
	}
	public void setEdat(Date edat) {
		this.edat = edat;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public double getCnt() {
		return cnt;
	}
	public void setCnt(double cnt) {
		this.cnt = cnt;
	}
	public Date getDat() {
		return dat;
	}
	public void setDat(Date dat) {
		this.dat = dat;
	}
	public double getCntold() {
		return cntold;
	}
	public void setCntold(double cntold) {
		this.cntold = cntold;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public MisSupply getMisSupply() {
		return misSupply;
	}
	public void setMisSupply(MisSupply misSupply) {
		this.misSupply = misSupply;
	}
	public Double getForecastcnt() {
		return forecastcnt;
	}
	public void setForecastcnt(Double forecastcnt) {
		this.forecastcnt = forecastcnt;
	}
	public Double getProrequi() {
		return prorequi;
	}
	public void setProrequi(Double prorequi) {
		this.prorequi = prorequi;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	
}
