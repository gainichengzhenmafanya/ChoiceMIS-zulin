package com.choice.misboh.domain.common;

/**
 * 描述：公共实体类
 * @author 马振
 * 创建时间：2015-4-16 下午4:59:07
 */
public class CommonMethod {
	
	private String vcode;		// 编码值
	private String vcodename;	// 编码字段名称
	private String vtablename;	// 表名称
	private String isortno;		// 排序列字段名称
	private String visortnoName;// 排序字段名称
	private String enablestate;	// 2启用，3禁用
	private String ids;			// 设置是否启用的id数据集合
	private String pk_id;		// id字段名称
	private String pk_id_name;	// 主键名称
	private String pk_store;	// 门店主键
	private String pk_team;		// 班组主键
	private String modulename;	// 模块名称 加日志用到
	private String querysql; 	// 查询的条件
	private String icode;		// 编码
	private String callBack;
	private String domId;
	private String single;
	private String vpacktype;	//套餐类型
	
	private String vname;
	private String vinit;
	private String vmemo;		//备注
	
	private String pk_packagetype;	//套餐类型主键
	
	private String pk_shiftsft;		//班次主键
	
	private String pk_groupdept;	//集团部门主键
	private String pk_balatype;		//科目
	private String sign = "0";		//标识是否有门店部门选择了相应的集团部门（0：标识未选 1：标识已选）
	private String iproportion;		//档口比例分成
	private String istatistic;		//部门属性
	
	private String pk_role;			//门店角色主键
	private String vfoodsign;		//门店餐饮类型
	private String pk_paymode;
	
	public String getPk_id_name() {
		return pk_id_name;
	}
	public void setPk_id_name(String pk_id_name) {
		this.pk_id_name = pk_id_name;
	}
	public String getVisortnoName() {
		return visortnoName;
	}
	public void setVisortnoName(String visortnoName) {
		this.visortnoName = visortnoName;
	}
	public String getIsortno() {
		return isortno;
	}
	public void setIsortno(String isortno) {
		this.isortno = isortno;
	}
	public String getVcodename() {
		return vcodename;
	}
	public void setVcodename(String vcodename) {
		this.vcodename = vcodename;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVtablename() {
		return vtablename;
	}
	public void setVtablename(String vtablename) {
		this.vtablename = vtablename;
	}
	/**
	 * @return enablestate
	 */
	public String getEnablestate() {
		return enablestate;
	}
	/**
	 * @param enablestate 要设置的 enablestate
	 */
	public void setEnablestate(String enablestate) {
		this.enablestate = enablestate;
	}
	/**
	 * @return ids
	 */
	public String getIds() {
		return ids;
	}
	/**
	 * @param ids 要设置的 ids
	 */
	public void setIds(String ids) {
		this.ids = ids;
	}
	/**
	 * @return pk_id
	 */
	public String getPk_id() {
		return pk_id;
	}
	/**
	 * @param pk_id 要设置的 pk_id
	 */
	public void setPk_id(String pk_id) {
		this.pk_id = pk_id;
	}
	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
	public String getModulename() {
		return modulename;
	}
	public void setModulename(String modulename) {
		this.modulename = modulename;
	}
	public String getQuerysql() {
		return querysql;
	}
	public void setQuerysql(String querysql) {
		this.querysql = querysql;
	}
	public String getIcode() {
		return icode;
	}
	public void setIcode(String icode) {
		this.icode = icode;
	}
	public String getCallBack() {
		return callBack;
	}
	public void setCallBack(String callBack) {
		this.callBack = callBack;
	}
	public String getDomId() {
		return domId;
	}
	public void setDomId(String domId) {
		this.domId = domId;
	}
	public String getSingle() {
		return single;
	}
	public void setSingle(String single) {
		this.single = single;
	}
	public String getPk_team() {
		return pk_team;
	}
	public void setPk_team(String pk_team) {
		this.pk_team = pk_team;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getVinit() {
		return vinit;
	}
	public void setVinit(String vinit) {
		this.vinit = vinit;
	}
	public String getPk_shiftsft() {
		return pk_shiftsft;
	}
	public void setPk_shiftsft(String pk_shiftsft) {
		this.pk_shiftsft = pk_shiftsft;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public String getPk_packagetype() {
		return pk_packagetype;
	}
	public void setPk_packagetype(String pk_packagetype) {
		this.pk_packagetype = pk_packagetype;
	}
	public String getVpacktype() {
		return vpacktype;
	}
	public void setVpacktype(String vpacktype) {
		this.vpacktype = vpacktype;
	}
	public String getPk_groupdept() {
		return pk_groupdept;
	}
	public void setPk_groupdept(String pk_groupdept) {
		this.pk_groupdept = pk_groupdept;
	}
	public String getPk_balatype() {
		return pk_balatype;
	}
	public void setPk_balatype(String pk_balatype) {
		this.pk_balatype = pk_balatype;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getIproportion() {
		return iproportion;
	}
	public void setIproportion(String iproportion) {
		this.iproportion = iproportion;
	}
	public String getIstatistic() {
		return istatistic;
	}
	public void setIstatistic(String istatistic) {
		this.istatistic = istatistic;
	}
	public String getVfoodsign() {
		return vfoodsign;
	}
	public void setVfoodsign(String vfoodsign) {
		this.vfoodsign = vfoodsign;
	}
	public String getPk_role() {
		return pk_role;
	}
	public void setPk_role(String pk_role) {
		this.pk_role = pk_role;
	}
	public String getPk_paymode() {
		return pk_paymode;
	}
	public void setPk_paymode(String pk_paymode) {
		this.pk_paymode = pk_paymode;
	}
}