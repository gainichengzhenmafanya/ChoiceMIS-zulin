package com.choice.misboh.domain.chkstom;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.choice.misboh.domain.declareGoodsGuide.Declare;
import com.choice.misboh.domain.salesforecast.SalePlan;

/**
 * 报货向导
 * @author wjf
 *
 */
public class ChkstomForecast implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2268208640486764563L;
	private String acct; //帐套号
	private String yearr; //年度
	
	private Date maded; //填制日期
	private String dept; //申购的部门编号
	private String deptName;//部门名称
	private String deptboh;//boh的部门编码
	private String firm; //申购分店
	
	//报货向导用
	private int sta;//0报货向导 1千元用量  2估清 3安全库存 4周期平均
	private String isdept;//是否使用档口
	private String isuseschedule;//是否使用配送班表
	private String vplantyp;
	private String vfoodsign;
	private String orderDayControl;//是否控制到货日
	private String codetyp;//报货类型  1千元用量 2直接填写申购单 3报货模板 4安全库存6菜品销售计划  8周期平均报货                           5千人用量    7菜品点击率
	private String dailyGoodsCnt;//每日生成的报货单数
	
	//报货向导左侧
	private String name;
	private String url;
	private int step;//-1是上一步 1是下一步
	
	private List<ScheduleD> sdList;//存放选择的报货类别
	
	private List<SalePlan> salePlanList;//存放预估值
	
	private String bdate;//开始日期  条件
	private String edate;//结束日期
	private List<Declare> qyylList;//千元用量
	
	private String isDistributionUnit;//是否使用配送单位
	
	private String qyylCalWay;//千元用量类是否最终计算量= 周期用量 +安全库存 -当前库存-在途 
	private String xsjhCalWay;//菜品销售计划类是否最终计算量= 周期用量 +安全库存 -当前库存-在途 
	
	public ChkstomForecast(){}
	
	public ChkstomForecast(String name,String url){
		this.name = name;
		this.url = url;
	}
	
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getYearr() {
		return yearr;
	}
	public void setYearr(String yearr) {
		this.yearr = yearr;
	}
	public Date getMaded() {
		return maded;
	}
	public void setMaded(Date maded) {
		this.maded = maded;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public String getDeptboh() {
		return deptboh;
	}
	public void setDeptboh(String deptboh) {
		this.deptboh = deptboh;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public int getSta() {
		return sta;
	}
	public void setSta(int sta) {
		this.sta = sta;
	}
	public String getIsdept() {
		return isdept;
	}
	public void setIsdept(String isdept) {
		this.isdept = isdept;
	}
	public String getIsuseschedule() {
		return isuseschedule;
	}
	public void setIsuseschedule(String isuseschedule) {
		this.isuseschedule = isuseschedule;
	}
	public String getOrderDayControl() {
		return orderDayControl;
	}
	public void setOrderDayControl(String orderDayControl) {
		this.orderDayControl = orderDayControl;
	}
	public String getCodetyp() {
		return codetyp;
	}
	public void setCodetyp(String codetyp) {
		this.codetyp = codetyp;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

	public int getStep() {
		return step;
	}

	public void setStep(int step) {
		this.step = step;
	}

	public String getDailyGoodsCnt() {
		return dailyGoodsCnt;
	}

	public void setDailyGoodsCnt(String dailyGoodsCnt) {
		this.dailyGoodsCnt = dailyGoodsCnt;
	}

	public List<ScheduleD> getSdList() {
		return sdList;
	}

	public void setSdList(List<ScheduleD> sdList) {
		this.sdList = sdList;
	}

	public String getVplantyp() {
		return vplantyp;
	}

	public void setVplantyp(String vplantyp) {
		this.vplantyp = vplantyp;
	}

	public String getVfoodsign() {
		return vfoodsign;
	}

	public void setVfoodsign(String vfoodsign) {
		this.vfoodsign = vfoodsign;
	}

	public List<SalePlan> getSalePlanList() {
		return salePlanList;
	}

	public void setSalePlanList(List<SalePlan> salePlanList) {
		this.salePlanList = salePlanList;
	}

	public String getBdate() {
		return bdate;
	}

	public void setBdate(String bdate) {
		this.bdate = bdate;
	}

	public String getEdate() {
		return edate;
	}

	public void setEdate(String edate) {
		this.edate = edate;
	}

	public List<Declare> getQyylList() {
		return qyylList;
	}

	public void setQyylList(List<Declare> qyylList) {
		this.qyylList = qyylList;
	}

	public String getIsDistributionUnit() {
		return isDistributionUnit;
	}

	public void setIsDistributionUnit(String isDistributionUnit) {
		this.isDistributionUnit = isDistributionUnit;
	}

	public String getQyylCalWay() {
		return qyylCalWay;
	}

	public void setQyylCalWay(String qyylCalWay) {
		this.qyylCalWay = qyylCalWay;
	}

	public String getXsjhCalWay() {
		return xsjhCalWay;
	}

	public void setXsjhCalWay(String xsjhCalWay) {
		this.xsjhCalWay = xsjhCalWay;
	}

}
