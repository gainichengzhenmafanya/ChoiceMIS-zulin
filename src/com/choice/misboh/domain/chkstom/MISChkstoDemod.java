package com.choice.misboh.domain.chkstom;

import java.util.Date;

import com.choice.scm.domain.Supply;

/**
 * 报货单单据模板子表
 * @author 孙胜彬
 */
public class MISChkstoDemod {
	private String acct;//帐套号
	private String yearr;//年度
	private Integer id;//主键
	private Integer chkstodemono;//主表主键
	private String sp_code;//物资编码
	private Supply supply;//物资
	private Double price;//单价
	private Double amount;//数量
	private String memo;//备注
	private Date ind;//到货日期
	private Double pricesale;
	private String chkstodemonos;
	private String scode;
	//wangjie 2014年10月23日 10:35:22
	private double reference_amount;//参考单位数量
	
	private Double totleMoney;//合计金额----用于模板子表的查询
	private String ynsto;//是否可报货条件  报货单填制 选模板的时候用 wjf
	private String typ_eas;//报货类别  报货向导用
	
	private double stomin;
	private double stocnt;
	
	private String disunit;//配送单位
	private double disunitper;//配送单位转换率
	private double dismincnt;//配送单位最小申购量
	
	public String getScode() {
		return scode;
	}
	public void setScode(String scode) {
		this.scode = scode;
	}
	public double getReference_amount() {
		return reference_amount;
	}
	public void setReference_amount(double reference_amount) {
		this.reference_amount = reference_amount;
	}
	public Double getTotleMoney() {
		return totleMoney;
	}
	public void setTotleMoney(Double totleMoney) {
		this.totleMoney = totleMoney;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getYearr() {
		return yearr;
	}
	public void setYearr(String yearr) {
		this.yearr = yearr;
	}
	public Integer getChkstodemono() {
		return chkstodemono;
	}
	public void setChkstodemono(Integer chkstodemono) {
		this.chkstodemono = chkstodemono;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public Date getInd() {
		return ind;
	}
	public void setInd(Date ind) {
		this.ind = ind;
	}
	public Double getPricesale() {
		return pricesale;
	}
	public void setPricesale(Double pricesale) {
		this.pricesale = pricesale;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Supply getSupply() {
		return supply;
	}
	public void setSupply(Supply supply) {
		this.supply = supply;
	}
	public String getChkstodemonos() {
		return chkstodemonos;
	}
	public void setChkstodemonos(String chkstodemonos) {
		this.chkstodemonos = chkstodemonos;
	}
	public String getYnsto() {
		return ynsto;
	}
	public void setYnsto(String ynsto) {
		this.ynsto = ynsto;
	}
	public String getTyp_eas() {
		return typ_eas;
	}
	public void setTyp_eas(String typ_eas) {
		this.typ_eas = typ_eas;
	}
	public double getStomin() {
		return stomin;
	}
	public void setStomin(double stomin) {
		this.stomin = stomin;
	}
	public double getStocnt() {
		return stocnt;
	}
	public void setStocnt(double stocnt) {
		this.stocnt = stocnt;
	}
	public String getDisunit() {
		return disunit;
	}
	public void setDisunit(String disunit) {
		this.disunit = disunit;
	}
	public double getDisunitper() {
		return disunitper;
	}
	public void setDisunitper(double disunitper) {
		this.disunitper = disunitper;
	}
	public double getDismincnt() {
		return dismincnt;
	}
	public void setDismincnt(double dismincnt) {
		this.dismincnt = dismincnt;
	}
	
}
