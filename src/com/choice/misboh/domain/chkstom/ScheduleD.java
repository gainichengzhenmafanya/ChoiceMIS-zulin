package com.choice.misboh.domain.chkstom;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.choice.misboh.domain.declareGoodsGuide.Declare;



/**
 * 配送班表 从
 *
 */
public class ScheduleD implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4637205781245832652L;
	private Integer scheduleDetailsID;		//id	
	private String category_Code;			//物资一级类别
	private String category;				//类别名称
	private Integer scheduleID;				//配送班id
	private String orderDate;					//订货日期
	private String orderDate_NY;					//订货日期
	private String orderTime;					//订货时间
	private String orderWeekday;			//周次
	private String orderColor;				//显示颜色
	private String receiveDate;				//到货日期
	private String receiveDate_NY;				//到货日期
	private String receiveTime;				//到货时间
	private String receiveWeekday;			//周次
	private String receiveColor;			//到货颜色
	private String positnCode;//门店编码
	
	//报货向导用
	private String des;
	private Integer codetyp;
	private Integer days;
	private String nextReceiveDate;
	private String firm;
	private String dept;
	private String yearr;
	private Date maded;//报货日
	private Date maded3;//前三周
	private Date maded2;//上上周
	private Date maded1;//上周
	
	private Integer chkstono;//报货单号
	private List<Declare> declareList;
	
	
	public String getPositnCode() {
		return positnCode;
	}
	public void setPositnCode(String positnCode) {
		this.positnCode = positnCode;
	}
	public String getOrderDate_NY() {
		return orderDate_NY;
	}
	public void setOrderDate_NY(String orderDate_NY) {
		this.orderDate_NY = orderDate_NY;
	}
	public String getReceiveDate_NY() {
		return receiveDate_NY;
	}
	public void setReceiveDate_NY(String receiveDate_NY) {
		this.receiveDate_NY = receiveDate_NY;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Integer getScheduleDetailsID() {
		return scheduleDetailsID;
	}
	public void setScheduleDetailsID(Integer scheduleDetailsID) {
		this.scheduleDetailsID = scheduleDetailsID;
	}
	public String getCategory_Code() {
		return category_Code;
	}
	public void setCategory_Code(String category_Code) {
		this.category_Code = category_Code;
	}
	public Integer getScheduleID() {
		return scheduleID;
	}
	public void setScheduleID(Integer scheduleID) {
		this.scheduleID = scheduleID;
	}
	public String getOrderWeekday() {
		return orderWeekday;
	}
	public void setOrderWeekday(String orderWeekday) {
		this.orderWeekday = orderWeekday;
	}
	public String getOrderColor() {
		return orderColor;
	}
	public void setOrderColor(String orderColor) {
		this.orderColor = orderColor;
	}
	public String getReceiveWeekday() {
		return receiveWeekday;
	}
	public void setReceiveWeekday(String receiveWeekday) {
		this.receiveWeekday = receiveWeekday;
	}
	public String getReceiveColor() {
		return receiveColor;
	}
	public void setReceiveColor(String receiveColor) {
		this.receiveColor = receiveColor;
	}
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public String getOrderTime() {
		return orderTime;
	}
	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}
	public String getReceiveDate() {
		return receiveDate;
	}
	public void setReceiveDate(String receiveDate) {
		this.receiveDate = receiveDate;
	}
	public String getReceiveTime() {
		return receiveTime;
	}
	public void setReceiveTime(String receiveTime) {
		this.receiveTime = receiveTime;
	}
	public String getDes() {
		return des;
	}
	public void setDes(String des) {
		this.des = des;
	}
	public Integer getCodetyp() {
		return codetyp;
	}
	public void setCodetyp(Integer codetyp) {
		this.codetyp = codetyp;
	}
	public Integer getDays() {
		return days;
	}
	public void setDays(Integer days) {
		this.days = days;
	}
	public String getNextReceiveDate() {
		return nextReceiveDate;
	}
	public void setNextReceiveDate(String nextReceiveDate) {
		this.nextReceiveDate = nextReceiveDate;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getYearr() {
		return yearr;
	}
	public void setYearr(String yearr) {
		this.yearr = yearr;
	}
	public List<Declare> getDeclareList() {
		return declareList;
	}
	public void setDeclareList(List<Declare> declareList) {
		this.declareList = declareList;
	}
	public Date getMaded() {
		return maded;
	}
	public void setMaded(Date maded) {
		this.maded = maded;
	}
	public Date getMaded3() {
		return maded3;
	}
	public void setMaded3(Date maded3) {
		this.maded3 = maded3;
	}
	public Date getMaded2() {
		return maded2;
	}
	public void setMaded2(Date maded2) {
		this.maded2 = maded2;
	}
	public Date getMaded1() {
		return maded1;
	}
	public void setMaded1(Date maded1) {
		this.maded1 = maded1;
	}
	public Integer getChkstono() {
		return chkstono;
	}
	public void setChkstono(Integer chkstono) {
		this.chkstono = chkstono;
	}
	 
}
