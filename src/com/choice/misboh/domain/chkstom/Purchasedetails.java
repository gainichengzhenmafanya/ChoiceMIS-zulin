package com.choice.misboh.domain.chkstom;

/***
 * 供应商邮件
 * @author wjf
 *
 */
public class Purchasedetails {
	
	//数据库列
	private String vouno  ;
	private String jdeno  ;
	private String pddcto ;
	private int rowno  ;
	private String sp_code;
	private String sp_name;
	private double amount ;
	private String unit   ;
	private String firm   ;
	private String pdnxtr ;
	private String pdlttr ;
	private String dat    ;
	private String ind    ;
	private String madat  ;
	private String madad  ;
	private String deliver;
	
	//查询条件
	private String bdate;
	private String edate;
	private String deliverDes;
	private String sp_desc;
	public String getVouno() {
		return vouno;
	}
	public void setVouno(String vouno) {
		this.vouno = vouno;
	}
	public String getJdeno() {
		return jdeno;
	}
	public void setJdeno(String jdeno) {
		this.jdeno = jdeno;
	}
	public String getPddcto() {
		return pddcto;
	}
	public void setPddcto(String pddcto) {
		this.pddcto = pddcto;
	}
	public int getRowno() {
		return rowno;
	}
	public void setRowno(int rowno) {
		this.rowno = rowno;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public String getPdnxtr() {
		return pdnxtr;
	}
	public void setPdnxtr(String pdnxtr) {
		this.pdnxtr = pdnxtr;
	}
	public String getPdlttr() {
		return pdlttr;
	}
	public void setPdlttr(String pdlttr) {
		this.pdlttr = pdlttr;
	}
	public String getDat() {
		return dat;
	}
	public void setDat(String dat) {
		this.dat = dat;
	}
	public String getInd() {
		return ind;
	}
	public void setInd(String ind) {
		this.ind = ind;
	}
	public String getMadat() {
		return madat;
	}
	public void setMadat(String madat) {
		this.madat = madat;
	}
	public String getMadad() {
		return madad;
	}
	public void setMadad(String madad) {
		this.madad = madad;
	}
	public String getDeliver() {
		return deliver;
	}
	public void setDeliver(String deliver) {
		this.deliver = deliver;
	}
	public String getBdate() {
		return bdate;
	}
	public void setBdate(String bdate) {
		this.bdate = bdate;
	}
	public String getEdate() {
		return edate;
	}
	public void setEdate(String edate) {
		this.edate = edate;
	}
	public String getDeliverDes() {
		return deliverDes;
	}
	public void setDeliverDes(String deliverDes) {
		this.deliverDes = deliverDes;
	}
	public String getSp_desc() {
		return sp_desc;
	}
	public void setSp_desc(String sp_desc) {
		this.sp_desc = sp_desc;
	}
	
}
