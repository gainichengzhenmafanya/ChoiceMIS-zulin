package com.choice.misboh.domain.chkstom;

import java.util.Date;
import java.util.List;
/**
 * 报货单单据模板主表
 * @author 孙胜彬
 */
public class MISChkstoDemom {
	private String acct;//帐套号
	private String yearr;//年度
	private Integer chkstodemono;//主键
	private Date maded;//填制日期
	private String madet;//时间 
	private Date checd;//审核日期
	private Date chect;//审核时间
	private String madeby;//填单人
	private String checby;//审核人
	private String vouno;//凭证号 ，规则算出的 一般为年度日期-001
	private String title;
	private Double totalamt;//合计金额 明细数据的合计金额
	private String status;//状态
	private String bak1;//
	private Integer bak2;//
	private String chectim;//审核时间
	private String memo;//备注
	private List<MISChkstoDemod> chkstoDemod;//报货单从表
	private String firm;//使用分店   wjf
	private String typ;//类型 标记模板是谁加的  N表示总部加的，Y表示门店加的 默认N
	private String tempTyp;//模板类型 BH报货单模板  CK出库模板
	private String typ_eas;//报货类型
	private String deliver;//供应商 入库单模板选择时用
	
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getYearr() {
		return yearr;
	}
	public void setYearr(String yearr) {
		this.yearr = yearr;
	}
	public Integer getChkstodemono() {
		return chkstodemono;
	}
	public void setChkstodemono(Integer chkstodemono) {
		this.chkstodemono = chkstodemono;
	}
	public Date getMaded() {
		return maded;
	}
	public void setMaded(Date maded) {
		this.maded = maded;
	}
	public String getMadet() {
		return madet;
	}
	public void setMadet(String madet) {
		this.madet = madet;
	}
	public Date getChecd() {
		return checd;
	}
	public void setChecd(Date checd) {
		this.checd = checd;
	}
	public Date getChect() {
		return chect;
	}
	public void setChect(Date chect) {
		this.chect = chect;
	}
	public String getMadeby() {
		return madeby;
	}
	public void setMadeby(String madeby) {
		this.madeby = madeby;
	}
	public String getChecby() {
		return checby;
	}
	public void setChecby(String checby) {
		this.checby = checby;
	}
	public String getVouno() {
		return vouno;
	}
	public void setVouno(String vouno) {
		this.vouno = vouno;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Double getTotalamt() {
		return totalamt;
	}
	public void setTotalamt(Double totalamt) {
		this.totalamt = totalamt;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getBak1() {
		return bak1;
	}
	public void setBak1(String bak1) {
		this.bak1 = bak1;
	}
	public Integer getBak2() {
		return bak2;
	}
	public void setBak2(Integer bak2) {
		this.bak2 = bak2;
	}
	public String getChectim() {
		return chectim;
	}
	public void setChectim(String chectim) {
		this.chectim = chectim;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public List<MISChkstoDemod> getChkstoDemod() {
		return chkstoDemod;
	}
	public void setChkstoDemod(List<MISChkstoDemod> chkstoDemod) {
		this.chkstoDemod = chkstoDemod;
	}
	public String getTempTyp() {
		return tempTyp;
	}
	public void setTempTyp(String tempTyp) {
		this.tempTyp = tempTyp;
	}
	public String getTyp_eas() {
		return typ_eas;
	}
	public void setTyp_eas(String typ_eas) {
		this.typ_eas = typ_eas;
	}
	public String getDeliver() {
		return deliver;
	}
	public void setDeliver(String deliver) {
		this.deliver = deliver;
	} 
	
}
