package com.choice.misboh.domain.declareGoodsGuide;

import java.io.Serializable;

public class Declare implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2655922892262623321L;
	private String sp_code;
	private String sp_name;
	private String unit;
	private String sp_desc;
	private String sp_unit;
	private double unitRate_x;
	private double unitRate;
	private double estimatedParameters;
	private double periodUse;
	private String dailyGoods_Typ;
	private double forecastMoney;
	private double tcurstore;
	private double orderGoods;
	private double orderGoodsCount;
	private double standardCount;
	private int editorCount = 0;
	
	private double mincnt;//最小宝货量
	private double spmin;//最低库存
	private double spnow;//当前库存
	private double spway;//在途
	private double spcal;//计算量
	private double spuse;//需求量
	private String unit3;//采购单位
	private double unitper3;//采购单位转换率
	private String typ_eas;//物资类别
	private String memo;//备注
	
	private double stomin;//最小申购量
	private double stocnt;//申购倍数
	
	private String disunit;//配送单位
	private double disunitper;//配送单位转换率
	private double dismincnt;//配送单位最小申购量
	private double amountdis;//配送单位数量
	
	private String positn;//报货仓位 因为档口可以多选
	private String positndes;
	
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public double getEstimatedParameters() {
		return estimatedParameters;
	}
	public void setEstimatedParameters(double estimatedParameters) {
		this.estimatedParameters = estimatedParameters;
	}
	public double getPeriodUse() {
		return periodUse;
	}
	public void setPeriodUse(double periodUse) {
		this.periodUse = periodUse;
	}
	public String getDailyGoods_Typ() {
		return dailyGoods_Typ;
	}
	public void setDailyGoods_Typ(String dailyGoods_Typ) {
		this.dailyGoods_Typ = dailyGoods_Typ;
	}
	public double getUnitRate() {
		return unitRate;
	}
	public void setUnitRate(double unitRate) {
		this.unitRate = unitRate;
	}
	public double getOrderGoodsCount() {
		return orderGoodsCount;
	}
	public void setOrderGoodsCount(double orderGoodsCount) {
		this.orderGoodsCount = orderGoodsCount;
	}
	public double getForecastMoney() {
		return forecastMoney;
	}
	public void setForecastMoney(double forecastMoney) {
		this.forecastMoney = forecastMoney;
	}
	public double getTcurstore() {
		return tcurstore;
	}
	public void setTcurstore(double tcurstore) {
		this.tcurstore = tcurstore;
	}
	public double getOrderGoods() {
		return orderGoods;
	}
	public void setOrderGoods(double orderGoods) {
		this.orderGoods = orderGoods;
	}
	public String getSp_desc() {
		return sp_desc;
	}
	public void setSp_desc(String sp_desc) {
		this.sp_desc = sp_desc;
	}
	public double getUnitRate_x() {
		return unitRate_x;
	}
	public void setUnitRate_x(double unitRate_x) {
		this.unitRate_x = unitRate_x;
	}
	public String getSp_unit() {
		return sp_unit;
	}
	public void setSp_unit(String sp_unit) {
		this.sp_unit = sp_unit;
	}
	public double getStandardCount() {
		return standardCount;
	}
	public void setStandardCount(double standardCount) {
		this.standardCount = standardCount;
	}
	public int getEditorCount() {
		if(editorCount==0){
			editorCount = 1;
		}
		return editorCount;
	}
	public void setEditorCount(int editorCount) {
		this.editorCount = editorCount;
	}
	public double getMincnt() {
		return mincnt;
	}
	public void setMincnt(double mincnt) {
		this.mincnt = mincnt;
	}
	public double getSpmin() {
		return spmin;
	}
	public void setSpmin(double spmin) {
		this.spmin = spmin;
	}
	public double getSpnow() {
		return spnow;
	}
	public void setSpnow(double spnow) {
		this.spnow = spnow;
	}
	public double getSpway() {
		return spway;
	}
	public void setSpway(double spway) {
		this.spway = spway;
	}
	public double getSpuse() {
		return spuse;
	}
	public void setSpuse(double spuse) {
		this.spuse = spuse;
	}
	public String getUnit3() {
		return unit3;
	}
	public void setUnit3(String unit3) {
		this.unit3 = unit3;
	}
	public double getUnitper3() {
		return unitper3;
	}
	public void setUnitper3(double unitper3) {
		this.unitper3 = unitper3;
	}
	public String getTyp_eas() {
		return typ_eas;
	}
	public void setTyp_eas(String typ_eas) {
		this.typ_eas = typ_eas;
	}
	public double getSpcal() {
		return spcal;
	}
	public void setSpcal(double spcal) {
		this.spcal = spcal;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public double getStomin() {
		return stomin;
	}
	public void setStomin(double stomin) {
		this.stomin = stomin;
	}
	public double getStocnt() {
		return stocnt;
	}
	public void setStocnt(double stocnt) {
		this.stocnt = stocnt;
	}
	public String getDisunit() {
		return disunit;
	}
	public void setDisunit(String disunit) {
		this.disunit = disunit;
	}
	public double getDisunitper() {
		return disunitper;
	}
	public void setDisunitper(double disunitper) {
		this.disunitper = disunitper;
	}
	public double getDismincnt() {
		return dismincnt;
	}
	public void setDismincnt(double dismincnt) {
		this.dismincnt = dismincnt;
	}
	public double getAmountdis() {
		return amountdis;
	}
	public void setAmountdis(double amountdis) {
		this.amountdis = amountdis;
	}
	public String getPositn() {
		return positn;
	}
	public void setPositn(String positn) {
		this.positn = positn;
	}
	public String getPositndes() {
		return positndes;
	}
	public void setPositndes(String positndes) {
		this.positndes = positndes;
	}
	
}
