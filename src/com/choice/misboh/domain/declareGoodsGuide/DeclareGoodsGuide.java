package com.choice.misboh.domain.declareGoodsGuide;

import java.util.List;
import java.util.Map;

import com.choice.misboh.commonutil.FormatDouble;

/**
 * 报货向导使用数据集合
 * @author 文清泉
 * @param 2015年4月8日 上午10:39:55
 */
public class DeclareGoodsGuide {
	/**
	 * 获取前台表格数据集
	 */
	private List<Map<String,Object>> tableList;

	/**
	 * 获取订货日
	 */
	private String dat;
	
	/**
	 * 获取预估周期
	 * @author 文清泉
	 * @param 2015年4月8日 下午1:19:32
	 * @return
	 */
	private String useMaxEndDate;
	
	/**
	 * 预估营业额
	 */
	private List<Map<String,Object>> salePlanList;
	
	/**
	 * 后台判断反馈前提错误提示
	 */
	private String errorLog;
	
	/**
	 * 本次到货日
	 */
	private String RECEIVEDATE;
	
	/**
	 * 部门
	 */
	private String dept;
	/**
	 * 报货类别预估值
	 */
	private List<Map<String,Object>> saleList;
	
	/**
	 * 预估方式计算结果
	 */
	private Map<String,Declare> declareMap;
	
	
	/**
	 * 千元用量显示
	 */
	private List<List<Object>> thousandsList;
	
	/**
	 * 生成订单    
	 */
	private Map<String,List<Map<String,Object>>> supplyordr;
	
	/**
	 * 计算用量开始时间
	 */
	private String bThousandsDat;
	
	/**
	 * 计算用量结束时间
	 */
	private String eThousandsDat;
	
	/**
	 * 小数位控制
	 */
	private Integer moneyLength;
	
	/**
	 * 千元用量计算后结果
	 * @author 文清泉
	 * @param 2015年1月1日 下午3:15:58
	 * @return
	 */
	private List<Map<String,Object>> thousList;

	/**
	 * 生成物资订单
	 */
	private List<Map<String,Object>> typList;
	
	/**
	 * 界面权限控制
	 * 0 报货向导   1 用量报货   2 沽清报货
	 */
	private String sta;


	public List<Map<String, Object>> getTypList() {
		return typList;
	}

	public void setTypList(List<Map<String, Object>> typList) {
		this.typList = typList;
	}

	public Map<String, List<Map<String, Object>>> getSupplyordr() {
		return supplyordr;
	}

	public void setSupplyordr(Map<String, List<Map<String, Object>>> supplyordr) {
		this.supplyordr = supplyordr;
	}

	public List<Map<String, Object>> getThousList() {
		return thousList;
	}

	public void setThousList(List<Map<String, Object>> thousList) {
		this.thousList = thousList;
	}

	public String getbThousandsDat() {
		return bThousandsDat;
	}

	public void setbThousandsDat(String bThousandsDat) {
		this.bThousandsDat = bThousandsDat;
	}

	public String geteThousandsDat() {
		return eThousandsDat;
	}

	public void seteThousandsDat(String eThousandsDat) {
		this.eThousandsDat = eThousandsDat;
	}

	
	public String getDat() {
		return dat;
	}
	
	public void setDat(String dat) {
		this.dat = dat;
	}
	
	public List<Map<String,Object>> getTableList() {
		return tableList;
	}

	public void setTableList(List<Map<String,Object>> tableList) {
		this.tableList = tableList;
	}
	public void setUseMaxEndDate(String useMaxEndDate) {
		this.useMaxEndDate = useMaxEndDate;
	}
	public String getUseMaxEndDate() {
		return useMaxEndDate;
	}

	public List<Map<String, Object>> getSalePlanList() {
		return salePlanList;
	}

	public void setSalePlanList(List<Map<String, Object>> salePlanList) {
		this.salePlanList = salePlanList;
	}

	public String getErrorLog() {
		return errorLog;
	}

	public String getSta() {
		return sta;
	}

	public void setSta(String sta) {
		this.sta = sta;
	}

	public void setErrorLog(String errorLog) {
		this.errorLog = errorLog;
	}

	public String getRECEIVEDATE() {
		return RECEIVEDATE;
	}

	public void setRECEIVEDATE(String rECEIVEDATE) {
		RECEIVEDATE = rECEIVEDATE;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public List<Map<String, Object>> getSaleList() {
		return saleList;
	}

	public void setSaleList(List<Map<String, Object>> saleList) {
		this.saleList = saleList;
	}

	public List<List<Object>> getThousandsList() {
		return thousandsList;
	}

	public void setThousandsList(List<List<Object>> thousandsList) {
		this.thousandsList = thousandsList;
	}

	public Map<String, Declare> getDeclareMap() {
		return declareMap;
	}

	public void setDeclareMap(Map<String, Declare> declareMap) {
		this.declareMap = declareMap;
	}

	public Integer getMoneyLength() {
		if(moneyLength == null || moneyLength == 0){
			moneyLength = FormatDouble.getBusClicksRatioBit();
		}
		return moneyLength;
	}

	public void setMoneyLength(Integer moneyLength) {
		this.moneyLength = moneyLength;
	}

}
