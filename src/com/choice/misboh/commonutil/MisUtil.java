package com.choice.misboh.commonutil;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.choice.misboh.constants.reportMis.MisStringConstant;
import com.choice.misboh.domain.reportMis.ReportObject;
import com.choice.orientationSys.util.ReadProperties;



/**
 * 描述：报表需要的一些公共方法类
 * @author 马振
 * 创建时间：2015-4-16 下午12:30:03
 */
public class MisUtil {

	private static Logger log = Logger.getLogger(MisUtil.class);
	
	public static String zipPath = "";
	public static String proFlag = "";
	public static String formatLen = "";
	public static String FinancialMapping = "";
	public static String isAll = "";
	public static String Synchronous = "";
	public static int threadSize = 3;	//每次创建的线程条数
	
	static{
		ReadProperties rp = new ReadProperties();
		if(ValueUtil.IsNotEmpty(rp.getStrByParam("zipPath"))){
			zipPath = rp.getStrByParam("zipPath");
		}
		if(ValueUtil.IsNotEmpty(rp.getStrByParam("bohPageLoad"))){
			proFlag = rp.getStrByParam("bohPageLoad");
		}
		if(ValueUtil.IsNotEmpty(rp.getStrByParam("formatLen"))){
			formatLen = rp.getStrByParam("formatLen");
		}
		
		//是否财务映射
		if(ValueUtil.IsNotEmpty(rp.getStrByParam("FinancialMapping"))){
			FinancialMapping = rp.getStrByParam("FinancialMapping");
		}
		
		////配置在不选择门店时是否查询出已分配的门店数据
		if(ValueUtil.IsNotEmpty(rp.getStrByParam("IsAll"))){
			isAll = rp.getStrByParam("IsAll");
		}
		
		//配置在不选择门店时是否查询出已分配的门店数据
		if(ValueUtil.IsNotEmpty(rp.getStrByParam("Synchronous"))){
			Synchronous = rp.getStrByParam("Synchronous");
		}
		
		if(ValueUtil.IsNotEmpty(rp.getStrByParam("threadSize"))){
			try{
				threadSize = Integer.parseInt(rp.getStrByParam("threadSize"));
			}catch (Exception e) {
				threadSize = 3;
			}
		}
	}
	
	/**
	 * 描述：数字与字符类型的数字相加
	 * @author 马振
	 * 创建时间：2015-4-16 上午11:28:19
	 * @param pl
	 * @param obj
	 * @return
	 */
	public static Double stringPlusDouble(Object pl,Object obj){
		Double number = 0.00;
		
		try{
			String model = "#0.00";	//数字格式
			DecimalFormat df = new DecimalFormat(model);
			df.setRoundingMode(RoundingMode.HALF_UP);	//设置五入 默认为HALF_EVEN 2，4，6，8的不进位
			
			if(ValueUtil.IsNotEmpty(pl)){
				number = Double.parseDouble(pl.toString());
			}
			
			if(ValueUtil.IsNotEmpty(obj)){
				
				//如果obj含有两个负号，如：抹零金额为负数  obj的值就为：（--0.01） 所以此处进行截取  负负得正
				if(obj.toString().indexOf("--") != -1){
					obj = obj.toString().substring(2,obj.toString().length());
				}
				number = number + Double.parseDouble(obj.toString());
			}
			number = Double.parseDouble(df.format(number));
		}catch(Exception e){
			log.error(e);
		}
		
		return number;
	}
	
	/**
	 * 描述： 动态执行类中的方法
	 * @author 马振
	 * 创建时间：2015-4-16 上午11:35:18
	 * @param aim
	 * @param reportName
	 * @param condition
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static ReportObject<Map<String,Object>> execMethod(Object aim,String reportName,Object... condition){
		ReportObject<Map<String,Object>> result = null;
		int len = condition.length;
		Class<?>[] params = new Class[condition.length];
		for(int i = 0 ; i < len ; i ++){
			params[i] = condition[i].getClass();
		}
		String classpath = aim.getClass().getName();
		String method = ReadReport.getReportMethod(MisStringConstant.class, reportName);
		try {
			Method m = Class.forName(classpath).getMethod(method,params);
			result =  (ReportObject<Map<String, Object>>) m.invoke(aim, condition);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 描述：格式化list集合中的数字（将空值格式化为0.00）
	 * @author 马振
	 * 创建时间：2015-4-16 下午12:24:56
	 * @param listResult
	 * @param cols
	 * @param len
	 * @return
	 */
	public static List<Map<String,Object>> formatNumForListResult(List<Map<String,Object>> listResult,String cols,int len){
		String num = "0";
		
		if (len > 0) {
			num += ".";
			
			for (int i = 0; i < len; i++) {
				num += "0";
			}
		}
		
		if(listResult != null && listResult.size()>0){
			String[] colsList = cols.split(",");
			
			for(Map<String,Object> map : listResult){
				
				//循环map并取里面的值
				for(String col : colsList){
					map.put(col,null == map.get(col) ? num : formatDoubleLength(map.get(col).toString(),len));
				}
			}
		}
		return listResult;
	}
	
	/**
	 * 描述：格式化数字
	 * @author 马振
	 * 创建时间：2015-4-16 下午12:26:43
	 * @param num  需要格式化的数字
	 * @param len  格式化的长度:<0 为默认2;  >=0 为格式化的小数位数
	 * @return
	 */
	public static String formatDoubleLength(String num,int len){
		Double formatNum = 0.00;
		
		//如果要格式化的数为空，则赋值
		if(ValueUtil.IsEmpty(num)){
			num = "0";
		}
		
		try{
			//如果输入的字符是数字则转换，不是直接返回该字符串
			formatNum = Double.valueOf(num);
		}catch (Exception e) {
			log.error(e);
			return num;
		}
		
		String result = "";	//存放最终格式化后的数字
		String model="#0";	//数字格式
		int length=2;		//数字格式的小数位数
		
		//判断如果输入的长度>=0则将长度修改<0则去系统配置的长度参数名：formatLen
		if (len >= 0) {			
			length = len;
		}else{
			
			if (ValueUtil.IsNotEmpty(formatLen)) {
				
				try{
					length = Integer.parseInt(formatLen);
				}catch (Exception e) {
					log.error(e);
				}
			}
		}
		
		if (length>1) {
			model += ".";
		}
		
		//循环小数位数格式
		for (int i = 0; i < length; i++){
			model+="0";
		}
		DecimalFormat df = new DecimalFormat(model);
		
		//设置五入 默认为HALF_EVEN 2，4，6，8的不进位
		df.setRoundingMode(RoundingMode.HALF_UP);
		result = df.format(formatNum);
		
		return result;
	}
	
	/**
	 * 描述：数字与字符类型的数字相除
	 * @author 马振
	 * 创建时间：2015-4-23 上午10:25:30
	 * @param pl
	 * @param obj
	 * @param len
	 * @return
	 */
	public static String dividedNum(Object pl,Object obj,int len){
		Double t = 0.00;
		try{
			if(ValueUtil.IsNotEmpty(pl)){
				t = Double.parseDouble(pl.toString());
			}
			if(ValueUtil.IsNotEmpty(obj)){
				if(Double.parseDouble(pl.toString())==0){
					return formatDoubleLength(t.toString(),len);
				}
				//计算
				if(!obj.equals("0")){
					t = t/Double.parseDouble(obj.toString());
				}
			}
		}catch(Exception e){
			log.error(e);
		}
		return formatDoubleLength(t.toString(),len);
	}
	
	/**
	 * 描述：若为null则赋值为空
	 * @author 马振
	 * 创建时间：2015-7-31 下午3:44:53
	 * @param obj
	 * @return
	 */
	public static String getValue(Object obj){
		if(null == obj){
			return "";
		}else{
			return obj.toString();
		}
	}
}