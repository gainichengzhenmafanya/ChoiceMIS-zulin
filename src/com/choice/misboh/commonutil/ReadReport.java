package com.choice.misboh.commonutil;

import java.lang.reflect.Field;

import com.choice.orientationSys.util.ReadProperties;

/**
 * 描述：报表所用的一些公共方法
 * @author 马振
 * 创建时间：2015-4-16 上午10:57:47
 */
public class ReadReport {

	public static String EXT = "";
	
	static{
		ReadProperties rp = new ReadProperties();
		if(ValueUtil.IsNotEmpty(rp.getStrByParam("bohPageLoad"))){
			EXT = rp.getStrByParam("bohPageLoad");
		}
	}
	
	/**
	 * 获取报表表格地址
	 * @param constant 表格地址存储的类
	 * @param name 报表名(不区分大小写)
	 * @return
	 */
	public static String getReportURL(Class<?> constant,String name){
		try {
			Field field = getAimField(constant,"REPORT_SHOW_" + name.toUpperCase());
			return (String) field.get(null);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}
	
	/**
	 * 根据配置文件获取页面路径
	 * @param constant
	 * @param name
	 * @return
	 */
	public static String getPageURL(Class<?> constant,String name){
		try {
			Field field = getAimField(constant,name.toUpperCase());
			return (String) field.get(null);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}
	
	/**
	 * 获取ireport模版地址
	 * @param constant
	 * @param name
	 * @return
	 */
	public static String getIReportPrint(Class<?> constant,String name){
		try {
			Field field =  getAimField(constant,"REPORT_URL_"+name.toUpperCase());
			return (String) field.get(null);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}
	
	/******************************************************TeleStringConstant**************************************************************/
	
	/**
	 * 获取表格表头index
	 * @param constant
	 * @param name
	 * @return
	 */
	public static String getDefaultHeader(Class<?> constant,String name){
		try {
			Field field =  getAimField(constant,"BASICINFO_REPORT_"+name.toUpperCase());
			return (String) field.get(null);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}
	
	/**
	 * 获取表格frozen表头index
	 * @param constant
	 * @param name
	 * @return
	 */
	public static String getFrozenHeader(Class<?> constant,String name){
		try {
			Field field =  getAimField(constant,"BASICINFO_REPORT_"+name.toUpperCase()+"_FROZEN");
			return (String) field.get(null);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}
	
	/**
	 * 获取报表名字(数据库中存储)
	 * @param constant
	 * @param name
	 * @return
	 */
	public static String getReportName(Class<?> constant,String name){
		try {
			Field field =  getAimField(constant,"REPORT_NAME_"+name.toUpperCase());
			return (String) field.get(null);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}
	
	/**
	 * 获取报表名字(中文)
	 * @param constant
	 * @param name
	 * @return
	 */
	public static String getReportNameCN(Class<?> constant,String name){
		try {
			Field field =  getAimField(constant,"REPORT_NAME_CN_"+name.toUpperCase());
			return (String) field.get(null);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}
	
	/**
	 * 获取报表查询方法
	 * @param constant
	 * @param name
	 * @return
	 */
	public static String getReportMethod(Class<?> constant,String name) {
		try {
			Field field =  getAimField(constant,"REPORT_QUERY_METHOD_"+name.toUpperCase());
			return (String) field.get(null);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}
	
	public static Integer[] getReportExcel(Class<?> constant,String name){
		try {
			return (Integer[]) constant.getField("EXCEL_"+name.toUpperCase()).get(null);
		} catch (Exception e) {
		} 
		return null;
	}
	
	/*************************************************public method**********************************************************************/
	
	private static Field getAimField(Class<?> constant,String name){
		Field field = null;
		try{
			field = constant.getField(name.toUpperCase()+EXT.toUpperCase());
		}catch(NoSuchFieldException e){
			try {
				field = constant.getField(name.toUpperCase()+"_WFZ");
			}  catch (NoSuchFieldException e1) {
				try {
					field = constant.getField(name.toUpperCase());
				} catch (NoSuchFieldException e2) {
					e2.printStackTrace();
				} catch (SecurityException e2) {
					e2.printStackTrace();
				}
			}
		}
		return field;
	}
}
