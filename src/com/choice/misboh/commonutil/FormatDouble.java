package com.choice.misboh.commonutil;


import java.math.BigDecimal;
import java.text.DecimalFormat;

import com.choice.framework.util.ForResourceFiles;

/**
 * 格式化金额
 * @author 王超
 *
 */
public class FormatDouble {
	
	public static int MoneyBit = getMoneyBit();
	public static int BusClicksRatioBit = getBusClicksRatioBit();
	
	/**
	 * 整数读取的时候自动补零
	 * @return0
	 */
	public static String formatDoubleLength(double price){
//		String model="#0.";
		int length=2;
		String MoneyBit=ForResourceFiles.getValByKey(null, "MoneyBit");
		try{
			length=Integer.parseInt(MoneyBit);
		}catch (Exception e) {
		}
//		for(int i=0;i<length;i++){
//			model+="0";
//		}
//		DecimalFormat df = new DecimalFormat(model);
//		return df.format(price);
		 String result = String.format("%."+length+"f", price);
		 if(Double.parseDouble(result) == 0){
		     result = "0.00";
		 }
		 return result;
	}
	
	
	/**
	 * 数据读取的时候自动补0  保留四位小数
	 * @param price
	 * @return
	 */
	public static String formatDoubleLth(double price){
		String model = "#0.";
		for (int i = 0; i < BusClicksRatioBit; i++) {
			model+="0";
		}
		DecimalFormat df = new DecimalFormat(model);
		if(Double.valueOf(df.format(price)) == -0D){
			return df.format(0D);
		}else{
			return df.format(price);
		}
	}
	
	/**
	 * 字符串类型的数字保留指定位数的小数
	 * @param str
	 * @return
	 */
	public static String formatDoubleLth(String str){
		double value = 0;
		try {
			value = Double.valueOf(str);
		} catch (NumberFormatException e) {
			return str;
		}
		return formatDoubleLth(value);
	}
	
	/**
	 * 数字类型的小数保留指定
	 * @param price
	 * @return
	 */
	public static String formatDoubleTH(double price){
		return formatDoubleLth(price);
	}
	
//	/**
//	 * 整数读取的时候自动补零
//	 * @return0
//	 */
//	public static String formatDoubleLength(double price,int length){
//		String model="#0.";
//		for(int i=0;i<length;i++){
//			model+="0";
//		}
//		DecimalFormat df = new DecimalFormat(model);
//		return df.format(price);
//		
//	}

	/**处理字符串类型的小数位
	 * @param str
	 * @param num
	 * @return
	 */
	public static String formatDoubleLength(String str) {
		double value = 0;
		try {
			value = Double.valueOf(str);
		} catch (NumberFormatException e) {
			return str;
		}
		return formatDoubleLength(value);
	}
	
	
	
	/**
	 * 
	* @Title: formatDoubleLength 
	* @Description: 处理字符串类型的小数位
	* @param @param str  字符串
	* @param @param len 小数点后数字长度
	* @param @return    设定文件 
	* @return String    返回类型 
	* @throws
	 */
	public static String formatDoubleLength(String str,int len) {
		str = str == null ? "0" : str;
		int num = len;
		if (str.indexOf(".") > -1) {
			String str1 = str.substring(str.indexOf(".") + 1, str.indexOf(".")
					+ 1 + str.length() - (str.indexOf(".") + 1));
			int i = 0;
			if (num < 0)
				num = 0; // 如果指定要保留的小数位小于零,则初始化为零
			if (str1.length() > num) {
				if (Integer.valueOf(str1.substring(num, num + 1)) > 4) {
					int val = 0;
					val = Integer.valueOf(str1.substring(0, num + 1))
							+ (10 - Integer.valueOf(str1
									.substring(num, num + 1)));
					if (String.valueOf(val).length() > (num + 1)) {
						i = 1;
						str1 = String.valueOf(val).substring(1, num + 1);
					} else {
						str1 = String.valueOf(val).substring(0, num);
					}
				} else {
					str1 = str1.substring(0, num);
				}
				if (str.indexOf(".") > 0) {
					String str2 = str.substring(0, str.indexOf("."));
					if (num > 0) {
						str = String.valueOf(Integer.valueOf(str2) + i) + "."
								+ str1;
					} else {
						str = String.valueOf(Integer.valueOf(str2) + i);
					}
				} else {
					if (num > 0) {
						str = String.valueOf(i) + "." + str1;
					} else {
						str = String.valueOf(i);
					}
				}
			} else if (str1.length() < num) {
				for (int n = 0; n < (num - str1.length()); n++) {
					str = str + "0";
				}
			}
		}
		return str;
	}

	public static Double getDoubleLength(double price){
		return ValueUtil.getDoubleValue(formatDoubleLength(price));
	}
	
	public static int getMoneyBit() {
		Object obj = ForResourceFiles.getValByKey("config-bsboh.properties", "MoneyBit");
		if(obj== null || obj.equals("")){
			obj = "2";
		}
		MoneyBit=ValueUtil.getIntValue(obj);
		return MoneyBit;
	}
	
	/**
	 * 获取菜品点击率的小数位数
	 * @return
	 */
	public static int getBusClicksRatioBit(){
		Object obj = ForResourceFiles.getValByKey("config-bsboh.properties", "BusClicksRatioBit");
		if(obj== null || obj.equals("")){
			obj = "4";
		}
		BusClicksRatioBit = ValueUtil.getIntValue(obj);
		return BusClicksRatioBit;
	}
	
	/**
	 * 
	 * 方法描述：取六位小数
	 */
	public static double getDouble6(Double num) {
		return new BigDecimal(num).setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
	}
	
	/**
	 * 描述：将“,”替换为“','”并且在字符串两端加“'”
	 * @author 马振
	 * 创建时间：2015-4-16 上午11:20:50
	 * @param param
	 * @return
	 */
	public static String StringCodeReplace(String param){
		if(ValueUtil.IsEmpty(param)) {
			return ""; 
		}else{
			//处理字符串前先将”','“替换成“,”;
			param=param.replace("'", "");
			return "'"+param.replace(",", "','")+"'";
		}
	}
	
	/**
	 * 描述：移除单引号
	 * @author 马振
	 * 创建时间：2015-5-27 上午11:09:26
	 * @param param
	 * @return
	 */
	public static String removeQuotes(String param){
		if(ValueUtil.IsEmpty(param)) {
			return ""; 
		}else{
			return param.replace("'", "");
		}
	}
	
	/**
	 * 描述：格式化字符串位数不足的以Param在前面补齐
	 * @author 马振
	 * 创建时间：2015-4-16 上午11:21:02
	 * @param param
	 * @param str
	 * @param len
	 * @return
	 */
	public static String formatStringLength(String param,String str,int len){
		if(ValueUtil.IsEmpty(str)) {
			return null; 
		}else{
			if(str.length()<len){
				int i = str.length();
				for(;i<len;i++){
					str = param+str;
				}
			}
			return str;
		}
	}
	
	/**
	 * 描述： 转换字符串方法
	 * @author 马振
	 * 创建时间：2015-4-16 上午11:21:14
	 * @param obj
	 * @return
	 */
	public static String toString(Object obj){
		return null ==obj?"":obj.toString();
	}
}