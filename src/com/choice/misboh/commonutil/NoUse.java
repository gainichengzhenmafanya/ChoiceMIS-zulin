package com.choice.misboh.commonutil;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 描述：标记field为非转换field，ConvertBean中识别此注解
 * @author 马振
 * 创建时间：2015-4-16 上午10:30:00
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface NoUse {

}
