package com.choice.misboh.commonutil;

/**
 * 
* @ClassName: ValueUtil 
* @Description: 值的工具类
* @author 王超
* @date 2014年8月15日 下午5:38:04 
*
 */
public class ValueUtil {
	/**
	 * 
	* @Title: checkString 
	* @Description: 检查字符串是否为空
	* @param value
	* @return    设定文件 
	* @return boolean    返回类型   false 空  true 非空
	 */
	public static boolean checkString(Object value){
		if("".equals(value) || value == null){
			return false;
		}
		return true;
	}
	
	/**
     * 
    * @Title: getStringValue 
    * @Description: 获取字符串
    * @param value
    * @return    设定文件 
    * @return String    返回类型
     */
    public static String getStringValue(Object value){
    	if(value == null || "null".equals(value.toString().toLowerCase())){
    		return "";
    	}
    	return value.toString();
    }
    
	/**
     * 
    * @Title: getStringValue 
    * @Description: 获取布尔类型
    * @param value
    * @return    设定文件 
    * @return String    返回类型
     */
    public static boolean getBooleanValue(Object value){
    	if(value == null || "".equals(value)){
    		return false;
    	}
    	return Boolean.valueOf(value.toString());
    }
    /**
     * 
    * @Title: getDoubleValue 
    * @Description: 获取Double类型的值
    * @param value
    * @return    设定文件 
    * @return Double    返回类型
     */
    public static Double getDoubleValue(Object value){
    	if(value != null && !"".equals(value)){
    		return Double.valueOf(value.toString());
    	}
    	return 0D;
    }
    
    /**
     * 
    * @Title: getFloatValue 
    * @Description: 获取Double类型的值
    * @param value
    * @return    设定文件 
    * @return Double    返回类型
     */
    public static Float getFloatValue(Object value){
    	if(value != null && !"".equals(value)){
    		try {
				return Float.valueOf(value.toString());
			} catch (NumberFormatException e) {
				return 0F;
			}
    	}
    	return 0F;
    }
    /**
     * 
    * @Title: getDoubleValue 
    * @Description: 获取int类型的值
    * @param value
    * @return    设定文件 
    * @return Double    返回类型
     */
    public static int getIntValue(Object value){
    	if(value != null && !"".equals(value)){
    		return Integer.valueOf(value.toString());
    	}
    	return 0;
    }
    
    /**
     * 验证MAP返回值
     * @author 文清泉
     * @param 2015年4月8日 下午1:24:57
     * @param _value MAP中返回值
     * @param typ  1 数值     0 字符
     * @return
     */
    public static Object getMapValue(Object _value,int typ){
    	if(checkString(_value)){
    			return _value;
    	}else{
    		if(typ == 1){
    			return 0;
    		}else{
    			return "";
    		}
    	}
    }
    
	/**
	 * 描述：判断参数非空
	 * @author 马振
	 * 创建时间：2015-4-16 上午10:55:54
	 * @param param
	 * @return
	 */
	public static Boolean IsNotEmpty(Object param){
		if(null !=  param && !"".equals( param) && !"null".equals(param) && !"-null".equals(param))
			return true;
		else
			return false;
	}
	
	/**
	 * 描述：判断参数为空
	 * @author 马振
	 * 创建时间：2015-4-16 上午10:56:05
	 * @param param
	 * @return
	 */
	public static Boolean IsEmpty(Object param){
		if(null ==  param || "".equals(param) || "null".equals(param)){
			return true;
		}else{
			return false;
		}
	}
}
