package com.choice.misboh.commonutil.excel;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Workbook;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.NumberFormat;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.springframework.stereotype.Component;

import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.orientationSys.domain.DictColumns;

/**
 * EXCEL导出
 * @author 文清泉
 * @param 2015年6月3日 上午10:47:16
 */
@Component
public class GenerateExcel<T> {

	
	
	WritableWorkbook workBook;
	WritableCellFormat titleStyle;
	WritableCellFormat contentStyle;
	WritableCellFormat numberStyle;
	private Integer[] rightColumn; 
	
	public GenerateExcel() {
	}
	
	/**
	 * Excel导出，固定列  -- 根据数据库中字段
	 * @param fileName 文件名
	 * @param request WEB响应
	 * @param response WEB响应
	 * @param data 数据集
	 * @param title 标题
	 * @param dictColumns 表头集合
	 * @return
	 */
	@SuppressWarnings("static-access")
	public boolean creatWorkBook_DictColumns(String fileName,HttpServletRequest request,HttpServletResponse response,List<T> data,String title,List<DictColumns> dictColumns,Integer[] rightColumn){
		this.rightColumn = rightColumn;
		OutputStream os = generateGile(fileName,request,response);
		boolean bool = false;
		try {
			if(os != null){
				bool =  new GenerateDictColumnsExcel().export(data, title, dictColumns,this);
			}
			 os.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return bool;
	}
	
	@SuppressWarnings("static-access")
	public boolean creatWorkBook_Report(String fileName,HttpServletRequest request,HttpServletResponse response,List<T> data,String title,List<DictColumns> dictColumns,String headers, Integer[] rightColumn){
		this.rightColumn = rightColumn;
		OutputStream os = generateGile(fileName,request,response);
		boolean bool = false;
		try {
			if(os != null){
				bool =  new GenerateDictColumnsExcel().exportReport(data, title, dictColumns,this,headers);
			}
			 os.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return bool;
	}
	
	
	/**
	 * 生成文件，反馈文件输出流
	 * @author 文清泉
	 * @param 2015年6月3日 上午10:46:53
	 * @param fileName
	 * @param request
	 * @param response
	 * @return
	 */
	private OutputStream generateGile(String fileName,HttpServletRequest request,HttpServletResponse response){
		OutputStream os = null;
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16,WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,Colour.BLACK);
		WritableFont contentFont = new WritableFont(WritableFont.TIMES, 10, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,Colour.BLACK);
		//设定数字导出格式
		String nb = "0.";
		for (int i = 0; i < FormatDouble.getMoneyBit(); i++) {
			nb +="0";
		}
		NumberFormat nf = new jxl.write.NumberFormat(nb); //设定带小数点数字格式
		numberStyle = new jxl.write.WritableCellFormat(contentFont,nf);//设定带小数数字单元格格式
		titleStyle = new WritableCellFormat(titleFont);
		contentStyle = new WritableCellFormat(contentFont);
		try {
			titleStyle.setAlignment(jxl.format.Alignment.CENTRE);	//标题居中
			contentStyle.setAlignment(jxl.format.Alignment.CENTRE);	//内容居中
			
			titleStyle.setBorder(Border.TOP, BorderLineStyle.THIN);		//标题添加上边框
			titleStyle.setBorder(Border.BOTTOM, BorderLineStyle.THIN);	//标题添加下边框
			titleStyle.setBorder(Border.LEFT, BorderLineStyle.THIN);	//标题添加左边框
			titleStyle.setBorder(Border.RIGHT, BorderLineStyle.THIN);	//标题添加右边框
			
			response.setContentType("application/msexcel; charset=UTF-8");
			if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
			    //IE  
			    fileName = URLEncoder.encode(fileName, "UTF-8");              
			}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
			    //firefox  
			    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
			}else{                
			    // other          
			    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
			}   
			response.setHeader("Content-disposition", "attachment; filename="+ fileName + ".xls");
			os = response.getOutputStream();
			workBook = Workbook.createWorkbook(os);
			return os;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
    /**
     * 判断数据类型
     * @author 文清泉
     * @param 2015年6月3日 上午10:46:53
     * @param valueAt 列表显示值
     * @param row  行  
     * @param column 行
     * @param sheet 
     * @param curCol 可以为空， 为空读取默认计算方式
     * @throws WriteException 
     * @throws RowsExceededException 
     */
	public void setHSSFDataFormat(Object valueAt,int row,int column, WritableSheet sheet, DictColumns curCol) throws RowsExceededException, WriteException {
		if (valueAt.equals("") || valueAt == null) {
			sheet.addCell(new Label(column,row,"",contentStyle));
		}
		String _value = valueAt.toString();
		if(rightColumn != null && curCol != null){
			boolean bool = true;
			for (Integer rightcol : rightColumn) {
				if(curCol.getId().equals(rightcol+"")){
					sheet.addCell(new jxl.write.Number(column, row, ValueUtil.getDoubleValue(_value), numberStyle));
					bool = false;
					break;
				}
			}
			if(bool){
				sheet.addCell(new Label(column,row,_value,contentStyle));
			}
		}else{
			Pattern p = Pattern.compile("^[+-]?([0-9]*\\.?[0-9]+|[0-9]+\\.?[0-9]*)([eE][+-]?[0-9]+)?$");
			Matcher matcher = p.matcher(_value);
			if(matcher.matches() && row != 0){
				sheet.addCell(new jxl.write.Number(column, row, ValueUtil.getDoubleValue(valueAt), numberStyle));
			}else{
				sheet.addCell(new Label(column,row,_value,contentStyle));
			}
		}
	}
	
	/**
	 * 描述：每日排班excel导出
	 * @author 马振
	 * 创建时间：2015-6-23 上午11:07:25
	 * @param fileName
	 * @param request
	 * @param response
	 * @param data
	 * @param title
	 * @param dictColumns
	 * @param headers
	 * @param rightColumn
	 * @return
	 */
	@SuppressWarnings("static-access")
	public boolean creatReportCrewSchedule(String fileName,HttpServletRequest request,HttpServletResponse response,List<Map<String,Object>> data,String title,List<DictColumns> dictColumns,String headers, Integer[] rightColumn){
		this.rightColumn = rightColumn;
		OutputStream os = workstationsGenerateGile(fileName,request,response);
		boolean bool = false;
		try {
			if(os != null){
				bool =  new GenerateDictColumnsExcel().exportReportCrewSchedule(data, title, dictColumns, this, headers);
			}
			 os.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return bool;
	}
	
	/**
	 * 描述：工作站excel导出
	 * @author 马振
	 * 创建时间：2015-6-19 上午11:06:46
	 * @param fileName
	 * @param request
	 * @param response
	 * @param data
	 * @param title
	 * @param dictColumns
	 * @param headers
	 * @param rightColumn
	 * @return
	 */
	@SuppressWarnings("static-access")
	public boolean creatReport(String fileName,HttpServletRequest request,HttpServletResponse response,List<Map<String,Object>> data,String title,List<DictColumns> dictColumns,String headers, Integer[] rightColumn){
		this.rightColumn = rightColumn;
		OutputStream os = workstationsGenerateGile(fileName,request,response);
		boolean bool = false;
		try {
			if(os != null){
				bool =  new GenerateDictColumnsExcel().exportReportWorkStation(data, title, dictColumns, this, headers);
			}
			 os.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return bool;
	}
	
	
	/**
	 * 描述：工作站生成文件，反馈文件输出流
	 * @author 马振
	 * 创建时间：2015-6-23 上午8:40:08
	 * @param fileName
	 * @param request
	 * @param response
	 * @return
	 */
	private OutputStream workstationsGenerateGile(String fileName,HttpServletRequest request,HttpServletResponse response){
		OutputStream os = null;
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16,WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,Colour.BLACK);
		WritableFont contentFont = new WritableFont(WritableFont.TIMES, 10, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,Colour.BLACK);
		//设定数字导出格式
		String nb = "0.";
		for (int i = 0; i < FormatDouble.getMoneyBit(); i++) {
			nb +="0";
		}
		NumberFormat nf = new jxl.write.NumberFormat(nb); //设定带小数点数字格式
		numberStyle = new jxl.write.WritableCellFormat(contentFont,nf);//设定带小数数字单元格格式
		titleStyle = new WritableCellFormat(titleFont);
		contentStyle = new WritableCellFormat(contentFont);
		try {
			titleStyle.setAlignment(jxl.format.Alignment.CENTRE);	//标题居中
			contentStyle.setAlignment(jxl.format.Alignment.CENTRE);	//内容居中
			
			titleStyle.setBorder(Border.TOP, BorderLineStyle.THIN);		//标题添加上边框
			titleStyle.setBorder(Border.BOTTOM, BorderLineStyle.THIN);	//标题添加下边框
			titleStyle.setBorder(Border.LEFT, BorderLineStyle.THIN);	//标题添加左边框
			titleStyle.setBorder(Border.RIGHT, BorderLineStyle.THIN);	//标题添加右边框
			
			contentStyle.setBorder(Border.TOP, BorderLineStyle.THIN);	//内容添加上边框
			contentStyle.setBorder(Border.BOTTOM, BorderLineStyle.THIN);//内容添加下边框
			contentStyle.setBorder(Border.LEFT, BorderLineStyle.THIN);	//内容添加左边框
			contentStyle.setBorder(Border.RIGHT, BorderLineStyle.THIN);	//内容添加右边框
			
			contentStyle.setBackground(jxl.format.Colour.GREEN);		//设置单元格的背景颜色
			
			response.setContentType("application/msexcel; charset=UTF-8");
			if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
			    //IE  
			    fileName = URLEncoder.encode(fileName, "UTF-8");              
			}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
			    //firefox  
			    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
			}else{                
			    // other          
			    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
			}   
			response.setHeader("Content-disposition", "attachment; filename="+ fileName + ".xls");
			os = response.getOutputStream();
			workBook = Workbook.createWorkbook(os);
			return os;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
}