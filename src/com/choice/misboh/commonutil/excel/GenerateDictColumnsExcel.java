package com.choice.misboh.commonutil.excel;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.regex.Pattern;

import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import net.sf.json.JSONObject;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.orientationSys.domain.DictColumns;

public class GenerateDictColumnsExcel {

	WritableCellFormat contentStyle;
	
	public static <T> boolean export(List<T> data, String title,List<DictColumns> dictColumns,GenerateExcel<?> generateExcel) throws Exception{
		WritableSheet sheet = generateExcel.workBook.createSheet(title, 0);
		Label label = new Label(0,0,title,generateExcel.titleStyle);
        sheet.addCell(label);
        //设置表格表头
        for(int i = 0 ; i < dictColumns.size();i ++){
        	Label head = new Label(i,1,dictColumns.get(i).getZhColumnName(),generateExcel.contentStyle);
        	sheet.addCell(head);
        }
        sheet.mergeCells(0, 0, dictColumns.size()-1, 0);
        //遍历list填充表格内容
        for(int i = 0 ; i < data.size() ; i ++ ){
        	for(int j = 0 ; j < dictColumns.size() ; j ++){
        		DictColumns curCol = dictColumns.get(j);
        		String result = "";
        		String typeName = "";
        		Stack<String> propStack = new Stack<String>();
        		propStack.addAll(Arrays.asList(curCol.getProperties().split("\\.")));
        		T cur = data.get(i);
        		Object curValue = getValue(propStack,cur);
        		curValue = curValue == null ? "":curValue;
        		typeName = curValue.getClass().getName();
        		typeName = typeName.substring(typeName.lastIndexOf('.')+1);
        		if("int".equals(typeName) || "Integer".equals(typeName)){
					result = curValue.toString();
				}else if("Date".equals(typeName)){
					DateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
					result = dft.format(curValue);
				}else{
					result = curValue.toString();
				}
        		generateExcel.setHSSFDataFormat(result,2+i,j,sheet,curCol);
        	}
        }
        generateExcel.workBook.write();
		return true;
	}
	
	//解析对象数据
	@SuppressWarnings("unchecked")
	private static Object getValue(Stack<String> stack,Object obj) throws Exception{
		String className = obj.getClass().getName();
		Object result = null;
		if(stack.empty())return null;
		String cur = stack.pop();
		if(className.indexOf("java.util.HashMap") >= 0){
			result = ((Map<Object,Object>)obj).get(cur.toUpperCase());
		}else{
			Field curFeild = Class.forName(className).getDeclaredField(cur);
			Boolean accessible = curFeild.isAccessible();
    		if(!accessible)curFeild.setAccessible(true);
    		result = curFeild.get(obj);
    		curFeild.setAccessible(accessible);
		}
		if(stack.empty())return result;
		return getValue(stack,result);
	}
	
	public static <T> boolean exportReport(List<T> data, String title,List<DictColumns> dictColumns,GenerateExcel<?> generateExcel,String headers) throws Exception{
		Stack<Map<String,String>> fields = new Stack<Map<String,String>>();//存储field属性，按表格顺序
		int headHeight = 0;
		WritableSheet sheet = generateExcel.workBook.createSheet(title, 0);
		Label label = new Label(0,0,title,generateExcel.titleStyle);
        sheet.addCell(label);
        if(null != headers && !"".equals(headers)){//如果headers不为空解析headers生成表头
        	execTitleReport(headers,generateExcel,sheet,fields,headHeight);
        }else{
        	headHeight = 1;
        	for(int i = 0 ; i < dictColumns.size();i ++){
            	Label head = new Label(i,1,dictColumns.get(i).getZhColumnName(),generateExcel.contentStyle);
            	sheet.addCell(head);
            	Map<String,String> local = new HashMap<String,String>();
            	local.put("key", dictColumns.get(i).getProperties());
            	local.put("type",dictColumns.get(i).getColumnType());
            	fields.push(local);
            }
        	sheet.mergeCells(0, 0, dictColumns.size()-1, 0);
        }
        
        //遍历list填充表格内容
        for(int i = 0 ; i < data.size() ; i ++ ){
        	for(int j = 0 ; j < fields.size() ; j ++){
        		String result = "";
        		String typeName = "";
        		Stack<String> propStack = new Stack<String>();
        		if(null != headers){
        			propStack.push(fields.get(j).get("key"));
        		}else{
        			propStack.addAll(Arrays.asList(fields.get(j).get("key").split("\\.")));
        		}
        		T cur = data.get(i);
        		Object curValue = getValue(propStack,cur);
        		curValue = curValue == null ? "":curValue;
        		typeName = curValue.getClass().getName();
        		typeName = typeName.substring(typeName.lastIndexOf('.')+1);
        		if("int".equals(typeName) || "Integer".equals(typeName)){
					result = curValue.toString();
				}else if("Date".equals(typeName)){
					DateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
					result = dft.format(curValue);
				}else{
					result = curValue.toString();
				}
        		generateExcel.setHSSFDataFormat(result,2+i,j,sheet,null);
        	}
        }
        generateExcel.workBook.write();
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public static void execTitleReport(String headers,GenerateExcel<?> generateExcel, WritableSheet sheet, Stack<Map<String, String>> fields, int headHeight) throws JDOMException, IOException, RowsExceededException, WriteException{
		//如果headers不为空解析headers生成表头
    	headers = headers.toUpperCase();
    	String Headers = "<div>" + headers + "</div>";
    	Map<String,String> fieldMap = null;
		ByteArrayInputStream xmlStream = null;
		xmlStream = new ByteArrayInputStream(Headers.getBytes());
		SAXBuilder reader = new SAXBuilder();
		Document doc = reader.build(new InputStreamReader(xmlStream));//转换输入数据为Document对象
		Element root = doc.getRootElement();//获取xml根标签
		ArrayList<Stack<Element>> eleList = new ArrayList<Stack<Element>>();//存储所有td
		List<Element> tables = root.getChildren("TABLE");//获取table标签
		Element f = root.getChildren("FIELDMAP").size() > 0 ? root.getChildren("FIELDMAP").get(0) : null;
		if(null != f && !"".equals(f.getValue()))
			fieldMap = JSONObject.fromObject(f.getValue());
		for(Element e : tables){//循环读取table，将td放入eleList
			List<Element> rows = e.getChildren().get(0).getChildren();
			for(int i = 0 ; i < rows.size() ; i ++){
				if(eleList.size() < (i + 1)){
					eleList.add(new Stack<Element>());
				}
				eleList.get(i).addAll(rows.get(i).getChildren());
			}
		}
		//改
		headHeight = eleList.size();
		ArrayList<ArrayList<Integer>> cellSta = new ArrayList<ArrayList<Integer>>();//存储单元格index
		for(int i = 0 ; i < headHeight ; i ++){
			Collections.reverse(eleList.get(i));
			cellSta.add(new ArrayList<Integer>());
		}
		Pattern p = Pattern.compile(".*DISPLAY\\s*:\\s*NONE\\s*;.*");//正则表达式匹配cell是否为隐藏
		Pattern rightAlign = Pattern.compile(".*TEXT-ALIGN\\s*:\\s*RIGHT\\s*;.*");//正则表达式匹配是否为右对齐
		Map<Integer,String> titles = new HashMap<Integer,String>();
		int cell = 0;
		int width = eleList.get(0).size();
		for(int i = 0 ; i < width ; i ++){
			Element top = eleList.get(0).pop();
			String style = top.getAttributeValue("STYLE");
			String divStyle = null == top.getChild("DIV") ? "": top.getChild("DIV").getAttributeValue("STYLE");
			String value = null == top.getValue() ? "" : top.getValue();
			String field = top.getAttributeValue("FIELD");
			if(p.matcher(null == style ? "" : style).matches() || value.equals(""))continue;
			int spanY = null == top.getAttributeValue("ROWSPAN") ? 1 : Integer.parseInt(top.getAttributeValue("ROWSPAN"));
			int spanX = null == top.getAttributeValue("COLSPAN") ? 1 : Integer.parseInt(top.getAttributeValue("COLSPAN"));
			for(int x = 0 ; x < spanX ; x ++){
				for(int y = 0 ; y < spanY ; y ++)
					cellSta.get(y).add(cell);
			}
			if(null != field && !"".equals(field)){
				Map<String,String> local = new HashMap<String,String>();
				if(rightAlign.matcher(divStyle).matches()){
					local.put("key",null != fieldMap && fieldMap.size() > 0 ?  fieldMap.get(field) : field);
					local.put("type","digital");
				}else{
					local.put("key",null != fieldMap && fieldMap.size() > 0 ?  fieldMap.get(field) : field);
					local.put("type","text");
				}
				fields.push(local);
			}
			titles.put(cell, value);
			cell ++;
			for(int h = spanY ; h < headHeight;){
				int innerDeep = 1;
				for(int j = 0 ; j < spanX;){
					if( eleList.get(h).size()<=0 ||  eleList.get(h) == null){
						continue;
					}
					Element e = eleList.get(h).pop();
        			String localdivStyle = null == e.getChild("DIV") ? "" : e.getChild("DIV").getAttributeValue("STYLE");
					String fieldinner = e.getAttributeValue("FIELD");
					int innerY = null == e.getAttributeValue("ROWSPAN") ? 1 : Integer.parseInt(e.getAttributeValue("ROWSPAN"));
					int innerX = null == e.getAttributeValue("COLSPAN") ? 1 : Integer.parseInt(e.getAttributeValue("COLSPAN"));
					for(int x = 0 ; x < innerX ; x ++){
        				for(int y = 0 ; y < innerY ; y ++)
        					cellSta.get(h+y).add(cell);
        			}
					if(null != fieldinner && !"".equals(fieldinner)){
						Map<String,String> local = new HashMap<String,String>();
						if(rightAlign.matcher(localdivStyle).matches()){
        					local.put("key",null != fieldMap && fieldMap.size() > 0 ?  fieldMap.get(fieldinner) : fieldinner);
        					local.put("type","digital");
        				}else{
        					local.put("key",null != fieldMap && fieldMap.size() > 0 ?  fieldMap.get(fieldinner) : fieldinner);
        					local.put("type","text");
        				}
						fields.push(local);
					}
					titles.put(cell, e.getValue());
					j += innerX;
					innerDeep = innerY;
					cell ++;
				}
				h += innerDeep;
			}
		}
		for(int i = --cell ; i >= 0 ; i --){//循环数组，合并单元格
			int Xstart = -1;
			int Ystart = -1;
			int Xend = -1;
			int Yend = -1;
			for(ArrayList<Integer> arr : cellSta){
				int indexS = arr.indexOf(i);
				int indexE = arr.lastIndexOf(i);
				Xstart = indexS >= 0 ? indexS : Xstart;
				Ystart = indexS >= 0 && Ystart == -1 ? cellSta.indexOf(arr) : Ystart;
				Xend = indexE >= 0 ? indexE : Xend;
				Yend = indexS >= 0 ? cellSta.indexOf(arr) : Yend;
			}
			Label head = new Label(Xstart,Ystart+1,titles.get(i),generateExcel.contentStyle);
			sheet.addCell(head);
			sheet.mergeCells(Xstart, Ystart+1, Xend, Yend+1);
		}
		sheet.mergeCells(0, 0, cellSta.get(0).size()-1, 0);
	}
	
	/**
	 * 描述：每日排班excel导出
	 * @author 马振
	 * 创建时间：2015-6-23 上午11:08:48
	 * @param data
	 * @param title
	 * @param dictColumns
	 * @param generateExcel
	 * @param headers
	 * @return
	 * @throws Exception
	 */
	public static boolean exportReportCrewSchedule(List<Map<String,Object>> data, String title,List<DictColumns> dictColumns,GenerateExcel<?> generateExcel,String headers) throws Exception{
		String dworkdate = "";//营业日
		
		WritableSheet sheet = generateExcel.workBook.createSheet(title, 0);
    	WritableFont contentFont = new WritableFont(WritableFont.TIMES, 10, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,Colour.BLACK);
    	
    	WritableCellFormat content = new WritableCellFormat(contentFont);
    	content.setAlignment(jxl.format.Alignment.CENTRE);		//内容居中
    	content.setBorder(Border.TOP, BorderLineStyle.THIN);	//内容添加上边框
    	content.setBorder(Border.BOTTOM, BorderLineStyle.THIN);	//内容添加下边框
    	content.setBorder(Border.LEFT, BorderLineStyle.THIN);	//内容添加左边框
    	content.setBorder(Border.RIGHT, BorderLineStyle.THIN);	//内容添加右边框
    	
    	WritableCellFormat empInfo = new WritableCellFormat(contentFont);
    	empInfo.setAlignment(jxl.format.Alignment.LEFT);		//员工信息居左
    	empInfo.setBorder(Border.TOP, BorderLineStyle.THIN);	//内容添加上边框
    	empInfo.setBorder(Border.BOTTOM, BorderLineStyle.THIN);	//内容添加下边框
    	empInfo.setBorder(Border.LEFT, BorderLineStyle.THIN);	//内容添加左边框
    	empInfo.setBorder(Border.RIGHT, BorderLineStyle.THIN);	//内容添加右边框

    	//获取日期
    	for (int i = 0; i < data.size(); i++) {
    		if (ValueUtil.IsNotEmpty(data.get(i).get("dworkdate"))) {
    			dworkdate = data.get(i).get("dworkdate").toString();
    		}
    	}
    	
		//标题
		Label label = new Label(0,0,title + "(" + dworkdate + ")",generateExcel.titleStyle);
        sheet.addCell(label);
        sheet.mergeCells(0, 0, 3 + data.size(), 0);	//第一行标题合并单元格(第一列、第一行、第25列、第一行)
        
        //第一列，第二行：编码
        Label headCode = new Label(0,1,"编码",content);
    	sheet.addCell(headCode);
    	sheet.mergeCells(0, 1, 0, 2);
    	
    	//第二列，第二行：名称
        Label headName = new Label(1,1,"名称",content);
    	sheet.addCell(headName);
    	sheet.mergeCells(1, 1, 1, 2);
    	
    	//第三列，第二行：岗位
        Label headStation = new Label(2,1,"岗位",content);
    	sheet.addCell(headStation);
    	sheet.mergeCells(2, 1, 2, 2);
    	
    	//第四列，第二行：考勤号
        Label headCardNo = new Label(3,1,"考勤号",content);
    	sheet.addCell(headCardNo);
    	sheet.mergeCells(3, 1, 3, 2);

    	String[] pk_hrdepts = data.get(0).get("pk_hrdept").toString().split(",");
    	String[] pk_teams = data.get(0).get("pk_team").toString().split(",");
    	String[] pk_shifts = data.get(0).get("pk_shift").toString().split(",");
    	
        //设置表格表头
        for(int i = 0 ; i < data.size();i ++){
        	
        	Label headDept = new Label(i + 4, 1, data.get(i).get("vname").toString(), content);
        	sheet.mergeCells(i + 4, 1, i + 3 + Integer.parseInt(data.get(i).get("size").toString()), 1);
        	sheet.addCell(headDept);
        	
        	Label headShift = new Label(i + 4, 2, data.get(i).get("vshiftname").toString(), content);
        	sheet.addCell(headShift);
        	sheet.setColumnView(i + 4, 17);
        	
        	for (int j = 0; j < pk_hrdepts.length; j++) {
        		if (data.get(i).get("pkHrdept").toString().equals(pk_hrdepts[j])) {
        			for (int k = 0; k < pk_teams.length; k++) {
        				if (data.get(i).get("pkTeam").toString().equals(pk_teams[k]) && 
        						data.get(i).get("pkShift").toString().equals(pk_shifts[k])) {
        					Label value = new Label(i + 4,k + 3,"",generateExcel.contentStyle);
				            sheet.addCell(value);
                		} else {
                			Label value = new Label(i + 4,k + 3,"",empInfo);
				            sheet.addCell(value);
                		}
        			}
        		}
            }
        }
    	
        //遍历list填充表格内容
        String[] employees = data.get(0).get("employee").toString().split(",");
        String[] empnos = data.get(0).get("empno").toString().split(",");
        String[] stations = data.get(0).get("station").toString().split(",");
        String[] kqcardnos = data.get(0).get("kqcardno").toString().split(",");
    	
        for (int i = 0; i < employees.length; i++) {
        	Label headEmpno = new Label(0, i + 3, empnos[i], empInfo);
        	sheet.addCell(headEmpno);
        	
        	Label headEmployee = new Label(1, i + 3, employees[i], empInfo);
        	sheet.addCell(headEmployee);

        	Label headWork = new Label(2, i + 3, stations[i], empInfo);
        	sheet.addCell(headWork);

        	Label headKqcardno = new Label(3, i + 3, kqcardnos[i], empInfo);
        	sheet.addCell(headKqcardno);
        }
        generateExcel.workBook.write();
		return true;
	}
	
	/**
	 * 描述：工作站所有
	 * @author 马振
	 * 创建时间：2015-6-19 上午11:07:35
	 * @param data
	 * @param title
	 * @param dictColumns
	 * @param generateExcel
	 * @param headers
	 * @return
	 * @throws Exception
	 */
	public static boolean exportReportWorkStation(List<Map<String,Object>> data, String title,List<DictColumns> dictColumns,GenerateExcel<?> generateExcel,String headers) throws Exception{
		String dworkdate = "";
		
		WritableSheet sheet = generateExcel.workBook.createSheet(title, 0);
    	WritableFont contentFont = new WritableFont(WritableFont.TIMES, 10, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,Colour.BLACK);
    	
    	WritableCellFormat content = new WritableCellFormat(contentFont);

    	WritableCellFormat style = new WritableCellFormat(contentFont);
		style.setAlignment(jxl.format.Alignment.CENTRE);		//内容居中
		
    	WritableCellFormat contentStyle = new WritableCellFormat(contentFont);
		
    	for (int i = 0; i < data.size(); i++) {
    		if (ValueUtil.IsNotEmpty(data.get(i).get("dworkdate"))) {
    			dworkdate = data.get(i).get("dworkdate").toString();
    		}
    	}
    	
		//标题
		Label label = new Label(0,0,title + "(" + dworkdate + ")",generateExcel.titleStyle);
        sheet.addCell(label);
        sheet.mergeCells(0, 0, 49, 0);	//第一行标题合并单元格(第一列、第一行、第25列、第一行)
        
        //第一列，第二行
        Label head1 = new Label(0,1,"工作站/时间",content);
    	sheet.addCell(head1);
        
        //设置表格表头
        for(int i = 0 ; i < 48;i ++){
        	
        	if (i % 2 == 0) {
        		Label head = new Label(i + 1,1,FormatDouble.formatStringLength("0",i / 2 + "",2) + ":00",style);
            	sheet.addCell(head);
        	} else {
        		Label head = new Label(i + 1,1,FormatDouble.formatStringLength("0",(i - 1) / 2 + "",2) + ":00",style);
            	sheet.addCell(head);
        	}
        	
        	sheet.setColumnView(i + 1, 3);			//设置第i+2列列宽
        	sheet.mergeCells(i + 1, 1, i + 2, 1);
        }
        
        //最后一列
        Label head2 = new Label(49,1,"工时",content);
    	sheet.addCell(head2);
    	
    	sheet.setColumnView(0, 16);		//设置第一列列宽
    	
        //遍历list填充表格内容
        for(int i = 0 ; i < data.size() ; i ++ ){
    		
        	//工作站
        	Label workstation = new Label(0,i + 2,data.get(i).get("workstation").toString(),contentStyle);
            sheet.addCell(workstation);
            
            for (int k = 0; k < 48; k++) {
				
    			if (k % 2 == 0) {
					if (ValueUtil.IsNotEmpty(data.get(i).get("vbegintime" + FormatDouble.formatStringLength("0",(k / 2) + "",2) + "00"))) {
						if (data.get(i).get("vbegintime" + FormatDouble.formatStringLength("0",(k / 2) + "",2) + "00").toString().equals(FormatDouble.formatStringLength("0",(k / 2) + "",2) + ":00")) {
							Label npeople = new Label(k + 1,i + 2,data.get(i).get("npeople" + FormatDouble.formatStringLength("0",(k / 2) + "",2) + "00").toString(),generateExcel.contentStyle);
				            sheet.addCell(npeople);
				            sheet.mergeCells(k + 1, i + 2, k + Integer.parseInt(data.get(i).get("vendtime" + FormatDouble.formatStringLength("0",(k / 2) + "",2) + "00").toString()), i + 2);
						} else {
							Label npeople = new Label(k + 1,i + 2,"",content);
				            sheet.addCell(npeople);
						}
					}
				} else {
					if (ValueUtil.IsNotEmpty(data.get(i).get("vbegintime" + FormatDouble.formatStringLength("0",(k / 2) + "",2) + "30"))) {
						if (data.get(i).get("vbegintime" + FormatDouble.formatStringLength("0",(k / 2) + "",2) + "30").toString().equals(FormatDouble.formatStringLength("0",(k / 2) + "",2) + ":30")) {
							Label npeople = new Label(k + 1,i + 2,data.get(i).get("npeople" + FormatDouble.formatStringLength("0",(k / 2) + "",2) + "30").toString(),generateExcel.contentStyle);
				            sheet.addCell(npeople);
				            sheet.mergeCells(k + 1, i + 2, k + Integer.parseInt(data.get(i).get("vendtime" + FormatDouble.formatStringLength("0",(k / 2) + "",2) + "30").toString()), i + 2);
						} else {
							Label npeople = new Label(k + 1,i + 2,"",content);
				            sheet.addCell(npeople);
						}	
					}
				}
			}
            
            if (ValueUtil.IsNotEmpty(data.get(i).get("totalhour"))) {
            	//工时
            	Label totalhour = new Label(49,i + 2,data.get(i).get("totalhour").toString(),contentStyle);
                sheet.addCell(totalhour);
            }
        }
        generateExcel.workBook.write();
		return true;
	}
}