package com.choice.misboh.commonutil.publish;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.sql.Blob;
import java.util.StringTokenizer;

import oracle.sql.BLOB;

import com.choice.misboh.commonutil.ItemPath;

/**
 * 描述：文件操作方法
 * @author 马振
 * 创建时间：2015-8-26 下午5:37:22
 */
@SuppressWarnings("resource")
public class FileUtils {
	private String message;

	public FileUtils() {
	}

	/**
	 * 描述：读取文本文件内容
	 * @author 马振
	 * 创建时间：2015-8-26 下午5:37:37
	 * @param filePathAndName	带有完整绝对路径的文件名
	 * @param encoding			文本文件打开的编码方式
	 * @return					返回文本文件的内容
	 * @throws IOException
	 */
	public String readTxt(String filePathAndName, String encoding) throws IOException {
		encoding = encoding.trim();
		StringBuffer str = new StringBuffer("");
		String st = "";
		try {
			FileInputStream fs = new FileInputStream(filePathAndName);
			InputStreamReader isr;
			if (encoding.equals("")) {
				isr = new InputStreamReader(fs);
			} else {
				isr = new InputStreamReader(fs, encoding);
			}
			BufferedReader br = new BufferedReader(isr);
			try {
				String data = "";
				while ((data = br.readLine()) != null) {
					str.append(data + " ");
				}
			} catch (Exception e) {
				str.append(e.toString());
			}
			st = str.toString();
		} catch (IOException es) {
			st = "";
		}
		return st;
	}

	/**
	 * 描述：新建目录
	 * @author 马振
	 * 创建时间：2015-8-26 下午5:38:15
	 * @param folderPath	目录
	 * @return				返回目录创建后的路径
	 */
	public String createFolder(String folderPath) {
		String txt = folderPath;
		try {
			java.io.File myFilePath = new java.io.File(txt);
			txt = folderPath;
			if (!myFilePath.exists()) {
				myFilePath.mkdir();
			}
		} catch (Exception e) {
			message = "创建目录操作出错";
		}
		return txt;
	}

	/**
	 * 描述：多级目录创建
	 * @author 马振
	 * 创建时间：2015-8-26 下午5:38:45
	 * @param folderPath	准备要在本级目录下创建新目录的目录路径 例如 c:myf
	 * @param paths			无限级目录参数，各级目录以单数线区分 例如 a|b|c
	 * @return				返回创建文件后的路径 例如 c:myfac
	 */
	public String createFolders(String folderPath, String paths) {
		String txts = folderPath;
		try {
			String txt;
			txts = folderPath;
			StringTokenizer st = new StringTokenizer(paths, "|");
			for (; st.hasMoreTokens();) {
				txt = st.nextToken().trim();
				if (txts.lastIndexOf("/") != -1) {
					txts = createFolder(txts + txt);
				} else {
					txts = createFolder(txts + txt + "/");
				}
			}
		} catch (Exception e) {
			message = "创建目录操作出错！";
		}
		return txts;
	}

	/**
	 * 描述：新建文件
	 * @author 马振
	 * 创建时间：2015-8-26 下午5:39:21
	 * @param filePathAndName	文本文件完整绝对路径及文件名
	 * @param fileContent		文本文件内容
	 */
	public void createFile(String filePathAndName, String fileContent) {

		try {
			String filePath = filePathAndName;
			filePath = filePath.toString();
			File myFilePath = new File(filePath);
			if (!myFilePath.exists()) {
				myFilePath.createNewFile();
			}
			FileWriter resultFile = new FileWriter(myFilePath);
			PrintWriter myFile = new PrintWriter(resultFile);
			String strContent = fileContent;
			myFile.println(strContent);
			myFile.close();
			resultFile.close();
		} catch (Exception e) {
			message = "创建文件操作出错";
		}
	}

	/**
	 * 描述：有编码方式的文件创建
	 * @author 马振
	 * 创建时间：2015-8-26 下午5:39:42
	 * @param filePathAndName	文本文件完整绝对路径及文件名
	 * @param fileContent		文本文件内容
	 * @param encoding			编码方式 例如 GBK 或者 UTF-8
	 */
	public void createFile(String filePathAndName, String fileContent,
			String encoding) {

		try {
			String filePath = filePathAndName;
			filePath = filePath.toString();
			File myFilePath = new File(filePath);
			if (!myFilePath.exists()) {
				myFilePath.createNewFile();
			}
			PrintWriter myFile = new PrintWriter(myFilePath, encoding);
			String strContent = fileContent;
			myFile.println(strContent);
			myFile.close();
		} catch (Exception e) {
			message = "创建文件操作出错";
		}
	}

	/**
	 * 
	 * 
	 * @param filePathAndName
	 *            
	 * @return Boolean 
	 */
	/**
	 * 描述：删除文件
	 * @author 马振
	 * 创建时间：2015-8-26 下午5:40:10
	 * @param filePathAndName	文本文件完整绝对路径及文件名
	 * @return					成功删除返回true遭遇异常返回false
	 */
	public boolean delFile(String filePathAndName) {
		boolean bea = false;
		try {
			String filePath = filePathAndName;
			File myDelFile = new File(filePath);
			if (myDelFile.exists()) {
				myDelFile.delete();
				bea = true;
			} else {
				bea = false;
				message = (filePathAndName + "删除文件操作出错！");
			}
		} catch (Exception e) {
			message = e.toString();
		}
		return bea;
	}

	/**
	 * 描述：删除文件夹
	 * @author 马振
	 * 创建时间：2015-8-26 下午5:40:41
	 * @param folderPath	文件夹完整绝对路径
	 */
	public void delFolder(String folderPath) {
		try {
			delAllFile(folderPath); // 删除完里面所有内容
			String filePath = folderPath;
			filePath = filePath.toString();
			java.io.File myFilePath = new java.io.File(filePath);
			myFilePath.delete(); // 删除空文件夹
		} catch (Exception e) {
			message = ("删除文件夹操作出错");
		}
	}

	/**
	 * 描述：删除指定文件夹下所有文件
	 * @author 马振
	 * 创建时间：2015-8-26 下午5:40:54
	 * @param path	文件夹完整绝对路径
	 * @return
	 */
	public boolean delAllFile(String path) {
		boolean bea = false;
		File file = new File(path);
		if (!file.exists()) {
			return bea;
		}
		if (!file.isDirectory()) {
			return bea;
		}
		String[] tempList = file.list();
		File temp = null;
		for (int i = 0; i < tempList.length; i++) {
			if (path.endsWith(File.separator)) {
				temp = new File(path + tempList[i]);
			} else {
				temp = new File(path + File.separator + tempList[i]);
			}
			if (temp.isFile()) {
				temp.delete();
			}
			if (temp.isDirectory()) {
				delAllFile(path + "/" + tempList[i]);	// 先删除文件夹里面的文件
				delFolder(path + "/" + tempList[i]);	// 再删除空文件夹
				bea = true;
			}
		}
		return bea;
	}

	/**
	 * 描述：复制单个文件
	 * @author 马振
	 * 创建时间：2015-8-26 下午5:41:18
	 * @param oldPathFile	准备复制的文件源
	 * @param newPathFile	拷贝到新绝对路径带文件名
	 */
	@SuppressWarnings("unused")
	public void copyFile(String oldPathFile, String newPathFile) {
		try {
			int bytesum = 0;
			int byteread = 0;
			File oldfile = new File(oldPathFile);
			if (oldfile.exists()) { // 文件存在时
				InputStream inStream = new FileInputStream(oldPathFile); // 读入原文件
				FileOutputStream fs = new FileOutputStream(newPathFile);
				byte[] buffer = new byte[4096];
				while ((byteread = inStream.read(buffer)) != -1) {
					bytesum += byteread; // 字节数 文件大小
					fs.write(buffer, 0, byteread);
				}
				inStream.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			message = ("复制单个文件操作出错");
		}
	}
	
	/**
	 * 描述：复制整个文件夹的内容
	 * @author 马振
	 * 创建时间：2015-8-26 下午5:42:01
	 * @param oldPath	准备拷贝的目录
	 * @param newPath	指定绝对路径的新目录
	 */
	public void copyFolder(String oldPath, String newPath) {
		try {
			new File(newPath).mkdirs(); // 如果文件夹不存在 则建立新文件夹
			File a = new File(oldPath);
			String[] file = a.list();
			File temp = null;
			for (int i = 0; i < file.length; i++) {
				if (oldPath.endsWith(File.separator)) {
					temp = new File(oldPath + file[i]);
				} else {
					temp = new File(oldPath + File.separator + file[i]);
				}
				if (temp.isFile()) {
					FileInputStream input = new FileInputStream(temp);
					FileOutputStream output = new FileOutputStream(newPath
							+ "/" + (temp.getName()).toString());
					byte[] b = new byte[1024 * 5];
					int len;
					while ((len = input.read(b)) != -1) {
						output.write(b, 0, len);
					}
					output.flush();
					output.close();
					input.close();
				}
				if (temp.isDirectory()) {// 如果是子文件夹
					copyFolder(oldPath + "/" + file[i], newPath + "/" + file[i]);
				}
			}
		} catch (Exception e) {
			message = "复制整个文件夹内容操作出错";
		}
	}

	/**
	 * 描述：移动文件
	 * @author 马振
	 * 创建时间：2015-8-26 下午5:42:24
	 * @param oldPath
	 * @param newPath
	 */
	public void moveFile(String oldPath, String newPath) {
		copyFile(oldPath, newPath);
		delFile(oldPath);
	}

	/**
	 * 描述：移动目录
	 * @author 马振
	 * 创建时间：2015-8-26 下午5:42:34
	 * @param oldPath
	 * @param newPath
	 */
	public void moveFolder(String oldPath, String newPath) {
		copyFolder(oldPath, newPath);
		delFolder(oldPath);
	}

	public String getMessage() {
		return this.message;
	}
	
	/**
	 * 描述：数据库读取BLOB对象
	 * @author 马振
	 * 创建时间：2015-8-26 下午5:42:45
	 * @param blobContent
	 * @return
	 */
	public String  readFileBlob(Blob blobContent){
		String memo = "";
		String fileName = "blob_temp.notice";
		 String filepath = ItemPath.getItemRootPath() + "temp/";
		    if(!new File(filepath).exists()){
		    	new File(filepath).mkdir();
		    }
		    if(blobContent == null){
		    	return "";
		    }
			oracle.sql.BLOB blob = (BLOB) blobContent;
			try {
				 InputStream in = blob.getBinaryStream(); // 建立输出流
		         FileOutputStream file = new FileOutputStream(filepath+fileName);
		         int len = (int) blob.length();
		         byte[] buffer = new byte[len]; // 建立缓冲区
		         while ( (len = in.read(buffer)) != -1) {
		           file.write(buffer, 0, len);
		         }
		         Reader  reader = new InputStreamReader(new FileInputStream(filepath+fileName),"GB2312");
		         int tempchar;
		         while ((tempchar = reader.read()) != -1) {
		         	memo+=(char)tempchar;
		         }
		         file.close();
		         in.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		 new File(filepath+fileName).delete();
         return memo;
	}
}