package com.choice.misboh.commonutil.publish;

import java.sql.Connection;
import java.sql.Statement;

/**
 * 描述：类说明
 * @author 马振
 * 创建时间：2015-8-26 下午5:47:04
 */
public class TheMainMethod {
	
	/**
	 * 描述：读取ipadmain.xml配置文件，并生成对应的insert语句
	 * @author 马振
	 * 创建时间：2015-8-26 下午5:47:14
	 * @param sourcepath	ipadmain.xml 和*.propertis所在的路径
	 * @param scode			门店编号
	 * @param path			要读取的xml所在的路径（发布生成的xml所在的路径 product_sub.xml）
	 * @return
	 */
	public String readConfigXml(String sourcepath,String scode,String path){
		String filesql = "";
		String insertsql = "";
		try {
			//根据ipadmain.xml配置获取需同步数据的插入语句
			insertsql = new ReadXmlUtil().readXmls(sourcepath + "/ipadsources", sourcepath+"/ipadsources/ipadmain.xml", "ipadRely" , "ipadmain",path);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return filesql+insertsql;
	}
	
	/**
	 * 描述：将生成的insert语句插入到sqlite中
	 * @author 马振
	 * 创建时间：2015-8-26 下午5:48:16
	 * @param conn		sqlite数据库连接
	 * @param filesql	sql语句
	 * @return
	 * @throws Exception
	 */
	public boolean confirm(Connection conn,String filesql)throws Exception{
		boolean b=true;
		Statement pstmt = null;
		try {
			int batchNum = 500;
			pstmt = conn.createStatement();
			String[] localsql = filesql.split("##");
			conn.setAutoCommit(false);   
			for (int i = 0; i <localsql.length -1 ;i++ ) {
				if (localsql[i]!=null && localsql[i].length()>0) {
					pstmt.addBatch(localsql[i]);
					if ((i+1) % batchNum == 0) {
						pstmt.executeBatch();
						conn.commit();
					}
				}
			}
			pstmt.executeBatch();
			conn.commit();
			if (null != pstmt) {
				pstmt.close();
			}
		} finally {
			if (null != pstmt) {
				pstmt.close();
			}
			if (null != conn) {
				conn.close();
			}
			
		}
		return b;
	}
}