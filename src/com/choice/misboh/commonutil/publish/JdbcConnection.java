package com.choice.misboh.commonutil.publish;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import net.sf.json.JSONObject;

public class JdbcConnection {
	
	/**
	 * 描述：连接
	 * @author 马振
	 * 创建时间：2015-8-26 下午5:56:39
	 * @param url		数据连接
	 * @param user		用户名
	 * @param password	密码
	 * @param driver	数据库驱动
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public Connection addConnection(String url,String user,String password,String driver) throws SQLException, ClassNotFoundException{	
		Connection conn = null;
		Class.forName(driver);
		if(user.equals("")){
			conn =  DriverManager.getConnection(url);
		}else{
			conn = DriverManager.getConnection(url, user, password);
		}
		return conn;
	}

	/**
	 * 描述：连接
	 * @author 马振
	 * 创建时间：2015-8-26 下午5:57:42
	 * @param object	数据库连接对象
	 * @return
	 * @throws ClassNotFoundException
	 */
	public Connection addConnection(JSONObject object) throws ClassNotFoundException {
		String driver = object.get("DRIVERCLASSNAME").toString();
		String url = object.get("URL").toString();
		String user = object.get("USERNAME").toString();
		String pass = object.get("PASSWORD").toString();
		Class.forName(driver);
		Connection conn = null;
		try {
			if(user.equals("")){
				conn =  DriverManager.getConnection(url);
			}else{
				conn = DriverManager.getConnection(url, user, pass);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}
}