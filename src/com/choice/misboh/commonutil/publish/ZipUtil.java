package com.choice.misboh.commonutil.publish;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Enumeration;

import org.apache.log4j.Logger;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;
import org.apache.tools.zip.ZipOutputStream;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DateFormat;

/**
 * 描述：生成zip文件
 * @author 马振
 * 创建时间：2015-8-26 下午6:24:03
 */
public class ZipUtil implements Runnable{


	private static Logger log = Logger.getLogger(ZipUtil.class);
	private String zipFileName;
	private String fileExtensions;
	private File inputFile;
	
	public ZipUtil() {
	}
	
	public ZipUtil(String zipFileName, String fileExtensions, File inputFile) {
		this.zipFileName = zipFileName;
		this.fileExtensions = fileExtensions;
		this.inputFile = inputFile;
	}
	
	@Override
	public synchronized void run() {
		try {
			zip(zipFileName, fileExtensions, inputFile.getPath(), inputFile.getName());
			System.out.println(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss")+";门店"+inputFile.getName()+fileExtensions+" done");
		} catch (IOException e) {
			log.error(e);
			throw new RuntimeException(e);   
		} catch (CRUDException e) {
			log.error(e);
			e.printStackTrace();
		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
		}
	}
	
	/**
	 * 描述：压缩成zip
	 * @author 马振
	 * 创建时间：2015-8-26 下午6:23:29
	 * @param zipFilep	压缩后的zip文件名
	 * @param fileExtensions
	 * @param path		压缩路径
	 * @param fileName
	 * @throws Exception
	 */
	public void zip(String zipFilep,String fileExtensions, String path, String fileName)throws Exception  {
		File zipFile = new File(zipFilep);
		if (!zipFile.exists()) {
			zipFile.createNewFile();
		}
		FileOutputStream fileOutputStream = null;
		ZipOutputStream out = null;
		try {
			fileOutputStream = new FileOutputStream(zipFile);
			out = new ZipOutputStream(fileOutputStream);
			write(out, path, fileName);
			out.setEncoding("gbk"); //设置防止压缩时中文文件名乱码
		} catch (Exception e) {
			throw new Exception();
		}finally{
			try {
				out.close();
			} catch (Exception e) {
			}
			try {
				fileOutputStream.close();
			} catch (Exception e) {
			}
		}
		
	}

	/**
	 * 描述：写压缩流
	 * @author 马振
	 * 创建时间：2015-8-26 下午6:23:02
	 * @param out	压缩输出流
	 * @param path	压缩路径
	 * @param base	压缩式的基础目录
	 * @throws Exception
	 */
	private void write(ZipOutputStream out, String path, String base) throws Exception {
		File file = new File(path);
		
		if (file.isDirectory()) {// 文件夹，递归
			base = base.length() == 0 ? "" : base + File.separator;
			File[] tempFiles = file.listFiles();
			for (int i = 0; i < tempFiles.length; i++) {
				write(out, tempFiles[i].getPath(), base
						+ tempFiles[i].getName());
			}
		} else {// 文件，压缩
			
			if(base.equals("")){
				base = file.getName();
			}
			
			byte[] buff = new byte[2048];
			int bytesRead = -1;
			ZipEntry entry = new ZipEntry(base);
			out.putNextEntry(entry);
			FileInputStream in2 = new FileInputStream(file);
			InputStream in = new BufferedInputStream(in2);
			while (-1 != (bytesRead = in.read(buff, 0, buff.length))) {
				out.write(buff, 0, bytesRead);
			}
			in.close();
			in2.close();
			out.flush();
		}
	}

	/**
	 * 描述：解压缩
	 * @author 马振
	 * 创建时间：2015-8-26 下午6:20:16
	 * @param zipFilename	zip文件
	 * @param destPath		解压缩路径
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public void unZip(String zipFilename, String destPath) throws Exception {
		ZipFile zip = new ZipFile(zipFilename, "GB2312");
		try {
			Enumeration enu = zip.getEntries();// 得到压缩文件夹中的所有文件
			while (enu.hasMoreElements()) {
				ZipEntry entry = (ZipEntry) enu.nextElement();
				String file = destPath + entry.getName();
				write(zip, entry, file);
				
				entry.clone();
			}
		} catch (Exception e) {
			throw new Exception();
		}finally{
			try{zip.close();}catch(Exception e){}
		}
	}

	/**
	 * 描述：将解压缩之后的文件写入目录
	 * @author 马振
	 * 创建时间：2015-8-26 下午6:20:37
	 * @param zip	压缩文件
	 * @param entry	压缩文件实体——压缩文件中的文件
	 * @param file	解压缩之后的文件路径
	 * @throws Exception
	 */
	private void write(ZipFile zip, ZipEntry entry, String file) throws Exception {
		if (entry.isDirectory()) {
			File f = new File(file);
			f.mkdirs();
		} else {
			File f = new File(file);
			createDir(f);
			FileOutputStream fos = null;
			InputStream is = null;
			
			try {
				fos = new FileOutputStream(f);
				byte[] buffer = new byte[8196];
				is = zip.getInputStream(entry);
				
				for (int len = is.read(buffer, 0, buffer.length); len != -1; len = is
						.read(buffer, 0, 8196)) {
					fos.write(buffer, 0, len);
				}
			} catch (Exception e) {
				throw new  Exception();
			} finally {
				try {
					is.close();
				} catch (Exception e) {
				}
				
				try {
					fos.close();
				} catch (Exception e) {
				}
			}
		}
	}

	/**
	 * 描述：创建目录
	 * @author 马振
	 * 创建时间：2015-8-26 下午6:22:06
	 * @param file	文件或目录
	 */
	private void createDir(File file) {
		if (file.isDirectory() && !file.exists()) {
			file.mkdirs();
		} else {
			String path = file.getPath();
			int i = path.lastIndexOf(File.separator);
			path = path.substring(0, i);
			new File(path).mkdirs();
		}
	}
}