package com.choice.misboh.commonutil.publish;

import java.util.Map;

import com.choice.misboh.domain.publish.PublishContent;

/**
 * 描述：数据储存
 * @author 马振
 * 创建时间：2015-8-25 上午11:10:48
 */
public class DataForPublish {
	private static Map<String,PublishContent> publishMap = null;
	private static String path = null;
	private static String result = null;
	private static int count = 0;
	
	public static Map<String, PublishContent> getPublishMap() {
		return publishMap;
	}

	public static void setPublishMap(Map<String, PublishContent> publishMap) {
		DataForPublish.publishMap = publishMap;
	}

	public static String getPath() {
		return path;
	}

	public static void setPath(String path) {
		DataForPublish.path = path;
	}

	public static String getResult() {
		return result;
	}

	public static void setResult(String result) {
		DataForPublish.result = result;
	}

	public static int getCount() {
		return count;
	}

	public static void setCount(int count) {
		DataForPublish.count = count;
	}
}