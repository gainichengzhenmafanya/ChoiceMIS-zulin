package com.choice.misboh.commonutil.publish;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.choice.misboh.commonutil.DateJudge;

/**
 * 描述：类说明
 * @author 马振
 * 创建时间：2015-8-26 下午5:49:14
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class ReadXmlUtil implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Document document;
	private SAXReader saxReader;
	private static Logger log = Logger.getLogger(ReadXmlUtil.class);
	/**
	 * 描述：这里用一句话描述这个方法的作用
	 * @author 马振
	 * 创建时间：2015-8-26 下午5:49:26
	 * @param xmlFile
	 * @param xmlnode
	 * @param praentnode
	 * @return
	 * @throws Exception	
	 */
	public String readXml(String xmlFile, String xmlnode, String praentnode) throws Exception {
		String insertsql = "";
		
		saxReader = new SAXReader();
		document = saxReader.read(new File(xmlFile));
		Element rootElt = document.getRootElement(); // 获取根节点
		Iterator iter = rootElt.element(praentnode).elementIterator(xmlnode); // 获取根节点下的子节点xmlnode
		
		
		String filename="";		//同步数据配置文件名称
		String conmment="";		//配置文件说明
		String fromxml="";		//同步数据，数据来源对应xml，即下发数据包中的xml
		String fromxmlnode=""; 	//xml中读取数据的节点
		String totable="";		//同步到那个表（表名称）
		
		// 遍历Screen节点
		while (iter.hasNext()) {
			Element recordEle = (Element) iter.next();
			List<Attribute> attributes = recordEle.attributes();
			
			//读取xml获取值；
			for (Attribute attr : attributes) {
				if (attr.getName().toUpperCase().equals("FILENAME")) {
					filename = attr.getValue();
				} else if (attr.getName().toUpperCase().equals("CONMMENT")) {
					conmment = attr.getValue();
				} else if (attr.getName().toUpperCase().equals("FROMXML")) {
					fromxml = attr.getValue();
				} else if (attr.getName().toUpperCase().equals("FROMXMLNODE")) {
					fromxmlnode = attr.getValue();
				} else if (attr.getName().toUpperCase().equals("TOTABLE")) {
					totable = attr.getValue();
				} 
			}
			
			//文件不存在，返回继续；
			if (!new File(filename).exists()) {
				log.info(filename+"文件不存在\r\n");
				continue;
			}
			//文件不存在，返回继续；
			if (!new File(fromxml).exists()) {
				log.info(fromxml+"文件不存在\r\n");
				continue;
			}
			
			//读取文件获得sql
			insertsql += readProperties(filename, conmment, fromxml, fromxmlnode, totable);
			
		}
		return insertsql;
	}
	
	/**
	 * 描述：
	 * @author 马振
	 * 创建时间：2015-8-26 下午5:50:51
	 * @param sourcepath
	 * @param xmlFile
	 * @param xmlnode
	 * @param praentnode
	 * @param path
	 * @return
	 * @throws Exception
	 */
	public String readXmls(String sourcepath,String xmlFile, String xmlnode, String praentnode,String path) throws Exception {
		
		String insertsql = "";
		
		saxReader = new SAXReader();
		document = saxReader.read(new File(xmlFile));
		Element rootElt = document.getRootElement(); // 获取根节点
		Iterator iter = rootElt.element(praentnode).elementIterator(xmlnode); // 获取根节点下的子节点xmlnode
		
		
		String filename="";//同步数据配置文件名称
		String conmment="";//配置文件说明
		String fromxml="";//同步数据，数据来源对应xml，即下发数据包中的xml
		String fromxmlnode=""; //xml中读取数据的节点
		String totable="";//同步到那个表（表名称）
		
		// 遍历Screen节点
		while (iter.hasNext()) {
			Element recordEle = (Element) iter.next();
			List<Attribute> attributes = recordEle.attributes();
			
			//读取xml获取值；
			for (Attribute attr : attributes) {
				if (attr.getName().toUpperCase().equals("FILENAME")) {
					filename = sourcepath + "/" + attr.getValue();
					if(!new File(filename).exists()){
						filename = sourcepath + "\\" + attr.getValue();
					}
				} else if (attr.getName().toUpperCase().equals("CONMMENT")) {
					conmment = attr.getValue();
				} else if (attr.getName().toUpperCase().equals("FROMXML")) {
					fromxml = path + "/" +attr.getValue();
					if(!new File(fromxml).exists()){
						fromxml = path + "\\" +attr.getValue();
					}
				} else if (attr.getName().toUpperCase().equals("FROMXMLNODE")) {
					fromxmlnode = attr.getValue();
				} else if (attr.getName().toUpperCase().equals("TOTABLE")) {
					totable = attr.getValue();
				} 
			}
			
			//文件不存在，返回继续；
			if (!new File(filename).exists()) {
				log.info(filename+"文件不存在\r\n");
				continue;
			}
			//文件不存在，返回继续；
			if (!new File(fromxml).exists()) {
				log.info(fromxml+"文件不存在\r\n");
				continue;
			}
			
			//读取文件获得sql
			insertsql += readProperties(filename, conmment, fromxml, fromxmlnode, totable);
			
		}
		return insertsql;
	}
	
	/**
	 * 描述：获取xml中节点数量，此节点为数据同步pad库连接配置，程序执行目录配置
	 * @author 马振
	 * 创建时间：2015-8-26 下午5:51:19
	 * @param xmlFile	配置xml文件路径
	 * @param xmlnode	获取数据节点
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, String>> getConnCanshu(String xmlFile, String xmlnode) throws Exception {
		saxReader = new SAXReader();
		document = saxReader.read(new File(xmlFile));
		Element rootElt = document.getRootElement(); 		// 获取根节点
		Iterator iter = rootElt.elementIterator(xmlnode); 	// 获取根节点下的子节点xmlnode
		
		Map<String, String> tmpmap = new HashMap<String, String>();
		List<Map<String, String>> maplist = new ArrayList<Map<String, String>>();
		
		// 遍历xmlnode节点
		while (iter.hasNext()) {
			Element recordEle = (Element) iter.next();
			List<Attribute> attributes = recordEle.attributes();
			
			for (Attribute attr : attributes) {
				tmpmap.put(attr.getName().toUpperCase(), attr.getValue());
			}
			maplist.add(tmpmap);
		}
		return maplist;
	}
	
	/**
	 * 描述：读取配置文件，获得节点对应数据
	 * @author 马振
	 * 创建时间：2015-8-26 下午5:52:08
	 * @param filename		同步数据配置文件名称
	 * @param conmment		配置文件说明
	 * @param fromxml		同步数据，数据来源对应xml，即下发数据包中的xml
	 * @param fromxmlnode	xml中读取数据的节点
	 * @param totable		同步到那个表（表名称）
	 * @return
	 * @throws Exception
	 */
	private String readProperties(String filename, String conmment, String fromxml, String fromxmlnode, String totable) throws Exception {
		String returnfromxml = "";
		String returnValue = "insert into " + totable + " ( ";//插入语句拼装
		Properties properties = new Properties();
		FileInputStream in = new FileInputStream(filename);
		InputStreamReader sr = new InputStreamReader(in, "UTF-8");
		properties.load(sr);
		
		Enumeration enu=properties.keys(); 
		while(enu.hasMoreElements()){ 
			//获得一个key 
			String key=enu.nextElement().toString(); 
			
			if(key.contains("#")) {
				continue;
			}
			//获得 该key 对应的  value 
			String value=properties.getProperty(key); 
			//将 key 和 value 以utf-8的方式进行编码转换 
			try { 
				key=new String(key.getBytes("UTF-8"),"UTF-8"); 
				value=new String(value.getBytes("UTF-8"),"UTF-8"); 
				returnValue += key.toUpperCase() + ", ";
				returnfromxml += "'~_" +value.toUpperCase() + "_~', ";//SQL中VALUES（值域）用字段占位 
			} catch (Exception e) { 
				System.out.println("编码失败！"); 
				e.printStackTrace(); 
			} 
		}
		
		//截取最后一位（去点，）
		if (returnValue.contains(",")) {
			returnValue = returnValue.substring(0, returnValue.lastIndexOf(","));
			returnfromxml = returnfromxml.substring(0, returnfromxml.lastIndexOf(","));
		}
		returnValue = returnValue+ ") values ";
		
		//调用方法，读取xml获得插入语句值域
		return readXml(returnValue , fromxml, fromxmlnode, returnfromxml, totable);
		
	}
	
	/**
	 * 描述：读取xml获得插入语句值域
	 * @author 马振
	 * 创建时间：2015-8-26 下午5:53:17
	 * @param inserthead	插入语句头
	 * @param fromxml		插入语句值域数据来源xml
	 * @param fromxmlnode	xml中读取数据的节点
	 * @param returnfromxml	插入语句中值域的占位字符串
	 * @param totable		所需插入数据的表
	 * @return
	 * @throws Exception
	 */
	private String readXml(String inserthead ,String fromxml, String fromxmlnode, String returnfromxml, String totable) throws Exception {
		String returnValue = "";
		String loaclStr = "";
		saxReader = new SAXReader();
		document = saxReader.read(new File(fromxml));
		if (fromxml.contains("table.xml")) {//xml来源判定（如果是table.xml之前需要同步台位区域数据，现已不适用此同步台位区域数据）
			
			if (fromxmlnode.contains(":")) {//节点中包含“:”的为台位区域数据同步需特殊处理
				String[] xmlnode = fromxmlnode.split(":");
				Element rootElt = document.getRootElement(); 		// 获取根节点
				Iterator iter = rootElt.elementIterator(xmlnode[0]);// 获取根节点下的子节点
				
				// 遍历Screen节点
				while (iter.hasNext()) {
					String tempvalue = returnfromxml;
					Element recordEle = (Element) iter.next();
					Iterator iterbutton = recordEle.elementIterator(xmlnode[1]); // 获取根节点下的子节点
					
					// 遍历Screen节点
					while (iterbutton.hasNext()) {
						loaclStr = tempvalue;
						returnValue += inserthead + "( ";
						Element recordbutton = (Element) iterbutton.next();
						List<Attribute> attributesButton = recordbutton.attributes();
						
						for (Attribute attrbu : attributesButton) {
							loaclStr = loaclStr.replace("~_"+attrbu.getName().toUpperCase()+"_~", attrbu.getValue());//替换值域数据
						}
						returnValue += loaclStr+ ") ## \r\n";
					}
				}
			} else { //不包含则为同步台位数据
				Element rootElt = document.getRootElement(); 			// 获取根节点
				Iterator iter = rootElt.elementIterator(fromxmlnode); 	// 获取根节点下的子节点Screen
				
				// 遍历Screen节点
				while (iter.hasNext()) {
					loaclStr = returnfromxml;
					returnValue += inserthead + "( ";
					Element recordEle = (Element) iter.next();
					List<Attribute> attributes = recordEle.attributes();
					
					for (Attribute attr : attributes) {
						loaclStr = loaclStr.replace("~_"+attr.getName().toUpperCase()+"_~", attr.getValue());//替换值域数据
					}
					returnValue += loaclStr+ ") ## \r\n";
				}
			}
		} else if (fromxml.toUpperCase().contains("PRODUCT.XML")) { 
			
			//为PRODUCT.XML时需特殊处理，门店对应多方案时需处理数据，不能使菜品重复，根据方案日期，
			//判定当前日期处于哪个方案时间段，如果处于多个时间段，需根据优先级判定，删除重复数据
			Element rootElt = document.getRootElement(); // 获取根节点
			Iterator iter = rootElt.elementIterator(fromxmlnode); // 获取根节点下的子节点Screen
			String workdate = DateJudge.YYYY_MM_DD.format(new Date());
			
			// 遍历PRODUCT节点
			while (iter.hasNext()) {
				loaclStr = returnfromxml;
				//获取删除语句
				String deletesql = "delete from " + totable + " where itcode = '~_PCODE_~' AND PRI < (SELECT MAX(PRI)  FROM FOOD WHERE ITCODE = '~_PCODE_~')";
				String tmpreturn = inserthead + "( ";
				
				Element recordEle = (Element) iter.next();
				List<Attribute> attributes = recordEle.attributes();
				
				for (Attribute attr : attributes) {
					loaclStr = loaclStr.replace("~_"+attr.getName().toUpperCase()+"_~", attr.getValue());
					//判断当前日期与方案开始日期大小
					if (attr.getName().toUpperCase().equals("STIME")) {
						if (DateJudge.timeCompare(workdate, attr.getValue())) {
							continue;
						}
					}
					//判断当前日期与方案结束日期大小
					if (attr.getName().toUpperCase().equals("ETIME")  ) {
						if (DateJudge.timeCompare(attr.getValue(), workdate)) {
							continue;
						}
					}
					if (attr.getName().toUpperCase().equals("PRI")||attr.getName().toUpperCase().equals("PCODE")) {
						deletesql = deletesql.replace("~_"+attr.getName().toUpperCase()+"_~", attr.getValue()); //值域替换
					}
				}
				tmpreturn += loaclStr+ ") ## \r\n";
				
				//获取更新语句
				returnValue +=  tmpreturn;
				
				//获取删除语句
				returnValue += deletesql + " ## \r\n";
			}
		} else {//值域替换通用方法
			Element rootElt = document.getRootElement(); // 获取根节点
			Iterator iter = rootElt.elementIterator(fromxmlnode); // 获取根节点下的子节点
			
			// 遍历节点
			while (iter.hasNext()) {
				loaclStr = returnfromxml;
				returnValue += inserthead + "( ";
				Element recordEle = (Element) iter.next();
				List<Attribute> attributes = recordEle.attributes();
				
				for (Attribute attr : attributes) {
					loaclStr = loaclStr.replace("~_"+attr.getName().toUpperCase()+"_~", attr.getValue());
				}
				returnValue += loaclStr+ ") ## \r\n";
			}
		}
		
		if (!returnValue.equals("") && returnValue.length()>0) {
			returnValue = "delete from " + totable + " ## \r\n" + returnValue;//在插入语句前加上整个表的删除语句
		}
		return returnValue;
	}
	
	public static void main(String[] args) {
		try {
			new ReadXmlUtil().readXml("ipadsources/ipadmain.xml", "ipadRely" ,"ipadmain");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}