package com.choice.misboh.commonutil.publish;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.aspectj.util.FileUtil;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.ForResourceFiles;
import com.choice.misboh.commonutil.ItemPath;
import com.choice.misboh.commonutil.MisUtil;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.domain.publish.PublishContent;
import com.choice.misboh.domain.store.Store;

/**
 * 描述：数据下发工具类
 * @author 马振
 * 创建时间：2015-8-25 上午11:01:56
 */
@SuppressWarnings("resource")
public  class PublishUtil{
	
	private static Logger log = Logger.getLogger(PublishUtil.class);
	private static FileLock locker = null;
	private static String url = ForResourceFiles.getValByKey("config-bsboh.properties", "coverPath");

	/**
	 * 描述：生成XML
	 * @author 马振
	 * 创建时间：2015-8-25 上午11:02:10
	 * @param data				要生成XML的数据
	 * @param publishContent	数据定义类型
	 * @param store				门店编号
	 * @throws CRUDException
	 */
	public void toXml(List<Map<String,Object>> data, PublishContent publishContent,String store) throws CRUDException{
		try {
			exportToXml(publishContent, url + "/" + store, data); //在以门店编码命名的文件夹下
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：批量创建门店文件夹
	 * @author 马振
	 * 创建时间：2015-8-25 上午11:11:45
	 * @param stores
	 * @return
	 * @throws CRUDException
	 */
	public String setUpStoreFile(Store store) throws CRUDException {
		try {
			String str = PublishUtil.checkFileStatus(store);
			if (!"true".equals(str)) {
				return str;
			}
			
			//修改路径，支持linux
			delFolder(DataForPublish.getPath() + "/" + store.getVcode());	//清除该门店下发文件夹内所有数据
			delFile(DataForPublish.getPath() + "/" + store.getVcode());		//删除该门店的压缩包
			File file=new File(DataForPublish.getPath() + "/" + store.getVcode());
			file.mkdirs();	
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
		return "true";
	}
	
	/**
	 * 描述：删除该门店的压缩包
	 * @author 马振
	 * 创建时间：2015-8-25 上午11:13:33
	 * @param path
	 * @throws CRUDException
	 */
	private void delFile(String path) throws CRUDException {
		try {
			File file =new File(path + ".zip");
			file.delete();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：下发图片
	 * @author 马振
	 * 创建时间：2015-8-25 上午11:13:51
	 * @param stores
	 * @throws CRUDException
	 */
	public static void copyImgToStoreFile(Store store) throws CRUDException {
		try {
			//循环构建pos图片存放路径
			File storeImg = new File(DataForPublish.getPath() + "/" + store.getVcode() + "/img");	//修改路径，支持linux
			storeImg.mkdirs();//创建pos图片文件夹
			try {
				copyPngToStoreFile(ItemPath.getItemRootPath() + "scrimg/", storeImg.getPath());		//将pos图片复制到门店文件夹下的img文件夹下
			} catch (CRUDException e) {
				e.printStackTrace();
			}	
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：将pos图片复制到门店文件夹下的img文件夹下
	 * @author 马振
	 * 创建时间：2015-8-25 上午11:16:22
	 * @param imgFiles
	 * @param storeFile
	 * @throws CRUDException
	 */
	public static void copyPngToStoreFile(String imgFiles,String storeFile) throws CRUDException{
		try{
			File imgFile = new File(imgFiles);
			String[] listFile = imgFile.list();
			File temp = null;
			for (int i = 0; i < listFile.length; i++) {
				if (imgFiles.endsWith(File.separator)) {
					temp = new File(imgFiles + listFile[i]);
				}else{
					temp = new File(imgFiles + File.separator + listFile[i]);
				}
				if (temp.isFile()) {
					String strValidate = "PNG,jpg,png,JPG";
					if (strValidate.indexOf(temp.getName().substring(temp.getName().lastIndexOf(".")+1)) != -1) {
						FileInputStream input = new FileInputStream(temp);
						FileOutputStream output = new FileOutputStream(storeFile + "/" + (temp.getName()).toString());
						byte[] bytes = new byte[1024 * 5];
						int len;
						while ((len = input.read(bytes)) != -1) {
							output.write(bytes, 0, len);
						}
						output.flush();
						output.close();
						input.close();
					}else{
						if (temp.isDirectory()) {// 如果是子文件夹
							copyPngToStoreFile(imgFiles + "/" + listFile[i], storeFile + "/" + listFile[i]);
						}
					}
				}
			}
		} catch(Exception e) {
			log.error(e);
		}
	}
		
	/**
	 * 描述：导出查询结果到xml
	 * @author 马振
	 * 创建时间：2015-8-25 上午11:17:28
	 * @param content
	 * @param path
	 * @param result
	 * @return
	 * @throws Exception
	 * @throws CRUDException
	 */
	@SuppressWarnings("unchecked")
	private boolean exportToXml(PublishContent content,String path,List<Map<String,Object>> result) throws Exception,CRUDException{
		File f = new File(path);
		Document doc = null;
		Element root = new Element(content.getRootName());
		Format format = Format.getPrettyFormat(); 
		format.setEncoding("UTF-8");
		XMLOutputter out = new XMLOutputter(format);
    	FileOutputStream fileWriter = null;
		Properties prop = null;
		Stack<AttributeFilter> chain = null;
		boolean isAttribute = true;
		String verifiedparam = "";//存放验证编码重复时的编码，直到循环到不同的编码
				
		//新建导出文件
		try {
			if (!f.exists()) {
				f.mkdirs();
			}
			f = new File(f,content.getFileName());
			if (!f.exists()) {
				f.createNewFile();
			}
			//加载数据字段映射文件
			if(null != content.getMapping() && content.getMapping().trim() != ""){
				prop = new Properties();
				prop.load(this.getClass().getResourceAsStream("dataMapping/" + content.getMapping()));
			}
		} catch (IOException e) {
			throw new IOException("为[" + content.getDes() + "]生成xml时，创建文件失败！");
		}
		
		doc = new Document(root); 
		
		//循环查询结果生成xml文件
		for(Map<String,Object> cur : result){
			
			//重复验证　如果vcode不为空并且文件名不在fileName中存在，则验证
			if(ValueUtil.IsNotEmpty(cur.get("VCODE"))){
				if(ValueUtil.IsEmpty(verifiedparam)){
					verifiedparam = cur.get("VCODE").toString();
				}else{
					if(verifiedparam.equals(cur.get("VCODE"))){
						//如果本次取出的值与上次的值相同，跳过本次循环直接进入下个循环
						continue;
					}else{
						verifiedparam = cur.get("VCODE").toString();
					}
				}
			}
			
			//门店信息属性处理类
			AttributeFilter storeFilter = new AttributeFilter(){
				@Override
				public void beforeBuildHandler(ArrayList<String> keys) throws CRUDException {
					try{
						//将门店编码属性放到一条数据的首位
						int index = keys.indexOf("VCODE");
						if(index >= 0){
							keys.remove(index);
						}
						keys.add(0, "VCODE");
					}catch(Exception e){
						log.error(e);
						throw new CRUDException(e);
					}
				}
			};
			
			if (content.getFileName().equals("PosDB.xml") || content.getFileName().equals("globalconfig.xml")) {
				isAttribute = false;
				for(String str : cur.keySet()){
					Map<String,Object> second = (Map<String, Object>) cur.get(str);
					root.addContent(buildElement(second,str,prop,chain,isAttribute));
				}
			} else if (content.getFileName().equals("globalconfigpos.xml")){
				for(String str : cur.keySet()){
					Map<String,Object> second = (Map<String, Object>) cur.get(str);
					Set<String> set = second.keySet();
					List<String> attributes = Arrays.asList(set.toArray(new String[set.size()]));
					for(int i=0;i<attributes.size();i++){
						root.addContent(buildElementPos((Map<String,Object>)second.get(attributes.get(i)),str));
					}
				}
			} else {
				if(content.getFileName().equals("store.xml") || content.getFileName().equals("STORE_TABLE.xml")){
					chain = new Stack<AttributeFilter>();
					chain.add(storeFilter);
				}
				root.addContent(buildElement(cur,content.getElementName(),prop,chain,isAttribute));
			}
		}
		
		//将xml文件写入文件
		try {
			fileWriter = new FileOutputStream(f);
			out.output(doc,fileWriter);
			fileWriter.flush();
		} catch (IOException e) {
			throw new IOException("为[" + content.getVo()+ "]生成xml时，生成文件失败！");
		}finally{
			if(null != fileWriter){
				fileWriter.close();
			}
		}
		return true;
	}
	
	/**
	 * 描述：拼接element
	 * @author 马振
	 * 创建时间：2015-8-25 上午11:22:45
	 * @param record
	 * @param eleName		所要生成的Element的名字
	 * @param prop			字段映射Properties对象
	 * @param chain			处理类，处理当前记录（Map）中的keys和value
	 * @param isAttribute	是否将属性信息作为xml Element的attribute 生成，false时将属性信息作为子Element生成
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Element buildElement(Map<String,Object> record,String eleName,Properties prop,Stack<AttributeFilter> chain,boolean isAttribute){
		try{
			Element element = new Element(eleName);
			Set<String> set = record.keySet();
			List<String> attributes = Arrays.asList(set.toArray(new String[set.size()]));
			ArrayList<String> keys = new ArrayList<String>();
			keys.addAll(attributes);
			AttributeFilter filter = null == chain || chain.empty() ? null : chain.peek();
			
			//处理属性信息（可控制attribute的出现顺序）
			if (null != filter) {
				filter.beforeBuildHandler(keys);
			}
			
			for(Object key : keys){
				
				//通过元素只类型判断是否存在下级节点，以做相应处理
				if (record.get(key) instanceof List) {
					
					//获取下级节点属性处理类
					if (null != chain && !chain.empty()) chain.pop();
					for (Map<String,Object> cur : (List<Map<String,Object>>)record.get(key)) {
						if(null != prop){
							if(null!=prop.getProperty(key.toString())){
								String curKey = prop.getProperty(key.toString());
								
								//递归调用生成子级元素
								element.addContent(buildElement(cur,null == curKey ? key.toString() : curKey, prop, chain, isAttribute));
							}
						}else{
							element.addContent(buildElement(cur,key.toString(),prop,chain,isAttribute));
						}
					}
				}else{
					String curkey = String.valueOf(key);
					Object value = record.get(key);
					
					//是否进行字段映射（只生成映射文件中存在的字段，其他忽略）
					if(null != prop){
						if(null != prop.getProperty(key.toString().toUpperCase())){
							curkey = prop.getProperty(key.toString().toUpperCase());
						}else{
							continue;
						}
					}
					
					//判断是否存在属性处理类，判断是否要对其中结果数据进行处理
					if (null != filter) {
						filter.init(curkey, value);
						if (filter.propertiesHandler()) {
							if (isAttribute) {
								element.setAttribute(filter.key, null == filter.value ? "" : filter.value.toString());
							} else {
								Element e = new Element(filter.key);
								e.addContent(null == filter.value ? "" : filter.value.toString());
								element.addContent(e);
							}
						}
					}else{
						if(isAttribute){//是否作为属性进行文件生成
							element.setAttribute(curkey,null == value ? "" : value.toString());
						}else{
							Element e = new Element(curkey);
							e.addContent(null == value ? "" : value.toString());
							element.addContent(e);
						}
					}
				}
			}
			return element;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 描述：拼接elementPOS
	 * @author 马振
	 * 创建时间：2015-8-25 上午11:25:48
	 * @param record
	 * @param eleName	所要生成的Element的名字
	 * @return
	 */
	private Element buildElementPos(Map<String,Object> record, String eleName){
		try{
			Element element = new Element(eleName);
			Set<String> set = record.keySet();
			List<String> attributes = Arrays.asList(set.toArray(new String[set.size()]));
			ArrayList<String> keys = new ArrayList<String>();
			keys.addAll(attributes);
			for (Object key : keys) {
				if ("POSID".equals(key)) {
					element.setAttribute(key.toString(),record.get(key).toString());
				} else {
					if (null != record.get(key) && !"".equals(record.get(key))) {
						Element e = new Element(key.toString());
						e.addContent(record.get(key).toString());
						element.addContent(e);
					}
				}
			}
			return element;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 描述：获取场景图片
	 * @author 马振
	 * 创建时间：2015-8-25 上午11:26:35
	 * @throws CRUDException
	 */
	public void getImages() throws CRUDException{
		try {
			System.out.println(DataForPublish.getPath());
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	
	/**
	 * 描述：拷贝公用文件 到所有分店目录下 
	 * @author 马振
	 * 创建时间：2015-10-10 下午6:04:35
	 * @param storeList
	 * @return
	 * @throws IOException
	 */
	public static String copyToStore(Store store) throws IOException{
		String result = "ok";
		try{
			File file = new File(DataForPublish.getPath());
			List<File> files = new ArrayList<File>();
			List<File> docs = new ArrayList<File>();
			StringBuffer storeStr = new StringBuffer();
			
			//根据选择的门店生成相应文件夹
			storeStr.append(store.getVcode());
			
			//判断是否覆盖总部xml：0为覆盖，1为不覆盖
//			if ("0".equals(ForResourceFiles.getValByKey("config-bsboh.properties", "Cover"))) {
//				files.add(new File(ForResourceFiles.getValByKey("config-bsboh.properties", "coverPath") + "/" + store.getVcode()));
//			} else {
//				files.add(new File(DataForPublish.getPath() + "/" + store.getVcode())); 
//			}
			files.add(new File(url + "/" + store.getVcode()));
			
			for (File storeFile : file.listFiles()) {
//				if(storeFile.isDirectory()&&!storeFile.getName().equals("img")){//判断是不是文件
//					files.add(storeFile);//如果是文件保存到文件夹数组里 IMG文件夹除外
//					continue;
//				}else 
				if(storeFile.getName().endsWith(".xml")){
					docs.add(storeFile);//如果是文件保存到文件数组里
				}
			}
			
			for(File doc : docs){//遍历文件
				for(File storeFile : files){//遍历门店
					FileUtil.copyFile(doc, storeFile);//复制公用文件到每个门店文件内
				}
				doc.delete();//删除复制完的文件
			}
			
			ExecutorService exec = Executors.newFixedThreadPool(MisUtil.threadSize);
			String str = checkFileStatus(store);
			
			if (!"true".equals(str)) {
				return str;
			}
			
			for (File storeFile : files) {
				//多线程压缩--新的压缩方法-大幅提高压缩速度
				exec.execute(new ZipUtil(storeFile.getPath()+".zip","zip",storeFile));
//				zipUtil.zip(storeFile.getPath()+".zip","zip",storeFile);
			}
			//关闭线程池
			exec.shutdown();
			//检测线程执行的任务是否已经完结，没有的话检测直到完成
			while(!exec.awaitTermination(1, TimeUnit.MILLISECONDS)){
				result = "false";
			}
			result = "ok";
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
			throw new IOException(e);
		}
		return result;
	}
	
	/**
	 * 描述：删除文件夹
	 * @author 马振
	 * 创建时间：2015-8-25 下午1:16:47
	 * @param folderPath
	 * @throws CRUDException
	 */
	public void delFolder(String folderPath) throws CRUDException {
		try {
			delAllFile(folderPath); //删除完里面所有内容
			String filePath = folderPath;
			filePath = filePath.toString();
			java.io.File myFilePath = new java.io.File(filePath);
			myFilePath.delete(); //删除空文件夹
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 描述：删除指定文件夹下所有文件
	 * @author 马振
	 * 创建时间：2015-8-25 下午1:17:06
	 * @param path	文件夹完整绝对路径
	 * @return
	 * @throws CRUDException
	 */
	public boolean delAllFile(String path) throws CRUDException{
       try{
    	   boolean flag = false;
	       File file = new File(path);
	       
	       if (!file.exists()) {
	         return flag;
	       }
	       
	       if (!file.isDirectory()) {
	         return flag;
	       }
	       String[] tempList = file.list();
	       File temp = null;
	       
	       for (int i = 0; i < tempList.length; i++) {
	          if (path.endsWith(File.separator)) {
	             temp = new File(path + tempList[i]);
	          } else {
	              temp = new File(path + File.separator + tempList[i]);
	          }
	          if (temp.isFile()) {
	             temp.delete();
	          }
	          if (temp.isDirectory()) {
	             delAllFile(path + "/" + tempList[i]);	//先删除文件夹里面的文件
	             delFolder(path + "/" + tempList[i]);	//再删除空文件夹
	             flag = true;
	          }
	       }
	       return flag;
       }catch(Exception e){
    	   log.error(e);
    	   throw new CRUDException(e);
       }
	}
	
	/**
	 * 描述：转换颜色（Hex颜色转换为RGB颜色）
	 * @author 马振
	 * 创建时间：2015-8-25 下午1:18:07
	 * @param color
	 * @return
	 */
	public static String transformColor(String color){
		String newColor = "";
		try{
			for(int i=0;i<color.length();i+=2){
				newColor += Integer.valueOf(color.substring(i,i+2),16).toString()+",";
			}
			if(newColor.length()>0){
				newColor = newColor.substring(0,newColor.length()-1);
			}
		}catch(Exception e){
			return ""; 
		}
		return newColor;
	}
	
	/**
	 * 描述：xml下发工具类
	 * @author 马振
	 * 创建时间：2015-8-25 下午1:18:19
	 */
	@SuppressWarnings("unused")
	private class AttributeFilter{
		private String key;
		public String getKey() {
			return key;
		}
		public Object getValue() {
			return value;
		}
		private Object value;
		public void beforeBuildHandler(ArrayList<String> keys) throws CRUDException{
			
		}
		public void init(String key,Object value){
			this.key = key;
			this.value = value;
		}
		public boolean propertiesHandler(){
			this.setKey(key);
			this.setValue(value);
			return true;
		}
		public void setValue(Object value) {
			this.value = value;
		}
		public void setKey(String key) {
			this.key = key;
		}
	}
	
	/**
	 * 描述：重复验证
	 * @author 马振
	 * 创建时间：2015-8-25 下午1:18:46
	 * @param listParam
	 * @param param
	 * @param strVerified
	 * @return
	 */
	public boolean RepeatVerified(List<Map<String,Object>> listParam,String param,String strVerified){
		boolean result = false;
		for (Map<String,Object> map : listParam) {
			if (strVerified.equals(map.get(param))) {
				result = true;
				return result;
			}
		}
		return result;
	}
	
	/**
	 * 描述：为当前下发操作加锁
	 * @author 马振
	 * 创建时间：2015-8-25 下午1:19:04
	 * @param lockDir
	 * @return
	 * @throws IOException
	 */
	public static String lockOperate(File lockDir) throws IOException{
		try{
			File lockf = new File(lockDir, "lock.lck");
			if (!lockf.isFile()) {
				lockf.createNewFile();
			}
			RandomAccessFile lockFile = new RandomAccessFile(new File(lockDir,"lock.lck"), "rw");
			FileChannel lck = lockFile.getChannel();
			locker = lck.tryLock();
			return "ok";
		}catch (OverlappingFileLockException e) {
			e.printStackTrace();
			return "no";
		}
	}
	
	/**
	 * 描述：解除当前操作的锁定
	 * @author 马振
	 * 创建时间：2015-8-25 下午1:19:22
	 */
	public static void unlockOperate(){
		if(locker.isValid()){
			try {
				locker.release();
			} catch (IOException e) {
				System.out.println("操作解锁异常！！");
			}
		}
	}
	
	/**
	 * 描述：当前操作是否被锁定
	 * @author 马振
	 * 创建时间：2015-8-25 下午1:19:34
	 * @return
	 */
	public boolean isLocked(){
		return null != locker && locker.isValid();
	}
	
	/**
	 * 描述：检测删除的文件，加访问锁，防止下发同时有程序在访问造成资源死锁
	 * @author 马振
	 * 创建时间：2015-8-25 下午1:19:44
	 * @param storeList
	 * @return
	 */
	public static String checkFileStatus(Store store){
		String fileName = "",flag = "true";
		
		//给要删除的文件加访问锁，防止下发同时有程序在访问造成资源死锁
		File fileZip = new File(url + "/" + store.getVcode() + ".zip");
		FileLock flout = null;  
        try {  
        	//对该文件加锁  
        	RandomAccessFile out = new RandomAccessFile(fileZip, "rw");  
	        FileChannel fcout = out.getChannel();  
            flout = fcout.tryLock(); 
            flout.release();   
            fcout.close(); 
        }catch (Exception e) {
             System.out.println("有程序正在操作文件：" + fileZip.getName() + ",请稍后再下发。");
             fileName = fileZip.getName();
             flag = "false";
        }    
		if("false".equals(flag)){
			fileName = "有程序正在访问这些文件："+fileName+",请稍后再下发。";
		} else {
			fileName = "true";
		}
		return fileName;
	}
}