package com.choice.misboh.commonutil;

import java.util.HashMap;
import java.util.Map;

import com.choice.scm.constants.ScmStringConstant;
import com.choice.tele.constants.TeleStringConstant;

/**
 * 描述：判断打印的类型
 * @author 马振
 * 创建时间：2015-5-5 上午10:58:25
 */
public class ReadReportUrl {
	public static Map<String,String> redReportUrl(String type,String print,String exp){
		Map<String,String> result = new HashMap<String,String>();
		result.put("url", ScmStringConstant.IREPORT_HTML);
		result.put("reportUrl", print);
		if("".equals(type)||null==type){
	 		result.put("url", ScmStringConstant.IREPORT_HTML);//html
		}else if(type.equals("pdf")){
			result.put("url", ScmStringConstant.IREPORT_PDF);//pdf
			result.put("reportUrl", exp);
	 	}else if(type.equals("excel")){
	 		result.put("url", ScmStringConstant.IREPORT_EXCEL);
	 		result.put("reportUrl", exp);
	 	}else if(type.equals("word")){
	 		result.put("url", ScmStringConstant.IREPORT_WORD);
	 		result.put("reportUrl", exp);
	 	}else if(type.equals("printPdf")){
	 		result.put("url", ScmStringConstant.IREPORT_PRINT_PDF);//直接打印pdf
	 		result.put("reportUrl", print);
		}else if(type.equals("printExcel")){
			result.put("url", ScmStringConstant.IREPORT_PRINT_EXCEL);
			result.put("reportUrl", print);
		}else if(type.equals("printWord")){
			result.put("url", ScmStringConstant.IREPORT_PRINT_WORD);
			result.put("reportUrl", print);
		}
		return  result;
	}
	
	public static Map<String,String> redReportUrl(String type,String print){
		Map<String,String> result = new HashMap<String,String>();
		result.put("url", TeleStringConstant.IREPORT_HTML);
		result.put("urlBean", TeleStringConstant.IREPORT_HTML_BEAN);
		result.put("reportUrl", print);
		if("".equals(type)||null==type){
	 		result.put("url", TeleStringConstant.IREPORT_HTML);//html
	 		result.put("urlBean", TeleStringConstant.IREPORT_HTML_BEAN);
		}else if(type.equals("pdf")){
			result.put("url", TeleStringConstant.IREPORT_PDF);//pdf
			result.put("urlBean", TeleStringConstant.IREPORT_PDF_BEAN);
			result.put("reportUrl", print);
	 	}else if(type.equals("excel")){
	 		result.put("url", TeleStringConstant.IREPORT_EXCEL);
	 		result.put("urlBean", TeleStringConstant.IREPORT_EXCEL_BEAN);
	 		result.put("reportUrl", print);
	 	}else if(type.equals("word")){
	 		result.put("url", TeleStringConstant.IREPORT_WORD);
	 		result.put("urlBean", TeleStringConstant.IREPORT_WORD_BEAN);
	 		result.put("reportUrl", print);
	 	}else if(type.equals("printPdf")){
	 		result.put("url", TeleStringConstant.IREPORT_PRINT_PDF);//直接打印pdf
	 		result.put("urlBean", TeleStringConstant.IREPORT_PRINT_PDF_BEAN);
	 		result.put("reportUrl", print);
		}else if(type.equals("printExcel")){
			result.put("url", TeleStringConstant.IREPORT_PRINT_EXCEL);
			result.put("urlBean", TeleStringConstant.IREPORT_PRINT_EXCEL_BEAN);
			result.put("reportUrl", print);
		}else if(type.equals("printWord")){
			result.put("url", TeleStringConstant.IREPORT_PRINT_WORD);
			result.put("urlBean", TeleStringConstant.IREPORT_PRINT_WORD_BEAN);
			result.put("reportUrl", print);
		}
		return  result;
	}

}
