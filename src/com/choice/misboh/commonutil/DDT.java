package com.choice.misboh.commonutil;

/**
 * 静态常量类
 * @author 文清泉
 *
 */
public class DDT {
	
	/**
	 * 预估方式 --- 人数/客流
	 */
	public static final String PAX="PAX";
	/**
	 * 预估方式 ----营业额
	 */
	public static final String AMT="AMT";
	
	/**
	 * 预估方式 --- 账单 /TC
	 */
	public static final String TC="TC";
	
	/**
	 * 报货方式-默认（现在默认千元）
	 */
	public static final String SHIPMENT_MR="0";
	/**
	 * 报货方式-千元
	 */
	public static final int SHIPMENT_QY=1;
	/**
	 * 报货方式-直接填写申购单
	 */
	public static final int SHIPMENT_ZJSGD=2;
	/**
	 * 报货方式-申购模板
	 */
	public static final int SHIPMENT_SGMB=3;
	/**
	 * 报货方式-安全库存
	 */
	public static final int SHIPMENT_AQKC=4;
	/**
	 * 报货方式-千人
	 */
	public static final int SHIPMENT_QR=5;
	/**
	 * 报货方式-菜品销售计划
	 */
	public static final int SHIPMENT_CPXSJH=6;
	/**
	 * 报货方式-菜品点击率
	 */
	public static final int SHIPMENT_CPDJL=7;
	/**
	 * 报货方式-周次平均
	 */
	public static final int SHIPMENT_ZCPJ=8;
	
	/**
	 * ==============================================人事数据字典===================================
	 */
	/**
	 * 数据字典-民族
	 */
	public static final String DATATYPE_TYP1 = "1";
	
	/**
	 * 数据字典-学历
	 */
	public static final String DATATYPE_TYP2 = "2";
	
	/**
	 * 数据字典-学位
	 */
	public static final String DATATYPE_TYP3 = "3";
	
	/**
	 * 数据字典-政治面貌
	 */
	public static final String DATATYPE_TYP4 = "4";
	
	/**
	 * 数据字典-户口类型
	 */
	public static final String DATATYPE_TYP5 = "5";
	
	/**
	 * 数据字典-用工形式
	 */
	public static final String DATATYPE_TYP6 = "6";
	
	/**
	 * 数据字典-籍贯
	 */
	public static final String DATATYPE_TYP7 = "7";
	
	/**
	 * 数据字典-银行卡类型
	 */
	public static final String DATATYPE_TYP8 = "8";
	
	/**
	 * 数据字典-健康证办理地区
	 */
	public static final String DATATYPE_TYP9 = "9";
	
	/**
	 * 数据字典-税率类型
	 */
	public static final String DATATYPE_TYP10 = "10";
	
	/**
	 * 数据字典-合同类型
	 */
	public static final String DATATYPE_TYP11 = "11";
	
	/**
	 * 数据字典-工资级别
	 */
	public static final String DATATYPE_TYP12 = "12";
	
	/**
	 * 数据字典-用工来源
	 */
	public static final String DATATYPE_TYP13 = "13";
	
	/**
	 * 数据字典-保险状态
	 */
	public static final String DATATYPE_TYP14 = "14";
	
	/**
	 * 数据字典-岗位级别
	 */
	public static final String DATATYPE_TYP15 = "15";
	
	/**
	 * 数据字典-外语等级
	 */
	public static final String DATATYPE_TYP16 = "16";
	
	/**
	 * 数据字典-在职岗位
	 */
	public static final String DATATYPE_TYP17 = "17";
	
	/**
	 * 数据字典-员工状态
	 */
	public static final String DATATYPE_TYP18 = "18";

	/**
	 * 数据字典-试用期限
	 */
	public static final String DATATYPE_TYP19 = "19";
	
	/**
	 * 数据字典-婚姻状况
	 */
	public static final String DATATYPE_TYP20 = "20";
	
	/**
	 * 数据字典-性别
	 */
	public static final String DATATYPE_TYP21 = "21";
	
	/**
	 * 数据字典 --- 转正理由
	 */
	public static final String DATATYPE_TYP22 = "22";
	
	/**
	 * 数据字典 --- 休假类型
	 */
	public static final String DATATYPE_TYP23 = "23";
	
	/**
	 * 数据字典 --- 奖惩类型
	 */
	public static final String DATATYPE_TYP24 = "24";
	
	/**
	 * 数据字典 --- 工作站
	 */
	public static final String DATATYPE_TYP25 = "25";
	
	
	/**
	 * ================================================考勤机打卡状态=================================================
	 */

	/**
	 * 打卡状态-上班签到
	 */
	public  static final String CHECKIN = "0";
	
	/**
	 * 打卡状态-下班签退
	 */
	public  static final String CHECKOUT = "1";
	
	/**
	 * 打卡状态-外出
	 */
	public  static final String BREAKOUT = "2";
	
	/**
	 * 打卡状态-外出返回
	 */
	public  static final String BREAKIN = "3";
	
	/**
	 * 打卡状态-加班签到
	 */
	public  static final String OTIN = "4";
	
	/**
	 * 打卡状态-加班签退
	 */
	public  static final String OTOUT = "5";
	
	/**********************************期初，盘点，报货，出入库等 start******************************************/
	
	/**
	 * 期初
	 */
	public static final String BEGIN = "begin";
	
	/**
	 * 入库
	 */
	public static final String IN = "in";
	
	/**
	 * 出库
	 */
	public static final String OUT = "out";
	
	/**
	 * 直发
	 */
	public static final String INOUT = "inout";
	
	/**
	 * 直配
	 */
	public static final String DIRE = "dire";
	
	/**
	 * codedes正常入库
	 */
	public static final String ZCRK = "9900";
	
	/**
	 * codedes盘盈入库
	 */
	public static final String PYRK = "9901";
	
	/**
	 * codedes调拨入库
	 */
	public static final String DBRK = "9940";
	
	/**
	 * codedes验货冲消
	 */
	public static final String YHCX = "9941";
	
	/**
	 * codedes正常出库
	 */
	public static final String ZCCK = "9908";
	
	/**
	 * codedes调拨出库
	 */
	public static final String DBCK = "9909";
	
	/**
	 * codedes盘亏出库
	 */
	public static final String PKCK = "9910";
	
	/**
	 * codedes赠品出库
	 */
	public static final String ZPCK = "9911";
	
	/**
	 * codedes出库退库 
	 */
	public static final String CKTK = "9913";
	
	/**
	 * codedes出库冲消
	 */
	public static final String CKCX = "9914";
	
	/**
	 * codedes出库退货 
	 */
	public static final String CKTH = "9921";
	
	/**
	 * codedes批次出库
	 */
	public static final String PCCK = "9924";
	
	/**
	 * codedes出库报废
	 */
	public static final String CKBF = "9915";
	
	/**
	 * codedes员工餐出库
	 */
	public static final String YGCCK = "9951";
	
	/**
	 * codedes加工领料
	 */
	public static final String JGLL = "9927";

	/**
	 * codedes差错补出
	 */
	public static final String CCBC = "9925";
	
	/**
	 * codedes正常直发
	 */
	public static final String ZCZF = "9916";
	
	/**
	 * codedes调拨直发
	 */
	public static final String DBZF = "9960";
	
	/**
	 * codedes直发验货冲消
	 */
	public static final String ZFYHCX = "9961";
	
	/**
	 * codedes直配入库
	 */
	public static final String ZPRK = "9942";
	
	/***
	 * codedes直配直发
	 */
	public static final String ZPZF = "9962";
	
	/***
	 * codedes配送入库
	 */
	public static final String PSRK = "9943";
	
	/***
	 * codedes配送直发
	 */
	public static final String PSZF = "9963";
	
	/***
	 * codedes分店直发
	 */
	public static final String FDZF = "9922";
	
	/**
	 * codedes报货类别
	 */
	public static final String CODEDES11 = "11";
	
	/**
	 * codedes报废类型
	 */
	public static final String CODEDES13 = "13";
	
	/**
	 * codedes单据类型
	 */
	public static final String CODEDES18 = "18";
	
	/**
	 * codedes单据类型 入库类型
	 */
	public static final String CODEDES18RK = "RK";
	
	/**
	 * codedes单据类型 出库类型
	 */
	public static final String CODEDES18CK = "CK";
	
	/**
	 * codedes单据类型 直发类型
	 */
	public static final String CODEDES18ZF = "ZF";
	
	/****************凭证号相关常量****************/
	/**
	 * 凭证号类型BH
	 */
	public static final String VOUNO_BH = "BH";
	
	/**
	 * 凭证号类型TH
	 */
	public static final String VOUNO_TH = "TH";
	
	/**
	 * 凭证号类型RK
	 */
	public static final String VOUNO_RK = "RK";
	
	/**
	 * 凭证号类型CK
	 */
	public static final String VOUNO_CK = "CK";
	
	/**
	 * 凭证号类型ZF
	 */
	public static final String VOUNO_ZF = "ZF";
	
	/**
	 * 凭证号类型调出DC
	 */
	public static final String VOUNO_DC = "DC";
	
	/**
	 * 凭证号类型调入DR
	 */
	public static final String VOUNO_DR = "DR";
	
	/**
	 * 凭证号读取表CHKSTOM_DEPT
	 */
	public static final String VOUNO_CHKSTOMDEPT = "CHKSTOM_DEPT";
	
	/**
	 * 凭证号读取表CHKSTOM
	 */
	public static final String VOUNO_CHKSTOM = "CHKSTOM";
	
	/**
	 * 凭证号读取表CHKINM
	 */
	public static final String VOUNO_CHKINM = "CHKINM";
	
	/**
	 * 凭证号读取表CHKOUTM
	 */
	public static final String VOUNO_CHKOUTM = "CHKOUTM";
	
	
	/**********************************期初，盘点，报货，出入库等 end******************************************/
	
	/**
	 * ===============================================人事 员工状态====================================
	 */
	/**
	 * 员工状态 ----试用
	 */
	public static final String HR_SY="1";
	/**
	 * 员工状态 ----在职
	 */
	public static final String HR_ZZ="2";
	/**
	 * 员工状态 ----离职
	 */
	public static final String HR_LZ="3";
	/**
	 * 员工状态 ----退休
	 */
	public static final String HR_TX="4";
	/**
	 * 员工状态 ----删除
	 */
	public static final String HR_SC="5";
	/**
	 * =========================================报货==================================================
	 */
	/**
	 * 报货单据生成    手工填写 ==1
	 */
	public static final String HANDINHAND="1";
	
	/**
	 * 报货单据生成  向导生成  == 2
	 */
	public static final String GUIDEGENERATION="2";
	
	/**
	 * 报货附加项长度
	 */
	public static final String BH_FJX_LEN = "96db93524fb349e1aafef1c01b775502";
	
	/**
	 * 正常报货
	 */
	public static final String ZCBH = "9970";
	
	/**
	 * 加单报货
	 */
	public static final String JDBH = "9971";
	
	/**
	 * 报货退货
	 */
	public static final String BHTH = "9972";
	
	/**
	 * 西贝报货分隔时间
	 */
	public static final String BHFGSJ = "14:00";
	
	/***
	 * 报货验货不显示价格  默认N显示 不显示Y 只有金色三麦用
	 */
	public static final String isDistributionUnit= "N";
	
	/***
	 * 是否启用配送单位
	 */
	public static final String isNotShowSp_price= "N";
	
	/***
	 * 统配验货根据到货数量还是验货数量 (默认I根据验货数量) A根据到货数量
	 */
	public static final String insByArrOrIns = "I";

	/***
	 * 盘亏仓位
	 */
	public static final String profitDeliver = "1999";
	
	/***
	 * 报货页面
	 */
	public static final String isChkstomJmj="N";
	
	/***
	 * 到货日不能修改
	 */
	public static final String isNotUpdateHoped = "N";
	
	/***
	 * 营业预估是否分餐次，报货向导用   默认N按天预估  Y分餐次预估
	 */
	public static final String isForecastByTime ="N";
	
	/***
	 * 提交类别
	 */
	public static final String autoTyp = "-1";
	
	/***
	 * 非0入库许可
	 */
	public static final String isChkinPriceNot0 = "N";
	/***
	 * 直配验货按单据
	 */
	public static final String isDireInsByBill ="N";
	
	/***
	 * 盘点配置,是否根据我的桌面每日指引做完 才能盘点 (默认否N) Y启用
	 */
	public static final String isPdByMydesk = "N";
	
	/***
	 * 用采购单位的盘点  (默认N不启用) 
	 */
	public static final String isPdUnit2 = "N";
	
	/***
	 * 分店物资查询  (默认N查询分店物资属性)，Y查询所有物资 
	 */
	public static final String isPositnSpcodeJmj = "N";
}