package com.choice.misboh.commonutil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 集合类型转换
 * @author 文清泉
 * @param 2015年1月1日 下午5:01:59
 */
public class TypeConversionUtil {

	/**
	 * 集合转换成MAP KEY 与 VALUE  
	 * @author 文清泉
	 * @param 2015年1月1日 下午5:04:00
	 * @param listMap 转换前集合
	 * @param key 转换后MAP KEY  通过设定KEY 读取转换前集合
	 * @param _value []  转换后MAP VALUE 通过设定VALUE 读取转换前集合
	 * @return
	 */
	public static Map<String,Object[]> listMapToMap(List<Map<String,Object>> listMap,String key,String[] _value){
		Map<String,Object[]> _map = new HashMap<String,Object[]>();
		if(listMap!=null){
			for (Map<String, Object> map : listMap) {
				Object[] obj = new Object[_value.length];
				for (int i = 0; i < _value.length; i++) {
					obj[i] = map.get(_value[i]);
				}
				_map.put(ValueUtil.getStringValue(map.get(key)), obj);
			}
		}
		return _map;
	}
	
}
