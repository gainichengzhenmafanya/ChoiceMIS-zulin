package com.choice.misboh.commonutil;

/**
 * 描述： 数字累加      (例：可用于报表的合计。)
 * @author 马振
 * 创建时间：2015-4-23 上午10:17:31
 */
public class Total {
	private int i=0;
	private double d=0;
	private float f=0;
	
	/**
	 * int 取值
	 * @return
	 */
	public int getI() {
		return i;
	}
	/**
	 * int 累加
	 * @param i
	 */
	public void addI(int i) {
		this.i += i;
	}
	
	/**
	 * double 取值
	 * @return
	 */
	public double getD() {
		return d;
	}
	/**
	 * double 累加
	 * @param d
	 */
	public void addD(double d) {
		this.d += d;
	}
	
	/**
	 * float 取值
	 * @return
	 */
	public float getF() {
		return f;
	}
	/**
	 * float 累加
	 * @param f
	 */
	public void addF(float f) {
		this.f += f;
	}
	
	/**
	 * int 归零
	 * @return
	 */
	public int zeroI() {
		i=0;
		return i;
	}
	/**
	 * double 归零
	 * @return
	 */
	public double zeroD() {
		d=0;
		return d;
	}
	/**
	 * float 归零
	 * @return
	 */
	public float zeroF() {
		f=0;
		return f;
	}
	
}
