package com.choice.misboh.commonutil;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class LocalUtil {

	private static final String basename = "content/Language";

	public static String getI18N(String key) {
		RequestAttributes ra = RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = ((ServletRequestAttributes) ra)
				.getRequest();
		HttpSession session = request.getSession();
		String locale = "zh_CN";
		if(null != session.getAttribute("locale"))
			locale = session.getAttribute("locale").toString();
		
		InputStream inputStream = LocalUtil.class.getClassLoader()
				.getResourceAsStream(basename + "_"+ locale +".properties");
		Properties properties = new Properties();
		String result = null;
		try {
			if ((key != null) && (!"".equals(key))) {
				properties.load(inputStream);
				result = properties.getProperty(key);
			}
		} catch (IOException e) {
			e.printStackTrace();
			try {
				inputStream.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} finally {
			try {
				inputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
	public static void main(String[] args){
		System.out.println(getI18N("Custom_report"));
	}
}
