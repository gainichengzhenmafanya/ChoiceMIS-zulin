package com.choice.misboh.commonutil;

/**
 * 描述：获取图片的路径
 * @author 马振
 * 创建时间：2015-5-17 下午3:28:01
 */
public class ItemPath {
	
	/**
	 * 描述：获取文件所在工程根目录
	 * @author 马振
	 * 创建时间：2015-5-17 下午3:28:16
	 * @return
	 */
	public static String getItemRootPath() {
		String path = ItemPath.class.getClassLoader().getResource("../../").getPath();
		return path;
	}
	
	/**
	 * 描述：获取tomcat中的class路径
	 * @author 马振
	 * 创建时间：2015-5-17 下午3:27:49
	 * @return
	 */
	public static String getItemClassPath() {
		String path = ItemPath.class.getClassLoader().getResource("").getPath();
		return path;
	}
}