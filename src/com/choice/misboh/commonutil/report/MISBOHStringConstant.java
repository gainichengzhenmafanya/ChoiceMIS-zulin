package com.choice.misboh.commonutil.report;

public final class MISBOHStringConstant {
	
	//列选择跳转的页面
	public static final String TO_COLUMNS_CHOOSE_VIEW="share/columnsChoose";
		
	//原材料成本差异
	public static final String BASICINFO_REPORT_MISCOSTVARIANCE = "583,584,585,586,587,588,589,590,592,593,594,595,596,597,607,608,598,599,600,601,602,603,604,605,606";//默认选择列
	public static final String REPORT_NAME_MISCOSTVARIANCE = "MISCostVariance";//选择表名
	public static final String REPORT_NAME_CN_MISCOSTVARIANCE = "原材料成本差异";  //中文名
	public static final String BASICINFO_REPORT_MISCOSTVARIANCE_FROZEN = "";//左侧固定列
	public static final String REPORT_QUERY_METHOD_MISCOSTVARIANCE = "findCostVariance";// service执行方法
	public static final Integer[] EXCEL_MISCOSTVARIANCE = {587,588,589,590,592,593,594,595,596,597,607,608,598,599,600,601,602,603,604,605,606};// 导出右侧
	
	//原材料成本差异choice3
	public static final String BASICINFO_REPORT_MISCOSTVARIANCECHOICE3 = "581,582,583,584,585,586,587,588,589,590,598,599,600,602,604,606,608,610,612,614,616";//默认选择列
	public static final String REPORT_NAME_MISCOSTVARIANCECHOICE3 = "MISCostVarianceChoice3";//选择表名
	public static final String REPORT_NAME_CN_MISCOSTVARIANCECHOICE3 = "原材料成本差异";  //中文名
	public static final String BASICINFO_REPORT_MISCOSTVARIANCECHOICE3_FROZEN = "";//左侧固定列
	public static final String REPORT_QUERY_METHOD_MISCOSTVARIANCECHOICE3 = "findCostVarianceChoice3";// service执行方法
	public static final Integer[] EXCEL_MISCOSTVARIANCECHOICE3 = {587,588,589,590,598,599,600,602,604,606,608,610,612,614,616};// 导出右侧

	//毛利查询
	public static final String BASICINFO_REPORT_GROSSPROFIT = "691,692,693,694,695,696,697,698,699";
	public static final String REPORT_NAME_GROSSPROFIT = "GrossProfit";//选择表名
	public static final String BASICINFO_REPORT_GROSSPROFIT_FROZEN = "";//左侧固定列
	public static final String REPORT_NAME_CN_GROSSPROFIT = "分店毛利查询";  //中文名
	public static final String REPORT_QUERY_METHOD_GROSSPROFIT = "findCalForGrossProfit";// service执行方法
	public static final Integer[] EXCEL_GROSSPROFIT = {693,694,695,696,697,698,699};

	/**----------------------------------------报表start-------------------------------------------------------**/
	public static final String IREPORT_HTML = "share/ireport/mapSource/html";  //html页面
	public static final String IREPORT_EXCEL = "share/ireport/mapSource/excel";  //excel页面
	public static final String IREPORT_PDF = "share/ireport/mapSource/pdf";  //pdf页面
	public static final String IREPORT_WORD = "share/ireport/mapSource/word";  //word页面
	public static final String IREPORT_PRINT_EXCEL = "share/ireport/mapSource/printExcel";  //直接打印excel页面
	public static final String IREPORT_PRINT_PDF = "share/ireport/mapSource/printPdf";  //直接打印pdf页面
	public static final String IREPORT_PRINT_WORD = "share/ireport/mapSource/printWord";  //直接打印word页面
	//bean
	public static final String IREPORT_HTML_BEAN = "share/ireport/html";  //html页面
	public static final String IREPORT_EXCEL_BEAN = "share/ireport/excel";  //excel页面
	public static final String IREPORT_PDF_BEAN = "share/ireport/pdf";  //pdf页面
	public static final String IREPORT_WORD_BEAN = "share/ireport/word";  //word页面
	public static final String IREPORT_PRINT_EXCEL_BEAN = "share/ireport/printExcel";  //直接打印excel页面
	public static final String IREPORT_PRINT_PDF_BEAN = "share/ireport/printPdf";  //直接打印pdf页面
	public static final String IREPORT_PRINT_WORD_BEAN = "share/ireport/printWord";  //直接打印word页面
	/**---------------------------------------------报表end------------------------------------------------------**/
}