package com.choice.misboh.commonutil.report;

public class ReadReportConstants {

	/**
	 * 获取报表表格地址
	 * @param constant 表格地址存储的类
	 * @param name 报表名(不区分大小写)
	 * @return
	 */
	public static String getReportURL(Class<?> constant,String name){
		try {
			return (String) constant.getField("REPORT_SHOW_"+name.toUpperCase()).get(null);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}
	/**
	 * 获取表格表头index
	 * @param constant
	 * @param name
	 * @return
	 */
	public static String getDefaultHeader(Class<?> constant,String name){
		try {
			return (String) constant.getField("BASICINFO_REPORT_"+name.toUpperCase()).get(null);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}
	/**
	 * 获取ireport模版地址
	 * @param constant
	 * @param name
	 * @return
	 */
	public static String getIReportPrint(Class<?> constant,String name){
		try {
			return (String) constant.getField("REPORT_URL_"+name.toUpperCase()).get(null);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}
	
	/**
	 * 获取表格frozen表头index
	 * @param constant
	 * @param name
	 * @return
	 */
	public static String getFrozenHeader(Class<?> constant,String name){
		try {
			return (String) constant.getField("BASICINFO_REPORT_"+name.toUpperCase()+"_FROZEN").get(null);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}
	/**
	 * 获取报表名字(数据库中存储)
	 * @param constant
	 * @param name
	 * @return
	 */
	public static String getReportName(Class<?> constant,String name){
		try {
			return (String) constant.getField("REPORT_NAME_"+name.toUpperCase()).get(null);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}
	/**
	 * 获取报表名字(中文)
	 * @param constant
	 * @param name
	 * @return
	 */
	public static String getReportNameCN(Class<?> constant,String name){
		try {
			return (String) constant.getField("REPORT_NAME_CN_"+name.toUpperCase()).get(null);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}
	/**
	 * 获取报表查询方法
	 * @param constant
	 * @param name
	 * @return
	 */
	public static String getReportMethod(Class<?> constant,String name) {
		try {
			return (String) constant.getField("REPORT_QUERY_METHOD_"+name.toUpperCase()).get(null);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}
	
	public static Integer[] getReportExcel(Class<?> constant,String name){
		try {
			return (Integer[]) constant.getField("EXCEL_"+name.toUpperCase()).get(null);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}
	
}
