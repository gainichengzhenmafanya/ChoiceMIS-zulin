package com.choice.misboh.commonutil.report;

import java.util.HashMap;
import java.util.Map;

/**
 * 判断打印的类型
 */
public class ReadReportUrl {
	public static Map<String,String> redReportUrl(String type,String print){
		Map<String,String> result = new HashMap<String,String>();
		result.put("url", MISBOHStringConstant.IREPORT_HTML);
		result.put("urlBean", MISBOHStringConstant.IREPORT_HTML_BEAN);
		result.put("reportUrl", print);
		if("".equals(type)||null==type){
	 		result.put("url", MISBOHStringConstant.IREPORT_HTML);//html
	 		result.put("urlBean", MISBOHStringConstant.IREPORT_HTML_BEAN);
		}else if(type.equals("pdf")){
			result.put("url", MISBOHStringConstant.IREPORT_PDF);//pdf
			result.put("urlBean", MISBOHStringConstant.IREPORT_PDF_BEAN);
			result.put("reportUrl", print);
	 	}else if(type.equals("excel")){
	 		result.put("url", MISBOHStringConstant.IREPORT_EXCEL);
	 		result.put("urlBean", MISBOHStringConstant.IREPORT_EXCEL_BEAN);
	 		result.put("reportUrl", print);
	 	}else if(type.equals("word")){
	 		result.put("url", MISBOHStringConstant.IREPORT_WORD);
	 		result.put("urlBean", MISBOHStringConstant.IREPORT_WORD_BEAN);
	 		result.put("reportUrl", print);
	 	}else if(type.equals("printPdf")){
	 		result.put("url", MISBOHStringConstant.IREPORT_PRINT_PDF);//直接打印pdf
	 		result.put("urlBean", MISBOHStringConstant.IREPORT_PRINT_PDF_BEAN);
	 		result.put("reportUrl", print);
		}else if(type.equals("printExcel")){
			result.put("url", MISBOHStringConstant.IREPORT_PRINT_EXCEL);
			result.put("urlBean", MISBOHStringConstant.IREPORT_PRINT_EXCEL_BEAN);
			result.put("reportUrl", print);
		}else if(type.equals("printWord")){
			result.put("url", MISBOHStringConstant.IREPORT_PRINT_WORD);
			result.put("urlBean", MISBOHStringConstant.IREPORT_PRINT_WORD_BEAN);
			result.put("reportUrl", print);
		}
		return  result;
	}

}
