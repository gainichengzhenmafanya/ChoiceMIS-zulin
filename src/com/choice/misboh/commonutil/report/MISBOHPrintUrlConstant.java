package com.choice.misboh.commonutil.report;

public class MISBOHPrintUrlConstant {
	
	//毛利查询
	public static final String REPORT_SHOW_GROSSPROFIT="misboh/reportMis/costitem/grossProfit";//毛利查询
	public static final String REPORT_URL_GROSSPROFIT="/report/misboh/costitem/grossProfit.jasper";//毛利查询
	
	//物资成本差异
	public static final String REPORT_SHOW_MISCOSTVARIANCE = "misboh/reportMis/costitem/listCostVariance";//物资进出明细(成本差异)
	public static final String REPORT_URL_MISCOSTVARIANCE = "report/misboh/costitem/CostVariance.jasper";//报表地址
	
	//物资成本差异
	public static final String REPORT_SHOW_MISCOSTVARIANCECHOICE3 = "misboh/reportMis/costitem/listCostVarianceChoice3";//物资进出明细(成本差异)
	public static final String REPORT_URL_MISCOSTVARIANCECHOICE3 = "report/misboh/costitem/CostVarianceChoice3.jasper";//报表地址
}
