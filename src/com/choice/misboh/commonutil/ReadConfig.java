package com.choice.misboh.commonutil;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 描述：根据key读取config.properties中的值
 * @author 马振
 * 创建时间：2015-4-16 下午12:20:37
 */
public class ReadConfig {
	
	private static ReadConfig ipConfig;
	
	private ReadConfig(){
	}
	
	public static ReadConfig getInstance(){
		if(null == ipConfig){
			ipConfig = new ReadConfig();
		}
		return ipConfig;
	}
	
	public Properties getProperties(){
		
		//获取属性文件中设置的ip和端口
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("config.properties");    
		Properties properties = new Properties();    
		
		try {    
			properties.load(inputStream);    
		} catch (IOException e1) {    
		    e1.printStackTrace();    
		}
		
		return properties;
	}
	
	public Properties getProperties(String configName){
		
		//获取属性文件中设置的ip和端口 
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(configName);   
		Properties properties = new Properties();    
		
		try {    
			properties.load(inputStream);    
		} catch (IOException e1) {    
		    e1.printStackTrace();    
		}
		
		return properties;
	}
	
	/**
	 * 描述：获取对应的值
	 * @author 马振
	 * 创建时间：2015-4-16 下午12:21:08
	 * @param key
	 * @return
	 */
	public String getValue(String key){
		if(null == getProperties().getProperty(key)){
			return "";
		}
		return getProperties().getProperty(key).trim();
	}
	
	public String getValue(String key,String configName){
		if(null == getProperties(configName).getProperty(key)){
			return "";
		}
		return getProperties(configName).getProperty(key).trim();
	}
}
