package com.choice.misboh.constants.inventory;

public class InventoryMisConstants {

	//页面跳转
	public static final String LIST_INVENTORY="misboh/inventory/listInventory";
	public static final String LIST_INVENTORY_DEPT="misboh/inventory/listInventory_dept";
	public static final String CHOOSEINVENTORY="misboh/inventory/chooseInventory";
	
	public static final String REPORT_PRINT_URL_INVENTORY_MIS_BLANK = "/report/misboh/inventory/inventoryMis_blank.jasper";//空白盘点表 
	public static final String REPORT_PRINT_URL_INVENTORY_MIS = "/report/misboh/inventory/inventoryMis.jasper";//非空白盘点表
	
	
	public static final String LIST_INVENTORY_UNIT2 = "misboh/inventory/listInventoryUnit2";//九毛九用成本单位盘点页面
	public static final String ADDINVENTORYDEMO = "misboh/inventory/addInventoryDemo";//盘点模板
	public static final String ADDINVENTORYDEMO1 = "misboh/inventory/addInventoryDemo1";//盘点模板
	public static final String REPORT_PRINT_URL_INVENTORY_MIS_BLANK_UNIT2 = "/report/misboh/inventory/inventoryMis_blankUnit2.jasper";//采购单位空白盘点表
	public static final String REPORT_PRINT_URL_INVENTORY_MIS_UNIT2 = "/report/misboh/inventory/inventoryMisUnit2.jasper";//采购单位非空白盘点表
	public static final String REPORT_PRINT_URL_INVENTORY_MIS_BLANK_DISUNIT = "/report/misboh/inventory/inventoryMis_blankDisunit.jasper";//配送单位空白盘点表
	public static final String REPORT_PRINT_URL_INVENTORY_MIS_DISUNIT = "/report/misboh/inventory/inventoryMisDisunit.jasper";//配送单位非空白盘点表
	public static final String IMPORT_INVENTORY = "misboh/inventory/importInventory";//导入页面
	public static final String IMPORT_RESULT="misboh/inventory/importInventoryResult";//导入结果

	//wap
	//public static final String ADD_INVENTORY="wap/inventory/addInventory.jsp";

	
	public static final String LIST_INVENTORY_DISUNIT = "misboh/inventory/listInventoryDisunit";//配送单位盘点页面
}
