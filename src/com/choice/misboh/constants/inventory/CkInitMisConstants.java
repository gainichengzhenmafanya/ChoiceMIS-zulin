package com.choice.misboh.constants.inventory;

/***
 * 仓库期初
 * @author wjf
 *
 */
public class CkInitMisConstants {

	public static final String CKINITDEPT="misboh/inventory/ckInitDept";//有档口的期初 未用
	public static final String CKINIT="misboh/inventory/ckInit";//没有档口的期初
	
	public static final String IMPORT_INIT="misboh/inventory/importInit";//期初导入
	public static final String IMPORT_RESULT="misboh/inventory/importResult";//导入结果
	public static final String QCZTCHAXUN = "misboh/inventory/qcZtchaxun";//期初状态查询
}
