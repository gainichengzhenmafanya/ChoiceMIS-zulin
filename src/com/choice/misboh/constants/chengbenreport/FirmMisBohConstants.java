package com.choice.misboh.constants.chengbenreport;

public class FirmMisBohConstants {

	public static final String ACCT = "1";  //默认帐套值
	
	//列选择：
	//列选择跳转的页面
	public static final String TO_COLUMNS_CHOOSE_VIEW="share/columnsChoose";

	//应产率分析
	public static final String BASICINFO_REPORT_SHOULDYIEDLANA = "617,618,619,620,621,622,623,624,625,626,627,628,629";//默认选择列
	public static final String TABLE_NAME_SHOULDYIEDLANA = "YingChanLvAna";//选择表名
	public static final String BASICINFO_REPORT_SHOULDYIEDLANA_FROZEN = "";
	//物资进出表明细
	public static final String BASICINFO_REPORT_COST_VARIANCE = "583,584,585,586,588,589,591,592,594,595,597,598,599,600,601,602,603";//默认选择列
	public static final String TABLE_NAME_COST_VARIANCE = "CostVariance";//选择表名
	public static final String BASICINFO_REPORT_COST_VARIANCE_FROZEN = "";//左侧固定列
	//毛利查询
	public static final String BASICINFO_REPORT_MAOLI = "691,692,693,694,695,696,697,698,699";
	public static final String TABLE_NAME_MAOLI = "maoLi";//选择表名
	public static final String BASICINFO_REPORT_MAOLI_FROZEN = "";//左侧固定列

	//核减明细
	public static final String BASICINFO_REPORT_HEJIAN = "604,605,606,607,608,609,610,611,612,613,614,615,616";//默认选择列
	public static final String TABLE_NAME_HEJIAN = "Hejian";//选择表名
	public static final String BASICINFO_REPORT_HEJIAN_FROZEN = "";//左侧固定列

	//每日差异报告
	public static final String BASICINFO_REPORT_DAYDIFREPORT = "683,684,685,686,687,688,689,690";
	public static final String TABLE_NAME_DAYDIFREPORT = "dayDifReport";//选择表名
	public static final String BASICINFO_REPORT_DAYDIFREPORT_FROZEN = "";//左侧固定列

	//部门物资进出表
	public static final String BASICINFO_REPORT_DEPTSUPPLYINOUT = "721,722,723,724,725,726,727,728,729,730,731,732,733,734,735";  //默认选择列
	public static final String REPORT_NAME_DEPTSUPPLYINOUT = "deptSupplyInOut";  //选择表名
	public static final Integer[] EXPORT_DEPTSUPPLYINOUT={726,727,728,729,730,711,732,733,734,735};
	
	//部门成本差异
	public static final String BASICINFO_REPORT_DEPTCOSTDIFF = "736,737,738,739,740,741,742,743,744,745,746,747,748,749,750,751,752,753,754,755,756,757,758,759,760,761";  //默认选择列
	public static final String REPORT_NAME_DEPTCOSTDIFF = "deptCostDiff";  //选择表名
	public static final String BASICINFO_REPORT_DEPTCOSTDIFF_FROZEN = "736,737,738,739,740,741";//左侧固定列
	public static final Integer[]EXPORT_DEPTCOSTDIFF={740,741,742,743,744,745,746,747,748,749,750,751,752,753,754,755,756,757,758,759,760,761};
	
	//分店菜品利润表
	public static final String BASICINFO_REPORT_FIRMFOODPROFIT = "778,779,780,782,783,784,785,786,787,788,789,790,791,795,796,797,798,799,800,801";  //默认选择列
	public static final String REPORT_NAME_FIRMFOODPROFIT = "firmFoodProfit";  //选择表名
	public static final Integer[] EXPORT_FIRMFOODPROFIT = {782,783,784,785,786,787,788,789,790,791,795,796,797,798,799,800,801};
	
	//页面跳转
	public static final String LIST_COST_VARIANCE = "mis/costMng/listCostVariance";//物资进出明细(成本差异)
	public static final String LIST_GROSS_PROFIT="mis/costMng/grossProfit";//毛利查询
	public static final String LIST_SHOULD_YIELD="mis/costMng/shouldYieldAna";//应产率分析
	public static final String LIST_HEJIAN="mis/costMng/hejianDetail";//核减明细
	public static final String LIST_DAYDIFREPORT="mis/costMng/dayDifReport";//每日差异报告
	public static final String LIST_DAYDIFCOMPARE="mis/costMng/dayDifCompare";//每日差异对比
	public static final String LIST_CANGKU_PANDIAN="mis/teShuCaoZuo/cangKuPanDian";//仓库盘点
	public static final String LIST_FENDIAN_YUEJIE="mis/teShuCaoZuo/fenDianYueJie";//分店月结
	public static final String LIST_NIAN_JIE_ZHUAN="mis/teShuCaoZuo/nianJieZhuan";//年结转
	public static final String LIST_YUEMO_JIEZHANG="mis/teShuCaoZuo/yueMoJieZhang";//月末结账
	public static final String LIST_DEPTSUPPLYINOUT="mis/report/deptSupplyInOut";//部门物资进出表
	public static final String LIST_DEPTCOSTDIFF="mis/report/deptCostDiff";//部门成本差异
	public static final String LIST_SUPPLYINPRICECOMPARE="mis/report/supplyInPriceCompare";//物资进货价格比较
	
	public static final String LIST_COSTCARD="mis/report/listCostCard";//分店菜品利润--查看成本卡
	public static final String LIST_COSTDETAIL="mis/report/listItemCostDetail";//分店菜品利润--菜品成本组成物资明细
	public static final String LIST_SALECOSTPROFITTRENDS="mis/report/listSaleCostProfitTrends";//分店菜品利润--菜品销售成本利润走势分析
	public static final String LIST_COSTINTERVAL="mis/report/listCostInterval";//分店菜品利润--菜品成本区间分析
	public static final String LIST_MAOLILVINTERVAL="mis/report/listMaolilvInterval";//分店菜品利润--菜品毛利率区间分析
		
	//报表
	public static final String REPORT_PRINT_URL_DEPTSUPPLYINOUT="/report/chkinm/chkoutmsynquery_catl.jasper";//部门物资进出表报表地址
	public static final String REPORT_PRINT_URL_DEPTCOSTDIFF="/report/chkinm/chkoutmsynquery_catl.jasper";//部门成本差异
	public static final String REPORT_PRINT_URL_CHKOUTMSYNQUERY_CATL="/report/chkinm/chkinmsynquery_catl.jasper";//出库综合查询报表地址
	public static final String REPORT_PRINT_URL_CHKOUTMSYNQUERY_SUM="/report/chkinm/chkinmsynquery_sum.jasper";//报表地址
	public static final String REPORT_PRINT_URL_CHKOUTMSYNQUERY_DETAIL="/report/chkinm/chkinmsynquery_detail.jasper";//报表地址
	public static final String REPORT_PRINT_URL_FIRMFOODPROFIT="/report/firmMis/firmItemSaleProfitAna.jasper";//报表地址
	public static final String REPORT_PRINT_URL_GROSS_PROFIT="/report/firmMis/grossProfit.jasper";//毛利查询
	public static final String REPORT_PRINT_URL_HEJIAN = "report/firmMis/heJian.jasper";//核减明细
	public static final String REPORT_PRINT_URL_DAYDIFREPORT="/report/firmMis/dayDifReport.jasper";//每日差异报告
	public static final String REPORT_PRINT_URL_DAYDIFCOMPARE="/report/firmMis/dayDifCompare.jasper";//每日差异对比
}
