package com.choice.misboh.constants.salesforecast;

public class ForecastChoice3Constants {

	//页面跳转(BS-BOH分店用)
	public static final String MIS_LIST_COSTCTR="misboh/salesforecastChoice3/listCostCTR";//菜品点击率表
	public static final String MIS_LIST_FORECAST="misboh/salesforecastChoice3/listForeCast";//营业预估
	public static final String MIS_LIST_PLAN="misboh/salesforecastChoice3/listPlan";//菜品销售计划
	public static final String MIS_LIST_SALECMP="misboh/salesforecastChoice3/listSaleCmp";//营业预估实际对比
	public static final String MIS_LIST_COSTCMP="misboh/salesforecastChoice3/listCostCmp";//菜品预估实际对比
	public static final String MIS_LIST_CMP="misboh/salesforecastChoice3/listCmp";//菜品预估沽清对比
	
	public static final String REPORT_FORECAST="report/misboh/saleforecastChoice3/itemUseReport.jasper";//菜品点击率表
	public static final String REPORT_CAST="report/misboh/saleforecastChoice3/posSalePlanReport.jasper";//营业预估
	public static final String REPORT_PLAN="report/misboh/saleforecastChoice3/posItemPlanReport.jasper";//菜品销售计划
	public static final String REPORT_SALECMP="report/misboh/saleforecastChoice3/saleCmpReport.jasper";//营业预估实际对比
	public static final String REPORT_COSTCMP="report/misboh/saleforecastChoice3/costCmpReport.jasper";//菜品预估实际对比
	public static final String REPORT_CMP="report/misboh/saleforecastChoice3/cmpReport.jasper";//菜品预估沽清对比
	
	//页面跳转(总店用)
	public static final String LIST_COSTCTR="scm/forecast/listCostCTR";//菜品点击率表
	public static final String LIST_FORECAST="scm/forecast/listForeCast";//营业预估
	public static final String LIST_PLAN="scm/forecast/listPlan";//菜品销售计划
	public static final String LIST_SALECMP="scm/forecast/listSaleCmp";//营业预估实际对比
	public static final String LIST_COSTCMP="scm/forecast/listCostCmp";//菜品预估实际对比
	public static final String LIST_CMP="scm/forecast/listCmp";//菜品预估沽清对比
}
