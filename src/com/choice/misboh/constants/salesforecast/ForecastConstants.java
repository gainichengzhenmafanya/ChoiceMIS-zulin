package com.choice.misboh.constants.salesforecast;

public class ForecastConstants {

	//页面跳转(分店用)
	public static final String MIS_LIST_COSTCTR="misboh/salesforecast/listCostCTR";//菜品点击率表
	public static final String MIS_LIST_FORECAST="misboh/salesforecast/listForeCast";//营业预估
	public static final String MIS_LIST_PLAN="misboh/salesforecast/listPlan";//菜品销售计划

	public static final String REPORT_FORECAST="report/forecast/itemUseReport.jasper";//菜品点击率表
	public static final String REPORT_CAST="report/forecast/posSalePlanReport.jasper";//营业预估
	public static final String REPORT_PLAN="report/forecast/posItemPlanReport.jasper";//菜品销售计划

	//添加菜品
	public static final String MIS_ADDITEM = "misboh/salesforecast/addItem";
	
	//新修改界面 20170730wjf
	public static final String LIST_POSSALEPLAN = "misboh/salesforecast/listPosSalePlan";//营业预估
	public static final String LIST_ITEMUSE = "misboh/salesforecast/listItemUse";//菜品点击率表
	public static final String LIST_POSITEMPALN = "misboh/salesforecast/listPosItemPlan";//菜品销售计划
	
}
