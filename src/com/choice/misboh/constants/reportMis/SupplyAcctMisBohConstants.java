package com.choice.misboh.constants.reportMis;

public class SupplyAcctMisBohConstants {
	
	//首页
	public static final String MISBOH_WARE_REPORT_LIST = "/misboh/reportMis/supplyacct/reportListWare";
	
	//入库汇总查询
	public static final String REPORT_SHOW_CHKINSUMQUERY_MIS = "/misboh/reportMis/supplyacct/rkHuizongChaxun";
	public static final String REPORT_PRINT_URL_CHKINSUMQUERY = "/report/misboh/supplyacct/chkinmsummaryquery.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKINSUMQUERY = "/report/misboh/supplyacct/chkinmsummaryquery.jasper";
	
	// 入库明细汇总
	public static final String REPORT_SHOW_CHKINDETAILSUM = "/misboh/reportMis/supplyacct/rkMingxiHuizong";
	public static final String REPORT_PRINT_URL_CHKINDETAILSUM = "/report/misboh/supplyacct/chkinmdetailsum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKINDETAILSUM = "/report/misboh/supplyacct/chkinmdetailsum.jasper";
	
	//入库明细查询
	public static final String REPORT_SHOW_CHKINDETAILS_MIS = "/misboh/reportMis/supplyacct/rkMingxiChaxun";// 分店用
	public static final String REPORT_PRINT_URL_CHKINDETAILS = "/report/misboh/supplyacct/chkinmdetailquery.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKINDETAILS = "/report/misboh/supplyacct/chkinmdetailquery.jasper";
	
	//出库明细查询
	public static final String REPORT_SHOW_CHKOUTDETAILQUERY_MIS = "/misboh/reportMis/supplyacct/ckMingxiChaxun";
	public static final String REPORT_PRINT_URL_CHKOUTDETAILQUERY = "/report/misboh/supplyacct/chkoutdetailquery.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKOUTDETAILQUERY = "/report/misboh/supplyacct/chkoutdetailquery.jasper";
	
	//出库汇总查询
	public static final String REPORT_SHOW_CHKOUTSUMQUERY_MIS = "/misboh/reportMis/supplyacct/ckHuizongChaxun";
	public static final String REPORT_PRINT_URL_CHKOUTSUMQUERY = "/report/misboh/supplyacct/chkoutsumquery.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKOUTSUMQUERY = "/report/misboh/supplyacct/chkoutsumquery.jasper";
	//出库汇总查询1
	public static final String REPORT_SHOW_CHKOUTSUMQUERY1_MIS = "/misboh/reportMis/supplyacct/ckHuizongChaxun1";
	public static final String REPORT_PRINT_URL_CHKOUTSUMQUERY1 = "/report/misboh/supplyacct/chkoutsumquery1.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKOUTSUMQUERY1 = "/report/misboh/supplyacct/chkoutsumquery1.jasper";
	
	// 类别供应商汇总
	public static final String REPORT_SHOW_CATEGORYDELIVERSUM = "/misboh/reportMis/supplyacct/leibieGysHuizong";
	public static final String REPORT_PRINT_URL_CATEGORYDELIVERSUM = "/report/misboh/supplyacct/CategoryDeliverSum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CATEGORYDELIVERSUM = "/report/misboh/supplyacct/CategoryDeliverSum.jasper";
	
	// 类别供应商汇总1
	public static final String REPORT_SHOW_CATEGORYDELIVERSUM1 = "/misboh/reportMis/supplyacct/leibieGysHuizong1";
	public static final String REPORT_PRINT_URL_CATEGORYDELIVERSUM1 = "/report/misboh/supplyacct/CategoryDeliverSum1.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CATEGORYDELIVERSUM1 = "/report/misboh/supplyacct/CategoryDeliverSum1.jasper";
	
	//供应商类别汇总
	public static final String REPORT_SHOW_DELIVERCATEGORYSUM_MIS = "/misboh/reportMis/supplyacct/gysLeibieHuizong";
	public static final String REPORT_PRINT_URL_DELIVERCATEGORYSUM = "/report/misboh/supplyacct/DeliverCategorySum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_DELIVERCATEGORYSUM = "/report/misboh/supplyacct/DeliverCategorySum.jasper";
	
	//供应商类别汇总3
	public static final String REPORT_SHOW_DELIVERCATEGORYSUM3 = "/misboh/reportMis/supplyacct/gysLeibieHuizong3";
	
	//物资明细账
	public static final String REPORT_SHOW_SUPPLYDETAILSINFO_MIS = "/misboh/reportMis/supplyacct/wzMingxiZhang";
	public static final String REPORT_PRINT_URL_SUPPLYDETAILSINFO = "/report/misboh/supplyacct/SupplyDetailsInfo.jasper";// 报表地址
	public static final String REPORT_EXP_URL_SUPPLYDETAILSINFO = "/report/misboh/supplyacct/SupplyDetailsInfo.jasper";
	
	//物资明细进出
	public static final String REPORT_SHOW_SUPPLYINOUTINFO_MIS = "/misboh/reportMis/supplyacct/wzMingxiJinchu";
	public static final String REPORT_PRINT_URL_SUPPLYINOUTINFO = "/report/misboh/supplyacct/SupplyInOutInfo.jasper";// 报表地址
	public static final String REPORT_EXP_URL_SUPPLYINOUTINFO = "/report/misboh/supplyacct/SupplyInOutInfo.jasper";
	
	//物资综合进出
	public static final String REPORT_SHOW_SUPPLYSUMINOUT_MIS = "/misboh/reportMis/supplyacct/wzZongheJinchubiao";
	public static final String REPORT_PRINT_URL_SUPPLYSUMINOUT = "/report/misboh/supplyacct/SupplySumInOut.jasper";// 报表地址
	public static final String REPORT_EXP_URL_SUPPLYSUMINOUT = "/report/misboh/supplyacct/SupplySumInOut.jasper";
	
	//物资余额查询
	public static final String REPORT_SHOW_SUPPLYBALANCE_MIS = "/misboh/reportMis/supplyacct/wzYueChaxun";
	public static final String REPORT_PRINT_URL_SUPPLYBALANCE = "/report/misboh/supplyacct/SupplyBalance.jasper";// 报表地址
	public static final String REPORT_EXP_URL_SUPPLYBALANCE = "/report/misboh/supplyacct/SupplyBalance.jasper";
	
	// 门店盘点查询 2015.9.1wjf
	public static final String REPORT_SHOW_MDPANDIAN = "/misboh/reportMis/supplyacct/mdPandian";
	public static final String REPORT_PRINT_URL_MDPANDIAN = "/report/misboh/supplyacct/mdPandian.jasper";// 报表地址
	public static final String REPORT_EXP_URL_MDPANDIAN = "/report/misboh/supplyacct/mdPandian.jasper";
	
	// 门店盘点查询 2015.9.1wjf
	public static final String REPORT_SHOW_MDPANDIAN_DETAIL = "/misboh/reportMis/supplyacct/mdPandianDetail";
	public static final String REPORT_PRINT_URL_MDPANDIAN_DETAIL = "/report/misboh/supplyacct/mdPandianDetail.jasper";// 报表地址
	public static final String REPORT_EXP_URL_MDPANDIAN_DETAIL = "/report/misboh/supplyacct/mdPandianDetail.jasper";
	
	// 门店盘点明细 2015.9.1wjf
	public static final String REPORT_SHOW_MDPANDIAN_MINGXI = "/misboh/reportMis/supplyacct/mdPandianMingxi";
	public static final String REPORT_PRINT_URL_MDPANDIAN_MINGXI = "/report/misboh/supplyacct/mdPandianMingxi.jasper";// 报表地址
	public static final String REPORT_EXP_URL_MDPANDIAN_MINGXI = "/report/misboh/supplyacct/mdPandianMingxi.jasper";
	
	// 门店盘点核减状态查询 2015.10.17wjf
	public static final String REPORT_SHOW_MDPDHJZHUANGTAICHAXUN = "/misboh/reportMis/supplyacct/mdPdhjState"; //门店盘点状态表
	public static final String REPORT_PRINT_URL_MDPDHJZHUANGTAICHAXUN = "/report/misboh/supplyacct/mdPdhjState.jasper";// 报表地址
	
	//自定义报表
	public static final String CUSTOMREPORTTREE = "/misboh/reportMis/customReport/customReportTree"; //报表树
	public static final String ADD_FIRSTMENU = "/misboh/reportMis/customReport/addFirstMenu"; //添加一级树节点
	public static final String ADD_CUSTOMREPORT = "/misboh/reportMis/customReport/addCustomReport"; //添加报表树节点
	public static final String UPDATE_FIRSTMENU = "/misboh/reportMis/customReport/updateFirstMenu"; //修改一级树节点
	public static final String UPDATE_CUSTOMREPORT = "/misboh/reportMis/customReport/updateCustomReport"; //修改报表树节点
	
	
	
	
	
	
	
	public static final String MIS_REPORT_LIST = "/mis/report/reportListWare";
	public static final String WARE_REPORT_LIST = "/scm/report/reportListWare";
	public static final String FIRM_REPORT_LIST = "/scm/reportFirm/reportListWare";
	
	// 入库类别汇总
	public static final String REPORT_SHOW_CHKINMCATEGORYSUMMARY = "scm/report/rkLeibieHuizong";
	public static final String REPORT_SHOW_CHKINMCATEGORYSUMMARY_MIS = "/mis/report/rkLeibieHuizong";
	public static final String REPORT_SHOW_CHKINMCATEGORYSUMMARY_FIRM = "/scm/reportFirm/rkLeibieHuizong";
	public static final String REPORT_PRINT_URL_CHKINMCATEGORYSUMMARY = "/report/chkinm/chkinmcategorysummary.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKINMCATEGORYSUMMARY = "/report/chkinm/chkinmcategorysummary.jasper";
	
	// 入库综合查询
	public static final String REPORT_SHOW_CHKINMSYNQUERY = "scm/report/rkZongheChaxun";
	public static final String REPORT_SHOW_CHKINMSYNQUERY_MIS = "/mis/report/rkZongheChaxun";
	public static final String REPORT_SHOW_CHKINMSYNQUERY_FIRM = "scm/reportFirm/rkZongheChaxun";
	public static final String REPORT_PRINT_URL_CHKINMSYNQUERY_CATL = "/report/chkinm/chkinmsynquery_catl.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKINMSYNQUERY_CATL = "/report/chkinm/chkinmsynquery_catl.jasper";
	public static final String REPORT_PRINT_URL_CHKINMSYNQUERY_SUM = "/report/chkinm/chkinmsynquery_sum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKINMSYNQUERY_SUM = "/report/chkinm/chkinmsynquery_sum.jasper";
	public static final String REPORT_PRINT_URL_CHKINMSYNQUERY_DETAIL = "/report/chkinm/chkinmsynquery_detail.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKINMSYNQUERY_DETAIL = "/report/chkinm/chkinmsynquery_detail.jasper";
	
	// 出库明细查询
	public static final String REPORT_SHOW_CHKOUTDETAILQUERY = "scm/report/ckMingxiChaxun";
	public static final String REPORT_SHOW_CHKOUTDETAILQUERY_FIRM = "scm/reportFirm/ckMingxiChaxun";
	
	// 出库明细汇总
	public static final String REPORT_SHOW_CHKOUTDETAILSUM = "scm/report/ckMingxiHuizong";
	public static final String REPORT_SHOW_CHKOUTDETAILSUM_FIRM = "scm/reportFirm/ckMingxiHuizong";
	public static final String REPORT_PRINT_URL_CHKOUTDETAILSUM = "/report/chkout/chkoutdetailsum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKOUTDETAILSUM = "/report/chkout/chkoutdetailsum.jasper";
	
	// 出库汇总查询
	public static final String REPORT_SHOW_CHKOUTSUMQUERY = "scm/report/ckHuizongChaxun";
	public static final String REPORT_SHOW_CHKOUTSUMQUERY_FIRM = "scm/reportFirm/ckHuizongChaxun";
	
	
	// 出库盈利查询
	public static final String REPORT_SHOW_CHKOUTPROFITQUERY = "scm/report/ckYingliChaxun";
	public static final String REPORT_PRINT_URL_CHKOUTPROFITQUERY = "/report/chkout/ChkoutProfitQuery.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKOUTPROFITQUERY = "/report/chkout/ChkoutProfitQuery.jasper";
		
	
	
	// 出库日期汇总
	public static final String REPORT_SHOW_CHKOUTDATESUM = "scm/report/ckRiqiHuizong";
	public static final String REPORT_PRINT_URL_CHKOUTDATESUM = "/report/chkout/chkoutdatesum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKOUTDATESUM = "/report/chkout/chkoutdatesum.jasper";
	
	// 出库日期汇总1
	public static final String REPORT_SHOW_CHKOUTDATESUM1 = "scm/report/ckRiqiHuizong1";
	public static final String REPORT_PRINT_URL_CHKOUTDATESUM1 = "/report/chkout/chkoutdatesum1.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKOUTDATESUM1 = "/report/chkout/chkoutdatesum1.jasper";
	
	// 出库日期汇总2
	public static final String REPORT_SHOW_CHKOUTDATESUM2 = "scm/report/ckRiqiHuizong2";
	public static final String REPORT_PRINT_URL_CHKOUTDATESUM2 = "/report/chkout/chkoutdatesum2.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKOUTDATESUM2 = "/report/chkout/chkoutdatesum2.jasper";
	
	// 出库类别汇总
	public static final String REPORT_SHOW_CHKOUTCATEGORYSUM = "scm/report/ckLeibieHuizong";
	// 出库类别汇总-分店用
	public static final String REPORT_SHOW_CHKOUTMSYNQUERY_MIS="mis/report/ckLeibieHuizong";
	public static final String REPORT_PRINT_URL_CHKOUTCATEGORYSUM = "/report/chkout/ChkoutCategorySum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKOUTCATEGORYSUM = "/report/chkout/ChkoutCategorySum.jasper";
	
	// 类别分店汇总
	public static final String REPORT_SHOW_CATEGORYPOSITNSUM = "scm/report/leibieFendianHuizong";
	public static final String REPORT_PRINT_URL_CATEGORYPOSITNSUM = "/report/chkout/CategoryPositnSum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CATEGORYPOSITNSUM = "/report/chkout/CategoryPositnSum.jasper";
	
	// 分店类别汇总
	public static final String REPORT_SHOW_POSITNCATEGORYSUM = "scm/report/fendianLeibieHuizong";
	public static final String REPORT_PRINT_URL_POSITNCATEGORYSUM = "/report/chkout/PositnCategorySum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_POSITNCATEGORYSUM = "/report/chkout/PositnCategorySum.jasper";
	
	// 出库单据汇总
	public static final String REPORT_SHOW_CHKOUTORDERSUM = "scm/report/ckDanjuHuizong";
	public static final String REPORT_PRINT_URL_CHKOUTORDERSUM = "/report/chkout/ChkoutOrderSum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKOUTORDERSUM = "/report/chkout/ChkoutOrderSum.jasper";
	
	// 配送验货查询
	public static final String REPORT_SHOW_DELIVERYEXAMINEQUERY = "scm/report/PsYanhuoChaxun";
	public static final String REPORT_PRINT_URL_DELIVERYEXAMINEQUERY = "/report/chkout/DeliveryExamineQuery.jasper";// 报表地址
	public static final String REPORT_EXP_URL_DELIVERYEXAMINEQUERY = "/report/chkout/DeliveryExamineQuery.jasper";
	
	// 调拨明细查询
	public static final String REPORT_SHOW_CHKALLOTDETAILQUERY = "scm/report/dbMingxiChaxun";
	public static final String REPORT_SHOW_CHKALLOTDETAILQUERY_FIRM = "scm/reportFirm/dbMingxiChaxun";
	public static final String REPORT_PRINT_URL_CHKALLOTDETAILQUERY = "/report/allot/allotdetailquery.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKALLOTDETAILQUERY="/report/allot/allotdetailquery.jasper";
	
	// 调拨汇总查询
	public static final String REPORT_SHOW_CHKALLOTDSUMQUERY = "scm/report/dbHuizongChaxun";
	public static final String REPORT_SHOW_CHKALLOTDSUMQUERY_FIRM = "scm/reportFirm/dbHuizongChaxun";
	public static final String REPORT_PRINT_URL_CHKALLOTDSUMQUERY = "/report/allot/allotsumquery.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKALLOTDSUMQUERY = "/report/allot/allotsumquery.jasper";
	
	// 供应商进货汇总
	public static final String REPORT_SHOW_DELIVERSTOCKSUM = "scm/report/GysJinhuoHuizong";
	public static final String REPORT_SHOW_DELIVERSTOCKSUM_MIS = "/mis/report/GysJinhuoHuizong";
	public static final String REPORT_PRINT_URL_DELIVERSTOCKSUM = "/report/deliver/DeliverStockSum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_DELIVERSTOCKSUM = "/report/deliver/DeliverStockSum.jasper";
	
	// 供应商分店汇总
	public static final String REPORT_SHOW_DELIVERPOSITNSUM = "scm/report/gysFendianHuizong";
	public static final String REPORT_PRINT_URL_DELIVERPOSITNSUM = "/report/deliver/DeliverPositnSum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_DELIVERPOSITNSUM = "/report/deliver/DeliverPositnSum.jasper";
	
	// 供应商类别汇总
	public static final String REPORT_SHOW_DELIVERCATEGORYSUM = "scm/report/gysLeibieHuizong";
	
	
	// 供应商类别汇总2
	public static final String REPORT_SHOW_DELIVERCATEGORYSUM2 = "scm/report/gysLeibieHuizong2";
	public static final String REPORT_PRINT_URL_DELIVERCATEGORYSUM2 = "/report/deliver/DeliverCategorySum2.jasper";// 报表地址
	public static final String REPORT_EXP_URL_DELIVERCATEGORYSUM2 = "/report/deliver/DeliverCategorySum2.jasper";
	
	// 供应商汇总列表
	public static final String REPORT_SHOW_DELIVERSUM = "scm/report/gysHuizongLiebiao";
	public static final String REPORT_SHOW_DELIVERSUM_MIS = "/mis/report/gysHuizongLiebiao";
	public static final String REPORT_PRINT_URL_DELIVERSUM = "/report/deliver/DeliverSum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_DELIVERSUM = "/report/deliver/DeliverSum.jasper";
	
	// 进货单据汇总
	public static final String REPORT_SHOW_STOCKBILLSUM  = "scm/report/jhDanjuHuizong";
	public static final String REPORT_SHOW_STOCKBILLSUM_MIS  = "/mis/report/jhDanjuHuizong";
	public static final String REPORT_PRINT_URL_STOCKBILLSUM = "/report/stockbill/StockBillSum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_STOCKBILLSUM = "/report/stockbill/StockBillSum.jasper";
	
	// 供应商付款情况
	public static final String REPORT_SHOW_DELIVERPAYMENT = "scm/report/gysFukuanQingkuang";
	public static final String REPORT_SHOW_DELIVERPAYMENT_MIS = "/mis/report/gysFukuanQingkuang";
	public static final String REPORT_PRINT_URL_DELIVERPAYMENT = "/report/deliver/DeliverPayment.jasper";// 报表地址
	public static final String REPORT_EXP_URL_DELIVERPAYMENT = "/report/deliver/DeliverPayment.jasper";
	
	// 物资明细账
	public static final String REPORT_SHOW_SUPPLYDETAILSINFO = "scm/report/wzMingxiZhang";
	
		
	// 物资余额查询
	public static final String REPORT_SHOW_SUPPLYBALANCE = "scm/report/wzYueChaxun";
	
	
	// 物资明细进出表
	public static final String REPORT_SHOW_SUPPLYINOUTINFO = "scm/report/wzMingxiJinchu";
	public static final String REPORT_SHOW_SUPPLYINOUTINFO_FIRM = "scm/reportFirm/wzMingxiJinchu";
	
	
	// 物资类别进出表
	public static final String REPORT_SHOW_SUPPLYTYPINOUT = "scm/report/wzLeibieJinchubiao";
	public static final String REPORT_SHOW_SUPPLYTYPINOUT_FIRM = "scm/reportFirm/wzLeibieJinchubiao";
	public static final String REPORT_SHOW_SUPPLYTYPINOUT_MIS = "/mis/report/wzLeibieJinchubiao";
	public static final String REPORT_PRINT_URL_SUPPLYTYPINOUT = "report/supply/SupplyTypInOut.jasper";// 报表地址
	public static final String REPORT_EXP_URL_SUPPLYTYPINOUT = "report/supply/SupplyTypInOut.jasper";
	
	// 物资综合进出表
	public static final String REPORT_SHOW_SUPPLYSUMINOUT = "scm/report/wzZongheJinchubiao";
	public static final String REPORT_SHOW_SUPPLYSUMINOUT_FIRM = "scm/reportFirm/wzZongheJinchubiao";
	
	
	// 物资仓库进出表		
	public static final String REPORT_SHOW_GOODSSTOREINOUT  = "scm/report/wzCangkuJinchubiao";
	public static final String REPORT_SHOW_GOODSSTOREINOUT_FIRM  = "scm/reportFirm/wzCangkuJinchubiao";
	public static final String REPORT_SHOW_GOODSSTOREINOUT_MIS  = "/mis/report/wzCangkuJinchubiao";
	public static final String REPORT_PRINT_URL_GOODSSTOREINOUT = "/report/supply/GoodsStoreInout.jasper";// 报表地址
	public static final String REPORT_EXP_URL_GOODSSTOREINOUT = "/report/supply/GoodsStoreInout.jasper";

	// 仓库类别进出表
	public static final String REPORT_SHOW_STOCKCATEGORYINOUT = "scm/report/cangkuLeibieJinchu";
	public static final String REPORT_PRINT_URL_STOCKCATEGORYINOUT = "/report/stockbill/StockCategoryInOut.jasper";// 报表地址
	public static final String REPORT_EXP_URL_STOCKCATEGORYINOUT = "/report/stockbill/StockCategoryInOut.jasper";
	
	// 分店毛利查询
	public static final String LIST_GROSS_PROFIT = "scm/prdPrcCM/grossProfit";
	public static final String REPORT_PRINT_URL_GROSS_PROFIT = "report/prdPrcCM/grossProfit.jasper";// 报表地址
	public static final String REPORT_EXP_URL_GROSS_PROFIT = "report/prdPrcCM/grossProfit.jasper";
	
	// 门店盘点状态查询查询
	public static final String LIST_MDRIPANDIANZHUANGTAIBIAO_PROFIT = "scm/report/mdRiPanDianZhuangTaiBiao"; //门店盘点状态表
	public static final String REPORT_PRINT_URL_MDRIPANDIANZHUANGTAIBIAO_PROFIT = "scm/report/mdRiPanDianZhuangTaiBiao";// 报表地址
	
	// 存货盘点
	public static final String REPORT_SHOW_CUNHUOPANDIAN_FIRM = "scm/reportFirm/cunhuoPandian";     
	public static final String REPORT_PRINT_URL_CUNHUOPANDIAN = "/report/cunhuoPandian/cunhuoPandian.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CUNHUOPANDIAN = "/report/cunhuoPandian/cunhuoPandian.jasper";
	
	// 月末盘点
	public static final String REPORT_SHOW_YUEMOPANDIAN_FIRM = "scm/reportFirm/yuemoPandian";     
	public static final String REPORT_SHOW_YUEMOPANDIAN_FIRM2 = "scm/reportFirm/yuemoPandian2";     
	public static final String REPORT_PRINT_URL_YUEMOPANDIAN = "/report/yuemoPandian/yuemoPandian.jasper";// 报表地址
	public static final String REPORT_EXP_URL_YUEMOPANDIAN = "/report/yuemoPandian/yuemoPandian.jasper";
	
	// 差异管理
	public static final String REPORT_SHOW_CHAYIGUANLI_FIRM = "scm/reportFirm/chayiGuanli";     
	public static final String REPORT_PRINT_URL_CHAYIGUANLI = "/report/chayiGuanli/chayiGuanli.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHAYIGUANLI = "/report/chayiGuanli/chayiGuanli.jasper";
	
	// 每日差异对照
	public static final String REPORT_SHOW_MEIRICHAYIDUIZHAO_FIRM = "scm/reportFirm/meiriChayiDuizhao";     
	public static final String REPORT_PRINT_URL_MEIRICHAYIDUIZHAO = "/report/meiriChayiDuizhao/meiriChayiDuizhao.jasper";// 报表地址
	public static final String REPORT_EXP_URL_MEIRICHAYIDUIZHAO = "/report/meiriChayiDuizhao/meiriChayiDuizhao.jasper";
	
	// 历史盘点
	public static final String REPORT_SHOW_LISHIPANDIAN_FIRM = "scm/reportFirm/lsPandianFirm";     
	public static final String REPORT_PRINT_URL_LISHIPANDIAN_FIRM = "/report/lsPandianFirm/lsPandianFirm.jasper";// 报表地址
	public static final String REPORT_EXP_URL_LISHIPANDIAN_FIRM = "/report/lsPandianFirm/lsPandianFirm.jasper";
		
	// 调拨汇总
	public static final String REPORT_SHOW_DIAOBOHUIZONG_FIRM = "scm/reportFirm/dbHuizongFirm";     
	public static final String REPORT_PRINT_URL_DIAOBOHUIZONG_FIRM = "/report/dbHuizongFirm/dbHuizongFirm.jasper";// 报表地址
	public static final String REPORT_EXP_URL_DIAOBOHUIZONG_FIRM = "/report/dbHuizongFirm/dbHuizongFirm.jasper";
		
	// 分店盈利情况表
	public static final String REPORT_SHOW_FDYINGLIQINGKUANG = "scm/report/fdYingliQingkuang";
	public static final String REPORT_PRINT_URL_FDYINGLIQINGKUANG = "/report/fdYingliQingkuang/fdYingliQingkuang.jasper";// 报表地址
	public static final String REPORT_EXP_URL_FDYINGLIQINGKUANG = "/report/fdYingliQingkuang/fdYingliQingkuang.jasper";
	
	// 物资成本汇总表
//	public static final String REPORT_SHOW_WZCHENGBENHUIZONG = "scm/reportFirm/wzChengbenHuizong";
	public static final String REPORT_SHOW_WZCHENGBENHUIZONG_FIRM = "scm/reportFirm/wzChengbenHuizong";
	public static final String REPORT_PRINT_URL_WZCHENGBENHUIZONG = "report/wzChengbenHuizong/wzChengbenHuizong.jasper";// 报表地址
	public static final String REPORT_EXP_URL_WZCHENGBENHUIZONG = "report/wzChengbenHuizong/wzChengbenHuizong.jasper";
	
	// 物资成本明细表
//		public static final String REPORT_SHOW_WZCHENGBENHUIZONG = "scm/reportFirm/wzChengbenHuizong";
	public static final String REPORT_SHOW_WZCHENGBENMINGXI_FIRM = "scm/reportFirm/wzChengbenMingxi";
	public static final String REPORT_PRINT_URL_WZCHENGBENMINGXI = "report/wzChengbenHuizong/wzChengbenMingxi.jasper";// 报表地址
	public static final String REPORT_EXP_URL_WZCHENGBENMINGXI = "report/wzChengbenHuizong/wzChengbenMingxi.jasper";
	
	// 月成本综合分析
//		public static final String REPORT_SHOW_WZCHENGBENHUIZONG = "scm/reportFirm/wzChengbenHuizong";
	public static final String REPORT_SHOW_YUECHENGBENZONGHEFENXI_FIRM = "scm/reportFirm/yueChengbenZongheFenxi";
	public static final String REPORT_PRINT_URL_YUECHENGBENZONGHEFENXI = "report/yueChengbenZongheFenxi/yueChengbenZongheFenxi.jasper";// 报表地址
	public static final String REPORT_EXP_URL_YUECHENGBENZONGHEFENXI = "report/yueChengbenZongheFenxi/yueChengbenZongheFenxi.jasper";
	
	// 存货汇总
	public static final String REPORT_SHOW_CUNHUOHUIZONG_FIRM = "scm/reportFirm/cunhuoHuizong";     
	public static final String REPORT_PRINT_URL_CUNHUOHUIZONG = "/report/cunhuoHuizong/cunhuoHuizong.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CUNHUOHUIZONG = "/report/cunhuoHuizong/cunhuoHuizong.jasper";
	
	// 第二单位查询
	public static final String REPORT_SHOW_SECONDUNIT_FIRM = "scm/reportFirm/secondunit";
	public static final String REPORT_SHOW_SECONDUNITCY_FIRM = "scm/reportFirm/secondunit_cy";
	public static final String REPORT_PRINT_URL_SECONDUNIT_SUPPLY = "/report/secondunit/secondunit_supply.jasper";// 报表地址
	public static final String REPORT_EXP_URL_SECONDUNIT_SUPPLY = "/report/secondunit/secondunit_supply.jasper";
	public static final String REPORT_PRINT_URL_SECONDUNIT_DATE = "/report/secondunit/secondunit_date.jasper";// 报表地址
	public static final String REPORT_EXP_URL_SECONDUNIT_DATE = "/report/secondunit/secondunit_date.jasper";
	public static final String REPORT_PRINT_URL_SECONDUNIT_CHAYI = "/report/secondunit/secondunit_chayi.jasper";// 报表地址
	public static final String REPORT_EXP_URL_SECONDUNIT_CHAYI = "/report/secondunit/secondunit_chayi.jasper";
	
	// 历史盘点查询 2014.12.9wjf
	public static final String REPORT_SHOW_LISHIPANDIAN = "scm/report/lsPandian";
	public static final String REPORT_PRINT_URL_LISHIPANDIAN = "report/lsPandian/lsPandian.jasper";// 报表地址
	public static final String REPORT_EXP_URL_LISHIPANDIAN = "report/lsPandian/lsPandian.jasper";
}
