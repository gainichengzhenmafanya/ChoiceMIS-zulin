package com.choice.misboh.constants.reportMis;

public final class CostItemMisBohConstants {
	
	private CostItemMisBohConstants(){}
	
	//列选择跳转的页面
	public static final String TO_COLUMNS_CHOOSE_VIEW="share/columnsChoose";
	
	//物资成本差异
	public static final String LIST_COST_VARIANCE = "misboh/reportMis/costitem/listCostVariance";//物资进出明细(成本差异)
	public static final String REPORT_PRINT_URL_COST_VARIANCE = "report/misboh/costitem/CostVariance.jasper";//报表地址
	public static final String REPORT_EXP_URL_COST_VARIANCE = "report/misboh/costitem/CostVariance.jasper";
	
	//应产率分析
	public static final String LIST_PTIVITYANA = "misboh/reportMis/costitem/ptivityAna";
	public static final String REPORT_PRINT_URL_PTIVITYANA = "report/misboh/costitem/ptivity.jasper";//打印文件
	
	//毛利查询
	public static final String LIST_GROSS_PROFIT="misboh/reportMis/costitem/grossProfit";//毛利查询
	public static final String REPORT_PRINT_URL_GROSS_PROFIT="/report/misboh/costitem/grossProfit.jasper";//毛利查询
	
	//月成本综合分析
	public static final String REPORT_SHOW_MISYUECHENGBENZONGHEFENXI = "misboh/reportMis/costitem/yueChengbenZongheFenxi";
	public static final String REPORT_PRINT_URL_MISYUECHENGBENZONGHEFENXI = "/report/misboh/costitem/yueChengbenZongheFenxi.jasper";// 报表地址
	public static final String REPORT_EXP_URL_MISYUECHENGBENZONGHEFENXI = "/report/misboh/costitem/yueChengbenZongheFenxi.jasper";
	public static final String REPORT_SHOW_MISYUECHENGBENZONGHEFENXICHOICE3 = "misboh/reportMis/costitem/yueChengbenZongheFenxiChoice3";
	public static final String REPORT_PRINT_URL_MISYUECHENGBENZONGHEFENXICHOICE3 = "/report/misboh/costitem/yueChengbenZongheFenxiChoice3.jasper";
	public static final String REPORT_EXP_URL_MISYUECHENGBENZONGHEFENXICHOICE3 = "/report/misboh/costitem/yueChengbenZongheFenxiChoice3.jasper";
	
	//菜品利润明细
	public static final String LIST_FIRMFOODPROFIT="misboh/reportMis/costitem/firmFoodProfit";//分店菜品利润
	public static final String LIST_COSTCARD="misboh/reportMis/costitem/listCostCard";//分店菜品利润--查看成本卡
	public static final String LIST_COSTDETAIL="misboh/reportMis/costitem/listItemCostDetail";//分店菜品利润--菜品成本组成物资明细
	public static final String LIST_SALECOSTPROFITTRENDS="misboh/reportMis/costitem/listSaleCostProfitTrends";//分店菜品利润--菜品销售成本利润走势分析
	public static final String LIST_COSTINTERVAL="misboh/reportMis/costitem/listCostInterval";//分店菜品利润--菜品成本区间分析
	public static final String LIST_MAOLILVINTERVAL="misboh/reportMis/costitem/listMaolilvInterval";//分店菜品利润--菜品毛利率区间分析
	public static final String REPORT_PRINT_URL_FIRMFOODPROFIT="/report/firmMis/firmItemSaleProfitAna.jasper";//报表地址

	//分店档口毛利
	public static final String REPORT_SHOW_FDDANGKOUMAOLI = "misboh/reportMis/costitem/fdDangkouMaoli";//分店菜品利润
	
	//核减明细
	public static final String LIST_HEJIAN="misboh/reportMis/costitem/hejianDetail";//核减明细
	public static final String REPORT_PRINT_URL_HEJIAN = "report/firmMis/heJian.jasper";//核减明细
	
	//单菜毛利率
	public static final String LIST_DANCAIMAOLILV="misboh/reportMis/costitem/dancaiMaolilv";
	public static final String REPORT_PRINT_URL_DANCAIMAOLILV = "/report/misboh/costitem/dancaiMaolilv.jasper";
	
	//应产率分析
	public static final String REPORT_SHOW_PTIVITYANA = "misboh/reportMis/costitem/ptivityAna";
	public static final String REPORT_SHOW_PTIVITYANATEST = "misboh/reportMis/costitem/ptivityAnaTest";
	public static final String REPORT_URL_PTIVITYANA = "report/misboh/costitem/ptivity.jasper";//打印文件
		
	//毛利查询
	public static final String REPORT_SHOW_GROSSPROFIT="misboh/reportMis/costitem/grossProfit";//毛利查询
	public static final String REPORT_URL_GROSSPROFIT="/report/misboh/costitem/grossProfit.jasper";//毛利查询
	
	//物资万元用量分析
	public static final String REPORT_SHOW_WZWANYUANYONGLIANGFENXI = "misboh/reportMis/costitem/wzWanyuanyongliangFenxi";
	//物资用量分析
	public static final String REPORT_SHOW_WZYONGLIANGFENCHAFENXI = "misboh/reportMis/costitem/wzYongliangfenchaFenxi";
}