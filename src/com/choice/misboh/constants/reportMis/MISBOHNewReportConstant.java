package com.choice.misboh.constants.reportMis;

/**
 * 描述：各项目报表页面跳转
 * @author 马振
 * 创建时间：2016-2-22 上午11:32:39
 */
public class MISBOHNewReportConstant {

	/**
	 * 西贝  营收客流分段统计明细表
	 */
	public static final String REPORT_SHOW_BUSINESSBURST = "misboh/newReport/xibei/businessburstanalysis";

	/**
	 * 西贝   营业时段解析表
	 */
	public static final String REPORT_SHOW_SALEDATABYTIME = "misboh/newReport/xibei/saledatabytime";
	
	/**
	 * 西贝   菜品见台率统计分析
	 */
	public static final String REPORT_SHOW_CAIPINJIANTAILV = "misboh/newReport/xibei/caiPinJianTaiLv";
	
	/**
	 * 权金城  排班报表
	 */
	public static final String REPORT_SHOW_SCHEDULEREPORT7 = "misboh/newReport/qjc/scheduleReport7";
}