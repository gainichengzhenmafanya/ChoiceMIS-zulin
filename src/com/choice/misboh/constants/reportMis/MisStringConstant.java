
package com.choice.misboh.constants.reportMis;

/**
 * 描述：获取报表表头的公共类
 * @author 马振
 * 创建时间：2015-4-16 下午6:18:08
 */
public class MisStringConstant {
	//map
	public static final String IREPORT_HTML = "share/ireport/mapSource/html";  				//html页面
	public static final String IREPORT_EXCEL = "share/ireport/mapSource/excel";  			//excel页面
	public static final String IREPORT_PDF = "share/ireport/mapSource/pdf";  				//pdf页面
	public static final String IREPORT_WORD = "share/ireport/mapSource/word";  				//word页面
	public static final String IREPORT_PRINT_EXCEL = "share/ireport/mapSource/printExcel";  //直接打印excel页面
	public static final String IREPORT_PRINT_PDF = "share/ireport/mapSource/printPdf";  	//直接打印pdf页面
	public static final String IREPORT_PRINT_WORD = "share/ireport/mapSource/printWord";  	//直接打印word页面
	
	public static final String IREPORT_HTML_BEAN = "share/ireport/html";  					//html页面
	public static final String IREPORT_EXCEL_BEAN = "share/ireport/excel";  				//excel页面
	public static final String IREPORT_PDF_BEAN = "share/ireport/pdf";  					//pdf页面
	public static final String IREPORT_WORD_BEAN = "share/ireport/word";  					//word页面
	public static final String IREPORT_PRINT_EXCEL_BEAN = "share/ireport/printExcel";  		//直接打印excel页面
	public static final String IREPORT_PRINT_PDF_BEAN = "share/ireport/printPdf";  			//直接打印pdf页面
	public static final String IREPORT_PRINT_WORD_BEAN = "share/ireport/printWord";  		//直接打印word页面
	public static final String TO_COLUMNS_CHOOSE_VIEW="share/columnsChoose";
	
	/**************************************营业分析***************************************/
	
	//人事管理——人员列表
	public static final String BASICINFO_REPORT_EMPLOYEE = "1200002,1200003,1200004," +
		"1200005,1200006,1200007,1200008,1200009,12000010,12000011,12000012,12000013,12000014," +
		"12000015,12000016,12000017,12000018,12000019,12000020,12000021";  					//默认选择列
	public static final String REPORT_NAME_EMPLOYEE = "MisBohEmployee";  					//选择表名
	public static final String REPORT_NAME_CN_EMPLOYEE = "人员列表";  							//文件名
	public static final String BASICINFO_REPORT_EMPLOYEE_FROZEN = "MisBohEmployee";
	public static final String REPORT_QUERY_METHOD_EMPLOYEE = "listEmployee";
	
	//现金日报表报表
	public static final String BASICINFO_REPORT_XJRREPORT = "413345,413347,413348";  		//默认选择列
	public static final String REPORT_NAME_XJRREPORT = "BohXjrReport";  					//选择表名
	public static final String REPORT_NAME_CN_XJRREPORT = "现金日报表";  						//文件名
	public static final String BASICINFO_REPORT_XJRREPORT_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_XJRREPORT = "queryXjrReport";
	public static final Integer[] EXCEL_XJRREPORT={413348};
	
	//账单查询
	public static final String REPORT_NAME_CN_ZDMXDET = "账单查询"; 
	public static final String REPORT_NAME_ZDMXDET = "";  									//选择表名
	public static final String BASICINFO_REPORT_ZDMXDET_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_ZDMXDET = "queryFolioDetailInfo";
	
	//当日查询——账单查询
	public static final String REPORT_NAME_CN_ZDMXDETCUR = "账单查询"; 
	public static final String REPORT_NAME_ZDMXDETCUR = "";  									//选择表名
	public static final String BASICINFO_REPORT_ZDMXDETCUR_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_ZDMXDETCUR = "queryFolioDetailInfoCur";
	
	//账单明细分析
	public static final String BASICINFO_REPORT_ZDMXFX = "79,80,81,82,83,84,85,86,87";  	//默认选择列
	public static final String REPORT_NAME_ZDMXFX = "BohZdMxfx";  							//选择表名
	public static final String REPORT_NAME_CN_ZDMXFX = "账单明细分析";  							//选择表名
	public static final String BASICINFO_REPORT_ZDMXFX_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_ZDMXFX = "queryZdMxfx";
	public static final Integer[] EXCEL_ZDMXFX = {81,82,85,86,87};
	
	//大类销售报表
	public static final String BASICINFO_REPORT_INVOICETYPE = "3328,3329,3330,3331,3332,3333,3334,3335,460018,460019,460020";  //默认选择列
	public static final String REPORT_NAME_INVOICETYPE = "InvoiceType";  //选择表名
	public static final String REPORT_NAME_CN_INVOICETYPE = "大类销售报表";  //选择表名
	public static final String BASICINFO_REPORT_INVOICETYPE_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_INVOICETYPE = "listInvoiceType";
	public static final Integer[] EXCEL_INVOICETYPE = {3330,3331,3332,3333,3334,3335,460018,460019,460020};
	
	//中类销售报表
	public static final String BASICINFO_REPORT_INVOICEGRP = "3328,3329,3330,3331,3332,3333,3334,3335";  //默认选择列
	public static final String REPORT_NAME_INVOICEGRP = "InvoiceType";  //选择表名与大类 公用
	public static final String REPORT_NAME_CN_INVOICEGRP = "中类销售报表";  //选择表名
	public static final String BASICINFO_REPORT_INVOICEGRP_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_INVOICEGRP = "listInvoiceType";
	public static final Integer[] EXCEL_INVOICEGRP = {3330,3331,3332,3333,3334,3335};
	
	//小类销售报表
	public static final String BASICINFO_REPORT_INVOICETYP = "3328,3329,3330,3331,3332,3333,3334,3335";  //默认选择列
	public static final String REPORT_NAME_INVOICETYP = "InvoiceType";  //选择表名与大类 公用
	public static final String REPORT_NAME_CN_INVOICETYP = "小类销售报表";  //选择表名
	public static final String BASICINFO_REPORT_INVOICETYP_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_INVOICETYP = "listInvoiceType";
	public static final Integer[] EXCEL_INVOICETYP = {3330,3331,3332,3333,3334,3335};
	
	//菜品销售汇总（最终产品售卖量表）
	public static final String BASICINFO_REPORT_CPXSHZ = "3244,3245,3246,3247,3248,3249,3250,3251,3252,3253,3254,3255,3285,3286";  //默认选择列
	public static final String REPORT_NAME_CPXSHZ = "BohCpxshz";  		//选择表名
	public static final String REPORT_NAME_CN_CPXSHZ = "菜品销售汇总报表";  //选择表名 最终产品售卖量表-》菜品活动类别分析
	public static final String BASICINFO_REPORT_CPXSHZ_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_CPXSHZ = "queryCpxshz";
	public static final Integer[] EXCEL_CPXSHZ = {3246,3247,3248,3249,3250,3251,3252,3253,3254,3255,3285,3286};
	
	/**
	 * 菜品销售表
	 */
	public static final String REPORT_NAME_POSCYBB = "";  				//选择表名
	public static final String REPORT_NAME_CN_POSCYBB = "菜品销售报表"; 	//选择表名
	public static final String BASICINFO_REPORT_POSCYBB = "";
	public static final String BASICINFO_REPORT_POSCYBB_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_POSCYBB = "queryPoscybb";

	/**
	 * 菜品销售明细查询表
	 */
	public static final String REPORT_NAME_ITEMDETAIL = "";  					//选择表名
	public static final String REPORT_NAME_CN_ITEMDETAIL = "菜品销售明细查询报表"; 	//选择表名
	public static final String BASICINFO_REPORT_ITEMDETAIL = "";
	public static final String BASICINFO_REPORT_ITEMDETAIL_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_ITEMDETAIL = "queryItemDetail";
	
	//套餐销售报表
	public static final String BASICINFO_REPORT_BOHSALES = "3279,3280,3281,3282,3283,3278,3284";  //默认选择列
	public static final String REPORT_NAME_BOHSALES = "BohSales";  //选择表名
	public static final String REPORT_NAME_CN_BOHSALES = "套餐销售报表";  //选择表名
	public static final String BASICINFO_REPORT_BOHSALES_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_BOHSALES = "listBohSales";
	public static final Integer[] EXCEL_BOHSALES = {3281,3282,3283,3284};
	
	//营运报告-日汇总
	public static final String REPORT_NAME_YYBG_YK = "";  
	public static final String REPORT_NAME_CN_YYBG_YK= "营运报告";  
	public static final String REPORT_QUERY_METHOD_YYBG_YK = "queryyybg";
	
	/**
	 * 当日查询——营运报告
	 */
	public static final String REPORT_NAME_YYBGCUR = "";  
	public static final String REPORT_NAME_CN_YYBGCUR= "营运报告";  
	public static final String REPORT_QUERY_METHOD_YYBGCUR = "queryYybgCur";
	
	//退菜明细报表
	public static final String REPORT_NAME_TCMXBB = ""; 
	public static final String REPORT_NAME_CN_TCMXBB = "退菜明细报表"; 
	public static final String REPORT_QUERY_METHOD_TCMXBB = "queryTcmxbb";
	
	//收银员统计
	public static final String REPORT_NAME_SYYTJ = ""; 
	public static final String REPORT_NAME_CN_SYYTJ = "收银员统计"; 
	public static final String REPORT_QUERY_METHOD_SYYTJ = "queryCashierStatistics";
	
	//附加项销售统计
	public static final String REPORT_NAME_ADDITIONALTERM = ""; 
	public static final String REPORT_NAME_CN_ADDITIONALTERM = "附加项销售统计"; 
	public static final String REPORT_QUERY_METHOD_ADDITIONALTERM = "queryAdditionalTerm";

	//时段营业额报告
	public static final String REPORT_NAME_PERIODTURNOVERREPORT = ""; 
	public static final String REPORT_NAME_CN_PERIODTURNOVERREPORT = "时段营业额报告"; 
	public static final String REPORT_QUERY_METHOD_PERIODTURNOVERREPORT = "queryPeriodTurnoverReport";
	
	//点菜员提成
	public static final String REPORT_NAME_ORDERINGMEMBERCOMMISSION = ""; 
	public static final String REPORT_NAME_CN_ORDERINGMEMBERCOMMISSION = "点菜员提成报表"; 
	public static final String REPORT_QUERY_METHOD_ORDERINGMEMBERCOMMISSION = "queryOrderingMemberCommission";
	
	//点菜员提成明细
	public static final String REPORT_NAME_DCYTCMX = ""; 
	public static final String REPORT_NAME_CN_DCYTCMX = "点菜员提成明细报表"; 
	public static final String REPORT_QUERY_METHOD_DCYTCMX = "queryOrderingMemberCommissionDetail";
	
	/**
	 * 支付活动明细报表
	 */
	public static final String REPORT_NAME_PAYDETAIL = ""; 
	public static final String REPORT_NAME_CN_PAYDETAIL = "支付方式明细报表"; 
	public static final String REPORT_QUERY_METHOD_PAYDETAIL = "queryPayDetail";
	
	/****************************************************中餐报表*******************************************************/
	//集团收入分析（按段汇总）
	public static final String BASICINFO_REPORT_GROUPINCOME_BYDATE = "470130,470131,470132,470133,470134,470135,470136,470137,470138,470139,470140,470141,470142";  //默认选择列
	public static final String REPORT_NAME_GROUPINCOME_BYDATE = "groupIncome_ByDate";  //选择表名
	public static final String REPORT_NAME_CN_GROUPINCOME_BYDATE = "集团收入分析-按段汇总";  //选择表名
	public static final String BASICINFO_REPORT_GROUPINCOME_BYDATE_FROZEN = "470130";
	public static final String REPORT_QUERY_METHOD_GROUPINCOME_BYDATE = "findGroupIncome";
	
	//集团收入分析（按日明细）
	public static final String BASICINFO_REPORT_GROUPINCOME_BYDAY = "470143,470144,470145,470146,470147,470148,470149,470150,470151,470152,470153,470154,470155,470156";  //默认选择列
	public static final String REPORT_NAME_GROUPINCOME_BYDAY = "groupIncome_ByDay";  //选择表名
	public static final String REPORT_NAME_CN_GROUPINCOME_BYDAY = "集团收入分析-按日明细";  //选择表名
	public static final String BASICINFO_REPORT_GROUPINCOME_BYDAY_FROZEN = "470143";
	public static final String REPORT_QUERY_METHOD_GROUPINCOME_BYDAY = "findGroupIncome";
	
	//集团销售分析按菜销售汇总
	public static final String BASICINFO_REPORT_SALES_SUM_BYFOOD = "470213,470214,470215,470216,470217,470218";  //默认选择列
	public static final String REPORT_NAME_SALES_SUM_BYFOOD = "GroupSalesSumByFood";  	//选择表名
	public static final String REPORT_NAME_CN_SALES_SUM_BYFOOD = "按菜销售汇总表";  			//选择表名
	public static final String BASICINFO_REPORT_SALES_SUM_BYFOOD_FROZEN = "470213";
	public static final String REPORT_QUERY_METHOD_SALES_SUM_BYFOOD = "findGroupSalesSum";
	
	//集团销售分析按类销售汇总
	public static final String BASICINFO_REPORT_SALES_SUM_BYCATEGORY = "470221,470222,470223,470224,470225,470226";  //默认选择列
	public static final String REPORT_NAME_SALES_SUM_BYCATEGORY = "GroupSalesSumByCate";//选择表名
	public static final String REPORT_NAME_CN_SALES_SUM_BYCATEGORY = "按类销售汇总表";  		//选择表名
	public static final String BASICINFO_REPORT_SALES_SUM_BYCATEGORY_FROZEN = "470221";
	public static final String REPORT_QUERY_METHOD_SALES_SUM_BYCATEGORY = "findGroupSalesSum";
	
	//集团销售分析按时间段销售汇总
	public static final String BASICINFO_REPORT_GROUPSALES_SUM_BYDATE = "470253,470254,470255,470256,470257,470258";  //默认选择列
	public static final String REPORT_NAME_GROUPSALES_SUM_BYDATE = "GroupSalesDetailByDate";  //选择表名
	public static final String REPORT_NAME_CN_GROUPSALES_SUM_BYDATE = "按时间段销售汇总表";  //选择表名
	public static final String BASICINFO_REPORT_GROUPSALES_SUM_BYDATE_FROZEN = "470253";
	public static final String REPORT_QUERY_METHOD_GROUPSALES_SUM_BYDATE = "findGroupSalesSumByDate";
	
	//集团菜品出错率分析
	public static final String BASICINFO_REPORT_JITUANDIANCAICHUCUOLV = "470280,470281,470282,470283,470284,470285,470286";  //默认选择列
	public static final String REPORT_NAME_JITUANDIANCAICHUCUOLV = "CaiPinChuCuoLv";  //选择表名
	public static final String REPORT_NAME_CN_JITUANDIANCAICHUCUOLV = "集团菜品出错率分析";  //选择表名
	public static final String BASICINFO_REPORT_JITUANDIANCAICHUCUOLV_FROZEN = "470280";
	public static final String REPORT_QUERY_METHOD_JITUANDIANCAICHUCUOLV = "findCaiPinChuCuoLv";
	
	//单店账单分析_表格显示
	public static final String REPORT_NAME_CN_DDZDFX_BYTABLE = "单店账单分析_表格显示";  //选择表名
	public static final String BASICINFO_REPORT_DDZDFX_BYTABLE_FROZEN = "";
	public static final String REPORT_NAME_DDZDFX_BYTABLE = "";
	public static final String BASICINFO_REPORT_DDZDFX_BYTABLE = "";
	public static final String REPORT_QUERY_METHOD_DDZDFX_BYTABLE="queryDdzdfx";
	
	//单店账单分析_汇总报表
	public static final String BASICINFO_REPORT_DDZDFX_BYALL = "470026,470027,470028,470029,470030,470031,470032,470033,470034,8470035,470036,470037,470038,470039,470040,470042";  //默认选择列
	public static final String REPORT_NAME_DDZDFX_BYALL = "DdzdfxByHuiZong";  //选择表名
	public static final String REPORT_NAME_CN_DDZDFX_BYALL = "单店账单分析_汇总报表";  //选择表名
	public static final String BASICINFO_REPORT_DDZDFX_BYALL_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_DDZDFX_BYALL = "queryDdzdfxByHuiZong";
			
	//单店账单分析_账单明细
	public static final String BASICINFO_REPORT_DDZDFX_BYINFO = "470044,470045,470046,470047,470048,470049,470050,470051,470052,470053,470054,470055";  //默认选择列
	public static final String REPORT_NAME_DDZDFX_BYINFO = "DdzdfxZdmx";  //选择表名
	public static final String REPORT_NAME_CN_DDZDFX_BYINFO = "单店账单分析_账单明细";  //选择表名
	public static final String BASICINFO_REPORT_DDZDFX_BYINFO_FROZEN = "470044,470045";
	public static final String REPORT_QUERY_METHOD_DDZDFX_BYINFO = "findZhangDanMingXi";
	
	//项目销售统计
	public static final String BASICINFO_REPORT_PROJECTSALESSTATISTICS = "470061,470062,470063,470064,470065,470066,470067,470068";  //默认选择列
	public static final String REPORT_NAME_PROJECTSALESSTATISTICS = "projectSalesStatistics";  //选择表名
	public static final String REPORT_NAME_CN_PROJECTSALESSTATISTICS = "项目销售统计";  //选择表名
	public static final String BASICINFO_REPORT_PROJECTSALESSTATISTICS_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_PROJECTSALESSTATISTICS = "findProjectSalesStatistics";
	
	//项目销售统计 --明细
	public static final String BASICINFO_REPORT_PROJECTSALESSTATISTICSDETAIL = "470069,470070,470071,470072,470073,470074,470075,470076";  //默认选择列
	public static final String REPORT_NAME_PROJECTSALESSTATISTICSDETAIL = "packageSalesStatisticsDetail";  //选择表名
	public static final String REPORT_NAME_CN_PROJECTSALESSTATISTICSDETAIL = "明细统计表";  //选择表名
	public static final String BASICINFO_REPORT_PROJECTSALESSTATISTICSDETAIL_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_PROJECTSALESSTATISTICSDETAIL = "findProjectSalesStatistics";
	
	//客单-时段销售分析报表
	public static final String BASICINFO_REPORT_SALETCBYTIME = "470078,470079,470080,470081,470082,470083,470084,470085,470086,470087";  //默认选择列
	public static final String REPORT_NAME_SALETCBYTIME = "saletcbytime";  //选择表名
	public static final String REPORT_NAME_CN_SALETCBYTIME = "客单-时段销售分析报表";  //选择表名
	public static final String BASICINFO_REPORT_SALETCBYTIME_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_SALETCBYTIME = "querySaleTcByTime";
	
	//类别-时段销售分析报表
	public static final String BASICINFO_REPORT_SALETYPEBYTIME = "470089,470090,470091,470092,470093,470094";  //默认选择列
	public static final String REPORT_NAME_SALETYPEBYTIME = "saletypebytime";  //选择表名
	public static final String REPORT_NAME_CN_SALETYPEBYTIME = "类别-时段销售分析报表";  //选择表名
	public static final String BASICINFO_REPORT_SALETYPEBYTIME_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_SALETYPEBYTIME = "querySaleTypeByTime";
	
	//套餐销售统计--销售统计
	public static final String BASICINFO_REPORT_SALETONGJI = "470330,470331,470332,470333,470334,470335";  //默认选择列
	public static final String REPORT_NAME_SALETONGJI = "saleTongJi";  //选择表名
	public static final String REPORT_NAME_CN_SALETONGJI = "销售统计表";  //选择表名
	public static final String BASICINFO_REPORT_SALETONGJI_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_SALETONGJI = "queryXstjb";

	//套餐销售统计--明细统计
	public static final String BASICINFO_REPORT_DETAILTONGJI = "470336,470337,470338,470339";  //默认选择列
	public static final String REPORT_NAME_DETAILTONGJI = "detailTongJi";  //选择表名
	public static final String REPORT_NAME_CN_DETAILTONGJI = "明细统计表";  //选择表名
	public static final String BASICINFO_REPORT_DETAILTONGJI_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_DETAILTONGJI = "queryMxtjb";
	
	//单店反结算分析
	public static final String BASICINFO_REPORT_DDFJSFX = "470095,470096,470097,470098,470099,470171,470100,470101,470102,470103,470104";  //默认选择列
	public static final String REPORT_NAME_DDFJSFX = "Ddfjsfx";  //选择表名
	public static final String REPORT_NAME_CN_DDFJSFX = "单店反结算分析";  //选择表名
	public static final String BASICINFO_REPORT_DDFJSFX_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_DDFJSFX = "queryDdfjsfx";
			
	//单店反结算分析明细
	public static final String BASICINFO_REPORT_DDFJSFXBYMX = "470105,470106,470107,470108,470109";  //默认选择列
	public static final String REPORT_NAME_DDFJSFXBYMX = "DdfjsfxBymx";  //选择表名
	public static final String REPORT_NAME_CN_DDFJSFXBYMX = "单店反结算分析明细";  //选择表名
	public static final String BASICINFO_REPORT_DDFJSFXBYMX_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_DDFJSFXBYMX = "findDdfjsmx";
			
	//单店收银统计_收银方式统计
	public static final String BASICINFO_REPORT_DDSYFSTJ = "470314,470315,470316";  //默认选择列
	public static final String REPORT_NAME_DDSYFSTJ = "DanDianShouYinTongJi";  //选择表名
	public static final String REPORT_NAME_CN_DDSYFSTJ = "单店收银统计_收银方式统计";  //选择表名
	public static final String BASICINFO_REPORT_DDSYFSTJ_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_DDSYFSTJ = "queryDdsyfstj";
			
	//单店收银统计_结算方式明细
	public static final String BASICINFO_REPORT_DDSYJSINFO = "470317,470318,470319,470320";  //默认选择列
	public static final String REPORT_NAME_DDSYJSINFO = "DanDianShouYinTongJiMx";  //选择表名
	public static final String REPORT_NAME_CN_DDSYJSINFO = "单店收银统计_结算方式明细";  //选择表名
	public static final String BASICINFO_REPORT_DDSYJSINFO_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_DDSYJSINFO = "queryDdsyjsinfo";
			
	//集团活动分析-按日`
	public static final String REPORT_NAME_CN_JITUANHUODONGFENXI = "集团活动分析-按日 ";  //选择表名
	public static final String BASICINFO_REPORT_JITUANHUODONGFENXI_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_JITUANHUODONGFENXI = "queryJiTuanHuoDongFenXi";
			
	//集团活动分析-按月
	public static final String REPORT_NAME_CN_JITUANHUODONGFENXIBYMONTH = "集团活动分析-按月 ";  //选择表名
	public static final String BASICINFO_REPORT_JITUANHUODONGFENXIBYMONTH_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_JITUANHUODONGFENXIBYMONTH = "queryJiTuanHuoDongFenXiByMonth";
			
	//台位活动明细报表
	public static final String REPORT_NAME_CN_ACTMBYTBL = "台位活动明细报表";  //选择表名
	public static final String REPORT_NAME_ACTMBYTBL = "";
	public static final String BASICINFO_REPORT_ACTMBYTBL_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_ACTMBYTBL = "findActmByTbl";
			
	//菜品活动明细报表
	public static final String REPORT_NAME_CN_ACTMBYITEM = "菜品活动明细报表";  //选择表名
	public static final String BASICINFO_REPORT_ACTMBYITEM_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_ACTMBYITEM = "findActmByItem";
	
	//菜品销售排行榜
	public static final String BASICINFO_REPORT_FOODLIST = "470321,470322,470323,470324,470325,470326,470327,470328,470329";  //默认选择列
	public static final String REPORT_NAME_FOODLIST = "foodlist";  //选择表名
	public static final String REPORT_NAME_CN_FOODLIST = "菜品销售排行榜";  //中文名
	public static final String BASICINFO_REPORT_FOODLIST_FROZEN = "470321,470322";
	public static final String REPORT_QUERY_METHOD_FOODLIST = "queryFoodList";
	
	//集团消退菜分析
	public static final String REPORT_NAME_GROUPCRDISHES = "groupcrdishes";  //选择表名
	public static final String REPORT_NAME_CN_GROUPCRDISHES = "集团消退菜分析表";  //选择表名
	public static final String REPORT_QUERY_METHOD_GROUPCRDISHES = "findGroupCRDishes";
	
	//集团消退菜分析_单店退菜明细
	public static final String BASICINFO_REPORT_GROUPCRDISHES_FIRM = "470110,470111,470112,470113,470114,470115,470116";  //默认选择列
	public static final String REPORT_NAME_GROUPCRDISHES_FIRM = "groupcrdishes_firm";  //选择表名
	public static final String REPORT_NAME_CN_GROUPCRDISHES_FIRM = "集团消退菜分析_单店退菜明细";  //选择表名
	public static final String BASICINFO_REPORT_GROUPCRDISHES_FIRM_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_GROUPCRDISHES_FIRM = "findGroupCRDishesByFirm";
	
	public static final String REPORT_NAME_SCHEDULEREPORT = "scheduleReport";  	//选择表名
	public static final String REPORT_NAME_CN_SCHEDULEREPORT = "排班报表";  		//选择表名
	public static final String BASICINFO_REPORT_SCHEDULEREPORT_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_SCHEDULEREPORT = "createScheduleReport";
	/****************************************************人力资源*******************************************************/
	
	//合同管理--登记列表
	public static final String BASICINFO_REPORT_CONTRACTREGISTRATION = "1002,1003,1004,1005,1006";	//默认选择列
	public static final String REPORT_NAME_CONTRACTREGISTRATION = "ContractRegistration";  			//选择表名
	public static final String REPORT_NAME_CN_CONTRACTREGISTRATION = "合同登记";  						//文件名
	public static final String BASICINFO_REPORT_CONTRACTREGISTRATION_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_CONTRACTREGISTRATION = "listContractRegistration";
	
	//合同管理--变更记录
	public static final String BASICINFO_REPORT_CONTRACTCHANGE = "1008,1009,1010,1011,1012,1013,1014,1015,1016";//默认选择列
	public static final String REPORT_NAME_CONTRACTCHANGE = "ContractChange";  					//选择表名
	public static final String REPORT_NAME_CN_CONTRACTCHANGE = "变更记录";  							//文件名
	public static final String BASICINFO_REPORT_CONTRACTCHANGE_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_CONTRACTCHANGE = "listContractChange";
	
	//合同管理--工龄计算
	public static final String BASICINFO_REPORT_SERVICECALCULATION = "1018,1019,1020,1021,1022,1023,1024,1025,1026,1027";//默认选择列
	public static final String REPORT_NAME_SERVICECALCULATION = "ServiceCalculation";  					//选择表名
	public static final String REPORT_NAME_CN_SERVICECALCULATION = "变更记录";  							//文件名
	public static final String BASICINFO_REPORT_SERVICECALCULATION_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_SERVICECALCULATION = "listServiceCalculation";
	
	//排班考勤——每日排班
	public static final String REPORT_NAME_DAILYSCHEDULE = "Shift"; 								//选择表名
	public static final String REPORT_NAME_CN_DAILYSCHEDULE = "每日排班";  	
	public static final String REPORT_QUERY_METHOD_DAILYSCHEDULE = "getEmpMap";
	public static final String BASICINFO_REPORT_DAILYSCHEDULE = ""; 									//文件名
	public static final String BASICINFO_REPORT_DAILYSCHEDULE_FROZEN = "";
	
	//排班考勤——班表设定
	public static final String REPORT_NAME_CLASSTABLESETTING = "ClassTableSetting"; 							//选择表名
	public static final String REPORT_NAME_CN_CLASSTABLESETTING = "班表设定";  	
	public static final String REPORT_QUERY_METHOD_CLASSTABLESETTING = "getListData";
	public static final String BASICINFO_REPORT_CLASSTABLESETTING = ""; 										//文件名
	public static final String BASICINFO_REPORT_CLASSTABLESETTING_FROZEN = "";
	
	/****************************************************西贝项目报表开始*******************************************************/
	
	/**
	 * 营收客流分段统计明细表
	 */
	public static final String REPORT_NAME_BUSINESSBURST = "businessburst";  	//选择表名
	public static final String REPORT_NAME_CN_BUSINESSBURST = "营收客流分段统计明细表";  //选择表名
	public static final String REPORT_QUERY_METHOD_BUSINESSBURST = "findBusinessBurst";
	
	/**
	 * 营业时段解析表
	 */
	public static final String REPORT_NAME_SALEDATABYTIME = "saledatabytime";  	//选择表名
	public static final String REPORT_NAME_CN_SALEDATABYTIME = "营业时段解析表";  	//选择表名
	public static final String REPORT_QUERY_METHOD_SALEDATABYTIME = "findSaleDataByTime";
	
	/**
	 * 菜品见台率统计分析
	 */
	public static final String BASICINFO_REPORT_CAIPINJIANTAILV = "470287,470288,470289,470290,470291,470292,470293,470295,470296,470297";  //默认选择列
	public static final String REPORT_NAME_CAIPINJIANTAILV = "CaiPinShangTaiLv";//选择表名
	public static final String REPORT_NAME_CN_CAIPINJIANTAILV = "集团菜品上台率分析";  	//选择表名
	public static final String BASICINFO_REPORT_CAIPINJIANTAILV_FROZEN = "470287,470288,470289,470290";
	public static final String REPORT_QUERY_METHOD_CAIPINJIANTAILV = "findCaiPinJianTaiLv";
	
	/****************************************************西贝项目报表结束*******************************************************/
	
	/**
	 * 菜品销售沽清表
	 * add by wangkai 2016-2-19
	 */
	public static final String BASICINFO_REPORT_ITEMSALEGQ = "90001001,90001002,90001003,90001004,90001005,90001006,90001007,90001008,90001009,90001010";//默认选择列
	public static final String REPORT_NAME_ITEMSALEGQ = "itemsalegq";  					//选择表名
	public static final String REPORT_NAME_CN_ITEMSALEGQ = "菜品销售沽清表";  				//文件名
	public static final String BASICINFO_REPORT_ITEMSALEGQ_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_ITEMSALEGQ = "listItemSaleGQ";
	
	/**
	 * 菜品销售明细报表——江边城外
	 */
	public static final String REPORT_NAME_JBCWPOSCYBB = "";  							//选择表名
	public static final String REPORT_NAME_CN_JBCWPOSCYBB = "菜品销售明细查询报表"; 	//选择表名
	public static final String BASICINFO_REPORT_JBCWPOSCYBB = "";
	public static final String BASICINFO_REPORT_JBCWPOSCYBB_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_JBCWPOSCYBB = "queryJbcwPoscybb";
	
	/**
	 * 鱼口味统计报表——江边城外
	 */
	public static final String REPORT_NAME_YKWTJBB = "";  					//选择表名
	public static final String REPORT_NAME_CN_YKWTJBB = "鱼口味统计报表"; 	//选择表名
	public static final String BASICINFO_REPORT_YKWTJBB = "";
	public static final String BASICINFO_REPORT_YKWTJBB_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_YKWTJBB = "queryFishFlavorStatistical";
	
	/**
	 * 翻台统计表——江边城外
	 */
	public static final String REPORT_NAME_FTTJB = ""; 							//选择表名
	public static final String REPORT_NAME_CN_FTTJB = "翻台统计表"; 			//选择表名
	public static final String BASICINFO_REPORT_FTTJB = "";
	public static final String BASICINFO_REPORT_FTTJB_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_FTTJB = "queryTableStatistics";
	
	/**
	 * 结算方式统计表——江边城外
	 */
	public static final String BASICINFO_REPORT_JSFSTJBB = "403240,403348,403349,403350,403351";	//默认选择列
	public static final String REPORT_NAME_JSFSTJBB = "BohJsfs";  				//选择表名
	public static final String REPORT_NAME_CN_JSFSTJBB = "结算方式统计表"; 	//选择表名
	public static final String BASICINFO_REPORT_JSFSTJBB_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_JSFSTJBB = "queryStatementAccounts";
	
	//结算方式统计明细——江边城外
	public static final String BASICINFO_REPORT_JSFSMX = "403353,403354,403355,403356,403357,403358"; 	//默认选择列
	public static final String REPORT_NAME_JSFSMX = "BohJsfsmx";  			//选择表名
	public static final String REPORT_NAME_CN_JSFSMX = "结算方式明细";  	//选择表名
	public static final String BASICINFO_REPORT_JSFSMX_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_JSFSMX = "queryStatementAccountsDetail";

	//折扣分析报表——江边城外
	public static final String BASICINFO_REPORT_ZKFXB = ""; 			//默认选择列
	public static final String REPORT_NAME_ZKFXB = "";  				//选择表名
	public static final String REPORT_NAME_CN_ZKFXB = "折扣分析表";  	//选择表名
	public static final String BASICINFO_REPORT_ZKFXB_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_ZKFXB = "queryDiscountAnalysis";

	//排班报表    权金城
	public static final String BASICINFO_REPORT_SCHEDULEREPORT7 = ""; 			//默认选择列
	public static final String REPORT_NAME_SCHEDULEREPORT7 = "scheduleReport7"; //选择表名
	public static final String REPORT_NAME_CN_SCHEDULEREPORT7 = "排班报表";  		//选择表名
	public static final String BASICINFO_REPORT_SCHEDULEREPORT7_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_SCHEDULEREPORT7 = "queryScheduleReport7";
}