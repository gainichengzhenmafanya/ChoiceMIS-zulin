package com.choice.misboh.constants.reportMis;

/**
 * 描述：营业分析下的各报表路径
 * @author 马振
 * 创建时间：2015-4-16 下午1:29:20
 */
public class BusinessAnalysisMisBohConstant {
	
	//现金日报表
	public static final String REPORT_SHOW_XJRREPORT = "misboh/reportMis/businessAnalysis/MisBohDailyCashreport";
	
	//账单查询
	public static final String REPORT_SHOW_ZDMXDET = "misboh/reportMis/businessAnalysis/MisBohFolioDetailInfo";
	public static final String REPORT_URL_ZDMXDET = "report/misboh/businessanalysismis/MisBohFolioDetailInfo.jasper";
	
	//賬單明細分析
//	public static final String REPORT_SHOW_ZDMXFX = "misboh/reportMis/businessAnalysis/MisBohZhangDanMxFenXi";
	public static final String REPORT_SHOW_ZDMXFX = "misboh/reportMis/businessAnalysis/ZhangDanMxFenXi";
	public static final String REPORT_URL_ZDMXFX = "report/misboh/businessanalysismis/MisBohZhangDanMxFenXi.jasper";
	
	//营运报告
	public static final String REPORT_SHOW_YYBG = "misboh/reportMis/businessAnalysis/MisBohYingYunBaoGao";
	public static final String REPORT_URL_YYBG_YK = "report/misboh/businessanalysismis/MisBohYingYunBaoGao.jasper";

	//营业日明细
	public static final String REPORT_SHOW_YINGYERIBAO = "misboh/reportMis/businessAnalysis/MisBohYingYeRibao";
	
	//收銀員統計
	public static final String REPORT_SHOW_SYYTJ = "misboh/reportMis/businessAnalysis/MisBohCashierStatistics";
	public static final String REPORT_URL_SYYTJ = "report/misboh/businessanalysismis/MisBohCashierStatistics.jasper";
	
	//台位活动明细报表
	public static final String REPORT_SHOW_ACTMBYTBL = "misboh/reportMis/businessAnalysis/actmbytbl";
	
	/**
	 * 账单查询(当日查询)
	 */
	public static final String REPORT_SHOW_ZDMXDETCUR = "misboh/reportMis/businessAnalysis/MisBohFolioDetailInfoCur";
	
	/**
	 * 当日查询——营运报告
	 */
	public static final String REPORT_SHOW_YYBGCUR = "misboh/reportMis/businessAnalysis/MisBohYingYunBaoGaoCur";
	
	/**
	 * 当日查询——营业日明细
	 */
	public static final String REPORT_SHOW_YINGYERIBAOCUR = "misboh/reportMis/businessAnalysis/MisBohYingYeRibaoCur";
	
	/**
	 * 支付活动明细表
	 */
	public static final String REPORT_SHOW_PAYDETAIL = "misboh/reportMis/businessAnalysis/paydetail";
}