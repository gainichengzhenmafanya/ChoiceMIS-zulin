package com.choice.misboh.constants.reportMis;

/**
 * 描述：销售分析下的各报表路径
 * @author 马振
 * 创建时间：2015-4-23 下午1:45:55
 */
public class SaleAnalysisMisBohConstant {
	
	//菜品类别销售报告
	public static final String REPORT_SHOW_INVOICETYPE = "misboh/reportMis/saleAnalysis/MisBohInvoiceType";
	public static final String REPORT_URL_INVOICETYPE = "report/misboh/saleanalysismis/MisBohInvoiceType.jasper";

	//菜品销售
	public static final String REPORT_SHOW_CPXSHZ = "misboh/reportMis/saleAnalysis/MisBohCaiPinXiaoShouHuiZong";
	public static final String REPORT_URL_CPXSHZ = "report/misboh/saleanalysismis/MisBohCaiPinXiaoShouHuiZong.jasper";

	//退菜明細报表
	public static final String REPORT_SHOW_TCMXBB = "misboh/reportMis/saleAnalysis/MisBohFoodDetailedReports";
	public static final String REPORT_URL_TCMXBB = "report/misboh/saleanalysismis/MisBohFoodDetailedReports.jasper";
	
	//套餐销售报表
	public static final String REPORT_SHOW_BOHSALES = "misboh/reportMis/saleAnalysis/MisBohPackageSales";
	public static final String REPORT_URL_BOHSALES = "report/misboh/saleanalysismis/listBohSales.jasper";
	
	/**
	 * 时段营业额报告
	 */
	public static final String REPORT_SHOW_PERIODTURNOVERREPORT = "misboh/reportMis/saleAnalysis/periodTurnoverReport";

	/**
	 * 附加项销售统计
	 */
	public static final String REPORT_SHOW_ADDITIONALTERM = "misboh/reportMis/saleAnalysis/additionalTerm";
	
	/**
	 * 点菜员提成报表
	 */
	public static final String REPORT_SHOW_ORDERINGMEMBERCOMMISSION = "misboh/reportMis/saleAnalysis/orderingMemberCommission";

	/**
	 * 点菜员提成明细
	 */
	public static final String REPORT_SHOW_DCYTCMX = "misboh/reportMis/saleAnalysis/orderingMemberCommissionDetail";

	/**
	 * 当日查询——点菜员提成报表
	 */
	public static final String REPORT_SHOW_ORDERINGMEMBERCOMMISSIONCUR = "misboh/reportMis/saleAnalysis/orderingMemberCommissionCur";
	
	/**
	 * 菜品销售明细表
	 */
	public static final String REPORT_SHOW_POSCYBB = "misboh/reportMis/saleAnalysis/poscybb";
	
	/**
	 * 菜品销售明细查询表
	 */
	public static final String REPORT_SHOW_ITEMDETAIL = "misboh/reportMis/saleAnalysis/itemdetail";
}