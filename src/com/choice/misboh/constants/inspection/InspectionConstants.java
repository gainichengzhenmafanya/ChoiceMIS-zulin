package com.choice.misboh.constants.inspection;

public class InspectionConstants {
	
	//直配验货
	public static final String MIS_TABLE_CHECK="misboh/inspection/dire/tableCheck";//供应商到货验收
	public static final String REPORT_RECEIPT_URL="report/misboh/inspection/Receipt.jasper";//验收单
	public static final String MIS_TABLE_CHECKIN_NEW = "misboh/inspection/dire/tableCheckin_new";//新版mis验收入库 wjf
	public static final String REPORT_YSRK_URL="/report/misboh/inspection/ysrkChkstomReport.jasper";//验收入库打印
	public static final String SAVE_CHKINM_DEPT_ZP="misboh/inspection/dire/saveChkinmDept_zp";//供应商到货验收多档口
	public static final String SAVE_CHKINM_GROUP_ZP="misboh/inspection/dire/saveChkinmGroup_zp";//供应商到货验收多档口
	
	//统配验货
	public static final String LIST_DISPATCH="misboh/inspection/out/listDispatch";//配送中心到货验收
	public static final String LIST_DISPATCH_CS="misboh/inspection/out/listDispatch_cs";//配送中心到货验收聪少
	public static final String REPORT_DISPATCH_URL="report/misboh/inspection/disReport.jasper";//配送中心到货验收表
	public static final String SAVE_CHKINM_DEPT="misboh/inspection/out/saveChkinmDept";//配送中心到货验收 多档口
	public static final String SAVE_CHKINM_GROUP="misboh/inspection/out/saveChkinmGroup";//配送中心到货验收 分 多档口
	
	//调拨验货
	public static final String LIST_DISPATCH_DB="misboh/inspection/out/listDispatchDb";//调拨验收
	public static final String SAVE_CHKINM_GROUP_DB="misboh/inspection/out/saveChkinmGroupDb";//调拨验收 多档口
	
	
	//九毛九单据验货
	public static final String LIST_ARRIVALM = "misboh/inspection/jmj/listArrivalm";//直配
	public static final String LIST_ARRIVALD = "misboh/inspection/jmj/listArrivald";
	public static final String SEND_EMAIL = "misboh/inspection/jmj/sendEmail";
	
	public static final String LIST_SUPPLYACCT_TP = "misboh/inspection/jmj/listSupplyAcctTP";//统配
	public static final String TABLE_SUPPLYACCT_TP = "misboh/inspection/jmj/tableSupplyAcctTP";
	
}
