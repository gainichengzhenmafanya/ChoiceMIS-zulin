package com.choice.misboh.constants.costReduction;

/**
 * 描述：成本核减跳转页面
 * @author 马振
 * 创建时间：2015-4-14 上午10:00:11
 */
public class CostReductionConstants {

	//成本核减显示
	public static final String LIST_COSTREDUCTION = "/misboh/costReduction/listCostReduction";
}
