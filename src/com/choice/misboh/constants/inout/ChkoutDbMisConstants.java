package com.choice.misboh.constants.inout;
/**
 * @author yp
 */
public class ChkoutDbMisConstants {
	
	public static final String UPDATE_CHKOUT = "misboh/inout/out/saveChkoutDb";
	public static final String MANAGE_CHKOUT = "misboh/inout/out/listChkoutDb";
	public static final String CHECKED_CHKOUT = "misboh/inout/out/listCheckedChkoutDb";
	public static final String SEARCH_CHECKED_CHKOUT = "misboh/inout/out/searchCheckedChkoutDb";
	
	
	public static final String REPORT_PRINT_URL="/report/misboh/inout/out/ChkoutDb_print.jasper";//报表地址

}
