package com.choice.misboh.constants.inout;

public class ChkinmZbMisConstants {

	//页面跳转
	public static final String LIST_CHKINM="misboh/inout/in/listChkinmZb";//查询所有未审核页面
	public static final String LIST_CHECKED_CHKINM="misboh/inout/in/listCheckedChkinmZb";//查询所有已审核页面
	public static final String SEARCH_CHECKED_CHKINMZB="misboh/inout/in/updateCheckedChkinmZb";
	public static final String UPDATE_CHKINMZB="misboh/inout/in/updateChkinmzb";//直拨单添加页面
	public static final String REPORT_URL_ZB="/report/misboh/inout/in/chkinmzbReport.jasper";//报表地址
	public static final String TABLE_CHKINMZF_CX = "misboh/inout/in/tableChkinmzfCx";
	public static final String ADD_CHKINZBDEMO = "misboh/inout/in/addChkinzbDemo";//直发单模板
	
}
