package com.choice.misboh.constants.inout;

public class ChkinmMisConstants {
	
	public static final String MIS_LIST_CHECKED_CHKINM="misboh/inout/in/listCheckedChkinm";//查询所有已审核页面MIS
	public static final String MIS_LIST_CHKINM="misboh/inout/in/listChkinm";//查询所有未审核页面 MIS
	public static final String MIS_UPDATE_CHKINM="misboh/inout/in/updateChkinm";//添加，修改  入库单页面  主要的MIS
	public static final String MIS_SAVE_CHKINM="misboh/inout/in/saveChkinm";      //mis
	public static final String MIS_SEARCH_CHECKED_CHKINMZB="misboh/inout/in/updateCheckedChkinmzb";//已审核直拨单
	public static final String MIS_SEARCH_CHECKED_CHKINM="misboh/inout/in/updateCheckedChkinm";//已审核入库单
	public static final String MIS_UPDATE_CHKINMZB="misboh/inout/in/updateChkinmzb";//直拨单添加页面
	public static final String MIS_TABLE_CHKINM_CX="misboh/inout/in/tableChkinmCx";//入库冲消
	public static final String TABLE_CHKINMZF_CX = "scm/chkinm/tableChkinmzfCx";//直发冲消
	public static final String ADD_CHKINDEMO = "misboh/inout/in/addChkinDemo";//入库单模板
	
	public static final String REPORT_URL="/report/misboh/inout/in/chkinmReport.jasper";//报表地址
	public static final String REPORT_URL_ZB="/report/misboh/inout/in/chkinmzbReport.jasper";//报表地址
	
	public static final String IMPORT_CHKINM = "scm/chkinm/importChkinm"; //导入入库单 wjf
	public static final String IMPORT_RESULT = "scm/chkinm/importResult";
	
}
