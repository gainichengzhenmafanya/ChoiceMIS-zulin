package com.choice.misboh.constants.inout;
/**
 * @author yp
 */
public class ChkoutMisConstants {
	
	public static final String SEARCH_CHECKED_CHKOUT = "misboh/inout/out/searchCheckedChkout";
	public static final String UPDATE_CHKOUT = "misboh/inout/out/saveChkout";
	public static final String CHECKED_CHKOUT = "misboh/inout/out/listCheckedChkout";
	public static final String MANAGE_CHKOUT = "misboh/inout/out/listChkout";
	public static final String TABLE_CHKOUT_CX="misboh/inout/out/tableChkoutCx";//添加冲消物资界面
	
	
	public static final String REPORT_PRINT_URL="/report/misboh/inout/out/Chkout_print.jasper";//报表地址
	
	public static final String IMPORT_CHKOUT = "mis/chkout/importChkout"; //导入出库单 wjf
	public static final String IMPORT_RESULT = "mis/chkout/importResult";
}
