package com.choice.misboh.constants.proReport;

/**
 * 描述：项目报表
 * @author wangkai
 * 创建时间：2016-2-19
 */
public class ProReportConstant {

	/**
	 * 菜品销售沽清表
	 * 西贝(中餐)
	 */
	public static final String REPORT_SHOW_ITEMSALEGQ = "misboh/proReport/itemSaleGQ";

	/**
	 * 菜品销售明细报表   江边城外 
	 */
	public static final String REPORT_SHOW_JBCWPOSCYBB = "misboh/newReport/jbcw/CanYinBaoBiao_new";
	
	/**
	 * 鱼口味统计报表——江边城外
	 */
	public static final String REPORT_SHOW_YKWTJBB = "misboh/newReport/jbcw/fishFlavorStatistical";
	
	/**
	 * 翻台统计表——江边城外
	 */
	public static final String REPORT_SHOW_FTTJB = "misboh/newReport/jbcw/tableStatistics";
	
	/**
	 * 结算方式统计表——江边城外
	 */
	public static final String REPORT_SHOW_JSFSTJBB = "misboh/newReport/jbcw/statementAccounts";
	
	/**
	 * 折扣分析表——江边城外
	 */
	public static final String REPORT_SHOW_ZKFXB = "misboh/newReport/jbcw/discountAnalysis";
}