package com.choice.misboh.constants.operationManagement;

/**
 * 描述：运营管理：营业外收入、坐支明细、运营情况登记、存款管理
 * @author 马振
 * 创建时间：2015-5-20 上午10:57:14
 */
public class OperationManagementConstants {

	/**
	 * 坐支明细页面
	 */
	public static final String LISTOUTLAY = "/misboh/operationManagement/listOutLay";
	
	/**
	 * 营业外收入页面
	 */
	public static final String LISTINCOME = "/misboh/operationManagement/listIncome";
	
	/**
	 * 运营情况登记——特殊事件
	 */
	public static final String LISTOPERATION = "/misboh/operationManagement/listOperation";
	
	/**
	 * 存款管理
	 */
	public static final String LISTCASHSAVING = "/misboh/operationManagement/listCashSaving";
	
	/**
	 * 新增、修改页面
	 */
	public static final String ADDCASHSAVING = "/misboh/operationManagement/addCashSaving";
}