package com.choice.misboh.constants.BaseRecord;

/**
 * 描述：外送商圈设定
 * @author 马振
 * 创建时间：2016-3-8 下午1:49:30
 */
public class ExternalBusinessSetConstant {

	/**
	 * 地图显示
	 */
	public static final String MAPDISPLAY = "/misboh/baseRecord/externalBusinessSet/mapDisplay";
}