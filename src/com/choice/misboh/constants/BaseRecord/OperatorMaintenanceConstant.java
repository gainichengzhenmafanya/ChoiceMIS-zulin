package com.choice.misboh.constants.BaseRecord;

/**
 * 描述：门店操作员维护页面跳转
 * @author 马振
 * 创建时间：2015-8-24 下午1:42:33
 */
public class OperatorMaintenanceConstant {

	/**
	 * 门店操作员全部显示页面
	 */
	public static final String LIST_OPERATORMAINTENANCE = "/misboh/baseRecord/operatorMaintenance/listOperatorMaintenance";
	
	/**
	 * 门店操作员新增修改页面
	 */
	public static final String EDIT_OPERATORMAINTENANCE = "/misboh/baseRecord/operatorMaintenance/editOperatorMaintenance";
}
