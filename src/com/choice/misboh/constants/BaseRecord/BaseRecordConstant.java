package com.choice.misboh.constants.BaseRecord;

/**
 * 描述：基础档案：菜品设置、套餐设置、门店POS、门店活动、水电气跳转页面
 * @author 马振
 * 创建时间：2015-4-14 上午9:40:03
 */
public class BaseRecordConstant {
	
	//菜品左侧类别树
	public static final String TREE_MARSALECLASS = "/misboh/baseRecord/treeMarsaleclass";
	
	//菜品显示
	public static final String LIST_PUBITEM = "/misboh/baseRecord/listPubItem";
	
	//菜品详细显示页面
	public static final String UPDATE_PUBITEM = "/misboh/baseRecord/updatePubitem";
	
	//弹出菜品列表界面，根据类别进行分组显示
	public static final String CHOOSEPUBITEM = "/misboh/baseRecord/choosePubItem";
	
	//套餐显示
	public static final String LIST_PUBPACKAGE = "/misboh/baseRecord/listPubPackage";
	
	//套餐显示明细
	public static final String UPDATE_PUBPACKAGE = "/misboh/baseRecord/updatePubPackage";
	
	//可换菜明细页面
	public static final String LIST_ITEMPKG = "/misboh/baseRecord/listItemPkg";
	
	//门店POS显示
	public static final String LIST_STOREPOS = "/misboh/baseRecord/listStorePos";
	
	//新增门店POS页面
	public static final String ADD_STOREPOS = "/misboh/baseRecord/addStorePos";
	
	//修改门店POS页面
	public static final String UPDATE_STOREPOS = "/misboh/baseRecord/updateStorePos";
	
	//门店POS弹出框
	public static final String CHOOSESTOREPOS = "/misboh/baseRecord/chooseStorePos";
	
	//活动左侧类别树
	public static final String TREE_ACTM = "/misboh/baseRecord/treeActm";
	
	//门店活动
	public static final String LIST_ACTM = "/misboh/baseRecord/listActm";
	
	//门店活动详细显示页面
	public static final String ACTM_DETAIL = "/misboh/baseRecord/actmDetail";
	
	//水电气显示
	public static final String LIST_OTHERSTOCKDATA = "/misboh/baseRecord/listOtherStockData";

	//新增水表
	public static final String ADD_WATER = "/misboh/baseRecord/addWater";
	
	//修改水电气主表
	public static final String UPDATEZHUBIAO = "/misboh/baseRecord/updateOtherstockDataF";
	
	//修改水电气
	public static final String UPDATEWATERGAS = "/misboh/baseRecord/updateOtherStockData";
	
	/**
	 * 菜品打印信息
	 */
	public static final String LISTPUBITEMPRINT = "/misboh/baseRecord/listPubitemPrint";
}