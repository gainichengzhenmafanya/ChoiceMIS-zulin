package com.choice.misboh.constants.chineseReport;

/**
 * 描述：中餐报表—活动分析路径
 * @author 马振
 * 创建时间：2015-6-25 上午11:27:10
 */
public class ChineseActivityReportConstant {
	
	/**
	 * 门店活动分析
	 */
	public static final String REPORT_SHOW_JITUANHUODONGFENXI = "misboh/chineseReport/activity/JiTuanHuoDongFenXi";
	
	/**
	 * 台位活动明细报表
	 */
	public static final String REPORT_SHOW_ACTMBYTBL = "misboh/chineseReport/activity/actmbytbl";
		
	/**
	 * 菜品活动明细报表
	 */
	public static final String REPORT_SHOW_ACTMBYITEM = "misboh/chineseReport/activity/actmbyitem";
}