package com.choice.misboh.constants.chineseReport;

/**
 * 描述：中餐报表—营业分析路径
 * @author 马振
 * 创建时间：2015-6-25 上午11:27:10
 */
public class ChineseSalesReportConstant {

	/**
	 * 项目销售统计
	 */
	public static final String REPORT_SHOW_PROJECTSALESSTATISTICS = "misboh/chineseReport/sales/projectSalesStatistics";

	/**
	 * 菜品销售排行榜
	 */
	public static final String REPORT_SHOW_FOODLIST = "misboh/chineseReport/sales/foodList";
	
	/**
	 * 时段销售分析报表
	 */
	public static final String REPORT_SHOW_SALEBYTIME = "misboh/chineseReport/sales/salebytime";
	
	/**
	 * 套餐销售统计
	 */
	public static final String REPORT_SHOW_TAOCANXIAOSHOUTONGJI = "misboh/chineseReport/sales/taoCanXiaoShouTongJi";
	
	/**
	 * 门店退菜分析
	 */
	public static final String REPORT_SHOW_GROUPCRDISHES = "misboh/chineseReport/sales/groupCRDishes";
	
	/**
	 * 排班报表
	 */
	public static final String REPORT_SHOW_SCHEDULEREPORT = "misboh/chineseReport/sales/scheduleReport";
}