package com.choice.misboh.constants.chineseReport;

/**
 * 描述：中餐报表—营业分析路径
 * @author 马振
 * 创建时间：2015-6-25 上午11:27:10
 */
public class ChineseBusinessReportConstant {
	
	/**
	 * 门店营业显示分析
	 */
	public static final String REPORT_SHOW_GROUPBUSINESSSHOW = "misboh/chineseReport/business/groupBusinessShow";
	
	/**
	 * 门店收入分析
	 */
	public static final String REPORT_SHOW_GROUPINCOME = "misboh/chineseReport/business/groupIncome";
	
	/**
	 * 门店销售分析汇总
	 */
	public static final String REPORT_SHOW_GROUPSALES_SUM = "misboh/chineseReport/business/groupSalesSum";

	/**
	 * 门店区域营业分析
	 */
	public static final String BUSSINESS_JTRFX = "misboh/chineseReport/business/JiTuanBaobiaoFenXi"; 
	
	/**
	 * 门店点菜出错率分析
	 */
	public static final String REPORT_SHOW_JITUANDIANCAICHUCUOLV = "misboh/chineseReport/business/JiTuanDianCaiChuCuoLv";	
}