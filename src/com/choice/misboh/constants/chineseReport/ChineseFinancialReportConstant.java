package com.choice.misboh.constants.chineseReport;

/**
 * 描述：中餐报表—财务分析路径
 * @author 马振
 * 创建时间：2015-6-25 上午11:27:10
 */
public class ChineseFinancialReportConstant {
	
	/**
	 * 收入日结算报表
	 */
	public static final String BUSSINESS_RJS = "misboh/chineseReport/financial/bussinessrjs"; 
	
	/**
	 * 营业日报表
	 */
	public static final String BUSSINESS_YYBB = "misboh/chineseReport/financial/bussinessyybb"; 
	
	/**
	 * 账单分析
	 */
	public static final String REPORT_SHOW_DDZDFX = "misboh/chineseReport/financial/DanDianZhangDanFenXi";

	/**
	 * 单店反结算分析
	 */
	public static final String REPORT_SHOW_DDFJSFX = "misboh/chineseReport/financial/DanDianFanJieSuanFenXi";
	
	/**
	 * 收银统计
	 */
	public static final String REPORT_SHOW_DDSYTJ = "misboh/chineseReport/financial/DanDianShouYinTongJi";
}