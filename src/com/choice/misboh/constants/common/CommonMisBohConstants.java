package com.choice.misboh.constants.common;

public class CommonMisBohConstants {
	
	//选择物资类别界面
	public static final String SELECT_TYP_MORE = "misboh/common/grptyp/selectTypMore";

	//物资选择界面
	public static final String SELECT_SUPPLY="misboh/common/supply/selectSupply";//查询物资页面
	public static final String SELECT_TABLE_SUPPLY="misboh/common/supply/selectTableSupply";
	
	//仓位选择界面
	public static final String SELECT_POSITN_SUPER = "misboh/common/positn/positnsuper/selectPositnSuper";
	public static final String SELECT_ONEPOSITN_SUPER="misboh/common/positn/positnsuper/selectOnePositnSuper";//选择单条仓位
	public static final String SELECT_ONE_TO_ONEPOSITN_SUPER="misboh/common/positn/positnsuper/selectOneTOnePositnSuper";//同一类别仓位单选
	public static final String SELECT_ONE_TO_MANYPOSITN_SUPER="misboh/common/positn/positnsuper/selectOneTManyPositnSuper";//同一类别仓位多选
	
	//供应商选择界面
	public static final String SELECT_ONEDELIVERS2 = "misboh/common/deliver/selectOneDeliver2";//选择供应商新页面，左右分栏
	public static final String TABLE_DELIVERS2="misboh/common/deliver/tableDeliver2";//选择供应商右侧栏
	
	/**
	 * 门店部门公共选择框
	 */
	public static final String CHOOSELIST_STOREDEPT="misboh/common/choose/chooseStoreDept";
	
	/**
	 * 菜品公共选择框
	 */
	public static final String SELECTPUBITEMS = "misboh/common/choose/selectPubitemsForComp";

	/**
	 * 门店操作员公共选择框
	 */
	public static final String CHOOSESTOREOPERATOR = "misboh/common/choose/selectStoreOperator";
}
