package com.choice.misboh.constants.declareGoodsGuide;

/**
 * 报货向导 --- 界面跳转路径
 * @author 文清泉
 * @param 2015-4-2 上午9:36:38
 */
public class DeclareGoodsGuideConstants {

	/**
	 *  报货向导左侧列表
	 */
    public static final String LEFT_DECLAREGOODSGUIDE ="misboh/declareGoodsGuide/DeclareGoodsGuideLeft";
    /**
     *  报货向导主题
     */
    public static final String MAIN_DECLAREGOODSGUIDE ="misboh/declareGoodsGuide/DeclareGoodsGuideMain";
    /**
     * 报货向导 右侧主界面
     */
    public static final String REGITH_DECLAREGOODSGUIDE ="misboh/declareGoodsGuide/DeclareGoodsGuideRegith";
    /**
     * 报货向导 调整预估值
     */
    public static final String LISTADJUSTTHEESTIMATED ="misboh/declareGoodsGuide/listAdjustTheEstimated";
    /**
     * 报货向导 千用量
     */
    public static final String LISTTHOUSANDSOFDOSAGE ="misboh/declareGoodsGuide/listThousandsOfDosage";
    /**
     * 报货向导 菜品点击率
     */
    public static final String LISTDISHESCLICKATE ="misboh/declareGoodsGuide/listDishesClickate";
    /**
     * 报货向导 菜品销售计划
     */
    public static final String LISTFOODSALESPLAN ="misboh/declareGoodsGuide/listFoodSalesPlan";
    /**
     * 报货向导 生成报货单
     */
    public static final String GENERATEREPORTLIST ="misboh/declareGoodsGuide/generateReportList";
    
    /**
     * 用量报货   用量显示界面
     */
    public static final String LISTDOSAGEDAILYGOODS = "misboh/dosageDailygoods/listDosageDailygoods";
}
