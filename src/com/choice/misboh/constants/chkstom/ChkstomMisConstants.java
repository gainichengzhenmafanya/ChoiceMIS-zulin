package com.choice.misboh.constants.chkstom;

/***
 * 门店报货相关页面
 * @author wjf
 *
 */
public class ChkstomMisConstants {

	public static final String TABLE_CHKSTOMMIS = "misboh/chkstom/store/tableChkstomMis";//报货页面
	public static final String ADD_CHKSTODEMOMIS = "misboh/chkstom/store/addChkstodemoMis";//报货模板
	public static final String SEARCH_CHKSTOM = "misboh/chkstom/store/searchChkstom";//查找报货单未审核
	public static final String LIST_CHECKEDCHKSTOM = "misboh/chkstom/store/listCheckedChkstom";//查询已审核报货单
	public static final String LIST_CHECKEDCHKSTOD = "misboh/chkstom/store/listCheckedChkstod";//查询报货详情页面
	
	public static final String IMPORT_CHKSTOM = "misboh/chkstom/store/importChkstom";//报货单导入wjf
	public static final String IMPORT_RESULT = "misboh/chkstom/store/importResult";//报货单导入wjf
	
	public static final String REPORT_PRINT_URL="report/misboh/chkstom/chkstomReport.jasper";
	
	
	public static final String TABLE_CHKSTOMMIS_JMJ = "misboh/chkstom/store/tableChkstomMis_jmj";//九毛九报货页面
	public static final String ADD_CHKSTODEMOMIS_JMJ = "misboh/chkstom/store/addChkstodemoMis_jmj";//九毛九报货模板
	public static final String SEARCH_CHKSTOM_JMJ = "misboh/chkstom/store/searchChkstom_jmj";//九毛九查找报货单未审核
	public static final String LIST_CHECKEDCHKSTOM_JMJ = "misboh/chkstom/store/listCheckedChkstom_jmj";//九毛九查询已审核报货单
	
	public static final String TABLE_CHKSTOMBACKMIS = "misboh/chkstom/back/tableChkstomBackMis";//退货页面
	public static final String TABLE_CHKSTOMBACKMIS_JMJ = "misboh/chkstom/back/tableChkstomBackMis_jmj";//九毛九退货页面
	
}
