package com.choice.misboh.constants.chkstom;

/***
 * 九毛九报货相关页面
 * @author wjf
 *
 */
public class ChkstomMisJMJConstants {

	public static final String TABLE_CHKSTOMDEPTMIS = "misboh/chkstom/jmj/tableChkstomMis_jmj";//报货页面
	public static final String SEARCH_CHKSTOM = "misboh/chkstom/jmj/searchChkstom_jmj";//查找报货单
	public static final String LIST_CHKSTOM = "misboh/chkstom/jmj/listChkstom_jmj";//报货单上传页面
	public static final String LIST_CHKSTOD = "misboh/chkstom/jmj/listChkstod_jmj";//报货详情页面
	public static final String TABLECHKSTOMFIRM = "misboh/chkstom/jmj/tableChkstomFirm_jmj";//生成门店报货单
	public static final String LISTDELIVERMAIL = "misboh/chkstom/jmj/listDeliverMail";//供应商邮件查询
	
}
