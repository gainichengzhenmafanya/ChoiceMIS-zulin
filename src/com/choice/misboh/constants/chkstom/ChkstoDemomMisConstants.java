package com.choice.misboh.constants.chkstom;
/**
 * 报货单单据模板
 * @author 孙胜彬
 */
public class ChkstoDemomMisConstants {
	//报货单单据模板主页面
	public static final String LIST_CHKSTODEMOM="misboh/chkstom/demo/treeChkstoDemom";
	//报货单单据模板子页面
	public static final String LIST_CHKSTODEMOD="misboh/chkstom/demo/listChkstoDemod";
	//修改报货单单据模板页面
	public static final String UPD_CHKSTODEMOD="misboh/chkstom/demo/updateChkstoDemod";
	//新增报货单单据模板页面
	public static final String ADD_ChkstoDemod="misboh/chkstom/demo/addChkstoDemod";
	
	public static final String ADD_CHKSTODEMOMIS = "misboh/chkstom/store/addChkstodemoMis";//报货模板
	
}
