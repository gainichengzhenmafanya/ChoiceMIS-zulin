package com.choice.misboh.constants.chkstom;

/***
 * 档口报货相关页面
 * @author wjf
 *
 */
public class ChkstomDeptMisConstants {

	public static final String TABLE_CHKSTOMDEPTMIS = "misboh/chkstom/dept/tableChkstomDeptMis";//档口报货页面
	public static final String SEARCH_CHKSTOM = "misboh/chkstom/dept/searchChkstom";//查找报货单
	public static final String LIST_CHKSTOM = "misboh/chkstom/dept/listChkstom";//档口报货单上传页面
	public static final String LIST_CHKSTOD = "misboh/chkstom/dept/listChkstod";//档口报货详情页面
	public static final String TABLECHKSTOMFIRM = "misboh/chkstom/dept/tableChkstomFirm";//生成门店报货单
	
}
