package com.choice.misboh.constants.chkstom;

/**
 * 报货向导
 * @author Wjf
 *
 */
public class ChkstomForecastConstants {
	
	/**
     *  报货向导主页面
     */
    public static final String LISTCHKSTOMFORECAST = "misboh/chkstom/forecast/listChkstomForecast";

	/**
	 *  报货向导左侧列表
	 */
    public static final String LEFT_CHKSTOMFORECAST = "misboh/chkstom/forecast/leftChkstomForecast";
    
    /**
     * 报货向导 右侧主界面
     */
    public static final String RIGHT_CHKSTOMFORECAST = "misboh/chkstom/forecast/rightChkstomForecast";
    
    /**
     * 报货向导 生成报货单
     */
    public static final String SAVECHKSTOM = "misboh/chkstom/forecast/saveChkstom";
    
    /**
     * 报货向导 生成报货单
     */
    public static final String SAVECHKSTOMDEPT = "misboh/chkstom/forecast/saveChkstomDept";
    
    /**
     * 报货向导 调用模板
     */
    public static final String ADD_CHKSTODEMOMIS = "misboh/chkstom/forecast/addChkstoDemoMis";
    
    /**
     * 报货向导 调整预估值
     */
    public static final String LISTADJUSTTHEESTIMATED ="misboh/chkstom/forecast/listAdjustTheEstimated";
    
    /**
     * 报货向导 千用量
     */
    public static final String LISTTHOUSANDSOFDOSAGE ="misboh/chkstom/forecast/listThousandsOfDosage";
    
    /**
     * 报货向导 千用量
     */
    public static final String LISTSPCODEUSE ="misboh/chkstom/forecast/listSpcodeUse";
    
    /**
     * 报货向导 菜品点击率
     */
    public static final String LISTDISHESCLICKATE ="misboh/chkstom/forecast/listDishesClickate";
    /**
     * 报货向导 菜品销售计划
     */
    public static final String LISTFOODSALESPLAN ="misboh/chkstom/forecast/listItemPlan";
    
    /**
     * 报货向导 生成报货单
     */
    public static final String GENERATEREPORTLIST ="misboh/declareGoodsGuide/generateReportList";
    
    /**
     * 用量报货   用量显示界面
     */
    public static final String LISTDOSAGEDAILYGOODS = "misboh/dosageDailygoods/listDosageDailygoods";
}
