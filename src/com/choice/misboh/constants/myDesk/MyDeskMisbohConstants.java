package com.choice.misboh.constants.myDesk;

/***
 * 我的桌面constants
 * @author wjf
 *
 */
public class MyDeskMisbohConstants {

	public static final String MYDESKMANAGE = "misboh/myDesk/myDeskManage";
	public static final String LISTMODULE = "misboh/myDesk/listModule";
}
