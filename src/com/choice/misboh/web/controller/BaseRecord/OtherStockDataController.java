package com.choice.misboh.web.controller.BaseRecord;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.constants.BaseRecord.BaseRecordConstant;
import com.choice.misboh.domain.BaseRecord.OtherStockDataF;
import com.choice.misboh.domain.BaseRecord.OtherStockDataO;
import com.choice.misboh.service.BaseRecord.BaseRecordService;
import com.choice.orientationSys.constants.StringConstant;
import com.choice.scm.domain.Positn;


/**
 * 描述：基础档案：水电气
 * @author 马振
 * 创建时间：2015-11-4 下午3:33:27
 */
@Controller
@RequestMapping(value="otherStockData")
public class OtherStockDataController {
	
	@Autowired
	private BaseRecordService baseRecordService;
	
	/**
	 * 描述：跳转到水电气显示页面
	 * @author 马振
	 * 创建时间：2015-4-8 上午9:31:22
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listOtherStockData")
	public ModelAndView listOtherStockData(ModelMap modelMap,OtherStockDataO otherStockDataO, HttpSession session,String workdate) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		String positnCode = "~";
		if (null != thePositn) {
			positnCode = thePositn.getCode();
		}
		if(workdate == null || workdate.equals("")){
			modelMap.put("workdate", new Date());
		}else{
			modelMap.put("workdate", DateJudge.YYYY_MM_DD.parse(workdate+"-01"));
		}
		otherStockDataO.setPk_store(positnCode);
		modelMap.put("water",baseRecordService.listWater(otherStockDataO));				 //获取水
		modelMap.put("waterCount", baseRecordService.waterCount(positnCode));		 				 //水的主表
		modelMap.put("electric", baseRecordService.listElectric(otherStockDataO));		 //获取电
		modelMap.put("electricCount", baseRecordService.electricCount(positnCode));				 //电的主表
		modelMap.put("gas", baseRecordService.listGas(otherStockDataO));				 //获取气
		modelMap.put("gasCount", baseRecordService.gasCount(positnCode));			 				 //气的主表
		return new ModelAndView(BaseRecordConstant.LIST_OTHERSTOCKDATA, modelMap);
	}
	
	/**
	 * 描述：调转到新增水表页面
	 * @author 马振
	 * 创建时间：2015-4-8 下午6:09:14
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "addWater")
	public ModelAndView addWater(ModelMap modelMap, String type, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		//如果type为shui，则调转到水表页面；type为dian，跳转到电表页面；type为qi，跳转到气表页面
		if ("shui".equals(type)) {
			modelMap.put("statistical", "shui");
			modelMap.put("type", 1);
		}
		
		if ("dian".equals(type)) {
			modelMap.put("statistical", "dian");
			modelMap.put("type", 2);
		}
		
		if ("qi".equals(type)) {
			modelMap.put("statistical", "qi");
			modelMap.put("type", 3);
		}	
		
		modelMap.put("sort", baseRecordService.getMaxSort());
		return new ModelAndView(BaseRecordConstant.ADD_WATER, modelMap);
	}
	
	/**
	 * 描述：保存水电气
	 * @author 马振
	 * 创建时间：2015-4-8 下午7:09:21
	 * @param modelMap
	 * @param storePos
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addWaterElectricGas")
	public ModelAndView addWaterElectricGas(ModelMap modelMap, OtherStockDataF otherStockDataF, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		String positnCode = "~";
		if (null != thePositn) {
			positnCode = thePositn.getCode();
		}
		otherStockDataF.setPk_otherstockdataf(CodeHelper.createUUID().substring(0, 20).toUpperCase());
		otherStockDataF.setPk_store(positnCode);
		otherStockDataF.setCtime(DateJudge.getNowTime());
		otherStockDataF.setEcode(session.getAttribute("accountName").toString());
		otherStockDataF.setScode(positnCode);
		baseRecordService.addWaterElectricGas(otherStockDataF);
		session.setAttribute("type", otherStockDataF.getType());
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 描述：根据主表主键删除水电气主子表信息
	 * @author 马振
	 * 创建时间：2015-4-9 下午2:22:02
	 * @param modelMap
	 * @param id
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/deleteAll")
	public ModelAndView deleteAll(ModelMap modelMap, String id, String type, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		String positnCode = "~";
		if (null != thePositn) {
			positnCode = thePositn.getCode();
		}
		baseRecordService.deleteAll(id, positnCode);
		session.setAttribute("type", type);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 描述：修改水电气主表
	 * @author 马振
	 * 创建时间：2015-5-25 下午3:17:32
	 * @param modelMap
	 * @param workdate
	 * @param type
	 * @param pk_otherstockdataf
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateOtherstockDataF")
	public ModelAndView updateOtherstockDataF(ModelMap modelMap, String type, String pk_otherstockdataf, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//如果type为shui，则调转到水表页面；type为dian，跳转到电表页面；type为qi，跳转到气表页面
		if ("shui".equals(type)) {
			modelMap.put("type", 1);
		}
		
		if ("dian".equals(type)) {
			modelMap.put("type", 2);
		}
		
		if ("qi".equals(type)) {
			modelMap.put("type", 3);
		}		
		modelMap.put("statistical", type);
		modelMap.put("dataF", baseRecordService.getOtherStockDataF(pk_otherstockdataf));
		return new ModelAndView(BaseRecordConstant.UPDATEZHUBIAO, modelMap);
	}
	
	/**
	 * 描述：保存修改的主
	 * @author 马振
	 * 创建时间：2015-5-25 下午3:33:47
	 * @param modelMap
	 * @param otherstockdataf
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveOtherstockDataF")
	public ModelAndView updateOtherstockDataF(ModelMap modelMap, OtherStockDataF otherstockdataf, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		baseRecordService.saveOtherStockDataF(otherstockdataf);
		session.setAttribute("type", otherstockdataf.getType());
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 描述：调转到水电气修改页面
	 * @author 马振
	 * 创建时间：2015-1-1 下午4:16:07
	 * @param modelMap
	 * @param otherStockDataF
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateWaterAndElectricAndGas")
	public ModelAndView updateWaterAndElectricAndGas(ModelMap modelMap,String workdate, String type, String pk_otherstockdataf, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//如果type为shui，则调转到水表页面；type为dian，跳转到电表页面；type为qi，跳转到气表页面
		if ("shui".equals(type)) {
			modelMap.put("statistical", "shui");
			modelMap.put("type", 1);
		}
		
		if ("dian".equals(type)) {
			modelMap.put("statistical", "dian");
			modelMap.put("type", 2);
		}
		
		if ("qi".equals(type)) {
			modelMap.put("statistical", "qi");
			modelMap.put("type", 3);
		}		
		
		modelMap.put("workdate", DateJudge.getNowDate());
		modelMap.put("dataF", baseRecordService.getOtherStockDataF(pk_otherstockdataf));
		List<OtherStockDataO> osdList = baseRecordService.allDataO(workdate, pk_otherstockdataf, type);
		modelMap.put("allDataO", osdList);
		modelMap.put("allDataOCount", osdList.size());
		return new ModelAndView(BaseRecordConstant.UPDATEWATERGAS, modelMap);
	}
	
	/**
	 * 描述：新增水电气子表
	 * @author 马振
	 * 创建时间：2015-4-10 下午7:00:40
	 * @param modelMap
	 * @param otherStockDataf
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveOtherStockData")
	public ModelAndView saveOtherStockData(ModelMap modelMap, OtherStockDataF otherStockDataf,String res, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		baseRecordService.insertOtherStockDataO(otherStockDataf.getStockDataList());
		session.setAttribute("type", otherStockDataf.getType());
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/***************************************手机水电气相关2016.1.7css (如需加方法  加到此分割线之上)****************************************/
	
	/**
	 * 查找各个水电气表
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findOtherStockCountWAP")
	@ResponseBody
	public Object findOtherStockCountWAP(String jsonpcallback, String firm) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("waterCount", baseRecordService.waterCountWap(firm));		 				 //水的主表
		map.put("electricCount", baseRecordService.electricCountWap(firm));				 //电的主表
		map.put("gasCount", baseRecordService.gasCountWap(firm));			 				 //气的主表
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/**
	 * 查找水电气月份合计
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findOtherStockListWAP")
	@ResponseBody
	public Object findOtherStockListWAP(String jsonpcallback, String workdate, String type, String firm) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		OtherStockDataO otherStockDataO = new OtherStockDataO();
		otherStockDataO.setWorkdate(workdate);
		if("1".equals(type)){
			map.put("detailList",baseRecordService.listWaterWap(otherStockDataO, firm));				 //获取水
		}else if("2".equals(type)){
			map.put("detailList", baseRecordService.listElectricWap(otherStockDataO, firm));		 //获取电
		}else if("3".equals(type)){
			map.put("detailList", baseRecordService.listGasWap(otherStockDataO, firm));				 //获取气
		}
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/**
	 * 描述：获取排序号
	 * @author css
	 * @throws Exception
	 */
	@RequestMapping(value = "getSortWAP")
	@ResponseBody
	public Object getSortWAP(String jsonpcallback) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("sort", baseRecordService.getMaxSort());
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/**
	 * 保存水电气主表
	 * @author css
	 * @param storePos
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addWaterElectricGasWAP")
	@ResponseBody
	public Object addWaterElectricGasWAP(OtherStockDataF otherStockDataF, String firm, String accountName) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		baseRecordService.addWaterElectricGasWAP(otherStockDataF, firm, accountName);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("pr", 1);
		return JSONObject.fromObject(map).toString();
	}
	
	/**
	 * 修改水电气主表
	 * @author css
	 * @param type
	 * @param pk_otherstockdataf
	 * @return map
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateOtherstockDataFWAP")
	@ResponseBody
	public Object updateOtherstockDataFWAP(String pk_otherstockdataf) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("dataF", baseRecordService.getOtherStockDataF(pk_otherstockdataf));
		return JSONObject.fromObject(map).toString();
	}
	
	/**
	 * 保存修改的主表
	 * @author css
	 * @param otherstockdataf
	 * @return Object
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveOtherstockDataFWAP")
	@ResponseBody
	public Object saveOtherstockDataFWAP(OtherStockDataF otherstockdataf) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		baseRecordService.saveOtherStockDataF(otherstockdataf);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("pr", 1);
		return JSONObject.fromObject(map).toString();
	}
	
	/**
	 * 根据主表主键删除水电气主子表信息
	 * @author 马振
	 * 创建时间：2015-4-9 下午2:22:02
	 * @param modelMap
	 * @param id
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/deleteAllWAP")
	@ResponseBody
	public Object deleteAllWAP(String jsonpcallback, String id, String firm) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		baseRecordService.deleteAllWAP(id, firm);
		map.put("pr", 1);
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/**
	 * 描述：调转到水电气修改页面
	 * @author css
	 * @throws Exception
	 */
	@RequestMapping(value = "/toupdateDetailWAP")
	public ModelAndView updateDetailWAP(ModelMap modelMap, String workdate, String type, String vname, String id) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("type", type);
		modelMap.put("workdate", workdate);
		modelMap.put("id", id);
		modelMap.put("vname", java.net.URLDecoder.decode(vname,"UTF-8"));
		return new ModelAndView(BaseRecordConstant.UPDATEWATERGAS, modelMap);
	}
	
	/**
	 * 描述：调转到水电气修改页面
	 * @author css
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateDetailWAP")
	@ResponseBody
	public Object updateDetailWAP(String workdate, String type, String pk_otherstockdataf) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("nowdate", DateJudge.getNowDate());
		map.put("dataF", baseRecordService.getOtherStockDataF(pk_otherstockdataf));
		List<OtherStockDataO> osdList = baseRecordService.allDataO(workdate, pk_otherstockdataf, type);
		map.put("allDataO", osdList);
		map.put("allDataOCount", osdList.size());
//		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
		return JSONObject.fromObject(map).toString();
	}
	
	/**
	 * 保存水电气子表
	 * @author css
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveOtherStockDataWAP")
	@ResponseBody
	public Object saveOtherStockDataWAP(OtherStockDataF otherStockDataf) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		baseRecordService.insertOtherStockDataO(otherStockDataf.getStockDataList());
		map.put("pr", 1);
		return JSONObject.fromObject(map).toString();
	}
	
	/**
	 * 根据sp_code获取物资信息并返回json对象
	 * @param supply
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/findByIdWAP")
	@ResponseBody
	public Object findById(OtherStockDataF otherStockDataf) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return baseRecordService.findById(otherStockDataf);
	}
}