package com.choice.misboh.web.controller.BaseRecord;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.misboh.constants.BaseRecord.ExternalBusinessSetConstant;
import com.choice.misboh.constants.BaseRecord.OperatorMaintenanceConstant;
import com.choice.misboh.domain.BaseRecord.MisBohOperator;
import com.choice.misboh.service.BaseRecord.OperatorMaintenanceService;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.orientationSys.constants.StringConstant;

/**
 * 描述：操作员维护
 * @author 马振
 * 创建时间：2015-8-24 下午1:15:53
 */
@Controller
@RequestMapping(value="operatorMaintenance")
public class OperatorMaintenanceController {
	
	@Autowired
	private OperatorMaintenanceService operatorMaintenanceService;

	@Autowired
	private CommonMISBOHService commonMISBOHService; 
	
	/**
	 * 描述：显示全部操作员
	 * @author 马振
	 * 创建时间：2015-8-24 下午1:19:00
	 * @param modelMap
	 * @param pubitem
	 * @param page
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listOperatorMaintenance")
	public ModelAndView listPubitems(ModelMap modelMap, MisBohOperator operator, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("listOperatorMaintenance", operatorMaintenanceService.listOperatorMaintenance(operator, session));
		return new ModelAndView(OperatorMaintenanceConstant.LIST_OPERATORMAINTENANCE, modelMap);
	}
	
	/**
	 * 描述：调转到操作员维护新增、修改页面
	 * @author 马振
	 * 创建时间：2015-8-24 下午1:19:23
	 * @param modelMap
	 * @param storePos
	 * @param page
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/edit")
	public ModelAndView editOperatorMaintenance(ModelMap modelMap, MisBohOperator operator, String iflag, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("operator", operatorMaintenanceService.getOperatorByPk(operator));
		modelMap.put("listStoreDept", operatorMaintenanceService.listStoreDept(session));
		modelMap.put("listRole", commonMISBOHService.getStoreRole(session));
		modelMap.put("pk_store", commonMISBOHService.getPkStore(session).getPk_store());
		modelMap.put("iflag", iflag);
		return new ModelAndView(OperatorMaintenanceConstant.EDIT_OPERATORMAINTENANCE, modelMap);
	}
	
	/**
	 * 描述：保存操作员维护
	 * @author 马振
	 * 创建时间：2015-8-24 下午1:35:42
	 * @param modelMap
	 * @param storePos
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/save")
	public ModelAndView saveOperatorMaintenance(ModelMap modelMap, MisBohOperator operator) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		operatorMaintenanceService.saveOperatorMaintenance(operator);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 描述：跳转到地图页面
	 * @author 马振
	 * 创建时间：2016-3-12 下午2:24:45
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/mapDisplay")
	public ModelAndView mapDisplay(ModelMap modelMap, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("listMapInfo", operatorMaintenanceService.listMapInfo(session));
		modelMap.put("count", operatorMaintenanceService.listMapInfo(session).size());
		modelMap.put("pk_store", commonMISBOHService.getPkStore(session).getPk_store());
		return new ModelAndView(ExternalBusinessSetConstant.MAPDISPLAY, modelMap);
	}
	
	/**
	 * 描述：保存地图上的区域坐标、区域颜色及起送价配送费
	 * @author 马振
	 * 创建时间：2016-3-12 上午11:29:49
	 * @param modelMap
	 * @param content
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveContent")
	@ResponseBody
	public String saveContent(String content, String pk_store, String iareaseq) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return operatorMaintenanceService.saveContent(content, pk_store, iareaseq);
	}
}