package com.choice.misboh.web.controller.BaseRecord;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.constants.BaseRecord.BaseRecordConstant;
import com.choice.misboh.domain.BaseRecord.Actm;
import com.choice.misboh.domain.BaseRecord.ItemPkg;
import com.choice.misboh.domain.BaseRecord.Marsaleclass;
import com.choice.misboh.domain.BaseRecord.OtherStockDataF;
import com.choice.misboh.domain.BaseRecord.PackageDtl;
import com.choice.misboh.domain.BaseRecord.PubItem;
import com.choice.misboh.domain.BaseRecord.PubPackage;
import com.choice.misboh.domain.BaseRecord.StorePos;
import com.choice.misboh.service.BaseRecord.BaseRecordService;
import com.choice.orientationSys.constants.StringConstant;
import com.choice.orientationSys.util.Page;


/**
 * 描述： 基础档案：菜品设置、套餐设置、POS定义、门店活动、水电气
 * @author 马振
 * 创建时间：2015-4-2 上午10:00:37
 */
@Controller
@RequestMapping(value="baseRecord")
public class BaseRecordController {
	
	@Autowired
	private BaseRecordService baseRecordService;
	
	/**
	 * 描述：菜品树显示
	 * @author 马振
	 * 创建时间：2015-4-20 上午11:29:06
	 * @param modelMap
	 * @param marsaleclass
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/treePubitems")
	public ModelAndView treeMarsaleclass(ModelMap modelMap, Marsaleclass marsaleclass,PubItem pubitem, HttpSession session, Page page) throws CRUDException{
		page.setPageSize(Integer.MAX_VALUE);
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<Map<String,Object>> listMarsaleclassTree = baseRecordService.marsaleclassTree(pubitem,session);
		modelMap.put("listMarsaleclassTree", listMarsaleclassTree);
		modelMap.put("pageobj", page);
		modelMap.put("pk_id", marsaleclass.getPk_Marsaleclass());
		return new ModelAndView(BaseRecordConstant.TREE_MARSALECLASS, modelMap);
	}
	
	/**
	 * 描述：菜品显示
	 * @author 马振
	 * 创建时间：2015-4-2 上午10:00:25
	 * @param modelMap
	 * @param pubitem
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listPubitems")
	public ModelAndView listPubitems(ModelMap modelMap, PubItem pubitem, Page page, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<Map<String, Object>> MarsaleclassList0=new ArrayList<Map<String,Object>>();
		List<Map<String, Object>> MarsaleclassList1=new ArrayList<Map<String,Object>>();
		List<Map<String, Object>> MarsaleclassList2=new ArrayList<Map<String,Object>>();
		Marsaleclass marsaleclass1  = new Marsaleclass();//中类查找参数
		Marsaleclass marsaleclass2  = new Marsaleclass();//小类查找参数
		Page page1 = new Page();
		page1.setPageSize(Integer.MAX_VALUE);
		
		//如果类别树选择的是三级类，查找二级一级类
		if(null != pubitem.getPk_vtyp() && (!"".equals(pubitem.getPk_vtyp()))){
			marsaleclass2 = baseRecordService.findMarsaleclassById(pubitem.getPk_vtyp());
			marsaleclass1 = baseRecordService.findMarsaleclassById(marsaleclass2.getPk_father());
			pubitem.setPk_vgrptyp(marsaleclass1.getPk_father());
			pubitem.setPk_vgrp(marsaleclass1.getPk_Marsaleclass());

			session.setAttribute("pk_vgrptyp", pubitem.getPk_vgrptyp());
			session.setAttribute("pk_vgrp", pubitem.getPk_vgrp());
		} 
		
		//大类，显示大类编码，大类名称，启用状态，排序序列，备注
		MarsaleclassList0 = baseRecordService.findAllMarsaleclassZero(new Marsaleclass(),page1);
		
		//中类，显示中类类编码，中类名称，大类编码，大类名称，启用状态，排序序列，备注
		marsaleclass1.setPk_father(pubitem.getPk_vgrptyp());
		MarsaleclassList1 = baseRecordService.findAllMarsaleclassOne(marsaleclass1,page1);
		
		//小类，显示小类编码，小类名称，中类类编码，中类名称，大类编码，大类名称，启用状态，排序序列，备注
		marsaleclass2.setPk_father(pubitem.getPk_vgrp());
		MarsaleclassList2 = baseRecordService.findAllMarsaleclassTwo(marsaleclass2,page1);
				
		modelMap.put("MarsaleclassList0", MarsaleclassList0);
		modelMap.put("MarsaleclassList1", MarsaleclassList1);
		modelMap.put("MarsaleclassList2", MarsaleclassList2);
		
		modelMap.put("listPubitem", baseRecordService.listPubitem(pubitem, page, session));
		modelMap.put("pubitem", pubitem);
		modelMap.put("pageobj", page);
		return new ModelAndView(BaseRecordConstant.LIST_PUBITEM, modelMap);
	}
	
	/**
	 * 描述：调转到菜谱详细显示页面
	 * @author 马振
	 * 创建时间：2015-4-7 下午4:28:56
	 * @param modelMap
	 * @param pubitem
	 * @param page
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updatePubitem")
	public ModelAndView updatePubitem(ModelMap modelMap, PubItem pubitem, Page page, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("pubitem", baseRecordService.listPubitem(pubitem, page, session).get(0));
		return new ModelAndView(BaseRecordConstant.UPDATE_PUBITEM, modelMap);
	}
	
	/**
	 * 描述：弹出菜品列表界面，根据类别进行分组显示
	 * @author 马振
	 * 创建时间：2015-4-24 下午2:23:44
	 * @param modelMap
	 * @param pubItem
	 * @param pk_id
	 * @param pk_parentId
	 * @param single
	 * @param callBack
	 * @param domId
	 * @param showPubpack
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toChoosePubItemToType")
	public ModelAndView toChoosePubItemToType(ModelMap modelMap, PubItem pubItem,String pk_id,String pk_parentId,String single,String callBack,String domId,String showPubpack) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		List<Map<String, Object>> MarsaleclassList0=new ArrayList<Map<String,Object>>();
		List<Map<String, Object>> MarsaleclassList1=new ArrayList<Map<String,Object>>();
		List<Map<String, Object>> MarsaleclassList2=new ArrayList<Map<String,Object>>();
		Page page1=new Page();
		page1.setPageSize(Integer.MAX_VALUE);
		//大类，显示大类编码，大类名称，启用状态，排序序列，备注
		MarsaleclassList0 = baseRecordService.findAllMarsaleclassZero(new Marsaleclass(),page1);
		//中类，显示中类类编码，中类名称，大类编码，大类名称，启用状态，排序序列，备注
		MarsaleclassList1 = baseRecordService.findAllMarsaleclassOne(new Marsaleclass(),page1);
//		//小类，显示小类编码，小类名称，中类类编码，中类名称，大类编码，大类名称，启用状态，排序序列，备注
		MarsaleclassList2 = baseRecordService.findAllMarsaleclassTwo(new Marsaleclass(),page1);
		Map<String,Object> map  = new HashMap<String,Object>();
		if(showPubpack=="true"||"true".equals(showPubpack)){
			map  = new HashMap<String,Object>();
			map.put("PK_MARSALECLASS", "99");
			map.put("NAME", "套餐");
			MarsaleclassList0.add(map);
			map  = new HashMap<String,Object>();
			map.put("PK_FATHER", "99");
			map.put("PK_MARSALECLASS", "99");
			map.put("NAME", "套餐");
			MarsaleclassList1.add(map);
		}
		modelMap.put("MarsaleclassList0", MarsaleclassList0);
		modelMap.put("MarsaleclassList1", MarsaleclassList1);
		modelMap.put("MarsaleclassList2", MarsaleclassList2);
		
		condition.put("pk_id","");
		condition.put("pk_parentId","");
		condition.put("pubItem", pubItem);
		List<Map<String, Object>> pubItemList=baseRecordService.findpubitemByMclassForPub(MarsaleclassList1,pk_id);
		if(showPubpack=="true"||"true".equals(showPubpack)){
			List<Map<String, Object>> pubpackList=baseRecordService.findpubpackList();
			pubItemList.addAll(pubpackList);
		}
		modelMap.put("pubItemList", pubItemList);
		
		modelMap.put("callBack", callBack);
		modelMap.put("domId", domId);
		modelMap.put("single", single);
		return new ModelAndView(BaseRecordConstant.CHOOSEPUBITEM, modelMap);
	}
	
	/**
	 * 描述：查询套餐设置信息
	 * @author 马振
	 * 创建时间：2015-4-2 下午1:10:52
	 * @param modelMap
	 * @param pubPackage
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listPubPackage")
	public ModelAndView listPubPackage(ModelMap modelMap, PubPackage pubPackage, Page page, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("listPackageType", baseRecordService.listPackageType());
		modelMap.put("listPubPackage", baseRecordService.listPubPackage(pubPackage, page, session));
		modelMap.put("pubPackage", pubPackage);
		modelMap.put("pageobj", page);
		return new ModelAndView(BaseRecordConstant.LIST_PUBPACKAGE, modelMap);
	}
	
	/**
	 * 描述：查询套餐明细
	 * @author 马振
	 * 创建时间：2015-4-7 下午6:42:05
	 * @param modelMap
	 * @param pubPackage
	 * @param page
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updatePubPackage")
	public ModelAndView updatePubPackage(ModelMap modelMap,PackageDtl packageDtl, PubPackage pubPackage, Page page, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		//根据套餐主键查询的套餐明细信息
		modelMap.put("listPackageDtl", baseRecordService.listPackageDtl(packageDtl));
		
		//套餐主表信息
		modelMap.put("pubPackage", baseRecordService.listPubPackage(pubPackage, page, session).get(0));
		return new ModelAndView(BaseRecordConstant.UPDATE_PUBPACKAGE, modelMap);
	}
	
	/**
	 * 描述：可换菜明细
	 * @author 马振
	 * 创建时间：2015-4-20 下午5:11:42
	 * @param modelMap
	 * @param packageDtl
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findPackageDtlById")
	@ResponseBody
	public Map<String,Object> findPackageDtlById(ModelMap modelMap, PackageDtl packageDtl) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		packageDtl.setPk_pubpack(FormatDouble.StringCodeReplace(packageDtl.getPk_pubpack()));
		List<Map<String,Object>> listPackageDtl = baseRecordService.listPackageDtl(packageDtl);
		Map<String,Object> map = new HashMap<String,Object>();
		if(listPackageDtl!=null && listPackageDtl.size()!=0){
			map.put("packageDtl", listPackageDtl.get(0));
		}else {
			map.put("packageDtl", packageDtl);
		}
		return map;
	}
	
	/**
	 * 描述：可换菜明细查询
	 * @author 马振
	 * 创建时间：2015-4-20 下午5:23:49
	 * @param modelMap
	 * @param itemPkg
	 * @param page
	 * @param ndiscountprice
	 * @param vpk_pubitem
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listItemPkg")
	public ModelAndView listItemPkg(ModelMap modelMap, ItemPkg itemPkg, Page page,String ndiscountprice,String vpk_pubitem) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		page.setPageSize(Integer.MAX_VALUE);
		List<Map<String,Object>> listItemPkg = baseRecordService.listItemPkg(itemPkg, page);
		modelMap.put("listItemPkg", listItemPkg);
		modelMap.put("pageobj", page);
		modelMap.put("itemPkg", itemPkg);
		modelMap.put("ndiscountprice", ndiscountprice);
		modelMap.put("vpk_pubitem", vpk_pubitem);
		return new ModelAndView(BaseRecordConstant.LIST_ITEMPKG, modelMap);
	}
	
	/**
	 * 描述：门店POS信息
	 * @author 马振
	 * 创建时间：2015-4-2 下午2:55:55
	 * @param modelMap
	 * @param storePos
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listStorePos")
	public ModelAndView listStorePos(ModelMap modelMap, StorePos storePos, Page page,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("listStorePos", baseRecordService.listStorePos(storePos, page, session));
		modelMap.put("pageobj", page);
		modelMap.put("storePos", storePos);
		return new ModelAndView(BaseRecordConstant.LIST_STOREPOS, modelMap);
	}
	
	/**
	 * 描述：活动树显示
	 * @author 马振
	 * 创建时间：2015-4-20 下午7:01:20
	 * @param modelMap
	 * @param marsaleclass
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/treeActm")
	public ModelAndView treeActm(ModelMap modelMap, Marsaleclass marsaleclass, Page page) throws CRUDException{
		page.setPageSize(Integer.MAX_VALUE);
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<Map<String,Object>> listMarsaleclassTree = baseRecordService.actmTree();
		modelMap.put("listMarsaleclassTree", listMarsaleclassTree);
		modelMap.put("pageobj", page);
		modelMap.put("pk_id", marsaleclass.getPk_Marsaleclass());
		return new ModelAndView(BaseRecordConstant.TREE_ACTM, modelMap);
	}
	
	/**
	 * 描述：门店活动显示
	 * @author 马振
	 * 创建时间：2015-4-2 下午5:42:08
	 * @param modelMap
	 * @param actm
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listActm")
	public ModelAndView listActm(ModelMap modelMap, Actm actm, Page page, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("listActm", baseRecordService.listActm(actm, page, session));
		modelMap.put("pageobj", page);
		modelMap.put("actm", actm);
		return new ModelAndView(BaseRecordConstant.LIST_ACTM, modelMap);
	}
	
	/**
	 * 描述：门店活动详细显示
	 * @author 马振
	 * 创建时间：2015-4-21 上午9:13:32
	 * @param modelMap
	 * @param actm
	 * @param page
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/actmDetail")
	public ModelAndView actmDetail(ModelMap modelMap, String pk_actm, Page page, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Actm actm = new Actm();
		actm.setPk_actm(pk_actm);
		modelMap.put("str", baseRecordService.getActmStr(actm, session));				//门店活动日期限制
		modelMap.put("time", baseRecordService.getWptime(actm, session));				//周次时间限制
		modelMap.put("lck", baseRecordService.findActlckById(actm, session));			//互斥活动限制
		modelMap.put("jianMian", baseRecordService.getJianMian(actm, session));			//金额减免
		modelMap.put("diaoJia", baseRecordService.getDiaoJia(actm, session));			//金额调价
		modelMap.put("fanQuan", baseRecordService.getFanQuan(actm, session));			//消费返券
		modelMap.put("jiFen", baseRecordService.getActmFen(actm, session));				//消费返积分
		modelMap.put("member", baseRecordService.getMemberLimit(actm, session));		//人数限制
		modelMap.put("ticket", baseRecordService.getTicketLimit(actm, session));		//张数限制
		modelMap.put("amount", baseRecordService.getAmountLimit(actm, session));		//金额限制
		modelMap.put("class", baseRecordService.getClassesLimit(actm, session));		//班次限制
		modelMap.put("pubitem", baseRecordService.getPubitemLimit(actm, session));		//菜品限制
		modelMap.put("actm", baseRecordService.listActm(actm, page, session).get(0));
		return new ModelAndView(BaseRecordConstant.ACTM_DETAIL, modelMap);
	}
	
	/**
	 * 描述：跳转到门店POS新增页面
	 * @author 马振
	 * 创建时间：2015-4-7 上午10:57:35
	 * @param modelMap
	 * @param storePos
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/add")
	public ModelAndView add(ModelMap modelMap, StorePos storePos, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("storePos", storePos);
		return new ModelAndView(BaseRecordConstant.ADD_STOREPOS, modelMap);
	}
	
	/**
	 * 描述：新增门店pos
	 * @author 马振
	 * 创建时间：2015-4-7 下午12:26:20
	 * @param modelMap
	 * @param storePos
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addStorePos")
	public ModelAndView addStorePos(ModelMap modelMap, StorePos storePos, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		baseRecordService.addStorePos(storePos, session);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 描述：调转到门店pos修改页面
	 * @author 马振
	 * 创建时间：2015-4-7 上午11:01:40
	 * @param modelMap
	 * @param storePos
	 * @param page
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update")
	public ModelAndView update(ModelMap modelMap, StorePos storePos, Page page, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("storePos", baseRecordService.listStorePos(storePos, page, session).get(0));
		return new ModelAndView(BaseRecordConstant.UPDATE_STOREPOS, modelMap);
	}
	
	/**
	 * 描述：保存修改的门店pos
	 * @author 马振
	 * 创建时间：2015-4-7 下午1:39:50
	 * @param modelMap
	 * @param storePos
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateStorePos")
	public ModelAndView updateStorePos(ModelMap modelMap, StorePos storePos) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		baseRecordService.updateStorePos(storePos);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 描述：门店pos弹出框
	 * @author 马振
	 * 创建时间：2015-4-16 下午2:58:48
	 * @param modelMap
	 * @param session
	 * @param pk_store
	 * @param single
	 * @param callBack
	 * @param domId
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toChooseStorePos")
	public ModelAndView toChooseStorePos(ModelMap modelMap,HttpSession session,String pk_store, String single,String callBack,String domId,Page page) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		//创建StorePos，存储查询条件
		StorePos storePos = new StorePos();
		storePos.setPk_store(baseRecordService.getPkStore(session));
		storePos.setEnablestate(2);
		
		modelMap.put("listStorePos", baseRecordService.listStorePos(storePos, page, session));
		modelMap.put("pageobj", page);
		modelMap.put("callBack", callBack);
		modelMap.put("domId", domId);
		modelMap.put("single", single);
		modelMap.put("pk_store", pk_store);
		return new ModelAndView(BaseRecordConstant.CHOOSESTOREPOS, modelMap);
	}

	/**
	 * 描述： 菜品打印信息
	 * @author 马振
	 * 创建时间：2016年4月19日下午9:50:30
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listPubitemPrint")
	public ModelAndView listPubitemPrint(ModelMap modelMap, PubItem pubitem, HttpSession session, Page page) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("pageobj", page);
		modelMap.put("listPubitemPrint", baseRecordService.listPubitemPrint(pubitem, page, session));
		return new ModelAndView(BaseRecordConstant.LISTPUBITEMPRINT, modelMap);
	}
	
	/**
	 * 描述：移除修改页面，跳转到显示页面
	 * @author 马振
	 * 创建时间：2015-4-11 下午5:30:57
	 * @param modelMap
	 * @param otherStockDataf
	 * @param res
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toList")
	public ModelAndView listA(ModelMap modelMap, OtherStockDataF otherStockDataf,String res, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
}