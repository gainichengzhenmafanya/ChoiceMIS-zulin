package com.choice.misboh.web.controller.lossmanagement;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.constants.lossmanagement.LossManageConstants;
import com.choice.misboh.domain.BaseRecord.PubItem;
import com.choice.misboh.domain.lossmanage.PostionLoss;
import com.choice.misboh.domain.store.Store;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.misboh.service.lossmanagement.LossManageService;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Positn;
import com.choice.scm.service.CodeDesService;

/**
 * 损耗管理
 * @author 王超
 * 2015-04-22
 */
@Controller
@RequestMapping("lossManage")
public class LossManageController {
	
	@Autowired
	private LossManageService lossManageService;
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	@Autowired
	private CodeDesService codeDesService;
	
	/**
	 * 查询菜品报损
	 * @param modelMap
	 * @param session
	 * @param workdate
	 * @param dept
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/listLossManage")
	public ModelAndView listLossManage(ModelMap modelMap, HttpSession session, Date workdate, String dept,String deptDes,int typ) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn positn = commonMISBOHService.getPositn(session);
		Store store = new Store();
		store.setPk_group(session.getAttribute("ChoiceAcct").toString());
		store.setVcode(positn.getCode());
//		Positn p = new Positn();
//		p.setUcode(commonMISBOHService.getPositn(accountId).getCode());
//		modelMap.put("deptList", positnService.findDeptByPositn(p));//根据门店编码获取所有部门
//		modelMap.put("deptList", commonMISBOHService.getAllStoreDept(store));
		if (workdate == null) {
			workdate = new Date();
		}
		Map<String,String> map = new HashMap<String,String>();
		map.put("workdate", DateJudge.getStringByDate(workdate, "yyyy-MM-dd"));
		if (dept == null) {
			map.put("dept", positn.getCode());
			dept = positn.getCode();
			deptDes = positn.getDes();
		} else {
			map.put("dept", dept);
		}
		map.put("scode", positn.getCode());
		map.put("typ", ValueUtil.getStringValue(typ));
		List<PostionLoss> list = new ArrayList<PostionLoss>();
		list = lossManageService.listLossManage(map);
		modelMap.put("listPostionLoss", list);
		modelMap.put("dept", dept);
		modelMap.put("deptDes", deptDes);
		modelMap.put("workdate", workdate);
		modelMap.put("ynusedept", positn.getYnUseDept());//是否启用多档口
		modelMap.put("typ", ValueUtil.getStringValue(typ));//损耗类型
		modelMap.put("scode", positn.getCode());
		CodeDes codes = new CodeDes();		
		codes.setTyp(DDT.CODEDES13);
		modelMap.put("codeDesList", codeDesService.findCodeDes(codes));//查询所有的报废原因(不分页)
		if (typ == 1) {
			return new ModelAndView(LossManageConstants.LIST_LOSSMANAGE,modelMap);
		}
		if (typ == 2) {
			return new ModelAndView(LossManageConstants.LIST_LOSSMANAGE_BCP,modelMap);
		}
		if (typ == 3) {
			return new ModelAndView(LossManageConstants.LIST_LOSSMANAGE_WZ,modelMap);
		}
		return null;
	}
	
	/**
	 * 自动检索菜品
	 * @param pubitem
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/queryPubItemmTop10", method=RequestMethod.POST)
	public List<PubItem> queryPubItemmTop10(PubItem pubitem,HttpSession session,String dept)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (dept == null) {
			dept = thePositn.getCode();
		}
		return lossManageService.queryPubItemmTop10(pubitem,session,dept);
	}
	
	/**
	 * 保存损耗单
	 * @param session
	 * @param listPostionLoss
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/saveLossManage")
	public String saveLossManage(HttpSession session,PostionLoss postionLoss)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (postionLoss.getDept() == null) {
			postionLoss.setDept(thePositn.getCode());
		}
		postionLoss.setScode(thePositn.getCode());
		return lossManageService.saveLossManage(session,postionLoss);
	}
	
	/**
	 * 清空损耗单
	 * @param session
	 * @param listPostionLoss
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteLossManage")
	public String deleteLossManage(HttpSession session,Date workdate, String dept,int typ)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn positn = (Positn)session.getAttribute("accountPositn");
		if (dept == null) {
			dept = positn.getCode();
		}
		PostionLoss postionLoss = new PostionLoss();
		postionLoss.setWorkdate(DateJudge.getStringByDate(workdate, "yyyy-MM-dd"));
		postionLoss.setDept(dept);
		postionLoss.setTyp(typ);
		postionLoss.setScode(positn.getCode());
		return lossManageService.deleteLossManage(postionLoss);
	}
	
	/**
	 * 确认损耗单
	 * @param session
	 * @param listPostionLoss
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/enterLossManage")
	public String enterLossManage(HttpSession session,Date workdate, String dept,int typ)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		try{
			Positn positn = (Positn)session.getAttribute("accountPositn");
			if (dept == null) {
				dept = positn.getCode();
			}
			PostionLoss postionLoss = new PostionLoss();
			postionLoss.setWorkdate(DateJudge.getStringByDate(workdate, "yyyy-MM-dd"));
			postionLoss.setDept(dept);
			postionLoss.setTyp(typ);
			postionLoss.setState(1);
			postionLoss.setScode(positn.getCode());
			List<String> BomErrorList = lossManageService.checkEnterLossManage(postionLoss,session,typ);
			if(BomErrorList!=null && BomErrorList.size()>0){
				String error = "";
				for(String bomcode:BomErrorList){
					error +=bomcode+",";
				}
				if(error!=null && !error.equals("")){
					error = error.substring(0,error.length()-1);
				}
				if(typ == 1){
					return "以下菜品未设置BOM："+error;
				}else if(typ == 2){
					return "以下半成品未设置BOM："+error;
				}
			}
			if (typ == 1) {
				return lossManageService.enterLossManage(postionLoss,session);
			}
			if (typ == 2) {
				return lossManageService.enterLossManage2(postionLoss,session);
			}
			if (typ == 3) {
				return lossManageService.enterLossManage3(postionLoss,session);
			}
			return null;
		}catch(Exception e){
			return e.getLocalizedMessage();
		}
	}
	
	/**
	 * 查询所有损耗原因
	 * @param supply
	 * @param session
	 * @param dept
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/getScrappedname", method=RequestMethod.POST)
	public List<CodeDes> getScrappedname()throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		CodeDes codes = new CodeDes();		
		codes.setTyp(DDT.CODEDES13);
		return codeDesService.findCodeDes(codes);//查询所有的报废原因(不分页)
	}
}
