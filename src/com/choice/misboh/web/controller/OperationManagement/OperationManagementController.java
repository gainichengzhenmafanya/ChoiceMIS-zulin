package com.choice.misboh.web.controller.OperationManagement;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.constants.operationManagement.OperationManagementConstants;
import com.choice.misboh.domain.OperationManagement.CashSaving;
import com.choice.misboh.domain.OperationManagement.OutLay;
import com.choice.misboh.domain.OperationManagement.SpecialEvent;
import com.choice.misboh.domain.reportMis.PublicEntity;
import com.choice.misboh.service.OperationManagement.OperationManagementService;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.misboh.service.reportMis.MisBohNewReportService;
import com.choice.orientationSys.constants.StringConstant;
import com.choice.scm.domain.Positn;

/**
 * 描述： 运营管理：营业外收入、坐支明细、运营情况登记、存款管理
 * @author 马振
 * 创建时间：2015-4-2 上午10:00:37
 */
@Controller
@RequestMapping(value="operationManagement")
public class OperationManagementController {
	
	@Autowired
	private OperationManagementService operationManagementService;
	
	@Autowired
	private CommonMISBOHService commonMISBOHService; 
	
	@Autowired
	private MisBohNewReportService newReportService;
	
	/**
	 * 描述：根据flag的值判断是营业外收入或坐支明细
	 * @author 马振
	 * 创建时间：2015-5-18 上午10:31:26
	 * @param modelMap
	 * @param otherStockDataf
	 * @param res
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findOutLay")
	public ModelAndView findOutLay(ModelMap modelMap,OutLay outLay,String flag, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String url = "";//跳转页面的路径
		Positn thePositn = commonMISBOHService.getPositn(session);
		modelMap.put("pk_store", operationManagementService.getPkStore(session));
		modelMap.put("vscode",thePositn.getCode());
		modelMap.put("vecode", session.getAttribute("accountName").toString());
		
		//判断是否日结，是则用positn表中的workdate
		if ("0" == ForResourceFiles.getValByKey("config-bsboh.properties","whetherDayKnot")){
			outLay.setDworkdate(DateJudge.getStringByDate(thePositn.getWorkdate(), "YYYY-MM-DD"));
		} else {
			if (ValueUtil.IsEmpty(outLay.getDworkdate())) {
				outLay.setDworkdate(DateJudge.getNowDate());
			}
		}

		//若flag=income，则为营业外收入
		if ("income".equals(flag)) {
			
			//判断日期是否为历史日期，是则查询历史数据，否则查询当前数据
			if (DateJudge.timeCompare(DateJudge.getNowDate(), outLay.getDworkdate())) {
				
				//判断表中是否已有今日数据，有则直接显示，否则显示总部数据
				if (0 == operationManagementService.findIncome(outLay,session).size()) {
					modelMap.put("listIncome", operationManagementService.findIncomeNow(outLay));
					modelMap.put("number", operationManagementService.findIncomeNow(outLay).size());
				} else {
					modelMap.put("listIncome", operationManagementService.findIncome(outLay,session));
					modelMap.put("number", operationManagementService.findIncome(outLay,session).size());
				}
			} else {
				modelMap.put("listIncome", operationManagementService.findIncome(outLay,session));
				modelMap.put("number", operationManagementService.findIncome(outLay,session).size());
			}
			modelMap.put("nprice", operationManagementService.findPriceAndQuantity(outLay, session).getNprice());
			modelMap.put("iquantity", operationManagementService.findPriceAndQuantity(outLay, session).getIquantity());
			modelMap.put("dworkdate", outLay.getDworkdate());
			url = OperationManagementConstants.LISTINCOME;
		}
		
		//若flag=outlay，则为坐支明细
		if ("outlay".equals(flag)) {
			
			//判断日期是否为历史日期，是则查询历史数据，否则查询当前数据
			if (DateJudge.timeCompare(DateJudge.getNowDate(), outLay.getDworkdate())) {
				
				//判断表中是否已有今日数据，有则直接显示，否则显示总部数据
				if (0 == operationManagementService.findOutLay(outLay,session).size()) {
					modelMap.put("listOutLay", operationManagementService.findOutLayNow(outLay));
					modelMap.put("number", operationManagementService.findOutLayNow(outLay).size());
				} else {
					modelMap.put("listOutLay", operationManagementService.findOutLay(outLay,session));
					modelMap.put("number", operationManagementService.findOutLay(outLay,session).size());
				}
			} else {
				modelMap.put("listOutLay", operationManagementService.findOutLay(outLay,session));
				modelMap.put("number", operationManagementService.findOutLay(outLay,session).size());
			}
			modelMap.put("nprice", operationManagementService.findPrice(outLay, session).getNprice());
			modelMap.put("dworkdate", outLay.getDworkdate());
			url = OperationManagementConstants.LISTOUTLAY;
		}
		
		modelMap.put("timeCompare", DateJudge.timeCompare(DateJudge.getNowDate(), outLay.getDworkdate()));
		return new ModelAndView(url, modelMap);
	}
	
	/**
	 * 描述：保存明细录入数据（营业外收入、坐支明细）
	 * @author 马振
	 * 创建时间：2015-5-18 上午11:18:34
	 * @param modelMap
	 * @param cashMx
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveCashMx")
	public ModelAndView saveCashMx(ModelMap modelMap, OutLay outLay, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		operationManagementService.saveCashMx(outLay.getListCashMx(),session);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 描述：查询现在所用事件
	 * @author 马振
	 * 创建时间：2015-5-20 下午6:54:50
	 * @param modelMap
	 * @param specialEvent
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findOperation")
	public ModelAndView listSpecialItem(ModelMap modelMap, SpecialEvent specialEvent, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = commonMISBOHService.getPositn(session);
		//判断是否日结，是则用positn表中的workdate
		if ("0" == ForResourceFiles.getValByKey("config-bsboh.properties","whetherDayKnot")){
			specialEvent.setDworkdate(DateJudge.getStringByDate(thePositn.getWorkdate(), "YYYY-MM-DD"));
		} else {
			if (ValueUtil.IsEmpty(specialEvent.getDworkdate())) {
				specialEvent.setDworkdate(DateJudge.getNowDate());
			}
		}
		
		//今日所显示数据
		List<SpecialEvent> listEvent = new ArrayList<SpecialEvent>();
		
		//判断日期是否比今日早，是则显示历史数据，否则直接显示总部的数据
		if (DateJudge.timeCompare(DateJudge.getNowDate(), specialEvent.getDworkdate())) {
			listEvent = operationManagementService.listSpecialItem(specialEvent);
			
			//判断特殊事件表中是否已有今天的数据，有则显示，否则直接显示总部的数据
			if (0 != operationManagementService.listSpecialEvent(specialEvent,session).size()) {
				listEvent = operationManagementService.listSpecialEvent(specialEvent,session);
			}
		} else {
			listEvent = operationManagementService.listSpecialEvent(specialEvent,session);
		}
		
		modelMap.put("listSpecialItem", listEvent);
		modelMap.put("dworkdate", specialEvent.getDworkdate());
		modelMap.put("pk_store", operationManagementService.getPkStore(session));
		modelMap.put("timeCompare", DateJudge.timeCompare(DateJudge.getNowDate(), specialEvent.getDworkdate()));
		return new ModelAndView(OperationManagementConstants.LISTOPERATION, modelMap);
	}
	
	/**
	 * 描述：保存修改的特殊事件子表
	 * @author 马振
	 * 创建时间：2015-5-20 下午4:01:25
	 * @param modelMap
	 * @param specialItem
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveSelectItemDtl")
	public ModelAndView saveSelectItemDtl(ModelMap modelMap, SpecialEvent specialEvent, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		operationManagementService.saveSpecialEvent(specialEvent.getListSelectItemDtl());
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 描述：存款管理
	 * @author 马振
	 * 创建时间：2015-5-20 上午11:30:26
	 * @param modelMap
	 * @param outLay
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findDepositManagement")
	public ModelAndView findCashSaving(ModelMap modelMap,CashSaving cashSaving, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = commonMISBOHService.getPositn(session);
		//判断是否日结，是则用positn表中的日期
		if ("0" == ForResourceFiles.getValByKey("config-bsboh.properties","whetherDayKnot")){
			cashSaving.setBdat(DateJudge.getStringByDate(thePositn.getWorkdate(), "YYYY-MM-DD"));//开始日期
			cashSaving.setEdat(DateJudge.getStringByDate(thePositn.getWorkdate(), "YYYY-MM-DD"));//结束日期
		} else {
			if (ValueUtil.IsEmpty(cashSaving.getBdat())) {
				cashSaving.setBdat(DateJudge.getNowDate());//开始日期
			}
			
			if (ValueUtil.IsEmpty(cashSaving.getEdat())) {
				cashSaving.setEdat(DateJudge.getNowDate());//结束日期
			}
		}
		
		modelMap.put("listCashSving", operationManagementService.findCashSaving(cashSaving, session));
		modelMap.put("cs", operationManagementService.findAmountCount(cashSaving, session));
//		modelMap.put("listPayMode", operationManagementService.listPayMode());
		modelMap.put("bdat", cashSaving.getBdat());
		modelMap.put("edat", cashSaving.getEdat());
		modelMap.put("queryMod", cashSaving.getQueryMod());
		modelMap.put("nowDate", DateJudge.getLenTime(-7, DateJudge.getNowDate()));
		return new ModelAndView(OperationManagementConstants.LISTCASHSAVING, modelMap);
	}
	
	/**
	 * 描述：跳转到新增、修改页面
	 * @author 马振
	 * 创建时间：2015-5-20 下午4:20:07
	 * @param modelMap
	 * @param cashSaving
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addData")
	public ModelAndView addData(ModelMap modelMap,CashSaving cashSaving, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		
		//若cashSaving中flag为add则是新增
		if ("add".equals(cashSaving.getFlag())) {
			modelMap.put("dworkdate", DateJudge.getNowDate());
			modelMap.put("vsavingdate", DateJudge.getNowDateTime());
			modelMap.put("ncash", operationManagementService.findCashByDateStore(cashSaving, session).getNcash());
		}
		
		//若cashSaving中flag为update则是修改
		if ("update".equals(cashSaving.getFlag())) {
			CashSaving cash = operationManagementService.getCashSavingByPk(cashSaving, session);
			modelMap.put("cashSaving", cash);
			modelMap.put("dworkdate", cash.getDworkdate());
			modelMap.put("vsavingdate", cash.getVsavingdate());
			modelMap.put("ncash", cash.getNcash());
		}

		modelMap.put("listAccountType", operationManagementService.listAccountType());
		modelMap.put("listPayMode", operationManagementService.listPayMode());
		modelMap.put("flag", cashSaving.getFlag());
		return new ModelAndView(OperationManagementConstants.ADDCASHSAVING, modelMap);
	}
	
	/**
	 * 描述： 根据营业日，获取当前营业日当前门店所有的存款金额
	 * @author 马振
	 * 创建时间：2016年3月24日下午3:01:50
	 * @param modelMap
	 * @param cashSaving
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findAmount")
	@ResponseBody
	public Double findAmount(CashSaving cashSaving, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		return operationManagementService.findAmount(cashSaving, session);
	}
	
	/**
	 * 描述： 获取当前营业日下的存款金额
	 * @author 马振
	 * 创建时间：2016年3月24日下午3:27:26
	 * @param cashSaving
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findNcash")
	@ResponseBody
	public Map<String, Double> findNcash(CashSaving cashSaving, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		Double cash = operationManagementService.findCashByDateStore(cashSaving, session).getNcash();	//当前营业日、门店的现金金额
		Double amount = operationManagementService.findAmount(cashSaving, session);						//当前营业日、门店的存款金额
		
		Map<String, Double> map = new HashMap<String, Double>();
		map.put("cash", cash);
		map.put("amount", amount);
		
		return map;
	}
	
	/**
	 * 描述： 是否日结之后的数据
	 * @author ygb
	 * @param cashSaving
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkUpFlag")
	@ResponseBody
	public Map<String, Object> checkUpFlag(PublicEntity condition, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null == condition.getEdat()){
			condition.setEdat(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		}
		condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("uploadFlag",newReportService.findUploadFlag(condition));
		
		return map;
	}
	
	/**
	 * 描述： 小于上个单据回单时间的数据不能新增
	 * @author ygb
	 * @param cashSaving
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkSavingDate")
	@ResponseBody
	public Map<String, Object> checkSavingDate(CashSaving cashSaving, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		String savdate = operationManagementService.findSavingDate(cashSaving, session);	
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("savdate",savdate);
		
		return map;
	}
	
	/**
	 * 描述： 回单流水号是否存在
	 * @author ygb
	 * @param cashSaving
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkSavingno")
	@ResponseBody
	public Map<String, Object> checkSavingno(CashSaving cashSaving, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		String savno = operationManagementService.findSavingNo(cashSaving, session);	
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("savno",savno);
		
		return map;
	}
	
	/**
	 * 描述：保存数据
	 * @author 马振
	 * 创建时间：2015-5-20 下午5:21:32
	 * @param modelMap
	 * @param cashSaving
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveData")
	public ModelAndView saveData(ModelMap modelMap,CashSaving cashSaving, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		//若cashSaving中flag为update则是新增
		if ("add".equals(cashSaving.getFlag())) {
			operationManagementService.addCashSaving(cashSaving, session);	
			//是否继续录入下一笔存款单
			if ("yes".equals(cashSaving.getIsgo())) {
				modelMap.put("dworkdate", DateJudge.getNowDate());
				modelMap.put("vsavingdate", DateJudge.getNowDateTime());
				modelMap.put("ncash", operationManagementService.findCashByDateStore(new CashSaving(), session).getNcash());
				modelMap.put("listAccountType", operationManagementService.listAccountType());
				modelMap.put("listPayMode", operationManagementService.listPayMode());
				modelMap.put("flag", "add");
				modelMap.put("cashSaving", new CashSaving());
				return new ModelAndView(OperationManagementConstants.ADDCASHSAVING, modelMap);
			}
		}
		
		//若cashSaving中flag为update则是新增
		if ("update".equals(cashSaving.getFlag())) {
			operationManagementService.updateCashSaving(cashSaving);
		}
		
		//删除数据
		if ("delete".equals(cashSaving.getFlag())) {
			operationManagementService.deleteCashSaving(cashSaving);
		}
		
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 描述：页面跳转提示
	 * @author 马振
	 * 创建时间：2015-5-26 上午9:33:57
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toList")
	public ModelAndView listA(ModelMap modelMap, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
}