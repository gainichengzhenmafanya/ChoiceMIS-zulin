package com.choice.misboh.web.controller.proReport;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.misboh.commonutil.MisUtil;
import com.choice.misboh.commonutil.ReadReport;
import com.choice.misboh.commonutil.ReadReportUrl;
import com.choice.misboh.commonutil.report.ExportExcel;
import com.choice.misboh.commonutil.report.ReadReportConstants;
import com.choice.misboh.constants.proReport.ProReportConstant;
import com.choice.misboh.constants.reportMis.MisStringConstant;
import com.choice.misboh.domain.BaseRecord.Actm;
import com.choice.misboh.domain.common.CommonMethod;
import com.choice.misboh.domain.reportMis.PublicEntity;
import com.choice.misboh.domain.reportMis.ReportObject;
import com.choice.misboh.service.BaseRecord.BaseRecordService;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.misboh.service.proReport.ProReportService;
import com.choice.misboh.web.controller.reportMis.MisBohBaseController;
import com.choice.orientationSys.util.Page;
import com.choice.tele.util.TeleUtils;

/**
 * 描述：项目报表
 * @author wangkai
 * 创建时间：2016-2-19
 */
@Controller
@RequestMapping("proReport")
public class ProReportController extends MisBohBaseController {

	@Autowired
	private CommonMISBOHService commonMISBOHService; 
	
	@Autowired
	private ProReportService proReportService;
	
	@Autowired
	private BaseRecordService baseRecordService;
	
	/**
	 * 描述：跳转到报表页面
	 * @author 马振
	 * 创建时间：2015-6-25 上午11:23:40
	 * @param modelMap
	 * @param reportName
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toReport")
	public ModelAndView toReport(ModelMap modelMap,String reportName,HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("condition", new PublicEntity());
		modelMap.put("sftList", commonMISBOHService.findAllShiftsft(new CommonMethod()));
		modelMap.put("store", commonMISBOHService.getPkStore(session));
		getRoleTiem(modelMap,session);
		return new ModelAndView(ReadReportConstants.getReportURL(ProReportConstant.class, reportName),modelMap);
	}
	
	/**
	 * 描述：报表导出
	 * @author 马振
	 * 创建时间：2015-4-23 下午2:17:51
	 * @param response
	 * @param request
	 * @param session
	 * @param publicEntity
	 * @param reportName
	 * @param headers
	 * @throws CRUDException
	 */
	@RequestMapping("/exportReport")
	public void exportReport(HttpServletResponse response,HttpServletRequest request,HttpSession session,PublicEntity publicEntity,String reportName,String headers,String page,String rows) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		//设置分页，查询所有数据
		publicEntity.getPager().setPageSize(Integer.MAX_VALUE);
		publicEntity.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ReadReport.getReportName(MisStringConstant.class, reportName));
		
		//获取并执行查询方法，获取查询结果
		ReportObject<Map<String,Object>> result = MisUtil.execMethod(proReportService, reportName, publicEntity);
		if(null != result.getFooter())
			result.getRows().addAll(result.getFooter());
		if(null != headers || !"".equals(headers))
			ExportExcel.creatWorkBook(request,response,result.getRows(),ReadReport.getReportNameCN(MisStringConstant.class, reportName),null,headers);
		else
			ExportExcel.creatWorkBook(ReadReport.getReportNameCN(MisStringConstant.class, reportName),request,response, result.getRows(), ReadReport.getReportNameCN(MisStringConstant.class, reportName), dictColumnsService.listDictColumnsByAccount(dictColumns, ReadReport.getDefaultHeader(MisStringConstant.class, reportName)));
	}
	
	/**
	 * 描述：报表打印
	 * @author 马振
	 * 创建时间：2015-5-5 上午10:38:12
	 * @param modelMap
	 * @param request
	 * @param session
	 * @param condition
	 * @param type
	 * @param reportName
	 * @param start
	 * @param end
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/printReport")
	public ModelAndView printReport(ModelMap modelMap,HttpServletRequest request,HttpSession session,PublicEntity condition,String type,String reportName,String start,String end) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			condition.setFirmid(commonMISBOHService.getPkStore(session).getPk_store());
		
		//获取并执行查询方法，获取查询结果
		modelMap.put("List",MisUtil.execMethod(proReportService, reportName, condition).getRows());
	 	HashMap<String,Object>  parameters = new HashMap<String,Object>();
	    parameters.put("report_name", ReadReport.getReportNameCN(MisStringConstant.class, reportName));
	    Map<String,String> map = TeleUtils.convertToMap(condition);
	    map.remove("pager");
	    map.put("bdat", condition.getBdat());
	    map.put("edat", condition.getEdat());
	    map.put("start", start);
	    map.put("end", end);
	    map.put("reportName", reportName);
	    modelMap.put("actionMap", map);
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	    parameters.put("bdat", condition.getBdat());
	    parameters.put("edat", condition.getEdat());
	    parameters.put("start", start);
	    parameters.put("end", end);
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", request.getServletPath());//传入回调路径
	 	Map<String,String> rs = ReadReportUrl.redReportUrl(type,ReadReport.getIReportPrint(ProReportConstant.class,reportName));//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
      		
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 描述:表头查询
	 * 作者:马振
	 * 时间:2016年7月29日下午3:09:11
	 * @param session
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findHeaderForDiscfx")
	@ResponseBody
	public Object findHeaderForDiscfx(HttpSession session, PublicEntity condition) throws CRUDException{
		String pk_stores = commonMISBOHService.getPkStore(session).getPk_store();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		Page page = new Page();
		page.setPageSize(Integer.MAX_VALUE);
		
		map.put("actTyp", baseRecordService.findAllActTyp());
		map.put("payment", commonMISBOHService.findAllPayment());
		
		Actm a = new Actm();
		a.setPk_store(pk_stores);
		a.setDstartdate(condition.getBdat());//开始日期
		a.setDenddate(condition.getEdat());//结束日期
		map.put("actm", baseRecordService.findAllActmByZheK(a));			//根据门店及日期查询这段时间产生营业数据的活动
		map.put("actmtyp", baseRecordService.findAllActTypByActm(a));		//根据活动查询所有大类
		map.put("actmtypmin", baseRecordService.findAllActTypMinByActm(a));	//查询活动的所有小类
		map.put("paymode", baseRecordService.findPaymodeByActm(a));			//查询支付方式
		return map;
	}
	
	/**
	 * 描述：菜品销售沽清表 西贝
	 * @author wangkai
	 * 创建时间：2016-2-19
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("listItemSaleGQ")
	@ResponseBody
	public ReportObject<Map<String,Object>> listItemSaleGQ(PublicEntity condition,HttpSession session) throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return proReportService.listItemSaleGQ(condition);
	}
	
	/**
	 * 描述:菜品销售明细报表——江边城外
	 * 作者:马振
	 * 时间:2016年7月11日下午4:13:20
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryJbcwPoscybb")
	@ResponseBody
	public Object queryJbcwPoscybb(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return proReportService.queryJbcwPoscybb(condition);
	}
	
	/**
	 * 描述:查询附加项信息
	 * 作者:马振
	 * 时间:2016年7月28日下午2:27:26
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findHeaderForZdmx")
	@ResponseBody
	public Object findHeaderForZdmx() throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("redefine",proReportService.findAllRedefine());
		return map;
	}
	
	/**
	 * 描述:鱼口味统计报表——江边城外
	 * 作者:马振
	 * 时间:2016年7月28日下午2:38:38
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryFishFlavorStatistical")
	@ResponseBody
	public Object queryFishFlavorStatistical(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return proReportService.queryFishFlavorStatistical(condition);
	}

	/**
	 * 描述:翻台统计表
	 * 作者:马振
	 * 时间:2016年7月28日下午3:09:38
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryTableStatistics")
	@ResponseBody
	public Object queryTableStatistics(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return proReportService.queryTableStatistics(condition);
	}
	
	/**
	 * 描述:结算方式统计报表
	 * 作者:马振
	 * 时间:2016年7月29日上午11:34:00
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryStatementAccounts")
	@ResponseBody
	public Object queryStatementAccounts(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return proReportService.queryStatementAccounts(condition);
	}
	
	/**
	 * 描述:结算方式统计明细表
	 * 作者:马振
	 * 时间:2016年7月29日上午11:35:43
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryStatementAccountsDetail")
	@ResponseBody
	public Object queryStatementAccountsDetail(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return proReportService.queryStatementAccountsDetail(condition);
	}
	
	/**
	 * 描述:折扣分析报表查询
	 * 作者:马振
	 * 时间:2016年7月29日下午3:37:36
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryDiscountAnalysis")
	@ResponseBody
	public Object queryDiscountAnalysis(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return proReportService.queryDiscountAnalysis(condition);
	}
}