package com.choice.misboh.web.controller.publish;

import java.io.File;
import java.io.IOException;
import java.nio.channels.OverlappingFileLockException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.commonutil.publish.PublishUtil;
import com.choice.misboh.domain.store.Store;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.misboh.service.publish.PublishService;
import com.choice.orientationSys.util.Util;

/**
 * 描述：数据下发
 * @author 马振
 * 创建时间：2015-8-25 下午1:39:56
 */
@Controller
@RequestMapping(value = "publish")
public class PublishController {
	
	@Autowired
	private PublishService publishService;

	@Autowired
	private CommonMISBOHService commonMISBOHService;
	
	@Autowired
	private LogsMapper logsMapper;
	
	/**
	 * 描述：数据下发
	 * @author 马振
	 * 创建时间：2015-8-25 下午1:42:57
	 * @param scmElement
	 * @param bohElement
	 * @param crmElement
	 * @param store
	 * @param request
	 * @param imgFlag
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @throws IOException
	 */
	@RequestMapping(value = "/toPublish")
	@ResponseBody
	public String toPublish(String scmElement,String bohElement, HttpServletRequest request,String imgFlag,HttpSession session) throws CRUDException, IOException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String result = "false";
		Store store = commonMISBOHService.getPkStore(session);
		
		//为文件加锁，防止同时下发
		try {
			result = PublishUtil.lockOperate(new File(request.getSession().getServletContext().getRealPath("/") + "/js"));
		} catch (OverlappingFileLockException e1) {
			if(result == "no"){
				System.out.println("文件加锁异常！");
				e1.printStackTrace();
				return "文件加锁异常！";
			}
		}
		if (result == "no") {
			System.out.println("文件加锁异常,其他人员正在发布数据！");
			return "不支持多人同时发布数据，其他人员正在发布数据，请稍候发布数据！";
		}
		
		//检测删除的文件是否被占用
		try {
			String str = PublishUtil.checkFileStatus(store);
			if(!"true".equals(str)){
				PublishUtil.unlockOperate();
				return str;
			}
		}catch(Exception e){
			PublishUtil.unlockOperate();
			e.printStackTrace();
		}
		
		//删除Publish下的所有文件，判断是否需要下发图片
		try {
			String reStr = publishService.operatorFilr(store, request, imgFlag);
			if(!"true".equals(reStr)){
				PublishUtil.unlockOperate();
				return reStr;
			}
		}catch(Exception e){
			PublishUtil.unlockOperate();
			e.printStackTrace();
		}
		
		//下发数据
		if(ValueUtil.IsNotEmpty(bohElement)){
			DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			result = publishService.toPublish(bohElement,store,request,DataSourceInstances.SCM);
			if(!"ok".equals(result)){
				return result;
			}
		}
		
		//打包生成的数据
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		try {
			result = PublishUtil.copyToStore(store);
		} catch (IOException e) {
			PublishUtil.unlockOperate();
			result = "生成压缩包失败！";
		}
		
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
		String storeCode = store.getVcode();
		
		try{
			Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(), new Date(),
					ProgramConstants.INSERT, "数据发布，门店编码【" + storeCode.length() + "】",
					session.getAttribute("ip").toString(), ProgramConstants.SCM);
			logsMapper.addLogs(logd);
		}catch(Exception e){
			PublishUtil.unlockOperate();
			result = "日志记录失败！";
		}
		
		PublishUtil.unlockOperate();
		return result;
	}
}