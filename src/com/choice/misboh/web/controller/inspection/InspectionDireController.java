package com.choice.misboh.web.controller.inspection;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ProgramConstants;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.commonutil.MailUtils;
import com.choice.misboh.constants.inspection.InspectionConstants;
import com.choice.misboh.domain.inspection.ArrivaldMis;
import com.choice.misboh.domain.inspection.ArrivalmMis;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.misboh.service.inspection.InspectionMisService;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.ana.DisList;
import com.choice.scm.service.PositnService;
import com.choice.scm.util.ReadReportUrl;

/***
 * 直配验货Controller
 * @author wjf
 *
 */
@Controller
@RequestMapping(value = "inspectionDire")
public class InspectionDireController {

	@Autowired
	private InspectionMisService inspectionService;
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	@Autowired
	private PositnService positnService;

	private final static int PAGE_SIZE = 25;// ajax每次加载的数据条数
	
	/******************************************************** 直配验收start *************************************************/

	/***
	 * 直配验货主界面
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param dis
	 * @param action
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/tableCheck.do")
	public ModelAndView findAllCheck(ModelMap modelMap, HttpSession session, Page page, Dis dis, String action,ArrivalmMis arrivalmMis,String isDireInsByBill) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		if(null == isDireInsByBill)
			isDireInsByBill = DDT.isDireInsByBill;
		if("Y".equals(isDireInsByBill)){//验货根据单据
			if(null == arrivalmMis.getBdate()){
				arrivalmMis.setBdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
				arrivalmMis.setEdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
				Positn positn = (Positn)session.getAttribute("accountPositn");
				arrivalmMis.setPk_org(positn.getCode());
			}
			List<ArrivalmMis> amList = inspectionService.findArrivalmList(arrivalmMis,page);
			modelMap.put("arrivalmList", amList);
			modelMap.put("arrivalm", arrivalmMis);
			modelMap.put("pageobj", page);
			return new ModelAndView(InspectionConstants.LIST_ARRIVALM,modelMap);
		}
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			dis.setFirmCode(thePositn.getCode());
		}
		dis.setInout(DDT.DIRE);
		if (null != action && !"".equals(action)) {
			dis.setBdat(new Date());
			dis.setEdat(new Date());
			modelMap.put("action", "init");
			dis.setYndo("NO");
		} else {
			String acct=session.getAttribute("ChoiceAcct").toString();//得到帐套信息 wjf
			dis.setAcct(acct);
			int totalCount = inspectionService.findAllDisCount(dis);
			dis.setStartNum(0);
			dis.setEndNum(PAGE_SIZE);
			dis.setPageSize(PAGE_SIZE);
			modelMap.put("totalCount", totalCount);
			if (totalCount <= PAGE_SIZE) {
				modelMap.put("currState", 1);
			} else {
				modelMap.put("currState", PAGE_SIZE * 1.0 / totalCount);
			}
			List<Dis> disList = inspectionService.findAllDisPage(dis);
			modelMap.put("disList", disList);
			//判断当前条件下是否有档口报货来的验货单
			StringBuffer ids = new StringBuffer();
			for(Dis d : disList){
				ids.append(d.getChkstoNo()+",");
			}
			ids.append("0");
			int count = inspectionService.findDeptChkstom(ids.toString());
			modelMap.put("is_dept", count > 0?"Y":"N");
			dis.setDeliverDes(dis.getDeliverDes() != null ? URLDecoder.decode(dis.getDeliverDes(), "UTF-8") : null);
		}
		modelMap.put("dis", dis);
		modelMap.put("pageSize", PAGE_SIZE);
		modelMap.put("disJson", JSONObject.fromObject(dis));
		modelMap.put("isNotShowSp_price", DDT.isNotShowSp_price);//是否报货验货显示价格列金色三麦用
		return new ModelAndView(InspectionConstants.MIS_TABLE_CHECK, modelMap);
	}

	/***
	 * 直配验货主界面  ajax 延迟加载
	 * @param session
	 * @param dis
	 * @param currPage
	 * @param totalCount
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listAjax")
	@ResponseBody
	public Object listAjax(HttpSession session, Dis dis, String currPage, String totalCount) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		int intCurrPage = Integer.valueOf(currPage);
		int intTotalCount = Integer.valueOf(totalCount);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		dis.setStartNum(intCurrPage * PAGE_SIZE);
		dis.setEndNum((intCurrPage + 1) * PAGE_SIZE);
		dis.setPageSize(PAGE_SIZE);
		List<Dis> disList = inspectionService.findAllDisPage(dis);
		modelMap.put("disesList1", disList);
		//判断当前条件下是否有档口报货来的验货单
		StringBuffer ids = new StringBuffer();
		for(Dis d : disList){
			ids.append(d.getChkstoNo()+",");
		}
		ids.append("0");
		int count = inspectionService.findDeptChkstom(ids.toString());
		modelMap.put("is_dept", count > 0?"Y":"N");
		modelMap.put("currState", (intCurrPage + 1) * PAGE_SIZE * 1.0 / intTotalCount);
		modelMap.put("currPage", currPage);
		if ((intCurrPage + 1) * PAGE_SIZE >= intTotalCount) {
			modelMap.put("currState", 1);
			modelMap.put("over", "over");
		}
		return JSONObject.fromObject(modelMap).toString();
	}
	
	/***
	 * 直配验货修改数量
	 * @param modelMap
	 * @param session
	 * @param disList
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateDis")
	@ResponseBody
	public Object updateDis(ModelMap modelMap, HttpSession session, DisList disList) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,
				"MISBOH修改直配验货单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		return inspectionService.updateDis(disList);
	}
	
	/**
	 * 导出
	 * @param response
	 * @param request
	 * @param session
	 * @param dis
	 * @param ind1
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/exportDis")
	@ResponseBody
	public boolean exportDis(HttpServletResponse response,HttpServletRequest request,HttpSession session, Dis dis, String ind1) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dis.setAcct(session.getAttribute("ChoiceAcct").toString());
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			dis.setFirmCode(thePositn.getCode());
			dis.setFirmDes(thePositn.getDes());
		}
		dis.setDanjuTyp("receipt");
		dis.setInout(DDT.DIRE);
//		dis.setChkin("N");
//		dis.setChkout("N");
//		dis.setSta("Y");//已审核的
		if (null != ind1 && !"".equals(ind1)) {
			dis.setInd(dis.getMaded());
			dis.setMaded(null);
		}
		// 根据关键字查询
		List<Dis> disList = inspectionService.findAllDisForReport(dis);
		String fileName = "直配验货单";
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
		}else{
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
		}
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");
		return inspectionService.exportDis(response.getOutputStream(),disList,dis);
	}
	
	/***
	 * 直配验货打印验货单
	 * @param modelMap
	 * @param session
	 * @param dis
	 * @param type
	 * @param ind1
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/printReceipt")
	public ModelAndView printReceipt(ModelMap modelMap, HttpSession session, Dis dis, String type, String ind1) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		String acct=session.getAttribute("ChoiceAcct").toString();//得到帐套信息 wjf
		dis.setAcct(acct);
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			dis.setFirmCode(thePositn.getCode());
		}
		// 接收用户输入的查询参数
		HashMap<String, Object> param = new HashMap<String, Object>();
		dis.setDanjuTyp("receipt");
		param.put("danjuTyp", "receipt");
		dis.setInout(DDT.DIRE);
//		dis.setChkin("N");
//		dis.setChkout("N");
//		dis.setSta("Y");//已审核的
//		param.put("chkin", "N");
//		param.put("chkout", "N");
		param.put("inout", DDT.DIRE);
		param.put("bdat", DateFormat.getStringByDate(dis.getBdat(), "yyyy-MM-dd"));
		param.put("edat", DateFormat.getStringByDate(dis.getEdat(), "yyyy-MM-dd"));
		param.put("deliverCode", dis.getDeliverCode());
		param.put("typCode", dis.getTypCode());
		param.put("firmCode", dis.getFirmCode());
		param.put("sp_code", dis.getSp_code());
		param.put("sp_init", dis.getSp_init());
		param.put("yndo", dis.getYndo());
		if (null != ind1 && !"".equals(ind1)) {
			dis.setInd(dis.getMaded());
			param.put("ind", dis.getMaded());
			dis.setMaded(null);
		}
		modelMap.put("actionMap", param);// 回调参数
		modelMap.put("action", "/inspectionDire/printReceipt.do");// 传入回调路径
		// 根据关键字查询
		List<Dis> disList = inspectionService.findAllDisForReport(dis);
		HashMap<Object, Object> parameters = new HashMap<Object, Object>();
		String report_name = new String("验收单");
		parameters.put("report_name", report_name);
		parameters.put("madeby", session.getAttribute("accountNames").toString());
		modelMap.put("List", disList);
		modelMap.put("parameters", parameters);// 报表文件用的输入参数
		Map<String, String> rs = ReadReportUrl.redReportUrl(type, InspectionConstants.REPORT_RECEIPT_URL, InspectionConstants.REPORT_RECEIPT_URL);// 判断跳转路径
		modelMap.put("reportUrl", rs.get("reportUrl"));// ireport文件地址
		return new ModelAndView(rs.get("url"), modelMap);
	}

	/***
	 * 验收入库主界面
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param dis
	 * @param action
	 * @param ind1
	 * @param maded1
	 * @param ysrkStr
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/tableCheckin.do")
	public ModelAndView tableCheckin(ModelMap modelMap, HttpSession session, Page page, Dis dis, String action, String ind1, Date maded1, String ysrkStr) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			String positnCode = thePositn.getCode();
			String positnDes = thePositn.getDes();
			dis.setFirmCode(positnCode);
			dis.setFirmDes(positnDes);
			Positn positn = positnService.findPositnByCode(thePositn);
			modelMap.put("ynUseDept", positn.getYnUseDept());//页面判断用
		}
		dis.setInout(DDT.DIRE);
		dis.setChkin("N");
		dis.setYndo("NO");
//		dis.setCheckinSelect("checkinSelect");//可以查验货数量为0 的 只是不生成入库单wjf
		String acct=session.getAttribute("ChoiceAcct").toString();//得到帐套信息 wjf
		dis.setAcct(acct);
		List<Dis> disList = inspectionService.findAllDis(dis, page);
		if (null != maded1) {
			modelMap.put("maded1", maded1);
		} else {
			modelMap.put("maded1", new Date());
		}
		modelMap.put("disList", disList);
		modelMap.put("chkMsg", "0");
		modelMap.put("ind1", ind1);
		modelMap.put("dis", dis);
		modelMap.put("pageobj", page);
		modelMap.put("isNotShowSp_price", DDT.isNotShowSp_price);//是否报货验货显示价格列金色三麦用
		modelMap.put("isChkinPriceNot0", DDT.isChkinPriceNot0);//是否验货，入库价格不能为0 西贝用
		return new ModelAndView(InspectionConstants.MIS_TABLE_CHECKIN_NEW,modelMap);
	}
	
	/***
	 * 直配验货生成入库单   不启用档口下的验货
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param dis
	 * @param ind1
	 * @param maded1
	 * @param ysrkStr
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveYsrkChkinm.do")
	public ModelAndView saveYsrkChkinm(ModelMap modelMap,HttpSession session,Page page,Dis dis,String ind1,Date maded1,String ysrkStr) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,
				"MISBOH直配验货,生成门店入库单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		String acct=session.getAttribute("ChoiceAcct").toString();//得到帐套信息 wjf
		dis.setAcct(acct);
		dis.setDanjuTyp("receipt");//根据供应商，物资进行排序
		List<Dis> disList=inspectionService.findAllDis(dis,page);//如果page为空，则不分页  查询所有的2014.10.24wjf
		//报货单转入库单方法
		String  accountName=session.getAttribute("accountName").toString();//当前用户
		//接收方法执行后的结果状态，1：执行成功；2：页面查询结果集为空，不能进行入库；
		int str=0;
		if(disList.size()!=0){
			inspectionService.saveYsrkChkinm(disList,acct,dis,dis.getFirmCode(),maded1!=null?maded1:new Date(),accountName);
			str=1;//不为空，成功
		}else{
			str=2;//为空，不能进行入库
		}
		modelMap.put("maded1", maded1);
		modelMap.put("chkMsg", str);
		modelMap.put("ind1", ind1);
		modelMap.put("dis", dis);
		modelMap.put("pageobj", page);
		modelMap.put("isNotShowSp_price", DDT.isNotShowSp_price);//是否报货验货显示价格列金色三麦用
		modelMap.put("isChkinPriceNot0", DDT.isChkinPriceNot0);//是否验货，入库价格不能为0 西贝用
		return new ModelAndView(InspectionConstants.MIS_TABLE_CHECKIN_NEW,modelMap);
	}

	/***
	 * 直配验货 入库单打印
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param dis
	 * @param type
	 * @param ind1
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/viewYsrkChkstom")
	public ModelAndView viewYsrkChkstom(ModelMap modelMap, HttpSession session,
			Page page, Dis dis, String type, String ind1) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		dis.setAcct(session.getAttribute("ChoiceAcct").toString());
		// 接收用户输入的查询参数
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("bdat", DateFormat.getStringByDate(dis.getBdat(), "yyyy-MM-dd"));
		param.put("edat", DateFormat.getStringByDate(dis.getEdat(), "yyyy-MM-dd"));
		param.put("positnCode", dis.getPositnCode());
		param.put("deliverCode", dis.getDeliverCode());
		param.put("typCode", dis.getTypCode());
		param.put("firmCode", dis.getFirmCode());
		param.put("memo1", dis.getMemo1());
		param.put("chkin", "N");
		param.put("inout", DDT.DIRE);
		if (null != ind1 && !"".equals(ind1)) {
			dis.setInd(dis.getMaded());
			param.put("ind", dis.getMaded());
			dis.setMaded(null);
		}
		dis.setInout(DDT.DIRE);
		dis.setChkin("N");
		dis.setCheckinSelect("checkinSelect");
		modelMap.put("actionMap", param);// 回调参数
		modelMap.put("action", "/inspectionDire/viewYsrkChkstom.do");// 传入回调路径
		// 根据关键字查询
		List<Dis> disList = inspectionService.findAllDisForReport(dis);
		HashMap<Object, Object> parameters = new HashMap<Object, Object>();
		String report_name = new String("物资入库明细表");
		parameters.put("report_name", report_name);
		parameters.put("report_date", DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		String accountName = session.getAttribute("accountName").toString();
		parameters.put("madeby", accountName);
		modelMap.put("List", disList);
		modelMap.put("parameters", parameters);// 报表文件用到的输入参数
		Map<String, String> rs = ReadReportUrl.redReportUrl(type, InspectionConstants.REPORT_YSRK_URL, InspectionConstants.REPORT_YSRK_URL);// 判断跳转路径
		modelMap.put("reportUrl", rs.get("reportUrl"));// ireport文件地址
		return new ModelAndView(rs.get("url"), modelMap);
	}
	
	/***
	 * 进入档口直发页面  档口报货来的
	 * @param modelMap
	 * @param dis
	 * @param session
	 * @param iszp
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toChkinmDept")
	public ModelAndView toChkinmDept(ModelMap modelMap, Dis dis, HttpSession session,String iszp) throws Exception {  
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<Dis> disList = inspectionService.findSupplyacctList(dis,iszp);
		modelMap.put("supplyacctList", disList);
		modelMap.put("isNotShowSp_price", DDT.isNotShowSp_price);//是否报货验货显示价格列金色三麦用
		if("Y".equals(iszp)){//直配验货查询
			return new ModelAndView(InspectionConstants.SAVE_CHKINM_DEPT_ZP, modelMap);
		}
		modelMap.put("dis", dis);
		return new ModelAndView(InspectionConstants.SAVE_CHKINM_DEPT, modelMap);
	}
	
	/***
	 * 生成档口直发单(直配验货)
	 * @param modelMap
	 * @param chkinm
	 * @param ids
	 * @param sp_ids
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveChkinmDept_zp")
	@ResponseBody
	public String saveChkinmDept_zp(ModelMap modelMap,Chkinm chkinm, String ids, String sp_ids,HttpSession session) throws Exception {  
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct=session.getAttribute("ChoiceAcct").toString();
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
				"MISBOH直配档口验货,生成档口直发单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		try{
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			if (null != thePositn) {
				chkinm.setPositn(thePositn);
			}
			chkinm.setAcct(acct);
			chkinm.setMadeby(session.getAttribute("accountName").toString());
			return inspectionService.saveChkinmDept_zp(chkinm,ids,sp_ids)+"";
		}catch(Exception e){
			return e.getLocalizedMessage();
		}
	}
	
	/***
	 * 进入分档口验货(直配验货)
	 * @param modelMap
	 * @param dis
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toChkinmGroupZP")
	public ModelAndView toChkinmGroupZP(ModelMap modelMap, Dis dis, HttpSession session) throws Exception {  
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct=session.getAttribute("ChoiceAcct").toString();//得到帐套信息 wjf
		dis.setAcct(acct);
		dis.setDanjuTyp("receipt");//根据供应商，物资进行排序
		List<Dis> disList=inspectionService.findAllDisForReport(dis);
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			String positnCode = thePositn.getCode();
			Positn positn = new Positn();
			positn.setUcode(positnCode);
			List<Positn> deptList = positnService.findPositnSuperNOPage(positn);
			List<Dis> list = new ArrayList<Dis>();
			for(Dis d : disList){
				list.add(d);
				for(Positn p : deptList){
					Dis s = new Dis();
					s.setFirmCode(p.getCode());
					s.setFirmDes(p.getDes());
					s.setChkstoNo(d.getChkstoNo());
					s.setUnitper(d.getUnitper());
					s.setDued(d.getDued());
					s.setPcno(d.getPcno());
					s.setDeliverCode(d.getDeliverCode());
					s.setAmountin(0.0);
					s.setAmount1in(0.0);
					s.setId(d.getId());
					list.add(s);
				}
			}
			modelMap.put("disList", list);
		}
		modelMap.put("ids", dis.getInCondition());
		modelMap.put("isNotShowSp_price", DDT.isNotShowSp_price);//是否报货验货显示价格列金色三麦用
		return new ModelAndView(InspectionConstants.SAVE_CHKINM_GROUP_ZP, modelMap);
	}
	
	/***
	 * 分档口验货，门店的就生成门店入库单，档口的生成档口直发单(直配验货)
	 * @param modelMap
	 * @param chkinm
	 * @param ids
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveChkinmGroupZP")
	@ResponseBody
	public String saveChkinmGroupZP(ModelMap modelMap,Chkinm chkinm, String ids, HttpSession session) throws Exception {  
		String acct=session.getAttribute("ChoiceAcct").toString();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
				"MISBO直配分档口验货",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		try{
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			if (null != thePositn) {
				chkinm.setPositn(thePositn);
			}
			chkinm.setAcct(acct);
			chkinm.setMadeby(session.getAttribute("accountName").toString());
			return inspectionService.saveChkinmGroupZP(chkinm,ids)+"";
		}catch(Exception e){
			return e.getLocalizedMessage();
		}
	}

	/******************************************************** 直配验收end ********************************************************/
	
	/*********************************************九毛九按单据验货start***************************************************/
	
	/***
	 * 查询九毛九直配到货单
	 * @param modelMap
	 * @param session
	 * @param arrivalmMis
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listArrivalm")
	public ModelAndView listArrivalm(ModelMap modelMap, HttpSession session, ArrivalmMis arrivalmMis,Page page) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"MISBOH查询九毛九直配到货单主表",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		if(null == arrivalmMis.getBdate()){
			arrivalmMis.setBdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
			arrivalmMis.setEdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
			Positn positn = (Positn)session.getAttribute("accountPositn");
			arrivalmMis.setIstate(0);//默认查未审核的
			arrivalmMis.setPk_org(positn.getCode());
		}
		List<ArrivalmMis> amList = inspectionService.findArrivalmList(arrivalmMis,page);
		modelMap.put("arrivalmList", amList);
		modelMap.put("arrivalm", arrivalmMis);
		modelMap.put("pageobj", page);
		return new ModelAndView(InspectionConstants.LIST_ARRIVALM,modelMap);
	}
	
	/***
	 * 查询九毛九直配到货单
	 * @param modelMap
	 * @param session
	 * @param arrivalmMis
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findArrivald")
	public ModelAndView findArrivald(ModelMap modelMap, HttpSession session, ArrivalmMis arrivalmMis) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"MISBOH查询九毛九直配到货单详情",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
//		arrivalmMis.setIstate(0);//未验货
		List<ArrivalmMis> amList = inspectionService.findArrivalmList(arrivalmMis);
		List<ArrivaldMis> adList = new ArrayList<ArrivaldMis>();
		if(amList.size() != 0){
			arrivalmMis = amList.get(0);
			adList = inspectionService.findArrivaldList(arrivalmMis);
			if(null == arrivalmMis.getMaded())//null说明没验货的,入库日期默认等于到货日期
				arrivalmMis.setMaded(DateFormat.getDateByString(arrivalmMis.getDarrbilldate(), "yyyy-MM-dd"));
		}
		modelMap.put("arrivalm", arrivalmMis);
		modelMap.put("arrivaldList", adList);
		return new ModelAndView(InspectionConstants.LIST_ARRIVALD,modelMap);
	}
	
	/***
	 * 九毛九直配验货修改数量
	 * @param modelMap
	 * @param session
	 * @param arrivalmMis
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateArrivald")
	@ResponseBody
	public String updateArrivald(ModelMap modelMap, HttpSession session,ArrivalmMis arrivalmMis) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,
				"MISBOH修改九毛九直配到货单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		return inspectionService.updateArrivald(arrivalmMis);
	}
	
	/***
	 * 九毛九直配验货生成入库单
	 * @param modelMap
	 * @param session
	 * @param arrivalmMis
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkArrivalm.do")
	public ModelAndView checkArrivalm(ModelMap modelMap,HttpSession session,ArrivalmMis arrivalmMis) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,
				"MISBOH九毛九直配验货,生成门店入库单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		arrivalmMis.setIstate(0);//未验货
		List<ArrivalmMis> amList = inspectionService.findArrivalmList(arrivalmMis);
		List<ArrivaldMis> adList = new ArrayList<ArrivaldMis>();
		if(amList.size() != 0){
			Date maded = arrivalmMis.getMaded() == null ? new Date() : arrivalmMis.getMaded();
			String acct = session.getAttribute("ChoiceAcct").toString();
			String accountName = session.getAttribute("accountName").toString();//当前用户
			arrivalmMis = amList.get(0);
			arrivalmMis.setMaded(maded);
			arrivalmMis.setAcct(acct);
			arrivalmMis.setMadeby(accountName);
			adList = inspectionService.findArrivaldList(arrivalmMis);
			String pr = inspectionService.checkArrivalm(arrivalmMis,adList);
			arrivalmMis.setPr(pr);
		}else{
			arrivalmMis.setPr("0");//数据为空 已经验货
		}
		modelMap.put("arrivalm", arrivalmMis);
		modelMap.put("arrivaldList", adList);
		return new ModelAndView(InspectionConstants.LIST_ARRIVALD,modelMap);
	}
	
	/***
	 * 进入发送邮件界面
	 * @param modelMap
	 * @param session
	 * @param arrivalmMis
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toSendEmail")
	public ModelAndView toSendEmail(ModelMap modelMap, HttpSession session, ArrivalmMis arrivalmMis, int flag) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"MISBOH九毛九直配进入发送邮件界面",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		List<ArrivalmMis> amList = inspectionService.findArrivalmList(arrivalmMis);
		List<ArrivaldMis> adList = new ArrayList<ArrivaldMis>();
		if(amList.size() != 0){
			arrivalmMis = amList.get(0);
			adList = inspectionService.findArrivaldList(arrivalmMis);
		}
		modelMap.put("arrivalm", arrivalmMis);
		modelMap.put("arrivaldList", adList);
		modelMap.put("flag", flag);
		return new ModelAndView(InspectionConstants.SEND_EMAIL,modelMap);
	}
	
	/***
	 * 进入发送邮件界面
	 * @param modelMap
	 * @param session
	 * @param arrivalmMis
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/sendEmail")
	public ModelAndView sendEmail(ModelMap modelMap, HttpSession session, HttpServletRequest request, HttpServletResponse response, ArrivalmMis arrivalmMis) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"MISBOH九毛九直配发送邮件",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		List<ArrivaldMis> adList = inspectionService.findArrivaldList(arrivalmMis);
		response.setContentType("application/msexcel; charset=UTF-8");
		File file = new File(request.getSession().getServletContext().getRealPath("/upload/"+arrivalmMis.getTitle()+".xls"));
		if(!file.exists()){
			file.createNewFile();
		}
		OutputStream out = new FileOutputStream(file);
		inspectionService.exportArrivaldList(out, arrivalmMis, adList);
		out.flush();
		out.close();
		String path = file.getAbsolutePath();
		if(MailUtils.sendMail(arrivalmMis.getTitle(), arrivalmMis.getContent(), path, arrivalmMis.getMail()))
			return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
		else
			return new ModelAndView(StringConstant.ERROR_DONE, modelMap);
	}
	
	/***
	 * 九毛九直配验货修改数量判断
	 * @param modelMap
	 * @param session
	 * @param arrivalmMis
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkState")
	@ResponseBody
	public String checkState(ModelMap modelMap, HttpSession session,ArrivalmMis arrivalmMis) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		List<ArrivalmMis> amList = inspectionService.findArrivalmList(arrivalmMis);
		if(amList.size() != 0){
			arrivalmMis = amList.get(0);
		}
		return arrivalmMis.getIstate()+"";
	}
	
	/******************************************************** 手机报货 到货验收  2015.2.25wjf *************************************************/
	
	/**
	 * 得到所有供应商列表
	 * 查分店供应商真正的供应商
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getDeliverListWAP")
	@ResponseBody
	public Object getDeliverListWAP(Deliver deliver,Page page,String jsonpcallback) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		deliver.setGysqx("2");//供应商权限:走分店供应商
		if("all".equals(deliver.getMemo()))
			page = null;
		//关键字查询
		map.put("deliverList", commonMISBOHService.findDeliver(deliver, page));
		map.put("page", page);
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/**
	 * 得到供应商验货列表
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/tableCheckinWAP")
	@ResponseBody
	public Object tableCheckinWAP(Dis dis,Page page,String jsonpcallback) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		dis.setInout(DDT.DIRE);
		dis.setChkin("N");
		dis.setYndo("NO");
		List<Dis> disList = inspectionService.findAllDis(dis,page);
		map.put("disList", disList);
		map.put("page", page);
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/**
	 * 修改验货数量
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateDisWAP")
	@ResponseBody
	public Object updateDisWAP(DisList disList) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String result= inspectionService.updateDis(disList);
		return  result;
	}
	
	/**
	 * 验收生成入库单
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveYsrkChkinmWAP")
	@ResponseBody
	public Object saveYsrkChkinmWAP(Dis dis,String accountName,Date maded1,String jsonpcallback) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		dis.setDanjuTyp("receipt");//根据供应商，物资进行排序
		List<Dis> disList = inspectionService.findAllDisForReport(dis);
		if(disList.size()!=0){
			inspectionService.saveYsrkChkinm(disList,dis.getAcct(),dis,dis.getFirmCode(),maded1!=null?maded1:new Date(),accountName);
			map.put("pr", 1);
		}else{
			map.put("pr", 2);//为空，不能进行入库
		}
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
}
