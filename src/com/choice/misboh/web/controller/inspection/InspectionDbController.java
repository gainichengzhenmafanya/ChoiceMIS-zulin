package com.choice.misboh.web.controller.inspection;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ProgramConstants;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.constants.inspection.InspectionConstants;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.misboh.service.inspection.InspectionMisService;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.PositnService;
import com.choice.scm.util.ReadReportUrl;

/***
 * 调拨验货Controller
 * @author wjf
 *
 */
@Controller
@RequestMapping(value = "inspectionDb")
public class InspectionDbController {

	@Autowired
	private InspectionMisService inspectionService;
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	@Autowired
	private PositnService positnService;

	/******************************************************** 门店调拨验收start *************************************************/
	/***
	 * 门店调拨验收主页面
	 * @param modelMap
	 * @param session
	 * @param mold
	 * @param sa
	 * @param page
	 * @param check
	 * @param bdate
	 * @param edate
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/dispatchDb.do")
	public ModelAndView diapatchDbList(ModelMap modelMap, HttpSession session,
			String mold, SupplyAcct sa, Page page, String check, Date bdate,
			Date edate,Date maded1) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 点击查询的时候符合条件的配送中心到货信息。
		if ("select".equals(mold)) {
			HashMap<String, Object> disMap = new HashMap<String, Object>();
			disMap.put("positn", sa.getPositn());
			disMap.put("bdate", bdate);
			disMap.put("edate", edate);
			disMap.put("check", check);
			disMap.put("db", "db");//查调拨类型
			disMap.put("acct", session.getAttribute("ChoiceAcct").toString());
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			if (null != thePositn) {
				String positnCode = thePositn.getCode();
				disMap.put("firm", positnCode);
				modelMap.put("firmCode", positnCode);
				modelMap.put("firmDes", thePositn.getDes());
				Positn positn = positnService.findPositnByCode(thePositn);
				modelMap.put("ynUseDept", positn.getYnUseDept());//页面判断用
			}
			List<SupplyAcct> disList = inspectionService.selectByKeyDb(disMap, page);
			modelMap.put("dis", sa);
			modelMap.put("disList", disList);
			sa.setPositndes(sa.getPositndes() != null ? URLDecoder.decode(sa.getPositndes(), "UTF-8") : null);
		}
		// 初次加载页面传值
		if (bdate == null || edate == null) {
			bdate = new Date();
			edate = new Date();
			maded1 = new Date();
			check = "2";//默认未审核
		}
		modelMap.put("bdate", bdate);
		modelMap.put("edate", edate);
		modelMap.put("maded1", maded1);
		modelMap.put("check", check);
		modelMap.put("pageobj", page);
		modelMap.put("isNotShowSp_price", DDT.isNotShowSp_price);//是否报货验货显示价格列金色三麦用
		return new ModelAndView(InspectionConstants.LIST_DISPATCH_DB, modelMap);
	}

	/***
	 * 调拨验收
	 * @param modelMap
	 * @param supplyAcct
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateAcctDb")
	@ResponseBody
	public String updateAcctDb(SupplyAcct supplyAcct, String ids, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,
				"MISBOH修改调拨验货单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		String acct = session.getAttribute("ChoiceAcct").toString();
		HashMap<String, Object> disMap = new HashMap<String, Object>();
		disMap.put("listids", ids);
		disMap.put("acct", acct);
		disMap.put("check", 2);
		disMap.put("db", "db");//查调拨类型
		List<SupplyAcct> disList = inspectionService.selectByKeyDb(disMap);//查未验货的
		if(null != supplyAcct && null != disList && supplyAcct.getSupplyAcctList().size() == disList.size()){
			return inspectionService.updateAcct(supplyAcct, acct);
		}
		return "-1";
	}

	/***
	 * 调拨验货(不分档口情况下)
	 * @param modelMap
	 * @param ids
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkDb")
	public ModelAndView checkDb(ModelMap modelMap, String ids, Date maded, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,
				"MISBOH审核调拨验货单,生成门店入库单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		String acct = session.getAttribute("ChoiceAcct").toString();
		String madeby = session.getAttribute("accountName").toString();
		try{
			//1.先更新状态
			HashMap<String, Object> disMap=new HashMap<String, Object>();
			disMap.put("listids", ids);
			disMap.put("acct", acct);
			disMap.put("check", 2);
			disMap.put("db", "db");//查调拨类型
			//生成门店的入库单
			disMap.put("ruku", "ruku");//按调出仓位，物资进行排序
			List<SupplyAcct> disList = inspectionService.selectByKeyDb(disMap);
			if(disList.size() != ids.split(",").length){
				return new ModelAndView(StringConstant.ERROR_DONE, modelMap);
			}
			inspectionService.check(ids, null, madeby, acct);
			inspectionService.checkDb(disList, ids, madeby, acct, maded);
			modelMap.put("check", "2");
			return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
		}catch(Exception e){//如果有异常就回滚
			inspectionService.uncheck(ids, null, acct);
			return new ModelAndView(StringConstant.ERROR_DONE, modelMap);
		}
	}
	
	/***
	 * 进入分档口验货
	 * @param modelMap
	 * @param ids
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toChkinmGroupDb")
	public ModelAndView toChkinmGroupDb(ModelMap modelMap, String ids, HttpSession session) throws Exception {  
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		HashMap<String, Object> disMap=new HashMap<String, Object>();
		disMap.put("listids", ids);
		disMap.put("db", "db");
		disMap.put("ruku", "ruku");//按调出仓位，物资进行排序
		List<SupplyAcct> disList = inspectionService.selectByKeyDb(disMap);
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			String positnCode = thePositn.getCode();
			String positnDes = thePositn.getDes();
			Positn positn = new Positn();
			positn.setUcode(positnCode);
			List<Positn> deptList = positnService.findPositnSuperNOPage(positn);
			List<SupplyAcct> list = new ArrayList<SupplyAcct>();
			for(SupplyAcct sa : disList){
				Positn po = new Positn();
				po.setCode(sa.getPositn());
				po = positnService.findPositnByCode(po);
				if(null != po && "1207".equals(po.getPcode())){//如果是档口调拨来的，那供应商应该是档口所在的分店
					sa.setDelivercode(po.getUcode());
				}else{
					sa.setDelivercode(sa.getPositn());//供应商
				}
				sa.setPositn(positnCode);
				sa.setPositndes(positnDes);
				list.add(sa);
				for(Positn p : deptList){
					SupplyAcct s = new SupplyAcct();
					Supply supply = new Supply();
					supply.setUnitper(sa.getSupply().getUnitper());
					s.setSupply(supply);
					s.setDued(sa.getDued());
					s.setPcno(sa.getPcno());
					s.setChkstoNo(sa.getChkstoNo());
					s.setDelivercode(sa.getDelivercode());
					s.setPositn(p.getCode());
					s.setPositndes(p.getDes());
					s.setId(sa.getId());
					s.setChkno(sa.getChkno());
					list.add(s);
				}
			}
			modelMap.put("disList", list);
		}
		modelMap.put("ids", ids);
		modelMap.put("isNotShowSp_price", DDT.isNotShowSp_price);//是否报货验货显示价格列金色三麦用
		return new ModelAndView(InspectionConstants.SAVE_CHKINM_GROUP_DB, modelMap);
	}
	
	/***
	 * 分档口验货  门店的就生成门店入库单，档口的生成档口直发单(调拨验货)
	 * @param modelMap
	 * @param chkinm
	 * @param ids
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveChkinmGroupDb")
	@ResponseBody
	public String saveChkinmGroupDb(ModelMap modelMap,Chkinm chkinm, String ids, HttpSession session) throws Exception {  
		String acct=session.getAttribute("ChoiceAcct").toString();
		String madeby = session.getAttribute("accountName").toString();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
				"MISBOH调拨分档口验货",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		try{
			//1.先判断状态,如果存在已经验货的，直接返回0
			int count = inspectionService.findOutCountByIds(ids, null, acct);
			if(count != 0){
				return "存在已验货的物资，请稍候再试。";
			}
			//2.先更新状态 
			inspectionService.check(ids, null, madeby, acct);
			//3.验货处理
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			if (null != thePositn) {
				chkinm.setPositn(thePositn);
			}
			chkinm.setAcct(acct);
			chkinm.setMadeby(madeby);
			return inspectionService.saveChkinmGroupDb(chkinm,ids)+"";
		}catch(Exception e){
			inspectionService.uncheck(ids, null, acct);
			return e.getLocalizedMessage();
		}
	}

	/***
	 * 调拨验货 打印
	 * @param modelMap
	 * @param session
	 * @param type
	 * @param check
	 * @param bdate
	 * @param edate
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printDispatchDb")
	public ModelAndView printDispatchDb(ModelMap modelMap, HttpSession session,
			String type, String check, Date bdate, Date edate, String positn)
			throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		HashMap<String, Object> disMap = new HashMap<String, Object>();
		disMap.put("positn", positn);
		disMap.put("bdate", bdate);
		disMap.put("edate", edate);
		disMap.put("check", check);
		disMap.put("db", "db");
		disMap.put("acct", session.getAttribute("ChoiceAcct").toString());
		String firm = "";
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			String positnCode = thePositn.getCode();
			disMap.put("firm", positnCode);
			firm = thePositn.getDes();
		}
		List<SupplyAcct> disList = inspectionService.selectByKeyDb(disMap);
		modelMap.put("List", disList);// list
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("report_name", "调拨到货验收表");
		parameters.put("firm", firm);
		parameters.put("madeby", session.getAttribute("accountNames").toString());
		parameters.put("report_time",
				DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		modelMap.put("parameters", parameters);// map

		HashMap<String, String> map = new HashMap<String, String>();
		map.put("check", check);
		map.put("bdate", DateFormat.getStringByDate(bdate, "yyyy-MM-dd"));
		map.put("edate", DateFormat.getStringByDate(edate, "yyyy-MM-dd"));
		map.put("positn", positn);
		modelMap.put("actionMap", map);
		modelMap.put("action", "/inspectionDb/printDispatchDb.do");// 传入回调路径
		Map<String, String> rs = ReadReportUrl.redReportUrl(type,
				InspectionConstants.REPORT_DISPATCH_URL,
				InspectionConstants.REPORT_DISPATCH_URL);// 判断跳转路径
		modelMap.put("reportUrl", rs.get("reportUrl"));// ireport文件地址
		return new ModelAndView(rs.get("url"), modelMap);
	}
	
}
