package com.choice.misboh.web.controller.inspection;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.framework.util.ProgramConstants;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.constants.inspection.InspectionConstants;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.misboh.service.inspection.InspectionMisService;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.Acct;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.PositnService;
import com.choice.scm.util.ReadReportUrl;

/***
 * 统配验货Controller
 * @author wjf
 *
 */
@Controller
@RequestMapping(value = "inspectionOut")
public class InspectionOutController {

	@Autowired
	private InspectionMisService inspectionService;
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	@Autowired
	private PositnService positnService;
	@Autowired
	private AcctService acctService;

	/***
	 * 进入档口直发页面  档口报货来的
	 * @param modelMap
	 * @param dis
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toChkinmDept")
	public ModelAndView toChkinmDept(ModelMap modelMap, Dis dis, HttpSession session) throws Exception {  
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<Dis> disList = inspectionService.findSupplyacctList(dis,null);
		modelMap.put("supplyacctList", disList);
		modelMap.put("dis", dis);
		return new ModelAndView(InspectionConstants.SAVE_CHKINM_DEPT, modelMap);
	}
	
	/******************************************************** 配送中心到货验收start *************************************************/
	/***
	 * 统配验货主页面
	 * @param modelMap
	 * @param session
	 * @param mold
	 * @param sa
	 * @param page
	 * @param check
	 * @param bdate
	 * @param edate
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/dispatch.do")
	public ModelAndView diapatchList(ModelMap modelMap, HttpSession session,String mold, SupplyAcct sa, Page page, String check, Date bdate,Date edate,Date maded1,String isInsByBill) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		if(null == isInsByBill)
			isInsByBill = ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "isInsByBill");
		if("Y".equals(isInsByBill)){//验货根据单据
			Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
					"MISBOH查询九毛九统配分单据验货",session.getAttribute("ip").toString(),ProgramConstants.SCM);
			commonMISBOHService.addLogs(logd);
			sa.setAcct(session.getAttribute("ChoiceAcct").toString());
			if(null == sa.getBdate()){
				sa.setBdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
				sa.setEdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
			}
			Positn positn = (Positn)session.getAttribute("accountPositn");
			sa.setFirmcode(positn.getCode());
			List<SupplyAcct> disList = inspectionService.listSupplyAcctTP(sa, page);
			modelMap.put("disList", disList);
			modelMap.put("supplyAcct", sa);
			modelMap.put("pageobj", page);
			return new ModelAndView(InspectionConstants.LIST_SUPPLYACCT_TP,modelMap);
		}
		Acct acct = acctService.findAcctById(session.getAttribute("ChoiceAcct").toString());
		// 点击查询的时候符合条件的配送中心到货信息。
		if ("select".equals(mold)) {
			HashMap<String, Object> disMap = new HashMap<String, Object>();
			disMap.put("bdate", bdate);
			disMap.put("edate", edate);
			disMap.put("check", check);
			disMap.put("vouno", sa.getVouno());//加个凭证号的查询条件
			disMap.put("acct", session.getAttribute("ChoiceAcct").toString());
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			if (null != thePositn) {
				String positnCode = thePositn.getCode();
				disMap.put("firm", positnCode);
				modelMap.put("firmCode", positnCode);
				modelMap.put("firmDes", thePositn.getDes());
				Positn positn = positnService.findPositnByCode(thePositn);
				modelMap.put("ynUseDept", positn.getYnUseDept());//页面判断用
			}
			disMap.put("acctDes", "聪少".equals(acct.getDes()) ? "cs" : null);
			List<SupplyAcct> disList = inspectionService.selectByKey(disMap, page);

			modelMap.put("dis", sa);
			modelMap.put("disList", disList);
		}
		// 初次加载页面传值
		if (bdate == null || edate == null) {
			bdate = new Date();
			edate = new Date();
			maded1 = new Date();
			check = "2";//默认未审核
		}
		modelMap.put("bdate", bdate);
		modelMap.put("edate", edate);
		modelMap.put("maded1", maded1);
		modelMap.put("check", check);
		modelMap.put("pageobj", page);
		modelMap.put("isNotShowSp_price", DDT.isNotShowSp_price);//是否报货验货显示价格列金色三麦用
		if("聪少".equals(acct.getDes())){//聪少页面单独做20160803wjf
			return new ModelAndView(InspectionConstants.LIST_DISPATCH_CS, modelMap);
		}
		return new ModelAndView(InspectionConstants.LIST_DISPATCH, modelMap);
	}

	/***
	 * 统配验货修改验货数量
	 * @param supplyAcct
	 * @param ids
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateAcct")
	@ResponseBody
	public String updateAcct(SupplyAcct supplyAcct, String ids, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,
				"MISBOH修改统配验货单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		String acct = session.getAttribute("ChoiceAcct").toString();
		HashMap<String, Object> disMap = new HashMap<String, Object>();
		disMap.put("listids", ids);
		disMap.put("acct", acct);
		disMap.put("check", 2);
		List<SupplyAcct> disList = inspectionService.selectByKey(disMap);//查未验货的
		if(null != supplyAcct && null != disList && supplyAcct.getSupplyAcctList().size() == disList.size()){
			return inspectionService.updateAcct(supplyAcct, acct);
		}
		return "-1";
	}

	/***
	 * 统配验货生成入库单 不启用档口情况
	 * @param modelMap
	 * @param ids
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/check")
	public ModelAndView check(ModelMap modelMap, String ids, Date maded, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,
				"MISBOH审核统配验货单,生成门店入库单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		String acct = session.getAttribute("ChoiceAcct").toString();
		String madeby = session.getAttribute("accountName").toString();
		try{
			//1.先更新状态
			HashMap<String, Object> disMap=new HashMap<String, Object>();
			disMap.put("listids", ids);
			disMap.put("acct", acct);
			disMap.put("check", 2);
			List<SupplyAcct> disList = inspectionService.selectByKey(disMap);//查未验货的
			if(disList.size() != ids.split(",").length){
				return new ModelAndView(StringConstant.ERROR_DONE, modelMap);
			}
			inspectionService.check(ids, null, madeby, acct);
			inspectionService.check(disList, ids, madeby, acct, maded);
			modelMap.put("check", "2");
			return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
		}catch(Exception e){//如果有异常就回滚
			inspectionService.uncheck(ids, null, acct);
			return new ModelAndView(StringConstant.ERROR_DONE, modelMap);
		}
	}
	
	/***
	 * 生成档口直发单(统配验货)
	 * @param modelMap
	 * @param chkinm
	 * @param ids
	 * @param sp_ids
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveChkinmDept")
	@ResponseBody
	public String saveChkinmDept(ModelMap modelMap,Chkinm chkinm, String ids, String sp_ids,String sp_codes,HttpSession session) throws Exception {  
		String acct = session.getAttribute("ChoiceAcct").toString();
		String madeby = session.getAttribute("accountName").toString();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
				"MISBOH统配档口验货,生成档口直发单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		try{
			//1.先判断状态,如果存在已经验货的，直接返回0
			int count = inspectionService.findOutCountByIds(ids, sp_codes, acct);
			if(count != 0){
				return "存在已验货的物资，请稍候再试。";
			}
			//2.先更新状态 
			inspectionService.check(ids, sp_codes, madeby, acct);
			//3.验货处理
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			if (null != thePositn) {
				chkinm.setPositn(thePositn);
			}
			chkinm.setAcct(acct);
			chkinm.setMadeby(madeby);
			return inspectionService.saveChkinmDept(chkinm,ids,sp_ids,sp_codes)+"";
		}catch(Exception e){//如果有异常就回滚
			inspectionService.uncheck(ids, sp_codes, acct);
			return e.getLocalizedMessage();
		}
	}
	
	/***
	 * 进入分档口验货  配送验货
	 * @param modelMap
	 * @param ids
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toChkinmGroup")
	public ModelAndView toChkinmGroup(ModelMap modelMap, String ids, HttpSession session) throws Exception {  
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		HashMap<String, Object> disMap=new HashMap<String, Object>();
		disMap.put("listids", ids);
		disMap.put("cntout", "all");
		disMap.put("check", 2);//未验货
		List<SupplyAcct> disList = inspectionService.selectByKey(disMap);
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			String positnCode = thePositn.getCode();
			String positnDes = thePositn.getDes();
			Positn positn = new Positn();
			positn.setUcode(positnCode);
			List<Positn> deptList = positnService.findPositnSuperNOPage(positn);
			List<SupplyAcct> list = new ArrayList<SupplyAcct>();
			for(SupplyAcct sa : disList){
				sa.setPositn(positnCode);
				sa.setPositndes(positnDes);
				list.add(sa);
				for(Positn p : deptList){
					SupplyAcct s = new SupplyAcct();
					Supply supply = new Supply();
					supply.setUnitper(sa.getSupply().getUnitper());
					s.setSupply(supply);
					s.setDued(sa.getDued());
					s.setPcno(sa.getPcno());
					s.setChkstoNo(sa.getChkstoNo());
					s.setPositn(p.getCode());
					s.setPositndes(p.getDes());
					s.setId(sa.getId());
					s.setChkno(sa.getChkno());
					list.add(s);
				}
			}
			modelMap.put("disList", list);
		}
		modelMap.put("ids", ids);
		modelMap.put("isNotShowSp_price", DDT.isNotShowSp_price);//是否报货验货显示价格列金色三麦用
		return new ModelAndView(InspectionConstants.SAVE_CHKINM_GROUP, modelMap);
	}
	
	/***
	 * 分档口验货  门店的就生成门店入库单，档口的生成档口直发单(统配验货)
	 * @param modelMap
	 * @param chkinm
	 * @param ids
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveChkinmGroup")
	@ResponseBody
	public String saveChkinmGroup(ModelMap modelMap,Chkinm chkinm, String ids, HttpSession session) throws Exception {  
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct=session.getAttribute("ChoiceAcct").toString();
		String madeby = session.getAttribute("accountName").toString();
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
				"MISBOH统配分档口验货",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		try{
			//1.先判断状态,如果存在已经验货的，直接返回0
			int count = inspectionService.findOutCountByIds(ids, null, acct);
			if(count != 0){
				return "存在已验货的物资，请稍候再试。";
			}
			//2.先更新状态 
			inspectionService.check(ids, null, madeby, acct);
			//3.验货处理
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			if (null != thePositn) {
				chkinm.setPositn(thePositn);
			}
			chkinm.setAcct(acct);
			chkinm.setMadeby(madeby);
			return inspectionService.saveChkinmGroup(chkinm,ids)+"";
		}catch(Exception e){
			inspectionService.uncheck(ids, null, acct);
			return e.getLocalizedMessage();
		}
	}
	
	/***
	 * 配送验货 打印
	 * @param modelMap
	 * @param session
	 * @param type
	 * @param check
	 * @param bdate
	 * @param edate
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printDispatch")
	public ModelAndView printDispatch(ModelMap modelMap, HttpSession session,
			String type, String check, Date bdate, Date edate, String vouno) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		HashMap<String, Object> disMap = new HashMap<String, Object>();
		disMap.put("bdate", bdate);
		disMap.put("edate", edate);
		disMap.put("check", check);
		disMap.put("vouno", vouno);//加个凭证号的查询条件
		disMap.put("acct", session.getAttribute("ChoiceAcct").toString());
		String firm = "";
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			String positnCode = thePositn.getCode();
			disMap.put("firm", positnCode);
			firm = thePositn.getDes();
		}
		List<SupplyAcct> disList = inspectionService.selectByKey(disMap);
		modelMap.put("List", disList);// list
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("report_name", "配送中心到货验收表");
		parameters.put("firm", firm);
		parameters.put("madeby", session.getAttribute("accountNames").toString());
		parameters.put("report_time",DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		modelMap.put("parameters", parameters);// map

		HashMap<String, String> map = new HashMap<String, String>();
		map.put("check", check);
		map.put("bdate", DateFormat.getStringByDate(bdate, "yyyy-MM-dd"));
		map.put("edate", DateFormat.getStringByDate(edate, "yyyy-MM-dd"));
		map.put("vouno", vouno);
		modelMap.put("actionMap", map);
		modelMap.put("action", "/inspectionOut/printDispatch.do");// 传入回调路径
		Map<String, String> rs = ReadReportUrl.redReportUrl(type,
				InspectionConstants.REPORT_DISPATCH_URL,
				InspectionConstants.REPORT_DISPATCH_URL);// 判断跳转路径
		modelMap.put("reportUrl", rs.get("reportUrl"));// ireport文件地址
		return new ModelAndView(rs.get("url"), modelMap);
	}
	
	/******************************************************** 配送中心到货验收end *************************************************/
	
	
	/*********************************************九毛九按单据验货start***************************************************/
	
	/***
	 * 统配验货主页面 走单据验货
	 * @param modelMap
	 * @param session
	 * @param sa
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listSupplyAcctTP")
	public ModelAndView listSupplyAcctTP(ModelMap modelMap, HttpSession session,SupplyAcct sa, Page page) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"MISBOH查询九毛九统配分单据验货",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		sa.setAcct(session.getAttribute("ChoiceAcct").toString());
		if(null == sa.getBdate()){
			sa.setBdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
			sa.setEdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
			Positn positn = (Positn)session.getAttribute("accountPositn");
			sa.setChkyh("no");//默认查未审核的
			sa.setFirmcode(positn.getCode());
		}
		List<SupplyAcct> disList = inspectionService.listSupplyAcctTP(sa, page);
		modelMap.put("disList", disList);
		modelMap.put("supplyAcct", sa);
		modelMap.put("pageobj", page);
		return new ModelAndView(InspectionConstants.LIST_SUPPLYACCT_TP,modelMap);
	}
	
	/***
	 * 查询九毛九统配验货单详情
	 * @param modelMap
	 * @param session
	 * @param sa
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/tableSupplyAcctTP")
	public ModelAndView tableSupplyAcctTP(ModelMap modelMap, HttpSession session, SupplyAcct sa) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"MISBOH查询九毛九统配单据详情",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		String acct = session.getAttribute("ChoiceAcct").toString();
		sa.setAcct(acct);
		Positn positn = (Positn)session.getAttribute("accountPositn");
		positn = positnService.findPositnByCode(positn);
		sa.setFirmcode(positn.getCode());
		modelMap.put("ynUseDept", positn.getYnUseDept());//是否启用档口 页面判断用
//		sa.setChkyh("no");//未验货
		List<SupplyAcct> saList = inspectionService.listSupplyAcctTP(sa);
		List<SupplyAcct> disList = new ArrayList<SupplyAcct>();
		if(saList.size() != 0){
			sa = saList.get(0);
			HashMap<String, Object> disMap = new HashMap<String, Object>();
			disMap.put("chkno", sa.getChkno());//单号
			disMap.put("bdate", sa.getDat());
			disMap.put("edate", sa.getDat());
			disMap.put("acct", acct);
			disMap.put("cntout", "all");
			disList = inspectionService.selectByKey(disMap);
			if(null == sa.getMaded())//null说明没验货的,入库日期默认等于到货日期
				sa.setMaded(sa.getDat());
		}
		modelMap.put("disList", disList);
		modelMap.put("supplyAcct", sa);
		return new ModelAndView(InspectionConstants.TABLE_SUPPLYACCT_TP,modelMap);
	}
	
	/***
	 * 九毛九统配验货生成入库单
	 * @param modelMap
	 * @param session
	 * @param sa
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkSupplyAcctTP.do")
	public ModelAndView checkSupplyAcctTP(ModelMap modelMap,HttpSession session,SupplyAcct sa) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,
				"MISBOH九毛九直配验货,生成门店入库单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		Positn positn = (Positn)session.getAttribute("accountPositn");
		positn = positnService.findPositnByCode(positn);
		modelMap.put("ynUseDept", positn.getYnUseDept());//是否启用档口 页面判断用
		String acct = session.getAttribute("ChoiceAcct").toString();
		sa.setAcct(acct);
		sa.setChkyh("no");//未验货
		List<SupplyAcct> saList = inspectionService.listSupplyAcctTP(sa);
		List<SupplyAcct> disList = new ArrayList<SupplyAcct>();
		if(saList.size() != 0){
			Date maded = sa.getMaded() == null ? new Date() : sa.getMaded();
			sa = saList.get(0);
			HashMap<String, Object> disMap = new HashMap<String, Object>();
			disMap.put("check", 2);//未验货
			disMap.put("chkno", sa.getChkno());//单号
			disMap.put("bdate", sa.getDat());
			disMap.put("edate", sa.getDat());
			disMap.put("acct", acct);
			disMap.put("cntout", "all");
			disList = inspectionService.selectByKey(disMap);
			sa.setMaded(maded);
			sa.setMadeby(session.getAttribute("accountName").toString());
			String pr = inspectionService.checkSupplyAcctTP(sa,disList);
			sa.setDeal(pr);
		}else{
			sa.setDeal("0");//数据为空 已经验货
		}
		modelMap.put("disList", disList);
		modelMap.put("supplyAcct", sa);
		return new ModelAndView(InspectionConstants.TABLE_SUPPLYACCT_TP,modelMap);
	}
	
	/***
	 * 判断档口报货发来的物资是否在档口报货单里存在
	 * @param modelMap
	 * @param session
	 * @param ids
	 * @param chkstoNos
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkChkstomDeptSpcode")
	@ResponseBody
	public int checkChkstomDeptSpcode(ModelMap modelMap, HttpSession session, String ids, String chkstoNos) throws Exception {  
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		HashMap<String, Object> disMap = new HashMap<String, Object>();
		disMap.put("ids", ids);
		disMap.put("chkstoNos", chkstoNos);
		String acct=session.getAttribute("ChoiceAcct").toString();
		disMap.put("acct", acct);
		return inspectionService.checkChkstomDeptSpcode(disMap);
	}
	
	/**********************************************************手机系统配送验货后台************************************************************************/
	
	/**
	 * 得到配送中心验货列表
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/diapatchListWAP")
	@ResponseBody
	public Object diapatchListWAP(SupplyAcct sa,String acct,String firmCode,String check, Date bdate, Date edate,Page page,String jsonpcallback) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		HashMap<String, Object> disMap = new HashMap<String, Object>();
		disMap.put("bdate", bdate);
		disMap.put("edate", edate);
		disMap.put("check", check);
		disMap.put("firm", firmCode);
		disMap.put("acct", acct);
		List<SupplyAcct> disList = inspectionService.selectByKey(disMap,page);
		map.put("disList", disList);
		map.put("page", page);
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/**
	 * 修改验货数量
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateAcctWAP")
	@ResponseBody
	public Object updateAcctWAP(SupplyAcct supplyAcct) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		inspectionService.updateAcct(supplyAcct, supplyAcct.getAcct());
		map.put("pr", "succ");
		return  JSONObject.fromObject(map).toString();
	}
	
	/**
	 * 配送中心验收生成入库单
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkWAP")
	@ResponseBody
	public Object checkWAP(String ids, String acct ,String accountName,Date maded1,String jsonpcallback) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		try{
			//1.先更新状态
			HashMap<String, Object> disMap=new HashMap<String, Object>();
			disMap.put("listids", ids);
			disMap.put("acct", acct);
			disMap.put("check", 2);
			List<SupplyAcct> disList = inspectionService.selectByKey(disMap);//查未验货的
			inspectionService.check(ids, null, accountName, acct);
			inspectionService.check(disList, ids, accountName, acct, maded1!=null?maded1:new Date());
			map.put("pr", 1);
			return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
		}catch(Exception e){//如果有异常就回滚
			inspectionService.uncheck(ids, null, acct);
			map.put("pr", -1);
			return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
		}
	}
}
