package com.choice.misboh.web.controller.inout;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ProgramConstants;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.constants.inout.ChkoutDbMisConstants;
import com.choice.misboh.domain.inventory.PositnSupply;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.misboh.service.inout.ChkoutMisService;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.Chkoutd;
import com.choice.scm.domain.Chkoutm;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.PositnService;
import com.choice.scm.util.CalChkNum;
import com.choice.scm.util.ReadReportUrl;

/***
 * 门店调拨单
 * @author wjf
 *
 */
@Controller
@RequestMapping("/chkoutDbMis")
public class ChkoutDbMisController {

	@Autowired
	private ChkoutMisService chkoutMisService;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private PositnService positnService;
	@Autowired
	private CodeDesService codeDesService;
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	
	/**
	 * 获取单号，跳转添加页面
	 * 
	 * @param modelMap
	 * @return
	 * @author yp
	 * @throws CRUDException
	 */
	@RequestMapping("addChkout")
	public ModelAndView addChkout(ModelMap modelMap, HttpSession session,
			String action, String tableFrom,Chkoutm chkoutm) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		CodeDes codes = new CodeDes();		
		String locale = session.getAttribute("locale").toString();
		codes.setLocale(locale);
		codes.setCode(DDT.DBCK);//调拨出库
		List<CodeDes> billTypes = new ArrayList<CodeDes>();
		billTypes.add(codeDesService.findDocumentByCode(codes));
		modelMap.put("billType", billTypes);
		if (action == null || !action.equals("init")) {
			// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			List<Positn> positnList = new ArrayList<Positn>();
			if (null != thePositn) {
				positnList.add(thePositn);
				Positn p = new Positn();
				p.setUcode(thePositn.getCode());
				positnList.addAll(positnService.findPositnSuperNOPage(p));
			}
			chkoutm.setVouno(calChkNum.getNextBytable(DDT.VOUNO_CHKOUTM,DDT.VOUNO_DC+thePositn.getCode()+"-",new Date()));
			chkoutm.setMaded(new Date());
			chkoutm.setMadeby(session.getAttribute("accountName").toString());
			chkoutm.setMadebyName(session.getAttribute("accountNames").toString());
			chkoutm.setChkoutno(chkoutMisService.findNextNo().intValue());
			//出库仓位
			modelMap.put("positnOut", positnList);
			//领用仓位   查询所有门店，及盘亏1999，前台js 过滤掉自己
			Positn positn1=new Positn();
			positn1.setTypn("'1203'");
			List<Positn> lyList = positnService.findPositnSuperNOPage(positn1);
			modelMap.put("positnIn", lyList);
			modelMap.put("chkoutm", chkoutm);
			modelMap.put("curStatus", "add");
		} else {
			modelMap.put("tableFrom", tableFrom);
		}
		return new ModelAndView(ChkoutDbMisConstants.UPDATE_CHKOUT, modelMap);
	}
	
	/**
	 * 查找已审核出库单详细信息
	 * 
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("searchCheckedChkout")
	public ModelAndView searchCheckedChkout(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"MISBOH查找已审核调拨出库单详细信息",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		String positnCode="";
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		List<Positn> positnList = new ArrayList<Positn>();
		if (null != thePositn) {
			positnCode = thePositn.getCode();
			positnList.add(thePositn);
			Positn positn = new Positn();
			positn.setCode(positnCode);
			chkoutm.setPositn(positn);
		}
		
		CodeDes codes = new CodeDes();		
		String locale = session.getAttribute("locale").toString();
		codes.setLocale(locale);
		codes.setCode(DDT.DBCK);//调拨出库
		List<CodeDes> billTypes = new ArrayList<CodeDes>();
		billTypes.add(codeDesService.findDocumentByCode(codes));
		modelMap.put("listBill", billTypes);
		chkoutm = chkoutMisService.findChkoutmById(chkoutm);
		if(chkoutm!=null &&chkoutm.getChkoutd()!=null && chkoutm.getChkoutd().size()>0){
			//查询库存量--物资余额表数据
			SupplyAcct supplyAcct = new SupplyAcct();
			supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
			supplyAcct.setPositn(chkoutm.getPositn().getCode());
			List<PositnSupply> psList = commonMISBOHService.findViewPositnSupplyList(supplyAcct);
			for(Chkoutd ckd:chkoutm.getChkoutd()){
				for(PositnSupply ps : psList){
					if(ckd.getSp_code().getSp_code().equals(ps.getSp_code())){
						ckd.getSp_code().setCnt(ps.getInc0());
					}
				}
			}
		}	
		modelMap.put("chkoutm", chkoutm);
		modelMap.put("curStatus", "show");
		return new ModelAndView(ChkoutDbMisConstants.SEARCH_CHECKED_CHKOUT, modelMap);
	}
	
	/**
	 * 通过id获取出库单详细信息
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @author yp
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("getChkoutById")
	public ModelAndView findChkoutById(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("chkoutm", chkoutMisService.findChkoutmById(chkoutm));
		return new ModelAndView(ChkoutDbMisConstants.UPDATE_CHKOUT, modelMap);
	}

	/**
	 * 模糊查询调拨单
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @param bdat
	 * @param edat
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("list")
	public ModelAndView findChkout(ModelMap modelMap, Chkoutm chkoutm,String action, Date bdat, Date edat,
			HttpSession session, Page page) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"MISBOH模糊查询调拨出库单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			//查询该分店的档口
			Positn positn = new Positn();
			positn.setUcode(thePositn.getCode());
			List<Positn> list = positnService.findPositnSuperNOPage(positn);
			List<String> listPositn = new ArrayList<String>();
			for(Positn posi:list){
				listPositn.add(posi.getCode());
			}
			listPositn.add(thePositn.getCode());//加上分店本身
			chkoutm.setListPositn(listPositn);
		}
		modelMap.put("pageobj", page);
		if (null != action && !"".equals(action)) {
			bdat = DateFormat.formatDate(new Date(), "yyyy-MM-dd");
			edat = DateFormat.formatDate(new Date(), "yyyy-MM-dd");
		}
		modelMap.put("bdat", bdat);
		modelMap.put("edat", edat);
		//只查调拨 
		chkoutm.setTyp(DDT.DBCK);//调拨出库
		modelMap.put("chkoutm", chkoutm);
		modelMap.put("chkoutList", chkoutMisService.findChkoutm(chkoutm, bdat, edat, page,session.getAttribute("locale").toString(),"db"));
		if (null != chkoutm.getChecby() && "c".equals(chkoutm.getChecby())) {//审核的
			return new ModelAndView(ChkoutDbMisConstants.CHECKED_CHKOUT, modelMap);
		} else {//未审核的
			return new ModelAndView(ChkoutDbMisConstants.MANAGE_CHKOUT, modelMap);
		}
	}

	/**
	 * 更新,添加出库单
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("saveOrUpdate")
	@ResponseBody
	public Object saveByAdd(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session, String curStatus, String scrapped,String scrappedname) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		String result = null;
		Map<String, String> resul = new HashMap<String, String>();
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		try{
			commonMISBOHService.getOnlyAccountMonth2(chkoutm.getMaded());//判断制单日期会计日
		}catch(Exception e){
			resul.put("pr", "-9");
			JSONObject rs = JSONObject.fromObject(resul);
			return rs.toString();
		}	
		if (curStatus.equals("add")) {
			Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
					"MISBOH添加调拨单:单号（"+chkoutm.getChkoutno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
			commonMISBOHService.addLogs(logd);
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			chkoutm.setVouno(calChkNum.getNextBytable(DDT.VOUNO_CHKOUTM,DDT.VOUNO_DC+thePositn.getCode()+"-",chkoutm.getMaded()));
			try{
				//供应商为当前调拨出库的门店对应的供应商
				Deliver deliver = chkoutMisService.findDeliverByPositn(thePositn);
				if(null == deliver){
					resul.put("pr", "-4");
					resul.put("error", "没有设置仓位当前门店对应的供应商，不能填调拨单！");
					JSONObject rs = JSONObject.fromObject(resul);
					return rs.toString();
				}
				result = chkoutMisService.saveChkoutm(chkoutm);
			}catch(Exception e){
				return e.getMessage();
			}
		} else if (curStatus.equals("edit")) {
			chkoutm.setMadeby(session.getAttribute("accountName").toString());
			Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,
					"MISBOH更新调拨单:单号（"+chkoutm.getChkoutno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
			commonMISBOHService.addLogs(logd);
			result = chkoutMisService.updateChkoutm(chkoutm);
		}
		return result;
	}

	/**
	 * 删除出库单
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("deleteChkout")
	@ResponseBody
	public Object deleteChkout(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,
				"Misboh删除调拨单:单号（"+chkoutm.getChkoutno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		return chkoutMisService.deleteChkoutm(chkoutm);
	}

	/**
	 * 跳转到更新页面
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("updateChkout")
	public ModelAndView updateChkout(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkoutm = chkoutMisService.findChkoutmById(chkoutm);
		if(chkoutm!=null &&chkoutm.getChkoutd()!=null && chkoutm.getChkoutd().size()>0){
			//查询库存量--物资余额表数据
			SupplyAcct supplyAcct = new SupplyAcct();
			supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
			supplyAcct.setPositn(chkoutm.getPositn().getCode());
			List<PositnSupply> psList = commonMISBOHService.findViewPositnSupplyList(supplyAcct);
			for(Chkoutd ckd:chkoutm.getChkoutd()){
				for(PositnSupply ps : psList){
					if(ckd.getSp_code().getSp_code().equals(ps.getSp_code())){
						ckd.getSp_code().setCnt(ps.getInc0());
					}
				}
			}
		}		
		modelMap.put("chkoutm",chkoutm );
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		List<Positn> positnList = new ArrayList<Positn>();
		if (null != thePositn) {
			positnList.add(thePositn);
			Positn p = new Positn();
			p.setUcode(thePositn.getCode());
			positnList.addAll(positnService.findPositnSuperNOPage(p));
		}
		//出库仓位
		modelMap.put("positnOut", positnList);
		CodeDes codes = new CodeDes();		
		String locale = session.getAttribute("locale").toString();
		codes.setLocale(locale);
		codes.setCode(DDT.DBCK);//调拨出库
		List<CodeDes> billTypes = new ArrayList<CodeDes>();
		billTypes.add(codeDesService.findDocumentByCode(codes));
		modelMap.put("billType", billTypes);
		
		//领用仓位   查询所有门店，前台js 过滤掉自己
		Positn positn1=new Positn();
		positn1.setTypn("'1203'");
		List<Positn> lyList = positnService.findPositnSuperNOPage(positn1);
		modelMap.put("positnIn", lyList);
		modelMap.put("curStatus", "show");
		return new ModelAndView(ChkoutDbMisConstants.UPDATE_CHKOUT, modelMap);
	}

	/**
	 * 审核出库单
	 * 
	 * @param chkoutm
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("auditChkout")
	@ResponseBody
	public Object auditChkout(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CHECK,
				"MISBOH审核调拨单:单号("+chkoutm.getChkoutno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		String acct = session.getAttribute("ChoiceAcct").toString();
		String chknos = null == chkoutm.getChkoutnos() ? chkoutm.getChkoutno()+"" : chkoutm.getChkoutnos();
		try{
			String chkectby=session.getAttribute("accountName").toString();
			chkoutm.setChecby(chkectby);
			String chkectbys=session.getAttribute("accountNames").toString();
			chkoutm.setChecbyName(chkectbys);
			chkoutm.setFirm((Positn)session.getAttribute("accountPositn"));
			//1.先判断状态
			int count = commonMISBOHService.findCountByChknos("CHKOUTM", "CHKOUTNO", acct, chknos);
			if(count != 0){
				return "此单已被审核。";
			}
			//2.然后更新状态
			commonMISBOHService.preCheck("CHKOUTM", "CHKOUTNO", acct, chknos);
			//3.真正审核方法
			return chkoutMisService.updateByAudit(chkoutm,chkectby);  //修改洞庭反应的问题2014.11.21
		}catch(Exception e){
			//4.异常返回
			commonMISBOHService.unPreCheck("CHKOUTM", "CHKOUTNO", acct, chknos);
			return e.getLocalizedMessage();
		}
	}

	/**
	 * 出库单打印
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @param type
	 * @param page
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping(value = "/printChkout")
	public ModelAndView viewChkout(ModelMap modelMap, Chkoutm chkoutm,
			String type, Page page, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.PRINT,
				"MISBOH调拨单打印",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkoutm = chkoutMisService.findChkoutmById(chkoutm);
		modelMap.put("List", chkoutm.getChkoutd());
		HashMap<Object, Object> parameters = new HashMap<Object, Object>();
		parameters.put("comName", "");
		parameters.put("firm", chkoutm.getFirm().getDes());
		parameters.put("positin", chkoutm.getPositn().getDes());
		CodeDes codeDes = new CodeDes();
		codeDes.setCode(chkoutm.getTyp());
		codeDes.setLocale(session.getAttribute("locale").toString());
		parameters.put("typ", codeDesService.findDocumentByCode(codeDes).getDes());
		parameters.put("maded", chkoutm.getMaded());
		parameters.put("vouno", chkoutm.getVouno());
		parameters.put("madeby", chkoutm.getMadebyName());//打印审核人姓名wjf
		parameters.put("checby", chkoutm.getChecbyName());

		modelMap.put("parameters", parameters);
		modelMap.put("action",
				"/chkoutDbMis/printChkout.do?chkoutno=" + chkoutm.getChkoutno());// 传入回调路径
		Map<String, String> rs = ReadReportUrl.redReportUrl(type,
				ChkoutDbMisConstants.REPORT_PRINT_URL,
				ChkoutDbMisConstants.REPORT_PRINT_URL);// 判断跳转路径
		modelMap.put("reportUrl", rs.get("reportUrl"));// ireport文件地址
		return new ModelAndView(rs.get("url"), modelMap);
	}

	/**
	 * 获取出库单单号
	 * 
	 * @param maded
	 * @return
	 * @throws ParseException
	 * @author yp
	 */
	@RequestMapping("getVouno")
	@ResponseBody
	public Object getVouno(HttpSession session, String maded) throws ParseException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		return calChkNum.getNextBytable(DDT.VOUNO_CHKOUTM,DDT.VOUNO_DC+thePositn.getCode()+"-",DateFormat.getDateByString(maded, "yyyy-MM-dd"));
	}

	/**
	 * 操作成功页面跳转
	 * 
	 * @return
	 * @author yp
	 */
	@RequestMapping("success")
	public ModelAndView success() {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		return new ModelAndView(StringConstant.ACTION_DONE);
	}

	/**
	 * 检测要保存、删除的出库单据是否被审核
	 * 
	 * @param modelMap
	 * @param session
	 * @param active
	 * @param chkoutm
	 * @return
	 * @throws Exception
	 * @author ZGL
	 */
	@RequestMapping(value = "/chkChect")
	@ResponseBody
	public String chkChect(ModelMap modelMap, HttpSession session,
			String active, Chkoutm chkoutm) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());// 帐套
		return chkoutMisService.chkChect(active, chkoutm);
	}

}
