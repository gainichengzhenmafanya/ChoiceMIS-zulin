package com.choice.misboh.web.controller.inout;

import java.io.IOException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.framework.util.ProgramConstants;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.constants.inout.ChkinmMisConstants;
import com.choice.misboh.domain.chkstom.MISChkstoDemod;
import com.choice.misboh.domain.chkstom.MISChkstoDemom;
import com.choice.misboh.domain.costReduction.MisSupply;
import com.choice.misboh.service.chkstom.ChkstoDemomMisService;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.misboh.service.inout.ChkinmMisService;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.Chkind;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Spbatch;
import com.choice.scm.domain.Spprice;
import com.choice.scm.domain.Supply;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.DeliverService;
import com.choice.scm.service.FirmDeliverService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.SupplyService;
import com.choice.scm.util.CalChkNum;
import com.choice.scm.util.ReadReportUrl;

/***
 * 门店入库单
 * @author wjf
 *
 */
@Controller
@RequestMapping(value = "chkinmMis")
public class ChkinmMisController {

	@Autowired
	private ChkinmMisService chkinmMisService;
	@Autowired
	PositnService positnService;
	@Autowired
	private DeliverService deliverService;
	@Autowired
	private FirmDeliverService firmDeliverService;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	@Autowired
	private CodeDesService codeDesService;
	@Autowired
	private ChkstoDemomMisService chkstoDemomService;
	@Autowired
	private SupplyService supplyService;
	
    /**
     * 查询门店入库单list
     * @param checkOrNot   是否审核
     * @param madedEnd  
     * @param searchDate
     * @param action  init表示初始页面
     * @param inout   zf  表示查询直发    rk  表示rk
     */
	@RequestMapping(value = "/list")
	public ModelAndView findAllChkinm(ModelMap modelMap, String checkOrNot, Chkinm chkinm, 
			Date madedEnd, String startDate, Page page, String action,HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"MISBOH查询门店入库单 ",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			chkinm.setPositn(thePositn);
		}
		if(null!=action && "init".equals(action)){
			Date date=new Date();
			if(null!=startDate && !"".equals(startDate)){
				chkinm.setMaded(DateFormat.getDateByString(startDate, "yyyy-MM-dd"));
			}else{
				chkinm.setMaded(DateFormat.formatDate(date, "yyyy-MM-dd"));
			}
			modelMap.put("chkinmList", chkinmMisService.findAllChkinm(checkOrNot,chkinm,page,DateFormat.formatDate(date, "yyyy-MM-dd"),session.getAttribute("locale").toString()));
			modelMap.put("madedEnd", date);
		}else{
			modelMap.put("chkinmList", chkinmMisService.findAllChkinm(checkOrNot,chkinm,page,madedEnd,session.getAttribute("locale").toString()));
			modelMap.put("madedEnd", madedEnd);
		}
		modelMap.put("chkinm", chkinm);
		modelMap.put("checkOrNot", checkOrNot);
		modelMap.put("pageobj", page);
		if("check".equals(checkOrNot)){
			return new ModelAndView(ChkinmMisConstants.MIS_LIST_CHECKED_CHKINM, modelMap);
		}else{
			return new ModelAndView(ChkinmMisConstants.MIS_LIST_CHKINM, modelMap);
		}
	}
	
	/**
	 * 转入添加页面    
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/add")
	public ModelAndView add(ModelMap modelMap, HttpSession session, String action, String tableFrom)throws Exception{
		String positnCode="";
		try {
			DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			//判断用户选择的语言系统
			String locale = session.getAttribute("locale").toString();
			CodeDes codeDes = new CodeDes();
			codeDes.setLocale(locale);
			codeDes.setCodetyp(DDT.CODEDES18RK);//单据类型为入库的
			modelMap.put("billType", getTypList(codeDes));
			//判断  当报价为0时候   是否 可以修改入库价格  
			modelMap.put("YNChinmSpprice",ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CHKINM_SPPRICE_IS_YN"));
			modelMap.put("isChkinPriceNot0",DDT.isChkinPriceNot0);//是否验货，入库价格不能为0 西贝用
			if(!"init".equals(action)){
				String accountName=session.getAttribute("accountName").toString();
				String accountNames=session.getAttribute("accountNames").toString();
				Chkinm chkinm=new Chkinm();
				// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
				Positn thePositn = (Positn)session.getAttribute("accountPositn");
				List<Positn> positnList = new ArrayList<Positn>();
				if (null != thePositn) {
					positnCode = thePositn.getCode();
					positnList.add(thePositn);
					chkinm.setPositn(thePositn);
				}
				chkinm.setMadeby(accountName);
				chkinm.setMadebyName(accountNames);
				chkinm.setMaded(new Date());
				chkinm.setVouno(calChkNum.getNextBytable(DDT.VOUNO_CHKINM,DDT.VOUNO_RK+thePositn.getCode()+"-",new Date()));
				chkinm.setChkinno(chkinmMisService.getMaxChkinno());
				modelMap.put("chkinm",chkinm);//日期等参数
				modelMap.put("positnList", positnList);//入库门店自己
				modelMap.put("deliverList",firmDeliverService.findDeliverByPositn(positnCode));//供应商走分店供应商
				modelMap.put("curStatus", "add");//当前页面状态
			}else{
				modelMap.put("tableFrom", tableFrom);//当前页面状态
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView(ChkinmMisConstants.MIS_UPDATE_CHKINM,modelMap);	
	}
	
	/***
	 * 过滤单据类型
	 * @param codeDes
	 * @return
	 * @throws Exception
	 */
	private List<CodeDes> getTypList(CodeDes codeDes) throws Exception{
		List<CodeDes> codeDesList = codeDesService.findDocumentType(codeDes);
		Iterator<CodeDes> car = codeDesList.iterator();
		while(car.hasNext()){//去掉后面这些：9941 验货冲消  调拨入库 直配入库 配送入库 
			CodeDes cd = car.next();
			if(DDT.YHCX.equals(cd.getCode()) || DDT.DBRK.equals(cd.getCode()) 
					|| DDT.ZPRK.equals(cd.getCode()) || DDT.PSRK.equals(cd.getCode())){
				car.remove();
			}
		}
		return codeDesList;
	}
	
	/**
	 * 新增或修改门店入库单
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/updateChkinm")
	@ResponseBody
	public int updateChkinm(ModelMap modelMap, HttpSession session, Chkinm chkinm)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,
				"MISBOH修改门店入库单:单号("+chkinm.getChkinno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		Chkinm chkinms=chkinmMisService.findChkinmByid(chkinm);
		try{
			commonMISBOHService.getOnlyAccountMonth2(chkinm.getMaded());//判断制单日期会计日
		}catch(CRUDException e1){
			return -9;
		}
		chkinm.setAcct(session.getAttribute("ChoiceAcct").toString());//帐套
		Integer result=0;
		if(null!=chkinms){//存在则修改
			if(null != chkinms.getChecby() && !"".equals(chkinms.getChecby())){//验证是否被审核
				return -1;
			}
			chkinm.setInout(DDT.IN);
			result=chkinmMisService.updateChkinm(chkinm);
		}else{//不存在则添加   
			chkinm.setInout(DDT.IN);
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			chkinm.setVouno(calChkNum.getNextBytable(DDT.VOUNO_CHKINM,DDT.VOUNO_RK+thePositn.getCode()+"-",chkinm.getMaded()));
			result=chkinmMisService.saveChkinm(chkinm,"in");
		}
		return result;	
	}
	
	/**
	 * 审核门店入库单  单条审核
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/checkChkinm")
	@ResponseBody
	public Object checkChkinm(ModelMap modelMap, Chkinm chkinm, HttpSession session, String zb)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CHECK,
				"MISBOH审核门店入库单:单号("+chkinm.getChkinno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		String acct = session.getAttribute("ChoiceAcct").toString();
		try{
			String accountId=session.getAttribute("accountName").toString();
			String accountNames = session.getAttribute("accountNames").toString();
			chkinm.setChecby(accountId);
			chkinm.setChecbyName(accountNames);
			chkinm.setFirm(((Positn)session.getAttribute("accountPositn")).getCode());
			//1.先判断状态
			int count = commonMISBOHService.findCountByChknos("CHKINM", "CHKINNO", acct, chkinm.getChkinno()+"");
			if(count != 0){
				return "此单已被审核。";
			}
			//2.然后更新状态
			commonMISBOHService.preCheck("CHKINM", "CHKINNO", acct, chkinm.getChkinno()+"");
			//3.真正审核方法
			return chkinmMisService.checkChkinm(chkinm,null);
		}catch(Exception e){
			//4.异常返回
			commonMISBOHService.unPreCheck("CHKINM", "CHKINNO", acct, chkinm.getChkinno()+"");
			return e.getLocalizedMessage();
		}
	}
	
	/**
	 * 检测要保存、删除的门店入库单据是否被审核
	 * @param modelMap
	 * @param session
	 * @param chkinm
	 * @return
	 * @throws Exception
	 * @author ZGL
	 */
	@RequestMapping(value="/chkChect")
	@ResponseBody
	public String chkChect(ModelMap modelMap, HttpSession session, String active, Chkinm chkinm)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chkinm.setAcct(session.getAttribute("ChoiceAcct").toString());//帐套
		return chkinmMisService.chkChect(active,chkinm);
	}
	
	/**
	 * 删除门店入库单
	 * @param modelMap
	 * @param chkinno
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete")
	public ModelAndView delete(ModelMap modelMap, String chkinnoids, String action, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,
				"MISBOH删除门店入库单:单号("+chkinnoids+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		//判断  当报价为0时候   是否 可以修改入库价格  
		modelMap.put("YNChinmSpprice",ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CHKINM_SPPRICE_IS_YN"));
		modelMap.put("isChkinPriceNot0", DDT.isChkinPriceNot0);//是否验货，入库价格不能为0 西贝用
		List<String> ids = Arrays.asList(chkinnoids.split(","));
		chkinmMisService.deleteChkinm(ids);
		if(!"init".equals(action)){
			return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
		}else{
			modelMap.put("chkinm", null);
			return new ModelAndView(ChkinmMisConstants.MIS_UPDATE_CHKINM, modelMap);
		}
	}
	
	/**
	 * 添加门店入库单
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/saveChkinm")
	public ModelAndView saveChkinm(ModelMap modelMap, Chkinm chkinm,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
				"MISBOH添加门店入库单:单号("+chkinm.getChkinno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		chkinmMisService.saveChkinm(chkinm,"in");
		modelMap.put("isChkinPriceNot0", DDT.isChkinPriceNot0);//是否验货，入库价格不能为0 西贝用
		return new ModelAndView(ChkinmMisConstants.MIS_UPDATE_CHKINM,modelMap);	
	}
	
	/**
	 * 冲消查询界面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkinmByCx")
	public ModelAndView addChkinmByCx(HttpSession session, ModelMap modelMap, Spbatch spbatch,String action) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (spbatch.getBdat() == null) {
			spbatch.setBdat(DateFormat.getDateBefore(DateFormat.formatDate(new Date(), "yyyy-MM-dd"),"day",-1,1));//昨天的数据
		}
		if (spbatch.getEdat() == null) {
			spbatch.setEdat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		}
		if(!"init".equals(action)){
			//默认只查入库，如果有门店走先进先出，则也要查出库退货的
			String firmOutWay = ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "firmOutWay");
			spbatch.setStatus("in");
			if ("1".equals(firmOutWay)) {// 特殊的 配置1 则所有的门店走先进先出
				spbatch.setStatus(null);
			} else if ("2".equals(firmOutWay)) {// 配置2  只有启用档口的店才走 先进先出
				Positn thePositn = (Positn)session.getAttribute("accountPositn");
				Positn p = positnService.findPositnByCode(thePositn);
				if ("Y".equals(p.getYnUseDept())) {// 启用档口的
					spbatch.setStatus(null);
				}
			}
			modelMap.put("spbatchList", chkinmMisService.addChkinmByCx(spbatch));//获取入库数据
		}
		modelMap.put("spbatch", spbatch);
		return new ModelAndView(ChkinmMisConstants.MIS_TABLE_CHKINM_CX,modelMap);
	}
	
	/**
	 * 获取门店入库单单号
	 * @param maded
	 * @return
	 * @throws ParseException
	 */
	@RequestMapping("getVouno")
	@ResponseBody
	public Object getVouno(HttpSession session, String maded) throws ParseException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		return calChkNum.getNextBytable(DDT.VOUNO_CHKINM,DDT.VOUNO_RK+thePositn.getCode()+"-",DateFormat.getDateByString(maded, "yyyy-MM-dd"));
	}
	
	/**
	 * 修改跳转
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/update")
	public ModelAndView update(ModelMap modelMap,HttpSession session, Chkinm chkinm, String isCheck, String inout) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//用户选择语言
		String locale = session.getAttribute("locale").toString();
		CodeDes codeDes = new CodeDes();
		codeDes.setLocale(locale);
		codeDes.setCodetyp(DDT.CODEDES18RK);
		modelMap.put("billType", getTypList(codeDes));
		//判断  当报价为0时候   是否 可以修改入库价格  
		modelMap.put("YNChinmSpprice",ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CHKINM_SPPRICE_IS_YN"));
		modelMap.put("isChkinPriceNot0", DDT.isChkinPriceNot0);//是否验货，入库价格不能为0 西贝用
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		String positnCode = "";
		List<Positn> positnList = new ArrayList<Positn>();
		if (null != thePositn) {
			positnCode = thePositn.getCode();
			positnList.add(thePositn);
		}
		modelMap.put("positnList", positnList);
		modelMap.put("deliverList",firmDeliverService.findDeliverByPositn(positnCode));
		modelMap.put("chkinm",chkinmMisService.findChkinmByid(chkinm));
		modelMap.put("curStatus", "show");//当前页面状态
		if(null!=isCheck && !"".equals(isCheck)){
			if(null!=inout && "zf".equals(inout))
				return new ModelAndView(ChkinmMisConstants.MIS_SEARCH_CHECKED_CHKINMZB,modelMap);
			else
				return new ModelAndView(ChkinmMisConstants.MIS_SEARCH_CHECKED_CHKINM,modelMap);
		}else{
			if(null!=inout && "zf".equals(inout)){
				return new ModelAndView(ChkinmMisConstants.MIS_UPDATE_CHKINMZB,modelMap);
			}else{
				return new ModelAndView(ChkinmMisConstants.MIS_UPDATE_CHKINM,modelMap);
			}
		}	
	}

	/**
	 * 打印
	 * @param modelMap
	 * @param chkinm
	 * @param type
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/viewChkinm")
	public ModelAndView viewChkinm(ModelMap modelMap,Chkinm chkinm,String type,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.PRINT,
				"MISBOH打印门店入库单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		chkinm=chkinmMisService.findChkinmByid(chkinm);
		List<Chkind> chkindList=chkinm.getChkindList();
		modelMap.put("List",chkindList);
 		HashMap<String, Object>  parameters = new HashMap<String, Object>(); 
 		String report_name = "门店入库单";
        parameters.put("report_name", report_name); 
        if(null!=chkinm.getDeliver() && null!= chkinm.getDeliver().getDes()){
        	parameters.put("deliver.des", chkinm.getDeliver().getDes()); 
        }
        parameters.put("positn.des", chkinm.getPositn().getDes()); 
        parameters.put("vouno", chkinm.getVouno()); 
        parameters.put("maded", DateFormat.getStringByDate(chkinm.getMaded(), "yyyy-MM-dd")); 
        parameters.put("madeby", chkinm.getMadebyName()); 
        parameters.put("checby", chkinm.getChecbyName());
        CodeDes codeDes = new CodeDes();
		codeDes.setCode(chkinm.getTyp());
		codeDes.setLocale(session.getAttribute("locale").toString());
		parameters.put("typ", codeDesService.findDocumentByCode(codeDes).getDes());
	    HashMap<String, String> map=new HashMap<String, String>();
	    map.put("chkinno", chkinm.getChkinno()+"");
        modelMap.put("parameters", parameters);
        modelMap.put("actionMap", map);
	 	modelMap.put("action", "/chkinmMis/viewChkinm.do");//传入回调路径
		Map<String,String> rs=ReadReportUrl.redReportUrl(type,ChkinmMisConstants.REPORT_URL,ChkinmMisConstants.REPORT_URL);//判断跳转路径
		modelMap.put("reportUrl", rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 审核操作  批量审核
	 * @param modelMap
	 * @param chkinno
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/checkAll")
	public ModelAndView checkAll(ModelMap modelMap, String chkinnoids, String checkOrNot, Chkinm chkinm, Date madedEnd, Page page, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CHECK,
				"MISBOH批量审核门店入库单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		String acct = session.getAttribute("ChoiceAcct").toString();
		try{
			String accountName = session.getAttribute("accountName").toString();
			chkinm.setChecby(accountName);
			chkinm.setFirm(((Positn)session.getAttribute("accountPositn")).getCode());
			//1.先判断状态
			int count = commonMISBOHService.findCountByChknos("CHKINM", "CHKINNO", acct, chkinnoids);
			if(count != 0){
				return new ModelAndView(StringConstant.ERROR_DONE, modelMap);
			}
			//2.然后更新状态
			commonMISBOHService.preCheck("CHKINM", "CHKINNO", acct, chkinnoids);
			//3.真正审核方法
			chkinmMisService.checkChkinm(chkinm,chkinnoids);
			return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
		}catch(Exception e){
			//4.异常返回
			commonMISBOHService.unPreCheck("CHKINM", "CHKINNO", acct, chkinm.getChkinno()+"");
			return new ModelAndView(StringConstant.ERROR_DONE, modelMap);
		}
	}
	
	/**
	 * 跳转到导入页面   门店入库单导入 wjf
	 */
	@RequestMapping("/importChkinm")
	public ModelAndView importChkinm(ModelMap modelMap) throws Exception{
		return new ModelAndView(ChkinmMisConstants.IMPORT_CHKINM,modelMap);
	}
	
	/**
	 * 下载模板信息 门店入库单模板下载 wjf
	 */
	@RequestMapping(value = "/downloadTemplate")
	public void downloadTemplate(HttpServletResponse response,HttpServletRequest request) throws IOException {
		chkinmMisService.downloadTemplate(response, request);
	}
	
	/**
	 * 先上传excel
	 */
	@RequestMapping(value = "/loadExcel", method = RequestMethod.POST)
	public ModelAndView loadExcel(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String realFilePath = chkinmMisService.upload(request, response);
		modelMap.put("realFilePath", realFilePath);
		return new ModelAndView(ChkinmMisConstants.IMPORT_RESULT, modelMap);
	}
	
	/**
	 * 导入门店入库单  wjf
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/importExcel")
	public ModelAndView importExcel(HttpSession session, ModelMap modelMap, @RequestParam String realFilePath)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId = session.getAttribute("accountId").toString();
		Object obj=chkinmMisService.check(realFilePath, accountId);
		if(obj instanceof Chkinm){//导入对了
			String accountName=session.getAttribute("accountName").toString();
			((Chkinm) obj).setMadeby(accountName);//得到当前操作人
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			((Chkinm) obj).setVouno(calChkNum.getNextBytable(DDT.VOUNO_CHKINM,DDT.VOUNO_RK+thePositn.getCode()+"-",new Date()));
			((Chkinm) obj).setChkinno(chkinmMisService.getMaxChkinno());
			modelMap.put("chkinm",(Chkinm) obj);
			modelMap.put("positnList", positnService.findPositn(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"), accountId,"'1201','1202','1203'"));//只能入到这些类型  加工间，办公室都不能选 2015.1.3wjf
			Deliver deliver = new Deliver();
			deliver.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
			deliver.setAccountId(session.getAttribute("accountId").toString());
			modelMap.put("deliverList", deliverService.findAllDelivers(deliver));
			modelMap.put("curStatus", "add");//当前页面状态
			modelMap.put("importFlag","OK");
			modelMap.put("isChkinPriceNot0", DDT.isChkinPriceNot0);//是否验货，入库价格不能为0 西贝用
			return new ModelAndView(ChkinmMisConstants.MIS_UPDATE_CHKINM,modelMap);
		}else{
			modelMap.put("importError", (List<String>)obj);
			return new ModelAndView(ChkinmMisConstants.MIS_UPDATE_CHKINM,modelMap);
		}
	}
	
	/***
	 * 入库模板调用
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param chkstodemo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkinDemo")
	public ModelAndView addChkinDemo(ModelMap modelMap, HttpSession session, String deliver, String madet, Integer chkstodemono) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		String acct = session.getAttribute("ChoiceAcct").toString();
		MISChkstoDemom chkstodemo = new MISChkstoDemom();
		chkstodemo.setAcct(acct);
		chkstodemo.setTempTyp("RK");
		chkstodemo.setFirm(thePositn.getCode());
		chkstodemo.setChkstodemono(chkstodemono);
		List<MISChkstoDemom> listChkstoDemo = chkstoDemomService.listChkstoDemom(chkstodemo);
		if(listChkstoDemo.size() > 0) {
			MISChkstoDemod chkstoDemod = new MISChkstoDemod();
			chkstoDemod.setScode(thePositn.getCode());
			chkstoDemod.setChkstodemono(listChkstoDemo.get(0).getChkstodemono());
			List<MISChkstoDemod> chkstodemoList = chkstoDemomService.listChkstoDemodd(chkstoDemod);
			//查询报价
			Spprice price = new Spprice();
			price.setAcct(acct);
			price.setDeliver(deliver);
			price.setArea(chkstodemo.getFirm());
			price.setMadet(madet);
			for(MISChkstoDemod ckd : chkstodemoList){
				price.setSp_code(ckd.getSupply().getSp_code());
				Spprice sp = supplyService.findBprice(price);
				if (null == sp||null == sp.getPrice()) {
					Supply supply = new Supply();
					supply.setAcct(acct);
					supply.setSp_code(ckd.getSupply().getSp_code());
					supply = supplyService.findSupplyById(supply);
					if (supply.getSp_price()!=null) {
						ckd.getSupply().setSp_price(supply.getSp_price());
					}
				}else {
					ckd.getSupply().setSp_price(sp.getPrice().doubleValue());
				}
				//2.查税率
				if(null != price.getDeliver()){
					MisSupply ms = new MisSupply();
					ms.setAcct(acct);
					ms.setSp_code(price.getSp_code());
					ms.setDeliver(price.getDeliver());
					ms = commonMISBOHService.findTaxByDeliverSpcode(ms);
					ckd.getSupply().setTax(ms.getTax());
					ckd.getSupply().setTaxdes(ms.getTaxdes());
				}
			}
			modelMap.put("chkstodemoList", chkstodemoList);
		}
		modelMap.put("deliver", deliver);
		modelMap.put("madet", madet);
		modelMap.put("chkstodemono", chkstodemono);
		//最后查标题栏
		chkstodemo.setChkstodemono(null);
		modelMap.put("listTitle", chkstoDemomService.listChkstoDemom(chkstodemo));//改为查带适用分店权限的2014.11.18wjf
		return new ModelAndView(ChkinmMisConstants.ADD_CHKINDEMO, modelMap);
	}
	
	/***************************************手机入库相关2016.1.6css (如需加方法  加到此分割线之上)****************************************/
	
	/**
	 * 入库模板调用
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getChkstoDemoWAP")
	@ResponseBody
	public Object getChkstoDemoWAP(MISChkstoDemom chkstodemo,String jsonpcallback) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		MISChkstoDemod chkstoDemod = new MISChkstoDemod();
		chkstoDemod.setScode(chkstodemo.getFirm());
		chkstoDemod.setChkstodemono(chkstodemo.getChkstodemono());
		List<MISChkstoDemod> chkstodemoList = chkstoDemomService.listChkstoDemodd(chkstoDemod);
		//查询报价
		Spprice price = new Spprice();
		price.setAcct(chkstodemo.getAcct());
		price.setDeliver(chkstodemo.getDeliver());
		price.setArea(chkstodemo.getFirm());
		price.setMadet(DateFormat.getStringByDate(chkstodemo.getMaded(),"yyyy-MM-dd"));
		for(MISChkstoDemod ckd : chkstodemoList){
			price.setSp_code(ckd.getSupply().getSp_code());
			Spprice sp = supplyService.findBprice(price);
			if (null == sp||null == sp.getPrice()) {
				Supply supply = new Supply();
				supply.setAcct(chkstodemo.getAcct());
				supply.setSp_code(ckd.getSupply().getSp_code());
				supply = supplyService.findSupplyById(supply);
				if (supply.getSp_price()!=null) {
					ckd.getSupply().setSp_price(supply.getSp_price());
				}
			}else {
				ckd.getSupply().setSp_price(sp.getPrice().doubleValue());
			}
			//2.查税率
			if(null != price.getDeliver()){
				MisSupply ms = new MisSupply();
				ms.setAcct(chkstodemo.getAcct());
				ms.setSp_code(price.getSp_code());
				ms.setDeliver(price.getDeliver());
				ms = commonMISBOHService.findTaxByDeliverSpcode(ms);
				ckd.getSupply().setTax(ms.getTax());
				ckd.getSupply().setTaxdes(ms.getTaxdes());
			}
		}
		
		map.put("chkstodemoList", chkstodemoList);
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/**
	 * 新增或修改保存入库单
	 */
	@RequestMapping(value = "/saveByAddOrUpdateWAP")
	@ResponseBody
	public Object saveByAddOrUpdateWAP(String sta, Chkinm chkinm) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		Integer result=0;
		for(Chkind chkind:chkinm.getChkindList()){
			chkind.setMemo(URLDecoder.decode(chkind.getMemo(), "UTF-8"));
		}
		//当前帐套
		if("add".equals(sta)){
			Integer chkinno = chkinmMisService.getMaxChkinno();
			chkinm.setChkinno(chkinno);
			chkinm.setInout(DDT.IN);
			chkinm.setVouno(calChkNum.getNextBytable(DDT.VOUNO_CHKINM,DDT.VOUNO_RK+chkinm.getPositn().getCode()+"-",chkinm.getMaded()));//从新获得单号
			map.put("chkinno", chkinno);
			result=chkinmMisService.saveChkinm(chkinm,"in");
		}else{
			Chkinm chkinm1 = chkinmMisService.findChkinmByid(chkinm);
			chkinm.setVouno(chkinm1.getVouno());
			chkinm.setTyp(chkinm1.getTyp());
			chkinm.setDeliver(chkinm1.getDeliver());
			chkinm.setInout("in");
			chkinm.setMadeby(chkinm1.getMadeby());
			result=chkinmMisService.updateChkinm(chkinm);
			map.put("chkinno", chkinm.getChkinno());
		}
		map.put("pr", result);
		return JSONObject.fromObject(map).toString();
	}

	/**
	 * 审核入库单
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkChkinmWAP")
	@ResponseBody
	public Object checkChkinmWAP(Chkinm chkinm, String chkinnoids, String jsonpcallback) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String result = "";
		try{
			result= chkinmMisService.checkChkinm(chkinm, chkinnoids);
		}catch(Exception e){
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("pr", "-1");
			map.put("error", e.getMessage());
			result = JSONObject.fromObject(map).toString();
		}
		return  jsonpcallback + "(" + result + ");";
	}
	
	/**
	 * 查找入库单
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findChkinmListWAP")
	@ResponseBody
	public Object findChkstomListWAP(Page page, Chkinm chkinm, String checked) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//把参数放到MAP
		HashMap<String, Object> chkstomMap=new HashMap<String, Object>();
		chkstomMap.put("checked", checked);
		chkstomMap.put("chkinm", chkinm);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("chkinmList", chkinmMisService.findAllChkinm(checked, chkinm, page, chkinm.getMaded(), "zh_CN"));//改为查带适用分店权限的2014.11.18wjf
		map.put("page", page);
		return JSONObject.fromObject(map).toString();
//		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/**
	 * 查看已审核入库单的信息
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findCheckedChkinmWAP")
	@ResponseBody
	public Object findCheckedChkstomWAP(Chkind chkind, Chkinm chkinm, String jsonpcallback) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		//关键字查询
		map.put("chkinm", chkinmMisService.findChkinmByid(chkinm));
//		map.put("chkindList", chkinmMisService.findByChkstoNo(chkinm));
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/**
	 * 删除
	 */
	@RequestMapping(value = "/deleteChkinmWAP")
	@ResponseBody
	public Object deleteChkstomWAP(Chkinm chkinm, String jsonpcallback) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		List<String> ids = new ArrayList<String>();
		ids.add(chkinm.getChkinno()+"");
		chkinmMisService.deleteChkinm(ids);
		map.put("pr", "1");
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
}
