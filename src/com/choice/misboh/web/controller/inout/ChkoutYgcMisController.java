package com.choice.misboh.web.controller.inout;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ProgramConstants;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.constants.inout.ChkoutMisConstants;
import com.choice.misboh.constants.inout.ChkoutYgcMisConstants;
import com.choice.misboh.domain.chkstom.MISChkstoDemod;
import com.choice.misboh.domain.chkstom.MISChkstoDemom;
import com.choice.misboh.domain.inventory.PositnSupply;
import com.choice.misboh.service.chkstom.ChkstoDemomMisService;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.misboh.service.inout.ChkoutMisService;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.Chkoutd;
import com.choice.scm.domain.Chkoutm;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.PositnService;
import com.choice.scm.util.CalChkNum;
import com.choice.scm.util.ReadReportUrl;

/***
 * 员工餐出库单
 * @author wjf
 *
 */
@Controller
@RequestMapping("/chkoutYgcMis")
public class ChkoutYgcMisController {

	@Autowired
	private ChkoutMisService chkoutMisService;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private PositnService positnService;
	@Autowired
	private CodeDesService codeDesService;
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	@Autowired
	private ChkstoDemomMisService chkstoDemomService;
	
	/**
	 * 获取单号，跳转添加页面
	 * 
	 * @param modelMap
	 * @return
	 * @author yp
	 * @throws CRUDException
	 */
	@RequestMapping("addChkout")
	public ModelAndView addChkout(ModelMap modelMap, HttpSession session,
			String action, String tableFrom,Chkoutm chkoutm) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		CodeDes codes = new CodeDes();		
		codes.setCode(DDT.YGCCK);//调拨出库
		List<CodeDes> billTypes = new ArrayList<CodeDes>();
		billTypes.add(codeDesService.findDocumentByCode(codes));
		modelMap.put("billType", billTypes);
		if (action == null || !action.equals("init")) {
			String positnCode="";
			// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			List<Positn> positnList = new ArrayList<Positn>();
			if (null != thePositn) {
				positnCode = thePositn.getCode();
				positnList.add(thePositn);
				Positn positn = new Positn();
				positn.setCode(positnCode);
				chkoutm.setPositn(positn);
				//查询该分店的档口
				Positn p = new Positn();
				p.setUcode(thePositn.getCode());
				positnList.addAll(positnService.findPositnSuperNOPage(p));
			}
			chkoutm.setVouno(calChkNum.getNextBytable(DDT.VOUNO_CHKOUTM,DDT.VOUNO_CK+thePositn.getCode()+"-",new Date()));
			chkoutm.setMaded(new Date());
			chkoutm.setMadeby(session.getAttribute("accountName").toString());
			chkoutm.setMadebyName(session.getAttribute("accountNames").toString());
			chkoutm.setChkoutno(chkoutMisService.findNextNo().intValue());
			//出库仓位
			modelMap.put("positnOut", positnList);
			//领用仓位   查询盘亏1999
			Positn positn1=new Positn();
			positn1.setTypn("'1206'");
			List<Positn> lyList = positnService.findPositnSuperNOPage(positn1);
			modelMap.put("positnIn", lyList);
			modelMap.put("chkoutm", chkoutm);
			modelMap.put("curStatus", "add");
		} else {
			modelMap.put("tableFrom", tableFrom);
		}
		return new ModelAndView(ChkoutYgcMisConstants.UPDATE_CHKOUT, modelMap);
	}
	
	/**
	 * 模糊查询出库单
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @param bdat
	 * @param edat
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("list")
	public ModelAndView findChkout(ModelMap modelMap, Chkoutm chkoutm,String action, Date bdat, Date edat,
			HttpSession session, Page page) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"MISBOH模糊查询出库单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			//查询该分店的档口
			Positn positn = new Positn();
			positn.setUcode(thePositn.getCode());
			List<Positn> list = positnService.findPositnSuperNOPage(positn);
			List<String> listPositn = new ArrayList<String>();
			for(Positn posi:list){
				listPositn.add(posi.getCode());
			}
			listPositn.add(thePositn.getCode());//加上分店本身
			chkoutm.setListPositn(listPositn);
		}
		
		modelMap.put("pageobj", page);
		if (null != action && !"".equals(action)) {
			bdat = DateFormat.formatDate(new Date(), "yyyy-MM-dd");
			edat = DateFormat.formatDate(new Date(), "yyyy-MM-dd");
		}
		modelMap.put("bdat", bdat);
		modelMap.put("edat", edat);
		chkoutm.setTyp(DDT.YGCCK);//调拨出库
		modelMap.put("chkoutm", chkoutm);
		modelMap.put("chkoutList", chkoutMisService.findChkoutm(chkoutm, bdat, edat, page,session.getAttribute("locale").toString(),"ck"));// 不查调拨到其他门店的
		if (null != chkoutm.getChecby() && "c".equals(chkoutm.getChecby())) {//审核的
			return new ModelAndView(ChkoutMisConstants.CHECKED_CHKOUT, modelMap);
		} else {//未审核的
			return new ModelAndView(ChkoutYgcMisConstants.MANAGE_CHKOUT, modelMap);
		}
	}

	/**
	 * 跳转到更新页面
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("updateChkout")
	public ModelAndView updateChkout(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkoutm = chkoutMisService.findChkoutmById(chkoutm);
		if(chkoutm!=null &&chkoutm.getChkoutd()!=null && chkoutm.getChkoutd().size()>0){
			//查询库存量--物资余额表数据
			SupplyAcct supplyAcct = new SupplyAcct();
			supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
			supplyAcct.setPositn(chkoutm.getPositn().getCode());
			List<PositnSupply> psList = commonMISBOHService.findViewPositnSupplyList(supplyAcct);
			for(Chkoutd ckd:chkoutm.getChkoutd()){
				for(PositnSupply ps : psList){
					if(ckd.getSp_code().getSp_code().equals(ps.getSp_code())){
						ckd.getSp_code().setCnt(ps.getInc0());
					}
				}
			}
		}		
		modelMap.put("chkoutm",chkoutm );
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		List<Positn> positnList = new ArrayList<Positn>();
		if (null != thePositn) {
			positnList.add(thePositn);
			//查询该分店的档口
			Positn p = new Positn();
			p.setUcode(thePositn.getCode());
			positnList.addAll(positnService.findPositnSuperNOPage(p));
		}
		//出库仓位
		modelMap.put("positnOut", positnList);
		CodeDes codes = new CodeDes();		
		codes.setCode(DDT.YGCCK);//调拨出库
		List<CodeDes> billTypes = new ArrayList<CodeDes>();
		billTypes.add(codeDesService.findDocumentByCode(codes));
		modelMap.put("billType", billTypes);
		
		//领用仓位   查询所有门店，及盘亏1999，前台js 过滤掉自己
		Positn positn1=new Positn();
		positn1.setTypn("'1206'");
		List<Positn> lyList = positnService.findPositnSuperNOPage(positn1);
		modelMap.put("positnIn", lyList);
		modelMap.put("curStatus", "show");
		return new ModelAndView(ChkoutYgcMisConstants.UPDATE_CHKOUT, modelMap);
	}

	/**
	 * 出库单打印
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @param type
	 * @param page
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping(value = "/printChkout")
	public ModelAndView viewChkout(ModelMap modelMap, Chkoutm chkoutm,
			String type, Page page, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.PRINT,
				"MISBOH出库单打印",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkoutm = chkoutMisService.findChkoutmById(chkoutm);
		
		modelMap.put("List", chkoutm.getChkoutd());
		HashMap<Object, Object> parameters = new HashMap<Object, Object>();
		parameters.put("comName", "");
		parameters.put("firm", chkoutm.getFirm().getDes());
		parameters.put("positin", chkoutm.getPositn().getDes());
		CodeDes codeDes = new CodeDes();
		codeDes.setCode(chkoutm.getTyp());
		codeDes.setLocale(session.getAttribute("locale").toString());
		parameters.put("typ", codeDesService.findDocumentByCode(codeDes).getDes());
//		parameters.put("typ", chkoutm.getTyp());
		parameters.put("maded", chkoutm.getMaded());
		parameters.put("vouno", chkoutm.getVouno());
		parameters.put("madeby", chkoutm.getMadebyName());//打印审核人姓名wjf
		parameters.put("checby", chkoutm.getChecbyName());

		modelMap.put("parameters", parameters);
		modelMap.put("action", "/chkoutYgcMis/printChkout.do?chkoutno=" + chkoutm.getChkoutno());// 传入回调路径
		Map<String, String> rs = ReadReportUrl.redReportUrl(type,
				ChkoutMisConstants.REPORT_PRINT_URL,
				ChkoutMisConstants.REPORT_PRINT_URL);// 判断跳转路径
		modelMap.put("reportUrl", rs.get("reportUrl"));// ireport文件地址
		return new ModelAndView(rs.get("url"), modelMap);
	}

	/***
	 * 员工餐出库模板调用
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param chkstodemo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkoutDemo")
	public ModelAndView addChkoutDemo(ModelMap modelMap, HttpSession session, MISChkstoDemom chkstodemo) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		chkstodemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstodemo.setTempTyp("CK");
		chkstodemo.setFirm(thePositn.getCode());
		List<MISChkstoDemom> listChkstoDemo = chkstoDemomService.listChkstoDemom(chkstodemo);
		if(listChkstoDemo.size() > 0) {
			MISChkstoDemod chkstoDemod = new MISChkstoDemod();
			chkstoDemod.setScode(thePositn.getCode());
			chkstoDemod.setChkstodemono(listChkstoDemo.get(0).getChkstodemono());
			List<MISChkstoDemod> chkstodemoList = chkstoDemomService.listChkstoDemodd(chkstoDemod);
			//查询库存量--物资余额表数据
			SupplyAcct supplyAcct = new SupplyAcct();
			supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
			supplyAcct.setPositn(chkstodemo.getFirm());
			List<PositnSupply> psList = commonMISBOHService.findViewPositnSupplyList(supplyAcct);
			for(MISChkstoDemod ckd : chkstodemoList){
				for(PositnSupply ps : psList){
					if(ckd.getSupply().getSp_code().equals(ps.getSp_code())){
						ckd.getSupply().setCnt(ps.getInc0());
						ckd.getSupply().setSp_price(ps.getSp_price());
					}
				}
			}
			modelMap.put("chkstodemoList", chkstodemoList);
		}
		modelMap.put("firm", chkstodemo.getFirm());//wjf
		modelMap.put("chkstodemono", chkstodemo.getChkstodemono());
		//最后查标题栏
		chkstodemo.setChkstodemono(null);
		modelMap.put("listTitle", chkstoDemomService.listChkstoDemom(chkstodemo));//改为查带适用分店权限的2014.11.18wjf
		return new ModelAndView(ChkoutYgcMisConstants.ADD_CHKOUTDEMO, modelMap);
	}
}
