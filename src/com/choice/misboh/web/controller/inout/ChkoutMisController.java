package com.choice.misboh.web.controller.inout;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.framework.util.ProgramConstants;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.constants.inout.ChkoutMisConstants;
import com.choice.misboh.domain.inventory.PositnSupply;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.misboh.service.inout.ChkoutMisService;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.Chkoutd;
import com.choice.scm.domain.Chkoutm;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Spbatch;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.PositnService;
import com.choice.scm.util.CalChkNum;
import com.choice.scm.util.ReadReportUrl;

/***
 * 门店出库单
 * @author wjf
 *
 */
@Controller
@RequestMapping("/chkoutMis")
public class ChkoutMisController {

	@Autowired
	private ChkoutMisService chkoutMisService;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private PositnService positnService;
	@Autowired
	private CodeDesService codeDesService;
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	
	/**
	 * 获取单号，跳转添加页面
	 * 
	 * @param modelMap
	 * @return
	 * @author yp
	 * @throws CRUDException
	 */
	@RequestMapping("addChkout")
	public ModelAndView addChkout(ModelMap modelMap, HttpSession session,
			String action, String tableFrom,Chkoutm chkoutm) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		CodeDes codes = new CodeDes();		
		codes.setTyp(DDT.CODEDES13);
		modelMap.put("codeDesList", codeDesService.findCodeDes(codes));//查询所有的报废原因(不分页)
		String locale = session.getAttribute("locale").toString();
		codes.setLocale(locale);
		codes.setCodetyp(DDT.CODEDES18CK);
		codes.setTyp(DDT.CODEDES18);
		modelMap.put("billType", getTypList(codes));
		if (action == null || !action.equals("init")) {
			String positnCode="";
			// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			List<Positn> positnList = new ArrayList<Positn>();
			if (null != thePositn) {
				positnCode = thePositn.getCode();
				positnList.add(thePositn);
				Positn positn = new Positn();
				positn.setCode(positnCode);
				chkoutm.setPositn(positn);
				//查询该分店的档口
				Positn p = new Positn();
				p.setUcode(thePositn.getCode());
				positnList.addAll(positnService.findPositnSuperNOPage(p));
			}
			chkoutm.setVouno(calChkNum.getNextBytable(DDT.VOUNO_CHKOUTM,DDT.VOUNO_CK+thePositn.getCode()+"-",new Date()));
			chkoutm.setMaded(new Date());
			chkoutm.setMadeby(session.getAttribute("accountName").toString());
			chkoutm.setMadebyName(session.getAttribute("accountNames").toString());
			chkoutm.setChkoutno(chkoutMisService.findNextNo().intValue());
			//出库仓位
			modelMap.put("positnOut", positnList);
			//领用仓位   查询盘亏1999，前台js 过滤掉自己
			Positn positn1=new Positn();
			positn1.setTypn("'1206'");
			List<Positn> lyList = positnService.findPositnSuperNOPage(positn1);
			lyList.addAll(positnList);
			modelMap.put("positnIn", lyList);
			modelMap.put("chkoutm", chkoutm);
			modelMap.put("curStatus", "add");
		} else {
			modelMap.put("tableFrom", tableFrom);
		}
		return new ModelAndView(ChkoutMisConstants.UPDATE_CHKOUT, modelMap);
	}
	
	/***
	 * 过滤单据类型
	 * @param codeDes
	 * @return
	 * @throws Exception
	 */
	private List<CodeDes> getTypList(CodeDes codeDes) throws Exception{
		List<CodeDes> codeDesList = codeDesService.findDocumentType(codeDes);
		Iterator<CodeDes> car = codeDesList.iterator();
		while(car.hasNext()){//去掉后面这些：9909调拨出库 9910盘亏出库    9921出库退货  9924批次出库   9911 赠品出库 加工领料
			CodeDes cd = car.next();
			if(DDT.DBCK.equals(cd.getCode()) || DDT.PKCK.equals(cd.getCode()) || DDT.CKTH.equals(cd.getCode()) 
					|| DDT.PCCK.equals(cd.getCode()) || DDT.ZPCK.equals(cd.getCode()) || DDT.JGLL.equals(cd.getCode()) || DDT.CCBC.equals(cd.getCode())){
				car.remove();
			}
		}
		return codeDesList;
	}
	
	/**
	 * 查找已审核出库单详细信息
	 * 
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("searchCheckedChkout")
	public ModelAndView searchCheckedChkout(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"MISBOH查找已审核出库单详细信息",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		String positnCode="";
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		List<Positn> positnList = new ArrayList<Positn>();
		if (null != thePositn) {
			positnCode = thePositn.getCode();
			positnList.add(thePositn);
			Positn positn = new Positn();
			positn.setCode(positnCode);
			chkoutm.setPositn(positn);
		}
		
		String locale = session.getAttribute("locale").toString();
		CodeDes codeDes = new CodeDes();
		codeDes.setLocale(locale);
		codeDes.setCodetyp(DDT.CODEDES18CK);
		modelMap.put("listBill", codeDesService.findDocumentType(codeDes));
		chkoutm = chkoutMisService.findChkoutmById(chkoutm);
		if(chkoutm!=null &&chkoutm.getChkoutd()!=null && chkoutm.getChkoutd().size()>0){
			//查询库存量--物资余额表数据
			SupplyAcct supplyAcct = new SupplyAcct();
			supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
			supplyAcct.setPositn(chkoutm.getPositn().getCode());
			List<PositnSupply> psList = commonMISBOHService.findViewPositnSupplyList(supplyAcct);
			for(Chkoutd ckd:chkoutm.getChkoutd()){
				for(PositnSupply ps : psList){
					if(ckd.getSp_code().getSp_code().equals(ps.getSp_code())){
						ckd.getSp_code().setCnt(ps.getInc0());
					}
				}
			}
		}	
		modelMap.put("chkoutm", chkoutm);
		// modelMap.put("positnIn", positnService.findPositnIn());
		// modelMap.put("positnOut", positnService.findPositnOut());
		modelMap.put("curStatus", "show");
		return new ModelAndView(ChkoutMisConstants.SEARCH_CHECKED_CHKOUT, modelMap);
	}

	/**
	 * 通过id获取出库单详细信息
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @author yp
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("getChkoutById")
	public ModelAndView findChkoutById(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("chkoutm", chkoutMisService.findChkoutmById(chkoutm));
		return new ModelAndView(ChkoutMisConstants.UPDATE_CHKOUT, modelMap);
	}

	/**
	 * 模糊查询出库单
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @param bdat
	 * @param edat
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("list")
	public ModelAndView findChkout(ModelMap modelMap, Chkoutm chkoutm,String action, Date bdat, Date edat,
			HttpSession session, Page page) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"MISBOH模糊查询出库单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			//查询该分店的档口
			Positn positn = new Positn();
			positn.setUcode(thePositn.getCode());
			List<Positn> list = positnService.findPositnSuperNOPage(positn);
			List<String> listPositn = new ArrayList<String>();
			for(Positn posi:list){
				listPositn.add(posi.getCode());
			}
			listPositn.add(thePositn.getCode());//加上分店本身
			chkoutm.setListPositn(listPositn);
		}
		
		modelMap.put("pageobj", page);
		if (null != action && !"".equals(action)) {
			bdat = DateFormat.formatDate(new Date(), "yyyy-MM-dd");
			edat = DateFormat.formatDate(new Date(), "yyyy-MM-dd");
		}
		modelMap.put("bdat", bdat);
		modelMap.put("edat", edat);
		modelMap.put("chkoutm", chkoutm);
		modelMap.put("chkoutList", chkoutMisService.findChkoutm(chkoutm, bdat, edat, page,session.getAttribute("locale").toString(),"ck"));// 不查调拨到其他门店的
		if (null != chkoutm.getChecby() && "c".equals(chkoutm.getChecby())) {//审核的
			return new ModelAndView(ChkoutMisConstants.CHECKED_CHKOUT, modelMap);
		} else {//未审核的
			return new ModelAndView(ChkoutMisConstants.MANAGE_CHKOUT, modelMap);
		}
	}

	/**
	 * 更新,添加出库单
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("saveOrUpdate")
	@ResponseBody
	public Object saveByAdd(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session, String curStatus, String scrapped,String scrappedname) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		String result = null;
		try{
			commonMISBOHService.getOnlyAccountMonth2(chkoutm.getMaded());//判断制单日期会计日
		}catch(Exception e){
			Map<String, String> resul = new HashMap<String, String>();
			resul.put("pr", "-3");
			JSONObject rs = JSONObject.fromObject(resul);
			return rs.toString();
		}
		//加判断：如果是报废，则memo取scrapped 和scrappedname ；如果是其他则不取   wjf
		if(DDT.CKBF.equals(chkoutm.getTyp())){//出库报废
			String memo = scrapped+'-'+scrappedname;		
			chkoutm.setMemo(memo);
		}
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		if (curStatus.equals("add")) {
			Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
					"MISBOH添加出库单:单号("+chkoutm.getChkoutno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
			commonMISBOHService.addLogs(logd);
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			chkoutm.setVouno(calChkNum.getNextBytable(DDT.VOUNO_CHKOUTM,DDT.VOUNO_CK+thePositn.getCode()+"-",chkoutm.getMaded()));
			result = chkoutMisService.saveChkoutm(chkoutm);
		} else if (curStatus.equals("edit")) {
			chkoutm.setMadeby(session.getAttribute("accountName").toString());
			Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,
					"MISBOH更新出库单:单号("+chkoutm.getChkoutno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
			commonMISBOHService.addLogs(logd);
			result = chkoutMisService.updateChkoutm(chkoutm);
		}
		return result;
	}

	/**
	 * 删除出库单
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("deleteChkout")
	@ResponseBody
	public Object deleteChkout(ModelMap modelMap, Chkoutm chkoutm, HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,
				"Misboh删除出库单:单号（"+chkoutm.getChkoutno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		return chkoutMisService.deleteChkoutm(chkoutm);
		// return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}

	/**
	 * 跳转到更新页面
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("updateChkout")
	public ModelAndView updateChkout(ModelMap modelMap, Chkoutm chkoutm, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkoutm = chkoutMisService.findChkoutmById(chkoutm);
		if(chkoutm!=null &&chkoutm.getChkoutd()!=null && chkoutm.getChkoutd().size()>0){
			//查询库存量--物资余额表数据
			SupplyAcct supplyAcct = new SupplyAcct();
			supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
			supplyAcct.setPositn(chkoutm.getPositn().getCode());
			List<PositnSupply> psList = commonMISBOHService.findViewPositnSupplyList(supplyAcct);
			for(Chkoutd ckd:chkoutm.getChkoutd()){
				for(PositnSupply ps : psList){
					if(ckd.getSp_code().getSp_code().equals(ps.getSp_code())){
						ckd.getSp_code().setCnt(ps.getInc0());
					}
				}
			}
		}		
		modelMap.put("chkoutm",chkoutm );
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		List<Positn> positnList = new ArrayList<Positn>();
		if (null != thePositn) {
			positnList.add(thePositn);
			//查询该分店的档口
			Positn p = new Positn();
			p.setUcode(thePositn.getCode());
			positnList.addAll(positnService.findPositnSuperNOPage(p));
		}
		//出库仓位
		modelMap.put("positnOut", positnList);
		CodeDes codeDes = new CodeDes();
		String locale = session.getAttribute("locale").toString();
		codeDes.setLocale(locale);
		codeDes.setCodetyp(DDT.CODEDES18CK);
		codeDes.setTyp(DDT.CODEDES18);
		modelMap.put("billType", getTypList(codeDes));
		
		//领用仓位   查询所有门店，及盘亏1999，前台js 过滤掉自己
		Positn positn1=new Positn();
		positn1.setTypn("'1206'");
		List<Positn> lyList = positnService.findPositnSuperNOPage(positn1);
		lyList.addAll(positnList);
		modelMap.put("positnIn", lyList);
		modelMap.put("curStatus", "show");
		//查询的时候也要查询报废原因  wjf
		CodeDes codes = new CodeDes();		
		codes.setTyp(DDT.CODEDES13);
		modelMap.put("codeDesList", codeDesService.findCodeDes(codes));//查询所有的报废原因(不分页)
		return new ModelAndView(ChkoutMisConstants.UPDATE_CHKOUT, modelMap);
	}

	/**
	 * 审核出库单
	 * 
	 * @param chkoutm
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("auditChkout")
	@ResponseBody
	public Object auditChkout(ModelMap modelMap, Chkoutm chkoutm, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CHECK,
				"MISBOH审核出库单:单号("+chkoutm.getChkoutno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		String acct = session.getAttribute("ChoiceAcct").toString();
		String chknos = null == chkoutm.getChkoutnos() ? chkoutm.getChkoutno()+"" : chkoutm.getChkoutnos();
		try{
			String chkectby=session.getAttribute("accountName").toString();
			chkoutm.setChecby(chkectby);
			String chkectbys=session.getAttribute("accountNames").toString();
			chkoutm.setChecbyName(chkectbys);
			chkoutm.setFirm((Positn)session.getAttribute("accountPositn"));
			//1.先判断状态
			int count = commonMISBOHService.findCountByChknos("CHKOUTM", "CHKOUTNO", acct, chknos);
			if(count != 0){
				return "此单已被审核。";
			}
			//2.然后更新状态
			commonMISBOHService.preCheck("CHKOUTM", "CHKOUTNO", acct, chknos);
			//3.真正审核方法
			return chkoutMisService.updateByAudit(chkoutm,chkectby);  //修改洞庭反应的问题2014.11.21
		}catch(Exception e){
			//4.异常返回
			commonMISBOHService.unPreCheck("CHKOUTM", "CHKOUTNO", acct, chknos);
			return e.getLocalizedMessage();
		}
	}

	/**
	 * 出库单打印
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @param type
	 * @param page
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping(value = "/printChkout")
	public ModelAndView viewChkout(ModelMap modelMap, Chkoutm chkoutm, String type, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.PRINT,
				"MISBOH出库单打印",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkoutm = chkoutMisService.findChkoutmById(chkoutm);
		
		modelMap.put("List", chkoutm.getChkoutd());
		HashMap<Object, Object> parameters = new HashMap<Object, Object>();
		parameters.put("comName", "");
		parameters.put("firm", chkoutm.getFirm().getDes());
		parameters.put("positin", chkoutm.getPositn().getDes());
		CodeDes codeDes = new CodeDes();
		codeDes.setCode(chkoutm.getTyp());
		codeDes.setLocale(session.getAttribute("locale").toString());
		parameters.put("typ", codeDesService.findDocumentByCode(codeDes).getDes());
//		parameters.put("typ", chkoutm.getTyp());
		parameters.put("maded", chkoutm.getMaded());
		parameters.put("vouno", chkoutm.getVouno());
		parameters.put("madeby", chkoutm.getMadebyName());//打印审核人姓名wjf
		parameters.put("checby", chkoutm.getChecbyName());

		modelMap.put("parameters", parameters);
		modelMap.put("action", "/chkoutMis/printChkout.do?chkoutno=" + chkoutm.getChkoutno());// 传入回调路径
		Map<String, String> rs = ReadReportUrl.redReportUrl(type,
				ChkoutMisConstants.REPORT_PRINT_URL,
				ChkoutMisConstants.REPORT_PRINT_URL);// 判断跳转路径
		modelMap.put("reportUrl", rs.get("reportUrl"));// ireport文件地址
		return new ModelAndView(rs.get("url"), modelMap);
	}

	/**
	 * 获取出库单单号
	 * 
	 * @param maded
	 * @return
	 * @throws ParseException
	 * @author yp
	 */
	@RequestMapping("getVouno")
	@ResponseBody
	public Object getVouno(HttpSession session, String maded) throws ParseException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		return calChkNum.getNextBytable(DDT.VOUNO_CHKOUTM,DDT.VOUNO_CK+thePositn.getCode()+"-",DateFormat.getDateByString(maded, "yyyy-MM-dd"));
	}

	/**
	 * 操作成功页面跳转
	 * 
	 * @return
	 * @author yp
	 */
	@RequestMapping("success")
	public ModelAndView success() {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		return new ModelAndView(StringConstant.ACTION_DONE);
	}

	/**
	 * 检测要保存、删除的出库单据是否被审核
	 * 
	 * @param modelMap
	 * @param session
	 * @param active
	 * @param chkoutm
	 * @return
	 * @throws Exception
	 * @author ZGL
	 */
	@RequestMapping(value = "/chkChect")
	@ResponseBody
	public String chkChect(ModelMap modelMap, HttpSession session, String active, Chkoutm chkoutm) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());// 帐套
		return chkoutMisService.chkChect(active, chkoutm);
	}

	/**
	 * 冲消查询界面
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkoutByCx")
	public ModelAndView addChkoutByCx(ModelMap modelMap,HttpSession session,Spbatch spbatch,String action) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		if (spbatch.getBdat() == null) {
			spbatch.setBdat(DateFormat.getDateBefore(DateFormat.formatDate(new Date(), "yyyy-MM-dd"),"day",-1,1));//昨天的数据
		}
		if (spbatch.getEdat() == null) {
			spbatch.setEdat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		}
		if(!"init".equals(action)){
			List<Spbatch> lists = chkoutMisService.findChkoutByCx(spbatch);
			//查询库存量--物资余额表数据
			SupplyAcct supplyAcct = new SupplyAcct();
			supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
			for(Spbatch sp : lists){
				supplyAcct.setPositn(spbatch.getDeliver());
				supplyAcct.setSp_code(sp.getSp_code());
				PositnSupply ps = commonMISBOHService.findViewPositnSupply(supplyAcct);
				if(null != ps){
					sp.setCnt(ps.getInc0()+"");
				}
			}
			modelMap.put("spbatchList", lists);// 获取出库冲销数据
		}
		modelMap.put("spbatch", spbatch);
		return new ModelAndView(ChkoutMisConstants.TABLE_CHKOUT_CX, modelMap);
	}
	
	/**
	 * 跳转到导入页面   出库单导入 wjf
	 */
	@RequestMapping("/importChkout")
	public ModelAndView importChkout(ModelMap modelMap) throws Exception{
		return new ModelAndView(ChkoutMisConstants.IMPORT_CHKOUT,modelMap);
	}
	
	/**
	 * 下载模板信息 出库单模板下载 wjf
	 */
	@RequestMapping(value = "/downloadTemplate")
	public void downloadTemplate(HttpServletResponse response,HttpServletRequest request) throws IOException {
		chkoutMisService.downloadTemplate(response, request);
	}
	
	/**
	 * 先上传excel
	 */
	@RequestMapping(value = "/loadExcel", method = RequestMethod.POST)
	public ModelAndView loadExcel(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String realFilePath = chkoutMisService.upload(request, response);
		modelMap.put("realFilePath", realFilePath);
		return new ModelAndView(ChkoutMisConstants.IMPORT_RESULT, modelMap);
	}
	
	/**
	 * 导入出库单  wjf
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/importExcel")
	public ModelAndView importExcel(HttpSession session, ModelMap modelMap, @RequestParam String realFilePath)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.IMPORT,
				"MISBOH导入出库单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		String accountId = session.getAttribute("accountId").toString();
		Object obj=chkoutMisService.check(realFilePath, accountId);
		if(obj instanceof Chkoutm){//导入对了
			String typ = "13";
			CodeDes  codes = new CodeDes();		
			codes.setTyp(typ);
			modelMap.put("codeDesList", codeDesService.findCodeDes(codes));//查询所有的报废原因(不分页)
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			((Chkoutm) obj).setVouno(calChkNum.getNextBytable(DDT.VOUNO_CHKOUTM,DDT.VOUNO_CK+thePositn.getCode()+"-",new Date()));
			((Chkoutm) obj).setMadeby(session.getAttribute("accountName").toString());
			((Chkoutm) obj).setChkoutno(chkoutMisService.findNextNo().intValue());
			modelMap.put("positnIn", positnService.findPositn(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"), accountId));
			modelMap.put("positnOut", positnService.findPositn(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"), accountId));
			modelMap.put("chkoutm", ((Chkoutm) obj));
			modelMap.put("curStatus", "add");
			modelMap.put("importFlag","OK");
			return new ModelAndView(ChkoutMisConstants.UPDATE_CHKOUT, modelMap);
		}else{
			modelMap.put("importError", (List<String>)obj);
			return new ModelAndView(ChkoutMisConstants.UPDATE_CHKOUT, modelMap);
		}
	}
	
}
