package com.choice.misboh.web.controller.inout;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.framework.util.ProgramConstants;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.constants.inout.ChkinmZbMisConstants;
import com.choice.misboh.domain.chkstom.MISChkstoDemod;
import com.choice.misboh.domain.chkstom.MISChkstoDemom;
import com.choice.misboh.domain.costReduction.MisSupply;
import com.choice.misboh.service.chkstom.ChkstoDemomMisService;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.misboh.service.inout.ChkinmZbMisService;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.Chkind;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Spbatch;
import com.choice.scm.domain.Spprice;
import com.choice.scm.domain.Supply;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.FirmDeliverService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.SupplyService;
import com.choice.scm.util.CalChkNum;
import com.choice.scm.util.ReadReportUrl;

/**
 * 门店直拨单
 * @author wjf
 *
 */
@Controller
@RequestMapping(value = "chkinmZbMis")
public class ChkinmZbMisController {

	@Autowired
	private ChkinmZbMisService chkinmZbMisService;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private FirmDeliverService firmDeliverService;
	@Autowired
	private CodeDesService codeDesService;
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	@Autowired
	private ChkstoDemomMisService chkstoDemomService;
	@Autowired
	private SupplyService supplyService;
	@Autowired
	private PositnService positnService;
    /**
     * 查询入库单list
     * @param checkOrNot   是否审核
     * @param madedEnd  
     * @param searchDate
     * @param action  init表示初始页面
     * @param inout   zf  表示查询直发    rk  表示rk
     */
	@RequestMapping(value = "/list")
	public ModelAndView findAllChkinm(ModelMap modelMap, String checkOrNot, Chkinm chkinm, Date madedEnd, String startDate, Page page, String action,HttpSession session)
		throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String positnCode="";
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		List<Positn> positnList = new ArrayList<Positn>();
		if (null != thePositn) {
			positnCode = thePositn.getCode();
			positnList.add(thePositn);
			Positn positn = new Positn();
			positn.setCode(positnCode);
			chkinm.setPositn(positn);
		}
	
		if(null!=action && "init".equals(action)){
			Date date=new Date();
			if(null!=startDate && !"".equals(startDate)){
				chkinm.setMaded(DateFormat.getDateByString(startDate, "yyyy-MM-dd"));
			}else{
				chkinm.setMaded(DateFormat.formatDate(date, "yyyy-MM-dd"));
			}
			modelMap.put("chkinmList", chkinmZbMisService.findAllChkinm(checkOrNot,chkinm,page,DateFormat.formatDate(date, "yyyy-MM-dd"),session.getAttribute("locale").toString()));
			modelMap.put("madedEnd", date);
		}else{
			modelMap.put("chkinmList", chkinmZbMisService.findAllChkinm(checkOrNot,chkinm,page,madedEnd,session.getAttribute("locale").toString()));
			modelMap.put("madedEnd", madedEnd);
		}
		modelMap.put("chkinm", chkinm);
		modelMap.put("checkOrNot", checkOrNot);
		modelMap.put("pageobj", page);
		if("check".equals(checkOrNot)){
			return new ModelAndView(ChkinmZbMisConstants.LIST_CHECKED_CHKINM, modelMap);
		}else{
			return new ModelAndView(ChkinmZbMisConstants.LIST_CHKINM, modelMap);
		}
	}
	
	/**
	 * 获取入库单单号
	 * @param maded
	 * @return
	 * @throws ParseException
	 */
	@RequestMapping("getVouno")
	@ResponseBody
	public Object getVouno(HttpSession session, String maded) throws ParseException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		return calChkNum.getNextBytable(DDT.VOUNO_CHKINM,DDT.VOUNO_ZF+thePositn.getCode()+"-",DateFormat.getDateByString(maded, "yyyy-MM-dd"));
	}
	
	/**
	 * 修改跳转
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/update")
	public ModelAndView update(HttpSession session, ModelMap modelMap, Chkinm chkinm, String isCheck) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String locale = session.getAttribute("locale").toString();
		CodeDes codeDes = new CodeDes();
		codeDes.setLocale(locale);
		codeDes.setCodetyp(DDT.CODEDES18ZF);
		modelMap.put("billType", getTypList(codeDes));
		List<Positn> positnList = new ArrayList<Positn>();
		String positnCode = "";
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			positnCode = thePositn.getCode();
			positnList.add(thePositn);
			chkinm.setPositn(thePositn);
		}
		//单据类型
		//判断  当报价为0时候   是否 可以修改入库价格  
		modelMap.put("YNChinmSpprice",ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CHKINM_SPPRICE_IS_YN"));
		modelMap.put("isChkinPriceNot0", DDT.isChkinPriceNot0);//是否验货，入库价格不能为0 西贝用
		modelMap.put("positnList", positnList);//当期门店
		modelMap.put("deliverList",firmDeliverService.findDeliverByPositn(positnCode));//供应商走分店供应商
		modelMap.put("chkinm",chkinmZbMisService.findChkinmByid(chkinm));
		modelMap.put("curStatus", "show");//当前页面状态
		if(null!=isCheck && !"".equals(isCheck)){
			return new ModelAndView(ChkinmZbMisConstants.SEARCH_CHECKED_CHKINMZB,modelMap);
		}else{
			return new ModelAndView(ChkinmZbMisConstants.UPDATE_CHKINMZB,modelMap);
		}
	}
	
	/***
	 * 过滤单据类型
	 * @param codeDes
	 * @return
	 * @throws Exception
	 */
	private List<CodeDes> getTypList(CodeDes codeDes) throws Exception{
		List<CodeDes> codeDesList = codeDesService.findDocumentType(codeDes);
		Iterator<CodeDes> car = codeDesList.iterator();
		while(car.hasNext()){//去掉后面这些： 验货冲消  调拨直发 直配直发 配送直发 分店直发 
			CodeDes cd = car.next();
			if(DDT.ZFYHCX.equals(cd.getCode()) || DDT.DBZF.equals(cd.getCode()) || DDT.ZPZF.equals(cd.getCode()) 
					|| DDT.PSZF.equals(cd.getCode()) || DDT.FDZF.equals(cd.getCode())){
				car.remove();
			}
		}
		return codeDesList;
	}

	/**
	 * 审核入库单  单条审核
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/checkChkinm")
	@ResponseBody
	public Object checkChkinm(ModelMap modelMap, Chkinm chkinm, HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CHECK,
				"MISBOH审核门店直发单:单号("+chkinm.getChkinno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		String acct = session.getAttribute("ChoiceAcct").toString();
		try{
			String accountId = session.getAttribute("accountName").toString();
			String accountNames = session.getAttribute("accountNames").toString();
			chkinm.setChecby(accountId);
			chkinm.setChecbyName(accountNames);
			chkinm.setFirm(((Positn)session.getAttribute("accountPositn")).getCode());
			//1.先判断状态
			int count = commonMISBOHService.findCountByChknos("CHKINM", "CHKINNO", acct, chkinm.getChkinno()+"");
			if(count != 0){
				return "此单已被审核。";
			}
			//2.然后更新状态
			commonMISBOHService.preCheck("CHKINM", "CHKINNO", acct, chkinm.getChkinno()+"");
			//3.真正审核方法
			return chkinmZbMisService.checkChkinm(chkinm,null);
		}catch(Exception e){
			//4.异常返回
			commonMISBOHService.unPreCheck("CHKINM", "CHKINNO", acct, chkinm.getChkinno()+"");
			return e.getLocalizedMessage();
		}
	}
	
	/**
	 * 审核操作  批量审核
	 * @param modelMap
	 * @param chkinno
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/checkAll")
	public ModelAndView checkAll(ModelMap modelMap, String chkinnoids, Chkinm chkinm, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CHECK,
				"MISBOH审核门店直发单:单号("+chkinm.getChkinno()+chkinnoids+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		String acct = session.getAttribute("ChoiceAcct").toString();
		try{
			String accountName = session.getAttribute("accountName").toString();
			chkinm.setChecby(accountName);
			chkinm.setFirm(((Positn)session.getAttribute("accountPositn")).getCode());
			//1.先判断状态
			int count = commonMISBOHService.findCountByChknos("CHKINM", "CHKINNO", acct, chkinnoids);
			if(count != 0){
				return new ModelAndView(StringConstant.ERROR_DONE, modelMap);
			}
			//2.然后更新状态
			commonMISBOHService.preCheck("CHKINM", "CHKINNO", acct, chkinnoids);
			//3.真正审核方法
			chkinmZbMisService.checkChkinm(chkinm,chkinnoids);
			return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
		}catch(Exception e){
			//4.异常返回
			commonMISBOHService.unPreCheck("CHKINM", "CHKINNO", acct, chkinm.getChkinno()+"");
			return new ModelAndView(StringConstant.ERROR_DONE, modelMap);
		}
	}
	
	/**
	 * 删除入库单
	 * @param modelMap
	 * @param chkinno
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete")
	public ModelAndView delete(ModelMap modelMap, HttpSession session,String chkinnoids, String action) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,
				"MISBOH删除门店入库单:单号("+chkinnoids+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		List<String> ids = Arrays.asList(chkinnoids.split(","));
		chkinmZbMisService.deleteChkinm(ids);
		if(!"init".equals(action)){
			return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
		}else{
			modelMap.put("chkinm", null);
			return new ModelAndView(ChkinmZbMisConstants.UPDATE_CHKINMZB, modelMap);
		}
	}
	
	/**
	 * 打印
	 * @param modelMap
	 * @param chkinm
	 * @param type
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/viewChkinm")
	public ModelAndView viewChkinm(ModelMap modelMap,Chkinm chkinm,String type,Page page,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源	
		chkinm=chkinmZbMisService.findChkinmByid(chkinm);
		List<Chkind> chkindList=chkinm.getChkindList();
		modelMap.put("List",chkindList);
 		HashMap<String, Object>  parameters = new HashMap<String, Object>(); 
 		String report_name = "直发单";
        parameters.put("report_name", report_name); 
        if(null!=chkinm.getDeliver() && null!= chkinm.getDeliver().getDes()){
        	parameters.put("deliver.des", chkinm.getDeliver().getDes()); 
        }
        parameters.put("positn.des", chkinm.getPositn().getDes()); 
        parameters.put("vouno", chkinm.getVouno()); 
        parameters.put("maded", DateFormat.getStringByDate(chkinm.getMaded(), "yyyy-MM-dd")); 
        parameters.put("madeby", chkinm.getMadebyName()); 
        parameters.put("checby", chkinm.getChecbyName()); 
        CodeDes codeDes = new CodeDes();
		codeDes.setCode(chkinm.getTyp());
		codeDes.setLocale(session.getAttribute("locale").toString());
		parameters.put("typ", codeDesService.findDocumentByCode(codeDes).getDes());
//	        parameters.put("typ", chkinm.getTyp());
	    HashMap<String, String> map=new HashMap<String, String>();
	    map.put("chkinno", chkinm.getChkinno()+"");
        modelMap.put("parameters", parameters);
        modelMap.put("actionMap", map);
	 	modelMap.put("action", "/chkinmZbMis/viewChkinm.do");//传入回调路径
 		Map<String,String> rs=ReadReportUrl.redReportUrl(type,ChkinmZbMisConstants.REPORT_URL_ZB,ChkinmZbMisConstants.REPORT_URL_ZB);//判断跳转路径
 		modelMap.put("reportUrl", rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
        
	}
	
	/**
	 * 转入直拨单添加页面    
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/addzb")
	public ModelAndView addzb(ModelMap modelMap, HttpSession session, String action)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String positnCode="";
		if(!"init".equals(action)){
			String accountName=session.getAttribute("accountName").toString();
			String accountNames=session.getAttribute("accountNames").toString();
			String locale = session.getAttribute("locale").toString();
			CodeDes codeDes = new CodeDes();
			codeDes.setLocale(locale);
			codeDes.setCodetyp(DDT.CODEDES18ZF);
			modelMap.put("billType", getTypList(codeDes));
			Chkinm chkinm=new Chkinm();
			// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			List<Positn> positnList = new ArrayList<Positn>();
			if (null != thePositn) {
				positnCode = thePositn.getCode();
				positnList.add(thePositn);
				chkinm.setPositn(thePositn);
			}
			chkinm.setMadeby(accountName);
			chkinm.setMadebyName(accountNames);
			chkinm.setMaded(new Date());
			chkinm.setVouno(calChkNum.getNextBytable(DDT.VOUNO_CHKINM,DDT.VOUNO_ZF+thePositn.getCode()+"-",new Date()));
			chkinm.setChkinno(chkinmZbMisService.getMaxChkinno());
			modelMap.put("chkinm",chkinm);//日期等参数
			
			modelMap.put("positnList", positnList);//当期门店

			modelMap.put("deliverList",firmDeliverService.findDeliverByPositn(positnCode));//供应商走分店供应商
			modelMap.put("curStatus", "add");//当前页面状态
		}
		modelMap.put("isChkinPriceNot0", DDT.isChkinPriceNot0);//是否验货，入库价格不能为0 西贝用
		return new ModelAndView(ChkinmZbMisConstants.UPDATE_CHKINMZB,modelMap);	
	}
	
	/**
	 * 修改直拨单
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/updateChkinmzb")
	@ResponseBody
	public int updateChkinmzb(ModelMap modelMap,HttpSession session,Chkinm chkinm)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,
				"MISBOH修改门店入库单:单号("+chkinm.getChkinno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		Chkinm chkinms=chkinmZbMisService.findChkinmByid(chkinm);
		chkinm.setAcct(session.getAttribute("ChoiceAcct").toString());//帐套
		Integer  result=0;
		try{
			commonMISBOHService.getOnlyAccountMonth2(chkinm.getMaded());//判断制单日期会计日
		}catch(CRUDException e1){
			return -9;
		}
		if(null!=chkinms){//存在则修改
			chkinm.setInout(DDT.INOUT);
			result=chkinmZbMisService.updateChkinm(chkinm);
		}else{//不存在则添加   
			chkinm.setInout(DDT.INOUT);
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			chkinm.setVouno(calChkNum.getNextBytable(DDT.VOUNO_CHKINM,DDT.VOUNO_ZF+thePositn.getCode()+"-",chkinm.getMaded()));
			result=chkinmZbMisService.saveChkinm(chkinm,"zb");
		}
		return result;	
	}

	/**
	 * 检测要保存、删除的入库单据是否被审核
	 * @param modelMap
	 * @param session
	 * @param chkinm
	 * @return
	 * @throws Exception
	 * @author ZGL
	 */
	@RequestMapping(value="/chkChect")
	@ResponseBody
	public String chkChect(ModelMap modelMap, HttpSession session, String active, Chkinm chkinm)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chkinm.setAcct(session.getAttribute("ChoiceAcct").toString());//帐套
		return chkinmZbMisService.chkChect(active,chkinm);
	}
	
	/**
	 * ajax查询分店信息
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/findAllPositnDeptN", method=RequestMethod.POST)
	@ResponseBody
	public List<Positn> findAllPositnDeptN(Positn positn, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		positn.setAcct(session.getAttribute("ChoiceAcct").toString());
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			String positnCode = thePositn.getCode();
			positn.setUcode(positnCode);
			return chkinmZbMisService.findAllPositnDeptN(positn);//根据门店编码获取所有部门前十条
		}else{
			return null;
		}
	}
	
	/**
	 * 直发物资冲消查询界面
	 * @author 2014.11.4wjf
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkinmzfByCx")
	public ModelAndView addChkinmzfByCx(ModelMap modelMap, HttpSession session, Spbatch spbatch, String action) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (spbatch.getBdat() == null) {
			spbatch.setBdat(DateFormat.getDateBefore(DateFormat.formatDate(new Date(), "yyyy-MM-dd"),"day",-1,1));//昨天的数据
		}
		if (spbatch.getEdat() == null) {
			spbatch.setEdat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		}
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			spbatch.setPositn(thePositn.getCode());
		}
		if(!"init".equals(action)){
			modelMap.put("spbatchList", chkinmZbMisService.addChkinmzfByCx(spbatch));//获取直发数据
		}
		modelMap.put("spbatch", spbatch);
		return new ModelAndView(ChkinmZbMisConstants.TABLE_CHKINMZF_CX,modelMap);
	}
	
	/***
	 * 直发模板调用
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param chkstodemo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkinzbDemo")
	public ModelAndView addChkinzbDemo(ModelMap modelMap, HttpSession session, String deliver, String madet, Integer chkstodemono) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			String positnCode = thePositn.getCode();
			Positn positn = new Positn();
			positn.setUcode(positnCode);
			modelMap.put("positnList", positnService.findDeptByPositn(positn));//根据门店编码获取所有部门
		}
		String acct = session.getAttribute("ChoiceAcct").toString();
		MISChkstoDemom chkstodemo = new MISChkstoDemom();
		chkstodemo.setAcct(acct);
		chkstodemo.setTempTyp("RK");
		chkstodemo.setFirm(thePositn.getCode());
		chkstodemo.setChkstodemono(chkstodemono);
		List<MISChkstoDemom> listChkstoDemo = chkstoDemomService.listChkstoDemom(chkstodemo);
		if(listChkstoDemo.size() > 0) {
			MISChkstoDemod chkstoDemod = new MISChkstoDemod();
			chkstoDemod.setScode(thePositn.getCode());
			chkstoDemod.setChkstodemono(listChkstoDemo.get(0).getChkstodemono());
			List<MISChkstoDemod> chkstodemoList = chkstoDemomService.listChkstoDemodd(chkstoDemod);
			//查询报价
			Spprice price = new Spprice();
			price.setAcct(acct);
			price.setDeliver(deliver);
			price.setArea(chkstodemo.getFirm());
			price.setMadet(madet);
			for(MISChkstoDemod ckd : chkstodemoList){
				price.setSp_code(ckd.getSupply().getSp_code());
				Spprice sp = supplyService.findBprice(price);
				if (null == sp||null == sp.getPrice()) {
					Supply supply = new Supply();
					supply.setAcct(acct);
					supply.setSp_code(ckd.getSupply().getSp_code());
					supply = supplyService.findSupplyById(supply);
					if (supply.getSp_price()!=null) {
						ckd.getSupply().setSp_price(supply.getSp_price());
					}
				}else {
					ckd.getSupply().setSp_price(sp.getPrice().doubleValue());
				}
				//2.查税率
				if(null != price.getDeliver()){
					MisSupply ms = new MisSupply();
					ms.setAcct(acct);
					ms.setSp_code(price.getSp_code());
					ms.setDeliver(price.getDeliver());
					ms = commonMISBOHService.findTaxByDeliverSpcode(ms);
					ckd.getSupply().setTax(ms.getTax());
					ckd.getSupply().setTaxdes(ms.getTaxdes());
				}
			}
			modelMap.put("chkstodemoList", chkstodemoList);
		}
		modelMap.put("deliver", deliver);
		modelMap.put("madet", madet);
		modelMap.put("chkstodemono", chkstodemono);
		//最后查标题栏
		chkstodemo.setChkstodemono(null);
		modelMap.put("listTitle", chkstoDemomService.listChkstoDemom(chkstodemo));//改为查带适用分店权限的2014.11.18wjf
		return new ModelAndView(ChkinmZbMisConstants.ADD_CHKINZBDEMO, modelMap);
	}
}
