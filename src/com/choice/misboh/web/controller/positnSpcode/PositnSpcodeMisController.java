package com.choice.misboh.web.controller.positnSpcode;

import java.net.URLEncoder;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.constants.positnSpcode.PositnSpcodeMisConstants;
import com.choice.misboh.service.positnSpcode.PositnSpcodeMisService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.PositnSpcode;

/**
 * 分店物资属性
 * @author wjf
 *
 */
@Controller
@RequestMapping("positnSpcodeMis")
public class PositnSpcodeMisController {
	@Autowired
	private PositnSpcodeMisService positnSpcodeMisService;
	
	/***
	 * 分店物资属性
	 * @param session
	 * @param modelMap
	 * @param positnSpcode
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/table")
	public ModelAndView table(HttpSession session, ModelMap modelMap, PositnSpcode positnSpcode, Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn positn = (Positn)session.getAttribute("accountPositn");
		positnSpcode.setPositn(positn.getCode());
		if("Y".equals(DDT.isPositnSpcodeJmj)){//九毛九的物资查询页面
			modelMap.put("listFirmSupply",positnSpcodeMisService.findPositnSpcodeJmj(positnSpcode, page));
		}else{
			modelMap.put("listFirmSupply",positnSpcodeMisService.findPositnSpcode(positnSpcode, page));
		}
		modelMap.put("pageobj", page);
		modelMap.put("positnSpcode", positnSpcode);
		modelMap.put("isDistributionUnit", DDT.isDistributionUnit);//是否根据配送单位报货
		modelMap.put("isPositnSpcodeJmj", DDT.isPositnSpcodeJmj);
		return new ModelAndView(PositnSpcodeMisConstants.TABLE_POSITNSPCODE, modelMap);
	}
	
	/***
	 * 导出分店物资属性  EXCEL
	 * @param session
	 * @param response
	 * @param request
	 * @param positnSpcode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/exportPositnSpcode")
	@ResponseBody
	public boolean exportPositnSpcode(HttpSession session,HttpServletResponse response,HttpServletRequest request,PositnSpcode positnSpcode) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn positn = (Positn)session.getAttribute("accountPositn");
		positnSpcode.setPositn(positn.getCode());
		List<PositnSpcode> positnSpcodeList = null;
		if("Y".equals(DDT.isPositnSpcodeJmj)){//九毛九的物资查询页面
			positnSpcodeList = positnSpcodeMisService.findPositnSpcodeJmj(positnSpcode);
		}else{
			positnSpcodeList = positnSpcodeMisService.findPositnSpcode(positnSpcode);
		}
		String fileName = "分店物资属性";
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		
		return positnSpcodeMisService.exportPositnSpcode(response.getOutputStream(),positn.getDes(),positnSpcodeList);
//		return false;
	}
}
