package com.choice.misboh.web.controller.chkstom;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.redis.RedisConfig;
import com.choice.framework.util.CacheUtil;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.framework.util.ProgramConstants;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.constants.chkstom.ChkstomForecastConstants;
import com.choice.misboh.constants.salesforecast.ForecastConstants;
import com.choice.misboh.domain.chkstom.ChkstomForecast;
import com.choice.misboh.domain.chkstom.MISChkstoDemod;
import com.choice.misboh.domain.chkstom.MISChkstoDemom;
import com.choice.misboh.domain.chkstom.ScheduleD;
import com.choice.misboh.domain.declareGoodsGuide.Declare;
import com.choice.misboh.domain.declareGoodsGuide.DeclareGoodsGuide;
import com.choice.misboh.domain.salesforecast.FirmItemUse;
import com.choice.misboh.domain.salesforecast.ItemPlan;
import com.choice.misboh.domain.salesforecast.SalePlan;
import com.choice.misboh.domain.salesforecast.SpCodeUseMis;
import com.choice.misboh.service.chkstom.ChkstoDemomMisService;
import com.choice.misboh.service.chkstom.ChkstomDeptMisService;
import com.choice.misboh.service.chkstom.ChkstomForecastService;
import com.choice.misboh.service.chkstom.ChkstomMisService;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.misboh.service.salesforecast.ItemUseService;
import com.choice.misboh.service.salesforecast.PosItemPlanService;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Chkstom;
import com.choice.scm.domain.Positn;
import com.choice.scm.service.MainInfoService;
import com.choice.scm.service.PositnService;
import com.choice.scm.util.CalChkNum;

/**
 * 报货向导
 * @author wjf
 *
 */
@Controller
@RequestMapping(value = "/chkstomForecast")
public class ChkstomForecastController {

    @Autowired
	private CalChkNum calChkNum;
    @Autowired
    private MainInfoService mainInfoService;
    @Autowired
    private PositnService positnService;
    @Autowired
    private CommonMISBOHService commonMISBOHService;
    
    @Autowired
	private ChkstomMisService chkstomMisService;
    @Autowired
	private ChkstomDeptMisService chkstomDeptMisService;
    @Autowired
    private ChkstoDemomMisService chkstoDemomService;
    
    @Autowired
	private ChkstomForecastService chkstomForecastService;
    
    @Autowired
	private ItemUseService itemUseService;
    @Autowired
	private PosItemPlanService posItemPlanService;

    private static String opencluster = RedisConfig.getString("opencluster");
    
    /***
     * 进入报货向导
     * @param modelMap
     * @param session
     * @param cf
     * @return
     */
    @RequestMapping(value = "/toChkstomForecast")
    public ModelAndView toChkstomForecast(ModelMap modelMap,HttpSession session,ChkstomForecast cf){
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
    	cf.setIsDistributionUnit(DDT.isDistributionUnit);
    	if(null != opencluster && "1".equals(opencluster)){
    		session.removeAttribute("cf_cache");
    		session.setAttribute("cf_cache", cf);
    	} else {
    		CacheUtil cacheUtil = CacheUtil.getInstance();
    		cacheUtil.flush("cf_cache", session.getId());
    		cacheUtil.put("cf_cache", session.getId(), cf);
    	}
    	modelMap.put("cf",cf);
		return new ModelAndView(ChkstomForecastConstants.LISTCHKSTOMFORECAST,modelMap);
    }
    
    /***
     * 退出报货向导
     * @param session
     */
    @RequestMapping(value = "/closeChkstomForecast")
    @ResponseBody
    public void closeChkstomForecast(HttpSession session){
    	if(null != opencluster && "1".equals(opencluster)){
    		session.removeAttribute("cf_cache");
    	} else {
    		CacheUtil cacheUtil = CacheUtil.getInstance();
    		cacheUtil.flush("cf_cache", session.getId());
    	}
    }
    
    /***
     * 得到cf_cache
     * @param session
     * @return
     */
    private ChkstomForecast getCfCache(HttpSession session){
    	if(null != opencluster && "1".equals(opencluster)){
    		return (ChkstomForecast)session.getAttribute("cf_cache");
    	} else {
    		CacheUtil cacheUtil = CacheUtil.getInstance();
    		return (ChkstomForecast)cacheUtil.get("cf_cache", session.getId());
    	}
    }
    
    /***
     * 设置cf_cache
     * @param session
     * @param cf
     */
    private void saveCfCache(HttpSession session, ChkstomForecast cf){
    	if(null != opencluster && "1".equals(opencluster)){
    		session.setAttribute("cf_cache", cf);
    	}
    }
    
    /********************************************左侧页面刷新******************************************************/
    
    /***
     * 报货向导左侧界面
     * @param modelMap
     * @param session
     * @return
     */
    @RequestMapping(value = "/listLeftCf")
    public ModelAndView listLeftCf(ModelMap modelMap,HttpSession session){
    	modelMap.put("leftList",getLeftList(session));
		return new ModelAndView(ChkstomForecastConstants.LEFT_CHKSTOMFORECAST,modelMap);
    }
    
    /***
     * ajax刷新左侧
     * @param session
     * @return
     */
    @RequestMapping(value = "/getLeftList")
    @ResponseBody
    public List<ChkstomForecast> getLeftList(HttpSession session) {
    	ChkstomForecast cf = getCfCache(session);
    	List<ChkstomForecast> list = new ArrayList<ChkstomForecast>();
		list.add(new ChkstomForecast("选择订货日","/chkstomForecast/listRightCf.do"));
		//判断
		int sta = cf.getSta();//0 报货向导  1千元用量  2估清 3安全库存 4周期平均
		if(sta == 0){
			if(null == cf.getCodetyp()){
				list.add(new ChkstomForecast("调整预估值","/chkstomForecast/listAdjustTheEstimated.do"));
				list.add(new ChkstomForecast("千元用量","/chkstomForecast/listSpcodeUse.do"));
				if("N".equals(DDT.isForecastByTime)){//不分餐次预估 ，西贝走Y
					list.add(new ChkstomForecast("菜品点击率","/chkstomForecast/itemUselist.do?mis=1&isDeclare=1"));
					list.add(new ChkstomForecast("菜品销售计划","/chkstomForecast/planList.do"));
				} else {
					list.add(new ChkstomForecast("菜品销售计划","/chkstomForecast/planListChoice3.do"));
				}
				list.add(new ChkstomForecast("报货模板","/chkstomForecast/addChkstoDemo.do"));
			}else{
				if(cf.getCodetyp().contains(DDT.SHIPMENT_QR+"")|| cf.getCodetyp().contains(DDT.SHIPMENT_QY+"")//包含千元,千人用量
						|| cf.getCodetyp().contains(DDT.SHIPMENT_CPDJL+"") || cf.getCodetyp().contains(DDT.SHIPMENT_CPXSJH+"")){//菜品点击率 菜品销售计划
					list.add(new ChkstomForecast("调整预估值","/chkstomForecast/listAdjustTheEstimated.do"));
					if(cf.getCodetyp().contains(DDT.SHIPMENT_QR+"")|| cf.getCodetyp().contains(DDT.SHIPMENT_QY+"")){//包含千元,千人用量
						list.add(new ChkstomForecast("千元用量","/chkstomForecast/listSpcodeUse.do"));
					}
					if(cf.getCodetyp().contains(DDT.SHIPMENT_CPDJL+"") || cf.getCodetyp().contains(DDT.SHIPMENT_CPXSJH+"")){//菜品点击率 菜品销售计划
						if("N".equals(DDT.isForecastByTime)){
							list.add(new ChkstomForecast("菜品点击率","/chkstomForecast/itemUselist.do?mis=1&isDeclare=1"));
							list.add(new ChkstomForecast("菜品销售计划","/chkstomForecast/planList.do"));
						} else {
							list.add(new ChkstomForecast("菜品销售计划","/chkstomForecast/planListChoice3.do"));
						}
					}
				}
				if(cf.getCodetyp().contains(DDT.SHIPMENT_SGMB+"")){
					list.add(new ChkstomForecast("报货模板","/chkstomForecast/addChkstoDemo.do"));
				}
			}
		}else if(sta == 1){//千元用量
			list.add(new ChkstomForecast("调整预估值","/chkstomForecast/listAdjustTheEstimated.do"));
			list.add(new ChkstomForecast("千元用量","/chkstomForecast/listSpcodeUse.do"));
		}else if(sta == 2){//沽清
			list.add(new ChkstomForecast("调整预估值","/chkstomForecast/listAdjustTheEstimated.do"));
			if("N".equals(DDT.isForecastByTime)){
				list.add(new ChkstomForecast("菜品点击率","/chkstomForecast/itemUselist.do?mis=1&isDeclare=1"));
				list.add(new ChkstomForecast("菜品销售计划","/chkstomForecast/planList.do"));
			} else {
				list.add(new ChkstomForecast("菜品销售计划","/chkstomForecast/planListChoice3.do"));
			}
		}
		list.add(new ChkstomForecast("生成报货单","/chkstomForecast/toSaveChkstom.do"));
		saveCfCache(session, cf);
		return list;
    }
    
    /********************************************右侧第一个页面：选择订货日******************************************************/
    
    /***
     * 报货向导右侧界面
     * @param modelMap
     * @param session
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/listRightCf")
    public ModelAndView listRightCf(ModelMap modelMap,HttpSession session,Date maded) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		ChkstomForecast cf = getCfCache(session);
		String yearr = mainInfoService.findYearrList().get(0);//会计年wjf
		cf.setYearr(yearr);
		if(maded == null){//默认进来当前日期   如果启用配送班表的，选了日期后 刷新界面
			if(cf.getMaded() == null)
				cf.setMaded(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		}else{
			cf.setMaded(maded);
		}
    	Positn thePositn = (Positn)session.getAttribute("accountPositn");
    	Positn positn = positnService.findPositnByCode(thePositn);
    	cf.setFirm(thePositn.getCode());
    	cf.setIsdept(positn.getYnUseDept());//是否启用档口
    	cf.setIsuseschedule(positn.getVisuseschedule());//是否启用配送班表
    	cf.setVplantyp(positn.getVplantyp());//预估类型
    	cf.setVfoodsign("2");//餐饮类型，直接查第三方，不用管什么类型
		//读取订货日选择控制
		String orderDayControl = ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH,"OrderDayControl");
		cf.setOrderDayControl(orderDayControl);

		//获取报货类别
		List<ScheduleD> codetypList = chkstomForecastService.getCodetypList(cf);
		String isNotUpdateHoped = DDT.isNotUpdateHoped;//到货日，下次到货日不能改  默认N可以改  不能改Y 只有西贝用
		modelMap.put("isNotUpdateHoped", isNotUpdateHoped);
		if("Y".equals(isNotUpdateHoped)){
			/**原需求：如果报货日期是当天14:00之前，报货日是当天，如果报货时间是当天14:00之后 ，报货日是下一天
			 * 现需求：第二天凌晨2点之前报货都是当天，2点之后才是第二天    也就是0点到1:59点报货日期是前一天，2点到23:59日期是当天
			 */
			Date nowDate = new Date();
			String now = DateFormat.getStringByDate(nowDate, "yyyy-MM-dd");
			if(!DateJudge.timeCompareDate(DateJudge.HH_mm.format(nowDate), "00:00") && DateJudge.timeCompareDate(DateJudge.HH_mm.format(nowDate), "02:00")){
				now = DateJudge.getLenTime(-1, now);
				nowDate = DateFormat.getDateByString(now, "yyyy-MM-dd");
			}
			cf.setMaded(nowDate);
			for(ScheduleD sd : codetypList){
				sd.setReceiveDate(DateJudge.getLenTime(1, now));
				sd.setNextReceiveDate(DateJudge.getLenTime(2, now));
			}
		}
		modelMap.put("codetypList",codetypList);
		
		// 验证门店报货单据数量
		String DailyGoodsCnt=ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH,"DailyGoodsCnt");
		String flag ="-1";
		if(!"0".equals(DailyGoodsCnt)){
			cf.setDailyGoodsCnt(DailyGoodsCnt);
			flag = chkstomForecastService.getChkstomCountByCodetyp(cf);
		}
		modelMap.put("flag", flag);
		modelMap.put("cf",cf);
		
		//如果是上一步，将原来的值带上
		if(null != cf.getSdList() && cf.getSdList().size() != 0){
			for(ScheduleD sd1 : cf.getSdList()){
				for(ScheduleD sd2 : codetypList){
					if(sd1.getCategory_Code().equals(sd2.getCategory_Code())){
						sd2.setReceiveDate(sd1.getReceiveDate());
						sd2.setNextReceiveDate(sd1.getNextReceiveDate());
					}
				}
			}
		}
		saveCfCache(session, cf);
		return new ModelAndView(ChkstomForecastConstants.RIGHT_CHKSTOMFORECAST,modelMap);
    }
    
    /***
     * 确认当前时间是否可订货
     * @param modelMap
     * @param session
     * @param dat
     * @return
     */
    @RequestMapping(value = "/checkOrderTime")
    @ResponseBody
    public String checkOrderTime(ModelMap modelMap,HttpSession session,String dat) {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
    	Positn thePositn = (Positn)session.getAttribute("accountPositn");
    	String orderTime = chkstomMisService.eqOrderTime(dat, thePositn.getCode());
    	return orderTime;
    }
    
    /***
     * 保存报货类别  并判断是否做了营业预估
     * @param modelMap
     * @param session
     * @param cf1
     * @return
     */
    @RequestMapping(value = "/saveCodetyp")
    @ResponseBody
    public String saveCodetyp(ModelMap modelMap,HttpSession session,ChkstomForecast cf1) throws Exception {
    	ChkstomForecast cf = getCfCache(session);
    	cf.setMaded(cf1.getMaded());
    	if(null != cf1.getDept() && !"".equals(cf1.getDept())){
    		cf.setDept(cf1.getDept());
    		cf.setDeptName(cf1.getDeptName());
    	}
    	cf.setSdList(cf1.getSdList());
    	StringBuffer sb = new StringBuffer();
    	for(ScheduleD sd:cf1.getSdList()){
    		sb.append(sd.getCodetyp()+",");
    	}
    	cf.setCodetyp(sb.substring(0, sb.length()-1));//设置报货方式1,2,3,4等等
    	
    	//得到开始日期  结束日期=有预估报货的最后一个到货日
    	Date edate = cf.getMaded();
    	for(ScheduleD sd:cf.getSdList()){
    		if(sd.getCodetyp() == DDT.SHIPMENT_QR || sd.getCodetyp() == DDT.SHIPMENT_QY //包含千元,千人用量
					|| sd.getCodetyp() == DDT.SHIPMENT_CPDJL || sd.getCodetyp() == DDT.SHIPMENT_CPXSJH){//菜品点击率 菜品销售计划
    			Date nextDate = DateFormat.getDateByString(sd.getNextReceiveDate(),"yyyy-MM-dd");
    			Calendar calendar = Calendar.getInstance();
    			calendar.setTime(nextDate);
				calendar.add(Calendar.DATE,-1);
				nextDate = calendar.getTime();
    			if(sd.getDays() > 0){
    				calendar.add(Calendar.DATE,sd.getDays());
    			    nextDate = calendar.getTime();
    			}
    			if(nextDate.getTime() > edate.getTime()){
    				edate = nextDate;
    			}
    		}
    	}
    	Date bdate = edate;
    	for(ScheduleD sd:cf.getSdList()){
    		if(sd.getCodetyp() == DDT.SHIPMENT_QR || sd.getCodetyp() == DDT.SHIPMENT_QY //包含千元,千人用量
					|| sd.getCodetyp() == DDT.SHIPMENT_CPDJL || sd.getCodetyp() == DDT.SHIPMENT_CPXSJH){//菜品点击率 菜品销售计划
    			Date ind = DateFormat.getDateByString(sd.getReceiveDate(),"yyyy-MM-dd");
    			if(sd.getDays() > 0){
    				Calendar calendar = Calendar.getInstance();
    			    calendar.setTime(ind);
    			    calendar.add(Calendar.DATE,sd.getDays());
    			    ind = calendar.getTime();
    			}
    			if(ind.getTime() < bdate.getTime()){
    				bdate = ind;
    			}
    		}
    	}
    	//查询预估值
    	SalePlan salePlan = new SalePlan();
    	salePlan.setStartdate(bdate);
    	salePlan.setEnddate(edate);
    	salePlan.setFirm(cf.getFirm());
    	List<SalePlan> salePlanList = chkstomForecastService.listAdjustTheEstimated(salePlan);
    	//提示哪天没做预估
    	List<String> datList = DateJudge.getDateList(DateFormat.getStringByDate(bdate, "yyyy-MM-dd"),DateFormat.getStringByDate(edate, "yyyy-MM-dd"));
    	Iterator<String> car = datList.iterator();
    	while(car.hasNext()){
    		Date date = DateFormat.getDateByString(car.next(), "yyyy-MM-dd");
			for (SalePlan sp : salePlanList) {
				if(date.getTime() == sp.getDat().getTime()){
					car.remove();
				}
			}
    	}
		cf.setSalePlanList(salePlanList);
		saveCfCache(session, cf);
		if(datList.size() > 0){
			return datList.toString();
		}
    	return "0";
    }
    
    /******************************************右侧最后一个页面：生成报货单********************************************************/
    
    /***
     * 进入生成报货单页面
     * @param modelMap
     * @param session
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/toSaveChkstom")
    public ModelAndView toSaveChkstom(ModelMap modelMap,HttpSession session) throws CRUDException{
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
    	ChkstomForecast cf = getCfCache(session);
    	//报货向导预估量计算是否减去库存，在途  是否向上取整
    	String qyylCalWay = cf.getQyylCalWay();
    	String xsjhCalWay = cf.getXsjhCalWay();
    	for(ScheduleD sd:cf.getSdList()){
    		if(cf.getSta() == 1){
    			sd.setCodetyp(DDT.SHIPMENT_QY);
			}else if(cf.getSta() == 2){
				sd.setCodetyp(DDT.SHIPMENT_CPXSJH);
			}else if(cf.getSta() == 3){
				sd.setCodetyp(DDT.SHIPMENT_AQKC);
			}else if(cf.getSta() == 4){//周期平均
				sd.setCodetyp(DDT.SHIPMENT_ZCPJ);
			}
    		sd.setYearr(cf.getYearr());
    		if(null != cf.getDept() && !"".equals(cf.getDept())){//如果启用档口的
    			sd.setChkstono(chkstomDeptMisService.getMaxChkstono());
    		}else{
    			sd.setChkstono(chkstomMisService.getMaxChkstono());
    		}
    		sd.setFirm(cf.getFirm());
    		sd.setDept(cf.getDept());
    		//安全库存报货  报货量 = 安全库存-库存-在途
    		if(sd.getCodetyp() == DDT.SHIPMENT_AQKC){
    			List<Declare> declareList = chkstomForecastService.getSafeStockList(sd);
    			calSpuse(declareList);
    			sd.setDeclareList(declareList);
    		}
    		//周期平均用量 报货量 = 报货日期是周几的前三个周几的平均值
    		if(sd.getCodetyp() == DDT.SHIPMENT_ZCPJ){
    			sd.setMaded(cf.getMaded());
    			List<Declare> declareList = chkstomForecastService.getAvgWeekList(sd);
    			calSpuse(declareList);
    			sd.setDeclareList(declareList);
    		}
    		//直接填写申购单  显示所有此类别下的分店物资属性的物资
    		if(sd.getCodetyp() == DDT.SHIPMENT_ZJSGD){
    			List<Declare> declareList = chkstomForecastService.getAddChkstomList(sd);
    			sd.setDeclareList(declareList);
    		}
    		//选择报货模板  赋值附上选择的模板
    		if(sd.getCodetyp() == DDT.SHIPMENT_SGMB){
    			List<Declare> declareList = chkstomForecastService.getAddChkstomList(sd);
    			if(null != sd.getDeclareList() && sd.getDeclareList().size() > 0){
	    			for(Declare d1 : sd.getDeclareList()){
	    				for(Declare d2 : declareList){
	    					if(d1.getSp_code().equals(d2.getSp_code())){
	    						d1.setSp_name(d2.getSp_name());
	    						d1.setSp_desc(d2.getSp_desc());
	    						d1.setUnit(d2.getUnit());
	    						d1.setUnit3(d2.getUnit3());
	    						d1.setUnitper3(d2.getUnitper3());
	    						d1.setMincnt(d2.getMincnt());
	    						d1.setStomin(d2.getStomin());
	    						d1.setStocnt(d2.getStocnt());
	    						d1.setSpmin(d2.getSpmin());
	    						d1.setSpnow(d2.getSpnow());
	    						d1.setSpway(d2.getSpway());
	    						d1.setSpcal(0);
	    						d1.setDisunit(d2.getDisunit());
	    						d1.setDisunitper(d2.getDisunitper());
	    						d1.setDismincnt(d2.getDismincnt());
	    						break;
	    					}
	    				}
	    			}
    			}
    		}
    		
    		//千人用量 千元用量     计算量 = 周期用量 +安全库存 - 当前库存 - 在途       (考虑最小申购量)   目前先直接等于周期用量
    		if(sd.getCodetyp() == DDT.SHIPMENT_QR || sd.getCodetyp() == DDT.SHIPMENT_QY){
                List<Declare> declareList = new ArrayList<Declare>();//存放当前报货类别下的物资
                if(null != sd.getDept()){
                    String[] deptList = sd.getDept().split(",");
                    for(String dept : deptList){
                        sd.setDept(dept);
                        List<Declare> allList = chkstomForecastService.getAddChkstomList(sd);//当前类别下的所有物资
                        List<Declare> qyylList = cf.getQyylList();
                        for(Declare d1 : qyylList){
                        	if(dept.equals(d1.getPositn())){
                        		for(Declare d2 : allList){
                        			d2.setPositn(d1.getPositn());
            						d2.setPositndes(d1.getPositndes());
                        			if(d1.getSp_code().equals(d2.getSp_code())){
                        				double spcal = d1.getPeriodUse();
                        				if(spcal < 0.01){
                        					d2.setSpcal(0);
                        					d2.setSpuse(0);
                        					d2.setAmountdis(0);
                        					declareList.add(d2);
                            				break;
                        				}else{
                        					d2.setSpcal(spcal);
                        					if("Y".equals(qyylCalWay)){//减去库存和在途   计算量取整
                        						spcal = d1.getPeriodUse() + d2.getSpmin() - d2.getSpnow() - d2.getSpway();//计算量 = 周期用量 +安全库存 - 当前库存 - 在途
                        						if(spcal < 0.01){
                                					d2.setSpuse(0);
                                					d2.setAmountdis(0);
                                					declareList.add(d2);
                                    				break;
                        						}
                        					}
                        					//采购单位的
                        					double spuse = Math.ceil(spcal * d2.getUnitper3());
                        					if(d2.getStomin() != 0 && spuse < d2.getStomin()){//如果需求量小于最小申购量 ，则等于最小申购量
                        						spuse = d2.getStomin();
                        					}
                        					if(d2.getStocnt() != 0){
                        						spuse = Math.ceil(spuse / d2.getStocnt())*d2.getStocnt();
                        					}else{
                        						if(d2.getMincnt() != 0){
                        							spuse = Math.ceil(spuse / d2.getMincnt())*d2.getMincnt();
                        						}
                        					}
                        					d2.setSpuse(spuse);
                        					//配送单位的
                        					double amountdis = Math.ceil(spcal * d2.getDisunitper());
                        					if(d2.getDismincnt() != 0)
                        						amountdis = Math.ceil(amountdis / d2.getDismincnt())*d2.getDismincnt();
                        					d2.setAmountdis(amountdis);
                        				}
                        				declareList.add(d2);
                        				break;
                        			}
                        		}
                        		
                        	}
                        }
                    }
                }else{
                    List<Declare> allList = chkstomForecastService.getAddChkstomList(sd);//当前类别下的所有物资
                    List<Declare> qyylList = cf.getQyylList();
                    for(Declare d1 : qyylList){
                        for(Declare d2 : allList){
                            if(d1.getSp_code().equals(d2.getSp_code())){
                                double spcal = d1.getPeriodUse();
                                if(spcal < 0.01){
        							d2.setSpcal(0);
                					d2.setSpuse(0);
                					d2.setAmountdis(0);
                					declareList.add(d2);
                    				break;
        						}else{
                					d2.setSpcal(spcal);
                					if("Y".equals(qyylCalWay)){//减去库存和在途   计算量取整
                						spcal = d1.getPeriodUse() + d2.getSpmin() - d2.getSpnow() - d2.getSpway();//计算量 = 周期用量 +安全库存 - 当前库存 - 在途
                						if(spcal < 0.01){
                        					d2.setSpuse(0);
                        					d2.setAmountdis(0);
                        					declareList.add(d2);
                            				break;
                						}
                					}
                					//采购单位的
                					double spuse = Math.ceil(spcal * d2.getUnitper3());
                                    if(d2.getStomin() != 0 && spuse < d2.getStomin()){//如果需求量小于最小申购量 ，则等于最小申购量
                                        spuse = d2.getStomin();
                                    }
                                    if(d2.getStocnt() != 0){
                                        spuse = Math.ceil(spuse / d2.getStocnt())*d2.getStocnt();
                                    }else{
                                        if(d2.getMincnt() != 0){
                                            spuse = Math.ceil(spuse / d2.getMincnt())*d2.getMincnt();
                                        }
                                    }
                                    d2.setSpuse(spuse);
                                    //配送单位的
                                    double amountdis = Math.ceil(spcal * d2.getDisunitper());
                					if(d2.getDismincnt() != 0)
                						amountdis = Math.ceil(amountdis / d2.getDismincnt())*d2.getDismincnt();
                					d2.setAmountdis(amountdis);
                                }
                                declareList.add(d2);
                                break;
                            }
                        }
                    }
                }
                sd.setDeclareList(declareList);
            }
    		
			//菜品点击率 菜品销售计划 计算量 = 周期用量 +安全库存 - 当前库存 - 在途   (考虑最小申购量)
			if(sd.getCodetyp() == DDT.SHIPMENT_CPDJL || sd.getCodetyp() == DDT.SHIPMENT_CPXSJH){
				String bdate = DateJudge.getLastTime(-sd.getDays(), sd.getReceiveDate());
				String maxEndDate = DateJudge.getLastTime(-sd.getDays()+1, sd.getNextReceiveDate());
				List<Declare> declareList = new ArrayList<Declare>();//存放当前报货类别下的物资
				if(null != sd.getDept()){
					String[] deptList = sd.getDept().split(",");
					for(String dept : deptList){
						sd.setDept(dept);
						Positn p = new Positn();
						p.setCode(dept);
						p = positnService.findPositnByCode(p);
						List<Declare> allList = chkstomForecastService.getAddChkstomList(sd);//当前类别下的所有物资
						List<Declare> declareMap = chkstomForecastService.calculationFoodSale(sd.getCategory_Code(),bdate,maxEndDate,sd.getDept(),cf.getFirm());
						for(Declare d1 : declareMap){
							for(Declare d2 : allList){
								d2.setPositn(dept);
								d2.setPositndes(p.getDes());
								if(d1.getSp_code().equals(d2.getSp_code())){
									double spcal = d1.getPeriodUse();
									if(spcal < 0.01){
            							d2.setSpcal(0);
                    					d2.setSpuse(0);
                    					d2.setAmountdis(0);
                    					declareList.add(d2);
                        				break;
            						}else{
                    					d2.setSpcal(spcal);
                    					if("Y".equals(xsjhCalWay)){//减去库存和在途   计算量取整
                    						spcal = d1.getPeriodUse() + d2.getSpmin() - d2.getSpnow() - d2.getSpway();//计算量 = 周期用量 +安全库存 - 当前库存 - 在途
                    						if(spcal < 0.01){
                            					d2.setSpuse(0);
                            					d2.setAmountdis(0);
                            					declareList.add(d2);
                                				break;
                    						}
                    					}
                    					//采购单位的
                    					double spuse = Math.ceil(spcal * d2.getUnitper3());
										if(d2.getStomin() != 0 && spuse < d2.getStomin()){//如果需求量小于最小申购量 ，则等于最小申购量
											spuse = d2.getStomin();
										}
										if(d2.getStocnt() != 0){
											spuse = Math.ceil(spuse / d2.getStocnt())*d2.getStocnt();
										}else{
											if(d2.getMincnt() != 0){
												spuse = Math.ceil(spuse / d2.getMincnt())*d2.getMincnt();
											}
										}
										d2.setSpuse(spuse);
										//配送单位的
										double amountdis = Math.ceil(spcal * d2.getDisunitper());
                    					if(d2.getDismincnt() != 0)
                    						amountdis = Math.ceil(amountdis / d2.getDismincnt())*d2.getDismincnt();
                    					d2.setAmountdis(amountdis);
									}
									declareList.add(d2);
									break;
								}
							}
						}
					}
				}else{
					List<Declare> allList = chkstomForecastService.getAddChkstomList(sd);//当前类别下的所有物资
					List<Declare> declareMap = chkstomForecastService.calculationFoodSale(sd.getCategory_Code(),bdate,maxEndDate,sd.getDept(),cf.getFirm());
					for(Declare d1 : declareMap){
						for(Declare d2 : allList){
							if(d1.getSp_code().equals(d2.getSp_code())){
								double spcal = d1.getPeriodUse();
								if(spcal < 0.01){
        							d2.setSpcal(0);
                					d2.setSpuse(0);
                					d2.setAmountdis(0);
                					declareList.add(d2);
                    				break;
        						}else{
                					d2.setSpcal(spcal);
                					if("Y".equals(xsjhCalWay)){//减去库存和在途   计算量取整
                						spcal = d1.getPeriodUse() + d2.getSpmin() - d2.getSpnow() - d2.getSpway();//计算量 = 周期用量 +安全库存 - 当前库存 - 在途
                						if(spcal < 0.01){
                        					d2.setSpuse(0);
                        					d2.setAmountdis(0);
                        					declareList.add(d2);
                            				break;
                						}
                					}
                					//采购单位的
                					double spuse = Math.ceil(spcal * d2.getUnitper3());
									if(d2.getStomin() != 0 && spuse < d2.getStomin()){//如果需求量小于最小申购量 ，则等于最小申购量
										spuse = d2.getStomin();
									}
									if(d2.getStocnt() != 0){
										spuse = Math.ceil(spuse / d2.getStocnt())*d2.getStocnt();
									}else{
										if(d2.getMincnt() != 0){
											spuse = Math.ceil(spuse / d2.getMincnt())*d2.getMincnt();
										}
									}
									d2.setSpuse(spuse);
									//配送单位的
									double amountdis = Math.ceil(spcal * d2.getDisunitper());
                					if(d2.getDismincnt() != 0)
                						amountdis = Math.ceil(amountdis / d2.getDismincnt())*d2.getDismincnt();
                					d2.setAmountdis(amountdis);
								}
								declareList.add(d2);
								break;
							}
						}
					}
				}
				sd.setDeclareList(declareList);
			}
    	}
    	//得到需要报货的数据
    	modelMap.put("cf", cf);
    	saveCfCache(session, cf);
    	return new ModelAndView(ChkstomForecastConstants.SAVECHKSTOM,modelMap);
    }
    
    private void calSpuse(List<Declare> declareList){
    	//报货向导预估量计算是否减去库存，在途  是否向上取整
    	for(Declare d : declareList){
			double spcal = d.getSpcal();
			if(spcal < 0.01){
				d.setSpcal(0);
				d.setSpuse(0);
				d.setAmountdis(0);
			}else{
				d.setSpcal(spcal);
				//采购单位的
				double spuse = Math.ceil(spcal * d.getUnitper3());
				if(d.getStomin() != 0 && spuse < d.getStomin()){//如果需求量小于最小申购量 ，则等于最小申购量
					spuse = d.getStomin();
				}
				if(d.getStocnt() != 0){
					spuse = Math.ceil(spuse / d.getStocnt())*d.getStocnt();
				}else{
					if(d.getMincnt() != 0){
						spuse = Math.ceil(spuse / d.getMincnt())*d.getMincnt();
					}
				}
				d.setSpuse(spuse);
				//配送单位的
				double amountdis = Math.ceil(spcal * d.getDisunitper());
				if(d.getDismincnt() != 0)
					amountdis = Math.ceil(amountdis / d.getDismincnt())*d.getDismincnt();
				d.setAmountdis(amountdis);
			}
		}
    }
    
    /***
     * 生成订货单
     * @param modelMap
     * @param session
     * @param chkstom
     * @return
     */
    @RequestMapping(value = "/saveChkstom")
    @ResponseBody
    public int saveChkstom(ModelMap modelMap,HttpSession session,Chkstom chkstom) throws Exception{
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
				"MISBOH保存报货单,单号:"+chkstom.getChkstoNo(),session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		ChkstomForecast cf = getCfCache(session);
    	chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());//当前帐套
    	chkstom.setCreateTyp(Integer.parseInt(DDT.GUIDEGENERATION));
    	chkstom.setMaded(cf.getMaded());
    	chkstom.setFirm(cf.getFirm());
    	chkstom.setDept(cf.getDept());
    	chkstom.setMadeby(session.getAttribute("accountName").toString());
    	chkstom.setVouno(calChkNum.getNextBytable(DDT.VOUNO_CHKSTOM,DDT.VOUNO_BH+chkstom.getFirm()+"-",chkstom.getMaded()));
    	if(null == chkstom.getTyp())
    		chkstom.setTyp(DDT.ZCBH);
    	if("Y".equals(DDT.isChkstomJmj)){//九毛九的报货页面
    		chkstom.setHoped(DateFormat.getDateByString(chkstom.getChkstodList().get(0).getHoped(),"yyyy-MM-dd"));
		}
		//判断是否多档口报货
		if(null == cf.getDept() || "".equals(cf.getDept())){
			chkstomMisService.saveOrUpdateChk(chkstom, "add");
			//判断是否需要自动提交
			if(null != DDT.autoTyp && !"0".equals(DDT.autoTyp)){
				String accountName = session.getAttribute("accountName").toString();
				chkstom.setChecby(accountName);
				String accountNames = session.getAttribute("accountNames").toString();
				chkstom.setChecbyName(accountNames);
				//设置 -1 全部提交
				if("-1".equals(DDT.autoTyp)){
					checkChkstom(chkstom);
				}else{
					//判断是否设置多个类别
					if(DDT.autoTyp.indexOf(";")!=-1){
						String split[] = DDT.autoTyp.split(";");
						for (int i = 0; i < split.length; i++) {
							if(chkstom.getManifsttyp().equals(split[i])){
								checkChkstom(chkstom);
							}
						}
					}else{
						if(chkstom.getManifsttyp().equals(DDT.autoTyp)){
							checkChkstom(chkstom);
						}
					}
				}
			}
		}else{
			if(cf.getSta() == 1 || cf.getSta() == 2){
				String[] deptList = cf.getDept().split(",");
				for(String dept : deptList){
					Chkstom cm = new Chkstom();
					cm.setChkstoNo(chkstomDeptMisService.getMaxChkstono());
					cm.setManifsttyp(chkstom.getManifsttyp());//报货类别
					cm.setAcct(session.getAttribute("ChoiceAcct").toString());//当前帐套
			    	cm.setCreateTyp(Integer.parseInt(DDT.GUIDEGENERATION));
			    	cm.setMaded(cf.getMaded());
			    	cm.setFirm(cf.getFirm());
			    	cm.setDept(dept);
			    	cm.setMadeby(session.getAttribute("accountName").toString());
			    	cm.setVouno(calChkNum.getNextBytable(DDT.VOUNO_CHKSTOMDEPT,DDT.VOUNO_BH+cm.getFirm()+"-",cm.getMaded()));
			    	if(null == cm.getTyp())
			    		cm.setTyp(DDT.ZCBH);
			    	if("Y".equals(DDT.isChkstomJmj)){//九毛九的报货页面
			    		cm.setHoped(DateFormat.getDateByString(cm.getChkstodList().get(0).getHoped(),"yyyy-MM-dd"));
					}
					List<Chkstod> cdList = new ArrayList<Chkstod>();
					for(Chkstod cd : chkstom.getChkstodList()){
						if(dept.equals(cd.getPositn())){
							cdList.add(cd);
						}
					}
					cm.setChkstodList(cdList);
					if(cdList.size() > 0)
						chkstomDeptMisService.saveOrUpdateChk(cm, "add");
				}
			}else{
				chkstomDeptMisService.saveOrUpdateChk(chkstom, "add");
			}
		}
		saveCfCache(session, cf);
		return 1;
    }
    
	private String checkChkstom(Chkstom chkstom) throws Exception {
		//需要判断是否总部审核
		Positn positn = new Positn();
		positn.setAcct(chkstom.getAcct());
		positn.setCode(chkstom.getFirm());
		Positn p = positnService.findPositnByCode(positn);
		String ynZbChk = p.getYnZbChk();
		if("Y".equals(ynZbChk)){//如果总部审核，这里的审核则随便改个状态,门店不可编辑，总部可编辑
			return chkstomMisService.checkChkstom(chkstom,null);
		}else{//如果不需要总部审核 ，这里的审核则走总部审核
			//判断是否有需要总部特殊审核的物资，有就走门店审核
			String[] ids = null;
			ids = chkstom.getChkstoNo().toString().split(",");
			StringBuffer zb = new StringBuffer();
			StringBuffer md = new StringBuffer();
			for(String id : ids){
				Chkstom c = new Chkstom();
				c.setChkstoNo(Integer.parseInt(id));
				int count = chkstomMisService.findStoCheckByChkstom(c);//是否有特殊审核的物资
				if(count == 0){
					zb.append(id+",");
				}else{
					md.append(id+",");
				}
			}
			if(zb.length() > 0){
				return chkstomMisService.checkChk(chkstom,zb.substring(0, zb.length()-1));
			}
			if(md.length() > 0){
				return chkstomMisService.checkChkstom(chkstom,md.substring(0, md.length()-1));
			}
			return null;
		}
	}
    
	/******************************************右侧页面：选择报货模板********************************************************/
	
	/***
	 * 报货模板调用
	 * @param modelMap
	 * @param session
	 * @param chkstodemo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkstoDemo")
	public ModelAndView addChkstoDemo(ModelMap modelMap, HttpSession session, MISChkstoDemom chkstodemo) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		ChkstomForecast cf = getCfCache(session);
    	List<ScheduleD> sdList = new ArrayList<ScheduleD>();
    	for(ScheduleD sd:cf.getSdList()){
    		if(sd.getCodetyp() == DDT.SHIPMENT_SGMB){//报货模板
    			sdList.add(sd);
    		}
    	}
    	modelMap.put("sdList", sdList);
    	
    	String typ_eas = chkstodemo.getTyp_eas();
    	if(null == typ_eas || "".equals(typ_eas)){//如果是空，第一次进
    		typ_eas = sdList.get(0).getCategory_Code();
    	}
    	modelMap.put("typ_eas", typ_eas);
    	
		chkstodemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstodemo.setTempTyp("BH");
		chkstodemo.setFirm(cf.getFirm());
		List<MISChkstoDemom> listChkstoDemo = chkstoDemomService.listChkstoDemom(chkstodemo);
		if(listChkstoDemo.size() > 0) {
			MISChkstoDemod chkstoDemod = new MISChkstoDemod();
			chkstoDemod.setScode(cf.getFirm());
			chkstoDemod.setChkstodemono(listChkstoDemo.get(0).getChkstodemono());
			chkstoDemod.setYnsto("Y");
			chkstoDemod.setTyp_eas(CodeHelper.replaceCode(typ_eas));
			List<MISChkstoDemod> chkstodemoList = chkstoDemomService.listChkstoDemodd(chkstoDemod);
			modelMap.put("chkstodemoList", chkstodemoList);
		}
		modelMap.put("chkstodemono", chkstodemo.getChkstodemono());
		//最后查标题栏
		chkstodemo.setChkstodemono(null);
		modelMap.put("listTitle", chkstoDemomService.listChkstoDemom(chkstodemo));
		modelMap.put("isNotShowSp_price", DDT.isNotShowSp_price);//是否报货验货显示价格列金色三麦用
		modelMap.put("isDistributionUnit", DDT.isDistributionUnit);//是否根据配送单位报货
		saveCfCache(session, cf);
		return new ModelAndView(ChkstomForecastConstants.ADD_CHKSTODEMOMIS, modelMap);
	}
	
	/***
	 * 保存报货模板
	 * @param modelMap
	 * @param session
	 * @param scheduleD
	 * @return
	 */
    @RequestMapping(value = "/saveChkstoDemo")
    @ResponseBody
    public int saveChkstoDemo(ModelMap modelMap,HttpSession session,ScheduleD scheduleD) {
    	ChkstomForecast cf = getCfCache(session);
    	for(ScheduleD sd:cf.getSdList()){
    		if(sd.getCodetyp() == DDT.SHIPMENT_SGMB){//报货模板
    			if(sd.getCategory_Code().equals(scheduleD.getCategory_Code())){
    				sd.setDeclareList(scheduleD.getDeclareList());
    			}
    		}
    	}
    	saveCfCache(session, cf);
    	return 0;
    }
	
	/**********************************************调整预估值界面************************************************/
	
    /***
     * 报货向导--调整预估值
     * @param modelMap
     * @param session
     * @return
     */
    @RequestMapping(value = "/listAdjustTheEstimated")
    public ModelAndView listAdjustTheEstimated(ModelMap modelMap,HttpSession session) throws Exception{
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
    	ChkstomForecast cf = getCfCache(session);
		modelMap.put("cf", cf);
		saveCfCache(session, cf);
    	return new ModelAndView(ChkstomForecastConstants.LISTADJUSTTHEESTIMATED,modelMap);
    }
    
    /***
     * 保存预估值
     * @param modelMap
     * @param session
     * @param cf
     * @return
     */
    @RequestMapping(value = "/saveSalePlan")
    @ResponseBody
    public DeclareGoodsGuide saveSalePlan(ModelMap modelMap,HttpSession session,ChkstomForecast cf) {
    	ChkstomForecast chkFore = getCfCache(session);
    	chkFore.setSalePlanList(cf.getSalePlanList());
    	List<Map<String,Object>> salePlanMap = new ArrayList<Map<String,Object>>();
    	for(SalePlan sp : chkFore.getSalePlanList()){
    		Map<String,Object> map = new HashMap<String,Object>();
    		map.put("DAT", DateFormat.getStringByDate(sp.getDat(), "yyyy-MM-dd"));
    		map.put("SALEVALUE", sp.getMoney());
    		salePlanMap.add(map);
    	}
    	DeclareGoodsGuide dgg = new DeclareGoodsGuide();
    	dgg.setSalePlanList(salePlanMap);//放到map里是因为菜品点击率要用
    	saveCfCache(session, chkFore);
    	return dgg;
    }
    
    /******************************************千元用量界面此方法已作废20160720wjf*********************************************/
    
    /**
     * 报货向导--千用量
     * @param modelMap
     * @param session
     * @param chkstomF
     * @return
     */
    @RequestMapping(value = "/listThousandsOfDosage")
    public ModelAndView listThousandsOfDosage(ModelMap modelMap,HttpSession session,ChkstomForecast chkstomF){
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
    	ChkstomForecast cf = getCfCache(session);
    	cf.setBdate(chkstomF.getBdate());
    	cf.setEdate(chkstomF.getEdate());
    	List<Declare> qyylList = new ArrayList<Declare>();
		// 循环所有报货类别数据
    	for(ScheduleD sd:cf.getSdList()){
    		if(sd.getCodetyp() == DDT.SHIPMENT_QR || sd.getCodetyp() == DDT.SHIPMENT_QY){//包含千元,千人用量
    			
    			if(null == chkstomF.getBdate() && null == chkstomF.getEdate()){
					cf.setBdate(DateJudge.getLastTime(7, DateFormat.getStringByDate(cf.getMaded(), "yyyy-MM-dd")));
					cf.setEdate(DateFormat.getStringByDate(cf.getMaded(), "yyyy-MM-dd"));
    			}
    			//累加预估值    开始日期：到货日+采购提前期  结束日期：下次到货日 + 采购提前期
    			String maxEndDate = DateJudge.getLastTime(-sd.getDays(), sd.getNextReceiveDate());
				String receiveDate = DateJudge.getLastTime(-sd.getDays()+1, sd.getReceiveDate());
				double money = 0;//累加营业额
				for(SalePlan sp : cf.getSalePlanList()){
					if(DateFormat.getDateByString(receiveDate, "yyyy-MM-dd").getTime() <= sp.getDat().getTime()//日期大于等于到货日 小于等于 (下次到货日-采购提前期)
							&& sp.getDat().getTime() <= DateFormat.getDateByString(maxEndDate, "yyyy-MM-dd").getTime()){
						money += sp.getMoney();
					}
				}
				//判断用量计算
				List<Declare> declareList = chkstomForecastService.calculationThousands(sd.getCategory_Code(),cf.getBdate(),cf.getEdate(),money,cf.getDept(),cf.getVplantyp(),cf.getFirm());
				qyylList.addAll(declareList);
			}
		}
		cf.setQyylList(qyylList);
    	modelMap.put("cf", cf);
    	saveCfCache(session, cf);
    	return new ModelAndView(ChkstomForecastConstants.LISTTHOUSANDSOFDOSAGE,modelMap);
    }
    
    /***
     * 保存千元用量
     * @param modelMap
     * @param session
     * @param cf
     * @return
     */
    @RequestMapping(value = "/saveThousands")
    @ResponseBody
    public int saveThousands(ModelMap modelMap,HttpSession session,ChkstomForecast cf) {
    	ChkstomForecast chkFore = getCfCache(session);
    	for(Declare d1 : chkFore.getQyylList()){
    		for(Declare d2 : cf.getQyylList()){
    			if(d1.getSp_code().equals(d2.getSp_code())){
    				d1.setEstimatedParameters(d2.getEstimatedParameters());
    				d1.setPeriodUse(d2.getPeriodUse());
    			}
    		}
    	}
    	saveCfCache(session, chkFore);
    	return 0;
    }
    
/******************************************千元用量界面*********************************************/
    
    /**
     * 报货向导--千用量
     * @param modelMap
     * @param session
     * @param chkstomF
     * @return
     */
    @RequestMapping(value = "/listSpcodeUse")
    public ModelAndView listSpcodeUse(ModelMap modelMap,HttpSession session,ChkstomForecast chkstomF){
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
    	ChkstomForecast cf = getCfCache(session);
    	if(null != chkstomF.getBdate() && null != chkstomF.getEdate()){
    		cf.setBdate(chkstomF.getBdate());
    		cf.setEdate(chkstomF.getEdate());
    	}
    	if(chkstomF.getStep() != -1){//如果不是上一步，则需要计算，是上一步的无需继续计算
	    	List<Declare> qyylList = new ArrayList<Declare>();
			// 循环所有报货类别数据
	    	for(ScheduleD sd:cf.getSdList()){
	    		if(sd.getCodetyp() == DDT.SHIPMENT_QR || sd.getCodetyp() == DDT.SHIPMENT_QY){//包含千元,千人用量
	    			
	    			if(null == chkstomF.getBdate() && null == chkstomF.getEdate()){
						cf.setBdate(DateJudge.getLastTime(7, DateFormat.getStringByDate(cf.getMaded(), "yyyy-MM-dd")));
						cf.setEdate(DateFormat.getStringByDate(cf.getMaded(), "yyyy-MM-dd"));
	    			}
	    			//累加预估值    开始日期：到货日+采购提前期  结束日期：下次到货日 + 采购提前期
	    			String maxEndDate = DateJudge.getLastTime(-sd.getDays()+1, sd.getNextReceiveDate());
					String receiveDate = DateJudge.getLastTime(-sd.getDays(), sd.getReceiveDate());
					double money = 0;//累加营业额
					for(SalePlan sp : cf.getSalePlanList()){
						if(DateFormat.getDateByString(receiveDate, "yyyy-MM-dd").getTime() <= sp.getDat().getTime()//日期大于等于到货日 小于等于 (下次到货日-采购提前期)
								&& sp.getDat().getTime() <= DateFormat.getDateByString(maxEndDate, "yyyy-MM-dd").getTime()){
							money += sp.getMoney();
						}
					}
					SpCodeUseMis spCodeUse = new SpCodeUseMis();
					spCodeUse.setFirm(cf.getFirm());
					spCodeUse.setDept(CodeHelper.replaceCode(cf.getDept()));
					spCodeUse.setTyp(sd.getCategory_Code());
					if("AMT".equals(cf.getVplantyp()))//如果是按营业额，则是万元用量
						spCodeUse.setAmt(money/10000.00);
					else
						spCodeUse.setAmt(money/1000.00);
					List<Declare> declareList = new ArrayList<Declare>();
					if(chkstomF.getSta() == 1){//重新计算
						spCodeUse.setBdat(DateFormat.getDateByString(chkstomF.getBdate(), "yyyy-MM-dd"));
						spCodeUse.setEdat(DateFormat.getDateByString(chkstomF.getEdate(), "yyyy-MM-dd"));
						declareList = chkstomForecastService.calSpcodeUse(spCodeUse,cf.getVplantyp(),cf.getVfoodsign(),cf.getIsdept());
					}else{//一进来先查千元用量表
						declareList = chkstomForecastService.listSpcodeUse(spCodeUse);
					}
					qyylList.addAll(declareList);
				}
			}
			cf.setQyylList(qyylList);
    	}
    	modelMap.put("cf", cf);
    	saveCfCache(session, cf);
    	return new ModelAndView(ChkstomForecastConstants.LISTSPCODEUSE,modelMap);
    }
    
    /***
     * 保存千元用量
     * @param session
     * @param cf
     * @return
     */
    @RequestMapping(value = "/saveSpcodeUse")
    @ResponseBody
    public int saveSpcodeUse(HttpSession session,ChkstomForecast cf) {
    	ChkstomForecast chkFore = getCfCache(session);
    	chkFore.setQyylCalWay(cf.getQyylCalWay());
    	chkFore.setQyylList(cf.getQyylList());
    	saveCfCache(session, chkFore);
    	return 0;
    }
    
    /******************************************菜品点击率*********************************************/
    
    /**
	 * 分店MIS预估系统菜品点击率
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/itemUselist")
	public ModelAndView itemUselist(ModelMap modelMap, HttpSession session, Page page, FirmItemUse itemUse, String isDeclare) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		ChkstomForecast cf = getCfCache(session);
		if(null != cf.getDept()){
			String bohDept = chkstomForecastService.findPositnFirm(cf.getFirm(),cf.getDept());
			itemUse.setDept(bohDept);
			cf.setDeptboh(bohDept);
		}
		itemUse.setFirm(cf.getFirm());
		//1.参考日期  默认前一个月
		Date nowDate = new Date();
		if(null == itemUse.getBdate() || null == itemUse.getEdate()){
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(nowDate);
			calendar.add(Calendar.DATE, -31);
			itemUse.setBdate(calendar.getTime());
			calendar.setTime(nowDate);
			calendar.add(Calendar.DATE, -1);
			itemUse.setEdate(calendar.getTime());
			itemUse.setVplantyp(cf.getVplantyp());//得到营业预估方式
		}
		modelMap.put("itemUseList", itemUseService.findAllItemUse(itemUse));
		modelMap.put("itemUse", itemUse);
		modelMap.put("isDeclare", "1");//判断是否走预估报货
		saveCfCache(session, cf);
		return new ModelAndView(ForecastConstants.LIST_ITEMUSE, modelMap);
	}
    
    /******************************************菜品销售计划*********************************************/
    
    /**
	 * 预估菜品销售计划
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/planList")
	public ModelAndView planList(ModelMap modelMap, HttpSession session, ItemPlan itemPlan) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		ChkstomForecast cf = getCfCache(session);
		//得到开始日期  结束日期=有预估报货的最后一个到货日
    	Date edate = cf.getMaded();
    	for(ScheduleD sd:cf.getSdList()){
    		if(sd.getCodetyp() == DDT.SHIPMENT_CPDJL || sd.getCodetyp() == DDT.SHIPMENT_CPXSJH){//菜品点击率 菜品销售计划
    			Date nextDate = DateFormat.getDateByString(sd.getNextReceiveDate(),"yyyy-MM-dd");
    			Calendar calendar = Calendar.getInstance();
    			calendar.setTime(nextDate);
				calendar.add(Calendar.DATE,-1);
				nextDate = calendar.getTime();
    			if(sd.getDays() > 0){
    				calendar.add(Calendar.DATE,sd.getDays());
    			    nextDate = calendar.getTime();
    			}
    			if(nextDate.getTime() > edate.getTime()){
    				edate = nextDate;
    			}
    		}
    	}
    	Date bdate = edate;
    	for(ScheduleD sd:cf.getSdList()){
    		if(sd.getCodetyp() == DDT.SHIPMENT_CPDJL || sd.getCodetyp() == DDT.SHIPMENT_CPXSJH){//菜品点击率 菜品销售计划
    			Date ind = DateFormat.getDateByString(sd.getReceiveDate(),"yyyy-MM-dd");
    			if(sd.getDays() > 0){
    				Calendar calendar = Calendar.getInstance();
    			    calendar.setTime(ind);
    			    calendar.add(Calendar.DATE,sd.getDays());
    			    ind = calendar.getTime();
    			}
    			if(ind.getTime() < bdate.getTime()){
    				bdate = ind;
    			}
    		}
    	}
    	Positn thePositn = (Positn)session.getAttribute("accountPositn");
    	thePositn.setAcct(session.getAttribute("ChoiceAcct").toString());
    	thePositn.setYnUseDept(cf.getIsdept());
		List<String> listStr = new ArrayList<String>();
		listStr = DateJudge.getDateList(DateJudge.getStringByDate(bdate, "yyyy-MM-dd"), DateJudge.getStringByDate(edate, "yyyy-MM-dd"));
		modelMap.put("dateList", listStr);
		modelMap.put("listStrSize", listStr.size());
		itemPlan.setFirm(thePositn.getCode());
		itemPlan.setDat(bdate);
		itemPlan.setBdate(bdate);
		itemPlan.setEdate(edate);
		if(null != cf.getDept() && null != cf.getDeptboh()){
			itemPlan.setDept(cf.getDeptboh());
		}
		modelMap.put("itemPlanList", posItemPlanService.queryItemPlan(itemPlan));
		//XXX 暂不支持档口
//		Store store = new Store();
//		store.setPk_group(session.getAttribute("ChoiceAcct").toString());
//		store.setVcode(thePositn.getCode());
//		if ("Y".equals(thePositn.getYnUseDept())) {
//			modelMap.put("deptList", commonMISBOHService.getAllStoreDept(store));//根据门店编码获取所有部门
//		}
//		modelMap.put("isdept",thePositn.getYnUseDept());
		modelMap.put("msg", "a");
		modelMap.put("itemPlan", itemPlan);
		modelMap.put("isDeclare", "1");
		saveCfCache(session, cf);
		return new ModelAndView(ForecastConstants.LIST_POSITEMPALN, modelMap);
	}
	
	/******************************************************choice3菜品销售计划********************************************************************/
    
    /**
	 * 预估菜品销售计划
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/planListChoice3")
	public ModelAndView planListChoice3(ModelMap modelMap, HttpSession session, Page page, ItemPlan posItemPlan) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		ChkstomForecast cf = getCfCache(session);
		//得到开始日期  结束日期=有预估报货的最后一个到货日
    	Date edate = cf.getMaded();
    	for(ScheduleD sd:cf.getSdList()){
    		if(sd.getCodetyp() == DDT.SHIPMENT_CPDJL || sd.getCodetyp() == DDT.SHIPMENT_CPXSJH){//菜品点击率 菜品销售计划
    			Date nextDate = DateFormat.getDateByString(sd.getNextReceiveDate(),"yyyy-MM-dd");
    			Calendar calendar = Calendar.getInstance();
    			calendar.setTime(nextDate);
				calendar.add(Calendar.DATE,-1);
				nextDate = calendar.getTime();
    			if(sd.getDays() > 0){
    				calendar.add(Calendar.DATE,sd.getDays());
    			    nextDate = calendar.getTime();
    			}
    			if(nextDate.getTime() > edate.getTime()){
    				edate = nextDate;
    			}
    		}
    	}
    	Date bdate = edate;
    	for(ScheduleD sd:cf.getSdList()){
    		if(sd.getCodetyp() == DDT.SHIPMENT_CPDJL || sd.getCodetyp() == DDT.SHIPMENT_CPXSJH){//菜品点击率 菜品销售计划
    			Date ind = DateFormat.getDateByString(sd.getReceiveDate(),"yyyy-MM-dd");
    			if(sd.getDays() > 0){
    				Calendar calendar = Calendar.getInstance();
    			    calendar.setTime(ind);
    			    calendar.add(Calendar.DATE,sd.getDays());
    			    ind = calendar.getTime();
    			}
    			if(ind.getTime() < bdate.getTime()){
    				bdate = ind;
    			}
    		}
    	}
    	Positn thePositn = (Positn)session.getAttribute("accountPositn");
    	thePositn.setAcct(session.getAttribute("ChoiceAcct").toString());
    	thePositn.setYnUseDept(cf.getIsdept());
		posItemPlan.setFirm(thePositn.getCode());
		posItemPlan.setStartdate(bdate);
		posItemPlan.setEnddate(edate);
		if(null != cf.getDept()){
			posItemPlan.setDept1(CodeHelper.replaceCode(cf.getDept()));//多部门
		}
		modelMap.put("posItemPlanList", chkstomForecastService.queryItemPlanByChkstom(posItemPlan));
		modelMap.put("itemPlan", posItemPlan);
		List<String> listStr = DateJudge.getDateList(DateJudge.getStringByDate(bdate, "yyyy-MM-dd"), DateJudge.getStringByDate(edate, "yyyy-MM-dd"));
		modelMap.put("dateList", listStr);
		modelMap.put("listStrSize", listStr.size());
//		Store store = new Store();
//		store.setPk_group(session.getAttribute("ChoiceAcct").toString());
//		store.setVcode(thePositn.getCode());
//		if ("Y".equals(thePositn.getYnUseDept())) {
//			modelMap.put("deptList", commonMISBOHService.getAllStoreDept(store));//根据门店编码获取所有部门
//		}
		saveCfCache(session, cf);
		return new ModelAndView(ChkstomForecastConstants.LISTFOODSALESPLAN,modelMap);
	}
	
	/**
	 * 计算预估菜品销售计划choice3
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/calItemPlan")
	public ModelAndView calItemPlan(ModelMap modelMap, HttpSession session, ItemPlan itemPlan) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		ChkstomForecast cf = getCfCache(session);
		//营业预估
		List<SalePlan> salePlanList = cf.getSalePlanList();
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		FirmItemUse itemUse = new FirmItemUse();
		itemUse.setFirm(thePositn.getCode());
		itemUse.setDept1(CodeHelper.replaceCode(cf.getDept()));
		List<FirmItemUse> listItemUse = chkstomForecastService.findAllItemUse(itemUse);
		if (listItemUse.size() == 0) {
			modelMap.put("msg", "没有设定点击率，不能自动计算预估!");
		} else {
			// 数组存放某日四个班所对应的营业预估值
			chkstomForecastService.saveCalPlan(itemPlan,salePlanList,listItemUse);
			modelMap.put("msg", "");//标记正常计算营业预估
		}
		
		List<String> listStr = DateJudge.getDateList(DateJudge.getStringByDate(itemPlan.getStartdate(), "yyyy-MM-dd"), DateJudge.getStringByDate(itemPlan.getEnddate(), "yyyy-MM-dd"));
		modelMap.put("dateList", listStr);
		modelMap.put("listStrSize", listStr.size());
		modelMap.put("posItemPlanList", chkstomForecastService.queryItemPlanByChkstom(itemPlan));
		modelMap.put("itemPlan", itemPlan);
		saveCfCache(session, cf);
		return new ModelAndView(ChkstomForecastConstants.LISTFOODSALESPLAN,modelMap);
	}
	
	/***
	 * 保存菜品销售计划计算方式
	 * @param session
	 * @param xsjhCalWay
	 * @return
	 */
    @RequestMapping(value = "/saveXsjhCalWay")
    @ResponseBody
    public int saveXsjhCalWay(HttpSession session,String xsjhCalWay) {
    	ChkstomForecast cf = getCfCache(session);
    	cf.setXsjhCalWay(xsjhCalWay);
		saveCfCache(session, cf);
    	return 0;
    }
    
}
