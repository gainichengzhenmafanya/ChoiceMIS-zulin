package com.choice.misboh.web.controller.chkstom;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.misboh.constants.chkstom.ChkstoDemomMisConstants;
import com.choice.misboh.domain.chkstom.MISChkstoDemoFirm;
import com.choice.misboh.domain.chkstom.MISChkstoDemod;
import com.choice.misboh.domain.chkstom.MISChkstoDemom;
import com.choice.misboh.service.chkstom.ChkstoDemomMisService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;
import com.choice.scm.persistence.ExecSql;
import com.choice.scm.service.PositnService;

/**
 * 报货单单据模板
 * @author 孙胜彬
 */
@Controller
@RequestMapping(value="chkstodemomMis")
public class ChkstoDemomMisController {
	@Autowired
	private ChkstoDemomMisService chkstoDemomService;
	@Autowired
	private PositnService positnService;
	@Autowired
	private ExecSql execSql;
	
    private Positn positn;
	
	/**
	 * 查询报货单单据模板
	 * @param chkstoDemom
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("listChkstoDemom")
	public ModelAndView listChkstoDemom(ModelMap modelMap,MISChkstoDemom chkstoDemom,Page page,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		setPositn(session);
		if(chkstoDemom == null){
			chkstoDemom = new MISChkstoDemom();
		}
		chkstoDemom.setFirm(positn.getCode());
		List<MISChkstoDemom> listChkstoDemom=chkstoDemomService.listChkstoDemom(chkstoDemom, page);
		modelMap.put("listChkstoDemom", listChkstoDemom);
		modelMap.put("chkstoDemom", chkstoDemom);
		modelMap.put("pageobj", page);
		return new ModelAndView(ChkstoDemomMisConstants.LIST_CHKSTODEMOM);
	}
	
	/**
	 * 查询报货单单据模板子表
	 * @param chkstoDemod
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("listChkstoDemod")
	public ModelAndView listChkstoDemod(ModelMap modelMap,MISChkstoDemod chkstoDemod,Page page,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		setPositn(session);
		chkstoDemod.setScode(positn.getCode());
		List<MISChkstoDemod> listChkstoDemod=chkstoDemomService.listChkstoDemodd(chkstoDemod);
		modelMap.put("listChkstoDemod", listChkstoDemod);
		modelMap.put("chkstodemono", chkstoDemod.getChkstodemono());
		modelMap.put("pageobj", page);
		return new ModelAndView(ChkstoDemomMisConstants.LIST_CHKSTODEMOD);
	}
	
	/**
	 * 新增报货单单据模板页面
	 * @param chkstoDemod
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("addChkstoDemod")
	public ModelAndView addChkstoDemod(ModelMap modelMap,String chkstodemono,HttpSession session,String tempTyp) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		setPositn(session);
		//当前登录用户
		String accountName=session.getAttribute("accountName").toString();
		modelMap.put("chkstodemono", execSql.getMaxsequences("CHKSTODEMOM"));
		modelMap.put("accountName", accountName);
		modelMap.put("sta", "add");
		modelMap.put("tempTyp", tempTyp);
		return new ModelAndView(ChkstoDemomMisConstants.ADD_ChkstoDemod,modelMap);
	}
	
	/**
	 * 新增报货单单据模板
	 * @param chkstoDemom
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("saveChkstoDemom")
	@ResponseBody
	public String saveChkstoDemom(MISChkstoDemom chkstoDemom,ModelMap modelMap,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		setPositn(session);
		modelMap.put("sta", "add");
		chkstoDemom.setFirm(positn.getCode());
		return chkstoDemomService.saveChkstoDemoms(chkstoDemom,session);
	}
	
	/**
	 * 删除报货单单据模板
	 * @param ids
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("deleteChkstoDemom")
	public ModelAndView deleteChkstoDemom(String ids,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		setPositn(session);
		chkstoDemomService.deleteChkstoDemom(ids,positn.getCode());
		return new ModelAndView(StringConstant.ACTION_DONE);
	}
	
	/**
	 * 修改报货单单据页面
	 * @param modelMap
	 * @param chkstoDemom
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("updChkstoDemom")
	public ModelAndView updChkstoDemom(ModelMap modelMap,MISChkstoDemom chkstoDemom,Page page,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		setPositn(session);
		chkstoDemom.setFirm(positn.getCode());
		List<MISChkstoDemom> listChkstoDemom=chkstoDemomService.listChkstoDemom(chkstoDemom, page);
		MISChkstoDemod chkstoDemod=new MISChkstoDemod();
		chkstoDemod.setChkstodemono(chkstoDemom.getChkstodemono());
		List<MISChkstoDemod> listChkstoDemod=chkstoDemomService.listChkstoDemod(chkstoDemod, page,positn.getCode());
		modelMap.put("chkstoDemom", listChkstoDemom.get(0));
		modelMap.put("listChkstoDemod", listChkstoDemod);
		modelMap.put("sta", "add");
		return new ModelAndView(ChkstoDemomMisConstants.UPD_CHKSTODEMOD,modelMap);
	}
	
	/**
	 * 修改报货单单据模
	 * @param chkstoDemom
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("updateChkstoDemom")
	@ResponseBody
	public int updateChkstoDemom(ModelMap modelMap,MISChkstoDemom chkstoDemom,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		setPositn(session);
		chkstoDemom.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstoDemom.setFirm(positn.getCode());
		return chkstoDemomService.updChkstoDemom(chkstoDemom);
	}
	
	/**
	 * 新增适用分店
	 * @param modelMap
	 * @return
	 */
	@RequestMapping("saveChkstoDemoFirm")
	public ModelAndView saveChkstoDemoFirm(ModelMap modelMap,MISChkstoDemoFirm chkstoDemoFirm,String chkstodemonoa, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		setPositn(session);
		chkstoDemomService.saveChkstoDemoFirm(chkstoDemoFirm,chkstodemonoa);
		return new ModelAndView(StringConstant.ACTION_DONE);
	}

	/**
	 * 报货模板调用
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkstoDemo")
	public ModelAndView addChkstoDemo(ModelMap modelMap, HttpSession session, Page page, MISChkstoDemom chkstodemo) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		setPositn(session);
		chkstodemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<MISChkstoDemom> listChkstoDemo = chkstoDemomService.listChkstoDemom(chkstodemo);
		MISChkstoDemod chkstoDemod = new MISChkstoDemod();
		if(listChkstoDemo.size() > 0) {//报货单填制选模板时有点问题 2014.11.10 wjf
			chkstoDemod.setChkstodemono(listChkstoDemo.get(0).getChkstodemono());
		}else{
			chkstoDemod.setChkstodemono(0);
//			StringBuffer buffer = new StringBuffer();
//			buffer.append("(");
//			for(ChkstoDemom chkstoDemom:listChkstoDemo){
//				buffer.append(chkstoDemom.getChkstodemono()+",");
//			}
//			buffer.append("0)");
//			chkstoDemod.setChkstodemonos(buffer.toString());
		}
		modelMap.put("firm", chkstodemo.getFirm());//wjf
		modelMap.put("title", chkstodemo.getTitle());
		modelMap.put("chkstodemoList", chkstoDemomService.listChkstoDemod(chkstoDemod, page,positn.getCode()));
		modelMap.put("pageobj", page);
		//最后查标题栏
		chkstodemo.setTitle(null);//不要查特定的，查所有此门店适用的
		modelMap.put("listTitle", chkstoDemomService.listChkstoDemom(chkstodemo));//改为查带适用分店权限的2014.11.18wjf
		return new ModelAndView(ChkstoDemomMisConstants.ADD_CHKSTODEMOMIS, modelMap);
	}
    /**
     * 读取SESSION数据
     * @author 文清泉
     * @param 2015年1月1日 上午10:59:13
     * @param session
     */
    public void setPositn(HttpSession session){
		try {
			positn = (Positn)session.getAttribute("accountPositn");
	    	positn = positnService.findPositnByCode(positn);
		} catch (CRUDException e) {
		    e.printStackTrace();
		}
    }
    
    /*************************WAP 调用后台******************************/
    /**
	 * 查询报货单单据模板
	 * @param chkstoDemom
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("listChkstoDemomWAP")
	@ResponseBody
	public Object listChkstoDemomWAP(MISChkstoDemom chkstoDemom, Page page, String jsonpcallback) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		List<MISChkstoDemom> listChkstoDemom=chkstoDemomService.listChkstoDemom(chkstoDemom, page);
		map.put("listChkstoDemom", listChkstoDemom);
		map.put("chkstoDemom", chkstoDemom);
		map.put("page", page);
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
}
