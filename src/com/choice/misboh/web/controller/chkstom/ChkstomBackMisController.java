package com.choice.misboh.web.controller.chkstom;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ProgramConstants;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.constants.chkstom.ChkstomMisConstants;
import com.choice.misboh.service.chkstom.ChkstomMisService;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.orientationSys.service.DictService;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Chkstom;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Supply;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.SupplyService;
import com.choice.scm.util.CalChkNum;
import com.choice.scm.util.ReadReportUrl;

/***
 * 门店报货相关Controller
 * @author wjf
 *
 */
@Controller
@RequestMapping(value = "chkstomBackMis")
public class ChkstomBackMisController {

	@Autowired
	private ChkstomMisService chkstomMisService;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private PositnService positnService;
	@Autowired
	private SupplyService supplyService;
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	@Autowired
	private DictService dictService;
	
	/***
	 * 进入报货页面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/chkstomTable")
	public ModelAndView tableChkstom(ModelMap modelMap) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("isNotShowSp_price", DDT.isNotShowSp_price);//是否报货验货显示价格列金色三麦用
		modelMap.put("isDistributionUnit", DDT.isDistributionUnit);//是否根据配送单位报货
		if("Y".equals(DDT.isChkstomJmj)){//九毛九的报货页面
			return new ModelAndView(ChkstomMisConstants.TABLE_CHKSTOMBACKMIS_JMJ);
		}
		return new ModelAndView(ChkstomMisConstants.TABLE_CHKSTOMBACKMIS);
	}
	
	/***
	 * 点击新增报货单
	 * @param modelMap
	 * @param session
	 * @param chkstom
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkstom")
	public ModelAndView addChkstom(ModelMap modelMap,HttpSession session,Chkstom chkstom) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//当前登录用户
		String accountName=session.getAttribute("accountName").toString();
		chkstom.setMadeby(accountName);
		String accountNames=session.getAttribute("accountNames").toString();
		chkstom.setMadebyName(accountNames);
		chkstom.setMaded(new Date());
		//获取最大单号
		chkstom.setChkstoNo(chkstomMisService.getMaxChkstono());
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			String positnCode = thePositn.getCode();
			chkstom.setFirm(positnCode);
		}
		modelMap.put("chkstom", chkstom);
		modelMap.put("sta", "add");
		modelMap.put("bhfl", dictService.findDictById(DDT.BH_FJX_LEN).getEnum_value());
		modelMap.put("isNotShowSp_price", DDT.isNotShowSp_price);//是否报货验货显示价格列金色三麦用
		modelMap.put("isDistributionUnit", DDT.isNotShowSp_price);//是否根据配送单位报货
		if("Y".equals(DDT.isChkstomJmj)){//九毛九的报货页面
			return new ModelAndView(ChkstomMisConstants.TABLE_CHKSTOMBACKMIS_JMJ);
		}
		return new ModelAndView(ChkstomMisConstants.TABLE_CHKSTOMBACKMIS);
	}
	
	/***
	 * 新增或修改保存
	 * @param session
	 * @param sta
	 * @param chkstom
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveByAddOrUpdate")
	@ResponseBody
	public Object saveByAddOrUpdate(HttpSession session,String sta,Chkstom chkstom) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
				"MISBOH保存报货单,单号:"+chkstom.getChkstoNo(),session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		//当前帐套
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		if("add".equals(sta)){
			chkstom.setVouno(calChkNum.getNextBytable(DDT.VOUNO_CHKSTOM,DDT.VOUNO_TH+chkstom.getFirm()+"-",chkstom.getMaded()));
		}else{
			Chkstom chkstom1 = chkstomMisService.findByChkstoNo(chkstom);
			chkstom.setVouno(chkstom1.getVouno());
		}
		if(null == chkstom.getCreateTyp() || 0 == chkstom.getCreateTyp())
			chkstom.setCreateTyp(Integer.parseInt(DDT.HANDINHAND));
		if(null == chkstom.getManifsttyp())
			chkstom.setManifsttyp("");
		if(null == chkstom.getTyp())
			chkstom.setTyp(DDT.BHTH);
		return chkstomMisService.saveOrUpdateChk(chkstom, sta);
	}
	
	/***
	 * 审核报货单
	 * @param session
	 * @param chkstom
	 * @param chkstoNoIds
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkChkstom")
	@ResponseBody
	public Object checkChkstom(HttpSession session,Chkstom chkstom,String chkstoNos) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CHECK,
				"MISBOH审核报货单,单号:"+chkstom.getChkstoNo()==null?chkstoNos:chkstom.getChkstoNo()+"",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		try{
			String accountName = session.getAttribute("accountName").toString();
			chkstom.setChecby(accountName);
			String accountNames = session.getAttribute("accountNames").toString();
			chkstom.setChecbyName(accountNames);
			String acct = session.getAttribute("ChoiceAcct").toString();
			chkstom.setAcct(acct);
			//需要判断是否总部审核
			Positn positn = new Positn();
			positn.setAcct(acct);
			positn.setCode(chkstom.getFirm());
			Positn p = positnService.findPositnByCode(positn);
			String ynZbChk = p.getYnZbChk();
			if("Y".equals(ynZbChk)){//如果总部审核，这里的审核则随便改个状态,门店不可编辑，总部可编辑
				return chkstomMisService.checkChkstom(chkstom,chkstoNos);
			}else{//如果不需要总部审核 ，这里的审核则走总部审核
				//判断是否有需要总部特殊审核的物资，有就走门店审核
				String[] ids = null;
				if(null!=chkstoNos && !"".equals(chkstoNos)){
					ids = chkstoNos.split(",");
				}else{
					ids = chkstom.getChkstoNo().toString().split(",");
				}
				StringBuffer zb = new StringBuffer();
				StringBuffer md = new StringBuffer();
				for(String id : ids){
					Chkstom c = new Chkstom();
					c.setChkstoNo(Integer.parseInt(id));
					int count = chkstomMisService.findStoCheckByChkstom(c);//是否有特殊审核的物资
					if(count == 0){
						zb.append(id+",");
					}else{
						md.append(id+",");
					}
				}
				if(zb.length() > 0){
					return chkstomMisService.checkChk(chkstom,zb.substring(0, zb.length()-1));
				}
				if(md.length() > 0){
					return chkstomMisService.checkChkstom(chkstom,md.substring(0, md.length()-1));
				}
				return null;
			}
		}catch(Exception e){
			return e.getMessage();
		}
	}
	
	/***
	 * 删除报货单
	 * @param session
	 * @param chkstoNoIds
	 * @param chkstom
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/deleteChkstom")
	@ResponseBody
	public Object deleteChkstom(HttpSession session,String chkstoNoIds,Chkstom chkstom) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,
				"MISBOH删除报货单,单号:"+chkstom.getChkstoNo()==null?chkstoNoIds:chkstom.getChkstoNo()+"",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		//报货单填制页面上的整条删除
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		return chkstomMisService.deleteChkstom(chkstom,chkstoNoIds);
	}
	
	/***
	 * 查找报货单
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param sp_code
	 * @param init
	 * @param chkstom
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchByKey")
	public ModelAndView searchByKey(ModelMap modelMap,HttpSession session,Page page,String sp_code,String init,Chkstom chkstom) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收前台参数
		if(null == chkstom.getTyp())
			chkstom.setTyp(CodeHelper.replaceCode(DDT.BHTH));//报货退货
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		HashMap<String, Object> chkstomMap=new HashMap<String, Object>();
		if(null!=init && !"".equals(init)){
			chkstom.setbMaded(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			chkstom.seteMaded(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		}
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			String positnCode = thePositn.getCode();
			chkstom.setFirm(positnCode);
		}
		chkstomMap.put("chkstom", chkstom);
		chkstomMap.put("sp_code", sp_code);
		if("Y".equals(DDT.isChkstomJmj)){//九毛九的报货页面
			chkstomMap.put("hoped", "hoped");
		}
		modelMap.put("chkstomList", chkstomMisService.findByKey(chkstomMap,page));
		modelMap.put("chkstom", chkstom);
		modelMap.put("sp_code", sp_code);
		modelMap.put("pageobj", page);
		modelMap.put("isNotShowSp_price", DDT.isNotShowSp_price);//是否报货验货显示价格列金色三麦用
		if("Y".equals(DDT.isChkstomJmj)){//九毛九的报货页面
			return new ModelAndView(ChkstomMisConstants.SEARCH_CHKSTOM_JMJ,modelMap);
		}
		return new ModelAndView(ChkstomMisConstants.SEARCH_CHKSTOM,modelMap);
	}
	
	/***
	 * 双击报货单 查详细
	 * @param modelMap
	 * @param session
	 * @param chkstod
	 * @param chkstom
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findChk")
	public ModelAndView findChk(ModelMap modelMap,HttpSession session,Chkstod chkstod,Chkstom chkstom) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//从报货单弹窗查询页面上的双击单条数据，进行查看申购的物资详细
		chkstod.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("sta", "show");
		chkstom = chkstomMisService.findByChkstoNo(chkstom);
		modelMap.put("chkstom", chkstom);
		List<Chkstod> chkstodList = chkstomMisService.findByChkstoNo(chkstod);
		modelMap.put("chkstodList", chkstodList);
		modelMap.put("bhfl", dictService.findDictById(DDT.BH_FJX_LEN).getEnum_value());
		modelMap.put("isNotShowSp_price", DDT.isNotShowSp_price);//是否报货验货显示价格列金色三麦用
		modelMap.put("isDistributionUnit", DDT.isDistributionUnit);//是否根据配送单位报货
		if("Y".equals(DDT.isChkstomJmj)){//九毛九的报货页面
			//到货日  九毛九用
			if(chkstodList.size() > 0){
				chkstom.setHoped(DateFormat.getDateByString(chkstodList.get(0).getHoped(),"yyyy-MM-dd"));
			}
			return new ModelAndView(ChkstomMisConstants.TABLE_CHKSTOMBACKMIS_JMJ);
		}
		return new ModelAndView(ChkstomMisConstants.TABLE_CHKSTOMBACKMIS);
	}
	
	/**
	 * 报货单打印
	 * @param modelMap
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printChkstom")
	public ModelAndView printChkstomm(ModelMap modelMap,HttpSession session,Page page,String type,Chkstod chkstod)throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收参数，根据关键字查询收，进行结果集的打印
		HashMap<String, Object> disMap=new HashMap<String, Object>();
		chkstod.setAcct(session.getAttribute("ChoiceAcct").toString());
		disMap.put("chkstod", chkstod);
		List<Chkstod> list=chkstomMisService.findByChkstoNo(chkstod);
 		HashMap<String,Object>  parameters = new HashMap<String,Object>();
        String report_name=new String("报货数据打印");
        String report_date=DateFormat.getStringByDate(new Date(), "yyyy-MM-dd");      
        parameters.put("report_name", report_name); 
        parameters.put("report_date", report_date);
        parameters.put("chkstoNo", chkstod.getChkstoNo());
        Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			parameters.put("firm", thePositn.getDes());
		}
 	    modelMap.put("List",list);  
        modelMap.put("parameters", parameters);     
        modelMap.put("actionMap", disMap);//回调参数
	 	modelMap.put("action", "/chkstomBackMis/printChkstom.do?chkstoNo="+chkstod.getChkstoNo());//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,ChkstomMisConstants.REPORT_PRINT_URL,ChkstomMisConstants.REPORT_PRINT_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 导出
	 * @param response
	 * @param request
	 * @param session
	 * @param chkstod
	 * @param chkstom
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/exportChkstom")
	@ResponseBody
	public boolean exportChkstom(HttpServletResponse response,HttpServletRequest request,HttpSession session,Chkstod chkstod,Chkstom chkstom) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//从报货单弹窗查询页面上的双击单条数据，进行查看申购的物资详细
		chkstod.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstom = chkstomMisService.findByChkstoNo(chkstom);
		List<Chkstod> chkstodList = chkstomMisService.findByChkstoNo(chkstod);
		String fileName = "报货单";
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		return chkstomMisService.exportChkstom(response.getOutputStream(),chkstodList,chkstom);
	}
	
	/***
	 * 根据仓位和物资查询方向
	 * @param positn
	 * @param supply
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findTypByPositn")
	@ResponseBody
	public String findTypByPositn(Positn positn,Supply supply) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String result="RK";
		positn = positnService.findPositnByCode(positn);
		String typ = positn.getTyp();
		if("1201".equals(typ)||"1202".equals(typ)){
			return "RK";
		}else{//仓位是分店，判断是不是出库方向，只有出库方向才查售价
			//1.查物资是否设置了直发
			supply = supplyService.findSupplyById(supply);
			if("Y".equals(supply.getInout())){
				return "RK";
			}else{
				//2.查配送片区默认仓位 如果是基地仓库，则是出库 ，如果为空，则查supply的sp_position
				Positn spcodemod = supplyService.findSpcodeMod(supply.getSp_code(),positn.getCode());
				if(null == spcodemod){
					Positn positn1 = new Positn();
					positn1.setCode(supply.getSp_position());
					spcodemod = positnService.findPositnByCode(positn1);
				}
				if(null != spcodemod && "1202".equals(spcodemod.getTyp())){
					return "CK";
				}
			}
		}
		return result;
	}
	
    /**
     * 确认当前时间是否可订货
     * @author 文清泉
     * @param 2015年1月1日 下午3:44:38
     * @param dgg
     * @param session
     * @return
     * @throws CRUDException 
     */
	@RequestMapping(value = "/eqOrderTime")
    @ResponseBody
    public String eqOrderTime(ModelMap modelMap,Chkstom chk,HttpSession session) {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
    	String orderTime = chkstomMisService.eqOrderTime(DateJudge.YYYY_MM_DD.format(chk.getMaded()), chk.getFirm());
    	return orderTime;
    }
    
}
