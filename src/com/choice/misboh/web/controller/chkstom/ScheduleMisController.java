package com.choice.misboh.web.controller.chkstom;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.constants.chkstom.ScheduleMisConstants;
import com.choice.misboh.domain.chkstom.ScheduleD;
import com.choice.misboh.service.chkstom.ScheduleMisService;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Positn;
import com.choice.scm.service.CodeDesService;

/***
 * 配送班表
 * @author wjf
 *
 */
@Controller
@RequestMapping("scheduleMis")
public class ScheduleMisController {

	@Autowired
	private ScheduleMisService scheduleMisService;
	@Autowired
	private CodeDesService codeDesService;
	
	/**
	 * 查询配送班表 list
	 * @param modelMap
	 * @param schedule
	 * @throws Exception
	 * @author wjf
	 */
	@RequestMapping(value = "/list")
	public ModelAndView findSchedule(HttpSession session, ModelMap modelMap, ScheduleD scheduleD,String month,String sta) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//1.根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			String positnCode = thePositn.getCode();
			scheduleD.setPositnCode(positnCode);
		}
		modelMap.put("scheduleD", scheduleD);
		modelMap.put("month", month);
		modelMap.put("sta",sta);
		//报货类别
		CodeDes code = new CodeDes();
		code.setTyp(DDT.CODEDES11);
		modelMap.put("grptypList", codeDesService.findCodeDes(code));
		return new ModelAndView(ScheduleMisConstants.LIST_SCHEDULE,modelMap);
	}
	
	/**
	 * 配送班明细表(按天)
	 * @throws Exception
	 */
	@RequestMapping("/findScheduleByMonth")
	@ResponseBody
	public Object findScheduleByMonth(ModelMap modelMap, ScheduleD scheduleD, String scheduleMonth) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return scheduleMisService.findScheduleByMonth(scheduleD, scheduleMonth);
	}

}
