package com.choice.misboh.web.controller.chkstom;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ProgramConstants;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.constants.chkstom.ChkstomMisConstants;
import com.choice.misboh.constants.chkstom.ChkstomMisJMJConstants;
import com.choice.misboh.domain.chkstom.Purchasedetails;
import com.choice.misboh.service.chkstom.ChkstomDeptMisService;
import com.choice.misboh.service.chkstom.ChkstomMisService;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Chkstom;
import com.choice.scm.domain.Positn;
import com.choice.scm.service.PositnService;
import com.choice.scm.util.CalChkNum;
import com.choice.scm.util.ReadReportUrl;

/***
 * 九毛九报货相关Controller
 * @author wjf
 *
 */
@Controller
@RequestMapping(value = "chkstomMisJMJ")
public class ChkstomMisJMJController {

	@Autowired
	private ChkstomDeptMisService chkstomDeptMisService;
	@Autowired
	private PositnService positnService;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private ChkstomMisService chkstomMisService;
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	
	/***
	 * 进入档口报货页面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/table")
	public ModelAndView findAllChkstom(ModelMap modelMap) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(ChkstomMisJMJConstants.TABLE_CHKSTOMDEPTMIS);
	}
	
	/***
	 * 点击新增报货单
	 * @param modelMap
	 * @param session
	 * @param chkstom
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkstom")
	public ModelAndView addChkstom(ModelMap modelMap,HttpSession session,Chkstom chkstom) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//当前登录用户
		String accountName=session.getAttribute("accountName").toString();
		chkstom.setMadeby(accountName);
		String accountNames=session.getAttribute("accountNames").toString();
		chkstom.setMadebyName(accountNames);
		chkstom.setMaded(new Date());
		//获取最大单号
		chkstom.setChkstoNo(chkstomDeptMisService.getMaxChkstono());
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			String positnCode = thePositn.getCode();
			chkstom.setFirm(positnCode);
		}
		modelMap.put("chkstom", chkstom);
		modelMap.put("sta", "add");
		return new ModelAndView(ChkstomMisJMJConstants.TABLE_CHKSTOMDEPTMIS);
	}	
	
	/***
	 * 新增或修改保存
	 * @param session
	 * @param sta
	 * @param chkstom
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveByAddOrUpdate")
	@ResponseBody
	public Object saveByAddOrUpdate(HttpSession session,String sta,Chkstom chkstom) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
				"MISBOH保存档口报货单,单号:"+chkstom.getChkstoNo(),session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		//当前帐套
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		if("add".equals(sta)){
			chkstom.setVouno(calChkNum.getNextBytable(DDT.VOUNO_CHKSTOM,DDT.VOUNO_BH+chkstom.getFirm()+"-",chkstom.getMaded()));
		}else{
			Chkstom chkstom1 = chkstomDeptMisService.findByChkstoNo(chkstom);
			chkstom.setVouno(chkstom1.getVouno());
		}
		return chkstomDeptMisService.saveOrUpdateChk(chkstom, sta);
	}
	
	/***
	 * 删除报货单
	 * @param session
	 * @param chkstoNoIds
	 * @param chkstom
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/deleteChkstom")
	@ResponseBody
	public Object deleteChkstom(HttpSession session,String chkstoNoIds,Chkstom chkstom) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,
				"MISBOH删除档口报货单,单号:"+chkstom.getChkstoNo()==null?chkstoNoIds:chkstom.getChkstoNo()+"",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		//报货单填制页面上的整条删除
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		return chkstomDeptMisService.deleteChkstom(chkstom,chkstoNoIds);
	}
	
	/***
	 * 查找报货单
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param sp_code
	 * @param init
	 * @param chkstom
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchByKey")
	public ModelAndView searchByKey(ModelMap modelMap,HttpSession session,Page page,String sp_code,String init,Chkstom chkstom) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收前台参数
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		HashMap<String, Object> chkstomMap=new HashMap<String, Object>();
		if(null!=init && !"".equals(init)){
			chkstom.setbMaded(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			chkstom.seteMaded(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			if (null != thePositn) {
				String positnCode = thePositn.getCode();
				chkstom.setFirm(positnCode);
			}
		}
		chkstomMap.put("hoped", "hoped");
		chkstom.setBak1("N");
		chkstomMap.put("chkstom", chkstom);
		chkstomMap.put("sp_code", sp_code);
		modelMap.put("chkstomList", chkstomDeptMisService.findByKey(chkstomMap,page));
		modelMap.put("chkstom", chkstom);
		modelMap.put("sp_code", sp_code);
		modelMap.put("pageobj", page);
		return new ModelAndView(ChkstomMisJMJConstants.SEARCH_CHKSTOM,modelMap);
	}
	
	/***
	 * 双击报货单 查详细
	 * @param modelMap
	 * @param session
	 * @param chkstod
	 * @param chkstom
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findChk")
	public ModelAndView findChk(ModelMap modelMap,HttpSession session,Chkstod chkstod,Chkstom chkstom) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//从报货单弹窗查询页面上的双击单条数据，进行查看申购的物资详细
		chkstod.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("sta", "show");
		chkstom = chkstomDeptMisService.findByChkstoNo(chkstom);
		modelMap.put("chkstom", chkstom);
		List<Chkstod> chkstodList = chkstomDeptMisService.findByChkstoNo(chkstod);
		modelMap.put("chkstodList", chkstodList);
		//到货日  九毛九用
		if(chkstodList.size() > 0){
			chkstom.setHoped(DateFormat.getDateByString(chkstodList.get(0).getHoped(),"yyyy-MM-dd"));
		}
		return new ModelAndView(ChkstomMisJMJConstants.TABLE_CHKSTOMDEPTMIS);
	}
	
	/**
	 * 报货单打印
	 * @param modelMap
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printChkstom")
	public ModelAndView printChkstomm(ModelMap modelMap,HttpSession session,Page page,String type,Chkstom chkstom,Chkstod chkstod)throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收参数，根据关键字查询收，进行结果集的打印
		HashMap<String, Object> disMap=new HashMap<String, Object>();
		chkstod.setAcct(session.getAttribute("ChoiceAcct").toString());
		disMap.put("chkstod", chkstod);
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstom = chkstomDeptMisService.findByChkstoNo(chkstom);
		List<Chkstod> list=chkstomDeptMisService.findByChkstoNo(chkstod);
 		HashMap<String,Object>  parameters = new HashMap<String,Object>(); 
        String report_name=new String("九毛九报货数据打印");
        String report_date=DateFormat.getStringByDate(new Date(), "yyyy-MM-dd");      
        parameters.put("report_name", report_name); 
        parameters.put("report_date", report_date);
        parameters.put("chkstoNo", chkstod.getChkstoNo());
        Positn thePositn = (Positn)session.getAttribute("accountPositn");
		parameters.put("firm", thePositn.getDes());
 	    modelMap.put("List",list);  
        modelMap.put("parameters", parameters);     
        modelMap.put("actionMap", disMap);//回调参数
	 	modelMap.put("action", "/chkstomMisJMJ/printChkstom.do?chkstoNo="+chkstod.getChkstoNo());//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,ChkstomMisConstants.REPORT_PRINT_URL,ChkstomMisConstants.REPORT_PRINT_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/***
	 * 档口报货上传
	 * @param modelMap
	 * @param session
	 * @param chkstom
	 * @param page
	 * @param sp_code
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listCheckedChkstom")
	public ModelAndView listCheckedChkstom(ModelMap modelMap,HttpSession session, Chkstom chkstom,Page page,String sp_code) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收前台参数
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		HashMap<String, Object> chkstomMap=new HashMap<String, Object>();
		if(null == chkstom.getbMaded()){
			chkstom.setbMaded(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			chkstom.seteMaded(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			if (null != thePositn) {
				String positnCode = thePositn.getCode();
				chkstom.setFirm(positnCode);
			}
			chkstom.setBak1("N");
		}
		chkstomMap.put("hoped", "hoped");
		chkstomMap.put("chkstom", chkstom);
		chkstomMap.put("sp_code", sp_code);
		modelMap.put("chkstomList", chkstomDeptMisService.findByKey(chkstomMap,page));
		modelMap.put("chkstom", chkstom);
		modelMap.put("sp_code", sp_code);
		modelMap.put("pageobj", page);
		return new ModelAndView(ChkstomMisJMJConstants.LIST_CHKSTOM,modelMap);
	}
	
	/***
	 * 双击查找详情
	 * @param modelMap
	 * @param session
	 * @param chkstom
	 * @param chkstod
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findDetail")
	public ModelAndView findDetail(ModelMap modelMap,HttpSession session,Chkstom chkstom, Chkstod chkstod) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//从报货单弹窗查询页面上的双击单条数据，进行查看申购的物资详细
		chkstod.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstom = chkstomDeptMisService.findByChkstoNo(chkstom);
		modelMap.put("chkstom", chkstom);
		modelMap.put("chkstodList", chkstomDeptMisService.findByChkstoNo(chkstod));
		return new ModelAndView(ChkstomMisJMJConstants.LIST_CHKSTOD,modelMap);
	}
	
	/**
	 * 生成门店报货单
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toChkstom")
	public ModelAndView update(ModelMap modelMap, Chkstom chkstom, String idList, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountName=session.getAttribute("accountName").toString();
		chkstom.setMadeby(accountName);
		String accountNames=session.getAttribute("accountNames").toString();
		chkstom.setMadebyName(accountNames);
		chkstom.setMaded(new Date());
		//获取最大单号
		chkstom.setChkstoNo(chkstomMisService.getMaxChkstono());
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			String positnCode = thePositn.getCode();
			chkstom.setFirm(positnCode);
		}
		modelMap.put("chkstom", chkstom);
		modelMap.put("idList", idList);
		Chkstod chkstod = new Chkstod();
		chkstod.setChkstoNos(idList);
		chkstod.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("chkstodList", chkstomDeptMisService.findByChkstoNosJMJ(chkstod));
		return new ModelAndView(ChkstomMisJMJConstants.TABLECHKSTOMFIRM,modelMap);
	}
	
	/***
	 * 生成门店的报货单
	 * @param modelMap
	 * @param idList
	 * @param session
	 * @param chkstom
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveByAddOrUpdateDept")
	@ResponseBody
	public Object saveByAddOrUpdateDept(ModelMap modelMap, String idList, HttpSession session, Chkstom chkstom) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
				"MISBOH档口报货单上传,生成门店报货单,单号:"+chkstom.getChkstoNo(),session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		try{
			//当前帐套
			chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
			chkstom.setVouno(calChkNum.getNextBytable(DDT.VOUNO_CHKSTOM,DDT.VOUNO_BH+chkstom.getFirm()+"-",new Date()));
			chkstomDeptMisService.saveOrUpdateChkDept(chkstom, idList);
			//审核
			String accountName = session.getAttribute("accountName").toString();
			chkstom.setChecby(accountName);
			//需要判断是否总部审核
			Positn positn = new Positn();
			positn.setAcct(session.getAttribute("ChoiceAcct").toString());
			positn.setCode(chkstom.getFirm());
			Positn p = positnService.findPositnByCode(positn);
			String ynZbChk = p.getYnZbChk();
			if("Y".equals(ynZbChk)){//如果总部审核，这里的审核则随便改个状态,门店不可编辑，总部可编辑
				return chkstomMisService.checkChkstom(chkstom,null);
			}else{//如果不需要总部审核 ，这里的审核则走总部审核
				return chkstomMisService.checkChk(chkstom,null);
			}
		}catch(Exception e){
			return e.getMessage();
		}
	}
	
	/*********************************************九毛九供应商邮件查询 开始****************************************************/
	
	/***
	 * 供应商邮件查询 
	 * @param modelMap
	 * @param session
	 * @param pd
	 * @param page
	 * @return
	 */
	@RequestMapping("/listDeliverMail")
	public ModelAndView listDeliverMail(ModelMap modelMap, HttpSession session, Purchasedetails pd, Page page){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null == pd.getBdate()){
			pd.setBdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
			pd.setEdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		}
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		pd.setFirm(thePositn.getCode());
		List<Purchasedetails> pdList = chkstomMisService.findPdList(pd,page);
		modelMap.put("pd", pd);
		modelMap.put("pdList", pdList);
		modelMap.put("pageobj", page);
		return new ModelAndView(ChkstomMisJMJConstants.LISTDELIVERMAIL,modelMap);
	}
}
