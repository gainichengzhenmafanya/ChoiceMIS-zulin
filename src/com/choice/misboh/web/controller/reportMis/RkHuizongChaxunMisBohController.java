package com.choice.misboh.web.controller.reportMis;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.excel.GenerateExcel;
import com.choice.misboh.constants.reportMis.MisBohStringConstant;
import com.choice.misboh.constants.reportMis.SupplyAcctMisBohConstants;
import com.choice.misboh.service.reportMis.RkHuizongChaxunMisBohService;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.PositnService;
import com.choice.scm.util.ReadReportUrl;
@Controller
@RequestMapping("RkHuizongChaxunMisBoh")
public class RkHuizongChaxunMisBohController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private RkHuizongChaxunMisBohService rkHuizongChaxunMisBohService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private GenerateExcel<Map<String,Object>> exportExcel;
	@Autowired
	private PositnService positnService;
	
	/********************************************入库汇总查询报表****************************************************/
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findChkinSQHeaders")
	@ResponseBody
	public Object getChkinSQHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_CHKINSUMQUERYMISBOH);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, MisBohStringConstant.BASICINFO_REPORT_CHKINSUMQUERYMISBOH));
		columns.put("frozenColumns", MisBohStringConstant.BASICINFO_REPORT_CHKINSUMQUERYMISBOH_FROZEN);
		return columns;
	}
	
	/**
	 * 跳转到入库汇总查询报表页面
	 * @return
	 */
	@RequestMapping("/toChkinSumQuery")
	public ModelAndView toChkinSumQuery(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", MisBohStringConstant.REPORT_NAME_CHKINSUMQUERYMISBOH);
		return new ModelAndView(SupplyAcctMisBohConstants.REPORT_SHOW_CHKINSUMQUERY_MIS,modelMap);
	}
	
	/**
	 * 查询入库汇总查询报表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findChkinSumQuery")
	@ResponseBody
	public Object ChkinSumQuery(ModelMap modelMap, HttpSession session, String page, String rows,
			String sort, String order, SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		String accountId=session.getAttribute("accountId").toString();
		setDept(session,supplyAcct);//设置部门
		supplyAcct.setAccountId(accountId);
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return rkHuizongChaxunMisBohService.findChkinSumQuery(condition,pager);
	}
	
	/**
	 * 打印入库汇总查询报表
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printChkinSumQuery")
	public ModelAndView printChkinSumQuery(ModelMap modelMap, Page pager, HttpSession session, String type,
			SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		String accountId=session.getAttribute("accountId").toString();
		String positn = supplyAcct.getPositn();
		setDept(session,supplyAcct);//设置部门
		supplyAcct.setAccountId(accountId);
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		condition.put("supplyAcct", supplyAcct);
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		params.put("positn",positn);
		params.put("accountId",supplyAcct.getAccountId());
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("sp_code",supplyAcct.getSp_code());
		if(supplyAcct.getEdat() != null)
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		params.put("grp",supplyAcct.getGrp());
		params.put("typ",supplyAcct.getTyp());
		params.put("chktag",String.valueOf(supplyAcct.getChktag()));
		params.put("chktyp",supplyAcct.getChktyp());
		params.put("delivercode",supplyAcct.getDelivercode());
		modelMap.put("List",rkHuizongChaxunMisBohService.findChkinSumQuery(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "入库汇总查询");
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/RkHuizongChaxunMisBoh/printChkinSumQuery.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctMisBohConstants.REPORT_PRINT_URL_CHKINSUMQUERY,SupplyAcctMisBohConstants.REPORT_EXP_URL_CHKINSUMQUERY);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	
	/**
	 * 导出入库汇总查询报表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportChkinSumQuery")
	@ResponseBody
	public void exportChkinSumQuery(HttpServletResponse response, String sort, String order,
			HttpServletRequest request, HttpSession session, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "入库汇总查询报表";
		Map<String,Object> condition = new HashMap<String,Object>();
		String accountId=session.getAttribute("accountId").toString();
		setDept(session,supplyAcct);//设置部门
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		supplyAcct.setAccountId(accountId);
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_CHKINSUMQUERYMISBOH);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		ReportObject<Map<String,Object>> result = rkHuizongChaxunMisBohService.findChkinSumQuery(condition,pager);
		if(null != result.getFooter())
			result.getRows().addAll(result.getFooter());
//		exportExcel.creatWorkBook(response.getOutputStream(), rkHuizongChaxunMisBohService.findChkinSumQuery(condition,pager).getRows(), "入库汇总查询", dictColumnsService.listDictColumnsByTable(dictColumns));
		exportExcel.creatWorkBook_DictColumns(fileName, request, response, result.getRows(), "入库汇总查询", dictColumnsService.listDictColumnsByTable(dictColumns), MisBohStringConstant.EXCEL_CHKINSUMQUERYMISBOH);
	}
	
	/***
	 * 查询部门的方法
	 * @param session
	 * @param supplyAcct
	 * @throws CRUDException
	 */
	private void setDept(HttpSession session, SupplyAcct supplyAcct) throws CRUDException{
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		supplyAcct.setPositnNm(thePositn.getCode());//向阳渔港入库汇总报表查总部出库数用
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(supplyAcct.getPositn()==null || "".equals(supplyAcct.getPositn())){//选择档口为空时，默认查询该分店的入库明细
			if(null != thePositn){
				//查询该分店的档口
				Positn positn = new Positn();
				positn.setUcode(thePositn.getCode());
				List<Positn> list = positnService.findPositnSuperNOPage(positn);
				String positncode = "";
				for(Positn posi:list){
					positncode +=CodeHelper.replaceCode(posi.getCode())+",";
				}
				positncode+=CodeHelper.replaceCode(thePositn.getCode());
				supplyAcct.setPositn(positncode);
			}
		}else{
			supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		}
	}
}
