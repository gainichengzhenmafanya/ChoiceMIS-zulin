package com.choice.misboh.web.controller.reportMis;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.MisUtil;
import com.choice.misboh.commonutil.ReadReport;
import com.choice.misboh.commonutil.report.ExportExcel;
import com.choice.misboh.commonutil.report.ReadReportConstants;
import com.choice.misboh.constants.reportMis.MISBOHNewReportConstant;
import com.choice.misboh.constants.reportMis.MisStringConstant;
import com.choice.misboh.domain.common.CommonMethod;
import com.choice.misboh.domain.reportMis.PublicEntity;
import com.choice.misboh.domain.reportMis.ReportObject;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.misboh.service.reportMis.MisBohNewReportService;

/**
 * 描述：门店BOH项目报表
 * @author 马振
 * 创建时间：2015-7-28 下午5:08:52
 */
@Controller
@RequestMapping("/MISBOHNewreport")
public class MisBohNewReportController extends MisBohBaseController{

	@Autowired
	private MisBohNewReportService newReportService;
	
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	
	/**
	 * 描述：跳转到报表页面
	 * @author 马振
	 * 创建时间：2016-2-22 上午11:08:07
	 * @param modelMap
	 * @param reportName
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toReport")
	public ModelAndView toReport(ModelMap modelMap,String reportName,HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("sftList", commonMISBOHService.findAllShiftsftByStore(new CommonMethod(), session));
		modelMap.put("pk_store", commonMISBOHService.getPkStore(session).getPk_store());
		getRoleTiem(modelMap,session);
		return new ModelAndView(ReadReportConstants.getReportURL(MISBOHNewReportConstant.class, reportName),modelMap);
	}
	
	/**
	 * 描述：报表导出
	 * @author 马振
	 * 创建时间：2015-4-23 下午2:17:51
	 * @param response
	 * @param request
	 * @param session
	 * @param publicEntity
	 * @param reportName
	 * @param headers
	 * @throws CRUDException
	 */
	@RequestMapping("/exportReport")
	public void exportReport(HttpServletResponse response,HttpServletRequest request,HttpSession session,PublicEntity publicEntity,String reportName,String headers,String page,String rows) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		//设置分页，查询所有数据
		publicEntity.getPager().setPageSize(Integer.MAX_VALUE);
		publicEntity.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		publicEntity.setFirmdes(commonMISBOHService.getPkStore(session).getVname());	//获取门店名称放入PublicEntity
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ReadReport.getReportName(MisStringConstant.class, reportName));
		
		//获取并执行查询方法，获取查询结果
		ReportObject<Map<String,Object>> result = MisUtil.execMethod(newReportService, reportName, publicEntity);
		if(null != result.getFooter())
			result.getRows().addAll(result.getFooter());
		
		if(null != headers || !"".equals(headers))
			ExportExcel.creatWorkBook(request,response,result.getRows(),ReadReport.getReportNameCN(MisStringConstant.class, reportName),null,headers);
		else
			ExportExcel.creatWorkBook(ReadReport.getReportNameCN(MisStringConstant.class, reportName),request,response, result.getRows(), ReadReport.getReportNameCN(MisStringConstant.class, reportName), dictColumnsService.listDictColumnsByAccount(dictColumns, ReadReport.getDefaultHeader(MisStringConstant.class, reportName)));
	}
	
	/***************************************************九毛九报表开始**********************************************************/
		
	/**
	 * 查询营业日报表 
	 * @param modelMap
	 * @param condition
	 * @return
	 * @throws CRUDException
	 * @author YGB
	 */
	@RequestMapping("/queryYYRBB")
	public ModelAndView queryYYRBB(ModelMap modelMap,HttpSession session,PublicEntity condition) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null == condition.getBdat() && null == condition.getEdat()){
			condition.setBdat(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
			condition.setEdat(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		}
		condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		List<Map<String, Object>> paymentList = newReportService.findPayment(condition);
		List<Map<String, Object>> paymodeList = newReportService.findPaymode(condition);
		Map<String, Object> payCal = newReportService.findPayCal(condition);
		List<Map<String, Object>> marsaleClassList = newReportService.findMarsaleClass(condition);
		List<Map<String, Object>> marsaleClassTwoList = newReportService.findMarsaleClassTwo(condition);
		Map<String, Object> marsaleClassCal = newReportService.findMarsaleClassCal(condition);
		List<Map<String, Object>> shaoamtList = newReportService.findShaoamt(condition);
		Map<String, Object> shaoamtCal = newReportService.findShaoamtCal(condition);
		List<Map<String, Object>> duoamtList = newReportService.findDuoamt(condition);
		Map<String, Object> duoamtCal = newReportService.findDuoamtCal(condition);
		Map<String, Object> ncouponcamt = newReportService.findNcouponcamt(condition);
		Map<String, Object> ncashamt = newReportService.findNcashamt(condition);
		List<Map<String, Object>> billList = newReportService.findBillList(condition);
		Map<String, Object> billListCal = newReportService.findCalBillList(condition);
		Map<String, Object> stockdata = newReportService.findStockdata(condition);
		Map<String, Object> cancel = newReportService.findCancel(condition);
		Map<String, Object> fcnt = newReportService.findFcnt(condition);
		
		modelMap.put("condition",condition);
		modelMap.put("paymentList",paymentList);
		modelMap.put("paymodeList",paymodeList);
		modelMap.put("payCal",payCal);
		modelMap.put("marsaleClassList",marsaleClassList);
		modelMap.put("marsaleClassTwoList",marsaleClassTwoList);
		modelMap.put("marsaleClassCal",marsaleClassCal);
		modelMap.put("shaoamtList",shaoamtList);
		modelMap.put("shaoamtCal",shaoamtCal);
		modelMap.put("duoamtList",duoamtList);
		modelMap.put("duoamtCal",duoamtCal);
		modelMap.put("ncouponcamt",ncouponcamt);
		modelMap.put("ncashamt",ncashamt);
		modelMap.put("billList",billList);
		modelMap.put("billListCal",billListCal);
		modelMap.put("stockdata",stockdata);
		modelMap.put("cancel",cancel);
		modelMap.put("fcnt",fcnt);
		modelMap.put("pk_store", commonMISBOHService.getPkStore(session).getPk_store());
		
		modelMap.put("uploadFlag",newReportService.findUploadFlag(condition));
		getRoleTiem(modelMap,session);
		return new ModelAndView("misboh/reportMis/businessAnalysis/yyrbb",modelMap);
	}
	
	/**
	 * 查询营业日报表 _报表导出excel
	 * @param modelMap
	 * @param condition
	 * @return
	 * @throws CRUDException
	 * @author YGB
	 */
	@RequestMapping("/exportYYRBBReport")
	public void exportYYRBBReport(HttpServletResponse response,HttpServletRequest request,PublicEntity condition) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null == condition.getBdat() && null == condition.getEdat()){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			condition.setBdat(sdf.format(DateFormat.getDateBefore(new Date(),"day",-1,1)));
			condition.setEdat(sdf.format(DateFormat.getDateBefore(new Date(),"day",-1,1)));
		}
		List<Map<String, Object>> paymentList = newReportService.findPayment(condition);
		List<Map<String, Object>> paymodeList = newReportService.findPaymode(condition);
		Map<String, Object> payCal = newReportService.findPayCal(condition);
		List<Map<String, Object>> marsaleClassList = newReportService.findMarsaleClass(condition);
		List<Map<String, Object>> marsaleClassTwoList = newReportService.findMarsaleClassTwo(condition);
		Map<String, Object> marsaleClassCal = newReportService.findMarsaleClassCal(condition);
		List<Map<String, Object>> shaoamtList = newReportService.findShaoamt(condition);
		Map<String, Object> shaoamtCal = newReportService.findShaoamtCal(condition);
		List<Map<String, Object>> duoamtList = newReportService.findDuoamt(condition);
		Map<String, Object> duoamtCal = newReportService.findDuoamtCal(condition);
		Map<String, Object> ncouponcamt = newReportService.findNcouponcamt(condition);
		Map<String, Object> ncashamt = newReportService.findNcashamt(condition);
		List<Map<String, Object>> billList = newReportService.findBillList(condition);
		Map<String, Object> billListCal = newReportService.findCalBillList(condition);
		Map<String, Object> stockdata = newReportService.findStockdata(condition);
		Map<String, Object> cancel = newReportService.findCancel(condition);
		Map<String, Object> fcnt = newReportService.findFcnt(condition);
		
		newReportService.exportYYRBBReport(request, response, paymentList, paymodeList, payCal, marsaleClassList, marsaleClassTwoList, marsaleClassCal, shaoamtList, 
				shaoamtCal, duoamtList, duoamtCal,ncouponcamt,ncashamt,billList,billListCal,stockdata,cancel,fcnt,condition);
	}
	
	/***************************************************九毛九报表结束**********************************************************/
	
	/***************************************************西贝报表开始**********************************************************/
	
	/**
	 * 描述：西贝营收客流分段统计明细报表表头
	 * @author 马振
	 * 创建时间：2016-2-22 上午11:15:24
	 * @param session
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findItemTypeSaleHeaders")
	@ResponseBody
	public Object findItemTypeSaleHeaders(HttpSession session, PublicEntity condition) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		int start=0;
		int end=0;
		if (condition.getQueryMod() == 2) {
			start = DateJudge.getWeekOfYear(condition.getBdat());
			end = DateJudge.getWeekOfYear(condition.getEdat());
			if (end < start) {
				end += 52;
			}
		} else {
			start = Integer.parseInt(condition.getBdat().substring(5, 7));
			end = Integer.parseInt(condition.getEdat().substring(5, 7));
			if(end < start){
				end += 12;
			}
		}
		
		map.put("start", start);
		map.put("end", end);
		
		return map;
	}
	
	/**
	 * 描述：西贝  营收客流分段统计明细表
	 * @author 马振
	 * 创建时间：2016-2-22 上午11:00:12
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findBusinessBurst")
	@ResponseBody
	public Object findBusinessBurst(ModelMap modelMap, PublicEntity condition, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		condition.setFirmdes(commonMISBOHService.getPkStore(session).getVname());	//获取门店名称放入PublicEntity
		return newReportService.findBusinessBurst(condition);
	}
	
	/**
	 * 描述：西贝   营业时段解析表
	 * @author 马振
	 * 创建时间：2016-2-24 上午9:56:32
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("findSaleDataByTime")
	@ResponseBody
	public ReportObject<Map<String,Object>> findSaleDataByTime(ModelMap modelMap,PublicEntity condition,HttpSession session)throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
		return newReportService.findSaleDataByTime(condition);
	}
	
	/**
	 * 描述：西贝   菜品见台率统计分析
	 * @author 马振
	 * 创建时间：2016-3-3 上午9:55:50
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findCaiPinJianTaiLv")
	@ResponseBody
	public Object findCaiPinJianTaiLv(PublicEntity condition,HttpSession session) throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
		return newReportService.findCaiPinJianTaiLv(condition);
	} 

	/**
	 * 描述：排班报表    权金城
	 * 作者：马振
	 * 时间：2017年5月2日上午10:31:18
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryScheduleReport7")
	@ResponseBody
	public Object queryScheduleReport(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
		return newReportService.queryScheduleReport7(condition);
	}
	/***************************************************西贝报表结束**********************************************************/
}