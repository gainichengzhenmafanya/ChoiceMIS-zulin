package com.choice.misboh.web.controller.reportMis;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.misboh.commonutil.MisUtil;
import com.choice.misboh.commonutil.ReadReport;
import com.choice.misboh.commonutil.ReadReportUrl;
import com.choice.misboh.commonutil.report.ExportExcel;
import com.choice.misboh.constants.reportMis.MisStringConstant;
import com.choice.misboh.constants.reportMis.SaleAnalysisMisBohConstant;
import com.choice.misboh.domain.common.CommonMethod;
import com.choice.misboh.domain.reportMis.PublicEntity;
import com.choice.misboh.domain.reportMis.ReportObject;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.misboh.service.reportMis.MisBohSalesAnalysisService;
import com.choice.tele.util.TeleUtils;

/**
 * 描述：销售分析下的报表
 * @author 马振
 * 创建时间：2015-4-23 下午1:50:30
 */
@Controller
@RequestMapping("misSaleAnalysis")
public class MisBohSalesAnalysisController extends MisBohBaseController {

	@Autowired
	private MisBohSalesAnalysisService saleAnalysisService;
	
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	
	/**
	 * 描述：跳转到报表页面
	 * @author 马振
	 * 创建时间：2015-4-23 下午2:18:03
	 * @param modelMap
	 * @param reportName
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toReport")
	public ModelAndView toReport(ModelMap modelMap,String reportName,String vecode,String bdat,String edat,String iflag,HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("pk_store", commonMISBOHService.getPkStore(session).getPk_store());
		
		//若是调转到点菜员提成明细则
		if ("dcytcmx".equals(reportName)) {
			modelMap.put("vcode", vecode);
			modelMap.put("bdat", bdat);
			modelMap.put("edat", edat);
			modelMap.put("iflag", iflag);
		}
		//添加班次条件 wangkai 2015-10-16
		CommonMethod commonMethod = new CommonMethod();
		commonMethod.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		modelMap.put("sftList", commonMISBOHService.findAllShiftsftByStore(commonMethod, session));
		getRoleTiem(modelMap,session);
		return new ModelAndView(ReadReport.getReportURL(SaleAnalysisMisBohConstant.class, reportName),modelMap);
	}
	
	/**
	 * 描述：报表导出
	 * @author 马振
	 * 创建时间：2015-4-23 下午2:17:51
	 * @param response
	 * @param request
	 * @param session
	 * @param publicEntity
	 * @param reportName
	 * @param headers
	 * @throws CRUDException
	 */
	@RequestMapping("/exportReport")
	public void exportReport(HttpServletResponse response,HttpServletRequest request,HttpSession session,PublicEntity publicEntity,String reportName,String headers) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		//设置分页，查询所有数据
		publicEntity.getPager().setPageSize(Integer.MAX_VALUE);
		publicEntity.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ReadReport.getReportName(MisStringConstant.class, reportName));
		
		//获取并执行查询方法，获取查询结果
		ReportObject<Map<String,Object>> result = MisUtil.execMethod(saleAnalysisService, reportName, publicEntity);
		if(null != result.getFooter())
			result.getRows().addAll(result.getFooter());
		if(null != headers || !"".equals(headers))
			ExportExcel.creatWorkBook(request,response,result.getRows(),ReadReport.getReportNameCN(MisStringConstant.class, reportName),null,headers);
		else
			ExportExcel.creatWorkBook(ReadReport.getReportNameCN(MisStringConstant.class, reportName),request,response, result.getRows(), ReadReport.getReportNameCN(MisStringConstant.class, reportName), dictColumnsService.listDictColumnsByAccount(dictColumns, ReadReport.getDefaultHeader(MisStringConstant.class, reportName)));
	}
	
	/**
	 * 描述：报表打印
	 * @author 马振
	 * 创建时间：2015-5-5 上午10:38:12
	 * @param modelMap
	 * @param request
	 * @param session
	 * @param condition
	 * @param type
	 * @param reportName
	 * @param start
	 * @param end
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/printReport")
	public ModelAndView printReport(ModelMap modelMap,HttpServletRequest request,HttpSession session,PublicEntity condition,String type,String reportName,String start,String end) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			condition.setFirmid(commonMISBOHService.getPkStore(session).getPk_store());
		
		//获取并执行查询方法，获取查询结果
		modelMap.put("List",MisUtil.execMethod(saleAnalysisService, reportName, condition).getRows());
	 	HashMap<String,Object>  parameters = new HashMap<String,Object>();
	    parameters.put("report_name", ReadReport.getReportNameCN(MisStringConstant.class, reportName));
	    Map<String,String> map = TeleUtils.convertToMap(condition);
	    map.remove("pager");
	    map.put("bdat", condition.getBdat());
	    map.put("edat", condition.getEdat());
	    map.put("start", start);
	    map.put("end", end);
	    map.put("reportName", reportName);
	    modelMap.put("actionMap", map);
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	    parameters.put("bdat", condition.getBdat());
	    parameters.put("edat", condition.getEdat());
	    parameters.put("start", start);
	    parameters.put("end", end);
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", request.getServletPath());//传入回调路径
	 	Map<String,String> rs = ReadReportUrl.redReportUrl(type,ReadReport.getIReportPrint(SaleAnalysisMisBohConstant.class,reportName));//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
      		
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 描述：大类销售报表
	 * @author 马振
	 * 创建时间：2015-4-23 下午1:57:13
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/listInvoiceType")
	@ResponseBody
	public Object listInvoiceType(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
//		return saleAnalysisService.listInvoiceType(condition);
		return saleAnalysisService.listInvoiceType_new(condition);
	}
	
	/**
	 * 描述：菜品销售汇总
	 * @author 马振
	 * 创建时间：2015-4-23 下午2:17:19
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryCpxshz")
	@ResponseBody
	public Object queryCpxshz(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		return this.saleAnalysisService.queryCpxshz(condition);
	}
	
	/**
	 * 描述： 菜品销售明细报表
	 * @author 马振
	 * 创建时间：2016年4月12日下午8:30:38
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryPoscybb")
	@ResponseBody
	public Object queryPoscybb(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		return this.saleAnalysisService.queryPoscybb(condition);
	}
	
	/**
	 * 描述：退菜明细报表
	 * @author 马振
	 * 创建时间：2015-4-23 下午3:25:28
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryTcmxbb")
	@ResponseBody
	public Object queryTcmxbb(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		return saleAnalysisService.queryTcmxbb(condition);
	}
	
	/**
	 * 描述：套餐销售报表
	 * @author 马振
	 * 创建时间：2015-4-28 下午3:21:48
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/listBohSales")
	@ResponseBody
	public Object listBohSales(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		return saleAnalysisService.listBohSales(condition);
	}
	
	/**
	 * 描述：时段营业额报告
	 * @author 马振
	 * 创建时间：2015-7-31 上午11:24:38
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("queryPeriodTurnoverReport")
	@ResponseBody
	public ReportObject<Map<String,Object>> queryPeriodTurnoverReport(ModelMap modelMap,PublicEntity condition,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		return saleAnalysisService.queryPeriodTurnoverReport(condition);
	}
	
	/**
	 * 描述：附加项销售统计
	 * @author 马振
	 * 创建时间：2015-8-15 下午2:49:11
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryAdditionalTerm")
	@ResponseBody
	public Object queryAdditionalTerm(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		return saleAnalysisService.queryAdditionalTerm(condition);
	}
	
	/**
	 * 描述：点菜员提成汇总报表
	 * @author 马振
	 * 创建时间：2015-8-18 下午2:39:07
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryOrderingMemberCommission")
	@ResponseBody
	public Object queryOrderingMemberCommission(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		return saleAnalysisService.queryOrderingMemberCommission(condition);
	}
	
	/**
	 * 描述：当日查询——点菜员提成汇总报表
	 * @author 马振
	 * 创建时间：2016-3-7 下午2:37:23
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryOrderingMemberCommissionCur")
	@ResponseBody
	public Object queryOrderingMemberCommissionCur(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		return saleAnalysisService.queryOrderingMemberCommission(condition);
	}
	
	/**
	 * 描述：点菜员提成明细
	 * @author 马振
	 * 创建时间：2015-8-28 下午3:49:40
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryOrderingMemberCommissionDetail")
	@ResponseBody
	public Object queryOrderingMemberCommissionDetail(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		return saleAnalysisService.queryOrderingMemberCommissionDetail(condition);
	}
}