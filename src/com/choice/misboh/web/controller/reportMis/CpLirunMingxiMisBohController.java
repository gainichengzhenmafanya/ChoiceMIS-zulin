package com.choice.misboh.web.controller.reportMis;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.constants.reportMis.CostItemMisBohConstants;
import com.choice.misboh.constants.reportMis.MisBohStringConstant;
import com.choice.misboh.service.reportMis.CpLirunMingxiMisBohService;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
/**
 * 分店报表 
 * @author dell
 * 部门物资进出表、部门成本差异报表、分店物资进货价格比较、出库综合查询报表、分店菜品利润表、
 */
@Controller
@RequestMapping("CpLirunMingxiMisBoh")
public class CpLirunMingxiMisBohController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private CpLirunMingxiMisBohService cpLirunMingxiMisBohService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	
	/********************************************分店菜品利润表****************************************************/

	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findFirmFoodProfitHeaders")
	@ResponseBody
	public Object getFirmFoodProfitHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_FIRMFOODPROFIT);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, MisBohStringConstant.BASICINFO_REPORT_FIRMFOODPROFIT));
		return columns;
	}
	
	/**
	 * 跳转到分店菜品利润表页面
	 * @return
	 */
	@RequestMapping("/toFirmFoodProfit")
	public ModelAndView toFirmFoodProfit(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", MisBohStringConstant.REPORT_NAME_FIRMFOODPROFIT);
		return new ModelAndView(CostItemMisBohConstants.LIST_FIRMFOODPROFIT,modelMap);
	}
	/**
	 * 查询分店菜品利润表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findFirmFoodProfit")
	@ResponseBody
	public Object findFirmFoodProfit(ModelMap modelMap,HttpSession session,String page,String rows,String sort,String order,SupplyAcct supplyAcct,
			String exp0,String onlycbk,String doublefood,String foodType,String food,String payment) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null != thePositn){
			supplyAcct.setPositn(thePositn.getCode());
			supplyAcct.setFirm(thePositn.getCode());
		}
		condition.put("supplyAcct", supplyAcct);
		condition.put("food", food);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("exp0", exp0);
		condition.put("onlycbk", onlycbk);
		condition.put("doublefood", doublefood);
		condition.put("foodType", foodType);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		//cpLirunMingxiMisBohService.findFirmFoodProfit(condition, pager).getFooter().get(0).get("INFASHENGE");
		return cpLirunMingxiMisBohService.findFirmFoodProfit(condition, pager);
	}
	
	/**
	 * 查看成本卡
	 * @param modelMap
	 * @return
	 */
	@RequestMapping("/findCostCard")
	public ModelAndView findCostCard(ModelMap modelMap, String itcode, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("itcode", itcode);
		modelMap.put("costdtlmList", cpLirunMingxiMisBohService.findCostdtlm(map));
		modelMap.put("costdtlList", cpLirunMingxiMisBohService.findCostdtl(map));
		return new ModelAndView(CostItemMisBohConstants.LIST_COSTCARD,modelMap);
	}	

	/**
	 * 查看菜品成本组成物资明细
	 * @param modelMap
	 * @param itcode
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findCostDetail")
	public ModelAndView findCostDetail(ModelMap modelMap, String itcode, Date bdat, Date edat, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("itcode", itcode);
		map.put("bdat", bdat);
		map.put("edat", edat);
		modelMap.put("costDetailList", cpLirunMingxiMisBohService.findCostDetail(map));
		return new ModelAndView(CostItemMisBohConstants.LIST_COSTDETAIL,modelMap);
	}	
	/**
	 * 菜品销售成本利润走势分析
	 * @param modelMap
	 * @param itcode
	 * @param onlycbk
	 * @param bdat
	 * @param edat
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findSaleCostProfitTrends")
	public ModelAndView findSaleCostProfitTrends(ModelMap modelMap, String itcode, String onlycbk, Date bdat, Date edat, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("itcode", itcode);
		map.put("onlycbk", onlycbk);
		map.put("bdat", bdat);
		map.put("edat", edat);
		modelMap.put("itcode", itcode);
		modelMap.put("onlycbk", onlycbk);
		modelMap.put("bdat", DateFormat.getStringByDate(bdat, "yyyy-MM-dd"));
		modelMap.put("edat", DateFormat.getStringByDate(edat, "yyyy-MM-dd"));
		modelMap.put("saleCostList", cpLirunMingxiMisBohService.findSaleCostProfitTrends(map));
		return new ModelAndView(CostItemMisBohConstants.LIST_SALECOSTPROFITTRENDS,modelMap);
	}
	
	/**
	 * 菜品销售成本利润走势分析--图表
	 * @param response
	 * @param session
	 * @param id
	 * @throws Exception
	 */
	@RequestMapping("/findXmlForSaleCostProfitTrends")
	public void findXmlForSaleCostProfitTrends(HttpServletResponse response, HttpSession session, String itcode, String onlycbk, Date bdat, Date edat) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("itcode", itcode);
		map.put("onlycbk", onlycbk);
		map.put("bdat", DateFormat.formatDate(bdat, "yyyy-MM-dd"));
		map.put("edat", DateFormat.formatDate(edat, "yyyy-MM-dd"));
		cpLirunMingxiMisBohService.findXmlForSaleCostProfitTrends(response, map).output();
	}
	
	/**
	 * 菜品成本区间分析
	 * @param modelMap
	 * @param itcode
	 * @param onlycbk
	 * @param bdat
	 * @param edat
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findCostInterval")
	public ModelAndView findCostInterval(ModelMap modelMap, String itcode, String onlycbk, Date bdat, Date edat, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("itcode", itcode);
		map.put("onlycbk", onlycbk);
		map.put("bdat", bdat);
		map.put("edat", edat);
		modelMap.put("itcode", itcode);
		modelMap.put("onlycbk", onlycbk);
		modelMap.put("bdat", DateFormat.getStringByDate(bdat, "yyyy-MM-dd"));
		modelMap.put("edat", DateFormat.getStringByDate(edat, "yyyy-MM-dd"));
		modelMap.put("costIntervalList", cpLirunMingxiMisBohService.findCostIntervalList(map));
		return new ModelAndView(CostItemMisBohConstants.LIST_COSTINTERVAL,modelMap);
	}
	
	/**
	 * 菜品成本区间分析--图表
	 * @param response
	 * @param session
	 * @param id
	 * @throws Exception
	 */
	@RequestMapping("/findXmlCostInterval")
	public void findXmlCostInterval(HttpServletResponse response, HttpSession session, String itcode, String onlycbk, Date bdat, Date edat) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("itcode", itcode);
		map.put("onlycbk", onlycbk);
		map.put("bdat", DateFormat.formatDate(bdat, "yyyy-MM-dd"));
		map.put("edat", DateFormat.formatDate(edat, "yyyy-MM-dd"));
		cpLirunMingxiMisBohService.findXmlCostInterval(response, map).output();
	}
	
	/**
	 * 菜品毛利率区间分析
	 * @param modelMap
	 * @param itcode
	 * @param onlycbk
	 * @param bdat
	 * @param edat
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findMaolilvInterval")
	public ModelAndView findMaolilvInterval(ModelMap modelMap, String itcode, String onlycbk, Date bdat, Date edat, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("itcode", itcode);
		map.put("onlycbk", onlycbk);
		map.put("bdat", bdat);
		map.put("edat", edat);
		modelMap.put("itcode", itcode);
		modelMap.put("onlycbk", onlycbk);
		modelMap.put("bdat", DateFormat.getStringByDate(bdat, "yyyy-MM-dd"));
		modelMap.put("edat", DateFormat.getStringByDate(edat, "yyyy-MM-dd"));
		modelMap.put("maolilvIntervalList", cpLirunMingxiMisBohService.findMaolilvIntervalList(map));
		return new ModelAndView(CostItemMisBohConstants.LIST_MAOLILVINTERVAL,modelMap);
	}	
	
	/**
	 * 菜品毛利率区间--图表
	 * @param response
	 * @param session
	 * @param id
	 * @throws Exception
	 */
	@RequestMapping("/findXmlMaolilvInterval")
	public void findXmlMaolilvInterval(HttpServletResponse response,HttpSession session,String itcode,String onlycbk,Date bdat,Date edat) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("itcode", itcode);
		map.put("onlycbk", onlycbk);
		map.put("bdat", DateFormat.formatDate(bdat, "yyyy-MM-dd"));
		map.put("edat", DateFormat.formatDate(edat, "yyyy-MM-dd"));
		cpLirunMingxiMisBohService.findXmlMaolilvInterval(response, map).output();
	}
	
	/**
	 * 打印分店菜品利润表
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printFirmFoodProfit")
	public ModelAndView printFirmFoodProfit(ModelMap modelMap,Page pager,HttpSession session,String type,SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("sp_code",supplyAcct.getSp_code());
		params.put("grp",supplyAcct.getGrp());
		params.put("bdat", DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		params.put("edat", DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null != thePositn){
			supplyAcct.setPositn(thePositn.getCode());
			supplyAcct.setFirm(thePositn.getCode());
			params.put("positn",thePositn.getCode());
			params.put("firm",thePositn.getCode());
		}
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",cpLirunMingxiMisBohService.findFirmFoodProfit(condition, pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "分店项目销售利润分析");
	    parameters.put("bdat", supplyAcct.getBdat());
	    parameters.put("edat", supplyAcct.getEdat());
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
	    modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/firmMis/printFirmFoodProfit.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,CostItemMisBohConstants.REPORT_PRINT_URL_FIRMFOODPROFIT,CostItemMisBohConstants.REPORT_PRINT_URL_FIRMFOODPROFIT);//判断跳转路径
	    modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	/**
	 * 导出分店菜品利润表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportFirmFoodProfit")
	@ResponseBody
	public void exportFirmFoodProfit(HttpServletResponse response,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "分店菜品利润表";
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> conditions = new HashMap<String,Object>();
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null != thePositn){
			supplyAcct.setPositn(thePositn.getCode());
			supplyAcct.setFirm(thePositn.getCode());
		}
		conditions.put("supplyAcct", supplyAcct);
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_FIRMFOODPROFIT);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
	            + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), cpLirunMingxiMisBohService.findFirmFoodProfit(conditions, pager).getRows(), "分店菜品利润表", dictColumnsService.listDictColumnsByTable(dictColumns));
	}
}
