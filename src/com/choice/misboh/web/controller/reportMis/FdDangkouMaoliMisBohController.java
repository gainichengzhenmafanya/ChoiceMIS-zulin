package com.choice.misboh.web.controller.reportMis;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.misboh.constants.reportMis.CostItemMisBohConstants;
import com.choice.misboh.service.reportMis.FdDangKouMaoliMisBohService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Acct;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.ana.CostProfit;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.PositnService;
/**
 * 分店档口毛利
 * @author lq
 *
 */
@Controller
@RequestMapping("fdDangkouMaoliMisBoh")
public class FdDangkouMaoliMisBohController {
	
	@Autowired
	private Page pager;
	@Autowired
	private FdDangKouMaoliMisBohService fdDangKouMaoliMisBohService;
	@Autowired
	private PositnService positnService;
	@Autowired
    private AcctService acctService;
	
	/**************************************************分店档口毛利*********************************************/
	
	/***
	 * 跳转到报表
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toFddangkouMaoli")
	public ModelAndView toFddangkouMaoli(ModelMap modelMap, CostProfit costProfit, String showtyp, String chengben) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null == costProfit.getBdat())
			costProfit.setBdat(new Date());
		if(null == costProfit.getEdat())
			costProfit.setEdat(new Date());
		if(null == showtyp)
			showtyp = "2";
		modelMap.put("costProfit", costProfit);
		modelMap.put("showtyp", showtyp);
		modelMap.put("chengben", null == chengben ? "sj" : "ll");
		return new ModelAndView(CostItemMisBohConstants.REPORT_SHOW_FDDANGKOUMAOLI,modelMap);
	}
	
	/**
	 * 查询表头信息
	 * @param dept
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/findFdDangkouMaoliHeaders")
	@ResponseBody
	public List<Positn> findFdDangkouMaoliHeaders(HttpSession session, String dept) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		Positn positn = new Positn();
		positn.setUcode(thePositn.getCode());
		List<Positn> deptList = positnService.findDeptByPositn(positn);
		if(dept == null || "".equals(dept)){
			return deptList;
		}else{
			String[] stringList = dept.split(",");
			List<Positn> lists = new ArrayList<Positn>();
			for(String code:stringList){
				for(Positn p : deptList){
					if(code.equals(p.getCode())){
						lists.add(p);
					}
				}
			}
			return lists;
		}
	}
	
	/***
	 * 查询数据
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param costProfit
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findDangKouProfit")
	@ResponseBody
	public Object findDangKouProfit(ModelMap modelMap, HttpSession session, String page, String rows, String sort, String order, CostProfit costProfit, String showtyp, String chengben)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		costProfit.setPositn(thePositn.getCode());
		Acct acct = acctService.findAcctById(session.getAttribute("ChoiceAcct").toString());
		return fdDangKouMaoliMisBohService.findFirmDangKouProfit(findFdDangkouMaoliHeaders(session,costProfit.getDept()),costProfit,showtyp,chengben,acct.getDes(),pager);
	}
	
	/**
	 * 导出
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param costProfit
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/exportFirmDangKouProfit")
	@ResponseBody
	public boolean exportFirmDangKouProfit(HttpServletRequest request,HttpServletResponse response,ModelMap modelMap,Page page,CostProfit costProfit,String showtyp, String chengben, HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		costProfit.setPositn(thePositn.getCode());
		String fileName = "分店档口毛利";
		pager.setPageSize(Integer.MAX_VALUE);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		
		response.setContentType("application/msexcel; charset=UTF-8");
		Acct acct = acctService.findAcctById(session.getAttribute("ChoiceAcct").toString());
		return fdDangKouMaoliMisBohService.exportExcel(response.getOutputStream(), costProfit, showtyp,chengben,acct.getDes(),findFdDangkouMaoliHeaders(session,costProfit.getDept()));
	}
	
	
	/**************************************************分店档口毛利*********************************************/	

}
