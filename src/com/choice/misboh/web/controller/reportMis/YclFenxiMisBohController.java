package com.choice.misboh.web.controller.reportMis;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.excel.GenerateExcel;
import com.choice.misboh.constants.reportMis.CostItemMisBohConstants;
import com.choice.misboh.constants.reportMis.MisBohStringConstant;
import com.choice.misboh.service.reportMis.YclFenxiMisBohService;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.ana.PtivityAna;
import com.choice.scm.service.PositnService;
import com.choice.scm.util.ReadReportUrl;

/**
 * 分店应产率分析
 * @author lq
 *
 */
@Controller
@RequestMapping("YclFenxiMisBoh")
public class YclFenxiMisBohController {
	
	@Autowired
	private Page pager;
	@Autowired
	private GenerateExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private YclFenxiMisBohService yclFenxiMisBohService;
	@Autowired
	private PositnService positnService;
	
	/***********************************************************应产率分析start**************************************************/
	/**
	 * 跳转应产率分析
	 */
	@RequestMapping("/toPtivityAna")
	public ModelAndView toPtivityAna(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("bdat", new Date());
		modelMap.put("edat", new Date());
		modelMap.put("reportName", MisBohStringConstant.REPORT_NAME_MISPTIVITY);
		return new ModelAndView(CostItemMisBohConstants.REPORT_SHOW_PTIVITYANA,modelMap);
	}
	/**
	 * 查询表头信息
	 */
	@RequestMapping("/findPtivityAnaHeaders")
	@ResponseBody	
	public Object getPtivityAnaHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_MISPTIVITY);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, MisBohStringConstant.BASICINFO_REPORT_MISPTIVITY));
		columns.put("frozenColumns", MisBohStringConstant.BASICINFO_REPORT_MISPTIVITY_FROZEN);
		return columns;
	}
	/**
	 * 查询应产率分析分析
	 */
	@RequestMapping("/findPtivityAna")
	@ResponseBody
	public Object findPtivityAna(ModelMap modelMap, HttpSession session, String page, String rows,PtivityAna ptivity) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if(null != thePositn){
			ptivity.setFirm(thePositn.getCode());
		}else{
			return null;
		}
		if(null == ptivity.getPositn() || "".equals(ptivity.getPositn())){//如果没选仓位，默认查当前门店以及当前门店下的所有仓位
			List<Positn> positnList = new ArrayList<Positn>();
			positnList.add(thePositn);
			Positn positn = positnService.findPositnByCode(thePositn);
			if("Y".equals(positn.getYnUseDept())){
				Positn p = new Positn();
				p.setUcode(thePositn.getCode());
				positnList.addAll(positnService.findPositnSuperNOPage(p));
			}
			StringBuffer sb = new StringBuffer();
			for(Positn p : positnList){
				sb.append(p.getCode()+",");
			}
			ptivity.setPositn(CodeHelper.replaceCode(sb.substring(0,sb.length()-1)));
		}
		return yclFenxiMisBohService.findPtivityAna(ptivity, pager);
	}
	/**
	 * 导出
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportPtivityAna")
	@ResponseBody
	public void exportPtivityAna(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,String page,String rows,PtivityAna ptivity) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "应产率分析表";
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_MISPTIVITY);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if(null != thePositn){
			ptivity.setFirm(thePositn.getCode());
		}
		if(null == ptivity.getPositn() || "".equals(ptivity.getPositn())){//如果没选仓位，默认查当前门店以及当前门店下的所有仓位
			List<Positn> positnList = new ArrayList<Positn>();
			positnList.add(thePositn);
			Positn positn = positnService.findPositnByCode(thePositn);
			if("Y".equals(positn.getYnUseDept())){
				Positn p = new Positn();
				p.setUcode(thePositn.getCode());
				positnList.addAll(positnService.findPositnSuperNOPage(p));
			}
			StringBuffer sb = new StringBuffer();
			for(Positn p : positnList){
				sb.append(p.getCode()+",");
			}
			ptivity.setPositn(CodeHelper.replaceCode(sb.substring(0,sb.length()-1)));
		}
//		exportExcelMap.creatWorkBook(response.getOutputStream(), yclFenxiMisBohService.findPtivityAna(ptivity,pager).getRows(), "应产率分析表", dictColumnsService.listDictColumnsByAccount(dictColumns, MISBOHStringConstant.PTIVITYANA_COLUMN));	
		exportExcelMap.creatWorkBook_DictColumns(fileName, request, response, yclFenxiMisBohService.findPtivityAna(ptivity,pager).getRows(), "应产率分析表", dictColumnsService.listDictColumnsByAccount(dictColumns, MisBohStringConstant.BASICINFO_REPORT_MISPTIVITY), MisBohStringConstant.EXCEL_MISPTIVITY);
	}
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param month
	 * @param type
	 * @param supplyAcct
	 * @param delivertyp
	 * @param folio
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/printPtivityAna")
	public ModelAndView printPtivityAna(ModelMap modelMap, Page pager, HttpSession session, String type,
			String page, String rows, PtivityAna ptivity)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if(null != thePositn){
			ptivity.setFirm(thePositn.getCode());
		}else{
			return null;
		}
		if(null == ptivity.getPositn() || "".equals(ptivity.getPositn())){//如果没选仓位，默认查当前门店以及当前门店下的所有仓位
			List<Positn> positnList = new ArrayList<Positn>();
			positnList.add(thePositn);
			Positn positn = positnService.findPositnByCode(thePositn);
			if("Y".equals(positn.getYnUseDept())){
				Positn p = new Positn();
				p.setUcode(thePositn.getCode());
				positnList.addAll(positnService.findPositnSuperNOPage(p));
			}
			StringBuffer sb = new StringBuffer();
			for(Positn p : positnList){
				sb.append(p.getCode()+",");
			}
			ptivity.setPositn(CodeHelper.replaceCode(sb.substring(0,sb.length()-1)));
		}
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> params = new HashMap<String,Object>();
//		params.put("positn",ptivity.getPositn());
		params.put("sp_code",ptivity.getSp_code());
		params.put("grptyp",ptivity.getGrptyp());
		params.put("grp",ptivity.getGrp());
		params.put("typ",ptivity.getTyp());
		params.put("onlyjp",ptivity.getOnlyjp());
		params.put("bdat",DateFormat.getStringByDate(ptivity.getBdat(), "yyyy-MM-dd"));
		params.put("edat",DateFormat.getStringByDate(ptivity.getEdat(), "yyyy-MM-dd"));
		modelMap.put("actionMap", params);
		modelMap.put("List",yclFenxiMisBohService.findPtivityAna(ptivity, pager).getRows());
		
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "应产率分析");
	    parameters.put("bdat",DateFormat.getStringByDate(ptivity.getBdat(), "yyyy-MM-dd"));
	    parameters.put("edat",DateFormat.getStringByDate(ptivity.getEdat(), "yyyy-MM-dd"));
	    modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/YclFenxiMisBoh/printPtivityAna.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,CostItemMisBohConstants.REPORT_URL_PTIVITYANA,CostItemMisBohConstants.REPORT_URL_PTIVITYANA);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}		

	/***********************************************************应产率分析start**************************************************/		
	
	/**
	 * 跳转应产率分析
	 */
	@RequestMapping("/toPtivityAnaTest")
	public ModelAndView toPtivityAnaTest(ModelMap modelMap,Date date){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("bdat",date);
		modelMap.put("edat", date);
		modelMap.put("reportName", MisBohStringConstant.REPORT_NAME_MISPTIVITY);
		return new ModelAndView(CostItemMisBohConstants.REPORT_SHOW_PTIVITYANATEST,modelMap);
	}
}
