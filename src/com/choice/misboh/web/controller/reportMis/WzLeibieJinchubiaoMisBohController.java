package com.choice.misboh.web.controller.reportMis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.excel.GenerateExcel;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.reportMis.WzLeibieJinchubiaoMisService;
import com.choice.scm.util.ReadReportUrl;
@Controller
@RequestMapping("WzLeibieJinchubiaoMisBoh")
public class WzLeibieJinchubiaoMisBohController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private WzLeibieJinchubiaoMisService WzLeibieJinchubiaoMisService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private GenerateExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	private PositnService positnService;
	
	
	/********************************************物资类别进出表报表****************************************************/
	/**
	 * 跳转到物资类别进出表页面
	 * @return
	 */
	@RequestMapping("/toSupplyTypInOut")
	public ModelAndView toSupplyTypInOut(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", ScmStringConstant.REPORT_NAME_SUPPLYTYPINOUT);
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_SUPPLYTYPINOUT_MIS,modelMap);
	}
	
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findSupplyTypInOutHeaders")
	@ResponseBody
	public Object getSupplyTypInOutHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_SUPPLYTYPINOUT);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_SUPPLYTYPINOUT));
		columns.put("frozenColumns", ScmStringConstant.BASICINFO_REPORT_SUPPLYTYPINOUT_FROZEN);
		return columns;
	}
	
	/**
	 * 查询物资综合进出表报表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findSupplyTypInOut")
	@ResponseBody
	public Object findSupplyTypInOut(ModelMap modelMap, HttpSession session, String sort,
			String order, SupplyAcct supplyAcct, String page, String rows, String without0) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		condition.put("sort",sort);
		condition.put("order", order);
		condition.put("without0", without0);
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		setDept(session,supplyAcct);//设置部门
		condition.put("supplyAcct", supplyAcct);
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		return WzLeibieJinchubiaoMisService.findSupplyTypInOut(condition,pager);
	}
	
	/**
	 * 跳转到列选择页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/toColChooseSupplyTypInOut")
	public ModelAndView toColChooseSupplyTypInOut(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_SUPPLYTYPINOUT);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", ScmStringConstant.REPORT_NAME_SUPPLYTYPINOUT);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_SUPPLYTYPINOUT));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	
	/**
	 * 导出物资综合进出表报表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 * @author ZGL
	 */
	@RequestMapping("/exportSupplyTypInOut")
	@ResponseBody
	public void exportSupplyTypInOut(HttpServletResponse response, String sort, String order,
			 HttpServletRequest request, HttpSession session, String without0, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "物资类别进出表";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		setDept(session,supplyAcct);//设置部门
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("without0", without0);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_SUPPLYTYPINOUT);
		dictColumns.setLocale(session.getAttribute("locale").toString());
//		exportExcelMap.creatWorkBook(response.getOutputStream(), WzLeibieJinchubiaoMisService.findSupplyTypInOut(condition,pager).getRows(), "物资类别进出表", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_SUPPLYTYPINOUT));
		exportExcelMap.creatWorkBook_DictColumns(fileName, request, response, WzLeibieJinchubiaoMisService.findSupplyTypInOut(condition,pager).getRows(), "物资类别进出表", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_SUPPLYTYPINOUT),null);
	}
	
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 * @author ZGL
	 */
	@RequestMapping(value = "/printSupplyTypInOut")
	public ModelAndView printSupplyTypInOut(ModelMap modelMap, Page pager, HttpSession session, String sort,
			String order, String without0, String type, SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
		if(supplyAcct.getBdat() != null){
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
			parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		}
		params.put("positn",supplyAcct.getPositn());
		if(supplyAcct.getEdat() != null){
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
			parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		}
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("grp",supplyAcct.getGrp());
		params.put("typ",supplyAcct.getTyp());
		params.put("typea",supplyAcct.getTypea());
		//params.put("accby", supplyAcct.getAccby());
		params.put("sort",sort);
		params.put("order", order);
		params.put("without0", without0);
		setDept(session,supplyAcct);//设置部门
		condition.put("sort",sort);
		condition.put("order", order);
		condition.put("without0", without0);
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",WzLeibieJinchubiaoMisService.findSupplyTypInOut(condition,pager).getRows());
	    parameters.put("report_name", "物资类别进出表");
	    modelMap.put("actionMap", params);
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/WzLeibieJinchubiaoMis/printSupplyTypInOut.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_SUPPLYTYPINOUT,SupplyAcctConstants.REPORT_EXP_URL_SUPPLYTYPINOUT);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	
	/***
	 * 查询部门的方法
	 * @param session
	 * @param supplyAcct
	 * @throws CRUDException
	 */
	private void setDept(HttpSession session, SupplyAcct supplyAcct) throws CRUDException{
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(supplyAcct.getPositn()==null || "".equals(supplyAcct.getPositn())){//选择档口为空时，默认查询该分店的入库明细
			if(null != thePositn){
				//查询该分店的档口
				Positn positn = new Positn();
				positn.setUcode(thePositn.getCode());
				List<Positn> list = positnService.findPositnSuperNOPage(positn);
				String positncode = "";
				for(Positn posi:list){
					positncode +=CodeHelper.replaceCode(posi.getCode())+",";
				}
				positncode+=CodeHelper.replaceCode(thePositn.getCode());
				supplyAcct.setPositn(positncode);
			}
		}else{
			supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		}
	}
}
