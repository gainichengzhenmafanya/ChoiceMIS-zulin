package com.choice.misboh.web.controller.reportMis;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.excel.GenerateExcel;
import com.choice.misboh.constants.reportMis.MisBohStringConstant;
import com.choice.misboh.constants.reportMis.SupplyAcctMisBohConstants;
import com.choice.misboh.service.reportMis.GysLeibieHuizongMisBohService;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.GrpTyp;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.PositnService;
import com.choice.scm.util.ReadReportUrl;
@Controller
@RequestMapping("GysLeibieHuizongMisBoh")
public class GysLeibieHuizongMisBohController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private GysLeibieHuizongMisBohService gysLeibieHuizongMisBohService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private GenerateExcel<Map<String,Object>> exportExcel;
	@Autowired
	private PositnService positnService;
	
	/********************************************供应商类别汇总报表****************************************************/
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findDeliverCategorySumHeaders")
	@ResponseBody
	public Object getDeliverCategorySumHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_DELIVERCATEGORYSUMMISBOH);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, MisBohStringConstant.BASICINFO_REPORT_DELIVERCATEGORYSUMMISBOH));
		columns.put("frozenColumns", MisBohStringConstant.BASICINFO_REPORT_DELIVERCATEGORYSUMMISBOH_FROZEN);
		return columns;
	}
	/**
	 * 跳转到供应商类别汇总报表页面
	 * @return
	 */
	@RequestMapping("/toDeliverCategorySum")
	public ModelAndView toDeliverCategorySum(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", MisBohStringConstant.REPORT_NAME_DELIVERCATEGORYSUMMISBOH);
		return new ModelAndView(SupplyAcctMisBohConstants.REPORT_SHOW_DELIVERCATEGORYSUM_MIS,modelMap);
	}
	/**
	 * 查询供应商类别汇总报表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findDeliverCategorySum")
	@ResponseBody
	public Object ChkinDeliverCategorySum(ModelMap modelMap,HttpSession session,String page,String rows,String sort,String order,SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		String accountId=session.getAttribute("accountId").toString();
		supplyAcct.setAccountId(accountId);
		setDept(session,supplyAcct);
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return gysLeibieHuizongMisBohService.findDeliverCategorySum(condition,pager);
	}
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printDeliverCategorySum")
	public ModelAndView printDeliverCategorySum(ModelMap modelMap,Page pager,HttpSession session,String type,SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		String accountId=session.getAttribute("accountId").toString();
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		params.put("positn",supplyAcct.getPositn());
		params.put("grpdes",supplyAcct.getGrpdes());
		params.put("grptyp",supplyAcct.getGrptyp());
		//params.put("sp_code",supplyAcct.getSp_code());
		if(supplyAcct.getEdat() != null)
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		params.put("grp",supplyAcct.getGrp());
		params.put("typ",supplyAcct.getTyp());
		params.put("chktag",String.valueOf(supplyAcct.getChktag()));
		params.put("chktyp",supplyAcct.getChktyp());
		params.put("delivercode",supplyAcct.getDelivercode());
		supplyAcct.setAccountId(accountId);
		params.put("accountId", accountId);
		setDept(session,supplyAcct);
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",gysLeibieHuizongMisBohService.findDeliverCategorySum(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "供应商类别汇总");
	    modelMap.put("actionMap", params);
	    parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
	    parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/GysLeibieHuizongMisBoh/printDeliverCategorySum.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctMisBohConstants.REPORT_PRINT_URL_DELIVERCATEGORYSUM,SupplyAcctMisBohConstants.REPORT_EXP_URL_DELIVERCATEGORYSUM);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	/**
	 * 导出供应商类别汇总报表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportDeliverCategorySum")
	@ResponseBody
	public void exportDeliverCategorySum(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "供应商类别汇总";
		Map<String,Object> condition = new HashMap<String,Object>();
		String accountId=session.getAttribute("accountId").toString();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		supplyAcct.setAccountId(accountId);
		setDept(session,supplyAcct);
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_DELIVERCATEGORYSUMMISBOH);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		ReportObject<Map<String,Object>> result = gysLeibieHuizongMisBohService.findDeliverCategorySum(condition,pager);
		if(null != result.getFooter())
			result.getRows().addAll(result.getFooter());
//		exportExcel.creatWorkBook(response.getOutputStream(), gysLeibieHuizongMisBohService.findDeliverCategorySum(condition,pager).getRows(), "供应商类别汇总", dictColumnsService.listDictColumnsByTable(dictColumns));
		exportExcel.creatWorkBook_DictColumns(fileName, request,response, result.getRows(), "供应商类别汇总", dictColumnsService.listDictColumnsByTable(dictColumns),MisBohStringConstant.EXCEL_DELIVERCATEGORYSUMMISBOH);
	}
	
	/***
	 * 查询部门的方法
	 * @param session
	 * @param supplyAcct
	 * @throws CRUDException
	 */
	private void setDept(HttpSession session, SupplyAcct supplyAcct) throws CRUDException{
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(supplyAcct.getPositn()==null || "".equals(supplyAcct.getPositn())){//选择档口为空时，默认查询该分店的入库明细
			if(null != thePositn){
				//查询该分店的档口
				Positn positn = new Positn();
				positn.setUcode(thePositn.getCode());
				List<Positn> list = positnService.findPositnSuperNOPage(positn);
				String positncode = "";
				for(Positn posi:list){
					positncode +=CodeHelper.replaceCode(posi.getCode())+",";
				}
				positncode+=CodeHelper.replaceCode(thePositn.getCode());
				supplyAcct.setPositn(positncode);
			}
		}else{
			supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		}
	}
	
	/********************************************供应商类别汇总3  快客利定制 20151207wjf****************************************************/
	
	/**
	 * 查询表头信息(根据选择的领用仓位 动态显示表头)
	 * @param session
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/findGysLeibieHuizongHeaders3")
	@ResponseBody
	public List<GrpTyp> findGysLeibieHuizongHeaders3(String typ) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<GrpTyp> grpTypList = gysLeibieHuizongMisBohService.findAllType(typ);
		return grpTypList;
	}
	
	/**
	 * 跳转到报表html页面
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/toDeliverCategorySum3")
	public ModelAndView toDeliverCategorySum3(ModelMap modelMap, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(SupplyAcctMisBohConstants.REPORT_SHOW_DELIVERCATEGORYSUM3,modelMap);
	}
	
	/**
	 * 查询报表数据
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findDeliverCategorySum3")
	@ResponseBody
	public ReportObject<Map<String,Object>> findDeliverCategorySum3(ModelMap modelMap, HttpSession session, String page, String rows, String sort, String order, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		setDept(session,supplyAcct);
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return gysLeibieHuizongMisBohService.findDeliverCategorySum3(findGysLeibieHuizongHeaders3(supplyAcct.getTyp()),condition,pager);
	}
	
	/**
	 * 导出
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportDeliverCategorySum3")
	@ResponseBody
	public void exportDeliverCategorySum3(HttpServletResponse response,HttpServletRequest request,HttpSession session,
			String sort, String order, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "供应商类别汇总3";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		setDept(session,supplyAcct);
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
		}else{                
		    // other
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");
		List<GrpTyp> grpTypList = findGysLeibieHuizongHeaders3(supplyAcct.getTyp());
		List<Map<String,Object>> list = gysLeibieHuizongMisBohService.findDeliverCategorySum3(findGysLeibieHuizongHeaders3(supplyAcct.getTyp()),condition,null).getRows();
//		exportExcel.creatWorkBook(response.getOutputStream(), ckRiqiHuizongService.findChkoutDateSum(condition,pager).getRows(), "", dictColumnsService.listDictColumnsByTable(dictColumns));
		gysLeibieHuizongMisBohService.exportDeliverCategorySum3(response.getOutputStream(), grpTypList, list);

	}
}
