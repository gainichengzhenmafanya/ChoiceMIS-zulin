package com.choice.misboh.web.controller.reportMis;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.misboh.commonutil.report.MISBOHStringConstant;
import com.choice.misboh.commonutil.report.ReportObject;
import com.choice.misboh.domain.reportMis.PublicEntity;
import com.choice.misboh.service.reportMis.WzChengbenChayiMisBohService;
import com.choice.scm.util.ExportExcel;
@Controller
@RequestMapping("WzChengbenChayiMisBoh")
public class WzChengbenChayiMisBohController extends BaseController{

	@Autowired
	private WzChengbenChayiMisBohService wzChengbenChayiMisBohService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;

	/****************************************************物资成本差异 start***************************************************/
	/**
	 * 查询物资仓库进出表
	 */
	@RequestMapping("/findCostVariance")
	@ResponseBody
	public Object findCostVariance(ModelMap modelMap,HttpSession session,PublicEntity condition) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		condition.setFirm(getFirmPositn(session).getCode());
		return wzChengbenChayiMisBohService.findCostVariance(condition);
	}
	
	/***
	 * 导出
	 * @param response
	 * @param request
	 * @param session
	 * @param condition
	 * @throws Exception
	 */
	@RequestMapping("/exportCostVariance")
	@ResponseBody
	public void exportCostVariance(HttpServletResponse response,HttpServletRequest request,HttpSession session,PublicEntity condition) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		condition.setFirm(getFirmPositn(session).getCode());
		String fileName = "物资成本差异";
		condition.getPager().setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(MISBOHStringConstant.REPORT_NAME_MISCOSTVARIANCE);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), wzChengbenChayiMisBohService.findCostVariance(condition).getRows(), "物资成本差异", dictColumnsService.listDictColumnsByAccount(dictColumns, MISBOHStringConstant.BASICINFO_REPORT_MISCOSTVARIANCE));	
	}
	
	/**
	 * 查询物资仓库进出表choice3
	 */
	@RequestMapping("/findCostVarianceChoice3")
	@ResponseBody
	public Object findCostVarianceChoice3(ModelMap modelMap,HttpSession session,PublicEntity condition) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		condition.setFirm(getFirmPositn(session).getCode());
		return wzChengbenChayiMisBohService.findCostVarianceChoice3(condition);
	}
	
	/***
	 * 导出
	 * @param response
	 * @param request
	 * @param session
	 * @param condition
	 * @throws Exception
	 */
	@RequestMapping("/exportCostVarianceChoice3")
	@ResponseBody
	public void exportCostVarianceChoice3(HttpServletResponse response,HttpServletRequest request,HttpSession session,PublicEntity condition) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		condition.setFirm(getFirmPositn(session).getCode());
		String fileName = "物资成本差异";
		condition.getPager().setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(MISBOHStringConstant.REPORT_NAME_MISCOSTVARIANCECHOICE3);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), wzChengbenChayiMisBohService.findCostVarianceChoice3(condition).getRows(), "物资成本差异", dictColumnsService.listDictColumnsByAccount(dictColumns, MISBOHStringConstant.BASICINFO_REPORT_MISCOSTVARIANCECHOICE3));	
	}
	
	/************************************************************手机报货 2015.2.28 wjf*********************************************************************/
	/**
	 * 查询物资成本差异
	 *
	 */
	@RequestMapping("/findCostVarianceWAP")
	@ResponseBody
	public Object findCostVarianceWAP(PublicEntity condition,String jsonpcallback) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		ReportObject<Map<String,Object>> lists = wzChengbenChayiMisBohService.findCostVariance(condition);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("list", lists);
		map.put("page", condition.getPager());
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
}
