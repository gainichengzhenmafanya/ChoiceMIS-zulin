package com.choice.misboh.web.controller.reportMis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.excel.GenerateExcel;
import com.choice.misboh.constants.reportMis.CostItemMisBohConstants;
import com.choice.misboh.constants.reportMis.MisBohStringConstant;
import com.choice.misboh.service.reportMis.YueChengbenZongheFenxiMisBohService;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Acct;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.AcctService;
import com.choice.scm.util.ReadReportUrl;
@Controller
@RequestMapping("YueChengbenZongheFenxiMisBoh")
public class YueChengbenZongheFenxiMisBohController extends BaseController{

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private YueChengbenZongheFenxiMisBohService yueChengbenZongheFenxiMisBohService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private GenerateExcel<Map<String,Object>> exportExcelMap;
	@Autowired
    private AcctService acctService;
	
	/********************************************月成本综合分析报表****************************************************/
	
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findYueChengbenZongheFenxiHeaders")
	@ResponseBody
	public Object getYueChengbenZongheFenxiHeaders(HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String table_name = MisBohStringConstant.REPORT_NAME_MISYUECHENGBENZONGHEFENXI;
		String defaultColumns = MisBohStringConstant.BASICINFO_REPORT_MISYUECHENGBENZONGHEFENXI;
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(table_name);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, defaultColumns));
		columns.put("frozenColumns", MisBohStringConstant.BASICINFO_REPORT_MISYUECHENGBENZONGHEFENXI_FROZEN);
		return columns;
	}
	/**
	 * 跳转到月成本综合分析报表页面
	 * @return
	 */
	@RequestMapping("/toYueChengbenZongheFenxi")
	public ModelAndView toWzChengbenHuizong(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String table_name = MisBohStringConstant.REPORT_NAME_MISYUECHENGBENZONGHEFENXI;
		modelMap.put("reportName", table_name);
		return new ModelAndView(CostItemMisBohConstants.REPORT_SHOW_MISYUECHENGBENZONGHEFENXI,modelMap);
	}
	/**
	 * 查询月成本综合分析报表内容
	 * @param modelMap
	 * @param session
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findYueChengbenZongheFenxi")
	@ResponseBody
	public Object findYueChengbenZongheFenxi(ModelMap modelMap, HttpSession session, String sort, String order,SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setBdate(DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		supplyAcct.setEdate(DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		Map<String,Object> condition = new HashMap<String,Object>();
		String choiceAcct = session.getAttribute("ChoiceAcct").toString();
		condition.put("sort",sort);
		condition.put("order", order);
		supplyAcct.setAcct(choiceAcct);
		supplyAcct.setFirm(getFirmPositn(session).getCode());
		condition.put("supplyAcct", supplyAcct);
		Acct acct = acctService.findAcctById(choiceAcct);
		condition.put("acctDes", acct.getDes());
			return yueChengbenZongheFenxiMisBohService.findYueChengbenZongheFenxi(condition);
	}
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printYueChengbenZongheFenxi")
	public ModelAndView printYueChengbenZongheFenxi(ModelMap modelMap,Page pager,HttpSession session,String type,String without0,SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String choiceAcct = session.getAttribute("ChoiceAcct").toString();
		supplyAcct.setBdate(DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		supplyAcct.setEdate(DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		supplyAcct.setAcct(choiceAcct);
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		params.put("positn",supplyAcct.getPositn());
		if(supplyAcct.getEdat() != null)
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		//params.put("delivercode",supplyAcct.getDelivercode());
		//params.put("accby", supplyAcct.getAccby());
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("grp",supplyAcct.getGrp());
		params.put("typ",supplyAcct.getTyp());
		supplyAcct.setFirm(getFirmPositn(session).getCode());
		condition.put("supplyAcct", supplyAcct);
		Acct acct = acctService.findAcctById(choiceAcct);
		condition.put("acctDes", acct.getDes());
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
			list = yueChengbenZongheFenxiMisBohService.findYueChengbenZongheFenxi(condition).getRows();
		modelMap.put("List", list);
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "月成本综合分析");
	    modelMap.put("actionMap", params);
	    parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
	    parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/yueChengbenZongheFenxiMisboh/printYueChengbenZongheFenxi.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,CostItemMisBohConstants.REPORT_PRINT_URL_MISYUECHENGBENZONGHEFENXI,CostItemMisBohConstants.REPORT_EXP_URL_MISYUECHENGBENZONGHEFENXI);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	
	/**
	 * 导出月成本综合分析报表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportYueChengbenZongheFenxi")
	@ResponseBody
	public void exportYueChengbenZongheFenxi(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String choiceAcct = session.getAttribute("ChoiceAcct").toString();
		supplyAcct.setBdate(DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		supplyAcct.setEdate(DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		String fileName = "月成本综合分析";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setFirm(getFirmPositn(session).getCode());
		supplyAcct.setAcct(choiceAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("supplyAcct", supplyAcct);
		Acct acct = acctService.findAcctById(choiceAcct);
		condition.put("acctDes", acct.getDes());
		pager.setPageSize(Integer.MAX_VALUE);
		String table_name = MisBohStringConstant.REPORT_NAME_MISYUECHENGBENZONGHEFENXI;
		String defaultColumns = MisBohStringConstant.BASICINFO_REPORT_MISYUECHENGBENZONGHEFENXI;
		Integer[] numColumns = MisBohStringConstant.EXCEL_MISYUECHENGBENZONGHEFENXI_FROZEN;
		dictColumns.setTableName(table_name);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
			list = yueChengbenZongheFenxiMisBohService.findYueChengbenZongheFenxi(condition).getRows();
		exportExcelMap.creatWorkBook_DictColumns(fileName, request, response, list, "月成本综合分析", dictColumnsService.listDictColumnsByAccount(dictColumns,defaultColumns), numColumns);
	}
	
	/********************************************月成本综合分析报表choice3****************************************************/
	
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findYueChengbenZongheFenxiHeadersChoice3")
	@ResponseBody
	public Object findYueChengbenZongheFenxiHeadersChoice3(HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String table_name = MisBohStringConstant.REPORT_NAME_MISYUECHENGBENZONGHEFENXICHOICE3;
		String defaultColumns = MisBohStringConstant.BASICINFO_REPORT_MISYUECHENGBENZONGHEFENXICHOICE3;
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(table_name);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, defaultColumns));
		columns.put("frozenColumns", MisBohStringConstant.BASICINFO_REPORT_MISYUECHENGBENZONGHEFENXICHOICE3_FROZEN);
		return columns;
	}
	/**
	 * 跳转到月成本综合分析报表页面
	 * @return
	 */
	@RequestMapping("/toYueChengbenZongheFenxiChoice3")
	public ModelAndView toYueChengbenZongheFenxiChoice3(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String table_name = MisBohStringConstant.REPORT_NAME_MISYUECHENGBENZONGHEFENXICHOICE3;
		modelMap.put("reportName", table_name);
		return new ModelAndView(CostItemMisBohConstants.REPORT_SHOW_MISYUECHENGBENZONGHEFENXICHOICE3,modelMap);
	}
	/**
	 * 查询月成本综合分析报表内容
	 * @param modelMap
	 * @param session
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findYueChengbenZongheFenxiChoice3")
	@ResponseBody
	public Object findYueChengbenZongheFenxiChoice3(ModelMap modelMap, HttpSession session, String sort, String order,SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setBdate(DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		supplyAcct.setEdate(DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		Map<String,Object> condition = new HashMap<String,Object>();
		String choiceAcct = session.getAttribute("ChoiceAcct").toString();
		condition.put("sort",sort);
		condition.put("order", order);
		supplyAcct.setAcct(choiceAcct);
		supplyAcct.setFirm(getFirmPositn(session).getCode());
		condition.put("supplyAcct", supplyAcct);
		Acct acct = acctService.findAcctById(choiceAcct);
		condition.put("acctDes", acct.getDes());
		return yueChengbenZongheFenxiMisBohService.findYueChengbenZongheFenxiChoice3(condition);
	}
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printYueChengbenZongheFenxiChoice3")
	public ModelAndView printYueChengbenZongheFenxiChoice3(ModelMap modelMap,Page pager,HttpSession session,String type,String without0,SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String choiceAcct = session.getAttribute("ChoiceAcct").toString();
		supplyAcct.setBdate(DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		supplyAcct.setEdate(DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		supplyAcct.setAcct(choiceAcct);
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		params.put("positn",supplyAcct.getPositn());
		if(supplyAcct.getEdat() != null)
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		//params.put("delivercode",supplyAcct.getDelivercode());
		//params.put("accby", supplyAcct.getAccby());
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("grp",supplyAcct.getGrp());
		params.put("typ",supplyAcct.getTyp());
		supplyAcct.setFirm(getFirmPositn(session).getCode());
		condition.put("supplyAcct", supplyAcct);
		Acct acct = acctService.findAcctById(choiceAcct);
		condition.put("acctDes", acct.getDes());
		List<Map<String,Object>> list = yueChengbenZongheFenxiMisBohService.findYueChengbenZongheFenxiChoice3(condition).getRows();
		modelMap.put("List", list);
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "月成本综合分析");
	    modelMap.put("actionMap", params);
	    parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
	    parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/yueChengbenZongheFenxiMisboh/printYueChengbenZongheFenxiChoice3.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,CostItemMisBohConstants.REPORT_PRINT_URL_MISYUECHENGBENZONGHEFENXICHOICE3,CostItemMisBohConstants.REPORT_EXP_URL_MISYUECHENGBENZONGHEFENXICHOICE3);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	
	/**
	 * 导出月成本综合分析报表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportYueChengbenZongheFenxiChoice3")
	@ResponseBody
	public void exportYueChengbenZongheFenxiChoice3(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String choiceAcct = session.getAttribute("ChoiceAcct").toString();
		supplyAcct.setBdate(DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		supplyAcct.setEdate(DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		String fileName = "月成本综合分析";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setFirm(getFirmPositn(session).getCode());
		supplyAcct.setAcct(choiceAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("supplyAcct", supplyAcct);
		Acct acct = acctService.findAcctById(choiceAcct);
		condition.put("acctDes", acct.getDes());
		pager.setPageSize(Integer.MAX_VALUE);
		String table_name = MisBohStringConstant.REPORT_NAME_MISYUECHENGBENZONGHEFENXICHOICE3;
		String defaultColumns = MisBohStringConstant.BASICINFO_REPORT_MISYUECHENGBENZONGHEFENXICHOICE3;
		Integer[] numColumns = MisBohStringConstant.EXCEL_MISYUECHENGBENZONGHEFENXICHOICE3_FROZEN;
		dictColumns.setTableName(table_name);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		List<Map<String,Object>> list  = yueChengbenZongheFenxiMisBohService.findYueChengbenZongheFenxiChoice3(condition).getRows();
//		exportExcelMap.creatWorkBook(response.getOutputStream(), yueChengbenZongheFenxiMisBohService.findYueChengbenZongheFenxi(condition).getRows(), "月成本综合分析", dictColumnsService.listDictColumnsByAccount(dictColumns,CostItemMisBohConstants.BASICINFO_REPORT_YUECHENGBENZONGHEFENXI));
		exportExcelMap.creatWorkBook_DictColumns(fileName, request, response, list, "月成本综合分析", dictColumnsService.listDictColumnsByAccount(dictColumns,defaultColumns), numColumns);
	}
}
