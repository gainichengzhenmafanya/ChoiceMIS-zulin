package com.choice.misboh.web.controller.reportMis;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.excel.GenerateExcel;
import com.choice.misboh.constants.reportMis.MisBohStringConstant;
import com.choice.misboh.constants.reportMis.SupplyAcctMisBohConstants;
import com.choice.misboh.service.reportMis.WzMingxiJinchuMisBohService;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.PositnService;
import com.choice.scm.util.ReadReportUrl;
@Controller
@RequestMapping("WzMingxiJinchuMisBoh")
public class WzMingxiJinchuMisBohController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private WzMingxiJinchuMisBohService WzMingxiJinchuMisBohService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private GenerateExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	private PositnService positnService;
	
	/*******************************************物资明细进出表报表*****************************************************/
	/**
	 * 跳转到物资明细进出表页面
	 * @param modelMap
	 * @return
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/toSupplyInOutInfo")
	public ModelAndView toSupplyInOutInfo(ModelMap modelMap, SupplyAcct supplyAcct){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("supplyAcct", supplyAcct);
		modelMap.put("reportName",MisBohStringConstant.REPORT_NAME_SUPPLYINOUTINFO);
		return new ModelAndView(SupplyAcctMisBohConstants.REPORT_SHOW_SUPPLYINOUTINFO_MIS,modelMap);
	}
	
	/**
	 * 查询表头
	 * @param session
	 * @return
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/findSupplyInOutInfoHeaders")
	@ResponseBody
	public Object getSupplyInOutInfoHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_SUPPLYINOUTINFO);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, MisBohStringConstant.BASICINFO_REPORT_SUPPLYINOUTINFO));
		columns.put("frozenColumns", MisBohStringConstant.BASICINFO_REPORT_SUPPLYINOUTINFO_FROZEN);
		return columns;
	}
	
	/**
	 * 查询物资明细进出表
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param typ
	 * @param bz
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/findSupplyInOutInfo")
	@ResponseBody
	public Object findSupplyInOutInfo(ModelMap modelMap, HttpSession session, String page, String typ,
			String rows, String sort, String order, SupplyAcct supplyAcct, String zero, String qimo) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> content = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		setDept(session,supplyAcct);//设置部门
		content.put("supplyAcct",supplyAcct);
		content.put("sort",sort);
		content.put("order", order);
		content.put("zero", zero);
		content.put("qimo", qimo);
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		return WzMingxiJinchuMisBohService.findSupplyInOutInfo(content, pager);
	}
	
	/**
	 * 导出物资明细进出表excel文件
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportSupplyInOutInfo")
	@ResponseBody
	public void exportSupplyInOutInfo(HttpServletResponse response, String sort, String order,
			HttpServletRequest request, HttpSession session, SupplyAcct supplyAcct, String zero, String qimo) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "物资明细进出表";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		setDept(session,supplyAcct);//设置部门
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("zero", zero);
		condition.put("qimo", qimo);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_SUPPLYINOUTINFO);
		ReportObject<Map<String,Object>> result = WzMingxiJinchuMisBohService.findSupplyInOutInfo(condition,pager);
		if(null != result.getFooter())
			result.getRows().addAll(result.getFooter());
//		exportExcelMap.creatWorkBook(response.getOutputStream(), WzMingxiJinchuMisBohService.findSupplyInOutInfo(condition,pager).getRows(), "物资进出明细表", dictColumnsService.listDictColumnsByAccount(dictColumns, MisBohStringConstant.BASICINFO_REPORT_SUPPLYINOUTINFO));
		exportExcelMap.creatWorkBook_DictColumns(fileName, request, response, result.getRows(), "物资进出明细表", dictColumnsService.listDictColumnsByAccount(dictColumns, MisBohStringConstant.BASICINFO_REPORT_SUPPLYINOUTINFO),MisBohStringConstant.EXCEL_SUPPLYINOUTINFO);
		
	}
	
	/**
	 * 打印物资明细进出表
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @param delivertyp
	 * @param folio
	 * @return
	 * @throws CRUDException
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/printSupplyInOutInfo")
	public ModelAndView printSupplyInOutInfo(ModelMap modelMap, Page pager, HttpSession session,
			String type, SupplyAcct supplyAcct, String delivertyp, String folio, String zero, String qimo)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
		if(supplyAcct.getBdat() != null){
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
			parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		}
		params.put("positn",supplyAcct.getPositn());
		if(supplyAcct.getEdat() != null){
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
			parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		}
		//params.put("chktyp",supplyAcct.getChktyp());
		//params.put("delivercode",supplyAcct.getDelivercode());
		params.put("sp_code",supplyAcct.getSp_code());
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("grp",supplyAcct.getGrp());
		params.put("typ",supplyAcct.getTyp());
		//params.put("folio", folio);
		//params.put("delivertyp", delivertyp);
		setDept(session,supplyAcct);//设置部门
		condition.put("zero", zero);
		condition.put("qimo", qimo);
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",WzMingxiJinchuMisBohService.findSupplyInOutInfo(condition,pager).getRows());
	    parameters.put("report_name", "物资明细进出表");
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/WzMingxiJinchuMisBoh/printSupplyInOutInfo.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctMisBohConstants.REPORT_PRINT_URL_SUPPLYINOUTINFO,SupplyAcctMisBohConstants.REPORT_EXP_URL_SUPPLYINOUTINFO);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	
	/**
	 * 跳转到列选择页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/toColChooseSupplyInOutInfo")
	public ModelAndView toColChooseSupplyInOutInfo(ModelMap modelMap, HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_SUPPLYINOUTINFO);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", MisBohStringConstant.REPORT_NAME_SUPPLYINOUTINFO);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,MisBohStringConstant.BASICINFO_REPORT_SUPPLYINOUTINFO));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(MisBohStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	
	/***
	 * 查询部门的方法
	 * @param session
	 * @param supplyAcct
	 * @throws CRUDException
	 */
	private void setDept(HttpSession session, SupplyAcct supplyAcct) throws CRUDException{
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(supplyAcct.getPositn()==null || "".equals(supplyAcct.getPositn())){//选择档口为空时，默认查询该分店的入库明细
			if(null != thePositn){
				//查询该分店的档口
				Positn positn = new Positn();
				positn.setUcode(thePositn.getCode());
				List<Positn> list = positnService.findPositnSuperNOPage(positn);
				String positncode = "";
				for(Positn posi:list){
					positncode +=CodeHelper.replaceCode(posi.getCode())+",";
				}
				positncode+=CodeHelper.replaceCode(thePositn.getCode());
				supplyAcct.setPositn(positncode);
			}
		}else{
			supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		}
	}
}
