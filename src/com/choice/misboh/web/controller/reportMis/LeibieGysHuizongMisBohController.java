package com.choice.misboh.web.controller.reportMis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.excel.GenerateExcel;
import com.choice.misboh.constants.reportMis.MisBohStringConstant;
import com.choice.misboh.constants.reportMis.SupplyAcctMisBohConstants;
import com.choice.misboh.service.reportMis.LeibieGysHuizongMisBohService;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.PositnService;
import com.choice.scm.util.ReadReportUrl;
@Controller
@RequestMapping("LeibieGysHuizongMisBoh")
public class LeibieGysHuizongMisBohController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private LeibieGysHuizongMisBohService leibieGysHuizongMisBohService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private GenerateExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	private PositnService positnService;
	
	
	/********************************************类别供应商汇总报表****************************************************/
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findCategoryDeliverSumHeaders")
	@ResponseBody
	public Object getCategoryDeliverSumHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_CATEGORYDELIVERSUMMISBOH);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, MisBohStringConstant.BASICINFO_REPORT_CATEGORYDELIVERSUMMISBOH));
		columns.put("frozenColumns", MisBohStringConstant.BASICINFO_REPORT_CATEGORYDELIVERSUMMISBOH_FROZEN);
		return columns;
	}
	/**
	 * 跳转到类别供应商汇总报表页面
	 * @return
	 */
	@RequestMapping("/toCategoryDeliverSum")
	public ModelAndView toCategoryDeliverSum(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", MisBohStringConstant.REPORT_NAME_CATEGORYDELIVERSUMMISBOH);
		return new ModelAndView(SupplyAcctMisBohConstants.REPORT_SHOW_CATEGORYDELIVERSUM,modelMap);
	}
	
	/**
	 * 查询类别供应商汇总报表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findCategoryDeliverSum")
	@ResponseBody
	public Object ChkinCategoryDeliverSum(ModelMap modelMap, HttpSession session,String sort, String order, String page,
			String rows, SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		String accountId=session.getAttribute("accountId").toString();
		supplyAcct.setAccountId(accountId);
		setDept(session,supplyAcct);//设置部门
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		return leibieGysHuizongMisBohService.findCategoryDeliverSum(condition,pager);
	}
	
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printCategoryDeliverSum")
	public ModelAndView printCategoryDeliverSum(ModelMap modelMap, Page pager, HttpSession session,
			String type, SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		params.put("positn",supplyAcct.getPositn());
		params.put("grpdes", supplyAcct.getGrpdes());
		params.put("grptyp",supplyAcct.getGrptyp());
		if(supplyAcct.getEdat() != null)
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		params.put("grp",supplyAcct.getGrp());
		params.put("chktyp",supplyAcct.getChktyp());
		params.put("delivercode",supplyAcct.getDelivercode());
		String accountId=session.getAttribute("accountId").toString();
		supplyAcct.setAccountId(accountId);
		setDept(session,supplyAcct);//设置部门
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",leibieGysHuizongMisBohService.findCategoryDeliverSum(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "类别供应商汇总");
	    modelMap.put("actionMap", params);
	    parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
	    parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/LeibieGysHuizongMisBoh/printCategoryDeliverSum.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctMisBohConstants.REPORT_PRINT_URL_CATEGORYDELIVERSUM,SupplyAcctMisBohConstants.REPORT_EXP_URL_CATEGORYDELIVERSUM);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	
	/**
	 * 导出类别供应商汇总报表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportCategoryDeliverSum")
	@ResponseBody
	public void exportCategoryDeliverSum(HttpServletResponse response, String sort, String order,
			HttpServletRequest request, HttpSession session, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "类别供应商汇总";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		String accountId=session.getAttribute("accountId").toString();
		supplyAcct.setAccountId(accountId);
		setDept(session,supplyAcct);//设置部门
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_CATEGORYDELIVERSUMMISBOH);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		ReportObject<Map<String,Object>> result = leibieGysHuizongMisBohService.findCategoryDeliverSum(condition,pager);
		if(null != result.getFooter())
			result.getRows().addAll(result.getFooter());
//		exportExcelMap.creatWorkBook(response.getOutputStream(), leibieGysHuizongMisBohService.findCategoryDeliverSum(condition,pager).getRows(), "类别供应商汇总", dictColumnsService.listDictColumnsByTable(dictColumns));
		exportExcelMap.creatWorkBook_DictColumns(fileName, request,response, result.getRows(), "类别供应商汇总", dictColumnsService.listDictColumnsByTable(dictColumns),MisBohStringConstant.EXCEL_CATEGORYDELIVERSUMMISBOH);
	}
	
	/********************************************类别供应商汇总报表1****************************************************/
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findCategoryDeliverSum1Headers")
	@ResponseBody
	public Object getCategoryDeliverSum1Headers(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_CATEGORYDELIVERSUM1MISBOH);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, MisBohStringConstant.BASICINFO_REPORT_CATEGORYDELIVERSUM1MISBOH));
		columns.put("frozenColumns", MisBohStringConstant.BASICINFO_REPORT_CATEGORYDELIVERSUM1MISBOH_FROZEN);
		return columns;
	}
	/**
	 * 跳转到类别供应商汇总1报表页面
	 * @return
	 */
	@RequestMapping("/toCategoryDeliverSum1")
	public ModelAndView toCategoryDeliverSum1(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", MisBohStringConstant.REPORT_NAME_CATEGORYDELIVERSUM1MISBOH);
		return new ModelAndView(SupplyAcctMisBohConstants.REPORT_SHOW_CATEGORYDELIVERSUM1,modelMap);
	}
	/**
	 * 查询类别供应商汇总1报表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findCategoryDeliverSum1")
	@ResponseBody
	public Object ChkinCategoryDeliverSum1(ModelMap modelMap,HttpSession session,String sort, String order, SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		String accountId=session.getAttribute("accountId").toString();
		supplyAcct.setAccountId(accountId);
		setDept(session,supplyAcct);//设置部门
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		return leibieGysHuizongMisBohService.findCategoryDeliverSum1(condition,pager);
	}
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printCategoryDeliverSum1")
	public ModelAndView printCategoryDeliverSum1(ModelMap modelMap,Page pager,HttpSession session,String type,SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		String accountId=session.getAttribute("accountId").toString();
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		params.put("positn",supplyAcct.getPositn());
		params.put("grptyp",supplyAcct.getGrptyp());
		if(supplyAcct.getEdat() != null)
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		params.put("accountId", accountId);
		params.put("grp",supplyAcct.getGrp());
		params.put("chktyp",supplyAcct.getChktyp());
		params.put("delivercode",supplyAcct.getDelivercode());
		supplyAcct.setAccountId(accountId);
		setDept(session,supplyAcct);//设置部门
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",leibieGysHuizongMisBohService.findCategoryDeliverSum1(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "类别供应商汇总1");
	    modelMap.put("actionMap", params);
	    parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
	    parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/LeibieGysHuizongMisBoh/printCategoryDeliverSum1.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctMisBohConstants.REPORT_PRINT_URL_CATEGORYDELIVERSUM1,SupplyAcctMisBohConstants.REPORT_EXP_URL_CATEGORYDELIVERSUM1);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	/**
	 * 导出类别供应商汇总报表
	 * @param response	
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportCategoryDeliverSum1")
	@ResponseBody
	public void exportCategoryDeliverSum1(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "类别供应商汇总1";
		Map<String,Object> condition = new HashMap<String,Object>();
		String accountId=session.getAttribute("accountId").toString();
		supplyAcct.setAccountId(accountId);
		setDept(session,supplyAcct);//设置部门
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_CATEGORYDELIVERSUM1MISBOH);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		ReportObject<Map<String,Object>> result = leibieGysHuizongMisBohService.findCategoryDeliverSum1(condition,pager);
		if(null != result.getFooter())
			result.getRows().addAll(result.getFooter());
//		exportExcelMap.creatWorkBook(response.getOutputStream(), leibieGysHuizongMisBohService.findCategoryDeliverSum1(condition,pager).getRows(), "类别供应商汇总1", dictColumnsService.listDictColumnsByTable(dictColumns));
		exportExcelMap.creatWorkBook_DictColumns(fileName, request,response, result.getRows(), "类别供应商汇总1", dictColumnsService.listDictColumnsByTable(dictColumns),MisBohStringConstant.EXCEL_CATEGORYDELIVERSUM1MISBOH);
	}
	
	/***
	 * 查询部门的方法
	 * @param session
	 * @param supplyAcct
	 * @throws CRUDException
	 */
	private void setDept(HttpSession session, SupplyAcct supplyAcct) throws CRUDException{
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(supplyAcct.getPositn()==null || "".equals(supplyAcct.getPositn())){//选择档口为空时，默认查询该分店的入库明细
			if(null != thePositn){
				//查询该分店的档口
				Positn positn = new Positn();
				positn.setUcode(thePositn.getCode());
				List<Positn> list = positnService.findPositnSuperNOPage(positn);
				String positncode = "";
				for(Positn posi:list){
					positncode +=CodeHelper.replaceCode(posi.getCode())+",";
				}
				positncode+=CodeHelper.replaceCode(thePositn.getCode());
				supplyAcct.setPositn(positncode);
			}
		}else{
			supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		}
	}
}
