package com.choice.misboh.web.controller.reportMis;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.RoleTime;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountFirmService;
import com.choice.framework.service.system.RoleTimeService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.misboh.commonutil.ReadReport;
import com.choice.misboh.constants.reportMis.MisStringConstant;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;

public class MisBohBaseController {

	@Autowired
	protected DictColumnsService dictColumnsService;
	@Autowired
	protected DictColumns dictColumns;
	@Autowired
	protected AccountFirmService accountFirmService;
	@Autowired
	private RoleTimeService roleTimeService;
	
	/**
	 * 保存现有显示字段
	 * @param modelMap
	 * @param dictColumns
	 * @param session
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/saveColumnsChoose")
	public ModelAndView saveColumnsChoose(ModelMap modelMap,DictColumns dictColumns,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());	
		dictColumnsService.saveColumnsChoose(dictColumns);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 恢复默认显示字段
	 * @param modelMap
	 * @param dictColumns
	 * @param session
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/recoverDefaultColumns")
	public ModelAndView recoverDefaultColumns(ModelMap modelMap,DictColumns dictColumns,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumnsService.deleteColumnsChoose(dictColumns);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 跳转到列选择页面
	 * @throws CRUDException
	 */
	@RequestMapping("/toColumnsChoose")
	public ModelAndView toColumnsChoose(ModelMap modelMap,HttpSession session,String reportName)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ReadReport.getReportName(MisStringConstant.class,reportName));
		String[] objBean = this.getClass().getAnnotation(RequestMapping.class).value();
		if(objBean.length >= 1)
			modelMap.put("objBean", objBean[0]);
		modelMap.put("tableName", ReadReport.getReportName(MisStringConstant.class,reportName));
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ReadReport.getDefaultHeader(MisStringConstant.class, reportName)));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(MisStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findHeaders")
	@ResponseBody
	public Object getHeaders(HttpSession session,String reportName){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ReadReport.getReportName(MisStringConstant.class,reportName));
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ReadReport.getDefaultHeader(MisStringConstant.class, reportName)));
		columns.put("frozenColumns", ReadReport.getFrozenHeader(MisStringConstant.class,reportName));
		return columns;
	}
	
	/**
	 * 描述：获取时间权限
	 * 作者：马振
	 * 时间：2017年5月25日下午3:13:52
	 * @param modelMap
	 * @param session
	 * @throws CRUDException
	 */
	public void getRoleTiem(ModelMap modelMap,HttpSession session) throws CRUDException{
	    DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
	    String accountId = session.getAttribute("accountId").toString();
	    RoleTime roleTime = roleTimeService.findRoleTimeByAccountId(accountId);
	    if(roleTime == null){
	      roleTime = new RoleTime();
	      roleTime.setAfterDay("3000");
	      roleTime.setBeforeDay("3000");
	    }
	    SimpleDateFormat sft = new SimpleDateFormat("yyyy-MM-dd");
	    modelMap.put("newdate", new Date());
	    modelMap.put("servertime", sft.format(new Date()));
	    modelMap.put("roleTimeMap", roleTime);
	  }
}