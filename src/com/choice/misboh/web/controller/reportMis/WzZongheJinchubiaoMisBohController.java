package com.choice.misboh.web.controller.reportMis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.excel.GenerateExcel;
import com.choice.misboh.constants.reportMis.MisBohStringConstant;
import com.choice.misboh.constants.reportMis.SupplyAcctMisBohConstants;
import com.choice.misboh.service.reportMis.WzZongheJinchubiaoMisBohService;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.PositnService;
import com.choice.scm.util.ReadReportUrl;
@Controller
@RequestMapping("WzZongheJinchubiaoMisBoh")
public class WzZongheJinchubiaoMisBohController {

	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private WzZongheJinchubiaoMisBohService WzZongheJinchubiaoMisBohService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private GenerateExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	private PositnService positnService;
	
	
	/********************************************物资综合进出表报表****************************************************/
	/**
	 * 跳转到列选择页面
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseSupplySumInOut")
	public ModelAndView toColChooseSupplySumInOut(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_SUPPLYSUMINOUT);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", MisBohStringConstant.REPORT_NAME_SUPPLYSUMINOUT);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,MisBohStringConstant.BASICINFO_REPORT_SUPPLYSUMINOUT));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(MisBohStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findSupplySumInOutHeaders")
	@ResponseBody
	public Object getSupplySumInOutHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_SUPPLYSUMINOUT);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, MisBohStringConstant.BASICINFO_REPORT_SUPPLYSUMINOUT));
		columns.put("frozenColumns", MisBohStringConstant.BASICINFO_REPORT_SUPPLYSUMINOUT_FROZEN);
		return columns;
	}
	/**
	 * 跳转到物资综合进出表报表页面
	 * @return
	 */
	@RequestMapping("/toSupplySumInOut")
	public ModelAndView toSupplySumInOut(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", MisBohStringConstant.REPORT_NAME_SUPPLYSUMINOUT);
		return new ModelAndView(SupplyAcctMisBohConstants.REPORT_SHOW_SUPPLYSUMINOUT_MIS,modelMap);
	}
	/**
	 * 查询物资综合进出表报表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findSupplySumInOut")
	@ResponseBody
	public Object ChkinSupplySumInOut(ModelMap modelMap,HttpSession session,String without0,String sort,String order,SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		condition.put("sort",sort);
		condition.put("order", order);
		condition.put("without0", without0);
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		setDept(session,supplyAcct);//设置部门
		condition.put("supplyAcct", supplyAcct);
		return WzZongheJinchubiaoMisBohService.findSupplySumInOut(condition);
	}
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printSupplySumInOut")
	public ModelAndView printSupplySumInOut(ModelMap modelMap,Page pager,HttpSession session,String type,String without0,SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		String positn = supplyAcct.getPositn();
		setDept(session,supplyAcct);//设置部门
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		params.put("positn",positn);
		if(supplyAcct.getEdat() != null)
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		//params.put("delivercode",supplyAcct.getDelivercode());
		//params.put("accby", supplyAcct.getAccby());
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("grp",supplyAcct.getGrp());
		params.put("typ",supplyAcct.getTyp());
		params.put("without0", without0);
		condition.put("without0", without0);
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List", WzZongheJinchubiaoMisBohService.findSupplySumInOut(condition).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "物资综合进出表");
	    modelMap.put("actionMap", params);
	    parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
	    parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/WzZongheJinchubiaoMisBoh/printSupplySumInOut.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctMisBohConstants.REPORT_PRINT_URL_SUPPLYSUMINOUT,SupplyAcctMisBohConstants.REPORT_EXP_URL_SUPPLYSUMINOUT);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	/**
	 * 导出物资综合进出表报表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportSupplySumInOut")
	@ResponseBody
	public void exportSupplySumInOut(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,String without0,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "物资综合进出表";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		setDept(session,supplyAcct);//设置部门
		condition.put("without0", without0);
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_SUPPLYSUMINOUT);
		ReportObject<Map<String,Object>> result = WzZongheJinchubiaoMisBohService.findSupplySumInOut(condition);
		if(null != result.getFooter())
			result.getRows().addAll(result.getFooter());
//		exportExcelMap.creatWorkBook(response.getOutputStream(), WzZongheJinchubiaoMisBohService.findSupplySumInOut(condition).getRows(), "物资综合进出表", dictColumnsService.listDictColumnsByTable(dictColumns));
		exportExcelMap.creatWorkBook_DictColumns(fileName, request, response, result.getRows(), "物资综合进出表", dictColumnsService.listDictColumnsByTable(dictColumns),MisBohStringConstant.EXCEL_SUPPLYSUMINOUT);
	}
	
	/***
	 * 查询部门的方法
	 * @param session
	 * @param supplyAcct
	 * @throws CRUDException
	 */
	private void setDept(HttpSession session, SupplyAcct supplyAcct) throws CRUDException{
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(supplyAcct.getPositn()==null || "".equals(supplyAcct.getPositn())){//选择档口为空时，默认查询该分店的入库明细
			if(null != thePositn){
				//查询该分店的档口
				Positn positn = new Positn();
				positn.setUcode(thePositn.getCode());
				List<Positn> list = positnService.findPositnSuperNOPage(positn);
				String positncode = "";
				for(Positn posi:list){
					positncode +=CodeHelper.replaceCode(posi.getCode())+",";
				}
				positncode+=CodeHelper.replaceCode(thePositn.getCode());
				supplyAcct.setPositn(positncode);
			}
		}else{
			supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		}
	}
}
