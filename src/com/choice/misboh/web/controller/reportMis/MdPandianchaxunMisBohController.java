package com.choice.misboh.web.controller.reportMis;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.redis.RedisConfig;
import com.choice.framework.util.CacheUtil;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.misboh.constants.reportMis.MisBohStringConstant;
import com.choice.misboh.constants.reportMis.SupplyAcctMisBohConstants;
import com.choice.misboh.domain.inventory.Chkstoreo;
import com.choice.misboh.domain.inventory.PositnSupply;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.misboh.service.reportMis.MdPandianchaxunMisBohService;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyUnit;
import com.choice.scm.service.MainInfoService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
/***
 * 门店boh盘点查询报表
 * @author 2015.9.1wjf
 *
 */
@Controller
@RequestMapping("MdPandianchaxunMisBoh")
public class MdPandianchaxunMisBohController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private MdPandianchaxunMisBohService mdPandianchaxunService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	@Autowired
	private MainInfoService mainInfoService;
	
	private static String opencluster = RedisConfig.getString("opencluster");


	/********************************************门店盘点历史报表****************************************************/
	/**
	 * 跳转到盘点历史查询页面
	 * @param modelMap
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/toMdPandianchaxun")
	public ModelAndView toMdPandianchaxun(ModelMap modelMap,HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName",MisBohStringConstant.REPORT_NAME_MDPANDIAN);
		return new ModelAndView(SupplyAcctMisBohConstants.REPORT_SHOW_MDPANDIAN,modelMap);
	}
	
	/**
	 * 查询表头
	 * @param session
	 * @return
	 */
	@RequestMapping("/findMdPandianHeaders")
	@ResponseBody
	public Object findMdPandianHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_MDPANDIAN);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, MisBohStringConstant.BASICINFO_REPORT_MDPANDIAN));
		columns.put("frozenColumns",MisBohStringConstant.BASICINFO_REPORT_MDPANDIAN_FROZEN );
		return columns;
	}
	
	/***
	 * 查询门店盘点数据
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param positnPand
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findMdPandian")
	@ResponseBody
	public Object findMdPandian(ModelMap modelMap, HttpSession session, String page, String rows, Chkstoreo chkstore) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		chkstore.setFirm(thePositn.getCode());
		chkstore.setDept(CodeHelper.replaceCode(chkstore.getDept()));
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		return mdPandianchaxunService.findMdPandian(chkstore, pager);
	}
	/**
	 * 导出历史盘点查询excel文件
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param positnPand
	 * @throws Exception
	 */
	@RequestMapping("/exportMdPandian")
	@ResponseBody
	public void exportMdPandian(HttpServletResponse response, HttpServletRequest request, HttpSession session, Chkstoreo chkstore) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "门店盘点查询";
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		chkstore.setFirm(thePositn.getCode());
		chkstore.setDept(CodeHelper.replaceCode(chkstore.getDept()));
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_MDPANDIAN);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		ReportObject<Map<String,Object>> result = mdPandianchaxunService.findMdPandian(chkstore,pager);
		if(null != result.getFooter())
			result.getRows().addAll(result.getFooter());
		exportExcelMap.creatWorkBook(response.getOutputStream(), result.getRows(), "门店盘点查询", dictColumnsService.listDictColumnsByAccount(dictColumns, MisBohStringConstant.BASICINFO_REPORT_MDPANDIAN));
		
	}
	
	/***
	 * 打印盘点查询
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param month
	 * @param without0
	 * @param type
	 * @param supplyAcct
	 * @param withamountin
	 * @param folio
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/printMdPandian")
	public ModelAndView printMdPandian(ModelMap modelMap, Page pager, HttpSession session,String type, Chkstoreo chkstore)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		chkstore.setFirm(thePositn.getCode());
		chkstore.setDept(CodeHelper.replaceCode(chkstore.getDept()));
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,String> params = new HashMap<String,String>();
		if(chkstore.getBdate() != null)
			params.put("bdat",chkstore.getBdate());
		if(chkstore.getEdate() != null)
			params.put("edat",chkstore.getEdate());
		params.put("dept", CodeHelper.replaceCode(chkstore.getDept()));
		params.put("pantyp", chkstore.getPantyp());
		params.put("state", chkstore.getState());
		params.put("ecode", chkstore.getEcode());
		params.put("checkCode", chkstore.getCheckCode());
		modelMap.put("List",mdPandianchaxunService.findMdPandian(chkstore,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "门店盘点查询");
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountNames").toString());
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/MdPandianchaxunMisBoh/printMdPandian.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctMisBohConstants.REPORT_PRINT_URL_MDPANDIAN,SupplyAcctMisBohConstants.REPORT_EXP_URL_MDPANDIAN);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	
	/********************************************门店盘点历史报表明细 ****************************************************/
	
	/***
	 * 判断Supplyunit
	 * @param session
	 * @throws CRUDException
	 */
	private void setSupplyUnit(HttpSession session) throws CRUDException {
		if(null != opencluster && "1".equals(opencluster)){
			Object obj = session.getAttribute("supd_cache");
			if(null == obj){
				session.setAttribute("supd_cache", findSupplyUnit(session));
			}
    	} else {
    		CacheUtil cacheUtil = CacheUtil.getInstance();
    		@SuppressWarnings("unchecked")
			List<SupplyUnit> supd_cache = (List<SupplyUnit>)cacheUtil.get("supd_cache", session.getId());	
			if(null == supd_cache){//如果是空，加入缓存
				cacheUtil.flush("supd_cache", session.getId());
				cacheUtil.put("supd_cache", session.getId(), findSupplyUnit(session));
			}
    	}
	}
	
	/***
	 * 设置supplyunit
	 * @param session
	 * @return
	 * @throws Exception
	 */
	private List<SupplyUnit> findSupplyUnit(HttpSession session) throws CRUDException {
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		PositnSupply ps = new PositnSupply();
		String acct = session.getAttribute("ChoiceAcct").toString();
		String yearr = mainInfoService.findYearrList().get(0);//会计年wjf
		ps.setAcct(acct);
		ps.setYearr(yearr);
		ps.setPositn(thePositn.getCode());
		ps.setYnpd("Y");
		return commonMISBOHService.findAllSupplyUnitByPositn(ps);
	}
	
	/***
	 * 得到supplyunit
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@SuppressWarnings("unchecked")
	private List<SupplyUnit> getSupplyUnit(HttpSession session) throws CRUDException {
		if(null != opencluster && "1".equals(opencluster)){
    		return (List<SupplyUnit>) session.getAttribute("supd_cache");
    	} else {
    		CacheUtil cacheUtil = CacheUtil.getInstance();
    		return (List<SupplyUnit>) cacheUtil.get("supd_cache", session.getId());
    	}
	}
	
	/**
	 * 跳转到盘点历史查询页面
	 * @param modelMap
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/toMdPandianchaxunDetail")
	public ModelAndView toMdPandianchaxunDetail(ModelMap modelMap,HttpSession session,Chkstoreo chkstore) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName",MisBohStringConstant.REPORT_NAME_MDPANDIANDETAIL);
		setSupplyUnit(session);
		modelMap.put("chkstore", chkstore);
		return new ModelAndView(SupplyAcctMisBohConstants.REPORT_SHOW_MDPANDIAN_DETAIL,modelMap);
	}
	
	/**
	 * 查询表头
	 * @param session
	 * @return
	 */
	@RequestMapping("/findMdPandianDetailHeaders")
	@ResponseBody
	public Object findMdPandianDetailHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_MDPANDIANDETAIL);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, MisBohStringConstant.BASICINFO_REPORT_MDPANDIANDETAIL));
		columns.put("frozenColumns",MisBohStringConstant.BASICINFO_REPORT_MDPANDIANDETAIL_FROZEN );
		return columns;
	}
	
	/***
	 * 查询门店盘点数据
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param positnPand
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findMdPandianDetail")
	@ResponseBody
	public Object findMdPandianDetail(ModelMap modelMap, HttpSession session, String page, String rows, Chkstoreo chkstore) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		chkstore.setFirm(thePositn.getCode());
		chkstore.setDept(CodeHelper.replaceCode(chkstore.getDept()));
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		return mdPandianchaxunService.findMdPandian(chkstore, pager, getSupplyUnit(session));
	}
	/**
	 * 导出历史盘点查询excel文件
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param positnPand
	 * @throws Exception
	 */
	@RequestMapping("/exportMdPandianDetail")
	@ResponseBody
	public void exportMdPandianDetail(HttpServletResponse response, HttpServletRequest request, HttpSession session, Chkstoreo chkstore) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "门店盘点明细查询";
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		chkstore.setFirm(thePositn.getCode());
		chkstore.setDept(CodeHelper.replaceCode(chkstore.getDept()));
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_MDPANDIANDETAIL);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		ReportObject<Map<String,Object>> result = mdPandianchaxunService.findMdPandian(chkstore,pager,getSupplyUnit(session));
		if(null != result.getFooter())
			result.getRows().addAll(result.getFooter());
		exportExcelMap.creatWorkBook(response.getOutputStream(), result.getRows(), "门店盘点明细查询", dictColumnsService.listDictColumnsByAccount(dictColumns, MisBohStringConstant.BASICINFO_REPORT_MDPANDIANDETAIL));
		
	}
	
	/***
	 * 打印盘点查询
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param month
	 * @param without0
	 * @param type
	 * @param supplyAcct
	 * @param withamountin
	 * @param folio
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/printMdPandianDetail")
	public ModelAndView printMdPandianDetail(ModelMap modelMap, Page pager, HttpSession session,String type, Chkstoreo chkstore)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		chkstore.setFirm(thePositn.getCode());
		chkstore.setDept(CodeHelper.replaceCode(chkstore.getDept()));
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,String> params = new HashMap<String,String>();
		params.put("sp_code",chkstore.getSp_code());
		params.put("grptyp",chkstore.getGrptyp());
		params.put("grp",chkstore.getGrp());
		params.put("typ",chkstore.getTyp());
		params.put("ynpd",chkstore.getYnpd());
		modelMap.put("List",mdPandianchaxunService.findMdPandian(chkstore,pager,getSupplyUnit(session)).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "门店盘点明细查询");
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountNames").toString());
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/MdPandianchaxunMisBoh/printMdPandian.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctMisBohConstants.REPORT_PRINT_URL_MDPANDIAN_DETAIL,SupplyAcctMisBohConstants.REPORT_EXP_URL_MDPANDIAN_DETAIL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	
	/********************************************门店盘点明細****************************************************/
	/**
	 * 跳转到盘点历史查询页面
	 * @param modelMap
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/toMdPandianMingxi")
	public ModelAndView toMdPandianMingxi(ModelMap modelMap,HttpSession session,Chkstoreo chkstore) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName",MisBohStringConstant.REPORT_NAME_MDPANDIANMINGXI);
		setSupplyUnit(session);
		return new ModelAndView(SupplyAcctMisBohConstants.REPORT_SHOW_MDPANDIAN_MINGXI,modelMap);
	}
	
	/**
	 * 查询表头
	 * @param session
	 * @return
	 */
	@RequestMapping("/findMdPandianMingxiHeaders")
	@ResponseBody
	public Object findMdPandianMingxiHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_MDPANDIANMINGXI);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, MisBohStringConstant.BASICINFO_REPORT_MDPANDIANMINGXI));
		columns.put("frozenColumns",MisBohStringConstant.BASICINFO_REPORT_MDPANDIANMINGXI_FROZEN );
		return columns;
	}
	
	/***
	 * 查询门店盘点数据
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param positnPand
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findMdPandianMingxi")
	@ResponseBody
	public Object findMdPandianMingxi(ModelMap modelMap, HttpSession session, String page, String rows, Chkstoreo chkstore) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		chkstore.setFirm(thePositn.getCode());
		chkstore.setDept(CodeHelper.replaceCode(chkstore.getDept()));
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		return mdPandianchaxunService.findMdPandianMingxi(chkstore, pager, getSupplyUnit(session));
	}
	/**
	 * 导出历史盘点查询excel文件
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param positnPand
	 * @throws Exception
	 */
	@RequestMapping("/exportMdPandianMingxi")
	@ResponseBody
	public void exportMdPandianMingxi(HttpServletResponse response, HttpServletRequest request, HttpSession session, Chkstoreo chkstore) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "门店盘点明细";
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		chkstore.setFirm(thePositn.getCode());
		chkstore.setDept(CodeHelper.replaceCode(chkstore.getDept()));
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_MDPANDIANMINGXI);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		ReportObject<Map<String,Object>> result = mdPandianchaxunService.findMdPandianMingxi(chkstore,pager, getSupplyUnit(session));
		if(null != result.getFooter())
			result.getRows().addAll(result.getFooter());
		exportExcelMap.creatWorkBook(response.getOutputStream(), result.getRows(), "门店盘点明细", 
				dictColumnsService.listDictColumnsByAccount(dictColumns, MisBohStringConstant.BASICINFO_REPORT_MDPANDIANMINGXI));
		
	}
	
}
