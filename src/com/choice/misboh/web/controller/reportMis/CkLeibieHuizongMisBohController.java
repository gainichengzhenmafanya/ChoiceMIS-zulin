package com.choice.misboh.web.controller.reportMis;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.reportMis.CkLeibieHuizongMisService;
@Controller
@RequestMapping("CkLeibieHuizongMisBoh")
public class CkLeibieHuizongMisBohController {

	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private CkLeibieHuizongMisService ckLeibieHuizongService;
	@Autowired
	private PositnService positnService;
	
	
	/********************************************出库类别汇总报表****************************************************/
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/findChkoutCategorySumHeaders")
	@ResponseBody
	public Object getChkoutCategorySum(HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		columns.put("columns", positnService.findAllPositn(null));
		return columns;
	}
	
	/**
	 * 跳转到报表html页面
	 * 
	 * @return
	 */
	@RequestMapping("/toChkoutCategorySum")
	public ModelAndView toChkoutCategorySum(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", "出库类别汇总");
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_CHKOUTMSYNQUERY_MIS,modelMap);
	}
	
	/**
	 * 查询报表数据
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findChkoutCategorySum")
	@ResponseBody
	public Object findChkoutCategorySum(ModelMap modelMap, HttpSession session, SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> conditions = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		//wjf 解决查询门店时报错
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(supplyAcct.getPositn()==null || "".equals(supplyAcct.getPositn())){//选择档口为空时，默认查询该分店的入库明细
			if(null != thePositn){
				//查询该分店的档口
				Positn positn = new Positn();
				positn.setUcode(thePositn.getCode());
				List<Positn> list = positnService.findPositnSuperNOPage(positn);
				String positncode = "";
				for(Positn posi:list){
					positncode +=CodeHelper.replaceCode(posi.getCode())+",";
				}
				positncode+=CodeHelper.replaceCode(thePositn.getCode());
				supplyAcct.setPositn(positncode);
			}
		}else{
			supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		}
		supplyAcct.setFirm(CodeHelper.replaceCode(supplyAcct.getFirm()));
		conditions.put("supplyAcct", supplyAcct);
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		return ckLeibieHuizongService.findChkoutCategorySum(conditions);
	}
	
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
//	@RequestMapping(value = "/printChkoutCategorySum")
//	public ModelAndView printChkoutCategorySum(ModelMap modelMap,Page pager,HttpSession session,String type,SupplyAcct supplyAcct)throws CRUDException{
//		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
//		pager.setPageSize(Integer.MAX_VALUE);
//		Map<String,String> params = new HashMap<String,String>();
//		if(supplyAcct.getBdat() != null)
//			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
//		params.put("positn",supplyAcct.getPositn());
//		params.put("grptyp",supplyAcct.getGrptyp());
//		if(supplyAcct.getEdat() != null)
//			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
//		params.put("grp",supplyAcct.getGrp());
//		params.put("chktyp",supplyAcct.getChktyp());
//		params.put("delivercode",supplyAcct.getDelivercode());
//		
//		modelMap.put("List",supplyAcctService.findAllChkoutmGrp(supplyAcct).getRows());
//	 	HashMap  parameters = new HashMap();
//	    parameters.put("report_name", "入库类别汇总");
//	    modelMap.put("actionMap", params);
//	    parameters.put("maded",new Date());
//	    parameters.put("madeby", session.getAttribute("accountName").toString());
//	        
//        modelMap.put("parameters", parameters);
//	 	modelMap.put("action", "/SupplyAcct/printChkoutCategorySum.do");//传入回调路径
//	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_ChkoutMCATEGORYSUMMARY,SupplyAcctConstants.REPORT_EXP_URL_ChkoutMCATEGORYSUMMARY);//判断跳转路径
//        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
//		return new ModelAndView(rs.get("url"),modelMap);
//	}
	
	/**
	 * 导出
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportChkoutCategorySum")
	@ResponseBody
	public void exportChkoutCategorySum(HttpServletResponse response, String sort, String order, 
			HttpServletRequest request, HttpSession session, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "出库类别汇总";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		//wjf 解决查询门店时报错
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(supplyAcct.getPositn()==null || "".equals(supplyAcct.getPositn())){//选择档口为空时，默认查询该分店的入库明细
			if(null != thePositn){
				//查询该分店的档口
				Positn positn = new Positn();
				positn.setUcode(thePositn.getCode());
				List<Positn> list = positnService.findPositnSuperNOPage(positn);
				String positncode = "";
				for(Positn posi:list){
					positncode +=CodeHelper.replaceCode(posi.getCode())+",";
				}
				positncode+=CodeHelper.replaceCode(thePositn.getCode());
				supplyAcct.setPositn(positncode);
			}
		}else{
			supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		}

		supplyAcct.setFirm(CodeHelper.replaceCode(supplyAcct.getFirm()));
		condition.put("supplyAcct", supplyAcct);
		dictColumns.setTableName("出库类别汇总");
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		ckLeibieHuizongService.exportChkoutCategorySum(response.getOutputStream(), condition);
	}
}
