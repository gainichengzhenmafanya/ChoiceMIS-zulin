package com.choice.misboh.web.controller.reportMis;

import java.lang.reflect.Field;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.misboh.constants.reportMis.MisBohStringConstant;
import com.choice.misboh.constants.reportMis.SupplyAcctMisBohConstants;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
@Controller
@RequestMapping("SupplyAcctMisBoh")
public class SupplyAcctMisBohController {

	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private DictColumnsService dictColumnsService;
	
	/**
	 * 仓储报表列表
	 * @return
	 */
	@RequestMapping("/toWareReport")
	public ModelAndView toWareReport(ModelMap modelMap, String checkMis, String checkFirm){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(SupplyAcctMisBohConstants.MISBOH_WARE_REPORT_LIST);
	}
	
	/**
	 * 保存现有显示字段
	 * @param modelMap
	 * @param dictColumns
	 * @param session
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/saveColumnsChoose")
	public ModelAndView saveColumnsChoose(ModelMap modelMap,DictColumns dictColumns,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());	
//		dictColumns.setLocale(session.getAttribute("locale").toString());
		dictColumnsService.saveColumnsChoose(dictColumns);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 恢复默认显示字段
	 * @param modelMap
	 * @param dictColumns
	 * @param session
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/recoverDefaultColumns")
	public ModelAndView recoverDefaultColumns(ModelMap modelMap,DictColumns dictColumns,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		dictColumnsService.deleteColumnsChoose(dictColumns);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/***
	 * 所有仓储报表公用的列选择页面
	 * @param modelMap
	 * @param session
	 * @param reportName
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/toColumnsChoose")
	public ModelAndView toColumnsChoose(ModelMap modelMap, HttpSession session, String reportName) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		dictColumns.setTableName(reportName);
		modelMap.put("objBean", "SupplyAcctMisBoh");
		modelMap.put("tableName",reportName);
		Class<MisBohStringConstant> c = MisBohStringConstant.class;
		Field f = c.getField("BASICINFO_REPORT_"+reportName.toUpperCase());
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,f.get(c).toString()));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(MisBohStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	
}
