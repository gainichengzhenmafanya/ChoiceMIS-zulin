package com.choice.misboh.web.controller.reportMis;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.constants.reportMis.CostItemMisBohConstants;
import com.choice.misboh.constants.reportMis.MisBohStringConstant;
import com.choice.misboh.service.reportMis.DancaiMaolilvMisBohService;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Acct;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.PositnService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
/**
 * 单菜毛利率
 * @author wjf
 *
 */
@Controller
@RequestMapping("DancaiMaolilvMisBoh")
public class DancaiMaolilvMisBohController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private DancaiMaolilvMisBohService dancaiMaolilvMisBohService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	private PositnService positnService;
	@Autowired
    private AcctService acctService;
	
	/********************************************单菜毛利率****************************************************/
	
	/**
	 * 跳转到单菜毛利率
	 * @return
	 */
	@RequestMapping("/toDancaiMaolilv")
	public ModelAndView toDancaiMaolilv(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", MisBohStringConstant.REPORT_NAME_DANCAIMAOLILV);
		return new ModelAndView(CostItemMisBohConstants.LIST_DANCAIMAOLILV,modelMap);
	}

	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findDancaiMaolilvHeaders")
	@ResponseBody
	public Object findDancaiMaolilvHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_DANCAIMAOLILV);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, MisBohStringConstant.BASICINFO_REPORT_DANCAIMAOLILV));
		return columns;
	}
	
	/**
	 * 查询分店菜品利润表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findDancaiMaolilv")
	@ResponseBody
	public Object findDancaiMaolilv(ModelMap modelMap,HttpSession session,String page,String rows,String sort,String order,SupplyAcct supplyAcct,String itcode,String itdes) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if(null != thePositn){
			supplyAcct.setFirm(thePositn.getCode());
		}else{
			return null;
		}
		if(null == supplyAcct.getPositn() || "".equals(supplyAcct.getPositn())){//如果没选仓位，默认查当前门店以及当前门店下的所有仓位
			List<Positn> positnList = new ArrayList<Positn>();
			positnList.add(thePositn);
			Positn positn = positnService.findPositnByCode(thePositn);
			if("Y".equals(positn.getYnUseDept())){
				Positn p = new Positn();
				p.setUcode(thePositn.getCode());
				positnList.addAll(positnService.findPositnSuperNOPage(p));
			}
			StringBuffer sb = new StringBuffer();
			for(Positn p : positnList){
				sb.append(p.getCode()+",");
			}
			supplyAcct.setPositn(sb.substring(0,sb.length()-1));
		}
		supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("itcode", itcode);
		condition.put("itdes", itdes);
		Acct acct = acctService.findAcctById(session.getAttribute("ChoiceAcct").toString());
		condition.put("acctDes", acct.getDes());
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return dancaiMaolilvMisBohService.findDancaiMaolilv(condition, pager);
	}
	
	/**
	 * 导出单菜毛利率
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportDancaiMaolilv")
	@ResponseBody
	public void exportDancaiMaolilv(HttpServletResponse response,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct,String itcode,String itdes) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "单菜毛利率";
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> conditions = new HashMap<String,Object>();
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null != thePositn){
			supplyAcct.setFirm(thePositn.getCode());
		}
		if(null == supplyAcct.getPositn() || "".equals(supplyAcct.getPositn())){//如果没选仓位，默认查当前门店以及当前门店下的所有仓位
			List<Positn> positnList = new ArrayList<Positn>();
			positnList.add(thePositn);
			Positn positn = positnService.findPositnByCode(thePositn);
			if("Y".equals(positn.getYnUseDept())){
				Positn p = new Positn();
				p.setUcode(thePositn.getCode());
				positnList.addAll(positnService.findPositnSuperNOPage(p));
			}
			StringBuffer sb = new StringBuffer();
			for(Positn p : positnList){
				sb.append(p.getCode()+",");
			}
			supplyAcct.setPositn(sb.substring(0,sb.length()-1));
		}
		supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		conditions.put("supplyAcct", supplyAcct);
		conditions.put("itcode", itcode);
		conditions.put("itdes", itdes);
		Acct acct = acctService.findAcctById(session.getAttribute("ChoiceAcct").toString());
		conditions.put("acctDes", acct.getDes());
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_DANCAIMAOLILV);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
	            + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), dancaiMaolilvMisBohService.findDancaiMaolilv(conditions, pager).getRows(), "单菜毛利率", dictColumnsService.listDictColumnsByAccount(dictColumns,MisBohStringConstant.BASICINFO_REPORT_DANCAIMAOLILV));
	}
	
	/**
	 * 打印单菜毛利率
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printDancaiMaolilv")
	public ModelAndView printDancaiMaolilv(ModelMap modelMap,Page pager,HttpSession session,String type,SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("sp_code",supplyAcct.getSp_code());
		params.put("grp",supplyAcct.getGrp());
		params.put("bdat", DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		params.put("edat", DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if(null != thePositn){
			supplyAcct.setFirm(thePositn.getCode());
			params.put("firm",thePositn.getCode());
		}
		if(null == supplyAcct.getPositn() || "".equals(supplyAcct.getPositn())){//如果没选仓位，默认查当前门店以及当前门店下的所有仓位
			List<Positn> positnList = new ArrayList<Positn>();
			positnList.add(thePositn);
			Positn positn = positnService.findPositnByCode(thePositn);
			if("Y".equals(positn.getYnUseDept())){
				Positn p = new Positn();
				p.setUcode(thePositn.getCode());
				positnList.addAll(positnService.findPositnSuperNOPage(p));
			}
			StringBuffer sb = new StringBuffer();
			for(Positn p : positnList){
				sb.append(p.getCode()+",");
			}
			supplyAcct.setPositn(sb.substring(0,sb.length()-1));
			params.put("positn",sb.substring(0,sb.length()-1));
		}
		supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		condition.put("supplyAcct", supplyAcct);
		Acct acct = acctService.findAcctById(session.getAttribute("ChoiceAcct").toString());
		condition.put("acctDes", acct.getDes());
		modelMap.put("List",dancaiMaolilvMisBohService.findDancaiMaolilv(condition, pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "分店项目销售利润分析");
	    parameters.put("bdat", supplyAcct.getBdat());
	    parameters.put("edat", supplyAcct.getEdat());
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
	    modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/DancaiMaolilvMisBoh/printDancaiMaolilv.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,CostItemMisBohConstants.REPORT_PRINT_URL_FIRMFOODPROFIT,CostItemMisBohConstants.REPORT_PRINT_URL_FIRMFOODPROFIT);//判断跳转路径
	    modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	
}
