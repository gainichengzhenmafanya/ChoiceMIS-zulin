package com.choice.misboh.web.controller.reportMis;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.excel.GenerateExcel;
import com.choice.misboh.commonutil.report.MISBOHPrintUrlConstant;
import com.choice.misboh.commonutil.report.MISBOHStringConstant;
import com.choice.misboh.commonutil.report.MISBOHUtils;
import com.choice.misboh.commonutil.report.ReadReportConstants;
import com.choice.misboh.commonutil.report.ReadReportUrl;
import com.choice.misboh.commonutil.report.ReportObject;
import com.choice.misboh.domain.reportMis.PublicEntity;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.scm.domain.Positn;

public class BaseController {

	@Autowired
	protected DictColumnsService dictColumnsService;
	@Autowired
	protected DictColumns dictColumns;
	@Autowired
	MISBOHUtils misbohUtils;
	
	
	public Positn getFirmPositn(HttpSession session){
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if(thePositn == null){
			return new Positn();	
		}else{
			return thePositn;
		}
	}
	
	/**
	 * 跳转到报表页面 
	 * @return
	 * @throws CRUDException
	 * @author WQQ
	 */
	@RequestMapping("/toReport")
	public ModelAndView toGroupSell(ModelMap modelMap,HttpSession session,PublicEntity publicEntity, String reportName) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("condition", publicEntity);
		return new ModelAndView(ReadReportConstants.getReportURL(MISBOHPrintUrlConstant.class, reportName),modelMap);
	}
	
	/**
	 * 保存现有显示字段
	 * @param modelMap
	 * @param dictColumns
	 * @param session
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/saveColumnsChoose")
	public ModelAndView saveColumnsChoose(ModelMap modelMap,DictColumns dictColumns,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());	
		dictColumnsService.saveColumnsChoose(dictColumns);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	/**
	 * 恢复默认显示字段
	 * @param modelMap
	 * @param dictColumns
	 * @param session
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/recoverDefaultColumns")
	public ModelAndView recoverDefaultColumns(ModelMap modelMap,DictColumns dictColumns,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumnsService.deleteColumnsChoose(dictColumns);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	/**
	 * 跳转到列选择页面
	 * @throws CRUDException
	 */
	@RequestMapping("/toColumnsChoose")
	public ModelAndView toColumnsChoose(ModelMap modelMap,HttpSession session,String reportName)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ReadReportConstants.getReportName(MISBOHStringConstant.class,reportName));
		String[] objBean = this.getClass().getAnnotation(RequestMapping.class).value();
		if(objBean.length >= 1)
			modelMap.put("objBean", objBean[0]);
		modelMap.put("tableName", ReadReportConstants.getReportName(MISBOHStringConstant.class,reportName));
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ReadReportConstants.getDefaultHeader(MISBOHStringConstant.class, reportName)));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(MISBOHStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findHeaders")
	@ResponseBody
	public Object getHeaders(HttpSession session,String reportName){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setLocale(session.getAttribute("locale").toString());
		dictColumns.setTableName(ReadReportConstants.getReportName(MISBOHStringConstant.class,reportName));
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ReadReportConstants.getDefaultHeader(MISBOHStringConstant.class, reportName)));
		columns.put("frozenColumns", ReadReportConstants.getFrozenHeader(MISBOHStringConstant.class,reportName));
		return columns;
	}
	/**
	 * 报表导出excel
	 * @param response
	 * @param request
	 * @param session
	 * @param condition
	 * @param reportName
	 * @throws CRUDException
	 */
	@RequestMapping("/exportReport")
	@ResponseBody
	public void exportReport(HttpServletResponse response,HttpServletRequest request,HttpSession session,PublicEntity condition,String reportName,String serviceName,String headers) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		 condition.setFirm(getFirmPositn(session).getCode());
		condition.getPager().setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ReadReportConstants.getReportName(MISBOHStringConstant.class, reportName));
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		ReportObject<Map<String,Object>> result = misbohUtils.execMethod(serviceName, reportName, condition);
		if(null != result.getFooter())
			result.getRows().addAll(result.getFooter());
		if(null == headers || "".equals(headers)){
				new GenerateExcel<Map<String, Object>>().creatWorkBook_DictColumns(ReadReportConstants.getReportNameCN(MISBOHStringConstant.class, reportName),request,response, result.getRows(),
						ReadReportConstants.getReportNameCN(MISBOHStringConstant.class, reportName),
						dictColumnsService.listDictColumnsByAccount(dictColumns,ReadReportConstants.getDefaultHeader(MISBOHStringConstant.class, reportName)),ReadReportConstants.getReportExcel(MISBOHStringConstant.class, reportName));
		}else{
				new GenerateExcel<Map<String, Object>>().creatWorkBook_DictColumns(ReadReportConstants.getReportNameCN(MISBOHStringConstant.class, reportName),request,response, result.getRows(),
						ReadReportConstants.getReportNameCN(MISBOHStringConstant.class, reportName),
						dictColumnsService.listDictColumnsByAccount(dictColumns,ReadReportConstants.getDefaultHeader(MISBOHStringConstant.class, reportName)),ReadReportConstants.getReportExcel(MISBOHStringConstant.class, reportName));
		}
	}
	/**
	 * 报表打印
	 * @param modelMap
	 * @param request
	 * @param session
	 * @param condition
	 * @param type
	 * @param reportName
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/printReport")
	public ModelAndView printReport(ModelMap modelMap,HttpServletRequest request,HttpSession session,PublicEntity condition,String type,String reportName,String serviceName) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        condition.setFirm(getFirmPositn(session).getCode());
		//设置分页，查询所有数据
		condition.getPager().setPageSize(Integer.MAX_VALUE);
	 	HashMap<String,Object>  parameters = new HashMap<String,Object>();
	    parameters.put("report_name", ReadReportConstants.getReportNameCN(MISBOHStringConstant.class, reportName));
	    Map<String,String> map = MISBOHUtils.convertToMap(condition);
	    map.put("reportName", reportName);
	    map.put("serviceName", serviceName);
	    modelMap.put("actionMap", map);
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	    if(null != condition.getBdat()){
	    	parameters.put("bdat",DateJudge.getDateByString(condition.getBdat()));
	    }
	    if(null != condition.getEdat()){
	    	parameters.put("edat", DateJudge.getDateByString(condition.getEdat()));
	    }
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", request.getServletPath());//传入回调路径
		//获取并执行查询方法，获取查询结果
		modelMap.put("List",misbohUtils.execMethod(serviceName, reportName, condition).getRows());
	 	Map<String,String> rs = ReadReportUrl.redReportUrl(type,ReadReportConstants.getIReportPrint(MISBOHPrintUrlConstant.class,reportName));//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
}
