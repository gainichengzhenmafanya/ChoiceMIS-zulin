package com.choice.misboh.web.controller.reportMis;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.excel.GenerateExcel;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.reportMis.JhDanjuHuizongMisService;
import com.choice.scm.util.ReadReportUrl;
@Controller
@RequestMapping("JhDanjuHuizongMisBoh")
public class JhDanjuHuizongMisBohController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private JhDanjuHuizongMisService JhDanjuHuizongMisService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private GenerateExcel<Map<String,Object>> exportExcelMap;
	
	/********************************************进货单据汇总报表****************************************************/
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/findStockBillSumHeaders")
	@ResponseBody	
	public Object getStockBillSumHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_STOCKBILLSUM);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_STOCKBILLSUM));
		columns.put("frozenColumns", ScmStringConstant.BASICINFO_REPORT_STOCKBILLSUM_FROZEN);
		return columns;
	}
	/**
	 * 跳转到进货单据汇总报表页面
	 * @param modelMap
	 * @return
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/toStockBillSum")
	public ModelAndView toStockBillSum(ModelMap modelMap,SupplyAcct supplyAcct){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("supplyAcct", supplyAcct);
		modelMap.put("reportName",ScmStringConstant.REPORT_NAME_STOCKBILLSUM);
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_STOCKBILLSUM_MIS,modelMap);
	}
	/**
	 * 查询进货单据货总内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param typ
	 * @param folio
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @author ZGL_ZANG
	 * @throws CRUDException 
	 */
	@RequestMapping("/findStockBillSum")
	@ResponseBody
	public Object findStockBillSum(ModelMap modelMap,HttpSession session,String page,String typ,String folio,String rows,String sort,String order,SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> content = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		String accountId=session.getAttribute("accountId").toString();
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null != thePositn){
			supplyAcct.setPositn(CodeHelper.replaceCode(thePositn.getCode()));
		}
		content.put("folio", folio);
		content.put("typ", typ);
		supplyAcct.setAccountId(accountId);
		content.put("supplyAcct",supplyAcct);
		String type = supplyAcct.getTyp();
		if(type==null || type==""){
			
		}else{
		type = "'"+type+"'";
		type = type.replace(",","','");
		supplyAcct.setTyp(type);
		}
		content.put("sort",sort);
		content.put("order", order);
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		return JhDanjuHuizongMisService.findStockBillSum(content, pager);
	}
	/**
	 * 进货单据汇总导出excel文件
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/exportStockBillSum")
	@ResponseBody
	public void exportStockBillSum(HttpServletResponse response,String sort,String order,String typ,String folio,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "进货单据汇总";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		String accountId=session.getAttribute("accountId").toString();
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null != thePositn){
			supplyAcct.setPositn(CodeHelper.replaceCode(thePositn.getCode()));
		}
		condition.put("folio", folio);
		condition.put("typ", typ);
		supplyAcct.setAccountId(accountId);
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_STOCKBILLSUM);
		dictColumns.setLocale(session.getAttribute("locale").toString());
//		exportExcelMap.creatWorkBook(response.getOutputStream(), JhDanjuHuizongMisService.findStockBillSum(condition,pager).getRows(), "进货单据汇总", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_STOCKBILLSUM));
		exportExcelMap.creatWorkBook_DictColumns(fileName, request,response,  JhDanjuHuizongMisService.findStockBillSum(condition,pager).getRows(), "进货单据汇总", dictColumnsService.listDictColumnsByTable(dictColumns),null);
	}
	/**
	 * 打印进货单据汇总
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @param delivertyp
	 * @param folio
	 * @return
	 * @throws CRUDException
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/printStockBillSum")
	public ModelAndView printStockBillSum(ModelMap modelMap,Page pager,HttpSession session,String type,SupplyAcct supplyAcct,String delivertyp,String folio)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		String accountId=session.getAttribute("accountId").toString();
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null != thePositn){
			supplyAcct.setPositn(CodeHelper.replaceCode(thePositn.getCode()));
		}
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		params.put("positn",supplyAcct.getPositn());
		if(supplyAcct.getEdat() != null)
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		params.put("chktyp",supplyAcct.getChktyp());
		params.put("delivercode",supplyAcct.getDelivercode());
		params.put("folio", folio);
		params.put("delivertyp", delivertyp);
		params.put("accountId", accountId);
		supplyAcct.setAccountId(accountId);
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",JhDanjuHuizongMisService.findStockBillSum(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "进货单据汇总");
	    modelMap.put("actionMap", params);
	    parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
	    parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/JhDanjuHuizongMis/printStockBillSum.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_STOCKBILLSUM,SupplyAcctConstants.REPORT_EXP_URL_STOCKBILLSUM);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	
	/**
	 * 跳转到列选择页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/toColChooseStockBillSum")
	public ModelAndView toColChooseStockBillSum(ModelMap modelMap, HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_STOCKBILLSUM);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", ScmStringConstant.REPORT_NAME_STOCKBILLSUM);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_STOCKBILLSUM));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
}
