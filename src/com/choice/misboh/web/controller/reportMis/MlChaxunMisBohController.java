package com.choice.misboh.web.controller.reportMis;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.misboh.domain.reportMis.PublicEntity;
import com.choice.misboh.service.reportMis.MlChaxunMisBohService;
@Controller
@RequestMapping("MlChaxunMisBoh")
public class MlChaxunMisBohController extends BaseController{

	@Autowired
	private MlChaxunMisBohService mlChaxunMisBohService;
	

	/****************************************************销售利润查询 start***************************************************/
	/**
	 * 毛利查询
	 */
	@RequestMapping("/findGrossProfit")
	@ResponseBody
	public Object findGrossProfit(ModelMap modelMap,HttpSession session,PublicEntity condition) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		condition.setFirm(getFirmPositn(session).getCode());
		return mlChaxunMisBohService.findCalForGrossProfit(condition);
	}
}
