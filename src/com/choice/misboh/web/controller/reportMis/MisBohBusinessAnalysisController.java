package com.choice.misboh.web.controller.reportMis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.misboh.commonutil.MisUtil;
import com.choice.misboh.commonutil.ReadReport;
import com.choice.misboh.commonutil.ReadReportUrl;
import com.choice.misboh.commonutil.report.ExportExcel;
import com.choice.misboh.constants.reportMis.BusinessAnalysisMisBohConstant;
import com.choice.misboh.constants.reportMis.MisStringConstant;
import com.choice.misboh.domain.BaseRecord.ActTyp;
import com.choice.misboh.domain.BaseRecord.Actm;
import com.choice.misboh.domain.BaseRecord.Payment;
import com.choice.misboh.domain.common.CommonMethod;
import com.choice.misboh.domain.reportMis.PublicEntity;
import com.choice.misboh.domain.reportMis.ReportObject;
import com.choice.misboh.service.BaseRecord.BaseRecordService;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.misboh.service.reportMis.MisBohBusinessAnalysisService;
import com.choice.orientationSys.util.Page;
import com.choice.tele.util.TeleUtils;

/**
 * 描述：营业分析下的报表
 * @author 马振
 * 创建时间：2015-4-16 下午12:36:45
 */
@Controller
@RequestMapping("MISBOHbusinessAnalysis")
public class MisBohBusinessAnalysisController extends MisBohBaseController {

	@Autowired
	private MisBohBusinessAnalysisService businessAnalysisService;
	
	@Autowired
	private BaseRecordService baseRecordService;

	@Autowired
	private CommonMISBOHService commonMISBOHService; 
	
	/**
	 * 描述：跳转到报表页面
	 * @author 马振
	 * 创建时间：2015-4-24 下午2:06:50
	 * @param modelMap
	 * @param reportName
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toReport")
	public ModelAndView toReport(ModelMap modelMap,String reportName, String vbcode, String bdat,String edat,HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("pk_store", businessAnalysisService.getPkStore(session));
		
		//若是调转到账单查询明细则传到账单号、开始结束日期
		if ("zdmxfx".equals(reportName)) {
			modelMap.put("vcode", vbcode);
			modelMap.put("bdat", bdat);
			modelMap.put("edat", edat);
		}
		
		//如果当日账单查询，需要查询活动和支付方式
		if("zdmxdetcur".equals(reportName)){
			List<Payment> listPayment = commonMISBOHService.findAllPayment();
			List<Actm> listActm = commonMISBOHService.findAllActm();//查询相关活动
			modelMap.put("listPayments", listPayment);
			modelMap.put("listActms", listActm);
		}
		
		//现金日报表传第长度标识
		if ("xjrreport".equals(reportName)) {
			modelMap.put("length", 0);
		}
		
		//若为支付活动明细查询，则查询当前门店下的班次
		if ("paydetail".equals(reportName)) {
			modelMap.put("listSft", commonMISBOHService.findAllShiftsftByStore(new CommonMethod(), session));
		}
		getRoleTiem(modelMap,session);
		return new ModelAndView(ReadReport.getReportURL(BusinessAnalysisMisBohConstant.class, reportName),modelMap);
	}
	
	/**
	 * 描述：报表导出
	 * @author 马振
	 * 创建时间：2015-4-24 下午2:07:00
	 * @param response
	 * @param request
	 * @param session
	 * @param publicEntity
	 * @param reportName
	 * @param headers
	 * @throws CRUDException
	 */
	@RequestMapping("/exportReport")
	public void exportReport(HttpServletResponse response,HttpServletRequest request,HttpSession session,PublicEntity publicEntity,String reportName,String headers) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		//设置分页，查询所有数据
		publicEntity.getPager().setPageSize(Integer.MAX_VALUE);
		publicEntity.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ReadReport.getReportName(MisStringConstant.class, reportName));
		
		//获取并执行查询方法，获取查询结果
		ReportObject<Map<String,Object>> result = MisUtil.execMethod(businessAnalysisService, reportName, publicEntity);
		if(null != result.getFooter())
			result.getRows().addAll(result.getFooter());
		if(null != headers || !"".equals(headers))
			ExportExcel.creatWorkBook(request,response,result.getRows(),ReadReport.getReportNameCN(MisStringConstant.class, reportName),null,headers);
		else
			ExportExcel.creatWorkBook(ReadReport.getReportNameCN(MisStringConstant.class, reportName),request,response, result.getRows(), ReadReport.getReportNameCN(MisStringConstant.class, reportName), dictColumnsService.listDictColumnsByAccount(dictColumns, ReadReport.getDefaultHeader(MisStringConstant.class, reportName)));
	}

	/**
	 * 描述：报表打印
	 * @author 马振
	 * 创建时间：2015-5-5 上午10:38:12
	 * @param modelMap
	 * @param request
	 * @param session
	 * @param condition
	 * @param type
	 * @param reportName
	 * @param start
	 * @param end
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/printReport")
	public ModelAndView printReport(ModelMap modelMap,HttpServletRequest request,HttpSession session,PublicEntity condition,String type,String reportName,String start,String end) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			condition.setFirmid(businessAnalysisService.getPkStore(session));
		
		//获取并执行查询方法，获取查询结果
		modelMap.put("List",MisUtil.execMethod(businessAnalysisService, reportName, condition).getRows());
	 	HashMap<String,Object>  parameters = new HashMap<String,Object>();
	    parameters.put("report_name", ReadReport.getReportNameCN(MisStringConstant.class, reportName));
	    Map<String,String> map = TeleUtils.convertToMap(condition);
	    map.remove("pager");
	    map.put("bdat", condition.getBdat());
	    map.put("edat", condition.getEdat());
	    map.put("start", start);
	    map.put("end", end);
	    map.put("reportName", reportName);
	    modelMap.put("actionMap", map);
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	    parameters.put("bdat", condition.getBdat());
	    parameters.put("edat", condition.getEdat());
	    parameters.put("start", start);
	    parameters.put("end", end);
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", request.getServletPath());//传入回调路径
	 	Map<String,String> rs = ReadReportUrl.redReportUrl(type,ReadReport.getIReportPrint(BusinessAnalysisMisBohConstant.class,reportName));//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
      		
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 描述：现金日报表
	 * @author 马振
	 * 创建时间：2015-4-16 下午1:45:38
	 * @param modelMap
	 * @param publicEntity
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
//	@RequestMapping("/queryXjrReport")
//	public ModelAndView queryXjrReport(ModelMap modelMap,PublicEntity publicEntity,HttpSession session) throws Exception {
//		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		publicEntity.setPk_store(businessAnalysisService.getPkStore(session));
//		modelMap.put("xjrReport", businessAnalysisService.queryXjrmx(publicEntity));
//		modelMap.put("cash", businessAnalysisService.findCash(publicEntity));
//		modelMap.put("length", businessAnalysisService.findCash(publicEntity).size());
//		modelMap.put("countCash", businessAnalysisService.findCountCashSaving(publicEntity));
//		modelMap.put("entity",publicEntity);
//		modelMap.put("pk_store",publicEntity.getPk_store());
//		return new ModelAndView(ReadReport.getPageURL(BusinessAnalysisMisBohConstant.class, "REPORT_SHOW_XJRREPORT"), modelMap);
//	}
	
	/**
	 * 描述:现金日报表
	 * 作者:马振
	 * 时间:2016年8月19日上午9:51:45
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryXjrReport")
	@ResponseBody
	public Object queryXjrReport(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		condition.setPk_store(businessAnalysisService.getPkStore(session));
		return this.businessAnalysisService.queryXjrReport_new(condition);
	}
	
	/**
	 * 描述：账单查询
	 * @author 马振
	 * 创建时间：2015-4-22 上午10:42:19
	 * @param modelMap
	 * @param publicEntity
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryFolioDetailInfo")
	@ResponseBody
	public Object queryFolioDetailInfo(ModelMap modelMap,PublicEntity publicEntity,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		publicEntity.setPk_store(businessAnalysisService.getPkStore(session));
		return businessAnalysisService.queryFolioDetailInfo(publicEntity);
	}
	
	/**
	 * 描述：账单查询表头
	 * @author 马振
	 * 创建时间：2016-3-1 下午4:21:37
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findHeaderForZdmx")
	@ResponseBody
	public Object findHeaderForBusinessAnalysisXwy() throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("actTyp", commonMISBOHService.findAllActTyp());
		map.put("payment", commonMISBOHService.findAllPayment());
		map.put("actm", commonMISBOHService.findAllActm());
		return map;
	}
	
	/**
	 * 描述：当日查询—账单查询
	 * @author 马振
	 * 创建时间：2016-3-1 下午2:14:41
	 * @param modelMap
	 * @param publicEntity
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryFolioDetailInfoCur")
	@ResponseBody
	public Object queryFolioDetailInfoCur(ModelMap modelMap,PublicEntity publicEntity,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		publicEntity.setPk_store(businessAnalysisService.getPkStore(session));
		return businessAnalysisService.queryFolioDetailInfoCur(publicEntity);
	}
	
	/**
	 * 描述：賬單明細分析
	 * @author 马振
	 * 创建时间：2015-4-22 下午3:54:22
	 * @param modelMap
	 * @param publicEntity
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryZdMxfx")
	@ResponseBody
	public Object queryZdMxfx(ModelMap modelMap,PublicEntity publicEntity,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		publicEntity.setPk_store(businessAnalysisService.getPkStore(session));
		return businessAnalysisService.queryZdMxfx(publicEntity);
	}
	
	/**
	 * 描述：查询账单支付明细
	 * @author 马振
	 * 创建时间：2015-8-19 下午3:14:55
	 * @param modelMap
	 * @param publicEntity
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryZdMxfx_pay")
	@ResponseBody
	public Object queryZdMxfx_pay(ModelMap modelMap,PublicEntity publicEntity,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		publicEntity.setPk_store(businessAnalysisService.getPkStore(session));
		return businessAnalysisService.queryZdMxfx_pay(publicEntity);
	}
	
	/**
	 * 描述：营运报告
	 * @author 马振
	 * 创建时间：2015-4-23 上午9:56:10
	 * @param modelMap
	 * @param publicEntity
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryYybg")
	@ResponseBody
	public Object queryYybg(ModelMap modelMap,PublicEntity publicEntity,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		publicEntity.setPk_store(businessAnalysisService.getPkStore(session));
		return businessAnalysisService.queryyybg(publicEntity);
	}
	
	/**
	 * 描述：当日查询——营运报告
	 * @author 马振
	 * 创建时间：2016-3-3 下午1:28:13
	 * @param modelMap
	 * @param publicEntity
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryYybgCur")
	@ResponseBody
	public Object queryYybgCur(ModelMap modelMap,PublicEntity publicEntity,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		publicEntity.setPk_store(businessAnalysisService.getPkStore(session));
		return businessAnalysisService.queryYybgCur(publicEntity);
	}
	
	/**
	 * 描述：营业日明细
	 * @author 马振
	 * 创建时间：2015-4-23 上午10:54:11
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/queryYingYeRibao")
	public ModelAndView queryYingYeRibao(ModelMap modelMap,PublicEntity condition,HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		PublicEntity cc = new PublicEntity();
		cc.setPk_store(businessAnalysisService.getPkStore(session));
		cc.setFirmdes(condition.getFirmdes());
		cc.setBdat(condition.getBdat());
		cc.setEdat(condition.getEdat());
		cc.setVcode(condition.getVcode());
		modelMap.put("searchCondition", cc);
		condition.setAccountId(session.getAttribute("accountId").toString());
		if(!StringUtils.isNotBlank(condition.getPk_store())){
			condition.setPk_store(businessAnalysisService.getPkStore(session));
		}
		
		//金额查询
		Map<String,Object> xsMap = businessAnalysisService.queryYingYeRibao(condition);
		modelMap.put("xsMap", xsMap);
		
		//退单统计
		modelMap.put("tdMap", businessAnalysisService.queryYingYeRibaoTd(condition));
		
		//时段统计
		List<Map<String,Object>> ccMap = businessAnalysisService.queryYingYeRibaoCanci2(condition);
		modelMap.put("ccMap", ccMap);
		
		//堂食、外送等方式统计
		Map<String,Object> wdMap = businessAnalysisService.queryYingYeRibaoWd(condition);
		modelMap.put("wdMap", wdMap);
		
		//菜品类别(中类)统计及其合计
		Map<String, Object> lbList = businessAnalysisService.queryYingYeRibaoLb(condition);
		modelMap.put("lbList", lbList.get("lbList"));
		modelMap.put("lbSumMap", lbList.get("lbSumMap"));
		
		//活动统计
		Map<String, Object> hdList = businessAnalysisService.queryYingYeRibaoHd(condition);
		modelMap.put("hdList", hdList.get("hdList"));
		modelMap.put("hdSumMap", hdList.get("hdSumMap"));
		
		//支付类型统计
		Map<String, Object> jsfsList = businessAnalysisService.queryYingYeRibaoZflx(condition);
		modelMap.put("jsfsList", jsfsList.get("jsfsList"));
		modelMap.put("jsfsSumMap", jsfsList.get("jsfsSumMap"));
		
		//结算方式统计
		Map<String, Object> xfList = businessAnalysisService.queryYingYeRibaoJsfs(condition);
		modelMap.put("xfList", xfList.get("tdMapList"));
		modelMap.put("tdSumMap", xfList.get("tdSumMap"));
		getRoleTiem(modelMap,session);
		return new ModelAndView(ReadReport.getPageURL(BusinessAnalysisMisBohConstant.class, "REPORT_SHOW_YINGYERIBAO"), modelMap);
	}
	
	/**
	 * 描述：当日查询——营业日明细
	 * @author 马振
	 * 创建时间：2016-3-4 上午11:10:48
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/queryYingYeRibaoCur")
	public ModelAndView queryYingYeRibaoCur(ModelMap modelMap,PublicEntity condition,HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(!StringUtils.isNotBlank(condition.getPk_store())){
			condition.setPk_store(businessAnalysisService.getPkStore(session));
		}
		
		modelMap.put("searchCondition", condition);
		
		//金额查询
		Map<String,Object> xsMap = businessAnalysisService.queryYingYeRibaoCur(condition);
		modelMap.put("xsMap", xsMap);

		//退单统计
		modelMap.put("tdMap", businessAnalysisService.queryYingYeRibaoTdCur(condition));
		
		//餐次统计
		List<Map<String,Object>> ccMap = businessAnalysisService.queryYingYeRibaoCanciCur(condition);
		modelMap.put("ccMap", ccMap);
		
		//外带统计
		Map<String,Object> wdMap = businessAnalysisService.queryYingYeRibaoWdCur(condition);
		modelMap.put("wdMap", wdMap);
		
		//类别统计及其合计
		Map<String, Object> lbList = businessAnalysisService.queryYingYeRibaoLbCur(condition);
		modelMap.put("lbList", lbList.get("lbList"));
		modelMap.put("lbSumMap", lbList.get("lbSumMap"));
		
		//活动统计
		Map<String, Object> hdList = businessAnalysisService.queryYingYeRibaoHdCur(condition);
		modelMap.put("hdList", hdList.get("hdList"));
		modelMap.put("hdSumMap", hdList.get("hdSumMap"));
		
		//应收
		Map<String, Object> jsfsList = businessAnalysisService.queryYingYeRibaoZffsYs(condition);
		modelMap.put("jsfsList", jsfsList.get("jsfsList"));
		modelMap.put("jsfsSumMap", jsfsList.get("jsfsSumMap"));
		
//		//实收
		Map<String, Object> xfList = businessAnalysisService.queryYingYeRibaoZffsSs(condition);
		modelMap.put("xfList", xfList.get("tdMapList"));
		modelMap.put("tdSumMap", xfList.get("tdSumMap"));
		getRoleTiem(modelMap,session);
		return new ModelAndView(ReadReport.getPageURL(BusinessAnalysisMisBohConstant.class, "REPORT_SHOW_YINGYERIBAOCUR"), modelMap);
	}
	
	/**
	 * 描述：收银员统计
	 * @author 马振
	 * 创建时间：2015-4-24 下午1:51:38
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryCashierStatistics")
	@ResponseBody
	public Object queryCashierStatistics(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		condition.setPk_store(businessAnalysisService.getPkStore(session));
		return businessAnalysisService.queryCashierStatistics(condition);
	}
	
	/**
	 * 描述：跳转到台位活动明细报表
	 * @author 马振
	 * 创建时间：2015-7-24 下午3:05:28
	 * @param modelMap
	 * @param reportName
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toReportByTbl")
	public ModelAndView toReportByTbl(ModelMap modelMap,String reportName,HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("condition", new PublicEntity());
		Page page=new Page();
		page.setPageSize(Integer.MAX_VALUE);
		List<ActTyp> acttypList = baseRecordService.findAllActTyp();
		modelMap.put("acttypList", acttypList);//大类
		List<PublicEntity> acttypminList = baseRecordService.findAllActTypMin();
		modelMap.put("acttypminList", acttypminList);//活动小类);
		getRoleTiem(modelMap,session);
		return new ModelAndView(BusinessAnalysisMisBohConstant.REPORT_SHOW_ACTMBYTBL,modelMap);
	}
	
	/**
	 * 描述：台位活动明细报表
	 * @author 马振
	 * 创建时间：2015-7-24 下午3:05:40
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryActmByTbl")
	@ResponseBody
	public Object queryActmByTbl(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		condition.setPk_store(businessAnalysisService.getPkStore(session));
		return businessAnalysisService.findActmByTbl(condition);
	}
	
	/**
	 * 描述： 支付方式明细及其合计
	 * @author 马振
	 * 创建时间：2016年4月19日下午7:59:26
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryPayDetail")
	@ResponseBody
	public Object queryPayDetail(ModelMap modelMap, PublicEntity condition,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return businessAnalysisService.queryPayDetail(condition);
	}
}