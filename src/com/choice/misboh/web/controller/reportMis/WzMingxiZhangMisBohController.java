package com.choice.misboh.web.controller.reportMis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.excel.GenerateExcel;
import com.choice.misboh.constants.reportMis.MisBohStringConstant;
import com.choice.misboh.constants.reportMis.SupplyAcctMisBohConstants;
import com.choice.misboh.service.reportMis.WzMingxiZhangMisBohService;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.PositnService;
import com.choice.scm.util.ReadReportUrl;
@Controller
@RequestMapping("WzMingxiZhangMisBoh")
public class WzMingxiZhangMisBohController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private WzMingxiZhangMisBohService WzMingxiZhangMisBohService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private GenerateExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	private PositnService positnService;
	
	/********************************************物资明细帐报表****************************************************/
	/**
	 * 跳转到物资明细帐页面
	 * @param modelMap
	 * @return
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/toSupplyDetailsInfo")
	public ModelAndView toSupplyDetailsInfo(ModelMap modelMap,SupplyAcct sa,String withdat) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName",MisBohStringConstant.REPORT_NAME_SUPPLYDETAILSINFO);
		modelMap.put("supplyAcct",sa);
		modelMap.put("withdat",withdat);
		return new ModelAndView(SupplyAcctMisBohConstants.REPORT_SHOW_SUPPLYDETAILSINFO_MIS,modelMap);
	}
	
	/**
	 * 查询表头
	 * @param session
	 * @return
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/findSupplyDetailsInfoHeaders")
	@ResponseBody
	public Object getSupplyDetailsInfoHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_SUPPLYDETAILSINFO);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, MisBohStringConstant.BASICINFO_REPORT_SUPPLYDETAILSINFO));
		columns.put("frozenColumns", MisBohStringConstant.BASICINFO_REPORT_SUPPLYDETAILSINFO_FROZEN);
		return columns;
	}
	/**
	 * 查询物资明细账列表
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param typ
	 * @param bz
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/findSupplyDetailsInfo")
	@ResponseBody
	public Object findSupplyDetailsInfo(ModelMap modelMap, HttpSession session, String page, String withdat,
			String rows, String sort, String order, SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> content = new HashMap<String,Object>();
		setDept(session,supplyAcct);//设置部门
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());

		content.put("withdat", withdat);
		content.put("supplyAcct",supplyAcct);
		content.put("sort",sort);
		content.put("order", order);
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		return WzMingxiZhangMisBohService.findSupplyDetailsInfo(content, pager);
	}
	
	/**
	 * 导出物资明细账excel文件
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportSupplyDetailsInfo")
	@ResponseBody
	public void exportSupplyDetailsInfo(HttpServletResponse response, String sort, String order, String withdat,
			HttpServletRequest request, HttpSession session, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "库存物资明细分类账";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		setDept(session,supplyAcct);//设置部门
		condition.put("withdat", withdat);
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(MisBohStringConstant.REPORT_NAME_SUPPLYDETAILSINFO);
//		exportExcelMap.creatWorkBook(response.getOutputStream(), WzMingxiZhangMisBohService.findSupplyDetailsInfo(condition,pager).getRows(), "库存物资明细分类账", dictColumnsService.listDictColumnsByAccount(dictColumns, MisBohStringConstant.BASICINFO_REPORT_SUPPLYDETAILSINFO));
		exportExcelMap.creatWorkBook_DictColumns(fileName, request, response, WzMingxiZhangMisBohService.findSupplyDetailsInfo(condition,pager).getRows(), "库存物资明细分类账", dictColumnsService.listDictColumnsByAccount(dictColumns, MisBohStringConstant.BASICINFO_REPORT_SUPPLYDETAILSINFO), MisBohStringConstant.EXCEL_SUPPLYDETAILSINFO);
		
	}
	
	/**
	 * 打印物资明细账
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @param delivertyp
	 * @param folio
	 * @return
	 * @throws CRUDException
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/printSupplyDetailsInfo")
	public ModelAndView printSupplyDetailsInfo(ModelMap modelMap, Page pager, HttpSession session,
			SupplyAcct supplyAcct, String type, String withdat)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		if(supplyAcct.getEdat() != null)
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		params.put("positn",supplyAcct.getPositn());
		params.put("positndes",supplyAcct.getPositndes());
		params.put("sp_code",supplyAcct.getSp_code());
		params.put("sp_name",supplyAcct.getSp_name());
		params.put("sp_desc",supplyAcct.getSp_desc());
		params.put("unit",supplyAcct.getUnit());
		params.put("chktyp",supplyAcct.getChktyp());
		setDept(session,supplyAcct);//设置部门
		condition.put("supplyAcct", supplyAcct);
		condition.put("withdat", withdat);
		modelMap.put("List",WzMingxiZhangMisBohService.findSupplyDetailsInfo(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "库存物资明细分类账");
	    modelMap.put("actionMap", params);
	    parameters.put("positn",supplyAcct.getPositndes());
		parameters.put("sp_code",supplyAcct.getSp_code());
		parameters.put("sp_name",supplyAcct.getSp_name());
		parameters.put("sp_desc",supplyAcct.getSp_desc());
		parameters.put("unit",supplyAcct.getUnit());
		parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/WzMingxiZhangMisBoh/printSupplyDetailsInfo.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctMisBohConstants.REPORT_PRINT_URL_SUPPLYDETAILSINFO,SupplyAcctMisBohConstants.REPORT_EXP_URL_SUPPLYDETAILSINFO);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	
	/***
	 * 查询部门的方法
	 * @param session
	 * @param supplyAcct
	 * @throws CRUDException
	 */
	private void setDept(HttpSession session, SupplyAcct supplyAcct) throws CRUDException{
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(supplyAcct.getPositn()==null || "".equals(supplyAcct.getPositn())){//选择档口为空时，默认查询该分店的入库明细
			if(null != thePositn){
				//查询该分店的档口
				Positn positn = new Positn();
				positn.setUcode(thePositn.getCode());
				List<Positn> list = positnService.findPositnSuperNOPage(positn);
				String positncode = "";
				for(Positn posi:list){
					positncode +=CodeHelper.replaceCode(posi.getCode())+",";
				}
				positncode+=CodeHelper.replaceCode(thePositn.getCode());
				supplyAcct.setPositn(positncode);
			}
		}else{
			supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		}
	}
}
