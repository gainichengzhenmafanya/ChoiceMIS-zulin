package com.choice.misboh.web.controller.reportMis;

import java.awt.print.PrinterJob;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.print.PrintService;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.engine.export.JRPrintServiceExporterParameter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.common.servlet.Jdbconfig;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.constants.reportMis.SupplyAcctMisBohConstants;
import com.choice.misboh.domain.reportMis.CustomReportMisBoh;
import com.choice.misboh.domain.reportMis.PublicEntity;
import com.choice.misboh.service.reportMis.CustomReportMisBohService;
import com.choice.scm.domain.Positn;
@Controller
@RequestMapping("customReportMisBoh")
public class CustomReportMisBohController {
	
	@Autowired
	private CustomReportMisBohService customReportMisBohService;
	@Autowired
	private Jdbconfig config;
	
	/**
	 * 查询报表树
	 * @param modelMap
	 * @param store
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/customReportTree")
	public ModelAndView storeTree(ModelMap modelMap) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<Map<String,Object>> listCustomReportTree = customReportMisBohService.listCustomReportTree();
		modelMap.put("listCustomReportTree", listCustomReportTree);
		return new ModelAndView(SupplyAcctMisBohConstants.CUSTOMREPORTTREE, modelMap);
	}
	/**
	 * 描述：添加报表节点
	 * @param modelMap
	 * @return
	 * @throws CRUDException
	 * @author spt
	 * 日期：2014-5-30
	 */
	@RequestMapping(value="add")
	public ModelAndView add(ModelMap modelMap,String type,String parentid) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
		if(type.equals("2")){//2级菜单
			modelMap.put("parentid", parentid);
			return new ModelAndView(SupplyAcctMisBohConstants.ADD_CUSTOMREPORT,modelMap);
		}else{//一级菜单
			modelMap.put("parentid", "00000000000000000000000000000000");
			return new ModelAndView(SupplyAcctMisBohConstants.ADD_FIRSTMENU,modelMap);
		}
	}
	/**
	 * 描述：保存报表节点
	 * @param modelMap
	 * @param customReport
	 * @return
	 * @throws Exception
	 * author:spt
	 * 日期：2014-5-30
	 */
	@RequestMapping(value="saveCustomReport")
	public ModelAndView saveCustomReport(ModelMap modelMap,CustomReportMisBoh customReport) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
		//保存操作
		customReportMisBohService.saveCustomReport(customReport);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 描述：删除自定义节点
	 * @param modelMap
	 * @param customReport
	 * @return
	 * @throws Exception
	 * author:spt
	 * 日期：2014-5-30
	 */
	@RequestMapping(value="deleteCustomReport")
	public ModelAndView deleteCustomReport(ModelMap modelMap,String pk_customReport) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
		customReportMisBohService.deleteCustomReport(pk_customReport);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	/**
	 * 描述：导向修改页面
	 * @param modelMap
	 * @param type
	 * @param parentid
	 * @return
	 * @throws CRUDException
	 * author:spt
	 * 日期：2014-6-6
	 */
	@RequestMapping(value="edit")
	public ModelAndView edit(ModelMap modelMap,String type,String pk_customReport) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
		//根据pk_customReport，查询出相应的单挑内容
		CustomReportMisBoh customReport=customReportMisBohService.findCustomReport(pk_customReport);
		modelMap.put("customReport", customReport);
		if(type.equals("2")){//2级菜单
			return new ModelAndView(SupplyAcctMisBohConstants.UPDATE_CUSTOMREPORT,modelMap);
		}else{//一级菜单
			return new ModelAndView(SupplyAcctMisBohConstants.UPDATE_FIRSTMENU,modelMap);
		}
	}
	
	/**
	 * 描述：节点修改
	 * @param modelMap
	 * @param customReport
	 * @return
	 * @throws Exception
	 * author:spt
	 * 日期：2014-6-6
	 */
	@RequestMapping(value="updateCustomReport")
	public ModelAndView updateCustomReport(ModelMap modelMap,CustomReportMisBoh customReport) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
		customReportMisBohService.updateCustomReport(customReport);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 描述：弹出查询条件窗体
	 * @param modelMap
	 * @return
	 * @throws CRUDException
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	@RequestMapping(value="findReport")
	@ResponseBody
	public Object findReport(HttpSession session, String pk_customReport) throws CRUDException, IllegalArgumentException, IllegalAccessException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
		CustomReportMisBoh customReport=customReportMisBohService.searchReport(pk_customReport);
		//通过反射得到启用的条件
		@SuppressWarnings("unchecked")
		Class<CustomReportMisBoh> custClass = (Class<CustomReportMisBoh>) customReport.getClass();
		List<String> flags = new ArrayList<String>();
		Field[] fs = custClass.getDeclaredFields();
		for(int i = 0 ; i < fs.length; i++){
			Field f = fs[i];
			f.setAccessible(true); //设置些属性是可以访问的
			Object val = f.get(customReport);//得到此属性的值  
			if(null != val && "on".equals(val.toString())){
				flags.add(f.getName());
			}
		}
		customReport.setFlags(flags);
		//设置默认值
		customReport.setStartDateFlag(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		customReport.setPositnCode(thePositn.getCode());
		customReport.setPositnDes(thePositn.getDes());
		return customReport;
	}
	
//	/**
//	 * 描述：弹出查询条件窗体
//	 * @param modelMap
//	 * @return
//	 * @throws CRUDException
//	 * author:spt
//	 * 日期：2014-5-30
//	 */
//	@RequestMapping(value="searchReport")
//	public ModelAndView searchReport(ModelMap modelMap,String pk_customReport) throws CRUDException{
//		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
//		CustomReportMisBoh customReport=customReportMisBohService.searchReport(pk_customReport);
//		modelMap.put("customReport", customReport);
//		return new ModelAndView(SupplyAcctMisBohConstants.SEARCH_CUSTOMREPORT,modelMap);
//	}
	/**
	 * 跳转到报表页面
	 * @return
	 * @throws CRUDException
	 * @author ZGL
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws JRException 
	 * @throws IOException 
	 */
	@RequestMapping("/toReport")
	@ResponseBody
	public Object toReport(HttpSession session, ModelMap modelMap,PublicEntity publicEntity,HttpServletResponse response,HttpServletRequest request) throws CRUDException, JRException, ClassNotFoundException, SQLException, IOException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String, Object> params = setCondition(session, publicEntity);
		params.put("reportName",URLDecoder.decode(publicEntity.getVname(), "UTF-8"));
		String filePath = request.getSession().getServletContext().getRealPath("/")+ "report/misboh/customReport/"+publicEntity.getReportPath();//jasper文件路径;
		InputStream inputStream = new FileInputStream(filePath+".jasper");  
		JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, params, getConnection());  
	    JasperExportManager.exportReportToHtmlFile(jasperPrint, filePath+".html");  
	    return filePath+".html";
	}
	private Connection getConnection() throws ClassNotFoundException, SQLException {  
        Connection conn = config.getCurrentConnetion();  
        return conn;  
    }
	
	/**
	 * 设置条件
	 * @param session
	 * @param publicEntity
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private Map<String, Object> setCondition(HttpSession session, PublicEntity publicEntity) throws UnsupportedEncodingException{
		Map<String, Object> params = new HashMap<String,Object>();
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if(publicEntity.getWeek() != null){//根据日期或得所在周一和周日
			Calendar cal = Calendar.getInstance();  
		    cal.setTime(publicEntity.getWeek());
		    //判断要计算的日期是否是周日，如果是则减一天计算周六的，否则会出问题，计算到下一周去了  
		    int dayWeek = cal.get(Calendar.DAY_OF_WEEK);//获得当前日期是一个星期的第几天  
		    if(1 == dayWeek) {  
		        cal.add(Calendar.DAY_OF_MONTH, -1);  
		    }
		    cal.setFirstDayOfWeek(Calendar.MONDAY);//设置一个星期的第一天，按中国的习惯一个星期的第一天是星期一  
		    int day = cal.get(Calendar.DAY_OF_WEEK);//获得当前日期是一个星期的第几天  
		    cal.add(Calendar.DATE, cal.getFirstDayOfWeek()-day);//根据日历的规则，给当前日期减去星期几与一个星期第一天的差值   
		    publicEntity.setBdat(DateFormat.getStringByDate(cal.getTime(), "yyyy-MM-dd"));
		    cal.add(Calendar.DATE, 6);
		    publicEntity.setEdat(DateFormat.getStringByDate(cal.getTime(), "yyyy-MM-dd"));
		}
		if(publicEntity.getBdat() != null)
			params.put("bdat",DateFormat.getDateByString(publicEntity.getBdat(),"yyyy-MM-dd"));
		if(publicEntity.getEdat() != null)
			params.put("edat",DateFormat.getDateByString(publicEntity.getEdat(),"yyyy-MM-dd"));
		StringBuffer sb = new StringBuffer();
		if(null != publicEntity.getPositn() && !"".equals(publicEntity.getPositn()))
			sb.append(" and sa.positn in ("+CodeHelper.replaceCode(publicEntity.getPositn())+") ");
		else
			sb.append(" and sa.positn in ("+CodeHelper.replaceCode(thePositn.getCode())+") ");
		if(null != publicEntity.getSp_code() && !"".equals(publicEntity.getSp_code()))
			sb.append(" and sa.sp_code in ("+CodeHelper.replaceCode(publicEntity.getSp_code())+") ");
		if(null != publicEntity.getTyp() && !"".equals(publicEntity.getTyp()))
			sb.append(" and sa.typ in "+publicEntity.getTyp()+" ");
		params.put("sql",sb.toString());
		if(publicEntity.getBdat() != null && publicEntity.getEdat() != null){
			StringBuffer inweekSql = new StringBuffer();
			StringBuffer outweekSql = new StringBuffer();
			Date bdat = DateFormat.getDateByString(publicEntity.getBdat(), "yyyy-MM-dd");
			Date edat = DateFormat.getDateByString(publicEntity.getEdat(), "yyyy-MM-dd");
			long day = (edat.getTime() - bdat.getTime())/(3600 * 24 * 1000);
			for (long i = 0; i <= day; i++) {
				String week = DateFormat.getStringByDate(new Date(bdat.getTime() + i*(3600 * 24 * 1000)), "yyyy-MM-dd");
				inweekSql.append("SUM(CASE WHEN SA.DAT = to_date('"+week+"','YYYY-MM-DD') THEN SA.CNTIN ELSE 0 END) AS WEEKCNT"+(i+1)+","+
						"SUM(CASE WHEN SA.DAT = to_date('"+week+"','YYYY-MM-DD') THEN SA.AMTIN ELSE 0 END) AS WEEKAMT"+(i+1)+",");
				outweekSql.append("SUM(CASE WHEN SA.DAT = to_date('"+week+"','YYYY-MM-DD') THEN SA.CNTOUT ELSE 0 END) AS WEEKCNT"+(i+1)+","+
						"SUM(CASE WHEN SA.DAT = to_date('"+week+"','YYYY-MM-DD') THEN SA.AMTOUT ELSE 0 END) AS WEEKAMT"+(i+1)+",");
			}
			params.put("weeksql", inweekSql.toString());
			params.put("outweeksql", outweekSql.toString());
		}
		return params;
	}
	
	/**
	 * 导出
	 * @return
	 * @throws CRUDException
	 * @author ygb
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws JRException 
	 * @throws IOException 
	 * @throws ServletException 
	 */
	@RequestMapping("/exportFile")
	@ResponseBody
	public void exportFile(HttpSession session,ModelMap modelMap,PublicEntity publicEntity,HttpServletResponse response,String type,HttpServletRequest request) throws CRUDException, JRException, ClassNotFoundException, SQLException, IOException, ServletException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String, Object> params = setCondition(session, publicEntity);
		params.put("reportName",publicEntity.getVname());
		String filePath = request.getSession().getServletContext().getRealPath("/")+ "report/misboh/customReport/"+publicEntity.getReportPath();//jasper文件路径;
		
        JRExporter exporter = null; 
        String contentType = "";
        String last = ".xls\"";
        if("excel".equals(type)){
        	exporter = new JExcelApiExporter(); 
        	contentType = "application/vnd.ms-excel"; 
        }else if("pdf".equals(type)){
        	exporter = new JRPdfExporter(); 
        	contentType = "application/pdf"; 
        	last = ".pdf\"";
        }else if("word".equals(type)){
        	exporter = new JRRtfExporter();
        	contentType = "application/msword"; 
        	last = ".doc\"";
        }
        
        response.setContentType(contentType);
        response.setCharacterEncoding("UTF-8");  
        response.setHeader("Content-Disposition", "attachment; filename=\""  
                + URLEncoder.encode(publicEntity.getVname(), "UTF-8") + last); 
        //得到jasper文件
        File jasperFile=new File(filePath+".jasper");
        @SuppressWarnings("deprecation")
		JasperReport jasperReport= (JasperReport)JRLoader.loadObject(jasperFile.getPath());
        JasperPrint jasperPrint=JasperFillManager.fillReport(jasperReport,params, getConnection());
        exporter.setParameter(JRExporterParameter.JASPER_PRINT,  jasperPrint); 
        OutputStream ouputStream = response.getOutputStream();  
        exporter.setParameter(JRExporterParameter.IGNORE_PAGE_MARGINS, Boolean.TRUE);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, ouputStream);  
        try {  
            exporter.exportReport();  
        } catch (JRException e) {  
            throw new ServletException(e);  
        } finally {  
            if (ouputStream != null) {  
                try {  
                    ouputStream.close();  
                } catch (IOException ex) {  
                }  
            }  
        }  
    } 
	
	/**
	 * 打印
	 * @return
	 * @throws CRUDException
	 * @author ygb
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws JRException 
	 * @throws IOException 
	 * @throws ServletException 
	 */
	@RequestMapping("/printReport")
	@ResponseBody
	public void printReport(HttpSession session,ModelMap modelMap,PublicEntity publicEntity,HttpServletResponse response,HttpServletRequest request) throws CRUDException, JRException, ClassNotFoundException, SQLException, IOException, ServletException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String, Object> params = setCondition(session, publicEntity);
		params.put("reportName",publicEntity.getVname());
		String filePath = request.getSession().getServletContext().getRealPath("/")+ "report/misboh/customReport/"+publicEntity.getReportPath();//jasper文件路径;
		
        //得到jasper文件
        File jasperFile=new File(filePath+".jasper");
        @SuppressWarnings("deprecation")
		JasperReport jasperReport= (JasperReport)JRLoader.loadObject(jasperFile.getPath());
        JasperPrint jasperPrint=JasperFillManager.fillReport(jasperReport,params, getConnection());
        PrintService[] pss = PrinterJob.lookupPrintServices();
		PrintService printService = null;
		for (int i = 0; i < pss.length; i++) {
//			String sps = pss[i].toString();
			// 如果打印机名称相同
//			if (sps.equalsIgnoreCase("Win32 Printer : " + printIp)) {
				printService = pss[0];
//			}
		}
		JRAbstractExporter je = new JRPrintServiceExporter();
		je.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		// 设置指定打印机
		je.setParameter(JRPrintServiceExporterParameter.PRINT_SERVICE,
				printService);
		je.setParameter(
				JRPrintServiceExporterParameter.DISPLAY_PAGE_DIALOG, false);
		je.setParameter(
				JRPrintServiceExporterParameter.DISPLAY_PRINT_DIALOG, false);
		// 打印
		je.exportReport();
    } 

}
