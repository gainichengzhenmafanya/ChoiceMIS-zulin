package com.choice.misboh.web.controller.costReduction;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.misboh.constants.costReduction.CostReductionConstants;
import com.choice.misboh.domain.costReduction.CostItem;
import com.choice.misboh.service.costReduction.CostReductionService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;
import com.choice.scm.service.PositnService;

/**
 * 描述：成本核减
 * @author 马振
 * 创建时间：2015-4-14 上午9:44:53
 */
@Controller
@RequestMapping(value = "costReduction")
public class CostReductionController {

	@Autowired
	private CostReductionService costReductionService;
	
	@Autowired
	private PositnService positnService;
	
	/**
	 * 描述：核减成本显示
	 * @author 马振
	 * 创建时间：2015-4-14 下午1:52:56
	 * @param modelMap
	 * @param firmCostItemSpcode
	 * @param page
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listCostReduction")
	public ModelAndView listCostReduction(ModelMap modelMap, CostItem costItem, HttpSession session, Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(costItem.getBdat() != null && costItem.getEdat() != null){
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			if (null != thePositn) {
				costItem.setFirm(thePositn.getCode());
			}
			modelMap.put("listCostReduction", costReductionService.listCostReduction(costItem, page));
		}
		modelMap.put("costItem", costItem);
		modelMap.put("pageobj", page);
		modelMap.put("sta","-2");
		return new ModelAndView(CostReductionConstants.LIST_COSTREDUCTION, modelMap);
	}
	
	/**
	 * 描述：成本核减
	 * @author 马振
	 * 创建时间：2015-4-15 上午9:52:05
	 * @param modelMap
	 * @param costItemSpcode
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/subtract")
	@ResponseBody
	public int subtract(CostItem costItem, HttpSession session) throws Exception{
		Map<String,String> costTime = new HashMap<String,String>();
		costTime.put("time", 0+"");
		costTime.put("des", "系统正准备进行核减...");
		session.setAttribute("costTime", costTime);
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		costItem.setAcct(acct);
		Positn positn = (Positn)session.getAttribute("accountPositn");
		positn.setAcct(acct);
		Positn p = positnService.findPositnByCode(positn);
		costItem.setFirm(positn.getCode());
		costItem.setFirmdes(positn.getDes());
		costItem.setYnUseDept(p.getYnUseDept());
		return costReductionService.subtract(costItem, costTime);
	}
	
	/***
	 * 获取核减时间
	 * @param session
	 * @param inventory
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/subtractTime")
	@ResponseBody
	public String subtractTime(HttpSession session) throws Exception{
		Map<String,String> costTime = new HashMap<String,String>();
		if(null == session.getAttribute("costTime")){
			costTime.put("time", 0+"");
			costTime.put("des", "系统正准备进行核减。");
		}else{
			costTime = (Map<String, String>) session.getAttribute("costTime");
		}
		return JSONObject.fromObject(costTime).toString();
	}
	
	/**
	 * 描述：反核减
	 * @author 马振
	 * 创建时间：2015-1-1 下午5:34:02
	 * @param modelMap
	 * @param startDate
	 * @param endDate
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/antiReduction")
	public ModelAndView antiReduction(ModelMap modelMap, CostItem costItem, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		costItem.setAcct(acct);
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		costItem.setFirm(thePositn.getCode());
		int sta = costReductionService.antiReduction(costItem);
		modelMap.put("sta",sta);
		Page page = new Page();
		modelMap.put("listCostReduction", costReductionService.listCostReduction(costItem, page));
		modelMap.put("pageobj", page);
		modelMap.put("costItem", costItem);
		return new ModelAndView(CostReductionConstants.LIST_COSTREDUCTION, modelMap);
	}
	
	/***
	 * 核减之前判断是否已核减
	 * @param costItemSpcode
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/checkCostReduction")
	@ResponseBody
	public String checkCostReduction(CostItem costItem, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		String positn = thePositn.getCode();
		costItem.setFirm(positn);
		return costReductionService.checkCostReduction(costItem);
	}
}
