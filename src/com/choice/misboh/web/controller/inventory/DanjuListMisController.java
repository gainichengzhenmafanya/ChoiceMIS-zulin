package com.choice.misboh.web.controller.inventory;

import java.net.URLEncoder;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.misboh.constants.inventory.DanjuListMisConstants;
import com.choice.misboh.domain.inventory.Danju;
import com.choice.misboh.service.inventory.DanjuListMisService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Positn;
import com.choice.scm.service.CodeDesService;

/***
 * 单据列表
 * @author wjf
 *
 */
@Controller
@RequestMapping(value = "danjuListMis")
public class DanjuListMisController {
	
	@Autowired
	private DanjuListMisService danjuListMisService;
	@Autowired
	private CodeDesService codeDesService;
	
	/***
	 * 门店盘点
	 * @param modelMap
	 * @param session
	 * @param action 初始init
	 * @param inventory
	 * @param status 盘点状态 start开始盘点 Y已盘点 N 未盘点
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listDanju")
	public ModelAndView listDanju(ModelMap modelMap, HttpSession session, Danju danju, Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null == danju.getBdate()){
			danju.setBdate(new Date());
			danju.setEdate(new Date());
		}else{
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			String code = thePositn.getCode();
			danju.setThePositn(code);
			String deliver = danju.getDeliver();
			String positn = danju.getPositn();
			String firm = danju.getFirm();
			danju.setDeliver(CodeHelper.replaceCode(deliver));
			danju.setPositn(CodeHelper.replaceCode(positn));
			danju.setFirm(CodeHelper.replaceCode(firm));
			List<Danju> danjuList = danjuListMisService.findListDanju(danju,page);
			modelMap.put("danjuList", danjuList);
			danju.setDeliver(deliver);
			danju.setPositn(positn);
			danju.setFirm(firm);
		}
		String locale = session.getAttribute("locale").toString();
		CodeDes codeDes = new CodeDes();
		codeDes.setLocale(locale);
		codeDes.setCodetyp("RK,CK,ZF");
		modelMap.put("danjuTyp", codeDesService.findDocumentType(codeDes));
		modelMap.put("danju", danju);
		modelMap.put("pageobj", page);
		return new ModelAndView(DanjuListMisConstants.LIST_DANJU, modelMap);
	}
	
	/***
	 * 导出
	 * @param response
	 * @param request
	 * @param session
	 * @param positnSupply
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/exportDanju")
	@ResponseBody
	public boolean exportDanju(HttpServletResponse response, HttpServletRequest request,HttpSession session, Danju danju) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		String code = thePositn.getCode();
		danju.setThePositn(code);
		danju.setPositnDes(thePositn.getDes());
		List<Danju> danjuList = danjuListMisService.findListDanju(danju);
		String fileName = "单据列表";
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //fire_fox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		return danjuListMisService.exportDanju(response.getOutputStream(), danjuList, danju);
	}
	
}
