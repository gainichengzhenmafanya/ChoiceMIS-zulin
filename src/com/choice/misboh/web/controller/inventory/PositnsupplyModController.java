package com.choice.misboh.web.controller.inventory;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.framework.util.ProgramConstants;
import com.choice.misboh.constants.inventory.PositnsupplyModConstants;
import com.choice.misboh.domain.inventory.Chkstoref;
import com.choice.misboh.domain.inventory.Inventory;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.misboh.service.inventory.InventoryMisService;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.MainInfo;
import com.choice.scm.domain.Positn;
import com.choice.scm.service.MainInfoService;
import com.choice.scm.service.PositnService;

/***
 * 库存调整
 * @author wjf
 *
 */
@Controller
@RequestMapping(value = "positnsupplyMod")
public class PositnsupplyModController {
	
	@Autowired
	private InventoryMisService inventoryMisService;
	@Autowired
	private PositnService positnService;
	@Autowired
	private MainInfoService mainInfoService;
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	
	/***
	 * 门店盘点
	 * @param modelMap
	 * @param session
	 * @param action 初始init
	 * @param inventory
	 * @param status 盘点状态 start开始盘点 Y已盘点 N 未盘点
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listPositnsupplyMod")
	public ModelAndView listPositnsupplyMod(ModelMap modelMap, HttpSession session, String action, Inventory inventory,String status) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn positn = inventory.getPositn();
		Positn p = new Positn();//仓位属性
		String acct = session.getAttribute("ChoiceAcct").toString();
		inventory.setAcct(acct);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		thePositn.setAcct(acct);
		Positn firm = positnService.findPositnByCode(thePositn);
		Date nowDate = new Date();
		String yearr = mainInfoService.findYearrList().get(0);//会计年wjf
		if(null != action && "init".endsWith(action)){//初始
			inventory.setYearr(yearr);
			inventory.setDate(DateFormat.formatDate(nowDate, "yyyy-MM-dd"));
			if("Y".equals(firm.getYnUseDept())){//如果本仓位设定使用多档口
				inventory.setFirm(firm.getCode());//页面上判断是否显示档口
			}else{
				positn = new Positn();
				positn.setCode(thePositn.getCode());
				positn.setDes(thePositn.getDes());
				inventory.setPositn(positn);
			}
			modelMap.put("action", "init");
		}else{//查询
			//1.如果勾选了仅查询，则查盘点表
			if("Y".equals(inventory.getOnlyQuery())){
				Chkstoref cf = new Chkstoref();
				cf.setDept(inventory.getPositn().getCode());//盘点仓位
				cf.setWorkDate(DateFormat.getStringByDate(inventory.getDate(), "yyyy-MM-dd"));//营业日
				cf.setPantyp(inventory.getPantyp());//日盘/周盘/月盘
				Chkstoref cf1 = inventoryMisService.findChkstoref(cf);
				modelMap.put("chkstoref", cf1);
				List<Inventory> lists = inventoryMisService.findAllInventoryEnd(inventory);
				modelMap.put("inventoryList", lists);//查询盘点列表
			}else{
				//1.查询今天是否有盘点，有盘点则显示 没有盘点显示0
				Chkstoref cf = new Chkstoref();
				cf.setDept(inventory.getPositn().getCode());//盘点仓位
				cf.setWorkDate(DateFormat.getStringByDate(inventory.getDate(), "yyyy-MM-dd"));//营业日
				cf.setPantyp(inventory.getPantyp());//日盘/周盘/月盘
				cf.setState("0");//盘点未审核的
				Chkstoref cf1 = inventoryMisService.findChkstoref(cf);
				inventory.setSave(cf1 != null?"save":null);//判断是否有盘点的 sql用
				
				int month = commonMISBOHService.getOnlyAccountMonth2(inventory.getDate());
				inventory.setMonth(month);//得到当前盘点日期所在会计月
				List<Inventory> lists = inventoryMisService.findAllInventory(inventory);
				MainInfo mainInfo = mainInfoService.findMainInfo(Integer.parseInt(yearr));
				String efun = "getEdat"+month;
				Method eget = MainInfo.class.getDeclaredMethod(efun);
				Date edate = (Date)eget.invoke(mainInfo);
				inventory.setEdate(edate);
				if(edate.getTime() != inventory.getDate().getTime()){//先判断如果是盘点当月最后一天则直接在positnsupply取余额，无需去计算;如果不是会计月最后一天，则要计算
																	//计算逻辑：当前月结存-（盘点日期到月末最后一天之间的结存）
					List<Inventory> listSupplyacct = inventoryMisService.findSupplyAcctTheMonth(inventory);
					session.setAttribute("time", 50);
					for(Inventory i1 : listSupplyacct){
						for(Inventory i2 : lists){
							if(i1.getSp_code().equals(i2.getSp_code())){
								i2.setCntbla(i2.getCntbla() - i1.getCntbla());
								i2.setCntubla(i2.getCntubla() - i1.getCntubla());
								i2.setAmtbla(i2.getAmtbla() - i1.getAmtbla());
								break;
							}
						}
					}
				}
				modelMap.put("inventoryList", lists);//查询盘点列表
			}
		}
		modelMap.put("inventory", inventory);
		modelMap.put("positnTyp", p.getPcode());//盘点的仓位类型  页面提示用
		modelMap.put("firmOutWay", ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "firmOutWay"));//提示根据配置
		modelMap.put("nowDate", nowDate);//系统当前日期，盘点的时候不能选择大于当前日期20160803
		return new ModelAndView(PositnsupplyModConstants.LISTPOSITNSUPPLYMOD, modelMap);
	}
	
	/***
	 * 保存盘点
	 * @param session
	 * @param inventory
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updatePositnsupplyMod")
	@ResponseBody
	public String updatePositnsupplyMod(HttpSession session,Inventory inventory) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
//				"MISBOH保存盘点:"+inventory.getPositn()==null?"":inventory.getPositn().getCode(),session.getAttribute("ip").toString(),ProgramConstants.SCM);
//		commonMISBOHService.addLogs(logd);
		String acct = session.getAttribute("ChoiceAcct").toString();
		String accountName = session.getAttribute("accountName").toString();
		inventory.setAcct(acct);
		inventory.setFromtyp("");
		inventoryMisService.updatePositnsupplyMod(inventory,accountName);
		return "success";
	}
	
	/***
	 * 删除库存调整
	 * @param session
	 * @param inventory
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delPositnsupplyMod")
	@ResponseBody
	public String delPositnsupplyMod(HttpSession session,Inventory inventory) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
				"MISBOH删除库存调整:"+inventory.getPositn()==null?"":inventory.getPositn().getCode(),session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		String acct = session.getAttribute("ChoiceAcct").toString();
		inventory.setAcct(acct);
		inventoryMisService.delPositnsupplyMod(inventory);
		return "success";
	}
	
	/***
	 * 结束盘点
	 * @param session
	 * @param inventory
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkPositnsupplyMod")
	@ResponseBody
	public String checkPositnsupplyMod(HttpSession session, Inventory inventory) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CHECK,
				"MISBOH结束盘点:"+inventory.getPositn()==null?"":inventory.getPositn().getCode(),session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		//得到盘点主表数据
		Chkstoref cf = new Chkstoref();
		cf.setDept(inventory.getPositn().getCode());//盘点仓位
		cf.setWorkDate(DateFormat.getStringByDate(inventory.getDate(), "yyyy-MM-dd"));//营业日
		cf.setPantyp(inventory.getPantyp());//日盘/周盘/月盘
		cf.setState("0");//盘点未审核的
		Chkstoref cf1 = inventoryMisService.findChkstoref(cf);//最后一条
		try{
			String acct=session.getAttribute("ChoiceAcct").toString();
			String accountName = session.getAttribute("accountName").toString();
			if(cf1 != null && (cf1.getCheckCode() == null || "".equals(cf1.getCheckCode()))){
				//更新盘点主表 状态和时间 还有出库单号
				cf1.setState("0");//已审核的
				cf1.setCheckCode(accountName);
				cf1.setCheckDate(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
				inventoryMisService.updateChkstoref(cf1);
			}else{
				return null;
			}
			Positn p = new Positn();
			p.setAcct(acct);
			p.setCode(inventory.getPositn().getCode());
			Positn positn = positnService.findPositnByCode(p);
			String yearr = mainInfoService.findYearrList().get(0);//会计年wjf
			inventory.setAcct(acct);
			inventory.setYearr(yearr);
			inventory.setSave("save");//查保存的
			inventory.setYnpd("N");//查禁盘的
			inventory.setYnprint("N");//所有的物资包括非打印的
			int month = commonMISBOHService.getOnlyAccountMonth2(inventory.getDate());
			inventory.setMonth(month);//得到当前盘点日期所在会计月
			List<Inventory> listInventory = inventoryMisService.findAllInventory(inventory);
			MainInfo mainInfo = mainInfoService.findMainInfo(Integer.parseInt(yearr));
			String efun = "getEdat"+month;
			Method eget = MainInfo.class.getDeclaredMethod(efun);
			Date edate = (Date)eget.invoke(mainInfo);
			inventory.setEdate(edate);
			if(edate.getTime() != inventory.getDate().getTime()){//先判断如果是盘点当月最后一天则直接在positnsupply取余额，无需去计算;如果不是会计月最后一天，则要计算
																//计算逻辑：当前月结存-（盘点日期到月末最后一天之间的结存）
				List<Inventory> listSupplyacct = inventoryMisService.findSupplyAcctTheMonth(inventory);
				for(Inventory i1 : listSupplyacct){
					for(Inventory i2 : listInventory){
						if(i1.getSp_code().equals(i2.getSp_code())){
							i2.setCntbla(i2.getCntbla() - i1.getCntbla());
							i2.setCntubla(i2.getCntubla() - i1.getCntubla());
							i2.setAmtbla(i2.getAmtbla() - i1.getAmtbla());
							break;
						}
					}
				}
			}
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			//特殊的  配置1 则所有的门店走先进先出
			if("1".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "firmOutWay"))){
				if("1203".equals(positn.getPcode())){
					return inventoryMisService.checkInventoryS(accountName,inventory,listInventory,cf1,thePositn.getCode());//生成盘盈入库 和盘亏出库
				}
			}else if("2".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "firmOutWay"))){//配置2 只有启用档口的店才走 先进先出
				if("1203".equals(positn.getPcode())){
					if("Y".equals(positn.getYnUseDept())){//启用档口的
						return inventoryMisService.checkInventoryS(accountName,inventory,listInventory,cf1,thePositn.getCode());//生成盘盈入库 和盘亏出库
					}
				}
			}
			return inventoryMisService.checkPositnsupplyMod(accountName,inventory,listInventory,cf1,thePositn.getCode());//生成盘亏出库
		}catch(Exception e){
			//更新盘点主表 状态和时间 还有出库单号
			cf1.setState("0");
			cf1.setCheckCode("");
			cf1.setCheckDate("");
			inventoryMisService.updateChkstoref(cf1);
			return e.getLocalizedMessage();
		}
	}
	
}
