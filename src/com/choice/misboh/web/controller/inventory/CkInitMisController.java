package com.choice.misboh.web.controller.inventory;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.redis.RedisConfig;
import com.choice.framework.util.CacheUtil;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.misboh.constants.inventory.CkInitMisConstants;
import com.choice.misboh.domain.inventory.PositnSupply;
import com.choice.misboh.service.chkstom.ChkstomMisService;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.misboh.service.inventory.CkInitMisService;
import com.choice.misboh.service.inventory.ReadInitMisExcel;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyUnit;
import com.choice.scm.service.MainInfoService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.SupplyService;
import com.choice.scm.util.FileWorked;

@Controller
@RequestMapping("ckInitMis")
public class CkInitMisController {

	@Autowired
	private CkInitMisService ckInitMisService;
	@Autowired
	private PositnService positnService;
	@Autowired
	private MainInfoService mainInfoService; 
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	@Autowired
	private SupplyService supplyService;
	@Autowired
	private ChkstomMisService chkstomMisService;
	
	private static String opencluster = RedisConfig.getString("opencluster");
	
	/***
	 * 仓库期初
	 * @param modelMap
	 * @param session
	 * @param positnSupply
	 * @param status
	 * @param action
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/ckInit")
	public ModelAndView ckInit(ModelMap modelMap, HttpSession session, PositnSupply positnSupply, String status, String action) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"MISBOH查询期初:"+positnSupply.getPositn(),session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		String acct = session.getAttribute("ChoiceAcct").toString();
		String yearr = mainInfoService.findYearrList().get(0);
		positnSupply.setAcct(acct);
		positnSupply.setYearr(yearr);
		modelMap.put("positnSupply", positnSupply);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		thePositn.setAcct(acct);
		Positn positn = positnService.findPositnByCode(thePositn);
		if(!"init".equals(action)){
			if("Y".equals(positn.getYnUseDept())){//如果本仓位设定使用多档口
				positnSupply.setFirm(thePositn.getCode());
				Positn p = new Positn();
				p.setAcct(session.getAttribute("ChoiceAcct").toString());
				p.setCode(positnSupply.getPositn());
				if ("Y".equals(ckInitMisService.getQC(p).getQc())) {
					modelMap.put("status", "init");
				}else {
					modelMap.put("status", status);
				}
			}else{
				//查询当前门店的数据
				positnSupply.setPositn(thePositn.getCode());
				if ("Y".equals(positn.getQc())) {
					modelMap.put("status", "init");
				}else {
					modelMap.put("status", status);
				}
			}
			//查询选定档口的数据
			List<PositnSupply> psList = ckInitMisService.getpositnSupplyList(positnSupply);
			setSupplyUnit(psList,session);//设置多规格
			modelMap.put("positnSupplyList", psList);
		}else{
			if("Y".equals(positn.getYnUseDept())){//如果本仓位设定使用多档口
				positnSupply.setFirm(thePositn.getCode());
			}else{
				positnSupply.setPositn(thePositn.getCode());
			}
			//一进节点默认查本门店下的物资规格
			PositnSupply ps = new PositnSupply();
			ps.setPositn(thePositn.getCode());
			List<SupplyUnit> suqc_cache = commonMISBOHService.findAllSupplyUnitByPositn(ps);
			if(null != opencluster && "1".equals(opencluster)){
	    		session.setAttribute("suqc_cache", suqc_cache);
	    	} else {
	    		CacheUtil cacheUtil = CacheUtil.getInstance();
	    		cacheUtil.flush("suqc_cache", session.getId());
	    		cacheUtil.put("suqc_cache", session.getId(), suqc_cache);
	    	}
//			suList.put(pk_group+thePositn.getCode(), (ArrayList<SupplyUnit>) commonMISBOHService.findAllSupplyUnitByPositn(ps));
		}
		return new ModelAndView(CkInitMisConstants.CKINIT,modelMap);
	}
	
	/**
	 * 给物资set多规格
	 * @param lists
	 * @param positn
	 */
	@SuppressWarnings("unchecked")
	private void setSupplyUnit(List<PositnSupply> psList,HttpSession session){
		List<SupplyUnit> mapList = null;
		if(null != opencluster && "1".equals(opencluster)){
    		mapList = (List<SupplyUnit>) session.getAttribute("suqc_cache");
    	} else {
    		CacheUtil cacheUtil = CacheUtil.getInstance();
    		mapList = (List<SupplyUnit>) cacheUtil.get("suqc_cache", session.getId());
    	}
		for(PositnSupply ps : psList){
			for(SupplyUnit su : mapList){
				if(ps.getSp_code().equals(su.getSp_code())){
					if(1 == su.getSequence()){
						ps.setSpec1(su.getUnit());
						ps.setUnitper1(su.getUnitper());
					}else if(2 == su.getSequence()){
						ps.setSpec2(su.getUnit());
						ps.setUnitper2(su.getUnitper());
					}else if(3 == su.getSequence()){
						ps.setSpec3(su.getUnit());
						ps.setUnitper3(su.getUnitper());
					}else if(4 == su.getSequence()){
						ps.setSpec4(su.getUnit());
						ps.setUnitper4(su.getUnitper());
					}
				}
			}
			//2.如果循环完了 还没有的话，需要去查一遍了，这种是为了添加的那种物资
			if(ps.getSpec1() == null || "".equals(ps.getSpec1())){
				List<SupplyUnit> suList = findSupplyUnitBySpcode(ps.getSp_code());
				for(SupplyUnit su : suList){
					if(ps.getSp_code().equals(su.getSp_code())){
						if(1 == su.getSequence()){
							ps.setSpec1(su.getUnit());
							ps.setUnitper1(su.getUnitper());
						}else if(2 == su.getSequence()){
							ps.setSpec2(su.getUnit());
							ps.setUnitper2(su.getUnitper());
						}else if(3 == su.getSequence()){
							ps.setSpec3(su.getUnit());
							ps.setUnitper3(su.getUnitper());
						}else if(4 == su.getSequence()){
							ps.setSpec4(su.getUnit());
							ps.setUnitper4(su.getUnitper());
						}
					}
				}
				//3.把查出来的放到map
				mapList.addAll(suList);
			}
		}
	}
	
	/***
	 * 查特定物资的多规格
	 * @param sp_code
	 * @return
	 */
	private List<SupplyUnit> findSupplyUnitBySpcode(String sp_code){
		SupplyUnit su = new SupplyUnit();
		su.setSp_code(sp_code);
		try {
			return supplyService.findAllSupplyUnit(su);
		} catch (CRUDException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/***
	 * 保存仓库期初
	 * @param modelMap
	 * @param session
	 * @param positnSupply
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateCkInit")
	@ResponseBody
	public String updateCkInit(ModelMap modelMap, HttpSession session, PositnSupply positnSupply) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
				"MISBOH保存期初:"+positnSupply.getPositn(),session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		positnSupply.setAcct(session.getAttribute("ChoiceAcct").toString());
		positnSupply.setYearr(mainInfoService.findYearrList().get(0));
		ckInitMisService.updateCkInit(positnSupply);
		return "success";
	}
	
	/***
	 * 仓库期初-确认初始
	 * @param modelMap
	 * @param session
	 * @param positnSupply
	 * @throws Exception
	 */
	@RequestMapping(value = "/initation")
	@ResponseBody
	public void initation(ModelMap modelMap, HttpSession session, PositnSupply positnSupply) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CHECK,
				"MISBOH确认期初:"+positnSupply.getPositn(),session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		String acct = session.getAttribute("ChoiceAcct").toString();
		positnSupply.setAcct(acct);
		positnSupply.setYearr(mainInfoService.findYearrList().get(0));
		List<PositnSupply> psList = ckInitMisService.getpositnSupplyList(positnSupply);
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		setSupplyUnit(psList, session);
		positnSupply.setFirm(thePositn.getCode());
		ckInitMisService.initation(psList, positnSupply);
	}
	
	/***
	 * 导出
	 * @param response
	 * @param request
	 * @param session
	 * @param positnSupply
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/exportCangkuInit")
	@ResponseBody
	public boolean exportCangkuInit(HttpServletResponse response, HttpServletRequest request,HttpSession session, PositnSupply positnSupply) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		positnSupply.setAcct(session.getAttribute("ChoiceAcct").toString());
		positnSupply.setYearr(mainInfoService.findYearrList().get(0));
		Positn p = new Positn();
		p.setCode(positnSupply.getPositn());
		positnSupply.setPositnName(positnService.findPositnByCode(p).getDes());
		Positn positn = (Positn)session.getAttribute("accountPositn");
		List<PositnSupply> positnSupplyList = new ArrayList<PositnSupply>();
		if("Y".equals(positnSupply.getIsTemplete())){
			positnSupplyList = ckInitMisService.getpositnSupplyListByPositnSpcode(positn);
		}else{
			positnSupplyList = ckInitMisService.getpositnSupplyList(positnSupply);
		}
		
		setSupplyUnit(positnSupplyList, session);
		String fileName = "仓库期初表";
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //fire_fox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		return ckInitMisService.exportCangkuInit(response.getOutputStream(), positnSupplyList, positnSupply);
	}
	
	/**
	 * 跳转到导入页面   导入 wjf
	 */
	@RequestMapping("/importInit")
	public ModelAndView importChkstom(ModelMap modelMap,String positn) throws Exception{
		modelMap.put("positn", positn);
		return new ModelAndView(CkInitMisConstants.IMPORT_INIT,modelMap);
	}
	
	/**
	 * 先上传excel
	 */
	@RequestMapping(value = "/loadExcel", method = RequestMethod.POST)
	public ModelAndView loadExcel(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap,String positn) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String realFilePath = chkstomMisService.upload(request, response,"ckInit.xls");
		modelMap.put("realFilePath", realFilePath);
		modelMap.put("positn", positn);
		return new ModelAndView(CkInitMisConstants.IMPORT_RESULT, modelMap);
	}
	
	/**
	 * 导入期初  wjf
	 */
	@RequestMapping(value = "/importExcel")
	public ModelAndView importExcel(HttpSession session, ModelMap modelMap, @RequestParam String realFilePath,PositnSupply positnSupply)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.IMPORT,
				"Misboh导入期初 ",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		String positnCode = "";
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			positnCode = thePositn.getCode();
		}
		boolean checkResult = ReadInitMisExcel.check(realFilePath, supplyService, positnCode);
		if (checkResult) {//成功
			List<PositnSupply> psList = ReadInitMisExcel.psList;
			setSupplyUnit(psList, session);//设置多规格
			//根据录入数量得到盘点总数量
			for(PositnSupply ps : psList){
				double sumCnt = ps.getCnt1();
				if(null != ps.getSpec2() && ps.getUnitper2() != 0){
					sumCnt += ps.getCnt2()/ps.getUnitper2();
				}
				if(null != ps.getSpec3() && ps.getUnitper3() != 0){
					sumCnt += ps.getCnt3()/ps.getUnitper3();
				}
				if(null != ps.getSpec4() && ps.getUnitper4() != 0){
					sumCnt += ps.getCnt4()/ps.getUnitper4();
				}
				ps.setInc0(sumCnt);
			}
			String acct = session.getAttribute("ChoiceAcct").toString();
			String yearr = mainInfoService.findYearrList().get(0);
			positnSupply.setAcct(acct);
			positnSupply.setYearr(yearr);
			modelMap.put("positnSupply", positnSupply);
			Positn p = new Positn();
			p.setCode(positnSupply.getPositn());
			Positn positn = positnService.findPositnByCode(p);
			positnSupply.setPositnName(positn.getDes());
			Positn positn1 = positnService.findPositnByCode(thePositn);
			if("Y".equals(positn1.getYnUseDept())){//如果本仓位设定使用多档口
				positnSupply.setFirm(thePositn.getCode());
			}
			modelMap.put("status", "import");
			modelMap.put("positnSupplyList", psList);
		} else {
			List<String> errorList = ReadInitMisExcel.errorList;
			modelMap.put("status", "select");
			modelMap.put("importError", errorList);
		}
		FileWorked.deleteFile(realFilePath);//删除上传后的的文件
		return new ModelAndView(CkInitMisConstants.CKINIT,modelMap);
	}
	
	
	/***
	 * 期初状态查询
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/qcZtchaxun")
	public ModelAndView chkstomDkqc(ModelMap modelMap,HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//判断ucode
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			modelMap.put("positnList",ckInitMisService.qcZtchaxun(thePositn));
		}
		return new ModelAndView(CkInitMisConstants.QCZTCHAXUN,modelMap);
	}
	
}
