package com.choice.misboh.web.controller.inventory;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.constants.inventory.MonthEndMisConstants;
import com.choice.misboh.domain.inventory.Chkstoref;
import com.choice.misboh.service.inventory.InventoryMisService;
import com.choice.misboh.service.myDesk.MyDeskMisbohService;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.Chkoutm;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.MainInfo;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.MainInfoService;
import com.choice.scm.service.PositnService;

/***
 * 分店月结
 * @author wjf
 *
 */
@Controller
@RequestMapping("monthEndMis")
public class MonthEndMisController {

	@Autowired
	private MainInfoService mainInfoService;
	@Autowired
	private MyDeskMisbohService myDeskMisbohService;
	@Autowired
	private PositnService positnService;
	@Autowired
	private InventoryMisService inventoryMisService;
	
	/***
	 * 进入分店月结
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/tableMonthEnd")
	public ModelAndView tableMonthEnd(ModelMap modelMap,HttpSession session,String action) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		Map<String,String> endTime = new HashMap<String,String>();
		endTime.put("time", 0+"");
		endTime.put("des", "系统正准备进行月结检查...");
		session.setAttribute("endTime", endTime);
		if (null != thePositn && "select".equals(action)) {
			String positnCode = thePositn.getCode();
			//得到当前会计月的开始日期和结束日期
			endTime.put("time", 5+"");
			endTime.put("des", "获取当前会计月的开始日期和结束日期。");
			Positn positn = positnService.findPositnByCode(thePositn);
			String monthh = "1";
			if(null != positn.getMonthh() && !"".equals(positn.getMonthh())){
				monthh = positn.getMonthh();
			}
			int year = Integer.parseInt(mainInfoService.findYearrList().get(0));
			MainInfo mainInfo= mainInfoService.findMainInfo(year);
			String bdat = "getBdat"+monthh;
			String edat = "getEdat"+monthh;
			Method getBdat = MainInfo.class.getDeclaredMethod(bdat);
			Date bdate = (Date)getBdat.invoke(mainInfo);
			Method getEdat = MainInfo.class.getDeclaredMethod(edat);
			Date edate = (Date)getEdat.invoke(mainInfo);
			modelMap.put("month", monthh);
			modelMap.put("bdate", DateFormat.getStringByDate(bdate, "yyyy-MM-dd"));
			modelMap.put("edate", DateFormat.getStringByDate(edate, "yyyy-MM-dd"));
			if(Integer.parseInt(monthh) == 13){
				return new ModelAndView(MonthEndMisConstants.TABLEMONTHEND,modelMap);
			}
			//得到下个月
			endTime.put("time", 10+"");
			endTime.put("des", "获取下一个会计月日期.");
			String nextBdat = "getBdat"+(Integer.parseInt(monthh)+1);
			String nextEdat = "getEdat"+(Integer.parseInt(monthh)+1);
			Method getNextBdat = MainInfo.class.getDeclaredMethod(nextBdat);
			Date nextBdate = (Date)getNextBdat.invoke(mainInfo);
			Method getNextEdat = MainInfo.class.getDeclaredMethod(nextEdat);
			Date nextEdate = (Date)getNextEdat.invoke(mainInfo);
			modelMap.put("nextMonth", Integer.parseInt(monthh)+1);
			modelMap.put("nextBdate", DateFormat.getStringByDate(nextBdate, "yyyy-MM-dd"));
			modelMap.put("nextEdate", DateFormat.getStringByDate(nextEdate, "yyyy-MM-dd"));
			String bdateString = DateFormat.getStringByDate(bdate, "yyyy-MM-dd");
			String edateString = DateFormat.getStringByDate(edate, "yyyy-MM-dd");
			//1.有没有直配，配送，调拨的未验货
			//1.查询当月直配未验货单据数
			endTime.put("time", 20+"");
			endTime.put("des", "查询当月是否有直配未验货数据。");
			Dis dis = new Dis();
			dis.setAcct(acct);
			dis.setFirmCode(positnCode);
			dis.setInout(DDT.DIRE);
			dis.setBdat(bdate);
			dis.setEdat(edate);
			dis.setYndo("NO");
			int direCount = inventoryMisService.findAllDisCount(dis);
			modelMap.put("direCount", direCount);
			//2.查询统配未验货单据数
			endTime.put("time", 30+"");
			endTime.put("des", "查询当月是否有统配未验货数据。");
			SupplyAcct sa = new SupplyAcct();
			sa.setAcct(acct);
			sa.setBdate(bdateString);
			sa.setEdate(edateString);
			sa.setFirmcode(positnCode);
			sa.setChkyh("no");
			if("Y".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "isInsByBill"))){//验货根据单据
				int outCount = inventoryMisService.findOutBillCount(sa);
				modelMap.put("outCount", outCount);
			}else{
				int outCount = inventoryMisService.findOutCount(sa);
				modelMap.put("outCount", outCount);
			}
			//3.查询调拨未验货单据数
			endTime.put("time", 40+"");
			endTime.put("des", "查询当月是否有调拨未验货数据.");
			int dbCount = inventoryMisService.findOutDbCount(sa);
			modelMap.put("dbCount", dbCount);
			//2.报损单有没有确认
			
			//3.单据有未审核的吗
			//当月有多少入库单未审核
			endTime.put("time", 50+"");
			endTime.put("des", "查询当月是否有入库单未审核.");
			Map<String,Object> map = new HashMap<String,Object>();
			Chkinm chkinm = new Chkinm();
			chkinm.setInout("in");
			chkinm.setPositn(thePositn);
			chkinm.setMaded(bdate);
			map.put("chkinm", chkinm);
			map.put("madedEnd", edate);
			int chkinmCount = inventoryMisService.findChkinmCount(map);
			modelMap.put("chkinmCount", chkinmCount);
			//当月多少直发单未审核
			endTime.put("time", 60+"");
			endTime.put("des", "查询当月是否有直发单未审核.");
			chkinm.setInout("inout");
			int chkinmzfCount = inventoryMisService.findChkinmCount(map);
			modelMap.put("chkinmzfCount", chkinmzfCount);
			//当月有多少出库单未审核
			endTime.put("time", 70+"");
			endTime.put("des", "查询当月是否有出库单未审核.");
			Map<String,Object> map1 = new HashMap<String,Object>();
			map1.put("bdat", bdate);
			map1.put("edat", edate);
			Chkoutm chkoutm = new Chkoutm();
			chkoutm.setAcct(acct);
			Positn p = new Positn();
			p.setUcode(positnCode);
			List<Positn> list = positnService.findPositnSuperNOPage(p);//查询该分店的档口
			List<String> listPositn = new ArrayList<String>();
			for(Positn posi:list){
				listPositn.add(posi.getCode());
			}
			listPositn.add(thePositn.getCode());//加上分店本身
			chkoutm.setListPositn(listPositn);
			map1.put("chkoutm", chkoutm);
			map1.put("db", "ck");
			int chkoutmCount = inventoryMisService.findChkoutmCount(map1);
			modelMap.put("chkoutmCount", chkoutmCount);
			map1.put("db", "db");
			//当月调拨出库未审核的
			endTime.put("time", 75+"");
			endTime.put("des", "查询当月是否有调拨单未审核.");
			int chkoutmdbCount = inventoryMisService.findChkoutmCount(map1);
			modelMap.put("chkoutmdbCount", chkoutmdbCount);
			//4.当月最后一天盘点有没有做
			endTime.put("time", 90+"");
			endTime.put("des", "查询当月最后一天所有仓位是否都进行了盘点.");
			Chkstoref cf = new Chkstoref();
			cf.setFirm(positnCode);
			cf.setWorkDate(DateFormat.getStringByDate(edate, "yyyy-MM-dd"));
			cf.setState("1");//已审核
			List<Positn> depts = myDeskMisbohService.findUnInventoryPositn(cf);
//			List<Positn> allPositns = new ArrayList<Positn>();//存放未盘点仓位
//			allPositns.add(thePositn);
//			allPositns.addAll(list);
			Iterator<Positn> car = list.iterator();
			while(car.hasNext()){
				Positn d1 = car.next();
				for(Positn d2 : depts){
					if(d1.getCode().equals(d2.getCode())){
						car.remove();
						break;
					}
				}
			}
			modelMap.put("unInventoryPositn", list);
		}
		modelMap.put("action", action);
		//是否必须验货盘点才能月结
		modelMap.put("isControPdYj", ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "isControPdYj"));
		endTime.put("time", 100+"");
		endTime.put("des", "检查完毕.");
		return new ModelAndView(MonthEndMisConstants.TABLEMONTHEND,modelMap);
	}
	
	/**
	 * 月结
	 */
	@RequestMapping(value = "/checkMonthEnd") 
	@ResponseBody
	public int checkMonthEnd(ModelMap modelMap,HttpSession session,String monthh) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			String positnCode = thePositn.getCode();
			myDeskMisbohService.updateMonthhByPositn(positnCode,monthh);
			return 1;
		}
		return 0;
	}
	
	/***
	 * 获取月结时间
	 * @param session
	 * @param inventory
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/monthEndTime")
	@ResponseBody
	public String monthEndTime(HttpSession session) throws Exception{
		Map<String,String> costTime = new HashMap<String,String>();
		if(null == session.getAttribute("endTime")){
			costTime.put("time", 0+"");
			costTime.put("des", "系统正准备进行月结检查。");
		}else{
			costTime = (Map<String, String>) session.getAttribute("endTime");
		}
		return JSONObject.fromObject(costTime).toString();
	}
	
}
