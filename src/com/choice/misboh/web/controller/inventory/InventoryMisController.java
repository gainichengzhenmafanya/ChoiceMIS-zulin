package com.choice.misboh.web.controller.inventory;

import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.redis.RedisConfig;
import com.choice.framework.util.CacheUtil;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.framework.util.ProgramConstants;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.constants.inventory.InventoryMisConstants;
import com.choice.misboh.domain.BaseRecord.OtherStockDataO;
import com.choice.misboh.domain.inspection.ArrivalmMis;
import com.choice.misboh.domain.inventory.Chkstoref;
import com.choice.misboh.domain.inventory.Chkstoreo;
import com.choice.misboh.domain.inventory.Inventory;
import com.choice.misboh.domain.inventory.PositnSupply;
import com.choice.misboh.domain.lossmanage.PostionLoss;
import com.choice.misboh.domain.myDesk.GuideConfig;
import com.choice.misboh.domain.store.Store;
import com.choice.misboh.service.chkstom.ChkstomMisService;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.misboh.service.inout.ChkoutMisService;
import com.choice.misboh.service.inventory.InventoryMisService;
import com.choice.misboh.service.inventory.ReadInventoryMisExcel;
import com.choice.misboh.service.inventory.ReadInventoryMisExcel1;
import com.choice.misboh.service.myDesk.MyDeskMisbohService;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.Chkoutm;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.InventoryDemod;
import com.choice.scm.domain.InventoryDemom;
import com.choice.scm.domain.MainInfo;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.domain.SupplyUnit;
import com.choice.scm.domain.WorkBench;
import com.choice.scm.service.MainInfoService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.SupplyService;
import com.choice.scm.util.FileWorked;
import com.choice.scm.util.ReadReportUrl;

/***
 * 盘点
 * @author wjf
 *
 */
@Controller
@RequestMapping(value = "inventoryMis")
public class InventoryMisController {
	
	@Autowired
	private InventoryMisService inventoryMisService;
	@Autowired
	private PositnService positnService;
	@Autowired
	private MainInfoService mainInfoService;
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	@Autowired
	private SupplyService supplyService;
	@Autowired
	private MyDeskMisbohService myDeskMisbohService;
	@Autowired
	private ChkoutMisService chkoutMisService;
	@Autowired
	private ChkstomMisService chkstomMisService;

	private final static int PAGE_SIZE = 25;

	private static String opencluster = RedisConfig.getString("opencluster");
	
	/***
	 * 门店盘点
	 * @param modelMap
	 * @param session
	 * @param action 初始init
	 * @param inventory
	 * @param status 盘点状态 start开始盘点 Y已盘点 N 未盘点
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/list")
	public ModelAndView findAllInventory(ModelMap modelMap, HttpSession session, String action, Inventory inventory,String status) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if("Y".equals(inventory.getMonthpan())){//如果是月盘单独节点的 默认月盘且不可改
			inventory.setPantyp("monthpan");
		}
		Positn positn = inventory.getPositn();
		Positn p = new Positn();//仓位属性
		String acct = session.getAttribute("ChoiceAcct").toString();
		inventory.setAcct(acct);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		thePositn.setAcct(acct);
		Positn firm = positnService.findPositnByCode(thePositn);
		Date nowDate = new Date();
		if(null != action && "init".endsWith(action)){//初始
			String yearr = mainInfoService.findYearrList().get(0);//会计年wjf
			inventory.setYearr(yearr);
			if("Y".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "ynInventoryNowDate"))){//默认日期是今天
				inventory.setDate(DateFormat.formatDate(nowDate, "yyyy-MM-dd"));
			}else{
				Calendar calendar = Calendar.getInstance();
				calendar.add(Calendar.DATE, -1); // 得到前一天
				inventory.setDate(DateFormat.formatDate(calendar.getTime(), "yyyy-MM-dd"));
			}
			if("Y".equals(inventory.getMonthpan())){//如果是月盘单独节点的，日期是当前月所在会计月的最后一天
				MainInfo mainInfo = mainInfoService.findMainInfo(Integer.parseInt(yearr));
				int month = 0;
				try{
					month = commonMISBOHService.getOnlyAccountMonth2(nowDate);
				}catch(Exception e){
					month = 13;
				}
				String fun = "getEdat"+month;
				Method get = MainInfo.class.getDeclaredMethod(fun);
				Date date = (Date)get.invoke(mainInfo);
				inventory.setDate(date);
			}
			if("Y".equals(firm.getYnUseDept())){//如果本仓位设定使用多档口
				inventory.setFirm(firm.getCode());//页面上判断是否显示档口
			}else{
				positn = new Positn();
				positn.setCode(thePositn.getCode());
				positn.setDes(thePositn.getDes());
				inventory.setPositn(positn);
			}
			modelMap.put("action", "init");
			//一进节点默认查本门店下的物资规格
			//1.先判断session或者缓存中是否有  有就取，没有就写 因为盘点报表里也用
			List<SupplyUnit> supd_cache = null;
			PositnSupply ps = new PositnSupply();
			ps.setAcct(acct);
			ps.setYearr(yearr);
			ps.setPositn(thePositn.getCode());
			ps.setYnpd("Y");
			if(null != opencluster && "1".equals(opencluster)){
				Object obj = session.getAttribute("supd_cache");
				if(null == obj){
					supd_cache = commonMISBOHService.findAllSupplyUnitByPositn(ps);
					session.setAttribute("supd_cache", supd_cache);
				}
	    	} else {
	    		CacheUtil cacheUtil = CacheUtil.getInstance();
	    		supd_cache = (List<SupplyUnit>)cacheUtil.get("supd_cache", session.getId());	
				if(null == supd_cache){//如果是空，加入缓存
					supd_cache = commonMISBOHService.findAllSupplyUnitByPositn(ps);
					cacheUtil.flush("supd_cache", session.getId());
					cacheUtil.put("supd_cache", session.getId(), supd_cache);
				}
	    	}
//			suList.put(pk_group+thePositn.getCode(), (ArrayList<SupplyUnit>) commonMISBOHService.findAllSupplyUnitByPositn(ps));
		}else{//查询
			//1.如果勾选了仅查询，则查盘点表
			if("Y".equals(inventory.getOnlyQuery())){
				Chkstoref cf = new Chkstoref();
				cf.setDept(inventory.getPositn().getCode());//盘点仓位
				cf.setWorkDate(DateFormat.getStringByDate(inventory.getDate(), "yyyy-MM-dd"));//营业日
				cf.setPantyp(inventory.getPantyp());//日盘/周盘/月盘
				Chkstoref cf1 = inventoryMisService.findChkstoref(cf);
				modelMap.put("chkstoref", cf1);
				List<Inventory> lists = inventoryMisService.findAllInventoryEnd(inventory);
				setSupplyUnit(lists, session);//设置多规格
				modelMap.put("inventoryList", lists);//查询盘点列表
			}else{
			//判断配置如果走每日指引做完才能盘点，则要判断每日指引
			if("Y".equals(DDT.isPdByMydesk)){
				String result = isPdByMydesk(thePositn,session,inventory);
				if(!"".equals(result)){
					modelMap.put("guideConfig", result);
					if("Y".equals(DDT.isPdUnit2)){//九毛九的盘点
						if("Y".equals(DDT.isDistributionUnit)){//是否根据配送单位盘点
							return new ModelAndView(InventoryMisConstants.LIST_INVENTORY_DISUNIT, modelMap);
						}
						return new ModelAndView(InventoryMisConstants.LIST_INVENTORY_UNIT2, modelMap);
					}
					return new ModelAndView(InventoryMisConstants.LIST_INVENTORY, modelMap);
				}
			}
			if(null != status && "start".endsWith(status)){//开始盘点
				p.setAcct(acct);
				p.setOldcode(inventory.getPositn().getCode());
				p.setPd("N");
				positnService.updatePositn(p);
				positn.setPd("N");
			}
			positn.setAcct(acct);
			positn.setCode(inventory.getPositn().getCode());
			p = positnService.findPositnByCode(positn);
			positn.setPd(p.getPd());//  查询是否盘点	
			
//			inventory.setStartNum(0);
//			inventory.setEndNum(PAGE_SIZE);
			//如果选择了某天的盘点,则查哪天的盘点
			if(null != inventory.getChkstorefid() && !"".equals(inventory.getChkstorefid())){
				Chkstoreo co = new Chkstoreo();
				co.setChkstorefid(inventory.getChkstorefid());
				List<Inventory> lists = inventoryMisService.findChkstoreo(co);
				setSupplyUnit(lists, session);//设置多规格
				modelMap.put("inventoryList", lists);//查询盘点列表
				modelMap.put("fromtyp", inventory.getFromtyp());
			} else {
				//1.查询今天是否有盘点，有盘点则显示 没有盘点显示0
				Chkstoref cf = new Chkstoref();
				cf.setDept(inventory.getPositn().getCode());//盘点仓位
				cf.setWorkDate(DateFormat.getStringByDate(inventory.getDate(), "yyyy-MM-dd"));//营业日
				cf.setPantyp(inventory.getPantyp());//日盘/周盘/月盘
				cf.setState("0");//盘点未审核的
				Chkstoref cf1 = inventoryMisService.findChkstoref(cf);
				inventory.setSave(cf1 != null?"save":null);//判断是否有盘点的 sql用
//				inventory.setEnd(cf1 != null?cf1.getState():null);//界面判断按钮状态用
				List<Inventory> lists = inventoryMisService.findAllInventory(inventory);
				setSupplyUnit(lists, session);//设置多规格
				modelMap.put("inventoryList", lists);//查询盘点列表
			}
//			int totalCount = inventoryMisService.findAllInventoryCount(inventory);
//			modelMap.put("totalCount", totalCount);
			
//			if(totalCount<=PAGE_SIZE){
//				modelMap.put("currState", 1);
//			}else{
//				modelMap.put("currState", PAGE_SIZE*1.0/totalCount);//总页数
//			}
//			modelMap.put("currPage", 1);
//			modelMap.put("pageSize", PAGE_SIZE);
//			modelMap.put("disJson", JSONObject.fromObject(inventory));
			}
		}
		modelMap.put("inventory", inventory);
		modelMap.put("positnTyp", p.getPcode());//盘点的仓位类型  页面提示用
		modelMap.put("firmOutWay", ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "firmOutWay"));//提示根据配置
		modelMap.put("nowDate", nowDate);//系统当前日期，盘点的时候不能选择大于当前日期20160803
		if("Y".equals(DDT.isPdUnit2)){
			if("Y".equals(DDT.isDistributionUnit)){//是否根据配送单位盘点
				return new ModelAndView(InventoryMisConstants.LIST_INVENTORY_DISUNIT, modelMap);
			}
			return new ModelAndView(InventoryMisConstants.LIST_INVENTORY_UNIT2, modelMap);
		}
		return new ModelAndView(InventoryMisConstants.LIST_INVENTORY, modelMap);
	}
	
	/**
	 * 盘点是否根据我的桌面 每日指引
	 * @param thePositn
	 * @param session
	 * @param inventory
	 * @return
	 * @throws Exception
	 */
	private String isPdByMydesk(Positn thePositn,HttpSession session,Inventory inventory) throws Exception{
		StringBuffer sb = new StringBuffer();
		String positnCode = thePositn.getCode();
		//查询用户自定义菜单信息
		String accountId = (String)(session.getAttribute("accountId"));
		WorkBench workBench = mainInfoService.findWorkBench(accountId);
		if(null!= workBench && null != workBench.getMenu() && !"".equals(workBench.getMenu())){
			String[] menuArray = workBench.getMenu().split(",");
			for(String menu:menuArray){
				if(menu.equals("m_33_3")){//每日指引
					//查询我的桌面配置
					List<GuideConfig> list = myDeskMisbohService.findGuideConfig();
					for(GuideConfig gc : list){
						Dis dis = new Dis();
						dis.setFirmCode(positnCode);
						dis.setInd(DateFormat.formatDate(inventory.getDate(), "yyyy-MM-dd"));
						String date = DateFormat.getStringByDate(inventory.getDate(), "yyyy-MM-dd");
						if("1".equals(gc.getPk_guideconfig())){//1.直配验货
							int count = myDeskMisbohService.findDireCount(dis);
							if(count != 0){
								sb.append("&nbsp;&nbsp;当前门店在"+date+"仍有"+count+"条直配单据未验货;\n");
							}
						}else if("2".equals(gc.getPk_guideconfig())){//2.统配验货
							int count = myDeskMisbohService.findOutCount1(dis);
							if(count != 0){
								sb.append("&nbsp;&nbsp;当前门店在"+date+"仍有"+count+"条统配单据未验货;\n");
							}
						}else if("3".equals(gc.getPk_guideconfig())){//3.调拨验货
							int count = myDeskMisbohService.findDbCount(dis);
							if(count != 0){
								sb.append("&nbsp;&nbsp;当前门店在"+date+"仍有"+count+"条调拨单据未验货;\n");
							}
						}else if("4".equals(gc.getPk_guideconfig())){//4.成本核减
							int count = myDeskMisbohService.findCostitemspcodeCount(dis);
							if(count == 0){
								sb.append("&nbsp;&nbsp;当前门店在"+date+"未进行核减;\n");
							}
						}else if("5".equals(gc.getPk_guideconfig())){//5.水电气录入
							//将编码放入实体类
							Store store = new Store();
							store.setVcode(positnCode);
							//返回根据当前门店编码获取的当前门店主键
							String pk_store = commonMISBOHService.getStore(store).getPk_store();
							OtherStockDataO oo = new OtherStockDataO();
							oo.setPk_store(pk_store);
							oo.setWorkdate(date);
							if(pk_store == null){
								sb.append("&nbsp;&nbsp;当前门店在"+date+"未录入水电气;\n");
							}else{
								List<OtherStockDataO> ooList = myDeskMisbohService.findOtherStockdataCount(oo);
								if(ooList.size() > 0){
									for(OtherStockDataO o : ooList){
										if(1 == o.getType()){//水
											sb.append("&nbsp;&nbsp;当前门店在"+date+"电表未录入;\n");
										}else{
											sb.append("&nbsp;&nbsp;当前门店在"+date+"水表未录入;\n");
											break;
										}
										if(2 == o.getType()){//电
											sb.append("&nbsp;&nbsp;当前门店在"+date+"气表未录入;\n");
										}else{
											sb.append("&nbsp;&nbsp;当前门店在"+date+"电表未录入;\n");
											break;
										}
										if(3 == o.getType()){//气
										}else{
											sb.append("&nbsp;&nbsp;当前门店在"+date+"气表未录入;\n");
											break;
										}
									}
								}else{
									sb.append("&nbsp;&nbsp;当前门店在"+date+"未录入水电气;\n");
								}
							}
						}else if("6".equals(gc.getPk_guideconfig())){//6.成品损耗
							PostionLoss pl = new PostionLoss();
							pl.setDept(positnCode);
							pl.setWorkdate(date);
							pl.setTyp(1);//成品
							pl.setState(2);//已确认
							int count = myDeskMisbohService.findPostionLossCount(pl);
							if(count == 0){
								sb.append("&nbsp;&nbsp;当前门店在"+date+"成品损耗未录入;\n");
							}
						}else if("7".equals(gc.getPk_guideconfig())){//7.半成品损耗
							PostionLoss pl = new PostionLoss();
							pl.setDept(positnCode);
							pl.setWorkdate(date);
							pl.setTyp(2);//半成品
							pl.setState(2);//已确认
							int count = myDeskMisbohService.findPostionLossCount(pl);
							if(count == 0){
								sb.append("&nbsp;&nbsp;当前门店在"+date+"半成品损耗未录入;\n");
							}
						}else if("8".equals(gc.getPk_guideconfig())){//8.原料损耗
							PostionLoss pl = new PostionLoss();
							pl.setDept(positnCode);
							pl.setWorkdate(date);
							pl.setTyp(3);//物资
							pl.setState(2);//已确认
							int count = myDeskMisbohService.findPostionLossCount(pl);
							if(count == 0){
								sb.append("&nbsp;&nbsp;当前门店在"+date+"原料损耗未录入;\n");
							}
						}else if("10".equals(gc.getPk_guideconfig())){//10.千元用量报货
							int count = myDeskMisbohService.findChkstomCount(dis);
							if(count == 0){
								sb.append("&nbsp;&nbsp;当前门店在"+date+"未报货;\n");
							}
						}else if("12".equals(gc.getPk_guideconfig())){//12.员工餐出库
							Chkoutm chkoutm = new Chkoutm();
							chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
							Positn outPositn = new Positn();
							outPositn.setCode(positnCode);
							chkoutm.setPositn(outPositn);
							chkoutm.setTyp(DDT.YGCCK);
							chkoutm.setChecby("c");
							List<Chkoutm> listCk = chkoutMisService.findChkoutm(chkoutm, inventory.getDate(), inventory.getDate(), null,session.getAttribute("locale").toString(),"ck");// 不查调拨到其他门店的
							if(listCk.size() == 0){
								sb.append("&nbsp;&nbsp;当前门店在"+date+"未录入员工餐出库;\n");
							}
						}
					}
				}
			}
		}
		return sb.toString();
	}
	
	/**
	 * 给物资set多规格
	 * @param lists
	 * @param positn
	 */
	@SuppressWarnings("unchecked")
	private void setSupplyUnit(List<Inventory> lists, HttpSession session){
		if("Y".equals(DDT.isPdUnit2)){//走九毛九的不走多规格
			return;
		}
		List<SupplyUnit> mapList = null;
		if(null != opencluster && "1".equals(opencluster)){
    		mapList = (List<SupplyUnit>) session.getAttribute("supd_cache");
    	} else {
    		CacheUtil cacheUtil = CacheUtil.getInstance();
    		mapList = (List<SupplyUnit>) cacheUtil.get("supd_cache", session.getId());
    	}
		for(Inventory ps : lists){
			//1.从多规格map中取
			for(SupplyUnit su : mapList){
				if(ps.getSp_code().equals(su.getSp_code())){
					if(1 == su.getSequence()){
						ps.setSpec1(su.getUnit());
						ps.setUnitper1(su.getUnitper());
					}else if(2 == su.getSequence()){
						ps.setSpec2(su.getUnit());
						ps.setUnitper2(su.getUnitper());
					}else if(3 == su.getSequence()){
						ps.setSpec3(su.getUnit());
						ps.setUnitper3(su.getUnitper());
					}else if(4 == su.getSequence()){
						ps.setSpec4(su.getUnit());
						ps.setUnitper4(su.getUnitper());
					}
				}
			}
			//2.如果循环完了 还没有的话，需要去查一遍了，这种是为了添加的那种物资
			if(ps.getSpec1() == null || "".equals(ps.getSpec1())){
				List<SupplyUnit> suList = findSupplyUnitBySpcode(ps.getSp_code());
				for(SupplyUnit su : suList){
					if(ps.getSp_code().equals(su.getSp_code())){
						if(1 == su.getSequence()){
							ps.setSpec1(su.getUnit());
							ps.setUnitper1(su.getUnitper());
						}else if(2 == su.getSequence()){
							ps.setSpec2(su.getUnit());
							ps.setUnitper2(su.getUnitper());
						}else if(3 == su.getSequence()){
							ps.setSpec3(su.getUnit());
							ps.setUnitper3(su.getUnitper());
						}else if(4 == su.getSequence()){
							ps.setSpec4(su.getUnit());
							ps.setUnitper4(su.getUnitper());
						}
					}
				}
				//3.把查出来的放到map
				mapList.addAll(suList);
			}
		}
	}
	
	/***
	 * 查特定物资的多规格
	 * @param sp_code
	 * @return
	 */
	private List<SupplyUnit> findSupplyUnitBySpcode(String sp_code){
		SupplyUnit su = new SupplyUnit();
		su.setSp_code(sp_code);
		try {
			return supplyService.findAllSupplyUnit(su);
		} catch (CRUDException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/***
	 * 查今日的理论结存  未用
	 * @param session
	 * @param inventory
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getWzyue")
	@ResponseBody
	public Inventory getWzyue(HttpSession session,Inventory inventory) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		inventory.setAcct(acct);
		return inventoryMisService.getWzyue(inventory);
	}
	
	/***
	 * 查物资在此仓位的状态
	 * @param session
	 * @param inventory
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getPositnSupply")
	@ResponseBody
	public Inventory getPositnSupply(HttpSession session,Inventory inventory) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		inventory.setAcct(acct);
		return inventoryMisService.getPositnSupply(inventory);
	}
	
	/***
	 * 门店盘点Ajax加载  暂时未用
	 * @param session
	 * @param inventory
	 * @param currPage
	 * @param totalCount
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listAjax")
	@ResponseBody
	public Object listAjax(HttpSession session,Inventory inventory,String currPage,String totalCount) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		int intCurrPage = Integer.valueOf(currPage);
		int intTotalCount = Integer.valueOf(totalCount);
		Map<String,Object> modelMap = new HashMap<String, Object>();
		inventory.setStartNum(intCurrPage*PAGE_SIZE);
		inventory.setEndNum((intCurrPage+1)*PAGE_SIZE);
		String acct = session.getAttribute("ChoiceAcct").toString();
		inventory.setAcct(acct);
		Chkstoref cf = new Chkstoref();
		cf.setDept(inventory.getPositn().getCode());//盘点仓位
		cf.setWorkDate(DateFormat.getStringByDate(inventory.getDate(), "yyyy-MM-dd"));//营业日
		cf.setPantyp(inventory.getPantyp());//日盘/周盘/月盘
		cf.setState("0");//盘点未审核的
		Chkstoref cf1 = inventoryMisService.findChkstoref(cf);
		inventory.setSave(cf1 != null?"save":null);//判断是否有盘点的 sql用
//		inventory.setEnd(cf1 != null?cf1.getState():null);//界面判断按钮状态用
		List<Inventory> lists = inventoryMisService.findAllInventory(inventory);
		setSupplyUnit(lists, session);
		modelMap.put("inventoryList", lists);
		modelMap.put("currState", (intCurrPage+1)*PAGE_SIZE*1.0/intTotalCount);
		modelMap.put("currPage", currPage);
		if((intCurrPage+1)*PAGE_SIZE>=intTotalCount){
			modelMap.put("currState", 1);
			modelMap.put("over", "over");
		}
		return JSONObject.fromObject(modelMap).toString();
	}
	
	/***
	 * 保存盘点
	 * @param session
	 * @param inventory
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateInventory")
	@ResponseBody
	public String updateInventory(HttpSession session,Inventory inventory) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
				"MISBOH保存盘点:"+inventory.getPositn()==null?"":inventory.getPositn().getCode(),session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		String acct = session.getAttribute("ChoiceAcct").toString();
		String accountName = session.getAttribute("accountName").toString();
		inventory.setAcct(acct);
		//1.查找盘点数据
		Chkstoref cf = new Chkstoref();
		cf.setDept(inventory.getPositn().getCode());//盘点仓位
		cf.setWorkDate(DateFormat.getStringByDate(inventory.getDate(), "yyyy-MM-dd"));//营业日
		cf.setPantyp(inventory.getPantyp());//日盘/周盘/月盘
		cf.setState("0");//盘点未审核的
		Chkstoref cf1 = inventoryMisService.findChkstoref(cf);
		inventory.setSave(cf1 != null?"save":null);//判断是否有盘点的 sql用
		inventory.setYnprint("N");//所有的物资包括非打印的
		if("template".equals(inventory.getFromtyp()) || (cf1 != null && "template".equals(cf1.getFromtyp()))){//盘点的类型不会写非打印物资
			inventory.setFromtyp("template");
		}else{
			//2.将不显示的数据也要保存到盘点表
			List<Inventory> pdLists = inventory.getInventoryList();
			List<Inventory> lists = inventoryMisService.findAllInventory(inventory);
			Iterator<Inventory> car = lists.iterator();
			a:while(car.hasNext()){
				Inventory i1 = car.next();
				for(Inventory i2 : pdLists){
					if(i1.getSp_code().equals(i2.getSp_code())){//如果这个物资在盘点表存在，删掉之后继续下次循环
						car.remove();
						continue a;
					}
				}
			}
			pdLists.addAll(lists);
		}
		inventoryMisService.updateInventory(inventory,accountName);
		return "success";
	}
	
	/***
	 * 结束盘点
	 * @param session
	 * @param inventory
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/endInventory")
	@ResponseBody
	public String endInventory(HttpSession session, Inventory inventory) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CHECK,
				"MISBOH结束盘点:"+inventory.getPositn()==null?"":inventory.getPositn().getCode(),session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		try{
			session.setAttribute("time", 1);//此方法执行进度
			String acct=session.getAttribute("ChoiceAcct").toString();
			//1.先判断有没有结束盘点
			Positn p = new Positn();
			p.setAcct(acct);
			p.setCode(inventory.getPositn().getCode());
			Positn positn = positnService.findPositnByCode(p);
			session.setAttribute("time", 2);//此方法执行进度
			if("Y".equals(positn.getPd())){//已经结束
				return null;
			}
			//2.先更新仓位盘点状态
			p.setCode(null);
			p.setOldcode(inventory.getPositn().getCode());
			p.setPd("Y");
			positnService.updatePositn(p);//先更新pd状态为Y
			session.setAttribute("time", 3);//此方法执行进度
			String accountName = session.getAttribute("accountName").toString();
			String yearr = mainInfoService.findYearrList().get(0);//会计年wjf
			session.setAttribute("time", 5);//此方法执行进度
			inventory.setAcct(acct);
			inventory.setYearr(yearr);
			inventory.setSave("save");//查保存的
			inventory.setYnpd(null);//不查禁盘的
			inventory.setYnprint("N");//所有的物资包括非打印的
			int month = commonMISBOHService.getOnlyAccountMonth2(inventory.getDate());
			session.setAttribute("time", 10);
			inventory.setMonth(month);//得到当前盘点日期所在会计月
			List<Inventory> listInventory = inventoryMisService.findAllInventory(inventory);
			session.setAttribute("time", 20);
			MainInfo mainInfo = mainInfoService.findMainInfo(Integer.parseInt(yearr));
			session.setAttribute("time", 30);//此方法执行进度
			String efun = "getEdat"+month;
			Method eget = MainInfo.class.getDeclaredMethod(efun);
			Date edate = (Date)eget.invoke(mainInfo);
			inventory.setEdate(edate);
			if(edate.getTime() != inventory.getDate().getTime()){//先判断如果是盘点当月最后一天则直接在positnsupply取余额，无需去计算;如果不是会计月最后一天，则要计算
																//计算逻辑：当前月结存-（盘点日期到月末最后一天之间的结存）
				List<Inventory> listSupplyacct = inventoryMisService.findSupplyAcctTheMonth(inventory);
				session.setAttribute("time", 50);
				for(Inventory i1 : listSupplyacct){
					for(Inventory i2 : listInventory){
						if(i1.getSp_code().equals(i2.getSp_code())){
							i2.setCntbla(i2.getCntbla() - i1.getCntbla());
							i2.setCntubla(i2.getCntubla() - i1.getCntubla());
							i2.setAmtbla(i2.getAmtbla() - i1.getAmtbla());
							break;
						}
					}
				}
			}
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			setSupplyUnit(listInventory, session);
			//得到盘点主表数据
			Chkstoref cf = new Chkstoref();
			cf.setDept(inventory.getPositn().getCode());//盘点仓位
			cf.setWorkDate(DateFormat.getStringByDate(inventory.getDate(), "yyyy-MM-dd"));//营业日
			cf.setPantyp(inventory.getPantyp());//日盘/周盘/月盘
			cf.setState("0");//盘点未审核的
			Chkstoref cf1 = inventoryMisService.findChkstoref(cf);//最后一条
			session.setAttribute("time", 55);//此方法执行进度
			//特殊的  配置1 则所有的门店走先进先出
			if("1".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "firmOutWay"))){
				if("1203".equals(positn.getPcode())){
					return inventoryMisService.checkInventoryS(accountName,inventory,listInventory,cf1,thePositn.getCode());//生成盘盈入库 和盘亏出库
				}
			}else if("2".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "firmOutWay"))){//配置2 只有启用档口的店才走 先进先出
				if("1203".equals(positn.getPcode())){
					if("Y".equals(positn.getYnUseDept())){//启用档口的
						return inventoryMisService.checkInventoryS(accountName,inventory,listInventory,cf1,thePositn.getCode());//生成盘盈入库 和盘亏出库
					}
				}
			}
			return inventoryMisService.checkInventory(session,accountName,inventory,listInventory,cf1,thePositn.getCode());//生成盘亏出库
		}catch(Exception e){
			//有异常  盘点状态再改为N
			String acct=session.getAttribute("ChoiceAcct").toString();
			Positn p = new Positn();
			p.setAcct(acct);
			p.setOldcode(inventory.getPositn().getCode());
			p.setPd("N");
			positnService.updatePositn(p);
			return e.getLocalizedMessage();
		}
	}
	
	/***
	 * 获取审核盘点时间
	 * @param session
	 * @param inventory
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/endInventoryTime")
	@ResponseBody
	public int endInventoryTime(HttpSession session) throws Exception{
		return null == session.getAttribute("time")? 0 : Integer.parseInt(session.getAttribute("time").toString());
	}
	/***
	 * 删除盘点单
	 * @param session
	 * @param inventory
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delInventory")
	@ResponseBody
	public String delInventory(HttpSession session,Inventory inventory) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
				"MISBOH删除盘点:"+inventory.getPositn()==null?"":inventory.getPositn().getCode(),session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		String acct = session.getAttribute("ChoiceAcct").toString();
		inventory.setAcct(acct);
		inventoryMisService.delPositnsupplyMod(inventory);
		return "success";
	}
	
	/***
	 * 选择某天的盘点
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param chkstore
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/chooseInventory")
	public ModelAndView chooseInventory(ModelMap modelMap, HttpSession session, Page page, Chkstoreo chkstore) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		chkstore.setFirm(thePositn.getCode());
		List<Chkstoreo> chkstoreList = inventoryMisService.findMdPandian(chkstore, page);
		modelMap.put("chkstoreList", chkstoreList);
		modelMap.put("pageobj", page);
		modelMap.put("chkstore", chkstore);
		return new ModelAndView(InventoryMisConstants.CHOOSEINVENTORY, modelMap);
	}
	
	/**
	 * 导出
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportInventory")
	@ResponseBody
	public boolean exportInventory(HttpServletResponse response,HttpServletRequest request,HttpSession session,Inventory inventory) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if("Y".equals(inventory.getMonthpan())){//如果是月盘单独节点的 默认月盘且不可改
			inventory.setPantyp("monthpan");
		}
		String acct = session.getAttribute("ChoiceAcct").toString();
		inventory.setAcct(acct);
		inventory.setYearr(mainInfoService.findYearrList().get(0));
		List<Inventory> lists = null;
		//1.如果勾选了仅查询，则查盘点表
		if("Y".equals(inventory.getOnlyQuery())){
			lists = inventoryMisService.findAllInventoryEnd(inventory);
			setSupplyUnit(lists, session);//设置多规格
		}else{
			Chkstoref cf = new Chkstoref();
			cf.setDept(inventory.getPositn().getCode());//盘点仓位
			cf.setWorkDate(DateFormat.getStringByDate(inventory.getDate(), "yyyy-MM-dd"));//营业日
			cf.setPantyp(inventory.getPantyp());//日盘/周盘/月盘
			cf.setState("0");//盘点未审核的
			Chkstoref cf1 = inventoryMisService.findChkstoref(cf);
			inventory.setSave(cf1 != null?"save":null);//判断显示0的还是显示保存的
			lists = inventoryMisService.findAllInventory(inventory);
			setSupplyUnit(lists, session);
		}
		String fileName = "门店盘点表";
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");
		if(null == inventory.getPositn().getDes()){
			inventory.setPositn(positnService.findPositnByCode(inventory.getPositn()));
		}
		if("Y".equals(DDT.isPdUnit2)){//不走九毛九的走多规格
			return inventoryMisService.exportInventoryUnit2(response.getOutputStream(),lists,inventory);
		}
		return inventoryMisService.exportInventory(response.getOutputStream(),lists,inventory);
	}
	
	/***
	 * 打印盘点表
	 * @author wjf
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param inventory
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/printInventory")
	public ModelAndView printInventory(ModelMap modelMap, Page pager, HttpSession session,String type, Inventory inventory,boolean blank)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		inventory.setYnprint(null);//所有的物资包括非打印的
		if("Y".equals(inventory.getMonthpan())){//如果是月盘单独节点的 默认月盘且不可改
			inventory.setPantyp("monthpan");
		}
		String acct = session.getAttribute("ChoiceAcct").toString();
		inventory.setAcct(acct);
		inventory.setYearr(mainInfoService.findYearrList().get(0));
		List<Inventory> lists = null;
		//1.如果勾选了仅查询，则查盘点表
		if("Y".equals(inventory.getOnlyQuery())){
			lists = inventoryMisService.findAllInventoryEnd(inventory);
			setSupplyUnit(lists, session);//设置多规格
		}else{
			Chkstoref cf = new Chkstoref();
			cf.setDept(inventory.getPositn().getCode());//盘点仓位
			cf.setWorkDate(DateFormat.getStringByDate(inventory.getDate(), "yyyy-MM-dd"));//营业日
			cf.setPantyp(inventory.getPantyp());//日盘/周盘/月盘
			cf.setState("0");//盘点未审核的
			Chkstoref cf1 = inventoryMisService.findChkstoref(cf);
			inventory.setSave(cf1 != null?"save":null);//判断显示0的还是显示保存的
			lists = inventoryMisService.findAllInventory(inventory);
			setSupplyUnit(lists, session);
		}
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,String> params = new HashMap<String,String>();
		params.put("positn.code",inventory.getPositn().getCode());
		params.put("date",DateFormat.getStringByDate(inventory.getDate(), "yyyy-MM-dd"));
		params.put("pantyp",inventory.getPantyp());
		params.put("blank",blank+"");
		params.put("firm",inventory.getFirm());
		modelMap.put("actionMap", params);
		
		modelMap.put("List",lists);
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "门店盘点表");
	    if(null == inventory.getPositn().getDes()){
			inventory.setPositn(positnService.findPositnByCode(inventory.getPositn()));
		}
	    parameters.put("positn",inventory.getPositn().getDes());
	    parameters.put("date", DateFormat.formatDate(inventory.getDate(), "yyyy-MM-dd"));
	    parameters.put("pantyp","daypan".equals(inventory.getPantyp())?"日盘":"weekpan".equals(inventory.getPantyp())?"周盘":"月盘");
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/inventoryMis/printInventory.do");//传入回调路径
	 	Map<String,String> rs = new HashMap<String,String>();
	 	if("Y".equals(DDT.isPdUnit2)){//不走九毛九的走多规格
	 		if("Y".equals(DDT.isDistributionUnit)){//是否根据配送单位盘点
	 			if(blank){
	 				rs = ReadReportUrl.redReportUrl(type,InventoryMisConstants.REPORT_PRINT_URL_INVENTORY_MIS_BLANK_DISUNIT,InventoryMisConstants.REPORT_PRINT_URL_INVENTORY_MIS_BLANK_DISUNIT);//判断跳转路径
	 			}else{
	 				rs=ReadReportUrl.redReportUrl(type,InventoryMisConstants.REPORT_PRINT_URL_INVENTORY_MIS_DISUNIT,InventoryMisConstants.REPORT_PRINT_URL_INVENTORY_MIS_DISUNIT);//判断跳转路径
	 			}
	 		}else{
	 			if(blank){
	 				rs = ReadReportUrl.redReportUrl(type,InventoryMisConstants.REPORT_PRINT_URL_INVENTORY_MIS_BLANK_UNIT2,InventoryMisConstants.REPORT_PRINT_URL_INVENTORY_MIS_BLANK_UNIT2);//判断跳转路径
	 			}else{
	 				rs=ReadReportUrl.redReportUrl(type,InventoryMisConstants.REPORT_PRINT_URL_INVENTORY_MIS_UNIT2,InventoryMisConstants.REPORT_PRINT_URL_INVENTORY_MIS_UNIT2);//判断跳转路径
	 			}
	 		}
	 	}else{//正常情况下多规格
	 		if(blank){
	 			rs=ReadReportUrl.redReportUrl(type,InventoryMisConstants.REPORT_PRINT_URL_INVENTORY_MIS_BLANK,InventoryMisConstants.REPORT_PRINT_URL_INVENTORY_MIS_BLANK);//判断跳转路径
	 		}else{
	 			rs=ReadReportUrl.redReportUrl(type,InventoryMisConstants.REPORT_PRINT_URL_INVENTORY_MIS,InventoryMisConstants.REPORT_PRINT_URL_INVENTORY_MIS);//判断跳转路径
	 		}
	 	}
	 	modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
	 	return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/***
	 * 盘点模板调用
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param chkstodemo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addInventoryDemo")
	public ModelAndView addInventoryDemo(ModelMap modelMap, HttpSession session, InventoryDemom inventoryDemom) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		inventoryDemom.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<InventoryDemom> listChkstoDemo = inventoryMisService.listInventoryDemom(inventoryDemom);//查这个仓位能用的模板
		if(listChkstoDemo.size() > 0) {
			InventoryDemod inventoryDemod = new InventoryDemod();
			inventoryDemod.setChkstodemono(listChkstoDemo.get(0).getChkstodemono());
			inventoryDemod.setMemo(inventoryDemom.getFirm());
			if("Y".equals(DDT.isDistributionUnit)){//查询配送单位
				inventoryDemod.setId(1);
			}
			modelMap.put("chkstodemoList", inventoryMisService.listInventoryDemod(inventoryDemod));
		}
		modelMap.put("firm", inventoryDemom.getFirm());
		modelMap.put("chkstodemono", inventoryDemom.getChkstodemono());
		modelMap.put("status", inventoryDemom.getStatus());
		//最后查标题栏
		inventoryDemom.setChkstodemono(null);
		modelMap.put("listTitle", inventoryMisService.listInventoryDemom(inventoryDemom));//改为查带适用分店权限的2014.11.18wjf
		return new ModelAndView(InventoryMisConstants.ADDINVENTORYDEMO, modelMap);
	}
	
	/***
	 * 盘点模板调用2
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param chkstodemo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addInventoryDemo1")
	public ModelAndView addInventoryDemo1(ModelMap modelMap, HttpSession session, InventoryDemom inventoryDemom) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		inventoryDemom.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<InventoryDemom> listChkstoDemo = inventoryMisService.listInventoryDemom(inventoryDemom);//查这个仓位能用的模板
		if(listChkstoDemo.size() > 0) {
			InventoryDemod inventoryDemod = new InventoryDemod();
			inventoryDemod.setChkstodemono(listChkstoDemo.get(0).getChkstodemono());
			inventoryDemod.setMemo(thePositn.getCode());
			List<Inventory> listInventory = inventoryMisService.listInventoryDemod1(inventoryDemod);
			setSupplyUnit(listInventory, session);
			modelMap.put("chkstodemoList", listInventory);
		}
		modelMap.put("firm", inventoryDemom.getFirm());
		modelMap.put("chkstodemono", inventoryDemom.getChkstodemono());
		modelMap.put("status", inventoryDemom.getStatus());
		//最后查标题栏
		inventoryDemom.setChkstodemono(null);
		modelMap.put("listTitle", inventoryMisService.listInventoryDemom(inventoryDemom));//改为查带适用分店权限的2014.11.18wjf
		return new ModelAndView(InventoryMisConstants.ADDINVENTORYDEMO1, modelMap);
	}
	
	/***
	 * 判断是否有保存但未结束的盘点数据
	 * @param session
	 * @param inventory
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkIsSaveData")
	@ResponseBody
	public int checkIsSaveData(HttpSession session,Inventory inventory) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
				"MISBOH判断是否有保存但未结束的盘点数据:"+inventory.getPositn()==null?"":inventory.getPositn().getCode(),session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		Chkstoref cf = new Chkstoref();
		cf.setDept(inventory.getPositn().getCode());//盘点仓位
		cf.setWorkDate(DateFormat.getStringByDate(inventory.getDate(), "yyyy-MM-dd"));//营业日
		cf.setPantyp(inventory.getPantyp());//日盘/周盘/月盘
		cf.setState("0");//盘点未审核的
		Chkstoref cf1 = inventoryMisService.findChkstoref(cf);
		if(cf1 == null){
			return 0;
		}
		return 1;
	}
	
	/**
	 * 跳转到导入页面   导入 wjf
	 */
	@RequestMapping("/importInventory")
	public ModelAndView importChkstom(ModelMap modelMap,String positn) throws Exception{
		modelMap.put("positn", positn);
		return new ModelAndView(InventoryMisConstants.IMPORT_INVENTORY,modelMap);
	}
	
	/**
	 * 先上传excel
	 */
	@RequestMapping(value = "/loadExcel", method = RequestMethod.POST)
	public ModelAndView loadExcel(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap,String positn) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String realFilePath = chkstomMisService.upload(request, response,positn+"inventory.xls");
		modelMap.put("realFilePath", realFilePath);
		modelMap.put("positn", positn);
		return new ModelAndView(InventoryMisConstants.IMPORT_RESULT, modelMap);
	}
	
	/**
	 * 导入盘点  wjf
	 */
	@RequestMapping(value = "/importExcel")
	public ModelAndView importExcel(HttpSession session, ModelMap modelMap, @RequestParam String realFilePath,Inventory inventory)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.IMPORT,
				"Misboh盘点导入 ",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		commonMISBOHService.addLogs(logd);
		String positnCode = "";
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			positnCode = thePositn.getCode();
		}
		if("Y".equals(DDT.isPdUnit2)){//走九毛九盘点的
			String isDistributionUnit = DDT.isDistributionUnit;
			boolean checkResult = ReadInventoryMisExcel.check(realFilePath, commonMISBOHService, positnCode);
			if (checkResult) {//成功
				List<Inventory> lists = ReadInventoryMisExcel.psList;
	//			setSupplyUnit(psList,positnCode);//设置多规格
				//根据录入数量得到盘点总数量
				for(Inventory ps : lists){
					double sumCnt = ps.getCnt1();
					if("Y".equals(isDistributionUnit)){
						if(ps.getDisunitper() != 0){
							sumCnt += ps.getCnt3()/ps.getDisunitper();
						}
					}else{
						if(ps.getUnitper3() != 0){
							sumCnt += ps.getCnt3()/ps.getUnitper3();
						}
					}
					ps.setStockcnt(sumCnt);
					ps.setCnt4(sumCnt*ps.getUnitper4());//盘点单位
				}
				
				String acct = session.getAttribute("ChoiceAcct").toString();
				String yearr = mainInfoService.findYearrList().get(0);
				inventory.setAcct(acct);
				inventory.setYearr(yearr);
				inventory.setSave("imp");//页面判断按钮显示隐藏用
				modelMap.put("inventoryList", lists);//查询盘点列表
				modelMap.put("inventory", inventory);
			} else {
				List<String> errorList = ReadInventoryMisExcel.errorList;
				modelMap.put("importError", errorList);
			}
			FileWorked.deleteFile(realFilePath);//删除上传后的的文件
			if("Y".equals(isDistributionUnit)){//是否根据配送单位盘点
				return new ModelAndView(InventoryMisConstants.LIST_INVENTORY_DISUNIT, modelMap);
			}
			return new ModelAndView(InventoryMisConstants.LIST_INVENTORY_UNIT2, modelMap);
		}else{
			boolean checkResult = ReadInventoryMisExcel1.check(realFilePath, commonMISBOHService, positnCode);
			if (checkResult) {//成功
				List<Inventory> lists = ReadInventoryMisExcel1.psList;
				setSupplyUnit(lists, session);//设置多规格
				//根据录入数量得到盘点总数量
				for(Inventory ps : lists){
					double sumCnt = ps.getCnt1();
					if(null != ps.getSpec2() && ps.getUnitper2() != 0){
						sumCnt += ps.getCnt2()/ps.getUnitper2();
					}
					if(null != ps.getSpec3() && ps.getUnitper3() != 0){
						sumCnt += ps.getCnt3()/ps.getUnitper3();
					}
					if(null != ps.getSpec4() && ps.getUnitper4() != 0){
						sumCnt += ps.getCnt4()/ps.getUnitper4();
					}
					ps.setStockcnt(sumCnt);
				}
				String acct = session.getAttribute("ChoiceAcct").toString();
				Positn p = new Positn();
				p.setAcct(acct);
				p.setCode(inventory.getPositn().getCode());
				p = positnService.findPositnByCode(p);
				inventory.setPositn(p);
				String yearr = mainInfoService.findYearrList().get(0);
				inventory.setAcct(acct);
				inventory.setYearr(yearr);
				inventory.setSave("imp");//页面判断按钮显示隐藏用
				modelMap.put("inventoryList", lists);//查询盘点列表
				modelMap.put("inventory", inventory);
				modelMap.put("positnTyp", p.getPcode());//盘点的仓位类型  页面提示用
				modelMap.put("firmOutWay", ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "firmOutWay"));//提示根据配置
				modelMap.put("nowDate", new Date());//系统当前日期，盘点的时候不能选择大于当前日期20160803
			} else {
				List<String> errorList = ReadInventoryMisExcel1.errorList;
				modelMap.put("importError", errorList);
			}
			FileWorked.deleteFile(realFilePath);//删除上传后的的文件
		}
		return new ModelAndView(InventoryMisConstants.LIST_INVENTORY, modelMap);
	}
	
	/***
	 * 判断是否有未验货，未审核的单据
	 * @param session
	 * @param dat
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkBeforeInventory")
	@ResponseBody
	public String checkBeforeInventory(HttpSession session,Date edate) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,String> result = new HashMap<String,String>();
		String acct = session.getAttribute("ChoiceAcct").toString();
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null != thePositn) {
			String positnCode = thePositn.getCode();
			//0.如果是九毛九的盘点，一天只能有一个盘点单
			if("Y".equals(DDT.isPdUnit2)){//九毛九的盘点
				Chkstoref cf = new Chkstoref();
				cf.setDept(positnCode);//盘点仓位
				cf.setWorkDate(DateFormat.getStringByDate(edate, "yyyy-MM-dd"));//营业日
				cf.setState("1");
				Chkstoref cf1 = inventoryMisService.findChkstoref(cf);
				if(null != cf1){
					result.put("pr", "-10");
					String pantyp = "daypan".equals(cf1.getPantyp()) ? "日盘单" : 
						("weekpan".equals(cf1.getPantyp()) ? "周盘单" : ("monthpan".equals(cf1.getPantyp()) ? "月盘单" : "库存调整单"));
					result.put("error", "当前盘点日期已存在一张"+pantyp+"，不能重复盘点。");
					return JSONObject.fromObject(result).toString();
				}
			}
			int year = Integer.parseInt(mainInfoService.findYearrList().get(0));
			MainInfo mainInfo= mainInfoService.findMainInfo(year);
			String monthh = "1";
			if(!"Y".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH,"ynCenterMonthEnd"))){//如果启用门店月结
				Positn positn = positnService.findPositnByCode(thePositn);
				if(null != positn.getMonthh() && !"".equals(positn.getMonthh())){
					monthh = positn.getMonthh();
				}
			}else{
				monthh = mainInfo.getMonthh();
			}
			String bdat = "getBdat"+monthh;
			Method getBdat = MainInfo.class.getDeclaredMethod(bdat);
			Date bdate = (Date)getBdat.invoke(mainInfo);
			//1.有没有直配，配送，调拨的未验货
			//1.查询当月直配未验货单据数
			String bdateString = DateFormat.getStringByDate(bdate, "yyyy-MM-dd");
			String edateString = DateFormat.getStringByDate(edate, "yyyy-MM-dd");
			if("Y".equals(DDT.isDireInsByBill)){//九毛九的直配验货单据
				ArrivalmMis arrivalmMis = new ArrivalmMis();
				arrivalmMis.setBdate(bdateString);
				arrivalmMis.setEdate(edateString);
				arrivalmMis.setPk_org(positnCode);
				int direCount = inventoryMisService.findArrilvalmCount(arrivalmMis);
				if(direCount != 0){
					result.put("pr", "-1");
					result.put("error", "当前会计月开始日期"+bdateString+"到当前盘点日期之间您有"+direCount+"张直配单据未验货，不能进行盘点。");
					return JSONObject.fromObject(result).toString();
				}
			}else{
				Dis dis = new Dis();
				dis.setAcct(acct);
				dis.setFirmCode(positnCode);
				dis.setInout(DDT.DIRE);
				dis.setBdat(bdate);
				dis.setEdat(edate);
				dis.setYndo("NO");
				int direCount = inventoryMisService.findAllDisCount(dis);
				if(direCount != 0){
					result.put("pr", "-1");
					result.put("error", "当前会计月开始日期"+bdateString+"到当前盘点日期之间您有"+direCount+"个直配物资未验货，不能进行盘点。");
					return JSONObject.fromObject(result).toString();
				}
			}
			//2.查询统配未验货单据数
			SupplyAcct sa = new SupplyAcct();
			sa.setAcct(acct);
			sa.setBdate(bdateString);
			sa.setEdate(edateString);
			sa.setFirmcode(positnCode);
			sa.setChkyh("no");
			if("Y".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "isInsByBill"))){//验货根据单据
				int outCount = inventoryMisService.findOutBillCount(sa);
				if(outCount != 0){
					result.put("pr", "-2");
					result.put("error", "当前会计月开始日期"+bdateString+"到当前盘点日期之间您有"+outCount+"张配送单据未验货，不能进行盘点。");
					return JSONObject.fromObject(result).toString();
				}
			}else{
				int outCount = inventoryMisService.findOutCount(sa);
				if(outCount != 0){
					result.put("pr", "-2");
					result.put("error", "当前会计月开始日期"+bdateString+"到当前盘点日期之间您有"+outCount+"个配送物资未验货，不能进行盘点。");
					return JSONObject.fromObject(result).toString();
				}
			}
			//3.查询调拨未验货单据数
			int dbCount = inventoryMisService.findOutDbCount(sa);
			if(dbCount != 0){
				result.put("pr", "-3");
				result.put("error", "当前会计月开始日期"+bdateString+"到当前盘点日期之间您有"+dbCount+"个调拨物资未验货，不能进行盘点。");
				return JSONObject.fromObject(result).toString();
			}
			//2.报损单有没有确认
			
			//3.单据有未审核的吗
			//当月有多少入库单未审核
			Map<String,Object> map = new HashMap<String,Object>();
			Chkinm chkinm = new Chkinm();
			chkinm.setInout("in");
			chkinm.setPositn(thePositn);
			chkinm.setMaded(bdate);
			map.put("chkinm", chkinm);
			map.put("madedEnd", edate);
			int chkinmCount = inventoryMisService.findChkinmCount(map);
			if(chkinmCount != 0){
				result.put("pr", "-4");
				result.put("error", "当前会计月开始日期"+bdateString+"到当前盘点日期之间您有"+chkinmCount+"张入库单未审核，不能进行盘点。");
				return JSONObject.fromObject(result).toString();
			}
			//当月多少直发单未审核
			chkinm.setInout("inout");
			int chkinmzfCount = inventoryMisService.findChkinmCount(map);
			if(chkinmzfCount != 0){
				result.put("pr", "-5");
				result.put("error", "当前会计月开始日期"+bdateString+"到当前盘点日期之间您有"+chkinmzfCount+"张直发单未审核，不能进行盘点。");
				return JSONObject.fromObject(result).toString();
			}
			//当月有多少出库单未审核
			Map<String,Object> map1 = new HashMap<String,Object>();
			map1.put("bdat", bdate);
			map1.put("edat", edate);
			Chkoutm chkoutm = new Chkoutm();
			chkoutm.setAcct(acct);
			Positn p = new Positn();
			p.setUcode(positnCode);
			List<Positn> list = positnService.findPositnSuperNOPage(p);//查询该分店的档口
			List<String> listPositn = new ArrayList<String>();
			for(Positn posi:list){
				listPositn.add(posi.getCode());
			}
			listPositn.add(thePositn.getCode());//加上分店本身
			chkoutm.setListPositn(listPositn);
			map1.put("chkoutm", chkoutm);
			map1.put("db", "ck");
			int chkoutmCount = inventoryMisService.findChkoutmCount(map1);
			if(chkoutmCount != 0){
				result.put("pr", "-6");
				result.put("error", "当前会计月开始日期"+bdateString+"到当前盘点日期之间您有"+chkoutmCount+"张出库单未审核，不能进行盘点。");
				return JSONObject.fromObject(result).toString();
			}
			map1.put("db", "db");
			//当月调拨出库未审核的
			int chkoutmdbCount = inventoryMisService.findChkoutmCount(map1);
			if(chkoutmdbCount != 0){
				result.put("pr", "-7");
				result.put("error", "当前会计月开始日期"+bdateString+"到当前盘点日期之间您有"+chkoutmdbCount+"张调拨出库单未审核，不能进行盘点。");
				return JSONObject.fromObject(result).toString();
			}
		}
		result.put("pr", "1");
		return JSONObject.fromObject(result).toString();
	}
	
	/************************************************手机盘点*************************************************************/
	
	/***
	 * 手机门店盘点Ajax加载
	 * @param session
	 * @param inventory
	 * @param currPage
	 * @param totalCount
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findInventoryWAP")
	@ResponseBody
	public Object findInventoryWAP(Inventory inventory, String jsonpcallback) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		Chkstoref cf = new Chkstoref();
		cf.setDept(inventory.getPositn().getCode());//盘点仓位
		cf.setWorkDate(DateFormat.getStringByDate(inventory.getDate(), "yyyy-MM-dd"));//营业日
		cf.setPantyp(inventory.getPantyp());//日盘/周盘/月盘
		cf.setState("0");//盘点未审核的
		Chkstoref cf1 = inventoryMisService.findChkstoref(cf);
		inventory.setSave(cf1 != null?"save":null);//判断是否有盘点的 sql用
		
		String yearr = mainInfoService.findYearrList().get(0);//会计年
		inventory.setYearr(yearr);
//		if("Y".equals(inventory.getMonthpan())){//如果是月盘单独节点的，日期是当前月所在会计月的最后一天
//			MainInfo mainInfo = mainInfoService.findMainInfo(Integer.parseInt(yearr));
//			int month = commonMISBOHService.getOnlyAccountMonth2(new Date());
//			String fun = "getEdat"+month;
//			Method get = MainInfo.class.getDeclaredMethod(fun);
//			Date date = (Date)get.invoke(mainInfo);
//			inventory.setDate(date);
//		}
		List<Inventory> lists = inventoryMisService.findAllInventory(inventory);
		map.put("inventoryList", lists);
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/**
	 * 跳转到盘点界面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
//	@RequestMapping("/addInventoryWAP")
//	public ModelAndView addInventory(ModelMap modelMap, String pantyp, String date) throws Exception {
//		modelMap.put("pantyp", pantyp);
//		modelMap.put("date", date);
//		return new ModelAndView(InventoryMisConstants.ADD_INVENTORY, modelMap);
//	}
	
	/***
	 * 保存盘点
	 * @param session
	 * @param inventory
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateInventoryWAP")
	@ResponseBody
	public Object updateInventoryWap(String accountName, Inventory inventory) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		//1.查找盘点数据
		Chkstoref cf = new Chkstoref();
		cf.setDept(inventory.getPositn().getCode());//盘点仓位
		cf.setWorkDate(DateFormat.getStringByDate(inventory.getDate(), "yyyy-MM-dd"));//营业日
		cf.setPantyp(inventory.getPantyp());//日盘/周盘/月盘
		cf.setState("0");//盘点未审核的
		Chkstoref cf1 = inventoryMisService.findChkstoref(cf);
		inventory.setSave(cf1 != null?"save":null);//判断是否有盘点的 sql用
		inventory.setYnprint("N");//所有的物资包括非打印的
		List<Inventory> lists = inventoryMisService.findAllInventory(inventory);
		//2.将不显示的数据也要保存到盘点表
		List<Inventory> pdLists = inventory.getInventoryList();
		Iterator<Inventory> car = lists.iterator();
		a:while(car.hasNext()){
			Inventory i1 = car.next();
			for(Inventory i2 : pdLists){
				if(i1.getSp_code().equals(i2.getSp_code())){//如果这个物资在盘点表存在，删掉之后继续下次循环
					car.remove();
					continue a;
				}
			}
		}
		pdLists.addAll(lists);
		inventoryMisService.updateInventory(inventory,accountName);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("pr", 1);
		map.put("firmOutWay", ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "firmOutWay"));//提示根据配置
		return JSONObject.fromObject(map).toString();
	}
	
	/***
	 * 结束盘点
	 * @param session
	 * @param inventory
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/endInventoryWAP")
	@ResponseBody
	public Object endInventoryWap(String accountName, Inventory inventory, String jsonpcallback) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		try{
			//1.先判断有没有结束盘点
			Positn p = new Positn();
			p.setAcct(inventory.getAcct());
			p.setCode(inventory.getPositn().getCode());
			Positn positn = positnService.findPositnByCode(p);
			if("Y".equals(positn.getPd())){//已经结束
				return null;
			}
			//2.先更新仓位盘点状态
			p.setCode(null);
			p.setOldcode(inventory.getPositn().getCode());
			p.setPd("Y");
			positnService.updatePositn(p);//先更新pd状态为Y
			String yearr = mainInfoService.findYearrList().get(0);//会计年wjf
			inventory.setAcct(inventory.getAcct());
			inventory.setYearr(yearr);
			inventory.setSave("save");//查保存的
			inventory.setYnpd(null);//不查禁盘的
			inventory.setYnprint("N");//所有的物资包括非打印的
			List<Inventory> listInventory=inventoryMisService.findAllInventory(inventory);
			//得到盘点主表数据
			Chkstoref cf = new Chkstoref();
			cf.setDept(inventory.getPositn().getCode());//盘点仓位
			cf.setWorkDate(DateFormat.getStringByDate(inventory.getDate(), "yyyy-MM-dd"));//营业日
			cf.setPantyp(inventory.getPantyp());//日盘/周盘/月盘
			cf.setState("0");//盘点未审核的
			Chkstoref cf1 = inventoryMisService.findChkstoref(cf);//最后一条
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("pr", 1);
			//特殊的  配置1 则所有的门店走先进先出
			if("1".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "firmOutWay"))){
				if("1203".equals(positn.getPcode())){
					inventoryMisService.checkInventoryS(accountName,inventory,listInventory,cf1,p.getCode());//生成盘盈入库 和盘亏出库
					return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
				}
			}else if("2".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "firmOutWay"))){//配置2 只有启用档口的店才走 先进先出
				if("1203".equals(positn.getPcode())){
					if("Y".equals(positn.getYnUseDept())){//启用档口的
						inventoryMisService.checkInventoryS(accountName,inventory,listInventory,cf1,p.getCode());//生成盘盈入库 和盘亏出库
						return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
					}
				}
			}
			inventoryMisService.checkInventory(null, accountName,inventory,listInventory,cf1,p.getCode());//生成盘亏出库
			return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
		}catch(Exception e){
			//有异常  盘点状态再改为N
			Positn p = new Positn();
			p.setAcct(inventory.getAcct());
			p.setOldcode(inventory.getPositn().getCode());
			p.setPd("N");
			positnService.updatePositn(p);
			return e.getLocalizedMessage();
		}
	}
}
