package com.choice.misboh.web.controller.common;

import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.redis.RedisConfig;
import com.choice.framework.util.CacheUtil;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.constants.common.CommonMisBohConstants;
import com.choice.misboh.domain.BaseRecord.MisBohOperator;
import com.choice.misboh.domain.BaseRecord.PubItem;
import com.choice.misboh.domain.BaseRecord.PubitemTeam;
import com.choice.misboh.domain.BaseRecord.PubitemTeamDetail;
import com.choice.misboh.domain.common.CommonMethod;
import com.choice.misboh.domain.costReduction.MisSupply;
import com.choice.misboh.domain.inventory.Chkstoref;
import com.choice.misboh.domain.inventory.PositnSupply;
import com.choice.misboh.domain.store.Store;
import com.choice.misboh.domain.store.StoreDept;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.misboh.service.myDesk.MyDeskMisbohService;
import com.choice.orientationSys.constants.StringConstant;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Spprice;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.domain.SupplyUnit;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.DeliverService;
import com.choice.scm.service.GrpTypService;
import com.choice.scm.service.PositnService;

/**
 * 公共方法
 * @author wangchao
 *
 */
@Controller
@RequestMapping("misbohcommon")
public class CommonMISBOHController {
	
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	@Autowired
	private PositnService positnService;
	@Autowired
	private GrpTypService grpTypService;
	@Autowired
	private CodeDesService codeDesService;
	@Autowired
	private DeliverService deliverService;
	@Autowired
	private MyDeskMisbohService myDeskMisbohService;
	
	/**
	 * 获取当前店铺的预估方式
	 * @param store
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/getVplantyp")
	public String getVplantyp(Store store,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//获取登录用户的编码 
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		store.setVcode(thePositn.getCode());
		return ValueUtil.getStringValue(commonMISBOHService.getStore(store).getVplantyp());
	}

	/**
	 * 当前店铺是否适用虚拟物料
	 * @param store
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/getVxnsupply")
	public String getVxnsupply(Store store,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//获取登录用户的编码 
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		store.setVcode(thePositn.getCode());
		return ValueUtil.getStringValue(commonMISBOHService.getStore(store).getVxnsupply());
	}

	/**
	 * 当前店铺是否适用虚拟物料
	 * @param store
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/getVisuseschedule")
	public String getVisuseschedule(Store store,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//获取登录用户的编码 
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		store.setVcode(thePositn.getCode());
		return ValueUtil.getStringValue(commonMISBOHService.getStore(store).getVisuseschedule());
	}
	
	/**
	 * 描述：验证编码是否重复
	 * @author 马振
	 * 创建时间：2015-4-16 下午4:56:10
	 * @param commonMethod
	 * @return
	 */
	@RequestMapping(value = "/validateVcode")
	@ResponseBody
	public String validateVcode(CommonMethod commonMethod){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return commonMISBOHService.validateVcode(commonMethod);
	}
	
	/**
	 * 描述：获取功能新增时默认的编码    
	 * @author 马振
	 * 创建时间：2015-4-16 下午4:56:52
	 * @param tablename  	表名
	 * @param namefield  	查询的编码字段
	 * @param tg_name    	赋值的页面id
	 * @param defaultvalue	如果查询为空默认返回的第一个值
	 * @param wheresql		其他的查询条件
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getDefaultVcode")
	@ResponseBody
	public String getDefaultVcode(String tablename,String namefield,
			String tg_name, String defaultvalue, String wheresql) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,String> condition = new HashMap<String, String>();
		condition.put("tablename", tablename);
		condition.put("namefield", namefield);
		condition.put("tg_name", tg_name);
		condition.put("defaultvalue", defaultvalue);
		condition.put("wheresql", wheresql);
		return commonMISBOHService.getDefaultVcode(condition);
	}
	
	/**
	 * 描述：新增时获取默认的排序
	 * @author 马振
	 * 创建时间：2015-8-25 上午9:03:04
	 * @param commonMethod
	 * @return
	 */
	@RequestMapping(value = "/getSortNo")
	@ResponseBody
	public String getSortNo(CommonMethod commonMethod){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return commonMISBOHService.getSortNo(commonMethod);
	}
	
	/********************************************供应链 用:物资，仓位，供应商****************************************************/
	
	/***
	 * 查找物资前10条公用方法  走分店物资属性
	 * @author wjf
	 * @param supply
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findSupplyTop10", method=RequestMethod.POST)
	@ResponseBody
	@SuppressWarnings("unchecked")
	public  List<MisSupply> findSupplyTop10(MisSupply supply, HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<MisSupply> listSupply = null;
		String opencluster = RedisConfig.getString("opencluster");
		if(null != opencluster && "1".equals(opencluster)){
			listSupply = (List<MisSupply>)session.getAttribute("misSupply_cache");
		} else {
			Positn accountPositn = (Positn)session.getAttribute("accountPositn");
			String positnCode = accountPositn.getCode();
			supply.setSp_position(positnCode);//当前分店
			String acct = session.getAttribute("ChoiceAcct").toString();
			String pk_group = null == session.getAttribute("pk_group") ? "" : session.getAttribute("pk_group").toString();
			supply.setAcct(acct);//帐套
			CacheUtil cacheUtil = CacheUtil.getInstance();
			listSupply = (List<MisSupply>)cacheUtil.get("misSupply_cache", pk_group+positnCode);	
			if(null == listSupply){//如果是空，加入缓存   
				MisSupply s = new MisSupply();
				s.setAcct(acct);
				s.setSp_position(positnCode);
				s.setOrderBy("length(s.sp_init)");
				listSupply = commonMISBOHService.findAllSupply(s);
				cacheUtil.put("misSupply_cache", pk_group+positnCode, listSupply);	
			}
		}
		//从list中取出前十条   
		List<MisSupply> supplyList = new ArrayList<MisSupply>();//临时list
		for (MisSupply s : listSupply) {
			if("Y".equals(supply.getYnsto())){//如果查可申购的
				if(!"Y".equals(s.getYnsto())){
					continue;
				}
			}
			if("Y".equals(supply.getYnpd())){//如果查可盘点的
				if(!"Y".equals(s.getYnpd())){
					continue;
				}
			}
			if("N".equals(supply.getEx())){//如果不查半成品的
				if("Y".equals(s.getEx())){
					continue;
				}
			}
			if("Y".equals(supply.getEx())){//如果只查半成品的
				if("N".equals(s.getEx())){
					continue;
				}
			}
			if(null != supply.getTyp_eas() && !"".equals(supply.getTyp_eas())){//如果查某一个报货类别的
				if(!supply.getTyp_eas().equals(s.getTyp_eas())){
					continue;
				}
			}
			if(supplyList.size()==10){
				break;
			}
			if(null != supply.getSp_code()){
				if(s.getSp_code().contains(supply.getSp_code()) || s.getSp_name().contains(supply.getSp_code()) || s.getSp_init().contains(supply.getSp_code()))
					supplyList.add(s);
			}
		}  
//		List<MisSupply> supplyList = commonMISBOHService.findSupplyTop10(supply);
		if("Y".equals(supply.getSpec1())){//启用多规格的，期初，盘点 查物资才走
			List<SupplyUnit> suList = commonMISBOHService.findAllSupplyUnit(supplyList);
			for(MisSupply s : supplyList){
				for(SupplyUnit su : suList){
					if(s.getSp_code().equals(su.getSp_code())){
						if(1 == su.getSequence()){
							s.setSpec1(su.getUnit());
							s.setSpecUnitper1(su.getUnitper());
						}else if(2 == su.getSequence()){
							s.setSpec2(su.getUnit());
							s.setSpecUnitper2(su.getUnitper());
						}else if(3 == su.getSequence()){
							s.setSpec3(su.getUnit());
							s.setSpecUnitper3(su.getUnitper());
						}else if(4 == su.getSequence()){
							s.setSpec4(su.getUnit());
							s.setSpecUnitper4(su.getUnitper());
						}
					}
				}
			}
		}
		return supplyList;
	}
	
	/***
	 * 根据sp_code获取物资信息并返回json对象
	 * @param supply
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/findSupplyBySpcode")
	@ResponseBody
	public MisSupply findSupplyBySpcode(MisSupply supply,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		Positn accountPositn = (Positn)session.getAttribute("accountPositn");
		supply.setSp_position(accountPositn.getCode());
		return commonMISBOHService.findSupplyBySpcode(supply);
	}
	
	/****
	 * 判断指定仓位有没有做期初
	 * @author wjf
	 * @param positn
	 * @return
	 */
	@RequestMapping("/checkQC")
	@ResponseBody
	public boolean checkQC(HttpSession session,Positn positn) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null == positn.getCode() || "".equals(positn.getCode())){
			return false;
		}
		positn.setAcct(session.getAttribute("ChoiceAcct").toString());
		Positn positn1 = positnService.findPositnByCode(positn);
		if(null != positn1){
			if ("Y".equals(positn1.getQc())) {
				return true;
			}else {
				return false;
			}
		}else{
			return false;
		}
	}
	
	/**
	 * 获取所有物资类别信息,多选
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author wjf
	 */
	@RequestMapping(value = "/selectMoreGrpTyp")
	public ModelAndView selectMoreGrpTyp(ModelMap modelMap, HttpSession session, String defaultCode, String brackets) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));// 大类
		modelMap.put("grpList", grpTypService.findAllGrpA(acct, null));// 中类
		modelMap.put("typList", grpTypService.findAllTypA(acct, null));// 小类
		modelMap.put("defaultCode", defaultCode);
		modelMap.put("brackets", brackets);//是否带括号
		return new ModelAndView(CommonMisBohConstants.SELECT_TYP_MORE, modelMap);
	}
	
	/***
	 * 弹出物资选择框  左侧   走分店物资属性
	 * @param modelMap
	 * @param session
	 * @param defaultCode
	 * @param single
	 * @param positn   不为空就查分店物资属性的物资
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectSupplyLeft")
	public ModelAndView selectSupplyLeft(ModelMap modelMap,HttpSession session,MisSupply supply,String defaultCode, String single,String positn)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));//大类
		modelMap.put("grpList", grpTypService.findAllGrpA(acct,null));//中类
		modelMap.put("typList", grpTypService.findAllTypA(acct,null));//小类
		modelMap.put("supply",supply);
		modelMap.put("defaultCode", defaultCode);//默认选中的编码
		modelMap.put("single", single);//是否单选 默认单选  false多选
		if(null != positn && !"".equals(positn)){//positn 不为空 ，就查当前分店的物资
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			if(null != thePositn){
				modelMap.put("positn", thePositn.getCode());
			}
		}else{
			modelMap.put("positn", positn);
		}
		return new ModelAndView(CommonMisBohConstants.SELECT_SUPPLY, modelMap);
	}
	
	/***
	 * 弹出物资选择框                右侧
	 * @param modelMap
	 * @param supply
	 * @param level
	 * @param code
	 * @param single
	 * @param page
	 * @param session
	 * @param defaultCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectSupply")
	public ModelAndView selectSupply(ModelMap modelMap, MisSupply supply, String level, String code,String single ,Page page,
			HttpSession session, String defaultCode)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("supplyList", commonMISBOHService.findAllSupplyByLeftGrpTyp(supply,level,code,page));
		modelMap.put("code", code);
		modelMap.put("level", level);
		modelMap.put("pageobj",page);
		modelMap.put("supply",supply);
		modelMap.put("defaultCode", defaultCode);//默认选中的编码
		modelMap.put("single", single );//是否单选 默认单选  false多选
		return new ModelAndView(CommonMisBohConstants.SELECT_TABLE_SUPPLY, modelMap);
	}
	
	/***
	 * 仓位选择公共方法
	 * @param modelMap
	 * @param positn
	 * @param mold
	 * @param defaultName
	 * @param defaultCode
	 * @param page
	 * @param iffirm  不为空走当前所属分店    !=1 只选当前下的档口      =1 选门店和档口
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findPositnSuper")
	public ModelAndView findPositnSuper(ModelMap modelMap,Positn positn,String mold, String defaultName,String defaultCode, Page page,String iffirm,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<Positn> positns = new ArrayList<Positn>();
		positn.setLocale(session.getAttribute("locale").toString());
		if(positn.getTypn().equals("1")){
			positn.setTypn("'1201'");
		}else  if(positn.getTypn().equals("2")){
			positn.setTypn("'1202'");
		}else  if(positn.getTypn().equals("3")){
			positn.setTypn("'1203'");
		}else  if(positn.getTypn().equals("4")){
			positn.setTypn("'1204'");
		}else  if(positn.getTypn().equals("5")){
			positn.setTypn("'1205'");
		}else  if(positn.getTypn().equals("6")){
			positn.setTypn("'1206'");
		}else  if(positn.getTypn().equals("7")){
			positn.setTypn("'1207'");
		}else  if(positn.getTypn().equals("1-2-4")){// 以下照着这个去写
			positn.setTypn("'1201','1202','1204'");
		}else  if(positn.getTypn().equals("1-2-3")){
			positn.setTypn("'1201','1202','1203'");
		}else  if(positn.getTypn().equals("2-4")){
			positn.setTypn("'1202','1204'");
		}else  if(positn.getTypn().equals("3-7")){
			positn.setTypn("'1203','1207'");
		}else  if(positn.getTypn().equals("1-2-3-4-5-6-7")){
			positn.setTypn("'1201','1202','1203','1204','1205','1206','1207'");
		}else  if("3-6".equals(positn.getTypn()) || "'1203','1206'".equals(positn.getTypn())){//这种情况要并上当前门店的档口
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			if(null != thePositn){
				positn.setTypn("'1207'");
				positn.setUcode(thePositn.getCode());
			}
			List<Positn> listPositn = positnService.findPositnSuper(positn,page);
			positns.addAll(listPositn);
			positn.setUcode(null);
			positn.setTypn("'1203','1206'");
		}
		
		if(!"".equals(iffirm) && iffirm != null){
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			if(null != thePositn){
				positn.setUcode(thePositn.getCode());
			}
			Positn positn2 = new Positn();
			positn2.setCode(thePositn.getCode());
			positn2.setTypn("'1203'");
			positn2.setDes(positn.getDes());
			positn2.setInit(positn.getInit());
			positn2.setLocale(session.getAttribute("locale").toString());
			List<Positn> list = positnService.findPositnSuper(positn2,page);
			if(list !=null && list.size() == 1 && "1".equals(iffirm)){
				positns.add(list.get(0));
			}
		}
		List<Positn> listPositn = positnService.findPositnSuper(positn,page);
		positns.addAll(listPositn);
		modelMap.put("listPositn", positns);
		modelMap.put("queryPositn", positn);
		modelMap.put("pageobj", page);
		modelMap.put("iffirm", iffirm);
		
		CodeDes code = new CodeDes();
		Page p = new Page();
		p.setPageSize(Integer.MAX_VALUE);
		code.setTyp(String.valueOf(CodeDes.S_SYQ));
		modelMap.put("listMod", codeDesService.findCodeDes(code, p));
		code.setTyp(String.valueOf(CodeDes.S_AREA));
		modelMap.put("listArea", codeDesService.findCodeDes(code, p));
		code.setTyp(String.valueOf(CodeDes.S_POSITN_TYP));
		modelMap.put("listPositnTyp", codeDesService.findCodeDes(code, p));
		modelMap.put("defaultCode", defaultCode);
		if(null!=defaultName){
			modelMap.put("defaultName", URLDecoder.decode(defaultName, "UTF-8"));
		}
		if("one".equals(mold)){//选择单条仓位
			return new ModelAndView(CommonMisBohConstants.SELECT_ONEPOSITN_SUPER, modelMap);
		}else if("oneTone".equals(mold)){//同一类别仓位单选
			return new ModelAndView(CommonMisBohConstants.SELECT_ONE_TO_ONEPOSITN_SUPER, modelMap);
		}else if("oneTmany".equals(mold)){//同一类别仓位多选
			return new ModelAndView(CommonMisBohConstants.SELECT_ONE_TO_MANYPOSITN_SUPER, modelMap);
		}else{
			return new ModelAndView(CommonMisBohConstants.SELECT_POSITN_SUPER, modelMap);
		}
	}
	
	/***
	 * 选择供应商 公用方法   gysqx = 0  查所有供应商;=1走账号权限     =2 查分店供应商
	 * @param modelMap
	 * @param gysqx
	 * @param defaultCode
	 * @param defaultName
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectOneDeliver")
	public ModelAndView selectOneDeliver(ModelMap modelMap, String gysqx, String defaultCode, String defaultName) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("gysqx", gysqx);
		modelMap.put("defaultCode", defaultCode);
		if(null!=defaultName){
			modelMap.put("defaultName", URLDecoder.decode(defaultName, "UTF-8"));
		}
		//供应商定义主界面
		modelMap.put("delivierListType", deliverService.findDeliverType());
		return new ModelAndView(CommonMisBohConstants.SELECT_ONEDELIVERS2, modelMap);
	}
	
	/***
	 * 查找供应商
	 * @param modelMap
	 * @param page
	 * @param deliver
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/deliverList2")
	public ModelAndView findAllDelivers2(ModelMap modelMap, Page page, Deliver deliver, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//供应商定义主界面
//		CodeDes codeDes=new CodeDes();
//		codeDes.setTyp(String.valueOf(CodeDes.S_DEL_TYPE));//供应商类型
//		modelMap.put("codeDesList", codeDesService.findCodeDes(codeDes));
		//供应商权限筛选
		String accountId=session.getAttribute("accountId").toString();
		if(null != deliver.getGysqx() && !"".equals(deliver.getGysqx())){
			if("1".equals(deliver.getGysqx())){
				deliver.setAccountId(accountId);
			}else if("2".equals(deliver.getGysqx())){
				Positn thePositn = (Positn)session.getAttribute("accountPositn");
				if(null != thePositn){
					deliver.setFirm(thePositn.getCode());
				}
			}
		}
		modelMap.put("deliverList", commonMISBOHService.findDeliver(deliver, page));
		modelMap.put("pageobj", page);
		modelMap.put("typCode", deliver.getTypCode());
		modelMap.put("deliver", deliver);
		return new ModelAndView(CommonMisBohConstants.TABLE_DELIVERS2,modelMap);
	}
	
	/***
	 * 专门用来查询物资在某个仓位的余额情况 
	 * @author wjf
	 * @param modelMap
	 * @param session
	 * @param supplyAcct
	 * @return
	 */
	@RequestMapping("/findViewPositnSupply")
	@ResponseBody
	public PositnSupply findViewPositnSupply(ModelMap modelMap,HttpSession session,SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//查询库存量--物资余额表数据
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		return commonMISBOHService.findViewPositnSupply(supplyAcct);
	}
	
	/***
	 * 判断 当日有没有做过盘点
	 * @param modelMap
	 * @param session
	 * @param positn
	 * @param maded
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/chkYnInout")
	@ResponseBody
	public int chkYnInout(ModelMap modelMap,HttpSession session,String positn,String maded)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Integer ynInout = Integer.parseInt(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "ynInout"));
		if(0 == ynInout){
			return 0;
		}else{
			Chkstoref cf = new Chkstoref();
			cf.setWorkDate(null == maded || "".equals(maded) ? DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"):maded);
			cf.setDept(positn);
			cf.setState("1");
			int cf1 = myDeskMisbohService.findChkstorefCount(cf);
			if(0 == cf1){//没有做过盘点
				return 0;
			}else{
				if(1 == ynInout){//提示
					return 1;
				}else if(2 == ynInout){//不能出入库
					return 2;
				}
			}
		}
		return 0;
	}
	
	/**
	 * 根据条件查询报价
	 */
	@RequestMapping("/findBprice")
	@ResponseBody
	public Spprice findBprice(Spprice spprice, HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//帐套
		String acct = session.getAttribute("ChoiceAcct").toString();
		spprice.setAcct(acct);
		//1.查报价
		Spprice sp = commonMISBOHService.findBprice(spprice);
		spprice.setPrice(new BigDecimal(0));
		spprice.setSta("N");
		if (null == sp||null == sp.getPrice()) {
			MisSupply supply = new MisSupply();
			supply.setAcct(acct);
			supply.setSp_code(spprice.getSp_code());
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			supply.setSp_position(thePositn.getCode());
			supply = commonMISBOHService.findSupplyBySpcode(supply);
			if("Y".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "ynChkinByLastPrice"))){//是否取最后进价
				if(null == supply){
					spprice.setPrice(new BigDecimal(0));
				}else{
					spprice.setPrice(new BigDecimal(supply.getLast_price()));
				}
			}else{
				if (supply.getSp_price()!=null) {
					spprice.setPrice(new BigDecimal(supply.getSp_price()));
				}
			}
		} else {
			spprice.setPrice(sp.getPrice());
			spprice.setSta("Y");
		}
		//2.查税率
		if(null != spprice.getDeliver()){
			MisSupply ms = new MisSupply();
			ms.setAcct(acct);
			ms.setSp_code(spprice.getSp_code());
			ms.setDeliver(spprice.getDeliver());
			ms = commonMISBOHService.findTaxByDeliverSpcode(ms);
			spprice.setTax(ms.getTax());
			spprice.setTaxdes(ms.getTaxdes());
		}
		return spprice;
	}
	
	/***
	 * 查询所有的年列表
	 * @author wjf
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findAllYears") 
	@ResponseBody
	public Object findAllYears(ModelMap modelMap,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = (String)(session.getAttribute("ChoiceAcct"));
		return commonMISBOHService.findAllYears(acct);
	}
	
	/***
	 * 获取所有单据类型
	 * @param codetyp
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findAllBillType")
	@ResponseBody
	public Object findAllBillType(String codetyp,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String locale = session.getAttribute("locale").toString();
		CodeDes codeDes = new CodeDes();
		codeDes.setLocale(locale);
		codeDes.setCodetyp(codetyp);
		return codeDesService.findDocumentType(codeDes);
	}
	
	/***
	 * keepSessionAjax
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/keepSessionAjax")
	@ResponseBody
	public String keepSessionAjax(HttpSession session) throws Exception{
		return session.getAttribute("ChoiceAcct").toString();
	}
	
	/********************************************供应链 用:物资，仓位，供应商  end****************************************************/
	
	/**
	 * 描述：页面跳转提示
	 * @author 马振
	 * 创建时间：2015-6-10 下午2:22:15
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toList")
	public ModelAndView listA(ModelMap modelMap, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 描述：查询所有套餐类别信息  中餐专用
	 * @author 马振
	 * 创建时间：2015-6-27 下午3:54:47
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findAllType")
	@ResponseBody	
	public Object findAllType() throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return commonMISBOHService.findAllType();
	}
	
	/**
	 * 描述：查询所有套餐信息  ---中餐专用
	 * @author 马振
	 * 创建时间：2015-6-27 下午2:49:22
	 * @param vpacktype
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findAllPack")
	@ResponseBody	
	public Object findAllPack(CommonMethod commonMethod) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return commonMISBOHService.findAllPack(commonMethod);
	}
	
	/**
	 * 描述：查询所有集团部门
	 * @author 马振
	 * 创建时间：2015-7-14 下午3:32:39
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findAllGroupDept")
	@ResponseBody	
	public Object findAllGroupDept() throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return commonMISBOHService.findAllGroupDept();
	}
	
	/**
	 * 描述：门店部门选择框
	 * @author 马振
	 * 创建时间：2015-8-24 下午5:37:26
	 * @param modelMap
	 * @param single
	 * @param callBack
	 * @param pk_id
	 * @param domId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toChooseStoreDept")
	public ModelAndView toChooseStoreDept(ModelMap modelMap,String single,String callBack,String pk_id,String domId, HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Store store = new Store();
		store.setVcode(commonMISBOHService.getPkStore(session).getVcode());
		List<StoreDept> listStoreDept = commonMISBOHService.getAllStoreDept(store);
		
		modelMap.put("storeDeptList", listStoreDept);
		modelMap.put("callBack", callBack);
		modelMap.put("single", single);
		modelMap.put("pk_id", pk_id);
		modelMap.put("domId", domId);
		modelMap.put("pk_store", commonMISBOHService.getPkStore(session).getPk_store());
		return new ModelAndView(CommonMisBohConstants.CHOOSELIST_STOREDEPT, modelMap);
	}
	
	/**
	 * 描述：菜品公共选择框
	 * @author 马振
	 * 创建时间：2015-8-28 下午5:51:13
	 * @param modelMap
	 * @param session
	 * @param pubItem
	 * @param callBack
	 * @param domId
	 * @param single
	 * @param isTc
	 * @param reportName
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/selectPubitems")
	public ModelAndView selectPubitems(ModelMap modelMap,HttpSession session, PubItem pubItem, String callBack,String domId,String single,String isTc,String reportName) throws CRUDException {
		String accountId=session.getAttribute("accountId").toString();//accountId
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("callBack", callBack);
		modelMap.put("domId", domId);
		modelMap.put("isTc", isTc);
		
		if(ValueUtil.IsNotEmpty(domId)){
			
			//将页面已选择的菜品主键传到后台
			pubItem.setPk_pubitem(domId);
			List<PubItem> listPubitem = commonMISBOHService.listPubitem(pubItem, session);
			modelMap.put("listSelectedPubitem", listPubitem);
		}
		Map<String,Object> condition = new HashMap<String,Object>();
		condition.put("isTc",isTc);
		condition.put("pk_store", commonMISBOHService.getPkStore(session).getPk_store());
		
		//获取菜类树
		modelMap.put("listPubitemTree", commonMISBOHService.listPubitemTree(condition));
		
		//获取登录用户全部可选菜品列表
		if("93bce7e4a7884946bba212e9370e216f".equals(accountId) || "d30f3bbfb4ce4856ad6b14d22cb99e47".equals(accountId)){
			//如果是admin的话，不做限制
			accountId = "";
		}
		condition.put("accountId","");
		List<Map<String,Object>> listPubitemInfo = commonMISBOHService.listPubitemInfo(condition);
		modelMap.put("listPubitemInfo", listPubitemInfo);
		
		//获取菜品组
		condition.put("accountId",accountId);
		List<Map<String,Object>>  listPubitemTeam = commonMISBOHService.listPubitemTeamByAccount(condition);
		modelMap.put("listPubitemTeam", listPubitemTeam);
		
		return new ModelAndView(CommonMisBohConstants.SELECTPUBITEMS, modelMap);
	}
	
	/**
	 * 描述：查询菜品组
	 * @author 马振
	 * 创建时间：2015-8-29 上午10:55:38
	 * @param modelMap
	 * @param pubitemTeam
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listTeamDetail")
	@ResponseBody
	public Object listTeamDetail(ModelMap modelMap, PubitemTeam pubitemTeam) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<PubitemTeamDetail> listDetail = null;
		if(pubitemTeam.getPk_pubitemTeam()!= null && !"".equals(pubitemTeam.getPk_pubitemTeam()) ){
			listDetail = commonMISBOHService.listTeamDetail(pubitemTeam);
		}
		return listDetail;
	}

	/**
	 * 描述:获取该门店的门店操作员
	 * 作者:马振
	 * 时间:2016年7月13日下午3:30:39
	 * @param modelMap
	 * @param session
	 * @param pk_store
	 * @param single
	 * @param callBack
	 * @param domId
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toChooseStoreOperator")
	public ModelAndView toChooseStoreOperator(ModelMap modelMap,HttpSession session,String pk_store, String single,String callBack,String domId,Page page) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		MisBohOperator operator = new MisBohOperator();
		operator.setPk_store(pk_store);
		
		modelMap.put("listStoreOperator", commonMISBOHService.listStoreOperator(operator));
		modelMap.put("pageobj", page);
		modelMap.put("callBack", callBack);
		modelMap.put("domId", domId);
		modelMap.put("single", single);
		modelMap.put("pk_store", pk_store);
		return new ModelAndView(CommonMisBohConstants.CHOOSESTOREOPERATOR, modelMap);
	}
}