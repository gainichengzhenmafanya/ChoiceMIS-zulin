package com.choice.misboh.web.controller.salesforecast;


import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.constants.salesforecast.SpCodeUseMisConstants;
import com.choice.misboh.domain.salesforecast.SpCodeUseMis;
import com.choice.misboh.service.salesforecast.SpCodeUseMisService;
import com.choice.scm.domain.Positn;
import com.choice.scm.service.PositnService;

/**
* 物资千人、万元用量
* @author -  css
*/

@Controller
@RequestMapping("spCodeUseMis")
public class SpCodeUseMisController {

	@Autowired
	private SpCodeUseMisService spCodeUseMisService;
	@Autowired
    private PositnService positnService;

	/***
	 * 进入千人/万元用量
	 * @param modelMap
	 * @param session
	 * @param spCodeUse
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/list")
	public ModelAndView list(ModelMap modelMap, HttpSession session, SpCodeUseMis spCodeUse) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String typ = "PAX";
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if(null != thePositn){
			thePositn = positnService.findPositnByCode(thePositn);
			spCodeUse.setFirm(thePositn.getCode());
			spCodeUse.setYnUseDept(thePositn.getYnUseDept());
			typ = thePositn.getVplantyp();//AMT   TC    PAX
			spCodeUse.setTyp(typ);
		}
		//初次加载页面传值
		if(null == spCodeUse.getBdat() || null == spCodeUse.getEdat()){
			Date nowDate = new Date();
			nowDate = DateFormat.formatDate(nowDate, "yyyy-MM-dd");
			Calendar calendar = Calendar.getInstance();  
	        calendar.setTime(nowDate);
	        calendar.add(Calendar.DATE, -7);
	        spCodeUse.setBdat(calendar.getTime());
	        calendar.setTime(nowDate);
	        calendar.add(Calendar.DATE, -1);
	        spCodeUse.setEdat(calendar.getTime());
		}else{
			String dept = spCodeUse.getDept();
			spCodeUse.setDept(CodeHelper.replaceCode(dept));
			modelMap.put("spCodeUseList", spCodeUseMisService.listSpCodeUse(spCodeUse));
			spCodeUse.setDept(dept);
		}
		modelMap.put("spCodeUse", spCodeUse);
		return new ModelAndView(SpCodeUseMisConstants.LIST_SPCODEUSE,modelMap);
	}

	/***
	 * 删除千人/万元用量
	 * @param modelMap
	 * @param session
	 * @param spCodeUse
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/delete")
	public ModelAndView delete(ModelMap modelMap, HttpSession session, SpCodeUseMis spCodeUse) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		// 获取分店code
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if(null != thePositn){
			thePositn = positnService.findPositnByCode(thePositn);
			spCodeUse.setFirm(thePositn.getCode());
			spCodeUse.setYnUseDept(thePositn.getYnUseDept());
		}
		// 删除预估用量
		spCodeUseMisService.deleteAll(spCodeUse);
		modelMap.put("spCodeUseList", spCodeUseMisService.listSpCodeUse(spCodeUse));
		modelMap.put("spCodeUse", spCodeUse);
		return new ModelAndView(SpCodeUseMisConstants.LIST_SPCODEUSE,modelMap);
	}

	/***
	 * 计算千人/万元用量
	 * @param modelMap
	 * @param session
	 * @param spCodeUse
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/calculate")
	public ModelAndView calculate(ModelMap modelMap, HttpSession session, SpCodeUseMis spCodeUse) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		// 获取分店code
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		String vplantyp = "PAX";
		if(null != thePositn){
			thePositn = positnService.findPositnByCode(thePositn);
			spCodeUse.setFirm(thePositn.getCode());
			spCodeUse.setYnUseDept(thePositn.getYnUseDept());
	    	vplantyp = thePositn.getVplantyp();//AMT   TC    PAX
			spCodeUse.setVplantyp(vplantyp);
		}
		SpCodeUseMis spCode = spCodeUseMisService.getAmt_boh(spCodeUse);
		if (spCode!=null && (spCode.getPax() != 0 || spCode.getAmt() != 0d)) {
			//判断是用千人还是万元
			if ("PAX".equals(vplantyp)) {
				//千人
				spCodeUse.setLv(spCode.getPax()/1000.0);
			} else if("AMT".equals(vplantyp)) {
				//万元
				spCodeUse.setLv(spCode.getAmt()/10000.0);
			} else if("TC".equals(vplantyp)){
				spCodeUse.setLv(spCode.getTc()/1000.0);
			}
		}else {
			spCodeUse.setLv(0);
			modelMap.put("yysj", "0");
			modelMap.put("spCodeUse", spCodeUse);
			return new ModelAndView(SpCodeUseMisConstants.LIST_SPCODEUSE, modelMap);
		}
		if ("Y".equals(thePositn.getYnUseDept())) {
			String dept = spCodeUse.getDept();
			spCodeUse.setDept(CodeHelper.replaceCode(dept));
			modelMap.put("spCodeUseList", spCodeUseMisService.calculateDept(spCodeUse));
			spCodeUse.setDept(dept);
		} else {
			modelMap.put("spCodeUseList", spCodeUseMisService.calculate(spCodeUse));
		}
		modelMap.put("spCodeUse", spCodeUse);
		return new ModelAndView(SpCodeUseMisConstants.LIST_SPCODEUSE, modelMap);
	}

	/***
	 * 保存千人/万元用量修改
	 * @param session
	 * @param modelMap
	 * @param spCodeUse
	 * @return
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public String update(HttpSession session,ModelMap modelMap, SpCodeUseMis spCodeUse) {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if(null != thePositn){
			spCodeUse.setFirm(thePositn.getCode());
		}
		try {
			spCodeUse.setDept(CodeHelper.replaceCode(spCodeUse.getDept()));
			spCodeUseMisService.update(spCodeUse);
			return "1";
		} catch (Exception e) {
			return "0";
		}
	}	

}
