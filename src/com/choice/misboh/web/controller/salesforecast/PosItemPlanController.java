package com.choice.misboh.web.controller.salesforecast;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.constants.salesforecast.ForecastConstants;
import com.choice.misboh.domain.salesforecast.ItemPlan;
import com.choice.misboh.service.salesforecast.PosItemPlanService;
import com.choice.scm.domain.Positn;


/***
 * 菜品销售计划
 * @author wjf 20170802
 */
@Controller
@RequestMapping("/posItemPlan")
public class PosItemPlanController {

	@Autowired
	private PosItemPlanService posItemPlanService;

	/***
	 * 进入菜品销售计划
	 * @param modelMap
	 * @param session
	 * @param itemPlan
	 * @param isDeclare
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/list")
	public ModelAndView list(ModelMap modelMap, HttpSession session, ItemPlan itemPlan, String isDeclare) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		itemPlan.setFirm(thePositn.getCode());
		Date nowDate = new Date();
		itemPlan.setDate(DateFormat.getStringByDate(nowDate, "yyyy-MM-dd"));
		/**初次进入页面**/
		if(null == itemPlan.getBdate() || null == itemPlan.getEdate()){
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(nowDate);
			calendar.add(Calendar.DATE, 1);
			itemPlan.setBdate(DateFormat.formatDate(calendar.getTime(),"yyyy-MM-dd"));
			calendar.setTime(nowDate);
			calendar.add(Calendar.DATE, 2);
			itemPlan.setEdate(DateFormat.formatDate(calendar.getTime(),"yyyy-MM-dd"));
		}
		List<String> listStr = new ArrayList<String>();
		listStr = DateJudge.getDateList(DateJudge.getStringByDate(itemPlan.getBdate(), "yyyy-MM-dd"), DateJudge.getStringByDate(itemPlan.getEdate(), "yyyy-MM-dd"));
		modelMap.put("dateList", listStr);
		modelMap.put("listStrSize", listStr.size());
		modelMap.put("itemPlanList", posItemPlanService.queryItemPlan(itemPlan));
		modelMap.put("msg", "a");
		modelMap.put("itemPlan", itemPlan);
		modelMap.put("isDeclare", isDeclare);
		return new ModelAndView(ForecastConstants.LIST_POSITEMPALN, modelMap);
	}
	
	/**
	 * 计算预估菜品销售计划
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/calPlan")
	public ModelAndView calPlan(ModelMap modelMap, HttpSession session, ItemPlan itemPlan, String isDeclare,String salePlan) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		itemPlan.setFirm(thePositn.getCode());
		itemPlan.setDat(itemPlan.getBdate());
		List<String> listStr = DateJudge.getDateList(DateJudge.getStringByDate(itemPlan.getBdate(), "yyyy-MM-dd"), DateJudge.getStringByDate(itemPlan.getEdate(), "yyyy-MM-dd"));
		int sta = posItemPlanService.saveCalPlan(itemPlan, listStr, salePlan, isDeclare);
		if("1".equals(isDeclare)){
			modelMap.put("msg", "a");//标记正常计算营业预估
		}else{
			if (sta == -1) {
				modelMap.put("msg", "选择的日期内有没有做营业预估，请检查营业预估!");
			} else if (sta == -2){
				modelMap.put("msg", "没有设定点击率，不能自动计算预估!");
			} else {
				modelMap.put("msg", "a");//标记正常计算营业预估
			}
		}
		modelMap.put("dateList", listStr);
		modelMap.put("listStrSize", listStr.size());
		modelMap.put("itemPlanList", posItemPlanService.queryItemPlan(itemPlan));
		modelMap.put("isDeclare", isDeclare);
		modelMap.put("itemPlan", itemPlan);
		return new ModelAndView(ForecastConstants.LIST_POSITEMPALN, modelMap);
	}

	/***
	 * 保存菜品销售计划
	 * @param session
	 * @param posItemPlan
	 * @return
	 */
	@RequestMapping(value = "/updatePlan")
	@ResponseBody
	public String updatePlan(HttpSession session, ItemPlan posItemPlan)  {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		try {
			// 获取分店code
			String firmId = "~";
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			if(null != thePositn){
				firmId = thePositn.getCode();
			}
			posItemPlanService.updateItemPlan(posItemPlan, firmId);
			return "1";
		} catch (Exception e) {
			return "0";
		}
	}

	/***
	 * 导出菜品销售计划
	 * @param response
	 * @param request
	 * @param session
	 * @param itemPlan
	 * @throws Exception
	 */
	@RequestMapping("/exportPlan")
	@ResponseBody
	public void exportPlan(HttpServletResponse response, HttpServletRequest request,HttpSession session, ItemPlan itemPlan) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<String> listStr = DateJudge.getDateList(DateJudge.getStringByDate(itemPlan.getBdate(), "yyyy-MM-dd"), DateJudge.getStringByDate(itemPlan.getEdate(), "yyyy-MM-dd"));
		List<ItemPlan> disList = posItemPlanService.queryItemPlan(itemPlan);
		String fileName = "预估菜品销售计划";
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){              
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");
		posItemPlanService.exportExcelPlan(response.getOutputStream(), disList,listStr);
		
	}
	
}
