package com.choice.misboh.web.controller.salesforecast;

import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.constants.salesforecast.ForecastChoice3Constants;
import com.choice.misboh.domain.salesforecast.FirmItemUse;
import com.choice.misboh.service.salesforecast.ItemUseChoice3Service;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;
import com.choice.scm.util.ReadReportUrl;

/**
* 菜品点击率
* @author -    xlh  新加入 西贝方式 
*/
@Controller
@RequestMapping("itemUseChoice3")
public class ItemUseChoice3Controller {
	@Autowired
	private ItemUseChoice3Service itemUseChoice3Service;
	@Autowired
	private AccountPositnService accountPositnService;
	@Autowired
	private Page pager;

	/**
	 * BSBOH预估系统菜品点击率--查询
	 */
	@RequestMapping("/list")
	public ModelAndView forecastList(ModelMap modelMap, HttpSession session, Page page, FirmItemUse firmItemUse) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		firmItemUse.setFirm(thePositn.getCode());
		/**初次进入页面**/
		//1.参考日期  默认前一个月
		Date nowDate = new Date();
		if(null == firmItemUse.getBdate() || null == firmItemUse.getEdate()){
			Calendar calendar = Calendar.getInstance();  
	        calendar.setTime(nowDate);
	        calendar.add(Calendar.DATE, -31);
	        firmItemUse.setBdate(calendar.getTime());
	        calendar.setTime(nowDate);
	        calendar.add(Calendar.DATE, -1);
	        firmItemUse.setEdate(calendar.getTime());
		}
		modelMap.put("firmItemUseList", itemUseChoice3Service.findAllItemUse(firmItemUse));
		modelMap.put("firmItemUse", firmItemUse);
		return new ModelAndView(ForecastChoice3Constants.MIS_LIST_COSTCTR,modelMap);

	}
	
	/**
	 * 删除菜品点击率
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(ModelMap modelMap, HttpSession session, FirmItemUse firmItemUse) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		firmItemUse.setFirm(thePositn.getCode());
		// 删除所有菜品点击率
		itemUseChoice3Service.deleteAll(firmItemUse);
		return "1";

	}
	
	/**
	 * BSBOH预估系统菜品点击率--计算
	 */
	@RequestMapping("/calculate")
	public ModelAndView calculate(ModelMap modelMap, HttpSession session, FirmItemUse firmItemUse) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		firmItemUse.setFirm(thePositn.getCode());
		// 重新计算点击率
		itemUseChoice3Service.saveCalculate(firmItemUse);
		List<FirmItemUse> firmItemUseList = itemUseChoice3Service.findAllItemUse(firmItemUse);
		modelMap.put("firmItemUseList", firmItemUseList);
		modelMap.put("firmItemUse", firmItemUse);
		modelMap.put("msg", "ok");//改变msg的值，使计算等待提示信息不显示
		return new ModelAndView(ForecastChoice3Constants.MIS_LIST_COSTCTR,modelMap);

	}
	
	/**
	 *  保存修改调整量
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public String update(ModelMap modelMap, FirmItemUse firmItemUse,HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		firmItemUse.setFirm(thePositn.getCode());
		itemUseChoice3Service.update(firmItemUse);
		return "1";
	}
	
	/**
	 * 菜品点击率表打印
	 */
	@RequestMapping(value = "/print")
	public ModelAndView print(ModelMap modelMap, HttpSession session, String firmId, String type, String mis)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		FirmItemUse firmItemUse = new FirmItemUse();
		if (null == firmId) {
			// 获取分店code
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				firmId=accountPositn.getPositn().getCode();
			}
		}
		firmItemUse.setFirm(firmId);
		List<FirmItemUse> disList=itemUseChoice3Service.findAllItemUse(firmItemUse);
		int num=disList.size()%19;
		for (int i = 0; i < 19-num; i++) {
			disList.add(null);
		}
		modelMap.put("List",disList);//list
 		HashMap<String, Object>  parameters = new HashMap<String, Object>(); 
        parameters.put("report_name", "菜品点击率表");  
        parameters.put("report_time", DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));  
        modelMap.put("parameters", parameters);//map
	       
	    HashMap<String, String> map=new HashMap<String, String>();
        modelMap.put("actionMap", map);
	 	modelMap.put("action", "/forecast/print.do?firmId="+firmId);//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,ForecastChoice3Constants.REPORT_FORECAST,ForecastChoice3Constants.REPORT_FORECAST);//判断跳转路径
        modelMap.put("reportUrl", rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 导出菜品点击率excel表
	 */
	@RequestMapping("/export")
	@ResponseBody
	public void export(HttpServletResponse response, String sort, String firmId, 
			String order, HttpServletRequest request, HttpSession session) throws Exception{
		FirmItemUse firmItemUse = new FirmItemUse();
		if (null == firmId) {
			// 获取分店code
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				firmId=accountPositn.getPositn().getCode();
			}
		}
		firmItemUse.setFirm(firmId);
		List<FirmItemUse> disList=itemUseChoice3Service.findAllItemUse(firmItemUse);
		String fileName = "菜品点击率表";
		Map<String,Object> condition = new HashMap<String,Object>();
		condition.put("firmItemUse", firmItemUse);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		itemUseChoice3Service.exportExcel(response.getOutputStream(), disList);
		
	}
}
