package com.choice.misboh.web.controller.salesforecast;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.constants.salesforecast.ForecastChoice3Constants;
import com.choice.misboh.domain.salesforecast.FirmItemUse;
import com.choice.misboh.domain.salesforecast.ItemPlan;
import com.choice.misboh.domain.salesforecast.SalePlan;
import com.choice.misboh.service.salesforecast.ItemUseChoice3Service;
import com.choice.misboh.service.salesforecast.PosItemPlanChoice3Service;
import com.choice.misboh.service.salesforecast.PosSalePlanChoice3Service;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;
import com.choice.scm.util.ReadReportUrl;

/**
*  预估菜品销售计划 
* @author -  xlh  西贝新加入
*/

@Controller
@RequestMapping("posItemPlanChoice3")
public class PosItemPlanChoice3Controller {

	@Autowired
	private PosItemPlanChoice3Service posItemPlanChoice3Service;
	@Autowired
	private PosSalePlanChoice3Service posSalePlanChoice3Service;
	@Autowired
	private ItemUseChoice3Service itemUseChoice3Service;
	@Autowired
	private Page pager;
	
	/**
	 * 预估菜品销售计划
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/planList")
	public ModelAndView planList(ModelMap modelMap, HttpSession session, ItemPlan itemPlan) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		itemPlan.setFirm(thePositn.getCode());
		Date nowDate = new Date();
		itemPlan.setDate(DateFormat.getStringByDate(nowDate, "yyyy-MM-dd"));
		/**初次进入页面**/
		if(null == itemPlan.getBdate() || null == itemPlan.getEdate()){
			Calendar calendar = Calendar.getInstance();  
	        calendar.setTime(nowDate);
	        calendar.add(Calendar.DATE, 1);
	        itemPlan.setBdate(DateFormat.formatDate(calendar.getTime(),"yyyy-MM-dd"));
	        calendar.setTime(nowDate);
	        calendar.add(Calendar.DATE, 2);
	        itemPlan.setEdate(DateFormat.formatDate(calendar.getTime(),"yyyy-MM-dd"));
		}
		List<String> listStr = new ArrayList<String>();
		listStr = DateJudge.getDateList(DateJudge.getStringByDate(itemPlan.getBdate(), "yyyy-MM-dd"), DateJudge.getStringByDate(itemPlan.getEdate(), "yyyy-MM-dd"));
		modelMap.put("dateList", listStr);
		modelMap.put("listStrSize", listStr.size());
		modelMap.put("itemPlanList", posItemPlanChoice3Service.findPosItemPlan(itemPlan));
		modelMap.put("msg", "a");
		modelMap.put("itemPlan", itemPlan);
		return new ModelAndView(ForecastChoice3Constants.MIS_LIST_PLAN,modelMap);

	}
	
	/**
	 * 删除预估菜品销售计划
	 * @param modelMap
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/deletePlan")
	@ResponseBody
	public String deletePlan(HttpSession session,ItemPlan itemPlan) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		itemPlan.setFirm(thePositn.getCode());
		posItemPlanChoice3Service.deleteAllPlan(itemPlan);
		return "1";
	}
	
	/**
	 * 计算预估菜品销售计划
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/calPlan")
	public ModelAndView calPlan(ModelMap modelMap, HttpSession session, ItemPlan itemPlan) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		itemPlan.setFirm(thePositn.getCode());
		// 1.查找营业预估
		SalePlan salePlan = new SalePlan();
		salePlan.setFirm(thePositn.getCode());
		Date nowDate = new Date();
		itemPlan.setDate(DateFormat.getStringByDate(nowDate, "yyyy-MM-dd"));
		List<SalePlan> posSalePlanList = posSalePlanChoice3Service.findPosSalePlan(salePlan);
		if (posSalePlanList.size()==0) {
			modelMap.put("msg", "选择的日期内有没有做营业预估，请检查营业预估!");
		}
		// 2. 查找点击率
		FirmItemUse firmItemUse = new FirmItemUse();
		firmItemUse.setFirm(thePositn.getCode());
		List<FirmItemUse> itemUseList = itemUseChoice3Service.findAllItemUse(firmItemUse);
		//  3.  判断是否有点击率  或者 营业预估  
		if (itemUseList.size()==0) {
			modelMap.put("msg", "没有设定点击率，不能自动计算预估!");
		}
		if(posSalePlanList.size() != 0 && itemUseList.size() != 0){
			posItemPlanChoice3Service.saveCalPlan(itemPlan, posSalePlanList,itemUseList);
			modelMap.put("msg","a");
		}
		List<String> listStr = new ArrayList<String>();
		listStr = DateJudge.getDateList(DateJudge.getStringByDate(itemPlan.getBdate(), "yyyy-MM-dd"), DateJudge.getStringByDate(itemPlan.getEdate(), "yyyy-MM-dd"));
		modelMap.put("dateList", listStr);
		modelMap.put("listStrSize", listStr.size());
		modelMap.put("itemPlanList", posItemPlanChoice3Service.findPosItemPlan(itemPlan));
		modelMap.put("itemPlan", itemPlan);
		return new ModelAndView(ForecastChoice3Constants.MIS_LIST_PLAN,modelMap);
	}
	
	/**
	 *  保存修改调整量（预估菜品销售计划）
	 * @param modelMap
	 * @param posSalePlan
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updatePlan")
	@ResponseBody
	public String updatePlan(HttpSession session, ItemPlan itemPlan) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		 try {
			 Positn thePositn = (Positn)session.getAttribute("accountPositn");
			 itemPlan.setFirm(thePositn.getCode());
			 posItemPlanChoice3Service.updateItemPlan(itemPlan);
			 return "1";
		} catch (Exception e) {
			return "0";
		}
	}
	
	/**
	 * 预估菜品销售计划打印
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printPlan")
	public ModelAndView printPlan(ModelMap modelMap, HttpSession session, String type, String firmId, Date bdate, String mis)throws CRUDException{
		ItemPlan itemPlan = new ItemPlan();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		itemPlan.setFirm(thePositn.getCode());
		itemPlan.setDat(bdate);
		List<ItemPlan> disList=posItemPlanChoice3Service.findPosItemPlan(itemPlan);
		int num=disList.size()%19;
		for (int i = 0; i < 19-num; i++) {
			disList.add(null);
		}
		modelMap.put("List",disList);//list
 		HashMap<String, Object>  parameters = new HashMap<String, Object>(); 
        parameters.put("report_name", "分店菜品销售预估");//修改名字 wjf
        parameters.put("report_time", DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));  
        modelMap.put("parameters", parameters);//map
	       
	    HashMap<String, String> map=new HashMap<String, String>();
        modelMap.put("actionMap", map);
	 	modelMap.put("action", "/forecast/printPlan.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,ForecastChoice3Constants.REPORT_PLAN,ForecastChoice3Constants.REPORT_PLAN);//判断跳转路径
        modelMap.put("reportUrl", rs.get("reportUrl"));//ireport文件地址
        modelMap.put("msg", "a");
        modelMap.put("mis", mis);
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 预估菜品销售计划excel表
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/exportPlan")
	@ResponseBody
	public void exportPlan(HttpServletResponse response, HttpServletRequest request,HttpSession session,ItemPlan ItemPlan) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		List<String> listStr = new ArrayList<String>();
		listStr = DateJudge.getDateList(DateJudge.getStringByDate(ItemPlan.getBdate(), "yyyy-MM-dd"), DateJudge.getStringByDate(ItemPlan.getEdate(), "yyyy-MM-dd"));
		ItemPlan.setFirm(thePositn.getCode());
		List<ItemPlan> disList=posItemPlanChoice3Service.findPosItemPlan(ItemPlan);
		String fileName = "预估菜品销售计划";
		Map<String, Object> condition = new HashMap<String,Object>();
		condition.put("posItemPlan", ItemPlan);
		pager.setPageSize(Integer.MAX_VALUE);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){              
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		posItemPlanChoice3Service.exportExcelPlan(response.getOutputStream(), disList,listStr);
	}
}
