package com.choice.misboh.web.controller.salesforecast;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.constants.salesforecast.ForecastConstants;
import com.choice.misboh.domain.salesforecast.FirmItemUse;
import com.choice.misboh.domain.salesforecast.ItemPlan;
import com.choice.misboh.domain.salesforecast.SalePlan;
import com.choice.misboh.domain.store.Store;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.misboh.service.salesforecast.ForecastMisService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;
import com.choice.scm.util.ReadReportUrl;


/**
* 菜品点击率,营业预估,预估菜品销售计划,营业预估实际对比,菜品预估实际对比,菜品预估沽清对比
* @author -  css
*/

@Controller
@RequestMapping("forecastMis")
public class ForecastMisController {

	@Autowired
	private ForecastMisService forecastService;
	@Autowired
	private Page pager;
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	
	/********************************************************菜品点击率start*************************************************/
	/**
	 * 分店MIS预估系统菜品点击率
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/list")
	public ModelAndView forecastList(ModelMap modelMap, HttpSession session, Page page, FirmItemUse itemUse, 
			String firmId, String firmName, Date bdate, Date edate, String type, String mis,String isDeclare) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn positn = commonMISBOHService.getPositn(session);
		Store store = new Store();
		store.setVcode(positn.getCode());
		store = commonMISBOHService.getStore(store);
		if (null==type) {
			itemUse.setFirm(positn.getCode());
			modelMap.put("itemUseList", forecastService.findAllItemUse(itemUse));
		}
		//默认mis的值是1
		if (null == mis) {
			mis = "1";
		}
		//初次加载页面传值
		if (bdate==null) {
			bdate=DateJudge.YYYY_MM_DD.parse(DateJudge.getLenTime(-7, DateJudge.YYYY_MM_DD.format(new Date())));
		}
		if (null==edate) {
			edate=DateJudge.YYYY_MM_DD.parse(DateJudge.getLenTime(-1, DateJudge.YYYY_MM_DD.format(new Date())));
		}
		modelMap.put("itemUse", itemUse);//里面存档口
		modelMap.put("bdate", bdate);
		modelMap.put("firmId", positn.getCode());
		modelMap.put("firmName", positn.getDes());
		modelMap.put("edate", edate);
		modelMap.put("mis", mis);
		modelMap.put("positn", positn);
		modelMap.put("store", store);
		if(isDeclare !=null && !isDeclare.equals("")){
			modelMap.put("isDeclare", "1");//判断是否走预估报货
		}
		return new ModelAndView(ForecastConstants.MIS_LIST_COSTCTR,modelMap);
	}
	
	/**
	 * 删除菜品点击率
	 * @param modelMap
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/delete")
	public ModelAndView delete(ModelMap modelMap, HttpSession session, Date bdate, Date edate, 
			String firmId,  String firmName, String msg, String mis, Page page) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn positn = commonMISBOHService.getPositn(session);
		firmId = positn.getCode();
		//默认mis的值是1
		if (null == mis) {
			mis = "1";
		}
		FirmItemUse itemUse = new FirmItemUse();
		itemUse.setFirm(firmId);
		// 删除所有菜品点击率
		forecastService.deleteAll(itemUse);
		modelMap.put("itemUseList", forecastService.findAllItemUse(itemUse));
		
		if (bdate==null) {
			bdate=new Date();
		}
		if (edate==null) {
			edate=new Date();
		}
		modelMap.put("pageobj", page);
		modelMap.put("bdate", bdate);
		modelMap.put("edate", edate);
		modelMap.put("firmId", firmId);
		modelMap.put("firmName", firmName);
		modelMap.put("mis", mis);
//		modelMap.put("pageobj", page);
		modelMap.put("positn", positn);
		return new ModelAndView(ForecastConstants.MIS_LIST_COSTCTR,modelMap);
	}
	
	/**
	 * 分店MIS预估系统菜品点击率
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/calculate")
	public ModelAndView calculate(ModelMap modelMap, HttpSession session, Page page, FirmItemUse itemUse,
			String firmId, String firmName, Date bdate, Date edate, String mis,String isDeclare) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn positn = commonMISBOHService.getPositn(session);
		Store store = new Store();
    	store.setVcode(positn.getCode());
    	store = commonMISBOHService.getStore(store);
		//默认mis的值是1
		if (null == mis) {
			mis = "1";
		}
		itemUse.setFirm(positn.getCode());
		// 重新计算点击率
		forecastService.saveNewCalculateItemuse(firmId, session.getAttribute("ChoiceAcct").toString(), bdate, edate, store);
		
		List<FirmItemUse> itemUseList = forecastService.findAllItemUse(itemUse);
		if (itemUseList.size()==0) {
			modelMap.put("dataNull", "dataNull");
		}
		modelMap.put("itemUseList", itemUseList);
		//初次加载页面传值
		if (bdate==null) {
			bdate=DateJudge.YYYY_MM_DD.parse(DateJudge.getLenTime(-7, DateJudge.YYYY_MM_DD.format(new Date())));
		}
		if (edate==null) {
			edate=DateJudge.YYYY_MM_DD.parse(DateJudge.getLenTime(-1, DateJudge.YYYY_MM_DD.format(new Date())));
		}
		modelMap.put("itemUse", itemUse);//里面存档口
		modelMap.put("bdate", bdate);
		modelMap.put("edate", edate);
		modelMap.put("firmId", positn.getCode());
		modelMap.put("firmName", positn.getDes());
		modelMap.put("msg", "ok");//改变msg的值，使计算等待提示信息不显示
		modelMap.put("mis", mis);
		modelMap.put("positn", positn);
		modelMap.put("store", store);
		modelMap.put("isDeclare", isDeclare);
		return new ModelAndView(ForecastConstants.MIS_LIST_COSTCTR,modelMap);
	}
	
	/**
	 *  保存修改调整量
	 * @param modelMap
	 * @param itemUse
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(ModelMap modelMap, HttpSession session, FirmItemUse itemUse) {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		try {
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			itemUse.setFirm(thePositn.getCode());
			forecastService.update(itemUse);
			return "1";
		} catch (CRUDException e) {
			e.printStackTrace();
			return "0";
		}
	}
	
	/**
	 * 菜品点击率表打印
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/print")
	public ModelAndView print(ModelMap modelMap, HttpSession session, String firmId, String type, String mis)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		FirmItemUse itemUse = new FirmItemUse();
		// 获取分店code
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if(null != thePositn){
			firmId = thePositn.getCode();
		}
		//默认mis的值是1
		if (null == mis) {
			mis = "1";
		}
		itemUse.setFirm(firmId);
		List<FirmItemUse> disList=forecastService.findAllItemUse(itemUse);
		int num=disList.size()%19;
		for (int i = 0; i < 19-num; i++) {
			disList.add(null);
		}
		modelMap.put("List",disList);//list
 		HashMap<String, Object>  parameters = new HashMap<String, Object>(); 
        parameters.put("report_name", "菜品点击率表");  
        parameters.put("report_time", DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));  
        modelMap.put("parameters", parameters);//map
	       
	    HashMap<String, String> map=new HashMap<String, String>();
        modelMap.put("actionMap", map);
	 	modelMap.put("action", "/forecastMis/print.do?firmId="+firmId);//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,ForecastConstants.REPORT_FORECAST,ForecastConstants.REPORT_FORECAST);//判断跳转路径
        modelMap.put("reportUrl", rs.get("reportUrl"));//ireport文件地址
        modelMap.put("mis", mis);
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 导出菜品点击率excel表
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/export")
	@ResponseBody
	public void export(HttpServletResponse response, String sort, String firmId, 
			String order, HttpServletRequest request, HttpSession session) throws Exception{
		FirmItemUse itemUse = new FirmItemUse();
		// 获取分店code
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if(null != thePositn){
			firmId = thePositn.getCode();
		}
		itemUse.setFirm(firmId);
		List<FirmItemUse> disList=forecastService.findAllItemUse(itemUse);
		String fileName = "菜品点击率表";
		Map<String,Object> condition = new HashMap<String,Object>();
		condition.put("itemUse", itemUse);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		forecastService.exportExcel(response.getOutputStream(), disList);
		
	}
	
	/**
	 * 
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/addNewItem")
	public ModelAndView addNewItem(ModelMap modelMap, HttpSession session,String firmId)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		// 获取分店code
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if(null != thePositn){
			firmId = thePositn.getCode();
		}
		modelMap.put("itemList", forecastService.addNewItem(firmId));
		return new ModelAndView(ForecastConstants.MIS_ADDITEM,modelMap);
	}
	
	/********************************************************菜品点击率end*************************************************/
	
	/********************************************************营业预估start*************************************************/
	
	/**
	 * 分店MIS预估系统营业预估
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/castList")
	public ModelAndView castList(ModelMap modelMap, HttpSession session, SalePlan salePlan) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null == salePlan.getStartdate() && null == salePlan.getEnddate()) {
			salePlan.setFirm(thePositn.getCode());
			salePlan.setFirmNm(thePositn.getDes());
			//得到当前月的开始和结束日期
			String nowDate = DateJudge.getStringByDate(new Date(), "yyyy-MM-dd");
			salePlan.setStartdate(DateJudge.getDateByString(DateJudge.getMonthStart(nowDate)));
			salePlan.setEnddate(DateJudge.getDateByString(DateJudge.getMonthEnd(nowDate)));
			//得到上个月的开始和结束日期
			String lastMonthDate = nowDate.substring(0, 5)+(Integer.parseInt(nowDate.substring(5,7)) - 1)+"-01";
			salePlan.setBdate(DateJudge.getDateByString(DateJudge.getMonthStart(lastMonthDate)));
			salePlan.setEdate(DateJudge.getDateByString(DateJudge.getMonthEnd(lastMonthDate)));
		}else{
			salePlan.setDept(CodeHelper.replaceCode(salePlan.getDept()));
			List<SalePlan> list = forecastService.findPosSalePlan(salePlan);
			modelMap.put("posSalePlanList", list);
			if (list != null && list.size() > 0) {
				modelMap.put("posSalePlanHeJi", list.get(list.size() - 1));
			} else {
				modelMap.put("posSalePlanHeJi", new SalePlan());
			}
		}
		modelMap.put("salePlan", salePlan);
		return new ModelAndView(ForecastConstants.MIS_LIST_FORECAST,modelMap);
	}
	
	/**
	 * 分店MIS预估系统营业预估 计算  保存
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/calPosSalePlan")
	public ModelAndView calPosSalePlan(ModelMap modelMap, HttpSession session, SalePlan salePlan) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		salePlan.setAcct(session.getAttribute("ChoiceAcct").toString());
		// 重新计算分店MIS预估系统营业预估
		forecastService.saveCalPosSalePlan(salePlan);
		modelMap.put("posSalePlanList", forecastService.findPosSalePlan(salePlan));
		modelMap.put("salePlan", salePlan);
		return new ModelAndView(ForecastConstants.MIS_LIST_FORECAST,modelMap);
	}
	
	/**
	 *  保存修改调整量
	 * @param modelMap
	 * @param posSalePlan
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateSalePlan")
	@ResponseBody
	public String updateSalePlan(ModelMap modelMap, HttpSession session, SalePlan posSalePlan)  {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		try {
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			forecastService.updateSalePlan(posSalePlan,thePositn.getCode());
			return "1";
		} catch (Exception e) {
			return "0";
		}
	}
	
	/**
	 * 营业预估打印
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printCast")
	public ModelAndView printCast(ModelMap modelMap, HttpSession session,String type, 
			String firmId, Date startdate, Date enddate, String mis)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		SalePlan posSalePlan = new SalePlan();
		// 获取分店code
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if(null != thePositn){
			firmId = thePositn.getCode();
		}
		
		//List<String> listId = Arrays.asList(firmId.split(","));
		posSalePlan.setFirm(firmId);
		posSalePlan.setStartdate(startdate);
		posSalePlan.setEnddate(enddate);
		List<SalePlan> disList =  forecastService.findPosSalePlan(posSalePlan);
		int num=disList.size()%19;
		for (int i = 0; i < 19-num; i++) {
			disList.add(null);
		}
		modelMap.put("List",disList);//list
 		HashMap<String, Object>  parameters = new HashMap<String, Object>(); 
        parameters.put("report_name", "营业预估");  
        parameters.put("report_time", DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));  
        modelMap.put("parameters", parameters);//map
	       
	    HashMap<String, String> map=new HashMap<String, String>();
	    map.put("firmId", posSalePlan.getFirm());
	    map.put("startdate", DateFormat.getStringByDate(posSalePlan.getStartdate(),"yyyy-MM-dd"));
	    map.put("enddate", DateFormat.getStringByDate(posSalePlan.getEnddate(),"yyyy-MM-dd"));
        modelMap.put("actionMap", map);
	 	modelMap.put("action", "/forecastMis/printCast.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,ForecastConstants.REPORT_CAST,ForecastConstants.REPORT_CAST);//判断跳转路径
        modelMap.put("reportUrl", rs.get("reportUrl"));//ireport文件地址
        modelMap.put("mis", mis);
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 营业预估excel表
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/exportCast")
	@ResponseBody
	public void exportCast(HttpServletResponse response, HttpServletRequest request, HttpSession session, SalePlan posSalePlan) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<SalePlan> disList =  forecastService.findPosSalePlan(posSalePlan);
		String fileName = "营业预估";
		Map<String, Object> condition = new HashMap<String,Object>();
		condition.put("posSalePlan", posSalePlan);
		pager.setPageSize(Integer.MAX_VALUE);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){              
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		forecastService.exportExcelcast(response.getOutputStream(), disList);
		
	}
/********************************************************营业预估end*************************************************/
	
/****************************************************预估菜品销售计划start*************************************************/
	
	/**
	 * 预估菜品销售计划
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/planList")
	public ModelAndView planList(ModelMap modelMap, HttpSession session, String type, Page page,
			String firmId, String firmName, ItemPlan posItemPlan, Date bdate,Date edate, String mis,String isDeclare) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn positn = commonMISBOHService.getPositn(session);
		positn.setAcct(session.getAttribute("ChoiceAcct").toString());
		firmId=positn.getCode();
		if (null==type) {
				// 获取分店code
			List<String> listStr = new ArrayList<String>();
			listStr = DateJudge.getDateList(DateJudge.getStringByDate(bdate, "yyyy-MM-dd"), DateJudge.getStringByDate(edate, "yyyy-MM-dd"));
			modelMap.put("dateList", listStr);
			modelMap.put("listStrSize", listStr.size());
			//List<String> listId = Arrays.asList(firmId.split(","));
			posItemPlan.setFirm(firmId);
			posItemPlan.setDat(bdate);
			posItemPlan.setStartdate(bdate);
			posItemPlan.setEnddate(edate);
			posItemPlan.setDept(posItemPlan.getDept());
			modelMap.put("posItemPlanList", forecastService.queryItemPlan(posItemPlan, positn));
		} else {
			List<String> listStr = new ArrayList<String>();
			String bdat = DateJudge.getStringByDate(new Date(), "yyyy-MM-dd");
			String edat = DateJudge.getLenTime(6, bdat);
			listStr = DateJudge.getDateList(bdat, edat);
			modelMap.put("dateList", listStr);
		}
		//默认mis的值是1
		if (null == mis) {
			mis = "1";
		}
		//初次加载页面传值
		if (null==bdate) {
			bdate = DateJudge.YYYY_MM_DD.parse(DateJudge.getLenTime(1, DateJudge.YYYY_MM_DD.format(new Date())));
		}
		if (edate==null) {
			edate = DateJudge.YYYY_MM_DD.parse(DateJudge.getLenTime(7, DateJudge.YYYY_MM_DD.format(new Date())));
		}
		Store store = new Store();
		store.setPk_group(session.getAttribute("ChoiceAcct").toString());
		store.setVcode(positn.getCode());
		if ("Y".equals(positn.getYnUseDept())) {
			modelMap.put("deptList", commonMISBOHService.getAllStoreDept(store));//根据门店编码获取所有部门
		}
		modelMap.put("pageobj", page);
		modelMap.put("bdate", bdate);
		modelMap.put("edate", edate);
		modelMap.put("msg", "a");
		modelMap.put("firmId", firmId);
		modelMap.put("firmName", firmName);
		modelMap.put("posItemPlan", posItemPlan);
		modelMap.put("mis", mis);
		modelMap.put("positn", positn);
		modelMap.put("isdept",positn.getYnUseDept());
		modelMap.put("isDeclare", isDeclare);
		return new ModelAndView(ForecastConstants.MIS_LIST_PLAN,modelMap);
	}
	
	/**
	 * 删除预估菜品销售计划
	 * @param modelMap
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/deletePlan")
	public ModelAndView deletePlan(ModelMap modelMap, HttpSession session,
			String firmId, String firmName, Date bdate, String mis,Page page,String chkValue) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn positn = commonMISBOHService.getPositn(session);
		firmId=positn.getCode();
		//默认mis的值是1
		if (null == mis) {
			mis = "1";
		}
		ItemPlan posItemPlan = new ItemPlan();
		//删除预估菜品 改为删除选择的一天 不是删除所有  wjf
		posItemPlan.setDat(bdate);
		//List<String> listId = Arrays.asList(firmId.split(","));
		posItemPlan.setFirm(firmId);
		forecastService.deleteAllPlan(posItemPlan);
		if (bdate==null) {
			bdate=new Date();
		}
		modelMap.put("posItemPlanList", forecastService.queryItemPlan(posItemPlan, positn));
		modelMap.put("bdate", bdate);
		modelMap.put("msg", "a");
		modelMap.put("firmId", firmId);
		modelMap.put("firmName", firmName);
		modelMap.put("mis", mis);
		modelMap.put("pageobj", page);
		modelMap.put("isdept",positn.getYnUseDept());
		return new ModelAndView(ForecastConstants.MIS_LIST_PLAN,modelMap);
	}
	
	/**
	 * 计算预估菜品销售计划
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/calPlan")
	public ModelAndView calPlan(ModelMap modelMap, HttpSession session, String firmId, 
			String firmName, Date bdate, Date edate, String mis,ItemPlan posItemPlan, Page page,String isDeclare,String salePlan) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn positn = commonMISBOHService.getPositn(session);
		firmId=positn.getCode();
		//默认mis的值是1
		if (null == mis) {
			mis = "1";
		}
		// 重新计算预估菜品销售计划
		SalePlan posSalePlan = new SalePlan();
		posSalePlan.setEnddate(bdate);
		posSalePlan.setStartdate(bdate);
		posSalePlan.setFirm(firmId);
		List<SalePlan> posSalePlanList = forecastService.findPosSalePlan(posSalePlan);
		FirmItemUse itemUse = new FirmItemUse();
		itemUse.setFirm(firmId);
		List<FirmItemUse> itemUseList = forecastService.findAllItemUse(itemUse);
		posItemPlan.setFirm(firmId);
		posItemPlan.setDat(bdate);
		if(isDeclare !=null && isDeclare.equals("1")){
			forecastService.saveCalPlan(firmId, bdate, edate,posItemPlan,session,salePlan,isDeclare);
			modelMap.put("msg", "a");//标记正常计算营业预估
		}else{
			if (posSalePlanList == null || posSalePlanList.size()==0) {
				modelMap.put("msg", "选择的日期内有没有做营业预估，请检查营业预估!");
			} else if (itemUseList.size()==0) {
				modelMap.put("msg", "没有设定点击率，不能自动计算预估!");
			} else {
				modelMap.put("msg", "a");//标记正常计算营业预估
				// 数组存放某日四个班所对应的营业预估值
//				double a[] = {posSalePlanList.get(0).getPax1(),posSalePlanList.get(0).getPax2(),posSalePlanList.get(0).getPax3(),posSalePlanList.get(0).getPax4()};
				//加入部门  posSalePlanList.get(0).getDept()
//				forecastService.saveCalPlan(firmId, bdate, a,posSalePlanList.get(0).getDept());
				forecastService.saveCalPlan(firmId, bdate, edate,posItemPlan,session,salePlan,isDeclare);
			}
		}
		List<String> listStr = new ArrayList<String>();
		listStr = DateJudge.getDateList(DateJudge.getStringByDate(bdate, "yyyy-MM-dd"), DateJudge.getStringByDate(edate, "yyyy-MM-dd"));
		posItemPlan.setStartdate(bdate);
		posItemPlan.setEnddate(edate);
		modelMap.put("dateList", listStr);
		modelMap.put("listStrSize", listStr.size());
		modelMap.put("posItemPlanList", forecastService.queryItemPlan(posItemPlan, positn));
		modelMap.put("pageobj", page);
		modelMap.put("bdate", bdate);
		modelMap.put("edate", edate);
		modelMap.put("firmId", firmId);
		modelMap.put("isDeclare", isDeclare);
		modelMap.put("firmName", firmName);
		modelMap.put("posItemPlan", posItemPlan);
		modelMap.put("mis", mis);
		modelMap.put("isdept",positn.getYnUseDept());
		return new ModelAndView(ForecastConstants.MIS_LIST_PLAN,modelMap);
	}
	
	/**
	 *  保存修改调整量（预估菜品销售计划）
	 * @param modelMap
	 * @param posSalePlan
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updatePlan")
	@ResponseBody
	public String updatePlan(ModelMap modelMap, HttpSession session, String firmId, ItemPlan posItemPlan)  {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		try {
			// 获取分店code
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			if(null != thePositn){
				firmId = thePositn.getCode();
			}
			forecastService.updateItemPlan(posItemPlan, firmId);
			return "1";
		} catch (Exception e) {
			return "0";
		}
	}
	
	/**
	 * 预估菜品销售计划打印
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printPlan")
	public ModelAndView printPlan(ModelMap modelMap, HttpSession session, String type, String firmId, Date bdate, String mis)throws CRUDException{
		ItemPlan posItemPlan = new ItemPlan();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		// 获取分店code
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if(null != thePositn){
			firmId = thePositn.getCode();
		}
		//默认mis的值是1
		if (null == mis) {
			mis = "1";
		}
		posItemPlan.setFirm(firmId);
		posItemPlan.setDat(bdate);
		List<ItemPlan> disList=forecastService.findPosItemPlan(posItemPlan);
		int num=disList.size()%19;
		for (int i = 0; i < 19-num; i++) {
			disList.add(null);
		}
		modelMap.put("List",disList);//list
 		HashMap<String, Object>  parameters = new HashMap<String, Object>(); 
        parameters.put("report_name", "分店菜品销售预估");//修改名字 wjf
        parameters.put("report_time", DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));  
        modelMap.put("parameters", parameters);//map
	       
	    HashMap<String, String> map=new HashMap<String, String>();
        modelMap.put("actionMap", map);
	 	modelMap.put("action", "/forecast/printPlan.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,ForecastConstants.REPORT_PLAN,ForecastConstants.REPORT_PLAN);//判断跳转路径
        modelMap.put("reportUrl", rs.get("reportUrl"));//ireport文件地址
        modelMap.put("msg", "a");
        modelMap.put("mis", mis);
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 预估菜品销售计划excel表
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/exportPlan")
	@ResponseBody
	public void exportPlan(HttpServletResponse response, String sort, String order, String mis,
			String firmId, HttpServletRequest request,HttpSession session, Date bdate,Date edate) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn positn = commonMISBOHService.getPositn(session);
		List<String> listStr = new ArrayList<String>();
		listStr = DateJudge.getDateList(DateJudge.getStringByDate(bdate, "yyyy-MM-dd"), DateJudge.getStringByDate(edate, "yyyy-MM-dd"));
		ItemPlan posItemPlan = new ItemPlan();
		firmId = positn.getCode();
		posItemPlan.setFirm(firmId);
		posItemPlan.setDat(bdate);
		posItemPlan.setStartdate(bdate);
		posItemPlan.setEnddate(edate);
		List<ItemPlan> disList=forecastService.queryItemPlan(posItemPlan,positn);
		String fileName = "预估菜品销售计划";
		Map<String, Object> condition = new HashMap<String,Object>();
		condition.put("posItemPlan", posItemPlan);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){              
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		forecastService.exportExcelPlan(response.getOutputStream(), disList,listStr);
		
	}
	
/********************************************************预估菜品销售计划end*************************************************/

	/***************************************手机预估相关2016.1.14css (如需加方法  加到此分割线之上)****************************************/
	/********************************************************营业预估start*******************************************
	 * 
	 */
	/**
	 * 分店MIS预估系统营业预估
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getDatesWAP")
	@ResponseBody
	public Object getDatesWAP(String jsonpcallback) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		//页面传值
		String yearr = DateJudge.getStringByDate(new Date(), "yyyy-MM-dd").substring(0, 4);
		String monthh = DateJudge.getStringByDate(new Date(), "yyyy-MM-dd").substring(5, 7);
		String bd = "";
		String ed = "";		
		if (ValueUtil.getIntValue(monthh)-1==0){
			bd = (ValueUtil.getIntValue(yearr)-1) +"-12-01";
			ed = (ValueUtil.getIntValue(yearr)-1) +"-12-31";
		}else{
			bd = yearr +"-"+ (ValueUtil.getIntValue(monthh)-1) + "-01";
			ed = yearr +"-"+ (ValueUtil.getIntValue(monthh)-1) +"-"+ DateJudge.getMonthEndDay(yearr +"-"+ (ValueUtil.getIntValue(monthh)-1) + "-01");
		}
		map.put("bdate", bd);
		map.put("edate", ed);
		map.put("year", yearr);
		map.put("month", monthh);
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/**
	 * 分店MIS预估系统营业预估
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/castListWAP")
	@ResponseBody
	public Object castListWAP(String firmName, String firmId, String yearr , String monthh) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		firmName = java.net.URLDecoder.decode(firmName,"UTF-8");
		Map<String,Object> map = new HashMap<String,Object>();
		String startdate = yearr + "-" + monthh + "-01";
		String enddate = yearr + "-" + monthh + "-" + DateJudge.getMonthEndDay(startdate);
		SalePlan posSalePlan = new SalePlan();
		posSalePlan.setFirm(firmId);
		posSalePlan.setStartdate(DateJudge.getDateByString(startdate));
		posSalePlan.setEnddate(DateJudge.getDateByString(enddate));
		posSalePlan.setDept(CodeHelper.replaceCode(posSalePlan.getDept()));
		List<SalePlan> list = forecastService.findPosSalePlan(posSalePlan);
		map.put("posSalePlanList", list);
		if (list != null && list.size() > 0) {
			map.put("posSalePlanHeJi", list.get(list.size() - 1));
		} else {
			map.put("posSalePlanHeJi", new SalePlan());
		}
		map.put("posSalePlan", posSalePlan);
		return JSONObject.fromObject(map).toString();
	}
	
	/**
	 * 分店MIS预估系统营业预估 计算  保存
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/calPosSalePlanWAP")
	@ResponseBody
	public Object calPosSalePlanWAP(String jsonpcallback, SalePlan salePlan) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		salePlan.setFirmNm(java.net.URLDecoder.decode(salePlan.getFirmNm(),"UTF-8"));
		try {
			// 计算分店MIS预估系统营业预估
			forecastService.saveCalPosSalePlan(salePlan);
			map.put("pr", "1");
			return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
		}catch (Exception e) {
			map.put("pr", "0");
			return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
		}
		
	}
	
	/**
	 *  保存修改调整量
	 * @param modelMap
	 * @param posSalePlan
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateSalePlanWAP")
	@ResponseBody
	public Object updateSalePlanWAP(SalePlan posSalePlan, String firmId)  {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		try {
			DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			forecastService.updateSalePlan(posSalePlan, firmId);
			map.put("pr", "1");
			return JSONObject.fromObject(map).toString();
		} catch (Exception e) {
			map.put("pr", "0");
			return JSONObject.fromObject(map).toString();
		}
	}
	
	/********************************************************营业预估end*******************************************
	/********************************************************菜品点击率start*************************************************/
	/**
	 * 分店MIS预估系统菜品点击率
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getDatesCTRWAP")
	@ResponseBody
	public Object getDatesCTRWAP(String jsonpcallback)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		//初次加载页面传值
		String bdate=DateFormat.getStringByDate(DateJudge.YYYY_MM_DD.parse(DateJudge.getLenTime(-7, DateJudge.YYYY_MM_DD.format(new Date()))), "yyyy-MM-dd");
		String edate=DateFormat.getStringByDate(new Date(), "yyyy-MM-dd");
		map.put("bdate", bdate);
		map.put("edate", edate);
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/**
	 * 分店MIS预估系统菜品点击率
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/listCostCTRWAP")
	@ResponseBody
	public Object listCostCTRWAP(FirmItemUse itemUse, String firmId, String firmName, Date bdate, Date edate) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		itemUse.setFirm(firmId);
		map.put("itemUseList", forecastService.findAllItemUse(itemUse));
		return JSONObject.fromObject(map).toString();
	}	
	
	/**
	 * 分店MIS预估系统菜品点击率
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/calculateWAP")
	@ResponseBody
	public Object calculateWAP(String jsonpcallback, String acct, String firmId, String firmName, Date bdate, Date edate) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		try{
			FirmItemUse itemUse = new FirmItemUse();
			itemUse.setFirm(firmId);
			Store store = new Store();
	    	store.setVcode(firmId);
	    	store = commonMISBOHService.getStore(store);
			// 重新计算点击率
			forecastService.saveNewCalculateItemuse(firmId, acct, bdate, edate, store);		
			List<FirmItemUse> itemUseList = forecastService.findAllItemUse(itemUse);
			if (itemUseList.size()==0) {
				map.put("dataNull", "dataNull");
			}
			map.put("pr", "1");
			return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
		} catch (Exception e) {
			map.put("pr", "0");
			return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
		}
	}
	
	/**
	 *  保存修改调整量
	 * @param modelMap
	 * @param itemUse
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateCTRWAP")
	@ResponseBody
	public Object updateCTRWAP(FirmItemUse itemUse, String firmId) {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		try {
			itemUse.setFirm(firmId);
			forecastService.update(itemUse);
			map.put("pr", "1");
			return JSONObject.fromObject(map).toString();
		} catch (Exception e) {
			map.put("pr", "0");
			return JSONObject.fromObject(map).toString();
		}
	}
/****************************************************预估菜品销售计划start*************************************************/
	
	/**
	 * 预估菜品销售计划
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/planListWAP")
	@ResponseBody
	public Object planListWAP(ItemPlan posItemPlan) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		posItemPlan.setStartdate(posItemPlan.getDat());
		posItemPlan.setEnddate(posItemPlan.getDat());
		posItemPlan.setDept(CodeHelper.replaceCode(posItemPlan.getDept()));
		map.put("posItemPlanList", forecastService.queryItemPlan(posItemPlan, new Positn()));
		return JSONObject.fromObject(map).toString();
	}	
	
	/**
	 * 计算预估菜品销售计划
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	@RequestMapping("/calPlanWAP")
	@ResponseBody
	public Object calPlanWAP(String jsonpcallback, ItemPlan posItemPlan) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		// 重新计算预估菜品销售计划
		SalePlan posSalePlan = new SalePlan();
		posSalePlan.setEnddate(posItemPlan.getDat());
		posSalePlan.setStartdate(posItemPlan.getDat());
		posSalePlan.setFirm(posItemPlan.getFirm());
		List<SalePlan> posSalePlanList = forecastService.findPosSalePlan(posSalePlan);
		FirmItemUse itemUse = new FirmItemUse();
		itemUse.setFirm(posItemPlan.getFirm());
		List<FirmItemUse> itemUseList = forecastService.findAllItemUse(itemUse);
		posItemPlan.setDat(posItemPlan.getDat());
		List<ItemPlan> posItemPlanist = forecastService.findPosItemPlan(posItemPlan);
		if (posSalePlanList == null || posSalePlanList.size()==0) {
			map.put("msg", "选择的日期内有没有做营业预估，请检查营业预估!");
		} else if (itemUseList.size()==0) {
			map.put("msg", "没有设定点击率，不能自动计算预估!");
		} else {
			map.put("msg", "a");//标记正常计算营业预估
			forecastService.saveCalPlanWAP(posItemPlan.getFirm(), posItemPlan.getDat(), posItemPlan.getDat(), posItemPlan);
		}
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/**
	 *  保存修改调整量（预估菜品销售计划）
	 * @param modelMap
	 * @param posSalePlan
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updatePlanWAP")
	@ResponseBody
	public Object updatePlanWAP(ItemPlan posItemPlan)  {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		try {
			forecastService.updateItemPlan(posItemPlan, posItemPlan.getFirm());
			map.put("pr", "1");
			return JSONObject.fromObject(map).toString();
		} catch (Exception e) {
			map.put("pr", "0");
			return JSONObject.fromObject(map).toString();
		}
	}
	
}
