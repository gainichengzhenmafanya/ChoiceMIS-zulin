package com.choice.misboh.web.controller.salesforecast;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.constants.salesforecast.ForecastConstants;
import com.choice.misboh.domain.salesforecast.SalePlan;
import com.choice.misboh.service.salesforecast.PosSalePlanService;
import com.choice.scm.domain.Positn;


/***
 * 营业预估
 * @author wjf 20170730
 *
 */
@Controller
@RequestMapping("posSalePlan")
public class PosSalePlanController {

	@Autowired
	private PosSalePlanService posSalePlanService;
	
	/***
	 * 打开营业预估
	 * @param modelMap
	 * @param session
	 * @param salePlan
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/list")
	public ModelAndView list(ModelMap modelMap, HttpSession session, SalePlan salePlan) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if (null == salePlan.getStartdate() && null == salePlan.getEnddate()) {
			salePlan.setFirm(thePositn.getCode());
			salePlan.setFirmNm(thePositn.getDes());
			//得到当前月的开始和结束日期
			String nowDate = DateJudge.getStringByDate(new Date(), "yyyy-MM-dd");
			salePlan.setStartdate(DateJudge.getDateByString(DateJudge.getMonthStart(nowDate)));
			salePlan.setEnddate(DateJudge.getDateByString(DateJudge.getMonthEnd(nowDate)));
			//得到上个月的开始和结束日期
			String lastMonthDate = nowDate.substring(0, 5)+(Integer.parseInt(nowDate.substring(5,7)) - 1)+"-01";
			salePlan.setBdate(DateJudge.getDateByString(DateJudge.getMonthStart(lastMonthDate)));
			salePlan.setEdate(DateJudge.getDateByString(DateJudge.getMonthEnd(lastMonthDate)));
		}else{
			salePlan.setDept(CodeHelper.replaceCode(salePlan.getDept()));
			List<SalePlan> list = posSalePlanService.findPosSalePlan(salePlan);
			modelMap.put("posSalePlanList", list);
			if (list != null && list.size() > 0) {
				modelMap.put("posSalePlanHeJi", list.get(list.size() - 1));
			} else {
				modelMap.put("posSalePlanHeJi", new SalePlan());
			}
		}
		modelMap.put("salePlan", salePlan);
		return new ModelAndView(ForecastConstants.LIST_POSSALEPLAN,modelMap);
	}
	
	/***
	 * 计算营业预估
	 * @param modelMap
	 * @param session
	 * @param salePlan
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/calPosSalePlan")
	public ModelAndView calPosSalePlan(ModelMap modelMap, HttpSession session, SalePlan salePlan) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		salePlan.setAcct(session.getAttribute("ChoiceAcct").toString());
		// 重新计算分店MIS预估系统营业预估
		posSalePlanService.saveCalPosSalePlan(salePlan);
		modelMap.put("posSalePlanList", posSalePlanService.findPosSalePlan(salePlan));
		modelMap.put("salePlan", salePlan);
		return new ModelAndView(ForecastConstants.LIST_POSSALEPLAN,modelMap);
	}
	
	/**
	 *  保存修改调整量
	 * @param modelMap
	 * @param posSalePlan
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateSalePlan")
	@ResponseBody
	public String updateSalePlan(ModelMap modelMap, HttpSession session, SalePlan posSalePlan)  {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		try {
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			posSalePlanService.updateSalePlan(posSalePlan,thePositn.getCode());
			return "1";
		} catch (Exception e) {
			return "0";
		}
	}
	
	/***
	 * 导出营业预估
	 * @param response
	 * @param request
	 * @param session
	 * @param posSalePlan
	 * @throws Exception
	 */
	@RequestMapping("/exportCast")
	@ResponseBody
	public void exportCast(HttpServletResponse response, HttpServletRequest request, HttpSession session, SalePlan posSalePlan) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<SalePlan> disList =  posSalePlanService.findPosSalePlan(posSalePlan);
		String fileName = "营业预估";
		Map<String, Object> condition = new HashMap<String,Object>();
		condition.put("posSalePlan", posSalePlan);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){              
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		posSalePlanService.exportExcelcast(response.getOutputStream(), disList);
		
	}
	
}
