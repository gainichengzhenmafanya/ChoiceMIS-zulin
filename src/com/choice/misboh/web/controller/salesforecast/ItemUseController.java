package com.choice.misboh.web.controller.salesforecast;

import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.misboh.constants.salesforecast.ForecastConstants;
import com.choice.misboh.domain.salesforecast.FirmItemUse;
import com.choice.misboh.service.salesforecast.ItemUseService;
import com.choice.scm.domain.Positn;
import com.choice.scm.service.PositnService;


/***
 * 菜品点击率
 * @author wjf 20170801
 *
 */
@Controller
@RequestMapping("/itemUse")
public class ItemUseController {

	@Autowired
	private ItemUseService itemUseService;
	@Autowired
	private PositnService positnService;

	/***
	 * 进入菜品点击率
	 * @param modelMap
	 * @param session
	 * @param itemUse
	 * @param isDeclare
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/list")
	public ModelAndView list(ModelMap modelMap, HttpSession session, FirmItemUse itemUse, String isDeclare) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		itemUse.setFirm(thePositn.getCode());
		/**初次进入页面**/
		//1.参考日期  默认前一个月
		Date nowDate = new Date();
		if(null == itemUse.getBdate() || null == itemUse.getEdate()){
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(nowDate);
			calendar.add(Calendar.DATE, -31);
			itemUse.setBdate(calendar.getTime());
			calendar.setTime(nowDate);
			calendar.add(Calendar.DATE, -1);
			itemUse.setEdate(calendar.getTime());
			//得到营业预估方式
			thePositn = positnService.findPositnByCode(thePositn);
			itemUse.setVplantyp(thePositn.getVplantyp());
		}
		modelMap.put("itemUseList", itemUseService.findAllItemUse(itemUse));
		modelMap.put("itemUse", itemUse);
		if(isDeclare !=null && !isDeclare.equals("")){
			modelMap.put("isDeclare", "1");//判断是否走预估报货
		}
		return new ModelAndView(ForecastConstants.LIST_ITEMUSE, modelMap);
	}

	/***
	 * 计算菜品点击率
	 * @param modelMap
	 * @param session
	 * @param itemUse
	 * @param isDeclare
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/calculate")
	public ModelAndView calculate(ModelMap modelMap, HttpSession session, FirmItemUse itemUse, String isDeclare) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		itemUse.setFirm(thePositn.getCode());
		// 重新计算点击率
		String result = itemUseService.saveNewCalculateItemuse(itemUse);
		if(!"1".equals(result)){
			modelMap.put("msg", "dataNull");//提示数据为空
		} else {
			modelMap.put("msg", "ok");//改变msg的值，使计算等待提示信息不显示
		}
		List<FirmItemUse> itemUseList = itemUseService.findAllItemUse(itemUse);
		modelMap.put("itemUseList", itemUseList);
		modelMap.put("itemUse", itemUse);//里面存档口
		modelMap.put("isDeclare", isDeclare);
		return new ModelAndView(ForecastConstants.LIST_ITEMUSE, modelMap);
	}

	/***
	 * 保存菜品点击率
	 * @param session
	 * @param itemUse
	 * @return
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public String update(HttpSession session, FirmItemUse itemUse) {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		try {
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			itemUse.setFirm(thePositn.getCode());
			itemUseService.update(itemUse);
			return "1";
		} catch (CRUDException e) {
			e.printStackTrace();
			return "0";
		}
	}

	/***
	 * 导出菜品点击率
	 * @param response
	 * @param request
	 * @param session
	 * @param itemUse
	 * @throws Exception
	 */
	@RequestMapping("/export")
	@ResponseBody
	public void export(HttpServletResponse response, HttpServletRequest request, HttpSession session, FirmItemUse itemUse) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		if(null != thePositn){
			itemUse.setFirm(thePositn.getCode());
		}
		List<FirmItemUse> disList=itemUseService.findAllItemUse(itemUse);
		String fileName = "菜品点击率表";
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");
		itemUseService.exportExcel(response.getOutputStream(), disList);
	}
	
	/**
	 * 手动添加菜品
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/addNewItem")
	public ModelAndView addNewItem(ModelMap modelMap, HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		modelMap.put("itemList", itemUseService.addNewItem(thePositn.getCode()));
		return new ModelAndView(ForecastConstants.MIS_ADDITEM, modelMap);
	}
	
}
