package com.choice.misboh.web.controller.salesforecast;

import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.constants.salesforecast.ForecastChoice3Constants;
import com.choice.misboh.domain.salesforecast.SalePlan;
import com.choice.misboh.service.salesforecast.PosSalePlanChoice3Service;
import com.choice.scm.domain.Positn;
import com.choice.scm.util.ReadReportUrl;


/***
 * 营业预估
 * @author wjf
 *
 */
@Controller
@RequestMapping("posSalePlanChoice3")
public class PosSalePlanChoice3Controller {

	@Autowired
	private PosSalePlanChoice3Service posSalePlanChoice3Service;
	
	/********************************************************营业预估start*************************************************/
	
	/**
	 * 分店MIS预估系统营业预估
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/listPosSalePlan")
	public ModelAndView listPosSalePlan(ModelMap modelMap, HttpSession session, SalePlan salePlan) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		salePlan.setFirm(thePositn.getCode());
		
		/**初次进入页面**/
		//1.预估日期  默认今天 往后7天 
		Date nowDate = new Date();
		nowDate = DateFormat.formatDate(nowDate, "yyyy-MM-dd");
		salePlan.setDat(nowDate);
		if(null == salePlan.getStartdate() || null == salePlan.getEnddate()){
			salePlan.setStartdate(nowDate);
			Calendar calendar = Calendar.getInstance();  
	        calendar.setTime(nowDate);  
	        calendar.add(Calendar.DATE, 7);  
			salePlan.setEnddate(calendar.getTime());
		}
		//2.参考日期  默认前一个月
		if(null == salePlan.getBdate() || null == salePlan.getEdate()){
			Calendar calendar = Calendar.getInstance();  
	        calendar.setTime(nowDate);
	        calendar.add(Calendar.DATE, -31);
	        salePlan.setBdate(calendar.getTime());
	        calendar.setTime(nowDate);
	        calendar.add(Calendar.DATE, -1);
	        salePlan.setEdate(calendar.getTime());
		}
		
		modelMap.put("posSalePlanList", posSalePlanChoice3Service.findPosSalePlan(salePlan));
		modelMap.put("salePlan", salePlan);
		return new ModelAndView(ForecastChoice3Constants.MIS_LIST_FORECAST,modelMap);
	}
	
	/**
	 * 分店MIS预估系统营业预估
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/calPosSalePlan")
	public ModelAndView calPosSalePlan(ModelMap modelMap, HttpSession session,SalePlan salePlan) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		salePlan.setFirm(thePositn.getCode());
		posSalePlanChoice3Service.saveCalPosSalePlan(salePlan);
		modelMap.put("posSalePlanList", posSalePlanChoice3Service.findPosSalePlan(salePlan));
		Date nowDate = new Date();
		nowDate = DateFormat.formatDate(nowDate, "yyyy-MM-dd");
		salePlan.setDat(nowDate);
		modelMap.put("salePlan", salePlan);
		return new ModelAndView(ForecastChoice3Constants.MIS_LIST_FORECAST,modelMap);
	}
	
	/**
	 *  保存修改调整量
	 * @param modelMap
	 * @param posSalePlan
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateSalePlan")
	@ResponseBody
	public int updateSalePlan(ModelMap modelMap, HttpSession session, SalePlan salePlan) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		salePlan.setFirm(thePositn.getCode());
		return posSalePlanChoice3Service.updateSalePlan(salePlan);
	}
	
	/**
	 * 营业预估打印
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printCast")
	public ModelAndView printCast(ModelMap modelMap, HttpSession session,String type, SalePlan salePlan)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		salePlan.setFirm(thePositn.getCode());
		List<SalePlan> salePlanList = posSalePlanChoice3Service.findPosSalePlan(salePlan);
		modelMap.put("List",salePlanList);//list
 		HashMap<String, Object>  parameters = new HashMap<String, Object>(); 
        parameters.put("report_name", "营业预估");  
        parameters.put("report_time", DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));  
        modelMap.put("parameters", parameters);//map
	       
	    HashMap<String, String> map=new HashMap<String, String>();
	    map.put("startdate", DateFormat.getStringByDate(salePlan.getStartdate(),"yyyy-MM-dd"));
	    map.put("enddate", DateFormat.getStringByDate(salePlan.getEnddate(),"yyyy-MM-dd"));
        modelMap.put("actionMap", map);
	 	modelMap.put("action", "/posSalePlanChoice3/printCast.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,ForecastChoice3Constants.REPORT_CAST,ForecastChoice3Constants.REPORT_CAST);//判断跳转路径
        modelMap.put("reportUrl", rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 营业预估excel表
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/exportCast")
	@ResponseBody
	public void exportCast(HttpServletResponse response, HttpServletRequest request, HttpSession session, SalePlan salePlan) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn thePositn = (Positn)session.getAttribute("accountPositn");
		salePlan.setFirm(thePositn.getCode());
		salePlan.setFirmNm(thePositn.getDes());
		String fileName = "营业预估";
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){              
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");
		List<SalePlan> salePlanList = posSalePlanChoice3Service.findPosSalePlan(salePlan);
		posSalePlanChoice3Service.exportExcelcast(response.getOutputStream(), salePlan, salePlanList);
		
	}
/********************************************************营业预估end*************************************************/
}
