package com.choice.misboh.web.controller.chineseReport;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.misboh.commonutil.MisUtil;
import com.choice.misboh.commonutil.ReadReport;
import com.choice.misboh.commonutil.report.ExportExcel;
import com.choice.misboh.commonutil.report.ReadReportConstants;
import com.choice.misboh.constants.chineseReport.ChineseSalesReportConstant;
import com.choice.misboh.constants.reportMis.MisStringConstant;
import com.choice.misboh.domain.common.CommonMethod;
import com.choice.misboh.domain.reportMis.PublicEntity;
import com.choice.misboh.domain.reportMis.ReportObject;
import com.choice.misboh.domain.store.Store;
import com.choice.misboh.service.chineseReport.ChineseSalesReportService;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.misboh.web.controller.reportMis.MisBohBaseController;

/**
 * 描述：中餐报表-销售分析
 * @author 马振
 * 创建时间：2015-6-25 上午11:06:25
 */
@Controller
@RequestMapping("chineseSalesReport")
public class ChineseSalesReportController extends MisBohBaseController {

	@Autowired
	private CommonMISBOHService commonMISBOHService; 
	
	@Autowired
	private ChineseSalesReportService chineseSalesReportService;
	
	/**
	 * 描述：跳转到报表页面
	 * @author 马振
	 * 创建时间：2015-6-25 上午11:23:40
	 * @param modelMap
	 * @param reportName
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toReport")
	public ModelAndView toReport(ModelMap modelMap,String reportName,HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("condition", new PublicEntity());
		modelMap.put("sftList", commonMISBOHService.findAllShiftsft(new CommonMethod()));
		modelMap.put("pk_store", commonMISBOHService.getPkStore(session).getPk_store());
		getRoleTiem(modelMap,session);
		return new ModelAndView(ReadReportConstants.getReportURL(ChineseSalesReportConstant.class, reportName),modelMap);
	}
	
	/**
	 * 描述：报表导出
	 * @author 马振
	 * 创建时间：2015-4-23 下午2:17:51
	 * @param response
	 * @param request
	 * @param session
	 * @param publicEntity
	 * @param reportName
	 * @param headers
	 * @throws CRUDException
	 */
	@RequestMapping("/exportReport")
	public void exportReport(HttpServletResponse response,HttpServletRequest request,HttpSession session,PublicEntity publicEntity,String reportName,String headers,String page,String rows) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		//设置分页，查询所有数据
		publicEntity.getPager().setPageSize(Integer.MAX_VALUE);
		publicEntity.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ReadReport.getReportName(MisStringConstant.class, reportName));
		
		//获取并执行查询方法，获取查询结果
		ReportObject<Map<String,Object>> result = MisUtil.execMethod(chineseSalesReportService, reportName, publicEntity);
		if(null != result.getFooter())
			result.getRows().addAll(result.getFooter());
		if(null != headers || !"".equals(headers))
			ExportExcel.creatWorkBook(request,response,result.getRows(),ReadReport.getReportNameCN(MisStringConstant.class, reportName),null,headers);
		else
			ExportExcel.creatWorkBook(ReadReport.getReportNameCN(MisStringConstant.class, reportName),request,response, result.getRows(), ReadReport.getReportNameCN(MisStringConstant.class, reportName), dictColumnsService.listDictColumnsByAccount(dictColumns, ReadReport.getDefaultHeader(MisStringConstant.class, reportName)));
	}
	
	/**
	 * 描述：项目销售统计
	 * @author 马振
	 * 创建时间：2015-6-27 下午2:36:44
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("findProjectSalesStatistics")
	@ResponseBody
	public ReportObject<Map<String,Object>> findProjectSalesStatistics(PublicEntity condition,HttpSession session) throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return chineseSalesReportService.findProjectSalesStatistics(condition);
	}
	
	/**
	 * 描述：菜品销售排行榜
	 * @author 马振
	 * 创建时间：2015-7-14 下午2:15:47
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("queryFoodList")
	@ResponseBody
	public ReportObject<Map<String,Object>> queryFoodList(PublicEntity condition,HttpSession session)throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return chineseSalesReportService.queryFoodList(condition);
	}
	
	/**********************************************时段销售分析**********************************************/
	
	/**
	 * 描述：查询数据 客单-时段销售分析报表
	 * @author 马振
	 * 创建时间：2015-6-27 下午2:58:09
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("querySaleTcByTime")
	@ResponseBody
	public ReportObject<Map<String,Object>> querySaleTcByTime(ModelMap modelMap,PublicEntity condition,HttpSession session)throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return chineseSalesReportService.querySaleTcByTime(condition);
	}
	
	/**
	 * 描述：查询数据 类别-时段销售分析报表
	 * @author 马振
	 * 创建时间：2015-6-27 下午2:58:00
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("querySaleTypeByTime")
	@ResponseBody
	public ReportObject<Map<String,Object>> querySaleTypeByTime(ModelMap modelMap,PublicEntity condition,HttpSession session)throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return chineseSalesReportService.querySaleTypeByTime(condition);
	}
	
	/**********************************************套餐销售统计**********************************************/
	
	/**
	 * 描述：套餐明细统计表
	 * @author 马振
	 * 创建时间：2015-6-27 下午2:57:51
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryMxtjb")
	@ResponseBody
	public Object queryMxtjb(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return chineseSalesReportService.queryMxtjb(condition);
	}
	
	/**
	 * 描述：套餐销售统计表
	 * @author 马振
	 * 创建时间：2015-6-27 下午2:57:41
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryXstjb")
	@ResponseBody
	public Object queryXstjb(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return chineseSalesReportService.queryXstjb(condition);
	}	
	
	/**********************************************集团消退菜分析**********************************************/
	
	/**
	 * 描述：查询消退菜分析
	 * @author 马振
	 * 创建时间：2015-7-14 下午2:18:59
	 * @param modelMap
	 * @param session
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findGroupCRDishes")
	@ResponseBody
	public Object findGroupCRDishes(ModelMap modelMap,HttpSession session,PublicEntity condition) throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return chineseSalesReportService.findGroupCRDishes(condition);
	}
	
	/**
	 * 描述：查询消退菜分析表头
	 * @author 马振
	 * 创建时间：2015-7-14 下午2:18:50
	 * @param store
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findGroupCRDishesHeader")
	@ResponseBody
	public Object findHeadersForCRDishes(Store store,HttpSession session) throws CRUDException{
		if(null == store.getPk_store() || "".equals(store.getPk_store()))
			store.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<Store> listStore = new ArrayList<Store>();
		listStore.add(commonMISBOHService.getPkStore(session));
		return listStore;
	}
	
	/**
	 * 描述：查询消退菜分析_单店退菜明细
	 * @author 马振
	 * 创建时间：2015-7-14 下午2:18:37
	 * @param modelMap
	 * @param session
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findGroupCRDishesByFirm")
	@ResponseBody
	public Object findGroupCRDishesByFirm(ModelMap modelMap,HttpSession session,PublicEntity condition) throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return chineseSalesReportService.findGroupCRDishesByFirm(condition);
	}
	
	/**********************************************集团消退菜分析**********************************************/
	
	/************************************************排班报表************************************************/
	
	/**
	 * 描述：排班报表
	 * @author 马振
	 * 创建时间：2015-10-30 下午3:19:26
	 * @param modelMap
	 * @param session
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/createScheduleReport")
	@ResponseBody
	public Object createScheduleReport(ModelMap modelMap,HttpSession session,PublicEntity condition) throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return chineseSalesReportService.createScheduleReport(condition);
	}
}