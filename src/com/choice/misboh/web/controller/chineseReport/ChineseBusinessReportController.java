package com.choice.misboh.web.controller.chineseReport;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.MisUtil;
import com.choice.misboh.commonutil.ReadReport;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.commonutil.report.ExportExcel;
import com.choice.misboh.commonutil.report.ReadReportConstants;
import com.choice.misboh.constants.chineseReport.ChineseBusinessReportConstant;
import com.choice.misboh.constants.reportMis.MisStringConstant;
import com.choice.misboh.domain.BaseRecord.Marsaleclass;
import com.choice.misboh.domain.common.CommonMethod;
import com.choice.misboh.domain.reportMis.PublicEntity;
import com.choice.misboh.domain.reportMis.ReportObject;
import com.choice.misboh.service.BaseRecord.BaseRecordService;
import com.choice.misboh.service.chineseReport.ChineseBusinessReportService;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.misboh.web.controller.reportMis.MisBohBaseController;
import com.choice.orientationSys.util.Page;

/**
 * 描述：中餐报表-营业分析
 * @author 马振
 * 创建时间：2015-6-25 上午11:06:25
 */
@Controller
@RequestMapping("chineseBusinessReport")
public class ChineseBusinessReportController extends MisBohBaseController {

	@Autowired
	private CommonMISBOHService commonMISBOHService; 
	
	@Autowired
	private ChineseBusinessReportService chineseBusinessReportService;
	
	@Autowired
	private BaseRecordService baseRecordService;
	
	/**
	 * 描述：跳转到报表页面
	 * @author 马振
	 * 创建时间：2015-6-25 上午11:23:40
	 * @param modelMap
	 * @param reportName
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toReport")
	public ModelAndView toReport(ModelMap modelMap,String reportName,HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("condition", new PublicEntity());
		modelMap.put("sftList", commonMISBOHService.findAllShiftsft(new CommonMethod()));
		modelMap.put("pk_store", commonMISBOHService.getPkStore(session).getPk_store());
		getRoleTiem(modelMap,session);
		return new ModelAndView(ReadReportConstants.getReportURL(ChineseBusinessReportConstant.class, reportName),modelMap);
	}
	
	/**
	 * 描述：报表导出
	 * @author 马振
	 * 创建时间：2015-4-23 下午2:17:51
	 * @param response
	 * @param request
	 * @param session
	 * @param publicEntity
	 * @param reportName
	 * @param headers
	 * @throws CRUDException
	 */
	@RequestMapping("/exportReport")
	public void exportReport(HttpServletResponse response,HttpServletRequest request,HttpSession session,PublicEntity publicEntity,String reportName,String headers,String page,String rows) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		//设置分页，查询所有数据
		publicEntity.getPager().setPageSize(Integer.MAX_VALUE);
		publicEntity.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ReadReport.getReportName(MisStringConstant.class, reportName));
		
		//获取并执行查询方法，获取查询结果
		ReportObject<Map<String,Object>> result = MisUtil.execMethod(chineseBusinessReportService, reportName, publicEntity);
		if(null != result.getFooter())
			result.getRows().addAll(result.getFooter());
		if(null != headers || !"".equals(headers))
			ExportExcel.creatWorkBook(request,response,result.getRows(),ReadReport.getReportNameCN(MisStringConstant.class, reportName),null,headers);
		else
			ExportExcel.creatWorkBook(ReadReport.getReportNameCN(MisStringConstant.class, reportName),request,response, result.getRows(), ReadReport.getReportNameCN(MisStringConstant.class, reportName), dictColumnsService.listDictColumnsByAccount(dictColumns, ReadReport.getDefaultHeader(MisStringConstant.class, reportName)));
	}
	
	/*****************************************************门店营业显示分析******************************************************/
	
	/**
	 * 描述：门店营业显示分析
	 * @author 马振
	 * 创建时间：2015-7-8 下午2:38:21
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findGroupBusinessShow")
	@ResponseBody
	public Object findGroupBusinessShow(ModelMap modelMap,PublicEntity condition,HttpSession session)throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("condition", new PublicEntity());
		return chineseBusinessReportService.findGroupBusinessShow(condition);
	}
	
	/*****************************************************门店收入分析******************************************************/
	
	/**
	 * 描述：门店收入分析（获取表头）
	 * @author 马振
	 * 创建时间：2015-7-9 上午8:56:05
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findHeaderForGroupIncome")
	@ResponseBody
	public Object findHeaderForGroupIncome(HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return commonMISBOHService.findPayment();
	}
	
	/**
	 * 描述：门店收入分析
	 * @author 马振
	 * 创建时间：2015-7-9 上午8:56:15
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findGroupIncome")
	@ResponseBody
	public Object findGroupIncome(PublicEntity condition,HttpSession session)throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return chineseBusinessReportService.findGroupIncome(condition);
	}
	
	/*****************************************************门店销售分析******************************************************/
	
	/**
	 * 描述：门店销售分析报表（汇总查询）
	 * @author 马振
	 * 创建时间：2015-7-9 上午8:56:25
	 * @param modelMap
	 * @param session
	 * @param publicEntity
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findGroupSales")
	@ResponseBody
	public Object findGroupSales(ModelMap modelMap,HttpSession session,PublicEntity publicEntity) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null == publicEntity.getPk_store() || "".equals(publicEntity.getPk_store()))
			publicEntity.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		//若开始日期为空，则赋值当前日期
		if (ValueUtil.IsEmpty(publicEntity.getBdat())) {
			publicEntity.setBdat(DateJudge.getNowDate());
		}
		//若结束日期为空，则赋值当前日期
		if (ValueUtil.IsEmpty(publicEntity.getEdat())) {
			publicEntity.setEdat(DateJudge.getNowDate());
		}
		return chineseBusinessReportService.findGroupSalesSum(publicEntity);
	}
	
	/**
	 * 描述：查询大类 (中餐)
	 * @author 马振
	 * 创建时间：2015-6-25 下午2:50:39
	 * @param marsaleclass
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findAllMarsaleclassOne")
	@ResponseBody
	public Object findAllMarsaleclassOne(Marsaleclass marsaleclass,Page page) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return baseRecordService.findAllMarsaleclassOne(marsaleclass,page);
	}
	
	/**
	 * 描述：查询小类 (中餐)
	 * @author 马振
	 * 创建时间：2015-6-25 下午2:50:21
	 * @param marsaleclass
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findAllMarsaleclassTwo")
	@ResponseBody
	public Object findAllMarsaleclassTwo(Marsaleclass marsaleclass,Page page) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return baseRecordService.findAllMarsaleclassTwo(marsaleclass,page);
	}
	
	/*****************************************************门店营业区域分析******************************************************/
	
	/**
	 * 描述：跳转到门店营业区域分析
	 * @author 马振
	 * 创建时间：2015-7-13 下午2:46:32
	 * @param modelMap
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("toReportJTRFX")
	public ModelAndView toReportJTRFX(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
		modelMap.put("pk_store", commonMISBOHService.getPkStore(session).getPk_store());
		getRoleTiem(modelMap,session);
		return new ModelAndView(ChineseBusinessReportConstant.BUSSINESS_JTRFX,modelMap);
	}
	
	/**
	 * 描述：excel导出  门店区域营业分析
	 * @author 马振
	 * 创建时间：2015-7-13 下午3:02:17
	 * @param modelMap
	 * @param response
	 * @param request
	 * @param condition
	 * @param headers
	 * @param title
	 * @throws CRUDException
	 */
	@RequestMapping("exportReportJTRFX")
	public void exportReportJTRFX(ModelMap modelMap,HttpServletResponse response,HttpServletRequest request,PublicEntity condition,String headers,String title)throws CRUDException{
		ReportObject<Map<String,String>> obj = chineseBusinessReportService.findJTRFX(condition);
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);	
		List<Map<String,String>> list = obj.getRows();
		ExportExcel.creatWorkBook(request, response, list, "营业日报区域汇总表",null, headers);
	}
	
	/**
	 * 描述：门店区域营业分析，查询表头
	 * @author 马振
	 * 创建时间：2015-7-13 下午3:02:51
	 * @param modelMap
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findHeaderForJTRFX")
	@ResponseBody
	public Object findHeaderForJTRFX(ModelMap modelMap) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("pay",commonMISBOHService.findPayment());
		map.put("dept",commonMISBOHService.findAllGroupDept());
		return map;
	}
	
	/**
	 * 描述：门店区域营业分析
	 * @author 马振
	 * 创建时间：2015-7-13 下午3:03:36
	 * @param modelMap
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("findJTRFX")
	@ResponseBody
	public ReportObject<Map<String,String>> findJTRFX(ModelMap modelMap,PublicEntity condition)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
		return chineseBusinessReportService.findJTRFX(condition);
	}
	
	/*****************************************************门店点菜出错率分析******************************************************/

	/**
	 * 描述：门店点菜出错率分析
	 * @author 马振
	 * 创建时间：2015-7-13 下午5:14:19
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findCaiPinChuCuoLv")
	@ResponseBody
	public Object findCaiPinChuCuoLv(PublicEntity condition,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		return chineseBusinessReportService.findCaiPinChuCuoLv(condition);
	}
}