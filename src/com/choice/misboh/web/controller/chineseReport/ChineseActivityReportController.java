package com.choice.misboh.web.controller.chineseReport;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.misboh.commonutil.MisUtil;
import com.choice.misboh.commonutil.ReadReport;
import com.choice.misboh.commonutil.report.ExportExcel;
import com.choice.misboh.commonutil.report.ReadReportConstants;
import com.choice.misboh.constants.chineseReport.ChineseActivityReportConstant;
import com.choice.misboh.constants.reportMis.MisStringConstant;
import com.choice.misboh.domain.BaseRecord.ActTyp;
import com.choice.misboh.domain.common.CommonMethod;
import com.choice.misboh.domain.reportMis.PublicEntity;
import com.choice.misboh.domain.reportMis.ReportObject;
import com.choice.misboh.service.BaseRecord.BaseRecordService;
import com.choice.misboh.service.chineseReport.ChineseActivityReportService;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.misboh.web.controller.reportMis.MisBohBaseController;
import com.choice.orientationSys.util.Page;

/**
 * 描述：中餐报表-活动分析
 * @author 马振
 * 创建时间：2015-6-25 上午11:06:25
 */
@Controller
@RequestMapping("chineseActivityReport")
public class ChineseActivityReportController extends MisBohBaseController {

	@Autowired
	private CommonMISBOHService commonMISBOHService; 
	
	@Autowired
	private ChineseActivityReportService chineseActivityReportService;
	
	@Autowired
	private BaseRecordService baseRecordService;
	
	/**
	 * 描述：跳转到报表页面
	 * @author 马振
	 * 创建时间：2015-6-25 上午11:23:40
	 * @param modelMap
	 * @param reportName
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toReport")
	public ModelAndView toReport(ModelMap modelMap,String reportName,HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("condition", new PublicEntity());
		modelMap.put("sftList", commonMISBOHService.findAllShiftsft(new CommonMethod()));
		modelMap.put("pk_store", commonMISBOHService.getPkStore(session).getPk_store());
		getRoleTiem(modelMap,session);
		return new ModelAndView(ReadReportConstants.getReportURL(ChineseActivityReportConstant.class, reportName),modelMap);
	}
	
	/**
	 * 描述：报表导出
	 * @author 马振
	 * 创建时间：2015-4-23 下午2:17:51
	 * @param response
	 * @param request
	 * @param session
	 * @param publicEntity
	 * @param reportName
	 * @param headers
	 * @throws CRUDException
	 */
	@RequestMapping("/exportReport")
	public void exportReport(HttpServletResponse response,HttpServletRequest request,HttpSession session,PublicEntity publicEntity,String reportName,String headers,String page,String rows) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		//设置分页，查询所有数据
		publicEntity.getPager().setPageSize(Integer.MAX_VALUE);
		publicEntity.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ReadReport.getReportName(MisStringConstant.class, reportName));
		
		//获取并执行查询方法，获取查询结果
		ReportObject<Map<String,Object>> result = MisUtil.execMethod(chineseActivityReportService, reportName, publicEntity);
		if(null != result.getFooter())
			result.getRows().addAll(result.getFooter());
		
		if(null != headers || !"".equals(headers))
			ExportExcel.creatWorkBook(request,response,result.getRows(),ReadReport.getReportNameCN(MisStringConstant.class, reportName),null,headers);
		else
			ExportExcel.creatWorkBook(ReadReport.getReportNameCN(MisStringConstant.class, reportName),request,response, result.getRows(), ReadReport.getReportNameCN(MisStringConstant.class, reportName), dictColumnsService.listDictColumnsByAccount(dictColumns, ReadReport.getDefaultHeader(MisStringConstant.class, reportName)));
	}
	
	/**
	 * 描述：集团活动分析(按日)
	 * @author 马振
	 * 创建时间：2015-7-14 下午1:47:41
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryJiTuanHuoDongFenXi")
	@ResponseBody
	public Object queryJiTuanHuoDongFenXi(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		return chineseActivityReportService.queryJiTuanHuoDongFenXi(condition);
	}
	
	/**
	 * 描述：集团活动分析(按月)
	 * @author 马振
	 * 创建时间：2015-7-14 下午1:48:08
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryJiTuanHuoDongFenXiByMonth")
	@ResponseBody
	public Object queryJiTuanHuoDongFenXiByMonth(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		return chineseActivityReportService.queryJiTuanHuoDongFenXiByMonth(condition);
	}
	
	/**
	 * 描述：跳转到台位活动明细报表
	 * @author 马振
	 * 创建时间：2015-7-14 上午10:38:21
	 * @param modelMap
	 * @param reportName
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toReportByTbl")
	public ModelAndView toReportByTbl(ModelMap modelMap,String reportName,HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("condition", new PublicEntity());
		Page page=new Page();
		page.setPageSize(Integer.MAX_VALUE);
		List<ActTyp> acttypList = baseRecordService.findAllActTyp();
		modelMap.put("acttypList", acttypList);//大类
		List<PublicEntity> acttypminList = baseRecordService.findAllActTypMin();
		modelMap.put("acttypminList", acttypminList);//活动小类);
		getRoleTiem(modelMap,session);
		return new ModelAndView(ChineseActivityReportConstant.REPORT_SHOW_ACTMBYTBL,modelMap);
	}
	
	/**
	 * 描述：台位活动明细报表
	 * @author 马振
	 * 创建时间：2015-7-14 上午8:59:05
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryActmByTbl")
	@ResponseBody
	public Object queryActmByTbl(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return chineseActivityReportService.findActmByTbl(condition);
	}
	
	/**
	 * 菜品活动明细报表
	 * @param modelMap
	 * @param condition
	 * @return
	 * @throws CRUDException
	 * @author YGB
	 */
	@RequestMapping("/queryActmByItem")
	@ResponseBody
	public Object queryActmByItem(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return chineseActivityReportService.findActmByItem(condition);
	}
}