package com.choice.misboh.web.controller.chineseReport;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.misboh.commonutil.MisUtil;
import com.choice.misboh.commonutil.ReadReport;
import com.choice.misboh.commonutil.report.ExportExcel;
import com.choice.misboh.commonutil.report.ReadReportConstants;
import com.choice.misboh.constants.chineseReport.ChineseFinancialReportConstant;
import com.choice.misboh.constants.reportMis.MisStringConstant;
import com.choice.misboh.domain.BaseRecord.Marsaleclass;
import com.choice.misboh.domain.common.CommonMethod;
import com.choice.misboh.domain.reportMis.PublicEntity;
import com.choice.misboh.domain.reportMis.ReportObject;
import com.choice.misboh.service.BaseRecord.BaseRecordService;
import com.choice.misboh.service.chineseReport.ChineseFinancialReportService;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.misboh.web.controller.reportMis.MisBohBaseController;
import com.choice.orientationSys.util.Page;

/**
 * 描述：中餐报表-财务分析
 * @author 马振
 * 创建时间：2015-6-25 上午11:06:25
 */
@Controller
@RequestMapping("chineseFinancialReport")
public class ChineseFinancialReportController extends MisBohBaseController {

	@Autowired
	private CommonMISBOHService commonMISBOHService; 
	
	@Autowired
	private ChineseFinancialReportService chineseFinancialReportService;
	
	@Autowired
	private BaseRecordService baseRecordService;
	
	/**
	 * 描述：跳转到报表页面
	 * @author 马振
	 * 创建时间：2015-6-25 上午11:23:40
	 * @param modelMap
	 * @param reportName
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toReport")
	public ModelAndView toReport(ModelMap modelMap,String reportName,HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("condition", new PublicEntity());
		modelMap.put("sftList", commonMISBOHService.findAllShiftsft(new CommonMethod()));
		modelMap.put("pk_store", commonMISBOHService.getPkStore(session).getPk_store());
		getRoleTiem(modelMap,session);
		return new ModelAndView(ReadReportConstants.getReportURL(ChineseFinancialReportConstant.class, reportName),modelMap);
	}
	
	/**
	 * 描述：报表导出
	 * @author 马振
	 * 创建时间：2015-4-23 下午2:17:51
	 * @param response
	 * @param request
	 * @param session
	 * @param publicEntity
	 * @param reportName
	 * @param headers
	 * @throws CRUDException
	 */
	@RequestMapping("/exportReport")
	public void exportReport(HttpServletResponse response,HttpServletRequest request,HttpSession session,PublicEntity publicEntity,String reportName,String headers,String page,String rows) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		//设置分页，查询所有数据
		publicEntity.getPager().setPageSize(Integer.MAX_VALUE);
		publicEntity.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ReadReport.getReportName(MisStringConstant.class, reportName));
		
		//获取并执行查询方法，获取查询结果
		ReportObject<Map<String,Object>> result = MisUtil.execMethod(chineseFinancialReportService, reportName, publicEntity);
		if(null != result.getFooter())
			result.getRows().addAll(result.getFooter());
		if(null != headers || !"".equals(headers))
			ExportExcel.creatWorkBook(request,response,result.getRows(),ReadReport.getReportNameCN(MisStringConstant.class, reportName),null,headers);
		else
			ExportExcel.creatWorkBook(ReadReport.getReportNameCN(MisStringConstant.class, reportName),request,response, result.getRows(), ReadReport.getReportNameCN(MisStringConstant.class, reportName), dictColumnsService.listDictColumnsByAccount(dictColumns, ReadReport.getDefaultHeader(MisStringConstant.class, reportName)));
	}
	
	/**
	 * 描述：跳转到收入日结算报表
	 * @author 马振
	 * 创建时间：2015-6-26 下午3:49:13
	 * @param modelMap
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("toReportRJS")
	public ModelAndView toReportRJS(ModelMap modelMap,HttpSession session)throws CRUDException{
		modelMap.put("sftList", commonMISBOHService.findAllShiftsft(new CommonMethod()));
		modelMap.put("pk_store", commonMISBOHService.getPkStore(session).getPk_store());
		getRoleTiem(modelMap,session);
		return new ModelAndView(ChineseFinancialReportConstant.BUSSINESS_RJS,modelMap);
	}
	
	/**
	 * 描述：查询数据   收入日结算报表 
	 * @author 马振
	 * 创建时间：2015-6-26 下午3:55:49
	 * @param modelMap
	 * @param publicEntity
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("getDataRJS")
	@ResponseBody
	public ReportObject<Map<String,String>> getDataRJS(ModelMap modelMap,PublicEntity publicEntity,CommonMethod commonMethod,HttpSession session)throws CRUDException{
		if(null == publicEntity.getPk_store() || "".equals(publicEntity.getPk_store()))
			publicEntity.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return chineseFinancialReportService.getDataRJS(publicEntity,commonMethod);
	}
	
	/**
	* excel导出  收入日结算报表
	* @throws CRUDException
	*/
	@RequestMapping("exportReportRJS")
	public void exportReportRJS(ModelMap modelMap,HttpServletResponse response,HttpServletRequest request,PublicEntity publicEntity,String headers,String title,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		ReportObject<Map<String,String>> obj = chineseFinancialReportService.exportDataRJS();
		List<Map<String,String>> list = obj.getRows();
		ExportExcel.creatWorkBook(request, response, list, "收入日结算报表",null, headers);
	}
	
	//////////////////////////////////////////////营业日报表 ///////////////////////////////////////////
	
	/**
	 * 描述：跳转到营业报表   营业日报表 
	 * @author 马振
	 * 创建时间：2015-6-26 下午5:28:04
	 * @param modelMap
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("toReportYYBB")
	public ModelAndView toReportYYBB(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
		modelMap.put("pk_store", commonMISBOHService.getPkStore(session).getPk_store());
		getRoleTiem(modelMap,session);
		return new ModelAndView(ChineseFinancialReportConstant.BUSSINESS_YYBB,modelMap);
	}
	
	/**
	 * 描述：营业日报表 （获取表头）
	 * @author 马振
	 * 创建时间：2015-6-26 下午5:29:47
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findHeaderForYYBB")
	@ResponseBody
	public Object findHeaderForYYBB(HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("sfts",commonMISBOHService.findAllShiftsft(new CommonMethod()));
		return map;
	}
	
	/**
	 * 描述：查询数据   营业日报表 
	 * @author 马振
	 * 创建时间：2015-6-26 下午5:31:29
	 * @param modelMap
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("getDataYYBB")
	@ResponseBody
	public ReportObject<Map<String,String>> getDataYYBB(ModelMap modelMap,PublicEntity condition)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
		return chineseFinancialReportService.getDataYYBB(condition);
	}
	
	/**
	 * 描述：excel导出  营业日报表 
	 * @author 马振
	 * 创建时间：2015-6-26 下午5:31:40
	 * @param modelMap
	 * @param response
	 * @param request
	 * @param condition
	 * @param headers
	 * @param title
	 * @throws CRUDException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("exportReportYYBB")
	public void exportReportYYBB(ModelMap modelMap,HttpServletResponse response,HttpServletRequest request,PublicEntity condition,String headers,String title)throws CRUDException{
		ReportObject<Map<String,String>> obj = getDataYYBB(modelMap,condition);
		List<Map<String,String>> list = obj.getRows();
		list.addAll((List) obj.getFooter());
		chineseFinancialReportService.creatWorkBook_YYBB(request, response, list);
	}
	
	/**********************************************门店账单分析**********************************************/
	
	/**
	 * 描述：查询门店账单分析报表_表格显示表头（动态）
	 * @author 马振
	 * 创建时间：2015-7-9 上午8:49:13
	 * @param session
	 * @param condition
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toDdzdfxHeader")
	@ResponseBody
	public Object toDdzdfxHeader(HttpSession session,PublicEntity condition,Page page) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("pays",commonMISBOHService.findPayment());
		map.put("typs",baseRecordService.findAllMarsaleclassOne(new Marsaleclass(),page));
		return map;
	}
	
	/**
	 * 描述：查询门店账单分析报表_表格显示
	 * @author 马振
	 * 创建时间：2015-7-9 上午8:49:00
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryDdzdfx")
	@ResponseBody
	public Object queryDdzdfx(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return chineseFinancialReportService.queryDdzdfx(condition);
	}
	
	/**
	 * 描述：查询门店账单分析报表_账单明细
	 * @author 马振
	 * 创建时间：2015-7-9 上午8:48:48
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findZhangDanMingXi")
	@ResponseBody
	public Object findZhangDanMingXi(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return chineseFinancialReportService.findZhangDanMingXi(condition);
	}
	
	/**
	 * 描述：查询门店账单分析报表_汇总报表
	 * @author 马振
	 * 创建时间：2015-7-9 上午8:48:38
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryDdzdfxByHuiZong")
	@ResponseBody
	public Object queryDdzdfxByHuiZong(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return chineseFinancialReportService.queryDdzdfxByHuiZong(condition);
	}
	
	/**********************************************门店反结算分析**********************************************/
	
	/**
	 * 描述：门店反结算分析
	 * @author 马振
	 * 创建时间：2015-7-13 下午7:55:02
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryDdfjsfx")
	@ResponseBody
	public Object queryDdfjsfx(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return chineseFinancialReportService.queryDdfjsfx(condition);
	}
	
	/**
	 * 描述：门店反结算分析明细
	 * @author 马振
	 * 创建时间：2015-7-13 下午7:55:14
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findDdfjsmx")
	@ResponseBody
	public Object findDdfjsmx(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return chineseFinancialReportService.findDdfjsmx(condition);
	}
	
	/**********************************************门店收银统计**********************************************/
	
	/**
	 * 描述：查询门店收银统计_收银方式统计
	 * @author 马振
	 * 创建时间：2015-7-13 下午7:55:58
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryDdsyfstj")
	@ResponseBody
	public Object queryDdsyfstj(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return chineseFinancialReportService.queryDdsyfstj(condition);
	}
	
	/**
	 * 描述：单店收银统计_收银方式统计图表2
	 * @author 马振
	 * 创建时间：2015-7-13 下午8:16:42
	 * @param response
	 * @param condition
	 */
	@RequestMapping("/buildChart")
	public void getXmlForGroupSales(HttpServletResponse response,PublicEntity condition){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chineseFinancialReportService.findXmlForCharts(response, condition).output();
	}
	
	/**
	 * 描述：单店收银统计_收银方式统计图表2
	 * @author 马振
	 * 创建时间：2015-7-13 下午8:16:25
	 * @param response
	 * @param condition
	 */
	@RequestMapping("/buildChart2")
	public void getXmlForGroupSales2(HttpServletResponse response,PublicEntity condition){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chineseFinancialReportService.findXmlForCharts2(response, condition).output();
	}

	/**
	 * 描述：查询门店收银统计_结算方式明细
	 * @author 马振
	 * 创建时间：2015-7-13 下午8:16:13
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/queryDdsyjsinfo")
	@ResponseBody
	public Object queryDdsyjsinfo(ModelMap modelMap,PublicEntity condition,HttpSession session) throws CRUDException{
		if(null == condition.getPk_store() || "".equals(condition.getPk_store()))
			condition.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return chineseFinancialReportService.queryDdsyjsinfo(condition);
	}
}