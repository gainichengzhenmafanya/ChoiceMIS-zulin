package com.choice.misboh.service.BaseRecord;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.domain.BaseRecord.ActTyp;
import com.choice.misboh.domain.BaseRecord.Actlck;
import com.choice.misboh.domain.BaseRecord.Actm;
import com.choice.misboh.domain.BaseRecord.ActmFen;
import com.choice.misboh.domain.BaseRecord.ActmVoucher;
import com.choice.misboh.domain.BaseRecord.Actstr;
import com.choice.misboh.domain.BaseRecord.Item;
import com.choice.misboh.domain.BaseRecord.ItemPkg;
import com.choice.misboh.domain.BaseRecord.Itmdtl;
import com.choice.misboh.domain.BaseRecord.Marsaleclass;
import com.choice.misboh.domain.BaseRecord.OtherStockDataF;
import com.choice.misboh.domain.BaseRecord.OtherStockDataO;
import com.choice.misboh.domain.BaseRecord.PackageDtl;
import com.choice.misboh.domain.BaseRecord.PackageType;
import com.choice.misboh.domain.BaseRecord.PubItem;
import com.choice.misboh.domain.BaseRecord.PubPackage;
import com.choice.misboh.domain.BaseRecord.StorePos;
import com.choice.misboh.domain.BaseRecord.Wptime;
import com.choice.misboh.domain.reportMis.PublicEntity;
import com.choice.misboh.domain.store.Store;
import com.choice.misboh.persistence.BaseRecord.BaseRecordMapper;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * 描述： 基础档案：菜品设置、套餐设置、POS定义、门店活动、水电气
 * @author 马振
 * 创建时间：2015-4-2 上午10:22:46
 */
@Service
public class BaseRecordService {
	
	private static Logger log = Logger.getLogger(BaseRecordService.class);
	
	@Autowired
	private PageManager<PubItem> pubitemPage;
	
	@Autowired
	private PageManager<Map<String,Object>> pubPackagePage;
	
	@Autowired
	private PageManager<Map<String,Object>> pageManagerpkg;
	
	@Autowired
	private PageManager<StorePos> storePosPage;
	
	@Autowired
	private BaseRecordMapper baseRecordMapper;
	
	@Autowired
	private CommonMISBOHService commonMISBOHService; 
	
	@Autowired
	private PageManager<Map<String, Object>> pageManager;

	/**
	 * 描述：获取 菜品类别  元数据
	 * @author 马振
	 * 创建时间：2015-4-20 上午11:18:28
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> marsaleclassTree(PubItem pubitem, HttpSession session) throws CRUDException {
		try{
			List<Map<String, Object>> list=new ArrayList<Map<String, Object>>();
			pubitem.setPk_store(this.getPkStore(session));
			list = baseRecordMapper.marsaleclassTree(pubitem);
			return list;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询菜品全部信息
	 * @author 马振
	 * 创建时间：2015-4-2 上午10:23:44
	 * @param pubitem
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<PubItem> listPubitem(PubItem pubitem, Page page, HttpSession session) throws CRUDException{
		try{
			pubitem.setPk_store(this.getPkStore(session));
			return pubitemPage.selectPage(pubitem, page, BaseRecordMapper.class.getName() + ".listPubitem");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：根据id查询数据
	 * @author 马振
	 * 创建时间：2015-4-20 上午11:37:43
	 * @param pk_Marsaleclass
	 * @return
	 * @throws CRUDException
	 */
	public Marsaleclass findMarsaleclassById(String pk_Marsaleclass) throws CRUDException{
		try{
			return baseRecordMapper.findMarsaleclassById(pk_Marsaleclass);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：获取大类
	 * @author 马振
	 * 创建时间：2015-4-24 下午3:05:56
	 * @param Marsaleclass
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> findAllMarsaleclassZero(Marsaleclass Marsaleclass,Page page)   throws CRUDException{
		try{
			return pageManager.selectPage(Marsaleclass, page,BaseRecordMapper.class.getName()+".findAllMarsaleclassZero");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：根据中类查找菜品(选择菜品的弹窗)
	 * @author 马振
	 * 创建时间：2015-4-24 下午3:14:29
	 * @param marsaleclassList1
	 * @param pk_pubitems
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> findpubitemByMclassForPub(List<Map<String, Object>> marsaleclassList1,String pk_pubitems) throws CRUDException {
		try{
			pk_pubitems = CodeHelper.replaceCode(pk_pubitems);
			List<Map<String, Object>> pubitemtoid=new ArrayList<Map<String, Object>>();
			for (int i = 0; i < marsaleclassList1.size(); i++) {
				Map<String,Object> mcmap = marsaleclassList1.get(i);
				
				Object id=mcmap.get("PK_MARSALECLASS");
				if(ValueUtil.IsNotEmpty(id)){
					Map<String,Object> pubitemmap=new HashMap<String, Object>();
					Map<String,Object> map = new HashMap<String,Object>();
					map.put("pk_id", id.toString());
					map.put("pk_pubitems", pk_pubitems);
					List<PubItem> list = baseRecordMapper.findpubitemByMclassForPub(map);
					pubitemmap.put("PK_ID", id.toString());
					pubitemmap.put("VALUELIST", list);
					pubitemtoid.add(pubitemmap);
				}
			}
			return pubitemtoid;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述： 菜品打印信息
	 * @author 马振
	 * 创建时间：2016年4月19日下午9:54:58
	 * @param pubitem
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> listPubitemPrint(PubItem pubitem, Page page, HttpSession session)   throws CRUDException{
		try{
			pubitem.setPk_store(this.getPkStore(session));
			return pageManager.selectPage(pubitem, page,BaseRecordMapper.class.getName() + ".listPubitemPrint");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	
	/**
	 * 描述： 查询套餐信息选择用
	 * @author 马振
	 * 创建时间：2015-4-24 下午3:31:47
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> findpubpackList()  throws CRUDException {
		try{
			List<Map<String, Object>> pubpackList=new ArrayList<Map<String, Object>>();
			Map<String,Object> pubpackMap = new HashMap<String,Object>();
			pubpackMap.put("PK_ID", "99");
			pubpackMap.put("VALUELIST", baseRecordMapper.findpubpackList());
			pubpackList.add(pubpackMap);
			return pubpackList;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：获取中类
	 * @author 马振
	 * 创建时间：2015-4-24 下午3:06:09
	 * @param Marsaleclass
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> findAllMarsaleclassOne(
			Marsaleclass Marsaleclass, Page page)   throws CRUDException{
		try{
			return pageManager.selectPage(Marsaleclass, page,BaseRecordMapper.class.getName()+".findAllMarsaleclassOne");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：获取小类
	 * @author 马振
	 * 创建时间：2015-4-24 下午3:06:16
	 * @param Marsaleclass
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> findAllMarsaleclassTwo(
			Marsaleclass Marsaleclass, Page page)   throws CRUDException{
		try{
			return pageManager.selectPage(Marsaleclass, page,BaseRecordMapper.class.getName()+".findAllMarsaleclassTwo");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询所有套餐类别
	 * @author 马振
	 * 创建时间：2015-4-20 下午6:31:27
	 * @return
	 * @throws CRUDException
	 */
	public List<PackageType> listPackageType() throws CRUDException{
		try{
			return baseRecordMapper.listPackageType();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询套餐设置全部信息
	 * @author 马振
	 * 创建时间：2015-4-2 下午1:03:24
	 * @param pubPackage
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String,Object>> listPubPackage(PubPackage pubPackage, Page page, HttpSession session) throws CRUDException{
		try{
			pubPackage.setPk_store(this.getPkStore(session));
			return pubPackagePage.selectPage(pubPackage, page, BaseRecordMapper.class.getName() + ".listPubPackage");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：根据套餐主键查询套餐明细
	 * @author 马振
	 * 创建时间：2015-4-20 下午2:45:37
	 * @param packageDtl
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String,Object>> listPackageDtl(PackageDtl packageDtl) throws CRUDException{
		try{
			return baseRecordMapper.listPackageDtl(packageDtl);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：可换菜明细查询
	 * @author 马振
	 * 创建时间：2015-4-20 下午5:27:25
	 * @param itemPkg
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String,Object>> listItemPkg(ItemPkg itemPkg, Page page) throws CRUDException{
		try{
			return pageManagerpkg.selectPage(itemPkg, page, BaseRecordMapper.class.getName()+".listItemPkg");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询门店POS信息
	 * @author 马振
	 * 创建时间：2015-4-2 下午2:29:02
	 * @param storePos
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<StorePos> listStorePos(StorePos storePos, Page page, HttpSession session) throws CRUDException{
		try{
			storePos.setPk_store(this.getPkStore(session));
			return storePosPage.selectPage(storePos, page, BaseRecordMapper.class.getName() + ".listStorePos");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：获取活动类别树
	 * @author 马振
	 * 创建时间：2015-4-20 下午7:00:34
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> actmTree() throws CRUDException {
		try{
			List<Map<String, Object>> list=new ArrayList<Map<String, Object>>();
			
			list = baseRecordMapper.actmTree();
			return list;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询门店活动
	 * @author 马振
	 * 创建时间：2015-4-2 下午5:41:06
	 * @param actm
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<Actm> listActm(Actm actm, Page page, HttpSession session) throws CRUDException{
		try{
			actm.setPk_store(this.getPkStore(session));
			return baseRecordMapper.listActm(actm);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查找所有活动分类
	 * @author 马振
	 * 创建时间：2015-7-14 上午10:46:15
	 * @param actTyp
	 * @return
	 * @throws CRUDException
	 */
	public List<ActTyp> findAllActTyp() throws CRUDException{
		try{
			return baseRecordMapper.findAllActTyp();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询活动管理小类
	 * @author 马振
	 * 创建时间：2015-7-14 上午10:55:34
	 * @return
	 * @throws CRUDException
	 */
	public List<PublicEntity> findAllActTypMin() throws CRUDException{
		try{
			return baseRecordMapper.findAllActTypMin();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：获取门店活动日期限制
	 * @author 马振
	 * 创建时间：2015-4-21 上午9:36:52
	 * @param actm
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public Actstr getActmStr(Actm actm, HttpSession session) throws CRUDException{
		try{
			actm.setPk_store(this.getPkStore(session));
			return baseRecordMapper.getActmStr(actm);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：星期及时段
	 * @author 马振
	 * 创建时间：2015-4-21 上午10:40:35
	 * @param actm
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<Wptime> getWptime(Actm actm, HttpSession session) throws CRUDException{
		try{
			actm.setPk_store(this.getPkStore(session));
			return baseRecordMapper.getWptime(actm);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：互斥活动条件
	 * @author 马振
	 * 创建时间：2015-4-21 下午12:32:49
	 * @param actm
	 * @return
	 * @throws CRUDException
	 */
	public List<Actlck> findActlckById(Actm actm, HttpSession session) throws CRUDException{
		try{
			actm.setPk_store(this.getPkStore(session));
			return baseRecordMapper.findActlckById(actm);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：账单减免--金额减免
	 * @author 马振
	 * 创建时间：2015-4-21 下午3:32:58
	 * @param actm
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<Item> getJianMian(Actm actm, HttpSession session) throws CRUDException{
		try{
			actm.setPk_store(this.getPkStore(session));
			return baseRecordMapper.getJianMian(actm);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：账单减免--金额减免
	 * @author 马振
	 * 创建时间：2015-4-21 下午3:32:58
	 * @param actm
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<Item> getDiaoJia(Actm actm, HttpSession session) throws CRUDException{
		try{
			actm.setPk_store(this.getPkStore(session));
			return baseRecordMapper.getDiaoJia(actm);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：消费返券
	 * @author 马振
	 * 创建时间：2015-4-21 下午4:32:20
	 * @param actm
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<ActmVoucher> getFanQuan(Actm actm, HttpSession session) throws CRUDException{
		try{
			actm.setPk_store(this.getPkStore(session));
			return baseRecordMapper.getFanQuan(actm);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：消费返积分
	 * @author 马振
	 * 创建时间：2015-4-21 下午4:53:13
	 * @param actm
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<ActmFen> getActmFen(Actm actm, HttpSession session) throws CRUDException{
		try{
			actm.setPk_store(this.getPkStore(session));
			return baseRecordMapper.getActmFen(actm);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：人数限制
	 * @author 马振
	 * 创建时间：2015-4-21 下午5:16:44
	 * @param actm
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public Actm getMemberLimit(Actm actm, HttpSession session) throws CRUDException{
		try{
			actm.setPk_store(this.getPkStore(session));
			return baseRecordMapper.getMemberLimit(actm);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：张数限制
	 * @author 马振
	 * 创建时间：2015-4-21 下午5:17:02
	 * @param actm
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public Actm getTicketLimit(Actm actm, HttpSession session) throws CRUDException{
		try{
			actm.setPk_store(this.getPkStore(session));
			return baseRecordMapper.getTicketLimit(actm);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：金额限制
	 * @author 马振
	 * 创建时间：2015-4-21 下午6:03:19
	 * @param actm
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public Actm getAmountLimit(Actm actm, HttpSession session) throws CRUDException{
		try{
			actm.setPk_store(this.getPkStore(session));
			return baseRecordMapper.getAmountLimit(actm);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：班次限制
	 * @author 马振
	 * 创建时间：2015-4-21 下午6:03:26
	 * @param actm
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public Actm getClassesLimit(Actm actm, HttpSession session) throws CRUDException{
		try{
			actm.setPk_store(this.getPkStore(session));
			return baseRecordMapper.getClassesLimit(actm);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：菜品限制
	 * @author 马振
	 * 创建时间：2015-4-23 下午5:53:58
	 * @param actm
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<Itmdtl> getPubitemLimit(Actm actm, HttpSession session) throws CRUDException{
		try{
			actm.setPk_store(this.getPkStore(session));
			return baseRecordMapper.getPubitemLimit(actm);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：新增门店信息
	 * @author 马振
	 * 创建时间：2015-4-7 上午10:58:09
	 * @param storePos
	 * @param session
	 * @throws CRUDException
	 */
	public void addStorePos(StorePos storePos, HttpSession session) throws CRUDException{
		try{
			storePos.setPk_pos(CodeHelper.createUUID().substring(0, 20).toUpperCase());
			baseRecordMapper.addStorePos(storePos);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：修改门店pos
	 * @author 马振
	 * 创建时间：2015-4-7 下午1:38:48
	 * @param storePos
	 * @throws CRUDException
	 */
	public void updateStorePos(StorePos storePos) throws CRUDException{
		try{
			baseRecordMapper.updateStorePos(storePos);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：获取水电气最大排序号
	 * @author 马振
	 * 创建时间：2015-4-9 下午1:32:42
	 * @return
	 * @throws CRUDException
	 */
	public int getMaxSort() throws CRUDException{
		try{
			return baseRecordMapper.getMaxSort();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：获取水电气子表信息，并在星期日后加周计的公共方法
	 * @author 马振
	 * 创建时间：2015-4-10 下午1:00:50
	 * @param otherStockDataO
	 * @param listOther
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<OtherStockDataO> listAll(List<OtherStockDataO> listOther) throws CRUDException{
		try{
			double incount = 0.00;	//定义进货数量初始值
			double count = 0.00;	//定义变量，放入list周计中一星期前的总进货数量的值
			double outcount = 0.00;	//定义用量初始值
			double out = 0.00;		//定义变量，放入list周计中一星期前的总用量的值
			double amount = 0.00;	//定义金额初始值
			double mount = 0.00;	//定义变量，放入list周计中一星期前的总金额的值
			double monthCnt = 0d;
			double monthAmt = 0d;
			double monthInt = 0d;
			String month = "";
			//获取查询到子表信息
			List<OtherStockDataO> listData = listOther;
			
			//创建list对象
			List<OtherStockDataO> list = new ArrayList<OtherStockDataO>();
			
			//循环每一天，如果得到的是星期日，则在后面加一条"周计"
			for (int i = 0; i < listData.size(); i++) {
				OtherStockDataO oo = listData.get(i);
				month = oo.getWorkdate();
				//根据日期，向明细中的星期赋值
				oo.setWeek(DateJudge.getWeekOfDateToString(oo.getWorkdate()));
				//循环出的进货数量累加
				incount = incount + (oo.getIncount() == null ? 0.00 : oo.getIncount());
				//循环出的用量累加
				outcount = outcount + (oo.getOutcount() == null ? 0.00 : oo.getOutcount());
				//循环出计算后的总额
				amount = amount + (null == oo.getAmount() ? 0 : oo.getAmount());
				//为金额赋值
				oo.setAmount(oo.getAmount());
				monthCnt = monthCnt + (oo.getOutcount() == null ? 0.00 : oo.getOutcount());
				monthInt = monthInt + (oo.getIncount() == null ? 0.00 : oo.getIncount());
				monthAmt = monthAmt + (oo.getAmount() == null ? 0.00 : oo.getAmount());
				
				//将明细中的每一条数据放入list
				list.add(oo);

				OtherStockDataO data = new OtherStockDataO();
				
				//如果星期为“日”，则在后面添加一条“周计：”
				if ("日".equals(DateJudge.getWeekOfDateToString(oo.getWorkdate()))) {
					data.setWorkdate("周计：");
					data.setWeek("");
					data.setAmount(amount- mount);
					data.setIncount(incount - count);
					data.setOutcount(outcount - out);
					count = incount;
					out = outcount;
					mount = amount;
					list.add(data);
				}
				
				//如果金额为空，则赋值00.00
				if (null == list.get(i).getAmount()) {
					list.get(i).setAmount(00.00);
				}
			}
			if(!DateJudge.getWeekOfDateToString(month).equals("日")){
				OtherStockDataO data = new OtherStockDataO();
				data.setWorkdate("周计：");
				data.setWeek("");
				data.setAmount(amount- mount);
				data.setIncount(incount - count);
				data.setOutcount(outcount - out);
				count = incount;
				out = outcount;
				mount = amount;
				list.add(data);
			}
			//最后加月合计
			OtherStockDataO other = new OtherStockDataO();
			other.setWorkdate("月合计：");
			other.setWeek("");
			other.setAmount(monthAmt);
			other.setIncount(monthInt);
			other.setOutcount(monthCnt);
			list.add(other);
			
			return list;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：显示水(查询条件有日期)
	 * @author 马振
	 * 创建时间：2015-4-8 上午9:24:06
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<OtherStockDataO> listWater(OtherStockDataO otherStockDataO) throws CRUDException{
		try{
			if (null == otherStockDataO.getWorkdate()) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
				otherStockDataO.setWorkdate(sdf.format(new Date()));
			}
			
			//获取查询到的水表信息明细
			List<OtherStockDataO> listWater = baseRecordMapper.listWater(otherStockDataO);
			
			//创建list对象
			List<OtherStockDataO> list = this.listAll(listWater);
			
			return list;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：显示水(查询条件有日期)
	 * @author 马振
	 * 创建时间：2015-4-8 上午9:24:06
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<OtherStockDataO> listWaterWap(OtherStockDataO otherStockDataO, String firm) throws CRUDException{
		try{
			//将编码放入实体类
			Store store = new Store();
			store.setVcode(firm);
			otherStockDataO.setPk_store(commonMISBOHService.getStore(store).getPk_store());
			if (null == otherStockDataO.getWorkdate()) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
				otherStockDataO.setWorkdate(sdf.format(new Date()));
			}
			
			//获取查询到的水表信息明细
			List<OtherStockDataO> listWater = baseRecordMapper.listWater(otherStockDataO);
			
			//创建list对象
			List<OtherStockDataO> list = this.listAll(listWater);
			
			return list;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：根据主键查询水电气的总用量
	 * @author 马振
	 * 创建时间：2015-4-9 下午12:43:44
	 * @param pk_otherstockdataf
	 * @return
	 * @throws CRUDException
	 */
	public Double allCount(OtherStockDataF otherStockDataF) throws CRUDException{
		try{
			double allCount = 0.00;
			if (null == baseRecordMapper.allCount(otherStockDataF)) {
				allCount = 0.00;
			} else {
				allCount = baseRecordMapper.allCount(otherStockDataF);
			}
			return allCount;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：水主表
	 * @author 马振
	 * 创建时间：2015-4-8 下午2:45:54
	 * @param otherStockDataO
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<OtherStockDataF> waterCount(String pk_store) throws CRUDException{
		try{
			OtherStockDataF water = new OtherStockDataF();
			water.setPk_store(pk_store);
			List<OtherStockDataF> listWater = baseRecordMapper.waterCount(water);
			
			//将根据主键查询的用量放到主键对应的list中
			for (int i = 0; i < listWater.size(); i++) {
				water.setPk_otherstockdataf(listWater.get(i).getPk_otherstockdataf());
				listWater.get(i).setZcount(this.allCount(water));
			}
			return listWater;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：水主表
	 * @author css
	 * @throws CRUDException
	 */
	public List<OtherStockDataF> waterCountWap(String firm) throws CRUDException{
		try{
			OtherStockDataF water = new OtherStockDataF();
			//将编码放入实体类
			Store store = new Store();
			store.setVcode(firm);
			water.setPk_store(commonMISBOHService.getStore(store).getPk_store());
			List<OtherStockDataF> listWater = baseRecordMapper.waterCount(water);
			
			//将根据主键查询的用量放到主键对应的list中
			for (int i = 0; i < listWater.size(); i++) {
				water.setPk_otherstockdataf(listWater.get(i).getPk_otherstockdataf());
				listWater.get(i).setZcount(this.allCount(water));
			}
			return listWater;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：显示电
	 * @author 马振
	 * 创建时间：2015-4-8 上午9:24:40
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<OtherStockDataO> listElectric(OtherStockDataO otherStockDataO) throws CRUDException{
		try{
			if (null == otherStockDataO.getWorkdate()) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
				otherStockDataO.setWorkdate(sdf.format(new Date()));
			}
			
			//获取查询到的电表信息明细
			List<OtherStockDataO> listElectric = baseRecordMapper.listElectric(otherStockDataO);
			
			//创建list对象
			List<OtherStockDataO> list = this.listAll(listElectric);
			
			return list;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 描述：显示电
	 * @author css
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<OtherStockDataO> listElectricWap(OtherStockDataO otherStockDataO, String firm) throws CRUDException{
		try{
			//将编码放入实体类
			Store store = new Store();
			store.setVcode(firm);
			otherStockDataO.setPk_store(commonMISBOHService.getStore(store).getPk_store());
			if (null == otherStockDataO.getWorkdate()) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
				otherStockDataO.setWorkdate(sdf.format(new Date()));
			}
			
			//获取查询到的电表信息明细
			List<OtherStockDataO> listElectric = baseRecordMapper.listElectric(otherStockDataO);
			
			//创建list对象
			List<OtherStockDataO> list = this.listAll(listElectric);
			
			return list;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 描述：电主表
	 * @author 马振
	 * 创建时间：2015-4-8 下午2:45:54
	 * @param otherStockDataO
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<OtherStockDataF> electricCount(String pk_store) throws CRUDException{
		try{
			OtherStockDataF electric = new OtherStockDataF();
			electric.setPk_store(pk_store);
			List<OtherStockDataF> listElectric = baseRecordMapper.electricCount(electric);
			
			//将根据主键查询的用量放到主键对应的list中
			for (int i = 0; i < listElectric.size(); i++) {
				electric.setPk_otherstockdataf(listElectric.get(i).getPk_otherstockdataf());
				listElectric.get(i).setZcount(this.allCount(electric));
			}
			return listElectric;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 描述：电主表
	 * @author css
	 * @throws CRUDException
	 */
	public List<OtherStockDataF> electricCountWap(String firm) throws CRUDException{
		try{
			OtherStockDataF electric = new OtherStockDataF();
			//将编码放入实体类
			Store store = new Store();
			store.setVcode(firm);
			electric.setPk_store(commonMISBOHService.getStore(store).getPk_store());
			List<OtherStockDataF> listElectric = baseRecordMapper.electricCount(electric);
			
			//将根据主键查询的用量放到主键对应的list中
			for (int i = 0; i < listElectric.size(); i++) {
				electric.setPk_otherstockdataf(listElectric.get(i).getPk_otherstockdataf());
				listElectric.get(i).setZcount(this.allCount(electric));
			}
			return listElectric;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：显示天然气或煤气
	 * @author 马振
	 * 创建时间：2015-4-8 上午9:25:35
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<OtherStockDataO> listGas(OtherStockDataO otherStockDataO) throws CRUDException{
		try{
			if (null == otherStockDataO.getWorkdate()) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
				otherStockDataO.setWorkdate(sdf.format(new Date()));
			}
			
			//获取查询到的气表信息明细
			List<OtherStockDataO> listGas = baseRecordMapper.listGas(otherStockDataO);
			
			//创建list对象
			List<OtherStockDataO> list = this.listAll(listGas);
			
			return list;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：显示天然气或煤气
	 * @author 马振
	 * 创建时间：2015-4-8 上午9:25:35
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<OtherStockDataO> listGasWap(OtherStockDataO otherStockDataO, String firm) throws CRUDException{
		try{
			//将编码放入实体类
			Store store = new Store();
			store.setVcode(firm);
			otherStockDataO.setPk_store(commonMISBOHService.getStore(store).getPk_store());
			if (null == otherStockDataO.getWorkdate()) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
				otherStockDataO.setWorkdate(sdf.format(new Date()));
			}
			
			//获取查询到的气表信息明细
			List<OtherStockDataO> listGas = baseRecordMapper.listGas(otherStockDataO);
			
			//创建list对象
			List<OtherStockDataO> list = this.listAll(listGas);
			
			return list;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：气主表
	 * @author 马振
	 * 创建时间：2015-4-8 下午2:45:54
	 * @param otherStockDataO
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<OtherStockDataF> gasCount(String pk_store) throws CRUDException{
		try{
			OtherStockDataF gas = new OtherStockDataF();
			gas.setPk_store(pk_store);
			List<OtherStockDataF> listGas = baseRecordMapper.gasCount(gas);
			
			//将根据主键查询的用量放到主键对应的list中
			for (int i = 0; i < listGas.size(); i++) {
				gas.setPk_otherstockdataf(listGas.get(i).getPk_otherstockdataf());
				listGas.get(i).setZcount(this.allCount(gas));
			}
			return listGas;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：气主表
	 * @author css
	 * @throws CRUDException
	 */
	public List<OtherStockDataF> gasCountWap(String firm) throws CRUDException{
		try{
			OtherStockDataF gas = new OtherStockDataF();
			//将编码放入实体类
			Store store = new Store();
			store.setVcode(firm);
			gas.setPk_store(commonMISBOHService.getStore(store).getPk_store());
			List<OtherStockDataF> listGas = baseRecordMapper.gasCount(gas);
			
			//将根据主键查询的用量放到主键对应的list中
			for (int i = 0; i < listGas.size(); i++) {
				gas.setPk_otherstockdataf(listGas.get(i).getPk_otherstockdataf());
				listGas.get(i).setZcount(this.allCount(gas));
			}
			return listGas;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：新增水电气
	 * @author 马振
	 * 创建时间：2015-4-8 下午7:18:53
	 * @param otherStockDataF
	 * @param session
	 * @throws CRUDException
	 */
	public void addWaterElectricGas(OtherStockDataF otherStockDataF) throws CRUDException{
		try{
			baseRecordMapper.addWaterElectricGas(otherStockDataF);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：新增水电气
	 * @author css
	 * @param otherStockDataF
	 * @throws CRUDException
	 */
	public void addWaterElectricGasWAP(OtherStockDataF otherStockDataF, String firm, String accountName) throws CRUDException{
		try{
			otherStockDataF.setPk_otherstockdataf(CodeHelper.createUUID().substring(0, 20).toUpperCase());
			//将编码放入实体类
			Store store = new Store();
			store.setVcode(firm);
			otherStockDataF.setPk_store(commonMISBOHService.getStore(store).getPk_store());
			otherStockDataF.setCtime(DateJudge.getNowTime());
			otherStockDataF.setEcode(accountName);
			otherStockDataF.setScode(firm);
			baseRecordMapper.addWaterElectricGas(otherStockDataF);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：保存修改水电气主表
	 * @author 马振
	 * 创建时间：2015-5-25 下午3:41:05
	 * @param otherStockDataF
	 * @param session
	 * @throws CRUDException
	 */
	public void saveOtherStockDataF(OtherStockDataF otherStockDataF) throws CRUDException{
		try{
			baseRecordMapper.saveOtherStockDataF(otherStockDataF);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：根据主表主键删除水电气主子表信息
	 * @author 马振
	 * 创建时间：2015-4-9 下午2:20:41
	 * @param otherStockDataF
	 * @param id
	 * @param session
	 * @throws CRUDException
	 */
	public void deleteAll(String id, String pk_store) throws CRUDException{
		try{
			OtherStockDataF otherStockDataF = new OtherStockDataF();
			otherStockDataF.setPk_store(pk_store);
			String[] pk_otherstockdataf = id.split(",");
			for (int i = 0; i < pk_otherstockdataf.length; i++) {
				otherStockDataF.setPk_otherstockdataf(pk_otherstockdataf[i]);
				baseRecordMapper.deleteDataO(otherStockDataF);
				baseRecordMapper.deleteDataF(otherStockDataF);
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 描述：根据主表主键删除水电气主子表信息
	 * @author 马振
	 * 创建时间：2015-4-9 下午2:20:41
	 * @param otherStockDataF
	 * @param id
	 * @param session
	 * @throws CRUDException
	 */
	public void deleteAllWAP(String id, String firm) throws CRUDException{
		try{
			OtherStockDataF otherStockDataF = new OtherStockDataF();
			//将编码放入实体类
			Store store = new Store();
			store.setVcode(firm);
			otherStockDataF.setPk_store(commonMISBOHService.getStore(store).getPk_store());
			String[] pk_otherstockdataf = id.split(",");
			for (int i = 0; i < pk_otherstockdataf.length; i++) {
				otherStockDataF.setPk_otherstockdataf(pk_otherstockdataf[i]);
				baseRecordMapper.deleteDataO(otherStockDataF);
				baseRecordMapper.deleteDataF(otherStockDataF);
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：根据主键获取主表信息
	 * @author 马振
	 * 创建时间：2015-1-1 下午5:41:30
	 * @param pk_otherstockdataf
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public OtherStockDataF getOtherStockDataF(String pk_otherstockdataf) throws CRUDException{
		try{
			return baseRecordMapper.getOtherStockDataF(pk_otherstockdataf);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
		
	/**
	 * 描述：循环出水电气子表
	 * @author 马振
	 * 创建时间：2015-9-18 下午2:35:56
	 * @param workdate
	 * @param pk_otherstockdataf
	 * @param type
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<OtherStockDataO> allDataO(String workdate, String pk_otherstockdataf, String type) throws CRUDException{
		try{
			List<OtherStockDataO> listStockO = new ArrayList<OtherStockDataO>();
			
			//根据主表主键及开始、结束日期查询子表信息
			List<OtherStockDataO> listDataByIDF = baseRecordMapper.selectAllByIdF(pk_otherstockdataf,DateJudge.getMonthStart(workdate),DateJudge.getMonthEnd(workdate));
			
			//根据主表主键，获取上个月最后一天的子表信息
			OtherStockDataF of = baseRecordMapper.getOtherStockDataFByWorkdate(pk_otherstockdataf,DateJudge.getLenTime(-1, DateJudge.getMonthStart(workdate)));
			
			//如果根据主表主键查询的子表信息为空，则说明子表中没有相关的数据，新生产一个月内的数据；如果不为空，说明子表中已存在相应的数据，只需显示出
			if (0 == listDataByIDF.size()) {// 无明细
				
				//获取所选月份的所有日期
				List<String> listDate = DateJudge.getDateList(DateJudge.getMonthStart(workdate), DateJudge.getMonthEnd(workdate));
				
				//将得到的日期放入水电气子表中
				for (int i = 0; i < listDate.size(); i++) {
					OtherStockDataO otherStockDataO = new OtherStockDataO();
					
					//主表数据下无明细时：若是当前时间之前的日期，则为其期末、期初赋值主表定义的期初值;若是为当前日期，则期初赋值主表定义的期初值，期末为0;若是当前时间之后的日期，则期初、期末皆为0
					if (!DateJudge.timeCompare(DateJudge.getNowDate(), listDate.get(i))){
						otherStockDataO.setBegincount(FormatDouble.getDoubleLength(of.getBegincount()));
						otherStockDataO.setEndcount(FormatDouble.getDoubleLength(of.getBegincount()));
					} else if (DateJudge.getNowDate().equals(listDate.get(i))) {
						otherStockDataO.setBegincount(FormatDouble.getDoubleLength(of.getBegincount()));
						otherStockDataO.setEndcount(0.00);
					} else {
						otherStockDataO.setBegincount(0.00);
						otherStockDataO.setEndcount(0.00);
					}
					
					OtherStockDataO data = new OtherStockDataO();
					
					//如果得到的是星期日，则在后面加一条"周计"
					if ("一".equals(DateJudge.getWeekOfDateToString(listDate.get(i))) && i != 0) {
						data.setWorkdate("周计：");
						data.setWeek("");
						listStockO.add(data);
					}
					
					otherStockDataO.setWeek(DateJudge.getWeekOfDateToString(listDate.get(i)));
					otherStockDataO.setWorkdate(listDate.get(i));
					otherStockDataO.setFlag(DateJudge.timeCompare(DateJudge.getNowDate(), listDate.get(i)));
					otherStockDataO.setPk_otherstockdataf(pk_otherstockdataf);
					otherStockDataO.setIncount(0.00);
					otherStockDataO.setOutcount(0.00);
					otherStockDataO.setPrice(of.getPrice());
					otherStockDataO.setAmount((otherStockDataO.getOutcount() == null ? 0 : otherStockDataO.getOutcount()) * (otherStockDataO.getPrice() == null ? 0 : otherStockDataO.getPrice()));
					
					listStockO.add(otherStockDataO);
					
				}
				OtherStockDataO other = new OtherStockDataO();
				if(!DateJudge.getWeekOfDateToString(workdate).equals("日")){
					other.setWorkdate("周计：");
					other.setWeek("");
					listStockO.add(other);
				}
				//最后加月合计
				other = new OtherStockDataO();
				other.setWorkdate("月合计：");
				other.setWeek("");
				listStockO.add(other);
			} else {
				List<OtherStockDataO> listOther = listDataByIDF;
				
				//为获取的子表信息中的每一条数据中实体类里的flag赋值,并判断amount是否为空，若为空则赋值0.00
				for (int i = 0; i < listOther.size(); i++) {
					listOther.get(i).setFlag(DateJudge.timeCompare(DateJudge.getNowDate(), listOther.get(i).getWorkdate()));
					
					//为期初赋前一天的期末值
					if (i == 0) {
						listOther.get(i).setBegincount(of.getBegincount());
					} else if (i > 0) {
						listOther.get(i).setBegincount(listOther.get(i - 1).getEndcount());
					}
					
					//若期末为0，则赋值为同一天的期初
					if (!DateJudge.timeCompare(DateJudge.getNowDate(), listOther.get(i).getWorkdate()) && 0 == listOther.get(i).getEndcount()) {
						listOther.get(i).setEndcount(listOther.get(i).getBegincount());
					}
					
					//计算用量=期初+购入-期末
//					double begincount = listOther.get(i).getBegincount();
//					double incount = listOther.get(i).getIncount();
//					double endcount = listOther.get(i).getEndcount();
//					listOther.get(i).setOutcount(begincount + incount - endcount);
//					listOther.get(i).setAmount(listOther.get(i).getPrice() * (begincount + incount - endcount));
				}
				listStockO = this.listAll(listOther);
			}
			return listStockO;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：新增水电气子表
	 * @author 马振
	 * 创建时间：2015-4-10 下午6:56:40
	 * @param list
	 * @throws CRUDException
	 */
	public void insertOtherStockDataO(List<OtherStockDataO> list) throws CRUDException {
		try {
			Double meterrate = 1.00;
			
			//创建主表对象，并将主键和门店主键放入其中，然后根据主表主键删除当前月份子表信息
			OtherStockDataF otherStockDataF = new OtherStockDataF();
			otherStockDataF.setPk_otherstockdataf(list.get(0).getPk_otherstockdataf());
			otherStockDataF.setPk_store(list.get(0).getPk_store());
			otherStockDataF.setWorkdate(DateJudge.getNowDate().substring(0, 7));
			baseRecordMapper.deleteDataO(otherStockDataF);
			
			//如果是电表(即：type=2)则进行倍率计算，获取主表中的倍率
			if (2 == list.get(0).getType()) {
				meterrate = baseRecordMapper.getOtherStockDataF(list.get(0).getPk_otherstockdataf()).getMeterrate();
			}
			
			//为每一条数据的主键、时间赋值
			for (int i = 0; i < list.size(); i++) {
				list.get(i).setPk_OtherStockDataO(CodeHelper.createUUID().substring(0,20).toUpperCase());
				list.get(i).setCtime(DateJudge.getNowTime());
				
				//判断week的值，如果不是"week"才新增
				if (!"week".equals(list.get(i).getWeek())) {

					//如果是电表(即：type=2)则进行倍率计算
					if (2 == list.get(i).getType()) {
						list.get(i).setOutcount(list.get(i).getOutcount() * meterrate);
					}
					baseRecordMapper.insertOtherStockDataO(list.get(i));
				}
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：返回当前门店主键
	 * @author 马振
	 * 创建时间：2015-4-7 下午3:58:41
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public String getPkStore(HttpSession session) throws CRUDException{
		try{
			//返回根据当前门店编码获取的当前门店主键
			return commonMISBOHService.getPkStore(session).getPk_store() == null ? "" : commonMISBOHService.getPkStore(session).getPk_store();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：返回当前门店主键
	 * @author 马振
	 * 创建时间：2015-4-7 下午3:58:41
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public OtherStockDataF findById(OtherStockDataF otherStockDataf) throws CRUDException{
		try{
			//返回根据当前门店编码获取的当前门店主键
			return baseRecordMapper.getOtherStockDataFById(otherStockDataf);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述:根据门店及日期查询这段时间产生营业数据的活动
	 * 作者:马振
	 * 时间:2016年7月29日下午3:17:30
	 * @param actm
	 * @return
	 * @throws CRUDException
	 */
	public List<Actm> findAllActmByZheK(Actm actm) throws CRUDException{
		try{
			return baseRecordMapper.findAllActmByZheK(actm);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述:根据活动查询所有大类
	 * 作者:马振
	 * 时间:2016年7月29日下午3:26:25
	 * @param actm
	 * @return
	 * @throws CRUDException
	 */
	public List<ActTyp> findAllActTypByActm(Actm actm) throws CRUDException{
		try{
			return baseRecordMapper.findAllActTypByActm(actm);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 描述:根据活动查询所有小类
	 * 作者:马振
	 * 时间:2016年7月29日下午3:33:12
	 * @param actm
	 * @return
	 * @throws CRUDException
	 */
	public List<PublicEntity> findAllActTypMinByActm(Actm actm) throws CRUDException{
		try{
			return baseRecordMapper.findAllActTypMinByActm(actm);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 描述:查询支付方式
	 * 作者:马振
	 * 时间:2016年7月29日下午3:36:45
	 * @param actm
	 * @return
	 * @throws CRUDException
	 */
	public List<PublicEntity> findPaymodeByActm(Actm actm) throws CRUDException{
		try{
			return baseRecordMapper.findPaymodeByActm(actm);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}