package com.choice.misboh.service.BaseRecord;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.ForResourceFiles;
import com.choice.misboh.domain.BaseRecord.MisBohOperator;
import com.choice.misboh.domain.OperationManagement.StoreRange;
import com.choice.misboh.domain.store.Store;
import com.choice.misboh.domain.store.StoreDept;
import com.choice.misboh.persistence.BaseRecord.OperatorMaintenanceMapper;
import com.choice.misboh.service.common.CommonMISBOHService;

/**
 * 描述：操作员维护
 * @author 马振
 * 创建时间：2015-8-24 下午1:16:58
 */
@Service
public class OperatorMaintenanceService {

	private static Logger log = Logger.getLogger(OperatorMaintenanceService.class);
	
	@Autowired
	private CommonMISBOHService commonMISBOHService; 
	
	@Autowired
	private OperatorMaintenanceMapper operatorMaintenanceMapper;
	
	/**
	 * 描述：显示全部门店操作员
	 * @author 马振
	 * 创建时间：2015-8-24 下午1:39:29
	 * @param operator
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<MisBohOperator> listOperatorMaintenance(MisBohOperator operator, HttpSession session) throws CRUDException {
		try{
			operator.setPk_store(commonMISBOHService.getPkStore(session).getPk_store());
			return operatorMaintenanceMapper.listOperatorMaintenance(operator);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：根据门店操作员主键查询门店操作员信息
	 * @author 马振
	 * 创建时间：2015-8-24 下午2:16:09
	 * @param operator
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public MisBohOperator getOperatorByPk(MisBohOperator operator) throws CRUDException {
		try{
			return operatorMaintenanceMapper.getOperatorByPk(operator);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询当前门店编码所有部门
	 * @author 马振
	 * 创建时间：2015-8-24 下午6:09:23
	 * @return
	 * @throws CRUDException
	 */
	public List<StoreDept> listStoreDept(HttpSession session) throws CRUDException {
		try{
			Store store = new Store();
			store.setVcode(commonMISBOHService.getPkStore(session).getVcode());
			return commonMISBOHService.getAllStoreDept(store);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：新增、修改、删除、启用、停用门店操作员
	 * @author 马振
	 * 创建时间：2015-8-24 下午4:58:18
	 * @param operator
	 * @return
	 * @throws CRUDException
	 */
	public void saveOperatorMaintenance(MisBohOperator operator) throws CRUDException {
		try{
			if ("add".equals(operator.getIflag())) {			//新增
				operator.setPk_operator(CodeHelper.createUUID().substring(0, 20));
				operatorMaintenanceMapper.insertOperator(operator);
			} else if ("update".equals(operator.getIflag())) {	//修改
				operatorMaintenanceMapper.updateOperator(operator);
			} else if ("delete".equals(operator.getIflag())) {	//删除
				String[] pk_operators = operator.getPk_operator().split(",");
				for (int i = 0; i < pk_operators.length; i++) {
					operator.setPk_operator(pk_operators[i]);
					operatorMaintenanceMapper.deleteOperator(operator);
				}
			} else if ("enable".equals(operator.getIflag())) {	//启用
				String[] pk_operators = operator.getPk_operator().split(",");
				for (int i = 0; i < pk_operators.length; i++) {
					operator.setPk_operator(pk_operators[i]);
					operator.setEnablestate(2);
					operatorMaintenanceMapper.ableOperator(operator);
				}
			} else if ("disable".equals(operator.getIflag())) {	//停用
				String[] pk_operators = operator.getPk_operator().split(",");
				for (int i = 0; i < pk_operators.length; i++) {
					operator.setPk_operator(pk_operators[i]);
					operator.setEnablestate(3);
					operatorMaintenanceMapper.ableOperator(operator);
				}
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：根据门店主键获取该门店地图信息
	 * @author 马振
	 * 创建时间：2016-3-14 下午2:00:03
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<StoreRange> listMapInfo(HttpSession session) throws CRUDException {
		List<StoreRange> listMapInfo = new ArrayList<StoreRange>();
		StoreRange storeRange = new StoreRange();
		String pk_store = commonMISBOHService.getPkStore(session).getPk_store();
		storeRange.setPk_store(pk_store);	//为门店主键赋值
		
		List<StoreRange> listStoreRange = operatorMaintenanceMapper.listStoreRange(storeRange);	//获取该门店配送范围主表信息
		
		//判断主表数据是否大于0，若大于0则进行循环，取子表数据
		if (0 < listStoreRange.size()) {
			for (int i = 0; i < listStoreRange.size(); i++) {
				storeRange = listStoreRange.get(i);
				List<StoreRange> listRangeCoordi = operatorMaintenanceMapper.listRangeCoordi(storeRange);//获取该门店配送范围子表信息
				
				//判断子表数据是否大于0，若大于0则将其放入实体类中
				if (0 < listRangeCoordi.size()) {
					storeRange.setListRangeCoordi(listRangeCoordi);
				}
				
				listMapInfo.add(storeRange);	//将封装完的storeRange放入listMapInfo中
			}
		}
		
		return listMapInfo;
	}
	
	/**
	 * 描述：保存地图上的区域坐标、区域颜色及起送价配送费
	 * content组成为(以两个区域为例，第一个区域均为四边形，第二个为五边形)
	 * 颜色:纬度值-经度值;纬度值-经度值;纬度值-经度值;纬度值-经度值;区域编号;起送价;配送费,颜色:纬度值-经度值;纬度值-经度值;纬度值-经度值;纬度值-经度值;纬度值-经度值;区域编号;起送价;配送费
	 * @author 马振
	 * 创建时间：2016-3-12 下午2:28:18
	 * @param content
	 * @throws CRUDException
	 */
	public String saveContent(String content, String pk_store, String areacode) throws CRUDException {
		String flag = "N";
		int count = 0;	//子表数据条数
		List<StoreRange> listStoreRange = new ArrayList<StoreRange>();	//主表所用
		List<StoreRange> listRangeCoordi = new ArrayList<StoreRange>();	//子表所用
		StoreRange storeRange = new StoreRange();
		String pk_group = ForResourceFiles.getValByKey("config-bsboh.properties", "pk_group");	//获取配置文件中的企业号
		
		String[] contents = content.split(",");					//将用逗号隔开的各区域内容组成数组
		storeRange.setPk_store(pk_store);
		storeRange.setIareaseq(areacode);
		
		List<StoreRange> list = operatorMaintenanceMapper.listStoreRange(storeRange);	//获取该门店配送范围主表信息
		
		//若areacode为0则是保存操作，此时先删除全部然后新增所有
		//若areacode不为0且list.size() == 0,说明为删除操作且库中没有此编号的数据，此时先删除全部然后新增所有，最后再删除此区域编号的数据
		//若areacode不为0且list.size() > 0,说明为删除操作且库中有此编号的数据，此时只删除此编号的数据即可
		if ("0".equals(areacode) || (!"0".equals(areacode) && 0 == list.size())) {	
			storeRange.setIareaseq("");
			operatorMaintenanceMapper.deleteStoreRange(storeRange);	//删除配送范围主表
			operatorMaintenanceMapper.deleteRangeCoordi(storeRange);//删除配送范围子表
		
			//循环数组，获取各区域内容
			for (int i = 0; i < contents.length; i++) {
				storeRange = new StoreRange();
				String str = contents[i];
				String[] details = str.split(":")[1].split(";");						//获取冒号后的字符串，然后根据分号隔开，组成数组
				String iareaseq = str.split(";")[str.split(";").length - 3];			//获取区域编码
				
				storeRange.setPk_storerange(CodeHelper.createUUID().substring(0, 20));	//配送范围主键
				storeRange.setPk_group(pk_group);										//企业号
				storeRange.setPk_store(pk_store);										//当前门店主键
				storeRange.setIareaseq(iareaseq);										//区域编号
				storeRange.setVareaname("区域" + iareaseq);								//区域名称
				storeRange.setNstartprice(str.split(";")[str.split(";").length - 2]);	//该区域起送价
				storeRange.setNdistributfee(str.split(";")[str.split(";").length - 1]);	//该区域配送费
				storeRange.setVcolor(str.split(":")[0]);								//该区域颜色
				
				//为子表赋值
				for (int j = 0; j < details.length - 3; j++) {
					StoreRange storeRangeDetail = new StoreRange();
					String[] detail = details[j].split("-");										//将数组中循环出的每一个根据"-"隔开
					
					storeRangeDetail.setPk_rangecoordi(CodeHelper.createUUID().substring(0, 20));	//配送范围子表主键
					storeRangeDetail.setPk_group(pk_group);											//企业号
					storeRangeDetail.setPk_store(pk_store);											//当前门店主键
					storeRangeDetail.setIareaseq(iareaseq);											//区域编号
					storeRangeDetail.setVlat(detail[0]);											//获取"-"前的数据即：纬度值
					storeRangeDetail.setVlng(detail[1]);											//获取"-"后的数据即：经度值
	
					listRangeCoordi.add(storeRangeDetail);
				}
	
				count = count + details.length - 3;								//获取要插入子表库中的总数据条数
				
				listStoreRange.add(storeRange);									//将每一个封装后的实体类放入list
			}
	
			int zhu = operatorMaintenanceMapper.addStoreRange(listStoreRange);	//添加主表数据并获取返回值
	
			int zi = operatorMaintenanceMapper.addRangeCoordi(listRangeCoordi);	//添加子表数据并获取返回值
			
			//若添加主表数据获取的返回值和传过来的数据相等，并且添加子表数据获取的返回值和插入的数据个数相等，则flag赋值true
			if (contents.length == zhu && count == zi) {
				flag = "Y";
			}
			
			//若areacode不为0，且list.size() == 0,说明为删除操作且库中没有此编号的数据，所以在新增之后要将此编号的数据删除掉
			if (!"0".equals(areacode) && 0 == list.size()) {
				storeRange.setIareaseq(areacode);
				int delZhu = operatorMaintenanceMapper.deleteStoreRange(storeRange);//删除配送范围主表
				int delZi = operatorMaintenanceMapper.deleteRangeCoordi(storeRange);//删除配送范围子表
				
				//若delZhu == 1，delZi > 0说明删除成功
				if (1 == delZhu && delZi > 0) {
					flag = "Y";
				} else {
					flag = "N";
				}
			}
		} else if (!"0".equals(areacode) && 0 < list.size()) {
			int delZhu = operatorMaintenanceMapper.deleteStoreRange(storeRange);//删除配送范围主表
			int delZi = operatorMaintenanceMapper.deleteRangeCoordi(storeRange);//删除配送范围子表
			
			//若delZhu == 1，delZi > 0说明删除成功
			if (1 == delZhu && delZi > 0) {
				flag = "Y";
			} else {
				flag = "N";
			}
		}
		
		return flag;
	}
}