package com.choice.misboh.service.lossmanagement;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.ForResourceFiles;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.domain.BaseRecord.PubItem;
import com.choice.misboh.domain.costReduction.Costdtl;
import com.choice.misboh.domain.costReduction.MisSupply;
import com.choice.misboh.domain.inventory.PositnSupply;
import com.choice.misboh.domain.lossmanage.PostionLoss;
import com.choice.misboh.domain.util.ConditionMis;
import com.choice.misboh.persistence.common.CommonMISBOHMapper;
import com.choice.misboh.persistence.inout.ChkoutdMisMapper;
import com.choice.misboh.persistence.inout.ChkoutmMisMapper;
import com.choice.misboh.persistence.lossmanagement.LossManageMapper;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Spcodeexm;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.DeliverMapper;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.util.CalChkNum;

/**
 * 损耗管理
 * @author 王超
 * 2015-04-22
 */
@Service
public class LossManageService {

	@Autowired
	private LossManageMapper lossManageMapper;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private ChkoutmMisMapper chkoutmMisMapper;
	@Autowired
	private ChkoutdMisMapper chkoutdMisMapper;
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	@Autowired
	private CommonMISBOHMapper commonMISBOHMapper;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	@Autowired
	private PositnMapper positnMapper;
	@Autowired
	private DeliverMapper deliverMapper;
	
	private final transient Log log = LogFactory.getLog(LossManageService.class);
	
	/**
	 * 根据营业日期和部门查询损耗单据
	 * @param map
	 * @return
	 * @throws CRUDException
	 */
	public List<PostionLoss> listLossManage(Map<String,String> map)throws CRUDException{
		try {
			PostionLoss postionLoss = new PostionLoss();
			postionLoss.setWorkdate(map.get("workdate"));
			postionLoss.setDept(map.get("dept"));
			postionLoss.setTyp(ValueUtil.getIntValue(map.get("typ")));
			postionLoss.setScode(map.get("scode"));
			return lossManageMapper.listLossManage(postionLoss);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 自动检索菜品
	 * @param pubitem
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public List<PubItem> queryPubItemmTop10(PubItem pubitem,HttpSession session,String storecode)throws CRUDException{
		try {
			if (storecode == null || "".equals(storecode)) {
				Positn thePositn = (Positn)session.getAttribute("accountPositn");
				storecode = thePositn.getCode();
			}
			ConditionMis conditionMis = new ConditionMis();
			conditionMis.setPubitecode(pubitem.getVcode());
			conditionMis.setPubitename(pubitem.getVname());
			conditionMis.setPubiteinit(pubitem.getVinit());
			conditionMis.setStorecode(storecode);
			return lossManageMapper.queryPubItemmTop10(conditionMis);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存损耗单
	 * @param session
	 * @param pl
	 * @return
	 * @throws Exception
	 */
	public String saveLossManage(HttpSession session,PostionLoss pl)throws CRUDException{
		try {
			String inputby = ValueUtil.getStringValue(session.getAttribute("accountId"));
			String inputTime = DateJudge.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss");
			lossManageMapper.deleteLossManage(pl);
			for (PostionLoss postionLoss : pl.getListPostionLoss()) {
				if (postionLoss.getCnt() == 0) {
					continue;
				}
				if (postionLoss.getPk_postionloss() == null || "".equals(postionLoss.getPk_postionloss())) {
					postionLoss.setPk_postionloss(CodeHelper.createUUID());
				}
				postionLoss.setInputby(inputby);
				postionLoss.setState(1);
				postionLoss.setInputtime(inputTime);
				postionLoss.setDept(pl.getDept());
				postionLoss.setScode(pl.getScode());
				lossManageMapper.saveLossManage(postionLoss);
			}
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 清空损耗单
	 * @param postionLoss
	 * @return
	 * @throws Exception
	 */
	public String deleteLossManage(PostionLoss postionLoss)throws CRUDException{
		try {
			lossManageMapper.deleteLossManage(postionLoss);
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 成品损耗确认
	 * @param postionLoss
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public String enterLossManage(PostionLoss postionLoss,HttpSession session)throws CRUDException{
		try {
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			String acct = ValueUtil.getStringValue(session.getAttribute("ChoiceAcct"));
			String yearr = mainInfoMapper.findYearrList().get(0);//会计年2015.1.5wjf
			String accountuser = ValueUtil.getStringValue(session.getAttribute("accountName"));
			Date maded = DateJudge.getDateByString(postionLoss.getWorkdate());
			Positn positn = new Positn();
			positn.setCode(postionLoss.getDept());
			List<PostionLoss> list = new ArrayList<PostionLoss>();
			list = lossManageMapper.listLossManage(postionLoss);
			Map<String,String> result = new HashMap<String,String>();
			//获取单号
			int chkoutno = chkoutmMisMapper.findNextNo();
			Positn pk = new Positn();
			pk.setCode(DDT.profitDeliver);
			pk = positnMapper.findPositnByCode(pk);
			if(pk == null){
				throw new CRUDException("未设置盘亏仓位！无法确认，请联系总部进行设置！");
			}
			Deliver pk2 = new Deliver();
			pk2.setCode(DDT.profitDeliver);
			pk2 = deliverMapper.findDeliverByCode(pk2);
			if(pk2 == null || pk2.getPositn() == null){
				throw new CRUDException("未设置盘亏供应商或者盘亏供应商未对应盘亏仓位！无法确认，请联系总部进行设置！");
			}
			if (list.size() > 0) {
				Map<String,Object> map = new HashMap<String,Object>();
				map.put("acct", acct);
				map.put("yearr", yearr);
				map.put("chkoutno", chkoutno);
				map.put("vouno", calChkNum.getNextBytable(DDT.VOUNO_CHKOUTM,DDT.VOUNO_CK+thePositn.getCode()+"-", maded));
				map.put("maded", maded);
				map.put("madet", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				map.put("positn", postionLoss.getDept());
				map.put("firm", DDT.profitDeliver);
				map.put("madeby", accountuser);
				map.put("totalamt", 0d);
				map.put("typ", "9915");//出库报损
				map.put("memo", "报损出库");
				map.put("pr", 0);
				//判断物资是否重复
				Map<String,Map<String,String>> mapSpcode = new HashMap<String,Map<String,String>>();
				//记录物资用量的集合
				List<Map<String,String>> listMap = new ArrayList<Map<String,String>>();
				for (PostionLoss ploss : list) {
					//每道菜品的BOM，用来计算物资的毛用量
					ploss.setAcct(acct);
					ploss.setScode(postionLoss.getScode());
					List<Costdtl> lt = lossManageMapper.queryCostItemByCode(ploss);
					if (lt != null && lt.size() > 0) {
						for (Costdtl costdtl : lt) {
							if (mapSpcode.get(costdtl.getSp_code()) == null) {
								Map<String,String> mapSp = new HashMap<String,String>();
								//现在的用量
								double nowyl = ValueUtil.getDoubleValue(costdtl.getCnt() * ploss.getCnt());
								mapSp.put("sp_code", costdtl.getSp_code());//物资编码
								mapSp.put(costdtl.getSp_code(), ValueUtil.getStringValue(nowyl));//每种物资的用量
								mapSp.put("memo", ploss.getMemo());
								mapSpcode.put(costdtl.getSp_code(), mapSp);//用物资编码作为主键
								listMap.add(mapSp);//将用量的map保存到list集合中
							} else {
								Map<String,String> mapSp = mapSpcode.get(costdtl.getSp_code());
								mapSp.put("sp_code", costdtl.getSp_code());
								//之前的用量
								double beforeyl = ValueUtil.getDoubleValue(mapSp.get(costdtl.getSp_code()));
								//现在的用量
								double nowyl = ValueUtil.getDoubleValue(costdtl.getCnt() * ploss.getCnt());
								mapSp.put(costdtl.getSp_code(), ValueUtil.getStringValue(beforeyl + nowyl));
								mapSp.put("memo", ploss.getMemo());
								mapSpcode.put(costdtl.getSp_code(), mapSp);
								listMap.add(mapSp);
							}
						}
					}
				}
				//循环用量生成出库单
				if (listMap != null && listMap.size() > 0) {
					chkoutmMisMapper.saveChkoutm(map);
					result.put("pr", ValueUtil.getStringValue(map.get("pr")));
					if(!(ValueUtil.getIntValue(map.get("pr")) == 1)){
						result.put("pr", "-1");
						throw new Exception("出库单"+map.get("vouno")+"添加失败");
					}
					String isControAmount = ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "isControAmount");
					for (Map<String,String> m : listMap) {
//						SppriceSale sppricesale = new SppriceSale();
//						sppricesale.setAcct(acct);
//						sppricesale.setSp_code(m.get("sp_code"));
//						sppricesale.setArea(positn);
//						sppricesale.setBdat(maded);
//						Supply supply = lossManageMapper.queryPriceSale(sppricesale);
						Map<String,Object> mm = new HashMap<String,Object>();
						mm.put("acct", acct);
						mm.put("chkoutno", chkoutno);///
						mm.put("sp_code", m.get("sp_code"));
						mm.put("amount", ValueUtil.getDoubleValue(m.get(m.get("sp_code"))));
						double price = 0.0;
						SupplyAcct supplyAcct = new SupplyAcct();
						supplyAcct.setAcct(acct);
						supplyAcct.setYearr(yearr);
						supplyAcct.setSp_code(m.get("sp_code"));
						supplyAcct.setPositn(postionLoss.getDept());
						PositnSupply ps = commonMISBOHMapper.findViewPositnSupply(supplyAcct);
						if("Y".equals(isControAmount)){
							if(ValueUtil.getDoubleValue(m.get(m.get("sp_code")))>ps.getInc0()){
								throw new Exception("BOM原料" + m.get("sp_code")+"库存不足，不能生成耗用。");			
							}
						}
						if(ps != null && ps.getInc0() > 0){//有结存的取结存金额/结存数量
							price = ps.getSp_price();
						}else{
							//改为取物资在本仓位的最后进价，需要查supplyacct，没有则取标准价
							MisSupply supply = new MisSupply();
							supply.setAcct(acct);
							supply.setSp_code(m.get("sp_code"));
							supply.setSp_position(thePositn.getCode());
							supply.setMemo("all");
							supply = commonMISBOHService.findSupplyBySpcode(supply);
							price = 0 != supply.getLast_price() ? supply.getLast_price() : supply.getSp_price();
//							Double lastPrice = inventoryMisMapper.findLastPriceBySupplyAcct(supplyAcct);
//							price = (lastPrice != null && lastPrice != 0) ? lastPrice : supply != null ? supply.getSp_price():0 ;//有最后进价取最后进价没有取标准价
						}
						mm.put("price", price);
						mm.put("memo", m.get("memo"));
						mm.put("stoid", 0d);
						mm.put("batchid", 0);
						mm.put("deliver", DDT.profitDeliver);
						mm.put("amount1", 0d);
						mm.put("pricesale", 0d);
						mm.put("chkstono", 0);
						mm.put("pr", 0);
						chkoutdMisMapper.saveChkoutd(mm);
						switch(ValueUtil.getIntValue(mm.get("pr"))){
						case -1:
							result.put("pr", "-1");
							throw new Exception("出库单" + map.get("vouno") + "添加失败" + m.get("sp_code"));
						case -2:
							result.put("pr", "-2");
							throw new Exception("出库单" + map.get("vouno") + "添加失败" + m.get("sp_code"));
						}
						result.put("pr", ValueUtil.getStringValue(mm.get("pr")));
					}
					//更新出库单的金额***********
//					Chkoutm chkoutm = new Chkoutm();
//					chkoutm.setChkoutno(chkoutno);
//					chkoutm.setTotalamt(money);
//					chkoutm.setAcct(acct);
//					lossManageMapper.updateChkoutm(chkoutm);
					//***********************
					//****审核出库单************
					Map<String,Object> map1 = new HashMap<String,Object>();
					map1.put("chkoutno", chkoutno);
					map1.put("month",  ValueUtil.getStringValue(commonMISBOHService.getOnlyAccountMonth(maded,thePositn.getCode())));//会计月
					map1.put("emp", accountuser);
					map1.put("sta", "P");
					Positn p = positnMapper.findPositnByCode(positn);
					//特殊的  配置1 则所有的门店走先进先出
					if("1".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "firmOutWay"))){
						if("1203".equals(p.getPcode())){
							map1.put("sta", "A");//传A 存储过程会判断 走Y
						}
					}else if("2".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "firmOutWay"))){//配置2 只有启用档口的店才走 先进先出
						if("1203".equals(p.getPcode())){
							if("Y".equals(p.getYnUseDept())){//启用档口的
								map1.put("sta", "A");//传A 存储过程会判断 走Y
							}
						}
					}
					map1.put("chk1memo", "");//审核备注
					map1.put("pr", 0);
					map1.put("errdes", "");
					chkoutmMisMapper.AuditChkout(map1);
					result.put("pr", ValueUtil.getStringValue(map1.get("pr")));
					result.put("msg", ValueUtil.getStringValue(map1.get("errdes")));
					int i = ValueUtil.getIntValue(map1.get("pr"));
					if (i == 1) {
						//**************更新postionLoss表的确认状态**************
						lossManageMapper.updatePostionLoss(postionLoss);
//						JSONObject rs = JSONObject.fromObject(result);
						return "操作成功！";
					} else {
						return ValueUtil.getStringValue(map1.get("errdes"));
					}
				}
			}else{
				return "没有数据！";
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
		return "生成报损出库单失败！";
	}
	
	/**
	 * 自动检索物资
	 * @param supply
	 * @param session
	 * @param storecode
	 * @return
	 * @throws Exception
	 */
	public List<Supply> findSupplyListTopN(Supply supply,HttpSession session,String storecode)throws CRUDException{
		try {
			if (storecode == null || "".equals(storecode)) {
				Positn thePositn = (Positn)session.getAttribute("accountPositn");
				storecode = thePositn.getCode();
			}
			ConditionMis conditionMis = new ConditionMis();
			conditionMis.setSupplycode(supply.getSp_code());
			conditionMis.setSupplydes(supply.getSp_name());
			conditionMis.setSupplyinit(supply.getSp_init());
			conditionMis.setPositncode(storecode);
			conditionMis.setAcct(ValueUtil.getStringValue(session.getAttribute("ChoiceAcct")));
			return lossManageMapper.findSupplyListTopN(conditionMis);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 自动检索半成品
	 * @param supply
	 * @param session
	 * @param storecode
	 * @return
	 * @throws Exception
	 */
	public List<Supply> findSupplyListTopNBCP(Supply supply,HttpSession session,String storecode)throws CRUDException{
		try {
			if (storecode == null || "".equals(storecode)) {
				Positn thePositn = (Positn)session.getAttribute("accountPositn");
				storecode = thePositn.getCode();
			}
			ConditionMis conditionMis = new ConditionMis();
			conditionMis.setSupplycode(supply.getSp_code());
			conditionMis.setSupplydes(supply.getSp_name());
			conditionMis.setSupplyinit(supply.getSp_init());
			conditionMis.setPositncode(storecode);
			conditionMis.setAcct(ValueUtil.getStringValue(session.getAttribute("ChoiceAcct")));
			conditionMis.setIsOrNotBCP("Y");
			return lossManageMapper.findSupplyListTopNBCP(conditionMis);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 原料损耗确认
	 * @param postionLoss
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public String enterLossManage3(PostionLoss postionLoss,HttpSession session)throws CRUDException{
		try {
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			String acct = ValueUtil.getStringValue(session.getAttribute("ChoiceAcct"));
			String yearr = mainInfoMapper.findYearrList().get(0);//会计年2015.1.5wjf
			String accountuser = ValueUtil.getStringValue(session.getAttribute("accountName"));
			Date maded = DateJudge.getDateByString(postionLoss.getWorkdate());
			Positn positn = new Positn();
			positn.setCode(postionLoss.getDept());
			List<PostionLoss> list = new ArrayList<PostionLoss>();
			list = lossManageMapper.listLossManage(postionLoss);
			Map<String,String> result = new HashMap<String,String>();
			//获取单号
			int chkoutno = chkoutmMisMapper.findNextNo();
			Positn pk = new Positn();
			pk.setCode(DDT.profitDeliver);
			pk = positnMapper.findPositnByCode(pk);
			if(pk == null){
				throw new CRUDException("未设置盘亏仓位！无法确认，请联系总部进行设置！");
			}
			Deliver pk2 = new Deliver();
			pk2.setCode(DDT.profitDeliver);
			pk2 = deliverMapper.findDeliverByCode(pk2);
			if(pk2 == null || pk2.getPositn() == null){
				throw new CRUDException("未设置盘亏供应商或者盘亏供应商未对应盘亏仓位！无法确认，请联系总部进行设置！");
			}
			if (list.size() > 0) {
				String isControAmount = ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "isControAmount");
				if("Y".equals(isControAmount)){
					String sp_code="";
					//查询库存量--物资余额表数据
					SupplyAcct supplyAcct1 = new SupplyAcct();
					supplyAcct1.setAcct(session.getAttribute("ChoiceAcct").toString());
					supplyAcct1.setPositn(postionLoss.getDept());
					List<PositnSupply> psList = commonMISBOHService.findViewPositnSupplyList(supplyAcct1);
					for(PostionLoss pol :list){
						for(PositnSupply postn:psList){
							if(pol.getPcode().equals(postn.getSp_code())){
								double amo=pol.getCnt();
								double am=postn.getInc0();
								if (am < amo) {
									sp_code += pol.getPcode()+",";
								}
							}
						}
						
					}
					if (!"".equals(sp_code)) {//2016/09/05 ghc
						String rss = "物资："+sp_code+"出库单数量大于库存数量，审核失败";
						return rss;
					}
				}
				Map<String,Object> map = new HashMap<String,Object>();
				map.put("acct", acct);
				map.put("yearr", yearr);
				map.put("chkoutno", chkoutno);
				map.put("vouno", calChkNum.getNextBytable(DDT.VOUNO_CHKOUTM,DDT.VOUNO_CK+thePositn.getCode()+"-", maded));
				map.put("maded", maded);
				map.put("madet", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				map.put("positn", postionLoss.getDept());
				map.put("firm", DDT.profitDeliver);
				map.put("madeby", accountuser);
				map.put("totalamt", 0d);
				map.put("typ", "9915");//出库报损
				map.put("memo", "报损出库");
				map.put("pr", 0);
				map.put("totalamt", 0);
				chkoutmMisMapper.saveChkoutm(map);
				result.put("pr", ValueUtil.getStringValue(map.get("pr")));
				if(!(ValueUtil.getIntValue(map.get("pr")) == 1)){
					result.put("pr", "-1");
					throw new Exception("出库单"+map.get("vouno")+"添加失败");
				}
				for (PostionLoss p : list) {
//					SppriceSale sppricesale = new SppriceSale();
//					sppricesale.setAcct(acct);
//					sppricesale.setSp_code(p.getPcode());
//					sppricesale.setArea(positn);
//					sppricesale.setBdat(maded);
//					Supply supply = lossManageMapper.queryPriceSale(sppricesale);
					Map<String,Object> mm = new HashMap<String,Object>();
					mm.put("acct", acct);
					mm.put("chkoutno", chkoutno);///
					mm.put("sp_code", p.getPcode());
					mm.put("amount", p.getCnt());
					double price = 0.0;
					SupplyAcct supplyAcct = new SupplyAcct();
					supplyAcct.setAcct(acct);
					supplyAcct.setYearr(yearr);
					supplyAcct.setSp_code(p.getPcode());
					supplyAcct.setPositn(postionLoss.getDept());
					PositnSupply ps = commonMISBOHMapper.findViewPositnSupply(supplyAcct);
					if(ps != null && ps.getInc0() > 0){//有结存的取结存金额/结存数量
						price = ps.getSp_price();
					}else{
						//改为取物资在本仓位的最后进价，需要查supplyacct，没有则取标准价
						MisSupply supply = new MisSupply();
						supply.setAcct(acct);
						supply.setSp_code(p.getPcode());
						supply.setSp_position(thePositn.getCode());
						supply.setMemo("all");
						supply = commonMISBOHService.findSupplyBySpcode(supply);
						price = 0 != supply.getLast_price() ? supply.getLast_price() : supply.getSp_price();
//						Double lastPrice = inventoryMisMapper.findLastPriceBySupplyAcct(supplyAcct);
//						price = (lastPrice != null && lastPrice != 0) ? lastPrice : supply != null ? supply.getSp_price():0 ;//有最后进价取最后进价没有取标准价
					}
					mm.put("price", price);
					mm.put("memo", p.getMemo());
					mm.put("stoid", 0d);
					mm.put("batchid", 0);
					mm.put("deliver", DDT.profitDeliver);
					mm.put("amount1", p.getCnt1());
					mm.put("pricesale", 0d);
					mm.put("chkstono", 0);
					mm.put("pr", 0);
					chkoutdMisMapper.saveChkoutd(mm);
					switch(ValueUtil.getIntValue(mm.get("pr"))){
					case -1:
						result.put("pr", "-1");
						throw new Exception("出库单" + map.get("vouno") + "添加失败" + p.getPcode());
					case -2:
						result.put("pr", "-2");
						throw new Exception("出库单" + map.get("vouno") + "添加失败" + p.getPcode());
					}
					result.put("pr", ValueUtil.getStringValue(mm.get("pr")));
				}
				//****审核出库单************
				Map<String,Object> map1 = new HashMap<String,Object>();
				map1.put("chkoutno", chkoutno);
				map1.put("month",  ValueUtil.getStringValue(commonMISBOHService.getOnlyAccountMonth(maded,thePositn.getCode())));//会计月
				map1.put("emp", accountuser);
				map1.put("sta", "P");
				Positn p = positnMapper.findPositnByCode(positn);
				//特殊的  配置1 则所有的门店走先进先出
				if("1".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "firmOutWay"))){
					if("1203".equals(p.getPcode())){
						map1.put("sta", "A");//传A 存储过程会判断 走Y
					}
				}else if("2".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "firmOutWay"))){//配置2 只有启用档口的店才走 先进先出
					if("1203".equals(p.getPcode())){
						if("Y".equals(p.getYnUseDept())){//启用档口的
							map1.put("sta", "A");//传A 存储过程会判断 走Y
						}
					}
				}
				map1.put("chk1memo", "");//审核备注  wjf
				map1.put("pr", 0);
				map1.put("errdes", "");
				chkoutmMisMapper.AuditChkout(map1);
				result.put("pr", ValueUtil.getStringValue(map1.get("pr")));
				result.put("msg", ValueUtil.getStringValue(map1.get("errdes")));
				int i = ValueUtil.getIntValue(map1.get("pr"));
				if (i == 1) {
					//**************更新postionLoss表的确认状态**************
					lossManageMapper.updatePostionLoss(postionLoss);
//					JSONObject rs = JSONObject.fromObject(result);
					return "操作成功！";
				} else {
					return ValueUtil.getStringValue(map1.get("errdes"));
				}
			}else{
				return "没有数据！";
			}
			//*************************************************
		} catch (Exception e) {
			log.error(e.getMessage());
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/**
	 * 半成品损耗确认
	 * @param postionLoss
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public String enterLossManage2(PostionLoss postionLoss,HttpSession session)throws CRUDException{
		try {
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			String acct = ValueUtil.getStringValue(session.getAttribute("ChoiceAcct"));
			String yearr = mainInfoMapper.findYearrList().get(0);//会计年2015.1.5wjf
			String accountuser = ValueUtil.getStringValue(session.getAttribute("accountName"));
			Date maded = DateJudge.getDateByString(postionLoss.getWorkdate());
			Positn positn = new Positn();
			positn.setCode(postionLoss.getDept());
			List<PostionLoss> list = new ArrayList<PostionLoss>();
			list = lossManageMapper.listLossManage(postionLoss);
			Map<String,String> result = new HashMap<String,String>();
			//获取单号
			int chkoutno = chkoutmMisMapper.findNextNo();
			
			Positn pk = new Positn();
			pk.setCode(DDT.profitDeliver);
			pk = positnMapper.findPositnByCode(pk);
			if(pk == null){
				throw new CRUDException("未设置盘亏仓位！无法确认，请联系总部进行设置！");
			}
			Deliver pk2 = new Deliver();
			pk2.setCode(DDT.profitDeliver);
			pk2 = deliverMapper.findDeliverByCode(pk2);
			if(pk2 == null || pk2.getPositn() == null){
				throw new CRUDException("未设置盘亏供应商或者盘亏供应商未对应盘亏仓位！无法确认，请联系总部进行设置！");
			}
			if (list.size() > 0) {
				Map<String,Object> map = new HashMap<String,Object>();
				map.put("acct", acct);
				map.put("yearr", yearr);
				map.put("chkoutno", chkoutno);
				map.put("vouno", calChkNum.getNextBytable(DDT.VOUNO_CHKOUTM,DDT.VOUNO_CK+thePositn.getCode()+"-", maded));
				map.put("maded", maded);
				map.put("madet", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				map.put("positn", postionLoss.getDept());
				map.put("firm", DDT.profitDeliver);
				map.put("madeby", accountuser);
				map.put("totalamt", 0d);
				map.put("typ", "9915");//出库报废
				map.put("memo", "报损出库");
				map.put("pr", 0);
				chkoutmMisMapper.saveChkoutm(map);
				result.put("pr", ValueUtil.getStringValue(map.get("pr")));
				if(!(ValueUtil.getIntValue(map.get("pr")) == 1)){
					result.put("pr", "-1");
					throw new Exception("出库单"+map.get("vouno")+"添加失败");
				}
				//判断物资是否重复
				Map<String,Map<String,String>> mapSpcode = new HashMap<String,Map<String,String>>();
				//记录物资用量的集合
				List<Map<String,String>> listMap = new ArrayList<Map<String,String>>();
				for (PostionLoss ploss : list) {
					//每道菜品的BOM，用来计算物资的毛用量
					ploss.setAcct(acct);
					ploss.setScode(postionLoss.getScode());
					List<Spcodeexm> lt = lossManageMapper.querySpcodeexmByCode(ploss);
					if (lt != null && lt.size() > 0) {
						for (Spcodeexm spcodeexm : lt) {
							if (mapSpcode.get(spcodeexm.getSp_code()) == null) {
								Map<String,String> mapSp = new HashMap<String,String>();
								//现在的用量
								double nowyl = ValueUtil.getDoubleValue(spcodeexm.getCnt() * ploss.getCnt());
								mapSp.put("sp_code", spcodeexm.getSp_code());//物资编码
								mapSp.put(spcodeexm.getSp_code(), ValueUtil.getStringValue(nowyl));//每种物资的用量
								mapSp.put("memo", ploss.getMemo());
								mapSpcode.put(spcodeexm.getSp_code(), mapSp);//用物资编码作为主键
								listMap.add(mapSp);//将用量的map保存到list集合中
							} else {
								Map<String,String> mapSp = mapSpcode.get(spcodeexm.getSp_code());
								mapSp.put("sp_code", spcodeexm.getSp_code());
								//之前的用量
								double beforeyl = ValueUtil.getDoubleValue(mapSp.get(spcodeexm.getSp_code()));
								//现在的用量
								double nowyl = ValueUtil.getDoubleValue(spcodeexm.getCnt() * ploss.getCnt());
								mapSp.put(spcodeexm.getSp_code(), ValueUtil.getStringValue(beforeyl + nowyl));
								mapSp.put("memo", ploss.getMemo());
								mapSpcode.put(spcodeexm.getSp_code(), mapSp);
								listMap.add(mapSp);
							}
						}
					}
				}
				String isControAmount = ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "isControAmount");
				//循环用量生成出库单
				if (listMap != null && listMap.size() > 0) {
					for (Map<String,String> m : listMap) {
//						SppriceSale sppricesale = new SppriceSale();
//						sppricesale.setAcct(acct);
//						sppricesale.setSp_code(m.get("sp_code"));
//						sppricesale.setArea(positn);
//						sppricesale.setBdat(maded);
//						Supply supply = lossManageMapper.queryPriceSale(sppricesale);
						Map<String,Object> mm = new HashMap<String,Object>();
						mm.put("acct", acct);
						mm.put("chkoutno", chkoutno);///
						mm.put("sp_code", m.get("sp_code"));
						mm.put("amount", ValueUtil.getDoubleValue(m.get(m.get("sp_code"))));
						double price = 0.0;
						SupplyAcct supplyAcct = new SupplyAcct();
						supplyAcct.setAcct(acct);
						supplyAcct.setYearr(yearr);
						supplyAcct.setSp_code(m.get("sp_code"));
						supplyAcct.setPositn(postionLoss.getDept());
						PositnSupply ps = commonMISBOHMapper.findViewPositnSupply(supplyAcct);
						if("Y".equals(isControAmount)){
							if(ValueUtil.getDoubleValue(m.get(m.get("sp_code")))>ps.getInc0()){
								throw new Exception("BOM原料" + m.get("sp_code")+"库存不足，不能生成耗用。");
							}
						}
						if(ps != null && ps.getInc0() > 0){//有结存的取结存金额/结存数量
							price = ps.getSp_price();
						}else{
							//改为取物资在本仓位的最后进价，需要查supplyacct，没有则取标准价
							MisSupply supply = new MisSupply();
							supply.setAcct(acct);
							supply.setSp_code(m.get("sp_code"));
							supply.setSp_position(thePositn.getCode());
							supply.setMemo("all");
							supply = commonMISBOHService.findSupplyBySpcode(supply);
							price = 0 != supply.getLast_price() ? supply.getLast_price() : supply.getSp_price();
//							Double lastPrice = inventoryMisMapper.findLastPriceBySupplyAcct(supplyAcct);
//							price = (lastPrice != null && lastPrice != 0) ? lastPrice : supply != null ? supply.getSp_price():0 ;//有最后进价取最后进价没有取标准价
						}
						mm.put("price", price);
						mm.put("memo", m.get("memo"));
						mm.put("stoid", 0d);
						mm.put("batchid", 0);
						mm.put("deliver", DDT.profitDeliver);
						mm.put("amount1", 0d);
						mm.put("pricesale", 0d);
						mm.put("chkstono", 0);
						mm.put("pr", 0);
						chkoutdMisMapper.saveChkoutd(mm);
						switch(ValueUtil.getIntValue(mm.get("pr"))){
						case -1:
							result.put("pr", "-1");
							throw new Exception("出库单" + map.get("vouno") + "添加失败" + m.get("sp_code"));
						case -2:
							result.put("pr", "-2");
							throw new Exception("出库单" + map.get("vouno") + "添加失败" + m.get("sp_code"));
						}
						result.put("pr", ValueUtil.getStringValue(mm.get("pr")));
					}
				}
				//更新出库单的金额***********
//				Chkoutm chkoutm = new Chkoutm();
//				chkoutm.setChkoutno(chkoutno);
//				chkoutm.setTotalamt(money);
//				chkoutm.setAcct(acct);
//				lossManageMapper.updateChkoutm(chkoutm);
				//***********************
				//****审核出库单************
				Map<String,Object> map1 = new HashMap<String,Object>();
				map1.put("chkoutno", chkoutno);
				map1.put("month",  ValueUtil.getStringValue(commonMISBOHService.getOnlyAccountMonth(maded,thePositn.getCode())));//会计月
				map1.put("emp", accountuser);
				map1.put("sta", "P");
				Positn p = positnMapper.findPositnByCode(positn);
				//特殊的  配置1 则所有的门店走先进先出
				if("1".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "firmOutWay"))){
					if("1203".equals(p.getPcode())){
						map1.put("sta", "A");//传A 存储过程会判断 走Y
					}
				}else if("2".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "firmOutWay"))){//配置2 只有启用档口的店才走 先进先出
					if("1203".equals(p.getPcode())){
						if("Y".equals(p.getYnUseDept())){//启用档口的
							map1.put("sta", "A");//传A 存储过程会判断 走Y
						}
					}
				}
				map1.put("chk1memo", "");//审核备注  wjf
				map1.put("pr", 0);
				map1.put("errdes", "");
				chkoutmMisMapper.AuditChkout(map1);
				result.put("pr", ValueUtil.getStringValue(map1.get("pr")));
				result.put("msg", ValueUtil.getStringValue(map1.get("errdes")));
				int i = ValueUtil.getIntValue(map1.get("pr"));
				if (i == 1) {
					//**************更新postionLoss表的确认状态**************
					lossManageMapper.updatePostionLoss(postionLoss);
//					JSONObject rs = JSONObject.fromObject(result);
					return "操作成功！";
				} else {
					return ValueUtil.getStringValue(map1.get("errdes"));
				}
			}else{
				return "没有数据！";
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}

	
	/**
	 * 检测菜品是否存在BOM
	 * @author 文清泉 2015年5月30日 下午8:53:22
	 * @param postionLoss 
	 * @param session
	 * @param typ
	 * @return
	 */
	public List<String> checkEnterLossManage(PostionLoss postionLoss,
			HttpSession session, int typ) {
		List<String> list = new ArrayList<String>();
		List<PostionLoss> listPos = new ArrayList<PostionLoss>();
		listPos = lossManageMapper.listLossManage(postionLoss);
		if(typ == 1){
			for (PostionLoss ploss : listPos) {
				ploss.setScode(postionLoss.getScode());
				List<Costdtl> lt = lossManageMapper.queryCostItemByCode(ploss);
				if(lt.size()<=0){
					list.add(ploss.getPname());
				}
			}
		}else if(typ == 2){
			for (PostionLoss ploss : listPos) {
				ploss.setScode(postionLoss.getScode());
				List<Spcodeexm> lt = lossManageMapper.querySpcodeexmByCode(ploss);
				if(lt.size()<=0){
					list.add(ploss.getPname());
				}
			}
		}
		return list;
	}
	
}
