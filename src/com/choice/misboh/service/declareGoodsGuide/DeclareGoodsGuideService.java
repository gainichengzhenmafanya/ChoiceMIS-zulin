package com.choice.misboh.service.declareGoodsGuide;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.domain.declareGoodsGuide.Declare;
import com.choice.misboh.persistence.DeclareGoodsGuideMapper;
import com.choice.scm.persistence.MainInfoMapper;

/**
 * 报货向导  -  控制逻辑层
 * @author 文清泉
 * @param 2015-4-2 上午9:37:20
 */
@Service
public class DeclareGoodsGuideService {
    
    @Autowired
    private DeclareGoodsGuideMapper declareGoodsGuideMapper;
    @Autowired
    private MainInfoMapper mainInfoMapper;
    /**
     * 获取配送班表中可订货类别
     * @author 文清泉
     * @param 2015-4-2 下午3:16:01
     * @param dat 订货日
     * @param positnCode 店铺编码
     * @param isuseschedule 是否启用配送班表
     * @return
     */
    public List<Map<String,Object>> getCategoryList(String dat, String positnCode, String isuseschedule) {
    	if(isuseschedule!=null && isuseschedule.equals("Y")){
    		return declareGoodsGuideMapper.getCategoryList(dat,positnCode,DDT.CODEDES11);
    	}else{
    		return declareGoodsGuideMapper.getCategoryNoList(DDT.CODEDES11);
    	}
    }
    
    /**
     * 获取预估营业额
     * @author 文清泉
     * @param 2015年4月8日 下午1:41:56
     * @param code  餐厅编码
     * @param dat 订货日
     * @param useMaxEndDate 下次到货日
     * @return
     */
	public List<Map<String, Object>> listAdjustTheEstimated(String code,
			String dat, String useMaxEndDate) {
		return declareGoodsGuideMapper.listAdjustTheEstimated(code,dat,useMaxEndDate);
	}

	/**
	 * 计算千元用量 
	 * @author 文清泉
	 * @param 2015年4月8日 下午6:21:57
	 * @param category_code 报货类别
	 * @param bdat 参考开始时间
	 * @param edat 参考结束时间
	 * @param SALEVALUE 预估值
	 * @param dept 部门
	 * @param vplantyp 报货方式 
	 * @param scode 店铺编码
	 * @return
	 */
	public List<Declare> calculationThousands(String category_code, String bdat,
			String edat, Object SALEVALUE, String dept, String vplantyp, String scode) {
			List<Map<String,Object>> list =  declareGoodsGuideMapper.getCalculationMoney(bdat,edat,dept,scode);
			List<Map<String,Object>> spList = declareGoodsGuideMapper.getCalculationSpcode(bdat,edat,dept,scode,category_code);
			Double _value = 0d;
			// 确认是否存在实际营业额
			if(list!=null && list.size()>0 && list.get(0)!= null){
				if(vplantyp.equals(DDT.AMT)){
					_value = ValueUtil.getDoubleValue(list.get(0).get("MONEY"));
				}else if(vplantyp.equals(DDT.TC)){
					_value =ValueUtil.getDoubleValue(list.get(0).get("TC"));
				}else if(vplantyp.equals(DDT.PAX)){
					_value = ValueUtil.getDoubleValue(list.get(0).get("PEOLENUM"));
				}
			}
			// 计算千用量
			List<Declare> declareList = new ArrayList<Declare>();
			for (Map<String, Object> spmap : spList) {
				Declare declare = new Declare();
				declare.setDailyGoods_Typ(ValueUtil.getStringValue(spmap.get("DAILYGOODS_TYP")));
				declare.setSp_code(ValueUtil.getStringValue(spmap.get("SP_CODE")));
				declare.setSp_name(ValueUtil.getStringValue(spmap.get("SP_NAME")));
				declare.setForecastMoney(ValueUtil.getDoubleValue(SALEVALUE));
				declare.setUnit(ValueUtil.getStringValue(spmap.get("UNIT")));
				declare.setSp_desc(ValueUtil.getStringValue(spmap.get("SP_DESC")));
				declare.setSp_unit(ValueUtil.getStringValue(spmap.get("SP_UNIT")));
				declare.setUnitRate_x(ValueUtil.getDoubleValue(spmap.get("UNITRATE_X")));
				declare.setUnitRate(ValueUtil.getDoubleValue(spmap.get("UNITPER3")));
				// 计算千用量    【实际用量／实际营业额　×1000】
				if(_value ==0d){
					declare.setEstimatedParameters(0d);
				}else{
					declare.setEstimatedParameters(ValueUtil.getDoubleValue(spmap.get("YL"))/_value*1000.0);
				}
				declare.setPeriodUse(declare.getEstimatedParameters() * ValueUtil.getDoubleValue(SALEVALUE));
				declareList.add(declare);
			}
		return declareList;
	}

	/**
	 * 获取当前门店所有物资实际库存
	 * @author 文清泉
	 * @param 2015年1月1日 下午4:32:37
	 * @param code
	 * @param dept
	 * @param dat 
	 * @return
	 */
	public List<Map<String, Object>> findSupplyTcurstore(String scode,
			String dept, String dat) {
		String yearr = mainInfoMapper.findYearrList().get(0);
		return declareGoodsGuideMapper.findSupplyTcurstore(scode,dept,yearr);
	}

	/**
	 * 菜品销售计划与点击率物资用量计算
	 * @author 文清泉
	 * @param 2015年4月25日 下午3:42:34
	 * @param category_code 报货类别
	 * @param bdat 开始时间
	 * @param edat 结束时间
	 * @param dept 部门
	 * @param scode 店铺编码
	 * @param supplyList 
	 * @return
	 */
	public Map<String,Declare> calculationFoodSale(String category_code,
			String bdat, String edat, String dept,String scode, List<Map<String, Object>> supplyList) {
		//物资序列KEY值  supplyList
//		"KCCNT","MINCNT","SP_NAME","SP_DESC","UNIT","UNIT3","UNITPER3","CATEGORY_CODE"
		List<Map<String,Object>> spList = declareGoodsGuideMapper.calculationFoodSale(category_code,bdat,edat,dept,scode);
		Map<String,Declare> declareMap = new HashMap<String,Declare>();
		for (Map<String, Object> map : supplyList) {
			if(map.get("CATEGORY_CODE")==null||!map.get("CATEGORY_CODE").equals(category_code)){
				continue;
			}
			Declare declare = new Declare();
			declare.setDailyGoods_Typ(ValueUtil.getStringValue(map.get("CATEGORY_CODE")));//报货类别
			declare.setSp_code(ValueUtil.getStringValue(map.get("SP_CODE")));//物资编码
			declare.setSp_name(ValueUtil.getStringValue(map.get("SP_NAME")));//物资名称
			declare.setUnit(ValueUtil.getStringValue(map.get("UNIT3")));//采购单位
			declare.setSp_desc(ValueUtil.getStringValue(map.get("SP_DESC")));//规格
			declare.setSp_unit(ValueUtil.getStringValue(map.get("UNIT")));//标准单位
			declare.setUnitRate(ValueUtil.getDoubleValue(map.get("UNITPER3")));//单位转换率
			declare.setMincnt(ValueUtil.getDoubleValue(map.get("MINCNT")));//最小申购
			declare.setSpmin(ValueUtil.getDoubleValue(map.get("SPMIN")));//安全库存
			declare.setSpnow(ValueUtil.getDoubleValue(map.get("KCCNT")));//当前库存
			declare.setEstimatedParameters(0d);
			declare.setPeriodUse(0d);
			declareMap.put(ValueUtil.getStringValue(map.get("SP_CODE")), declare);
		}
		for (Map<String,Object> map : spList) {
			if(declareMap!=null){
//				Declare declare = new Declare();
				Declare declare = declareMap.get(ValueUtil.getStringValue(map.get("SP_CODE")));
				declare.setUnitRate_x(ValueUtil.getDoubleValue(map.get("UNITRATE_X")));//虚拟单位转换率
				declare.setEstimatedParameters(ValueUtil.getDoubleValue(map.get("QY")));
				declare.setPeriodUse(ValueUtil.getDoubleValue(map.get("PERIODUSE")));
//				declareMap.put(ValueUtil.getStringValue(map.get("SP_CODE")), declare);
			}
		}
		return declareMap;
	}

	/**
	 * 安全库存报货
	 * @author 文清泉
	 * @param supplyList 
	 * @param 2015年4月25日 下午4:10:36
	 * @param string
	 * @param code
	 * @return
	 */
	public List<Declare> getSafeStockMap(String category_code, String scode,String dept) {
		//物资序列KEY值  supplyList
//		"KCCNT","MINCNT","SP_NAME","SP_DESC","UNIT","UNIT3","UNITPER3","CATEGORY_CODE"
		String yearr = mainInfoMapper.findYearrList().get(0);
		List<Declare> spList = declareGoodsGuideMapper.getSafeStockMap(category_code,dept,scode,yearr);
		return spList;
//		Map<String,Declare> declareMap = new HashMap<String,Declare>();
//		for (Map<String, Object> map : supplyList) {
//			if(map.get("CATEGORY_CODE")==null||!map.get("CATEGORY_CODE").equals(category_code)){
//				continue;
//			}
//			Declare declare = new Declare();
//			declare.setDailyGoods_Typ(ValueUtil.getStringValue(map.get("CATEGORY_CODE")));//报货类别
//			declare.setSp_code(ValueUtil.getStringValue(map.get("SP_CODE")));//物资编码
//			declare.setSp_name(ValueUtil.getStringValue(map.get("SP_NAME")));//物资名称
//			declare.setUnit(ValueUtil.getStringValue(map.get("UNIT3")));//采购单位
//			declare.setSp_desc(ValueUtil.getStringValue(map.get("SP_DESC")));//规格
//			declare.setSp_unit(ValueUtil.getStringValue(map.get("UNIT")));//标准单位
//			declare.setUnitRate(ValueUtil.getDoubleValue(map.get("UNITPER3")));//单位转换率
//			declare.setMincnt(ValueUtil.getDoubleValue(map.get("MINCNT")));//最小申购
//			declare.setSpnow(ValueUtil.getDoubleValue(map.get("KCCNT")));//当前库存
//			declare.setSpway(0d);
//			declare.setSpmin(0d);
//			declare.setSpway(0d);
//			declare.setSpuse(0d);
//			declareMap.put(ValueUtil.getStringValue(map.get("SP_CODE")), declare);
//		}
//		for (Map<String,Object> map : spList) {
//			if(declareMap!=null){
//				Declare declare = new Declare();
//				declare = declareMap.get(ValueUtil.getStringValue(map.get("SP_CODE")));
//				declare.setSpway(ValueUtil.getDoubleValue(map.get("SPWAY")));//在途
//				declare.setSpuse(ValueUtil.getDoubleValue(map.get("SPUSE")));//需求量
//				declare.setUnitRate_x(ValueUtil.getDoubleValue(map.get("UNITRATE_X")));
//				declare.setSpmin(ValueUtil.getDoubleValue(map.get("SPMIN")));//最低库存
//				declareMap.put(ValueUtil.getStringValue(map.get("SP_CODE")), declare);
//			}
//		}
//		return declareMap;
	}

	/**
	 * 周次平均计算 --- 平均三周
	 * @author 文清泉
	 * @param 2015年6月9日 下午6:11:06
	 * @param category_code
	 * @param code
	 * @param dept
	 * @param dat 
	 * @param supplyList 
	 * @return
	 */
	public Map<String,Declare> getAverageWeeklyMap(String category_code, String scode,
			String dept, String dat, List<Map<String, Object>> supplyList) {
		//物资序列KEY值  supplyList
//		"KCCNT","MINCNT","SP_NAME","SP_DESC","UNIT","UNIT3","UNITPER3","CATEGORY_CODE"
		String yearr = mainInfoMapper.findYearrList().get(0);
		String bdat = DateJudge.getLenTime(-21, dat);
		// 获取物资数量
		List<Map<String,Object>> spList = declareGoodsGuideMapper.getAverageWeeklyMap(category_code,dept,scode,yearr,bdat,dat);
		Map<String,List<Map<String,Object>>> avgMap = new HashMap<String,List<Map<String,Object>>>();
		String dweek = DateJudge.getWeekOfDateToString(dat);
		for (Map<String, Object> map : spList) {
			String week = DateJudge.getWeekOfDateToString(ValueUtil.getStringValue(map.get("DAT")));
			List<Map<String,Object>> weekList = new ArrayList<Map<String,Object>>();
			if(!week.equals(dweek)){
				continue;
			}
			// 将周次相同数据存在相同MAP中
			if(avgMap.get(week) == null){
				weekList.add(map);
				avgMap.put(week,weekList);
			}else{
				weekList = avgMap.get(week);
				weekList.add(map);
				avgMap.put(week, weekList);
			}
		}
		Map<String,Declare> declareMap = new HashMap<String,Declare>();
		for (Map<String, Object> map : supplyList) {
			if(map.get("CATEGORY_CODE")==null||!map.get("CATEGORY_CODE").equals(category_code)){
				continue;
			}
			Declare declare = new Declare();
			declare.setDailyGoods_Typ(ValueUtil.getStringValue(map.get("CATEGORY_CODE")));//报货类别
			declare.setSp_code(ValueUtil.getStringValue(map.get("SP_CODE")));//物资编码
			declare.setSp_name(ValueUtil.getStringValue(map.get("SP_NAME")));//物资名称
			declare.setUnit(ValueUtil.getStringValue(map.get("UNIT3")));//采购单位
			declare.setSp_desc(ValueUtil.getStringValue(map.get("SP_DESC")));//规格
			declare.setSp_unit(ValueUtil.getStringValue(map.get("UNIT")));//标准单位
			declare.setUnitRate(ValueUtil.getDoubleValue(map.get("UNITPER3")));//单位转换率
			declare.setSpmin(ValueUtil.getDoubleValue(map.get("SPMIN")));//安全库存
			declare.setMincnt(ValueUtil.getDoubleValue(map.get("MINCNT")));//最小申购
			declare.setSpnow(ValueUtil.getDoubleValue(map.get("KCCNT")));//当前库存
			declare.setSpway(0d);
			declare.setSpuse(0d);
			declareMap.put(ValueUtil.getStringValue(map.get("SP_CODE")), declare);
		}
		
		
		//循环所以周次数据
		Map<String,Declare> spMap = new HashMap<String,Declare>();
		for (String key : avgMap.keySet()) {
			List<Map<String,Object>> weekList = avgMap.get(key);
			//循环周次下所有物资
			for (Map<String, Object> map : weekList) {
				String sp_code = ValueUtil.getStringValue(map.get("SP_CODE"));
				Declare declare= new Declare();
				declare.setUnitRate_x(ValueUtil.getDoubleValue(map.get("UNITRATE_X")));
				declare.setSpway(ValueUtil.getDoubleValue(map.get("SPWAY")));//在途数量
				declare.setSpmin(ValueUtil.getDoubleValue(map.get("SPMIN")));//最低库存
				//计算物资周期用量
				if(spMap.get(sp_code)==null){
					declare.setEditorCount(1);
					declare.setSpuse(ValueUtil.getDoubleValue(map.get("SPUSE")));//需求量
				}else{
					declare.setSpuse(spMap.get(sp_code).getSpuse()+ValueUtil.getDoubleValue(map.get("SPUSE")));
					declare.setEditorCount(declare.getEditorCount()+1);
				}
				spMap.put(sp_code, declare);
			}
		}
		// 循环所有物资用量
		for (String key : spMap.keySet()) {
			if(spMap!=null){
				Declare declare= declareMap.get(key);
				declare.setUnitRate_x(declare.getUnitRate_x());
				declare.setSpway(declare.getSpway());//在途数量
				declare.setSpuse(declare.getSpuse());
				declare.setEditorCount(declare.getEditorCount());
//				declareMap.put(key,declare);
			}
		}
		return declareMap;
	}

	/**
	 * 根据报货类别检测门店填写单据数量
	 * @author 文清泉
	 * @param 2015年8月13日 上午11:14:54
	 * @param bdat 订货日期
	 * @param code 店铺编码
	 * @param dailyGoodsCnt 控制数量 
	 * @return
	 */
	public String getcheckChkstomList(String bdat,String code, String dailyGoodsCnt) {
		//获取总共报货次数
		List<Map<String, Object>> list = declareGoodsGuideMapper.getcheckChkstomList(bdat,code,DDT.CODEDES11);
		// 类别订货次数
		Integer dcnt = ValueUtil.getIntValue(dailyGoodsCnt);
		String flag = "";
		for (Map<String, Object> map : list) {
			Integer cnt = ValueUtil.getIntValue(map.get("CNT"));
			if(cnt >= dcnt){
				flag+="【"+ValueUtil.getStringValue(map.get("CODE"))+","+ValueUtil.getStringValue(map.get("DES"))+"】";
			}
		}
		if(flag.equals("")){
			flag = "-1";
		}
		return flag;
	}
}
