package com.choice.misboh.service.inout;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.persistence.inout.ChkindMisMapper;
import com.choice.misboh.persistence.inout.ChkinmMisMapper;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Chkind;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.Spbatch;
import com.choice.scm.persistence.DeliverMapper;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.SupplyMapper;
import com.choice.scm.service.ReadChkinmExcel;
import com.choice.scm.util.FileWorked;

@Service

public class ChkinmMisService {
	
	@Autowired
	private ChkinmMisMapper chkinmMisMapper;
	@Autowired
	private ChkindMisMapper chkindMisMapper;
	@Autowired
	private PageManager<Chkinm> pageManager;
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	@Autowired
	private SupplyMapper supplyMapper;
	@Autowired
	private PositnMapper positnMapper;
	@Autowired
	private DeliverMapper deliverMapper;
	private final transient Log log = LogFactory.getLog(ChkinmMisService.class);
	
	
	/**
	 * 查询所有的门店入库单
	 * @param chkinm
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkinm> findAllChkinm(String checkOrNot,Chkinm chkinm,Page page,Date madedEnd,String locale) throws CRUDException {
		try {
			Map<String,Object> map = new HashMap<String,Object>();
			if(null!=chkinm && null!=chkinm.getInout() && "zf".equals(chkinm.getInout())){
				chkinm.setInout(DDT.INOUT);
			}else if(null!=chkinm && null!=chkinm.getInout() && "rk".equals(chkinm.getInout())){
				chkinm.setInout(DDT.IN);
			}
			map.put("chkinm", chkinm);
			map.put("locale", locale);
			map.put("checkOrNot", checkOrNot);
			map.put("madedEnd", madedEnd);//checkOrNot == check 或者 uncheck   时候 模糊查询  已审核或则未审核
			map.put("typ", chkinm.getTyp());
			log.warn("门店入库单查询:\n"+
					"bdat:"+DateFormat.getStringByDate(chkinm.getMaded(), "yyyy-MM-dd")+
					",edat:"+DateFormat.getStringByDate(madedEnd, "yyyy-MM-dd")+
					",checkOrNot:"+map.get("checkOrNot")+","+
					chkinm
					);
			return pageManager.selectPage(map,page,ChkinmMisMapper.class.getName()+".findAllChkinmByinput");
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}

	/**
	 * 查询门店入库单号序列最大值
	 */
	public Integer getMaxChkinno() throws CRUDException {
		return chkinmMisMapper.getMaxChkinno();
	}
	
	/**
	 * 保存门店入库单
	 * @param chkinm
	 * @throws CRUDException
	 */
	public int saveChkinm(Chkinm chkinm, String inout) throws CRUDException {
		try {
			Date date=new Date();
			chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
			chkinm.setMadet(DateFormat.getStringByDate(date,"yyyy-MM-dd HH:mm:ss"));
			log.warn("添加门店入库单(主单)：\n"+chkinm);
	 		chkinmMisMapper.saveChkinm(chkinm);
	 		log.warn("添加门店入库单(详单)：");
			for (int i = 0; i < chkinm.getChkindList().size(); i++) {
				Chkind chkind=chkinm.getChkindList().get(i);
				chkind.setChkinno(chkinm.getChkinno());
				//chkind.setDued(DateFormat.getDateByString("1899-12-30", "yyyy-MM-dd"));新版本应该都有生产日期了2014.12.30wjf
				if(inout.equals("bh")){
					chkind.setInout(chkind.getInout());   //加入子单    标志    
				}else if(inout.equals("in")){
					chkind.setInout(DDT.IN);   //加入入库标志
				}else if(inout.equals("zb")){
					chkind.setInout(DDT.INOUT);   //加入直发标志
				}
				log.warn(chkind);
	 			chkindMisMapper.saveChkind(chkind);
			}
			return  chkinm.getPr();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改门店入库单
	 * @param chkinm
	 * @throws CRUDException
	 */
	public int updateChkinm(Chkinm chkinm) throws CRUDException {
		try {
			Date date=new Date();
			chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
			chkinm.setMadet(DateFormat.getStringByDate(date,"yyyy-MM-dd HH:mm:ss"));
			
			log.warn("更新门店入库单(主单):\n"+chkinm);
	 		chkinmMisMapper.updateChkinm(chkinm);//修改门店入库单
	 		//再添加入数据
	 		log.warn("更新门店入库单(详单):");
			for (int i = 0; i < chkinm.getChkindList().size(); i++) {
				Chkind chkind=chkinm.getChkindList().get(i);
				chkind.setChkinno(chkinm.getChkinno());
				log.warn(chkind);
	 			chkindMisMapper.saveChkind(chkind);
			}
			return chkinm.getPr();
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 审核门店入库单   单条或批量审核
	 * @param listId
	 * @throws CRUDException
	 */
	public String checkChkinm(Chkinm chkinm,String chkinnoids) throws CRUDException {
		try {
			Map<String,String> result = new HashMap<String,String>();
			int pr=1;
			
			log.warn("审核门店入库单：\n");
			if(null==chkinnoids){
				log.warn(chkinm);
				chkinm.setMonth(commonMISBOHService.getOnlyAccountMonth(chkinm.getMaded(),chkinm.getFirm()));//  此字段当成月份使用    加入会计月
				chkinmMisMapper.checkChkinm(chkinm);//单条审核门店入库单
				 pr=chkinm.getPr();
			}else{
				log.warn(chkinnoids);
				List<String> ids = Arrays.asList(chkinnoids.split(","));
				for (int i = 0; i < ids.size(); i++) {
					chkinm.setChkinno(Integer.parseInt(ids.get(i)));
					Chkinm chkinm_ = chkinmMisMapper.findChkinmByid(chkinm);
					chkinm.setMonth(commonMISBOHService.getOnlyAccountMonth(chkinm_.getMaded(),chkinm.getFirm()));//  此字段当成月份使用    加入会计月
					chkinmMisMapper.checkChkinm(chkinm);//循环批量 审核门店入库单
					pr=chkinm.getPr();
				}
			}
			result.put("pr", pr+"");	
			result.put("checby", chkinm.getChecbyName());
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/**
	 * 检测要保存、删除的门店入库单据是否被审核
	 * @param chkinm
	 * @return
	 * @throws CRUDException
	 */
	public String chkChect(String active,Chkinm chkinm) throws CRUDException {
		try {
			String showM = "YES",action = ""; 
			if("edit".equals(active)){
				action = "修改保存";
			}else if("delete".equals(active)){
				action = "删除";
			}
			Chkinm chkinm1=new Chkinm();
			chkinm1.setChkinno(chkinm.getChkinno());
			chkinm1 = chkinmMisMapper.findChkinmByid(chkinm);
			if(null == chkinm1){
				showM = "凭证号："+chkinm.getVouno()+"的单据已被删除，不能进行"+action+"操作。"; 
			}else if(null != chkinm1.getChecby() && !"".equals(chkinm1.getChecby())){
				showM = "凭证号："+chkinm.getVouno()+"的单据已被审核，不能进行"+action+"操作。"; 
			}
			return showM;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取入库数据
	 * @param listId
	 * @throws CRUDException
	 */
	public List<Spbatch> addChkinmByCx(Spbatch spbatch) throws CRUDException {
		try {
			return chkinmMisMapper.addChkinmByCx(spbatch);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据id查询 门店入库单
	 * @param chkinm
	 * @return
	 * @throws CRUDException
	 */
	public Chkinm findChkinmByid(Chkinm chkinm) throws CRUDException {
		try {
			if(null==chkinm.getChkinno()){
				return null;
			}else{
				log.warn("获取门店入库单：" + chkinm.getChkinno());
				return chkinmMisMapper.findChkinmByid(chkinm);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除门店入库单
	 * @param listId
	 * @throws CRUDException
	 */
	public void deleteChkinm(List<String> listId) throws CRUDException {
		try {
			log.warn("删除门店入库单:\n"+listId);
			//验证要删除的门店入库单是否已被审核：n--未审核，y--已审核
			String sta = "n";
			for(String id : listId){
				Chkinm chkinm = new Chkinm();
				chkinm.setChkinno(Integer.parseInt(id));
				Chkinm chkinmChec = findChkinmByid(chkinm);
				if(null != chkinmChec.getChecby() && !"".equals(chkinmChec.getChecby())){
					sta = "y";
				}
			}
			if("y".equals(sta)){
				return;
			}else{
				chkinmMisMapper.deleteChkinm(listId);
				//删除从表
				chkindMisMapper.deleteChkindByChkinno(listId);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据主键字符串查找数据
	* @param chkinno
	* @return
	 * @throws CRUDException 
	 */
	public List<Chkinm> findChkinmByIds(String chkinno) throws CRUDException {
		try {
			return chkinmMisMapper.findChkinmByIds(CodeHelper.replaceCode(chkinno));
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 下载模板信息 wjf
	 * 
	 * @param response
	 * @param request
	 * @throws IOException
	 */
	public void downloadTemplate(HttpServletResponse response,
			HttpServletRequest request) throws IOException {
		OutputStream outp = null;
		FileInputStream in = null;
		try {
			String fileName = "门店入库单.xls";
			String ctxPath = request.getSession().getServletContext()
					.getRealPath("/")
					+ "\\" + "template\\";
			String filedownload = ctxPath + fileName;
			fileName = URLEncoder.encode(fileName, "UTF-8");
			// 要下载的模板所在的绝对路径
			response.reset();
			response.addHeader("Content-Disposition", "attachment; filename="
					+ fileName);
			response.setContentType("application/octet-stream;charset=UTF-8");
			outp = response.getOutputStream();
			in = new FileInputStream(filedownload);
			byte[] b = new byte[1024];
			int i = 0;
			while ((i = in.read(b)) > 0) {
				outp.write(b, 0, i);
			}
			outp.flush();
		} catch (Exception e) {
			System.out.println("Error!");
			e.printStackTrace();
		} finally {
			if (in != null) {
				in.close();
				in = null;
			}
			if (outp != null) {
				outp.close();
				outp = null;
			}
		}
	}
	
	/**
	 * 将文件上传到temp文件夹下wjf
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	public String upload(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String realFilePath = "";
		try {
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("file");
			String realFileName = "chkinm.xls";
			String ctxPath = request.getSession().getServletContext()
					.getRealPath("/")
					+ "temp\\";
			String fileuploadPath = ctxPath;
			File dirPath = new File(fileuploadPath);
			if (!dirPath.exists()) {
				dirPath.mkdir();
			}
			realFilePath = fileuploadPath + realFileName;
			File uploadFile = new File(realFilePath);
			FileCopyUtils.copy(file.getBytes(), uploadFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return realFilePath;
	}
	
	/**
	 * 对execl进行验证
	 */
	public Object check(String path,String accountId) throws CRUDException {
		boolean checkResult = ReadChkinmExcel.check(accountId,path, supplyMapper,positnMapper,deliverMapper);
//		Map<String, String> map = new HashMap<String, String>();
		List<String> errorList = new ArrayList<String>();
		if (checkResult) {
			Chkinm chkinm = ReadChkinmExcel.chkinm;
			chkinm.setAcct(accountId);
			List<Chkind> chkindList = ReadChkinmExcel.chkindList;
			chkinm.setChkindList(chkindList);
			return chkinm;
		} else {
//			map = ReadChkstomExcel.map;
			errorList = ReadChkinmExcel.errorList;
		}
		FileWorked.deleteFile(path);//删除上传后的的文件
		return errorList;
	}
}
