package com.choice.misboh.service.inout;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.persistence.inout.ChkindMisMapper;
import com.choice.misboh.persistence.inout.ChkinmMisMapper;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Chkind;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Spbatch;
import com.choice.scm.persistence.MainInfoMapper;

@Service
public class ChkinmZbMisService {
	
	@Autowired
	private ChkinmMisMapper chkinmMisMapper;
	@Autowired
	private ChkindMisMapper chkindMisMapper;
	@Autowired
	private PageManager<Chkinm> pageManager;
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	private final transient Log log = LogFactory.getLog(ChkinmZbMisService.class);

	
	/**
	 * 查询入库单号序列最大值
	 */
	public Integer getMaxChkinno() throws CRUDException {
		return chkinmMisMapper.getMaxChkinno();
	}
		
	/**
	 * 查询所有的入库单
	 * @param chkinm
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkinm> findAllChkinm(String checkOrNot,Chkinm chkinm,Page page,Date madedEnd,String locale) throws CRUDException {
		try {
			Map<String,Object> map = new HashMap<String,Object>();
			chkinm.setInout(DDT.INOUT);
			map.put("chkinm", chkinm);
			map.put("locale", locale);
			map.put("checkOrNot", checkOrNot);
			map.put("madedEnd", madedEnd);//checkOrNot == check 或者 uncheck   时候 模糊查询  已审核或则未审核
			map.put("typ", chkinm.getTyp());
			log.warn("直拨单查询:\n"+
					"bdat:"+DateFormat.getStringByDate(chkinm.getMaded(), "yyyy-MM-dd")+
					",edat:"+DateFormat.getStringByDate(madedEnd, "yyyy-MM-dd")+
					",checkOrNot:"+map.get("checkOrNot")+","+
					chkinm
					);
			return pageManager.selectPage(map,page,ChkinmMisMapper.class.getName()+".findAllChkinmByinput");
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
		
	/**
	 * 查询所有的入库单   模糊查询           暂时没用到
	 * @param chkinm
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkinm> findAllChkinmByinput(Chkinm chkinm,Page page,Date madedEnd) throws CRUDException {
		try {
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("chkinm", chkinm);
			map.put("madedEnd", madedEnd);
			log.warn("入库单查询:\n"+
					"bdat:"+DateFormat.getStringByDate(chkinm.getMaded(), "yyyy-MM-dd")+
					",edat:"+DateFormat.getStringByDate(madedEnd, "yyyy-MM-dd")+","+
					chkinm
					);
			return pageManager.selectPage(map,page,ChkinmMisMapper.class.getName()+".findAllChkinmByinput");
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	/**
	 * 保存门店入库单
	 * @param chkinm
	 * @throws CRUDException
	 */
	public int saveChkinm(Chkinm chkinm,String inout) throws CRUDException {
		try {
			Date date=new Date();
			chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
			chkinm.setMadet(DateFormat.getStringByDate(date,"yyyy-MM-dd HH:mm:ss"));
			log.warn("添加门店入库单(主单)：\n"+chkinm);
	 		chkinmMisMapper.saveChkinm(chkinm);
	 		log.warn("添加门店入库单(详单)：");
			for (int i = 0; i < chkinm.getChkindList().size(); i++) {
				Chkind chkind=chkinm.getChkindList().get(i);
				chkind.setChkinno(chkinm.getChkinno());
				//chkind.setDued(DateFormat.getDateByString("1899-12-30", "yyyy-MM-dd"));新版本应该都有生产日期了2014.12.30wjf
				if(inout.equals("bh")){
					chkind.setInout(chkind.getInout());   //加入子单    标志    
				}else if(inout.equals("in")){
					chkind.setInout(DDT.IN);   //加入入库标志
				}else if(inout.equals("zb")){
					chkind.setInout(DDT.INOUT);   //加入直发标志
				}
				log.warn(chkind);
	 			chkindMisMapper.saveChkind(chkind);
			}
			return  chkinm.getPr();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 根据id查询 入库单
	 * @param chkinm
	 * @return
	 * @throws CRUDException
	 */
	public Chkinm findChkinmByid(Chkinm chkinm) throws CRUDException {
		try {
			if(null==chkinm.getChkinno()){
				return null;
			}else{
				log.warn("获取入库单：" + chkinm.getChkinno());
				return chkinmMisMapper.findChkinmByid(chkinm);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 修改入库单
	 * @param chkinm
	 * @throws CRUDException
	 */
	public int updateChkinm(Chkinm chkinm) throws CRUDException {
		try {
			//验证该入库单是否已被审核
			Chkinm chkinmChec = findChkinmByid(chkinm);
			if(null != chkinmChec.getChecby() && !"".equals(chkinmChec.getChecby())){
				return -1;
			}
			//删除该条记录

			Date date=new Date();
			chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
			chkinm.setMadet(DateFormat.getStringByDate(date,"yyyy-MM-dd HH:mm:ss"));
			
			log.warn("更新入库单(主单):\n"+chkinm);
	 		chkinmMisMapper.updateChkinm(chkinm);//修改入库单
	 		
	 		//再添加入数据
	 		log.warn("更新入库单(详单):");
			for (int i = 0; i < chkinm.getChkindList().size(); i++) {
				Chkind chkind=chkinm.getChkindList().get(i);
				chkind.setChkinno(chkinm.getChkinno());
				log.warn(chkind);
	 			chkindMisMapper.saveChkind(chkind);
			}
			return  chkinm.getPr();
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}		
	
	/**
	 * 删除入库单
	 * @param listId
	 * @throws CRUDException
	 */
	public void deleteChkinm(List<String> listId) throws CRUDException {
		try {
			log.warn("删除入库单:\n"+listId);
			//验证要删除的入库单是否已被审核：n--未审核，y--已审核
			String sta = "n";
			for(String id : listId){
				Chkinm chkinm = new Chkinm();
				chkinm.setChkinno(Integer.parseInt(id));
				Chkinm chkinmChec = findChkinmByid(chkinm);
				if(null != chkinmChec.getChecby() && !"".equals(chkinmChec.getChecby())){
					sta = "y";
				}
			}
			if("y".equals(sta)){
				return;
			}else{
				chkinmMisMapper.deleteChkinm(listId);
				//删除从表
				chkindMisMapper.deleteChkindByChkinno(listId);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取直发冲消数据
	 * @author 2014.11.4 wjf
	 * @param spbatch
	 * @throws CRUDException
	 */
	public List<Spbatch> addChkinmzfByCx(Spbatch spbatch) throws CRUDException {
		try {
			return chkinmMisMapper.addChkinmzfByCx(spbatch);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 审核入库单   单条或批量审核
	 * @param listId
	 * @throws CRUDException
	 */
	public String checkChkinm(Chkinm chkinm,String chkinnoids) throws CRUDException {
		try {
			Map<String,String> result = new HashMap<String,String>();
			int pr=1;
			log.warn("审核入库单：\n");
			if(null == chkinnoids){
				log.warn(chkinm);
				chkinm.setMonth(commonMISBOHService.getOnlyAccountMonth(chkinm.getMaded(),chkinm.getFirm()));//  此字段当成月份使用    加入会计月
				chkinmMisMapper.checkChkinm(chkinm);//单条审核入库单
				pr = chkinm.getPr();
			}else{
				log.warn(chkinnoids);
				List<String> ids = Arrays.asList(chkinnoids.split(","));
				for (int i = 0; i < ids.size(); i++) {
					chkinm.setChkinno(Integer.parseInt(ids.get(i)));
					Chkinm chkinm_ = chkinmMisMapper.findChkinmByid(chkinm);
					chkinm.setMonth(commonMISBOHService.getOnlyAccountMonth(chkinm_.getMaded(),chkinm.getFirm()));//  此字段当成月份使用    加入会计月
					chkinmMisMapper.checkChkinm(chkinm);//循环批量 审核入库单
					pr = chkinm.getPr();
				}
			}
			result.put("pr", pr+"");
			result.put("checby", chkinm.getChecbyName());
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/**
	 * 检测要保存、删除的入库单据是否被审核
	 * @param chkinm
	 * @return
	 * @throws CRUDException
	 */
	public String chkChect(String active,Chkinm chkinm) throws CRUDException {
		try {
			String showM = "YES",action = ""; 
			if("edit".equals(active)){
				action = "修改保存";
			}else if("delete".equals(active)){
				action = "删除";
			}
			Chkinm chkinm1=new Chkinm();
			chkinm1.setChkinno(chkinm.getChkinno());
			chkinm1 = chkinmMisMapper.findChkinmByid(chkinm);
			if(null == chkinm1){
				showM = "凭证号："+chkinm.getVouno()+"的单据已被删除，不能进行"+action+"操作。"; 
			}else if(null != chkinm1.getChecby() && !"".equals(chkinm1.getChecby())){
				showM = "凭证号："+chkinm.getVouno()+"的单据已被审核，不能进行"+action+"操作。"; 
			}
			return showM;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/***
	 * 查询门店下的部门前十条
	 * @param positn
	 * @return
	 */
	public List<Positn> findAllPositnDeptN(Positn positn) {
		return chkinmMisMapper.findAllPositnDeptN(positn);
	}
	
}
