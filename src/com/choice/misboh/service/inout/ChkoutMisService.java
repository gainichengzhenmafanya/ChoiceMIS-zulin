package com.choice.misboh.service.inout;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.misboh.persistence.inout.ChkoutdMisMapper;
import com.choice.misboh.persistence.inout.ChkoutmMisMapper;
import com.choice.misboh.persistence.inspection.InspectionMisMapper;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.ReadProperties;
import com.choice.scm.domain.Chkoutd;
import com.choice.scm.domain.Chkoutm;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Spbatch;
import com.choice.scm.persistence.CodeDesMapper;
import com.choice.scm.persistence.DeliverMapper;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.SupplyMapper;
import com.choice.scm.service.ReadChkoutExcel;
import com.choice.scm.util.FileWorked;

@Service
public class ChkoutMisService {
	@Autowired
	private ChkoutdMisMapper chkoutdMisMapper;
	@Autowired
	private ChkoutmMisMapper chkoutmMisMapper;
	@Autowired
	private PageManager<Chkoutm> pageManager;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	@Autowired
	private SupplyMapper supplyMapper;
	@Autowired
	private PositnMapper positnMapper;
	@Autowired
	private DeliverMapper deliverMapper;
	@Autowired
	private CodeDesMapper codeDesMapper;
	@Autowired
	private InspectionMisMapper inspectionMapper;
	private final transient Log log = LogFactory.getLog(ChkoutMisService.class);

	/**
	 * 模糊查询 出库单
	 * 
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkoutm> findChkoutm(Chkoutm chkoutm, Date bdat, Date edat, Page page, String locale, String db)
			throws CRUDException {
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			if (null == bdat) {
				bdat = new Date();
			}
			if (null == edat) {
				edat = new Date();
			}
			map.put("bdat", bdat);
			map.put("edat", edat);
			map.put("locale", locale);
			map.put("chkoutm", chkoutm);
			map.put("db", db);
			log.warn("出库单模糊查询：\n" + "bdat:" + DateFormat.getStringByDate(bdat, "yyyy-MM-dd") + ",edat:"
					+ DateFormat.getStringByDate(edat, "yyyy-MM-dd") + ",chkoutm.checkby:" + chkoutm.getChecby()
					+ ",chkoutm.firm:" + chkoutm.getChecby() + ",chkoutm.positn:" + chkoutm.getChecby()
					+ ",chkoutm.chkoutno:" + chkoutm.getChecby() + ",chkoutm.vouno:" + chkoutm.getChecby());
			if (null == page) {
				return chkoutmMisMapper.findChkoutm(map);
			}
			return pageManager.selectPage(map, page, ChkoutmMisMapper.class.getName() + ".findChkoutm");
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			throw new CRUDException(e);
		}

	}

	/**
	 * 根据id获取出库单信息
	 * 
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 */
	public Chkoutm findChkoutmById(Chkoutm chkoutm) throws CRUDException {
		try {
			log.warn("获取出库单：" + chkoutm.getChkoutno());
			return chkoutmMisMapper.findChkoutmById(chkoutm);
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}

	/**
	 * 新增出库单信息
	 * 
	 * @param chkoutm
	 * @throws CRUDException
	 */
	public String saveChkoutm(Chkoutm chkoutm) throws CRUDException {
		Map<String, String> result = new HashMap<String, String>();
		try {
			result.put("chkoutno", String.valueOf(chkoutm.getChkoutno()));
			// call
			// add_chkoutm(#{acct},#{yearr},#{chkoutno},#{vouno},#{maded},#{madet},#{positn.code},
			// #{firm.code},#{madeby},#{totalamt},#{typ},#{memo},#{pr,jdbcType=INTEGER,mode=OUT})
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("acct", chkoutm.getAcct());
			map.put("yearr", mainInfoMapper.findYearrList().get(0) + "");
			map.put("chkoutno", chkoutm.getChkoutno());
			map.put("vouno", chkoutm.getVouno());
			map.put("maded", chkoutm.getMaded());
			map.put("madet", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			map.put("positn", chkoutm.getPositn().getCode());
			map.put("firm", chkoutm.getFirm().getCode());
			map.put("madeby", chkoutm.getMadeby());
			// 修改出库单没有总价 wjf
			double totalAmt = 0;
			if (chkoutm.getChkoutd().size() != 0) {
				for (Chkoutd chkoutd : chkoutm.getChkoutd()) {
					double amount = chkoutd.getAmount();
					double price = chkoutd.getPrice();
					totalAmt += amount * price;
				}
			}
			chkoutm.setTotalamt(totalAmt);
			map.put("totalamt", chkoutm.getTotalamt());
			map.put("typ", chkoutm.getTyp());
			map.put("memo", chkoutm.getMemo());
			map.put("pr", 0);
			log.warn("新增出库单(主单):\n" + "yearr:" + map.get("yearr") + ",chkoutno:" + map.get("chkoutno") + ",vouno:"
					+ map.get("vouno") + ",maded:" + map.get("maded") + ",positn:" + map.get("positn") + ",firm:"
					+ map.get("firm") + ",madeby:" + map.get("madeby") + ",totalamt:" + map.get("totalamt") + ",typ:"
					+ map.get("typ") + ",memo:" + map.get("memo"));
			chkoutmMisMapper.saveChkoutm(map);
			result.put("pr", map.get("pr").toString());
			if (!(Integer.parseInt(map.get("pr").toString()) == 1)) {
				result.put("pr", "-1");
				throw new Exception("出库单" + chkoutm.getVouno() + "添加失败");
			}

			log.warn("新增出库单(详单):");
			if (chkoutm.getChkoutd() != null)
				for (Chkoutd chkoutd : chkoutm.getChkoutd()) {
					// call
					// add_chkoutd(#{chkoutno},#{sp_code.sp_code},#{amount},#{price},#{memo},#{stoid},
					// #{batchid},#{deliver.code},#{amount1},#{pricesale},#{pr,mode=OUT,jdbcType=INTEGER})
					Map<String, Object> m = new HashMap<String, Object>();
					m.put("chkoutno", chkoutm.getChkoutno());///
					m.put("sp_code", chkoutd.getSp_code().getSp_code());
					m.put("amount", chkoutd.getAmount());
					m.put("price", chkoutd.getPrice());
					m.put("memo", chkoutd.getMemo());
					m.put("stoid", chkoutd.getStoid());
					m.put("batchid", chkoutd.getBatchno());
					if (null != chkoutd.getDeliver())
						m.put("deliver", chkoutd.getDeliver().getCode());
					m.put("amount1", chkoutd.getAmount1());
					m.put("pricesale", 0);// 门店出库都不要售价 wjf2015.9.11
					m.put("chkstono", chkoutd.getChkstono());// wjf
					m.put("pr", 0);
					log.warn("sp_code:" + m.get("sp_code") + ",amount:" + m.get("amount") + ",price:" + m.get("price")
							+ ",memo:" + m.get("memo") + ",stoid:" + m.get("stoid") + ",batchid:" + m.get("batchid")
							+ ",deliver:" + m.get("deliver") + ",amount1:" + m.get("amount1") + ",pricesale:"
							+ m.get("pricesale"));
					chkoutdMisMapper.saveChkoutd(m);

					// int aasdf=10/0;

					result.put("pr", map.get("pr").toString());
					switch (Integer.parseInt(m.get("pr").toString())) {
					case -1:
						result.put("pr", "-1");
						throw new Exception("出库单" + chkoutm.getVouno() + "添加失败" + chkoutd.getSp_code().getSp_code());
					case -2:
						result.put("pr", "-2");
						throw new Exception("出库单" + chkoutm.getVouno() + "添加失败" + chkoutd.getSp_code().getSp_code());
					}
				}
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e.getMessage());
		}
	}

	/**
	 * 更新出库单信息
	 * 
	 * @param chkoutm
	 * @throws CRUDException
	 */
	public String updateChkoutm(Chkoutm chkoutm) throws CRUDException {
		Map<String, String> result = new HashMap<String, String>();
		try {
			// 检测要保存的出库单据是否被审核 // 去掉凭证号
			Chkoutm chkoutmchke = new Chkoutm();
			chkoutmchke.setChkoutno(chkoutm.getChkoutno());
			Chkoutm chkoutChec = chkoutmMisMapper.findChkoutmById(chkoutmchke);
			if (null != chkoutChec.getChecby() && !"".equals(chkoutChec.getChecby())) {
				result.put("pr", "-2");
				JSONObject rs = JSONObject.fromObject(result);
				return rs.toString();
			}
			result.put("chkoutno", chkoutm.getChkoutno().toString());
			Map<String, Object> chkout = new HashMap<String, Object>();
			chkout.put("chkoutno", chkoutm.getChkoutno().toString());
			chkout.put("vouno", chkoutm.getVouno());
			chkout.put("maded", chkoutm.getMaded());
			chkout.put("madet", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			chkout.put("positn", chkoutm.getPositn().getCode());
			chkout.put("madeby", chkoutm.getMadeby());
			chkout.put("firm", chkoutm.getFirm().getCode());
			chkout.put("totalamt", chkoutm.getTotalamt());
			chkout.put("typ", chkoutm.getTyp());
			chkout.put("memo", chkoutm.getMemo());
			chkout.put("pr", 0);
			log.warn("更新出库单(主单):\n" + "chkoutno:" + chkout.get("chkoutno") + ",vouno:" + chkout.get("vouno") + ",maded:"
					+ chkout.get("maded") + ",madet:" + chkout.get("madet") + ",positn:" + chkout.get("positn")
					+ ",madeby:" + chkout.get("madeby") + ",firm:" + chkout.get("firm") + ",totalamt:"
					+ chkout.get("totalamt") + ",typ:" + chkout.get("typ") + ",memo:" + chkout.get("memo"));
			chkoutmMisMapper.updateChkoutm(chkout);
			result.put("pr", chkout.get("pr").toString());
			if (!(Integer.parseInt(chkout.get("pr").toString()) == 1))
				throw new Exception("出库单修改失败");
			log.warn("更新出库单(详单):");
			for (Chkoutd chkoutd : chkoutm.getChkoutd()) {
				Map<String, Object> m = new HashMap<String, Object>();
				m.put("chkoutno", chkoutm.getChkoutno());
				m.put("sp_code", chkoutd.getSp_code().getSp_code());
				m.put("amount", chkoutd.getAmount());
				m.put("price", chkoutd.getPrice());
				m.put("memo", chkoutd.getMemo());
				m.put("stoid", chkoutd.getStoid());
				m.put("batchid", chkoutd.getBatchno());
				m.put("deliver", chkoutd.getDeliver());
				m.put("amount1", chkoutd.getAmount1());
				m.put("pricesale", 0);// 门店出库都不要售价 wjf2015.9.11
				m.put("pr", 0);
				log.warn("sp_code:" + m.get("sp_code") + ",amount:" + m.get("amount") + ",price:" + m.get("price")
						+ ",memo:" + m.get("memo") + ",stoid:" + m.get("stoid") + ",batchid:" + m.get("batchid")
						+ ",deliver:" + m.get("deliver") + ",amount1:" + m.get("amount1") + ",pricesale:"
						+ m.get("pricesale"));
				chkoutdMisMapper.saveChkoutd(m);
				switch (Integer.parseInt(m.get("pr").toString())) {
				case -1:
					result.put("pr", chkout.get("pr").toString());
					throw new Exception("出库单" + chkoutm.getVouno() + "更新失败" + chkoutd.getSp_code().getSp_code());
				case -2:
					result.put("pr", chkout.get("pr").toString());
					throw new Exception("出库单" + chkoutm.getVouno() + "已审核，更新失败" + chkoutd.getSp_code().getSp_code());
				}
			}
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e.getMessage());
		}
	}

	/**
	 * 删除出库单信息
	 * 
	 * @param chkoutm
	 * @throws CRUDException
	 */
	public String deleteChkoutm(Chkoutm chkoutm) throws CRUDException {
		Map<String, String> result = new HashMap<String, String>();
		try {
			Map<String, Object> chkout = new HashMap<String, Object>();
			String[] no = chkoutm.getVouno().split(",");
			log.warn("删除出库单:\n" + chkoutm.getVouno());
			for (String n : no) {
				chkout.put("chkoutno", n);
				chkout.put("pr", 0);
				chkoutmMisMapper.deleteChkoutm(chkout);
				if (!(Integer.parseInt(chkout.get("pr").toString()) == 1))
					throw new Exception("单据已审核，不能删除！");
				result.put("pr", chkout.get("pr").toString());
			}
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}

	/**
	 * 审核出库单信息
	 * 
	 * @param chkoutm
	 * @throws CRUDException
	 */
	public String updateByAudit(Chkoutm chkoutm, String checby) throws CRUDException {
		Map<String, String> result = new HashMap<String, String>();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			// 使用vouno接收chkoutno参数
			if (null == chkoutm.getChkoutnos()) {
				ReadProperties rp = new ReadProperties();
				String profitDeliver = rp.getStrByParam("profitDeliver");// 获取盘点仓位
				Chkoutm chkoutm1 = chkoutmMisMapper.findChkoutmById(chkoutm);
				chkoutm1.setChecby(checby);
				map.put("chkoutno", chkoutm1.getChkoutno());
				map.put("month",
						commonMISBOHService.getOnlyAccountMonth(chkoutm1.getMaded(), chkoutm.getFirm().getCode()) + "");
				map.put("emp", checby);

				// 一般情况下的出库 移动平均
				map.put("sta", "M");// 出库默认是M
				map.put("chk1memo", chkoutm.getChk1memo());// 审核备注 wjf
				if (chkoutm1.getFirm().getCode().equals(profitDeliver)) {// 如果使用仓位是盘亏，并且出库仓位是分店类型或者部门 则sta=P
					Deliver pk2 = new Deliver();
					pk2.setCode(profitDeliver);
					pk2 = deliverMapper.findDeliverByCode(pk2);
					if(pk2 == null || pk2.getPositn() == null){
						throw new CRUDException("未设置盘亏供应商或者盘亏供应商未对应盘亏仓位！无法确认，请联系总部进行设置！");
					}
					map.put("sta", "P");
					String isControAmount = ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "isControAmount");
					if("Y".equals(isControAmount)){
						List<Chkoutd> cko = chkoutm1.getChkoutd();
						String sp_code="";// 出库判断 ghc
						for (Chkoutd chkoutd : cko) {
							double amo = chkoutd.getAmount();
							double am = chkoutd.getSp_code().getCnt();
							if (am < amo) {
								sp_code += chkoutd.getSp_code().getSp_code()+",";
							}
						}
						if (!"".equals(sp_code)) {
							String rss = "物资："+sp_code+"出库单数量大于库存数量，审核失败";
							String prr = "9";
							result.put("pr", prr);
							result.put("rss", rss);
							JSONObject rs = JSONObject.fromObject(result);
							return rs.toString();
						}
					}
				}
				if ("9913".equals(chkoutm1.getTyp()) || "9914".equals(chkoutm1.getTyp())) {// 退库和冲消
					map.put("sta", "M");
				}
				// 特殊的 配置1 则所有的门店走先进先出
				if ("1".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "firmOutWay"))) {
					if ("1203".equals(chkoutm1.getPositn().getTyp())) {
						map.put("sta", "A");// 传A 存储过程会判断 走Y
					}
				} else if ("2".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "firmOutWay"))) {// 配置2
																												// 只有启用档口的店才走
																												// 先进先出
					if ("1203".equals(chkoutm1.getPositn().getTyp())) {
						Positn p = positnMapper.findPositnByCode(chkoutm1.getPositn());
						if ("Y".equals(p.getYnUseDept())) {// 启用档口的
							map.put("sta", "A");// 传A 存储过程会判断 走Y
						}
					}
				}
				map.put("pr", 0);
				map.put("errdes", "");
				log.warn("\tchkoutno:" + chkoutm1.getChkoutno() + "\n" + "month:" + map.get("month") + ",emp:"
						+ map.get("emp") + ",sta:" + map.get("sta"));
				chkoutmMisMapper.AuditChkout(map);
				chkoutmMisMapper.updateChkoutAmount(chkoutm1);
				result.put("pr", map.get("pr").toString());
				result.put("msg", map.get("errdes") != null ? map.get("errdes").toString() : "");
				switch (Integer.parseInt(map.get("pr").toString())) {
				case 0:
					break;
				case 1:
					break;
				case -1:
					break;
				case -3:
					break;
				case -4:
					break;
				case -5:
					break;
				default:
					throw new CRUDException(map.get("errdes").toString());
				}
			} else {
				String[] no = chkoutm.getChkoutnos().split(",");
				log.warn("审核出库单:\n" + chkoutm.getVouno());
				ReadProperties rp = new ReadProperties();
				String profitDeliver = rp.getStrByParam("profitDeliver");// 获取盘点仓位
				for (String n : no) {
					Chkoutm chkoutm1 = new Chkoutm();// 原先好大的bug 循环里居然用 一个对象 到底
					chkoutm1.setChkoutno(Integer.valueOf(n));
					chkoutm1 = chkoutmMisMapper.findChkoutmById(chkoutm1);
					
					map.put("chkoutno", n);
					map.put("month",
							commonMISBOHService.getOnlyAccountMonth(chkoutm1.getMaded(), chkoutm.getFirm().getCode())
									+ "");
					map.put("emp", checby);
					// 一般情况下的出库 移动平均
					map.put("sta", "M");// 出库默认是M
					if (chkoutm1.getFirm().getCode().equals(profitDeliver)) {// 如果使用仓位是盘亏，并且出库仓位是分店类型或者部门
						map.put("sta", "P");
						String isControAmount = ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "isControAmount");
						if("Y".equals(isControAmount)){
							List<Chkoutd> cko = chkoutm1.getChkoutd();
							String sp_code="";
							for (Chkoutd chkoutd : cko) {
								double amo = chkoutd.getAmount();
								double am = chkoutd.getSp_code().getCnt();
								if (am < amo) {
									sp_code += chkoutd.getSp_code().getSp_code()+",";
								}
							}
							if (!"".equals(sp_code)) {
								String rss = "单号："+n+"中物资："+sp_code+"出库单数量大于库存数量，审核失败";
								String prr = "9";
								result.put("pr", prr);
								result.put("rss", rss);
								JSONObject rs = JSONObject.fromObject(result);
								return rs.toString();
							}
						}
					}
					if ("9913".equals(chkoutm1.getTyp()) || "9914".equals(chkoutm1.getTyp())) {// 退库和冲消
						map.put("sta", "M");
					}
					// 特殊的 配置1 则所有的门店走先进先出
					if ("1".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "firmOutWay"))) {
						if ("1203".equals(chkoutm1.getPositn().getTyp())) {
							map.put("sta", "A");// 传A 存储过程会判断 走Y
						}
					} else if ("2".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "firmOutWay"))) {// 配置2
																													// 只有启用档口的店才走
																													// 先进先出
						if ("1203".equals(chkoutm1.getPositn().getTyp())) {
							Positn p = positnMapper.findPositnByCode(chkoutm1.getPositn());
							if ("Y".equals(p.getYnUseDept())) {// 启用档口的
								map.put("sta", "A");// 传A 存储过程会判断 走Y
							}
						}
					}
					map.put("pr", 0);
					map.put("errdes", "");
					log.warn("\tchkoutno:" + n + "\n" + "month:" + map.get("month") + ",emp:" + map.get("emp") + ",sta:"
							+ map.get("sta"));
					chkoutmMisMapper.AuditChkout(map);
					chkoutmMisMapper.updateChkoutAmount(chkoutm1);
					String pr = map.get("pr") != null ? map.get("pr").toString() : "";
					String errdes = map.get("errdes") != null ? map.get("errdes").toString() : "";
					result.put("pr", pr);
					result.put("msg", errdes);

					switch (Integer.parseInt(map.get("pr").toString())) {
					case 0:
						break;
					case 1:
						break;
					case -1:
						break;
					case -3:
						break;
					case -4:
						break;
					case -5:
						break;
					default:
						throw new CRUDException(map.get("errdes").toString());
					}
				}
			}
			result.put("checby", chkoutm.getChecbyName());// 审核人
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e.getMessage());
		}
	}

	/**
	 * 获取下一单号
	 * 
	 * @return
	 * @throws CRUDException
	 */
	public BigDecimal findNextNo() throws CRUDException {
		try {
			return BigDecimal.valueOf(chkoutmMisMapper.findNextNo());
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 检测要保存、删除的出库单据是否被审核
	 * 
	 * @param active
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 */
	public String chkChect(String active, Chkoutm chkoutm) throws CRUDException {
		try {
			String showM = "YES", action = "";
			if ("edit".equals(active)) {
				action = "修改保存";
			} else if ("delete".equals(active)) {
				action = "删除";
			}
			chkoutm.setVouno("");
			Chkoutm listChecked = chkoutmMisMapper.findChkoutmById(chkoutm);
			if (null == listChecked) {
				showM = "单号：" + chkoutm.getChkoutno() + "的单据已被删除，不能进行" + action + "操作。";
			} else if (null != listChecked.getChecby() && !"".equals(listChecked.getChecby())) {
				showM = "凭证号：" + listChecked.getVouno() + "的单据已被审核，不能进行" + action + "操作。";
			}
			return showM;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 获取出库冲消数据
	 * @param spbatch
	 * @throws CRUDException
	 */
	public List<Spbatch> findChkoutByCx(Spbatch spbatch) throws CRUDException {
		try {
			return chkoutmMisMapper.findChkoutByCx(spbatch);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 下载模板信息 wjf
	 * 
	 * @param response
	 * @param request
	 * @throws IOException
	 */
	public void downloadTemplate(HttpServletResponse response, HttpServletRequest request) throws IOException {
		OutputStream outp = null;
		FileInputStream in = null;
		try {
			String fileName = "出库单.xls";
			String ctxPath = request.getSession().getServletContext().getRealPath("/") + "\\" + "template\\";
			String filedownload = ctxPath + fileName;
			fileName = URLEncoder.encode(fileName, "UTF-8");
			// 要下载的模板所在的绝对路径
			response.reset();
			response.addHeader("Content-Disposition", "attachment; filename=" + fileName);
			response.setContentType("application/octet-stream;charset=UTF-8");
			outp = response.getOutputStream();
			in = new FileInputStream(filedownload);
			byte[] b = new byte[1024];
			int i = 0;
			while ((i = in.read(b)) > 0) {
				outp.write(b, 0, i);
			}
			outp.flush();
		} catch (Exception e) {
			System.out.println("Error!");
			e.printStackTrace();
		} finally {
			if (in != null) {
				in.close();
				in = null;
			}
			if (outp != null) {
				outp.close();
				outp = null;
			}
		}
	}

	/**
	 * 将文件上传到temp文件夹下wjf
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	public String upload(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String realFilePath = "";
		try {
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("file");
			String realFileName = "chkout.xls";
			String ctxPath = request.getSession().getServletContext().getRealPath("/") + "temp\\";
			String fileuploadPath = ctxPath;
			File dirPath = new File(fileuploadPath);
			if (!dirPath.exists()) {
				dirPath.mkdir();
			}
			realFilePath = fileuploadPath + realFileName;
			File uploadFile = new File(realFilePath);
			FileCopyUtils.copy(file.getBytes(), uploadFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return realFilePath;
	}

	/**
	 * 对execl进行验证
	 */
	public Object check(String path, String accountId) throws CRUDException {
		boolean checkResult = ReadChkoutExcel.check(accountId, path, supplyMapper, positnMapper, deliverMapper,
				codeDesMapper);
		List<String> errorList = new ArrayList<String>();
		if (checkResult) {
			Chkoutm chkoutm = ReadChkoutExcel.chkoutm;
			chkoutm.setAcct(accountId);
			List<Chkoutd> chkoutdList = ReadChkoutExcel.chkoutdList;
			chkoutm.setChkoutd(chkoutdList);
			return chkoutm;
		} else {
			errorList = ReadChkoutExcel.errorList;
		}
		FileWorked.deleteFile(path);// 删除上传后的的文件
		return errorList;
	}

	/***
	 * 查询仓位的对应供应商
	 * @param thePositn
	 * @return
	 */
	public Deliver findDeliverByPositn(Positn thePositn) {
		return inspectionMapper.findDeliverByPositn(thePositn);
	}

}