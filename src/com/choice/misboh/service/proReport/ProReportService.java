package com.choice.misboh.service.proReport;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.commonutil.MisUtil;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.commonutil.report.ReadConfig;
import com.choice.misboh.domain.BaseRecord.ActTyp;
import com.choice.misboh.domain.BaseRecord.Actm;
import com.choice.misboh.domain.BaseRecord.Payment;
import com.choice.misboh.domain.common.CommonMethod;
import com.choice.misboh.domain.reportMis.PublicEntity;
import com.choice.misboh.domain.reportMis.ReportObject;
import com.choice.misboh.persistence.BaseRecord.BaseRecordMapper;
import com.choice.misboh.persistence.common.CommonMISBOHMapper;
import com.choice.misboh.persistence.reportMis.MisBohNewReportMapper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * 描述：中餐报表-销售分析
 * @author 马振
 * 创建时间：2015-6-25 下午1:08:29
 */
@Service
public class ProReportService {

	@Autowired
	private ReportObject<Map<String,Object>> reportObject;
	
	@Autowired
	private PageManager<Map<String,Object>> pageManager;
	
	@Autowired
	private MisBohNewReportMapper newReportMapper;
	
	@Autowired
	private BaseRecordMapper baseRecordMapper;
	
	@Autowired
	private CommonMISBOHMapper commonMISBOHMapper;
	
	/**
	 * 描述:查询所有已启用的附加项
	 * 作者:马振
	 * 时间:2016年7月28日下午2:31:42
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public List<PublicEntity> findAllRedefine()  throws CRUDException {
		try{
			return newReportMapper.findAllRedefine();
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：菜品销售沽清表
	 * @author wangkai
	 * 创建时间：2016-2-19
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> listItemSaleGQ(PublicEntity condition)throws CRUDException{
		try{
			if(null != condition.getStatus() && "on".equals(condition.getStatus())){
				//如果勾选了核心菜
				condition.setStatus("Y");
			}else{
				condition.setStatus("");
			}
			Page pager = condition.getPager();
			reportObject.setRows(pageManager.selectPage(condition, pager, MisBohNewReportMapper.class.getName()+".listItemSaleGQ"));
			reportObject.setFooter(newReportMapper.listItemSaleGQSum(condition));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述:菜品销售明细报表——江边城外
	 * 作者:马振
	 * 时间:2016年7月11日下午4:14:24
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> queryJbcwPoscybb(PublicEntity condition)throws CRUDException{
		try{
			condition.setVpcode(FormatDouble.StringCodeReplace(condition.getVpcode()));
			condition.setPk_pubitem(FormatDouble.StringCodeReplace(condition.getPk_pubitem()));
			if(ValueUtil.IsNotEmpty(condition.getEnablestate())){//给启用状态添加‘’
				condition.setEnablestate(FormatDouble.StringCodeReplace(condition.getEnablestate()));
			}
			
			String typName = "";
			String cols = "NPRICE,NMONEY,NZMONEY,JAMT,JBNMONEY,JBNZMONEY";
			String colName = "MCNAME,VPNAME,NPRICE,VPCODE,NPRICE,SALESSPCODE,PK_PUBITEM,DWORKDATE,PK_STORE";//不合计的列
			Page pager = condition.getPager();
			List<Map<String, Object>> mapList = new ArrayList<Map<String,Object>>();
			Map<String, Object> mapxj = new HashMap<String, Object>();
			List<Map<String, Object>> list = pageManager.selectPage(condition, pager, MisBohNewReportMapper.class.getName() + ".queryPoscybb_new");
			
			for(int i = 0; i < list.size(); i++){
				Map<String, Object> map = list.get(i);
				
				for(Iterator<Map.Entry<String, Object>> it = map.entrySet().iterator(); it.hasNext();){
					Map.Entry<String, Object> entry = (Map.Entry<String, Object>)it.next();
					if (colName.indexOf(entry.getKey()) <= -1) {
						mapxj.put(entry.getKey(), null != mapxj.get(entry.getKey()) ? new BigDecimal((Double)mapxj.get(entry.getKey()) + ((BigDecimal)entry.getValue()).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue() : ((BigDecimal)entry.getValue()).doubleValue());
					} else if ("MCNAME".equals(entry.getKey())) {
						mapxj.put("MCNAME", "小计");
					}else if("NPRICE".equals(entry.getKey())) {
						mapxj.put("NPRICE", "");
					}
				} 
				if (null == map.get("MCNAME") || typName == map.get("MCNAME").toString() || typName.equals(map.get("MCNAME").toString())){
					map.put("MCNAME", "");
				} else {
					typName = map.get("MCNAME").toString();
				}
				mapList.add(map);
				if (i + 1 <list.size()) {
					Map<String, Object> map2 = list.get(i + 1);
					if (!typName.equals(map2.get("MCNAME"))) {
						mapList.add(mapxj);
						mapxj = new HashMap<String, Object>();
					}
				} else {
					mapList.add(mapxj);
				}
			}
			
			reportObject.setRows(MisUtil.formatNumForListResult(mapList, cols, 2));
			reportObject.setFooter(MisUtil.formatNumForListResult(newReportMapper.queryCalForPoscybb_new(condition), cols, 2));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch(Exception e) {
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述:鱼口味统计报表——江边城外
	 * 作者:马振
	 * 时间:2016年7月28日下午2:39:54
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> queryFishFlavorStatistical(PublicEntity condition)throws CRUDException{
		try{
			if (ValueUtil.IsNotEmpty(condition.getVpcode())) {
				condition.setVpcode(FormatDouble.StringCodeReplace(condition.getVpcode()));
			}
			
			Page pager = condition.getPager();
			StringBuffer sqlstr = new StringBuffer();
			
			List<PublicEntity> listPublicEntity = newReportMapper.findAllRedefine();
			for (int i = 0; i < listPublicEntity.size(); i++) {
				String vcode = listPublicEntity.get(i).getVcode();
				sqlstr.append(", SUM(CASE WHEN VFCODE = '" + vcode + "' THEN NCOUNTS ELSE 0 END) AS NFCOUNT" + vcode);
			}
			
			condition.setSqlstr(sqlstr.toString());
			reportObject.setRows(pageManager.selectPage(condition, pager, MisBohNewReportMapper.class.getName()+".queryFishFlavorStatistical"));
			reportObject.setFooter(newReportMapper.queryFishFlavorStatisticalSum(condition));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}

	/**
	 * 描述:翻台统计表
	 * 作者:马振
	 * 时间:2016年7月28日下午3:10:09
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> queryTableStatistics(PublicEntity condition)throws CRUDException{
		try{
			Page pager = condition.getPager();
			reportObject.setRows(pageManager.selectPage(condition, pager, MisBohNewReportMapper.class.getName()+".queryTableStatistics"));
			reportObject.setFooter(newReportMapper.queryTableStatisticsSum(condition));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述:结算方式统计表
	 * 作者:马振
	 * 时间:2016年7月29日下午12:48:28
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Object queryStatementAccounts(PublicEntity condition)  throws CRUDException {
		try {
			Page pager = condition.getPager();
			
			condition.setVcode(FormatDouble.StringCodeReplace(condition.getVcode()));
			reportObject.setRows(MisUtil.formatNumForListResult(pageManager.selectPage(condition, pager, MisBohNewReportMapper.class.getName() + ".queryStatementAccounts"), "SUMAMT", 2));
			reportObject.setFooter(MisUtil.formatNumForListResult(newReportMapper.queryStatementAccountsSum(condition), "SUMAMT", 2));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述:结算方式统计明细表
	 * 作者:马振
	 * 时间:2016年7月29日上午11:40:52
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Object queryStatementAccountsDetail(PublicEntity condition)  throws CRUDException {
		try {
			Page pager = condition.getPager();
			
			condition.setVcode(FormatDouble.StringCodeReplace(condition.getVcode()));
			reportObject.setRows(MisUtil.formatNumForListResult(pageManager.selectPage(condition, pager, MisBohNewReportMapper.class.getName() + ".queryStatementAccountsDetail"), "NMONEY", 2));
			reportObject.setFooter(MisUtil.formatNumForListResult(newReportMapper.queryStatementAccountsDetailSum(condition), "NMONEY", 2));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述:折扣分析表
	 * 作者:马振
	 * 时间:2016年7月29日下午3:40:30
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Object queryDiscountAnalysis(PublicEntity condition)  throws CRUDException {
		try {
			String cols = "NMONEY,NYMONEY,NZMONEY,ZSMONEY,ZBYS";
			
			StringBuffer sqlData = new StringBuffer();	//活动大类
			StringBuffer sqlDataS = new StringBuffer();	//支付类型
			StringBuffer sqlDataSS = new StringBuffer();	//活动明细
			StringBuffer sqlCol = new StringBuffer();
			StringBuffer sqlColSum = new StringBuffer();
			Map<String,Object> paramMap = new HashMap<String,Object>();
			Page pager = condition.getPager();
			
			//活动大类查询
			List<ActTyp> listActTyp = baseRecordMapper.findAllActTyp();
			
			//支付类型查询
			Payment pm = new Payment();
			pm.setVfoodsign("2");	//快餐报表查询
			pm.setEnablestate(2);
			List<Payment> listPayment = baseRecordMapper.findAllPayment(pm);
			
			//查询当前门店所有活动
			Actm a = new Actm();
			a.setPk_store(condition.getPk_store());
			List<Actm> listActm = baseRecordMapper.listActm(a);				
			
			List<PublicEntity> listActTypMin = baseRecordMapper.findAllActTypMin();	//活动小类
			List<Map<String,Object>> listPaymode = commonMISBOHMapper.listPaymode(new CommonMethod());	//支付方式
			StringBuffer sqlDataHdxl = new StringBuffer();//活动小类
			StringBuffer sqlDataZffs = new StringBuffer();//活动支付方式
			
			String sqlFlag = ReadConfig.getInstance().getValue("dataSource");
			if(sqlFlag.equals("sqlserver")){
				//活动设置类别
				for(ActTyp actTyp : listActTyp){
					sqlData.append(",SUM(CASE WHEN VCODE='").append(actTyp.getVcode()).append("' THEN ISNULL(NMONEY,0) ELSE 0 END) AS AMT").append(actTyp.getVcode());
					sqlCol.append(",ISNULL(AMT").append(actTyp.getVcode()).append(",0) AS AMT").append(actTyp.getVcode());
					sqlCol.append(",cast(((CASE WHEN MONEYTOTAL.NMONEY>0 THEN ROUND(ISNULL(AMT").append(actTyp.getVcode()).append(",0)/moneytotal.nmoney,4)*100 ELSE 0 END )) as VARCHAR(100) ) +'%' AS TYPZB").append(actTyp.getVcode());
					sqlColSum.append(",SUM(AMT").append(actTyp.getVcode()).append(") as AMT").append(actTyp.getVcode());
					cols += ",AMT"+actTyp.getVcode();
				}
				//支付类型
				for(Payment payment : listPayment){
					if("2".equals(payment.getIvalue())){
						sqlDataS.append(",SUM(case when VCODE='").append(payment.getVcode()).append("' then ISNULL(NMONEY-novercash,0) else 0 end) AS PAYMENT").append(payment.getVcode());
					}else{
						sqlDataS.append(",SUM(case when VCODE='").append(payment.getVcode()).append("' then ISNULL(NMONEY,0) else 0 end) AS PAYMENT").append(payment.getVcode());
					}
					sqlCol.append(",ISNULL(PAYMENT").append(payment.getVcode()).append(",0) as PAYMENT").append(payment.getVcode());
					sqlColSum.append(",SUM(PAYMENT").append(payment.getVcode()).append(") as PAYMENT").append(payment.getVcode());
					cols += ",PAYMENT"+payment.getVcode();
				}
				//活动设置
				for(Actm actm : listActm){
					sqlDataSS.append(",SUM(CASE WHEN VOPERATE='").append(actm.getVcode()).append("' THEN ISNULL(NMONEY,0) ELSE 0 END) AS ACTM").append(actm.getVcode());;
					sqlCol.append(",ISNULL(ACTM").append(actm.getVcode()).append(",0) as ACTM").append(actm.getVcode());
					sqlColSum.append(",SUM(ACTM").append(actm.getVcode()).append(") as ACTM").append(actm.getVcode());
					cols+=",ACTM"+actm.getVcode();
				}
			}else if(sqlFlag.equals("mysql")){
				//活动设置类别
				for(ActTyp actTyp : listActTyp){
					sqlData.append(",SUM(CASE WHEN VCODE='").append(actTyp.getVcode()).append("' THEN IFNULL(NMONEY,0) ELSE 0 END) AS AMT").append(actTyp.getVcode());
					sqlCol.append(",IFNULL(AMT").append(actTyp.getVcode()).append(",0) AS AMT").append(actTyp.getVcode());
					sqlCol.append(",CONCAT(FORMAT(CASE WHEN MONEYTOTAL.NMONEY>0 THEN ROUND(IFNULL(AMT").append(actTyp.getVcode()).append(",0)/moneytotal.nmoney,4) * 100 ELSE 0 END,2),'%') AS TYPZB").append(actTyp.getVcode());
					sqlColSum.append(",SUM(AMT").append(actTyp.getVcode()).append(") as AMT").append(actTyp.getVcode());
					cols += ",AMT"+actTyp.getVcode();
				}
				//支付类型
				for(Payment payment : listPayment){
					if("2".equals(payment.getIvalue())){
						sqlDataS.append(",SUM(case when VCODE='").append(payment.getVcode()).append("' then IFNULL(NMONEY-novercash,0) else 0 end) AS PAYMENT").append(payment.getVcode());
					}else{
						sqlDataS.append(",SUM(case when VCODE='").append(payment.getVcode()).append("' then IFNULL(NMONEY,0) else 0 end) AS PAYMENT").append(payment.getVcode());
					}
					sqlCol.append(",IFNULL(PAYMENT").append(payment.getVcode()).append(",0) as PAYMENT").append(payment.getVcode());
					sqlColSum.append(",SUM(PAYMENT").append(payment.getVcode()).append(") as PAYMENT").append(payment.getVcode());
					cols += ",PAYMENT"+payment.getVcode();
				}
				//活动设置
				for(Actm actm : listActm){
					sqlDataSS.append(",SUM(CASE WHEN VOPERATE='").append(actm.getVcode()).append("' THEN IFNULL(NMONEY,0) ELSE 0 END) AS ACTM").append(actm.getVcode());;
					sqlCol.append(",IFNULL(ACTM").append(actm.getVcode()).append(",0) as ACTM").append(actm.getVcode());
					sqlColSum.append(",SUM(ACTM").append(actm.getVcode()).append(") as ACTM").append(actm.getVcode());
					cols+=",ACTM"+actm.getVcode();
				}
			}else{
				//活动设置类别
				for(ActTyp actTyp : listActTyp){
					sqlData.append(",SUM(CASE WHEN VCODE='").append(actTyp.getVcode()).append("' THEN NVL(NMONEY,0) ELSE 0 END) AS AMT").append(actTyp.getVcode());
					sqlCol.append(",NVL(AMT").append(actTyp.getVcode()).append(",0) AS AMT").append(actTyp.getVcode());
					sqlCol.append(",TO_CHAR(CASE WHEN MONEYTOTAL.NMONEY>0 THEN ROUND(NVL(AMT").append(actTyp.getVcode()).append(",0)/moneytotal.nmoney,4)*100 ELSE 0 END,'9999999990.99') ||'%' AS TYPZB").append(actTyp.getVcode());
					sqlColSum.append(",SUM(AMT").append(actTyp.getVcode()).append(") as AMT").append(actTyp.getVcode());
					cols += ",AMT"+actTyp.getVcode();
				}
				//支付类型
				for(Payment payment : listPayment){
					if("2".equals(payment.getIvalue())){
						sqlDataS.append(",SUM(case when VCODE='").append(payment.getVcode()).append("' then NVL(NMONEY-novercash,0) else 0 end) AS PAYMENT").append(payment.getVcode());
					}else{
						sqlDataS.append(",SUM(case when VCODE='").append(payment.getVcode()).append("' then NVL(NMONEY,0) else 0 end) AS PAYMENT").append(payment.getVcode());
					}
					sqlCol.append(",NVL(PAYMENT").append(payment.getVcode()).append(",0) as PAYMENT").append(payment.getVcode());
					sqlColSum.append(",SUM(PAYMENT").append(payment.getVcode()).append(") as PAYMENT").append(payment.getVcode());
					cols += ",PAYMENT"+payment.getVcode();
				}
				//活动设置
				for(Actm actm : listActm){
					sqlDataSS.append(",SUM(CASE WHEN VOPERATE='").append(actm.getVcode()).append("' THEN NVL(NMONEY,0) ELSE 0 END) AS ACTM").append(actm.getVcode());;
					sqlCol.append(",NVL(ACTM").append(actm.getVcode()).append(",0) as ACTM").append(actm.getVcode());
					sqlColSum.append(",SUM(ACTM").append(actm.getVcode()).append(") as ACTM").append(actm.getVcode());
					cols+=",ACTM"+actm.getVcode();
				}
				//活动小类
				for(PublicEntity acttypmin : listActTypMin){
					sqlDataHdxl.append(",SUM(CASE WHEN VCODE='").append(acttypmin.getVcode()).append("' THEN NVL(NMONEY,0) ELSE 0 END) AS ACTTYPMIN").append(acttypmin.getVcode());
					sqlCol.append(",NVL(ACTTYPMIN").append(acttypmin.getVcode()).append(",0) AS ACTTYPMIN").append(acttypmin.getVcode());
					sqlColSum.append(",SUM(ACTTYPMIN").append(acttypmin.getVcode()).append(") as ACTTYPMIN").append(acttypmin.getVcode());
					cols += ",ACTTYPMIN"+acttypmin.getVcode();
				}
				
				for(Map<String,Object> map : listPaymode){
					if("2".equals(map.get("IVALUE"))){
						sqlDataZffs.append(",SUM(case when VCODE='").append(map.get("VCODE")).append("' then NVL(NMONEY-novercash,0) else 0 end) AS PAYMODE").append(map.get("VCODE"));
					}else{
						sqlDataZffs.append(",SUM(case when VCODE='").append(map.get("VCODE")).append("' then NVL(NMONEY,0) else 0 end) AS PAYMODE").append(map.get("VCODE"));
					}
					sqlCol.append(",NVL(PAYMODE").append(map.get("VCODE")).append(",0) as PAYMODE").append(map.get("VCODE"));
					sqlColSum.append(",SUM(PAYMODE").append(map.get("VCODE")).append(") as PAYMODE").append(map.get("VCODE"));
					cols += ",PAYMODE" + map.get("VCODE");
				}
			}
			paramMap.put("sqlData",sqlData.toString());
			paramMap.put("sqlDataS",sqlDataS.toString());
			paramMap.put("sqlDataSS",sqlDataSS.toString());
			paramMap.put("sqlDataHdxl",sqlDataHdxl.toString());
			paramMap.put("sqlDataZffs",sqlDataZffs.toString());
			paramMap.put("sqlCol",sqlCol);
			paramMap.put("sqlColSum",sqlColSum.toString());
			paramMap.put("con",condition);
			reportObject.setRows(MisUtil.formatNumForListResult(pageManager.selectPage(paramMap, pager, MisBohNewReportMapper.class.getName() + ".queryDiscountAnalysis"), cols, 2));
			reportObject.setFooter(MisUtil.formatNumForListResult(newReportMapper.queryDiscountAnalysisSum(paramMap), cols, 2));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
}