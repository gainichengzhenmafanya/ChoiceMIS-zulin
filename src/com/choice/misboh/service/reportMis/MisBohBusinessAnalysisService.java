package com.choice.misboh.service.reportMis;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.commonutil.MisUtil;
import com.choice.misboh.commonutil.ReadConfig;
import com.choice.misboh.commonutil.Total;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.domain.BaseRecord.ActTyp;
import com.choice.misboh.domain.BaseRecord.Actm;
import com.choice.misboh.domain.BaseRecord.Interval;
import com.choice.misboh.domain.BaseRecord.Payment;
import com.choice.misboh.domain.reportMis.PublicEntity;
import com.choice.misboh.domain.reportMis.ReportObject;
import com.choice.misboh.persistence.BaseRecord.BaseRecordMapper;
import com.choice.misboh.persistence.common.CommonMISBOHMapper;
import com.choice.misboh.persistence.reportMis.MisBohBusinessAnalysisMapper;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.ReadProperties;

/**
 * 描述：营业分析下的报表service
 * @author 马振
 * 创建时间：2015-4-16 上午11:04:50
 */
@Service
public class MisBohBusinessAnalysisService {

	private static Logger log = Logger.getLogger(MisBohBusinessAnalysisService.class);
	
	@Autowired
	private CommonMISBOHService commonMISBOHService; 
	
	@Autowired
	private ReportObject<Map<String,Object>> reportObject;
	
	@Autowired
	private MisBohBusinessAnalysisMapper businessAnalysisMapper;
	
	@Autowired
	private BaseRecordMapper baseRecordMapper;
	
	@Autowired
	private PageManager<Map<String,Object>> pageManager;
	
	@Autowired
	private CommonMISBOHMapper commonMISBOHMapper; 

	/**
	 * 描述：获取门店主键
	 * @author 马振
	 * 创建时间：2015-4-16 上午11:05:57
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public String getPkStore(HttpSession session) throws CRUDException{
		try{
			return commonMISBOHService.getPkStore(session).getPk_store();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：格式化list中的集合
	 * @author 马振
	 * 创建时间：2015-4-23 上午10:12:21
	 * @param listResult
	 * @param cols
	 * @param len
	 * @return
	 */
	public List<Map<String,Object>> formatNumForListResult(List<Map<String,Object>> listResult,String cols,int len){
		String num = "0";
		if(len>0){
			num +=".";
			for(int i=0;i<len;i++){
				num += "0";
			}
		}
		if(listResult != null && listResult.size()>0){
			String[] colsList = cols.split(",");
			for(Map<String,Object> map : listResult){
				//循环map并取里面的值
				for(String col : colsList){
					if(col.equals("IPEOLENUM") || col.equals("IPEOLENUMLJ")){//格式户人数和合计人数   去掉小数点
						map.put(col,null==map.get(col)?0:MisUtil.formatDoubleLength(map.get(col).toString(),0));
					}else{
						map.put(col,null==map.get(col)?num:MisUtil.formatDoubleLength(map.get(col).toString(),len));
					}
				}
			}
		}
		return listResult;
	}
	
	
	
	public Object queryXjrReport_new(PublicEntity condition)  throws CRUDException {
		try {
			StringBuffer sqlDataS = new StringBuffer();
			StringBuffer sqlCol = new StringBuffer();
			StringBuffer sqlColS = new StringBuffer();
			StringBuffer sqlColYq = new StringBuffer();
			Double totalMoney = 0.00,qxyyxj=0.00;
			Map<String,Object> paramMap = new HashMap<String,Object>();
			Payment paymentPar = new Payment();
			paymentPar.setEnablestate(2);
			paymentPar.setVfoodsign("2");//快餐报表查询标识
			List<Payment> listPayment = baseRecordMapper.findAllPayment(paymentPar);
			List<Map<String,Object>>  listResult = new ArrayList<Map<String,Object>>();
			String str = "";
			
			String sqlFlag=ReadConfig.getInstance().getValue("dataSource");
			if(sqlFlag.equals("sqlserver")){
				//支付方式
				for(Payment payment : listPayment){
					if(str.indexOf("s"+payment.getIvalue())==-1){//如果没有找到，往下执行
						str += ",s"+payment.getIvalue();
						sqlDataS.append(",SUM(case when T.IGROUPID="+payment.getIvalue()+" then T.NMONEY else 0 end) AS YS").append(payment.getIvalue());
						
						sqlCol.append(",ISNULL(SUM( case T.iGROUPID when "+payment.getIvalue()+" then T.NCASHAMT end),0) AS SS").append(payment.getIvalue());
						
						sqlColYq.append(",ISNULL(SS").append(payment.getIvalue()).append(",0)-ISNULL(YS").append(payment.getIvalue()).append(",0) AS YQ").append(payment.getIvalue());
						sqlColS.append(",ISNULL(SS").append(payment.getIvalue()).append(",0) AS SS"+payment.getIvalue()+",ISNULL(YS").append(payment.getIvalue()).append(",0) AS YS").append(payment.getIvalue());
					}
				}
			}else if(sqlFlag.equals("mysql")){
				//支付方式
				for(Payment payment : listPayment){
					if(str.indexOf("s"+payment.getIvalue())==-1){//如果没有找到，往下执行
						str += ",s"+payment.getIvalue();
						sqlDataS.append(",SUM(case when T.IGROUPID="+payment.getIvalue()+" then T.NMONEY else 0 end) AS YS").append(payment.getIvalue());
						
						sqlCol.append(",IFNULL(SUM( case T.iGROUPID when "+payment.getIvalue()+" then T.NCASHAMT end),0) AS SS").append(payment.getIvalue());
						
						sqlColYq.append(",IFNULL(SS").append(payment.getIvalue()).append(",0)-IFNULL(YS").append(payment.getIvalue()).append(",0) AS YQ").append(payment.getIvalue());
						sqlColS.append(",IFNULL(SS").append(payment.getIvalue()).append(",0) AS SS"+payment.getIvalue()+",IFNULL(YS").append(payment.getIvalue()).append(",0) AS YS").append(payment.getIvalue());
					}
				}
			}else{
				//支付方式
				for(Payment payment : listPayment){
					if(str.indexOf("s"+payment.getIvalue())==-1){//如果没有找到，往下执行
						str += ",s"+payment.getIvalue();
						sqlDataS.append(",SUM(case when T.IGROUPID="+payment.getIvalue()+"   then T.NMONEY else 0 end) AS YS").append(payment.getIvalue());
						sqlCol.append(",NVL(SUM( DECODE (T.iGROUPID,"+payment.getIvalue()+", T.NCASHAMT)),0) AS SS").append(payment.getIvalue());
						sqlColYq.append(",NVL(SS").append(payment.getIvalue()).append(",0)-NVL(YS").append(payment.getIvalue()).append(",0) AS YQ").append(payment.getIvalue());
						sqlColS.append(",NVL(SS").append(payment.getIvalue()).append(",0) AS SS"+payment.getIvalue()+",NVL(YS").append(payment.getIvalue()).append(",0) AS YS").append(payment.getIvalue());
					}
				}
			}
			
			paramMap.put("sqlDataS",sqlDataS.toString().substring(1));
			paramMap.put("sqlCol",sqlCol.toString().substring(1));
			paramMap.put("sqlColS",sqlColS.toString());
			paramMap.put("sqlColYq",sqlColYq.toString());
			paramMap.put("con",condition);
			List<Map<String,Object>> listXjrReport = businessAnalysisMapper.queryXjrReport_new(paramMap);
			if(listXjrReport != null && listXjrReport.size()>0){
				for(Map<String,Object> map : listXjrReport){
					int i = 0;
					Map<String,Object> mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VENGLISH","营业收入");
					mapResult.put("VCHINESE","营业收入");
					mapResult.put("NAMT",map.get("MONEY"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VENGLISH","+附加项金额");
					mapResult.put("VCHINESE","+附加项金额");
					mapResult.put("NAMT",map.get("FJMONEY"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VENGLISH","+服务费");
					mapResult.put("VCHINESE","+服务费");
					mapResult.put("NAMT",map.get("NSVR"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VENGLISH","主营业务收入");
					mapResult.put("VCHINESE","主营业务收入");
					mapResult.put("NAMT",map.get("TOTAL"));
					totalMoney = MisUtil.stringPlusDouble(0.00,map.get("TOTAL"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VENGLISH","-折扣金额");
					mapResult.put("VCHINESE","-折扣金额");
					mapResult.put("NAMT",map.get("ZMONEY"));
					totalMoney = MisUtil.stringPlusDouble(totalMoney,"-"+map.get("ZMONEY"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VENGLISH","-抹零金额");
					mapResult.put("VCHINESE","-抹零金额");
					mapResult.put("NAMT",map.get("NBZERO"));
					totalMoney = MisUtil.stringPlusDouble(totalMoney,"-"+map.get("NBZERO"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VENGLISH","含税收入");
					mapResult.put("VCHINESE","含税收入");
					mapResult.put("NAMT",totalMoney);
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VENGLISH","-门店挂账金额");
					mapResult.put("VCHINESE","-门店挂账金额");
					mapResult.put("NAMT",map.get("NCOUPONAAMT"));
					totalMoney = MisUtil.stringPlusDouble(totalMoney,"-"+map.get("NCOUPONAAMT"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VENGLISH","-集团挂账金额");
					mapResult.put("VCHINESE","-集团挂账金额");
					mapResult.put("NAMT",map.get("JTGZMONEY"));
					totalMoney = MisUtil.stringPlusDouble(totalMoney,"-"+map.get("JTGZMONEY"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VENGLISH","积分消费");
					mapResult.put("VCHINESE","积分消费");
					mapResult.put("NAMT",map.get("JFXF"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VENGLISH","-抵用券应收");
					mapResult.put("VCHINESE","-抵用券应收");
					mapResult.put("NAMT",map.get("DYQYS"));
					totalMoney = MisUtil.stringPlusDouble(totalMoney,"-"+map.get("DYQYS"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VENGLISH","+抵用券超收");
					mapResult.put("VCHINESE","+抵用券超收");
					mapResult.put("NAMT",map.get("DYQCS"));
					totalMoney = MisUtil.stringPlusDouble(totalMoney,map.get("DYQCS"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VENGLISH","-团购券实收");
					mapResult.put("VCHINESE","-团购券实收");
					mapResult.put("NAMT",map.get("TGQSS"));
					totalMoney = MisUtil.stringPlusDouble(totalMoney,"-"+map.get("TGQSS"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VENGLISH","团购券超收");
					mapResult.put("VCHINESE","团购券超收");
					mapResult.put("NAMT",map.get("TGQCS"));
//					totalMoney = MisUtil.stringPlusDouble(totalMoney,map.get("TGQCS"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VENGLISH","团购券应收");
					mapResult.put("VCHINESE","团购券应收");
					mapResult.put("NAMT",map.get("TGQYS"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VENGLISH","团购券优惠");
					mapResult.put("VCHINESE","团购券优惠");
					mapResult.put("NAMT",(null != map.get("TGQYS")?Double.parseDouble(map.get("TGQYS").toString()):0)-(null !=map.get("TGQSS")?Double.parseDouble(map.get("TGQSS").toString()):0));
					listResult.add(mapResult);
//					mapResult = new HashMap<String,Object>();
//					mapResult.put("VSEQUENCE",++i);
//					mapResult.put("VENGLISH","-会员卡储值消费");
//					mapResult.put("VCHINESE","-会员卡储值消费");
//					mapResult.put("NAMT",map.get("HYKCZXF"));
//					totalMoney = MisUtil.stringPlusDouble(totalMoney,"-"+map.get("HYKCZXF"));
//					listResult.add(mapResult);
//					mapResult = new HashMap<String,Object>();
//					mapResult.put("VSEQUENCE",++i);
//					mapResult.put("VENGLISH","+会员卡现金售卡");
//					mapResult.put("VCHINESE","+会员卡现金售卡");
//					mapResult.put("NAMT",map.get("HYKGBK"));
//					totalMoney = MisUtil.stringPlusDouble(totalMoney,map.get("HYKGBK"));
//					listResult.add(mapResult);
					//支付方式实收
					String strGl = "s1,s2,s5,s51,s55";
					for(Payment payment : listPayment){
						str = "";
						if(StringUtils.isNotBlank(payment.getIvalue())){
							if(str.indexOf(payment.getIvalue())==-1 && strGl.indexOf(payment.getIvalue())==-1 && !payment.getIvalue().equals("100")){//如果没有找到，往下执行
								str += ",s"+payment.getIvalue();
								mapResult = new HashMap<String,Object>();
								mapResult.put("VSEQUENCE",++i);
								mapResult.put("VENGLISH","-"+payment.getVname());
								mapResult.put("VCHINESE","-"+payment.getVname());
								mapResult.put("NAMT",map.get("YS"+payment.getIvalue()));
								listResult.add(mapResult);
								totalMoney = MisUtil.stringPlusDouble(totalMoney,"-"+map.get("YS"+payment.getIvalue()));
							}
						}
					}
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VENGLISH","+会员卡现金充值");
					mapResult.put("VCHINESE","+会员卡现金充值");
					mapResult.put("NAMT",map.get("HYKCZJE"));
					totalMoney = MisUtil.stringPlusDouble(totalMoney,map.get("HYKCZJE"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VENGLISH","-会员卡反充值金额");
					mapResult.put("VCHINESE","-会员卡反充值金额");
					mapResult.put("NAMT",map.get("HYKFCZJE"));
					totalMoney = MisUtil.stringPlusDouble(totalMoney,"-"+map.get("HYKFCZJE"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VENGLISH","=应收现金");
					mapResult.put("VCHINESE","=应收现金");
					mapResult.put("NAMT",totalMoney);//计算
					qxyyxj = totalMoney;
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VENGLISH","+备用金");
					mapResult.put("VCHINESE","+备用金");
					mapResult.put("NAMT",map.get("BYJ"));
					qxyyxj = MisUtil.stringPlusDouble(qxyyxj,map.get("BYJ"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VENGLISH","-抽大钞");
					mapResult.put("VCHINESE","-抽大钞");
					mapResult.put("NAMT",map.get("DRAWOFFAMT"));
					qxyyxj = MisUtil.stringPlusDouble(qxyyxj,"-"+map.get("DRAWOFFAMT"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VENGLISH","钱箱应有金额");
					mapResult.put("VCHINESE","钱箱应有金额");
					mapResult.put("NAMT",qxyyxj);
					listResult.add(mapResult);
					for(Payment payment : listPayment){
						str = "s2,s51";
						if(StringUtils.isNotBlank(payment.getIvalue())){
						if(payment.getIvalue().equals("5")){
							mapResult = new HashMap<String,Object>();
							mapResult.put("VSEQUENCE",++i);
							mapResult.put("VENGLISH","实收现金");
							mapResult.put("VCHINESE","实收现金");
							mapResult.put("NAMT",map.get("COUPONCAMT"));
							listResult.add(mapResult);
							mapResult = new HashMap<String,Object>();
							mapResult.put("VSEQUENCE",++i);
							mapResult.put("VENGLISH",payment.getVname()+"溢缺");
							mapResult.put("VCHINESE",payment.getVname()+"溢缺");
							mapResult.put("NAMT",MisUtil.stringPlusDouble(map.get("COUPONCAMT"),"-"+totalMoney));
							listResult.add(mapResult);
							mapResult = new HashMap<String,Object>();
							mapResult.put("VSEQUENCE",++i);
							mapResult.put("VENGLISH","实收券");
							mapResult.put("VCHINESE","实收券");
							mapResult.put("NAMT",map.get("COUPONBAMT"));
							listResult.add(mapResult);
							mapResult = new HashMap<String,Object>();
							mapResult.put("VSEQUENCE",++i);
							mapResult.put("VENGLISH","券溢缺");
							mapResult.put("VCHINESE","券溢缺");
							mapResult.put("NAMT",MisUtil.stringPlusDouble(map.get("COUPONBAMT"),"-"+map.get("DYQYS")));
							listResult.add(mapResult);
						}else if(str.indexOf(payment.getIvalue())==-1){//如果没有找到，往下执行
							str += ",s"+payment.getIvalue();
							mapResult = new HashMap<String,Object>();
							mapResult.put("VSEQUENCE",++i);
							mapResult.put("VENGLISH",payment.getVname()+"实收");
							mapResult.put("VCHINESE",payment.getVname()+"实收");
							mapResult.put("NAMT",map.get("SS"+payment.getIvalue()));
							listResult.add(mapResult);
							mapResult = new HashMap<String,Object>();
							mapResult.put("VSEQUENCE",++i);
							mapResult.put("VENGLISH",payment.getVname()+"溢缺");
							mapResult.put("VCHINESE",payment.getVname()+"溢缺");
							mapResult.put("NAMT",map.get("YQ"+payment.getIvalue()));
							listResult.add(mapResult);
						}
						}
					}
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VENGLISH","赔款金额");
					mapResult.put("VCHINESE","赔款金额");
					mapResult.put("NAMT",map.get("REPARATIONS"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VENGLISH","会员卡赠送金额");
					mapResult.put("VCHINESE","会员卡赠送金额");
					mapResult.put("NAMT",map.get("HYKZSJE"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VENGLISH","会员卡反充赠送金额");
					mapResult.put("VCHINESE","会员卡反充赠送金额");
					mapResult.put("NAMT",map.get("HYKFCZZSJE"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VENGLISH","反结算金额");
					mapResult.put("VCHINESE","反结算金额");
					mapResult.put("NAMT",map.get("FJSMONEY"));
					listResult.add(mapResult);
				}
			}
			reportObject.setRows(MisUtil.formatNumForListResult(listResult, "NAMT", 2));
			return reportObject;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	
	
	
	
	/**
	 * 描述：现金日报表
	 * @author 马振
	 * 创建时间：2015-4-30 上午10:10:55
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> queryXjrmx(PublicEntity condition) throws CRUDException{
		try {
			//为获取的门店主键、门店pos、是否测试门店加单引号
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			condition.setVistest(FormatDouble.StringCodeReplace(condition.getVistest()));
			condition.setDworkdate(FormatDouble.StringCodeReplace(condition.getDworkdate()));
			
			//创建查询语句对象
			StringBuffer sqlDataS = new StringBuffer();
			StringBuffer sqlCol = new StringBuffer();
			StringBuffer sqlColS = new StringBuffer();
			StringBuffer sqlColYq = new StringBuffer();
			
			Double totalMoney = 0.00;
			Double qxyyxj = 0.00;
			String str = "";
			
			Map<String,Object> paramMap = new HashMap<String,Object>();
			
			//创建支付类型对象存储查询条件
			Payment paymentPar = new Payment();
			paymentPar.setEnablestate(2);
			paymentPar.setVfoodsign("2");//快餐报表查询标识
			
			//获取支付类型
			List<Payment> listPayment = baseRecordMapper.findAllPayment(paymentPar);
			List<Map<String,Object>>  listResult = new ArrayList<Map<String,Object>>();
			
			//读取数据库类别，判断是sqlserver还是oracle
			String sqlFlag = ReadConfig.getInstance().getValue("dataSource");
			if(sqlFlag.equals("sqlserver")){
				//支付方式
				for(Payment payment : listPayment){
					if(str.indexOf("s"+payment.getIvalue())==-1){//如果没有找到，往下执行
						str += ",s"+payment.getIvalue();
						sqlDataS.append(",SUM(case when T.IGROUPID="+payment.getIvalue()+" then T.NMONEY else 0 end) AS YS").append(payment.getIvalue());
						
						sqlCol.append(",ISNULL(SUM( case T.iGROUPID when "+payment.getIvalue()+" then T.NCASHAMT end),0) AS SS").append(payment.getIvalue());
						
						sqlColYq.append(",ISNULL(SS").append(payment.getIvalue()).append(",0)-ISNULL(YS").append(payment.getIvalue()).append(",0) AS YQ").append(payment.getIvalue());
						sqlColS.append(",ISNULL(SS").append(payment.getIvalue()).append(",0) AS SS"+payment.getIvalue()+",ISNULL(YS").append(payment.getIvalue()).append(",0) AS YS").append(payment.getIvalue());
					}
				}
			} else if ("mysql".equals(sqlFlag)) { //数据库选择mysql时
				//支付方式
				for(Payment payment : listPayment){
					if(str.indexOf("s" + payment.getIvalue()) == -1){//如果没有找到，往下执行
						str += ",s"+payment.getIvalue();
						sqlDataS.append(",SUM(case when T.IGROUPID="+payment.getIvalue()+"   then T.NMONEY else 0 end) AS YS").append(payment.getIvalue());
						sqlCol.append(",IFNULL(SUM(case when T.iGROUPID="+payment.getIvalue()+" then T.NCASHAMT else 0 end),0) AS SS").append(payment.getIvalue());
						sqlColYq.append(",IFNULL(SS").append(payment.getIvalue()).append(",0)-IFNULL(YS").append(payment.getIvalue()).append(",0) AS YQ").append(payment.getIvalue());
						sqlColS.append(",IFNULL(SS").append(payment.getIvalue()).append(",0) AS SS"+payment.getIvalue()+",IFNULL(YS").append(payment.getIvalue()).append(",0) AS YS").append(payment.getIvalue());
					}
				}
			} else {
				//支付方式
				for(Payment payment : listPayment){
					if(str.indexOf("s" + payment.getIvalue()) == -1){//如果没有找到，往下执行
						str += ",s"+payment.getIvalue();
						sqlDataS.append(",SUM(case when T.IGROUPID="+payment.getIvalue()+"   then T.NMONEY else 0 end) AS YS").append(payment.getIvalue());
						sqlCol.append(",NVL(SUM(case when T.iGROUPID="+payment.getIvalue()+" then T.NCASHAMT else 0 end),0) AS SS").append(payment.getIvalue());
						sqlColYq.append(",NVL(SS").append(payment.getIvalue()).append(",0)-NVL(YS").append(payment.getIvalue()).append(",0) AS YQ").append(payment.getIvalue());
						sqlColS.append(",NVL(SS").append(payment.getIvalue()).append(",0) AS SS"+payment.getIvalue()+",NVL(YS").append(payment.getIvalue()).append(",0) AS YS").append(payment.getIvalue());
					}
				}
			}
			
			paramMap.put("sqlDataS",sqlDataS.toString().substring(1));
			paramMap.put("sqlCol",sqlCol.toString().substring(1));
			paramMap.put("sqlColS",sqlColS.toString());
			paramMap.put("sqlColYq",sqlColYq.toString());
			paramMap.put("con",condition);
			List<Map<String, Object>> listXjrReport = businessAnalysisMapper.queryXjrReport(paramMap);
			if(listXjrReport != null && listXjrReport.size()>0){
				for(Map<String,Object> map : listXjrReport){
					int i = 0;
					Map<String,Object> mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","销售额");
					mapResult.put("NAMT",map.get("MONEY"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","+附加项金额");
					mapResult.put("NAMT",map.get("FJMONEY"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","+服务费");
					mapResult.put("NAMT",map.get("NSVR"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","营业总收入");
					mapResult.put("NAMT",map.get("TOTAL"));
					totalMoney = MisUtil.stringPlusDouble(0.00,map.get("TOTAL"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","-折扣金额");
					mapResult.put("NAMT",map.get("ZMONEY"));
					totalMoney = MisUtil.stringPlusDouble(totalMoney,"-"+map.get("ZMONEY"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","-抹零金额");
					mapResult.put("NAMT",map.get("NBZERO"));
					totalMoney = MisUtil.stringPlusDouble(totalMoney,"-"+map.get("NBZERO"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","=含税收入");
					mapResult.put("NAMT",map.get("NHSMONEY"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","-挂账金额");
					mapResult.put("NAMT",map.get("NCOUPONAAMT"));
					totalMoney = MisUtil.stringPlusDouble(totalMoney,"-"+map.get("NCOUPONAAMT"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","积分消费");
					mapResult.put("NAMT",map.get("JFXF"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","-抵用券应收");
					mapResult.put("NAMT",map.get("DYQYS"));
					totalMoney = MisUtil.stringPlusDouble(totalMoney,"-"+map.get("DYQYS"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","+抵用券超收");
					mapResult.put("NAMT",map.get("DYQCS"));
					totalMoney = MisUtil.stringPlusDouble(totalMoney,map.get("DYQCS"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","-团购券实收");
					mapResult.put("NAMT",map.get("TGQSS"));
					totalMoney = MisUtil.stringPlusDouble(totalMoney,"-"+map.get("TGQSS"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","+团购券超收");
					mapResult.put("NAMT",map.get("TGQCS"));
					totalMoney = MisUtil.stringPlusDouble(totalMoney,map.get("TGQCS"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","团购券应收");
					mapResult.put("NAMT",map.get("TGQYS"));
					listResult.add(mapResult);
//					mapResult = new HashMap<String,Object>();
//					mapResult.put("VSEQUENCE",++i);
//					mapResult.put("VCHINESE","-会员卡储值消费");
//					mapResult.put("NAMT",map.get("HYKCZXF"));
//					totalMoney = MisUtil.stringPlusDouble(totalMoney,"-"+map.get("HYKCZXF"));
//					listResult.add(mapResult);
					//支付方式实收
					String strGl = "s1,s2,s5,s51,s55";
					for(Payment payment : listPayment){
						str = "";
						if(StringUtils.isNotBlank(payment.getIvalue())){
							if(str.indexOf(payment.getIvalue())==-1 && strGl.indexOf(payment.getIvalue())==-1 && !payment.getIvalue().equals("100")){//如果没有找到，往下执行
								str += ",s"+payment.getIvalue();
								mapResult = new HashMap<String,Object>();
								mapResult.put("VSEQUENCE",++i);
								mapResult.put("VCHINESE","-"+payment.getVname());
								mapResult.put("NAMT",map.get("YS"+payment.getIvalue()));
								listResult.add(mapResult);
								totalMoney = MisUtil.stringPlusDouble(totalMoney,"-"+map.get("YS"+payment.getIvalue()));
							}
						}
					}
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","+会员卡现金售卡");
					mapResult.put("NAMT",map.get("HYKGBK"));
					totalMoney = MisUtil.stringPlusDouble(totalMoney,map.get("HYKGBK"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","+会员卡现金充值");
					mapResult.put("NAMT",map.get("HYKCZJE"));
					totalMoney = MisUtil.stringPlusDouble(totalMoney,map.get("HYKCZJE"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","=应收现金");
					mapResult.put("NAMT",totalMoney);//计算
					qxyyxj = totalMoney;
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","+备用金");
					mapResult.put("NAMT",map.get("BYJ"));
					qxyyxj = MisUtil.stringPlusDouble(qxyyxj,map.get("BYJ"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","-抽大钞");
					mapResult.put("NAMT",map.get("DRAWOFFAMT"));
					qxyyxj = MisUtil.stringPlusDouble(qxyyxj,"-"+map.get("DRAWOFFAMT"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","钱箱应有金额");
					mapResult.put("NAMT",qxyyxj);
					listResult.add(mapResult);
					for(Payment payment : listPayment){
						str = "s2,s51";
						if(StringUtils.isNotBlank(payment.getIvalue())){
						if(payment.getIvalue().equals("5")){
							mapResult = new HashMap<String,Object>();
							mapResult.put("VSEQUENCE",++i);
							mapResult.put("VCHINESE","实收现金");
							mapResult.put("NAMT",map.get("COUPONCAMT"));
							listResult.add(mapResult);
							mapResult = new HashMap<String,Object>();
							mapResult.put("VSEQUENCE",++i);
							mapResult.put("VCHINESE",payment.getVname()+"溢缺");
							mapResult.put("NAMT",MisUtil.stringPlusDouble(map.get("COUPONCAMT"),"-"+totalMoney));
							listResult.add(mapResult);
							mapResult = new HashMap<String,Object>();
							mapResult.put("VSEQUENCE",++i);
							mapResult.put("VCHINESE","实收券");
							mapResult.put("NAMT",map.get("COUPONBAMT"));
							listResult.add(mapResult);
							mapResult = new HashMap<String,Object>();
							mapResult.put("VSEQUENCE",++i);
							mapResult.put("VCHINESE","券溢缺");
							mapResult.put("NAMT",MisUtil.stringPlusDouble(map.get("COUPONBAMT"),"-"+map.get("DYQYS")));
							listResult.add(mapResult);
						}else if(str.indexOf(payment.getIvalue())==-1){//如果没有找到，往下执行
							str += ",s"+payment.getIvalue();
							mapResult = new HashMap<String,Object>();
							mapResult.put("VSEQUENCE",++i);
							mapResult.put("VCHINESE",payment.getVname()+"实收");
							mapResult.put("NAMT",map.get("SS"+payment.getIvalue()));
							listResult.add(mapResult);
							mapResult = new HashMap<String,Object>();
							mapResult.put("VSEQUENCE",++i);
							mapResult.put("VCHINESE",payment.getVname()+"溢缺");
							mapResult.put("NAMT",map.get("YQ"+payment.getIvalue()));
							listResult.add(mapResult);
						}
						}
					}
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","赔款金额");
					mapResult.put("NAMT",map.get("REPARATIONS"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","会员卡赠送金额");
					mapResult.put("NAMT",map.get("HYKZSJE"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","会员卡反充值金额");
					mapResult.put("NAMT",map.get("HYKFCZJE"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","反结算金额");
					mapResult.put("NAMT",map.get("FJSMONEY"));
					listResult.add(mapResult);
				}
			}
			return MisUtil.formatNumForListResult(listResult, "NAMT", 2);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：现金日报表(导出Excel表格专用)
	 * @author 马振
	 * 创建时间：2015-8-19 上午10:34:44
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Object queryXjrReport(PublicEntity condition) throws CRUDException{
		try {
			//为获取的门店主键、门店pos、是否测试门店加单引号
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			condition.setVistest(FormatDouble.StringCodeReplace(condition.getVistest()));
			condition.setDworkdate(FormatDouble.StringCodeReplace(condition.getDworkdate()));
			
			//创建查询语句对象
			StringBuffer sqlDataS = new StringBuffer();
			StringBuffer sqlCol = new StringBuffer();
			StringBuffer sqlColS = new StringBuffer();
			StringBuffer sqlColYq = new StringBuffer();
			
			Double totalMoney = 0.00;
			Double qxyyxj = 0.00;
			String str = "";
			
			Map<String,Object> paramMap = new HashMap<String,Object>();
			
			//创建支付类型对象存储查询条件
			Payment paymentPar = new Payment();
			paymentPar.setEnablestate(2);
			paymentPar.setVfoodsign("2");//快餐报表查询标识
			
			//获取支付类型
			List<Payment> listPayment = baseRecordMapper.findAllPayment(paymentPar);
			List<Map<String,Object>>  listResult = new ArrayList<Map<String,Object>>();
			
			//读取数据库类别，判断是sqlserver还是oracle
			String sqlFlag = ReadConfig.getInstance().getValue("dataSource");
			if("sqlserver".equals(sqlFlag)){
				//支付方式
				for(Payment payment : listPayment){
					if(str.indexOf("s"+payment.getIvalue())==-1){//如果没有找到，往下执行
						str += ",s"+payment.getIvalue();
						sqlDataS.append(",SUM(case when T.IGROUPID="+payment.getIvalue()+" then T.NMONEY else 0 end) AS YS").append(payment.getIvalue());
						
						sqlCol.append(",ISNULL(SUM( case T.iGROUPID when "+payment.getIvalue()+" then T.NCASHAMT end),0) AS SS").append(payment.getIvalue());
						
						sqlColYq.append(",ISNULL(SS").append(payment.getIvalue()).append(",0)-ISNULL(YS").append(payment.getIvalue()).append(",0) AS YQ").append(payment.getIvalue());
						sqlColS.append(",ISNULL(SS").append(payment.getIvalue()).append(",0) AS SS"+payment.getIvalue()+",ISNULL(YS").append(payment.getIvalue()).append(",0) AS YS").append(payment.getIvalue());
					}
				}
			} else if ("mysql".equals(sqlFlag)) {
				//支付方式
				for(Payment payment : listPayment){
					if(str.indexOf("s" + payment.getIvalue()) == -1){//如果没有找到，往下执行
						str += ",s"+payment.getIvalue();
						sqlDataS.append(",SUM(case when T.IGROUPID="+payment.getIvalue()+" then T.NMONEY else 0 end) AS YS").append(payment.getIvalue());
						sqlCol.append(",IFNULL(SUM(case when T.iGROUPID="+payment.getIvalue()+" then T.NCASHAMT else 0 end),0) AS SS").append(payment.getIvalue());
						sqlColYq.append(",IFNULL(SS").append(payment.getIvalue()).append(",0)-IFNULL(YS").append(payment.getIvalue()).append(",0) AS YQ").append(payment.getIvalue());
						sqlColS.append(",IFNULL(SS").append(payment.getIvalue()).append(",0) AS SS"+payment.getIvalue()+",IFNULL(YS").append(payment.getIvalue()).append(",0) AS YS").append(payment.getIvalue());
					}
				}
			} else {
				//支付方式
				for(Payment payment : listPayment){
					if(str.indexOf("s" + payment.getIvalue()) == -1){//如果没有找到，往下执行
						str += ",s"+payment.getIvalue();
						sqlDataS.append(",SUM(case when T.IGROUPID="+payment.getIvalue()+" then T.NMONEY else 0 end) AS YS").append(payment.getIvalue());
						sqlCol.append(",NVL(SUM(case when T.iGROUPID="+payment.getIvalue()+" then T.NCASHAMT else 0 end),0) AS SS").append(payment.getIvalue());
						sqlColYq.append(",NVL(SS").append(payment.getIvalue()).append(",0)-NVL(YS").append(payment.getIvalue()).append(",0) AS YQ").append(payment.getIvalue());
						sqlColS.append(",NVL(SS").append(payment.getIvalue()).append(",0) AS SS"+payment.getIvalue()+",NVL(YS").append(payment.getIvalue()).append(",0) AS YS").append(payment.getIvalue());
					}
				}
			}
			
			paramMap.put("sqlDataS",sqlDataS.toString().substring(1));
			paramMap.put("sqlCol",sqlCol.toString().substring(1));
			paramMap.put("sqlColS",sqlColS.toString());
			paramMap.put("sqlColYq",sqlColYq.toString());
			paramMap.put("con",condition);
			List<Map<String, Object>> listXjrReport = businessAnalysisMapper.queryXjrReport(paramMap);
			if(listXjrReport != null && listXjrReport.size()>0){
				for(Map<String,Object> map : listXjrReport){
					int i = 0;
					Map<String,Object> mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","销售额");
					mapResult.put("NAMT",map.get("MONEY"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","+附加项金额");
					mapResult.put("NAMT",map.get("FJMONEY"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","+服务费");
					mapResult.put("NAMT",map.get("NSVR"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","营业总收入");
					mapResult.put("NAMT",map.get("TOTAL"));
					totalMoney = MisUtil.stringPlusDouble(0.00,map.get("TOTAL"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","-折扣金额");
					mapResult.put("NAMT",map.get("ZMONEY"));
					totalMoney = MisUtil.stringPlusDouble(totalMoney,"-"+map.get("ZMONEY"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","-抹零金额");
					mapResult.put("NAMT",map.get("NBZERO"));
					totalMoney = MisUtil.stringPlusDouble(totalMoney,"-"+map.get("NBZERO"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","=含税收入");
					mapResult.put("NAMT",map.get("NHSMONEY"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","-挂账金额");
					mapResult.put("NAMT",map.get("NCOUPONAAMT"));
					totalMoney = MisUtil.stringPlusDouble(totalMoney,"-"+map.get("NCOUPONAAMT"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","积分消费");
					mapResult.put("NAMT",map.get("JFXF"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","-抵用券应收");
					mapResult.put("NAMT",map.get("DYQYS"));
					totalMoney = MisUtil.stringPlusDouble(totalMoney,"-"+map.get("DYQYS"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","+抵用券超收");
					mapResult.put("NAMT",map.get("DYQCS"));
					totalMoney = MisUtil.stringPlusDouble(totalMoney,map.get("DYQCS"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","-团购券实收");
					mapResult.put("NAMT",map.get("TGQSS"));
					totalMoney = MisUtil.stringPlusDouble(totalMoney,"-"+map.get("TGQSS"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","+团购券超收");
					mapResult.put("NAMT",map.get("TGQCS"));
					totalMoney = MisUtil.stringPlusDouble(totalMoney,map.get("TGQCS"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","团购券应收");
					mapResult.put("NAMT",map.get("TGQYS"));
					listResult.add(mapResult);
//					mapResult = new HashMap<String,Object>();
//					mapResult.put("VSEQUENCE",++i);
//					mapResult.put("VCHINESE","-会员卡储值消费");
//					mapResult.put("NAMT",map.get("HYKCZXF"));
//					totalMoney = MisUtil.stringPlusDouble(totalMoney,"-"+map.get("HYKCZXF"));
//					listResult.add(mapResult);
					//支付方式实收
					String strGl = "s1,s2,s5,s51,s55";
					for(Payment payment : listPayment){
						str = "";
						if(StringUtils.isNotBlank(payment.getIvalue())){
							if(str.indexOf(payment.getIvalue())==-1 && strGl.indexOf(payment.getIvalue())==-1 && !payment.getIvalue().equals("100")){//如果没有找到，往下执行
								str += ",s"+payment.getIvalue();
								mapResult = new HashMap<String,Object>();
								mapResult.put("VSEQUENCE",++i);
								mapResult.put("VCHINESE","-"+payment.getVname());
								mapResult.put("NAMT",map.get("YS"+payment.getIvalue()));
								listResult.add(mapResult);
								totalMoney = MisUtil.stringPlusDouble(totalMoney,"-"+map.get("YS"+payment.getIvalue()));
							}
						}
					}
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","+会员卡现金售卡");
					mapResult.put("NAMT",map.get("HYKGBK"));
					totalMoney = MisUtil.stringPlusDouble(totalMoney,map.get("HYKGBK"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","+会员卡现金充值");
					mapResult.put("NAMT",map.get("HYKCZJE"));
					totalMoney = MisUtil.stringPlusDouble(totalMoney,map.get("HYKCZJE"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","=应收现金");
					mapResult.put("NAMT",totalMoney);//计算
					qxyyxj = totalMoney;
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","+备用金");
					mapResult.put("NAMT",map.get("BYJ"));
					qxyyxj = MisUtil.stringPlusDouble(qxyyxj,map.get("BYJ"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","-抽大钞");
					mapResult.put("NAMT",map.get("DRAWOFFAMT"));
					qxyyxj = MisUtil.stringPlusDouble(qxyyxj,"-"+map.get("DRAWOFFAMT"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","钱箱应有金额");
					mapResult.put("NAMT",qxyyxj);
					listResult.add(mapResult);
					for(Payment payment : listPayment){
						str = "s2,s51";
						if(StringUtils.isNotBlank(payment.getIvalue())){
						if(payment.getIvalue().equals("5")){
							mapResult = new HashMap<String,Object>();
							mapResult.put("VSEQUENCE",++i);
							mapResult.put("VCHINESE","实收现金");
							mapResult.put("NAMT",map.get("COUPONCAMT"));
							listResult.add(mapResult);
							mapResult = new HashMap<String,Object>();
							mapResult.put("VSEQUENCE",++i);
							mapResult.put("VCHINESE",payment.getVname()+"溢缺");
							mapResult.put("NAMT",MisUtil.stringPlusDouble(map.get("COUPONCAMT"),"-"+totalMoney));
							listResult.add(mapResult);
							mapResult = new HashMap<String,Object>();
							mapResult.put("VSEQUENCE",++i);
							mapResult.put("VCHINESE","实收券");
							mapResult.put("NAMT",map.get("COUPONBAMT"));
							listResult.add(mapResult);
							mapResult = new HashMap<String,Object>();
							mapResult.put("VSEQUENCE",++i);
							mapResult.put("VCHINESE","券溢缺");
							mapResult.put("NAMT",MisUtil.stringPlusDouble(map.get("COUPONBAMT"),"-"+map.get("DYQYS")));
							listResult.add(mapResult);
						}else if(str.indexOf(payment.getIvalue())==-1){//如果没有找到，往下执行
							str += ",s"+payment.getIvalue();
							mapResult = new HashMap<String,Object>();
							mapResult.put("VSEQUENCE",++i);
							mapResult.put("VCHINESE",payment.getVname()+"实收");
							mapResult.put("NAMT",map.get("SS"+payment.getIvalue()));
							listResult.add(mapResult);
							mapResult = new HashMap<String,Object>();
							mapResult.put("VSEQUENCE",++i);
							mapResult.put("VCHINESE",payment.getVname()+"溢缺");
							mapResult.put("NAMT",map.get("YQ"+payment.getIvalue()));
							listResult.add(mapResult);
						}
						}
					}
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","赔款金额");
					mapResult.put("NAMT",map.get("REPARATIONS"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","会员卡赠送金额");
					mapResult.put("NAMT",map.get("HYKZSJE"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","会员卡反充值金额");
					mapResult.put("NAMT",map.get("HYKFCZJE"));
					listResult.add(mapResult);
					mapResult = new HashMap<String,Object>();
					mapResult.put("VSEQUENCE",++i);
					mapResult.put("VCHINESE","反结算金额");
					mapResult.put("NAMT",map.get("FJSMONEY"));
					listResult.add(mapResult);
				}
			}
			
			reportObject.setRows(MisUtil.formatNumForListResult(listResult, "NAMT", 2));
			
			return reportObject;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	
	/**
	 * 描述：存款查询
	 * @author 马振
	 * 创建时间：2015-4-30 上午10:10:31
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public List<PublicEntity> findCash(PublicEntity condition) throws CRUDException {
		try {
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			List<PublicEntity> listPublicEntity = businessAnalysisMapper.findCashSaving(condition);
			return listPublicEntity;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：存款查询合计
	 * @author 马振
	 * 创建时间：2015-4-30 上午10:09:33
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public PublicEntity findCountCashSaving(PublicEntity condition) throws CRUDException {
		try {
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			PublicEntity publicEntity = businessAnalysisMapper.findCountCashSaving(condition);
			return publicEntity;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：账单查询
	 * @author 马振
	 * 创建时间：2015-4-22 上午10:44:11
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Object queryFolioDetailInfo(PublicEntity condition) throws CRUDException {
		try {
			Page pager = condition.getPager();
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			
			StringBuffer sqlData = new StringBuffer();
			StringBuffer sqlDataS = new StringBuffer();
			StringBuffer sqlColGroup = new StringBuffer();
			StringBuffer sqlCol = new StringBuffer();
			StringBuffer sqlColSum = new StringBuffer();
			StringBuffer sqlCoAtpPay = new StringBuffer();
			Map<String,Object> paramMap = new HashMap<String,Object>();
			String formatCol = "NMONEY,NBZERO,NZMONEY,ZSMONEY,NYMONEY,NSVR,NMONEYV,JDMONEY,JDMONEY,JFMONEY,CARDMONEY,AVGAMT,AVGAMT";
			List<ActTyp> listActTyp = baseRecordMapper.findAllActTyp();
			Payment pm = new Payment();
			pm.setVfoodsign("2");//快餐报表查询
			List<Payment> listPayment = baseRecordMapper.findAllPayment(pm);
			
			//读取配置文件  查看数据源
			ReadProperties rp = new ReadProperties();
			String dataSource = rp.getStrByParam("dataSource");
			if(null == dataSource || "".equals(dataSource) || "oracle".equals(dataSource)){
				for(ActTyp actTyp : listActTyp){
					sqlData.append(",SUM(CASE WHEN VCODE='").append(actTyp.getVcode()).append("'THEN NVL(AMT,0) ELSE 0 END) AS AMT").append(actTyp.getVcode());
					sqlColGroup.append(",AMT").append(actTyp.getVcode());
					sqlCol.append(",NVL(AMT").append(actTyp.getVcode()).append(",0) as AMT").append(actTyp.getVcode());
					sqlColSum.append(",SUM(AMT").append(actTyp.getVcode()).append(") as AMT").append(actTyp.getVcode());
					formatCol+=",AMT"+actTyp.getVcode();
					sqlCoAtpPay.append(", NVL(AMT"+actTyp.getVcode()+",0) AS AMT").append(actTyp.getVcode());
				}
				for(Payment payment : listPayment){
					if("51".equals(payment.getIvalue()) || "2".equals(payment.getIvalue())){
						sqlDataS.append(",SUM(case when VCODE='").append(payment.getVcode()).append("' then NVL(NMONEY,0) else 0 end) AS PAYMENT").append(payment.getVcode());
					}else{
						sqlDataS.append(",SUM(case when VCODE='").append(payment.getVcode()).append("' then NVL(NMONEY,0)  else 0 end) AS PAYMENT").append(payment.getVcode());
					}
					sqlColGroup.append(",PAYMENT").append(payment.getVcode());
					sqlCol.append(",NVL(PAYMENT").append(payment.getVcode()).append(",0) as PAYMENT").append(payment.getVcode());
					sqlColSum.append(",SUM(PAYMENT").append(payment.getVcode()).append(") as PAYMENT").append(payment.getVcode());
					formatCol+=",PAYMENT"+payment.getVcode();
					sqlCoAtpPay.append(", NVL(PAYMENT"+payment.getVcode()+",0) AS PAYMENT").append(payment.getVcode());
				}
			} else if ("sqlserver".equals(dataSource)){
				for(ActTyp actTyp : listActTyp){
					sqlData.append(",SUM((CASE WHEN VCODE ='").append(actTyp.getVcode()).append("' THEN ISNULL(AMT,0) END)) AS AMT").append(actTyp.getVcode());
					sqlColGroup.append(",AMT").append(actTyp.getVcode());
					sqlCol.append(",ISNULL(AMT").append(actTyp.getVcode()).append(",0) as AMT").append(actTyp.getVcode());
					sqlColSum.append(",SUM(AMT").append(actTyp.getVcode()).append(") as AMT").append(actTyp.getVcode());
					formatCol+=",AMT"+actTyp.getVcode();
					sqlCoAtpPay.append(", ISNULL(AMT"+actTyp.getVcode()+",0) AS AMT").append(actTyp.getVcode());
				}
				for(Payment payment : listPayment){
					if("51".equals(payment.getIvalue()) || "2".equals(payment.getIvalue())){
						sqlDataS.append(",SUM(case when VCODE='").append(payment.getVcode()).append("' then ISNULL(NMONEY,0) else 0 end) AS PAYMENT").append(payment.getVcode());
					}else{
						sqlDataS.append(",SUM(case when VCODE='").append(payment.getVcode()).append("' then ISNULL(NMONEY,0)  else 0 end) AS PAYMENT").append(payment.getVcode());
					}
					sqlColGroup.append(",PAYMENT").append(payment.getVcode());
					sqlCol.append(",ISNULL(PAYMENT").append(payment.getVcode()).append(",0) as PAYMENT").append(payment.getVcode());
					sqlColSum.append(",SUM(PAYMENT").append(payment.getVcode()).append(") as PAYMENT").append(payment.getVcode());
					formatCol+=",PAYMENT"+payment.getVcode();
					sqlCoAtpPay.append(", ISNULL(PAYMENT"+payment.getVcode()+",0) AS PAYMENT").append(payment.getVcode());
				}
			} else if ("mysql".equals(dataSource)) {
				for(ActTyp actTyp : listActTyp){
					sqlData.append(",SUM(CASE WHEN VCODE='").append(actTyp.getVcode()).append("'THEN IFNULL(AMT,0) ELSE 0 END) AS AMT").append(actTyp.getVcode());
					sqlColGroup.append(",AMT").append(actTyp.getVcode());
					sqlCol.append(",IFNULL(AMT").append(actTyp.getVcode()).append(",0) as AMT").append(actTyp.getVcode());
					sqlColSum.append(",SUM(AMT").append(actTyp.getVcode()).append(") as AMT").append(actTyp.getVcode());
					formatCol+=",AMT"+actTyp.getVcode();
					sqlCoAtpPay.append(", IFNULL(AMT"+actTyp.getVcode()+",0) AS AMT").append(actTyp.getVcode());
				}
				for(Payment payment : listPayment){
					if("51".equals(payment.getIvalue()) || "2".equals(payment.getIvalue())){
						sqlDataS.append(",SUM(case when VCODE='").append(payment.getVcode()).append("' then IFNULL(NMONEY,0) else 0 end) AS PAYMENT").append(payment.getVcode());
					}else{
						sqlDataS.append(",SUM(case when VCODE='").append(payment.getVcode()).append("' then IFNULL(NMONEY,0)  else 0 end) AS PAYMENT").append(payment.getVcode());
					}
					sqlColGroup.append(",PAYMENT").append(payment.getVcode());
					sqlCol.append(",IFNULL(PAYMENT").append(payment.getVcode()).append(",0) as PAYMENT").append(payment.getVcode());
					sqlColSum.append(",SUM(PAYMENT").append(payment.getVcode()).append(") as PAYMENT").append(payment.getVcode());
					formatCol+=",PAYMENT"+payment.getVcode();
					sqlCoAtpPay.append(", IFNULL(PAYMENT"+payment.getVcode()+",0) AS PAYMENT").append(payment.getVcode());
				}
			}
			
			paramMap.put("sqlData",sqlData);
			paramMap.put("sqlDataS",sqlDataS);
			paramMap.put("sqlCol",sqlCol);
			paramMap.put("sqlColGroup",sqlColGroup);
			paramMap.put("sqlColSum",sqlColSum);
			paramMap.put("con",condition);
			paramMap.put("sqlCoAtpPay",sqlCoAtpPay);
			
			reportObject.setRows(MisUtil.formatNumForListResult(pageManager.selectPage(paramMap, pager, MisBohBusinessAnalysisMapper.class.getName()+".queryFolioDetailInfo"), formatCol, 2));
			reportObject.setFooter(MisUtil.formatNumForListResult(businessAnalysisMapper.queryCalForFolioDetailInfo(paramMap), formatCol, 2));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：当日查询——账单查询
	 * @author 马振
	 * 创建时间：2016-3-1 下午2:21:17
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> queryFolioDetailInfoCur(PublicEntity condition) throws CRUDException {
		try {
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			
			StringBuffer sqlData = new StringBuffer();
			StringBuffer sqlDataS = new StringBuffer();
			StringBuffer sqlColGroup = new StringBuffer();
			StringBuffer sqlCol = new StringBuffer();
			StringBuffer sqlColSum = new StringBuffer();
			
			StringBuffer sqlDataSS = new StringBuffer();//活动内层sum字段
			StringBuffer sqlDataHdzd = new StringBuffer();//活动最外层字段统计
			StringBuffer sqlDataHdSum= new StringBuffer();//活动合计行
			StringBuffer sqlColHdGroup = new StringBuffer();
			StringBuffer sqlstr = new StringBuffer();//最终过滤支付方式和活动的where条件的sql
			
			Map<String,Object> paramMap = new HashMap<String,Object>();
			Page pager = condition.getPager();
			String formatCol = "NMONEY,NBZERO,NZMONEY,ZSMONEY,NYMONEY,NSVR,NMONEYV,JDMONEY,JDMONEY,JFMONEY,CARDMONEY,AVGAMT,AVGAMT";
			List<ActTyp> listActTyp = commonMISBOHMapper.findAllActTyp();
			Payment pm = new Payment();
			pm.setVfoodsign("2");//快餐报表查询
			List<Payment> listPayment = commonMISBOHMapper.findAllPayment();
			List<Actm> listActm = commonMISBOHMapper.findAllActm();//查询相关活动
			
			//读取配置文件  查看数据源
			ReadProperties rp = new ReadProperties();
			String dataSource = rp.getStrByParam("dataSource");
			if(dataSource.equals("mysql")){
				for(ActTyp actTyp : listActTyp){
					sqlData.append(",SUM(CASE WHEN TYP.VCODE='").append(actTyp.getVcode()).append("' THEN IFNULL(PAY.NMONEY,0) END) AS AMT").append(actTyp.getVcode());
					sqlColGroup.append(",AMT").append(actTyp.getVcode());
					sqlCol.append(",IFNULL(AMT").append(actTyp.getVcode()).append(",0) as AMT").append(actTyp.getVcode());
					sqlColSum.append(",SUM(AMT").append(actTyp.getVcode()).append(") as AMT").append(actTyp.getVcode());
					formatCol+=",AMT"+actTyp.getVcode();
				}
				for(Payment payment : listPayment){
					if(null != condition.getPk_paymode() && !"".equals(condition.getPk_paymode())){//如果选择了支付方式，就按支付方式查
						if(payment.getVcode().equals(condition.getPk_paymode())){
							if("51".equals(payment.getIvalue()) || "2".equals(payment.getIvalue())){
								sqlDataS.append(",SUM(case when PAY.VCODE='").append(payment.getVcode()).append("' then IFNULL(FOLIOPAY.NMONEY-FOLIOPAY.novercash,0) else 0 end) AS PAYMENT").append(payment.getVcode());
							}else{
								sqlDataS.append(",SUM(case when PAY.VCODE='").append(payment.getVcode()).append("' then IFNULL(FOLIOPAY.NMONEY,0)  else 0 end) AS PAYMENT").append(payment.getVcode());
							}
							sqlColGroup.append(",PAYMENT").append(payment.getVcode());
							sqlCol.append(",IFNULL(PAYMENT").append(payment.getVcode()).append(",0) as PAYMENT").append(payment.getVcode());
							sqlColSum.append(",SUM(PAYMENT").append(payment.getVcode()).append(") as PAYMENT").append(payment.getVcode());
							formatCol+=",PAYMENT"+payment.getVcode();
							sqlstr.append(" AND PAYMENT").append(payment.getVcode()).append(" >0 ");
						}
					}else{
						if("51".equals(payment.getIvalue()) || "2".equals(payment.getIvalue())){
							sqlDataS.append(",SUM(case when PAY.VCODE='").append(payment.getVcode()).append("' then IFNULL(FOLIOPAY.NMONEY-FOLIOPAY.novercash,0) else 0 end) AS PAYMENT").append(payment.getVcode());
						}else{
							sqlDataS.append(",SUM(case when PAY.VCODE='").append(payment.getVcode()).append("' then IFNULL(FOLIOPAY.NMONEY,0)  else 0 end) AS PAYMENT").append(payment.getVcode());
						}
						sqlColGroup.append(",PAYMENT").append(payment.getVcode());
						sqlCol.append(",IFNULL(PAYMENT").append(payment.getVcode()).append(",0) as PAYMENT").append(payment.getVcode());
						sqlColSum.append(",SUM(PAYMENT").append(payment.getVcode()).append(") as PAYMENT").append(payment.getVcode());
						formatCol+=",PAYMENT"+payment.getVcode();
					}
					
				}
				//活动设置
				for(Actm actm : listActm){
					sqlDataSS.append(",SUM(CASE WHEN ACTM.VOPERATE='").append(actm.getVcode()).append("' THEN IFNULL(ACTM.NMONEY,0) ELSE 0 END) AS ACTM").append(actm.getVcode());
					sqlDataHdzd.append(",IFNULL(ACTM"+actm.getVcode()+",0) AS ").append("ACTM"+actm.getVcode());
					formatCol+=",ACTM"+actm.getVcode();
					sqlDataHdSum.append(",IFNULL(SUM(ACTM"+actm.getVcode()+"),0) AS ").append("ACTM"+actm.getVcode());
					sqlColHdGroup.append(",ACTM").append(actm.getVcode());
				}
			}else if(dataSource.equals("sqlserver")){
				for(ActTyp actTyp : listActTyp){
					sqlData.append(",SUM(CASE WHEN TYP.VCODE='").append(actTyp.getVcode()).append("' THEN ISNULL(PAY.NMONEY,0) END) AS AMT").append(actTyp.getVcode());
					sqlColGroup.append(",AMT").append(actTyp.getVcode());
					sqlCol.append(",ISNULL(AMT").append(actTyp.getVcode()).append(",0) as AMT").append(actTyp.getVcode());
					sqlColSum.append(",SUM(AMT").append(actTyp.getVcode()).append(") as AMT").append(actTyp.getVcode());
					formatCol+=",AMT"+actTyp.getVcode();
				}
				for(Payment payment : listPayment){
	//				sqlDataS.append(",CASE WHEN PAY.VCODE='").append(payment.getVcode()).append("' THEN SUM(ISNULL(PAY.NMONEY,0)-ISNULL(PAY.NOVERCASH,0)) ELSE 0 END AS PAYMENT").append(payment.getVcode());;
	//				sqlDataS.append(",SUM(DECODE(PAY.VCODE,'").append(payment.getVcode()).append("', CASE WHEN FOLIO.VORCLASS IN (1,6) THEN ISNULL(FOLIOPAY.NMONEY,0) ELSE 0 END - CASE WHEN FOLIO.VORCLASS IN (2,3) then ISNULL(FOLIOPAY.NMONEY,0) ELSE 0 END)) AS PAYMENT").append(payment.getVcode());
					if("51".equals(payment.getIvalue()) || "2".equals(payment.getIvalue())){
						sqlDataS.append(",SUM(case when PAY.VCODE='").append(payment.getVcode()).append("' then ISNULL(FOLIOPAY.NMONEY-FOLIOPAY.novercash,0) else 0 end) AS PAYMENT").append(payment.getVcode());
					}else{
						sqlDataS.append(",SUM(case when PAY.VCODE='").append(payment.getVcode()).append("' then ISNULL(FOLIOPAY.NMONEY,0)  else 0 end) AS PAYMENT").append(payment.getVcode());
					}
					sqlColGroup.append(",PAYMENT").append(payment.getVcode());
					sqlCol.append(",ISNULL(PAYMENT").append(payment.getVcode()).append(",0) as PAYMENT").append(payment.getVcode());
					sqlColSum.append(",SUM(PAYMENT").append(payment.getVcode()).append(") as PAYMENT").append(payment.getVcode());
					formatCol+=",PAYMENT"+payment.getVcode();
				}
				//活动设置
				for(Actm actm : listActm){
					if(null != condition.getVactCode() && !"".equals(condition.getVactCode())){//如果选择了活动，就按活动查
						if(actm.getVcode().equals(condition.getVactCode())){
							sqlDataSS.append(",SUM(CASE WHEN ACTM.VOPERATE='").append(actm.getVcode()).append("' THEN ISNULL(ACTM.NMONEY,0) ELSE 0 END) AS ACTM").append(actm.getVcode());
							sqlDataHdzd.append(",ISNULL(ACTM"+actm.getVcode()+",0) AS ").append("ACTM"+actm.getVcode());
							formatCol+=",ACTM"+actm.getVcode();
							sqlDataHdSum.append(",ISNULL(SUM(ACTM"+actm.getVcode()+"),0) AS ").append("ACTM"+actm.getVcode());
							sqlColHdGroup.append(",ACTM").append(actm.getVcode());
							sqlstr.append(" AND ACTM").append(actm.getVcode()).append(" >0 ");
						}
					}else{
						sqlDataSS.append(",SUM(CASE WHEN ACTM.VOPERATE='").append(actm.getVcode()).append("' THEN ISNULL(ACTM.NMONEY,0) ELSE 0 END) AS ACTM").append(actm.getVcode());
						sqlDataHdzd.append(",ISNULL(ACTM"+actm.getVcode()+",0) AS ").append("ACTM"+actm.getVcode());
						formatCol+=",ACTM"+actm.getVcode();
						sqlDataHdSum.append(",ISNULL(SUM(ACTM"+actm.getVcode()+"),0) AS ").append("ACTM"+actm.getVcode());
						sqlColHdGroup.append(",ACTM").append(actm.getVcode());
					}
					
				}
			}else{
				for(ActTyp actTyp : listActTyp){
					sqlData.append(",SUM(DECODE(TYP.VCODE,'").append(actTyp.getVcode()).append("',NVL(PAY.NMONEY,0))) AS AMT").append(actTyp.getVcode());
					sqlColGroup.append(",AMT").append(actTyp.getVcode());
					sqlCol.append(",NVL(AMT").append(actTyp.getVcode()).append(",0) as AMT").append(actTyp.getVcode());
					sqlColSum.append(",SUM(AMT").append(actTyp.getVcode()).append(") as AMT").append(actTyp.getVcode());
					formatCol+=",AMT"+actTyp.getVcode();
				}
				for(Payment payment : listPayment){
					if(null != condition.getPk_paymode() && !"".equals(condition.getPk_paymode())){//如果选择了支付方式，就按支付方式查
						if(payment.getVcode().equals(condition.getPk_paymode())){
							if("51".equals(payment.getIvalue()) || "2".equals(payment.getIvalue())){
								sqlDataS.append(",SUM(case when PAY.VCODE='").append(payment.getVcode()).append("' then NVL(FOLIOPAY.NMONEY-FOLIOPAY.novercash,0) else 0 end) AS PAYMENT").append(payment.getVcode());
							}else{
								sqlDataS.append(",SUM(case when PAY.VCODE='").append(payment.getVcode()).append("' then NVL(FOLIOPAY.NMONEY,0)  else 0 end) AS PAYMENT").append(payment.getVcode());
							}
							sqlColGroup.append(",PAYMENT").append(payment.getVcode());
							sqlCol.append(",NVL(PAYMENT").append(payment.getVcode()).append(",0) as PAYMENT").append(payment.getVcode());
							sqlColSum.append(",SUM(PAYMENT").append(payment.getVcode()).append(") as PAYMENT").append(payment.getVcode());
							formatCol+=",PAYMENT"+payment.getVcode();
							sqlstr.append(" AND PAYMENT").append(payment.getVcode()).append(" >0 ");
						}
					}else{
						if("51".equals(payment.getIvalue()) || "2".equals(payment.getIvalue())){
							sqlDataS.append(",SUM(case when PAY.VCODE='").append(payment.getVcode()).append("' then NVL(FOLIOPAY.NMONEY-FOLIOPAY.novercash,0) else 0 end) AS PAYMENT").append(payment.getVcode());
						}else{
							sqlDataS.append(",SUM(case when PAY.VCODE='").append(payment.getVcode()).append("' then NVL(FOLIOPAY.NMONEY,0)  else 0 end) AS PAYMENT").append(payment.getVcode());
						}
						sqlColGroup.append(",PAYMENT").append(payment.getVcode());
						sqlCol.append(",NVL(PAYMENT").append(payment.getVcode()).append(",0) as PAYMENT").append(payment.getVcode());
						sqlColSum.append(",SUM(PAYMENT").append(payment.getVcode()).append(") as PAYMENT").append(payment.getVcode());
						formatCol+=",PAYMENT"+payment.getVcode();
					}
				}
				//活动设置
				for(Actm actm : listActm){
					if(null != condition.getVactCode() && !"".equals(condition.getVactCode())){//如果选择了活动，就按活动查
						if(actm.getVcode().equals(condition.getVactCode())){
							sqlDataSS.append(",SUM(CASE WHEN ACTM.VOPERATE='").append(actm.getVcode()).append("' THEN NVL(ACTM.NMONEY,0) ELSE 0 END) AS ACTM").append(actm.getVcode());;
							sqlDataHdzd.append(",NVL(ACTM"+actm.getVcode()+",0) AS ").append("ACTM"+actm.getVcode());
							formatCol+=",ACTM"+actm.getVcode();
							sqlDataHdSum.append(",NVL(SUM(ACTM"+actm.getVcode()+"),0) AS ").append("ACTM"+actm.getVcode());
							sqlColHdGroup.append(",ACTM").append(actm.getVcode());
							sqlstr.append(" AND ACTM").append(actm.getVcode()).append(" >0 ");
						}
					}else{
						sqlDataSS.append(",SUM(CASE WHEN ACTM.VOPERATE='").append(actm.getVcode()).append("' THEN NVL(ACTM.NMONEY,0) ELSE 0 END) AS ACTM").append(actm.getVcode());;
						sqlDataHdzd.append(",NVL(ACTM"+actm.getVcode()+",0) AS ").append("ACTM"+actm.getVcode());
						formatCol+=",ACTM"+actm.getVcode();
						sqlDataHdSum.append(",NVL(SUM(ACTM"+actm.getVcode()+"),0) AS ").append("ACTM"+actm.getVcode());
						sqlColHdGroup.append(",ACTM").append(actm.getVcode());
					}
					
				}
			}
			if("0".equals(condition.getSft())){
				condition.setSft("");
			}
			paramMap.put("sqlData",sqlData);
			paramMap.put("sqlDataS",sqlDataS);
			paramMap.put("sqlCol",sqlCol);
			paramMap.put("sqlColGroup",sqlColGroup);
			paramMap.put("sqlColSum",sqlColSum);
			paramMap.put("con",condition);
			paramMap.put("sqlstr",sqlstr.toString());
			paramMap.put("sqlDataSS",sqlDataSS.toString());
			paramMap.put("sqlDataHdzd",sqlDataHdzd.toString());
			paramMap.put("sqlDataHdSum",sqlDataHdSum.toString());
			paramMap.put("sqlColHdGroup",sqlColHdGroup.toString());
			

			List<Map<String,Object>> listResult = pageManager.selectPage(paramMap, pager, MisBohBusinessAnalysisMapper.class.getName()+".queryFolioDetailInfoCur");
			reportObject.setRows(MisUtil.formatNumForListResult(listResult, formatCol, 2));
			reportObject.setFooter(MisUtil.formatNumForListResult(businessAnalysisMapper.queryCalFolioDetailInfoCur(paramMap), formatCol, 2));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：賬單明細分析
	 * @author 马振
	 * 创建时间：2015-4-22 下午3:56:07
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> queryZdMxfx(PublicEntity condition) throws CRUDException {
		try {
			Page pager = condition.getPager();
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			condition.setVpcode(FormatDouble.StringCodeReplace(condition.getVpcode()));
			
			reportObject.setRows(pageManager.selectPage(condition, pager, MisBohBusinessAnalysisMapper.class.getName() + ".queryZdMxfx"));
			reportObject.setFooter(businessAnalysisMapper.queryCalForZdMxfx(condition));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询账单支付明细
	 * @author 马振
	 * 创建时间：2015-8-19 下午3:15:59
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> queryZdMxfx_pay(PublicEntity condition) throws CRUDException {
		try {
			Page pager = condition.getPager();
			if(condition.getVbcode() != null){
				
				reportObject.setRows(pageManager.selectPage(condition, pager, MisBohBusinessAnalysisMapper.class.getName()+".queryZdMxfx_pay"));
				reportObject.setFooter(businessAnalysisMapper.queryCalForZdMxfx_pay(condition));
				reportObject.setTotal(pager.getCount());
			}
			return reportObject;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：营运报告
	 * @author 马振
	 * 创建时间：2015-4-23 上午10:39:33
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Object queryyybg(PublicEntity condition) throws CRUDException {
		try {
			Map<String,Object> map=new HashMap<String, Object>();
			map.put("pk_store", FormatDouble.StringCodeReplace(condition.getPk_store()));
			map.put("bdat", condition.getBdat());
			if(ValueUtil.IsNotEmpty(condition.getBdat())){
				String edat = DateJudge.getLastYearMonth(condition.getBdat() + "-01");
				map.put("edat", edat);
			}
			if(ValueUtil.IsNotEmpty(condition.getEnablestate())){//给启用状态添加‘’
				map.put("enablestate",FormatDouble.StringCodeReplace(condition.getEnablestate()));
			}
			if(ValueUtil.IsNotEmpty(condition.getVistest())){//是否是测试门店
				map.put("vistest", condition.getVistest());
			}
			//结果集
			List<Map<String,Object>> relustList=new ArrayList<Map<String,Object>>();
			//获取营业时段数据
			Interval inter = new Interval();
			inter.setVintervaltype("0");
			List<Interval> listinter = baseRecordMapper.listInterval(inter);
			//格式化字段两位小数
			StringBuffer sb1=new StringBuffer();
			sb1.append("NMONEY,NMONEYLJ,NYMONEY,NYMONEYLJ,TNYMONEY,TNYMONEYL,TNYMONEYLJ,AVGTC,AVGTCLJ,TCL,TCLLJ,PTC,PTCLJ,IPEOLENUM,IPEOLENUMLJ,IPL,IPLLJ,SSMONEY");
			//格式化数字空时默认为0
			String str = "TC,SHOUMAI";
			
			String sqlFlag=ReadConfig.getInstance().getValue("dataSource");
			StringBuffer sqlinter=new StringBuffer();//最外出字段显示
			StringBuffer sqlshow=new StringBuffer();//里面字段显示
			StringBuffer sqlbody=new StringBuffer();//字段计算
			StringBuffer sqlsum=new StringBuffer();//合计行拼接显示字段
			StringBuffer sqlshowSum = new StringBuffer();//鱼品统计
			StringBuffer sqlFD = new StringBuffer();//鱼品统计
			if(sqlFlag.equals("sqlserver")){
				for (int y = 0; y < listinter.size(); y++) {
					Interval interval=listinter.get(y);
					String vcode=interval.getVcode();
					// SUM(CASE  WHEN SD.SDTIME = '101' THEN   SD.NMONEY  else    0  end) as NMONEY101,
					sqlshow.append("\n ,SUM(CASE WHEN  SDTIME='"+vcode+"'"); 
					sqlshow.append(" THEN  NMONEY else 0 end) as NMONEY"+vcode);
					
					sqlshow.append("\n ,SUM(CASE WHEN  SDTIME='"+vcode+"'"); 
					sqlshow.append(" THEN  TC else 0 end) as TC"+vcode);
					
					sqlshow.append("\n ,SUM(CASE WHEN  SDTIME='"+vcode+"'"); 
					sqlshow.append(" THEN  IPEOLENUM else 0 end) as PAX"+vcode);
					
					sqlshowSum.append(" ,SUM(NMONEY"+vcode+") as NMONEY"+vcode);
					sqlshowSum.append(" ,SUM(TC"+vcode+") as TC"+vcode);
					sqlshowSum.append(" ,SUM(PAX"+vcode+") as PAX"+vcode);
					str += ",TC"+vcode+",PAX"+vcode;
				}
			}else{
				for (int y = 0; y < listinter.size(); y++) {
					Interval interval=listinter.get(y);
					String vcode=interval.getVcode();
					// SUM(CASE  WHEN SD.SDTIME = '101' THEN   SD.NMONEY  else    0  end) as NMONEY101,
					sqlshow.append("\n ,SUM(CASE WHEN  SDTIME='"+vcode+"'"); 
					sqlshow.append(" THEN  NMONEY else 0 end) as NMONEY"+vcode);
					
					sqlshow.append("\n ,SUM(CASE WHEN  SDTIME='"+vcode+"'"); 
					sqlshow.append(" THEN  TC else 0 end) as TC"+vcode);
					
					sqlshow.append("\n ,SUM(CASE WHEN  SDTIME='"+vcode+"'"); 
					sqlshow.append(" THEN  IPEOLENUM else 0 end) as PAX"+vcode);
					
					sqlshowSum.append(" ,SUM(NMONEY"+vcode+") as NMONEY"+vcode);
					sqlshowSum.append(" ,SUM(TC"+vcode+") as TC"+vcode);
					sqlshowSum.append(" ,SUM(PAX"+vcode+") as PAX"+vcode);
					str += ",TC"+vcode+",PAX"+vcode;
				}
			}
			
			//获取预估值
			String str1 = "";	
			double ygmoney = 0; 
			
			//获取传入的日期 形如（2015-01）
			String datstr = condition.getBdat();
			String dat[];				//定义一个字符串数组(用来存储通过"2015-01"分隔的数组)
			String year = "";			//年份
			String month = "BUDGETM";	//月份
			if (datstr.length() > 0) {
				dat =  datstr.split("-");
				year = dat[0];//年份
				month = month + Integer.parseInt(dat[1]);
			}
			Map<String,Object> ygmap = new HashMap<String,Object>();
			
			if ("sqlserver".equals(sqlFlag)) {
				str1 = "SUM(ISNULL(" + month + ", 0))";
			} else if ("mysql".equals(sqlFlag)) {
				str1 = "SUM(IFNULL(" + month + ", 0))";
			} else {
				str1 = "SUM(NVL(" + month + ", 0))";
			}
			
			ygmap.put("month", str1);
			ygmap.put("year", year);
			ygmap.put("pk_store", FormatDouble.StringCodeReplace(condition.getPk_store()));
			
			//获取数据库中的预估值
			List<Map<String, Object>> yglist = commonMISBOHMapper.getYuGu(ygmap);
			if (yglist.size() > 0) {
				ygmoney = Double.parseDouble(null == yglist.get(0) ? "0" : yglist.get(0).get("YGMONEY").toString());
			}
			
			String sqlshowStr="";
			String sqlbodyStr="";
			String sqlFDStr="";
			String sqlsumStr="";
			String monthSql="";
			 
			sqlshowStr=sqlshow.toString();
			sqlbodyStr=sqlbody.toString();
			sqlFDStr=sqlFD.toString();
			sqlsumStr=sqlsum.toString();
			 
			map.put("sqlshowSum", sqlshowSum.toString());
			
			map.put("sqlshow", sqlshowStr);
			map.put("sqlinter", sqlinter.toString());
			map.put("sqlbody", sqlbodyStr);
			map.put("sqlsum", sqlsumStr);
			map.put("sqlFD", sqlFDStr);
			map.put("monthSql", monthSql);
			List<Map<String, Object>> list = businessAnalysisMapper.queryyybg(map);
			
			Total nmoney =new Total();		//营业收入累计
			Total nymoney =new Total();		//实际营业累计
			Total tnymoney =new Total();	//去年同期累计
			Total tc =new Total();			//开台累计
			double ptc = 0.0;				//开台率累计
			Total ipeolenum =new Total();	//人数累计
			Total yk =new Total();			//盈亏累计金额
			double tblcnt = 0.0;			//台位累计
			double ichangetable = 0.0;		//最大班次
			//循环处理查询出的结果
			for (int i = 0; i < list.size(); i++) {
				String dworkdate=condition.getBdat();
				Map<String, Object> smap = list.get(i);
				if(smap.get("DWORKDATE").toString().length()==1){
					dworkdate+="-0"+smap.get("DWORKDATE").toString();
				}else{
					dworkdate+="-"+smap.get("DWORKDATE").toString();
				}
				smap.put("DWORKDATE", dworkdate);
				smap.put("XQ", DateJudge.getWeekOfDateToString(dworkdate));
				nmoney.addD(Double.parseDouble(MisUtil.formatDoubleLength(MisUtil.getValue(smap.get("NMONEY")), 2)));
				smap.put("NMONEYLJ", nmoney.getD());
				nymoney.addD(Double.parseDouble(MisUtil.formatDoubleLength(MisUtil.getValue(smap.get("NYMONEY")), 2)));
				smap.put("NYMONEYLJ", nymoney.getD());
				tnymoney.addD(Double.parseDouble(MisUtil.formatDoubleLength(MisUtil.getValue(smap.get("TNYMONEYL")), 2)));
				smap.put("TNYMONEYLJ", MisUtil.formatDoubleLength(String.valueOf(ygmoney == 0 ? 0 : (nymoney.getD() / ygmoney) * 100), 2) + "%");
				tc.addD(Double.parseDouble(MisUtil.formatDoubleLength(MisUtil.getValue(smap.get("TC")), 0)));
				smap.put("TCLJ",MisUtil.formatDoubleLength(String.valueOf(tc.getD()),0));
				//台位按天累计
				tblcnt=tblcnt==0.0?Double.parseDouble(MisUtil.formatDoubleLength(MisUtil.getValue(smap.get("TBLCNT")), 0)):tblcnt;
				
				//获取当前门店当日最大班次
				ichangetable = ichangetable == 0.0?Double.parseDouble(MisUtil.formatDoubleLength(MisUtil.getValue(smap.get("ICHANGETABLE")), 0)):ichangetable;
				ptc=Double.parseDouble(MisUtil.formatDoubleLength(MisUtil.getValue(smap.get("TCLJ")), 2));
				smap.put("PTCLJ", MisUtil.stringPlusDouble(tblcnt==0?0:ptc/tblcnt/ichangetable,0));
				smap.put("AVGTCLJ", Double.parseDouble(MisUtil.dividedNum(smap.get("NYMONEYLJ"),smap.get("TCLJ"), 2)));
				ipeolenum.addD(Double.parseDouble(MisUtil.formatDoubleLength(MisUtil.getValue(smap.get("IPEOLENUM")), 2)));
				smap.put("IPEOLENUMLJ", ipeolenum.getD());
				smap.put("IPLLJ", Double.parseDouble(MisUtil.dividedNum(smap.get("NYMONEYLJ"),smap.get("IPEOLENUMLJ"), 2)));
				yk.addD(Double.parseDouble(MisUtil.formatDoubleLength(MisUtil.getValue(smap.get("YK")), 2)));
				smap.put("YKLJ", yk.getD());
			
				relustList.add(smap);
				smap = null;
			}
			
			//格式化金额保留2为小数 
			for (int y = 0; y < listinter.size(); y++) {
				Interval interval=listinter.get(y);
				sb1.append(",NMONEY"+interval.getVcode()+",MONEYL"+interval.getVcode()+",PTC"+interval.getVcode());
				
			}
			sb1.append(",YK,YKLJ,ZSMONEY,DQMONEY,WATERCNT,ELECNT,GASCNT");
			
			relustList = formatNumForListResult(list, sb1.toString(), 2);	//格式化相关数据
			relustList = MisUtil.formatNumForListResult(list, str, 0);		//将数量格式化为整数
			reportObject.setRows(relustList);
			return reportObject;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 描述：当日查询——营运报告
	 * @author 马振
	 * 创建时间：2016-3-3 下午4:19:05
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Object queryYybgCur(PublicEntity condition) throws CRUDException {
		try {
			//获取年月，若为空则赋值当前年月
			String edat = null == condition.getEdat() ? DateJudge.getNowDate() : condition.getEdat();
			
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("pk_store", FormatDouble.StringCodeReplace(condition.getPk_store()));
			map.put("bdat", edat.substring(0, 7) + "-01");
			map.put("edat", edat);
			
			String sqlFlag = ReadConfig.getInstance().getValue("dataSource");
			
			//获取预估值
			String str = "";	
			double ygmoney = 0; 
			String day = "BUDGETM" + Integer.valueOf(edat.substring(5, 7));
			Map<String,Object> ygmap = new HashMap<String,Object>();
			
			if ("sqlserver".equals(sqlFlag)) {
				str = "SUM(ISNULL(" + day + ", 0))";
			} else if ("mysql".equals(sqlFlag)) {
				str = "SUM(IFNULL(" + day + ", 0))";
			} else {
				str = "SUM(NVL(" + day + ", 0))";
			}
			
			ygmap.put("month", str);
			ygmap.put("year", edat.substring(0, 4));
			ygmap.put("pk_store", FormatDouble.StringCodeReplace(condition.getPk_store()));
			
			//获取数据库中的预估值
			List<Map<String, Object>> yglist = commonMISBOHMapper.getYuGu(ygmap);
			if (yglist.size() > 0) {
				ygmoney = Double.parseDouble(null == yglist.get(0) ? "0" : yglist.get(0).get("YGMONEY").toString());
			}
			
			List<Map<String, Object>> list = businessAnalysisMapper.queryYybgCur(map);
			
			List<Map<String,Object>> relustList = new ArrayList<Map<String,Object>>();	//结果集	
			double nymoney = 0.00;			//实际营业额
			
			//循环处理查询出的结果
			for (int i = 0; i < list.size(); i++) {
				String dworkdate = null == list.get(i) ? "" : list.get(i).get("DWORKDATE").toString();	//获取日期
				Map<String, Object> smap = list.get(i);
				smap.put("XQ", DateJudge.getWeekOfDateToString(dworkdate));	//获取当期日期的星期
				
				//获取实收营业额累计
				nymoney = Double.parseDouble(MisUtil.formatDoubleLength(MisUtil.getValue(smap.get("NYMONEYLJ")), 2));
				smap.put("TNYMONEYLJ", 
					MisUtil.formatDoubleLength(String.valueOf(ygmoney == 0 ? 0.00 : (nymoney / ygmoney) * 100), 2) + "%");
			
				relustList.add(smap);
				smap = null;
			}
			
			StringBuffer strs = new StringBuffer();
			strs.append("NMONEY, NMONEYLJ, NYMONEY, NYMONEYLJ, TNYMONEY, AVGTC, AVGTCLJ, IPL,")
			    .append("IPLLJ, YK, YKLJ, ZSMONEY, DQMONEY, SSMONEY");
			
			reportObject.setRows(formatNumForListResult(relustList, strs.toString(), 2));
			return reportObject;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：营业日明细金额查询
	 * @author 马振
	 * 创建时间：2015-4-23 上午10:56:58
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Map<String, Object> queryYingYeRibao(PublicEntity condition) throws CRUDException{
		try {
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			condition.setPubgrp(FormatDouble.StringCodeReplace(condition.getPubgrp()));
			Map<String, Object> list = businessAnalysisMapper.queryYingYeRibao(condition);
//			reportObject.setFooter(MisUtil.formatNumForListResult(businessAnalysisMapper.queryCalForFzlb(condition),cols, 2));
			return list;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：当日查询——营业日明细金额查询
	 * @author 马振
	 * 创建时间：2016-3-4 上午11:35:50
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Map<String, Object> queryYingYeRibaoCur(PublicEntity condition) throws CRUDException{
		try {
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			return businessAnalysisMapper.queryYingYeRibaoCur(condition);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：营业日报 餐次统计
	 * @author 马振
	 * 创建时间：2015-4-23 上午10:59:48
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> queryYingYeRibaoCanci2(PublicEntity condition) throws CRUDException{
		try {
			String bdat = condition.getBdat();
			PublicEntity con = new PublicEntity(); //定义计算去年数据的对象
			con.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			con.setBdat((Integer.parseInt(condition.getBdat().substring(0, 4))-1)+condition.getBdat().substring(4));
			con.setEdat((Integer.parseInt(condition.getEdat().substring(0, 4))-1)+condition.getEdat().substring(4));
			
			//获取预估值
			String str = "";	
			double ygmoney = 0; 
			String day = "BUDGETM" + Integer.valueOf(bdat.substring(5, 7));
			Map<String,Object> ygmap = new HashMap<String,Object>();

			String sqlFlag = ReadConfig.getInstance().getValue("dataSource");	//获取数据库类型
			
			if ("sqlserver".equals(sqlFlag)) {
				str = "SUM(ISNULL(" + day + ", 0))";
			} else if ("mysql".equals(sqlFlag)) {
				str = "SUM(IFNULL(" + day + ", 0))";
			} else {
				str = "SUM(NVL(" + day + ", 0))";
			}
			
			ygmap.put("month", str);
			ygmap.put("year", bdat.substring(0, 4));
			ygmap.put("pk_store", FormatDouble.StringCodeReplace(condition.getPk_store()));
			
			//获取数据库中的预估值
			List<Map<String, Object>> yglist = commonMISBOHMapper.getYuGu(ygmap);
			if (yglist.size() > 0) {
				ygmoney = Double.parseDouble(null == yglist.get(0) ? "0" : yglist.get(0).get("YGMONEY").toString());
			}
			
			//查询去年同期总额
			Map<String,Object> result3 = businessAnalysisMapper.queryYingYeRibaoCanci2(con);
			con.setBdat(condition.getBdat());
			con.setEdat(condition.getEdat());
			List<Map<String, Object>> ccMap = new ArrayList<Map<String,Object>>(); //定义结果集
			
			//首先查询营业时段设置信息
			Interval inte =new Interval();
			inte.setEnablestate(2);
			List<Interval> listint = baseRecordMapper.listInterval(inte);
			DecimalFormat df = new DecimalFormat("#.##"); 
			int totalcount=0; 		//定义总数量
			double totalmoney = 0D; // 定义总金额
			
			//如果设置了营业时段，就遍历之后查询各营业时段的数据
			if(null != listint){ 	
				for(Interval interval : listint){
					condition.setBinterval(interval.getVstarttime()); //设置开始时间
					condition.setEinterval(interval.getVendtime()); //设置结束时间
					Map<String,Object> result = businessAnalysisMapper.queryYingYeRibaoCanci2(condition);
					result.put("CANCI", interval.getVname()); //加入餐次的名称
					//单均
					result.put("DANJUN", Double.parseDouble(result.get("NUMB").toString())==0D?0:(df.format(Double.parseDouble(result.get("MONEY").toString())/Double.parseDouble(result.get("NUMB").toString()))));
					result.put("DCYG", MisUtil.formatDoubleLength(String.valueOf(ygmoney == 0 ? 100 : (Double.parseDouble(result.get("MONEY").toString()) / ygmoney) * 100), 2) + "%"); //达成预估，默认100%
					
					//计算去年同期的值
					con.setBinterval(interval.getVstarttime()); //设置开始时间
					con.setEinterval(interval.getVendtime()); //设置结束时间
					Map<String,Object> result2 = businessAnalysisMapper.queryYingYeRibaoCanci2(con);
					
					//达成去年
					result.put("DCQN", Double.parseDouble(result2.get("MONEY").toString()) == 0D?"100%":(df.format(Double.parseDouble(result.get("MONEY").toString())/Double.parseDouble(result2.get("MONEY").toString())*100))+"%"); //计算达成去年
					totalcount += Integer.parseInt(result.get("NUMB").toString());
					totalmoney += Double.parseDouble(result.get("MONEY").toString());
					ccMap.add(result);
				}
			}
			
			//计算合计
			Map<String,Object> resulttotal  = new HashMap<String, Object>();
			resulttotal.put("CANCI", "合计"); //加入餐次的名称
			resulttotal.put("NUMB", totalcount);
			resulttotal.put("MONEY", totalmoney);
			resulttotal.put("DANJUN", totalcount == 0 ? 0 : df.format(totalmoney/totalcount));
			resulttotal.put("DCYG", MisUtil.formatDoubleLength(String.valueOf(ygmoney == 0 ? 100 : (totalmoney / ygmoney) * 100), 2) + "%");
			resulttotal.put("DCQN", Double.parseDouble(result3.get("MONEY").toString()) == 0D?"100%":(df.format(totalmoney/Double.parseDouble(result3.get("MONEY").toString())*100))+"%");
			ccMap.add(resulttotal);
			return ccMap;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：当日查询——营业日明细   餐次统计
	 * @author 马振
	 * 创建时间：2016-3-4 下午2:25:43
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> queryYingYeRibaoCanciCur(PublicEntity condition) throws CRUDException{
		try {
			String bdat = condition.getBdat();
			condition.setEdat(bdat);	//将结束日期设置为当期日期
			PublicEntity con = new PublicEntity(); //定义计算去年数据的对象
			con.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			con.setBdat((Integer.parseInt(bdat.substring(0, 4)) - 1) + bdat.substring(4));
			con.setEdat((Integer.parseInt(bdat.substring(0, 4)) - 1) + bdat.substring(4));

			//获取预估值
			String str = "";	
			double ygmoney = 0; 
			String day = "BUDGETM" + Integer.valueOf(bdat.substring(5, 7));
			Map<String,Object> ygmap = new HashMap<String,Object>();

			String sqlFlag = ReadConfig.getInstance().getValue("dataSource");	//获取数据库类型
			
			if ("sqlserver".equals(sqlFlag)) {
				str = "SUM(ISNULL(" + day + ", 0))";
			} else if ("mysql".equals(sqlFlag)) {
				str = "SUM(IFNULL(" + day + ", 0))";
			} else {
				str = "SUM(NVL(" + day + ", 0))";
			}
			
			ygmap.put("month", str);
			ygmap.put("year", bdat.substring(0, 4));
			ygmap.put("pk_store", FormatDouble.StringCodeReplace(condition.getPk_store()));
			
			//获取数据库中的预估值
			List<Map<String, Object>> yglist = commonMISBOHMapper.getYuGu(ygmap);
			if (yglist.size() > 0) {
				ygmoney = Double.parseDouble(null == yglist.get(0) ? "0" : yglist.get(0).get("YGMONEY").toString());
			}
			
			//查询去年同期总额
			Map<String,Object> result3 = businessAnalysisMapper.queryYingYeRibaoCanci2(con);
			List<Map<String, Object>> ccMap = new ArrayList<Map<String,Object>>(); //定义结果集
			
			//首先查询营业时段设置信息
			Interval inte =new Interval();
			inte.setEnablestate(2);
			List<Interval> listint = baseRecordMapper.listInterval(inte);
			DecimalFormat df = new DecimalFormat("#.##"); 
			int totalcount=0; 		//定义总数量
			double totalmoney = 0D; // 定义总金额
			
			//如果设置了营业时段，就遍历之后查询各营业时段的数据
			if (null != listint) { 	
				for(Interval interval : listint){
					condition.setBinterval(interval.getVstarttime()); 	//设置开始时间
					condition.setEinterval(interval.getVendtime()); 	//设置结束时间
					Map<String,Object> result = businessAnalysisMapper.queryYingYeRibaoCanci2(condition);
					result.put("CANCI", interval.getVname()); 	//加入餐次的名称
					//单均
					result.put("DANJUN", Double.parseDouble(result.get("NUMB").toString())==0D?0:(df.format(Double.parseDouble(result.get("MONEY").toString())/Double.parseDouble(result.get("NUMB").toString()))));
					result.put("DCYG", MisUtil.formatDoubleLength(String.valueOf(ygmoney == 0 ? 0 : (Double.parseDouble(result.get("MONEY").toString()) / ygmoney) * 100), 2) + "%"); //达成预估，默认100%
					
					//计算去年同期的值
					con.setBinterval(interval.getVstarttime()); //设置开始时间
					con.setEinterval(interval.getVendtime()); 	//设置结束时间
					Map<String,Object> result2 = businessAnalysisMapper.queryYingYeRibaoCanci2(con);
					
					//达成去年
					result.put("DCQN", Double.parseDouble(result2.get("MONEY").toString()) == 0D?"100%":(df.format(Double.parseDouble(result.get("MONEY").toString())/Double.parseDouble(result2.get("MONEY").toString())*100))+"%"); //计算达成去年
					totalcount += Integer.parseInt(result.get("NUMB").toString());
					totalmoney += Double.parseDouble(result.get("MONEY").toString());
					ccMap.add(result);
				}
			}
			//计算合计
			Map<String,Object> resulttotal  = new HashMap<String, Object>();
			resulttotal.put("CANCI", "合计"); //加入餐次的名称
			resulttotal.put("NUMB", totalcount);
			resulttotal.put("MONEY", totalmoney);
			resulttotal.put("DANJUN", totalcount==0?0:df.format(totalmoney/totalcount));
			resulttotal.put("DCYG", MisUtil.formatDoubleLength(String.valueOf(ygmoney == 0 ? 0 : (totalmoney / ygmoney) * 100), 2) + "%");
			resulttotal.put("DCQN", Double.parseDouble(result3.get("MONEY").toString()) == 0D ? "100%" : (df.format(totalmoney/Double.parseDouble(result3.get("MONEY").toString())*100))+"%");
			ccMap.add(resulttotal);
			return ccMap;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：营业日明细--退单统计
	 * @author 马振
	 * 创建时间：2015-4-23 上午11:10:33
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> queryYingYeRibaoTd(PublicEntity condition) throws CRUDException{
		try {
			return MisUtil.formatNumForListResult(businessAnalysisMapper.queryYingYeRibaoTd(condition),"NYMONEY",2);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：当日查询——营业日明细     退单统计
	 * @author 马振
	 * 创建时间：2016-3-4 下午3:27:09
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> queryYingYeRibaoTdCur(PublicEntity condition) throws CRUDException{
		try {
			return businessAnalysisMapper.queryYingYeRibaoTdCur(condition);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：营业日明细--外带统计
	 * @author 马振
	 * 创建时间：2015-4-23 上午11:17:04
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Map<String, Object> queryYingYeRibaoWd(PublicEntity condition) throws CRUDException{
		try {
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			condition.setPubgrp(FormatDouble.StringCodeReplace(condition.getPubgrp()));
			
			Map<String, Object> wdMapSum = businessAnalysisMapper.queryYingYeRibaoWdSum(condition);
			if(null!=wdMapSum){
				condition.setNmoneySum(null==wdMapSum.get("TSNMONEY")?"1":wdMapSum.get("TSNMONEY")+"");
				condition.setTcSum(null==wdMapSum.get("TSNCOUNT")?"1":wdMapSum.get("TSNCOUNT")+"");
				condition.setIpeopleSum(null==wdMapSum.get("IPEOLENUM")?"1":wdMapSum.get("IPEOLENUM")+"");
				condition.setFwsjSum(null==wdMapSum.get("FWSJ")?"1":wdMapSum.get("FWSJ")+"");
			}else{
				condition.setNmoneySum("1");
				condition.setTcSum("1");
				condition.setIpeopleSum("1");
				condition.setFwsjSum("1");
			}
			
			List<Map<String, Object>> listRow =businessAnalysisMapper.queryYingYeRibaoWd(condition);
			
			Map<String,Object> wdMap=new HashMap<String, Object>();
			wdMap.put("wdMapSum", wdMapSum);
			wdMap.put("listRow", listRow);
			
			return wdMap;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：当日查询——营业日明细    外带统计
	 * @author 马振
	 * 创建时间：2016-3-4 下午3:53:14
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Map<String, Object> queryYingYeRibaoWdCur(PublicEntity condition) throws CRUDException{
		try {
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			
			Map<String, Object> wdMapSum = businessAnalysisMapper.queryYingYeRibaoWdCurSum(condition);
			if (null != wdMapSum) {
				condition.setNmoneySum(null == wdMapSum.get("TSNMONEY") ? "1" : wdMapSum.get("TSNMONEY").toString());
				condition.setTcSum(null == wdMapSum.get("TSNCOUNT") ? "1" : wdMapSum.get("TSNCOUNT").toString());
				condition.setIpeopleSum(null == wdMapSum.get("IPEOLENUM") ? "1" : wdMapSum.get("IPEOLENUM").toString());
				condition.setFwsjSum(null == wdMapSum.get("FWSJ") ? "1" : wdMapSum.get("FWSJ").toString());
			} else {
				condition.setNmoneySum("1");
				condition.setTcSum("1");
				condition.setIpeopleSum("1");
				condition.setFwsjSum("1");
			}
			
			List<Map<String, Object>> listRow = businessAnalysisMapper.queryYingYeRibaoWdCur(condition);
			
			Map<String,Object> wdMap = new HashMap<String, Object>();
			wdMap.put("wdMapSum", wdMapSum);
			wdMap.put("listRow", listRow);
			
			return wdMap;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：营业日明细--类别统计
	 * @author 马振
	 * 创建时间：2015-4-23 上午11:22:07
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Map<String, Object> queryYingYeRibaoLb(PublicEntity condition) throws CRUDException{
		try {
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			condition.setPubgrp(FormatDouble.StringCodeReplace(condition.getPubgrp()));
			List<Map<String, Object>> lbList =businessAnalysisMapper.queryYingYeRibaoLb(condition);
			Map<String, Object> lbSumMap =businessAnalysisMapper.queryYingYeRibaoLbSum(condition);
			Map<String, Object> map=new HashMap<String, Object>();
			map.put("lbList", lbList);
			map.put("lbSumMap", lbSumMap);
			
			return map;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：当日查询——营业日明细    类别统计
	 * @author 马振
	 * 创建时间：2016-3-4 下午5:35:17
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Map<String, Object> queryYingYeRibaoLbCur(PublicEntity condition) throws CRUDException{
		try {
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			List<Map<String, Object>> lbList = businessAnalysisMapper.queryYingYeRibaoLbCur(condition);
			Map<String, Object> lbSumMap = businessAnalysisMapper.queryYingYeRibaoLbCurSum(condition);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("lbList", lbList);
			map.put("lbSumMap", lbSumMap);
			
			return map;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：营业日明细--活动统计
	 * @author 马振
	 * 创建时间：2015-4-23 上午11:23:23
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public  Map<String, Object> queryYingYeRibaoHd(PublicEntity condition) throws CRUDException{
		try {
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			condition.setPubgrp(FormatDouble.StringCodeReplace(condition.getPubgrp()));
			List<Map<String, Object>> hdList =businessAnalysisMapper.queryYingYeRibaoHd(condition);
			Map<String, Object> hdSumMap =businessAnalysisMapper.queryYingYeRibaoHdSum(condition);
			Map<String, Object> map=new HashMap<String, Object>();
			map.put("hdList", hdList);
			map.put("hdSumMap", hdSumMap);
			
			return map;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：当日查询——营业日明细     活动统计
	 * @author 马振
	 * 创建时间：2016-3-7 上午9:01:46
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public  Map<String, Object> queryYingYeRibaoHdCur(PublicEntity condition) throws CRUDException{
		try {
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			List<Map<String, Object>> hdList = businessAnalysisMapper.queryYingYeRibaoHdCur(condition);
			Map<String, Object> hdSumMap = businessAnalysisMapper.queryYingYeRibaoHdCurSum(condition);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("hdList", hdList);
			map.put("hdSumMap", hdSumMap);
			
			return map;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：营业日明细--支付类型统计
	 * @author 马振
	 * 创建时间：2015-4-23 上午11:24:32
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Map<String, Object> queryYingYeRibaoZflx(PublicEntity condition) throws CRUDException{
		try {
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			condition.setPubgrp(FormatDouble.StringCodeReplace(condition.getPubgrp()));
			List<Map<String, Object>> jsfsList =businessAnalysisMapper.queryYingYeRibaoZflx(condition);
			List<Map<String, Object>> jsfsSumMap =businessAnalysisMapper.queryYingYeRibaoZflxSum(condition);
		
			Map<String, Object> map=new HashMap<String, Object>();
			map.put("jsfsList", jsfsList);
			if(null!=jsfsSumMap && jsfsSumMap.size()>0){
				map.put("jsfsSumMap", jsfsSumMap.get(0));
			}else{
				Map<String, Object> footMap=new HashMap<String, Object>();
				footMap.put("CNT",0);
				footMap.put("NMONEY", 0);
				map.put("jsfsSumMap", footMap);
			}
			
			return map;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：当日查询——营业日明细   应收
	 * @author 马振
	 * 创建时间：2016-3-7 上午10:17:48
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Map<String, Object> queryYingYeRibaoZffsYs(PublicEntity condition) throws CRUDException{
		try {
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			List<Map<String, Object>> jsfsList = businessAnalysisMapper.queryYingYeRibaoZffsYs(condition);
			List<Map<String, Object>> jsfsSumMap = businessAnalysisMapper.queryYingYeRibaoZffsYsSum(condition);
		
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("jsfsList", jsfsList);
			if (null != jsfsSumMap && jsfsSumMap.size() > 0) {
				map.put("jsfsSumMap", jsfsSumMap.get(0));
			} else {
				Map<String, Object> footMap = new HashMap<String, Object>();
				footMap.put("CNT", 0);
				footMap.put("NMONEY", 0);
				map.put("jsfsSumMap", footMap);
			}
			
			return map;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：营业日明细--结算方式统计
	 * @author 马振
	 * 创建时间：2015-4-23 上午11:25:27
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Map<String, Object>  queryYingYeRibaoJsfs(PublicEntity condition) throws CRUDException{
		try {
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			condition.setPubgrp(FormatDouble.StringCodeReplace(condition.getPubgrp()));
			condition.setVcode(FormatDouble.StringCodeReplace(condition.getVcode()));
			List<Map<String, Object>>  tdMapList = businessAnalysisMapper.queryYingYeRibaoJsfs(condition);
			Map<String, Object>  tdSumMap = businessAnalysisMapper.queryYingYeRibaoJsfsSum(condition);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("tdMapList", tdMapList);
			map.put("tdSumMap", tdSumMap);
			return map;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：当日查询——营业日明细    实收
	 * @author 马振
	 * 创建时间：2016-3-7 下午1:10:15
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Map<String, Object>  queryYingYeRibaoZffsSs(PublicEntity condition) throws CRUDException{
		try {
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			List<Map<String, Object>>  tdMapList = businessAnalysisMapper.queryYingYeRibaoZffsSs(condition);
			Map<String, Object>  tdSumMap = businessAnalysisMapper.queryYingYeRibaoZffsSsSum(condition);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("tdMapList", tdMapList);
			map.put("tdSumMap", tdSumMap);
			return map;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：收银员统计
	 * @author 马振
	 * 创建时间：2015-4-24 下午1:10:56
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Object queryCashierStatistics(PublicEntity condition)  throws CRUDException {
		try {
			Page pager = condition.getPager();
			
			reportObject.setRows(pageManager.selectPage(condition, pager, MisBohBusinessAnalysisMapper.class.getName()+".queryCashierStatistics"));
			reportObject.setFooter(businessAnalysisMapper.queryCalCashierStatistics(condition));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：台位活动明细报表
	 * @author 马振
	 * 创建时间：2015-7-24 下午3:32:28
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String, Object>> findActmByTbl(PublicEntity condition) throws CRUDException {
		try {
			Page pager = condition.getPager();
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			
			//需要格式化的字段
			String str = "NMONEY,ACTYM,ACTJS,NOVERMONEY";
			
			reportObject.setRows(MisUtil.formatNumForListResult(pageManager.selectPage(condition, pager, MisBohBusinessAnalysisMapper.class.getName()+".findActmByTbl"), str, 2));
			reportObject.setFooter(MisUtil.formatNumForListResult(businessAnalysisMapper.findActmByTblSUM(condition), str, 2));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述： 支付方式明细及其合计
	 * @author 马振
	 * 创建时间：2016年4月19日下午7:50:04
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> queryPayDetail(PublicEntity condition) throws CRUDException{
		try{
			Page pager = condition.getPager();
			
			reportObject.setRows(pageManager.selectPage(condition, pager, MisBohBusinessAnalysisMapper.class.getName() + ".queryPayDetail"));
			reportObject.setFooter(businessAnalysisMapper.queryCalPayDetail(condition));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
}