package com.choice.misboh.service.reportMis;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.misboh.persistence.reportMis.SupplyAcctMisBohMapper;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.AcctMapper;
@Service
public class WzZongheJinchubiaoMisBohService {

	@Autowired
	private SupplyAcctMisBohMapper wzZongheJinchubiaoMisBohMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired
	private AcctMapper acctMapper;
	
	/**
	 * 物资综合进出表
	 * @param conditions
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findSupplySumInOut(Map<String,Object> conditions) throws CRUDException{
		try{
			SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
			conditions.put("yearr", acctMapper.findYearrByDate(sa.getBdat()));
			List<Map<String,Object>> foot = wzZongheJinchubiaoMisBohMapper.findCalForSupplySumInOut(conditions);
			mapReportObject.setRows(wzZongheJinchubiaoMisBohMapper.findSupplySumInOut(conditions));
			mapReportObject.setFooter(foot);
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
}
