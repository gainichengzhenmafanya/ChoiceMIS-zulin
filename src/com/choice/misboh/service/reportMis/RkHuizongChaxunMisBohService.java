package com.choice.misboh.service.reportMis;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.misboh.persistence.reportMis.SupplyAcctMisBohMapper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;

@Service
public class RkHuizongChaxunMisBohService {

	@Autowired
	private SupplyAcctMisBohMapper rkHuizongChaxunMisBohMapper;
	@Autowired
	private ReportObject<Map<String,Object>> reportObject;
	@Autowired
	private PageManager<Map<String,Object>> pageManager;
	
	/**
	 * 入库汇总查询
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<Map<String,Object>> findChkinSumQuery(Map<String,Object> conditions,Page page) throws CRUDException{
		try{
			SupplyAcct sa = (SupplyAcct)conditions.get("supplyAcct");
			//判断供应商
			if(null != sa && null != sa.getDelivercode() && !"".equals(sa.getDelivercode())){
				sa.setDelivercode(CodeHelper.replaceCode(sa.getDelivercode()));
			}
			//判断单据类型
			if(null != sa && null != sa.getChktyp() && !"".equals(sa.getChktyp())){
				sa.setChktyp(CodeHelper.replaceCode(sa.getChktyp()));
			}
			List<Map<String,Object>> foot = rkHuizongChaxunMisBohMapper.findCalForChkinSumQuery(conditions);
			reportObject.setRows(pageManager.selectPage(conditions, page, SupplyAcctMisBohMapper.class.getName()+".findChkinSumQuery"));
			reportObject.setFooter(foot);
			reportObject.setTotal(page.getCount());
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
}
