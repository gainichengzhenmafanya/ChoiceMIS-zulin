package com.choice.misboh.service.reportMis;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.misboh.persistence.reportMis.CostItemMisBohMapper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.ana.PtivityAna;
import com.choice.scm.persistence.AcctMapper;

@Service
public class WzWanyuanyongliangFenxiMisBohService {

	@Autowired 
	private PageManager<Map<String,Object>> pageManager;
	@Autowired
	private ReportObject<Map<String,Object>> reportObject;
	@Autowired
	private CostItemMisBohMapper wzWanyuanyongliangFenxiMisBohMapper;
	@Autowired
	private AcctMapper acctMapper;
 
	/**
	 * 统计物资万元用量
	 * @param ptivity
	 * @return
	 */
	public String calWzWanyuanyongliangFenxi(PtivityAna ptivity) throws CRUDException{
		try{
			ptivity.setYearr(acctMapper.findYearrByDate(ptivity.getBdat()));
			wzWanyuanyongliangFenxiMisBohMapper.callScmWzWanyuanyongliangTemp(ptivity);
			return "1";
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 物资万元用量
	 * @param ptivity
	 * @param page
	 * @return
	 */
	public ReportObject<Map<String,Object>> findWzWanyuanyongliangFenxi(PtivityAna ptivity,Page page) throws CRUDException{
		try{
			ptivity.setYearr(acctMapper.findYearrByDate(ptivity.getBdat()));
			ptivity.setPositn(CodeHelper.replaceCode(ptivity.getPositn()));
			List<Map<String,Object>> foot = wzWanyuanyongliangFenxiMisBohMapper.findWzWanyuanyongliangFenxiSum(ptivity);
			if(null == page){
				reportObject.setRows(wzWanyuanyongliangFenxiMisBohMapper.findWzWanyuanyongliangFenxi(ptivity));
			}else{
				reportObject.setRows(pageManager.selectPage(ptivity, page, CostItemMisBohMapper.class.getName()+".findWzWanyuanyongliangFenxi"));
				reportObject.setTotal(page.getCount());
			}
			reportObject.setFooter(foot);
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
}