package com.choice.misboh.service.reportMis;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.misboh.persistence.reportMis.SupplyAcctMisBohMapper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
@Service
public class CkHuizongChaxunMisBohService {

	@Autowired
	private SupplyAcctMisBohMapper ckHuizongChaxunMisBohMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	
	/**
	 * 出库汇总查询
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<Map<String,Object>> findChkoutSumQuery(Map<String,Object> conditions,Page page) throws CRUDException{
		try{
			SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
			sa.setChktyp(CodeHelper.replaceCode(sa.getChktyp()));
			sa.setFirm(CodeHelper.replaceCode(sa.getFirm()));
			List<Map<String,Object>> foot = ckHuizongChaxunMisBohMapper.findCalForChkout(conditions);
			if("true".equals(conditions.get("bycomp")))
				mapReportObject.setRows(mapPageManager.selectPage(conditions, page, SupplyAcctMisBohMapper.class.getName()+".findChkoutSQComp"));
			else
				mapReportObject.setRows(mapPageManager.selectPage(conditions, page, SupplyAcctMisBohMapper.class.getName()+".findChkoutSumQuery"));
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(page.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	/**
	 * 出库汇总查询1
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<Map<String,Object>> findChkoutSumQuery1(Map<String,Object> conditions,Page page) throws CRUDException{
		try{
			SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
			sa.setChktyp(CodeHelper.replaceCode(sa.getChktyp()));
			sa.setFirm(CodeHelper.replaceCode(sa.getFirm()));
			List<Map<String,Object>> foot = ckHuizongChaxunMisBohMapper.findCalForChkout(conditions);
			mapReportObject.setRows(mapPageManager.selectPage(conditions, page, SupplyAcctMisBohMapper.class.getName()+".findChkoutSumQuery1"));
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(page.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
}
