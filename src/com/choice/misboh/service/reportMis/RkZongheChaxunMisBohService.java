package com.choice.misboh.service.reportMis;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.reportMis.SupplyAcctMisMapper;

@Service
public class RkZongheChaxunMisBohService {

	@Autowired
	private SupplyAcctMisMapper rkZongheChaxunMisMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	
	/**
	 * 入库综合查询
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<Map<String,Object>> findAllChkinmZh(Map<String,Object> conditions,Page page) throws CRUDException{
		try{
			SupplyAcct supplyAcct = (SupplyAcct) conditions.get("supplyAcct");
			if(supplyAcct.getChktyp() != null && !"".equals(supplyAcct.getChktyp())){
				supplyAcct.setChktyp(CodeHelper.replaceCode(supplyAcct.getChktyp()));
			}
			if(supplyAcct.getDelivercode() != null && !supplyAcct.getDelivercode().equals("")){
				supplyAcct.setDelivercode(CodeHelper.replaceCode(supplyAcct.getDelivercode()));
			}
			List<Map<String,Object>> foot = rkZongheChaxunMisMapper.findAllChkinmSum(conditions);
			
			if(conditions.get("querytype").equals("3")){
				mapReportObject.setRows(mapPageManager.selectPage(conditions, page, SupplyAcctMisMapper.class.getName()+".findAllChkinmTypSum"));
			}else if(conditions.get("querytype").equals("2")){
				mapReportObject.setRows(mapPageManager.selectPage(conditions, page, SupplyAcctMisMapper.class.getName()+".findAllChkinmSumQuery"));
			}else if(conditions.get("querytype").equals("1")){
				mapReportObject.setRows(mapPageManager.selectPage(conditions, page, SupplyAcctMisMapper.class.getName()+".findAllChkinmDetailQuery"));
			}
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(page.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
		//导出Excel 
		public boolean exportExcel(OutputStream os,List<SupplyAcct> list) {   
			WritableWorkbook workBook = null;
			try {
				workBook = Workbook.createWorkbook(os);
				WritableSheet sheet = workBook.createSheet("Sheet1", 0);
	            sheet.addCell(new Label(0,0,"配送中心到货审核"));
	            //设置表格表头
	            sheet.addCell(new Label(0, 2, "单号"));   
				sheet.addCell(new Label(1, 2, "编码"));  
				sheet.addCell(new Label(2, 2, "名称"));   
				sheet.addCell(new Label(3, 2, "规格"));  
				sheet.addCell(new Label(4, 2, "标准单位"));   
				sheet.addCell(new Label(5, 2, "参考单位"));  
				sheet.addCell(new Label(6, 2, "单价"));   
				sheet.addCell(new Label(7, 2, "配送数量"));  
				sheet.addCell(new Label(8, 2, "配送数量2"));   
				sheet.addCell(new Label(9, 2, "验货数量"));  
				sheet.addCell(new Label(10, 2, "验货数量2"));   
				sheet.addCell(new Label(11, 2, "金额"));  
				sheet.addCell(new Label(12, 2, "验货比率"));   
				sheet.addCell(new Label(13, 2, "分店亏损"));  
				sheet.addCell(new Label(14, 2, "中心亏损"));   
				sheet.addCell(new Label(15, 2, "备注"));
	            sheet.mergeCells(0, 0, list.size()-1, 0);
	            //遍历list填充表格内容
	            for(int i=0;i<list.size();i++) {
	            	sheet.addCell(new Label(0, i+3, String.valueOf(list.get(i).getChkno())));
	            	sheet.addCell(new Label(1, i+3, list.get(i).getSp_code()));
	            	sheet.addCell(new Label(2, i+3, list.get(i).getSp_name()));
	            	sheet.addCell(new Label(3, i+3, list.get(i).getSpdesc()));
	            	sheet.addCell(new Label(4, i+3, list.get(i).getUnit()));
		    		sheet.addCell(new Label(5, i+3, list.get(i).getUnit1()));
		    		sheet.addCell(new Label(6, i+3, String.valueOf(list.get(i).getPriceout())));
		    		sheet.addCell(new Label(7, i+3, String.valueOf(list.get(i).getCntout())));
		    		sheet.addCell(new Label(8, i+3, String.valueOf(list.get(i).getCntout1())));
		    		sheet.addCell(new Label(9, i+3, String.valueOf(list.get(i).getCntfirm())));
		    		sheet.addCell(new Label(10, i+3, String.valueOf(list.get(i).getCntfirm1())));
		    		sheet.addCell(new Label(11, i+3, String.valueOf(list.get(i).getAmount())));
		    		sheet.addCell(new Label(12, i+3, String.valueOf(list.get(i).getSupply().getAccprate())));
		    		sheet.addCell(new Label(13, i+3, String.valueOf(list.get(i).getCntfirmks())));
		    		sheet.addCell(new Label(14, i+3, String.valueOf(list.get(i).getCntlogistks())));
		    		sheet.addCell(new Label(15, i+3, list.get(i).getDes()));
		    		}
	            workBook.write();
	            os.flush();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (RowsExceededException e) {
				e.printStackTrace();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}catch (Exception e) {
				e.printStackTrace();
			}finally{
				try {
					workBook.close();
					os.close();
				} catch (WriteException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return true;
		}	
}
