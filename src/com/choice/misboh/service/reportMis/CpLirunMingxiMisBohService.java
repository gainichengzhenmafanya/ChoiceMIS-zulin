package com.choice.misboh.service.reportMis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.ChartsXml;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.CreateChart;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.persistence.reportMis.CostItemMisBohMapper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;

@Service
public class CpLirunMingxiMisBohService {

	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	@Autowired
	private CostItemMisBohMapper cpLirunMingxiMisBohMapper;
	
	private final transient Log log = LogFactory.getLog(CostItemMisBohMapper.class);

	/**
	 * 分店菜品利润明细
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<Map<String,Object>> findFirmFoodProfit(Map<String,Object> conditions,Page page) throws CRUDException{
		try {
			SupplyAcct supplyAcct = (SupplyAcct) conditions.get("supplyAcct");
			if(null!=supplyAcct && !"".equals(supplyAcct)){
				if(null!=supplyAcct.getFirm()&&!"".equals(supplyAcct.getFirm())){
					supplyAcct.setFirm(CodeHelper.replaceCode(supplyAcct.getFirm()));
				}
				if(null!=supplyAcct.getTyp()&&!"".equals(supplyAcct.getTyp())){
					supplyAcct.setTyp(CodeHelper.replaceCode(supplyAcct.getTyp()));
				}
			}
			mapReportObject.setRows(mapPageManager.selectPage(conditions, page, CostItemMisBohMapper.class.getName()+".findFirmFoodProfit"));
			List<Map<String,Object>> foot = cpLirunMingxiMisBohMapper.findCalForFirmFoodProfit(conditions);
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(page.getCount());
			return mapReportObject;
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	/**
	 * 根据物资编码查询菜品成本明细  -- 主表
	 * @param sp_code
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String,Object>> findCostdtlm(Map<String,Object> map) throws CRUDException{
		return cpLirunMingxiMisBohMapper.findCostdtlm(map);
	}
	/**
	 * 根据物资编码查询菜品成本明细  -- 子表
	 * @param sp_code
	 * @return
	 */
	public List<Map<String,Object>> findCostdtl(Map<String,Object> map) throws CRUDException{
		return cpLirunMingxiMisBohMapper.findCostdtl(map);
	}
	/**
	 * 菜品成本组成物资明细
	 * @param map
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String,Object>> findCostDetail(Map<String,Object> map) throws CRUDException{
		return cpLirunMingxiMisBohMapper.findCostDetail(map);
	}
	/**
	 * 菜品销售成本利润走势分析
	 * @param map
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String,Object>> findSaleCostProfitTrends(Map<String,Object> map) throws CRUDException{
		return cpLirunMingxiMisBohMapper.findSaleCostProfitTrends(map);
	}
	
	/**
	 * 菜品销售成本利润走势分析--图表
	 * @param response
	 * @param map
	 * @return
	 * @throws CRUDException
	 */
	public ChartsXml findXmlForSaleCostProfitTrends(HttpServletResponse response,Map<String,Object> map) throws CRUDException{
		try{
			List<Map<String,Object>> list = cpLirunMingxiMisBohMapper.findSaleCostProfitTrends(map);
			List<String> xNameList = new ArrayList<String>();
			List<String> typeList = new ArrayList<String>();
			List<Object[]> dataList = new ArrayList<Object[]>();
			Object[] cntObj = new Object[list.size()];
			Object[] amtObj = new Object[list.size()];
			Object[] costObj = new Object[list.size()];
			Object[] maoliObj = new Object[list.size()];
			Object[] maolilvObj = new Object[list.size()];
			for(int i=0;i<list.size();i++){
				xNameList.add(DateFormat.getStringByDate(DateFormat.getDateByString(list.get(i).get("DAT").toString(), "yyyy-MM-dd"), "yyyy-MM-dd"));
				cntObj[i] = list.get(i).get("CNT").toString();
				amtObj[i] = list.get(i).get("AMT").toString();
				costObj[i] = list.get(i).get("COST").toString();
				maoliObj[i] = list.get(i).get("MAOLI").toString();
				maolilvObj[i] = list.get(i).get("MAOLILV").toString();
			}
			typeList.add("销售金额");
			typeList.add("物资成本");
			typeList.add("利润");
			typeList.add("销售数量");
			typeList.add("利润率");
			
			dataList.add(amtObj);
			dataList.add(costObj);
			dataList.add(maoliObj);
			dataList.add(cntObj);
			dataList.add(maolilvObj);
			return CreateChart.createMSHistogram(response, "菜品销售成本利润走势分析", xNameList, typeList, "", dataList, null);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 菜品成本区间分析
	 * @param map
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String,Object>> findCostIntervalList(Map<String,Object> map) throws CRUDException{
		return cpLirunMingxiMisBohMapper.findCostIntervalList(map);
	}
	
	/**
	 * 菜品成本区间分析--图表
	 * @param response
	 * @param map
	 * @return
	 * @throws CRUDException
	 */
	public ChartsXml findXmlCostInterval(HttpServletResponse response,Map<String,Object> map) throws CRUDException{
		try{
			String value;
			List<Map<String,Object>> list = cpLirunMingxiMisBohMapper.findCostIntervalList(map);
			List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
			Map<String,Object> dataMap0 = new HashMap<String,Object>();
			dataMap0.put("name","0-5");
			value=list.get(0).get("BAIFENBI").toString().substring(0,list.get(0).get("BAIFENBI").toString().indexOf("%"));
			dataMap0.put("value",value);
			dataMap0.put("hoverText","成本:"+list.get(0).get("COSTHEJI").toString()+"营业额:"+list.get(0).get("INFASHENGE").toString());
			dataList.add(dataMap0);
			
			Map<String,Object> dataMap1 = new HashMap<String,Object>();
			dataMap1.put("name","5-10");
			value=list.get(1).get("BAIFENBI").toString().substring(0,list.get(1).get("BAIFENBI").toString().indexOf("%"));
			dataMap1.put("value",value);
			dataMap1.put("hoverText","成本:"+list.get(1).get("COSTHEJI").toString()+"营业额:"+list.get(1).get("INFASHENGE").toString());
			dataList.add(dataMap1);
			
			Map<String,Object> dataMap2 = new HashMap<String,Object>();
			dataMap2.put("name","10-20");
			value=list.get(2).get("BAIFENBI").toString().substring(0,list.get(2).get("BAIFENBI").toString().indexOf("%"));
			dataMap2.put("value",value);
			dataMap2.put("hoverText","成本:"+list.get(2).get("COSTHEJI").toString()+"营业额:"+list.get(2).get("INFASHENGE").toString());
			dataList.add(dataMap2);
			
			Map<String,Object> dataMap3 = new HashMap<String,Object>();
			dataMap3.put("name","20-40");
			value=list.get(3).get("BAIFENBI").toString().substring(0,list.get(3).get("BAIFENBI").toString().indexOf("%"));
			dataMap3.put("value",value);
			dataMap3.put("hoverText","成本:"+list.get(3).get("COSTHEJI").toString()+"营业额:"+list.get(3).get("INFASHENGE").toString());
			dataList.add(dataMap3);

			Map<String,Object> dataMap4 = new HashMap<String,Object>();
			dataMap4.put("name","40-60");
			value=list.get(4).get("BAIFENBI").toString().substring(0,list.get(4).get("BAIFENBI").toString().indexOf("%"));
			dataMap4.put("value",list.get(4).get("BAIFENBI").toString());
			dataMap4.put("hoverText","成本:"+list.get(4).get("COSTHEJI").toString()+"营业额:"+list.get(4).get("INFASHENGE").toString());
			dataList.add(dataMap4);
			
			Map<String,Object> dataMap5 = new HashMap<String,Object>();
			dataMap5.put("name","60-80");
			value=list.get(5).get("BAIFENBI").toString().substring(0,list.get(5).get("BAIFENBI").toString().indexOf("%"));
			dataMap5.put("value",value);
			dataMap5.put("hoverText","成本:"+list.get(5).get("COSTHEJI").toString()+"营业额:"+list.get(5).get("INFASHENGE").toString());
			dataList.add(dataMap5);
			
			Map<String,Object> dataMap6 = new HashMap<String,Object>();
			dataMap6.put("name","80-100");
			value=list.get(6).get("BAIFENBI").toString().substring(0,list.get(6).get("BAIFENBI").toString().indexOf("%"));
			dataMap6.put("value",value);
			dataMap6.put("hoverText","成本:"+list.get(6).get("COSTHEJI").toString()+"营业额:"+list.get(6).get("INFASHENGE").toString());
			dataList.add(dataMap6);
			
			Map<String,Object> dataMap7 = new HashMap<String,Object>();
			dataMap7.put("name","100-150");
			value=list.get(7).get("BAIFENBI").toString().substring(0,list.get(7).get("BAIFENBI").toString().indexOf("%"));
			dataMap7.put("value",value);
			dataMap7.put("hoverText","成本:"+list.get(7).get("COSTHEJI").toString()+"营业额:"+list.get(7).get("INFASHENGE").toString());
			dataList.add(dataMap7);
			
			Map<String,Object> dataMap8 = new HashMap<String,Object>();
			dataMap8.put("name","150-200");
			value=list.get(8).get("BAIFENBI").toString().substring(0,list.get(8).get("BAIFENBI").toString().indexOf("%"));
			dataMap8.put("value",value);
			dataMap8.put("hoverText","成本:"+list.get(8).get("COSTHEJI").toString()+"营业额:"+list.get(8).get("INFASHENGE").toString());
			dataList.add(dataMap8);
			
			Map<String,Object> dataMap9 = new HashMap<String,Object>();
			dataMap9.put("name","200-300");
			value=list.get(9).get("BAIFENBI").toString().substring(0,list.get(9).get("BAIFENBI").toString().indexOf("%"));
			dataMap9.put("value",value);
			dataMap9.put("hoverText","成本:"+list.get(9).get("COSTHEJI").toString()+"营业额:"+list.get(9).get("INFASHENGE").toString());
			dataList.add(dataMap9);
			
			Map<String,Object> dataMap10 = new HashMap<String,Object>();
			dataMap10.put("name","300-500");
			value=list.get(10).get("BAIFENBI").toString().substring(0,list.get(10).get("BAIFENBI").toString().indexOf("%"));
			dataMap10.put("value",value);
			dataMap10.put("hoverText","成本:"+list.get(10).get("COSTHEJI").toString()+"营业额:"+list.get(10).get("INFASHENGE").toString());
			dataList.add(dataMap10);
			
			Map<String,Object> dataMap11 = new HashMap<String,Object>();
			dataMap11.put("name","500-700");
			value=list.get(11).get("BAIFENBI").toString().substring(0,list.get(11).get("BAIFENBI").toString().indexOf("%"));
			dataMap11.put("value",value);
			dataMap11.put("hoverText","成本:"+list.get(11).get("COSTHEJI").toString()+"营业额:"+list.get(11).get("INFASHENGE").toString());
			dataList.add(dataMap11);
			
			Map<String,Object> dataMap12 = new HashMap<String,Object>();
			dataMap12.put("name","700-1000");
			value=list.get(12).get("BAIFENBI").toString().substring(0,list.get(12).get("BAIFENBI").toString().indexOf("%"));
			dataMap12.put("value",value);
			dataMap12.put("hoverText","成本:"+list.get(12).get("COSTHEJI").toString()+"营业额:"+list.get(12).get("INFASHENGE").toString());
			dataList.add(dataMap12);
			
			Map<String,Object> dataMap13 = new HashMap<String,Object>();
			dataMap13.put("name","1000以上");
			value=list.get(13).get("BAIFENBI").toString().substring(0,list.get(13).get("BAIFENBI").toString().indexOf("%"));
			dataMap13.put("value",value);
			dataMap13.put("hoverText","成本:"+list.get(13).get("COSTHEJI").toString()+"营业额:"+list.get(13).get("INFASHENGE").toString());
			dataList.add(dataMap13);
			return CreateChart.createDoughnut2D(response, "菜品成本区间分析", dataList, null);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 菜品毛利率区间分析
	 * @param map
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String,Object>> findMaolilvIntervalList(Map<String,Object> map) throws CRUDException{
		return cpLirunMingxiMisBohMapper.findMaolilvIntervalList(map);
	}
	
	/**
	 * 菜品毛利率区间分析--图表
	 * @param response
	 * @param map
	 * @return
	 * @throws CRUDException
	 */
	public ChartsXml findXmlMaolilvInterval(HttpServletResponse response,Map<String,Object> map) throws CRUDException{
		try{
			List<Map<String,Object>> list = cpLirunMingxiMisBohMapper.findMaolilvIntervalList(map);
			List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
			Map<String,Object> dataMap0 = new HashMap<String,Object>();
			dataMap0.put("name","0以下");
			dataMap0.put("value",list.get(0).get("BAIFENBI").toString().substring(0,list.get(0).get("BAIFENBI").toString().indexOf("%")));
			dataMap0.put("hoverText","成本:"+list.get(0).get("COSTHEJI").toString()+"营业额:"+list.get(0).get("INFASHENGE").toString());
			dataList.add(dataMap0);
			
			Map<String,Object> dataMap1 = new HashMap<String,Object>();
			dataMap1.put("name","0-10");
			dataMap1.put("value",list.get(1).get("BAIFENBI").toString().substring(0,list.get(1).get("BAIFENBI").toString().indexOf("%")));
			dataMap1.put("hoverText","成本:"+list.get(1).get("COSTHEJI").toString()+"营业额:"+list.get(1).get("INFASHENGE").toString());
			dataList.add(dataMap1);
			
			Map<String,Object> dataMap2 = new HashMap<String,Object>();
			dataMap2.put("name","10-20");
			dataMap2.put("value",list.get(2).get("BAIFENBI").toString().substring(0,list.get(2).get("BAIFENBI").toString().indexOf("%")));
			dataMap2.put("hoverText","成本:"+list.get(2).get("COSTHEJI").toString()+"营业额:"+list.get(2).get("INFASHENGE").toString());
			dataList.add(dataMap2);
			
			Map<String,Object> dataMap3 = new HashMap<String,Object>();
			dataMap3.put("name","20-30");
			dataMap3.put("value",list.get(3).get("BAIFENBI").toString().substring(0,list.get(3).get("BAIFENBI").toString().indexOf("%")));
			dataMap3.put("hoverText","成本:"+list.get(3).get("COSTHEJI").toString()+"营业额:"+list.get(3).get("INFASHENGE").toString());
			dataList.add(dataMap3);

			Map<String,Object> dataMap4 = new HashMap<String,Object>();
			dataMap4.put("name","30-40");
			dataMap4.put("value",list.get(4).get("BAIFENBI").toString().substring(0,list.get(4).get("BAIFENBI").toString().indexOf("%")));
			dataMap4.put("hoverText","成本:"+list.get(4).get("COSTHEJI").toString()+"营业额:"+list.get(4).get("INFASHENGE").toString());
			dataList.add(dataMap4);
			
			Map<String,Object> dataMap5 = new HashMap<String,Object>();
			dataMap5.put("name","40-50");
			dataMap5.put("value",list.get(5).get("BAIFENBI").toString().substring(0,list.get(5).get("BAIFENBI").toString().indexOf("%")));
			dataMap5.put("hoverText","成本:"+list.get(5).get("COSTHEJI").toString()+"营业额:"+list.get(5).get("INFASHENGE").toString());
			dataList.add(dataMap5);
			
			Map<String,Object> dataMap6 = new HashMap<String,Object>();
			dataMap6.put("name","50-60");
			dataMap6.put("value",list.get(6).get("BAIFENBI").toString().substring(0,list.get(6).get("BAIFENBI").toString().indexOf("%")));
			dataMap6.put("hoverText","成本:"+list.get(6).get("COSTHEJI").toString()+"营业额:"+list.get(6).get("INFASHENGE").toString());
			dataList.add(dataMap6);
			
			Map<String,Object> dataMap7 = new HashMap<String,Object>();
			dataMap7.put("name","60-70");
			dataMap7.put("value",list.get(7).get("BAIFENBI").toString().substring(0,list.get(7).get("BAIFENBI").toString().indexOf("%")));
			dataMap7.put("hoverText","成本:"+list.get(7).get("COSTHEJI").toString()+"营业额:"+list.get(7).get("INFASHENGE").toString());
			dataList.add(dataMap7);
			
			Map<String,Object> dataMap8 = new HashMap<String,Object>();
			dataMap8.put("name","70-80");
			dataMap8.put("value",list.get(8).get("BAIFENBI").toString().substring(0,list.get(8).get("BAIFENBI").toString().indexOf("%")));
			dataMap8.put("hoverText","成本:"+list.get(8).get("COSTHEJI").toString()+"营业额:"+list.get(8).get("INFASHENGE").toString());
			dataList.add(dataMap8);
			
			Map<String,Object> dataMap9 = new HashMap<String,Object>();
			dataMap9.put("name","80-90");
			dataMap9.put("value",list.get(9).get("BAIFENBI").toString().substring(0,list.get(9).get("BAIFENBI").toString().indexOf("%")));
			dataMap9.put("hoverText","成本:"+list.get(9).get("COSTHEJI").toString()+"营业额:"+list.get(9).get("INFASHENGE").toString());
			dataList.add(dataMap9);
			
			Map<String,Object> dataMap10 = new HashMap<String,Object>();
			dataMap10.put("name","90-100");
			dataMap10.put("value",list.get(10).get("BAIFENBI").toString().substring(0,list.get(10).get("BAIFENBI").toString().indexOf("%")));
			dataMap10.put("hoverText","成本:"+list.get(10).get("COSTHEJI").toString()+"营业额:"+list.get(10).get("INFASHENGE").toString());
			dataList.add(dataMap10);
			
			Map<String,Object> dataMap11 = new HashMap<String,Object>();
			dataMap11.put("name","100以上");
			dataMap11.put("value",list.get(11).get("BAIFENBI").toString().substring(0,list.get(11).get("BAIFENBI").toString().indexOf("%")));
			dataMap11.put("hoverText","成本:"+list.get(11).get("COSTHEJI").toString()+"营业额:"+list.get(11).get("INFASHENGE").toString());
			dataList.add(dataMap11);
			
			return CreateChart.createDoughnut2D(response, "菜品毛利率区间分析", dataList, null);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}
