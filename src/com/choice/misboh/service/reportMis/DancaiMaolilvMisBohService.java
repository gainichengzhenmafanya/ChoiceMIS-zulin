package com.choice.misboh.service.reportMis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.misboh.persistence.reportMis.CostItemMisBohMapper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;

@Service
public class DancaiMaolilvMisBohService {

	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	@Autowired
	private CostItemMisBohMapper dancaiMaolilvMisBohMapper;
	
	private final transient Log log = LogFactory.getLog(CostItemMisBohMapper.class);

	/**
	 * 分店菜品利润明细
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<Map<String,Object>> findDancaiMaolilv(Map<String,Object> conditions,Page page) throws CRUDException{
		try {
			List<Map<String,Object>> list = mapPageManager.selectPage(conditions, page, CostItemMisBohMapper.class.getName()+".findDancaiMaolilv");
			List<Map<String,Object>> list1 = dancaiMaolilvMisBohMapper.findDancaiMaolilvOut(conditions);
			List<Map<String,Object>> list2 = dancaiMaolilvMisBohMapper.findDancaiMaolilvCost(conditions);
			//循环便利比较数据，得到实际耗用合计
			String item = "";
			double amtout = 0.0;
			if(list.size() > 0)
				item = list.get(0).get("ITCODE")==null ? "" : list.get(0).get("ITCODE").toString();
			Map<String,Double> itmap = new HashMap<String,Double>();//放耗用合计
			for(int i=0;i<list.size();i++){
				Map<String,Object> map = list.get(i);
				if (map.containsKey("ITCODE")){
					if (null != map.get("ITCODE")){
						String positn = map.get("DEPT").toString();
						String sp_code = map.get("SP_CODE").toString();
						double llcnt = Double.parseDouble(map.get("LLCNT").toString());
						for(Map<String,Object> map1 : list1){
							String positn1 = map1.get("POSITN").toString();
							String sp_code1 = map1.get("SP_CODE").toString();
							if(positn.equals(positn1) && sp_code.equals(sp_code1)){
								double cntout = Double.parseDouble(map1.get("CNTOUT").toString());
								double amtoutt = Double.parseDouble(map1.get("AMTOUT").toString());
								map.put("SJCNT", Math.round(cntout*100.0)/100.0);
								map.put("SJAMT", Math.round(amtoutt*100.0)/100.0);
								map.put("YLCCL", Math.round(cntout == 0 ? 0 : llcnt/cntout*10000.0)/100.0);
							}
						}
						for(Map<String,Object> map2 : list2){
							String positn2 = map2.get("POSITN").toString();
							String sp_code2 = map2.get("SP_CODE").toString();
							if(positn.equals(positn2) && sp_code.equals(sp_code2)){
								double cnt = Double.parseDouble(map2.get("CNT").toString());
								double sjcnt = Double.parseDouble(map.get("SJCNT").toString());
								double price = sjcnt == 0 ? 0 : Double.parseDouble(map.get("SJAMT").toString())/sjcnt;
								if(cnt != 0){
									sjcnt = sjcnt*llcnt/cnt;
									map.put("SJCNT", Math.round(sjcnt*100.0)/100.0);
									map.put("SJAMT", Math.round(sjcnt*price*100.0)/100.0);
									map.put("YLCCL", Math.round(sjcnt == 0 ? 0 : llcnt/sjcnt*10000.0)/100.0);
								}
							}
						}
						if (item.equals(map.get("ITCODE").toString())){
							amtout += Double.parseDouble(map.get("SJAMT").toString());
							if(i == list.size()-1)
								itmap.put(item, amtout);
							continue;
						}
					}
				}
				if(i != 0)
					itmap.put(item, amtout);
				amtout = 0.0;
				item = map.get("ITCODE")==null ? "" : map.get("ITCODE").toString();
			}
			List<Map<String,Object>> rs = new ArrayList<Map<String,Object>>();
			String vcodejs="";
			//循环便利比较数据，加入同一物资，则合并显示
			for(int i=0;i<list.size();i++){
				Map<String,Object> map = list.get(i);
				rs.add(map);
				if (map.containsKey("ITCODE")){
					if (null != map.get("ITCODE")){
						if (vcodejs.equals(map.get("ITCODE").toString())){
							map.put("ITCODE", "");
							map.put("ITDES", "");
							map.put("ITUNIT", "");
							map.put("ITCNT", "");
							map.put("ITAMT", "");
							map.put("DCMLL", "");
							continue;
						}
					}
				}
				vcodejs=map.get("ITCODE")==null?"":map.get("ITCODE").toString();
				if("西贝莜面".equals(conditions.get("acctDes").toString()))
					map.put("DCMLL", Math.round((Double.parseDouble(map.get("ITAMT").toString())/1.06 - itmap.get(vcodejs))
							/Double.parseDouble(map.get("ITAMT").toString())*1.06*10000.0)/100.0);
				else
					map.put("DCMLL", Math.round((Double.parseDouble(map.get("ITAMT").toString()) - itmap.get(vcodejs))/Double.parseDouble(map.get("ITAMT").toString())*10000.0)/100.0);
			}
			mapReportObject.setRows(rs);
//			List<Map<String,Object>> foot = dancaiMaolilvMisBohMapper.findCalForDancaiMaolilv(conditions);
//			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(page.getCount());
			return mapReportObject;
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
}
