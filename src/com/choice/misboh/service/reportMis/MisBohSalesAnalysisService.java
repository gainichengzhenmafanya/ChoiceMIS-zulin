package com.choice.misboh.service.reportMis;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.commonutil.MisUtil;
import com.choice.misboh.commonutil.Total;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.domain.reportMis.PublicEntity;
import com.choice.misboh.domain.reportMis.ReportObject;
import com.choice.misboh.persistence.reportMis.MisBohSaleAnalysisMapper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * 描述：销售分析下的报表service
 * @author 马振
 * 创建时间：2015-4-23 下午1:49:47
 */
@Service
public class MisBohSalesAnalysisService {

	private static Logger log = Logger.getLogger(MisBohSalesAnalysisService.class);
	
	@Autowired
	private ReportObject<Map<String,Object>> reportObject;
	
	@Autowired
	private MisBohSaleAnalysisMapper saleAnalysisMapper;
	
	@Autowired
	private PageManager<Map<String,Object>> pageManager;
	
	/**
	 * 描述：大类销售报表
	 * @author 马振
	 * 创建时间：2015-4-23 下午2:00:38
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Object listInvoiceType(PublicEntity condition) throws CRUDException {
		try {
			Page pager = condition.getPager();
			
			//当Integer.MAX_VALUE != pager.getPageSize()时，为正常查询，不相等时为Excel导出
			if (Integer.MAX_VALUE != pager.getPageSize()) {
				pager.setNowPage(condition.getPage());
				pager.setPageSize(condition.getRows());
			}
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			
			if(ValueUtil.IsNotEmpty(condition.getEnablestate())){
				condition.setEnablestate(FormatDouble.StringCodeReplace(condition.getEnablestate()));
			}
			
			List<Map<String, Object>> rowList=new ArrayList<Map<String,Object>>();
			List<Map<String, Object>> footList=new ArrayList<Map<String,Object>>();
			
			//判断按哪个类别查询（大类、中类、小类）
			if (condition.getPubgrp().equals("dl")) {
				
				//页面选中查询大类
				rowList = saleAnalysisMapper.listInvoiceType(condition);
				
				//若查出的数据不为0，则采用分页的语句
				if (rowList.size() != 0) {
					rowList = pageManager.selectPage(condition, pager, MisBohSaleAnalysisMapper.class.getName() + ".listInvoiceType");
				}
				
				footList = saleAnalysisMapper.listSumInvoiceType(condition);
			} else if (condition.getPubgrp().equals("zl")){
				
				//选中查询中类
				rowList = saleAnalysisMapper.listInvoiceGrp(condition);
				
				//若查出的数据不为0时，则采用分页的语句
				if (rowList.size() != 0) {
					rowList = pageManager.selectPage(condition, pager, MisBohSaleAnalysisMapper.class.getName() + ".listInvoiceGrp");
				}
				
				footList = saleAnalysisMapper.listSumInvoiceGrp(condition);
			} else {
				
				//选中查询小类
				rowList = saleAnalysisMapper.listInvoiceTyp(condition);
				
				//若查出的数据不为0时，则采用分页的语句
				if (rowList.size() != 0) {
					rowList = pageManager.selectPage(condition, pager, MisBohSaleAnalysisMapper.class.getName() + ".listInvoiceTyp");
				}
				
				footList = saleAnalysisMapper.listSumInvoiceTyp(condition);
			}
			reportObject.setRows(rowList);
			reportObject.setFooter(footList);
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述:菜品销售分类报告
	 * 作者:马振
	 * 时间:2016年9月29日上午10:59:04
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Object listInvoiceType_new(PublicEntity condition) throws CRUDException {
		try {
			Page pager = condition.getPager();
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			
			List<Map<String, Object>> rowList=new ArrayList<Map<String,Object>>();
			List<Map<String, Object>> footList=new ArrayList<Map<String,Object>>();
			//判断按哪个类别查询（大类、中类、小类）
			if(condition.getPubgrp().equals("dl")){//页面选中查询大类
				 condition.setPubgrp("1");
			}else if(condition.getPubgrp().equals("zl")){//选中查询中类
				 condition.setPubgrp("2");
			}else{//选中查询小类
				 condition.setPubgrp("3");
			}
			rowList=pageManager.selectPage(condition, pager, MisBohSaleAnalysisMapper.class.getName()+".listInvoiceType_new");
			footList=saleAnalysisMapper.listSumInvoiceType_new(condition);
			reportObject.setRows(rowList);
			reportObject.setFooter(footList);
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：菜品销售汇总
	 * @author 马振
	 * 创建时间：2015-4-23 下午2:23:13
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Object queryCpxshz(PublicEntity condition) throws CRUDException {
		try {
			Page pager = condition.getPager();
			
			if(ValueUtil.IsNotEmpty(condition.getPk_store())){
				//拼接门店id，进行过滤
				condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			}
			
			if(ValueUtil.IsNotEmpty(condition.getPk_pubitem())){
				//拼菜品编码，进行过滤
				condition.setPk_pubitem(FormatDouble.StringCodeReplace(condition.getPk_pubitem()));
			}
			
			reportObject.setRows(pageManager.selectPage(condition, pager, MisBohSaleAnalysisMapper.class.getName()+".queryCpxshz"));
			reportObject.setFooter(saleAnalysisMapper.queryCalForCpxshz(condition));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述： 菜品销售明细报表
	 * @author 马振
	 * 创建时间：2016年4月12日下午8:34:49
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Object queryPoscybb(PublicEntity condition) throws CRUDException {
		try {
			Page pager = condition.getPager();
			
			//拼接菜品主键，传递查询条件
			if(ValueUtil.IsNotEmpty(condition.getPk_pubitem())){
				condition.setPk_pubitem(FormatDouble.StringCodeReplace(condition.getPk_pubitem()));
			}
			
			String typName = "";
			String cols = "NPRICE,NMONEY,NZMONEY,JAMT";	//保留两位小数的列
			String colName = "MCNAME,VPNAME,NPRICE,VPCODE,SALESSPCODE,PK_PUBITEM,DWORKDATE,PK_STORE";//不合计的列
			List<Map<String, Object>> mapList = new ArrayList<Map<String,Object>>();
			Map<String, Object> mapxj = new HashMap<String, Object>();
			
			//查询出所有的菜品信息
			List<Map<String, Object>> list = pageManager.selectPage(condition, pager, MisBohSaleAnalysisMapper.class.getName() + ".queryPoscybb");
			
			for (int i = 0; i < list.size(); i++) {
				Map<String, Object> map = list.get(i);
				
				for(Iterator<Map.Entry<String, Object>> it = map.entrySet().iterator(); it.hasNext();){
					Map.Entry<String, Object> entry = (Map.Entry<String, Object>)it.next();
					if (colName.indexOf(entry.getKey()) <= -1) {
						mapxj.put(entry.getKey(), null != mapxj.get(entry.getKey()) ? new BigDecimal((Double)mapxj.get(entry.getKey()) + ((BigDecimal)entry.getValue()).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue() : ((BigDecimal)entry.getValue()).doubleValue());
					} else if ("MCNAME".equals(entry.getKey())) {
						mapxj.put("MCNAME", "小计");
					} else if ("NPRICE".equals(entry.getKey())) {
						mapxj.put("NPRICE", "");
					}
				} 
				
				if(null == map.get("MCNAME") || typName == map.get("MCNAME").toString() || typName.equals(map.get("MCNAME").toString())){
					map.put("MCNAME", "");
				}else{
					typName = map.get("MCNAME").toString();
				}
				mapList.add(map);
				
				if (i + 1 <list.size()){
					Map<String, Object> map2 = list.get(i + 1);
					if (!typName.equals(map2.get("MCNAME"))) {
						mapList.add(mapxj);
						mapxj = new HashMap<String, Object>();
					}
				} else {
					mapList.add(mapxj);
				}
			}
			
			reportObject.setRows(MisUtil.formatNumForListResult(mapList, cols, 2));
			reportObject.setFooter(MisUtil.formatNumForListResult(saleAnalysisMapper.queryCalPoscybb(condition), cols, 2));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：退菜明细报表
	 * @author 马振
	 * 创建时间：2015-4-23 下午3:26:39
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Object queryTcmxbb(PublicEntity condition) throws CRUDException{
		try {
			Page pager = condition.getPager();

			//当Integer.MAX_VALUE != pager.getPageSize()时，为正常查询，不相等时为Excel导出
			if (Integer.MAX_VALUE != pager.getPageSize()) {
				pager.setNowPage(condition.getPage());
				pager.setPageSize(condition.getRows());
			}
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			
			String typName = "";
			String colName = "DWORKDATE,DIVISION,SUBDIVISION,VPNAME,RREASON,VBCODE,ORTIME,VENAME,VJENAME";//不合计的列
			List<Map<String, Object>> mapList = new ArrayList<Map<String,Object>>();
			Map<String, Object> mapxj = new HashMap<String, Object>();
			
			//查询退菜明细数据
			List<Map<String,Object>> list = saleAnalysisMapper.queryTcmxbb(condition);
			
			//若查出的数据不为0，则采用分页的语句
			if (list.size() != 0) {
				list = pageManager.selectPage(condition, pager, MisBohSaleAnalysisMapper.class.getName()+".queryTcmxbb");
			}
			
			for(int i=0;i<list.size();i++){
				Map<String, Object> map=list.get(i);
				
				for(Iterator<Map.Entry<String, Object>> it = map.entrySet().iterator();it.hasNext();){
					Map.Entry<String, Object> entry = (Map.Entry<String, Object>)it.next();
					if(colName.indexOf(entry.getKey())<=-1){
						if ("VPCODE".equals(entry.getKey()) || "VPNAME".equals(entry.getKey())) {
							mapxj.put(entry.getKey(), null != mapxj.get(entry.getKey()) ? mapxj.get(entry.getKey()) + entry.getValue().toString() : entry.getValue().toString());
						} else {
							mapxj.put(entry.getKey(), null != mapxj.get(entry.getKey()) ? Double.valueOf(mapxj.get(entry.getKey()).toString()) + Double.valueOf(entry.getValue().toString()) : Double.valueOf(entry.getValue().toString()));
						}						
					}else if("DWORKDATE".equals(entry.getKey())){
						mapxj.put("DWORKDATE", "小计");
					}
				} 
				if(typName == map.get("DIVISION").toString() || typName.equals(map.get("DIVISION").toString())){
					map.put("DIVISION", "");
				}else{
					typName = map.get("DIVISION").toString();
				}
				mapList.add(map);
				if(i+1<list.size()){
					Map<String, Object> map2 = list.get(i+1);
					if(!typName.equals(map2.get("DIVISION"))){
						mapList.add(mapxj);
						mapxj = new HashMap<String, Object>();
					}
				}else{
					mapList.add(mapxj);
				}
			}
			
			reportObject.setRows(MisUtil.formatNumForListResult(mapList, "RMONEY", 2));		//格式化为2为小数
			reportObject.setFooter(MisUtil.formatNumForListResult(saleAnalysisMapper.CalQueryTcmxbb(condition), "RMONEY", 2));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：套餐销售报告
	 * @author 马振
	 * 创建时间：2015-4-28 下午3:19:30
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Object listBohSales(PublicEntity condition) throws CRUDException {
		try {
			Page pager = condition.getPager();

			//当Integer.MAX_VALUE != pager.getPageSize()时，为正常查询，不相等时为Excel导出
			if (Integer.MAX_VALUE != pager.getPageSize()) {
				pager.setNowPage(condition.getPage());
				pager.setPageSize(condition.getRows());
			}
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			if(ValueUtil.IsNotEmpty(condition.getEnablestate())){//给启用状态添加‘’
				condition.setEnablestate(FormatDouble.StringCodeReplace(condition.getEnablestate()));
			}
			
			//查询套餐销售报告数据
			List<Map<String,Object>> list = saleAnalysisMapper.listBohSales(condition);
			
			//若查出的数据不为0，则采用分页的语句
			if (list.size() != 0) {
				list = pageManager.selectPage(condition, pager, MisBohSaleAnalysisMapper.class.getName()+".listBohSales");
			}
			
			reportObject.setRows(list);
			reportObject.setFooter(saleAnalysisMapper.listSumBohSales(condition));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：时段营业额报告
	 * @author 马振
	 * 创建时间：2015-7-31 上午11:24:31
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> queryPeriodTurnoverReport(PublicEntity condition)throws CRUDException{
		try{
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("bdat", condition.getBdat());
			map.put("edat", condition.getEdat());
			map.put("binterval", condition.getBinterval());
			map.put("einterval", condition.getEinterval());
			map.put("pk_store", condition.getPk_store());
			map.put("dateList", DateJudge.getListBetweenInterval(condition.getBinterval(),condition.getEinterval(),condition.getInterval()));
			
			//结果集
			List<Map<String,Object>> relustList = new ArrayList<Map<String,Object>>();
			
			List<Map<String, Object>> list = saleAnalysisMapper.queryPeriodTurnoverReport(map);
			Total nmoney = new Total();//营业收入累计
			
			for (int i = 0; i < list.size(); i++) {
				Map<String, Object> smap = list.get(i);
				
				nmoney.addD(Double.parseDouble(MisUtil.formatDoubleLength(MisUtil.getValue(smap.get("NMONEY")), 2)));
				smap.put("NMONEYLJ", nmoney.getD());
				
				relustList.add(smap);
				smap = null;
			}
			
			//需要格式化的字段
			String str = "NSERVICETIME,NMONEY,NSJMONEY,AVGTC,PEOPLEAVG,NMONEYLJ";
			
			reportObject.setRows(MisUtil.formatNumForListResult(relustList, str, 2));
			reportObject.setFooter(MisUtil.formatNumForListResult(saleAnalysisMapper.queryPeriodTurnoverReportSUM(map), str, 2));
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：附加项销售统计
	 * @author 马振
	 * 创建时间：2015-8-15 下午2:47:23
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Object queryAdditionalTerm(PublicEntity condition) throws CRUDException {
		try {
			Page pager = condition.getPager();
			String str = "NFMONEY,NFYMONEY,NFZMONEY";
			reportObject.setRows(MisUtil.formatNumForListResult(pageManager.selectPage(condition, pager, MisBohSaleAnalysisMapper.class.getName() + ".queryAdditionalTerm"), str, 2));
			reportObject.setFooter(MisUtil.formatNumForListResult(saleAnalysisMapper.queryAdditionalTermSum(condition), str, 2));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：点菜员提成汇总报表
	 * @author 马振
	 * 创建时间：2015-8-18 下午2:39:47
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Object queryOrderingMemberCommission(PublicEntity condition) throws CRUDException {
		try {
			Page pager = condition.getPager();
			reportObject.setRows(pageManager.selectPage(condition, pager, MisBohSaleAnalysisMapper.class.getName() + ".queryOrderingMemberCommission"));
			reportObject.setFooter(saleAnalysisMapper.queryCalOrderingMemberCommission(condition));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：点菜员提成明细
	 * @author 马振
	 * 创建时间：2015-8-28 下午3:50:21
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Object queryOrderingMemberCommissionDetail(PublicEntity condition) throws CRUDException {
		try {
			Page pager = condition.getPager();
			reportObject.setRows(pageManager.selectPage(condition, pager, MisBohSaleAnalysisMapper.class.getName() + ".queryOrderingMemberCommissionDetail"));
			reportObject.setFooter(saleAnalysisMapper.queryCalOrderingMemberCommissionDetail(condition));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
}