package com.choice.misboh.service.reportMis;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.scm.domain.Grp;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.GrpTypMapper;
import com.choice.scm.persistence.reportMis.SupplyAcctMisMapper;

@Service
public class RkLeibieHuizongMisBohService {

	@Autowired
	private SupplyAcctMisMapper rkLeibieHuizongMisMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired
	private GrpTypMapper grpTypMapper;
	
	/**
	 * 入库类别汇总
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<Map<String,Object>> findAllChkinmGrp(SupplyAcct supplyAcct) throws CRUDException{
		try{
			supplyAcct.setChktyp(CodeHelper.replaceCode(supplyAcct.getChktyp()));
			Map<String,Object> conditions = new HashMap<String,Object>();
			if(supplyAcct.getChktyp() != null && !supplyAcct.getChktyp().equals(""))
				conditions.put("listTyp", supplyAcct.getChktyp());
			if(supplyAcct.getPositn() != null && !supplyAcct.getPositn().equals(""))
				conditions.put("listPositn", supplyAcct.getPositn());
			if(supplyAcct.getDelivercode() != null && !supplyAcct.getDelivercode().equals(""))
				conditions.put("listDeliver", Arrays.asList(supplyAcct.getDelivercode().split(",")));
			if(supplyAcct.getGrp() != null && !supplyAcct.getGrp().equals(""))
				conditions.put("listGrp", Arrays.asList(supplyAcct.getGrp().split(",")));
			if(supplyAcct.getGrptyp() != null && !supplyAcct.getGrptyp().equals(""))
				conditions.put("listGrptyp", Arrays.asList(supplyAcct.getGrptyp().split(",")));
			if(supplyAcct.getBdat() != null)
				conditions.put("madedStart", supplyAcct.getBdat());
			if(supplyAcct.getEdat() != null)
				conditions.put("madedEnd", supplyAcct.getEdat());
			if(supplyAcct.getAccountId() != null)
				conditions.put("accountId", supplyAcct.getAccountId());
			List<Map<String,Object>> foot = rkLeibieHuizongMisMapper.findChkinmCalSum(conditions);
			mapReportObject.setRows(rkLeibieHuizongMisMapper.findAllChkinmGrp(conditions));
			mapReportObject.setFooter(foot);
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	//导出入库类别汇总报表
	public boolean exportChkinCategorySum(OutputStream os,SupplyAcct supplyAcct) throws Exception{
		supplyAcct.setChktyp(CodeHelper.replaceCode(supplyAcct.getChktyp()));
		WritableWorkbook workBook = null;
		try{
			//记录表头位置
			Map<String,Integer> position = new HashMap<String,Integer>();
			mapReportObject = findAllChkinmGrp(supplyAcct);
			List<Grp> headers = grpTypMapper.findAllGrpA(null,supplyAcct.getAcct());
			List<Map<String,Object>> rows = mapReportObject.getRows();
			List<Map<String,Object>> foot = mapReportObject.getFooter();
			workBook = Workbook.createWorkbook(os);
			//新建工作表
			WritableSheet sheet = workBook.createSheet("入库类别汇总", 0);
			//定义label
			Label label = new Label(0,0,"入库类别汇总");
			//添加label到cell
            sheet.addCell(label);
            //设置表格表头
            Label headfirst = new Label(0,1,"项目");
            sheet.addCell(headfirst);
            Label headsecond = new Label(1,1,"合计");
            sheet.addCell(headsecond);
            for(int i = 2 ; i < headers.size()+2;i ++){
            	Label head = new Label(i,1,headers.get(i-2).getDes());
            	position.put(headers.get(i-2).getDes(), i);
            	sheet.addCell(head);
            }
            sheet.mergeCells(0, 0, headers.size()+1, 0);
            //遍历list填充表格内容
            String curDeliver = "";
            double total = 0;
            int j = 2;
            for(int i = 0 ; i < rows.size() ; i ++ ){
        		String innerDeliver = (String) rows.get(i).get("DELIVERDES");
        		if(!innerDeliver.equals(curDeliver) || i == rows.size()-1){
        			if(!"".equals(curDeliver))j++;
            		Label project = new Label(0,j,innerDeliver);
            		sheet.addCell(project);
        			Label totalLab = new Label(1,j,String.valueOf(total));
        			sheet.addCell(totalLab);
        			curDeliver = innerDeliver;
        			total = 0;
        		}
        		total += ((BigDecimal)rows.get(i).get("AMT")).doubleValue();
        		Label lab = new Label(position.get((String) rows.get(i).get("TYPDES")),j,String.valueOf(((BigDecimal)rows.get(i).get("AMT")).doubleValue()));
    			sheet.addCell(lab);
            }
            //填充合计
            j++;
            Label project = new Label(0,j,"合计");
    		sheet.addCell(project);
    		for(int i = 0 ; i < foot.size() ; i ++ ){
    			Label lab = new Label(position.get(foot.get(i).get("TYPDES")),j,String.valueOf(((BigDecimal)foot.get(i).get("AMT")).doubleValue()));
    			sheet.addCell(lab);
    		}
            workBook.write();
            os.flush();
		}catch (Exception e) {
			throw new CRUDException(e);
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
		
		
	}	
}
