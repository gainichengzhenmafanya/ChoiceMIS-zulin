package com.choice.misboh.service.reportMis;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.misboh.persistence.reportMis.CostItemMisBohMapper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.ana.PtivityAna;
import com.choice.scm.persistence.AcctMapper;

@Service
public class WzYongliangfenchaFenxiMisBohService {

	@Autowired 
	private PageManager<Map<String,Object>> pageManager;
	@Autowired
	private ReportObject<Map<String,Object>> reportObject;
	@Autowired
	private CostItemMisBohMapper wzWanyuanyongliangFenxiMisBohMapper;
	@Autowired
	private AcctMapper acctMapper;
 
	/**
	 * 统计物资用量分差
	 * @param ptivity
	 * @return
	 */
	public String callScmWzYongliangFenchaFenxi(PtivityAna ptivity) throws CRUDException{
		try{
			ptivity.setYearr(acctMapper.findYearrByDate(ptivity.getBdat()));
			wzWanyuanyongliangFenxiMisBohMapper.callScmWzYongliangFenchaFenxiTemp(ptivity);
			return "1";
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 物资用量分差
	 * @param ptivity
	 * @param page
	 * @return
	 */
	public ReportObject<Map<String,Object>> findWzYongliangFenchaFenxi(PtivityAna ptivity,Page page) throws CRUDException{
		try{
			ptivity.setYearr(acctMapper.findYearrByDate(ptivity.getBdat()));
			ptivity.setPositn(CodeHelper.replaceCode(ptivity.getPositn()));
			List<Map<String,Object>> foot = wzWanyuanyongliangFenxiMisBohMapper.findWzYongliangFenchaFenxiSum(ptivity);
			if(null == page){
				reportObject.setRows(wzWanyuanyongliangFenxiMisBohMapper.findWzYongliangFenchaFenxi(ptivity));
			}else{
				reportObject.setRows(pageManager.selectPage(ptivity, page, CostItemMisBohMapper.class.getName()+".findWzYongliangFenchaFenxi"));
				reportObject.setTotal(page.getCount());
			}
			reportObject.setFooter(foot);
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
}