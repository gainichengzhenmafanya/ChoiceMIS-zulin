package com.choice.misboh.service.reportMis;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.misboh.domain.inventory.Chkstoreo;
import com.choice.misboh.persistence.reportMis.MdPandianchaxunMisBohMapper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyUnit;
@Service
public class MdPandianchaxunMisBohService {

	@Autowired
	private MdPandianchaxunMisBohMapper mdPandianchaxunMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	
	/**
	 * 查询历史盘点
	 * @param content
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String,Object>> findMdPandian(Chkstoreo chkstore,Page pager) throws CRUDException{
		try {
			List<Map<String,Object>> foot = mdPandianchaxunMapper.findMdPandianCount(chkstore);
			List<Map<String,Object>> listInfo = mapPageManager.selectPage(chkstore, pager, MdPandianchaxunMisBohMapper.class.getName()+".findMdPandian");
			mapReportObject.setRows(listInfo);
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}

	/**
	 * 查询历史盘点
	 * @param content
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String,Object>> findMdPandian(Chkstoreo chkstore, Page pager, List<SupplyUnit> mapList) throws CRUDException{
		try {
			List<Map<String,Object>> foot = mdPandianchaxunMapper.findMdPandianDetailCount(chkstore);
			List<Map<String,Object>> listInfo = mapPageManager.selectPage(chkstore, pager, MdPandianchaxunMisBohMapper.class.getName()+".findMdPandianDetailQuery");
			for(Map<String,Object> map : listInfo){
				//1.从多规格map中取
				for(SupplyUnit su : mapList){
					if(map.get("SP_CODE").equals(su.getSp_code())){
						if(1 == su.getSequence()){
							map.put("SPEC1", su.getUnit());
						}else if(2 == su.getSequence()){
							map.put("SPEC2", su.getUnit());
						}else if(3 == su.getSequence()){
							map.put("SPEC3", su.getUnit());
						}else if(4 == su.getSequence()){
							map.put("SPEC4", su.getUnit());
						}
					}
				}
			}
			mapReportObject.setRows(listInfo);
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询历史盘点
	 * @param content
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String,Object>> findMdPandianMingxi(Chkstoreo chkstore, Page pager, List<SupplyUnit> mapList) throws CRUDException{
		try {
			List<Map<String,Object>> foot = mdPandianchaxunMapper.findMdPandianMingxiCount(chkstore);
			List<Map<String,Object>> listInfo = mapPageManager.selectPage(chkstore, pager, MdPandianchaxunMisBohMapper.class.getName()+".findMdPandianMingxiDetailQuery");
			for(Map<String,Object> map : listInfo){
				//1.从多规格map中取
				for(SupplyUnit su : mapList){
					if(map.get("SP_CODE").equals(su.getSp_code())){
						if(1 == su.getSequence()){
							map.put("SPEC1", su.getUnit());
						}else if(2 == su.getSequence()){
							map.put("SPEC2", su.getUnit());
						}else if(3 == su.getSequence()){
							map.put("SPEC3", su.getUnit());
						}else if(4 == su.getSequence()){
							map.put("SPEC4", su.getUnit());
						}
					}
				}
			}
			mapReportObject.setRows(listInfo);
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
}
