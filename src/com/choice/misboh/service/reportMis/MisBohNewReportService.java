package com.choice.misboh.service.reportMis;

import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.commonutil.MisUtil;
import com.choice.misboh.domain.BaseRecord.SiteType;
import com.choice.misboh.domain.reportMis.PublicEntity;
import com.choice.misboh.domain.reportMis.ReportObject;
import com.choice.misboh.domain.util.Holiday;
import com.choice.misboh.persistence.reportMis.MisBohNewReportMapper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * 描述：九毛九收入日结算报表、营业结算月份汇总表
 * @author 马振
 * 创建时间：2015-7-28 下午5:08:19
 */
@Service
public class MisBohNewReportService {

	@Autowired
	private ReportObject<Map<String,Object>> reportObject;
	
	@Autowired
	private MisBohNewReportMapper newReportMapper;
	
	@Autowired
	private PageManager<Map<String,Object>> pageManager;
	
	/**
	 * 九毛九营业日报表  ------ 支付类型
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> findPayment(PublicEntity condition) throws CRUDException{
		try{
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			return newReportMapper.findPayment(condition);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	/**
	 * 九毛九营业日报表  ------ 支付方式
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> findPaymode(PublicEntity condition) throws CRUDException{
		try{
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			return newReportMapper.findPaymode(condition);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	/**
	 * 九毛九营业日报表  ------ 支付合计
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Map<String, Object> findPayCal(PublicEntity condition) throws CRUDException{
		try{
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			return newReportMapper.findPayCal(condition);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 九毛九营业日报表  ------ 大类 
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> findMarsaleClass(PublicEntity condition) throws CRUDException{
		try{
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			return newReportMapper.findMarsaleClass(condition);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 九毛九营业日报表  ------ 中类 
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> findMarsaleClassTwo(PublicEntity condition) throws CRUDException{
		try{
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			return newReportMapper.findMarsaleClassTwo(condition);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 九毛九营业日报表  ------ 大类 合计
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Map<String, Object> findMarsaleClassCal(PublicEntity condition) throws CRUDException{
		try{
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			return newReportMapper.findMarsaleClassCal(condition);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	/**
	 * 九毛九营业日报表  ------ 少款 
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> findShaoamt(PublicEntity condition) throws CRUDException{
		try{
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			return newReportMapper.findShaoamt(condition);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	/**
	 * 九毛九营业日报表  ------ 少款 合计
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Map<String, Object> findShaoamtCal(PublicEntity condition) throws CRUDException{
		try{
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			return newReportMapper.findShaoamtCal(condition);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	/**
	 * 九毛九营业日报表  ------ 多款 
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> findDuoamt(PublicEntity condition) throws CRUDException{
		try{
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			return newReportMapper.findDuoamt(condition);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	/**
	 * 九毛九营业日报表  ------ 多款 合计
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Map<String, Object> findDuoamtCal(PublicEntity condition) throws CRUDException{
		try{
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			return newReportMapper.findDuoamtCal(condition);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	/**
	 * 九毛九营业日报表  ------ 应收现金
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Map<String, Object> findNcouponcamt(PublicEntity condition) throws CRUDException{
		try{
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			return newReportMapper.findNcouponcamt(condition);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	/**
	 * 九毛九营业日报表  ------ 银行卡
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Map<String, Object> findNcashamt(PublicEntity condition) throws CRUDException{
		try{
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			return newReportMapper.findNcashamt(condition);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	/**
	 * 九毛九营业日报表  ------ 银行交易记录
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> findBillList(PublicEntity condition) throws CRUDException{
		try{
			List<Map<String, Object>> rs = new ArrayList<Map<String,Object>>(); 
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			
			List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();
			//判断单店查询还是多店查询
			if(condition.getPk_store().indexOf(",")!=-1){
				list = newReportMapper.findBillList(condition);
			}else{
				list = newReportMapper.findBillListSign(condition);
			}
			for(int i=0;i<list.size();i++){
				Map<String, Object> map = new HashMap<String, Object>();
				if(i==0){
					map.put("ID",i+1);
					map.put("DWORKDATE", list.get(i).get("DWORKDATE").toString());
					map.put("NCASH", list.get(i).get("NCASH").toString());
					map.put("VSAVINGDATE", list.get(i).get("VSAVINGDATE").toString());
					map.put("VSAVINGNO", list.get(i).get("VSAVINGNO")==null?"":list.get(i).get("VSAVINGNO").toString());
					map.put("NAMOUNT", list.get(i).get("NAMOUNT").toString());
				}else{
					if(list.get(i).get("DWORKDATE").toString().equals(list.get(i-1).get("DWORKDATE").toString())){
						map.put("ID",i+1);
						map.put("DWORKDATE", "");
						map.put("NCASH", "");
						map.put("VSAVINGDATE", list.get(i).get("VSAVINGDATE").toString());
						map.put("VSAVINGNO", list.get(i).get("VSAVINGNO")==null?"":list.get(i).get("VSAVINGNO").toString());
						map.put("NAMOUNT", list.get(i).get("NAMOUNT").toString());
					}else{
						map.put("ID",i+1);
						map.put("DWORKDATE", list.get(i).get("DWORKDATE").toString());
						map.put("NCASH", list.get(i).get("NCASH").toString());
						map.put("VSAVINGDATE", list.get(i).get("VSAVINGDATE").toString());
						map.put("VSAVINGNO", list.get(i).get("VSAVINGNO")==null?"":list.get(i).get("VSAVINGNO").toString());
						map.put("NAMOUNT", list.get(i).get("NAMOUNT").toString());
					}
				}
				rs.add(map);
			}
			return rs;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述： 银行交易记录  合计
	 * @author 马振
	 * 创建时间：2016年4月14日下午2:58:56
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Map<String, Object> findCalBillList(PublicEntity condition) throws CRUDException{
		try{
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			return newReportMapper.findCalBillList(condition);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 九毛九营业日报表  ------ 水电气
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Map<String, Object> findStockdata(PublicEntity condition) throws CRUDException{
		try{
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			return newReportMapper.findStockdata(condition);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	/**
	 * 九毛九营业日报表  ------ 退菜
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Map<String, Object> findCancel(PublicEntity condition) throws CRUDException{
		try{
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			return newReportMapper.findCancel(condition);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	/**
	 * 九毛九营业日报表  ------ 反结算
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Map<String, Object> findFcnt(PublicEntity condition) throws CRUDException{
		try{
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			return newReportMapper.findFcnt(condition);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	/**
	 * 九毛九营业日报表  ------ 是否上传
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public String findUploadFlag(PublicEntity condition) throws CRUDException{
		try{
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			String rs=newReportMapper.findUploadFlag(condition);
			return null==rs||"".equals(rs)?"2":rs;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	/**
	 * 查询营业日报表_报表导出excel
	 * @param request
	 * @param response
	 * @param paymentList, paymodeList, payCal, marsaleClassList, marsaleClassTwoList, marsaleClassCal, shaoamtList, 
	 * @param shaoamtCal, duoamtList, duoamtCal,ncouponcamt,ncashamt,billList,billListCal,stockdata,cancel,fcnt
	 * @return
	 */
	public<T> boolean exportYYRBBReport(HttpServletRequest request,HttpServletResponse response,List<Map<String, Object>> paymentList,
			List<Map<String, Object>> paymodeList,Map<String, Object> payCal,List<Map<String, Object>> marsaleClassList,List<Map<String, Object>> marsaleClassTwoList,Map<String, Object> marsaleClassCal,
			List<Map<String, Object>> shaoamtList,Map<String, Object> shaoamtCal,List<Map<String, Object>> duoamtList,Map<String, Object> duoamtCal,
			Map<String, Object> ncouponcamt,Map<String, Object> ncashamt,List<Map<String, Object>> billList,Map<String, Object> billListCal,Map<String, Object> stockdata,
			Map<String, Object> cancel,Map<String, Object> fcnt,PublicEntity condition){
		OutputStream os = null;
		XSSFWorkbook wb =  new XSSFWorkbook();
		XSSFSheet sheet = wb.createSheet("Sheet1");
		XSSFRow row = null;
		XSSFFont font = wb.createFont();
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		font.setFontHeightInPoints((short)16);
		XSSFCellStyle titleStyle = wb.createCellStyle();
		titleStyle.setFont(font);
		titleStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		titleStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		XSSFCellStyle contentStyle1 = wb.createCellStyle();
		contentStyle1.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		contentStyle1.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		XSSFCellStyle contentStyle = wb.createCellStyle();
		contentStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		contentStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		contentStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN); //下边框  
		contentStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);//左边框  
		contentStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);//上边框  
		contentStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);//右边框
		XSSFCellStyle cellStyle = wb.createCellStyle();
		cellStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN); //下边框  
		cellStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);//左边框  
		cellStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);//上边框  
		cellStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);//右边框
		String title="营业日报表";
		try {
			os = response.getOutputStream();
			row = sheet.createRow((short)0);
			XSSFCell titleCell = row.createCell(0);
			titleCell.setCellValue(title);
			titleCell.setCellStyle(titleStyle);
			sheet.addMergedRegion(new CellRangeAddress(0, (short)0, 0, (short)15));
			
			response.setContentType("application/msexcel; charset=UTF-8");
			if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
			    //IE  
				title = URLEncoder.encode(title, "UTF-8");              
			}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
			    //firefox  
				title = new String(title.getBytes("UTF-8"), "ISO8859-1");              
			}else{                
			    // other          
				title = new String(title.getBytes("UTF-8"), "ISO8859-1");              
			}   
			response.setHeader("Content-disposition", "attachment; filename="  
	                + title + ".xlsx");
			
			int paylen=paymentList.size()+paymodeList.size()+1;
			int typlen=marsaleClassList.size()+marsaleClassTwoList.size()+1;
			int len=paylen;
			if(paylen<typlen){
				len=typlen;
			}
            //设置表格表头
			row = sheet.createRow((short)1);
			XSSFCell cell = row.createCell(0);
			cell.setCellValue("分店：");
			cell.setCellStyle(contentStyle1);
			XSSFCell cell1 = row.createCell(1);
			cell1.setCellValue(condition.getFirmdes());
			cell1.setCellStyle(contentStyle1);
			sheet.addMergedRegion(new CellRangeAddress(1, (short)1, 1, (short)10));
			XSSFCell cell2 = row.createCell(11);
			cell2.setCellValue("日期：");
			cell2.setCellStyle(contentStyle1);
			XSSFCell cell3 = row.createCell(12);
			cell3.setCellValue(condition.getBdat()+"至"+condition.getEdat());
			cell3.setCellStyle(contentStyle1);
			sheet.addMergedRegion(new CellRangeAddress(1, (short)1, 12, (short)15));
			row = sheet.createRow((short)2);
			XSSFCell cell4 = row.createCell(0);
			cell4.setCellValue("（一）营业收入款");
			cell4.setCellStyle(cellStyle);
			CellRangeAddress cellRangeAddress1 = new CellRangeAddress(2, (short)2, 0, (short)15);
			sheet.addMergedRegion(cellRangeAddress1);
			row = sheet.createRow((short)3);
			XSSFCell cell5 = row.createCell(0);
			cell5.setCellValue("营业结算方式");
			cell5.setCellStyle(contentStyle);
			CellRangeAddress cellRangeAddress2 = new CellRangeAddress(3, (short)len+3, 0, (short)0);
			sheet.addMergedRegion(cellRangeAddress2);
			XSSFCell cell6 = row.createCell(1);
			cell6.setCellValue("项目");
			cell6.setCellStyle(contentStyle);
			XSSFCell cell7 = row.createCell(2);
			cell7.setCellValue("笔数/张数");
			cell7.setCellStyle(contentStyle);
			XSSFCell cell8 = row.createCell(3);
			cell8.setCellValue("原价金额");
			cell8.setCellStyle(contentStyle);
			XSSFCell cell9 = row.createCell(4);
			cell9.setCellValue("优惠金额");
			cell9.setCellStyle(contentStyle);
			XSSFCell cell10 = row.createCell(5);
			cell10.setCellValue("含税收入");
			cell10.setCellStyle(contentStyle);
			XSSFCell cell11 = row.createCell(6);
			cell11.setCellValue("手续费");
			cell11.setCellStyle(contentStyle);
			XSSFCell cell12 = row.createCell(7);
			cell12.setCellValue("实收金额");
			cell12.setCellStyle(contentStyle);
			XSSFCell cell13 = row.createCell(8);
			cell13.setCellValue("收入大类");
			cell13.setCellStyle(contentStyle);
			CellRangeAddress cellRangeAddress3 = new CellRangeAddress(3, (short)len+3, 8, (short)8);
			sheet.addMergedRegion(cellRangeAddress3);
			
			XSSFCell cell14 = row.createCell(9);
			cell14.setCellValue("项目");
			cell14.setCellStyle(contentStyle);
			CellRangeAddress newcellRangeAddress1 = new CellRangeAddress(3, (short)3, 9, (short)10);
			sheet.addMergedRegion(newcellRangeAddress1);
			XSSFCell cell15 = row.createCell(11);
			cell15.setCellValue("原价金额");
			cell15.setCellStyle(contentStyle);
			XSSFCell cell16 = row.createCell(12);
			cell16.setCellValue("优惠金额");
			cell16.setCellStyle(contentStyle);
			XSSFCell cell17 = row.createCell(13);
			cell17.setCellValue("含税收入");
			cell17.setCellStyle(contentStyle);
			XSSFCell newcell1 = row.createCell(14);
			newcell1.setCellValue("销项税");
			newcell1.setCellStyle(contentStyle);
			XSSFCell newcell2 = row.createCell(15);
			newcell2.setCellValue("不含税收入");
			newcell2.setCellStyle(contentStyle);
			
			//处理数据
			List<Map<String, Object>> payList = new ArrayList<Map<String,Object>>();
			List<Map<String, Object>> typList = new ArrayList<Map<String,Object>>();
			//结算方式
			for(int i=0;i<paymentList.size();i++){
				Map<String,Object> pay = paymentList.get(i);
				Map<String,Object> payMap = new HashMap<String, Object>();
				payMap.put("VNAME", (i+1)+"、"+(pay.get("VNAME")==null?"未定义":pay.get("VNAME").toString()));
				payMap.put("CNT", pay.get("CNT")==null?"0":pay.get("CNT").toString());
				payMap.put("NMONEY", pay.get("NMONEY")==null?"0.00":pay.get("NMONEY").toString());
				payMap.put("NPAIDMONEY", pay.get("NPAIDMONEY")==null?"0.00":pay.get("NPAIDMONEY").toString());
				payMap.put("AMT", pay.get("AMT")==null?"0.00":pay.get("AMT").toString());
				payMap.put("NPOUNDAGE", pay.get("NPOUNDAGE")==null?"0.00":pay.get("NPOUNDAGE").toString());
				payMap.put("JAMT", pay.get("JAMT")==null?"0.00":pay.get("JAMT").toString());
				payList.add(payMap);
				int n=1;
				for(int j=0;j<paymodeList.size();j++){
					Map<String,Object> mode = paymodeList.get(j);
					if(pay.get("VOPERATE").toString().equals(mode.get("IGROUPID").toString())){
						payMap = new HashMap<String, Object>();
						payMap.put("VNAME", ((i+1)+"."+n+++" ")+(mode.get("VNAME")==null?"未定义":mode.get("VNAME").toString()));
						payMap.put("CNT", mode.get("CNT")==null?"0":mode.get("CNT").toString());
						payMap.put("NMONEY", mode.get("NMONEY")==null?"0.00":mode.get("NMONEY").toString());
						payMap.put("NPAIDMONEY", mode.get("NPAIDMONEY")==null?"0.00":mode.get("NPAIDMONEY").toString());
						payMap.put("AMT", mode.get("AMT")==null?"0.00":mode.get("AMT").toString());
						payMap.put("NPOUNDAGE", mode.get("NPOUNDAGE")==null?"0.00":mode.get("NPOUNDAGE").toString());
						payMap.put("JAMT", mode.get("JAMT")==null?"0.00":mode.get("JAMT").toString());
						payList.add(payMap);
					}
				}
			}
			//类别
			for(int i=0;i<marsaleClassList.size();i++){
				Map<String,Object> typ = marsaleClassList.get(i);
				Map<String,Object> typMap = new HashMap<String, Object>();
				typMap.put("TYPENAME", typ.get("TYPENAME")==null?"未定义":typ.get("TYPENAME").toString());
				typMap.put("NAME", (typ.get("RN")+"、")+(typ.get("NAME")==null?"未定义":typ.get("NAME").toString()));
				typMap.put("NMONEY", typ.get("NMONEY")==null?"0.00":typ.get("NMONEY").toString());
				typMap.put("NZMONEY", typ.get("NZMONEY")==null?"0.00":typ.get("NZMONEY").toString());
				typMap.put("NYMONEY", typ.get("NYMONEY")==null?"0.00":typ.get("NYMONEY").toString());
				typMap.put("NTAX", typ.get("NTAX")==null?"0.00":typ.get("NTAX").toString());
				typMap.put("JAMT", typ.get("JAMT")==null?"0.00":typ.get("JAMT").toString());
				typList.add(typMap);
				int n=1;
				for(int j=0;j<marsaleClassTwoList.size();j++){
					Map<String,Object> typTwo = marsaleClassTwoList.get(j);
					if(typ.get("ID").toString().equals(typTwo.get("ID").toString()) && typ.get("TYPENAME").toString().equals(typTwo.get("TYPENAME").toString())){
						typMap = new HashMap<String, Object>();
						typMap.put("TYPENAME", typTwo.get("TYPENAME")==null?"未定义":typTwo.get("TYPENAME").toString());
						typMap.put("NAME", (typ.get("RN")+"."+n+++" ")+(typTwo.get("NAME")==null?"未定义":typTwo.get("NAME").toString()));
						typMap.put("NMONEY", typTwo.get("NMONEY")==null?"0.00":typTwo.get("NMONEY").toString());
						typMap.put("NZMONEY", typTwo.get("NZMONEY")==null?"0.00":typTwo.get("NZMONEY").toString());
						typMap.put("NYMONEY", typTwo.get("NYMONEY")==null?"0.00":typTwo.get("NYMONEY").toString());
						typMap.put("NTAX", typTwo.get("NTAX")==null?"0.00":typTwo.get("NTAX").toString());
						typMap.put("JAMT", typTwo.get("JAMT")==null?"0.00":typTwo.get("JAMT").toString());
						typList.add(typMap);
					}
				}
			}
			
            //遍历list填充表格内容
			int num=3;
			int t=0;
			for(int i=0;i<len;i++){
				row = sheet.createRow((short)++num);
				if(i<payList.size()){
					Map<String,Object> pay = payList.get(i);
					XSSFCell cellp1 = row.createCell(1);
					cellp1.setCellValue(pay.get("VNAME").toString());
					cellp1.setCellStyle(cellStyle);
					XSSFCell cellp2 = row.createCell(2);
					cellp2.setCellValue(pay.get("CNT").toString());
					cellp2.setCellStyle(cellStyle);
					XSSFCell cellp3 = row.createCell(3);
					cellp3.setCellValue(pay.get("NMONEY").toString());
					cellp3.setCellStyle(cellStyle);
					XSSFCell cellp4 = row.createCell(4);
					cellp4.setCellValue(pay.get("NPAIDMONEY").toString());
					cellp4.setCellStyle(cellStyle);
					XSSFCell cellp5 = row.createCell(5);
					cellp5.setCellValue(pay.get("AMT").toString());
					cellp5.setCellStyle(cellStyle);
					XSSFCell cellp6 = row.createCell(6);
					cellp6.setCellValue(pay.get("NPOUNDAGE").toString());
					cellp6.setCellStyle(cellStyle);
					XSSFCell cellp7 = row.createCell(7);
					cellp7.setCellValue(pay.get("JAMT").toString());
					cellp7.setCellStyle(cellStyle);
				}else{
					XSSFCell cellp1 = row.createCell(1);
					cellp1.setCellValue("");
					cellp1.setCellStyle(cellStyle);
					XSSFCell cellp2 = row.createCell(2);
					cellp2.setCellValue("");
					cellp2.setCellStyle(cellStyle);
					XSSFCell cellp3 = row.createCell(3);
					cellp3.setCellValue("");
					cellp3.setCellStyle(cellStyle);
					XSSFCell cellp4 = row.createCell(4);
					cellp4.setCellValue("");
					cellp4.setCellStyle(cellStyle);
					XSSFCell cellp5 = row.createCell(5);
					cellp5.setCellValue("");
					cellp5.setCellStyle(cellStyle);
					XSSFCell cellp6 = row.createCell(6);
					cellp6.setCellValue("");
					cellp6.setCellStyle(cellStyle);
					XSSFCell cellp7 = row.createCell(7);
					cellp7.setCellValue("");
					cellp7.setCellStyle(cellStyle);
				}
				if(t<typList.size()){
					Map<String,Object> typ = typList.get(t++);
					XSSFCell newcellt1 = row.createCell(9);
					newcellt1.setCellValue(typ.get("TYPENAME").toString());
					newcellt1.setCellStyle(cellStyle);
					XSSFCell cellt1 = row.createCell(10);
					cellt1.setCellValue(typ.get("NAME").toString());
					cellt1.setCellStyle(cellStyle);
					XSSFCell cellt2 = row.createCell(11);
					cellt2.setCellValue(typ.get("NMONEY").toString());
					cellt2.setCellStyle(cellStyle);
					XSSFCell cellt3 = row.createCell(12);
					cellt3.setCellValue(typ.get("NZMONEY").toString());
					cellt3.setCellStyle(cellStyle);
					XSSFCell cellt4 = row.createCell(13);
					cellt4.setCellValue(typ.get("NYMONEY").toString());
					cellt4.setCellStyle(cellStyle);
					XSSFCell newcellt2 = row.createCell(14);
					newcellt2.setCellValue(typ.get("NTAX").toString());
					newcellt2.setCellStyle(cellStyle);
					XSSFCell newcellt3 = row.createCell(15);
					newcellt3.setCellValue(typ.get("JAMT").toString());
					newcellt3.setCellStyle(cellStyle);
				}else{
					XSSFCell newcellt1 = row.createCell(9);
					newcellt1.setCellValue("");
					newcellt1.setCellStyle(cellStyle);
					XSSFCell cellt1 = row.createCell(10);
					cellt1.setCellValue("");
					cellt1.setCellStyle(cellStyle);
					XSSFCell cellt2 = row.createCell(11);
					cellt2.setCellValue("");
					cellt2.setCellStyle(cellStyle);
					XSSFCell cellt3 = row.createCell(12);
					cellt3.setCellValue("");
					cellt3.setCellStyle(cellStyle);
					XSSFCell cellt4 = row.createCell(13);
					cellt4.setCellValue("");
					cellt4.setCellStyle(cellStyle);
					XSSFCell newcellt2 = row.createCell(14);
					newcellt2.setCellValue("");
					newcellt2.setCellStyle(cellStyle);
					XSSFCell newcellt3 = row.createCell(15);
					newcellt3.setCellValue("");
					newcellt3.setCellStyle(cellStyle);
				}
			}
			
			row = sheet.createRow((short)len+3);
			XSSFCell cellp1 = row.createCell(1);
			cellp1.setCellValue(payCal.get("VNAME").toString());
			cellp1.setCellStyle(cellStyle);
			XSSFCell cellp2 = row.createCell(2);
			cellp2.setCellValue(payCal.get("CNT").toString());
			cellp2.setCellStyle(cellStyle);
			XSSFCell cellp3 = row.createCell(3);
			cellp3.setCellValue(payCal.get("NMONEY").toString());
			cellp3.setCellStyle(cellStyle);
			XSSFCell cellp4 = row.createCell(4);
			cellp4.setCellValue(payCal.get("NPAIDMONEY").toString());
			cellp4.setCellStyle(cellStyle);
			XSSFCell cellp5 = row.createCell(5);
			cellp5.setCellValue(payCal.get("AMT").toString());
			cellp5.setCellStyle(cellStyle);
			XSSFCell cellp6 = row.createCell(6);
			cellp6.setCellValue(payCal.get("NPOUNDAGE").toString());
			cellp6.setCellStyle(cellStyle);
			XSSFCell cellp7 = row.createCell(7);
			cellp7.setCellValue(payCal.get("JAMT").toString());
			cellp7.setCellStyle(cellStyle);
			
			XSSFCell cellt1 = row.createCell(9);
			cellt1.setCellValue(marsaleClassCal.get("NAME").toString());
			cellt1.setCellStyle(cellStyle);
			CellRangeAddress newcellRangeAddress2 = new CellRangeAddress((short)len+3, (short)len+3, 9, (short)10);
			sheet.addMergedRegion(newcellRangeAddress2);
			XSSFCell cellt2 = row.createCell(11);
			cellt2.setCellValue(marsaleClassCal.get("NMONEY").toString());
			cellt2.setCellStyle(cellStyle);
			XSSFCell cellt3 = row.createCell(12);
			cellt3.setCellValue(marsaleClassCal.get("NZMONEY").toString());
			cellt3.setCellStyle(cellStyle);
			XSSFCell cellt4 = row.createCell(13);
			cellt4.setCellValue(marsaleClassCal.get("NYMONEY").toString());
			cellt4.setCellStyle(cellStyle);
			XSSFCell newcellt1 = row.createCell(14);
			newcellt1.setCellValue(marsaleClassCal.get("NTAX").toString());
			newcellt1.setCellStyle(cellStyle);
			XSSFCell newcellt2 = row.createCell(15);
			newcellt2.setCellValue(marsaleClassCal.get("JAMT").toString());
			newcellt2.setCellStyle(cellStyle);
			
			row = sheet.createRow((short)len+4);
			XSSFCell cell18 = row.createCell(0);
			cell18.setCellValue("（二）非营业收入款");
			CellRangeAddress cellRangeAddress4 = new CellRangeAddress(len+4, (short)len+4, 0, (short)15);
			sheet.addMergedRegion(cellRangeAddress4);
			
			row = sheet.createRow((short)len+5);
			XSSFCell cell19 = row.createCell(0);
			cell19.setCellValue("其它少款");
			cell19.setCellStyle(contentStyle);
			CellRangeAddress cellRangeAddress5 = new CellRangeAddress(len+5, (short)len+15, 0, (short)0);
			sheet.addMergedRegion(cellRangeAddress5);
			XSSFCell cell20 = row.createCell(1);
			cell20.setCellValue("项目");
			cell20.setCellStyle(contentStyle);
			CellRangeAddress cellRangeAddress6 = new CellRangeAddress(len+5, (short)len+5, 1, (short)2);
			sheet.addMergedRegion(cellRangeAddress6);
			XSSFCell cell21 = row.createCell(3);
			cell21.setCellValue("-现金");
			cell21.setCellStyle(contentStyle);
			CellRangeAddress cellRangeAddress7 = new CellRangeAddress(len+5, (short)len+5, 3, (short)4);
			sheet.addMergedRegion(cellRangeAddress7);
			XSSFCell cell22 = row.createCell(5);
			cell22.setCellValue("-刷卡");
			cell22.setCellStyle(contentStyle);
			CellRangeAddress cellRangeAddress8 = new CellRangeAddress(len+5, (short)len+5, 5, (short)7);
			sheet.addMergedRegion(cellRangeAddress8);
			XSSFCell cell23 = row.createCell(8);
			cell23.setCellValue("其它多款");
			cell23.setCellStyle(contentStyle);
			CellRangeAddress cellRangeAddress9 = new CellRangeAddress(len+5, (short)len+15, 8, (short)8);
			sheet.addMergedRegion(cellRangeAddress9);
			XSSFCell cell24 = row.createCell(9);
			cell24.setCellValue("项目");
			cell24.setCellStyle(contentStyle);
			CellRangeAddress cellRangeAddress10 = new CellRangeAddress(len+5, (short)len+5, 9, (short)10);
			sheet.addMergedRegion(cellRangeAddress10);
			XSSFCell cell25 = row.createCell(11);
			cell25.setCellValue("+现金");
			cell25.setCellStyle(contentStyle);
			CellRangeAddress cellRangeAddress11 = new CellRangeAddress(len+5, (short)len+5, 11, (short)12);
			sheet.addMergedRegion(cellRangeAddress11);
			XSSFCell cell26 = row.createCell(13);
			cell26.setCellValue("+刷卡");
			cell26.setCellStyle(contentStyle);
			CellRangeAddress newcellRangeAddress3 = new CellRangeAddress(len+5, (short)len+5, 13, (short)15);
			sheet.addMergedRegion(newcellRangeAddress3);
			for(int i=0;i<duoamtList.size();i++){
				row = sheet.createRow((short)len+6+i);
				if(i<shaoamtList.size()){
					Map<String,Object> shao = shaoamtList.get(i);
					XSSFCell cell27 = row.createCell(1);
					cell27.setCellValue(shao.get("NAME")==null?"未定义":shao.get("NAME").toString());
					CellRangeAddress cellRangeAddress12 = new CellRangeAddress(len+6+i, (short)len+6+i, 1, (short)2);
					sheet.addMergedRegion(cellRangeAddress12);
					XSSFCell cell28 = row.createCell(3);
					cell28.setCellValue(shao.get("XAMT")==null?"0":shao.get("XAMT").toString());
					CellRangeAddress cellRangeAddress13 = new CellRangeAddress(len+6+i, (short)len+6+i, 3, (short)4);
					sheet.addMergedRegion(cellRangeAddress13);
					XSSFCell cell29 = row.createCell(5);
					cell29.setCellValue(shao.get("SAMT")==null?"0":shao.get("SAMT").toString());
					CellRangeAddress cellRangeAddress14 = new CellRangeAddress(len+6+i, (short)len+6+i, 5, (short)7);
					sheet.addMergedRegion(cellRangeAddress14);
					
					setBorder(cellRangeAddress12, sheet, wb);
					setBorder(cellRangeAddress13, sheet, wb);
					setBorder(cellRangeAddress14, sheet, wb);
				}else{
					XSSFCell cell27 = row.createCell(1);
					cell27.setCellValue("");
					CellRangeAddress cellRangeAddress12e = new CellRangeAddress(len+6+i, (short)len+6+i, 1, (short)2);
					sheet.addMergedRegion(cellRangeAddress12e);
					XSSFCell cell28 = row.createCell(3);
					cell28.setCellValue("");
					CellRangeAddress cellRangeAddress13e = new CellRangeAddress(len+6+i, (short)len+6+i, 3, (short)4);
					sheet.addMergedRegion(cellRangeAddress13e);
					XSSFCell cell29 = row.createCell(5);
					cell29.setCellValue("");
					CellRangeAddress cellRangeAddress14e = new CellRangeAddress(len+6+i, (short)len+6+i, 5, (short)7);
					sheet.addMergedRegion(cellRangeAddress14e);
					
					setBorder(cellRangeAddress12e, sheet, wb);
					setBorder(cellRangeAddress13e, sheet, wb);
					setBorder(cellRangeAddress14e, sheet, wb);
				}
				Map<String,Object> duo = duoamtList.get(i);
				XSSFCell cell30 = row.createCell(9);
				cell30.setCellValue(duo.get("NAME")==null?"未定义":duo.get("NAME").toString());
				CellRangeAddress cellRangeAddress15 = new CellRangeAddress(len+6+i, (short)len+6+i, 9, (short)10);
				sheet.addMergedRegion(cellRangeAddress15);
				XSSFCell cell31 = row.createCell(11);
				cell31.setCellValue(duo.get("XAMT")==null?"0":duo.get("XAMT").toString());
				CellRangeAddress cellRangeAddress16 = new CellRangeAddress(len+6+i, (short)len+6+i, 11, (short)12);
				sheet.addMergedRegion(cellRangeAddress16);
				XSSFCell cell32 = row.createCell(13);
				cell32.setCellValue(duo.get("SAMT")==null?"0":duo.get("SAMT").toString());
				cell32.setCellStyle(cellStyle);
				CellRangeAddress newcellRangeAddress4 = new CellRangeAddress(len+6+i, (short)len+6+i, 13, (short)15);
				sheet.addMergedRegion(newcellRangeAddress4);
				
				setBorder(cellRangeAddress15, sheet, wb);
				setBorder(cellRangeAddress16, sheet, wb);
				setBorder(newcellRangeAddress4, sheet, wb);
			}
			row = sheet.createRow((short)len+15);
			XSSFCell cell27 = row.createCell(1);
			cell27.setCellValue(shaoamtCal.get("NAME")==null?"未定义":shaoamtCal.get("NAME").toString());
			CellRangeAddress cellRangeAddress17 = new CellRangeAddress(len+15, (short)len+15, 1, (short)2);
			sheet.addMergedRegion(cellRangeAddress17);
			XSSFCell cell28 = row.createCell(3);
			cell28.setCellValue(shaoamtCal.get("XAMT")==null?"0":shaoamtCal.get("XAMT").toString());
			CellRangeAddress cellRangeAddress18 = new CellRangeAddress(len+15, (short)len+15, 3, (short)4);
			sheet.addMergedRegion(cellRangeAddress18);
			XSSFCell cell29 = row.createCell(5);
			cell29.setCellValue(shaoamtCal.get("SAMT")==null?"0":shaoamtCal.get("SAMT").toString());
			CellRangeAddress cellRangeAddress19 = new CellRangeAddress(len+15, (short)len+15, 5, (short)7);
			sheet.addMergedRegion(cellRangeAddress19);
			XSSFCell cell30 = row.createCell(9);
			cell30.setCellValue(duoamtCal.get("NAME")==null?"未定义":duoamtCal.get("NAME").toString());
			CellRangeAddress cellRangeAddress20 = new CellRangeAddress(len+15, (short)len+15, 9, (short)10);
			sheet.addMergedRegion(cellRangeAddress20);
			XSSFCell cell31 = row.createCell(11);
			cell31.setCellValue(duoamtCal.get("XAMT")==null?"0":duoamtCal.get("XAMT").toString());
			CellRangeAddress cellRangeAddress21 = new CellRangeAddress(len+15, (short)len+15, 11, (short)12);
			sheet.addMergedRegion(cellRangeAddress21);
			XSSFCell cell32 = row.createCell(13);
			cell32.setCellValue(duoamtCal.get("SAMT")==null?"0":duoamtCal.get("SAMT").toString());
			cell32.setCellStyle(cellStyle);
			cell31.setCellValue(duoamtCal.get("XAMT")==null?"0":duoamtCal.get("XAMT").toString());
			CellRangeAddress newcellRangeAddress5 = new CellRangeAddress(len+15, (short)len+15, 13, (short)15);
			sheet.addMergedRegion(newcellRangeAddress5);
			
			row = sheet.createRow((short)len+16);
			XSSFCell cell33 = row.createCell(0);
			cell33.setCellValue("（三）应上缴现金合计（账存现金金额-少款现金+多款现金）");
			CellRangeAddress cellRangeAddress22 = new CellRangeAddress(len+16, (short)len+16, 0, (short)10);
			sheet.addMergedRegion(cellRangeAddress22);
			XSSFCell cell34 = row.createCell(11);
			cell34.setCellValue(Double.parseDouble(ncouponcamt.get("NCOUPONCAMT").toString())-Double.parseDouble(shaoamtCal.get("XAMT").toString())+Double.parseDouble(duoamtCal.get("XAMT").toString()));
			CellRangeAddress cellRangeAddress23 = new CellRangeAddress(len+16, (short)len+16, 11, (short)15);
			sheet.addMergedRegion(cellRangeAddress23);
			
			row = sheet.createRow((short)len+17);
			XSSFCell cell35 = row.createCell(0);
			cell35.setCellValue("（四）当日刷卡合计（账存刷卡金额-少款刷卡+多款刷卡）");
			CellRangeAddress cellRangeAddress24 = new CellRangeAddress(len+17, (short)len+17, 0, (short)10);
			sheet.addMergedRegion(cellRangeAddress24);
			XSSFCell cell36 = row.createCell(11);
			cell36.setCellValue(Double.parseDouble(ncashamt.get("NCASHAMT").toString())-Double.parseDouble(shaoamtCal.get("SAMT").toString())+Double.parseDouble(duoamtCal.get("SAMT").toString()));
			CellRangeAddress cellRangeAddress25 = new CellRangeAddress(len+17, (short)len+17, 11, (short)15);
			sheet.addMergedRegion(cellRangeAddress25);
			
			row = sheet.createRow((short)len+18);
			XSSFCell cell37 = row.createCell(0);
			cell37.setCellValue("（五）现金存银行明细信息");
			CellRangeAddress cellRangeAddress26 = new CellRangeAddress(len+18, (short)len+18, 0, (short)15);
			sheet.addMergedRegion(cellRangeAddress26);
			
			row = sheet.createRow((short)len+19);
			XSSFCell cell38 = row.createCell(0);
			cell38.setCellValue("序号");
			cell38.setCellStyle(contentStyle);
			CellRangeAddress cellRangeAddress27 = new CellRangeAddress(len+19, (short)len+19, 0, (short)1);
			sheet.addMergedRegion(cellRangeAddress27);
			XSSFCell cell39 = row.createCell(2);
			cell39.setCellValue("现金所属营业日期");
			cell39.setCellStyle(contentStyle);
			CellRangeAddress cellRangeAddress28 = new CellRangeAddress(len+19, (short)len+19, 2, (short)3);
			sheet.addMergedRegion(cellRangeAddress28);
			XSSFCell cell40 = row.createCell(4);
			cell40.setCellValue("应存营业现金金额");
			cell40.setCellStyle(contentStyle);
			CellRangeAddress cellRangeAddress29 = new CellRangeAddress(len+19, (short)len+19, 4, (short)5);
			sheet.addMergedRegion(cellRangeAddress29);
			XSSFCell cell41 = row.createCell(6);
			cell41.setCellValue("存款回单日期");
			cell41.setCellStyle(contentStyle);
			CellRangeAddress cellRangeAddress30 = new CellRangeAddress(len+19, (short)len+19, 6, (short)8);
			sheet.addMergedRegion(cellRangeAddress30);
			XSSFCell cell42 = row.createCell(9);
			cell42.setCellValue("存款交易流水号");
			cell42.setCellStyle(contentStyle);
			CellRangeAddress cellRangeAddress31 = new CellRangeAddress(len+19, (short)len+19, 9, (short)13);
			sheet.addMergedRegion(cellRangeAddress31);
			XSSFCell cell43 = row.createCell(14);
			cell43.setCellValue("现金存款金额");
			cell43.setCellStyle(contentStyle);
			CellRangeAddress cellRangeAddress32 = new CellRangeAddress(len+19, (short)len+19, 14, (short)15);
			sheet.addMergedRegion(cellRangeAddress32);
			
			int len1=len+19;
			for(int i=0;i<billList.size();i++){
				len1+=1;
				row = sheet.createRow((short)len1);
				XSSFCell cellb1 = row.createCell(0);
				cellb1.setCellValue(billList.get(i).get("ID")==null?"":billList.get(i).get("ID").toString());
				CellRangeAddress cellRangeAddress33 = new CellRangeAddress(len1, (short)len1, 0, (short)1);
				sheet.addMergedRegion(cellRangeAddress33);
				XSSFCell cellb2 = row.createCell(2);
				cellb2.setCellValue(billList.get(i).get("DWORKDATE")==null?"":billList.get(i).get("DWORKDATE").toString());
				CellRangeAddress cellRangeAddress34 = new CellRangeAddress(len1, (short)len1, 2, (short)3);
				sheet.addMergedRegion(cellRangeAddress34);
				XSSFCell cellb3 = row.createCell(4);
				cellb3.setCellValue(billList.get(i).get("NCASH")==null?"0":billList.get(i).get("NCASH").toString());
				CellRangeAddress cellRangeAddress35 = new CellRangeAddress(len1, (short)len1, 4, (short)5);
				sheet.addMergedRegion(cellRangeAddress35);
				XSSFCell cellb4 = row.createCell(6);
				cellb4.setCellValue(billList.get(i).get("VSAVINGDATE")==null?"":billList.get(i).get("VSAVINGDATE").toString());
				CellRangeAddress cellRangeAddress36 = new CellRangeAddress(len1, (short)len1, 6, (short)8);
				sheet.addMergedRegion(cellRangeAddress36);
				XSSFCell cellb5 = row.createCell(9);
				cellb5.setCellValue(billList.get(i).get("VSAVINGNO")==null?"":billList.get(i).get("VSAVINGNO").toString());
				CellRangeAddress cellRangeAddress37 = new CellRangeAddress(len1, (short)len1, 9, (short)13);
				sheet.addMergedRegion(cellRangeAddress37);
				XSSFCell cellb6 = row.createCell(14);
				cellb6.setCellValue(billList.get(i).get("NAMOUNT")==null?"0":billList.get(i).get("NAMOUNT").toString());
				CellRangeAddress cellRangeAddress38 = new CellRangeAddress(len1, (short)len1, 14, (short)15);
				sheet.addMergedRegion(cellRangeAddress38);
				
				setBorder(cellRangeAddress33, sheet, wb);
				setBorder(cellRangeAddress34, sheet, wb);
				setBorder(cellRangeAddress35, sheet, wb);
				setBorder(cellRangeAddress36, sheet, wb);
				setBorder(cellRangeAddress37, sheet, wb);
				setBorder(cellRangeAddress38, sheet, wb);
			}
			row = sheet.createRow((short)len1+1);
			XSSFCell cellb11 = row.createCell(0);
			cellb11.setCellValue("");
			CellRangeAddress cellRangeAddress39 = new CellRangeAddress(len1+1, (short)len1+1, 0, (short)1);
			sheet.addMergedRegion(cellRangeAddress39);
			XSSFCell cellb12 = row.createCell(2);
			cellb12.setCellValue("合计");
			CellRangeAddress cellRangeAddress40 = new CellRangeAddress(len1+1, (short)len1+1, 2, (short)3);
			sheet.addMergedRegion(cellRangeAddress40);
			XSSFCell cellb13 = row.createCell(4);
			cellb13.setCellValue(billListCal.get("NCASH")==null?"0":billListCal.get("NCASH").toString());
			CellRangeAddress cellRangeAddress41 = new CellRangeAddress(len1+1, (short)len1+1, 4, (short)5);
			sheet.addMergedRegion(cellRangeAddress41);
			XSSFCell cellb14 = row.createCell(6);
			cellb14.setCellValue("");
			CellRangeAddress cellRangeAddress42 = new CellRangeAddress(len1+1, (short)len1+1, 6, (short)8);
			sheet.addMergedRegion(cellRangeAddress42);
			XSSFCell cellb15 = row.createCell(9);
			cellb15.setCellValue("");
			CellRangeAddress cellRangeAddress43 = new CellRangeAddress(len1+1, (short)len1+1, 9, (short)13);
			sheet.addMergedRegion(cellRangeAddress43);
			XSSFCell cellb16 = row.createCell(14);
			cellb16.setCellValue(billListCal.get("NAMOUNT")==null?"0":billListCal.get("NAMOUNT").toString());
			CellRangeAddress cellRangeAddress44 = new CellRangeAddress(len1+1, (short)len1+1, 14, (short)15);
			sheet.addMergedRegion(cellRangeAddress44);
			
			row = sheet.createRow((short)len1+2);
			XSSFCell cell44 = row.createCell(0);
			cell44.setCellValue("（六）存款差异（应存营业现金金额合计-现金存款金额合计）");
			CellRangeAddress cellRangeAddress45 = new CellRangeAddress(len1+2, (short)len1+2, 0, (short)10);
			sheet.addMergedRegion(cellRangeAddress45);
			XSSFCell cell45 = row.createCell(11);
			cell45.setCellValue(Double.parseDouble(billListCal.get("NCASH").toString())-Double.parseDouble(billListCal.get("NAMOUNT").toString()));
			CellRangeAddress cellRangeAddress46 = new CellRangeAddress(len1+2, (short)len1+2, 11, (short)15);
			sheet.addMergedRegion(cellRangeAddress46);
			
			row = sheet.createRow((short)len1+3);
			XSSFCell cell46 = row.createCell(0);
			cell46.setCellValue("水");
			CellRangeAddress cellRangeAddress47 = new CellRangeAddress(len1+3, (short)len1+3, 0, (short)1);
			sheet.addMergedRegion(cellRangeAddress47);
			XSSFCell cell47 = row.createCell(2);
			cell47.setCellValue("昨日数:");
			CellRangeAddress cellRangeAddress48 = new CellRangeAddress(len1+3, (short)len1+3, 2, (short)3);
			sheet.addMergedRegion(cellRangeAddress48);
			XSSFCell cell48 = row.createCell(4);
			cell48.setCellValue(stockdata.get("SBCNT")==null?"0":stockdata.get("SBCNT").toString());
			CellRangeAddress cellRangeAddress49 = new CellRangeAddress(len1+3, (short)len1+3, 4, (short)5);
			sheet.addMergedRegion(cellRangeAddress49);
			XSSFCell cell49 = row.createCell(6);
			cell49.setCellValue("今日数:");
			CellRangeAddress cellRangeAddress50 = new CellRangeAddress(len1+3, (short)len1+3, 6, (short)8);
			sheet.addMergedRegion(cellRangeAddress50);
			XSSFCell cell50 = row.createCell(9);
			cell50.setCellValue(stockdata.get("SECNT")==null?"0":stockdata.get("SECNT").toString());
			CellRangeAddress cellRangeAddress51 = new CellRangeAddress(len1+3, (short)len1+3, 9, (short)10);
			sheet.addMergedRegion(cellRangeAddress51);
			XSSFCell cell51 = row.createCell(11);
			cell51.setCellValue("本日用量:    "+stockdata.get("SCNT")==null?"0":stockdata.get("SCNT").toString());
			CellRangeAddress cellRangeAddress52 = new CellRangeAddress(len1+3, (short)len1+3, 11, (short)15);
			sheet.addMergedRegion(cellRangeAddress52);
			row = sheet.createRow((short)len1+4);
			XSSFCell cell52 = row.createCell(0);
			cell52.setCellValue("电");
			CellRangeAddress cellRangeAddress53 = new CellRangeAddress(len1+4, (short)len1+4, 0, (short)1);
			sheet.addMergedRegion(cellRangeAddress53);
			XSSFCell cell53 = row.createCell(2);
			cell53.setCellValue("昨日数:");
			CellRangeAddress cellRangeAddress54 = new CellRangeAddress(len1+4, (short)len1+4, 2, (short)3);
			sheet.addMergedRegion(cellRangeAddress54);
			XSSFCell cell54 = row.createCell(4);
			cell54.setCellValue(stockdata.get("DBCNT")==null?"0":stockdata.get("DBCNT").toString());
			CellRangeAddress cellRangeAddress55 = new CellRangeAddress(len1+4, (short)len1+4, 4, (short)5);
			sheet.addMergedRegion(cellRangeAddress55);
			XSSFCell cell55 = row.createCell(6);
			cell55.setCellValue("今日数:");
			CellRangeAddress cellRangeAddress56 = new CellRangeAddress(len1+4, (short)len1+4, 6, (short)8);
			sheet.addMergedRegion(cellRangeAddress56);
			XSSFCell cell56 = row.createCell(9);
			cell56.setCellValue(stockdata.get("DECNT")==null?"0":stockdata.get("DECNT").toString());
			CellRangeAddress cellRangeAddress57 = new CellRangeAddress(len1+4, (short)len1+4, 9, (short)10);
			sheet.addMergedRegion(cellRangeAddress57);
			XSSFCell cell57 = row.createCell(11);
			cell57.setCellValue("本日用量:    "+stockdata.get("DCNT")==null?"0":stockdata.get("DCNT").toString());
			CellRangeAddress cellRangeAddress58 = new CellRangeAddress(len1+4, (short)len1+4, 11, (short)15);
			sheet.addMergedRegion(cellRangeAddress58);
			row = sheet.createRow((short)len1+5);
			XSSFCell cell58 = row.createCell(0);
			cell58.setCellValue("燃气");
			CellRangeAddress cellRangeAddress59 = new CellRangeAddress(len1+5, (short)len1+5, 0, (short)1);
			sheet.addMergedRegion(cellRangeAddress59);
			XSSFCell cell59 = row.createCell(2);
			cell59.setCellValue("昨日数:");
			CellRangeAddress cellRangeAddress60 = new CellRangeAddress(len1+5, (short)len1+5, 2, (short)3);
			sheet.addMergedRegion(cellRangeAddress60);
			XSSFCell cell60 = row.createCell(4);
			cell60.setCellValue(stockdata.get("QBCNT")==null?"0":stockdata.get("QBCNT").toString());
			CellRangeAddress cellRangeAddress61 = new CellRangeAddress(len1+5, (short)len1+5, 4, (short)5);
			sheet.addMergedRegion(cellRangeAddress61);
			XSSFCell cell61 = row.createCell(6);
			cell61.setCellValue("今日数:");
			CellRangeAddress cellRangeAddress62 = new CellRangeAddress(len1+5, (short)len1+5, 6, (short)8);
			sheet.addMergedRegion(cellRangeAddress62);
			XSSFCell cell62 = row.createCell(9);
			cell62.setCellValue(stockdata.get("QECNT")==null?"0":stockdata.get("QECNT").toString());
			CellRangeAddress cellRangeAddress63 = new CellRangeAddress(len1+5, (short)len1+5, 9, (short)10);
			sheet.addMergedRegion(cellRangeAddress63);
			XSSFCell cell63 = row.createCell(11);
			cell63.setCellValue("本日用量:    "+stockdata.get("QCNT")==null?"0":stockdata.get("QCNT").toString());
			CellRangeAddress cellRangeAddress64 = new CellRangeAddress(len1+5, (short)len1+5, 11, (short)15);
			sheet.addMergedRegion(cellRangeAddress64);
			row = sheet.createRow((short)len1+6);
			XSSFCell cell64 = row.createCell(0);
			cell64.setCellValue("退菜数量");
			CellRangeAddress cellRangeAddress65 = new CellRangeAddress(len1+6, (short)len1+6, 0, (short)2);
			sheet.addMergedRegion(cellRangeAddress65);
			XSSFCell cell65 = row.createCell(3);
			cell65.setCellValue(cancel.get("NTCOUNT")==null?"0":cancel.get("NTCOUNT").toString());
			CellRangeAddress cellRangeAddress66 = new CellRangeAddress(len1+6, (short)len1+6, 3, (short)4);
			sheet.addMergedRegion(cellRangeAddress66);
			XSSFCell cell66 = row.createCell(5);
			cell66.setCellValue("退菜金额");
			CellRangeAddress cellRangeAddress67 = new CellRangeAddress(len1+6, (short)len1+6, 5, (short)7);
			sheet.addMergedRegion(cellRangeAddress67);
			XSSFCell cell67 = row.createCell(8);
			cell67.setCellValue(cancel.get("NTMONEY")==null?"0":cancel.get("NTMONEY").toString());
			CellRangeAddress cellRangeAddress68 = new CellRangeAddress(len1+6, (short)len1+6, 8, (short)9);
			sheet.addMergedRegion(cellRangeAddress68);
			XSSFCell cell68 = row.createCell(10);
			cell68.setCellValue("反结账次数");
			CellRangeAddress cellRangeAddress69 = new CellRangeAddress(len1+6, (short)len1+6, 10, (short)11);
			sheet.addMergedRegion(cellRangeAddress69);
			XSSFCell cell69 = row.createCell(12);
			cell69.setCellValue(fcnt.get("FCNT")==null?"0":fcnt.get("FCNT").toString());
			CellRangeAddress cellRangeAddress70 = new CellRangeAddress(len1+6, (short)len1+6, 12, (short)15);
			sheet.addMergedRegion(cellRangeAddress70);
			row = sheet.createRow((short)len1+7);
			XSSFCell cell70 = row.createCell(0);
			cell70.setCellValue("备注:");
			CellRangeAddress cellRangeAddress71 = new CellRangeAddress(len1+7, (short)len1+7, 0, (short)15);
			sheet.addMergedRegion(cellRangeAddress71);
			row = sheet.createRow((short)len1+8);
			XSSFCell cell71 = row.createCell(2);
			cell71.setCellValue("门店经理:");
			XSSFCell cell72 = row.createCell(6);
			cell72.setCellValue("收银部长:");
			XSSFCell cell73 = row.createCell(10);
			cell73.setCellValue("会计:");
			
			setBorder(cellRangeAddress1, sheet, wb);
			setBorder(cellRangeAddress2, sheet, wb);
			setBorder(cellRangeAddress3, sheet, wb);
			setBorder(cellRangeAddress4, sheet, wb);
			setBorder(cellRangeAddress5, sheet, wb);
			setBorder(cellRangeAddress6, sheet, wb);
			setBorder(cellRangeAddress7, sheet, wb);
			setBorder(cellRangeAddress8, sheet, wb);
			setBorder(cellRangeAddress9, sheet, wb);
			setBorder(cellRangeAddress10, sheet, wb);
			setBorder(cellRangeAddress11, sheet, wb);
			setBorder(cellRangeAddress17, sheet, wb);
			setBorder(cellRangeAddress18, sheet, wb);
			setBorder(cellRangeAddress19, sheet, wb);
			setBorder(cellRangeAddress20, sheet, wb);
			setBorder(cellRangeAddress21, sheet, wb);
			setBorder(cellRangeAddress22, sheet, wb);
			setBorder(cellRangeAddress23, sheet, wb);
			setBorder(cellRangeAddress24, sheet, wb);
			setBorder(cellRangeAddress25, sheet, wb);
			setBorder(cellRangeAddress26, sheet, wb);
			setBorder(cellRangeAddress27, sheet, wb);
			setBorder(cellRangeAddress28, sheet, wb);
			setBorder(cellRangeAddress29, sheet, wb);
			setBorder(cellRangeAddress30, sheet, wb);
			setBorder(cellRangeAddress31, sheet, wb);
			setBorder(cellRangeAddress32, sheet, wb);
			setBorder(cellRangeAddress39, sheet, wb);
			setBorder(cellRangeAddress40, sheet, wb);
			setBorder(cellRangeAddress41, sheet, wb);
			setBorder(cellRangeAddress42, sheet, wb);
			setBorder(cellRangeAddress43, sheet, wb);
			setBorder(cellRangeAddress44, sheet, wb);
			setBorder(cellRangeAddress45, sheet, wb);
			setBorder(cellRangeAddress46, sheet, wb);
			setBorder(cellRangeAddress47, sheet, wb);
			setBorder(cellRangeAddress48, sheet, wb);
			setBorder(cellRangeAddress49, sheet, wb);
			setBorder(cellRangeAddress50, sheet, wb);
			setBorder(cellRangeAddress51, sheet, wb);
			setBorder(cellRangeAddress52, sheet, wb);
			setBorder(cellRangeAddress53, sheet, wb);
			setBorder(cellRangeAddress54, sheet, wb);
			setBorder(cellRangeAddress55, sheet, wb);
			setBorder(cellRangeAddress56, sheet, wb);
			setBorder(cellRangeAddress57, sheet, wb);
			setBorder(cellRangeAddress58, sheet, wb);
			setBorder(cellRangeAddress59, sheet, wb);
			setBorder(cellRangeAddress60, sheet, wb);
			setBorder(cellRangeAddress61, sheet, wb);
			setBorder(cellRangeAddress62, sheet, wb);
			setBorder(cellRangeAddress63, sheet, wb);
			setBorder(cellRangeAddress64, sheet, wb);
			setBorder(cellRangeAddress65, sheet, wb);
			setBorder(cellRangeAddress66, sheet, wb);
			setBorder(cellRangeAddress67, sheet, wb);
			setBorder(cellRangeAddress68, sheet, wb);
			setBorder(cellRangeAddress69, sheet, wb);
			setBorder(cellRangeAddress70, sheet, wb);
			setBorder(cellRangeAddress71, sheet, wb);
			
			setBorder(newcellRangeAddress1, sheet, wb);
			setBorder(newcellRangeAddress2, sheet, wb);
			setBorder(newcellRangeAddress3, sheet, wb);
			setBorder(newcellRangeAddress5, sheet, wb);
            wb.write(os);
            os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	
	public void setBorder(CellRangeAddress cellRangeAddress, XSSFSheet sheet,  
			XSSFWorkbook wb) throws Exception {  
        RegionUtil.setBorderLeft(1, cellRangeAddress, sheet, wb);  
        RegionUtil.setBorderBottom(1, cellRangeAddress, sheet, wb);  
        RegionUtil.setBorderRight(1, cellRangeAddress, sheet, wb);  
        RegionUtil.setBorderTop(1, cellRangeAddress, sheet, wb);  
	}
	
	/**
	 * 描述：西贝  营收客流分段统计报表
	 * @author 马振
	 * 创建时间：2016-2-22 上午11:36:17
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findBusinessBurst(PublicEntity condition) throws CRUDException{
		try{
			StringBuffer sqlstr = new StringBuffer();
			
			int len = DateJudge.getDaysBetween(condition.getBdat(), condition.getEdat()) + 1;
			String dat = condition.getBdat();
			int w = 0;
			int days = 0;
			int days1 = 0;
			int days2 = 0;
			int daysw1 = 0;
			int daysw2 = 0;
			Map<String, Object> map = new HashMap<String, Object>();
			Map<String, Object> map1 = new HashMap<String, Object>();
			Map<String, Object> map2 = new HashMap<String, Object>();
			Map<String, Object> mapw1 = new HashMap<String, Object>();
			Map<String, Object> mapw2 = new HashMap<String, Object>();
			for (int i = 0; i < len; i++) {
				int weeks = 0;
				if (condition.getQueryMod() == 2) {
					weeks = DateJudge.getWeekOfYear(dat);
				} else {
					weeks=Integer.parseInt(dat.substring(5, 7));
				}
				
				List<Holiday> list = newReportMapper.fingHolidayByDholidaydate(dat);
				List<Holiday> list1 = newReportMapper.fingHolidayByDholidaydate(DateJudge.getLenTime(1, dat));
				
				//是否节假日
				if (list.size() == 0 && list1.size() > 0) {
					sqlstr.append(" SELECT '" + dat + "' AS DWORKDATE, '1' AS ICHANGETABLE, '" + weeks + "' AS WEEKS, 'N' AS ISHOLIDAY FROM DUAL");
					sqlstr.append(" UNION ALL ");
					sqlstr.append(" SELECT '" + dat + "' AS DWORKDATE, '2' AS ICHANGETABLE, '" + weeks + "' AS WEEKS, 'Y' AS ISHOLIDAY FROM DUAL");
				} else if (list.size() > 0) {
					sqlstr.append(" SELECT '" + dat + "' AS DWORKDATE, '1' AS ICHANGETABLE, '" + weeks + "' AS WEEKS, 'Y' AS ISHOLIDAY FROM DUAL");
					sqlstr.append(" UNION ALL ");
					sqlstr.append(" SELECT '" + dat + "' AS DWORKDATE, '2' AS ICHANGETABLE, '" + weeks + "' AS WEEKS, 'Y' AS ISHOLIDAY FROM DUAL");
				} else {
					sqlstr.append(" SELECT '" + dat + "' AS DWORKDATE, '1' AS ICHANGETABLE, '" + weeks + "' AS WEEKS,'N' AS ISHOLIDAY FROM DUAL");
					sqlstr.append(" UNION ALL ");
					sqlstr.append(" SELECT '" + dat + "' AS DWORKDATE, '2' AS ICHANGETABLE, '" + weeks + "' AS WEEKS,'N' AS ISHOLIDAY FROM DUAL");
				}
				
				if ((i + 1) != len) {
					sqlstr.append(" UNION ALL ");
				}
				
				// 周   天数
				if (w != weeks) {
					w = weeks;
					days = 1;
					if (list.size() == 0 && list1.size() > 0) {
						days1 = 1;
						daysw2 = 1;
					} else if (list.size() > 0) {
						daysw1 = 1;
						daysw2 = 1;
					} else {
						days1 = 1;
						days2 = 1;
					}
				} else {
					days += 1;
					if (list.size() == 0 && list1.size() > 0) {
						days1 += 1;
						daysw2 += 1;
					} else if (list.size() > 0) {
						daysw1 += 1;
						daysw2 += 1;
					} else {
						days1 += 1;
						days2 += 1;
					}
				}
				map.put("W"+weeks, days);
				map1.put("W"+weeks, days1);
				map2.put("W"+weeks, days2);
				mapw1.put("W"+weeks, daysw1);
				mapw2.put("W"+weeks, daysw2);
				dat = DateJudge.getLenTime(1, dat);
			}
			condition.setSqlstr(sqlstr.toString());
			
			List<String> ls = new ArrayList<String>();
			List<SiteType> st = newReportMapper.listSiteType(new SiteType());
			for (int i = 0; i < st.size(); i++) {
				ls.add(st.get(i).getVname());
			}
			
			List<Map<String, Object>> rs = new ArrayList<Map<String,Object>>();
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			List<Map<String, Object>> list = newReportMapper.findBusinessBurst(condition);
			
			//拼接数据
			Map<String, Object> lm = new HashMap<String, Object>();
			lm.put("STORENAME", condition.getFirmdes());
			
			//汇总--营收净额
			lm.put("VTYPE", "汇总");
			lm.put("VNAME", "营收净额");
			for (int j = 0; j < list.size(); j++) {
				Map<String, Object> li = list.get(j);
				for (int a = 0; a < map.size(); a++) {
					String ww = ((String) map.keySet().toArray()[a]).substring(1);
					lm.put("NYMONEY" + ww, Double.parseDouble(null != lm.get("NYMONEY" + ww) ? lm.get("NYMONEY"+ww).toString() : "0") + Double.parseDouble(ww.equals(li.get("WEEKS")) ? li.get("NYMONEY").toString() : "0"));
					lm.put("NYMONEYAVG" + ww, "0".equals(map.get("W" + ww).toString()) ? 0.00 : new BigDecimal(Double.parseDouble(lm.get("NYMONEY" + ww).toString()) / Double.parseDouble(map.get("W" + ww).toString())).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
				}
			}
			rs.add(lm);
			
			//汇总--客流
			lm = new HashMap<String, Object>();
			lm.put("VNAME", "客流");
			for (int j = 0; j < list.size(); j++) {
				Map<String, Object> li = list.get(j);
				for (int a = 0; a < map.size(); a++) {
					String ww = ((String) map.keySet().toArray()[a]).substring(1);
					lm.put("NYMONEY" + ww, Double.parseDouble(null != lm.get("NYMONEY" + ww) ? lm.get("NYMONEY" + ww).toString() : "0") + Double.parseDouble(ww.equals(li.get("WEEKS")) ? li.get("IPEOLENUM").toString() : "0"));
					lm.put("NYMONEYAVG" + ww, "0".equals(map.get("W" + ww).toString()) ? 0.00 : new BigDecimal(Double.parseDouble(lm.get("NYMONEY" + ww).toString()) / Double.parseDouble(map.get("W" + ww).toString())).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
				}
			}
			rs.add(lm);
			
			//汇总--折后单客
			lm = new HashMap<String, Object>();
			lm.put("VNAME", "折后单客");
			for (int j = 0; j < list.size(); j++) {
				Map<String, Object> li = list.get(j);
				for (int a = 0; a < map.size(); a++) {
					String ww = ((String) map.keySet().toArray()[a]).substring(1);
					lm.put("AMT" + ww, Double.parseDouble(null != lm.get("AMT" + ww) ? lm.get("AMT" + ww).toString():"0") + Double.parseDouble(ww.equals(li.get("WEEKS")) ? li.get("NYMONEY").toString() : "0"));
					lm.put("PAX" + ww, Double.parseDouble(null != lm.get("PAX" + ww) ? lm.get("PAX" + ww).toString():"0") + Double.parseDouble(ww.equals(li.get("WEEKS")) ? li.get("IPEOLENUM").toString() : "0"));
					lm.put("NYMONEY" + ww, "0.0".equals(lm.get("PAX" + ww).toString()) ? 0.00 : new BigDecimal(Double.parseDouble(lm.get("AMT" + ww).toString()) / Double.parseDouble(lm.get("PAX" + ww).toString())).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
					lm.put("NYMONEYAVG" + ww, "0.0".equals(lm.get("PAX" + ww).toString()) ? 0.00 : new BigDecimal(Double.parseDouble(lm.get("AMT" + ww).toString()) / Double.parseDouble(lm.get("PAX" + ww).toString()) / Double.parseDouble(map.get("W" + ww).toString())).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
				}
			}
			rs.add(lm);
			
			//桌台类型拼接
			for (int m = 0; m < ls.size(); m++) {
				
				//工午收入
				lm = new HashMap<String, Object>();
				lm.put("VTYPE", ls.get(m));
				lm.put("VNAME", "工午收入");	
				for (int j = 0; j < list.size(); j++) {
					Map<String, Object> li = list.get(j);
					for (int a = 0; a < map.size(); a++){
						String ww = ((String) map.keySet().toArray()[a]).substring(1);
						lm.put("NYMONEY" + ww, Double.parseDouble(null != lm.get("NYMONEY" + ww) ? lm.get("NYMONEY" + ww).toString() : "0") + Double.parseDouble(ww.equals(li.get("WEEKS")) && "N".equals(li.get("ISHOLIDAY")) && ls.get(m).equals(li.get("SITETYPENAME")) && "1".equals(li.get("ICHANGETABLE").toString()) ? li.get("NYMONEY").toString() : "0"));
						lm.put("NYMONEYAVG" + ww, "0".equals(map1.get("W" + ww).toString()) ? 0.00 : new BigDecimal(Double.parseDouble(lm.get("NYMONEY" + ww).toString()) / Double.parseDouble(map1.get("W" + ww).toString())).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
					}
				}
				rs.add(lm);
				
				//工晚收入
				lm = new HashMap<String, Object>();
				lm.put("VNAME", "工晚收入");
				for (int j = 0; j < list.size(); j++) {
					Map<String, Object> li = list.get(j);
					for (int a = 0; a < map.size(); a++) {
						String ww = ((String) map.keySet().toArray()[a]).substring(1);
						lm.put("NYMONEY" + ww, Double.parseDouble(null != lm.get("NYMONEY" + ww) ? lm.get("NYMONEY" + ww).toString() : "0") + Double.parseDouble(ww.equals(li.get("WEEKS")) && "N".equals(li.get("ISHOLIDAY")) && ls.get(m).equals(li.get("SITETYPENAME")) && "2".equals(li.get("ICHANGETABLE").toString()) ? li.get("NYMONEY").toString() : "0"));
						lm.put("NYMONEYAVG" + ww, "0".equals(map2.get("W" + ww).toString()) ? 0.00 : new BigDecimal(Double.parseDouble(lm.get("NYMONEY" + ww).toString()) / Double.parseDouble(map2.get("W" + ww).toString())).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
					}
				}
				rs.add(lm);
				
				//节午收入
				lm = new HashMap<String, Object>();
				lm.put("VNAME", "节午收入");
				for (int j = 0; j < list.size(); j++) {
					Map<String, Object> li = list.get(j);
					for (int a = 0; a < map.size(); a++) {
						String ww = ((String) map.keySet().toArray()[a]).substring(1);
						lm.put("NYMONEY" + ww, Double.parseDouble(null != lm.get("NYMONEY" + ww) ? lm.get("NYMONEY" + ww).toString() : "0") + Double.parseDouble(ww.equals(li.get("WEEKS")) && "Y".equals(li.get("ISHOLIDAY")) && ls.get(m).equals(li.get("SITETYPENAME")) && "1".equals(li.get("ICHANGETABLE").toString()) ? li.get("NYMONEY").toString() : "0"));
						lm.put("NYMONEYAVG" + ww, "0".equals(mapw1.get("W" + ww).toString()) ? 0.00 : new BigDecimal(Double.parseDouble(lm.get("NYMONEY" + ww).toString()) / Double.parseDouble(mapw1.get("W" + ww).toString())).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
					}
				}
				rs.add(lm);
				
				//节晚收入
				lm = new HashMap<String, Object>();
				lm.put("VNAME", "节晚收入");
				for (int j = 0; j < list.size(); j++) {
					Map<String, Object> li = list.get(j);
					for (int a = 0; a < map.size(); a++) {
						String ww = ((String) map.keySet().toArray()[a]).substring(1);
						lm.put("NYMONEY" + ww, Double.parseDouble(null != lm.get("NYMONEY" + ww) ? lm.get("NYMONEY" + ww).toString() : "0") + Double.parseDouble(ww.equals(li.get("WEEKS")) && "Y".equals(li.get("ISHOLIDAY")) && ls.get(m).equals(li.get("SITETYPENAME")) && "2".equals(li.get("ICHANGETABLE").toString()) ? li.get("NYMONEY").toString() : "0"));
						lm.put("NYMONEYAVG" + ww, "0".equals(mapw2.get("W" + ww).toString()) ? 0.00 : new BigDecimal(Double.parseDouble(lm.get("NYMONEY" + ww).toString()) / Double.parseDouble(mapw2.get("W" + ww).toString())).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
					}
				}
				rs.add(lm);
				
				//工午客流
				lm = new HashMap<String, Object>();
				lm.put("VNAME", "工午客流");
				for (int j = 0; j < list.size(); j++) {
					Map<String, Object> li = list.get(j);
					for (int a = 0; a < map.size(); a++) {
						String ww = ((String) map.keySet().toArray()[a]).substring(1);
						lm.put("NYMONEY" + ww, Double.parseDouble(null != lm.get("NYMONEY" + ww) ? lm.get("NYMONEY" + ww).toString() : "0") + Double.parseDouble(ww.equals(li.get("WEEKS")) && "N".equals(li.get("ISHOLIDAY")) && ls.get(m).equals(li.get("SITETYPENAME")) && "1".equals(li.get("ICHANGETABLE").toString()) ? li.get("IPEOLENUM").toString() : "0"));
						lm.put("NYMONEYAVG" + ww, "0".equals(map1.get("W" + ww).toString()) ? 0.00 : new BigDecimal(Double.parseDouble(lm.get("NYMONEY" + ww).toString()) / Double.parseDouble(map1.get("W" + ww).toString())).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
					}
				}
				rs.add(lm);
				
				//工晚客流
				lm = new HashMap<String, Object>();
				lm.put("VNAME", "工晚客流");
				for (int j = 0; j < list.size(); j++) {
					Map<String, Object> li = list.get(j);
					for (int a = 0; a < map.size(); a++) {
						String ww = ((String) map.keySet().toArray()[a]).substring(1);
						lm.put("NYMONEY" + ww, Double.parseDouble(null != lm.get("NYMONEY" + ww) ? lm.get("NYMONEY" + ww).toString() : "0")+Double.parseDouble(ww.equals(li.get("WEEKS")) && "N".equals(li.get("ISHOLIDAY")) && ls.get(m).equals(li.get("SITETYPENAME")) && "2".equals(li.get("ICHANGETABLE").toString()) ? li.get("IPEOLENUM").toString() : "0"));
						lm.put("NYMONEYAVG" + ww, "0".equals(map2.get("W" + ww).toString()) ? 0.00 : new BigDecimal(Double.parseDouble(lm.get("NYMONEY"+ww).toString()) / Double.parseDouble(map2.get("W" + ww).toString())).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
					}
				}
				rs.add(lm);
				
				//节午客流
				lm = new HashMap<String, Object>();
				lm.put("VNAME", "节午客流");
				for (int j = 0; j < list.size(); j++){
					Map<String, Object> li = list.get(j);
					for (int a = 0; a < map.size(); a++) {
						String ww = ((String) map.keySet().toArray()[a]).substring(1);
						lm.put("NYMONEY" + ww, Double.parseDouble(null != lm.get("NYMONEY" + ww) ? lm.get("NYMONEY" + ww).toString() : "0") + Double.parseDouble(ww.equals(li.get("WEEKS")) && "Y".equals(li.get("ISHOLIDAY")) && ls.get(m).equals(li.get("SITETYPENAME")) && "1".equals(li.get("ICHANGETABLE").toString()) ? li.get("IPEOLENUM").toString() : "0"));
						lm.put("NYMONEYAVG" + ww, "0".equals(mapw1.get("W" + ww).toString()) ? 0.00 : new BigDecimal(Double.parseDouble(lm.get("NYMONEY" + ww).toString()) / Double.parseDouble(mapw1.get("W" + ww).toString())).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
					}
				}
				rs.add(lm);
				
				//节晚客流
				lm = new HashMap<String, Object>();
				lm.put("VNAME", "节晚客流");
				for (int j = 0; j < list.size(); j++) {
					Map<String, Object> li = list.get(j);
					for (int a = 0; a < map.size(); a++) {
						String ww = ((String) map.keySet().toArray()[a]).substring(1);
						lm.put("NYMONEY"+ww, Double.parseDouble(null!=lm.get("NYMONEY"+ww)?lm.get("NYMONEY"+ww).toString():"0")+Double.parseDouble(ww.equals(li.get("WEEKS")) && "Y".equals(li.get("ISHOLIDAY")) && ls.get(m).equals(li.get("SITETYPENAME")) && "2".equals(li.get("ICHANGETABLE").toString())?li.get("IPEOLENUM").toString():"0"));
						lm.put("NYMONEYAVG"+ww, "0".equals(mapw2.get("W"+ww).toString())?0.00:new BigDecimal(Double.parseDouble(lm.get("NYMONEY"+ww).toString())/Double.parseDouble(mapw2.get("W"+ww).toString())).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
					}
				}
				rs.add(lm);
				
				//工午单客
				lm = new HashMap<String, Object>();
				lm.put("VNAME", "工午单客");
				for (int j = 0; j < list.size(); j++) {
					Map<String, Object> li = list.get(j);
					for (int a = 0; a < map.size(); a++) {
						String ww = ((String) map.keySet().toArray()[a]).substring(1);
						lm.put("AMT" + ww, Double.parseDouble(null != lm.get("AMT" + ww) ? lm.get("AMT" + ww).toString() : "0") + Double.parseDouble(ww.equals(li.get("WEEKS")) && "N".equals(li.get("ISHOLIDAY")) && ls.get(m).equals(li.get("SITETYPENAME")) && "1".equals(li.get("ICHANGETABLE").toString()) ? li.get("NYMONEY").toString() : "0"));
						lm.put("PAX" + ww, Double.parseDouble(null != lm.get("PAX" + ww) ? lm.get("PAX" + ww).toString() : "0") + Double.parseDouble(ww.equals(li.get("WEEKS")) && "N".equals(li.get("ISHOLIDAY")) && ls.get(m).equals(li.get("SITETYPENAME")) && "1".equals(li.get("ICHANGETABLE").toString()) ? li.get("IPEOLENUM").toString() : "0"));
						lm.put("NYMONEY" + ww, "0.0".equals(lm.get("PAX" + ww).toString()) ? 0.00 : new BigDecimal(Double.parseDouble(lm.get("AMT" + ww).toString()) / Double.parseDouble(lm.get("PAX" + ww).toString())).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
						lm.put("NYMONEYAVG" + ww, "0.0".equals(lm.get("PAX" + ww).toString()) ? 0.00 : new BigDecimal(Double.parseDouble(lm.get("AMT" + ww).toString()) / Double.parseDouble(lm.get("PAX" + ww).toString()) / Double.parseDouble(map1.get("W" + ww).toString())).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
					}
				}
				rs.add(lm);
				
				//工晚单客
				lm = new HashMap<String, Object>();
				lm.put("VNAME", "工晚单客");
				for (int j = 0; j < list.size(); j++) {
					Map<String, Object> li = list.get(j);
					for (int a = 0; a < map.size(); a++){
						String ww = ((String) map.keySet().toArray()[a]).substring(1);
						lm.put("AMT" + ww, Double.parseDouble(null != lm.get("AMT" + ww) ? lm.get("AMT" + ww).toString() : "0") + Double.parseDouble(ww.equals(li.get("WEEKS")) && "N".equals(li.get("ISHOLIDAY")) && ls.get(m).equals(li.get("SITETYPENAME")) && "2".equals(li.get("ICHANGETABLE").toString()) ? li.get("NYMONEY").toString() : "0"));
						lm.put("PAX" + ww, Double.parseDouble(null != lm.get("PAX" + ww) ? lm.get("PAX" + ww).toString() : "0") + Double.parseDouble(ww.equals(li.get("WEEKS")) && "N".equals(li.get("ISHOLIDAY")) && ls.get(m).equals(li.get("SITETYPENAME")) && "2".equals(li.get("ICHANGETABLE").toString()) ? li.get("IPEOLENUM").toString() : "0"));
						lm.put("NYMONEY" + ww, "0.0".equals(lm.get("PAX" + ww).toString()) ? 0.00 : new BigDecimal(Double.parseDouble(lm.get("AMT" + ww).toString()) / Double.parseDouble(lm.get("PAX" + ww).toString())).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
						lm.put("NYMONEYAVG" + ww, "0.0".equals(lm.get("PAX" + ww).toString()) ? 0.00 : new BigDecimal(Double.parseDouble(lm.get("AMT" + ww).toString()) / Double.parseDouble(lm.get("PAX" + ww).toString()) / Double.parseDouble(map2.get("W" + ww).toString())).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
					}
				}
				rs.add(lm);
				
				//节午单客
				lm = new HashMap<String, Object>();
				lm.put("VNAME", "节午单客");
				for (int j = 0; j < list.size(); j++) {
					Map<String, Object> li = list.get(j);
					for (int a = 0; a < map.size(); a++) {
						String ww = ((String) map.keySet().toArray()[a]).substring(1);
						lm.put("AMT" + ww, Double.parseDouble(null != lm.get("AMT" + ww) ? lm.get("AMT" + ww).toString() : "0") + Double.parseDouble(ww.equals(li.get("WEEKS")) && "Y".equals(li.get("ISHOLIDAY")) && ls.get(m).equals(li.get("SITETYPENAME")) && "1".equals(li.get("ICHANGETABLE").toString()) ? li.get("NYMONEY").toString() : "0"));
						lm.put("PAX" + ww, Double.parseDouble(null != lm.get("PAX" + ww) ? lm.get("PAX" + ww).toString() : "0") + Double.parseDouble(ww.equals(li.get("WEEKS")) && "Y".equals(li.get("ISHOLIDAY")) && ls.get(m).equals(li.get("SITETYPENAME")) && "1".equals(li.get("ICHANGETABLE").toString()) ? li.get("IPEOLENUM").toString() : "0"));
						lm.put("NYMONEY" + ww, "0.0".equals(lm.get("PAX" + ww).toString()) ? 0.00:new BigDecimal(Double.parseDouble(lm.get("AMT" + ww).toString()) / Double.parseDouble(lm.get("PAX" + ww).toString())).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
						lm.put("NYMONEYAVG" + ww, "0.0".equals(lm.get("PAX" + ww).toString()) ? 0.00:new BigDecimal(Double.parseDouble(lm.get("AMT" + ww).toString()) / Double.parseDouble(lm.get("PAX" + ww).toString()) / Double.parseDouble(mapw1.get("W" + ww).toString())).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
					}
				}
				rs.add(lm);
				
				//节晚单客
				lm = new HashMap<String, Object>();
				lm.put("VNAME", "节晚单客");
				for (int j = 0; j < list.size(); j++) {
					Map<String, Object> li = list.get(j);
					for (int a = 0; a < map.size(); a++) {
						String ww = ((String) map.keySet().toArray()[a]).substring(1);
						lm.put("AMT" + ww, Double.parseDouble(null != lm.get("AMT" + ww) ? lm.get("AMT" + ww).toString():"0") + Double.parseDouble(ww.equals(li.get("WEEKS")) && "Y".equals(li.get("ISHOLIDAY")) && ls.get(m).equals(li.get("SITETYPENAME")) && "2".equals(li.get("ICHANGETABLE").toString()) ? li.get("NYMONEY").toString() : "0"));
						lm.put("PAX" + ww, Double.parseDouble(null != lm.get("PAX" + ww) ? lm.get("PAX" + ww).toString():"0") + Double.parseDouble(ww.equals(li.get("WEEKS")) && "Y".equals(li.get("ISHOLIDAY")) && ls.get(m).equals(li.get("SITETYPENAME")) && "2".equals(li.get("ICHANGETABLE").toString()) ? li.get("IPEOLENUM").toString() : "0"));
						lm.put("NYMONEY" + ww, "0.0".equals(lm.get("PAX" + ww).toString()) ? 0.00:new BigDecimal(Double.parseDouble(lm.get("AMT" + ww).toString()) / Double.parseDouble(lm.get("PAX" + ww).toString())).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
						lm.put("NYMONEYAVG" + ww, "0.0".equals(lm.get("PAX" + ww).toString()) ? 0.00:new BigDecimal(Double.parseDouble(lm.get("AMT" + ww).toString()) / Double.parseDouble(lm.get("PAX" + ww).toString()) / Double.parseDouble(mapw2.get("W" + ww).toString())).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
					}
				}
				rs.add(lm);
			}
			
			reportObject.setRows(rs);
			reportObject.setFooter(null);
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：西贝  营业时段解析表
	 * @author 马振
	 * 创建时间：2016-2-24 上午9:57:18
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findSaleDataByTime(PublicEntity condition)throws CRUDException{
		try{
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("bdat", condition.getBdat());
			map.put("edat", condition.getEdat());
			map.put("pk_store", FormatDouble.StringCodeReplace(condition.getPk_store()));
			map.put("dateList", DateJudge.getTimeList(condition.getInterval()));
			map.put("days", DateJudge.getDaysBetween(condition.getBdat(), condition.getEdat())+1);
			
			reportObject.setRows(newReportMapper.findSaleDataByTime(map));
			reportObject.setFooter(newReportMapper.findSaleDataByTimeCal(map));
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：西贝   菜品见台率统计分析
	 * @author 马振
	 * 创建时间：2016-3-3 上午9:57:22
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findCaiPinJianTaiLv(PublicEntity condition) throws CRUDException {
		try{
			Page page = condition.getPager();
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			reportObject.setRows(pageManager.selectPage(condition, page, MisBohNewReportMapper.class.getName() + ".findCaiPinJianTaiLv"));
			reportObject.setFooter(newReportMapper.findCalCaiPinJianTaiLv(condition));
			reportObject.setTotal(page.getCount());
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：排班报表    权金城
	 * 作者：马振
	 * 时间：2017年5月2日上午10:35:39
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> queryScheduleReport7(PublicEntity condition)  throws CRUDException {
		try {
			Page pager = condition.getPager();
			String cols = "NMONEY,NYMONEY";
			
			reportObject.setRows(MisUtil.formatNumForListResult(pageManager.selectPage(condition, pager, MisBohNewReportMapper.class.getName() + ".queryScheduleReport7"),cols,2));
			reportObject.setFooter(MisUtil.formatNumForListResult(newReportMapper.queryCalScheduleReport7(condition),cols,2));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
}