package com.choice.misboh.service.reportMis;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.misboh.persistence.reportMis.CostItemMisBohMapper;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
@Service
public class YueChengbenZongheFenxiMisBohService {

	@Autowired
	private CostItemMisBohMapper costItemMisBohMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
    private final transient Log log = LogFactory.getLog(YueChengbenZongheFenxiMisBohService.class);
	
	/**
	 * 月成本综合分析
	 * @param conditions
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findYueChengbenZongheFenxi(Map<String,Object> conditions) throws CRUDException{
		try{
			SupplyAcct supplyAcct = (SupplyAcct)conditions.get("supplyAcct");
			List<Map<String,Object>> listNMoney = costItemMisBohMapper.findNMoneyList(supplyAcct);//查营业数据
			List<Map<String,Object>> chengben = costItemMisBohMapper.findYueChengbenZongheFenxi_chengb(supplyAcct);//成本
            List<Map<String,Object>> sdq = costItemMisBohMapper.findYueChengbenZongheFenxi_sdq(supplyAcct);//水电气
            if((chengben!=null&&chengben.size()>0)||(sdq!=null&&sdq.size()>0)) {
                List<Map<String, Object>> mapResult = new ArrayList<Map<String, Object>>();
                if (listNMoney != null && listNMoney.size() > 0) {
                    for (Map<String, Object> moneyMap : listNMoney) {
                    	//都默认按0算
                    	//1收入|折前饭菜毛利率  zqmll （折前饭菜-实际成本）/折前饭菜*100  暂不用
//                      if(string2Big(moneyMap.get("MONEY").toString()).doubleValue()!=0) {
//                          moneyMap.put("ZQMLL", string2Big(moneyMap.get("MONEY")
//                                  .toString()).setScale(4, BigDecimal.ROUND_HALF_UP)
//                                  .divide(string2Big(moneyMap.get("MONEY").toString()), 4).multiply(BigDecimal.valueOf(100)));
//                      }else {
//                          moneyMap.put("ZQMLL",0);
//                      }
                      //2.收入|综合毛利率     （营业额-实际成本）/营业额*100  暂不用
//                      if(string2Big(moneyMap.get("YMONEY").toString()).doubleValue()!=0) {
//                          moneyMap.put("ZHMLL", string2Big(moneyMap.get("YMONEY")
//                                  .toString()).setScale(4, BigDecimal.ROUND_HALF_UP)
//                                  .divide(string2Big(moneyMap.get("YMONEY").toString()), 4).multiply(BigDecimal.valueOf(100)));
//                      }else {
//                          moneyMap.put("ZHMLL",0);
//                      }
                      //3.收入|折前毛利率   BPROFITS  （营业额-实际成本）/营业额*100
                      if(string2Big(moneyMap.get("YMONEY").toString()).doubleValue()!=0) {
                          moneyMap.put("BPROFITS", string2Big(moneyMap.get("YMONEY")
                                  .toString()).setScale(4, BigDecimal.ROUND_HALF_UP)
                                  .divide(string2Big(moneyMap.get("YMONEY").toString()), 4).multiply(BigDecimal.valueOf(100)));
                      }else {
                          moneyMap.put("BPROFITS",0);
                      }
                      //4.收入|折后毛利率   APROFITS  （净营业额-实际成本）/净营业额*100
                      if(string2Big(moneyMap.get("NETSALES").toString()).doubleValue()!=0) {
                          moneyMap.put("APROFITS", string2Big(moneyMap.get("NETSALES")
                                  .toString()).setScale(4, BigDecimal.ROUND_HALF_UP)
                                  .divide(string2Big(moneyMap.get("NETSALES").toString()), 4).multiply(BigDecimal.valueOf(100)));
                      }else {
                          moneyMap.put("APROFITS",0);
                      }
                        if(chengben!=null && chengben.size() > 0){
                            for(int i=0;i<chengben.size();i++){
                                Map<String,Object> cbMap=chengben.get(i);
                                if(cbMap.get("DAT").equals(moneyMap.get("DAT"))) {
                                    moneyMap.putAll(cbMap);
                                    //1收入|折前饭菜毛利率  zqmll （折前饭菜-实际成本）/折前饭菜*100  未算
//                                    if(string2Big(moneyMap.get("MONEY").toString()).doubleValue()!=0) {
//                                        moneyMap.put("ZQMLL", string2Big(moneyMap.get("MONEY")
//                                                .toString()).subtract(string2Big(cbMap.get("TOTALCOST").toString()))
//                                                .setScale(4, BigDecimal.ROUND_HALF_UP)
//                                                .divide(string2Big(moneyMap.get("MONEY").toString()), 4).multiply(BigDecimal.valueOf(100)));
//                                    }else {
//                                        moneyMap.put("ZQMLL",0);
//                                    }
                                    //2.收入|综合毛利率     （营业额-实际成本）/营业额*100
//                                    if(string2Big(moneyMap.get("YMONEY").toString()).doubleValue()!=0) {
//                                        moneyMap.put("ZHMLL", string2Big(moneyMap.get("YMONEY")
//                                                .toString()).subtract(string2Big(cbMap.get("TOTALCOST").toString()))
//                                                .setScale(4, BigDecimal.ROUND_HALF_UP)
//                                                .divide(string2Big(moneyMap.get("YMONEY").toString()), 4).multiply(BigDecimal.valueOf(100)));
//                                    }else {
//                                        moneyMap.put("ZHMLL",0);
//                                    }
                                    //3.收入|折前毛利率   BPROFITS  
                                    if(string2Big(moneyMap.get("YMONEY").toString()).doubleValue()!=0) {
                                        moneyMap.put("BPROFITS", string2Big(moneyMap.get("YMONEY")
                                                .toString()).subtract(string2Big(cbMap.get("TOTALCOST").toString()))
                                                .setScale(4, BigDecimal.ROUND_HALF_UP)
                                                .divide(string2Big(moneyMap.get("YMONEY").toString()), 4).multiply(BigDecimal.valueOf(100)));
                                    }else {
                                        moneyMap.put("BPROFITS",0);
                                    }
                                    //4.收入|折后毛利率   APROFITS  
                                    if(string2Big(moneyMap.get("NETSALES").toString()).doubleValue()!=0) {
                                        moneyMap.put("APROFITS", string2Big(moneyMap.get("NETSALES")
                                                .toString()).subtract(string2Big(cbMap.get("TOTALCOST").toString()))
                                                .setScale(4, BigDecimal.ROUND_HALF_UP)
                                                .divide(string2Big(moneyMap.get("NETSALES").toString()), 4).multiply(BigDecimal.valueOf(100)));
                                    }else {
                                        moneyMap.put("APROFITS",0);
                                    }
                                    //5.厨房原料占折前饭菜   cfylzqfc
                                    if(string2Big(moneyMap.get("MONEY").toString()).doubleValue()!=0) {
                                        moneyMap.put("CFYLZQFC", string2Big(cbMap.get("CFYL").toString())
                                                .setScale(4, BigDecimal.ROUND_HALF_UP)
                                                .divide(string2Big(moneyMap.get("MONEY").toString()), 4).multiply(BigDecimal.valueOf(100)));
                                    }else {
                                        moneyMap.put("CFYLZQFC",0);
                                    }
                                    //6.厨房调料占折前饭菜   cftlzqfc
                                    if(string2Big(moneyMap.get("MONEY").toString()).doubleValue()!=0) {
                                        moneyMap.put("CFTLZQFC", string2Big(cbMap.get("CFTL").toString())
                                                .setScale(4, BigDecimal.ROUND_HALF_UP)
                                                .divide(string2Big(moneyMap.get("MONEY").toString()), 4).multiply(BigDecimal.valueOf(100)));
                                    }else {
                                        moneyMap.put("CFTLZQFC",0);
                                    }
                                    //8.成本|职工餐人均 rjzgc = 职工餐/人数
                                    if(string2Big(moneyMap.get("YGS").toString()).doubleValue()!=0) {
                                        moneyMap.put("RJZGC", string2Big(cbMap.get("ZGC").toString()).setScale(4, BigDecimal.ROUND_HALF_UP)
                                                .divide(string2Big(moneyMap.get("YGS").toString()), 4));
                                    }else {
                                        moneyMap.put("RJZGC",0);
                                    }
                                }
                            }
                        }else{
                            
                        }
                        if(sdq!=null){
                            for(int i=0;i<sdq.size();i++){
                                Map<String,Object> sdqMap=sdq.get(i);
                                if(sdqMap.get("CODE").equals(moneyMap.get("CODE"))&&sdqMap.get("DAT").equals(moneyMap.get("DAT"))) {
                                    moneyMap.putAll(sdqMap);
                                    //7.菜均天然气用量 cjtrq
                                    if(string2Big(moneyMap.get("CPZSL").toString()).doubleValue()!=0) {
                                        moneyMap.put("CJTRQ", string2Big(moneyMap.get("TRQ").toString()).setScale(4, BigDecimal.ROUND_HALF_UP)
                                                .divide(string2Big(moneyMap.get("CPZSL").toString()), 4));
                                    }else {
                                        moneyMap.put("CJTRQ",0);
                                    }
                                    //9.费用|人均水费 rjsf
                                    if(string2Big(moneyMap.get("YGS").toString()).doubleValue()!=0) {
                                        moneyMap.put("RJSF", string2Big(moneyMap.get("SF").toString()).setScale(4, BigDecimal.ROUND_HALF_UP)
                                                .divide(string2Big(moneyMap.get("YGS").toString()), 4));
                                    }else {
                                        moneyMap.put("RJSF",0);
                                    }
                                }
                            }
                        }
                        mapResult.add(moneyMap);
                    }
                }
                mapReportObject.setRows(mapResult);
                mapReportObject.setFooter(findYueChengbenZongheFenxi_hj(supplyAcct));
                return mapReportObject;
            }
		}catch(Exception e){
            log.error(e.getMessage());
			throw new CRUDException(e);
		}
		return mapReportObject;
	}
	
	/**
	 * 月成本综合分析
	 * @param conditions
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String,Object>> findYueChengbenZongheFenxi_hj(SupplyAcct supplyAcct) throws CRUDException{
		try{
			List<Map<String,Object>> listNMoney = costItemMisBohMapper.findNMoneyList_hj(supplyAcct);//查营业数据
			List<Map<String,Object>> chengben = costItemMisBohMapper.findYueChengbenZongheFenxi_chengb_hj(supplyAcct);//成本
            List<Map<String,Object>> sdq = costItemMisBohMapper.findYueChengbenZongheFenxi_sdq_hj(supplyAcct);//水电气
            if((chengben!=null&&chengben.size()>0)||(sdq!=null&&sdq.size()>0)) {
                List<Map<String, Object>> mapResult = new ArrayList<Map<String, Object>>();
                if (listNMoney != null && listNMoney.size() > 0) {
                    for (Map<String, Object> moneyMap : listNMoney) {
                        if(chengben!=null && chengben.size() > 0){
                            for(int i=0;i<chengben.size();i++){
                                Map<String,Object> cbMap=chengben.get(i);
                                moneyMap.putAll(cbMap);
                                //1收入|折前饭菜毛利率  zqmll （折前饭菜-实际成本）/折前饭菜*100
                                if(string2Big(moneyMap.get("MONEY").toString()).doubleValue()!=0) {
                                    moneyMap.put("ZQMLL", string2Big(moneyMap.get("MONEY")
                                            .toString()).subtract(string2Big(cbMap.get("TOTALCOST").toString()))
                                            .setScale(4, BigDecimal.ROUND_HALF_UP)
                                            .divide(string2Big(moneyMap.get("MONEY").toString()), 4).multiply(BigDecimal.valueOf(100)));
                                }else {
                                    moneyMap.put("ZQMLL",0);
                                }
                                //2.收入|综合毛利率     （营业额-实际成本）/营业额*100
                                if(string2Big(moneyMap.get("YMONEY").toString()).doubleValue()!=0) {
                                    moneyMap.put("ZHMLL", string2Big(moneyMap.get("YMONEY")
                                            .toString()).subtract(string2Big(cbMap.get("TOTALCOST").toString()))
                                            .setScale(4, BigDecimal.ROUND_HALF_UP)
                                            .divide(string2Big(moneyMap.get("YMONEY").toString()), 4).multiply(BigDecimal.valueOf(100)));
                                }else {
                                    moneyMap.put("ZHMLL",0);
                                }
                                //3.收入|折前毛利率   BPROFITS  
                                if(string2Big(moneyMap.get("YMONEY").toString()).doubleValue()!=0) {
                                    moneyMap.put("BPROFITS", string2Big(moneyMap.get("YMONEY")
                                            .toString()).subtract(string2Big(cbMap.get("TOTALCOST").toString()))
                                            .setScale(4, BigDecimal.ROUND_HALF_UP)
                                            .divide(string2Big(moneyMap.get("YMONEY").toString()), 4).multiply(BigDecimal.valueOf(100)));
                                }else {
                                    moneyMap.put("BPROFITS",0);
                                }
                                //4.收入|折后毛利率   APROFITS  
                                if(string2Big(moneyMap.get("NETSALES").toString()).doubleValue()!=0) {
                                    moneyMap.put("APROFITS", string2Big(moneyMap.get("NETSALES")
                                            .toString()).subtract(string2Big(cbMap.get("TOTALCOST").toString()))
                                            .setScale(4, BigDecimal.ROUND_HALF_UP)
                                            .divide(string2Big(moneyMap.get("NETSALES").toString()), 4).multiply(BigDecimal.valueOf(100)));
                                }else {
                                    moneyMap.put("APROFITS",0);
                                }
                                //5.厨房原料占折前饭菜   cfylzqfc
                                if(string2Big(moneyMap.get("MONEY").toString()).doubleValue()!=0) {
                                    moneyMap.put("CFYLZQFC", string2Big(moneyMap.get("CFYL").toString())
                                            .setScale(4, BigDecimal.ROUND_HALF_UP)
                                            .divide(string2Big(moneyMap.get("MONEY").toString()), 4).multiply(BigDecimal.valueOf(100)));
                                }else {
                                    moneyMap.put("CFYLZQFC",0);
                                }
                                //6.厨房调料占折前饭菜   cftlzqfc
                                if(string2Big(moneyMap.get("MONEY").toString()).doubleValue()!=0) {
                                    moneyMap.put("CFTLZQFC", string2Big(cbMap.get("CFTL").toString())
                                            .setScale(4, BigDecimal.ROUND_HALF_UP)
                                            .divide(string2Big(moneyMap.get("MONEY").toString()), 4).multiply(BigDecimal.valueOf(100)));
                                }else {
                                    moneyMap.put("CFTLZQFC",0);
                                }
                                //8.成本|职工餐人均 rjzgc = 职工餐/人数
                                if(string2Big(moneyMap.get("YGS").toString()).doubleValue()!=0) {
                                    moneyMap.put("RJZGC", string2Big(moneyMap.get("ZGC").toString()).setScale(4, BigDecimal.ROUND_HALF_UP)
                                            .divide(string2Big(moneyMap.get("YGS").toString()), 4));
                                }else {
                                    moneyMap.put("RJZGC",0);
                                }
                            }
                        }
                        if(sdq!=null){
                            for(int i=0;i<sdq.size();i++){
                                Map<String,Object> sdqMap=sdq.get(i);
                                moneyMap.putAll(sdqMap);
                                //7.菜均天然气用量 cjtrq
                                if(string2Big(moneyMap.get("CPZSL").toString()).doubleValue()!=0) {
                                    moneyMap.put("CJTRQ", string2Big(moneyMap.get("TRQ").toString()).setScale(4, BigDecimal.ROUND_HALF_UP)
                                            .divide(string2Big(moneyMap.get("CPZSL").toString()), 4));
                                }else {
                                    moneyMap.put("CJTRQ",0);
                                }
                                //9.费用|人均水费 rjsf
                                if(string2Big(moneyMap.get("YGS").toString()).doubleValue()!=0) {
                                    moneyMap.put("RJSF", string2Big(moneyMap.get("SF").toString()).setScale(4, BigDecimal.ROUND_HALF_UP)
                                            .divide(string2Big(moneyMap.get("YGS").toString()), 4));
                                }else {
                                    moneyMap.put("RJSF",0);
                                }
                            }
                        }
                        mapResult.add(moneyMap);
                    }
                }
                return mapResult;
            }
		}catch(Exception e){
            log.error(e.getMessage());
			throw new CRUDException(e);
		}
		return null;
	}

    private BigDecimal string2Big(String val){
        return new BigDecimal((val!=null&&val.trim()!="")?val:"0");
    }
    
    /**
	 * 月成本综合分析
	 * @param conditions
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findYueChengbenZongheFenxiChoice3(Map<String,Object> conditions) throws CRUDException{
		try{
			SupplyAcct supplyAcct = (SupplyAcct)conditions.get("supplyAcct");
			List<Map<String,Object>> listNMoney = costItemMisBohMapper.findNMoneyListChoice3(supplyAcct);//查营业数据
			List<Map<String,Object>> chengben = costItemMisBohMapper.findYueChengbenZongheFenxi_chengbChoice3(supplyAcct);//成本
            List<Map<String, Object>> mapResult = new ArrayList<Map<String, Object>>();
            if (listNMoney != null && listNMoney.size() > 0) {
                for (Map<String, Object> moneyMap : listNMoney) {
                    if(chengben!=null && chengben.size() > 0){
                        for(int i=0;i<chengben.size();i++){
                            Map<String,Object> cbMap=chengben.get(i);
                            if(cbMap.get("DAT").equals(moneyMap.get("DAT"))) {
                                moneyMap.putAll(cbMap);
                                //（折前饭菜收入-厨房原料-厨房调料）/折前饭菜收入*100%
                                //1收入|折前饭菜毛利率  zqmll （折前饭菜-厨房成本）/折前饭菜*100  厨房成本=厨房原料+厨房调料  西贝20160323发来的算法
                                if(string2Big(moneyMap.get("ZQFC").toString()).doubleValue()!=0) {
	                                if("西贝莜面".equals(conditions.get("acctDes").toString()))//如果是西贝  计算折前饭菜毛利率使用的收入=折前饭菜收入/1.06；计算综合毛利率使用的收入=营收净额/1.06
	                            		moneyMap.put("ZQMLL", string2Big(moneyMap.get("ZQFC").toString()).divide(BigDecimal.valueOf(1.06),4)
	                            				.subtract(string2Big(cbMap.get("CFCB").toString()))
	                                            .setScale(4, BigDecimal.ROUND_HALF_UP)
	                                            .divide(string2Big(moneyMap.get("ZQFC").toString()), 4).multiply(BigDecimal.valueOf(1.06)).multiply(BigDecimal.valueOf(100)));
	                            	else
	                                    moneyMap.put("ZQMLL", string2Big(moneyMap.get("ZQFC").toString())
	                                    		.subtract(string2Big(cbMap.get("CFCB").toString()))
	                                            .setScale(4, BigDecimal.ROUND_HALF_UP)
	                                            .divide(string2Big(moneyMap.get("ZQFC").toString()), 4).multiply(BigDecimal.valueOf(100)));
                                }else {
                                    moneyMap.put("ZQMLL",0);
                                }
                                //（营收净额-厨房原调料-吧台展台原料）/营收净额*100%
                                //2.收入|综合毛利率     （营业额-实际成本）/营业额*100  
                                if(string2Big(moneyMap.get("YYE").toString()).doubleValue()!=0) {
                                	if("西贝莜面".equals(conditions.get("acctDes").toString()))//如果是西贝  计算折前饭菜毛利率使用的收入=折前饭菜收入/1.06；计算综合毛利率使用的收入=营收净额/1.06
                                		moneyMap.put("ZHMLL", string2Big(moneyMap.get("YYE").toString()).divide(BigDecimal.valueOf(1.06),4)
                                				.subtract(string2Big(cbMap.get("CFBTCB").toString()))
                                                .setScale(4, BigDecimal.ROUND_HALF_UP)
                                                .divide(string2Big(moneyMap.get("YYE").toString()), 4).multiply(BigDecimal.valueOf(1.06)).multiply(BigDecimal.valueOf(100)));
                                	else
                                		moneyMap.put("ZHMLL", string2Big(moneyMap.get("YYE").toString())
                                				.subtract(string2Big(cbMap.get("CFBTCB").toString()))
                                				.setScale(4, BigDecimal.ROUND_HALF_UP)
                                				.divide(string2Big(moneyMap.get("YYE").toString()), 4).multiply(BigDecimal.valueOf(100)));
                                }else {
                                    moneyMap.put("ZHMLL",0);
                                }
                                //3.厨房原料占折前饭菜   cfylzqfc
                                if(string2Big(moneyMap.get("ZQFC").toString()).doubleValue()!=0) {
                                    moneyMap.put("CFYLZQFC", string2Big(cbMap.get("CFYL").toString())
                                            .setScale(4, BigDecimal.ROUND_HALF_UP)
                                            .divide(string2Big(moneyMap.get("ZQFC").toString()), 4).multiply(BigDecimal.valueOf(100)));
                                }else {
                                    moneyMap.put("CFYLZQFC",0);
                                }
                                //4.厨房调料占折前饭菜   cftlzqfc
                                if(string2Big(moneyMap.get("ZQFC").toString()).doubleValue()!=0) {
                                    moneyMap.put("CFTLZQFC", string2Big(cbMap.get("CFTL").toString())
                                            .setScale(4, BigDecimal.ROUND_HALF_UP)
                                            .divide(string2Big(moneyMap.get("ZQFC").toString()), 4).multiply(BigDecimal.valueOf(100)));
                                }else {
                                    moneyMap.put("CFTLZQFC",0);
                                }
                                //5.成本|职工餐人均 rjzgc = 职工餐/人数
                                if(string2Big(moneyMap.get("YGS").toString()).doubleValue()!=0) {
                                    moneyMap.put("ZGCRJ", string2Big(cbMap.get("ZGC").toString()).setScale(4, BigDecimal.ROUND_HALF_UP)
                                            .divide(string2Big(moneyMap.get("YGS").toString()), 4));
                                }else {
                                    moneyMap.put("ZGCRJ",0);
                                }
                                
                                //6.菜均天然气用量 cjtrq
                                if(string2Big(moneyMap.get("FCFS").toString()).doubleValue()!=0) {
                                    moneyMap.put("CJTRQ", string2Big(moneyMap.get("TRQ").toString()).setScale(4, BigDecimal.ROUND_HALF_UP)
                                            .divide(string2Big(moneyMap.get("FCFS").toString()), 4));
                                }else {
                                    moneyMap.put("CJTRQ",0);
                                }
                                //7.费用|人均水费 rjsf
                                if(string2Big(moneyMap.get("YGS").toString()).doubleValue()!=0) {
                                    moneyMap.put("RJSF", string2Big(moneyMap.get("SF").toString()).setScale(4, BigDecimal.ROUND_HALF_UP)
                                            .divide(string2Big(moneyMap.get("YGS").toString()), 4));
                                }else {
                                    moneyMap.put("RJSF",0);
                                }
                            }
                        }
                    }
                    mapResult.add(moneyMap);
                }
            }
            mapReportObject.setRows(mapResult);
            mapReportObject.setFooter(findYueChengbenZongheFenxi_hjChoice3(conditions));
            return mapReportObject;
		}catch(Exception e){
            log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 月成本综合分析
	 * @param conditions
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String,Object>> findYueChengbenZongheFenxi_hjChoice3(Map<String,Object> conditions) throws CRUDException{
		try{
			SupplyAcct supplyAcct = (SupplyAcct)conditions.get("supplyAcct");
			List<Map<String,Object>> listNMoney = costItemMisBohMapper.findNMoneyList_hjChoice3(supplyAcct);//查营业数据
			List<Map<String,Object>> chengben = costItemMisBohMapper.findYueChengbenZongheFenxi_chengb_hjChoice3(supplyAcct);//成本
            List<Map<String, Object>> mapResult = new ArrayList<Map<String, Object>>();
            if (listNMoney != null && listNMoney.size() > 0) {
                for (Map<String, Object> moneyMap : listNMoney) {
                    if(chengben!=null && chengben.size() > 0 && chengben.get(0).get("CFCB") != null){
                        for(int i=0;i<chengben.size();i++){
                            Map<String,Object> cbMap=chengben.get(i);
                            moneyMap.putAll(cbMap);
                            //1收入|折前饭菜毛利率  zqmll （折前饭菜-实际成本）/折前饭菜*100  未算
                            if(string2Big(moneyMap.get("ZQFC").toString()).doubleValue()!=0) {
                            	if("西贝莜面".equals(conditions.get("acctDes").toString()))//如果是西贝  计算折前饭菜毛利率使用的收入=折前饭菜收入/1.06；计算综合毛利率使用的收入=营收净额/1.06
                            		moneyMap.put("ZQMLL", string2Big(moneyMap.get("ZQFC").toString()).divide(BigDecimal.valueOf(1.06),4)
                            				.subtract(string2Big(cbMap.get("CFCB").toString()))
                                            .setScale(4, BigDecimal.ROUND_HALF_UP)
                                            .divide(string2Big(moneyMap.get("ZQFC").toString()), 4).multiply(BigDecimal.valueOf(1.06)).multiply(BigDecimal.valueOf(100)));
                            	else
                            		moneyMap.put("ZQMLL", string2Big(moneyMap.get("ZQFC").toString())
                            				.subtract(string2Big(cbMap.get("CFCB").toString()))
                            				.setScale(4, BigDecimal.ROUND_HALF_UP)
                            				.divide(string2Big(moneyMap.get("ZQFC").toString()), 4).multiply(BigDecimal.valueOf(100)));
                            }else {
                                moneyMap.put("ZQMLL",0);
                            }
                            //2.收入|综合毛利率     （营业额-实际成本）/营业额*100
                            if(string2Big(moneyMap.get("YYE").toString()).doubleValue()!=0) {
                            	if("西贝莜面".equals(conditions.get("acctDes").toString()))//如果是西贝  计算折前饭菜毛利率使用的收入=折前饭菜收入/1.06；计算综合毛利率使用的收入=营收净额/1.06
                            		moneyMap.put("ZHMLL", string2Big(moneyMap.get("YYE").toString()).divide(BigDecimal.valueOf(1.06),4)
                            				.subtract(string2Big(cbMap.get("CFBTCB").toString()))
                                            .setScale(4, BigDecimal.ROUND_HALF_UP)
                                            .divide(string2Big(moneyMap.get("YYE").toString()), 4).multiply(BigDecimal.valueOf(1.06)).multiply(BigDecimal.valueOf(100)));
                            	else
                            		moneyMap.put("ZHMLL", string2Big(moneyMap.get("YYE").toString())
                            				.subtract(string2Big(cbMap.get("CFBTCB").toString()))
                            				.setScale(4, BigDecimal.ROUND_HALF_UP)
                            				.divide(string2Big(moneyMap.get("YYE").toString()), 4).multiply(BigDecimal.valueOf(100)));
                            }else {
                                moneyMap.put("ZHMLL",0);
                            }
                            //3.厨房原料占折前饭菜   cfylzqfc
                            if(string2Big(moneyMap.get("ZQFC").toString()).doubleValue()!=0) {
                                moneyMap.put("CFYLZQFC", string2Big(cbMap.get("CFYL").toString())
                                        .setScale(4, BigDecimal.ROUND_HALF_UP)
                                        .divide(string2Big(moneyMap.get("ZQFC").toString()), 4).multiply(BigDecimal.valueOf(100)));
                            }else {
                                moneyMap.put("CFYLZQFC",0);
                            }
                            //4.厨房调料占折前饭菜   cftlzqfc
                            if(string2Big(moneyMap.get("ZQFC").toString()).doubleValue()!=0) {
                                moneyMap.put("CFTLZQFC", string2Big(cbMap.get("CFTL").toString())
                                        .setScale(4, BigDecimal.ROUND_HALF_UP)
                                        .divide(string2Big(moneyMap.get("ZQFC").toString()), 4).multiply(BigDecimal.valueOf(100)));
                            }else {
                                moneyMap.put("CFTLZQFC",0);
                            }
                            //5.成本|职工餐人均 rjzgc = 职工餐/人数
                            if(string2Big(moneyMap.get("YGS").toString()).doubleValue()!=0) {
                                moneyMap.put("ZGCRJ", string2Big(cbMap.get("ZGC").toString()).setScale(4, BigDecimal.ROUND_HALF_UP)
                                        .divide(string2Big(moneyMap.get("YGS").toString()), 4));
                            }else {
                                moneyMap.put("ZGCRJ",0);
                            }
                            
                            //6.菜均天然气用量 cjtrq
                            if(string2Big(moneyMap.get("FCFS").toString()).doubleValue()!=0) {
                                moneyMap.put("CJTRQ", string2Big(moneyMap.get("TRQ").toString()).setScale(4, BigDecimal.ROUND_HALF_UP)
                                        .divide(string2Big(moneyMap.get("FCFS").toString()), 4));
                            }else {
                                moneyMap.put("CJTRQ",0);
                            }
                            //7.费用|人均水费 rjsf
                            if(string2Big(moneyMap.get("YGS").toString()).doubleValue()!=0) {
                                moneyMap.put("RJSF", string2Big(moneyMap.get("SF").toString()).setScale(4, BigDecimal.ROUND_HALF_UP)
                                        .divide(string2Big(moneyMap.get("YGS").toString()), 4));
                            }else {
                                moneyMap.put("RJSF",0);
                            }
                        }
                    }
                    mapResult.add(moneyMap);
                }
            }
            return mapResult;
		}catch(Exception e){
            log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
}
