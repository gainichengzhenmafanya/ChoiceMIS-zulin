package com.choice.misboh.service.reportMis;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.misboh.domain.reportMis.CustomReportMisBoh;
import com.choice.misboh.persistence.reportMis.CustomReportMisBohMapper;

@Service
public class CustomReportMisBohService {
	@Autowired
	private CustomReportMisBohMapper customReportMisBohMapper;
	private static Logger log = Logger.getLogger(CustomReportMisBohService.class);
	
	/**
	 * 描述：查询自定义报表树
	 * @return
	 * @throws CRUDException
	 * author:spt
	 * 日期：2014-5-27
	 */
	public List<Map<String, Object>> listCustomReportTree() throws CRUDException{
		try{
			return customReportMisBohMapper.listCustomReportTree();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 描述：保存自定义报表节点
	 * @param customReport
	 * @throws CRUDException
	 * author:spt
	 * 日期：2014-5-30
	 */
	public void saveCustomReport(CustomReportMisBoh customReport) throws CRUDException{
		try{
			customReport.setPk_customReport(CodeHelper.createUUID().substring(0,20));
			 customReportMisBohMapper.saveCustomReport(customReport);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询自定义报表对象
	 * @return
	 * @throws CRUDException
	 * author:spt
	 * 日期：2014-5-27
	 */
	public CustomReportMisBoh searchReport(String pk_customReport) throws CRUDException {
		try{
			return customReportMisBohMapper.searchReport(pk_customReport);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 描述：根据id  查询自定义报表
	 * @param pk_customReport
	 * @return
	 * @throws CRUDException
	 * author:spt
	 * 日期：2014-6-6
	 */
	public CustomReportMisBoh findCustomReport(String pk_customReport) throws CRUDException {
		try{
			return customReportMisBohMapper.findCustomReport(pk_customReport);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 描述：删除自定义报表节点
	 * @return
	 * @throws CRUDException
	 * author:spt
	 * 日期：2014-5-27
	 */
	public void deleteCustomReport(String pk_customReport) throws CRUDException {
		try{
			 customReportMisBohMapper.deleteCustomReport(pk_customReport);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 描述：修改自定义报表节点
	 * @param customReport
	 * @throws CRUDException
	 * author:spt
	 * 日期：2014-6-6
	 */
	public void updateCustomReport(CustomReportMisBoh customReport) throws CRUDException {
		try{
			customReportMisBohMapper.updateCustomReport(customReport);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
}
