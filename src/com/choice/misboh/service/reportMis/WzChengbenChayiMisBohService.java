package com.choice.misboh.service.reportMis;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.report.MISBOHStringConstant;
import com.choice.misboh.commonutil.report.ReportObject;
import com.choice.misboh.domain.reportMis.PublicEntity;
import com.choice.misboh.persistence.reportMis.CostItemMisBohMapper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.scm.persistence.AcctMapper;

@Service("WzChengbenChayiMisBohService")
public class WzChengbenChayiMisBohService {

	@Autowired
	private CostItemMisBohMapper costItemMisBohMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	@Autowired
	private AcctMapper acctMapper;
	
	/**
	 * 分店部门物资进出表明细
	 * @param conditions
	 * @param reportName 
	 * @return
	 */
	public ReportObject<Map<String,Object>> findCostVariance(PublicEntity conditions){
		conditions.setYears(Integer.parseInt(acctMapper.findYearrByDate(DateFormat.getDateByString(conditions.getBdat(),"yyyy-MM-dd"))));
		mapReportObject.setRows(mapPageManager.selectPage(conditions, conditions.getPager(), CostItemMisBohMapper.class.getName()+"."+MISBOHStringConstant.REPORT_QUERY_METHOD_MISCOSTVARIANCE));
		List<Map<String,Object>> foot = costItemMisBohMapper.findCalForCostVariance(conditions);
		mapReportObject.setFooter(foot);
		mapReportObject.setTotal(conditions.getPager().getCount());
		return mapReportObject;
	}

	/***
	 * choice3 物资成本差异
	 * @param condition
	 * @return
	 */
	public ReportObject<Map<String,Object>> findCostVarianceChoice3(PublicEntity conditions) {
		conditions.setYears(Integer.parseInt(acctMapper.findYearrByDate(DateFormat.getDateByString(conditions.getBdat(),"yyyy-MM-dd"))));
		conditions.setCode(CodeHelper.replaceCode(conditions.getCode()));
		mapReportObject.setRows(mapPageManager.selectPage(conditions, conditions.getPager(), CostItemMisBohMapper.class.getName()+"."+MISBOHStringConstant.REPORT_QUERY_METHOD_MISCOSTVARIANCECHOICE3));
		List<Map<String,Object>> foot = costItemMisBohMapper.findCalForCostVarianceChoice3(conditions);
		mapReportObject.setFooter(foot);
		mapReportObject.setTotal(conditions.getPager().getCount());
		return mapReportObject;
	}
	
}