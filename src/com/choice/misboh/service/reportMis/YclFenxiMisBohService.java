package com.choice.misboh.service.reportMis;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.misboh.persistence.reportMis.CostItemMisBohMapper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.ana.PtivityAna;
import com.choice.scm.persistence.AcctMapper;

@Service
public class YclFenxiMisBohService {

	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired
	private AcctMapper acctMapper;
 
	/**
	 * 应产率分析
	 * @param conditions
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String,Object>> findPtivityAna(PtivityAna ptivity,Page page){
		ptivity.setYearr(acctMapper.findYearrByDate(ptivity.getBdat()));
		List<Map<String,Object>> list = mapPageManager.selectPage(ptivity, page, CostItemMisBohMapper.class.getName()+".findPtivityAna");
		List<Map<String,Object>> rs =new ArrayList<Map<String,Object>>();
		String vcodejs="";
		//循环便利比较数据，加入同一物资，则合并显示
		for(int i=0;i<list.size();i++){
			Map<String,Object> map = list.get(i);
			rs.add(map);
			if (map.containsKey("SP_CODE")){
				if (null != map.get("SP_CODE")){
					if (vcodejs.equals(map.get("SP_CODE").toString())){
						map.put("FIRM", "");
						map.put("SP_CODE", "");
						map.put("SP_NAME", "");
						map.put("SP_DESC", "");
						map.put("UNIT", "");
						map.put("COST", "");
						map.put("BZNAME", "");
						map.put("BZCNT", "");
						map.put("REALCNT", "");
						map.put("RATE", "");
						map.put("LLCNT", "");
						map.put("CYRATE", "");
						continue;
					}
				}
			}
			vcodejs=map.get("SP_CODE")==null?"":map.get("SP_CODE").toString();
		}
		mapReportObject.setRows(rs);
		mapReportObject.setTotal(page.getCount());
		return mapReportObject;
	}	

}