package com.choice.misboh.service.reportMis;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.misboh.persistence.reportMis.SupplyAcctMisBohMapper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Grp;
import com.choice.scm.domain.GrpTyp;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.domain.Typ;
@Service
public class GysLeibieHuizongMisBohService {

	@Autowired
	private SupplyAcctMisBohMapper gysLeibieHuizongMisBohMapper;
	@Autowired
	private ReportObject<Map<String,Object>> reportObject;
	@Autowired
	private PageManager<Map<String,Object>> pageManager;
	@Autowired
	private ReportObject<Map<String,Object>> reportObject1;
	
	 
	/**
	  * 供应商类别汇总
	  * @param conditions
	  * @param page
	  * @return
	 * @throws CRUDException 
	  */
	public ReportObject<Map<String,Object>> findDeliverCategorySum(Map<String,Object> conditions,Page page) throws CRUDException{
		try{
			SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
			sa.setChktyp(CodeHelper.replaceCode(sa.getChktyp()));
			//判断供应商
			if(null != sa && null != sa.getDelivercode() && !"".equals(sa.getDelivercode())){
				sa.setDelivercode(CodeHelper.replaceCode(sa.getDelivercode()));
			}
			List<Map<String,Object>> foot = gysLeibieHuizongMisBohMapper.findCalForDeliverCateSum(conditions);
			reportObject.setRows(gysLeibieHuizongMisBohMapper.findDeliverCategorySum(conditions));
			reportObject.setFooter(foot);
			reportObject.setTotal(page.getCount());
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 查询所有物资小类
	 * @param string
	 * @return
	 */
	public List<GrpTyp> findAllType(String typ) throws CRUDException{
		try{
			return gysLeibieHuizongMisBohMapper.findAllType(typ);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}

	/***
	 * 供应商类别汇总3
	 * @param grpTypList
	 * @param condition
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String,Object>> findDeliverCategorySum3(List<GrpTyp> grpTypList,Map<String, Object> map, Page page) throws CRUDException{
		try{
			SupplyAcct supplyAcct = (SupplyAcct) map.get("supplyAcct");
			if(null!=supplyAcct && !"".equals(supplyAcct)){
				if(null!=supplyAcct.getDelivercode()&&!"".equals(supplyAcct.getDelivercode())){//供应商
					supplyAcct.setDelivercode(CodeHelper.replaceCode(supplyAcct.getDelivercode()));
				}
			}
			String amt = null != supplyAcct.getTax() && supplyAcct.getTax() == 1 ? "AMTIN*(1+TAX)" : "AMTIN";
			//要查询的物资类别
			StringBuffer sqlStr = new StringBuffer();//分项
			StringBuffer sqlStr2 = new StringBuffer();//合计
			
			StringBuffer zj = new StringBuffer();//总计
			for(GrpTyp gt : grpTypList){
				StringBuffer hj = new StringBuffer();//合计
				for(Grp g : gt.getGrpList()){
					StringBuffer xj = new StringBuffer();//小计
					for(Typ t : g.getTypList()){
						sqlStr.append(" SUM(CASE WHEN TYP='"+t.getCode()+"' THEN ROUND("+amt+",2) ELSE 0 END) AS T_"+t.getCode()+",");
						sqlStr2.append(" SUM(T_"+t.getCode()+") AS T_"+t.getCode()+",");
						xj.append("'"+t.getCode()+"',");//小计
						hj.append("'"+t.getCode()+"',");//合计
						zj.append("'"+t.getCode()+"',");//总计
					}
					sqlStr.append(" SUM(CASE WHEN TYP in ("+xj+" '') THEN ROUND("+amt+",2) ELSE 0 END) AS XJ_"+g.getCode()+",");
					sqlStr2.append(" SUM(XJ_"+g.getCode()+") AS XJ_"+g.getCode()+",");
				}
				sqlStr.append(" SUM(CASE WHEN TYP in("+hj+" '') THEN ROUND("+amt+",2) ELSE 0 END) AS HJ_"+gt.getCode()+",");
				sqlStr2.append(" SUM(HJ_"+gt.getCode()+") AS HJ_"+gt.getCode()+",");
			}
			sqlStr.append(" SUM(CASE WHEN TYP in("+zj+" '') THEN ROUND("+amt+",2) ELSE 0 END) AS ZJ");//总计
			sqlStr2.append(" SUM(ZJ) AS ZJ");
			map.put("sqlStr", sqlStr);
			map.put("sqlStr2", sqlStr2);
			
			List<Map<String,Object>> foot = gysLeibieHuizongMisBohMapper.findDeliverCategorySum3hj(map);
			reportObject1.setFooter(foot);
			if(null == page){
				reportObject1.setRows(gysLeibieHuizongMisBohMapper.findDeliverCategorySum3(map));
			}else{
				reportObject1.setRows(pageManager.selectPage(map, page, SupplyAcctMisBohMapper.class.getName()+".findDeliverCategorySum3"));
				reportObject1.setTotal(page.getCount());
			}
			return reportObject1;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}

	/***
	 * 供应商类别汇总3导出
	 * @param os
	 * @param grpTypList
	 * @param list
	 */
	public void exportDeliverCategorySum3(ServletOutputStream os,List<GrpTyp> grpTypList,List<Map<String, Object>> list) throws CRUDException{
		try{
			WritableWorkbook workBook = null;
			WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16,  
		            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
		            Colour.BLACK);
			WritableCellFormat titleStyle = new WritableCellFormat(titleFont);
			WritableFont titleFont2 = new WritableFont(WritableFont.TIMES, 12,  
		            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
		            Colour.BLACK);
			WritableCellFormat titleStyle2 = new WritableCellFormat(titleFont2);
			try {
				workBook = Workbook.createWorkbook(os);
				titleStyle.setAlignment(Alignment.CENTRE);
				titleStyle.setBorder(Border.ALL, BorderLineStyle.THIN,
					     jxl.format.Colour.BLACK);
				titleStyle2.setAlignment(Alignment.CENTRE);
				titleStyle2.setBorder(Border.ALL, BorderLineStyle.THIN,
					     jxl.format.Colour.BLACK);
				workBook = Workbook.createWorkbook(os);
				WritableSheet sheet = workBook.createSheet("供应商类别汇总报表", 0);
				//添加表头
				sheet.addCell(new Label(0, 0,"供应商类别汇总报表",titleStyle));
				sheet.mergeCells(0, 0, 21, 0);
				sheet.addCell(new Label(0, 1, "供应商编码",titleStyle2));
				sheet.mergeCells(0, 1, 0, 3);
				sheet.addCell(new Label(1, 1, "供应商",titleStyle2));
				sheet.mergeCells(1, 1, 1, 3);
				sheet.addCell(new Label(2, 1, "总计",titleStyle2));
				sheet.mergeCells(2, 1, 2, 3);
				//遍历其他表头
				int colspan = 3;//下一个大类在哪
				for(int i = 0; i < grpTypList.size(); i++){
					GrpTyp gt = grpTypList.get(i);//大类
					int colspan2 = 0;//下一个中类在哪
					for(int j = 0; j < gt.getGrpList().size(); j++){
						Grp g = gt.getGrpList().get(j);//中类
						for(int k = 0; k < g.getTypList().size(); k++){
							Typ t = g.getTypList().get(k);//小类
							//添加小类
							sheet.addCell(new Label(i+j+k+colspan+colspan2, 3, t.getDes(),titleStyle2));
							if(k == g.getTypList().size()-1){//如果是最后一个
								sheet.addCell(new Label(i+j+k+colspan+colspan2+1, 3, "小计",titleStyle2));//最后有个小计
							}
						}
						//添加中类
						sheet.addCell(new Label(i+j+colspan+colspan2, 2, g.getDes(),titleStyle2));
						sheet.mergeCells(i+j+colspan+colspan2, 2, i+j+colspan+colspan2+g.getTypList().size(), 2);
						if(j == gt.getGrpList().size()-1){//如果是最后一个
							sheet.addCell(new Label(i+j+colspan+colspan2+g.getTypList().size()+1, 2, "合计",titleStyle2));//最后有个合计
							sheet.mergeCells(i+j+colspan+colspan2+g.getTypList().size()+1, 2, i+j+colspan+colspan2+g.getTypList().size()+1, 3);
						}
						colspan2 += g.getTypList().size();
					}
					//添加大类
					sheet.addCell(new Label(colspan+i, 1, gt.getDes(),titleStyle2));
					sheet.mergeCells(colspan+i, 1, colspan +i + gt.getGrpList().size() + colspan2, 1);
					colspan += gt.getGrpList().size() + colspan2;
				}
				
	            //遍历list填充表格内容
				for(int l = 0;l<list.size();l++){
					Map<String,Object> map = list.get(l);
					sheet.addCell(new Label(0, l+4, map.get("DELIVERCODE").toString(),titleStyle2));
					sheet.addCell(new Label(1, l+4, map.get("DELIVERDES").toString(),titleStyle2));
					sheet.addCell(new Label(2, l+4, map.get("ZJ").toString(),titleStyle2));
					
					int colspana = 3;//下一个大类在哪
					for(int i = 0; i < grpTypList.size(); i++){
						GrpTyp gt = grpTypList.get(i);//大类
						int colspan2 = 0;//下一个中类在哪
						for(int j = 0; j < gt.getGrpList().size(); j++){
							Grp g = gt.getGrpList().get(j);//中类
							for(int k = 0; k < g.getTypList().size(); k++){
								Typ t = g.getTypList().get(k);//小类
								String code = t.getCode();
								String value = map.get("T_"+code).toString();
								sheet.addCell(new Label(i+j+k+colspana+colspan2, l+4, value,titleStyle2));
								if(k == g.getTypList().size()-1){//如果是最后一个
									sheet.addCell(new Label(i+j+k+colspana+colspan2+1, l+4, null == map.get("XJ_"+g.getCode())?"0.00":map.get("XJ_"+g.getCode()).toString(),titleStyle2));//最后有个小计
								}
							}
							
							if(j == gt.getGrpList().size()-1){//如果是最后一个
								sheet.addCell(new Label(i+j+colspana+colspan2+g.getTypList().size()+1, l+4, null == map.get("HJ_"+g.getCode())?"0.00":map.get("HJ_"+g.getCode()).toString(),titleStyle2));//最后有个合计
							}
							colspan2 += g.getTypList().size();
						}
						colspana += gt.getGrpList().size() + colspan2;
					}
				}
				workBook.write();
				os.flush();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}catch (Exception e) {
				e.printStackTrace();
			}finally{
				try {
					workBook.close();
					os.close();
				} catch (WriteException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
}
