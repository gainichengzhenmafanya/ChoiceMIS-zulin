package com.choice.misboh.service.reportMis;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.util.CodeHelper;
import com.choice.misboh.persistence.reportMis.CostItemMisBohMapper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;

@Service
public class HjMingxiChaxunMisBohService {

	@Autowired
	private CostItemMisBohMapper hjMingxiChaxunMisBohMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	/**
	 * 核减明细
	 * @param conditions
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String,Object>> findHejian(Map<String,Object> conditions,Page pager){
		SupplyAcct supplyAcct = (SupplyAcct) conditions.get("supplyAcct");
		if(null!=supplyAcct && !"".equals(supplyAcct)){
			if(null!=supplyAcct.getPositn()&&!"".equals(supplyAcct.getPositn())){
				supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
			}
		}
		mapReportObject.setRows(mapPageManager.selectPage(conditions, pager, CostItemMisBohMapper.class.getName()+".findHejian"));
		List<Map<String,Object>> foot = hjMingxiChaxunMisBohMapper.findCalForHejian(conditions);
		mapReportObject.setFooter(foot);
		mapReportObject.setTotal(pager.getCount());
		return mapReportObject;
	}	
}