package com.choice.misboh.service.reportMis;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.misboh.persistence.reportMis.CostItemMisBohMapper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.ana.CostProfit;

@Service
public class FdDangKouMaoliMisBohService {

	@Autowired
	private PageManager<Map<String,Object>> pageManager;
	@Autowired
	private CostItemMisBohMapper fdDangkouMaoliMisBohMapper;
	@Autowired
	private ReportObject<Map<String,Object>> reportObject;
	
	private final static int MAXNUM = 30000;
 
	/***
	 * 分店档口毛利
	 * @param deptList
	 * @param costProfit
	 * @param page
	 * @return
	 */
	public ReportObject<Map<String,Object>> findFirmDangKouProfit(List<Positn> deptList, CostProfit costProfit, String showtyp, String chengben, String acctDes, Page page){
		Map<String,Object> content = new HashMap<String,Object>();
		content.put("costProfit", costProfit);
		content.put("showtyp", showtyp);
		content.put("chengben", chengben);
		content.put("acctDes", "西贝莜面".equals(acctDes)?"xbym":"");
		costProfit.setDept(CodeHelper.replaceCode(costProfit.getDept()));
		StringBuffer sumF = new StringBuffer();
		StringBuffer caseF = new StringBuffer();
		if(null != deptList && deptList.size() != 0){
			for(Positn f : deptList){
	        	sumF.append(" IFNULL(ROUND(SUM(A_"+f.getCode()+"),2),0) AS A_"+f.getCode()+",");
	        	sumF.append(" IFNULL(ROUND(SUM(C_"+f.getCode()+"),2),0) AS C_"+f.getCode()+",");
	        	sumF.append(" IFNULL(ROUND(SUM(M_"+f.getCode()+"),2),0) AS M_"+f.getCode()+",");
		        caseF.append(" CASE WHEN AA.DEPT1='"+f.getCode()+"' THEN AA.AMT END AS A_"+f.getCode()+",");
		        caseF.append(" CASE WHEN AA.DEPT1='"+f.getCode()+"' THEN AA.COST END AS C_"+f.getCode()+",");
		        caseF.append(" CASE WHEN AA.DEPT1='"+f.getCode()+"' THEN AA.ML END AS M_"+f.getCode()+",");
			}
			costProfit.setSumF(sumF.substring(0, sumF.lastIndexOf(",")));
			costProfit.setCaseF(caseF.substring(0, caseF.lastIndexOf(",")));
			if(null == page){
				reportObject.setRows(fdDangkouMaoliMisBohMapper.findFdDangKouMaoli(content));
			}else{
				reportObject.setRows(pageManager.selectPage(content,page,CostItemMisBohMapper.class.getName()+".findFdDangKouMaoli"));
				reportObject.setTotal(page.getCount());
			}
			List<Map<String,Object>> foot = fdDangkouMaoliMisBohMapper.findFdDangKouMaoliSum(content);
			reportObject.setFooter(foot);
			return reportObject;
		}else{
			return null;
		}
	}
	
	/**
	 * excel
	 * 
	 */
	public boolean exportExcel(OutputStream os, CostProfit costProfit,String showtyp,String chengben,String acctDes,List<Positn> deptList) throws CRUDException {   
		WritableWorkbook workBook = null;
		reportObject = findFirmDangKouProfit(deptList,costProfit,showtyp,chengben,acctDes,null);
		List<Map<String,Object>> list = reportObject.getRows();
		try {
			int totalCount=0;
			if (list!=null) {
				totalCount = list.size();
			}
			int sheetNum = 1;
			if(totalCount>65535){
				sheetNum = totalCount/MAXNUM+1;
			}
			workBook = Workbook.createWorkbook(os);
			for(int i=0;i<sheetNum;i++){
				WritableSheet sheet = workBook.createSheet("Sheet"+(i+1), i);
				sheet.addCell(new Label(0, 0,"分店档口毛利"));
	            sheet.addCell(new Label(0, 1, "周次/月"));   
	            sheet.addCell(new Label(1, 1, "起止日期")); 
				for (int k=0;k<deptList.size();k++) {
					sheet.addCell(new Label(k*3+2, 1, deptList.get(k).getDes()+"销售"));
					sheet.addCell(new Label(k*3+1+2, 1, deptList.get(k).getDes()+"成本"));
					sheet.addCell(new Label(k*3+2+2, 1, deptList.get(k).getDes()+"毛利"));
				}
				
	            //遍历list填充表格内容
				int index = 0;
	            for(int j=i*MAXNUM;j<(i+1)*MAXNUM;j++) {
	            	if(j == totalCount){
	            		break;
	            	}
	            	index=j>=MAXNUM?(j-i*MAXNUM):j;
	            	sheet.addCell(new Label(0, index+2, list.get(j).get("WEEKNO").toString()));
	            	sheet.addCell(new Label(1, index+2, null == list.get(j).get("DES") ? "":list.get(j).get("DES").toString()));
	            	for (int k=0;k<deptList.size();k++) {
	            		String amt = "";
	            		String cnt = "";
	            		String ml = "";
	            		if (list.get(j).get("A_"+deptList.get(k).getCode())!=null) {
	            			amt = list.get(j).get("A_"+deptList.get(k).getCode()).toString();
	            			cnt = list.get(j).get("C_"+deptList.get(k).getCode()).toString();
	            			ml = list.get(j).get("M_"+deptList.get(k).getCode()).toString();
	            		}
						sheet.addCell(new Label(k*3+2, index+2, amt));
						sheet.addCell(new Label(k*3+1+2, index+2, cnt));
						sheet.addCell(new Label(k*3+2+2, index+2, ml));
					}
		    	}
			}
			workBook.write();
			os.flush();
            
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
		
	}
}