package com.choice.misboh.service.reportMis;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.misboh.commonutil.report.ReportObject;
import com.choice.misboh.domain.reportMis.PublicEntity;
import com.choice.misboh.persistence.reportMis.CostItemMisBohMapper;

@Service("MlChaxunMisBohService")
public class MlChaxunMisBohService {

	@Autowired
	private CostItemMisBohMapper costItemMisBohMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	/**
	 * 菜品销售利润查询
	 * @param conditions
	 * @return
	 */
	public ReportObject<Map<String,Object>> findCalForGrossProfit(PublicEntity condition){
		mapReportObject.setRows(costItemMisBohMapper.findCalForGrossProfit(condition));
		List<Map<String,Object>> foot = costItemMisBohMapper.findCalForGrossProfitSum(condition);
		mapReportObject.setFooter(foot);
		return mapReportObject;
	}
	
}