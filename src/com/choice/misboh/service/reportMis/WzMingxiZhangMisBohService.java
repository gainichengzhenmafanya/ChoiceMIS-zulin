package com.choice.misboh.service.reportMis;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.misboh.persistence.reportMis.SupplyAcctMisBohMapper;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.AcctMapper;
import com.choice.scm.persistence.MainInfoMapper;
@Service
public class WzMingxiZhangMisBohService {

	@Autowired
	private SupplyAcctMisBohMapper wzMingxiZhangMisBohMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	@Autowired
	private AcctMapper acctMapper;

	/**
	 * 物资明细账表
	 * @param conditions
	 * @param pager
	 * @return
	 * @throws CRUDException
	 * @author ZGL
	 */
	public ReportObject<Map<String,Object>> findSupplyDetailsInfo(Map<String,Object> conditions,Page pager) throws CRUDException{
		try {
			SupplyAcct supplyAcct = (SupplyAcct) conditions.get("supplyAcct");
			int withdat=0;//选择日期标志：0-为选，1-已选时间有效
//			int month = new Date().getMonth()+1;
//			int yearr = new Date().getYear()+1900;
			if(conditions.get("withdat")!=null && !conditions.get("withdat").equals(""))
				withdat=Integer.parseInt(conditions.get("withdat").toString());
			int yearr = 1900;
			if(withdat == 0){//没选日期默认当前年
				yearr = Integer.parseInt(mainInfoMapper.findYearrList().get(0));
			}else{//选了日期要查开始日期所在会计年
				yearr = Integer.parseInt(acctMapper.findYearrByDate(supplyAcct.getBdat()));
			}
			String dataColumns = "";//查询语句
			String constraints = " WHERE A.YEARR = '"+yearr+"'";
			if(supplyAcct.getAcct()!=null && !supplyAcct.getAcct().equals(""))
				constraints += " AND A.ACCT = '"+supplyAcct.getAcct()+"'";
			if(supplyAcct.getPositn()!=null && !supplyAcct.getPositn().equals(""))
				constraints += " AND A.Positn IN ("+supplyAcct.getPositn()+")";
			if(supplyAcct.getSp_code()!=null && !supplyAcct.getSp_code().equals(""))
				constraints += " AND A.SP_CODE = '"+supplyAcct.getSp_code()+"'";
			//查询条件
			if(withdat==1){
				String bdat = new SimpleDateFormat("yyyy-MM-dd").format(supplyAcct.getBdat());
				String edat = new SimpleDateFormat("yyyy-MM-dd").format(supplyAcct.getEdat());
				String positnsupply = "";//期初
				String totalSupply = "";//总计
				positnsupply = " UNION SELECT ROUND(A.INC0,2) AS INC,ROUND(A.INA0,2) AS INA,ROUND(A.OUTC0,2) AS OUTC,ROUND(A.OUTA0,2) AS OUTA FROM POSITNSUPPLY A "+ constraints;
				totalSupply = " UNION SELECT A.DAT,A.CHKNO,A.VOUNO,A.ID,A.DES,A.INC,A.PRICEIN,A.INA,A.OUTC,A.PRICEOUT,A.OUTA,B.CNTBAL,B.PRICEBAL,B.AMTBAL "+
						"FROM (SELECT '' AS DAT,'' AS CHKNO,'' AS VOUNO,'' AS ID,'累计' AS DES, ROUND(IFNULL(SUM(A.CNTIN), 0),2) AS INC,"+
						"ROUND((CASE WHEN SUM(A.CNTIN) != 0 THEN IFNULL(SUM(A.AMTIN), 0) / IFNULL(SUM(A.CNTIN), 0) ELSE 0 END),2) AS PRICEIN,"+
						"ROUND(IFNULL(SUM(A.AMTIN), 0),2) AS INA,ROUND(IFNULL(SUM(A.CNTOUT), 0),2) AS OUTC,"+
						"ROUND((CASE WHEN SUM(A.CNTOUT) != 0 THEN IFNULL(SUM(A.AMTOUT), 0) / IFNULL(SUM(A.CNTOUT), 0) ELSE 0 END),2) AS PRICEOUT,"+
						"ROUND(IFNULL(SUM(A.AMTOUT), 0),2) AS OUTA FROM SUPPLYACCT A " + constraints +
					" AND A.DAT>=STR_TO_DATE('"+bdat+"','%Y-%m-%d') AND A.DAT<=STR_TO_DATE('"+edat+"','%Y-%m-%d')) A,(SELECT ROUND((A.CNTBALA+B.CNTBALB),2) AS CNTBAL,ROUND((A.AMTBALA+B.AMTBALB),2) AS AMTBAL,"+
						  "ROUND((CASE WHEN A.CNTBALA+B.CNTBALB!=0 THEN (A.AMTBALA+B.AMTBALB)/(A.CNTBALA+B.CNTBALB) ELSE 0 END),2) AS PRICEBAL "+
						"FROM (SELECT ROUND(IFNULL(SUM(A.CNTIN - A.CNTOUT), 0),2) AS CNTBALA,ROUND(IFNULL(SUM(A.AMTIN - A.AMTOUT), 0),2) AS AMTBALA"+
							" FROM SUPPLYACCT A "+constraints+" AND A.DAT>=(SELECT BDAT1 FROM MAIN WHERE YEARR = '"+yearr+"') AND A.DAT<=STR_TO_DATE('"+edat+"','%Y-%m-%d')) a,"+
							"(SELECT ROUND(A.INC0-A.OUTC0,2) AS CNTBALB,ROUND(A.INA0-A.OUTA0,2) AS AMTBALB"+
							" FROM POSITNSUPPLY A "+constraints +") B) B";
				dataColumns = "SELECT DAT,CHKNO,VOUNO,ID,DES,ROUND(INC,2) AS INC,ROUND((CASE WHEN INC!=0 THEN INA/INC ELSE 0 END),2) AS PRICEIN,ROUND(INA,2) AS INA,"+
				      " ROUND(OUTC,2) AS OUTC,ROUND((CASE WHEN OUTC!=0 THEN OUTA/OUTC ELSE 0 END),2) AS PRICEOUT,ROUND(OUTA,2) AS OUTA,"+
				      " CNTBAL,"+
				      " ROUND((CASE WHEN (CNTBAL) != 0 THEN (AMTBAL) /(CNTBAL) ELSE 0 END),2) AS PRICEBAL,"+
				      " AMTBAL "+
				"FROM(SELECT ' ' AS DAT,'' AS CHKNO,'' AS VOUNO,'' AS ID,'期初' AS DES,0 AS INC,0 AS INA,0 AS OUTC,0 AS OUTA,ROUND(SUM(INC-OUTC),2) AS CNTBAL,ROUND(SUM(INA-OUTA),2) AS AMTBAL"+
				      " FROM (SELECT IFNULL(SUM(A.CNTIN),0) AS INC,IFNULL(SUM(A.AMTIN),0) AS INA, IFNULL(SUM(A.CNTOUT),0) AS OUTC, IFNULL(SUM(A.AMTOUT),0) AS OUTA FROM SUPPLYACCT A "+constraints+
				        " AND A.DAT>=(SELECT BDAT1 FROM MAIN WHERE YEARR = '"+yearr+"') AND A.DAT<STR_TO_DATE('"+bdat+"','%Y-%m-%d') GROUP BY A.CNTBAL,A.AMTBAL "+positnsupply+")T "+
				    " UNION SELECT DATE_FORMAT(A.DAT,'%Y-%m-%d') AS DAT,CAST(A.CHKNO AS CHAR) AS CHKNO,A.VOUNO,CAST(A.ID AS CHAR) AS ID,A.DES,A.CNTIN AS INC,A.AMTIN AS INA,A.CNTOUT AS OUTC,A.AMTOUT AS OUTA,(A.CNTIN-A.CNTOUT) AS CNTBAL,(A.AMTIN-A.AMTOUT) AS AMTBAL "+
				    " FROM SUPPLYACCT A "+constraints+
				      "  AND A.DAT>=STR_TO_DATE('"+bdat+"','%Y-%m-%d') AND A.DAT<=STR_TO_DATE('"+edat+"','%Y-%m-%d') ORDER BY DAT,ID )TT" + totalSupply;
			}else{
				//总计查询
				String totalSql = "";
				totalSql = " SELECT ' ' AS DAT,'' AS CHKNO,'' AS VOUNO,'' AS ID,'13' AS NUM,'本年合计' AS DES, "+
						"ROUND(SUM(INC0 + INC1 + INC2 + INC3 + INC4 + INC5 + INC6 + INC7 + INC8 + INC9 + INC10 + INC11 + INC12),2) AS INC,"+
						"ROUND(CASE WHEN SUM(INC0 + INC1 + INC2 + INC3 + INC4 + INC5 + INC6 + INC7 + INC8 + INC9 + INC10 + INC11 + INC12) != 0 THEN "+
						"SUM(INA0 + INA1 + INA2 + INA3 + INA4 + INA5 + INA6 + INA7 + INA8 + INA9 + INA10 + INA11 + INA12)/SUM(INC0 + INC1 + INC2 + INC3 + INC4 + INC5 + INC6 + INC7 + INC8 + INC9 + INC10 + INC11 + INC12) ELSE 0 END,2) AS PRICEIN,"+
						"ROUND(SUM(INA0 + INA1 + INA2 + INA3 + INA4 + INA5 + INA6 + INA7 + INA8 + INA9 + INA10 + INA11 + INA12),2) AS INA,"+
						"ROUND(SUM(OUTC0 + OUTC1 + OUTC2 + OUTC3 + OUTC4 + OUTC5 + OUTC6 + OUTC7 + OUTC8 + OUTC9 + OUTC10 + OUTC11 + OUTC12),2) AS OUTC,"+
						"ROUND(CASE WHEN SUM(OUTC0 + OUTC1 + OUTC2 + OUTC3 + OUTC4 + OUTC5 + OUTC6 + OUTC7 + OUTC8 + OUTC9 + OUTC10 + OUTC11 + OUTC12) != 0 THEN "+
						"SUM(OUTA0 + OUTA1 + OUTA2 + OUTA3 + OUTA4 + OUTA5 + OUTA6 + OUTA7 + OUTA8 + OUTA9 + OUTA10 + OUTA11 + OUTA12)/SUM(OUTC0 + OUTC1 + OUTC2 + OUTC3 + OUTC4 + OUTC5 + OUTC6 + OUTC7 + OUTC8 + OUTC9 + OUTC10 + OUTC11 + OUTC12) ELSE 0 END,2) AS PRICEOUT,"+
						"ROUND(SUM(OUTA0 + OUTA1 + OUTA2 + OUTA3 + OUTA4 + OUTA5 + OUTA6 + OUTA7 + OUTA8 + OUTA9 + OUTA10 + OUTA11 + OUTA12),2) AS OUTA,"+
						"ROUND(SUM(INC0 + INC1 + INC2 + INC3 + INC4 + INC5 + INC6 + INC7 + INC8 + INC9 + INC10 + INC11 + INC12)-SUM(OUTC0 + OUTC1 + OUTC2 + OUTC3 + OUTC4 + OUTC5 + OUTC6 + OUTC7 + OUTC8 + OUTC9 + OUTC10 + OUTC11 + OUTC12),2) AS CNTBAL,"+
						"ROUND(CASE WHEN (SUM(INC0 + INC1 + INC2 + INC3 + INC4 + INC5 + INC6 + INC7 + INC8 + INC9 + INC10 + INC11 + INC12)-SUM(OUTC0 + OUTC1 + OUTC2 + OUTC3 + OUTC4 + OUTC5 + OUTC6 + OUTC7 + OUTC8 + OUTC9 + OUTC10 + OUTC11 + OUTC12)) != 0 THEN "+
						"(SUM(INA0 + INA1 + INA2 + INA3 + INA4 + INA5 + INA6 + INA7 + INA8 + INA9 + INA10 + INA11 + INA12)-SUM(OUTA0 + OUTA1 + OUTA2 + OUTA3 + OUTA4 + OUTA5 + OUTA6 + OUTA7 + OUTA8 + OUTA9 + OUTA10 + OUTA11 + OUTA12))/(SUM(INC0 + INC1 + INC2 + INC3 + INC4 + INC5 + INC6 + INC7 + INC8 + INC9 + INC10 + INC11 + INC12)-SUM(OUTC0 + OUTC1 + OUTC2 + OUTC3 + OUTC4 + OUTC5 + OUTC6 + OUTC7 + OUTC8 + OUTC9 + OUTC10 + OUTC11 + OUTC12)) ELSE 0 END,2) AS PRICEBAL,"+
						"ROUND(SUM(INA0 + INA1 + INA2 + INA3 + INA4 + INA5 + INA6 + INA7 + INA8 + INA9 + INA10 + INA11 + INA12)-SUM(OUTA0 + OUTA1 + OUTA2 + OUTA3 + OUTA4 + OUTA5 + OUTA6 + OUTA7 + OUTA8 + OUTA9 + OUTA10 + OUTA11 + OUTA12),2) AS AMTBAL"+
						"  FROM POSITNSUPPLY A "+ constraints;
				//循环生成查询语句
				dataColumns += "SELECT ' ' AS DAT,' ' AS CHKNO,'' AS VOUNO,'' AS ID,'00' AS NUM,'上年转结' AS DES, ROUND(SUM(A.INC0),2) AS INC," +
						"ROUND((CASE WHEN SUM(A.INC0)!=0 THEN SUM(A.INA0)/SUM(A.INC0) ELSE 0 END),2) AS PRICEIN,ROUND(SUM(A.INA0),2) AS INA ,ROUND(SUM(A.OUTC0),2) AS OUTC," +
						"ROUND((CASE WHEN SUM(A.OUTC0)!=0 THEN SUM(A.OUTA0)/SUM(A.OUTC0) ELSE 0 END),2) AS PRICEOUT,ROUND(SUM(A.OUTA0),2) AS OUTA,ROUND((SUM(A.INC0)-SUM(A.OUTC0)),2) AS CNTBAL," +
						"ROUND((CASE WHEN (SUM(A.INC0)-SUM(A.OUTC0))!=0 THEN (SUM(A.INA0)-SUM(A.OUTA0))/(SUM(A.INC0)-SUM(A.OUTC0)) ELSE 0 END),2) AS PRICEBAL,ROUND((SUM(A.INA0)-SUM(A.OUTA0)),2) AS AMTBAL FROM  POSITNSUPPLY A";
				dataColumns += constraints + " UNION ";
				for(int i=1;i<=12;i++){
					String mon = "",mon1="";
					if(i<10){ 
						mon = "0"+i; 
					}else {
						mon = ""+i;
					}
					if(i-1<10){
						mon1 = "0"+(i-1);
					}else{
						mon1 = ""+(i-1);
					}
					dataColumns += "SELECT DATE_FORMAT(A.DAT,'%Y-%m-%d') AS DAT,CAST(A.CHKNO AS CHAR) AS CHKNO,A.VOUNO,CAST(A.ID AS CHAR) AS ID,'"+mon1+"' AS NUM,A.DELIVERDES AS DES,ROUND(A.CNTIN,2) AS INC,ROUND(A.PRICEIN,2) AS PRICEIN," +
							"ROUND(A.AMTIN,2) AS INA,ROUND(A.CNTOUT,2) AS OUTC,ROUND(A.PRICEOUT,2) AS PRICEOUT,ROUND(A.AMTOUT,2) AS OUTA,"+
							"ROUND(SUM(A.CNTIN-A.CNTOUT),2) CNTBAL,"+
							"ROUND((CASE WHEN SUM(A.CNTIN-A.CNTOUT) != 0 THEN "+
							"SUM(A.AMTIN-A.AMTOUT) /SUM(A.CNTIN-A.CNTOUT) "+
							"ELSE 0 END ),2) AS PRICEBAL,ROUND(SUM(A.AMTIN-A.AMTOUT),2) AMTBAL "+
							"FROM SUPPLYACCT A " + constraints + " AND A.DAT>=(SELECT BDAT"+i+" FROM MAIN WHERE YEARR = '"+yearr+"') AND (SELECT EDAT"+i+" FROM MAIN WHERE YEARR = '"+yearr+"')>=A.DAT GROUP BY A.DAT, A.CHKNO,A.VOUNO,A.DELIVERDES,A.CNTIN,A.AMTIN,A.CNTOUT,A.AMTOUT,A.PRICEIN,A.PRICEOUT,A.ID UNION ";
					dataColumns += "SELECT ' ' AS DAT,'' AS CHKNO,'' AS VOUNO,'' AS ID,'"+mon+"' AS NUM,'"+i+"月合计' AS DES, ROUND(SUM(A.CNTIN),2) AS INC," +
							"ROUND((CASE WHEN SUM(A.CNTIN)!=0 THEN SUM(A.AMTIN)/SUM(A.CNTIN) ELSE 0 END),2) AS PRICEIN,ROUND(SUM(A.AMTIN),2) AS INA ," +
							"ROUND(SUM(A.CNTOUT),2) AS OUTC,ROUND((CASE WHEN SUM(A.CNTOUT)!=0 THEN SUM(A.AMTOUT)/SUM(A.CNTOUT) ELSE 0 END),2) AS PRICEOUT," +
							"ROUND(SUM(A.AMTOUT),2) AS OUTA,0 AS CNTBAL," +
							"0 AS PRICEBAL,0 AS AMTBAL " +
							"FROM  SUPPLYACCT A" + constraints  + " AND A.DAT>=(SELECT BDAT"+i+" FROM MAIN WHERE YEARR = '"+yearr+"') AND (SELECT EDAT"+i+" FROM MAIN WHERE YEARR = '"+yearr+"')>=A.DAT UNION ";
				}
				dataColumns +=  totalSql + " ORDER BY NUM,DAT,ID ";
			}
			conditions.put("dataColumns", dataColumns);
//			List<Map<String,Object>> foot = wzMingxiZhangMisMapper.findCalForSupplyDetailsInfo(conditions);
//			List<Map<String,Object>> listInfo = mapPageManager.selectPage(conditions, pager, WzMingxiZhangMisMapper.class.getName()+".findSupplyDetailsInfo");
			List<Map<String, Object>> list = wzMingxiZhangMisBohMapper.findSupplyDetailsInfo(conditions);
//			if(withdat!=1){
				double cntbal = 0.00;
				double amtbal = 0.00;
				for(Map<String,Object> map:list){
					if(map == list.get(list.size()-1)){break;}
					map.put("CNTBAL", Math.round(Double.parseDouble(map.get("CNTBAL")==null?"0":map.get("CNTBAL").toString())*100.00)/100.00+Math.round(cntbal*100.00)/100.00);
					map.put("AMTBAL", Math.round(Double.parseDouble(map.get("AMTBAL")==null?"0":map.get("AMTBAL").toString())*100.00)/100.00+Math.round(amtbal*100.00)/100.00);
					map.put("PRICEBAL", Double.parseDouble(map.get("CNTBAL")==null?"0.00":map.get("CNTBAL").toString()) == 0.00 ? 0.00 : Double.parseDouble(map.get("AMTBAL")==null?"0.00":map.get("AMTBAL").toString())/Double.parseDouble(map.get("CNTBAL")==null?"0.00":map.get("CNTBAL").toString()));
					cntbal = Double.parseDouble(map.get("CNTBAL")==null?"0.00":map.get("CNTBAL").toString());
					amtbal = Double.parseDouble(map.get("AMTBAL")==null?"0.00":map.get("AMTBAL").toString());
				}
//			}
			mapReportObject.setRows(list);
//			mapReportObject.setFooter(foot);
//			mapReportObject.setRows(listInfo);
//			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}

}
