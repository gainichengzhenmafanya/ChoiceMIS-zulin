package com.choice.misboh.service.costReduction;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.domain.costReduction.CostItem;
import com.choice.misboh.domain.costReduction.CostItemSpcode;
import com.choice.misboh.persistence.costReduction.CostReductionMapper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.MainInfo;
import com.choice.scm.domain.Positn;
import com.choice.scm.persistence.AcctMapper;
import com.choice.scm.persistence.MainInfoMapper;

/**
 * 描述：成本核减
 * @author 马振
 * 创建时间：2015-4-14 上午9:55:52
 */
@Service
public class CostReductionService {

	private static Logger log = Logger.getLogger(CostReductionService.class);
	
	@Autowired
	private CostReductionMapper costReductionMapper;
	
	@Autowired
	private MainInfoMapper mainInfoMapper;
	
	@Autowired
	private AcctMapper acctMapper;
	
	@Autowired
	private PageManager<CostItemSpcode> pageManager;
	
	/**
	 * 描述：成本核减显示
	 * @author 马振
	 * 创建时间：2015-4-14 下午1:50:16
	 * @param firmCostItemSpcode
	 * @param page
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<CostItemSpcode> listCostReduction(CostItem costItem, Page page) throws CRUDException{
		try{
			if(null == page)
				return costReductionMapper.listCostReduction(costItem);
			return pageManager.selectPage(costItem,page,CostReductionMapper.class.getName()+".listCostReduction");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：反核减
	 * @author 马振
	 * 创建时间：2015-1-1 下午5:32:06
	 * @param startDate
	 * @param endDate
	 * @param session
	 * @throws CRUDException
	 */
	public int antiReduction(CostItem costItem) throws CRUDException{
		int sta = -3;
		try{
			String yearr = acctMapper.findYearrByDate(costItem.getBdat());
			MainInfo mainInfo = mainInfoMapper.findMainInfo(Integer.parseInt(yearr));
			//核减存储接收按日期来核减
			Date bdat = costItem.getBdat();
			Date edat = costItem.getEdat();
			long day = (edat.getTime() - bdat.getTime())/(3600 * 24 * 1000);
			for (long i = 0; i <= day; i++) {
				CostItem cost = new CostItem();
				cost.setAcct(costItem.getAcct());
				cost.setYyear(yearr);
				cost.setFirm(costItem.getFirm());
				Date date = new Date(bdat.getTime() + i*(3600 * 24 * 1000));
				cost.setDat(DateFormat.getStringByDate(date, "yyyy-MM-dd"));
				String month = getOnlyAccountMonth2(mainInfo,date)+"";
				cost.setPmonth(month);//应该是当前日期所在会计月
				costReductionMapper.excutecostcut_re(cost);
			}
			sta = 2;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
		return sta;
	}
	
	private int getOnlyAccountMonth2(MainInfo mainInfo, Date maded) throws CRUDException{
		int month=0;
		try{
			for(int i = 1; i <= 12; i++){
				String bdat = "getBdat"+i;
				String edat = "getEdat"+i;
				Method getBdat = MainInfo.class.getDeclaredMethod(bdat);
				Date bdate = (Date)getBdat.invoke(mainInfo);
				Method getEdat = MainInfo.class.getDeclaredMethod(edat);
				Date edate = (Date)getEdat.invoke(mainInfo);
				if(maded.getTime() >= bdate.getTime() && maded.getTime() <= edate.getTime()){
					month = i;
					break;
				}
			}
			if(month==0){
				throw new CRUDException("没有此会计日！请先停止使用，联系辰森技术人员。 ");
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
		return month;
	}
	
	/**
	 * 描述：获取当前登录门店的编码
	 * @author 马振
	 * 创建时间：2015-4-14 下午1:42:07
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public String getVcode(HttpSession session) throws CRUDException{
		try{
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			return thePositn.getCode();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 成本核减 - -   审核
	 * @author 文清泉
	 * @param 2015年4月28日 下午2:22:22
	 * @param startDate 开始时间
	 * @param endDate 结束时间
	 * @param session 
	 * @return 
	 * @throws CRUDException 
	 */
	public int subtract(CostItem costItem, Map<String,String> costTime) throws CRUDException {
		try {
			costTime.put("time", 10+"");
			costTime.put("des", "正在查询菜品销售数据...");
			List<Map<String,Object>> listMap = costReductionMapper.findDatSubtract(costItem);
			if(listMap == null || listMap.size() <=0){
				return 0;
			}
			List<CostItem> list = new ArrayList<CostItem>();
			costTime.put("time", 15+"");
			costTime.put("des", "正在查询门店成本卡...");
			int count = costReductionMapper.getCostDtl(costItem.getFirm());
			if(count == 0){
				return -1;
			}
			costTime.put("time", 20+"");
			costTime.put("des", "正在执行反核减...");
			antiReduction(costItem);//先反核减
			//查询供应链档口放到map
			List<Positn> pfList = costReductionMapper.findPositnfirmByPositn(costItem);
			HashMap<String,String> pfMap = new HashMap<String,String>();
			for(Positn pf : pfList){
				pfMap.put(pf.getOldcode(), pf.getCode());
			}
			String acct = costItem.getAcct();
			String firm = costItem.getFirm();
			costTime.put("time", 30+"");
			costTime.put("des", "正在保存菜品汇总数据...");
			for (Map<String, Object> map : listMap) {
				CostItem cost = new CostItem();
				cost.setAcct(acct);
				cost.setAmt(ValueUtil.getDoubleValue(map.get("AMT")));
				cost.setChkcost("N");//默认未做成本卡，存储过程会改回来
				cost.setTx("A");//现在核减只查的是正常菜
				cost.setCnt(ValueUtil.getDoubleValue(map.get("CNT")));
				cost.setDat(ValueUtil.getStringValue(map.get("DWORKDATE")));
				cost.setDept(ValueUtil.getStringValue(map.get("DEPT")));
				cost.setDeptdes(ValueUtil.getStringValue(map.get("DEPTDES")));
				String dept1 = pfMap.get(cost.getDept());
				cost.setDept1(dept1);
				cost.setFirm(firm);
				cost.setDes(ValueUtil.getStringValue(map.get("VPNAME")));
				cost.setFirmdes(costItem.getFirmdes());
				cost.setItcode(ValueUtil.getStringValue(map.get("VPCODE")));
				cost.setItem(ValueUtil.getStringValue(map.get("VPCODE")));
				if(ValueUtil.getDoubleValue(map.get("CNT")) >0){
					cost.setItprice(ValueUtil.getDoubleValue(map.get("AMT"))/ValueUtil.getDoubleValue(map.get("CNT")));
				}else{
					cost.setItprice(0d);
				}
				cost.setYnunit("N");//默认不是双单位 如果第二单位数量有值 则改为Y
				cost.setNum(ValueUtil.getDoubleValue(map.get("NUM")));
				if(cost.getNum() > 0)
					cost.setYnunit("Y");
				cost.setUnit(ValueUtil.getStringValue(map.get("VUNITS")));
				cost.setTyp(ValueUtil.getStringValue(map.get("VCCLEASSNAME")));
				cost.setPositn(null != dept1 && !"".equals(dept1) ? dept1 : firm);
				cost.setDisc(ValueUtil.getDoubleValue(map.get("DISC")));//折扣
				cost.setSvc(ValueUtil.getDoubleValue(map.get("SVC")));//服务费
				cost.setTax(ValueUtil.getDoubleValue(map.get("TAX")));//税金
				cost.setRefund(ValueUtil.getDoubleValue(map.get("REFUND")));//免项
				cost.setPickdisc(ValueUtil.getDoubleValue(map.get("PICKDISC")));//套餐优惠
				cost.setTotal(ValueUtil.getDoubleValue(map.get("TOTAL")));//实收额
				list.add(cost);
			}
			costReductionMapper.saveCostItem(list);
			String yearr = acctMapper.findYearrByDate(costItem.getBdat());
			MainInfo mainInfo = mainInfoMapper.findMainInfo(Integer.parseInt(yearr));
			//核减存储接收按日期来核减
			Date bdat = costItem.getBdat();
			Date edat = costItem.getEdat();
			long day = (edat.getTime() - bdat.getTime())/(3600 * 24 * 1000);
			for (long i = 0; i <= day; i++) {
				CostItem cost = new CostItem();
				cost.setAcct(acct);
				cost.setYyear(yearr);
				cost.setFirm(firm);
				Date date = new Date(bdat.getTime() + i*(3600 * 24 * 1000));
				String dat = DateFormat.getStringByDate(date, "yyyy-MM-dd");
				cost.setDat(dat);
				String month = getOnlyAccountMonth2(mainInfo,date)+"";
				cost.setPmonth(month);//应该是当前日期所在会计月
				long time = day == 0 ? 100 : 40 + i * (60 / day);
				costTime.put("time", time+"");
				costTime.put("des", "正在核减"+dat+"的营业数据...");
				costReductionMapper.excutecostcut(cost);
			}
			costTime.put("time", 100+"");
			costTime.put("des", "核减完成。");
			return 1;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/***
	 * 核减之前判断是否已核减
	 * @param costItemSpcode
	 * @return
	 */
	public String checkCostReduction(CostItem costItem) {
		List<CostItem> csList = costReductionMapper.listCostReductionGroupByDate(costItem);
		if(csList.size() == 0){
			return "0";
		}else{
			StringBuffer sb = new StringBuffer();
			for(CostItem cs : csList){
				sb.append(cs.getDat()+",");
			}
			return sb.toString().substring(0, sb.toString().length()-1);
		}
	}
	
	/**
	 * 根据门店编码查询门店类型
	 * add by wangkai 2016-1-27 
	 * @return
	 */
	public String findVfoodSignByVcode(Positn positn)  throws CRUDException {
		try{
			return costReductionMapper.findVfoodSignByVcode(positn);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	public List<Map<String, Object>> findDatSubtract(CostItem costItem) {
		return costReductionMapper.findDatSubtract(costItem);
	}
	
	public List<Map<String, Object>> findDatSubtract_cn(CostItem costItem) {
		return costReductionMapper.findDatSubtract_cn(costItem);
	}
	
	public List<Map<String, Object>> findDatSubtract_tele(CostItem costItem) {
		return costReductionMapper.findDatSubtract_tele(costItem);
	}
	
}
