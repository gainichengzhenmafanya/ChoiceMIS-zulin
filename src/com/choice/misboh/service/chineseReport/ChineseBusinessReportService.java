package com.choice.misboh.service.chineseReport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.domain.common.CommonMethod;
import com.choice.misboh.domain.reportMis.PublicEntity;
import com.choice.misboh.domain.reportMis.ReportObject;
import com.choice.misboh.persistence.chineseReport.ChineseBusinessReportMapper;
import com.choice.misboh.persistence.common.CommonMISBOHMapper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.ReadProperties;

/**
 * 描述：中餐报表-营业分析
 * @author 马振
 * 创建时间：2015-6-25 下午1:08:29
 */
@Service
public class ChineseBusinessReportService {

	@Autowired
	private ChineseBusinessReportMapper chineseBusinessReportMapper;
	
	@Autowired
	private ReportObject<Map<String,Object>> reportObject;
	
	@Autowired
	private ReportObject<Map<String,String>> reportObjectString;
	
	@Autowired
	private PageManager<Map<String,Object>> pageManager;
	
	@Autowired
	private CommonMISBOHMapper commonMapper;
	
	/**
	 * 描述：门店营业显示分析
	 * @author 马振
	 * 创建时间：2015-7-8 下午2:39:31
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findGroupBusinessShow(PublicEntity condition) throws CRUDException{
		try{
			List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("DES", "实收明细-大类统计");
			list.add(map);
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			List<CommonMethod> sftList = commonMapper.findAllShiftsft(new CommonMethod());
			int days = sftList.size();
			if(days == 0){
				days = 1;
			}
			condition.setDays("0".equals(condition.getSft())?days:1);
			reportObject.setRows(chineseBusinessReportMapper.findGroupBusinessShow(condition));
			reportObject.getRows().addAll(chineseBusinessReportMapper.findGroupPaymentInfo(condition));
            reportObject.getRows().addAll(list);
            reportObject.getRows().addAll(chineseBusinessReportMapper.findGroupGrptypInfo(condition));
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 门店收入分析
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findGroupIncome(PublicEntity condition) throws CRUDException{
		try{
			ReadProperties rp = new ReadProperties();
			String dataSource = rp.getStrByParam("dataSource");
			
			List<Map<String,Object>> cols = new ArrayList<Map<String,Object>>();
			Map<String,Object> paramMap = new HashMap<String,Object>();
			StringBuffer subCol = new StringBuffer();//行转列
			StringBuffer colResult = new StringBuffer();//最外层输出结果列
			cols = commonMapper.findPayment();
			for(Map<String,Object> map : cols){//获取分店支付方式typ
				String curTyp = map.get("VCODE").toString();
				if("sqlserver".equals(dataSource)){
					subCol.append(",SUM(CASE WHEN A.VOPERATE='"+ curTyp).append("' THEN CASE WHEN A.VYPAYFLAG='N' THEN ISNULL(A.NMONEY,0) ELSE 0 END ELSE 0 END) AS PAY" + curTyp);
				} else if ("mysql".equals(dataSource)) {
					subCol.append(",SUM(CASE WHEN A.VOPERATE='"+ curTyp).append("' THEN CASE WHEN A.VYPAYFLAG='N' THEN IFNULL(A.NMONEY,0) ELSE 0 END ELSE 0 END) AS PAY" + curTyp);
				} else {
					subCol.append(",SUM(CASE WHEN A.VOPERATE='"+ curTyp).append("' THEN CASE WHEN A.VYPAYFLAG='N' THEN NVL(A.NMONEY,0) ELSE 0 END ELSE 0 END) AS PAY" + curTyp);
				}
				
				if ("mysql".equals(dataSource)) {
					colResult.append(",CAST(CAST(B.PAY" + curTyp +" AS DECIMAL(28,2))  AS CHAR(30)) AS PAY" + curTyp);
				} else {
					colResult.append(",CAST(CAST(B.PAY" + curTyp +" AS DECIMAL(28,2))  AS VARCHAR(30)) AS PAY" + curTyp);
				}
			}
			paramMap.put("subCol", subCol);
			paramMap.put("colResult", colResult);
			paramMap.put("bdat", condition.getBdat());
			paramMap.put("edat", condition.getEdat());
			paramMap.put("pk_store", FormatDouble.StringCodeReplace(condition.getPk_store()));
			paramMap.put("sft", condition.getSft());
			paramMap.put("sumMod", condition.getSumMod());
			paramMap.put("sort", condition.getSort());
			paramMap.put("order", condition.getOrder());
			
			Page page = condition.getPager();
			switch (condition.getSumMod()){
			case PublicEntity.DETAILBYDAY:
				reportObject.setRows(pageManager.selectPage(paramMap, page, ChineseBusinessReportMapper.class.getName()+".findGroupIncomeByDay"));
				reportObject.setFooter(chineseBusinessReportMapper.findCalForGroupIncome(paramMap));
				break;
			default:
				reportObject.setRows(pageManager.selectPage(paramMap, page, ChineseBusinessReportMapper.class.getName()+".findGroupIncomeByDate"));
				reportObject.setFooter(chineseBusinessReportMapper.findCalForGroupIncome(paramMap));
			}
			reportObject.setTotal(page.getCount());
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：门店销售分析
	 * @author 马振
	 * 创建时间：2015-6-25 下午1:14:58
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findGroupSalesSum(PublicEntity condition) throws CRUDException{
		try{
			Page page = condition.getPager();
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			
			switch(condition.getSumMod()){
			case PublicEntity.BYCATEGORY:
				reportObject.setRows(pageManager.selectPage(condition, page, ChineseBusinessReportMapper.class.getName()+".findGroupSalesByCate"));
				reportObject.setFooter(chineseBusinessReportMapper.findCalForGroupSales(condition));
				break;
			case PublicEntity.BYFOOD:
				reportObject.setRows(pageManager.selectPage(condition, page, ChineseBusinessReportMapper.class.getName()+".findGroupSalesByFood"));
				reportObject.setFooter(chineseBusinessReportMapper.findCalForGroupSales(condition));
				break;
			default:
				reportObject.setRows(pageManager.selectPage(condition, page, ChineseBusinessReportMapper.class.getName()+".findGroupSalesSumByDate"));
				reportObject.setFooter(chineseBusinessReportMapper.findCalForGroupSales(condition));
				break;
			}
			reportObject.setTotal(page.getCount());
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询数据  营业日报区域汇总表
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,String>> findJTRFX(PublicEntity condition)throws CRUDException{
		try{
			Map<String,Object> map = new HashMap<String,Object>();
			List<Map<String, Object>> pays = new ArrayList<Map<String,Object>>();
			//支付方式
			List<String> paymentList = new ArrayList<String>();
			pays = commonMapper.findPayment();
			for(Map<String, Object> pay : pays){
				paymentList.add(pay.get("VCODE").toString());
			}
			//部门
			List<String> deptList = new ArrayList<String>();
			List<CommonMethod> listdept = commonMapper.findAllGroupDept();
			for(int i=0;i<listdept.size();i++){
				deptList.add(listdept.get(i).getVcode());
			}
			//餐次
			List<CommonMethod> sft = commonMapper.findAllShiftsft(new CommonMethod());
			StringBuffer sqlstr = new StringBuffer();
			for(int i=0;i<sft.size();i++){
				sqlstr.append(" WHEN "+sft.get(i).getVcode()+" THEN '"+sft.get(i).getVname()+"' ");
			}
			
			//日期间隔天数
			int size = DateJudge.getDaysBetween(condition.getBdat(), condition.getEdat()) + 1;
			map.put("bdat", condition.getBdat());
			map.put("edat", condition.getEdat());
			map.put("len", size);
			map.put("pk_store", FormatDouble.StringCodeReplace(condition.getPk_store()));
			map.put("sumMod", condition.getSumMod());
			map.put("paymentList", paymentList);
			map.put("deptList", deptList);
			map.put("sqlstr", sqlstr.toString());
			map.put("sfts", sft.size());

            List<Map<String, String>> result = new ArrayList<Map<String,String>>();
			List<Map<String, String>> mapList = chineseBusinessReportMapper.findJTRFX(map);
			List<Map<String, String>> mapSum = chineseBusinessReportMapper.findJTRFXSUM(map);
            Map<String,Map<String,String>> sumCollection = new HashMap<String,Map<String,String>>();
            for(Map<String,String> curSum : mapSum){
                sumCollection.put(curSum.get("SFT"),curSum);
                curSum.put("SFT","");
            }
            int len = 0;
			if((len = mapList.size())>0){
                String prevSft = "";
                String curSft = "";
                for(int i = 0 ; i < len ; i ++ ){
                    Map<String,String> curRow = null;
                    if(prevSft.equals(curSft = (curRow = mapList.get(i)).get("SFT"))){
                        curRow.put("SFT","");
                    }else{
                        if(null != sumCollection.get(prevSft) && i != 0){
                            result.add(sumCollection.get(prevSft));
                        }
                        prevSft = curSft;
                    }
                    result.add(curRow);
                }
			}
			
			reportObjectString.setRows(result);
			reportObjectString.setFooter(null);
			return reportObjectString;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：门店菜品出错率分析
	 * @author 马振
	 * 创建时间：2015-7-13 下午5:15:44
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findCaiPinChuCuoLv(PublicEntity condition) throws CRUDException {
		try{
			Page page = condition.getPager();
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			
			reportObject.setRows(pageManager.selectPage(condition, page, ChineseBusinessReportMapper.class.getName()+".findCaiPinChuCuoLv"));
			reportObject.setFooter(chineseBusinessReportMapper.findCalForCaiPinChuCuoLv(condition));
			reportObject.setTotal(page.getCount());
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
}