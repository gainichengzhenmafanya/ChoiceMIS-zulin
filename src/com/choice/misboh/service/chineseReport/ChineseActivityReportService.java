package com.choice.misboh.service.chineseReport;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.commonutil.MisUtil;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.domain.reportMis.PublicEntity;
import com.choice.misboh.domain.reportMis.ReportObject;
import com.choice.misboh.persistence.chineseReport.ChineseActivityReportMapper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * 描述：中餐报表-活动分析
 * @author 马振
 * 创建时间：2015-6-25 下午1:08:29
 */
@Service
public class ChineseActivityReportService {

	@Autowired
	private ChineseActivityReportMapper chineseActivityReportMapper;
	
	@Autowired
	private ReportObject<Map<String,Object>> reportObject;
	
	@Autowired
	private PageManager<Map<String,Object>> pageManager;
	
	/**
	 * 描述：集团活动分析-按日
	 * @author 马振
	 * 创建时间：2015-7-14 下午1:51:06
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Object queryJiTuanHuoDongFenXi(PublicEntity condition)  throws CRUDException {
		try{
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			Page pager = condition.getPager();
			pager.setPageSize(Integer.MAX_VALUE);
			String bdat=condition.getBdat();
			if(bdat==null || "".equals(bdat)){
				bdat=String.valueOf(Calendar.MONTH+1);
			}
			Date bdate=sdf.parse(bdat);
			//拼接日期
			condition.setBdat(sdf.format(bdate));
			if(ValueUtil.IsNotEmpty(condition.getPk_store())){
				//拼接门店id，进行过滤
				condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			}
			
			//结果集
			List<Map<String,Object>> relustList=new ArrayList<Map<String,Object>>();
			//获取所有优免活动数据
			List<Map<String, Object>> actmList =  MisUtil.formatNumForListResult(chineseActivityReportMapper.queryJiTuanHuoDongFenXi1(condition),"NMONEY",2);;
			//获取所有合计信息
			List<Map<String, Object>> hejie = MisUtil.formatNumForListResult(chineseActivityReportMapper.queryJiTuanHuoDongFenXi2(condition),"NMONEY",2);
			//获取分析数据
			List<Map<String, Object>> fenxi = MisUtil.formatNumForListResult(chineseActivityReportMapper.queryJiTuanHuoDongFenXi3(condition),"NMONEY",2);
			//ordr 产品数据
			Calendar c = Calendar.getInstance();
			c.setTime(bdate);
			
			Map<String,Object> codeMap = new HashMap<String, Object>();
			String vcodejs="";//临时变量 用来存储每次遍历取出的活动编码
			double totalAmt = 0;
			double totalcnt = 0;
			double totalhdjs = 0;
			//遍历活动类型数据
			for (int i = 0; i < actmList.size(); i++) {
				codeMap = new HashMap<String, Object>();
				if(i == 0){
					codeMap.put("DX", "");
				}
				//获取结果集的数据
				Map<String,Object> map1 = actmList.get(i);
				if(map1.containsKey("ACTCODE")){
					if(null != map1.get("ACTCODE")){
						if(vcodejs.equals(map1.get("ACTCODE").toString())){
							continue;
						}
					}
				}
				vcodejs = map1.get("ACTCODE")==null ? "" :map1.get("ACTCODE").toString();
				//编码和名称,动态数据
				codeMap.put("DX", map1.get("TYPNAME"));
				codeMap.put("WORKDATE", map1.get("ACTNAME"));
				//用来存储拼接之后的每一天的数据
				List<Map<String,Object>> list2 = new ArrayList<Map<String,Object>>();
				//再一次遍历活动结果集
				for (int j = 0; j < actmList.size(); j++) {
					//取出每一天结果级
					Map<String,Object> map3 = actmList.get(j);//取出数据
					//判断取出的记录中是否用活动编码
					if(map3.containsKey("ACTCODE")){
						//判断取出的活动编码是否为空
						if(null != map3.get("ACTCODE")){
							//如果第二次取出的活动编码和第一次取出的活动编码相同，拼接相应的数据
							if(vcodejs.equals(map3.get("ACTCODE").toString())){
								//map2建对应日，值对应具体数据,
								Map<String,Object> map2 = new HashMap<String, Object>();
								map2.put("DWORKDATE", map3.get("DWORKDATE"));
								map2.put("CNT", map3.get("CNT"));
								map2.put("YMJE", map3.get("YMJE"));
								map2.put("YMZB", map3.get("YMZB"));
								map2.put("HDJS", map3.get("HDJS"));
								list2.add(map2);	
							}
						}
					}
				}
				
				//若list2在每个月的某一天，没有对应的数据，则需要补空。保证有足够天数的数据
				for (int k = 1; k < c.getActualMaximum(Calendar.DAY_OF_MONTH)+1; k++) {
					if(list2.size()>0){
						boolean flag = true;
						for (int h = 0; h < list2.size(); h++) {
							
							Map<String,Object> map3=list2.get(h); 
							
							String date = map3.get("DWORKDATE")==null ? "" : map3.get("DWORKDATE").toString();
							
							int day = DateFormat.getDay(sdf.parse(date));
							
							if(day==k){
								codeMap.put("YMJE"+k, map3.get("YMJE"));
								codeMap.put("CNT"+k, map3.get("CNT"));
								codeMap.put("YMZB"+k, map3.get("YMZB"));
								codeMap.put("HDJS"+k, map3.get("HDJS"));
								
								totalAmt += Double.parseDouble(map3.get("YMJE").toString());
								totalcnt += Double.parseDouble(map3.get("CNT").toString());
								totalhdjs +=Double.parseDouble(map3.get("HDJS").toString());
								list2.remove(h);//删除数据，不参与数据拼接
								flag=false;
								break;
							}
						}
						if(flag){
							codeMap.put("YMJE"+k, "0.00");
							codeMap.put("CNT"+k, "0.00");
							codeMap.put("YMZB"+k, "0.00");
							codeMap.put("HDJS"+k, "0.00");
						}
					}else{//list2数据删除完了，当时天数内的数据还未补全，这里重新补
						codeMap.put("YMJE"+k, "0.00");
						codeMap.put("CNT"+k, "0.00");
						codeMap.put("YMZB"+k, "0.00");
						codeMap.put("HDJS"+k, "0.00");
					}
				}
				codeMap.put("YMJEHJ",MisUtil.formatDoubleLength(FormatDouble.toString(totalAmt),2));
				codeMap.put("CNTHJ",MisUtil.formatDoubleLength(FormatDouble.toString(totalcnt),2));
				codeMap.put("HDJSHJ",MisUtil.formatDoubleLength(FormatDouble.toString(totalhdjs),2));
				relustList.add(codeMap);
				totalAmt=0;
				totalcnt=0;
				totalhdjs=0;
			}
				
			
			//合计信息
			String vcodelb="";
			for (int i = 0; i < hejie.size(); i++) {
				codeMap=new HashMap<String, Object>();
				if(i==0){
//					codeMap.put("DX", "活动合计");
				}
				Map<String,Object> map1 = hejie.get(i);
				if(map1.containsKey("ACTCODE")){
					if(null != map1.get("ACTCODE")){
						if(vcodelb.equals(map1.get("ACTCODE").toString())){
							continue;
						}
					}
				}
				vcodelb = map1.get("ACTCODE")==null ? "" : map1.get("ACTCODE").toString();
				//编码和名称,动态数据
//					codeMap.put("WORKDATE", map1.get("VOPERATE"));
				codeMap.put("WORKDATE", map1.get("ACTNAME"));
				//取出数据
				List<Map<String,Object>> list2=new ArrayList<Map<String,Object>>();
				for (int j = 0; j < hejie.size(); j++) {
					Map<String,Object> map3 = hejie.get(j);
					if(map3.containsKey("ACTCODE")){
						if(null != map3.get("ACTCODE")){
							if(vcodelb.equals(map3.get("ACTCODE").toString())){
								//map2建对应日，值对应具体数据
								Map<String,Object> map2=new HashMap<String, Object>();
								map2.put("DWORKDATE", map3.get("DWORKDATE"));
								map2.put("CNT", map3.get("CNT"));
								map2.put("YMJE", map3.get("YMJE"));
								map2.put("HDJS", map3.get("HDJS"));
								list2.add(map2);	
							}
						}
					}
				}
				//若list2在每个月的某一天，没有对应的数据，则需要补空。保证有足够天数的数据
				for (int k = 1; k < c.getActualMaximum(Calendar.DAY_OF_MONTH)+1; k++) {
					if(list2.size()>0){
						boolean flag=true;
						for (int h = 0; h < list2.size(); h++) {
							Map<String,Object> map3=list2.get(h); 
							String date=map3.get("DWORKDATE")==null ? "" : map3.get("DWORKDATE").toString();
							int day=DateFormat.getDay(sdf.parse(date));
							if(day==k){
								codeMap.put("CNT"+k, map3.get("CNT"));
								codeMap.put("YMJE"+k, map3.get("YMJE"));
								codeMap.put("HDJS"+k, map3.get("HDJS"));
								totalcnt += Double.parseDouble(map3.get("CNT").toString());
								totalAmt += Double.parseDouble(map3.get("YMJE").toString());
								totalhdjs += Double.parseDouble(map3.get("HDJS").toString());
								list2.remove(h);//删除数据，不参与数据拼接
								flag=false;
								break;
							}
						}
						if(flag){
							codeMap.put("YMJE"+k, "0.00");
							codeMap.put("CNT"+k, "0.00");
							codeMap.put("HDJS"+k, "0.00");
						}
					}else{//list2数据删除完了，当时天数内的数据还未补全，这里重新补
						codeMap.put("YMJE"+k, "0.00");
						codeMap.put("CNT"+k, "0.00");
						codeMap.put("HDJS"+k, "0.00");
					}
				}
				codeMap.put("YMJEHJ",MisUtil.formatDoubleLength(FormatDouble.toString(totalAmt),2));
				codeMap.put("CNTHJ",MisUtil.formatDoubleLength(FormatDouble.toString(totalcnt),2));
				codeMap.put("HDJSHJ",MisUtil.formatDoubleLength(FormatDouble.toString(totalhdjs),2));
				relustList.add(codeMap);
				totalAmt=0;
				totalcnt=0;
			}
			
			//分析信息
			String vcodefx="";
			for (int i = 0; i < fenxi.size(); i++) {
				codeMap=new HashMap<String, Object>();
				if(i==0){
//					codeMap.put("DX", "分析");
				}
				Map<String,Object> map1=fenxi.get(i);
				if(map1.containsKey("ACTCODE")){
					if(null != map1.get("ACTCODE")){
						if(vcodefx.equals(map1.get("ACTCODE").toString())){
							continue;
						}
					}
				}
				vcodefx=map1.get("ACTCODE")==null ? "" :map1.get("ACTCODE").toString();
				//编码和名称,动态数据
//					codeMap.put("WORKDATE", map1.get("VOPERATE"));
				codeMap.put("DX", map1.get("TYPNAME"));
				codeMap.put("WORKDATE", map1.get("ACTNAME"));
				//取出数据
				List<Map<String,Object>> list2=new ArrayList<Map<String,Object>>();
				for (int j = 0; j < fenxi.size(); j++) {
					Map<String,Object> map3=fenxi.get(j);
					if(map3.containsKey("ACTCODE")){
						if(null != map3.get("ACTCODE")){
							if(vcodefx.equals(map3.get("ACTCODE").toString())){
								//map2建对应日，值对应具体数据
								Map<String,Object> map2=new HashMap<String, Object>();
								map2.put("DWORKDATE", map3.get("DWORKDATE"));
								map2.put("CNT", map3.get("CNT"));
								list2.add(map2);	
							}
						}
					}
				}
				//若list2在每个月的某一天，没有对应的数据，则需要补空。保证有足够天数的数据
				for (int k = 1; k < c.getActualMaximum(Calendar.DAY_OF_MONTH)+1; k++) {
					if(list2.size()>0){
						boolean flag=true;
						for (int h = 0; h < list2.size(); h++) {
							Map<String,Object> map3=list2.get(h); 
							String date=map3.get("DWORKDATE")==null ? "" : map3.get("DWORKDATE").toString();
							int day=DateFormat.getDay(sdf.parse(date));
							if(day==k){
								codeMap.put("CNT"+k, map3.get("CNT"));
								totalAmt += Double.parseDouble(map3.get("CNT").toString());
								list2.remove(h);//删除数据，不参与数据拼接
								flag=false;
								break;
							}
						}
						if(flag){
							codeMap.put("CNT"+k, "0.00");
						}
					}else{//list2数据删除完了，当时天数内的数据还未补全，这里重新补
						codeMap.put("CNT"+k, "0.00");
					}
				}
				codeMap.put("YMJEHJ",MisUtil.formatDoubleLength(FormatDouble.toString(totalAmt),2));
				relustList.add(codeMap);
				totalAmt=0;
			}
			
			
//			return "";
			reportObject.setRows(relustList);
//			reportObject.setFooter(relustSumList);
			
//			List<Map<String,Object>> footList=new ArrayList<Map<String,Object>>();
//			footList.add(footMap);
//			reportObject.setFooter(footList);
//			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
	
	
	/**
	 * 描述：集团活动分析-按月
	 * @author 马振
	 * 创建时间：2015-7-14 下午1:56:06
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Object queryJiTuanHuoDongFenXiByMonth(PublicEntity condition)  throws CRUDException {
		try{
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM");
			Page pager = condition.getPager();
			pager.setPageSize(Integer.MAX_VALUE);
			String bdat=condition.getEdat();
			if(bdat==null || "".equals(bdat)){
				bdat=String.valueOf(Calendar.YEAR+1);
			}
			Date bdate=sdf.parse(bdat+"-01");
			//拼接日期
			condition.setBdat(sdf.format(bdate));
			if(ValueUtil.IsNotEmpty(condition.getPk_store())){
				//拼接门店id，进行过滤
				condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			}
			
			//结果集
			List<Map<String,Object>> relustList=new ArrayList<Map<String,Object>>();
			//获取所有优免活动数据
			List<Map<String, Object>> actmList =  MisUtil.formatNumForListResult(chineseActivityReportMapper.queryJiTuanHuoDongFenXiByMonth1(condition),"NMONEY",2);;
			//获取所有合计信息
			List<Map<String, Object>> hejie = MisUtil.formatNumForListResult(chineseActivityReportMapper.queryJiTuanHuoDongFenXiByMonth2(condition),"NMONEY",2);
			//获取分析数据
			List<Map<String, Object>> fenxi = MisUtil.formatNumForListResult(chineseActivityReportMapper.queryJiTuanHuoDongFenXiByMonth3(condition),"NMONEY",2);
			//ordr 产品数据
//			Calendar c = Calendar.getInstance();
//			c.setTime(bdate);
			
			Map<String,Object> codeMap = new HashMap<String, Object>();
			String vcodejs="";//临时变量 用来存储每次遍历取出的活动编码
			double totalAmt = 0;
			double totalcnt = 0;
			double totalhdjs = 0;
			//遍历活动类型数据
			for (int i = 0; i < actmList.size(); i++) {
				codeMap = new HashMap<String, Object>();
				if(i == 0){
					codeMap.put("DX", "");
				}
				//获取结果集的数据
				Map<String,Object> map1 = actmList.get(i);
				if(map1.containsKey("ACTCODE")){
					if(null != map1.get("ACTCODE")){
						if(vcodejs.equals(map1.get("ACTCODE").toString())){
							continue;
						}
					}
				}
				vcodejs = map1.get("ACTCODE")==null ? "" :map1.get("ACTCODE").toString();
				//编码和名称,动态数据
				codeMap.put("DX", map1.get("TYPNAME"));
				codeMap.put("WORKDATE", map1.get("ACTNAME"));
				//用来存储拼接之后的每一天的数据
				List<Map<String,Object>> list2 = new ArrayList<Map<String,Object>>();
				//再一次遍历活动结果集
				for (int j = 0; j < actmList.size(); j++) {
					//取出每一天结果级
					Map<String,Object> map3 = actmList.get(j);//取出数据
					//判断取出的记录中是否用活动编码
					if(map3.containsKey("ACTCODE")){
						//判断取出的活动编码是否为空
						if(null != map3.get("ACTCODE")){
							//如果第二次取出的活动编码和第一次取出的活动编码相同，拼接相应的数据
							if(vcodejs.equals(map3.get("ACTCODE").toString())){
								//map2建对应日，值对应具体数据,
								Map<String,Object> map2 = new HashMap<String, Object>();
								map2.put("DWORKDATE", map3.get("DWORKDATE"));
								map2.put("CNT", map3.get("CNT"));
								map2.put("YMJE", map3.get("YMJE"));
								map2.put("YMZB", map3.get("YMZB"));
								map2.put("HDJS", map3.get("HDJS"));
								list2.add(map2);	
							}
						}
					}
				}
				
				//若list2在每个月的某一天，没有对应的数据，则需要补空。保证有足够天数的数据
				for (int k = 1; k < 13; k++) {
					if(list2.size()>0){
						boolean flag = true;
						for (int h = 0; h < list2.size(); h++) {
							
							Map<String,Object> map3=list2.get(h); 
							
							String date = map3.get("DWORKDATE")==null ? "" : map3.get("DWORKDATE").toString();
							
							int day = DateFormat.getMonth(sdf.parse(date));
							
							if(day==k){
								codeMap.put("YMJE"+k, map3.get("YMJE"));
								codeMap.put("CNT"+k, map3.get("CNT"));
								codeMap.put("YMZB"+k, map3.get("YMZB"));
								codeMap.put("HDJS"+k, map3.get("HDJS"));
								totalAmt += Double.parseDouble(map3.get("YMJE").toString());
								totalcnt += Double.parseDouble(map3.get("CNT").toString());
								totalhdjs += Double.parseDouble(map3.get("HDJS").toString());
								list2.remove(h);//删除数据，不参与数据拼接
								flag=false;
								break;
							}
						}
						if(flag){
							codeMap.put("YMJE"+k, "0.00");
							codeMap.put("CNT"+k, "0.00");
							codeMap.put("YMZB"+k, "0.00");
							codeMap.put("HDJS"+k, "0.00");
						}
					}else{//list2数据删除完了，当时天数内的数据还未补全，这里重新补
						codeMap.put("YMJE"+k, "0.00");
						codeMap.put("CNT"+k, "0.00");
						codeMap.put("YMZB"+k, "0.00");
						codeMap.put("HDJS"+k, "0.00");
					}
				}
				codeMap.put("YMJEHJ",MisUtil.formatDoubleLength(FormatDouble.toString(totalAmt),2));
				codeMap.put("CNTHJ",MisUtil.formatDoubleLength(FormatDouble.toString(totalcnt),2));
				relustList.add(codeMap);
				totalAmt=0;
				totalcnt=0;
			}
				
			
			//合计信息
			String vcodelb="";
			for (int i = 0; i < hejie.size(); i++) {
				codeMap=new HashMap<String, Object>();
				if(i==0){
//					codeMap.put("DX", "活动合计");
				}
				Map<String,Object> map1 = hejie.get(i);
				if(map1.containsKey("ACTCODE")){
					if(null != map1.get("ACTCODE")){
						if(vcodelb.equals(map1.get("ACTCODE").toString())){
							continue;
						}
					}
				}
				vcodelb = map1.get("ACTCODE")==null ? "" : map1.get("ACTCODE").toString();
				//编码和名称,动态数据
//					codeMap.put("WORKDATE", map1.get("VOPERATE"));
				codeMap.put("WORKDATE", map1.get("ACTNAME"));
				//取出数据
				List<Map<String,Object>> list2=new ArrayList<Map<String,Object>>();
				for (int j = 0; j < hejie.size(); j++) {
					Map<String,Object> map3 = hejie.get(j);
					if(map3.containsKey("ACTCODE")){
						if(null != map3.get("ACTCODE")){
							if(vcodelb.equals(map3.get("ACTCODE").toString())){
								//map2建对应日，值对应具体数据
								Map<String,Object> map2=new HashMap<String, Object>();
								map2.put("DWORKDATE", map3.get("DWORKDATE"));
								map2.put("CNT", map3.get("CNT"));
								map2.put("YMJE", map3.get("YMJE"));
								map2.put("HDJS", map3.get("HDJS"));
								list2.add(map2);	
							}
						}
					}
				}
				//若list2在每个月的某一天，没有对应的数据，则需要补空。保证有足够天数的数据
				for (int k = 1; k < 13; k++) {
					if(list2.size()>0){
						boolean flag=true;
						for (int h = 0; h < list2.size(); h++) {
							Map<String,Object> map3=list2.get(h); 
							String date=map3.get("DWORKDATE")==null ? "" : map3.get("DWORKDATE").toString();
							int day=DateFormat.getMonth(sdf.parse(date));
							if(day==k){
								codeMap.put("CNT"+k, map3.get("CNT"));
								codeMap.put("YMJE"+k, map3.get("YMJE"));
								codeMap.put("HDJS"+k, map3.get("HDJS"));
								totalcnt += Double.parseDouble(map3.get("CNT").toString());
								totalAmt += Double.parseDouble(map3.get("YMJE").toString());
								totalhdjs += Double.parseDouble(map3.get("HDJS").toString());
								list2.remove(h);//删除数据，不参与数据拼接
								flag=false;
								break;
							}
						}
						if(flag){
							codeMap.put("YMJE"+k, "0.00");
							codeMap.put("CNT"+k, "0.00");
							codeMap.put("HDJS"+k, "0.00");
						}
					}else{//list2数据删除完了，当时天数内的数据还未补全，这里重新补
						codeMap.put("YMJE"+k, "0.00");
						codeMap.put("CNT"+k, "0.00");
						codeMap.put("HDJS"+k, "0.00");
					}
				}
				codeMap.put("YMJEHJ",MisUtil.formatDoubleLength(FormatDouble.toString(totalAmt),2));
				codeMap.put("CNTHJ",MisUtil.formatDoubleLength(FormatDouble.toString(totalcnt),2));
				codeMap.put("HDJSHJ",MisUtil.formatDoubleLength(FormatDouble.toString(totalcnt),2));
				relustList.add(codeMap);
				totalAmt=0;
				totalcnt=0;
				totalhdjs=0;
			}
			
			//分析信息
			String vcodefx="";
			for (int i = 0; i < fenxi.size(); i++) {
				codeMap=new HashMap<String, Object>();
				if(i==0){
//					codeMap.put("DX", "分析");
				}
				Map<String,Object> map1=fenxi.get(i);
				if(map1.containsKey("ACTCODE")){
					if(null != map1.get("ACTCODE")){
						if(vcodefx.equals(map1.get("ACTCODE").toString())){
							continue;
						}
					}
				}
				vcodefx=map1.get("ACTCODE")==null ? "" :map1.get("ACTCODE").toString();
				//编码和名称,动态数据
//					codeMap.put("WORKDATE", map1.get("VOPERATE"));
				codeMap.put("DX", map1.get("TYPNAME"));
				codeMap.put("WORKDATE", map1.get("ACTNAME"));
				//取出数据
				List<Map<String,Object>> list2=new ArrayList<Map<String,Object>>();
				for (int j = 0; j < fenxi.size(); j++) {
					Map<String,Object> map3=fenxi.get(j);
					if(map3.containsKey("ACTCODE")){
						if(null != map3.get("ACTCODE")){
							if(vcodefx.equals(map3.get("ACTCODE").toString())){
								//map2建对应日，值对应具体数据
								Map<String,Object> map2=new HashMap<String, Object>();
								map2.put("DWORKDATE", map3.get("DWORKDATE"));
								map2.put("CNT", map3.get("CNT"));
								list2.add(map2);	
							}
						}
					}
				}
				//若list2在每个月的某一天，没有对应的数据，则需要补空。保证有足够天数的数据
				for (int k = 1; k < 13; k++) {
					if(list2.size()>0){
						boolean flag=true;
						for (int h = 0; h < list2.size(); h++) {
							Map<String,Object> map3=list2.get(h); 
							String date=map3.get("DWORKDATE")==null ? "" : map3.get("DWORKDATE").toString();
							int day=DateFormat.getMonth(sdf.parse(date));
							if(day==k){
								codeMap.put("CNT"+k, map3.get("CNT"));
								totalAmt += Double.parseDouble(map3.get("CNT").toString());
								list2.remove(h);//删除数据，不参与数据拼接
								flag=false;
								break;
							}
						}
						if(flag){
							codeMap.put("CNT"+k, "0.00");
						}
					}else{//list2数据删除完了，当时天数内的数据还未补全，这里重新补
						codeMap.put("CNT"+k, "0.00");
					}
				}
				codeMap.put("YMJEHJ",MisUtil.formatDoubleLength(FormatDouble.toString(totalAmt),2));
				codeMap.put("HDJSHJ",MisUtil.formatDoubleLength(FormatDouble.toString(totalhdjs),2));
				relustList.add(codeMap);
				totalAmt=0;
			}
			
			
//			return "";
			reportObject.setRows(relustList);
//			reportObject.setFooter(relustSumList);
			
//			List<Map<String,Object>> footList=new ArrayList<Map<String,Object>>();
//			footList.add(footMap);
//			reportObject.setFooter(footList);
//			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 台位活动明细报表
	 * 
	 * @param condition
	 * @return
	 * @throws CRUDException
	 * @author YGB
	 */
	public ReportObject<Map<String, Object>> findActmByTbl(PublicEntity condition) throws CRUDException {
		try {
			Page pager = condition.getPager();
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			reportObject.setRows(pageManager.selectPage(condition, pager,ChineseActivityReportMapper.class.getName()+ ".findActmByTbl"));
			reportObject.setFooter(chineseActivityReportMapper.findActmByTblSUM(condition));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：菜品活动明细报表
	 * @author 马振
	 * 创建时间：2015-7-14 上午8:53:44
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String, Object>> findActmByItem(PublicEntity condition) throws CRUDException {
		try {
			Page pager = condition.getPager();
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			reportObject.setRows(pageManager.selectPage(condition, pager,ChineseActivityReportMapper.class.getName()+ ".findActmByItem"));
			reportObject.setFooter(chineseActivityReportMapper.findActmByItemSUM(condition));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
}