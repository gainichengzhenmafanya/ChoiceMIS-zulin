package com.choice.misboh.service.chineseReport;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.commonutil.MisUtil;
import com.choice.misboh.domain.reportMis.PublicEntity;
import com.choice.misboh.domain.reportMis.ReportObject;
import com.choice.misboh.persistence.chineseReport.ChineseSalesReportMapper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * 描述：中餐报表-销售分析
 * @author 马振
 * 创建时间：2015-6-25 下午1:08:29
 */
@Service
public class ChineseSalesReportService {

	@Autowired
	private ChineseSalesReportMapper chineseSalesReportMapper;
	
	@Autowired
	private ReportObject<Map<String,Object>> reportObject;
	
	@Autowired
	private PageManager<Map<String,Object>> pageManager;
	
	/**
	 * 描述：项目销售统计
	 * @author 马振
	 * 创建时间：2015-6-27 下午2:37:37
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findProjectSalesStatistics(PublicEntity condition) throws CRUDException{
		try{
			Page page = condition.getPager();
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			
			reportObject.setRows(pageManager.selectPage(condition, page, ChineseSalesReportMapper.class.getName()+".findProjectSalesStatistics"));
			reportObject.setFooter(chineseSalesReportMapper.findProjectSalesStatisticsSum(condition));
			reportObject.setTotal(page.getCount());
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：菜品销售排行榜
	 * @author 马振
	 * 创建时间：2015-7-14 下午2:22:32
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String, Object>> queryFoodList(PublicEntity condition) throws CRUDException {
		try {
			SimpleDateFormat sft = new SimpleDateFormat("yyyy-MM-dd");
			Map<String, String> map1 = DateJudge.getFirstday_Lastday_Month(sft.parse(condition.getEdat()));
			String edat = condition.getEdat();
			String bdat = condition.getEdat().substring(0, 7)+"-01";
			String sbdat = map1.get("first");
			String sedat = map1.get("last");
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("pk_store", FormatDouble.StringCodeReplace(condition.getPk_store()));
			map.put("pubgrp", condition.getPubgrp());
			map.put("pubgrptyp", condition.getPubgrptyp());
			map.put("idept", condition.getIdept());
			map.put("bdat", bdat);
			map.put("edat", edat);
			map.put("sbdat", sbdat);
			map.put("sedat", sedat);
			map.put("interval", condition.getInterval());
			map.put("sort", condition.getSort());
			map.put("order", condition.getOrder());
			
			reportObject.setRows(chineseSalesReportMapper.findFoodList(map));
			reportObject.setFooter(null);
			return reportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：客单-时段销售分析报表
	 * @author 马振
	 * 创建时间：2015-6-27 下午2:59:39
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> querySaleTcByTime(PublicEntity condition)throws CRUDException{
		try{
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("bdat", condition.getBdat());
			map.put("edat", condition.getEdat());
			map.put("pk_store", condition.getPk_store());
			map.put("dateList", DateJudge.getTimeList(condition.getInterval()));
			
			reportObject.setRows(chineseSalesReportMapper.findSaleTcByTime(map));
			reportObject.setFooter(chineseSalesReportMapper.findSaleTcByTimeSUM(map));
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询数据    类别-时段销售分析报表
	 * @author 马振
	 * 创建时间：2015-6-27 下午2:59:47
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> querySaleTypeByTime(PublicEntity condition)throws CRUDException{
		try{
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("bdat", condition.getBdat());
			map.put("edat", condition.getEdat());
			map.put("pk_store", condition.getPk_store());
			map.put("dateList", DateJudge.getTimeList(condition.getInterval()));
			map.put("detailMod", condition.getDetailMod());
			map.put("pubgrp", condition.getPubgrp());
			
			reportObject.setRows(chineseSalesReportMapper.findSaleTypeByTime(map));
			reportObject.setFooter(chineseSalesReportMapper.findSaleTypeByTimeSUM(map));
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：套餐明细统计查询
	 * @author 马振
	 * 创建时间：2015-6-27 下午3:12:00
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> queryMxtjb(PublicEntity condition) throws CRUDException {
		try {
			Page pager = condition.getPager();
			pager.setNowPage(condition.getPage());
			pager.setPageSize(condition.getRows());
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			
			//查询套餐明细统计数据
			List<Map<String,Object>> list = chineseSalesReportMapper.queryMxtjb(condition);
			
			//若查出的数据不为0，则采用分页的语句
			if (list.size() != 0) {
				list = pageManager.selectPage(condition, pager, ChineseSalesReportMapper.class.getName()+".queryMxtjb");
			}
			
			reportObject.setRows(list);
			reportObject.setFooter(chineseSalesReportMapper.queryCalForMxtjb(condition));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：套餐销售统计查询
	 * @author 马振
	 * 创建时间：2015-6-27 下午3:11:50
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> queryXstjb(PublicEntity condition) throws CRUDException {
		try {
			Page pager = condition.getPager();
			pager.setNowPage(condition.getPage());
			pager.setPageSize(condition.getRows());
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			
			//查询套餐明细统计数据
			List<Map<String,Object>> list = chineseSalesReportMapper.queryXstjb(condition);
			
			//若查出的数据不为0，则采用分页的语句
			if (list.size() != 0) {
				list = pageManager.selectPage(condition, pager, ChineseSalesReportMapper.class.getName()+".queryXstjb");
			}
			
			reportObject.setRows(list);
			reportObject.setFooter(chineseSalesReportMapper.queryCalForXstjb(condition));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询消退菜分析
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findGroupCRDishes(PublicEntity condition) throws CRUDException{
		try{
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			reportObject.setRows(chineseSalesReportMapper.findGroupCRDishes(condition));
			reportObject.setFooter(chineseSalesReportMapper.findCalForGroupCRDishes(condition));
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询消退菜分析_单店退菜明细
	 * @author 马振
	 * 创建时间：2015-7-14 下午2:45:28
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findGroupCRDishesByFirm(PublicEntity condition) throws CRUDException{
		try{
			Page page = condition.getPager();
			page.setNowPage(condition.getPage());
			page.setPageSize(condition.getRows());
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			reportObject.setRows(pageManager.selectPage(condition, page, ChineseSalesReportMapper.class.getName()+".findGroupCRDishesByFirm"));
			reportObject.setFooter(chineseSalesReportMapper.findGroupCRDishesByFirmTotal(condition));
			reportObject.setTotal(page.getCount());
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：排班报表
	 * @author 马振
	 * 创建时间：2015-10-30 下午3:22:24
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> createScheduleReport(PublicEntity condition)throws CRUDException{
		try{
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("bdat", condition.getBdat());
			map.put("edat", condition.getEdat());
			map.put("pk_store", FormatDouble.StringCodeReplace(condition.getPk_store()));
			map.put("dateList", DateJudge.getTimeList(condition.getInterval()));
			
			List<Map<String, Object>> createScheduleReport = chineseSalesReportMapper.createScheduleReport(map);
			
			for (int i = 0; i < createScheduleReport.size(); i++) {
				Map<String, Object> mapStr = createScheduleReport.get(i);
				
				//从第二条开始，在台数=上一个时间段内的在台数+本时间段内的在台数；为第一个时间段时，在台数就是本时间段内的在台数
				if (i > 0) {
					mapStr.put("ZTCOUNT", MisUtil.stringPlusDouble(mapStr.get("CHACOUNT"), createScheduleReport.get(i - 1).get("ZTCOUNT")));
				} else {
					mapStr.put("ZTCOUNT", mapStr.get("CHACOUNT"));
				}
			}
			
			List<Map<String, Object>> createScheduleReportSUM = chineseSalesReportMapper.createScheduleReportSUM(map);
			reportObject.setRows(createScheduleReport);
			reportObject.setFooter(createScheduleReportSUM);
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
}