package com.choice.misboh.service.chineseReport;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.ChartsXml;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.domain.BaseRecord.Marsaleclass;
import com.choice.misboh.domain.common.CommonMethod;
import com.choice.misboh.domain.reportMis.Business;
import com.choice.misboh.domain.reportMis.PublicEntity;
import com.choice.misboh.domain.reportMis.ReportObject;
import com.choice.misboh.persistence.BaseRecord.BaseRecordMapper;
import com.choice.misboh.persistence.chineseReport.ChineseFinancialReportMapper;
import com.choice.misboh.persistence.common.CommonMISBOHMapper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.ReadProperties;

/**
 * 描述：中餐报表-财务分析
 * @author 马振
 * 创建时间：2015-6-25 下午1:08:29
 */
@Service
public class ChineseFinancialReportService {

	@Autowired
	private ChineseFinancialReportMapper chineseFinancialReportMapper;
	
	@Autowired
	private ReportObject<Map<String,Object>> reportObject;
	
	@Autowired
	private ReportObject<Map<String,String>> reportString;
	
	@Autowired
	private PageManager<Map<String,Object>> pageManager;
	
	@Autowired
	private CommonMISBOHMapper commonMapper;
	
	@Autowired
	private BaseRecordMapper baseRecordMapper;

	/**
	 * 描述：查询数据           收入日结算
	 * @author 马振
	 * 创建时间：2015-6-26 下午3:58:52
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,String>> getDataRJS(PublicEntity condition,CommonMethod commonMethod)throws CRUDException{
		try{
			chineseFinancialReportMapper.deleteTerm();
			
			PublicEntity term = null;
			
			//餐次
			if("0".equals(condition.getSft())){
				List<CommonMethod> sftList = commonMapper.findAllShiftsft(new CommonMethod());
				for(CommonMethod sft : sftList){
					term = new PublicEntity();
					term.setCode("sft");
					term.setVal(sft.getVcode());
					chineseFinancialReportMapper.insertterm(term);
				}
			}else{
				term = new PublicEntity();
				term.setCode("sft");
				term.setVal(condition.getSft());
				chineseFinancialReportMapper.insertterm(term);
			}
			
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("bdat", condition.getBdat());
			map.put("edat", condition.getEdat());
			map.put("pk_store", condition.getPk_store());
			map.put("pr", 0);
			
			chineseFinancialReportMapper.insertDaySumReport(map);
			reportString.setRows(chineseFinancialReportMapper.findSqlRJS());
			reportString.setFooter(null);
			return reportString;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：导出数据           收入日结算
	 * @author 马振
	 * 创建时间：2015-6-26 下午3:58:12
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,String>> exportDataRJS()throws CRUDException{
		try{
			reportString.setRows(chineseFinancialReportMapper.findSqlRJS());
			reportString.setFooter(null);
			return reportString;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询数据           营业日报表
	 * @author 马振
	 * 创建时间：2015-6-26 下午5:33:49
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,String>> getDataYYBB(PublicEntity condition)throws CRUDException{
		try{
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			condition.setBdat(condition.getEdat().substring(0, 8)+"01");
			List<Business> bus = chineseFinancialReportMapper.findSqlYYBB(condition);
			List<Business> list = new ArrayList<Business>();
			Business bean = new Business();
			for(int i=0;i<bus.size();i++){
				bean.setCode(bus.get(i).getCode());
				bean.setDes(bus.get(i).getDes());
				if("D".equals(bus.get(i).getTag())){
					if("1".equals(bus.get(i).getSft())){
						bean.setDm1(bus.get(i).getAmt());
					} else if("2".equals(bus.get(i).getSft())){
						bean.setDm2(bus.get(i).getAmt());
					} else if("3".equals(bus.get(i).getSft())){
						bean.setDm3(bus.get(i).getAmt());
					} else if("4".equals(bus.get(i).getSft())){
						bean.setDm4(bus.get(i).getAmt());
					} else if("5".equals(bus.get(i).getSft())){
						bean.setDm5(bus.get(i).getAmt());
					} else if("6".equals(bus.get(i).getSft())){
						bean.setDm6(bus.get(i).getAmt());
					}
				}else if("M".equals(bus.get(i).getTag())){
					if("1".equals(bus.get(i).getSft())){
						bean.setMm1(bus.get(i).getAmt());
					} else if("2".equals(bus.get(i).getSft())){
						bean.setMm2(bus.get(i).getAmt());
					} else if("3".equals(bus.get(i).getSft())){
						bean.setMm3(bus.get(i).getAmt());
					} else if("4".equals(bus.get(i).getSft())){
						bean.setMm4(bus.get(i).getAmt());
					} else if("5".equals(bus.get(i).getSft())){
						bean.setMm5(bus.get(i).getAmt());
					} else if("6".equals(bus.get(i).getSft())){
						bean.setMm6(bus.get(i).getAmt());
					}
				}
				
				if(i+1 != bus.size() && !bus.get(i).getCode().equals(bus.get(i+1).getCode())){
					list.add(bean);
					bean = new Business();
				}else if(i+1 == bus.size()){
					list.add(bean);
				}
			}
			
			Business Abean = new Business();
			Business BCbean = new Business();
			Business Dbean = new Business();
			Business Ebean = new Business();
			for(int i=0;i<list.size();i++){
				double dm0 = list.get(i).getDm1()+list.get(i).getDm2()+list.get(i).getDm3()+list.get(i).getDm4()+list.get(i).getDm5()+list.get(i).getDm6();
				list.get(i).setDm0(dm0);
				double mm0 = list.get(i).getMm1()+list.get(i).getMm2()+list.get(i).getMm3()+list.get(i).getMm4()+list.get(i).getMm5()+list.get(i).getMm6();
				list.get(i).setMm0(mm0);
				if("A".equals(list.get(i).getCode().substring(0, 1)) || "D".equals(list.get(i).getCode().substring(0, 1))){
					Abean.setDm0(Abean.getDm0()+list.get(i).getDm0());
					Abean.setDm1(Abean.getDm1()+list.get(i).getDm1());
					Abean.setDm2(Abean.getDm2()+list.get(i).getDm2());
					Abean.setDm3(Abean.getDm3()+list.get(i).getDm3());
					Abean.setDm4(Abean.getDm4()+list.get(i).getDm4());
					Abean.setDm5(Abean.getDm5()+list.get(i).getDm5());
					Abean.setDm6(Abean.getDm6()+list.get(i).getDm6());
					Abean.setMm0(Abean.getMm0()+list.get(i).getMm0());
					Abean.setMm1(Abean.getMm1()+list.get(i).getMm1());
					Abean.setMm2(Abean.getMm2()+list.get(i).getMm2());
					Abean.setMm3(Abean.getMm3()+list.get(i).getMm3());
					Abean.setMm4(Abean.getMm4()+list.get(i).getMm4());
					Abean.setMm5(Abean.getMm5()+list.get(i).getMm5());
					Abean.setMm6(Abean.getMm6()+list.get(i).getMm6());
				}else if("C".equals(list.get(i).getCode().substring(0, 1))){
					BCbean.setDm0(BCbean.getDm0()+list.get(i).getDm0());
					BCbean.setDm1(BCbean.getDm1()+list.get(i).getDm1());
					BCbean.setDm2(BCbean.getDm2()+list.get(i).getDm2());
					BCbean.setDm3(BCbean.getDm3()+list.get(i).getDm3());
					BCbean.setDm4(BCbean.getDm4()+list.get(i).getDm4());
					BCbean.setDm5(BCbean.getDm5()+list.get(i).getDm5());
					BCbean.setDm6(BCbean.getDm6()+list.get(i).getDm6());
					BCbean.setMm0(BCbean.getMm0()+list.get(i).getMm0());
					BCbean.setMm1(BCbean.getMm1()+list.get(i).getMm1());
					BCbean.setMm2(BCbean.getMm2()+list.get(i).getMm2());
					BCbean.setMm3(BCbean.getMm3()+list.get(i).getMm3());
					BCbean.setMm4(BCbean.getMm4()+list.get(i).getMm4());
					BCbean.setMm5(BCbean.getMm5()+list.get(i).getMm5());
					BCbean.setMm6(BCbean.getMm6()+list.get(i).getMm6());
				}else if("D".equals(list.get(i).getCode().substring(0, 1))){
					Dbean.setDm0(Dbean.getDm0()+list.get(i).getDm0());
					Dbean.setDm1(Dbean.getDm1()+list.get(i).getDm1());
					Dbean.setDm2(Dbean.getDm2()+list.get(i).getDm2());
					Dbean.setDm3(Dbean.getDm3()+list.get(i).getDm3());
					Dbean.setDm4(Dbean.getDm4()+list.get(i).getDm4());
					Dbean.setDm5(Dbean.getDm5()+list.get(i).getDm5());
					Dbean.setDm6(Dbean.getDm6()+list.get(i).getDm6());
					Dbean.setMm0(Dbean.getMm0()+list.get(i).getMm0());
					Dbean.setMm1(Dbean.getMm1()+list.get(i).getMm1());
					Dbean.setMm2(Dbean.getMm2()+list.get(i).getMm2());
					Dbean.setMm3(Dbean.getMm3()+list.get(i).getMm3());
					Dbean.setMm4(Dbean.getMm4()+list.get(i).getMm4());
					Dbean.setMm5(Dbean.getMm5()+list.get(i).getMm5());
					Dbean.setMm6(Dbean.getMm6()+list.get(i).getMm6());
				}else if("E".equals(list.get(i).getCode().substring(0, 1))){
					Ebean.setDm0(Ebean.getDm0()+list.get(i).getDm0());
					Ebean.setDm1(Ebean.getDm1()+list.get(i).getDm1());
					Ebean.setDm2(Ebean.getDm2()+list.get(i).getDm2());
					Ebean.setDm3(Ebean.getDm3()+list.get(i).getDm3());
					Ebean.setDm4(Ebean.getDm4()+list.get(i).getDm4());
					Ebean.setDm5(Ebean.getDm5()+list.get(i).getDm5());
					Ebean.setDm6(Ebean.getDm6()+list.get(i).getDm6());
					Ebean.setMm0(Ebean.getMm0()+list.get(i).getMm0());
					Ebean.setMm1(Ebean.getMm1()+list.get(i).getMm1());
					Ebean.setMm2(Ebean.getMm2()+list.get(i).getMm2());
					Ebean.setMm3(Ebean.getMm3()+list.get(i).getMm3());
					Ebean.setMm4(Ebean.getMm4()+list.get(i).getMm4());
					Ebean.setMm5(Ebean.getMm5()+list.get(i).getMm5());
					Ebean.setMm6(Ebean.getMm6()+list.get(i).getMm6());
				}
			}
			
			for(int i=0;i<list.size();i++){
				DecimalFormat df = new DecimalFormat("############0.00%");
				if("A".equals(list.get(i).getCode().substring(0, 1)) || "D".equals(list.get(i).getCode().substring(0, 1))){
					list.get(i).setDb0(list.get(i).getDm0()!=0 ? df.format(list.get(i).getDm0()/Abean.getDm0()) : "0.00%");
					list.get(i).setDb1(list.get(i).getDm1()!=0 ? df.format(list.get(i).getDm1()/Abean.getDm1()) : "0.00%");
					list.get(i).setDb2(list.get(i).getDm2()!=0 ? df.format(list.get(i).getDm2()/Abean.getDm2()) : "0.00%");
					list.get(i).setDb3(list.get(i).getDm3()!=0 ? df.format(list.get(i).getDm3()/Abean.getDm3()) : "0.00%");
					list.get(i).setDb4(list.get(i).getDm4()!=0 ? df.format(list.get(i).getDm4()/Abean.getDm4()) : "0.00%");
					list.get(i).setDb5(list.get(i).getDm5()!=0 ? df.format(list.get(i).getDm5()/Abean.getDm5()) : "0.00%");
					list.get(i).setDb6(list.get(i).getDm6()!=0 ? df.format(list.get(i).getDm6()/Abean.getDm6()) : "0.00%");
					list.get(i).setMb0(list.get(i).getMm0()!=0 ? df.format(list.get(i).getMm0()/Abean.getMm0()) : "0.00%");
					list.get(i).setMb1(list.get(i).getMm1()!=0 ? df.format(list.get(i).getMm1()/Abean.getMm1()) : "0.00%");
					list.get(i).setMb2(list.get(i).getMm2()!=0 ? df.format(list.get(i).getMm2()/Abean.getMm2()) : "0.00%");
					list.get(i).setMb3(list.get(i).getMm3()!=0 ? df.format(list.get(i).getMm3()/Abean.getMm3()) : "0.00%");
					list.get(i).setMb4(list.get(i).getMm4()!=0 ? df.format(list.get(i).getMm4()/Abean.getMm4()) : "0.00%");
					list.get(i).setMb5(list.get(i).getMm5()!=0 ? df.format(list.get(i).getMm5()/Abean.getMm5()) : "0.00%");
					list.get(i).setMb6(list.get(i).getMm6()!=0 ? df.format(list.get(i).getMm6()/Abean.getMm6()) : "0.00%");
				}else if("C".equals(list.get(i).getCode().substring(0, 1))){
					list.get(i).setDb0(list.get(i).getDm0()!=0 ? df.format(list.get(i).getDm0()/BCbean.getDm0()) : "0.00%");
					list.get(i).setDb1(list.get(i).getDm1()!=0 ? df.format(list.get(i).getDm1()/BCbean.getDm1()) : "0.00%");
					list.get(i).setDb2(list.get(i).getDm2()!=0 ? df.format(list.get(i).getDm2()/BCbean.getDm2()) : "0.00%");
					list.get(i).setDb3(list.get(i).getDm3()!=0 ? df.format(list.get(i).getDm3()/BCbean.getDm3()) : "0.00%");
					list.get(i).setDb4(list.get(i).getDm4()!=0 ? df.format(list.get(i).getDm4()/BCbean.getDm4()) : "0.00%");
					list.get(i).setDb5(list.get(i).getDm5()!=0 ? df.format(list.get(i).getDm5()/BCbean.getDm5()) : "0.00%");
					list.get(i).setDb6(list.get(i).getDm6()!=0 ? df.format(list.get(i).getDm6()/BCbean.getDm6()) : "0.00%");
					list.get(i).setMb0(list.get(i).getMm0()!=0 ? df.format(list.get(i).getMm0()/BCbean.getMm0()) : "0.00%");
					list.get(i).setMb1(list.get(i).getMm1()!=0 ? df.format(list.get(i).getMm1()/BCbean.getMm1()) : "0.00%");
					list.get(i).setMb2(list.get(i).getMm2()!=0 ? df.format(list.get(i).getMm2()/BCbean.getMm2()) : "0.00%");
					list.get(i).setMb3(list.get(i).getMm3()!=0 ? df.format(list.get(i).getMm3()/BCbean.getMm3()) : "0.00%");
					list.get(i).setMb4(list.get(i).getMm4()!=0 ? df.format(list.get(i).getMm4()/BCbean.getMm4()) : "0.00%");
					list.get(i).setMb5(list.get(i).getMm5()!=0 ? df.format(list.get(i).getMm5()/BCbean.getMm5()) : "0.00%");
					list.get(i).setMb6(list.get(i).getMm6()!=0 ? df.format(list.get(i).getMm6()/BCbean.getMm6()) : "0.00%");
				}else if("D".equals(list.get(i).getCode().substring(0, 1))){
					list.get(i).setDb0(list.get(i).getDm0()!=0 ? df.format(list.get(i).getDm0()/Dbean.getDm0()) : "0.00%");
					list.get(i).setDb1(list.get(i).getDm1()!=0 ? df.format(list.get(i).getDm1()/Dbean.getDm1()) : "0.00%");
					list.get(i).setDb2(list.get(i).getDm2()!=0 ? df.format(list.get(i).getDm2()/Dbean.getDm2()) : "0.00%");
					list.get(i).setDb3(list.get(i).getDm3()!=0 ? df.format(list.get(i).getDm3()/Dbean.getDm3()) : "0.00%");
					list.get(i).setDb4(list.get(i).getDm4()!=0 ? df.format(list.get(i).getDm4()/Dbean.getDm4()) : "0.00%");
					list.get(i).setDb5(list.get(i).getDm5()!=0 ? df.format(list.get(i).getDm5()/Dbean.getDm5()) : "0.00%");
					list.get(i).setDb6(list.get(i).getDm6()!=0 ? df.format(list.get(i).getDm6()/Dbean.getDm6()) : "0.00%");
					list.get(i).setMb0(list.get(i).getMm0()!=0 ? df.format(list.get(i).getMm0()/Dbean.getMm0()) : "0.00%");
					list.get(i).setMb1(list.get(i).getMm1()!=0 ? df.format(list.get(i).getMm1()/Dbean.getMm1()) : "0.00%");
					list.get(i).setMb2(list.get(i).getMm2()!=0 ? df.format(list.get(i).getMm2()/Dbean.getMm2()) : "0.00%");
					list.get(i).setMb3(list.get(i).getMm3()!=0 ? df.format(list.get(i).getMm3()/Dbean.getMm3()) : "0.00%");
					list.get(i).setMb4(list.get(i).getMm4()!=0 ? df.format(list.get(i).getMm4()/Dbean.getMm4()) : "0.00%");
					list.get(i).setMb5(list.get(i).getMm5()!=0 ? df.format(list.get(i).getMm5()/Dbean.getMm5()) : "0.00%");
					list.get(i).setMb6(list.get(i).getMm6()!=0 ? df.format(list.get(i).getMm6()/Dbean.getMm6()) : "0.00%");
				}else if("E".equals(list.get(i).getCode().substring(0, 1))){
					list.get(i).setDb0(list.get(i).getDm0()!=0 ? df.format(list.get(i).getDm0()/Ebean.getDm0()) : "0.00%");
					list.get(i).setDb1(list.get(i).getDm1()!=0 ? df.format(list.get(i).getDm1()/Ebean.getDm1()) : "0.00%");
					list.get(i).setDb2(list.get(i).getDm2()!=0 ? df.format(list.get(i).getDm2()/Ebean.getDm2()) : "0.00%");
					list.get(i).setDb3(list.get(i).getDm3()!=0 ? df.format(list.get(i).getDm3()/Ebean.getDm3()) : "0.00%");
					list.get(i).setDb4(list.get(i).getDm4()!=0 ? df.format(list.get(i).getDm4()/Ebean.getDm4()) : "0.00%");
					list.get(i).setDb5(list.get(i).getDm5()!=0 ? df.format(list.get(i).getDm5()/Ebean.getDm5()) : "0.00%");
					list.get(i).setDb6(list.get(i).getDm6()!=0 ? df.format(list.get(i).getDm6()/Ebean.getDm6()) : "0.00%");
					list.get(i).setMb0(list.get(i).getMm0()!=0 ? df.format(list.get(i).getMm0()/Ebean.getMm0()) : "0.00%");
					list.get(i).setMb1(list.get(i).getMm1()!=0 ? df.format(list.get(i).getMm1()/Ebean.getMm1()) : "0.00%");
					list.get(i).setMb2(list.get(i).getMm2()!=0 ? df.format(list.get(i).getMm2()/Ebean.getMm2()) : "0.00%");
					list.get(i).setMb3(list.get(i).getMm3()!=0 ? df.format(list.get(i).getMm3()/Ebean.getMm3()) : "0.00%");
					list.get(i).setMb4(list.get(i).getMm4()!=0 ? df.format(list.get(i).getMm4()/Ebean.getMm4()) : "0.00%");
					list.get(i).setMb5(list.get(i).getMm5()!=0 ? df.format(list.get(i).getMm5()/Ebean.getMm5()) : "0.00%");
					list.get(i).setMb6(list.get(i).getMm6()!=0 ? df.format(list.get(i).getMm6()/Ebean.getMm6()) : "0.00%");
				}
			}
			
			List<Business> listSun = new ArrayList<Business>();
			Ebean.setCode("W001");
			Ebean.setDes("营业收入合计");
			listSun.add(Ebean);
//			Dbean.setCode("X001");
//			Dbean.setDes("增收合计");
//			listSun.add(Dbean);
			BCbean.setCode("Y001");
			BCbean.setDes("优免合计");
			listSun.add(BCbean);
			Abean.setCode("Z001");
			Abean.setDes("净收入合计");
			listSun.add(Abean);
			
			reportString.setRows(getMapByListRows(list));
			reportString.setFooter(getMapByListFooter(listSun));
			return reportString;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：营业日报表数据
	 * @author 马振
	 * 创建时间：2015-6-26 下午5:36:55
	 * @param list
	 * @return
	 */
	private List<Map<String,String>> getMapByListRows(List<Business> list){
		List<Map<String, String>> rs = new ArrayList<Map<String,String>>();
		DecimalFormat df = new DecimalFormat("#,###,###,###,###,##0.00");
		for(int i=0;i<list.size();i++){
			Map<String, String> map = new HashMap<String, String>();
			map.put("CODE", list.get(i).getCode());
			map.put("DES", list.get(i).getDes());
			map.put("DM0", df.format(list.get(i).getDm0()));
			map.put("DB0", list.get(i).getDb0());
			map.put("DM1", df.format(list.get(i).getDm1()));
			map.put("DB1", list.get(i).getDb1());
			map.put("DM2", df.format(list.get(i).getDm2()));
			map.put("DB2", list.get(i).getDb2());
			map.put("DM3", df.format(list.get(i).getDm3()));
			map.put("DB3", list.get(i).getDb3());
			map.put("DM4", df.format(list.get(i).getDm4()));
			map.put("DB4", list.get(i).getDb4());
			map.put("DM5", df.format(list.get(i).getDm5()));
			map.put("DB5", list.get(i).getDb5());
			map.put("DM6", df.format(list.get(i).getDm6()));
			map.put("DB6", list.get(i).getDb6());
			map.put("MM0", df.format(list.get(i).getMm0()));
			map.put("MB0", list.get(i).getMb0());
			map.put("MM1", df.format(list.get(i).getMm1()));
			map.put("MB1", list.get(i).getMb1());
			map.put("MM2", df.format(list.get(i).getMm2()));
			map.put("MB2", list.get(i).getMb2());
			map.put("MM3", df.format(list.get(i).getMm3()));
			map.put("MB3", list.get(i).getMb3());
			map.put("MM4", df.format(list.get(i).getMm4()));
			map.put("MB4", list.get(i).getMb4());
			map.put("MM5", df.format(list.get(i).getMm5()));
			map.put("MB5", list.get(i).getMb5());
			map.put("MM6", df.format(list.get(i).getMm6()));
			map.put("MB6", list.get(i).getMb6());
			rs.add(map);
		}
		return rs;
	}
	
	/**
	 * 描述：营业日报表合计
	 * @author 马振
	 * 创建时间：2015-6-26 下午5:37:10
	 * @param list
	 * @return
	 */
	private List<Map<String,Object>> getMapByListFooter(List<Business> list){
		List<Map<String, Object>> rs = new ArrayList<Map<String,Object>>();
		DecimalFormat df = new DecimalFormat("#,###,###,###,###,##0.00");
		for(int i=0;i<list.size();i++){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("CODE", list.get(i).getCode());
			map.put("DES", list.get(i).getDes());
			map.put("DM0", df.format(list.get(i).getDm0()));
			map.put("DB0", list.get(i).getDb0());
			map.put("DM1", df.format(list.get(i).getDm1()));
			map.put("DB1", list.get(i).getDb1());
			map.put("DM2", df.format(list.get(i).getDm2()));
			map.put("DB2", list.get(i).getDb2());
			map.put("DM3", df.format(list.get(i).getDm3()));
			map.put("DB3", list.get(i).getDb3());
			map.put("DM4", df.format(list.get(i).getDm4()));
			map.put("DB4", list.get(i).getDb4());
			map.put("DM5", df.format(list.get(i).getDm5()));
			map.put("DB5", list.get(i).getDb5());
			map.put("DM6", df.format(list.get(i).getDm6()));
			map.put("DB6", list.get(i).getDb6());
			map.put("MM0", df.format(list.get(i).getMm0()));
			map.put("MB0", list.get(i).getMb0());
			map.put("MM1", df.format(list.get(i).getMm1()));
			map.put("MB1", list.get(i).getMb1());
			map.put("MM2", df.format(list.get(i).getMm2()));
			map.put("MB2", list.get(i).getMb2());
			map.put("MM3", df.format(list.get(i).getMm3()));
			map.put("MB3", list.get(i).getMb3());
			map.put("MM4", df.format(list.get(i).getMm4()));
			map.put("MB4", list.get(i).getMb4());
			map.put("MM5", df.format(list.get(i).getMm5()));
			map.put("MB5", list.get(i).getMb5());
			map.put("MM6", df.format(list.get(i).getMm6()));
			map.put("MB6", list.get(i).getMb6());
			rs.add(map);
		}
		return rs;
	}
	
	/**
	 * 描述：导出excel表格（营业报表专用）
	 * @author 马振
	 * 创建时间：2015-6-26 下午5:38:55
	 * @param request
	 * @param response
	 * @param list
	 * @return
	 */
	public<T> boolean creatWorkBook_YYBB(HttpServletRequest request,HttpServletResponse response,List<Map<String,String>> list){
		OutputStream os = null;
		WritableWorkbook workBook = null;
		Label label = null;
		String title="营业日报表";
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableFont contentFont = new WritableFont(WritableFont.TIMES, 10,  
	            WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableFont headerFont = new WritableFont(WritableFont.TIMES, 10,  
	            WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);
		WritableCellFormat contentStyle = new WritableCellFormat(contentFont);
		WritableCellFormat headerStyle = new WritableCellFormat(headerFont);
			try {
				titleStyle.setAlignment(Alignment.CENTRE);
				headerStyle.setAlignment(Alignment.CENTRE);
				os = response.getOutputStream();
				workBook = Workbook.createWorkbook(os);
				WritableSheet sheet = workBook.createSheet(title, 0);
	            //设置表格表头
				label = new Label(0,0,title,titleStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(0, 0, 17, 0);
	            label = new Label(0,1,"科目代码",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(0, 1, 0, 4);
	            label = new Label(1,1,"科目名称",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(1, 1, 1, 4);
	            label = new Label(2,1,"本日",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(2, 1, 9, 1);
	            label = new Label(10,1,"月初至本日",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(10, 1, 17, 1);
	            label = new Label(2,2,"全天合计",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(2, 2, 3, 3);
	            label = new Label(4,2,"班次",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(4, 2, 9, 2);
	            label = new Label(10,2,"全天合计",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(10, 2, 11, 3);
	            label = new Label(12,2,"班次",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(12, 2, 17, 2);
	            label = new Label(4,3,"早班",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(4, 3, 5, 3);
	            label = new Label(6,3,"中班",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(6, 3, 7, 3);
	            label = new Label(8,3,"晚班",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(8, 3, 9, 3);
	            label = new Label(12,3,"早班",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(12, 3, 13, 3);
	            label = new Label(14,3,"中班",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(14, 3, 15, 3);
	            label = new Label(16,3,"晚班",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(16, 3, 17, 3);
	            label = new Label(2,4,"金额",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(2, 4, 2, 4);
	            label = new Label(3,4,"比例(%)",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(3, 4, 3, 4);
	            label = new Label(2,4,"金额",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(2, 4, 2, 4);
	            label = new Label(3,4,"比例(%)",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(3, 4, 3, 4);
	            label = new Label(4,4,"金额",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(4, 4, 4, 4);
	            label = new Label(5,4,"比例(%)",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(5, 4, 5, 4);
	            label = new Label(6,4,"金额",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(6, 4, 6, 4);
	            label = new Label(7,4,"比例(%)",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(7, 4, 7, 4);
	            label = new Label(8,4,"金额",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(8, 4, 8, 4);
	            label = new Label(9,4,"比例(%)",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(9, 4, 9, 4);
	            label = new Label(10,4,"金额",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(10, 4, 10, 4);
	            label = new Label(11,4,"比例(%)",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(11, 4, 11, 4);
	            label = new Label(12,4,"金额",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(12, 4, 12, 4);
	            label = new Label(13,4,"比例(%)",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(13, 4, 13, 4);
	            label = new Label(14,4,"金额",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(14, 4, 14, 4);
	            label = new Label(15,4,"比例(%)",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(15, 4, 15, 4);
	            label = new Label(16,4,"金额",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(16, 4, 16, 4);
	            label = new Label(17,4,"比例(%)",headerStyle);
	            sheet.addCell(label);
	            sheet.mergeCells(17, 4, 17, 4);

	            //遍历list填充表格内容
	            for(int i=0;i<list.size();i++){
	            	Map<String,String> mapdata = list.get(i);
	            	label = new Label(0,i+5,String.valueOf(mapdata.get("CODE")),contentStyle);
		            sheet.addCell(label);
		            label = new Label(1,i+5,String.valueOf(mapdata.get("DES")),contentStyle);
		            sheet.addCell(label);
		            label = new Label(2,i+5,String.valueOf(mapdata.get("DM0")),contentStyle);
		            sheet.addCell(label);
		            label = new Label(3,i+5,String.valueOf(mapdata.get("DB0")),contentStyle);
		            sheet.addCell(label);
		            label = new Label(4,i+5,String.valueOf(mapdata.get("DM1")),contentStyle);
		            sheet.addCell(label);
		            label = new Label(5,i+5,String.valueOf(mapdata.get("DB1")),contentStyle);
		            sheet.addCell(label);
		            label = new Label(6,i+5,String.valueOf(mapdata.get("DM2")),contentStyle);
		            sheet.addCell(label);
		            label = new Label(7,i+5,String.valueOf(mapdata.get("DB2")),contentStyle);
		            sheet.addCell(label);
		            label = new Label(8,i+5,String.valueOf(mapdata.get("DM3")),contentStyle);
		            sheet.addCell(label);
		            label = new Label(9,i+5,String.valueOf(mapdata.get("DB3")),contentStyle);
		            sheet.addCell(label);
		            label = new Label(10,i+5,String.valueOf(mapdata.get("MM0")),contentStyle);
		            sheet.addCell(label);
		            label = new Label(11,i+5,String.valueOf(mapdata.get("MB0")),contentStyle);
		            sheet.addCell(label);
		            label = new Label(12,i+5,String.valueOf(mapdata.get("MM1")),contentStyle);
		            sheet.addCell(label);
		            label = new Label(13,i+5,String.valueOf(mapdata.get("MB1")),contentStyle);
		            sheet.addCell(label);
		            label = new Label(14,i+5,String.valueOf(mapdata.get("MM2")),contentStyle);
		            sheet.addCell(label);
		            label = new Label(15,i+5,String.valueOf(mapdata.get("MB2")),contentStyle);
		            sheet.addCell(label);
		            label = new Label(16,i+5,String.valueOf(mapdata.get("MM3")),contentStyle);
		            sheet.addCell(label);
		            label = new Label(17,i+5,String.valueOf(mapdata.get("MB3")),contentStyle);
		            sheet.addCell(label);
	            }
	            
				response.setContentType("application/msexcel; charset=UTF-8");
				if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
				    //IE  
					title = URLEncoder.encode(title, "UTF-8");              
				}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
				    //firefox  
					title = new String(title.getBytes("UTF-8"), "ISO8859-1");              
				}else{                
				    // other          
					title = new String(title.getBytes("UTF-8"), "ISO8859-1");              
				}   
				response.setHeader("Content-disposition", "attachment; filename="  
		                + title + ".xls");
				
            workBook.write();
            os.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/************************************************账单分析******************************************************/
	
	/**
	 * 描述：查询单店账单分析报表_表格显示(动态表头查询)
	 * @author 马振
	 * 创建时间：2015-6-27 上午11:03:04
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	@SuppressWarnings("unchecked")
	public ReportObject<Map<String, Object>> queryDdzdfx(PublicEntity condition) throws CRUDException {
		try {
			ReadProperties rp = new ReadProperties();
			String dataSource = rp.getStrByParam("dataSource");
			
			Page pager = condition.getPager();
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			StringBuffer sqlStr1 = new StringBuffer();
			StringBuffer sqlStr2 = new StringBuffer();
			StringBuffer sqlStr3 = new StringBuffer();
			StringBuffer sqlStr4 = new StringBuffer();
			StringBuffer sqlStr5 = new StringBuffer();
			StringBuffer sqlStr6 = new StringBuffer();
			StringBuffer sqlStr7 = new StringBuffer();
			StringBuffer sqlStr8 = new StringBuffer();
			StringBuffer sqlData = new StringBuffer();
			StringBuffer sqlAll = new StringBuffer();
			List<Map<String, Object>> pays = new ArrayList<Map<String,Object>>();
			List<Marsaleclass> typs = new ArrayList<Marsaleclass>();
			
			pays = commonMapper.findPayment();
			for(Map<String, Object> map : pays){
				String curPay = map.get("VCODE").toString();
				if("sqlserver".equals(dataSource)){
					sqlStr1.append(",D.PAY" + curPay);
					sqlStr2.append(",SUM(CASE WHEN A.VOPERATE='"+ curPay).append("' THEN (CASE WHEN A.VYPAYFLAG='N' THEN ISNULL(A.NMONEY,0) ELSE 0 END) ELSE 0 END) AS PAY" + curPay);
					sqlStr5.append(",CAST(CAST(SUM(PAY" + curPay).append(") AS DECIMAL(28,2))  AS VARCHAR(30)) AS PAY" + curPay);
					sqlStr6.append(",CAST(CAST(AVG(PAY" + curPay).append(") AS DECIMAL(28,2))  AS VARCHAR(30)) AS PAY" + curPay);
					sqlData.append(",CAST(CAST(PAY" + curPay).append(" AS DECIMAL(28,2))  AS VARCHAR(30)) AS PAY" + curPay);
				} else if ("mysql".equals(dataSource)) {
					sqlStr1.append(",D.PAY" + curPay);
					sqlStr2.append(",SUM(IF(A.VOPERATE='"+ curPay).append("',IF(A.VYPAYFLAG='N',IFNULL(A.NMONEY,0),0),0)) AS PAY" + curPay);
					sqlStr5.append(",SUM(IFNULL(PAY" + curPay).append(",'0.00')) AS PAY" + curPay);
					sqlStr6.append(",AVG(IFNULL(PAY" + curPay).append(",'0.00')) AS PAY" + curPay);
					sqlData.append(",IFNULL(PAY" + curPay).append(",'0.00') AS PAY" + curPay);
				} else {
					sqlStr1.append(",D.PAY" + curPay);
					sqlStr2.append(",SUM(DECODE(A.VOPERATE,'"+ curPay).append("',DECODE(A.VYPAYFLAG,'N',NVL(A.NMONEY,0)))) AS PAY" + curPay);
					sqlStr5.append(",TO_CHAR(SUM(NVL(PAY" + curPay).append(",'0.00')),'9,999,999,999,999,990.99') AS PAY" + curPay);
					sqlStr6.append(",TO_CHAR(AVG(NVL(PAY" + curPay).append(",'0.00')),'9,999,999,999,999,990.99') AS PAY" + curPay);
					sqlData.append(",TO_CHAR(NVL(PAY" + curPay).append(",'0.00'),'9,999,999,999,999,990.99') AS PAY" + curPay);
				}
			}
			
			typs = baseRecordMapper.findAllMarsaleclassOne(new Marsaleclass());
			for(int i=0;i<typs.size();i++){
				Map<String, Object> map = (Map<String, Object>) typs.get(i);
				if(ValueUtil.IsEmpty(map.get("VCNCODEONE"))){
					continue;
				}
				String curTyp = map.get("VCNCODEONE").toString();
				if("sqlserver".equals(dataSource)){
					sqlStr3.append(",E.TYP" + curTyp);
					sqlStr4.append(",SUM(CASE WHEN IGRPTYP="+ curTyp).append(" THEN NYMONEY+NTAX+NPACKDISC ELSE 0 END) AS TYP" + curTyp);
					sqlStr7.append(",CAST(CAST(SUM(TYP" + curTyp).append(") AS DECIMAL(28,2))  AS VARCHAR(30)) AS TYP" + curTyp);
					sqlStr8.append(",CAST(CAST(AVG(TYP" + curTyp).append(") AS DECIMAL(28,2))  AS VARCHAR(30)) AS TYP" + curTyp);
					sqlAll.append(",CAST(CAST(TYP" + curTyp).append(" AS DECIMAL(28,2))  AS VARCHAR(30)) AS TYP" + curTyp);
				} else if ("mysql".equals(dataSource)) {
					sqlStr3.append(",E.TYP" + curTyp);
					sqlStr4.append(",SUM(IF(IGRPTYP="+ curTyp).append(",NYMONEY+NTAX+NPACKDISC,0)) AS TYP" + curTyp);
					sqlStr7.append(",SUM(IFNULL(TYP" + curTyp).append(",'0.00')) AS TYP" + curTyp);
					sqlStr8.append(",AVG(IFNULL(TYP" + curTyp).append(",'0.00')) AS TYP" + curTyp);
					sqlAll.append(",IFNULL(TYP" + curTyp).append(",'0.00') AS TYP" + curTyp);
				} else {
					sqlStr3.append(",E.TYP" + curTyp);
					sqlStr4.append(",SUM(DECODE(IGRPTYP,"+ curTyp).append(",NYMONEY+NTAX+NPACKDISC)) AS TYP" + curTyp);
					sqlStr7.append(",TO_CHAR(SUM(NVL(TYP" + curTyp).append(",'0.00')),'9,999,999,999,999,990.99') AS TYP" + curTyp);
					sqlStr8.append(",TO_CHAR(AVG(NVL(TYP" + curTyp).append(",'0.00')),'9,999,999,999,999,990.99') AS TYP" + curTyp);
					sqlAll.append(",TO_CHAR(NVL(TYP" + curTyp).append(",'0.00'),'9,999,999,999,999,990.99') AS TYP" + curTyp);
				}
			}
			
			condition.setSqlStr1(sqlStr1.length()>0?sqlStr1.toString():",1");
			condition.setSqlStr2(sqlStr2.toString());
			condition.setSqlStr3(sqlStr3.length()>0?sqlStr3.toString():",1");
			condition.setSqlStr4(sqlStr4.toString());
			condition.setSqlStr5(sqlStr5.toString());
			condition.setSqlStr6(sqlStr6.toString());
			condition.setSqlStr7(sqlStr7.toString());
			condition.setSqlStr8(sqlStr8.toString());
			condition.setSqlData(sqlData.toString());
			condition.setSqlAll(sqlAll.toString());
			
			reportObject.setRows(pageManager.selectPage(condition, pager, ChineseFinancialReportMapper.class.getName()+".queryDdzdfx"));
			reportObject.setFooter(chineseFinancialReportMapper.queryCalForDdzdfx(condition));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询单店账单分析报表_账单明细
	 * @author 马振
	 * 创建时间：2015-6-27 上午11:03:33
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String, Object>> findZhangDanMingXi(PublicEntity condition) throws CRUDException {
		try {
			Page pager = condition.getPager();
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			
			reportObject.setRows(pageManager.selectPage(condition, pager, ChineseFinancialReportMapper.class.getName()+".findZhangDanMingXi"));
			reportObject.setFooter(chineseFinancialReportMapper.findCalForZhangDanMingXi(condition));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}

	/**
	 * 描述：查询单店账单分析报表_汇总报表
	 * @author 马振
	 * 创建时间：2015-6-27 上午11:03:44
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String, Object>> queryDdzdfxByHuiZong(PublicEntity condition) throws CRUDException {
		try {
			Page pager = condition.getPager();
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			
			reportObject.setRows(pageManager.selectPage(condition, pager, ChineseFinancialReportMapper.class.getName()+".queryDdzdfxByHuiZong"));
			reportObject.setFooter(chineseFinancialReportMapper.queryCalForDdzdfxByHuiZong(condition));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：门店反结算分析
	 * @author 马振
	 * 创建时间：2015-7-13 下午8:02:45
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String, Object>> queryDdfjsfx(PublicEntity condition) throws CRUDException {
		try {
			Page pager = condition.getPager();
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			
			reportObject.setRows(pageManager.selectPage(condition, pager, ChineseFinancialReportMapper.class.getName()+".queryDdfjsfx"));
			reportObject.setFooter(chineseFinancialReportMapper.queryCalForDdfjsfx(condition));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：门店反结算分析明细
	 * @author 马振
	 * 创建时间：2015-7-13 下午8:07:00
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String, Object>> findDdfjsmx(PublicEntity condition) throws CRUDException {
		try {
			Page pager = condition.getPager();
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			
			reportObject.setRows(pageManager.selectPage(condition, pager, ChineseFinancialReportMapper.class.getName()+".findDdfjsmx"));
			reportObject.setFooter(chineseFinancialReportMapper.findCalForDdfjsmx(condition));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询单店收银统计_收银方式统计
	 * @author 马振
	 * 创建时间：2015-7-13 下午8:13:36
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String, Object>> queryDdsyfstj(PublicEntity condition) throws CRUDException {
		SimpleDateFormat sft = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Page pager = condition.getPager();
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			if(!"".equals(condition.getBdat()) && !"".equals(condition.getEdat())){
				int days = new Long((sft.parse(condition.getEdat()).getTime() - sft.parse(condition.getBdat()).getTime())/1000/60/60/24+1).intValue();
				condition.setDays(days);
			}
			
			reportObject.setRows(pageManager.selectPage(condition, pager, ChineseFinancialReportMapper.class.getName()+".queryDdsyfstj"));
			reportObject.setFooter(chineseFinancialReportMapper.queryCalForDdsyfstj(condition));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}

	/**
	 * 描述：单店收银统计_收银方式统计图表1
	 * @author 马振
	 * 创建时间：2015-7-13 下午8:15:35
	 * @param response
	 * @param condition
	 * @return
	 */
	public ChartsXml findXmlForCharts(HttpServletResponse response,PublicEntity condition) {
		condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
		List<Map<String, Object>> list = chineseFinancialReportMapper.queryDdsyfstj(condition);
		Set<String> set = new HashSet<String>();
		Map<String, String> attribute = new HashMap<String, String>();
		attribute.put("caption", "结账单数对比图");
		attribute.put("xAxisName", "结算方式");
		attribute.put("yAxisName", "结账份数");
		attribute.put("decimalPrecision", "0");
		attribute.put("chartLeftMargin", "35");
		attribute.put("formatNumberScale", "0");
		attribute.put("baseFontSize", String.valueOf(11));
		attribute.put("showNames", "1");
		Random ran = new Random(666);
		ChartsXml xml = ChartsXml.builtChartsXml(attribute, response);
		for (Map<String, Object> cur : list) {
			Map<String, String> attr = new HashMap<String, String>();
			attr.put("name", null == cur.get("VNAME") ? "" : cur.get("VNAME").toString());
			attr.put("value", null == cur.get("CNT") ? "" : cur.get("CNT").toString());
			set.add(cur.get("CNT").toString());
			attr.put("color", String.format("%06x", ran.nextInt(0xffffff)));
			xml.addElement("set", attr);
		}
		if (set.size() == 1 && "0".equals(set.toArray()[0])) {
			xml.addRootAttribute("yAxisMinValue", "-10");
			xml.addRootAttribute("yAxisMaxValue", "10");
		}
		return xml;
	}

	/**
	 * 描述：单店收银统计_收银方式统计图表2
	 * @author 马振
	 * 创建时间：2015-7-13 下午8:15:47
	 * @param response
	 * @param condition
	 * @return
	 */
	public ChartsXml findXmlForCharts2(HttpServletResponse response,PublicEntity condition) {
		condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
		List<Map<String, Object>> list = chineseFinancialReportMapper.queryDdsyfstj(condition);
		Set<String> set = new HashSet<String>();
		Map<String, String> attribute = new HashMap<String, String>();
		attribute.put("caption", "结账金额对比图");
		attribute.put("xAxisName", "结算方式");
		attribute.put("yAxisName", "结账金额");
		attribute.put("decimalPrecision", "0");
		attribute.put("chartLeftMargin", "35");
		attribute.put("formatNumberScale", "0");
		attribute.put("baseFontSize", String.valueOf(11));
		attribute.put("showNames", "1");
		Random ran = new Random(666);
		ChartsXml xml = ChartsXml.builtChartsXml(attribute, response);
		for (Map<String, Object> cur : list) {
			Map<String, String> attr = new HashMap<String, String>();
			attr.put("name", null == cur.get("VNAME") ? "" : cur.get("VNAME").toString());
			attr.put("value", null == cur.get("AMT") ? "" : cur.get("AMT").toString());
			set.add(cur.get("AMT").toString());
			attr.put("color", String.format("%06x", ran.nextInt(0xffffff)));
			xml.addElement("set", attr);
		}
		if (set.size() == 1 && "0".equals(set.toArray()[0])) {
			xml.addRootAttribute("yAxisMinValue", "-10");
			xml.addRootAttribute("yAxisMaxValue", "10");
		}
		return xml;
	}

	/**
	 * 描述：查询单店收银统计_结算方式明细
	 * @author 马振
	 * 创建时间：2015-7-13 下午8:15:56
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String, Object>> queryDdsyjsinfo(PublicEntity condition) throws CRUDException {
		try {
			Page pager = condition.getPager();
			condition.setPk_store(FormatDouble.StringCodeReplace(condition.getPk_store()));
			
			reportObject.setRows(pageManager.selectPage(condition, pager, ChineseFinancialReportMapper.class.getName()+".queryDdsyjsinfo"));
			reportObject.setFooter(chineseFinancialReportMapper.queryCalForDdsyjsinfo(condition));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
}