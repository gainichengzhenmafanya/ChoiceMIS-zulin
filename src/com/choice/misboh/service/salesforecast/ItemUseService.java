package com.choice.misboh.service.salesforecast;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.domain.BaseRecord.PubItem;
import com.choice.misboh.domain.salesforecast.FirmItemUse;
import com.choice.misboh.persistence.salesforecast.ItemUseMapper;

@Service
public class ItemUseService {
	
	@Autowired
	private ItemUseMapper itemUseMapper;

	private final transient Log log = LogFactory.getLog(ItemUseService.class);

	/***
	 * 查找所有的菜品点击率
	 * @param itemUse
	 * @return
	 * @throws CRUDException
	 */
	public List<FirmItemUse> findAllItemUse(FirmItemUse itemUse) throws CRUDException{
		try {
			return itemUseMapper.findAllItemUse(itemUse);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除所有菜品点击率
	 * @param itemUse
	 * @throws CRUDException
	 */
	public void deleteAll(FirmItemUse itemUse) throws CRUDException{
		try{
			itemUseMapper.deleteAll(itemUse);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 计算菜品点击率
	 * @author 文清泉 2015年5月27日 上午10:53:47
	 * @param firmItemUse
	 * @throws CRUDException 
	 */
	public String saveNewCalculateItemuse(FirmItemUse firmItemUse) throws CRUDException {
		String firmId = firmItemUse.getFirm();
		Date bdate = firmItemUse.getBdate();
		Date edate = firmItemUse.getEdate();
		String vplantyp = firmItemUse.getVplantyp();
		List<Map<String,Object>> totalMap = new ArrayList<Map<String,Object>>();
		List<Map<String,Object>> listMap = new ArrayList<Map<String,Object>>();
		List<Map<String,Object>> pcodeMap = new ArrayList<Map<String,Object>>();
		// 读取总和
		totalMap = itemUseMapper.getCalculateItemuseTotal(firmId,DateJudge.YYYY_MM_DD.format(bdate),DateJudge.YYYY_MM_DD.format(edate));
		if(totalMap == null || totalMap.size() == 0){
			return "0";
		}
		// 读取菜品销售情况
		listMap = itemUseMapper.getCalculateItemuse(firmId,DateJudge.YYYY_MM_DD.format(bdate),DateJudge.YYYY_MM_DD.format(edate));
		//读取所以菜品编码与单位
		pcodeMap = itemUseMapper.getCalculatePcode(firmId,DateJudge.YYYY_MM_DD.format(bdate),DateJudge.YYYY_MM_DD.format(edate));
		
		Map<Object,Map<String,Object>> totalMap1 = new HashMap<Object,Map<String,Object>>();
		// 循环库中读取合计值，将节假日数据汇总
		for (Map<String, Object> map : totalMap) {
			if(ValueUtil.getStringValue(map.get("ISHOLIDAY")).equals("0")){
				totalMap1.put(map.get("WEEKDAY"), map);
			}else{
				// 确定是否第一次初始化
				if(totalMap1.get("holiday")==null){
					map.put("WEEKDAY", "holiday");
					totalMap1.put("holiday", map);
				}else{
					Map<String,Object> holidayMap = totalMap1.get("holiday");
					holidayMap.put("AMT", ValueUtil.getDoubleValue(holidayMap.get("AMT"))+ValueUtil.getDoubleValue(map.get("AMT")));
					holidayMap.put("TC", ValueUtil.getDoubleValue(holidayMap.get("TC"))+ValueUtil.getDoubleValue(map.get("TC")));
					holidayMap.put("PAX", ValueUtil.getDoubleValue(holidayMap.get("PAX"))+ValueUtil.getDoubleValue(map.get("PAX")));
					totalMap1.put("holiday", holidayMap);
				}
			}
		}
		List<FirmItemUse> list = new ArrayList<FirmItemUse>();
		// 循环菜品销售情况
//		Map<String,Map<String,String>> listPcodeMap = getDeptByPubitemCode(firmId);
		for (Map<String, Object> map : pcodeMap) {
			FirmItemUse itemUse = new FirmItemUse();
			itemUse.setFirm(firmId);
			itemUse.setItem(ValueUtil.getStringValue(map.get("VPCODE")));
			itemUse.setItcode(ValueUtil.getStringValue(map.get("VPCODE")));
			itemUse.setItdes(ValueUtil.getStringValue(map.get("VPNAME")));
			itemUse.setItunit(ValueUtil.getStringValue(map.get("VUNIT")));
//			Map<String,String> map1 = listPcodeMap.get(ValueUtil.getStringValue(map.get("VPCODE")));
//			if(map1 != null){//要把部门存进去20160504wjf
//				itemUse.setDept(map1.get("vcode"));
//				itemUse.setDeptdes(map1.get("vname"));
//				itemUse.setDept1(map1.get("dept1"));
//				itemUse.setDept1des(map1.get("dept1des"));
//			}
			//循环菜品周次销售情况
			Double holidayCnt = 0.0;
			for (Map<String, Object> ordrMap : listMap) {
				// 确定菜品是否已经存在
				if(!ordrMap.get("VPCODE").equals(map.get("VPCODE"))){
					continue;
				}else{
					// 确定是否节假日
					if(ValueUtil.getStringValue(ordrMap.get("ISHOLIDAY")).equals("0")){
						double value = ValueUtil.getDoubleValue(getDouble8(ValueUtil.getDoubleValue(ordrMap.get("YCOUNT")),ValueUtil.getDoubleValue(totalMap1.get(ordrMap.get("WEEKDAY")).get(vplantyp)))*1000.0);
						if(!"AMT".equals(vplantyp))
							value = Math.round(value);
						// 不同周次数据 写入对应值
						if(ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().toLowerCase().equals("sunday") || ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().equals("星期日")){
							itemUse.setPrev71(value);
							itemUse.setModv71(itemUse.getPrev71());
							continue;
						}else if(ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().toLowerCase().equals("monday")|| ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().equals("星期一")){
							itemUse.setPrev11(value);
							itemUse.setModv11(itemUse.getPrev11());
							continue;
						}else if(ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().toLowerCase().equals("tuesday")|| ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().equals("星期二")){
							itemUse.setPrev21(value);
							itemUse.setModv21(itemUse.getPrev21());
							continue;
						}else if(ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().toLowerCase().equals("wednesday")|| ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().equals("星期三")){
							itemUse.setPrev31(value);
							itemUse.setModv31(itemUse.getPrev31());
							continue;
						}else if(ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().toLowerCase().equals("thursday")|| ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().equals("星期四")){
							itemUse.setPrev41(value);
							itemUse.setModv41(itemUse.getPrev41());
							continue;
						}else if(ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().toLowerCase().equals("friday")|| ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().equals("星期五")){
							itemUse.setPrev51(value);
							itemUse.setModv51(itemUse.getPrev51());
							continue;
						}else if(ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().toLowerCase().equals("saturday")|| ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().equals("星期六")){
							itemUse.setPrev61(value);
							itemUse.setModv61(itemUse.getPrev61());
							continue;
						}
					}else{
						//节假日存在多天数据，每次累加 重新计算
						holidayCnt+=ValueUtil.getDoubleValue(ordrMap.get("YCOUNT"));
						double value = ValueUtil.getDoubleValue(getDouble8(holidayCnt,ValueUtil.getDoubleValue(totalMap1.get("holiday").get(vplantyp)))*1000.0);
						if(!"AMT".equals(vplantyp))
							value = Math.round(value);
						itemUse.setPrev81(value);
						itemUse.setModv81(itemUse.getPrev81());
					}
				}
			}
			list.add(itemUse);
		}
		FirmItemUse itemUse = new FirmItemUse();
		itemUse.setFirm(firmId);
		itemUseMapper.deleteAll(itemUse);
		for (int i = 0; i < list.size(); i++) {
			itemUseMapper.insertItemUse(list.get(i));
		}
		return "1";
	}
	
    /**
     * 计算保留6位小数
     * @param a
     * @param b
     * @return
     */
    private Double getDouble8(double a, double b) {
        double c = 0.0;
        if (b != 0) {
            c = a / b;
        }
        BigDecimal bi = new BigDecimal(c + "");
        BigDecimal bi2 = bi.setScale(6, BigDecimal.ROUND_HALF_UP);
        return bi2.doubleValue();
    }
	
    /**
     * 查询当前店或者部门下的菜品
     * @param firmId
     * @return
     * @throws CRUDException
     */
    public List<PubItem> addNewItem(String firmId) throws CRUDException{
    	try {
			return itemUseMapper.addNewItem(firmId);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
    }

	/**
	 * 保存修改调整量(菜品点击率)
	 * @param itemUse
	 * @throws CRUDException
	 */
	public void update(FirmItemUse itemUse) throws CRUDException {
		try {
			itemUseMapper.deleteAll(itemUse);
			for (int i = 0; i < itemUse.getItemUseList().size(); i++) {//批量修改
				FirmItemUse item = itemUse.getItemUseList().get(i);
				item.setFirm(itemUse.getFirm());
				itemUseMapper.insertItemUse(item);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 导出excel
	 * @param os
	 * @param list
	 * @return
	 */
	public boolean exportExcel(OutputStream os,List<FirmItemUse> list) {   
		WritableWorkbook workBook = null;
		try {
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet("Sheet1", 0);
            sheet.addCell(new Label(0,0,"菜品点击率"));
            //设置表格表头
            sheet.addCell(new Label(0, 2, "菜品编码"));   
			sheet.addCell(new Label(1, 2, "菜品名称"));  
			sheet.addCell(new Label(2, 2, "单位"));   
			sheet.addCell(new Label(3, 2, "星期一|参考"));  
			sheet.addCell(new Label(4, 2, "星期一|调整"));   
			sheet.addCell(new Label(5, 2, "星期二|参考"));  
			sheet.addCell(new Label(6, 2, "星期二|调整"));   
			sheet.addCell(new Label(7, 2, "星期三|参考"));  
			sheet.addCell(new Label(8, 2, "星期三|调整"));
			sheet.addCell(new Label(9, 2, "星期四|参考"));  
			sheet.addCell(new Label(10, 2, "星期四|调整")); 
			sheet.addCell(new Label(11, 2, "星期五|参考"));  
			sheet.addCell(new Label(12, 2, "星期五|调整"));   
			sheet.addCell(new Label(13, 2, "星期六|参考"));  
			sheet.addCell(new Label(14, 2, "星期六|调整"));   
			sheet.addCell(new Label(15, 2, "星期天|参考"));  
			sheet.addCell(new Label(16, 2, "星期天|调整"));   
			sheet.addCell(new Label(17, 2, "节假日|参考"));
			sheet.addCell(new Label(18, 2, "节假日|调整"));
            sheet.mergeCells(0, 0, 18, 1);
            //遍历list填充表格内容
            for(int i=0;i<list.size();i++) {
            	sheet.addCell(new Label(0, i+3, list.get(i).getItcode()));
            	sheet.addCell(new Label(1, i+3, list.get(i).getItdes()));
            	sheet.addCell(new Label(2, i+3, list.get(i).getItunit()));
            	sheet.addCell(new Label(3, i+3, ValueUtil.getStringValue(list.get(i).getPrev11())));
            	sheet.addCell(new Label(4, i+3, ValueUtil.getStringValue(list.get(i).getModv11())));
	    		sheet.addCell(new Label(5, i+3, ValueUtil.getStringValue(list.get(i).getPrev21())));
	    		sheet.addCell(new Label(6, i+3, ValueUtil.getStringValue(list.get(i).getModv21())));
	    		sheet.addCell(new Label(7, i+3, ValueUtil.getStringValue(list.get(i).getPrev31())));
	    		sheet.addCell(new Label(8, i+3, ValueUtil.getStringValue(list.get(i).getModv31())));
	    		sheet.addCell(new Label(9, i+3, ValueUtil.getStringValue(list.get(i).getPrev41())));
	    		sheet.addCell(new Label(10, i+3, ValueUtil.getStringValue(list.get(i).getModv41())));
	    		sheet.addCell(new Label(11, i+3, ValueUtil.getStringValue(list.get(i).getPrev51())));
	    		sheet.addCell(new Label(12, i+3, ValueUtil.getStringValue(list.get(i).getModv51())));
	    		sheet.addCell(new Label(13, i+3, ValueUtil.getStringValue(list.get(i).getPrev61())));
	    		sheet.addCell(new Label(15, i+3, ValueUtil.getStringValue(list.get(i).getModv61())));
	    		sheet.addCell(new Label(14, i+3, ValueUtil.getStringValue(list.get(i).getPrev71())));
	    		sheet.addCell(new Label(16, i+3, ValueUtil.getStringValue(list.get(i).getModv71())));
	    		sheet.addCell(new Label(17, i+3, ValueUtil.getStringValue(list.get(i).getPrev81())));
	    		sheet.addCell(new Label(18, i+3, ValueUtil.getStringValue(list.get(i).getModv81())));
	    		}
            workBook.write();
            os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
}