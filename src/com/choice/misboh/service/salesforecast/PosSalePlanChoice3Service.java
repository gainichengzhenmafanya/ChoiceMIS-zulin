package com.choice.misboh.service.salesforecast;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.domain.salesforecast.SalePlan;
import com.choice.misboh.domain.store.Store;
import com.choice.misboh.domain.util.Holiday;
import com.choice.misboh.persistence.common.CommonMISBOHMapper;
import com.choice.misboh.persistence.salesforecast.PosSalePlanChoice3Mapper;

@Service
public class PosSalePlanChoice3Service {
	
	@Autowired
	private PosSalePlanChoice3Mapper posSalePlanChoice3Mapper;
	@Autowired
	private CommonMISBOHMapper commonMISBOHMapper;

	private final transient Log log = LogFactory.getLog(PosSalePlanChoice3Service.class);

	
	/********************************************************营业预估start*************************************************/
	
	/**
	 * 查找营业预估
	 * @param posSalePlan
	 * @return
	 * @throws CRUDException
	 */
	public List<SalePlan> findPosSalePlan(SalePlan salePlan) throws CRUDException
	{
		try {
			return posSalePlanChoice3Mapper.findPosSalePlan(salePlan);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 重新计算营业预估
	 * @return
	 * @throws CRUDException
	 */
	public void saveCalPosSalePlan(SalePlan salePlan) throws CRUDException{
		try {
			// 1.获取点击率表达方式（单数，营业额，人数）
			Store store = new Store();
	    	store.setVcode(salePlan.getFirm());
	    	store = commonMISBOHMapper.getStore(store);
			String typ = store.getVplantyp();//AMT   TC    PAX
			if(null == typ || "".equals(typ))
				typ = "PAX";
			
			//2.获取要预估的时间之内的节假日
			List<Holiday> holidayList = commonMISBOHMapper.getHoliday(DateFormat.getStringByDate(salePlan.getStartdate(),"yyyy-MM-dd"),
					DateFormat.getStringByDate(salePlan.getEnddate(),"yyyy-MM-dd"));
			
			//3.获取参考日期的数据
			//非节假日的，周一至周五
			salePlan.setIsHoliday(0+"");
			List<SalePlan> saleList1 = posSalePlanChoice3Mapper.findPosSaleListChoice3(salePlan);
			//节假日的 
			salePlan.setIsHoliday(2+"");
			List<SalePlan> saleList2 = posSalePlanChoice3Mapper.findPosSaleListChoice3(salePlan);
			
			Calendar bcal = Calendar.getInstance();
			Calendar ecal = Calendar.getInstance();
			bcal.setTime(salePlan.getStartdate());
			ecal.setTime(salePlan.getEnddate());
			ecal.add(Calendar.DATE, 1);     // 加一天
			// 按日期循环
			while (!bcal.equals(ecal)) {
				SalePlan sp = new SalePlan();
				sp.setFirm(salePlan.getFirm());
				sp.setDat(DateFormat.formatDate(bcal.getTime(), "yyyy-MM-dd"));
				posSalePlanChoice3Mapper.deletePosSalePlan(sp); // 删除数据库中某日数据
				sp.setMemo("");//特殊事件
				sp.setYnZb("");
				sp.setDept("");
				sp.setIsHoliday("");
				//是否节假日
				boolean b = false;
				for(Holiday h : holidayList){
					if(h.getVholidaygrade() >= 2 && bcal.getTime().getTime() == DateFormat.getDateByString(h.getDholidaydate(), "yyyy-MM-dd").getTime()){//是节假日,且节假日级别>=2
						b = true;
						sp.setIsHoliday(h.getVholidayname()+"");
						double pax2 = 0.0;//午餐
						double pax3 = 0.0;//晚餐
						if(saleList2.size() > 0){//取节假日的平均值
							for(SalePlan p : saleList2){
								if(1 == p.getPax()){//午餐
									if("AMT".equals(typ)){
										pax2 = p.getMoney();
									}else if("TC".equals(typ)){
										pax2 = p.getTc();
									}else if("PAX".equals(typ)){
										pax2 = p.getPeoplenum();
									}
								} else if(2 == p.getPax()){
									if("AMT".equals(typ)){
										pax3 =  p.getMoney();
									}else if("TC".equals(typ)){
										pax3 = p.getTc();
									}else if("PAX".equals(typ)){
										pax3 = p.getPeoplenum();
									}
								}
							}
						}
						sp.setPax2old(pax2);
						sp.setPax2(pax2);
						sp.setPax3old(pax3);
						sp.setPax3(pax3);
					}
				}
				if(!b){//非节假日的
					int w = bcal.get(Calendar.DAY_OF_WEEK);//值为1~7分别代表星期日，星期一，星期二，星期三，星期四，星期五，星期六
					double pax2 = 0.0;//午餐
					double pax3 = 0.0;//晚餐
					if(saleList1.size() > 0){//取平常周一至周五的平均值
						for(SalePlan p : saleList1){
							if(Integer.parseInt(p.getWeek()) == w){
								if(1 == p.getPax()){//午餐
									if("AMT".equals(typ)){
										pax2 =  p.getMoney();
									}else if("TC".equals(typ)){
										pax2 = p.getTc();
									}else if("PAX".equals(typ)){
										pax2 = p.getPeoplenum();
									}
								} else if(2 == p.getPax()){
									if("AMT".equals(typ)){
										pax3 =  p.getMoney();
									}else if("TC".equals(typ)){
										pax3 = p.getTc();
									}else if("PAX".equals(typ)){
										pax3 = p.getPeoplenum();
									}
								}
							}
						}
					}
					sp.setPax2old(pax2);
					sp.setPax2(pax2);
					sp.setPax3old(pax3);
					sp.setPax3(pax3);
				}
				sp.setMoney_y(0.0);
				sp.setMoney_t(0.0);
				sp.setBills_y(0);
				sp.setBills_t(0);
				sp.setPeople_y(0);
				sp.setPeople_t(0);
				if("AMT".equals(typ)){
					sp.setMoney_y( (sp.getPax2()+sp.getPax3()));
					sp.setMoney_t( (sp.getPax2()+sp.getPax3()));
				}else if("TC".equals(typ)){
					sp.setBills_y((int) (sp.getPax2()+sp.getPax3()));
					sp.setBills_t((int) (sp.getPax2()+sp.getPax3()));
				}else if("PAX".equals(typ)){
					sp.setPeople_y((int) (sp.getPax2()+sp.getPax3()));
					sp.setPeople_t((int) (sp.getPax2()+sp.getPax3()));
				}
				posSalePlanChoice3Mapper.insertPosSalePlan(sp);
				bcal.add(Calendar.DATE, 1);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存修改调整量(营业预估)
	 * @param posSalePlan
	 * @throws CRUDException
	 */
	public int updateSalePlan(SalePlan salePlan) throws CRUDException {
		try {
			// 1.获取点击率表达方式（单数，营业额，人数）
			Store store = new Store();
	    	store.setVcode(salePlan.getFirm());
	    	store = commonMISBOHMapper.getStore(store);
			String typ = store.getVplantyp();//AMT   TC    PAX
			if(null == typ || "".equals(typ))
				typ = "PAX";
			for (int i = 0; i < salePlan.getSalePlanList().size(); i++) {//批量修改
				SalePlan SalePlan_=new SalePlan();
				SalePlan_.setFirm(salePlan.getFirm());
				SalePlan_.setDat(salePlan.getSalePlanList().get(i).getDat());
				SalePlan_.setMemo(salePlan.getSalePlanList().get(i).getMemo());
				SalePlan_.setPax1(salePlan.getSalePlanList().get(i).getPax1());
				SalePlan_.setPax2(salePlan.getSalePlanList().get(i).getPax2());
				SalePlan_.setPax3(salePlan.getSalePlanList().get(i).getPax3());
				SalePlan_.setPax4(salePlan.getSalePlanList().get(i).getPax4());
				if("AMT".equals(typ)){
					SalePlan_.setMoney_t( (SalePlan_.getPax2()+SalePlan_.getPax3()));
				}else if("TC".equals(typ)){
					SalePlan_.setBills_t((int) (SalePlan_.getPax2()+SalePlan_.getPax3()));
				}else if("PAX".equals(typ)){
					SalePlan_.setPeople_t((int) (SalePlan_.getPax2()+SalePlan_.getPax3()));
				}
				posSalePlanChoice3Mapper.updateSalePlan(SalePlan_);
			}
			return 1;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	//导出营业预估Excel 
	public boolean exportExcelcast(OutputStream os, SalePlan salePlan, List<SalePlan> list) {   
		WritableWorkbook workBook = null;
		try {
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet(salePlan.getFirmNm()+"营业预估", 0);
            sheet.addCell(new Label(0,0,salePlan.getFirmNm()+"营业预估"));
            //设置表格表头
            sheet.addCell(new Label(0, 2, "分店"));
            sheet.addCell(new Label(1, 2, "日期"));      
            sheet.addCell(new Label(2, 2, "星期"));      
            sheet.addCell(new Label(3, 2, "假日"));      
            sheet.addCell(new Label(4, 2, "特殊事件"));  
            sheet.addCell(new Label(5, 2, "午餐|预估")); 
            sheet.addCell(new Label(6, 2, "午餐|调整")); 
            sheet.addCell(new Label(7, 2, "晚餐|预估")); 
            sheet.addCell(new Label(8, 2, "晚餐|调整")); 
            sheet.addCell(new Label(9, 2, "合计|预估"));
            sheet.addCell(new Label(10, 2, "合计|调整"));

            sheet.mergeCells(0, 0, 10, 1);
            SimpleDateFormat sdf =  new SimpleDateFormat("yyyy-MM-dd"); 
            //遍历list填充表格内容
            for(int i=0;i<list.size();i++) {
            	sheet.addCell(new Label(0, i+3, list.get(i).getFirmNm())); 
            	sheet.addCell(new Label(1, i+3, String.valueOf(sdf.format(list.get(i).getDat()))));                  
            	sheet.addCell(new Label(2, i+3, list.get(i).getWeek()));                   
            	sheet.addCell(new Label(3, i+3, null == list.get(i).getWeek()?"":list.get(i).getWeek()));                                      
            	sheet.addCell(new Label(4, i+3, null == list.get(i).getIsHoliday()?"":list.get(i).getIsHoliday()));                                      
            	sheet.addCell(new Label(5, i+3, String.valueOf(list.get(i).getPax2old())));
            	sheet.addCell(new Label(6, i+3, String.valueOf(list.get(i).getPax2())));   
            	sheet.addCell(new Label(7, i+3, String.valueOf(list.get(i).getPax3old())));    
            	sheet.addCell(new Label(8, i+3, String.valueOf(list.get(i).getPax3())));       
            	sheet.addCell(new Label(9, i+3, String.valueOf(list.get(i).getTotalold())));  
            	sheet.addCell(new Label(10, i+3, String.valueOf(list.get(i).getTotal())));     
	    		}
            workBook.write();
            os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	/********************************************************营业预估end*************************************************/
	
}