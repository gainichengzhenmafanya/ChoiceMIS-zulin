package com.choice.misboh.service.salesforecast;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.misboh.domain.salesforecast.SpCodeUseMis;
import com.choice.misboh.persistence.salesforecast.SpCodeUseMisMapper;

@Service
public class SpCodeUseMisService {
	
	@Autowired
	private SpCodeUseMisMapper spCodeUseMisMapper;
	
	private final transient Log log = LogFactory.getLog(SpCodeUseMisService.class);
	
	
	/**
	 * 查找千人万元用量预估
	 * @return
	 * @throws CRUDException
	 */
	public List<SpCodeUseMis> listSpCodeUse(SpCodeUseMis spCodeUse) throws CRUDException
	{
		try {
			return spCodeUseMisMapper.listSpCodeUse(spCodeUse);	
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
		
	/**
	 * 查询营业额
	 * @return
	 * @throws CRUDException
	 */
	public SpCodeUseMis getAmt(SpCodeUseMis spCodeUse) throws CRUDException
	{
		try {
			return spCodeUseMisMapper.getAmt(spCodeUse);	
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	public SpCodeUseMis getAmt_pos(SpCodeUseMis spCodeUse) throws CRUDException
	{
		try {
			return spCodeUseMisMapper.getAmt_pos(spCodeUse);	
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	public SpCodeUseMis getAmt_boh(SpCodeUseMis spCodeUse) throws CRUDException
	{
		try {
			return spCodeUseMisMapper.getAmt_boh(spCodeUse);	
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * choice3查营业数据
	 * @param spCodeUse
	 * @return
	 * @throws CRUDException
	 */
	public SpCodeUseMis getAmtChoice3(SpCodeUseMis spCodeUse) throws CRUDException
	{
		try {
			return spCodeUseMisMapper.getAmtChoice3(spCodeUse);	
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除所有菜品点击率
	 * @param
	 * @throws CRUDException
	 */
	public void deleteAll(SpCodeUseMis spCodeUse) throws CRUDException{
		try{
			spCodeUseMisMapper.deleteAll(spCodeUse);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 重新计算物资用量===多部门
	 * @return
	 * @throws CRUDException
	 */
	public List<SpCodeUseMis> calculateDept(SpCodeUseMis spCodeUse) throws CRUDException
	{
		try {
			//查找预估物资的使用量信息
			return spCodeUseMisMapper.listSupplyAcctDept(spCodeUse);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 重新计算物资用量
	 * @return
	 * @throws CRUDException
	 */
	public List<SpCodeUseMis> calculate(SpCodeUseMis spCodeUse) throws CRUDException
	{
		try {
			//查找预估物资的使用量信息
			return spCodeUseMisMapper.listSupplyAcct(spCodeUse);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存修改调整量
	 * @param spCodeUse
	 * @throws CRUDException
	 */
	public void update(SpCodeUseMis spCodeUse) throws CRUDException {
		try {
			for (int i = 0; i < spCodeUse.getSpCodeUseList().size(); i++) {//批量修改
				SpCodeUseMis scum = spCodeUse.getSpCodeUseList().get(i);
				scum.setFirm(spCodeUse.getFirm());
				if(spCodeUse.getLv() == 0 && "Y".equals(scum.getChange())){//如果不是重新计算的 并且修改过的才更新
					int count = spCodeUseMisMapper.updateSpCodeUse(scum);
					if(count == 0)
						spCodeUseMisMapper.insertSpCodeUse(scum);
				}
			}
			if(spCodeUse.getLv() != 0){//如果是重新计算的就全删掉 按重新计算的
				spCodeUseMisMapper.deleteAll(spCodeUse);
				spCodeUseMisMapper.insertAll(spCodeUse.getSpCodeUseList());
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
}