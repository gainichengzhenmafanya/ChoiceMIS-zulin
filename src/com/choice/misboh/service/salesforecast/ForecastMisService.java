package com.choice.misboh.service.salesforecast;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import net.sf.json.JSONArray;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.domain.BaseRecord.PubItem;
import com.choice.misboh.domain.salesforecast.FirmItemUse;
import com.choice.misboh.domain.salesforecast.ItemPlan;
import com.choice.misboh.domain.salesforecast.SalePlan;
import com.choice.misboh.domain.salesforecast.SalesForecastEnum;
import com.choice.misboh.domain.store.Store;
import com.choice.misboh.domain.util.Holiday;
import com.choice.misboh.persistence.salesforecast.ForecastMisMapper;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;

@Service
public class ForecastMisService {
	
	@Autowired
	private ForecastMisMapper forecastMisMapper;
	@Autowired
	private PageManager<FirmItemUse> pageManager;
	@Autowired
	private PageManager<SalePlan> pageManager1;
	@Autowired
	private PageManager<ItemPlan> pageManager2;
	@Autowired
	private CommonMISBOHService commonMISBOHService;

	private final transient Log log = LogFactory.getLog(ForecastMisService.class);

	private String driver = "";
	private String url = "";
	private String uname = "";
	private String password = "";
	
	/**
	 * 销售数据库连接获取（决策，总部boh，老总部快餐）
	 * @return
	 * @throws CRUDException
	 */
	public void getJdbcConn() throws CRUDException, IOException {
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("jdbc.properties");  
		Properties properties = new Properties();
		properties.load(inputStream);
		driver = properties.getProperty("jdbc.driver_boh");
		url = properties.getProperty("jdbc.url_boh");
		uname = properties.getProperty("jdbc.username_boh");
		password = properties.getProperty("jdbc.password_boh");
	}
	
	/**
	 * 物流库连接获取
	 * @return
	 * @throws CRUDException
	 */
	public void getJdbcConn_scm() throws CRUDException, IOException {
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("jdbc.properties");  
		Properties properties = new Properties();
		properties.load(inputStream);
		driver = properties.getProperty("jdbc.driver");
		url = properties.getProperty("jdbc.url");
		uname = properties.getProperty("jdbc.username");
		password = properties.getProperty("jdbc.password");
	}
	
	/**
	 * 获取预估方式
	 * @param firmId
	 * @param acct
	 * @return 王超
	 * @throws CRUDException
	 */
	public String getYuGuFangShi(String firmId,String acct)throws CRUDException{
		try {
			Store store = new Store();
			store.setVcode(firmId);
			store.setPk_org(acct);
			return ValueUtil.getStringValue(commonMISBOHService.getStore(store).getVplantyp());
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/********************************************************菜品点击率start*************************************************/
	/**
	 * 查找所有的菜品点击率
	 * @return
	 * @throws CRUDException
	 */
	public List<FirmItemUse> findAllItemUse(FirmItemUse itemUse, Page page) throws CRUDException
	{
		try {
			return pageManager.selectPage(itemUse, page, ForecastMisMapper.class.getName()+".findAllItemUse");	
			//return forecastMisMapper.findAllItemUse();			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查找所有的菜品点击率
	 * @return
	 * @throws CRUDException
	 */
	public List<FirmItemUse> findAllItemUse(FirmItemUse itemUse) throws CRUDException
	{
		try {
			return forecastMisMapper.findAllItemUse(itemUse);			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除所有菜品点击率
	 * @param
	 * @throws CRUDException
	 */
	public void deleteAll(FirmItemUse itemUse) throws CRUDException{
		try{
			forecastMisMapper.deleteAll(itemUse);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 计算菜品点击率
	 * @author 文清泉 2015年5月27日 上午10:53:47
	 * @param firmId
	 * @param bdate
	 * @param edate
	 * @throws CRUDException 
	 */
	public void saveNewCalculateItemuse(String firmId, String acct,
			Date bdate, Date edate, Store store) throws CRUDException {
		List<Map<String,Object>> totalMap = new ArrayList<Map<String,Object>>();
		List<Map<String,Object>> listMap = new ArrayList<Map<String,Object>>();
		List<Map<String,Object>> pcodeMap = new ArrayList<Map<String,Object>>();
		if(null != store.getVfoodsign() && "2".equals(store.getVfoodsign())){
			// 读取总和
			totalMap = forecastMisMapper.getCalculateItemuseTotal(firmId,DateJudge.YYYY_MM_DD.format(bdate),DateJudge.YYYY_MM_DD.format(edate));
			// 读取菜品销售情况
			listMap = forecastMisMapper.getCalculateItemuse(firmId,DateJudge.YYYY_MM_DD.format(bdate),DateJudge.YYYY_MM_DD.format(edate));
			//读取所以菜品编码与单位
			pcodeMap = forecastMisMapper.getCalculatePcode(firmId,DateJudge.YYYY_MM_DD.format(bdate),DateJudge.YYYY_MM_DD.format(edate));
		} else {
			// 读取总和
			totalMap = forecastMisMapper.getCalculateItemuseTotal3(firmId,DateJudge.YYYY_MM_DD.format(bdate),DateJudge.YYYY_MM_DD.format(edate));
			// 读取菜品销售情况
			listMap = forecastMisMapper.getCalculateItemuse3(firmId,DateJudge.YYYY_MM_DD.format(bdate),DateJudge.YYYY_MM_DD.format(edate));
			//读取所以菜品编码与单位
			pcodeMap = forecastMisMapper.getCalculatePcode3(firmId,DateJudge.YYYY_MM_DD.format(bdate),DateJudge.YYYY_MM_DD.format(edate));
		}
		
		Map<Object,Map<String,Object>> totalMap1 = new HashMap<Object,Map<String,Object>>();
		String YGFS = getYuGuFangShi(firmId, acct);
		// 循环库中读取合计值，将节假日数据汇总
		for (Map<String, Object> map : totalMap) {
			if(ValueUtil.getStringValue(map.get("ISHOLIDAY")).equals("0")){
				totalMap1.put(map.get("WEEKDAY"), map);
			}else{
				// 确定是否第一次初始化
				if(totalMap1.get("holiday")==null){
					map.put("WEEKDAY", "holiday");
					totalMap1.put("holiday", map);
				}else{
					Map<String,Object> holidayMap = totalMap1.get("holiday");
					holidayMap.put("MONEY", ValueUtil.getDoubleValue(holidayMap.get("MONEY"))+ValueUtil.getDoubleValue(map.get("MONEY")));
					holidayMap.put("TC", ValueUtil.getDoubleValue(holidayMap.get("TC"))+ValueUtil.getDoubleValue(map.get("TC")));
					holidayMap.put("PEOLENUM", ValueUtil.getDoubleValue(holidayMap.get("PEOLENUM"))+ValueUtil.getDoubleValue(map.get("PEOLENUM")));
					totalMap1.put("holiday", holidayMap);
				}
			}
		}
		List<FirmItemUse> list = new ArrayList<FirmItemUse>();
		String totalKey = "money";
		if (YGFS == null || "".equals(YGFS) || YGFS.equals("AMT")) {
			totalKey = "MONEY";
		} else if (YGFS.equals("PAX")) {
			totalKey = "PEOLENUM";
		} else if (YGFS.equals("TC")) {
			totalKey = "TC";
		}
		// 循环菜品销售情况
		Map<String,Map<String,String>> listPcodeMap = getDeptByPubitemCode(firmId);
		for (Map<String, Object> map : pcodeMap) {
			FirmItemUse itemUse = new FirmItemUse();
			itemUse.setFirm(firmId);
			itemUse.setItem(ValueUtil.getStringValue(map.get("VPCODE")));
			itemUse.setItcode(ValueUtil.getStringValue(map.get("VPCODE")));
			itemUse.setItdes(ValueUtil.getStringValue(map.get("VPNAME")));
			itemUse.setItunit(ValueUtil.getStringValue(map.get("VUNIT")));
			Map<String,String> map1 = listPcodeMap.get(ValueUtil.getStringValue(map.get("VPCODE")));
			if(map1 != null){//要把部门存进去20160504wjf
				itemUse.setDept(map1.get("vcode"));
				itemUse.setDeptdes(map1.get("vname"));
				itemUse.setDept1(map1.get("dept1"));
				itemUse.setDept1des(map1.get("dept1des"));
			}
			//循环菜品周次销售情况
			Double holidayCnt = 0.0;
			for (Map<String, Object> ordrMap : listMap) {
				// 确定菜品是否已经存在
				if(!ordrMap.get("VPCODE").equals(map.get("VPCODE"))){
					continue;
				}else{
					// 确定是否节假日
					if(ValueUtil.getStringValue(ordrMap.get("ISHOLIDAY")).equals("0")){
						double value = ValueUtil.getDoubleValue(getDouble8(ValueUtil.getDoubleValue(ordrMap.get("YCOUNT")),ValueUtil.getDoubleValue(totalMap1.get(ordrMap.get("WEEKDAY")).get(totalKey)))*1000.0);
						if(!"MONEY".equals(totalKey))
							value = Math.round(value);
						// 不同周次数据 写入对应值
						if(ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().toLowerCase().equals("sunday") || ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().equals("星期日")){
							itemUse.setPrev71(value);
							itemUse.setModv71(itemUse.getPrev71());
							continue;
						}else if(ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().toLowerCase().equals("monday")|| ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().equals("星期一")){
							itemUse.setPrev11(value);
							itemUse.setModv11(itemUse.getPrev11());
							continue;
						}else if(ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().toLowerCase().equals("tuesday")|| ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().equals("星期二")){
							itemUse.setPrev21(value);
							itemUse.setModv21(itemUse.getPrev21());
							continue;
						}else if(ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().toLowerCase().equals("wednesday")|| ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().equals("星期三")){
							itemUse.setPrev31(value);
							itemUse.setModv31(itemUse.getPrev31());
							continue;
						}else if(ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().toLowerCase().equals("thursday")|| ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().equals("星期四")){
							itemUse.setPrev41(value);
							itemUse.setModv41(itemUse.getPrev41());
							continue;
						}else if(ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().toLowerCase().equals("friday")|| ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().equals("星期五")){
							itemUse.setPrev51(value);
							itemUse.setModv51(itemUse.getPrev51());
							continue;
						}else if(ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().toLowerCase().equals("saturday")|| ValueUtil.getStringValue(ordrMap.get("WEEKDAY")).trim().equals("星期六")){
							itemUse.setPrev61(value);
							itemUse.setModv61(itemUse.getPrev61());
							continue;
						}
					}else{
						//节假日存在多天数据，每次累加 重新计算
						holidayCnt+=ValueUtil.getDoubleValue(ordrMap.get("YCOUNT"));
						double value = ValueUtil.getDoubleValue(getDouble8(holidayCnt,ValueUtil.getDoubleValue(totalMap1.get("holiday").get(totalKey)))*1000.0);
						if(!"MONEY".equals(totalKey))
							value = Math.round(value);
						itemUse.setPrev81(value);
						itemUse.setModv81(itemUse.getPrev81());
					}
				}
			}
			list.add(itemUse);
		}
//		if(list!=null && list.size()>0){
			FirmItemUse itemUse = new FirmItemUse();
			itemUse.setFirm(firmId);
			forecastMisMapper.deleteAll(itemUse);
			for (int i = 0; i < list.size(); i++) {
				forecastMisMapper.insertItemUse(list.get(i));
			}
//		}
	}
	
    /**
     * 计算保留6位小数
     *
     * @param a
     * @param b
     * @return
     */
    private Double getDouble8(double a, double b) {
        double c = 0.0;
        if (b != 0) {
            c = a / b;
        }
        BigDecimal bi = new BigDecimal(c + "");
        BigDecimal bi2 = bi.setScale(6, BigDecimal.ROUND_HALF_UP);
        return bi2.doubleValue();
    }
	
    /**
     * 查询当前店或者部门下的菜品
     * @param firmId
     * @return
     * @throws CRUDException
     */
    public List<PubItem> addNewItem(String firmId)throws CRUDException{
    	try {
			return forecastMisMapper.addNewItem(firmId);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
    }

	
	/**
	 * 保存修改调整量(菜品点击率)
	 * @param itemUse
	 * @throws CRUDException
	 */
	public void update(FirmItemUse itemUse) throws CRUDException {
		try {
			forecastMisMapper.deleteAll(itemUse);
			for (int i = 0; i < itemUse.getItemUseList().size(); i++) {//批量修改
				FirmItemUse item = itemUse.getItemUseList().get(i);
				item.setFirm(itemUse.getFirm());
				forecastMisMapper.insertItemUse(item);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	//导出菜品点击率Excel 
	public boolean exportExcel(OutputStream os,List<FirmItemUse> list) {   
		WritableWorkbook workBook = null;
		try {
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet("Sheet1", 0);
            sheet.addCell(new Label(0,0,"菜品点击率"));
            //设置表格表头
            sheet.addCell(new Label(0, 2, "菜品编码"));   
			sheet.addCell(new Label(1, 2, "菜品名称"));  
			sheet.addCell(new Label(2, 2, "单位"));   
			sheet.addCell(new Label(3, 2, "星期一|参考"));  
			sheet.addCell(new Label(4, 2, "星期一|调整"));   
			sheet.addCell(new Label(5, 2, "星期二|参考"));  
			sheet.addCell(new Label(6, 2, "星期二|调整"));   
			sheet.addCell(new Label(7, 2, "星期三|参考"));  
			sheet.addCell(new Label(8, 2, "星期三|调整"));
			sheet.addCell(new Label(9, 2, "星期四|参考"));  
			sheet.addCell(new Label(10, 2, "星期四|调整")); 
			sheet.addCell(new Label(11, 2, "星期五|参考"));  
			sheet.addCell(new Label(12, 2, "星期五|调整"));   
			sheet.addCell(new Label(13, 2, "星期六|参考"));  
			sheet.addCell(new Label(14, 2, "星期六|调整"));   
			sheet.addCell(new Label(15, 2, "星期天|参考"));  
			sheet.addCell(new Label(16, 2, "星期天|调整"));   
			sheet.addCell(new Label(17, 2, "节假日|参考"));
			sheet.addCell(new Label(18, 2, "节假日|调整"));
            sheet.mergeCells(0, 0, 18, 1);
            //遍历list填充表格内容
            for(int i=0;i<list.size();i++) {
            	sheet.addCell(new Label(0, i+3, list.get(i).getItcode()));
            	sheet.addCell(new Label(1, i+3, list.get(i).getItdes()));
            	sheet.addCell(new Label(2, i+3, list.get(i).getItunit()));
            	sheet.addCell(new Label(3, i+3, ValueUtil.getStringValue(list.get(i).getPrev11())));
            	sheet.addCell(new Label(4, i+3, ValueUtil.getStringValue(list.get(i).getModv11())));
	    		sheet.addCell(new Label(5, i+3, ValueUtil.getStringValue(list.get(i).getPrev21())));
	    		sheet.addCell(new Label(6, i+3, ValueUtil.getStringValue(list.get(i).getModv21())));
	    		sheet.addCell(new Label(7, i+3, ValueUtil.getStringValue(list.get(i).getPrev31())));
	    		sheet.addCell(new Label(8, i+3, ValueUtil.getStringValue(list.get(i).getModv31())));
	    		sheet.addCell(new Label(9, i+3, ValueUtil.getStringValue(list.get(i).getPrev41())));
	    		sheet.addCell(new Label(10, i+3, ValueUtil.getStringValue(list.get(i).getModv41())));
	    		sheet.addCell(new Label(11, i+3, ValueUtil.getStringValue(list.get(i).getPrev51())));
	    		sheet.addCell(new Label(12, i+3, ValueUtil.getStringValue(list.get(i).getModv51())));
	    		sheet.addCell(new Label(13, i+3, ValueUtil.getStringValue(list.get(i).getPrev61())));
	    		sheet.addCell(new Label(15, i+3, ValueUtil.getStringValue(list.get(i).getModv61())));
	    		sheet.addCell(new Label(14, i+3, ValueUtil.getStringValue(list.get(i).getPrev71())));
	    		sheet.addCell(new Label(16, i+3, ValueUtil.getStringValue(list.get(i).getModv71())));
	    		sheet.addCell(new Label(17, i+3, ValueUtil.getStringValue(list.get(i).getPrev81())));
	    		sheet.addCell(new Label(18, i+3, ValueUtil.getStringValue(list.get(i).getModv81())));
	    		}
            workBook.write();
            os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	
	/********************************************************菜品点击率end*************************************************/
	
	/********************************************************营业预估start*************************************************/
	
	/**
	 * 查找营业预估
	 * @param posSalePlan
	 * @return
	 * @throws CRUDException
	 */
	public List<SalePlan> findPosSalePlan(SalePlan posSalePlan, Page page) throws CRUDException
	{
		try {
			return pageManager1.selectPage(posSalePlan, page, ForecastMisMapper.class.getName()+".findPosSalePlan");	
			//return forecastMisMapper.findPosSalePlan(posSalePlan);			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查找营业预估
	 * @param posSalePlan
	 * @return
	 * @throws CRUDException
	 */
	public List<SalePlan> findPosSalePlan(SalePlan posSalePlan) throws CRUDException {
		try {
			List<SalePlan> listSalePlan = forecastMisMapper.findPosSalePlan(posSalePlan);
			if (listSalePlan.size() <= 0) {
				return null;			
			}
			String nowDate = DateJudge.YYYY_MM_DD.format(new Date());
			double last_totalamt = 0d;//去年同期营业额合计
			double totalamt_y = 0d;//预估营业额合计
			double totalamt_t = 0d;//调整营业额合计
			double last_totalpercapita = 0d;//去年同期人均合计
			double totalpercapita_y = 0d;//预估人均合计
			double totalpercapita_t = 0d;//调整人均合计
			int last_totalpeople = 0;//去年同期人数合计
			int totalpeople_y = 0;//预估人数合计
			int totalpeople_t = 0;//调整人数合计
			int last_totaltc = 0;//去年同期单数合计
			int totaltc_y = 0;//预估单数合计
			int totaltc_t = 0;//调整单数合计
			double last_totalac = 0d;//去年同期单均合计
			double totalac_y = 0d;//预估单均合计
			double totalac_t = 0d;//调整但均合计
			for (int i = 0;i < listSalePlan.size(); i++) {
				SalePlan salePlan = listSalePlan.get(i);
				if (DateJudge.timeCompare(DateJudge.YYYY_MM_DD.format(salePlan.getDat()), nowDate)) {
					salePlan.setTrueorfalse(1);
				} else {
					salePlan.setTrueorfalse(0);
				}
				//周次
				salePlan.setWeek(DateJudge.getWeekOfDateToString(DateJudge.getStringByDate(salePlan.getDat(), "yyyy-MM-dd")));
				if ("1".equals(salePlan.getIsHoliday())) {
					salePlan.setIsHoliday("是");
				} else {
					salePlan.setIsHoliday("否");
				}
				last_totalamt += ValueUtil.getDoubleValue(salePlan.getLast_money());
				totalamt_y += ValueUtil.getDoubleValue(salePlan.getMoney_y());
				totalamt_t += ValueUtil.getDoubleValue(salePlan.getMoney_t());
				last_totalpercapita += ValueUtil.getDoubleValue(salePlan.getLast_percapita());
				totalpercapita_y += ValueUtil.getDoubleValue(salePlan.getPercapita_y());
				totalpercapita_t += ValueUtil.getDoubleValue(salePlan.getPercapita_t());
				last_totalpeople += ValueUtil.getIntValue(salePlan.getLast_people());
				totalpeople_y += salePlan.getPeople_y();
				totalpeople_t += salePlan.getPeople_t();
				last_totaltc += ValueUtil.getIntValue(salePlan.getLast_bills());
				totaltc_y += salePlan.getBills_y();
				totaltc_t += salePlan.getBills_t();
				last_totalac += ValueUtil.getDoubleValue(salePlan.getLast_ac());
				totalac_y += ValueUtil.getDoubleValue(salePlan.getAc_y());
				totalac_t += ValueUtil.getDoubleValue(salePlan.getAc_t());
			}
			SalePlan psp = new SalePlan();
			psp.setLast_money(last_totalamt);
			psp.setMoney_y(totalamt_y);
			psp.setMoney_t(totalamt_t);
			psp.setLast_percapita(last_totalpercapita);
			psp.setPercapita_t(totalpercapita_t);
			psp.setPercapita_y(totalpercapita_y);
			psp.setLast_people(last_totalpeople);
			psp.setPeople_y(totalpeople_y);
			psp.setPeople_t(totalpeople_t);
			psp.setLast_ac(last_totalac);
			psp.setAc_y(totalac_y);
			psp.setAc_t(totalac_t);
			psp.setLast_bills(last_totaltc);
			psp.setBills_t(totaltc_t);
			psp.setBills_y(totaltc_y);
			listSalePlan.add(psp);
			return listSalePlan;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 重新计算营业预估
	 * @return
	 * @throws CRUDException
	 */
	public void saveCalPosSalePlan(SalePlan salePlan) throws CRUDException{
//		String firmId, String firmName, String acct, Date bdate, Date edate, String year , String month, Store store
		String firmId = salePlan.getFirm();
		String firmName = salePlan.getFirmNm();
		Date bdate = salePlan.getBdate();
		Date edate = salePlan.getEdate();
		Store store = new Store();
    	store.setVcode(firmId);
    	store = commonMISBOHService.getStore(store);
		PreparedStatement ptmt = null;
		Connection conn=null;   
		try {
			getJdbcConn();
			StringBuffer sql = new StringBuffer();
			String dat1 = DateJudge.getStringByDate(bdate, "yyyy-MM-dd");
			String dat2 = DateJudge.getStringByDate(edate, "yyyy-MM-dd");
				
			if(null != store.getVfoodsign() && "2".equals(store.getVfoodsign())){
				//查询参考日期数据
				sql.append("\n select sum(case when b.vorclass in ('1', '6') then b.nmoney when b.vorclass in ('2', '3') then -b.nmoney else 0 end) as money,");
			    sql.append("\n sum(case when b.vorclass not in ('1','6') or b.icanrefound = 0 then 0 else 1 end)as tc,");
				sql.append("\n ifnull(sum(case when b.vorclass in ('2') and b.ipeolenum > 0 then -b.ipeolenum when b.vorclass in ('3', '4', '5', '7') then 0 else b.ipeolenum end), 0) as peolenum,");
				sql.append("\n (select count(vholidayname) from cboh_holidaydefine_3ch where vholidayGrade >= 2 and dholidaydate = b.dworkdate) as isholiday,");
				sql.append("\n b.dworkdate as dat from cboh_folio_3ch b  ");
				sql.append("\n where b.dworkdate >=  '"+ dat1 +"'  and b.dworkdate <=  '"+ dat2 +"' and b.vscode = '"+ firmId +"'");
				sql.append("\n group by b.dworkdate order by  b.dworkdate");
			
			} else {
				sql.append("\n select SUM(b.NYMONEY + b.NSVCITEM + b.NSVCCHG + b.NROOMAMT + b.NTAXITEM) as money,");
			    sql.append("\n COUNT(b.VTABLENUM) as tc,");
				sql.append("\n SUM(b.IPEOLENUM) as peolenum,");
				sql.append("\n (select count(vholidayname) from cboh_holidaydefine_3ch where vholidayGrade >= 2 and dholidaydate = b.dworkdate) as isholiday,");
				sql.append("\n b.dworkdate as dat from cboh_folio_3ch b  ");
				sql.append("\n where b.VORCLASS = 'C' and b.dworkdate >=  '"+ dat1 +"'  and b.dworkdate <=  '"+ dat2 +"' and b.vscode = '"+ firmId +"'");
				sql.append("\n group by b.dworkdate order by  b.dworkdate");
			}
			Class.forName(driver);  
			conn=DriverManager.getConnection(url, uname, password);
			System.out.println(sql);
			ptmt=conn.prepareStatement(ValueUtil.getStringValue(sql));
			ResultSet rs = ptmt.executeQuery();
			Map<String, Map<SalesForecastEnum, Object>> map = new HashMap<String, Map<SalesForecastEnum, Object>>();
			while (rs.next()) {
				Map<SalesForecastEnum, Object> rowMap = new HashMap<SalesForecastEnum, Object>();
				rowMap.put(SalesForecastEnum.HOLIDAY,rs.getString("isholiday"));
				rowMap.put(SalesForecastEnum.MONEY,rs.getString("money"));
				rowMap.put(SalesForecastEnum.TC,rs.getString("tc"));
				rowMap.put(SalesForecastEnum.PEOLENUM,rs.getString("peolenum"));
				rowMap.put(SalesForecastEnum.WEEKDAY,DateJudge.getWeekOfDateToString(rs.getString("dat")));
				map.put(DateJudge.getStringByDate(rs.getDate("dat"), "yyyy-MM-dd"),rowMap);
			}
			rs.close();
			Map<String,Object> totalMap = new HashMap<String,Object>(); // 营业额
		    Map<String,Object> tcMap = new HashMap<String,Object>();// TC
		    Map<String,Object> peolenumMap = new HashMap<String,Object>(); // 人数
		    Map<SalesForecastEnum,Object> rowMap = null;//集合对象
		    Map<String,Integer> countMap = new HashMap<String,Integer>();
		    Object week = null; // 周次
		    Object isHoliday = "0"; // 判断是否节假日
		    Object total = "0";// 金额
		    Object tc = "0";//单数
		    Object peolenum = "0";//人数
		    if(map != null && map.size() > 0){// 判断是否存在参考数据
				for (String key : map.keySet()) {
				    rowMap = map.get(key);
				    if(rowMap != null){//获取行
						week = getMapValue(rowMap,SalesForecastEnum.WEEKDAY);
						isHoliday = getMapValue(rowMap,SalesForecastEnum.HOLIDAY);
						total = getMapValue(rowMap,SalesForecastEnum.MONEY);
						tc = getMapValue(rowMap,SalesForecastEnum.TC);
						peolenum = getMapValue(rowMap,SalesForecastEnum.PEOLENUM);
						// 判断是否节假日
						if(isHoliday.equals("0")){
						    if(ValueUtil.getDoubleValue(total) > 0){
								sumMapValue(totalMap,week,1,total);
								totalCountMap(countMap,week+"_money");
						    }
						    if(ValueUtil.getIntValue(tc) > 0){
								sumMapValue(tcMap,week,2,tc);
								totalCountMap(countMap,week+"_tc");
						    }
						    if(ValueUtil.getIntValue(peolenum) > 0){
								sumMapValue(peolenumMap,week,2,peolenum);
								totalCountMap(countMap,week+"_peolenum");
						    }
						    
						}else {
						    totalMap.put("holiday",ValueUtil.getDoubleValue(total));
						    tcMap.put("holiday",ValueUtil.getIntValue(tc));
						    peolenumMap.put("holiday",ValueUtil.getIntValue(peolenum));
						}
				    }   
				}
		    }
			// 计算最终每个周次预估结果
		    calculationForecast(totalMap,countMap,"_money",1);
		    calculationForecast(tcMap,countMap,"_tc",2);
		    calculationForecast(peolenumMap,countMap,"_peolenum",2);
		    sql = new StringBuffer();
		    Calendar bcal = Calendar.getInstance();
			Calendar ecal = Calendar.getInstance();
			bcal.setTime(salePlan.getStartdate());
			ecal.setTime(salePlan.getEnddate());
			ecal.add(Calendar.DATE, 1);     // 加一天
			// 按日期循环
			while (!bcal.equals(ecal)) {
				String date = DateJudge.getStringByDate(bcal.getTime(),"yyyy-MM-dd");
				String lastDate = DateJudge.getLastYearDate(date);
				if(null != store.getVfoodsign() && "2".equals(store.getVfoodsign())){
					sql.append("\n select '"+date+"' as dat,");
					sql.append("\n (select count(vholidayname) from cboh_holidaydefine_3ch where vholidayGrade >= 2 and dholidaydate = '"+date+"') as isholiday,");
					sql.append("\n (select count(vholidayname) from cboh_holidaydefine_3ch where dholidaydate = '"+date+"') as showholiday, ");
					sql.append("\n (select ifnull(sum(case when b.vorclass in ('1', '6') then b.nmoney when b.vorclass in ('2', '3') then -b.nmoney else 0 end),0) ");
					sql.append("\n from cboh_folio_3ch b where b.vscode = '"+ firmId +"' and b.dworkdate = '"+lastDate+"' ) as money, ");
					sql.append("\n (select ifnull(sum(case when b.vorclass not in ('1', '6') or b.icanrefound = 0 then 0 else 1 end),0) ");
					sql.append("\n from cboh_folio_3ch b where b.vscode = '"+ firmId +"' and b.dworkdate = '"+lastDate+"' ) as tc, ");
					sql.append("\n (select ifnull(sum(case when b.vorclass in ('2') and b.ipeolenum > 0 then -b.ipeolenum when b.vorclass in ('3', '4', '5', '7') then 0 else b.ipeolenum end),0) ");
					sql.append("\n from cboh_folio_3ch b where b.vscode = '"+ firmId +"' and b.dworkdate = '"+lastDate+"' ) as peoplenum");
					sql.append("\n union ");
				} else {
					sql.append("\n select '"+date+"' as dat,");
					sql.append("\n (select count(vholidayname) from cboh_holidaydefine_3ch where vholidayGrade >= 2 and dholidaydate = '"+date+"') as isholiday,");
					sql.append("\n (select count(vholidayname) from cboh_holidaydefine_3ch where dholidaydate = '"+date+"') as showholiday, ");
					sql.append("\n (select ifnull(SUM(b.NYMONEY + b.NSVCITEM + b.NSVCCHG + b.NROOMAMT + b.NTAXITEM),0) ");
					sql.append("\n from cboh_folio_3ch b where b.VORCLASS = 'C' and b.vscode = '"+ firmId +"' and b.dworkdate = '"+lastDate+"' ) as money, ");
					sql.append("\n (select ifnull(COUNT(b.VTABLENUM),0) ");
					sql.append("\n from cboh_folio_3ch b where b.VORCLASS = 'C' and b.vscode = '"+ firmId +"' and b.dworkdate = '"+lastDate+"' ) as tc, ");
					sql.append("\n (select ifnull(SUM(b.IPEOLENUM),0) ");
					sql.append("\n from cboh_folio_3ch b where b.VORCLASS = 'C' and b.vscode = '"+ firmId +"' and b.dworkdate = '"+lastDate+"' ) as peoplenum");
					sql.append("\n union ");
				}
				bcal.add(Calendar.DATE, 1);
			}
		    List<SalePlan> listData = new ArrayList<SalePlan>();
		    PreparedStatement ptmtData=conn.prepareStatement(ValueUtil.getStringValue(sql.substring(0,sql.lastIndexOf("union"))));
			ResultSet rsData = ptmtData.executeQuery();
		    while (rsData.next()) {
		    	SalePlan p = new SalePlan();
		    	// 去年同期  - 营业额
				Double moneyData = rsData.getDouble("money");
				// 去年同期  -单数
				Integer tcData = rsData.getInt("tc");
				// 去年同期  - 人数
				Integer peopleData = rsData.getInt("peoplenum");
				
				Double money_y = 0D;
				Integer tc_y = 0;
				Integer people_y = 0;
				if (totalMap.size() > 7) {
					if ("1".equals(rsData.getString("isholiday"))) {
						money_y = Double.parseDouble(totalMap.get("holiday").toString());
					} else {
						money_y = Double.parseDouble(totalMap.get(DateJudge.getWeekOfDateToString(rsData.getString("dat"))).toString());
					}
				} else {
					money_y = Double.parseDouble(totalMap.get(DateJudge.getWeekOfDateToString(rsData.getString("dat"))).toString());
				}
				if (tcMap.size() > 7) {
					if ("1".equals(rsData.getString("isholiday"))) {
						tc_y = Integer.parseInt(tcMap.get("holiday").toString());
					} else {
						tc_y = Integer.parseInt(tcMap.get(DateJudge.getWeekOfDateToString(rsData.getString("dat"))).toString());
					}
				} else {
					tc_y = Integer.parseInt(tcMap.get(DateJudge.getWeekOfDateToString(rsData.getString("dat"))).toString());
				}
				if (peolenumMap.size() > 7) {
					if ("1".equals(rsData.getString("isholiday"))) {
						people_y = Integer.parseInt(peolenumMap.get("holiday").toString());
					} else {
						people_y = Integer.parseInt(peolenumMap.get(DateJudge.getWeekOfDateToString(rsData.getString("dat"))).toString());
					}
				} else {
					people_y = Integer.parseInt(peolenumMap.get(DateJudge.getWeekOfDateToString(rsData.getString("dat"))).toString());
				}
				//门店编码
				p.setFirm(firmId);
				//门店名称
				p.setFirmNm(firmName);
				// 日期
				p.setDat(rsData.getDate("dat"));
				// 周次
				p.setWeek(DateJudge.getWeekOfDateToString(DateJudge.getStringByDate(rsData.getDate("dat"), "yyyy-mm-dd")));
				// 特殊事件 
				p.setMemo("");
				// 节假日
				p.setIsHoliday(rsData.getString("isholiday"));
				// 营业额 -去年同期
				p.setLast_money(moneyData);
				// 营业额 -预估值
				p.setMoney_y(money_y);
				// 营业额 -调整值
				p.setMoney_t(money_y);
				// 单均 -去年同期环比
				if(tcData <= 0 || moneyData <= 0){
					p.setLast_ac(ValueUtil.getDoubleValue(FormatDouble.formatDoubleLength(0d)));
				}else {
					p.setLast_ac(ValueUtil.getDoubleValue(FormatDouble.formatDoubleLength(moneyData/tcData)));
				}
				// 单均 -预估值
				if(tc_y > 0){
					p.setAc_y(ValueUtil.getDoubleValue(FormatDouble.formatDoubleLength(money_y/tc_y)));
					p.setAc_t(ValueUtil.getDoubleValue(FormatDouble.formatDoubleLength(money_y/tc_y)));
				}else {
					p.setAc_y(ValueUtil.getDoubleValue(FormatDouble.formatDoubleLength(0d)));
					p.setAc_t(ValueUtil.getDoubleValue(FormatDouble.formatDoubleLength(0d)));
				}
				// 单均 -调整值
				// 单数 -去年同期环比
				p.setLast_bills(tcData);
				// 单数 -预估值
				p.setBills_y(ValueUtil.getIntValue(tc_y));
				// 单数 -调整值
				p.setBills_t(ValueUtil.getIntValue(tc_y));
				// 人均 -去年同期环比
				if(peopleData <= 0 || moneyData <= 0){
					p.setLast_percapita(ValueUtil.getDoubleValue(FormatDouble.formatDoubleLength(0d)));
				}else {
					p.setLast_percapita(ValueUtil.getDoubleValue(FormatDouble.formatDoubleLength(moneyData/peopleData)));
				}
				// 人均 -预估值
				// 人均 -调整值
				if(people_y > 0){
					p.setPercapita_y(ValueUtil.getDoubleValue(FormatDouble.formatDoubleLength(money_y/people_y)));
					p.setPercapita_t(ValueUtil.getDoubleValue(FormatDouble.formatDoubleLength(money_y/people_y)));
				}else {
					p.setPercapita_y(ValueUtil.getDoubleValue(FormatDouble.formatDoubleLength(0d)));
					p.setPercapita_t(ValueUtil.getDoubleValue(FormatDouble.formatDoubleLength(0d)));
				}
				// 人数 -去年同期环比
				p.setLast_people(ValueUtil.getIntValue(peopleData));
				// 人数 -预估值
				p.setPeople_y(ValueUtil.getIntValue(people_y));
				// 人数 -调整值
				p.setPeople_t(ValueUtil.getIntValue(people_y));
				listData.add(p);
			}
		     if (listData.size() > 0) {
				for (SalePlan sp : listData) {
					forecastMisMapper.deletePosSalePlan(sp);
					forecastMisMapper.insertPosSalePlan(sp);
				}
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}finally{
			try {
				if(ptmt!=null){
					ptmt.close();
				}
			} catch (SQLException e) {
			}
			try {
				if(conn!=null){
					conn.close();
				}
			} catch (SQLException e) {
			}
		}
	}
	
	/**
	 * 操作MAP对象结果
	 * @author 文清泉 2014年9月29日 下午4:10:26
	 * @param map 当前操作MAP 
	 * @param week KEY值
	 * @param typ 类型 1 DOUBLE 2 INTEGER
	 * @param value 修改值
	 */
	private void sumMapValue(Map<String, Object> map,Object week,int typ, Object value){
	    if(typ == 1){
		if(map.get(week) != null){
		    map.put(week.toString(),Double.parseDouble(value.toString())+Double.parseDouble(map.get(week).toString()));
		}else{
		    map.put(week.toString(),Double.parseDouble(value.toString()));
		}
	    }else if(typ == 2){
		if(map.get(week) != null){
		    map.put(week.toString(),Integer.parseInt(value.toString())+Integer.parseInt(map.get(week).toString()));
		}else{
		    map.put(week.toString(),Integer.parseInt(value.toString()));
		}
	    }
	}
	/**
	 * 
	 * @author 文清泉 2014年9月29日 下午4:12:58
	 * @param countMap
	 */
	private void totalCountMap(Map<String, Integer> countMap,String key) {
	   if(countMap.get(key) != null){
	       countMap.put(key,countMap.get(key)+1);
	   }else{
	       countMap.put(key,1);
	   }
	       
	}
	/**
	 * 获取对应MAP对象值
	 * @author 文清泉 2014年9月29日 下午3:23:41
	 * @param map
	 * @param key
	 */
	private Object getMapValue(Map<SalesForecastEnum,Object> map,SalesForecastEnum key){
	    if(map.get(key) != null && !map.get(key).equals("")){
		return map.get(key);
	    }else {
		return "";
	    }
	}
	
	/**
	 * 计算最终预估值
	 * @author 文清泉 2014年9月29日 下午4:37:49
	 * @param typKey 
	 * @param
	 */
	private void calculationForecast(Map<String, Object> totalMap,Map<String, Integer> countMap, String typKey,int typ) {
	    String[] week = new String[]{"一","二","三","四","五","六","日"};
	    Integer count = 0;
	    Object value;
	    for (int i = 0; i < week.length; i++) {
			value = totalMap.get(week[i]);
			count = countMap.get(week[i]+typKey);
			if(value == null){
			    totalMap.put(week[i],0);
			    continue;
			}
			if(typ == 1){
			    if(Double.parseDouble(value.toString()) > 0 && count > 0){
				totalMap.put(week[i],Double.parseDouble(value.toString())/count);
			    }
			}else if(typ == 2){
			    if(Integer.parseInt(value.toString()) > 0 && count > 0){
				totalMap.put(week[i],Integer.parseInt(value.toString())/count);
			    }
			}
	    }
	}
	
	/**
	 * 保存修改调整量(营业预估)
	 * @param posSalePlan
	 * @param scode
	 * @throws CRUDException
	 */
	public void updateSalePlan(SalePlan posSalePlan, String scode) throws CRUDException {
		try {
			for (int i = 0; i < posSalePlan.getSalePlanList().size(); i++) {//批量修改
				SalePlan posSalePlan_ = posSalePlan.getSalePlanList().get(i);
				posSalePlan_.setFirm(scode);
//				posSalePlan_.setDat(posSalePlan.getSalePlanList().get(i).getDat());
//				posSalePlan_.setMoney_t(posSalePlan.getSalePlanList().get(i).getMoney_t());
//				posSalePlan_.setBills_t(posSalePlan.getSalePlanList().get(i).getBills_t());
//				posSalePlan_.setPeople_t(posSalePlan.getSalePlanList().get(i).getPeople_t());
//				posSalePlan_.setAc_t(posSalePlan.getSalePlanList().get(i).getAc_t());
//				posSalePlan_.setPercapita_t(posSalePlan.getSalePlanList().get(i).getPercapita_t());
//				posSalePlan_.setIsHoliday(posSalePlan.getSalePlanList().get(i).getIsHoliday());
				
				forecastMisMapper.updateSalePlan(posSalePlan_);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	//导出营业预估Excel 
	public boolean exportExcelcast(OutputStream os,List<SalePlan> list) {   
		WritableWorkbook workBook = null;
		try {
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet("Sheet1", 0);
			sheet.mergeCells(0, 0, 18, 1);
            sheet.addCell(new Label(0,0,"营业预估"));
            //设置表格表头
            sheet.addCell(new Label(0, 2, "日期"));      
            sheet.addCell(new Label(1, 2, "星期"));      
            sheet.addCell(new Label(2, 2, "假日"));      
            sheet.addCell(new Label(3, 2, "特殊事件"));  
            sheet.addCell(new Label(4, 2, "营业额|去年同期")); 
            sheet.addCell(new Label(5, 2, "营业额|预估")); 
            sheet.addCell(new Label(6, 2, "营业额|调整")); 
            sheet.addCell(new Label(7, 2, "人均|去年同期")); 
            sheet.addCell(new Label(8, 2, "人均|预估")); 
            sheet.addCell(new Label(9, 2, "人均|调整")); 
            sheet.addCell(new Label(10, 2, "人数|去年同期")); 
            sheet.addCell(new Label(11, 2, "人数|预估")); 
            sheet.addCell(new Label(12, 2, "人数|调整")); 
            sheet.addCell(new Label(13, 2, "单均|去年同期")); 
            sheet.addCell(new Label(14, 2, "单均|预估")); 
            sheet.addCell(new Label(15, 2, "单均|调整")); 
            sheet.addCell(new Label(16, 2, "单数|去年同期")); 
            sheet.addCell(new Label(17, 2, "单数|预估")); 
            sheet.addCell(new Label(18, 2, "单数|调整")); 

            SimpleDateFormat sdf =  new SimpleDateFormat("yyyy-MM-dd"); 
            //遍历list填充表格内容
            for(int i=0;i<list.size();i++) {
            	if (i == list.size() - 1) {
            		sheet.addCell(new Label(0, i+3, "合计"));                  
				}else{
					sheet.addCell(new Label(0, i+3, String.valueOf(sdf.format(list.get(i).getDat()))));                  
				}
            	sheet.addCell(new Label(1, i+3, list.get(i).getWeek()));            
        		sheet.addCell(new Label(2, i+3, list.get(i).getIsHoliday()));                                      
            	sheet.addCell(new Label(3, i+3, list.get(i).getMemo()));                                      
            	sheet.addCell(new Label(4, i+3, ValueUtil.getStringValue(list.get(i).getLast_money())));
            	sheet.addCell(new Label(5, i+3, ValueUtil.getStringValue(list.get(i).getMoney_y())));   
            	sheet.addCell(new Label(6, i+3, ValueUtil.getStringValue(list.get(i).getMoney_t())));    
            	sheet.addCell(new Label(7, i+3, ValueUtil.getStringValue(list.get(i).getLast_percapita())));       
            	sheet.addCell(new Label(8, i+3, ValueUtil.getStringValue(list.get(i).getPercapita_y())));    
            	sheet.addCell(new Label(9, i+3, ValueUtil.getStringValue(list.get(i).getPercapita_t())));       
            	sheet.addCell(new Label(10, i+3, ValueUtil.getStringValue(list.get(i).getLast_people())));   
            	sheet.addCell(new Label(11, i+3, ValueUtil.getStringValue(list.get(i).getPeople_y())));      
            	sheet.addCell(new Label(12, i+3, ValueUtil.getStringValue(list.get(i).getPeople_t())));  
            	sheet.addCell(new Label(13, i+3, ValueUtil.getStringValue(list.get(i).getLast_ac())));   
            	sheet.addCell(new Label(14, i+3, ValueUtil.getStringValue(list.get(i).getAc_y())));   
            	sheet.addCell(new Label(15, i+3, ValueUtil.getStringValue(list.get(i).getAc_t())));   
            	sheet.addCell(new Label(16, i+3, ValueUtil.getStringValue(list.get(i).getLast_bills())));   
            	sheet.addCell(new Label(17, i+3, ValueUtil.getStringValue(list.get(i).getBills_y())));   
            	sheet.addCell(new Label(18, i+3, ValueUtil.getStringValue(list.get(i).getBills_t())));   
	    	}
            workBook.write();
            os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	/********************************************************营业预估end*************************************************/
	
	/****************************************************预估菜品销售计划start*************************************************/
	
	/**
	 * 查找预估菜品销售计划
	 * @param posItemPlan
	 * @return
	 * @throws CRUDException
	 */
	public List<ItemPlan> findPosItemPlan(ItemPlan posItemPlan, Page page) throws CRUDException
	{
		try {			
			return pageManager2.selectPage(posItemPlan, page, ForecastMisMapper.class.getName()+".findPosItemPlan");			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查找预估菜品销售计划
	 * @param posItemPlan
	 * @return
	 * @throws CRUDException
	 */
	public List<ItemPlan> findPosItemPlan(ItemPlan posItemPlan) throws CRUDException
	{
		try {
			return forecastMisMapper.findPosItemPlan(posItemPlan);			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查找菜品销售计划天数
	 * @param posItemPlan
	 * @return
	 * @throws CRUDException
	 */
	public List<ItemPlan> findPosItemPlanForDays(ItemPlan posItemPlan) throws CRUDException
	{
		try {
			return forecastMisMapper.findPosItemPlanForDays(posItemPlan);			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询菜品销售计划
	 * @param itemPlan
	 * @return
	 * @throws CRUDException
	 */
	public List<ItemPlan> queryItemPlan(ItemPlan itemPlan,Positn positn)throws CRUDException{
		try {
			List<String> listStr = new ArrayList<String>();//日期时间段
			listStr = DateJudge.getDateList(DateJudge.getStringByDate(itemPlan.getStartdate(), "yyyy-MM-dd"), DateJudge.getStringByDate(itemPlan.getEnddate(), "yyyy-MM-dd"));
			List<ItemPlan> listItemPlan = forecastMisMapper.findPosItemPlan(itemPlan);
			if (listItemPlan.size() <= 0) {
				return null;
			}
			//用来放点击率的数据
			FirmItemUse f = new FirmItemUse();
			f.setFirm(itemPlan.getFirm());
			f.setDept(ValueUtil.getStringValue(itemPlan.getDept()));
			List<FirmItemUse> li = forecastMisMapper.findAllItemUse(f);
			List<ItemPlan> list = new ArrayList<ItemPlan>();
			List<Map<String,String>> listPcodeMap = forecastMisMapper.getDeptByPubitemCode(itemPlan.getFirm());
			for (FirmItemUse firmItemUse : li) {
				ItemPlan ip = new ItemPlan();
				ip.setFirm(firmItemUse.getFirm());
				//查询部门信息
				boolean b = false;
				for(Map<String,String> map : listPcodeMap){
					if(firmItemUse.getItcode().equals(map.get("PCODE"))){
						ip.setDept(map.get("VCODE"));
						ip.setDeptdes(map.get("VNAME"));
						ip.setDept1(map.get("DEPT1"));
						ip.setDept1des(map.get("DEPT1DES"));
						b = true;
					}
				}
				if (!b) {
					continue;
//					if (map != null && itemPlan.getDept() != null && itemPlan.getDept().equals(ValueUtil.getStringValue(map.get("vcode")))) {
//					ip.setDept(map.get("vcode"));
//					ip.setDeptdes(map.get("vname"));
//					ip.setDeptdes(ValueUtil.getStringValue(map.get("vcode")));
				}
				ip.setItem(firmItemUse.getItem());
				ip.setItcode(firmItemUse.getItcode());
				ip.setItdes(firmItemUse.getItdes());
				ip.setItunit(ValueUtil.getStringValue(firmItemUse.getItunit()));
				List<ItemPlan> listDate = new ArrayList<ItemPlan>();
				List<Map<String,Object>> listmap = new ArrayList<Map<String,Object>>();   //修改用
				for (String str : listStr) {
					ItemPlan i = new ItemPlan();
					Map<String,Object> listDateMap = new HashMap<String,Object>();
					for (ItemPlan iplst : listItemPlan) {
						if (!str.equals(DateJudge.getStringByDate(iplst.getDat(), "yyyy-MM-dd"))) {
							continue;
						}
						if (firmItemUse.getItcode().equals(iplst.getItcode())) {
							i.setCaltotal(iplst.getCaltotal());//计算值
							i.setUpdtotal(iplst.getUpdtotal());//调整值
							listDateMap.put("cal", iplst.getCaltotal());
							listDateMap.put("upd", iplst.getUpdtotal());
							listDateMap.put("dat", str);
						}
					}
					listDate.add(i);
					listmap.add(listDateMap);
				}
				ip.setItemplanlist(listmap);
				list.add(ip);
			}
			return list;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 重新计算菜品销售计划
	 * @param isDeclare 
	 * @param salePlan 
	 * @return
	 * @throws CRUDException
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	public void saveCalPlan(String firmId, Date bdate, Date edate,ItemPlan  posItemPlan, HttpSession session, String salePlan, String isDeclare) throws CRUDException {
		try {
			Positn positn = commonMISBOHService.getPositn(session);
			int stat = 0;//判断选择日期是否是节假日
			ItemPlan itemPlan = new ItemPlan();//菜品销售计划实例
			FirmItemUse firmItemUse = new FirmItemUse();
			Store store = new Store();//门店实例
			List<String> listStr = new ArrayList<String>();//日期时间段
			String date = null;//参考的日期
			listStr = DateJudge.getDateList(DateJudge.getStringByDate(bdate, "yyyy-MM-dd"), DateJudge.getStringByDate(edate, "yyyy-MM-dd"));
			store.setVcode(positn.getCode());
			//获取预估方式
			String forecastType = ValueUtil.getStringValue(commonMISBOHService.getStore(store).getVplantyp());
			if (forecastType == null || "".equals(forecastType)) {
				forecastType = "AMT";
			}
			
			//判断当前店铺是否使用多部门
			FirmItemUse f = new FirmItemUse();
			f.setFirm(firmId);
			f.setDept(ValueUtil.getStringValue(posItemPlan.getDept()));
			//用来放点击率的数据
			List<FirmItemUse> li = forecastMisMapper.findAllItemUse(f);
			List<Map<String,Object>> saleMap = null;
			if(isDeclare != null && isDeclare.equals("1")){
				saleMap = JSONArray.fromObject(salePlan);
			}
			if (li.size() <= 0) {
			} else {
				for(String dat :listStr){
					posItemPlan.setDat(DateJudge.YYYY_MM_DD.parse(dat));
					forecastMisMapper.deleteItemPlan(posItemPlan);
				}
				
				List<Holiday> holidayList = commonMISBOHService.getHoliday(DateJudge.YYYY_MM_DD.format(bdate),DateJudge.YYYY_MM_DD.format(edate));
				Map<String,Map<String,String>> listmapBFC = getBusinessForecast(bdate,edate, forecastType,firmId);//根据预估方式和营业日查询预估数据
				Map<String,Map<String,String>> listPcodeMap = getDeptByPubitemCode(firmId);
				for (int i = 0; i < li.size(); i++) {//循环所有的点击率
					firmItemUse = li.get(i);
					Map<String,String> map = listPcodeMap.get(firmItemUse.getItcode());
					if(map == null ){
						continue;
					}
					itemPlan.setDept(ValueUtil.getStringValue(map.get("vcode")));//部门
					itemPlan.setDeptdes(ValueUtil.getStringValue(map.get("vname")));
					itemPlan.setDept1(ValueUtil.getStringValue(map.get("dept1")));//供应链中的部门20160504wjf
					itemPlan.setDept1des(ValueUtil.getStringValue(map.get("dept1des")));
					itemPlan.setItcode(firmItemUse.getItcode());//菜品编码
					itemPlan.setItdes(firmItemUse.getItdes());//菜品名称
					itemPlan.setItunit(ValueUtil.getStringValue(firmItemUse.getItunit()));//单位
					itemPlan.setFirm(firmId);
					itemPlan.setItem(firmItemUse.getItem());
					Double predictedValue = 0.0;
					Double adjustmentValue = 0.0;
					for (int j = 0; j < listStr.size(); j++) {//循环日期列表
						date = listStr.get(j);
						itemPlan.setDat(DateJudge.getDateByString(date));//营业日
						
						
						//验证是否报货向导中计算  -  读取预估值
						if(saleMap!=null){
							for (Map<String, Object> sale : saleMap) {
								if(DateJudge.YYYY_MM_DD.format(itemPlan.getDat()).equals(sale.get("DAT"))){
									predictedValue = ValueUtil.getDoubleValue(sale.get("SALEVALUE"));
									adjustmentValue = ValueUtil.getDoubleValue(sale.get("SALEVALUE"));
									break;
								}else{
									predictedValue = 0.0;
									adjustmentValue = 0.0;
								}
							}
						}else{
							Map<String,String> mapBFC = listmapBFC.get(DateJudge.YYYY_MM_DD.format(itemPlan.getDat()));
							//判断如果当前日期没有预估数据默认为0
							if (mapBFC == null) {
								itemPlan.setCaltotal(0);//计算值
								itemPlan.setUpdtotal(0);//调整值
								continue;
							}else{
								predictedValue = ValueUtil.getDoubleValue(mapBFC.get("predictedValue"));
								adjustmentValue = ValueUtil.getDoubleValue(mapBFC.get("adjustmentValue"));
							}
						}
						int grade = 0;
						//判断是否是节假日
						if(holidayList!=null){
							for (Holiday holiday : holidayList) {
								if(holiday.getDholidaydate().equals(date)){
									grade = holiday.getVholidaygrade();
									break;
								}
							}
						}
						if (grade != 1 && grade != 2) {
							stat = 0;
						} else {
							stat = grade;
						}
						String week = "星期"+DateJudge.getWeekOfDateToString(date);//获得当前循环日期的周次
						if (stat >= 2) {//法定节假日
//							double caltotal = (firmItemUse.getPrev81() * predictedValue)/1000;
							double updtotal = (firmItemUse.getModv81() * adjustmentValue)/1000.0;
							itemPlan.setCaltotal(Math.round(updtotal));
							itemPlan.setUpdtotal(Math.round(updtotal));
						} else if (stat == 1) {
							if (week.equals("星期六")) {//法定周六
								double updtotal = (firmItemUse.getModv61() * adjustmentValue)/1000.0;
								itemPlan.setCaltotal(Math.round(updtotal));
								itemPlan.setUpdtotal(Math.round(updtotal));
							} else if (week.equals("星期日")) {//法定周天
								double updtotal = (firmItemUse.getModv71() * adjustmentValue)/1000.0;
								itemPlan.setCaltotal(Math.round(updtotal));
								itemPlan.setUpdtotal(Math.round(updtotal));
							}
						} else {
							if (week.equals("星期一")) {
								double updtotal = (firmItemUse.getModv11() * adjustmentValue)/1000.0;
								itemPlan.setCaltotal(Math.round(updtotal));
								itemPlan.setUpdtotal(Math.round(updtotal));
							} else if (week.equals("星期二")) {
								double updtotal = (firmItemUse.getModv21() * adjustmentValue)/1000.0;
								itemPlan.setCaltotal(Math.round(updtotal));
								itemPlan.setUpdtotal(Math.round(updtotal));
							} else if (week.equals("星期三")) {
								double updtotal = (firmItemUse.getModv31() * adjustmentValue)/1000.0;
								itemPlan.setCaltotal(Math.round(updtotal));
								itemPlan.setUpdtotal(Math.round(updtotal));
							} else if (week.equals("星期四")) {
								double updtotal = (firmItemUse.getModv41() * adjustmentValue)/1000.0;
								itemPlan.setCaltotal(Math.round(updtotal));
								itemPlan.setUpdtotal(Math.round(updtotal));
							} else if (week.equals("星期五")) {
								double updtotal = (firmItemUse.getModv51() * adjustmentValue)/1000.0;
								itemPlan.setCaltotal(Math.round(updtotal));
								itemPlan.setUpdtotal(Math.round(updtotal));
							} else {
								double Calvalue1 = (firmItemUse.getModv11() * adjustmentValue)/1000.0;
								double Calvalue2 = (firmItemUse.getModv21() * adjustmentValue)/1000.0;
								double Calvalue3 = (firmItemUse.getModv31() * adjustmentValue)/1000.0;
								double Calvalue4 = (firmItemUse.getModv41() * adjustmentValue)/1000.0;
								double Calvalue5 = (firmItemUse.getModv51() * adjustmentValue)/1000.0;
								//周一到周五早班平均值
								double Caltotal = (Calvalue1 + Calvalue2 + Calvalue3 + Calvalue4 + Calvalue5)/5;
								itemPlan.setCaltotal(Math.round(Caltotal));//全天预估值
								itemPlan.setUpdtotal(Math.round(Caltotal));//全天预估值
							}
						}
						forecastMisMapper.insertItemPlan(itemPlan);
					}
				}
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/***
	 * 重新计算菜品销售计划
	 * @param firmId
	 * @param bdate
	 * @param edate
	 * @param posItemPlan
	 * @throws CRUDException
	 */
	@SuppressWarnings("unused")
	public void saveCalPlanWAP(String firmId, Date bdate, Date edate,ItemPlan  posItemPlan) throws CRUDException {
		try {
			int stat = 0;//判断选择日期是否是节假日
			ItemPlan itemPlan = new ItemPlan();//菜品销售计划实例
			FirmItemUse firmItemUse = new FirmItemUse();
			Store store = new Store();//门店实例
			List<String> listStr = new ArrayList<String>();//日期时间段
			String date = null;//参考的日期
			listStr = DateJudge.getDateList(DateJudge.getStringByDate(bdate, "yyyy-MM-dd"), DateJudge.getStringByDate(edate, "yyyy-MM-dd"));
//			accountId = ValueUtil.getStringValue(accountId);
			
			store.setVcode(firmId);
			//获取预估方式
			String forecastType = ValueUtil.getStringValue(commonMISBOHService.getStore(store).getVplantyp());
			if (forecastType == null || "".equals(forecastType)) {
				forecastType = "AMT";
			}
			//用来放点击率的数据
			List<FirmItemUse> li = new ArrayList<FirmItemUse>();
			//判断当前店铺是否使用多部门
			FirmItemUse f = new FirmItemUse();
			f.setFirm(firmId);
			f.setDept(ValueUtil.getStringValue(posItemPlan.getDept()));
			li = findAllItemUse(f);
			List<Map<String,Object>> saleMap = null;
			if (li.size() <= 0) {
			} else {
				for(String dat :listStr){
					posItemPlan.setDat(DateJudge.YYYY_MM_DD.parse(dat));
					forecastMisMapper.deleteItemPlan(posItemPlan);
				}
				
				List<Holiday> holidayList = commonMISBOHService.getHoliday(DateJudge.YYYY_MM_DD.format(bdate),DateJudge.YYYY_MM_DD.format(edate));
				Map<String,Map<String,String>> listmapBFC = getBusinessForecast(bdate,edate, forecastType,firmId);//根据预估方式和营业日查询预估数据
				Map<String,Map<String,String>> listPcodeMap = getDeptByPubitemCode(firmId);
				for (int i = 0; i < li.size(); i++) {//循环所有的点击率
					firmItemUse = li.get(i);
					Map<String,String> map = listPcodeMap.get(firmItemUse.getItcode());
					if(map == null ){
						continue;
					}
					itemPlan.setDept(ValueUtil.getStringValue(map.get("vcode")));//部门
					itemPlan.setDeptdes(ValueUtil.getStringValue(map.get("vname")));
					itemPlan.setDept1(ValueUtil.getStringValue(map.get("dept1")));//供应链中的部门20160504wjf
					itemPlan.setDept1des(ValueUtil.getStringValue(map.get("dept1des")));
					itemPlan.setItcode(firmItemUse.getItcode());//菜品编码
					itemPlan.setItdes(firmItemUse.getItdes());//菜品名称
					itemPlan.setItunit(ValueUtil.getStringValue(firmItemUse.getItunit()));//单位
					itemPlan.setFirm(firmId);
					itemPlan.setItem(firmItemUse.getItem());
					Double predictedValue = 0.0;
					Double adjustmentValue = 0.0;
					for (int j = 0; j < listStr.size(); j++) {//循环日期列表
						date = listStr.get(j);
						itemPlan.setDat(DateJudge.getDateByString(date));//营业日
						
						
						//验证是否报货向导中计算  -  读取预估值
						if(saleMap!=null){
							for (Map<String, Object> sale : saleMap) {
								if(DateJudge.YYYY_MM_DD.format(itemPlan.getDat()).equals(sale.get("DAT"))){
									predictedValue = ValueUtil.getDoubleValue(sale.get("SALEVALUE"));
									adjustmentValue = ValueUtil.getDoubleValue(sale.get("SALEVALUE"));
									break;
								}else{
									predictedValue = 0.0;
									adjustmentValue = 0.0;
								}
							}
						}else{
							Map<String,String> mapBFC = listmapBFC.get(DateJudge.YYYY_MM_DD.format(itemPlan.getDat()));
							//判断如果当前日期没有预估数据默认为0
							if (mapBFC == null) {
								itemPlan.setCaltotal(0);//计算值
								itemPlan.setUpdtotal(0);//调整值
								continue;
							}else{
								predictedValue = ValueUtil.getDoubleValue(mapBFC.get("predictedValue"));
								adjustmentValue = ValueUtil.getDoubleValue(mapBFC.get("adjustmentValue"));
							}
						}
						int grade = 0;
						//判断是否是节假日
						if(holidayList!=null){
							for (Holiday holiday : holidayList) {
								if(holiday.getDholidaydate().equals(date)){
									grade = holiday.getVholidaygrade();
									break;
								}
							}
						}
						if (grade != 1 && grade != 2) {
							stat = 0;
						} else {
							stat = grade;
						}
						String week = "星期"+DateJudge.getWeekOfDateToString(date);//获得当前循环日期的周次
						if (stat >= 2) {//法定节假日
//							double caltotal = (firmItemUse.getPrev81() * predictedValue)/1000;
							double updtotal = (firmItemUse.getModv81() * adjustmentValue)/1000.0;
							itemPlan.setCaltotal(Math.round(Math.round(updtotal)));
							itemPlan.setUpdtotal(Math.round(Math.round(updtotal)));
						} else if (stat == 1) {
							if (week.equals("星期六")) {//法定周六
								double updtotal = (firmItemUse.getModv61() * adjustmentValue)/1000.0;
								itemPlan.setCaltotal(Math.round(updtotal));
								itemPlan.setUpdtotal(Math.round(updtotal));
							} else if (week.equals("星期日")) {//法定周天
								double updtotal = (firmItemUse.getModv71() * adjustmentValue)/1000.0;
								itemPlan.setCaltotal(Math.round(updtotal));
								itemPlan.setUpdtotal(Math.round(updtotal));
							}
						} else {
							if (week.equals("星期一")) {
								double updtotal = (firmItemUse.getModv11() * adjustmentValue)/1000.0;
								itemPlan.setCaltotal(Math.round(updtotal));
								itemPlan.setUpdtotal(Math.round(updtotal));
							} else if (week.equals("星期二")) {
								double updtotal = (firmItemUse.getModv21() * adjustmentValue)/1000.0;
								itemPlan.setCaltotal(Math.round(updtotal));
								itemPlan.setUpdtotal(Math.round(updtotal));
							} else if (week.equals("星期三")) {
								double updtotal = (firmItemUse.getModv31() * adjustmentValue)/1000.0;
								itemPlan.setCaltotal(Math.round(updtotal));
								itemPlan.setUpdtotal(Math.round(updtotal));
							} else if (week.equals("星期四")) {
								double updtotal = (firmItemUse.getModv41() * adjustmentValue)/1000.0;
								itemPlan.setCaltotal(Math.round(updtotal));
								itemPlan.setUpdtotal(Math.round(updtotal));
							} else if (week.equals("星期五")) {
								double updtotal = (firmItemUse.getModv51() * adjustmentValue)/1000.0;
								itemPlan.setCaltotal(Math.round(updtotal));
								itemPlan.setUpdtotal(Math.round(updtotal));
							} else {
								double Calvalue1 = (firmItemUse.getModv11() * adjustmentValue)/1000.0;
								double Calvalue2 = (firmItemUse.getModv21() * adjustmentValue)/1000.0;
								double Calvalue3 = (firmItemUse.getModv31() * adjustmentValue)/1000.0;
								double Calvalue4 = (firmItemUse.getModv41() * adjustmentValue)/1000.0;
								double Calvalue5 = (firmItemUse.getModv51() * adjustmentValue)/1000.0;
								//周一到周五早班平均值
								double Caltotal = (Calvalue1 + Calvalue2 + Calvalue3 + Calvalue4 + Calvalue5)/5;
								itemPlan.setCaltotal(Math.round(Caltotal));//全天预估值
							}
						}
						forecastMisMapper.insertItemPlan(itemPlan);
					}
				}
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据预估方式和营业日查询预估数据
	 * @param bdate
	 * @param edate 
	 * @param forecastType
	 * @param firmId 
	 * @return
	 */
	public Map<String,Map<String,String>> getBusinessForecast(Date bdate,Date edate, String forecastType, String firmId)throws CRUDException{
		Connection conn=null;  
		PreparedStatement ptmt = null;
		try {
			String sql = "SELECT * FROM POSSALEPLAN WHERE date_format(dat,'%Y-%m-%d') >= '"+ DateJudge.getStringByDate(bdate,"yyyy-MM-dd") 
					+"' and date_format(dat,'%Y-%m-%d') <= '"+ DateJudge.getStringByDate(edate,"yyyy-MM-dd")+"' and firm = '"+firmId+"'";
			getJdbcConn();
			Class.forName(driver);  
			conn=DriverManager.getConnection(url, uname, password);
			ptmt=conn.prepareStatement(sql);
			ResultSet rs = ptmt.executeQuery(); 
			Map<String,Map<String,String>> mapList = new HashMap<String,Map<String,String>>();
			while (rs.next()) {
				Map<String,String> map = new HashMap<String,String>();
				if (forecastType.equals("PAX")) {
					map.put("predictedValue", rs.getString("people_t"));
					map.put("adjustmentValue", rs.getString("people_t"));
				} else if (forecastType == null || "".equals(forecastType) || forecastType.equals("AMT")) {
					map.put("predictedValue", rs.getString("money_t"));
					map.put("adjustmentValue", rs.getString("money_t"));
				} else if (forecastType.equals("TC")){
					map.put("predictedValue", rs.getString("bills_t"));
					map.put("adjustmentValue", rs.getString("bills_t"));
				}
				mapList.put(DateJudge.YYYY_MM_DD.format(rs.getDate("DAT")), map);
			}
			rs.close();
			return mapList;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}finally{
			if(ptmt!=null){
				try {
					ptmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * 根据店铺编码和菜品编码查询菜品所属的部门
	 * @param firmId
	 * @return
	 */
	public Map<String,Map<String,String>> getDeptByPubitemCode(String firmId)throws CRUDException{
		PreparedStatement ptmt = null;
		Connection conn=null;  
		try {
			StringBuffer sql = new StringBuffer();
			getJdbcConn_scm();
			sql.append("\n select p.vcode as pcode,dp.pk_storedept,dp.pk_groupdept,dp.vcode,dp.vname,pf.positn as dept1,pf.deptdes as dept1des,dp.vinit,dp.isortno,dp.enablestate,dp.vmemo,dp.pk_store,");
			sql.append("\n dp.pk_group,dp.iproportion,dp.istatistic ,p.pk_pubitem,p.vcode as pubitemcode,p.vname as pubitemname from cboh_pubitem_3ch p ");
			sql.append("\n left join cboh_printprog_3ch pr on pr.pk_pubitem = p.pk_pubitem ");
			sql.append("\n left join cboh_printprogt_3ch pt on pt.pk_printprogt = pr.pk_printprogt ");
			sql.append("\n left join cboh_storedept_3ch dp on pr.pk_storedept = dp.vcode and pt.pk_store = dp.pk_store ");
			sql.append("\n left join positnfirm pf on pf.firm = '"+ firmId +"' and dp.vcode = pf.dept ");//关联查供应链中设置的档口
			sql.append("\n where pt.pk_store = (select pk_store from cboh_store_3ch where vcode='"+ firmId +"')");
			Class.forName(driver);  
			conn = DriverManager.getConnection(url, uname, password);
			ptmt = conn.prepareStatement(ValueUtil.getStringValue(sql));
			ResultSet rs = ptmt.executeQuery(); 
			Map<String,Map<String,String>> mapList = new HashMap<String,Map<String,String>>();
			while (rs.next()) {
				Map<String,String> map = new HashMap<String,String>();
				map.put("pk_storedept", rs.getString("pk_storedept"));
				map.put("pk_groupdept", rs.getString("pk_groupdept"));
				map.put("vcode", rs.getString("vcode"));
				map.put("vname", rs.getString("vname"));
				map.put("dept1", rs.getString("dept1"));
				map.put("dept1des", rs.getString("dept1des"));
				map.put("vinit", rs.getString("vinit"));
				map.put("isortno", rs.getString("isortno"));
				map.put("enablestate", rs.getString("enablestate"));
				map.put("vmemo", rs.getString("vmemo"));
				map.put("pk_store", rs.getString("pk_store"));
				map.put("pk_group", rs.getString("pk_group"));
				map.put("iproportion", rs.getString("iproportion"));
				map.put("istatistic", rs.getString("istatistic"));
				map.put("pk_pubitem", rs.getString("pk_pubitem"));
				map.put("pubitemcode", rs.getString("pubitemcode"));
				map.put("pubitemname", rs.getString("pubitemname"));
				mapList.put(rs.getString("pcode"), map);
			}
			rs.close();
			return mapList;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}finally{
			try {
				ptmt.close();
			} catch (SQLException e) {
			}
			try {
				conn.close();
			} catch (SQLException e) {
			}
		}
	}
	
	
	/**
	 * 保存修改调整量(预估菜品销售计划)
	 * @param posItemPlan
	 * @throws CRUDException
	 */
	public void updateItemPlan(ItemPlan posItemPlan) throws CRUDException {
		try {
			for (int i = 0; i < posItemPlan.getListItemPlan().size(); i++) {//批量修改
				forecastMisMapper.updateItemPlan(posItemPlan.getListItemPlan().get(i));
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存修改调整量(预估菜品销售计划)
	 * @param posItemPlan
	 * @throws CRUDException
	 */
	public void updateItemPlan(ItemPlan posItemPlan, String firm) throws CRUDException {
		try {
			for (int i = 0; i < posItemPlan.getListItemPlan().size(); i++) {//批量修改
				ItemPlan posItemPlan_=new ItemPlan();
				posItemPlan_.setFirm(firm);
				posItemPlan_.setItem(posItemPlan.getListItemPlan().get(i).getItem());
				posItemPlan_.setDat(posItemPlan.getListItemPlan().get(i).getDat());
				posItemPlan_.setUpdtotal(posItemPlan.getListItemPlan().get(i).getUpdtotal());
				
				forecastMisMapper.updateItemPlan(posItemPlan_);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除所有预估菜品销售计划
	 * @param
	 * @throws CRUDException
	 */
	public void deleteAllPlan(ItemPlan posItemPlan) throws CRUDException{
		try{
			forecastMisMapper.deleteAllPlan(posItemPlan);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	//导出菜品销售计划Excel 
	public boolean exportExcelPlan(OutputStream os,List<ItemPlan> list,List<String> listStr) {   
		WritableWorkbook workBook = null;
		try {
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet("Sheet1", 0);
            sheet.addCell(new Label(0,0,"分店菜品销售预估"));
            //设置表格表头
            sheet.addCell(new Label(0, 2, "部门"));      
            sheet.addCell(new Label(1, 2, "编码"));      
            sheet.addCell(new Label(2, 2, "名称"));      
            sheet.addCell(new Label(3, 2, "单位"));  
            for (int i=0;i<listStr.size();i++) {
            	sheet.addCell(new Label(4+i*2, 2, listStr.get(i) +"|参考")); 
            	sheet.addCell(new Label(5+i*2, 2, listStr.get(i) +"|预估")); 
			}
            sheet.mergeCells(0, 0, 11, 1);
            //遍历list填充表格内容
            for(int i=0;i<list.size();i++) {
            	sheet.addCell(new Label(0, i+3, list.get(i).getDept()));                     
            	sheet.addCell(new Label(1, i+3, list.get(i).getItcode()));                     
            	sheet.addCell(new Label(2, i+3, list.get(i).getItdes()));                      
            	sheet.addCell(new Label(3, i+3, list.get(i).getItunit()));
            	for (int j=0;j<listStr.size();j++) {
            		sheet.addCell(new Label(4+j*2, i+3, ValueUtil.getStringValue(list.get(i).getItemplanlist().get(j).get("cal"))));    
            		sheet.addCell(new Label(5+j*2, i+3, ValueUtil.getStringValue(list.get(i).getItemplanlist().get(j).get("upd"))));       
    			}
	    	}
            workBook.write();
            os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/********************************************************预估菜品销售计划end*************************************************/

}