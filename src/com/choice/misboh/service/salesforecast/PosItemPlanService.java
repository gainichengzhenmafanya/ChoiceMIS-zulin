package com.choice.misboh.service.salesforecast;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import net.sf.json.JSONArray;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.domain.salesforecast.FirmItemUse;
import com.choice.misboh.domain.salesforecast.ItemPlan;
import com.choice.misboh.domain.salesforecast.SalePlan;
import com.choice.misboh.domain.util.Holiday;
import com.choice.misboh.persistence.salesforecast.ItemUseMapper;
import com.choice.misboh.persistence.salesforecast.PosItemPlanMapper;
import com.choice.misboh.persistence.salesforecast.PosSalePlanMapper;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.scm.domain.Positn;
import com.choice.scm.persistence.PositnMapper;

@Service
public class PosItemPlanService {
	
	@Autowired
	private PosItemPlanMapper posItemPlanMapper;
	@Autowired
	private PosSalePlanMapper posSalePlanMapper;
	@Autowired
	private ItemUseMapper itemUseMapper;
	@Autowired
	private PositnMapper positnMapper;
	@Autowired
	private CommonMISBOHService commonMISBOHService;

	private final transient Log log = LogFactory.getLog(PosItemPlanService.class);

	/**
	 * 查询菜品销售计划
	 * @param itemPlan
	 * @return
	 * @throws CRUDException
	 */
	public List<ItemPlan> queryItemPlan(ItemPlan itemPlan)throws CRUDException{
		try {
			List<String> listStr = new ArrayList<String>();//日期时间段
			listStr = DateJudge.getDateList(DateJudge.getStringByDate(itemPlan.getBdate(), "yyyy-MM-dd"), DateJudge.getStringByDate(itemPlan.getEdate(), "yyyy-MM-dd"));
			List<ItemPlan> listItemPlan = posItemPlanMapper.findPosItemPlan(itemPlan);
			if (listItemPlan.size() <= 0) {
				return null;
			}
			//用来放点击率的数据
			FirmItemUse f = new FirmItemUse();
			f.setFirm(itemPlan.getFirm());
			f.setDept(ValueUtil.getStringValue(itemPlan.getDept()));
			List<FirmItemUse> li = itemUseMapper.findAllItemUse(f);
			List<ItemPlan> list = new ArrayList<ItemPlan>();
			for (FirmItemUse firmItemUse : li) {
				ItemPlan ip = new ItemPlan();
				ip.setFirm(firmItemUse.getFirm());
				ip.setItem(firmItemUse.getItem());
				ip.setItcode(firmItemUse.getItcode());
				ip.setItdes(firmItemUse.getItdes());
				ip.setItunit(ValueUtil.getStringValue(firmItemUse.getItunit()));
				List<ItemPlan> listDate = new ArrayList<ItemPlan>();
				List<Map<String,Object>> listmap = new ArrayList<Map<String,Object>>();   //修改用
				for (String str : listStr) {
					ItemPlan i = new ItemPlan();
					Map<String,Object> listDateMap = new HashMap<String,Object>();
					for (ItemPlan iplst : listItemPlan) {
						if (!str.equals(DateJudge.getStringByDate(iplst.getDat(), "yyyy-MM-dd"))) {
							continue;
						}
						if (firmItemUse.getItcode().equals(iplst.getItcode())) {
							i.setCaltotal(iplst.getCaltotal());//计算值
							i.setUpdtotal(iplst.getUpdtotal());//调整值
							listDateMap.put("cal", iplst.getCaltotal());
							listDateMap.put("upd", iplst.getUpdtotal());
							listDateMap.put("dat", str);
						}
					}
					listDate.add(i);
					listmap.add(listDateMap);
				}
				ip.setItemplanlist(listmap);
				list.add(ip);
			}
			return list;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/***
	 * 重新计算菜品销售计划
	 * @param itemPlan
	 * @param listStr
	 * @param chkstomSalePlan
	 * @param isDeclare
	 * @return
	 * @throws CRUDException
	 */
	public int saveCalPlan(ItemPlan itemPlan, List<String> listStr, String chkstomSalePlan, String isDeclare) throws CRUDException {
		try {
			//报货向导中的营业预估
			List<Map<String,Object>> saleMap = null;
			List<SalePlan> salePlanList = null;
			List<FirmItemUse> itemUseList = null;
			if("1".equals(isDeclare)){//如果在报货向导走这个方法
				saleMap = JSONArray.fromObject(chkstomSalePlan);
			} else {
				//有没有做营业预估
				SalePlan salePlan = new SalePlan();
				salePlan.setStartdate(itemPlan.getBdate());
				salePlan.setEnddate(itemPlan.getEdate());
				salePlan.setFirm(itemPlan.getFirm());
				salePlanList = posSalePlanMapper.findPosSalePlan(salePlan);
				if(salePlanList == null || salePlanList.size() == 0){
					return -1;
				}
			}
			//有没有菜品点击率
			FirmItemUse itemUse = new FirmItemUse();
			itemUse.setFirm(itemPlan.getFirm());
			itemUseList = itemUseMapper.findAllItemUse(itemUse);
			if(itemUseList == null || itemUseList.size() == 0){
				return -2;
			}
			//获取预估方式
			Positn positn = new Positn();
			positn.setCode(itemPlan.getFirm());
			positn = positnMapper.findPositnByCode(positn);
			String vplantyp = positn.getVplantyp();
			if (vplantyp == null || "".equals(vplantyp)) {
				vplantyp = "AMT";
			}
			Map<String,Map<String,String>> listmapBFC = null;
			if(!"1".equals(isDeclare)){
				//营业预估值放到map中
				listmapBFC = getBusinessForecast(salePlanList, vplantyp);//根据预估方式和营业日查询预估数据
			}
			//得到节假日
			List<Holiday> holidayList = commonMISBOHService.getHoliday(DateJudge.YYYY_MM_DD.format(itemPlan.getBdate()),DateJudge.YYYY_MM_DD.format(itemPlan.getEdate()));
			//先删除预估日期的菜品销售计划
			posItemPlanMapper.deleteItemPlan(itemPlan);
			for (FirmItemUse iu : itemUseList) {//循环所有的点击率
				ItemPlan ip = new ItemPlan();
				ip.setItcode(iu.getItcode());//菜品编码
				ip.setItdes(iu.getItdes());//菜品名称
				ip.setItunit(ValueUtil.getStringValue(iu.getItunit()));//单位
				ip.setFirm(iu.getFirm());
				ip.setItem(iu.getItem());
				Double predictedValue = 0.0;
				Double adjustmentValue = 0.0;
				for (int j = 0; j < listStr.size(); j++) {//循环日期列表
					String sdate = listStr.get(j);
					Date date = DateJudge.getDateByString(sdate);
					ip.setDat(date);//营业日
					//验证是否报货向导中计算  -  读取预估值
					if(saleMap!=null){
						for (Map<String, Object> sale : saleMap) {
							if(DateJudge.YYYY_MM_DD.format(date).equals(sale.get("DAT"))){
								predictedValue = ValueUtil.getDoubleValue(sale.get("SALEVALUE"));
								adjustmentValue = ValueUtil.getDoubleValue(sale.get("SALEVALUE"));
								break;
							}else{
								predictedValue = 0.0;
								adjustmentValue = 0.0;
							}
						}
					}else{
						Map<String,String> mapBFC = listmapBFC.get(DateJudge.YYYY_MM_DD.format(date));
						//判断如果当前日期没有预估数据默认为0
						if (mapBFC == null) {
							ip.setCaltotal(0);//计算值
							ip.setUpdtotal(0);//调整值
							continue;
						}else{
							predictedValue = ValueUtil.getDoubleValue(mapBFC.get("predictedValue"));
							adjustmentValue = ValueUtil.getDoubleValue(mapBFC.get("adjustmentValue"));
						}
					}
					int grade = 0;
					//判断是否是节假日
					if(holidayList!=null){
						for (Holiday holiday : holidayList) {
							if(holiday.getDholidaydate().equals(date)){
								grade = holiday.getVholidaygrade();
								break;
							}
						}
					}
					int stat = 0;
					if (grade < 2) {
						stat = 0;
					} else {
						stat = grade;
					}
					String week = "星期"+DateJudge.getWeekOfDateToString(sdate);//获得当前循环日期的周次
					if (stat >= 2) {//法定节假日
						double updtotal = (iu.getModv81() * adjustmentValue)/1000.0;
						ip.setCaltotal(Math.round(updtotal));
						ip.setUpdtotal(Math.round(updtotal));
					} else if (stat == 1) {
						if (week.equals("星期六")) {//法定周六
							double updtotal = (iu.getModv61() * adjustmentValue)/1000.0;
							ip.setCaltotal(Math.round(updtotal));
							ip.setUpdtotal(Math.round(updtotal));
						} else if (week.equals("星期日")) {//法定周天
							double updtotal = (iu.getModv71() * adjustmentValue)/1000.0;
							ip.setCaltotal(Math.round(updtotal));
							ip.setUpdtotal(Math.round(updtotal));
						}
					} else {
						if (week.equals("星期一")) {
							double updtotal = (iu.getModv11() * adjustmentValue)/1000.0;
							ip.setCaltotal(Math.round(updtotal));
							ip.setUpdtotal(Math.round(updtotal));
						} else if (week.equals("星期二")) {
							double updtotal = (iu.getModv21() * adjustmentValue)/1000.0;
							ip.setCaltotal(Math.round(updtotal));
							ip.setUpdtotal(Math.round(updtotal));
						} else if (week.equals("星期三")) {
							double updtotal = (iu.getModv31() * adjustmentValue)/1000.0;
							ip.setCaltotal(Math.round(updtotal));
							ip.setUpdtotal(Math.round(updtotal));
						} else if (week.equals("星期四")) {
							double updtotal = (iu.getModv41() * adjustmentValue)/1000.0;
							ip.setCaltotal(Math.round(updtotal));
							ip.setUpdtotal(Math.round(updtotal));
						} else if (week.equals("星期五")) {
							double updtotal = (iu.getModv51() * adjustmentValue)/1000.0;
							ip.setCaltotal(Math.round(updtotal));
							ip.setUpdtotal(Math.round(updtotal));
						} else {
							double Calvalue1 = (iu.getModv11() * adjustmentValue)/1000.0;
							double Calvalue2 = (iu.getModv21() * adjustmentValue)/1000.0;
							double Calvalue3 = (iu.getModv31() * adjustmentValue)/1000.0;
							double Calvalue4 = (iu.getModv41() * adjustmentValue)/1000.0;
							double Calvalue5 = (iu.getModv51() * adjustmentValue)/1000.0;
							//周一到周五早班平均值
							double Caltotal = (Calvalue1 + Calvalue2 + Calvalue3 + Calvalue4 + Calvalue5)/5;
							ip.setCaltotal(Math.round(Caltotal));//全天预估值
							ip.setUpdtotal(Math.round(Caltotal));//全天预估值
						}
					}
					posItemPlanMapper.insertItemPlan(ip);
				}
			}
			return 1;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/***
	 * 设置营业预估
	 * @param salePlanList
	 * @param vplantyp
	 * @return
	 * @throws CRUDException
	 */
	private Map<String,Map<String,String>> getBusinessForecast(List<SalePlan> salePlanList, String vplantyp)throws CRUDException{
		Map<String,Map<String,String>> mapList = new HashMap<String,Map<String,String>>();
		for(SalePlan sp : salePlanList){
			Map<String,String> map = new HashMap<String,String>();
			if ("PAX".equals(vplantyp)) {
				map.put("predictedValue", sp.getPeople_t()+"");
				map.put("adjustmentValue", sp.getPeople_t()+"");
			} else if ("AMT".equals(vplantyp)) {
				map.put("predictedValue", sp.getMoney_t()+"");
				map.put("adjustmentValue", sp.getMoney_t()+"");
			} else if ("TC".equals(vplantyp)){
				map.put("predictedValue", sp.getBills_t()+"");
				map.put("adjustmentValue", sp.getBills_t()+"");
			}
			mapList.put(DateJudge.YYYY_MM_DD.format(sp.getDat()), map);
		}
		return mapList;
	}

	/***
	 * 保存菜品销售计划
	 * @param posItemPlan
	 * @param firm
	 * @throws CRUDException
	 */
	public void updateItemPlan(ItemPlan posItemPlan, String firm) throws CRUDException {
		try {
			for (int i = 0; i < posItemPlan.getListItemPlan().size(); i++) {//批量修改
				ItemPlan posItemPlan_=new ItemPlan();
				posItemPlan_.setFirm(firm);
				posItemPlan_.setItem(posItemPlan.getListItemPlan().get(i).getItem());
				posItemPlan_.setDat(posItemPlan.getListItemPlan().get(i).getDat());
				posItemPlan_.setUpdtotal(posItemPlan.getListItemPlan().get(i).getUpdtotal());
				
				posItemPlanMapper.updateItemPlan(posItemPlan_);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/***
	 * 导出菜品销售计划
	 * @param os
	 * @param list
	 * @param listStr
	 * @return
	 */
	public boolean exportExcelPlan(OutputStream os,List<ItemPlan> list,List<String> listStr) {   
		WritableWorkbook workBook = null;
		try {
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet("Sheet1", 0);
            sheet.addCell(new Label(0,0,"分店菜品销售预估"));
            //设置表格表头
            sheet.addCell(new Label(0, 2, "部门"));      
            sheet.addCell(new Label(1, 2, "编码"));      
            sheet.addCell(new Label(2, 2, "名称"));      
            sheet.addCell(new Label(3, 2, "单位"));  
            for (int i=0;i<listStr.size();i++) {
            	sheet.addCell(new Label(4+i*2, 2, listStr.get(i) +"|参考")); 
            	sheet.addCell(new Label(5+i*2, 2, listStr.get(i) +"|预估")); 
			}
            sheet.mergeCells(0, 0, 11, 1);
            //遍历list填充表格内容
            for(int i=0;i<list.size();i++) {
            	sheet.addCell(new Label(0, i+3, list.get(i).getDept()));                     
            	sheet.addCell(new Label(1, i+3, list.get(i).getItcode()));                     
            	sheet.addCell(new Label(2, i+3, list.get(i).getItdes()));                      
            	sheet.addCell(new Label(3, i+3, list.get(i).getItunit()));
            	for (int j=0;j<listStr.size();j++) {
            		sheet.addCell(new Label(4+j*2, i+3, ValueUtil.getStringValue(list.get(i).getItemplanlist().get(j).get("cal"))));    
            		sheet.addCell(new Label(5+j*2, i+3, ValueUtil.getStringValue(list.get(i).getItemplanlist().get(j).get("upd"))));       
    			}
	    	}
            workBook.write();
            os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

}