package com.choice.misboh.service.salesforecast;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.domain.salesforecast.FirmItemUse;
import com.choice.misboh.domain.salesforecast.ItemPlan;
import com.choice.misboh.domain.salesforecast.SalePlan;
import com.choice.misboh.domain.util.Holiday;
import com.choice.misboh.persistence.common.CommonMISBOHMapper;
import com.choice.misboh.persistence.salesforecast.ItemUseChoice3Mapper;
import com.choice.misboh.persistence.salesforecast.PosItemPlanChoice3Mapper;
import com.choice.scm.service.ForecastService;

@Service
public class PosItemPlanChoice3Service {
	
	@Autowired
	private PosItemPlanChoice3Mapper posItemPlanChoice3Mapper;
	@Autowired
	private ItemUseChoice3Mapper itemUseChoice3Mapper;
	@Autowired
	private CommonMISBOHMapper  commonMISBOHMapper;
	

	private final transient Log log = LogFactory.getLog(ForecastService.class);


//	/**
//	 * 查找预估菜品销售计划
//	 * @param itemPlan
//	 * @return
//	 * @throws CRUDException
//	 */
//	public List<ItemPlan> findPosItemPlan(ItemPlan itemPlan) throws CRUDException
//	{
//		try {			
//			return posItemPlanChoice3Mapper.findPosItemPlan(itemPlan) ;			
//		} catch (Exception e) {
//			log.error(e);
//			throw new CRUDException(e);
//		}
//	}
	
	/**
	 * 查找预估菜品销售计划
	 * @param itemPlan
	 * @return
	 * @throws CRUDException
	 */
	public List<ItemPlan> findPosItemPlan(ItemPlan itemPlan) throws CRUDException
	{
		try {
			//1.日期时间段
			List<String> listStr = new ArrayList<String>();//日期时间段
			listStr = DateJudge.getDateList(DateJudge.getStringByDate(itemPlan.getBdate(), "yyyy-MM-dd"), DateJudge.getStringByDate(itemPlan.getEdate(), "yyyy-MM-dd"));
			//2.用来查询某个时间段内所有菜品销售计划 
			List<ItemPlan> listItemPlan = posItemPlanChoice3Mapper.findPosItemPlan(itemPlan) ;			
			if (listItemPlan.size() <= 0) {
				return null;
			}
			//3.用来放点击率的数据
			List<FirmItemUse> ListFirmItemUse = new ArrayList<FirmItemUse>();
			FirmItemUse f = new FirmItemUse();
			f.setFirm(itemPlan.getFirm());
			f.setDept(ValueUtil.getStringValue(itemPlan.getDept()));
//			if ("Y".equals(positn.getYnUseDept())) {
			ListFirmItemUse = itemUseChoice3Mapper.findAllItemUse(f);
//			} else {
//				li = itemUseChoice3Mapper.findAllItem(f);
//			}
			List<ItemPlan> list = new ArrayList<ItemPlan>();// 最终返回的菜品销售计划list   
//			Map<String,Map<String,String>> listPcodeMap = getDeptByPubitemCode(itemPlan.getFirm());
			//4.循环点击率
			for (FirmItemUse firmItemUse : ListFirmItemUse) {
				//5.  每行一个 销售计划对象，放入 销售计划list 中
				ItemPlan ip = new ItemPlan();//  最终返回的菜品销售计划list  中 ，每一行的对象
				ip.setFirm(firmItemUse.getFirm());
				//查询部门信息
//				Map<String,String> map = listPcodeMap.get(firmItemUse.getItcode());
//				if (map == null) {
//					continue;
//					if (map != null && itemPlan.getDept() != null && itemPlan.getDept().equals(ValueUtil.getStringValue(map.get("vcode")))) {
//					ip.setDept(map.get("vcode"));
//					ip.setDeptdes(map.get("vname"));
//					ip.setDeptdes(ValueUtil.getStringValue(map.get("vcode")));
//				}
//				ip.setDept(map.get("vcode"));
//				ip.setDeptdes(map.get("vname"));
				ip.setItem(firmItemUse.getItem());
				ip.setItcode(firmItemUse.getItcode());
				ip.setItdes(firmItemUse.getItdes());
				ip.setItunit(ValueUtil.getStringValue(firmItemUse.getItunit()));
				ip.setDeptdes(firmItemUse.getDeptdes());
				List<ItemPlan> listDate = new ArrayList<ItemPlan>();
				//6. 每一行 销售计划对象 中 加入  list日期的内容 
				List<Map<String,Object>> listmap = new ArrayList<Map<String,Object>>();   //修改用   
				for (String str : listStr) {
					ItemPlan i = new ItemPlan();
					Map<String,Object> listDateMap = new HashMap<String,Object>();
					for (ItemPlan iplst : listItemPlan) {
						if (!str.equals(DateJudge.getStringByDate(iplst.getDat(), "yyyy-MM-dd"))) {
							continue;
						}
						if (firmItemUse.getItcode().equals(iplst.getItcode())) {
							i.setCnt2(iplst.getCnt2());//午餐调整
							i.setCnt2old(iplst.getCnt2old());//午餐预估
							i.setCnt3(iplst.getCnt3());
							i.setCnt3old(iplst.getCnt3old());
							i.setCaltotal(iplst.getCaltotal());//计算值
							i.setUpdtotal(iplst.getUpdtotal());//调整值
							listDateMap.put("cnt2", iplst.getCnt2());
							listDateMap.put("cnt2old", iplst.getCnt2old());
							listDateMap.put("cnt3", iplst.getCnt3());
							listDateMap.put("cnt3old", iplst.getCnt3old());
							listDateMap.put("cal", iplst.getCaltotal());
							listDateMap.put("upd", iplst.getUpdtotal());
							listDateMap.put("dat", str);
							listDateMap.put("deptdes",iplst.getDeptdes());
						}
					}
					listDate.add(i);
					listmap.add(listDateMap);
				}
				ip.setItemplanlist(listmap);
				list.add(ip);
			}
			return list;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 重新计算菜品销售计划
	 * @return
	 * @throws CRUDException
	 */
	public void saveCalPlan(ItemPlan posItemPlan, List<SalePlan> salePlanList, List<FirmItemUse> listItemUse) throws CRUDException{
		try {
			//1.删掉这段时间的菜品销售计划
			posItemPlanChoice3Mapper.deleteAllPlan(posItemPlan);		
			List<String> listStr = DateJudge.getDateList(DateJudge.getStringByDate(posItemPlan.getBdate(), "yyyy-MM-dd"), DateJudge.getStringByDate(posItemPlan.getEdate(), "yyyy-MM-dd"));
			
			List<Holiday> holidayList = commonMISBOHMapper.getHoliday(DateFormat.getStringByDate(posItemPlan.getStartdate(),"yyyy-MM-dd"),
					DateFormat.getStringByDate(posItemPlan.getEnddate(),"yyyy-MM-dd"));
			List<ItemPlan> list = new ArrayList<ItemPlan>();
			for (int i = 0; i < listItemUse.size(); i++) {//2.循环所有的点击率
				FirmItemUse firmItemUse = listItemUse.get(i);
				for (int j = 0; j < listStr.size(); j++) {//3.循环日期列表
					ItemPlan itemPlan = new ItemPlan();//菜品销售计划实例
					itemPlan.setFirm(posItemPlan.getFirm());
					itemPlan.setDept(firmItemUse.getDept());//部门
					itemPlan.setDeptdes(firmItemUse.getDeptdes());
					itemPlan.setDept1(firmItemUse.getDept1());//部门
					itemPlan.setDept1des(firmItemUse.getDept1des());
					itemPlan.setItem(firmItemUse.getItem());
					itemPlan.setItcode(firmItemUse.getItcode());//菜品编码
					itemPlan.setItdes(firmItemUse.getItdes());//菜品名称
					itemPlan.setItunit(firmItemUse.getItunit());//单位
					String date = listStr.get(j);
					itemPlan.setDat(DateJudge.getDateByString(date));//营业日
					double adjustmentValue = 0;//全天
					double pax2 = 0;//午餐调整
					double pax3 = 0;//晚餐调整
					// 读取预估值
					if(salePlanList!=null){
						for (SalePlan sale : salePlanList) {
							if(itemPlan.getDat().getTime() == sale.getDat().getTime()){//如果是预估的这一天
								adjustmentValue = sale.getMoney();//全天
								pax2=sale.getPax2();//午餐调整
								pax3=sale.getPax3();//晚餐调整
								break;
							}
						}
					}
					int grade = 0;
//						判断是否是节假日
					if(holidayList!=null){
						for (Holiday holiday : holidayList) {
							if(date.equals(holiday.getDholidaydate())){
								grade = holiday.getVholidaygrade();
								break;
							}
						}
					}
					if (grade >= 2) {//法定节假日
						double updtotal = ((firmItemUse.getHcnt2()+firmItemUse.getHcnt3())/2 * adjustmentValue)/1000.0;//全天
						itemPlan.setCnt2(Math.round(firmItemUse.getHcnt2() * pax2/1000.0));//午餐调整
						itemPlan.setCnt2old(Math.round(firmItemUse.getHcnt2() * pax2/1000.0));
						itemPlan.setCnt3(Math.round(firmItemUse.getHcnt3() * pax3/1000.0));//晚餐调整
						itemPlan.setCnt3old(Math.round(firmItemUse.getHcnt3() * pax3/1000.0));
						itemPlan.setCaltotal(Math.round(updtotal));
						itemPlan.setUpdtotal(Math.round(updtotal));
					} else {
						double updtotal = ((firmItemUse.getCnt2()+firmItemUse.getCnt3())/2 * adjustmentValue)/1000.0;//全天
						itemPlan.setCnt2(Math.round(firmItemUse.getCnt2() * pax2/1000.0));//午餐调整
						itemPlan.setCnt2old(Math.round(firmItemUse.getCnt2() * pax2/1000.0));
						itemPlan.setCnt3(Math.round(firmItemUse.getCnt3() * pax3/1000.0));//晚餐调整
						itemPlan.setCnt3old(Math.round(firmItemUse.getCnt3() * pax3/1000.0));
						itemPlan.setCaltotal(Math.round(updtotal));
						itemPlan.setUpdtotal(Math.round(updtotal));
					}
					list.add(itemPlan);
				}
			}
			posItemPlanChoice3Mapper.insertPosItemPlanBatch(list);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存修改调整量(预估菜品销售计划)
	 * @param posSalePlan
	 * @throws CRUDException
	 */
	public void updateItemPlan(ItemPlan itemPlan) throws CRUDException {
		try {
			for (int i = 0; i < itemPlan.getListItemPlan().size(); i++) {//批量修改
				ItemPlan ip = itemPlan.getListItemPlan().get(i);
				ip.setFirm(itemPlan.getFirm());
				ip.setCaltotal(ip.getCnt2()+ip.getCnt3());
				ip.setUpdtotal(ip.getCnt2()+ip.getCnt3());
				posItemPlanChoice3Mapper.updateItemPlan(ip);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除所有预估菜品销售计划
	 * @param
	 * @throws CRUDException
	 */
	public void deleteAllPlan(ItemPlan itemPlan) throws CRUDException{
		try{
			posItemPlanChoice3Mapper.deleteAllPlan(itemPlan);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	//导出菜品销售计划Excel 
	public boolean exportExcelPlan(OutputStream os,List<ItemPlan> list,List<String> listStr) {   
		WritableWorkbook workBook = null;
		try {
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet("Sheet1", 0);
            sheet.addCell(new Label(0,0,"分店菜品销售预估"));
            //设置表格表头
            sheet.addCell(new Label(0, 2, "部门"));      
            sheet.addCell(new Label(1, 2, "编码"));      
            sheet.addCell(new Label(2, 2, "名称"));      
            sheet.addCell(new Label(3, 2, "单位"));  
            for (int i=0;i<listStr.size();i++) {
            	sheet.addCell(new Label(4+i*4, 2, listStr.get(i) +"|午餐计算")); 
            	sheet.addCell(new Label(5+i*4, 2, listStr.get(i) +"|午餐调整")); 
            	sheet.addCell(new Label(6+i*4, 2, listStr.get(i) +"|晚餐计算")); 
            	sheet.addCell(new Label(7+i*4, 2, listStr.get(i) +"|晚餐调整")); 
			}
            sheet.mergeCells(0, 0, 11, 1);
            //遍历list填充表格内容
            for(int i=0;i<list.size();i++) {
            	sheet.addCell(new Label(0, i+3, list.get(i).getDeptdes()));                     
            	sheet.addCell(new Label(1, i+3, list.get(i).getItcode()));                     
            	sheet.addCell(new Label(2, i+3, list.get(i).getItdes()));                      
            	sheet.addCell(new Label(3, i+3, list.get(i).getItunit()));
            	for (int j=0;j<listStr.size();j++) {
            		sheet.addCell(new Label(4+j*4, i+3, ValueUtil.getStringValue(list.get(i).getItemplanlist().get(j).get("cnt2old"))));    
            		sheet.addCell(new Label(5+j*4, i+3, ValueUtil.getStringValue(list.get(i).getItemplanlist().get(j).get("cnt2"))));  
            		sheet.addCell(new Label(6+j*4, i+3, ValueUtil.getStringValue(list.get(i).getItemplanlist().get(j).get("cnt3old"))));    
            		sheet.addCell(new Label(7+j*4, i+3, ValueUtil.getStringValue(list.get(i).getItemplanlist().get(j).get("cnt3"))));  
//            		sheet.addCell(new Label(4+j*2, i+3, ValueUtil.getStringValue(list.get(i).getItemplanlist().get(j).get("cal"))));    
//            		sheet.addCell(new Label(5+j*2, i+3, ValueUtil.getStringValue(list.get(i).getItemplanlist().get(j).get("upd"))));       
    			}
	    	}
            workBook.write();
            os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/********************************************************预估菜品销售计划end*************************************************/

		
}