package com.choice.misboh.service.salesforecast;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.domain.salesforecast.SalePlan;
import com.choice.misboh.domain.salesforecast.SalesForecastEnum;
import com.choice.misboh.persistence.salesforecast.PosSalePlanMapper;

@Service
public class PosSalePlanService {
	
	@Autowired
	private PosSalePlanMapper posSalePlanMapper;

	private final transient Log log = LogFactory.getLog(PosSalePlanService.class);

	
	/********************************************************营业预估start*************************************************/
	
	/***
	 * 查找营业预估
	 * @param posSalePlan
	 * @return
	 * @throws CRUDException
	 */
	public List<SalePlan> findPosSalePlan(SalePlan posSalePlan) throws CRUDException {
		try {
			List<SalePlan> listSalePlan = posSalePlanMapper.findPosSalePlan(posSalePlan);
			if (listSalePlan.size() <= 0) {
				return null;			
			}
			String nowDate = DateJudge.YYYY_MM_DD.format(new Date());
			double last_totalamt = 0d;//去年同期营业额合计
			double totalamt_y = 0d;//预估营业额合计
			double totalamt_t = 0d;//调整营业额合计
			double last_totalpercapita = 0d;//去年同期人均合计
			double totalpercapita_y = 0d;//预估人均合计
			double totalpercapita_t = 0d;//调整人均合计
			int last_totalpeople = 0;//去年同期人数合计
			int totalpeople_y = 0;//预估人数合计
			int totalpeople_t = 0;//调整人数合计
			int last_totaltc = 0;//去年同期单数合计
			int totaltc_y = 0;//预估单数合计
			int totaltc_t = 0;//调整单数合计
			double last_totalac = 0d;//去年同期单均合计
			double totalac_y = 0d;//预估单均合计
			double totalac_t = 0d;//调整但均合计
			for (int i = 0;i < listSalePlan.size(); i++) {
				SalePlan salePlan = listSalePlan.get(i);
				if (DateJudge.timeCompare(DateJudge.YYYY_MM_DD.format(salePlan.getDat()), nowDate)) {
					salePlan.setTrueorfalse(1);
				} else {
					salePlan.setTrueorfalse(0);
				}
				//周次
				salePlan.setWeek(DateJudge.getWeekOfDateToString(DateJudge.getStringByDate(salePlan.getDat(), "yyyy-MM-dd")));
				if ("1".equals(salePlan.getIsHoliday())) {
					salePlan.setIsHoliday("是");
				} else {
					salePlan.setIsHoliday("否");
				}
				last_totalamt += ValueUtil.getDoubleValue(salePlan.getLast_money());
				totalamt_y += ValueUtil.getDoubleValue(salePlan.getMoney_y());
				totalamt_t += ValueUtil.getDoubleValue(salePlan.getMoney_t());
				last_totalpercapita += ValueUtil.getDoubleValue(salePlan.getLast_percapita());
				totalpercapita_y += ValueUtil.getDoubleValue(salePlan.getPercapita_y());
				totalpercapita_t += ValueUtil.getDoubleValue(salePlan.getPercapita_t());
				last_totalpeople += ValueUtil.getIntValue(salePlan.getLast_people());
				totalpeople_y += salePlan.getPeople_y();
				totalpeople_t += salePlan.getPeople_t();
				last_totaltc += ValueUtil.getIntValue(salePlan.getLast_bills());
				totaltc_y += salePlan.getBills_y();
				totaltc_t += salePlan.getBills_t();
				last_totalac += ValueUtil.getDoubleValue(salePlan.getLast_ac());
				totalac_y += ValueUtil.getDoubleValue(salePlan.getAc_y());
				totalac_t += ValueUtil.getDoubleValue(salePlan.getAc_t());
			}
			SalePlan psp = new SalePlan();
			psp.setLast_money(last_totalamt);
			psp.setMoney_y(totalamt_y);
			psp.setMoney_t(totalamt_t);
			psp.setLast_percapita(last_totalpercapita);
			psp.setPercapita_t(totalpercapita_t);
			psp.setPercapita_y(totalpercapita_y);
			psp.setLast_people(last_totalpeople);
			psp.setPeople_y(totalpeople_y);
			psp.setPeople_t(totalpeople_t);
			psp.setLast_ac(last_totalac);
			psp.setAc_y(totalac_y);
			psp.setAc_t(totalac_t);
			psp.setLast_bills(last_totaltc);
			psp.setBills_t(totaltc_t);
			psp.setBills_y(totaltc_y);
			listSalePlan.add(psp);
			return listSalePlan;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/***
	 * 计算营业用过
	 * @param salePlan
	 * @throws CRUDException
	 */
	public void saveCalPosSalePlan(SalePlan salePlan) throws CRUDException{
		try{
			String firmId = salePlan.getFirm();
			String firmName = salePlan.getFirmNm();
			List<SalePlan> salePlanList = posSalePlanMapper.findBusinessData(salePlan);
			Map<String, Map<SalesForecastEnum, Object>> map = new HashMap<String, Map<SalesForecastEnum, Object>>();
			for(SalePlan sp : salePlanList){
				Map<SalesForecastEnum, Object> rowMap = new HashMap<SalesForecastEnum, Object>();
				rowMap.put(SalesForecastEnum.HOLIDAY, sp.getIsHoliday());
				rowMap.put(SalesForecastEnum.MONEY, sp.getMoney());
				rowMap.put(SalesForecastEnum.TC, sp.getTc());
				rowMap.put(SalesForecastEnum.PEOLENUM, sp.getPeoplenum());
				rowMap.put(SalesForecastEnum.WEEKDAY, DateJudge.getWeekOfDateToString(sp.getDworkdate()));
				map.put(sp.getDworkdate(),rowMap);
				
			}
			Map<String,Object> totalMap = new HashMap<String,Object>(); // 营业额
		    Map<String,Object> tcMap = new HashMap<String,Object>();// TC
		    Map<String,Object> peolenumMap = new HashMap<String,Object>(); // 人数
		    Map<SalesForecastEnum,Object> rowMap = null;//集合对象
		    Map<String,Integer> countMap = new HashMap<String,Integer>();
		    Object week = null; // 周次
		    Object isHoliday = "0"; // 判断是否节假日
		    Object total = "0";// 金额
		    Object tc = "0";//单数
		    Object peolenum = "0";//人数
		    if(map != null && map.size() > 0){// 判断是否存在参考数据
				for (String key : map.keySet()) {
				    rowMap = map.get(key);
				    if(rowMap != null){//获取行
						week = getMapValue(rowMap,SalesForecastEnum.WEEKDAY);
						isHoliday = getMapValue(rowMap,SalesForecastEnum.HOLIDAY);
						total = getMapValue(rowMap,SalesForecastEnum.MONEY);
						tc = getMapValue(rowMap,SalesForecastEnum.TC);
						peolenum = getMapValue(rowMap,SalesForecastEnum.PEOLENUM);
						// 判断是否节假日
						if(isHoliday.equals("0")){
						    if(ValueUtil.getDoubleValue(total) > 0){
								sumMapValue(totalMap,week,1,total);
								totalCountMap(countMap,week+"_money");
						    }
						    if(ValueUtil.getIntValue(tc) > 0){
								sumMapValue(tcMap,week,2,tc);
								totalCountMap(countMap,week+"_tc");
						    }
						    if(ValueUtil.getIntValue(peolenum) > 0){
								sumMapValue(peolenumMap,week,2,peolenum);
								totalCountMap(countMap,week+"_peolenum");
						    }
						    
						}else {
						    totalMap.put("holiday",ValueUtil.getDoubleValue(total));
						    tcMap.put("holiday",ValueUtil.getIntValue(tc));
						    peolenumMap.put("holiday",ValueUtil.getIntValue(peolenum));
						}
				    }   
				}
		    }
			// 计算最终每个周次预估结果
		    calculationForecast(totalMap,countMap,"_money",1);
		    calculationForecast(tcMap,countMap,"_tc",2);
		    calculationForecast(peolenumMap,countMap,"_peolenum",2);
		    Calendar bcal = Calendar.getInstance();
			Calendar ecal = Calendar.getInstance();
			bcal.setTime(salePlan.getStartdate());
			ecal.setTime(salePlan.getEnddate());
			ecal.add(Calendar.DATE, 1);     // 加一天
			// 按日期循环
			StringBuffer sql = new StringBuffer();
			List<Map<String,String>> mapList = new ArrayList<Map<String,String>>();
			while (!bcal.equals(ecal)) {
				Map<String,String> map1 = new HashMap<String,String>();
				String date = DateJudge.getStringByDate(bcal.getTime(),"yyyy-MM-dd");
				String lastDate = DateJudge.getLastYearDate(date);
				map1.put("date", date);
				map1.put("lastDate", lastDate);
				map1.put("firm", firmId);
				mapList.add(map1);
				bcal.add(Calendar.DATE, 1);
			}
			List<SalePlan> listData = posSalePlanMapper.getCalPosSalePlan(mapList);
		    for (SalePlan p : listData) {
		    	// 去年同期  - 营业额
				Double moneyData = p.getMoney();
				// 去年同期  -单数
				Integer tcData = p.getTc();
				// 去年同期  - 人数
				Integer peopleData = p.getPeoplenum();
				
				Double money_y = 0D;
				Integer tc_y = 0;
				Integer people_y = 0;
				String isholiday = p.getIsHoliday();
				String dat = p.getDworkdate();
				String datWeek = DateJudge.getWeekOfDateToString(dat);
				if (totalMap.size() > 7) {
					if ("1".equals(isholiday)) {
						money_y = Double.parseDouble(totalMap.get("holiday").toString());
					} else {
						money_y = Double.parseDouble(totalMap.get(datWeek).toString());
					}
				} else {
					money_y = Double.parseDouble(totalMap.get(datWeek).toString());
				}
				if (tcMap.size() > 7) {
					if ("1".equals(isholiday)) {
						tc_y = Integer.parseInt(tcMap.get("holiday").toString());
					} else {
						tc_y = Integer.parseInt(tcMap.get(datWeek).toString());
					}
				} else {
					tc_y = Integer.parseInt(tcMap.get(datWeek).toString());
				}
				if (peolenumMap.size() > 7) {
					if ("1".equals(isholiday)) {
						people_y = Integer.parseInt(peolenumMap.get("holiday").toString());
					} else {
						people_y = Integer.parseInt(peolenumMap.get(datWeek).toString());
					}
				} else {
					people_y = Integer.parseInt(peolenumMap.get(datWeek).toString());
				}
				//门店编码
				p.setFirm(firmId);
				//门店名称
				p.setFirmNm(firmName);
				// 日期
				p.setDat(DateFormat.getDateByString(dat, "yyyy-MM-dd"));
				// 周次
				p.setWeek(datWeek);
				// 特殊事件 
				p.setMemo("");
				// 节假日
				p.setIsHoliday(isholiday);
				// 营业额 -去年同期
				p.setLast_money(moneyData);
				// 营业额 -预估值
				p.setMoney_y(money_y);
				// 营业额 -调整值
				p.setMoney_t(money_y);
				// 单均 -去年同期环比
				if(tcData <= 0 || moneyData <= 0){
					p.setLast_ac(ValueUtil.getDoubleValue(FormatDouble.formatDoubleLength(0d)));
				}else {
					p.setLast_ac(ValueUtil.getDoubleValue(FormatDouble.formatDoubleLength(moneyData/tcData)));
				}
				// 单均 -预估值
				if(tc_y > 0){
					p.setAc_y(ValueUtil.getDoubleValue(FormatDouble.formatDoubleLength(money_y/tc_y)));
					p.setAc_t(ValueUtil.getDoubleValue(FormatDouble.formatDoubleLength(money_y/tc_y)));
				}else {
					p.setAc_y(ValueUtil.getDoubleValue(FormatDouble.formatDoubleLength(0d)));
					p.setAc_t(ValueUtil.getDoubleValue(FormatDouble.formatDoubleLength(0d)));
				}
				// 单均 -调整值
				// 单数 -去年同期环比
				p.setLast_bills(tcData);
				// 单数 -预估值
				p.setBills_y(ValueUtil.getIntValue(tc_y));
				// 单数 -调整值
				p.setBills_t(ValueUtil.getIntValue(tc_y));
				// 人均 -去年同期环比
				if(peopleData <= 0 || moneyData <= 0){
					p.setLast_percapita(ValueUtil.getDoubleValue(FormatDouble.formatDoubleLength(0d)));
				}else {
					p.setLast_percapita(ValueUtil.getDoubleValue(FormatDouble.formatDoubleLength(moneyData/peopleData)));
				}
				// 人均 -预估值
				// 人均 -调整值
				if(people_y > 0){
					p.setPercapita_y(ValueUtil.getDoubleValue(FormatDouble.formatDoubleLength(money_y/people_y)));
					p.setPercapita_t(ValueUtil.getDoubleValue(FormatDouble.formatDoubleLength(money_y/people_y)));
				}else {
					p.setPercapita_y(ValueUtil.getDoubleValue(FormatDouble.formatDoubleLength(0d)));
					p.setPercapita_t(ValueUtil.getDoubleValue(FormatDouble.formatDoubleLength(0d)));
				}
				// 人数 -去年同期环比
				p.setLast_people(ValueUtil.getIntValue(peopleData));
				// 人数 -预估值
				p.setPeople_y(ValueUtil.getIntValue(people_y));
				// 人数 -调整值
				p.setPeople_t(ValueUtil.getIntValue(people_y));
				posSalePlanMapper.deletePosSalePlan(p);
				posSalePlanMapper.insertPosSalePlan(p);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 操作MAP对象结果
	 * @author 文清泉 2014年9月29日 下午4:10:26
	 * @param map 当前操作MAP 
	 * @param week KEY值
	 * @param typ 类型 1 DOUBLE 2 INTEGER
	 * @param value 修改值
	 */
	private void sumMapValue(Map<String, Object> map,Object week,int typ, Object value){
	    if(typ == 1){
		if(map.get(week) != null){
		    map.put(week.toString(),Double.parseDouble(value.toString())+Double.parseDouble(map.get(week).toString()));
		}else{
		    map.put(week.toString(),Double.parseDouble(value.toString()));
		}
	    }else if(typ == 2){
		if(map.get(week) != null){
		    map.put(week.toString(),Integer.parseInt(value.toString())+Integer.parseInt(map.get(week).toString()));
		}else{
		    map.put(week.toString(),Integer.parseInt(value.toString()));
		}
	    }
	}
	/**
	 * 
	 * @author 文清泉2014年9月29日 下午4:12:58
	 * @param countMap
	 */
	private void totalCountMap(Map<String, Integer> countMap,String key) {
	   if(countMap.get(key) != null){
	       countMap.put(key,countMap.get(key)+1);
	   }else{
	       countMap.put(key,1);
	   }
	       
	}
	/**
	 * 获取对应MAP对象值
	 * @author 文清泉 2014年9月29日 下午3:23:41
	 */
	private Object getMapValue(Map<SalesForecastEnum,Object> map,SalesForecastEnum key){
	    if(map.get(key) != null && !map.get(key).equals("")){
		return map.get(key);
	    }else {
		return "";
	    }
	}
	
	/**
	 * 计算最终预估值
	 * @author 文清泉 2014年9月29日 下午4:37:49
	 * @param typKey 
	 * @param
	 */
	private void calculationForecast(Map<String, Object> totalMap,Map<String, Integer> countMap, String typKey,int typ) {
	    String[] week = new String[]{"一","二","三","四","五","六","日"};
	    Integer count = 0;
	    Object value;
	    for (int i = 0; i < week.length; i++) {
			value = totalMap.get(week[i]);
			count = countMap.get(week[i]+typKey);
			if(value == null){
			    totalMap.put(week[i],0);
			    continue;
			}
			if(typ == 1){
			    if(Double.parseDouble(value.toString()) > 0 && count > 0){
				totalMap.put(week[i],Double.parseDouble(value.toString())/count);
			    }
			}else if(typ == 2){
			    if(Integer.parseInt(value.toString()) > 0 && count > 0){
				totalMap.put(week[i],Integer.parseInt(value.toString())/count);
			    }
			}
	    }
	}
	
	/**
	 * 保存修改调整量(营业预估)
	 * @param posSalePlan
	 * @param scode
	 * @throws CRUDException
	 */
	public void updateSalePlan(SalePlan posSalePlan, String scode) throws CRUDException {
		try {
			for (int i = 0; i < posSalePlan.getSalePlanList().size(); i++) {//批量修改
				SalePlan posSalePlan_ = posSalePlan.getSalePlanList().get(i);
				posSalePlan_.setFirm(scode);
				posSalePlanMapper.updateSalePlan(posSalePlan_);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/***
	 * 导出营业预估
	 * @param os
	 * @param list
	 * @return
	 */
	public boolean exportExcelcast(OutputStream os,List<SalePlan> list) {   
		WritableWorkbook workBook = null;
		try {
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet("Sheet1", 0);
			sheet.mergeCells(0, 0, 18, 1);
            sheet.addCell(new Label(0,0,"营业预估"));
            //设置表格表头
            sheet.addCell(new Label(0, 2, "日期"));      
            sheet.addCell(new Label(1, 2, "星期"));      
            sheet.addCell(new Label(2, 2, "假日"));      
            sheet.addCell(new Label(3, 2, "特殊事件"));  
            sheet.addCell(new Label(4, 2, "营业额|去年同期")); 
            sheet.addCell(new Label(5, 2, "营业额|预估")); 
            sheet.addCell(new Label(6, 2, "营业额|调整")); 
            sheet.addCell(new Label(7, 2, "人均|去年同期")); 
            sheet.addCell(new Label(8, 2, "人均|预估")); 
            sheet.addCell(new Label(9, 2, "人均|调整")); 
            sheet.addCell(new Label(10, 2, "人数|去年同期")); 
            sheet.addCell(new Label(11, 2, "人数|预估")); 
            sheet.addCell(new Label(12, 2, "人数|调整")); 
            sheet.addCell(new Label(13, 2, "单均|去年同期")); 
            sheet.addCell(new Label(14, 2, "单均|预估")); 
            sheet.addCell(new Label(15, 2, "单均|调整")); 
            sheet.addCell(new Label(16, 2, "单数|去年同期")); 
            sheet.addCell(new Label(17, 2, "单数|预估")); 
            sheet.addCell(new Label(18, 2, "单数|调整")); 

            SimpleDateFormat sdf =  new SimpleDateFormat("yyyy-MM-dd"); 
            //遍历list填充表格内容
            for(int i=0;i<list.size();i++) {
            	if (i == list.size() - 1) {
            		sheet.addCell(new Label(0, i+3, "合计"));                  
				}else{
					sheet.addCell(new Label(0, i+3, String.valueOf(sdf.format(list.get(i).getDat()))));                  
				}
            	sheet.addCell(new Label(1, i+3, list.get(i).getWeek()));            
        		sheet.addCell(new Label(2, i+3, list.get(i).getIsHoliday()));                                      
            	sheet.addCell(new Label(3, i+3, list.get(i).getMemo()));                                      
            	sheet.addCell(new Label(4, i+3, ValueUtil.getStringValue(list.get(i).getLast_money())));
            	sheet.addCell(new Label(5, i+3, ValueUtil.getStringValue(list.get(i).getMoney_y())));   
            	sheet.addCell(new Label(6, i+3, ValueUtil.getStringValue(list.get(i).getMoney_t())));    
            	sheet.addCell(new Label(7, i+3, ValueUtil.getStringValue(list.get(i).getLast_percapita())));       
            	sheet.addCell(new Label(8, i+3, ValueUtil.getStringValue(list.get(i).getPercapita_y())));    
            	sheet.addCell(new Label(9, i+3, ValueUtil.getStringValue(list.get(i).getPercapita_t())));       
            	sheet.addCell(new Label(10, i+3, ValueUtil.getStringValue(list.get(i).getLast_people())));   
            	sheet.addCell(new Label(11, i+3, ValueUtil.getStringValue(list.get(i).getPeople_y())));      
            	sheet.addCell(new Label(12, i+3, ValueUtil.getStringValue(list.get(i).getPeople_t())));  
            	sheet.addCell(new Label(13, i+3, ValueUtil.getStringValue(list.get(i).getLast_ac())));   
            	sheet.addCell(new Label(14, i+3, ValueUtil.getStringValue(list.get(i).getAc_y())));   
            	sheet.addCell(new Label(15, i+3, ValueUtil.getStringValue(list.get(i).getAc_t())));   
            	sheet.addCell(new Label(16, i+3, ValueUtil.getStringValue(list.get(i).getLast_bills())));   
            	sheet.addCell(new Label(17, i+3, ValueUtil.getStringValue(list.get(i).getBills_y())));   
            	sheet.addCell(new Label(18, i+3, ValueUtil.getStringValue(list.get(i).getBills_t())));   
	    	}
            workBook.write();
            os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

}