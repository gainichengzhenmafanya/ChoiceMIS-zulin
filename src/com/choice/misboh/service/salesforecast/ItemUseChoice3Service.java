package com.choice.misboh.service.salesforecast;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.domain.salesforecast.FirmItemUse;
import com.choice.misboh.domain.store.Store;
import com.choice.misboh.domain.util.Holiday;
import com.choice.misboh.persistence.common.CommonMISBOHMapper;
import com.choice.misboh.persistence.salesforecast.ItemUseChoice3Mapper;
import com.choice.orientationSys.util.Page;
import com.choice.scm.service.ForecastService;

@Service
public class ItemUseChoice3Service {
	
	@Autowired
	private ItemUseChoice3Mapper itemUseChoice3Mapper;
	@Autowired
	private CommonMISBOHMapper commonMISBOHMapper;
	private final transient Log log = LogFactory.getLog(ForecastService.class);

	
	/**
	 * 查找所有的菜品点击率
	 */
	public List<FirmItemUse> findAllItemUse(FirmItemUse firmItemUse, Page page) throws CRUDException
	{
		try {
//			return pageManager.selectPage(firmItemUse, page, ItemUseChoice3Mapper.class.getName()+".findAllItemUse");	
			return itemUseChoice3Mapper.findAllItemUse(firmItemUse);			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查找所有的菜品点击率
	 */
	public List<FirmItemUse> findAllItemUse(FirmItemUse firmItemUse) throws CRUDException
	{
		try {
			return itemUseChoice3Mapper.findAllItemUse(firmItemUse);			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除所有菜品点击率
	 */
	public void deleteAll(FirmItemUse firmItemUse) throws CRUDException{
		try{
			for (FirmItemUse itemUse : firmItemUse.getItemUseList()) {
				itemUse.setFirm(firmItemUse.getFirm());
				itemUseChoice3Mapper.delete(itemUse);
			}
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 重新计算点击率
	 */
	public void saveCalculate(FirmItemUse firmItemUse) throws CRUDException
	{
		try {
			//3. 获取每个营业日的数据合计
			List<FirmItemUse> listPaxAmtTc= itemUseChoice3Mapper.findAllPaxAmtTc(firmItemUse);
			//如果没有营业数据，直接返回 
			if(listPaxAmtTc.size()==0){
				return ;
			}
			//1.删除这个点已经有的点击率 
			itemUseChoice3Mapper.deleteAll(firmItemUse);
			//2.获取点击率表达方式（单数，营业额，人数）
			Store store = new Store();
	    	store.setVcode(firmItemUse.getFirm());
	    	store = commonMISBOHMapper.getStore(store);
			String typ = store.getVplantyp();//AMT 营业额    TC单数       PAX人数
			if(null == typ || "".equals(typ))
				typ = "PAX";
			//4. 获取各班次某道菜的数量
			List<FirmItemUse> listItem =itemUseChoice3Mapper.findAllItem(firmItemUse);
			//5.获取点击率的时间之内的节假日
			List<Holiday> holidayList = commonMISBOHMapper.getHoliday(DateFormat.getStringByDate(firmItemUse.getBdate(),"yyyy-MM-dd"),
					DateFormat.getStringByDate(firmItemUse.getEdate(),"yyyy-MM-dd"));
			
			//计算 步骤三 
			//数组值依次代表，工作日一班，工作日二班，工作日三班，工作日四班，节假日一班，节假日二班，节假日三班，节假日四班
			double[] a =  new double[]{0,0,0,0,0,0,0,0};
			for (int i = 0; i < listPaxAmtTc.size(); i++) {
				FirmItemUse paxAmtTC=listPaxAmtTc.get(i);
				int sft  = Integer.parseInt(paxAmtTC.getSft()); //班次
				double num=0;
				if("PAX".equals(typ)){
					 num = Double.valueOf(paxAmtTC.getPax());   // 人数
				}else if("TC".equals(typ)){
					 num = Double.valueOf(paxAmtTC.getTc());   // 单数
				}else{
					 num = Double.valueOf(paxAmtTC.getAmt());   // 金额
				}
				if (isHoliday(paxAmtTC.getDat(),holidayList)) {//判断如果是假期
					a[(4+sft)] += num;
				}else {
					a[sft] += num;
				}
			}
			Set<String> set = new HashSet<String>();
			List<FirmItemUse> list = new ArrayList<FirmItemUse>();
			for (int i = 0; i < listItem.size(); i++) {
				FirmItemUse items=listItem.get(i);
				if(set.contains(items.getItem()))
					continue;
				set.add(items.getItem());
				// 计算这道菜的点击率
				if (a[1]!=0) {
					items.setCnt2(Math.round(1000.0*items.getCnt2()/a[1]));
				}else {
					items.setCnt2(0);
				}
				if (a[2]!=0) {
					items.setCnt3(Math.round(1000.0*items.getCnt3()/a[2]));
				}else {
					items.setCnt3(0);
				}
				if (a[5]!=0) {
					items.setHcnt2(Math.round(1000.0*items.getHcnt2()/a[5]));
				}else {
					items.setHcnt2(0);
				}
				if (a[6]!=0) {
					items.setHcnt3(Math.round(1000.0*items.getHcnt3()/a[6]));
				}else {
					items.setHcnt3(0);
				}
				//把这道菜的点击率统计信息添加到表
				items.setFirm(firmItemUse.getFirm());
				list.add(items);
			}
			itemUseChoice3Mapper.insertItemUseBatch(list);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	
	/**
	 * 判断是否节假日
	 */
	public boolean isHoliday(Date date,List<Holiday> holidayList) throws CRUDException { 
		for(Holiday h : holidayList){
			if(h.getDholidaydate().equals(DateFormat.getStringByDate(date, "yyyy-MM-dd"))){//是节假日
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 保存修改调整量(菜品点击率)
	 */
	public void update(FirmItemUse firmItemUse) throws CRUDException {
		try {
			for (int i = 0; i < firmItemUse.getItemUseList().size(); i++) {//批量修改
				FirmItemUse itemUse_=new FirmItemUse();
				itemUse_.setItem(firmItemUse.getItemUseList().get(i).getItem());
				itemUse_.setCnt1(firmItemUse.getItemUseList().get(i).getCnt1());
				itemUse_.setCnt2(firmItemUse.getItemUseList().get(i).getCnt2());
				itemUse_.setCnt3(firmItemUse.getItemUseList().get(i).getCnt3());
				itemUse_.setCnt4(firmItemUse.getItemUseList().get(i).getCnt4());
				
				itemUse_.setHcnt1(firmItemUse.getItemUseList().get(i).getHcnt1());
				itemUse_.setHcnt2(firmItemUse.getItemUseList().get(i).getHcnt2());
				itemUse_.setHcnt3(firmItemUse.getItemUseList().get(i).getHcnt3());
				itemUse_.setHcnt4(firmItemUse.getItemUseList().get(i).getHcnt4());
				itemUse_.setFirm(firmItemUse.getFirm());
				itemUseChoice3Mapper.update(itemUse_);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	//导出菜品点击率Excel 
	public boolean exportExcel(OutputStream os,List<FirmItemUse> list) {   
		WritableWorkbook workBook = null;
		try {
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet("Sheet1", 0);
            sheet.addCell(new Label(0,0,"菜品点击率"));
            //设置表格表头
            sheet.addCell(new Label(0, 2, "菜品编码"));   
			sheet.addCell(new Label(1, 2, "菜品名称"));  
			sheet.addCell(new Label(2, 2, "单位"));   
//			sheet.addCell(new Label(3, 2, "工作日|一班|计算"));  
//			sheet.addCell(new Label(4, 2, "工作日|一班|调整"));   
			sheet.addCell(new Label(3, 2, "工作日|午餐|计算"));  
			sheet.addCell(new Label(4, 2, "工作日|午餐|调整"));   
			sheet.addCell(new Label(5, 2, "工作日|晚餐|计算"));  
			sheet.addCell(new Label(6, 2, "工作日|晚餐|调整"));
//			sheet.addCell(new Label(9, 2, "工作日|四班|计算"));  
//			sheet.addCell(new Label(10, 2, "工作日|四班|调整")); 
//			sheet.addCell(new Label(11, 2, "节假日|一班|计算"));  
//			sheet.addCell(new Label(12, 2, "节假日|一班|调整"));   
			sheet.addCell(new Label(7, 2, "节假日|午餐|计算"));  
			sheet.addCell(new Label(8, 2, "节假日|午餐|调整"));   
			sheet.addCell(new Label(9, 2, "节假日|晚餐|计算"));  
			sheet.addCell(new Label(10, 2, "节假日|晚餐|调整"));   
//			sheet.addCell(new Label(17, 2, "节假日|四班|计算"));
//			sheet.addCell(new Label(18, 2, "节假日|四班|调整"));
			sheet.addCell(new Label(11, 2, "部门"));  
            sheet.mergeCells(0, 0, 11, 1);
            //遍历list填充表格内容
            for(int i=0;i<list.size();i++) {
            	sheet.addCell(new Label(0, i+3, list.get(i).getItcode()));
            	sheet.addCell(new Label(1, i+3, list.get(i).getItdes()));
            	sheet.addCell(new Label(2, i+3, list.get(i).getItunit()));
//            	sheet.addCell(new Label(3, i+3, String.valueOf(list.get(i).getCnt1old())));
//            	sheet.addCell(new Label(4, i+3, String.valueOf(list.get(i).getCnt1())));
	    		sheet.addCell(new Label(3, i+3, String.valueOf(list.get(i).getCnt2old())));
	    		sheet.addCell(new Label(4, i+3, String.valueOf(list.get(i).getCnt2())));
	    		sheet.addCell(new Label(5, i+3, String.valueOf(list.get(i).getCnt3old())));
	    		sheet.addCell(new Label(6, i+3, String.valueOf(list.get(i).getCnt3())));
//	    		sheet.addCell(new Label(9, i+3, String.valueOf(list.get(i).getCnt4old())));
//	    		sheet.addCell(new Label(10, i+3, String.valueOf(list.get(i).getCnt4())));
//	    		sheet.addCell(new Label(11, i+3, String.valueOf(list.get(i).getHcnt1old())));
//	    		sheet.addCell(new Label(12, i+3, String.valueOf(list.get(i).getHcnt1())));
	    		sheet.addCell(new Label(7, i+3, String.valueOf(list.get(i).getHcnt2old())));
	    		sheet.addCell(new Label(8, i+3, String.valueOf(list.get(i).getHcnt2())));
	    		sheet.addCell(new Label(9, i+3, String.valueOf(list.get(i).getHcnt3old())));
	    		sheet.addCell(new Label(10, i+3, String.valueOf(list.get(i).getHcnt3())));
//	    		sheet.addCell(new Label(17, i+3, String.valueOf(list.get(i).getHcnt4old())));
//	    		sheet.addCell(new Label(18, i+3, String.valueOf(list.get(i).getHcnt4())));
	    		sheet.addCell(new Label(11, i+3, list.get(i).getDeptdes()));
	    		}
            workBook.write();
            os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
		
}