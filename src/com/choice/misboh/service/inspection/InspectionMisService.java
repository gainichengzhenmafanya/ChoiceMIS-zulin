package com.choice.misboh.service.inspection;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletOutputStream;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.NumberFormat;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.commonutil.LocalUtil;
import com.choice.misboh.domain.inspection.ArrivaldMis;
import com.choice.misboh.domain.inspection.ArrivalmMis;
import com.choice.misboh.persistence.inout.ChkindMisMapper;
import com.choice.misboh.persistence.inout.ChkinmMisMapper;
import com.choice.misboh.persistence.inspection.InspectionMisMapper;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Acct;
import com.choice.scm.domain.Chkind;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.domain.ana.DisList;
import com.choice.scm.persistence.AcctMapper;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.util.CalChkNum;
@Service
public class InspectionMisService {

	@Autowired
	private AcctMapper acctMapper;
	@Autowired
	private InspectionMisMapper inspectionMapper;
	@Autowired
	private PageManager<Dis> pageManager;
	@Autowired
	private PageManager<SupplyAcct> pageManager1;
	@Autowired
	private PageManager<ArrivalmMis> arrivalmPage;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private ChkinmMisMapper chkinmMisMapper;
	@Autowired
	private ChkindMisMapper chkindMisMapper;
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	private final transient Log log = LogFactory.getLog(InspectionMisService.class);
	
	/**********************************************************直配验货***************************************************************/
	
	/***
	 * 直配验货总数
	 * @param dis
	 * @return
	 * @throws CRUDException
	 */
	public int findAllDisCount(Dis dis) throws CRUDException {
		try {
			//改为判断是不是非要采购确认 采购审核之后才能查询 wjf
			Acct acct = acctMapper.findAcctById(dis.getAcct());
			if("Y".equals(acct.getYncgqr())){
				dis.setChk1("Y");
			}else{
				dis.setChk1(null);
			}
			if("Y".equals(acct.getYncgsh())){
				dis.setSta("Y");
			}else{
				dis.setSta(null);
			}
			String firmCode=dis.getFirmCode();//记录原始值
			String inout=dis.getInout();
			if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}
			if(null!=dis.getInout() && !"".equals(dis.getInout())){dis.setInout(CodeHelper.replaceCode(dis.getInout()));}
			if(null!=dis.getSp_init() && !"".equals(dis.getSp_init())){dis.setSp_init(dis.getSp_init().toUpperCase());}
			if("1".equals(dis.getYnArrival())){dis.setChksend("N");dis.setChk1("Y");}//确认到货
			if("2".equals(dis.getYnArrival())){dis.setChksend("Y");}//未知到货
			int count = inspectionMapper.findAllDisCount(dis); 	
			dis.setFirmCode(firmCode);//覆盖处理后的值
			dis.setInout(inout);
			return 	count;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 直配验货查询 分页
	 * @param dis
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<Dis> findAllDis(Dis dis,Page page) throws CRUDException {
		try {
			//改为判断是不是非要采购确认 采购审核之后才能查询 wjf
			Acct acct = acctMapper.findAcctById(dis.getAcct());
			if("Y".equals(acct.getYncgqr())){
				dis.setChk1("Y");
			}else{
				dis.setChk1(null);
			}
			if("Y".equals(acct.getYncgsh())){
				dis.setSta("Y");
			}else{
				dis.setSta(null);
			}
			List<Dis> list=null;
			String firmCode=dis.getFirmCode();//记录原始值
			String inout=dis.getInout();
			if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}
			if(null!=dis.getInout() && !"".equals(dis.getInout())){dis.setInout(CodeHelper.replaceCode(dis.getInout()));}
			if(null!=dis.getSp_init() && !"".equals(dis.getSp_init())){dis.setSp_init(dis.getSp_init().toUpperCase());}
			if("1".equals(dis.getYnArrival())){dis.setChksend("N");dis.setChk1("Y");}//确认到货
			if("2".equals(dis.getYnArrival())){dis.setChksend("Y");}//未知到货
			list=pageManager.selectPage(dis, page, InspectionMisMapper.class.getName()+".findAllDis");
			//list=inspectionMapper.findAllDis(dis);
			dis.setFirmCode(firmCode);//覆盖处理后的值
			dis.setInout(inout);
			return list;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 直配验货查询ajax 分页
	 * @param dis
	 * @return
	 * @throws CRUDException
	 */
	public List<Dis> findAllDisPage(Dis dis) throws CRUDException {
		try {
			//改为判断是不是非要采购确认 采购审核之后才能查询 wjf
			Acct acct = acctMapper.findAcctById(dis.getAcct());
			if("Y".equals(acct.getYncgqr())){
				dis.setChk1("Y");
			}else{
				dis.setChk1(null);
			}
			if("Y".equals(acct.getYncgsh())){
				dis.setSta("Y");
			}else{
				dis.setSta(null);
			}
			List<Dis> list=null;
			String firmCode=dis.getFirmCode();//记录原始值
			String inout=dis.getInout();
			if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}
			if(null!=dis.getInout() && !"".equals(dis.getInout())){dis.setInout(CodeHelper.replaceCode(dis.getInout()));}
			if(null!=dis.getSp_init() && !"".equals(dis.getSp_init())){dis.setSp_init(dis.getSp_init().toUpperCase());}
			if("1".equals(dis.getYnArrival())){dis.setChksend("N");dis.setChk1("Y");}//确认到货
			if("2".equals(dis.getYnArrival())){dis.setChksend("Y");}//未知到货
			list=inspectionMapper.findAllDisPage(dis); 			
			//list=inspectionMapper.findAllDis(dis);
			dis.setFirmCode(firmCode);//覆盖处理后的值
			dis.setInout(inout);
			return list;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 直配验货查询  打印用，不带分页
	 * @param dis
	 * @return
	 * @throws CRUDException
	 */
	public List<Dis> findAllDisForReport(Dis dis) throws CRUDException {
		try {
			//改为判断是不是非要采购确认 采购审核之后才能查询 wjf
			Acct acct = acctMapper.findAcctById(dis.getAcct());
			if("Y".equals(acct.getYncgqr())){
				dis.setChk1("Y");
			}else{
				dis.setChk1(null);
			}
			if("Y".equals(acct.getYncgsh())){
				dis.setSta("Y");
			}else{
				dis.setSta(null);
			}
			List<Dis> list=null;
			String firmCode=dis.getFirmCode();//记录原始值
			String inout=dis.getInout();
			if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}
			if(null!=dis.getInout() && !"".equals(dis.getInout())){dis.setInout(CodeHelper.replaceCode(dis.getInout()));}
			if(null!=dis.getSp_init() && !"".equals(dis.getSp_init())){dis.setSp_init(dis.getSp_init().toUpperCase());}
			if("1".equals(dis.getYnArrival())){dis.setChksend("N");dis.setChk1("Y");}//确认到货
			if("2".equals(dis.getYnArrival())){dis.setChksend("Y");}//未知到货
			list=inspectionMapper.findAllDis(dis);
			dis.setFirmCode(firmCode);//覆盖处理后的值
			dis.setInout(inout);
			return list;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/****
	 * 判断当前条件下是否有档口报货单生产的验货单
	 * @param ids
	 * @return
	 */
	public int findDeptChkstom(String ids) {
		return inspectionMapper.findDeptChkstom(ids);
	}
	
	/***
	 * 修改直配验货
	 * @param disList
	 * @return
	 * @throws CRUDException
	 */
	public String updateDis(DisList disList) throws CRUDException {
		Map<String,Object> result = new HashMap<String,Object>();
		try {
			for (int i = 0; i < disList.getListDis().size(); i++) {
				String chkin=findChkByKey(disList.getListDis().get(i).getId()).get(0).getChkin();
				String chkout=findChkByKey(disList.getListDis().get(i).getId()).get(0).getChkout();
				//已经入库或者出库的不能修改
				if("Y".equals(chkin) || "Y".equals(chkout)){
					result.put("pr", "fail");
				}else{
					Dis dis=new Dis();
					dis.setId(disList.getListDis().get(i).getId());
					dis.setDeliverCode(disList.getListDis().get(i).getDeliverCode());
					dis.setAmountin(disList.getListDis().get(i).getAmountin());
					dis.setAmount1in(disList.getListDis().get(i).getAmount1in());//参考数量
					dis.setPricein(disList.getListDis().get(i).getPricein());
					dis.setInd(disList.getListDis().get(i).getInd());
					dis.setBak2(disList.getListDis().get(i).getBak2());
					
					inspectionMapper.updateDis(dis);
					
					result.put("pr", "succ");
				}
			}
			result.put("updateNum", disList.getListDis().size());
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	private List<Chkstod> findChkByKey(String id) throws CRUDException {
		try {
			return inspectionMapper.findChkByKey(id);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 直配验收入库方法  不启用档口情况
	 * @param listDistribution
	 * @param acct
	 * @param dis
	 * @param positnIn
	 * @param maded
	 * @param accountName
	 * @throws CRUDException
	 */
	public void saveYsrkChkinm(List<Dis> listDistribution,String acct,Dis dis,String positnIn,Date maded,String accountName) throws CRUDException {
		try {
			log.warn("验收入库：");
			String deliverCode=listDistribution.get(0).getDeliverCode();//供应商code
			ArrayList<Dis>  list=new ArrayList<Dis>();//临时list
			for (int i = 0; i < listDistribution.size(); i++) {//循环数据
				if(listDistribution.get(i).getDeliverCode().equals(deliverCode)){
					list.add(listDistribution.get(i));
				}else{//不相等的时候
					if(list.size()!=0){
						//如果验收数量都是空，则没必要生成入库单
						boolean flag = false;//默认为false,如果有一条数据验货数量不是0，则继续进行入库操作
						for (int j = 0; j < list.size(); j++) {
							Dis d=(Dis)list.get(j);
							if(d.getAmountin() != 0){
								flag = true;
								break;
							}
						}
						if(flag){
							saveChkinmByChkstom(list,acct,positnIn,maded,accountName);
						}
					}
					list.clear();//清空
					list.add(listDistribution.get(i));//在list重新加入该条数据
				}
				deliverCode=listDistribution.get(i).getDeliverCode();//把循环外的值换为当前数据
			}
			if(list.size()!=0){
				//如果验收数量都是空，则没必要生成入库单
				boolean flag = false;//默认为false,如果有一条数据验货数量不是0，则继续进行入库操作
				for (int j = 0; j < list.size(); j++) {
					Dis d=(Dis)list.get(j);
					if(d.getAmountin() != 0){
						flag = true;
						break;
					}
				}
				if(flag){
					saveChkinmByChkstom(list,acct,positnIn,maded,accountName);
				}
			}
			dis.setChkin("Y");//已入库
			dis.setChkyh("Y");//已验货
			dis.setEmpfirm(accountName);//验货人
			dis.setId(dis.getInCondition());
			inspectionMapper.updateByIds(dis);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	private void saveChkinmByChkstom(ArrayList<Dis> list,String acct,String positnIn,Date maded,String accountName) throws CRUDException {
		try {
			log.warn("供应商到货生成入库单:");
			Chkinm chkinm=new Chkinm();
			String vouno = calChkNum.getNextBytable(DDT.VOUNO_CHKINM,DDT.VOUNO_RK+positnIn+"-",maded);
			int chkinno = chkinmMisMapper.getMaxChkinno();
		  	chkinm.setAcct(acct);
		  	chkinm.setMadeby(accountName);
		  	chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
		  	chkinm.setChkinno(chkinno);
		  	chkinm.setVouno(vouno);
		  	chkinm.setMaded(DateFormat.formatDate(maded,"yyyy-MM-dd"));
			chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
			Positn positn=new Positn();
			positn.setCode(positnIn);
			chkinm.setPositn(positn);
			Deliver deliver = new Deliver();
			deliver.setCode(list.get(0).getDeliverCode());
			chkinm.setDeliver(deliver);
			chkinm.setTyp(DDT.ZCRK);//正常入库
//			chkinm.setTyp(DDT.ZPRK);//直配入库  20160606wjf
			chkinm.setMemo(LocalUtil.getI18N("Dire_Inspection_Generation_In_Order"));//直配验货生成入库单审
			chkinm.setInout(DDT.IN);
			chkinmMisMapper.saveChkinm(chkinm);
			for (int j = 0; j < list.size(); j++) {
				Dis dis=(Dis)list.get(j);
				if(dis.getAmountin() == 0){//如果验货数量为0，则直接跳过这条，继续下次循环
					continue;
				}
				Chkind chkind=new Chkind();//入库单从表
				Supply supply=new Supply();
				supply.setSp_code(dis.getSp_code());
				chkind.setChkinno(chkinno);
				chkind.setSupply(supply);
				chkind.setAmount(dis.getAmountin());//数量为采购数量 而非报货数量
				chkind.setPrice(dis.getPricein());
				chkind.setMemo(dis.getMemo());
				chkind.setDued(dis.getDued());
				chkind.setInout(DDT.IN);
				chkind.setPound(0);
				chkind.setSp_id(dis.getSp_id());
				chkind.setChkstono(dis.getChkstoNo());
				chkind.setAmount1(dis.getAmount1in());//这里改为取amount1in  2014.12.30wjf 也能兼容以前版本
				chkind.setPcno(dis.getPcno());//保存批次号
				chkind.setChkno(dis.getChkstoNo());//入库单存报货单号
				chkindMisMapper.saveChkind(chkind);
			}
			chkinm.setMonth(commonMISBOHService.getOnlyAccountMonth(chkinm.getMaded(),positnIn));//  此字段当成月份使用     加入会计月
			chkinm.setChecby(accountName);
			chkinm.setStatus(DDT.IN);//存储过程中未用
			chkinm.setChk1memo(LocalUtil.getI18N("Dire_Inspection_Generation_In_Order")+LocalUtil.getI18N("Check_through"));//"直配验货生成入库单审核通过"
			chkinmMisMapper.checkChkinm(chkinm);//审核入库单
			//反写入库单号 报货单存入库单号
			StringBuffer sb = new StringBuffer();
			for(Dis s : list){
				sb.append(s.getId()+",");
			}
			Dis dis = new Dis();
			dis.setId(sb.substring(0, sb.length()-1));
			dis.setChkinno(chkinno);
			inspectionMapper.updateByIds(dis);
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 查询档口报货来的数据
	 * @param dis
	 * @param iszp
	 * @return
	 * @throws CRUDException
	 */
	public List<Dis> findSupplyacctList(Dis dis,String iszp) throws CRUDException{
		try {
			if("Y".equals(iszp)){//直配验货查询
				List<Dis> disList = inspectionMapper.findChkstodList(dis);
				if(disList.size() > 0){//在页面上不重复显示
					int first_id = 0;
					double totalAmount = 0.0;
					double totalAmount1 = 0.0;
					for(int i = 0;i<=disList.size()-1;i++){
						Dis d = disList.get(i);
						int id = Integer.parseInt(d.getId());
						if(first_id == id){
							d.setSta("noShow");
						}else{
							if(totalAmount != 0 && i != 0){//验多了的情况下
								disList.get(i-1).setAccprate(disList.get(i-1).getAccprate()+totalAmount);
								disList.get(i-1).setAccpratemin(disList.get(i-1).getAccpratemin()+totalAmount1);
							}
							first_id = id;
							totalAmount = d.getAmountin();
							totalAmount1 = d.getAmount1in();
						}
						if(d.getAmount() <= totalAmount){//如果报货数量小于发货数量 ，则验货数量=报货数量  验货数量借用accprate accpratemin  字段
							
							if(i ==(disList.size()-1)){
								d.setAccprate(totalAmount);
								d.setAccpratemin(totalAmount1);
								totalAmount = 0;
								totalAmount1 = 0;
							}else{
								d.setAccprate(d.getAmount());
								d.setAccpratemin(d.getAmount()*d.getUnitper());
								totalAmount -= d.getAmount();
								totalAmount1 -= d.getAmount()*d.getUnitper();
							}
							
						}else{//如果报货数量大于验货数量
							d.setAccprate(totalAmount);
							d.setAccpratemin(totalAmount1);
							totalAmount = 0;
							totalAmount1 = 0;
						}
					}
				}
				return disList;
			}else{//统配查询
				List<Dis> disList = inspectionMapper.findSupplyacctList(dis);
				if(disList.size() > 0){//在页面上不重复显示
					String firstSpcode = "";
//					double totalAmount = disList.get(0).getAmountin();
//					double totalAmount1 = disList.get(0).getAmount1in();
					double totalAmount = 0.0;
					double totalAmount1 = 0.0;
					for(int i = 0;i<=disList.size()-1;i++){
						Dis d = disList.get(i);
						String spcode = d.getSp_code();
						if(firstSpcode.equals(spcode)){
							d.setSta("noShow");
						}else{
							if(totalAmount != 0 && i != 0){//验多了的情况下
								disList.get(i-1).setAccprate(disList.get(i-1).getAccprate()+totalAmount);
								disList.get(i-1).setAccpratemin(disList.get(i-1).getAccpratemin()+totalAmount1);
							}
							firstSpcode = spcode;
							totalAmount = null == d.getAmountin() ? 0 : d.getAmountin();
							totalAmount1 = null == d.getAmount1in() ? 0 : d.getAmount1in();
						}
						if(d.getAmount() <= totalAmount){//如果报货数量小于发货数量 ，则验货数量=报货数量  验货数量借用accprate accpratemin  字段

							if(i ==(disList.size()-1)){
								d.setAccprate(totalAmount);
								d.setAccpratemin(totalAmount1);
								totalAmount = 0;
								totalAmount1 = 0;
							}else{
								d.setAccprate(d.getAmount());
								d.setAccpratemin(d.getAmount()*d.getUnitper());
								totalAmount -= d.getAmount();
								totalAmount1 -= d.getAmount()*d.getUnitper();
							}
						}else{//如果报货数量大于验货数量
							d.setAccprate(totalAmount);
							d.setAccpratemin(totalAmount1);
							totalAmount = 0;
							totalAmount1 = 0;
						}
					}
				}
				return disList;
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 生成档口直发单 档口报货来的（直配验货）
	 * @author wjf
	 * @param chkinm
	 * @return
	 */
	public int saveChkinmDept_zp(Chkinm chkinm,String ids,String sp_ids) throws CRUDException{
		try{
			log.warn("验收入库：");
			String deliverCode = chkinm.getChkindList().get(0).getDeliver();//供应商code
			ArrayList<Chkind> list=new ArrayList<Chkind>();//临时list
			for (int i = 0; i < chkinm.getChkindList().size(); i++) {//循环数据
				if(chkinm.getChkindList().get(i).getDeliver().equals(deliverCode)){
					list.add(chkinm.getChkindList().get(i));
				}else{//不相等的时候
					if(list.size()!=0){
						//如果验收数量都是空，则没必要生成入库单
						boolean flag = false;//默认为false,如果有一条数据验货数量不是0，则继续进行入库操作
						for (int j = 0; j < list.size(); j++) {
							Chkind d=(Chkind)list.get(j);
							if(d.getAmount() != 0){
								flag = true;
								break;
							}
						}
						if(flag){
							saveChkinmzbByChkstom(chkinm,list);
						}
					}
					list.clear();//清空
					list.add(chkinm.getChkindList().get(i));//在list重新加入该条数据
				}
				deliverCode=chkinm.getChkindList().get(i).getDeliver();//把循环外的值换为当前数据
			}
			if(list.size()!=0){
				//如果验收数量都是空，则没必要生成入库单
				boolean flag = false;//默认为false,如果有一条数据验货数量不是0，则继续进行入库操作
				for (int j = 0; j < list.size(); j++) {
					Chkind d=(Chkind)list.get(j);
					if(d.getAmount() != 0){
						flag = true;
						break;
					}
				}
				if(flag){
					saveChkinmzbByChkstom(chkinm,list);
				}
			}
			Dis dis = new Dis();
			dis.setChkin("Y");//已入库
			dis.setChkyh("Y");//已验货
			dis.setEmpfirm(chkinm.getMadeby());//验货人
			dis.setId(ids);
			inspectionMapper.updateByIds(dis);
			//更新chkstod_dept chkin  字段为已入库
			inspectionMapper.updateChkstodDeptChkin(sp_ids);
			return 1;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}	
	private int saveChkinmzbByChkstom(Chkinm chkinm,ArrayList<Chkind> list) throws CRUDException{
		try{
			Date date = null == chkinm.getMaded() ? new Date() : chkinm.getMaded();
			chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
			chkinm.setChkinno(chkinmMisMapper.getMaxChkinno());
			chkinm.setVouno(calChkNum.getNextBytable(DDT.VOUNO_CHKINM,DDT.VOUNO_ZF+chkinm.getPositn().getCode()+"-",date));
			chkinm.setMaded(date);
			chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
			Deliver deliver = new Deliver();
			deliver.setCode(list.get(0).getDeliver());//供应商为真正的供应商
			chkinm.setDeliver(deliver);
			chkinm.setTotalamt(0);
			chkinm.setInout(DDT.INOUT);
			chkinm.setTyp(DDT.ZCZF);//正常直发
//			chkinm.setTyp(DDT.ZPZF);//直配直发  20160606wjf
			chkinm.setMemo(LocalUtil.getI18N("Dire_Inspection_Generation_Inout_Order"));//"直配验货直发档口"
			log.warn("添加直发单(主单)：\n"+chkinm);
	 		chkinmMisMapper.saveChkinm(chkinm);
	 		for (int j = 0; j < list.size(); j++) {
				Chkind chkind = list.get(j);//入库单从表
				if(chkind.getAmount() == 0){//如果验货数量为0，则直接跳过这条，继续下次循环
					continue;
				}
				chkind.setChkinno(chkinm.getChkinno());
				chkind.setInout(DDT.INOUT);   //加入直发标志
				chkind.setPound(0);
				chkind.setChkno(chkind.getChkstono());//入库单存报货单号
				chkindMisMapper.saveChkind(chkind);
			}
			//审核
			chkinm.setMonth(commonMISBOHService.getOnlyAccountMonth(chkinm.getMaded(),chkinm.getPositn().getCode()));//  此字段当成月份使用     加入会计月
			chkinm.setChecby(chkinm.getMadeby());
			chkinm.setStatus(DDT.INOUT);
			chkinm.setChk1memo(LocalUtil.getI18N("Dire_Inspection_Generation_Inout_Order")+LocalUtil.getI18N("Check_through"));//"直配验货直发档口审核通过"
			chkinmMisMapper.checkChkinm(chkinm);//审核入库单
			//反写入库单号 报货单存入库单号  这里的id会有重复 ，所以先去除重复
			Set<Integer> set = new HashSet<Integer>();
			for(Chkind d : list){
				set.add(d.getId());
			}
			StringBuffer sb = new StringBuffer();
			for(Integer i : set){
				sb.append(i + ",");
			}
			Dis dis = new Dis();
			dis.setId(sb.substring(0, sb.length()-1));
			dis.setChkinno(chkinm.getChkinno());
			inspectionMapper.updateByIds(dis);
			return chkinm.getPr();
		} catch (CRUDException e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 分档口验货(直配验货)
	 * @author wjf
	 * @param chkinm
	 * @return
	 * @throws Exception 
	 */
	public int saveChkinmGroupZP(Chkinm chkinm,String ids) throws CRUDException {
		try{
			//1.先更新状态
			Dis di = new Dis();
			di.setChkin("Y");//已入库
			di.setChkyh("Y");//已验货
			di.setEmpfirm(chkinm.getMadeby());//验货人
			di.setId(ids);
			inspectionMapper.updateByIds(di);
			
			//判断走门店入库 还是档口直发
			if(chkinm.getChkindList().size()!=0){
				List<Chkind> firmChkind = new ArrayList<Chkind>();
				List<Chkind> deptChkind = new ArrayList<Chkind>();
				//如果验收数量都是空，则没必要生成入库单
				boolean flag = false;//默认为false,如果有一条数据验货数量不是0，则继续进行入库操作
				for (int j = 0; j < chkinm.getChkindList().size(); j++) {
					Chkind dis=(Chkind)chkinm.getChkindList().get(j);
					if(dis.getAmount() != 0){
						flag = true;
						//档口的和门店的分开
						if(null == dis.getSupply().getSp_name() || "".equals(dis.getSupply().getSp_name())){//档口的
							deptChkind.add(dis);
						}else{
							firmChkind.add(dis);
						}
					}
				}
				if(flag){
					//先入门店
					if(firmChkind.size()!=0){
						String deliver = firmChkind.get(0).getDeliver();
						List<Chkind> list = new ArrayList<Chkind>();
						for (int i = 0; i < firmChkind.size(); i++) {//循环数据
							if(deliver.equals(firmChkind.get(i).getDeliver())){
								list.add(firmChkind.get(i));
							}else{//不相等的时候
								if(list.size() != 0){
									chkinm.setChkindList(list);
									saveChkinmBySupplyAcctZP(chkinm);
									list.clear();//清空
									list.add(firmChkind.get(i));//在list重新加入该条数据
								}
								deliver = firmChkind.get(i).getDeliver();//把循环外的值换为当前数据
							}
						}
						if(list.size() != 0){
							chkinm.setChkindList(list);
							saveChkinmBySupplyAcctZP(chkinm);
						}
					}
					//再直发档口
					if(deptChkind.size()!=0){
						String deliver = deptChkind.get(0).getDeliver();
						List<Chkind> list = new ArrayList<Chkind>();
						for (int i = 0; i < deptChkind.size(); i++) {//循环数据
							if(deliver.equals(deptChkind.get(i).getDeliver())){
								list.add(deptChkind.get(i));
							}else{//不相等的时候
								if(list.size() != 0){
									chkinm.setChkindList(list);
									saveChkinmzbDeptBySupplyAcctZP(chkinm);
									list.clear();//清空
									list.add(deptChkind.get(i));//在list重新加入该条数据
								}
								deliver = deptChkind.get(i).getDeliver();//把循环外的值换为当前数据
							}
						}
						if(list.size() != 0){
							chkinm.setChkindList(list);
							saveChkinmzbDeptBySupplyAcctZP(chkinm);
						}
					}
				}
			}
			return 1;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	private int saveChkinmBySupplyAcctZP(Chkinm chkinm) throws CRUDException{
		try{
			Date date = null == chkinm.getMaded() ? new Date() : chkinm.getMaded();
			chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
			chkinm.setChkinno(chkinmMisMapper.getMaxChkinno());
			chkinm.setVouno(calChkNum.getNextBytable(DDT.VOUNO_CHKINM,DDT.VOUNO_RK+chkinm.getPositn().getCode()+"-",date));
			chkinm.setMaded(date);
			chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
//			Positn positn=new Positn();
//			positn.setCode(chkinm.getChkindList().get(0).getDeliver());
			Deliver deliver = new Deliver();
			deliver.setCode(chkinm.getChkindList().get(0).getDeliver());//供应商为真正的供应商
			chkinm.setDeliver(deliver);
			chkinm.setTotalamt(0);
			chkinm.setInout(DDT.IN);
			chkinm.setTyp(DDT.ZCRK);//正常入库
//			chkinm.setTyp(DDT.ZPRK);//直配入库  20160606wjf
			chkinm.setMemo(LocalUtil.getI18N("Dire_Inspection_Generation_In_Order_By_Dept"));//"直配分档口验货生成入库单"
			log.warn("添加入库单(主单)：\n"+chkinm);
	 		chkinmMisMapper.saveChkinm(chkinm);
			for (int j = 0; j < chkinm.getChkindList().size(); j++) {
				Chkind chkind = chkinm.getChkindList().get(j);//入库单从表
				if(chkind.getAmount() == 0){//如果验货数量为0，则直接跳过这条，继续下次循环
					continue;
				}
				chkind.setChkinno(chkinm.getChkinno());
				chkind.setIndept(null);
				chkind.setInout(DDT.IN);
				chkind.setPound(0);
				chkind.setChkno(chkind.getChkstono());
				chkindMisMapper.saveChkind(chkind);
			}
			//审核
			chkinm.setMonth(commonMISBOHService.getOnlyAccountMonth(chkinm.getMaded(),chkinm.getPositn().getCode()));//  此字段当成月份使用     加入会计月
			chkinm.setChecby(chkinm.getMadeby());
			chkinm.setStatus(chkinm.getInout());
			chkinm.setChk1memo(LocalUtil.getI18N("Dire_Inspection_Generation_In_Order_By_Dept")+LocalUtil.getI18N("Check_through"));
			chkinmMisMapper.checkChkinm(chkinm);//审核入库单
			//反写入库单号 报货单存入库单号  这里的id会有重复 ，所以先去除重复
			Set<Integer> set = new HashSet<Integer>();
			for(Chkind d : chkinm.getChkindList()){
				set.add(d.getId());
			}
			StringBuffer sb = new StringBuffer();
			for(Integer i : set){
				sb.append(i + ",");
			}
			Dis dis = new Dis();
			dis.setId(sb.substring(0, sb.length()-1));
			dis.setChkinno(chkinm.getChkinno());
			inspectionMapper.updateByIds(dis);
			return chkinm.getPr();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	private int saveChkinmzbDeptBySupplyAcctZP(Chkinm chkinm) throws CRUDException{
		try{
			Date date = null == chkinm.getMaded() ? new Date() : chkinm.getMaded();
			chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
			chkinm.setChkinno(chkinmMisMapper.getMaxChkinno());
			chkinm.setVouno(calChkNum.getNextBytable(DDT.VOUNO_CHKINM,DDT.VOUNO_ZF+chkinm.getPositn().getCode()+"-",date));
			chkinm.setMaded(date);
			chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
//			Positn positn=new Positn();
//			positn.setCode(chkinm.getChkindList().get(0).getDeliver());
			Deliver deliver = new Deliver();
			deliver.setCode(chkinm.getChkindList().get(0).getDeliver());//供应商为真正的供应商
			chkinm.setDeliver(deliver);
			chkinm.setTotalamt(0);
			chkinm.setInout(DDT.INOUT);
			chkinm.setTyp(DDT.ZCZF);//正常直发
//			chkinm.setTyp(DDT.ZPZF);//直配直发  20160606wjf
			chkinm.setMemo(LocalUtil.getI18N("Dire_Inspection_Generation_Inout_Order_By_Dept"));//"直配分档口验货生成直发单"
			log.warn("添加直发单(主单)：\n"+chkinm);
	 		chkinmMisMapper.saveChkinm(chkinm);
			for (int j = 0; j < chkinm.getChkindList().size(); j++) {
				Chkind chkind = chkinm.getChkindList().get(j);//入库单从表
				if(chkind.getAmount() == 0){//如果验货数量为0，则直接跳过这条，继续下次循环
					continue;
				}
				chkind.setChkinno(chkinm.getChkinno());
				chkind.setInout(DDT.INOUT);   //加入直发标志
				chkind.setPound(0);
				chkind.setChkno(chkind.getChkstono());
				chkindMisMapper.saveChkind(chkind);
			}
			//审核
			chkinm.setMonth(commonMISBOHService.getOnlyAccountMonth(chkinm.getMaded(),chkinm.getPositn().getCode()));//  此字段当成月份使用     加入会计月
			chkinm.setChecby(chkinm.getMadeby());
			chkinm.setStatus(DDT.INOUT);
			chkinm.setChk1memo(LocalUtil.getI18N("Dire_Inspection_Generation_Inout_Order_By_Dept")+LocalUtil.getI18N("Check_through"));
			chkinmMisMapper.checkChkinm(chkinm);//审核入库单
			//反写入库单号 报货单存入库单号  这里的id会有重复 ，所以先去除重复
			Set<Integer> set = new HashSet<Integer>();
			for(Chkind d : chkinm.getChkindList()){
				set.add(d.getId());
			}
			StringBuffer sb = new StringBuffer();
			for(Integer i : set){
				sb.append(i + ",");
			}
			Dis dis = new Dis();
			dis.setId(sb.substring(0, sb.length()-1));
			dis.setChkinno(chkinm.getChkinno());
			inspectionMapper.updateByIds(dis);
			return chkinm.getPr();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/**********************************************************直配验货end*****************************************************************/
	
	/**********************************************************配送验货*****************************************************************/
	
	/***
	 * 查询配送验货数据 分页用
	 * @param disMap
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<SupplyAcct> selectByKey(HashMap<String, Object> disMap, Page page) throws CRUDException{
		try {
			return pageManager1.selectPage(disMap, page, InspectionMisMapper.class.getName()+".selectByKey");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 查询配送验货数据 不分页
	 * @param disMap
	 * @return
	 * @throws CRUDException
	 */
	public List<SupplyAcct> selectByKey(HashMap<String, Object> disMap) throws CRUDException{
		try {
			return inspectionMapper.selectByKey(disMap);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 修改验货信息（配送验货）
	 * @param supplyAcct
	 * @param acct
	 * @throws CRUDException
	 */
	public String updateAcct(SupplyAcct supplyAcct, String acct) throws CRUDException {
		try {
			for (int i = 0; i < supplyAcct.getSupplyAcctList().size(); i++) {//批量修改
				SupplyAcct supplyAcct_ = new SupplyAcct();
				supplyAcct_.setAcct(acct);
				supplyAcct_.setId(supplyAcct.getSupplyAcctList().get(i).getId());
				supplyAcct_.setCntfirm(supplyAcct.getSupplyAcctList().get(i).getCntfirm());
				supplyAcct_.setCntfirm1(supplyAcct.getSupplyAcctList().get(i).getCntfirm1());
				supplyAcct_.setCntfirmks(supplyAcct.getSupplyAcctList().get(i).getCntfirmks());
				supplyAcct_.setCntlogistks(supplyAcct.getSupplyAcctList().get(i).getCntlogistks());
				inspectionMapper.updateAcct(supplyAcct_);
			}
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 配送验货生成入库单 不启用档口情况下
	 * @param ids
	 * @param accountName
	 * @param acct
	 * @throws CRUDException
	 */
	public void check(List<SupplyAcct> disList, String ids, String accountName, String acct, Date maded) throws CRUDException {
		try {
			//生成门店的入库单
			log.warn("配送中心到货验收入库：");
			//最后处理  list大小不为0  时候  在作为一条入库单据加入
			if(disList.size()!=0){
				//如果验收数量都是空，则没必要生成入库单
				boolean flag = false;//默认为false,如果有一条数据验货数量不是0，则继续进行入库操作
				for (int j = 0; j < disList.size(); j++) {
					SupplyAcct dis=(SupplyAcct)disList.get(j);
					if(dis.getCntfirm() != 0){
						flag = true;
					}else{//特殊情况 如果验货数量是0（可能是直接审核的极端情况  需要特殊处理一下wjf）
						SupplyAcct supplyAcct_ = new SupplyAcct();
						supplyAcct_.setAcct(acct);
						supplyAcct_.setId(dis.getId());
						supplyAcct_.setCntfirm(0);
						supplyAcct_.setCntfirm1(0);
						supplyAcct_.setCntfirmks(dis.getCntout());//门店亏
						supplyAcct_.setCntlogistks(0);
						inspectionMapper.updateAcct(supplyAcct_);
					}
				}
				if(flag){
					saveChkinmBySupplyAcct(disList,acct,accountName,maded,ids);
				}
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	private void saveChkinmBySupplyAcct(List<SupplyAcct> list,String acct,String accountName,Date maded,String ids) throws CRUDException {
		try {
			log.warn("配送中心到货验收入库生成入库单:");
			Date date = null == maded ? new Date() : maded;
			Chkinm chkinm=new Chkinm();
			String vouno = calChkNum.getNextBytable(DDT.VOUNO_CHKINM,DDT.VOUNO_RK+list.get(0).getFirm()+"-",date);
			int chkinno = chkinmMisMapper.getMaxChkinno();
		  	chkinm.setAcct(acct);
		  	chkinm.setMadeby(accountName);
		  	chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
		  	chkinm.setChkinno(chkinno);
		  	chkinm.setVouno(vouno);
		  	chkinm.setMaded(date);
			chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
			Positn positn=new Positn();
			positn.setCode(list.get(0).getFirm());
			chkinm.setPositn(positn);
			//供应商为当前分店所属片区的主直拨库对应的供应商
			Deliver deliver = inspectionMapper.findTpDeliver(positn);
			chkinm.setDeliver(deliver);
			chkinm.setTyp(DDT.ZCRK);//正常入库
//			chkinm.setTyp(DDT.PSRK);//配送入库  20160606wjf
			chkinm.setMemo(LocalUtil.getI18N("Out_Inspection_Generation_In_Order"));//"配送验货生成入库单"
			chkinm.setInout(DDT.IN); 
			chkinmMisMapper.saveChkinm(chkinm);
			for (int j = 0; j < list.size(); j++) {
				SupplyAcct dis=(SupplyAcct)list.get(j);
				if(dis.getCntfirm() == 0){//如果验货数量为0，则直接跳过这条，继续下次循环
					continue;
				}
				Chkind chkind=new Chkind();//入库单从表
				Supply supply=new Supply();
				supply.setSp_code(dis.getSp_code());
				chkind.setChkinno(chkinno);
				chkind.setSupply(supply);
				chkind.setAmount(dis.getCntfirm());//数量为门店验货数量
				chkind.setPrice(dis.getPricesale());//售价为0取出库价
				chkind.setMemo(dis.getDes());
				chkind.setDued(dis.getDued());
				chkind.setInout(DDT.IN);
				chkind.setPound(0);
				chkind.setSp_id(dis.getSpno());
				chkind.setChkstono(Integer.parseInt(null == dis.getChkstoNo() ? "0" : dis.getChkstoNo()));
				chkind.setAmount1(dis.getCntfirm1());//
				chkind.setPcno(dis.getPcno());//保存批次号
				chkind.setChkno(dis.getChkno());//保存出库单号到入库单
				chkindMisMapper.saveChkind(chkind);
				//如果报货来的，则修改CHKYH = Y ，安全库存报货用
				if(null != dis.getChkstoNo() && !"0".equals(dis.getChkstoNo())){
					inspectionMapper.updateChkstodByChkstonoSpcode(chkind);
				}
			}
			chkinm.setMonth(commonMISBOHService.getOnlyAccountMonth(chkinm.getMaded(),chkinm.getPositn().getCode()));//  此字段当成月份使用     加入会计月
			chkinm.setChecby(accountName);
			chkinm.setStatus(DDT.IN);
			chkinm.setChk1memo(LocalUtil.getI18N("Out_Inspection_Generation_In_Order")+LocalUtil.getI18N("Check_through"));
			chkinmMisMapper.checkChkinm(chkinm);//审核入库单
			
			//supplyacct反写入库单号
			HashMap<String, Object> disMap = new HashMap<String, Object>();
			disMap.put("listids", ids);
			disMap.put("accountName", accountName);
			disMap.put("acct", acct);
			disMap.put("chkinno", chkinno);
			inspectionMapper.check(disMap);//到货单存入库单号
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 更新出库单状态
	 * @param ids
	 * @param sp_codes
	 * @param checby
	 * @param acct
	 * @throws CRUDException
	 */
	public void check(String ids, String sp_codes, String checby, String acct) throws CRUDException {
		try{
			//1.先更新状态
			HashMap<String, Object> disMap=new HashMap<String, Object>();
			disMap.put("listids", ids);
			disMap.put("sp_codes", CodeHelper.replaceCode(sp_codes));
			disMap.put("accountName", checby);
			disMap.put("acct", acct);
			inspectionMapper.check(disMap);
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 反审核出库单状态
	 * @param ids
	 * @param sp_codes
	 * @param acct
	 * @throws CRUDException
	 */
	public void uncheck(String ids, String sp_codes, String acct) throws CRUDException {
		try{
			//1.先更新状态
			HashMap<String, Object> disMap=new HashMap<String, Object>();
			disMap.put("listids", ids);
			disMap.put("sp_codes", CodeHelper.replaceCode(sp_codes));
			disMap.put("acct", acct);
			inspectionMapper.uncheck(disMap);
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 生成档口直发单 档口报货 （配送验货）
	 * @author wjf
	 * @param chkinm
	 * @return
	 */
	public int saveChkinmDept(Chkinm chkinm,String ids,String sp_ids,String sp_codes) throws CRUDException {
		try{
			//1.5 更新报货单表chkstod为已验货状态 安全库存报货用
			inspectionMapper.updateChkstodByChkstodDeptId(sp_ids);
			
			//2.生成档口的直发单
			log.warn("配送中心到货直发档口：");
			//最后处理  list大小不为0  时候  在作为一条入库单据加入
			if(chkinm.getChkindList().size()!=0){
				//如果验收数量都是空，则没必要生成入库单
				boolean flag = false;//默认为false,如果有一条数据验货数量不是0，则继续进行入库操作
				for (int j = 0; j < chkinm.getChkindList().size(); j++) {
					Chkind dis=(Chkind)chkinm.getChkindList().get(j);
					if(dis.getAmount() != 0){
						flag = true;
						break;
					}
				}
				if(flag){
					saveChkinmzbDeptBySupplyAcct(chkinm,ids,sp_codes);
				}
			}
			//3.更新chkstod_dept chkin  字段为已入库
			inspectionMapper.updateChkstodDeptChkin(sp_ids);
			return 1;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}	
	private int saveChkinmzbDeptBySupplyAcct(Chkinm chkinm,String ids,String sp_codes) throws CRUDException {
		try {
			Date date = null == chkinm.getMaded() ? new Date() : chkinm.getMaded();
			chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
			chkinm.setChkinno(chkinmMisMapper.getMaxChkinno());
			chkinm.setVouno(calChkNum.getNextBytable(DDT.VOUNO_CHKINM,DDT.VOUNO_ZF+chkinm.getPositn().getCode()+"-",date));
			chkinm.setMaded(date);
			chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
			//供应商为当前分店所属片区的主直拨库对应的供应商
			Deliver deliver = inspectionMapper.findTpDeliver(chkinm.getPositn());
			chkinm.setDeliver(deliver);
			chkinm.setTotalamt(0);
			chkinm.setInout(DDT.INOUT);
			chkinm.setTyp(DDT.ZCZF);//正常直发
//			chkinm.setTyp(DDT.PSZF);//配送直发 20160606wjf
			if(chkinm.getChkindList().size() > 0 && chkinm.getChkindList().get(0).getAmount() < 0){
				chkinm.setTyp(DDT.ZFYHCX);//如果是小于0的单子，单据类型是 验货冲消
			}
			chkinm.setMemo(LocalUtil.getI18N("Out_Inspection_Generation_Inout_Order"));//"配送验货直发档口"
			log.warn("添加直发单(主单)：\n"+chkinm);
	 		chkinmMisMapper.saveChkinm(chkinm);
			for (int j = 0; j < chkinm.getChkindList().size(); j++) {
				Chkind chkind = chkinm.getChkindList().get(j);//入库单从表
				if(chkind.getAmount() == 0){//如果验货数量为0，则直接跳过这条，继续下次循环
					continue;
				}
				chkind.setChkinno(chkinm.getChkinno());
				chkind.setInout(DDT.INOUT);   //加入直发标志
				chkind.setPound(0);
//				chkind.setChkno();//supplyacct的chkno 这个还真取不到这个值 因为是sum的出库数量
				chkindMisMapper.saveChkind(chkind);
				//如果报货来的，则修改CHKYH = Y ，安全库存报货用
				if(0 != chkind.getChkstono()){
					inspectionMapper.updateChkstodByChkstonoSpcode(chkind);
				}
			}
			//审核
			chkinm.setMonth(commonMISBOHService.getOnlyAccountMonth(chkinm.getMaded(),chkinm.getPositn().getCode()));//  此字段当成月份使用     加入会计月
			chkinm.setChecby(chkinm.getMadeby());
			chkinm.setStatus(DDT.INOUT);
			chkinm.setChk1memo(LocalUtil.getI18N("Out_Inspection_Generation_Inout_Order")+LocalUtil.getI18N("Check_through"));
			chkinmMisMapper.checkChkinm(chkinm);//审核入库单
			//supplyacct反写入库单号
			HashMap<String, Object> disMap=new HashMap<String, Object>();
			disMap.put("listids", ids);
			disMap.put("accountName", chkinm.getMadeby());
			disMap.put("acct", chkinm.getAcct());
			disMap.put("chkinno", chkinm.getChkinno());
			disMap.put("sp_codes", CodeHelper.replaceCode(sp_codes));
			inspectionMapper.check(disMap);//到货单存入库单号
			return chkinm.getPr();
		} catch (CRUDException e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 分档口验货 统配验货生成档口直发单或者门店入库单
	 * @author wjf
	 * @param chkinm
	 * @return
	 */
	public int saveChkinmGroup(Chkinm chkinm,String ids) throws CRUDException {
		try{
			//判断走门店入库 还是档口直发
			if(chkinm.getChkindList().size()!=0){
				List<Chkind> firmChkind = new ArrayList<Chkind>();
				List<Chkind> deptChkind = new ArrayList<Chkind>();
				//如果验收数量都是空，则没必要生成入库单
				boolean flag = false;//默认为false,如果有一条数据验货数量不是0，则继续进行入库操作
				for (int j = 0; j < chkinm.getChkindList().size(); j++) {
					Chkind dis=(Chkind)chkinm.getChkindList().get(j);
					if(dis.getAmount() != 0){
						flag = true;
						//档口的和门店的分开
						if(null == dis.getSupply().getSp_name() || "".equals(dis.getSupply().getSp_name())){//档口的
							deptChkind.add(dis);
						}else{
							firmChkind.add(dis);
						}
					}
				}
				if(flag){
					//先入门店
					if(firmChkind.size() != 0){
						chkinm.setChkindList(firmChkind);
						saveChkinmBySupplyAcct(chkinm,ids);
					}
					//再直发档口
					if(deptChkind.size() != 0){
						chkinm.setChkindList(deptChkind);
						saveChkinmzbDeptBySupplyAcct(chkinm,ids,"");
					}
				}
			}
			return 1;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	private int saveChkinmBySupplyAcct(Chkinm chkinm,String ids) throws CRUDException{
		try {
			Date date = null == chkinm.getMaded() ? new Date() : chkinm.getMaded();
			chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
			chkinm.setChkinno(chkinmMisMapper.getMaxChkinno());
			chkinm.setVouno(calChkNum.getNextBytable(DDT.VOUNO_CHKINM,DDT.VOUNO_RK+chkinm.getPositn().getCode()+"-",date));
			chkinm.setMaded(date);
			chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
			//供应商为当前分店所属片区的主直拨库对应的供应商
			Deliver deliver = inspectionMapper.findTpDeliver(chkinm.getPositn());
			chkinm.setDeliver(deliver);
			chkinm.setTotalamt(0);
			chkinm.setInout(DDT.IN);
			chkinm.setTyp(DDT.ZCRK);//正常入库
//			chkinm.setTyp(DDT.PSRK);//配送入库  20160606wjf
			if(chkinm.getChkindList().size() > 0 && chkinm.getChkindList().get(0).getAmount() < 0){
				chkinm.setTyp(DDT.YHCX);//如果是小于0的单子，单据类型是 验货冲消
			}
			chkinm.setMemo(LocalUtil.getI18N("Out_Inspection_Generation_In_Order_By_Dept"));//"配送分档口验货生成入库单"
			log.warn("添加直发单(主单)：\n"+chkinm);
	 		chkinmMisMapper.saveChkinm(chkinm);
			for (int j = 0; j < chkinm.getChkindList().size(); j++) {
				Chkind chkind = chkinm.getChkindList().get(j);//入库单从表
				if(chkind.getAmount() == 0){//如果验货数量为0，则直接跳过这条，继续下次循环
					continue;
				}
				chkind.setChkinno(chkinm.getChkinno());
				chkind.setIndept(null);
				chkind.setInout(DDT.IN);
				chkind.setPound(0);
				chkindMisMapper.saveChkind(chkind);
				//如果报货来的，则修改CHKYH = Y ，安全库存报货用
				if(0 != chkind.getChkstono()){
					inspectionMapper.updateChkstodByChkstonoSpcode(chkind);
				}
			}
			//审核
			chkinm.setMonth(commonMISBOHService.getOnlyAccountMonth(chkinm.getMaded(),chkinm.getPositn().getCode()));//  此字段当成月份使用     加入会计月
			chkinm.setChecby(chkinm.getMadeby());
			chkinm.setStatus(DDT.IN);
			chkinm.setChk1memo(LocalUtil.getI18N("Out_Inspection_Generation_In_Order_By_Dept")+LocalUtil.getI18N("Check_through"));
			chkinmMisMapper.checkChkinm(chkinm);//审核入库单
			//反写入库单号 supplyacct存入库单号  这里的id会有重复 ，所以先去除重复
			Set<Integer> set = new HashSet<Integer>();
			for(Chkind d : chkinm.getChkindList()){
				set.add(d.getId());
			}
			StringBuffer sb = new StringBuffer();
			for(Integer i : set){
				sb.append(i + ",");
			}
			HashMap<String, Object> disMap=new HashMap<String, Object>();
			disMap.put("listids", sb.substring(0, sb.length()-1));
			disMap.put("accountName", chkinm.getMadeby());
			disMap.put("acct", chkinm.getAcct());
			disMap.put("chkinno", chkinm.getChkinno());
			inspectionMapper.check(disMap);
			return chkinm.getPr();
		} catch (CRUDException e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***********************************************配送验货 end**********************************************************/

	/***********************************************调拨验货**********************************************************/
	
	/***
	 * 查询调拨验货数据 分页用
	 * @param disMap
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<SupplyAcct> selectByKeyDb(HashMap<String, Object> disMap, Page page) throws CRUDException{
		try {
			return pageManager1.selectPage(disMap, page, InspectionMisMapper.class.getName()+".selectByKeyDb");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 查询配送验货数据 不分页
	 * @param disMap
	 * @return
	 * @throws CRUDException
	 */
	public List<SupplyAcct> selectByKeyDb(HashMap<String, Object> disMap) throws CRUDException{
		try {
			return inspectionMapper.selectByKeyDb(disMap);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 调拨验货生成入库单 不启用档口情况下
	 * @param ids
	 * @param accountName
	 * @param acct
	 * @throws CRUDException
	 */
	public void checkDb(List<SupplyAcct> disList, String ids, String accountName, String acct, Date maded) throws CRUDException {
		try {
			log.warn("门店调拨到货验收入库：");
			//最后处理  list大小不为0  时候  在作为一条入库单据加入
			if(disList.size()!=0){
				String positn = disList.get(0).getPositn();
				List<SupplyAcct> list = new ArrayList<SupplyAcct>();
				for (int i = 0; i < disList.size(); i++) {//循环数据
					if(positn.equals(disList.get(i).getPositn())){
						list.add(disList.get(i));
					}else{//不相等的时候
						if(list.size() != 0){
							//如果验收数量都是空，则没必要生成入库单
							boolean flag = false;//默认为false,如果有一条数据验货数量不是0，则继续进行入库操作
							for (int j = 0; j < list.size(); j++) {
								SupplyAcct dis=(SupplyAcct)list.get(j);
								if(dis.getCntfirm() != 0){
									flag = true;
								}else{//特殊情况 如果验货数量是0（可能是直接审核的极端情况  需要特殊处理一下wjf）
									SupplyAcct supplyAcct_ = new SupplyAcct();
									supplyAcct_.setAcct(acct);
									supplyAcct_.setId(dis.getId());
									supplyAcct_.setCntfirm(0);
									supplyAcct_.setCntfirm1(0);
									supplyAcct_.setCntfirmks(dis.getCntout());//门店亏
									supplyAcct_.setCntlogistks(0);
									inspectionMapper.updateAcct(supplyAcct_);
								}
							}
							if(flag){
								saveChkinmBySupplyAcctDb(list,acct,accountName,maded);
							}
							list.clear();//清空
							list.add(disList.get(i));//在list重新加入该条数据
						}
						positn = disList.get(i).getPositn();//把循环外的值换为当前数据
					}
				}
				if(list.size() != 0){
					//如果验收数量都是空，则没必要生成入库单
					boolean flag = false;//默认为false,如果有一条数据验货数量不是0，则继续进行入库操作
					for (int j = 0; j < list.size(); j++) {
						SupplyAcct dis=(SupplyAcct)list.get(j);
						if(dis.getCntfirm() != 0){
							flag = true;
						}else{//特殊情况 如果验货数量是0（可能是直接审核的极端情况  需要特殊处理一下wjf）
							SupplyAcct supplyAcct_ = new SupplyAcct();
							supplyAcct_.setAcct(acct);
							supplyAcct_.setId(dis.getId());
							supplyAcct_.setCntfirm(0);
							supplyAcct_.setCntfirm1(0);
							supplyAcct_.setCntfirmks(dis.getCntout());//门店亏
							supplyAcct_.setCntlogistks(0);
							inspectionMapper.updateAcct(supplyAcct_);
						}
					}
					if(flag){
						saveChkinmBySupplyAcctDb(list,acct,accountName,maded);
					}
				}
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	private void saveChkinmBySupplyAcctDb(List<SupplyAcct> list,String acct,String accountName,Date maded) throws CRUDException {
		try{
			log.warn("调拨验收入库生成入库单:");
			Date date = null == maded ? new Date() : maded;
			Chkinm chkinm=new Chkinm();
			String vouno = calChkNum.getNextBytable(DDT.VOUNO_CHKINM,DDT.VOUNO_DR+list.get(0).getFirm()+"-",date);
			int chkinno = chkinmMisMapper.getMaxChkinno();
		  	chkinm.setAcct(acct);
		  	chkinm.setMadeby(accountName);
		  	chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
		  	chkinm.setChkinno(chkinno);
		  	chkinm.setVouno(vouno);
		  	chkinm.setMaded(date);
			chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
			Positn p1=new Positn();
			p1.setCode(list.get(0).getFirm());
			chkinm.setPositn(p1);
			Positn positn=new Positn();
			positn.setCode(null == list.get(0).getPositnNm() || "".equals(list.get(0).getPositnNm()) ? list.get(0).getPositn() : list.get(0).getPositnNm());
			//供应商为当前调拨出库的门店对应的供应商
			Deliver deliver = inspectionMapper.findDeliverByPositn(positn);
			if(null == deliver){
				log.warn("没有设置仓位"+positn.getCode()+"对应的供应商！");
				throw new CRUDException("没有设置仓位"+positn.getCode()+"对应的供应商！");
			}
			chkinm.setDeliver(deliver);
			chkinm.setTyp(DDT.DBRK);//调拨入库
			chkinm.setInout(DDT.IN);
			chkinm.setMemo(LocalUtil.getI18N("Transfer_Inspection_Generation_In_Order"));//"调拨验货生成入库单"
			chkinmMisMapper.saveChkinm(chkinm);
			for (int j = 0; j < list.size(); j++) {
				SupplyAcct dis=(SupplyAcct)list.get(j);
				if(dis.getCntfirm() == 0){//如果验货数量为0，则直接跳过这条，继续下次循环
					continue;
				}
				Chkind chkind=new Chkind();//入库单从表
				Supply supply=new Supply();
				supply.setSp_code(dis.getSp_code());
				chkind.setChkinno(chkinno);
				chkind.setSupply(supply);
				chkind.setAmount(dis.getCntfirm());//数量为门店验货数量
				chkind.setPrice(dis.getPricesale());//售价为0取出库价
				chkind.setMemo(dis.getDes());
				chkind.setDued(dis.getDued());
				chkind.setInout(DDT.IN);
				chkind.setPound(0);
				chkind.setSp_id(dis.getSpno());
				chkind.setChkstono(Integer.parseInt(null == dis.getChkstoNo() ? "0" : dis.getChkstoNo()));
				chkind.setAmount1(dis.getCntfirm1());//
				chkind.setPcno(dis.getPcno());//保存批次号
				chkind.setChkno(dis.getChkno());//supplyacct的chkno
				chkindMisMapper.saveChkind(chkind);
			}
			chkinm.setMonth(commonMISBOHService.getOnlyAccountMonth(chkinm.getMaded(),chkinm.getPositn().getCode()));//  此字段当成月份使用     加入会计月
			chkinm.setChecby(accountName);
			chkinm.setStatus(DDT.IN);
			chkinm.setChk1memo(LocalUtil.getI18N("Transfer_Inspection_Generation_In_Order")+LocalUtil.getI18N("Check_through"));
			chkinmMisMapper.checkChkinm(chkinm);//审核入库单
			//反写
			StringBuffer sb = new StringBuffer();
			for(SupplyAcct s : list){
				sb.append(s.getId()+",");
			}
			HashMap<String, Object> disMap=new HashMap<String, Object>();
			disMap.put("listids", sb.substring(0, sb.length()-1));
			disMap.put("accountName", accountName);
			disMap.put("chkinno", chkinm.getChkinno());
			disMap.put("acct", acct);
			inspectionMapper.check(disMap);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 分档口验货  调拨验货生成档口直发单或者门店入库单
	 * @author wjf
	 * @param chkinm
	 * @return
	 * @throws Exception 
	 */
	public int saveChkinmGroupDb(Chkinm chkinm,String ids) throws CRUDException {
		try{
			//判断走门店入库 还是档口直发
			if(chkinm.getChkindList().size()!=0){
				List<Chkind> firmChkind = new ArrayList<Chkind>();
				List<Chkind> deptChkind = new ArrayList<Chkind>();
				//如果验收数量都是空，则没必要生成入库单
				boolean flag = false;//默认为false,如果有一条数据验货数量不是0，则继续进行入库操作
				for (int j = 0; j < chkinm.getChkindList().size(); j++) {
					Chkind dis=(Chkind)chkinm.getChkindList().get(j);
					if(dis.getAmount() != 0){
						flag = true;
						//档口的和门店的分开
						if(null == dis.getSupply().getSp_name() || "".equals(dis.getSupply().getSp_name())){//档口的
							deptChkind.add(dis);
						}else{
							firmChkind.add(dis);
						}
					}
					
				}
				if(flag){
					//先入门店
					if(firmChkind.size()!=0){
						String deliver = firmChkind.get(0).getDeliver();
						List<Chkind> list = new ArrayList<Chkind>();
						for (int i = 0; i < firmChkind.size(); i++) {//循环数据
							if(deliver.equals(firmChkind.get(i).getDeliver())){
								list.add(firmChkind.get(i));
							}else{//不相等的时候
								if(list.size() != 0){
									chkinm.setChkindList(list);
									saveChkinmBySupplyAcctDb(chkinm);
									list.clear();//清空
									list.add(firmChkind.get(i));//在list重新加入该条数据
								}
								deliver = firmChkind.get(i).getDeliver();//把循环外的值换为当前数据
							}
						}
						if(list.size() != 0){
							chkinm.setChkindList(list);
							saveChkinmBySupplyAcctDb(chkinm);
						}
					}
					//再直发档口
					if(deptChkind.size()!=0){
						String deliver = deptChkind.get(0).getDeliver();
						List<Chkind> list = new ArrayList<Chkind>();
						for (int i = 0; i < deptChkind.size(); i++) {//循环数据
							if(deliver.equals(deptChkind.get(i).getDeliver())){
								list.add(deptChkind.get(i));
							}else{//不相等的时候
								if(list.size() != 0){
									chkinm.setChkindList(list);
									saveChkinmzbDeptBySupplyAcctDb(chkinm);
									list.clear();//清空
									list.add(deptChkind.get(i));//在list重新加入该条数据
								}
								deliver = deptChkind.get(i).getDeliver();//把循环外的值换为当前数据
							}
						}
						if(list.size() != 0){
							chkinm.setChkindList(list);
							saveChkinmzbDeptBySupplyAcctDb(chkinm);
						}
					}
				}
			}
			return 1;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	private int saveChkinmBySupplyAcctDb(Chkinm chkinm) throws CRUDException{
		try{
			Date date = null == chkinm.getMaded() ? new Date() : chkinm.getMaded();
			chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
			chkinm.setChkinno(chkinmMisMapper.getMaxChkinno());
			chkinm.setVouno(calChkNum.getNextBytable(DDT.VOUNO_CHKINM,DDT.VOUNO_DR+chkinm.getPositn().getCode()+"-",date));
			chkinm.setMaded(date);
			chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
			Positn positn=new Positn();
			positn.setCode(chkinm.getChkindList().get(0).getDeliver());
			//供应商为当前调拨出库的门店对应的供应商
			Deliver deliver = inspectionMapper.findDeliverByPositn(positn);
			if(null == deliver){
				log.warn("没有设置仓位"+positn.getCode()+"对应的供应商！");
				throw new CRUDException("没有设置仓位"+positn.getCode()+"对应的供应商！");
			}
			chkinm.setDeliver(deliver);
			chkinm.setTotalamt(0);
			chkinm.setInout(DDT.IN);
			chkinm.setTyp(DDT.DBRK);//调拨入库
			chkinm.setMemo(LocalUtil.getI18N("Transfer_Inspection_Generation_In_Order_By_Dept"));//"调拨分档口验货生成入库单"
			log.warn("添加入库单(主单)：\n"+chkinm);
	 		chkinmMisMapper.saveChkinm(chkinm);
			for (int j = 0; j < chkinm.getChkindList().size(); j++) {
				Chkind chkind = chkinm.getChkindList().get(j);//入库单从表
				if(chkind.getAmount() == 0){//如果验货数量为0，则直接跳过这条，继续下次循环
					continue;
				}
				chkind.setChkinno(chkinm.getChkinno());
				chkind.setIndept(null);
				chkind.setInout(DDT.IN);
				chkind.setPound(0);
				chkindMisMapper.saveChkind(chkind);
			}
			//审核
			chkinm.setMonth(commonMISBOHService.getOnlyAccountMonth(chkinm.getMaded(),chkinm.getPositn().getCode()));//  此字段当成月份使用     加入会计月
			chkinm.setChecby(chkinm.getMadeby());
			chkinm.setStatus(DDT.IN);
			chkinm.setChk1memo(LocalUtil.getI18N("Transfer_Inspection_Generation_In_Order_By_Dept")+LocalUtil.getI18N("Check_through"));
			chkinmMisMapper.checkChkinm(chkinm);//审核入库单
			//反写入库单号 supplyacct存入库单号  这里的id会有重复 ，所以先去除重复
			Set<Integer> set = new HashSet<Integer>();
			for(Chkind d : chkinm.getChkindList()){
				set.add(d.getId());
			}
			StringBuffer sb = new StringBuffer();
			for(Integer i : set){
				sb.append(i + ",");
			}
			HashMap<String, Object> disMap=new HashMap<String, Object>();
			disMap.put("listids", sb.substring(0, sb.length()-1));
			disMap.put("accountName", chkinm.getMadeby());
			disMap.put("acct", chkinm.getAcct());
			disMap.put("chkinno", chkinm.getChkinno());
			inspectionMapper.check(disMap);
			return chkinm.getPr();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	private int saveChkinmzbDeptBySupplyAcctDb(Chkinm chkinm) throws CRUDException{
		try{
			Date date = null == chkinm.getMaded() ? new Date() : chkinm.getMaded();
			chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
			chkinm.setChkinno(chkinmMisMapper.getMaxChkinno());
			chkinm.setVouno(calChkNum.getNextBytable(DDT.VOUNO_CHKINM,DDT.VOUNO_DR+chkinm.getPositn().getCode()+"-",date));
			chkinm.setMaded(date);
			chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
			Positn positn=new Positn();
			positn.setCode(chkinm.getChkindList().get(0).getDeliver());
			//供应商为当前调拨出库的门店对应的供应商
			Deliver deliver = inspectionMapper.findDeliverByPositn(positn);
			if(null == deliver){
				log.warn("没有设置仓位"+positn.getCode()+"对应的供应商！");
				throw new CRUDException("没有设置仓位"+positn.getCode()+"对应的供应商！");
			}
			chkinm.setDeliver(deliver);
			chkinm.setTotalamt(0);
			chkinm.setInout(DDT.INOUT);
			chkinm.setTyp(DDT.DBZF);//调拨直发
			chkinm.setMemo(LocalUtil.getI18N("Transfer_Inspection_Generation_Inout_Order_By_Dept"));//"调拨分档口验货生成直发单"
			log.warn("添加直发单(主单)：\n"+chkinm);
	 		chkinmMisMapper.saveChkinm(chkinm);
			for (int j = 0; j < chkinm.getChkindList().size(); j++) {
				Chkind chkind = chkinm.getChkindList().get(j);//入库单从表
				if(chkind.getAmount() == 0){//如果验货数量为0，则直接跳过这条，继续下次循环
					continue;
				}
				chkind.setChkinno(chkinm.getChkinno());
				chkind.setInout(DDT.INOUT);   //加入直发标志
				chkind.setPound(0);
				chkindMisMapper.saveChkind(chkind);
			}
			//审核
			chkinm.setMonth(commonMISBOHService.getOnlyAccountMonth(chkinm.getMaded(),chkinm.getPositn().getCode()));//  此字段当成月份使用     加入会计月
			chkinm.setChecby(chkinm.getMadeby());
			chkinm.setStatus(DDT.INOUT);
			chkinm.setChk1memo(LocalUtil.getI18N("Transfer_Inspection_Generation_Inout_Order_By_Dept")+LocalUtil.getI18N("Check_through"));
			chkinmMisMapper.checkChkinm(chkinm);//审核入库单
			//反写入库单号 supplyacct存入库单号  这里的id会有重复 ，所以先去除重复
			Set<Integer> set = new HashSet<Integer>();
			for(Chkind d : chkinm.getChkindList()){
				set.add(d.getId());
			}
			StringBuffer sb = new StringBuffer();
			for(Integer i : set){
				sb.append(i + ",");
			}
			HashMap<String, Object> disMap=new HashMap<String, Object>();
			disMap.put("listids", sb.substring(0, sb.length()-1));
			disMap.put("accountName", chkinm.getMadeby());
			disMap.put("acct", chkinm.getAcct());
			disMap.put("chkinno", chkinm.getChkinno());
			inspectionMapper.check(disMap);
			return chkinm.getPr();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}

	/***
	 * 九毛九直配到货单
	 * @param arrivalmMis
	 * @param page
	 * @return
	 */
	public List<ArrivalmMis> findArrivalmList(ArrivalmMis arrivalmMis,Page page) {
		return arrivalmPage.selectPage(arrivalmMis, page, InspectionMisMapper.class.getName()+".findArrivalmList");
	}
	
	/***
	 * 九毛九直配到货单
	 * @param arrivalmMis
	 * @return
	 */
	public List<ArrivalmMis> findArrivalmList(ArrivalmMis arrivalmMis) {
		return inspectionMapper.findArrivalmList(arrivalmMis);
	}

	/***
	 * 根据主表查询到货单从表
	 * @param arrivalmMis
	 * @return
	 */
	public List<ArrivaldMis> findArrivaldList(ArrivalmMis arrivalmMis) {
		return inspectionMapper.findArrivaldList(arrivalmMis);
	}
	
	/***
	 * 修改九毛九直配验货
	 * @param arrivalmMis
	 * @return
	 * @throws CRUDException
	 */
	public String updateArrivald(ArrivalmMis arrivalmMis) throws CRUDException {
		Map<String,Object> result = new HashMap<String,Object>();
		try {
			arrivalmMis.setIstate(1);//保存改为1
			inspectionMapper.updateArrivalm(arrivalmMis);
			for (int i = 0; i < arrivalmMis.getArrivaldList().size(); i++) {
				ArrivaldMis ad = arrivalmMis.getArrivaldList().get(i);
				inspectionMapper.updateArrivald(ad);
				result.put("pr", "succ");
			}
			result.put("updateNum", arrivalmMis.getArrivaldList().size());
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}

	/***
	 * 九毛九直配验收入库
	 * @param arrivalmMis
	 * @param adList
	 * @return
	 */
	public String checkArrivalm(ArrivalmMis arrivalmMis, List<ArrivaldMis> adList) throws CRUDException {
		try{
			//1.先更新到货单主表的状态
			arrivalmMis.setIstate(3);//已验货
			inspectionMapper.updateArrivalm(arrivalmMis);
			//如果验收数量都是空，则没必要生成入库单
			boolean flag = false;//默认为false,如果有一条数据验货数量不是0，则继续进行入库操作
			if(adList.size() != 0){
				for (int j = 0; j < adList.size(); j++) {
					ArrivaldMis dis = (ArrivaldMis)adList.get(j);
					if(dis.getNinsnum() != 0){//如果验货数量为0，则直接跳过这条，继续下次循环
						flag = true;
						break;
					}
				}
			}
			if(!flag){//如果数量都是0  不生成入库单，直接返回成功
				inspectionMapper.updateChkstodByArrivalm(arrivalmMis);
				return "1";
			}
			//2.验收入库
			Chkinm chkinm = new Chkinm();
			String vouno = calChkNum.getNextBytable(DDT.VOUNO_CHKINM,DDT.VOUNO_RK+arrivalmMis.getPk_org()+"-",arrivalmMis.getMaded());
			int chkinno = chkinmMisMapper.getMaxChkinno();
		  	chkinm.setAcct(arrivalmMis.getAcct());
		  	chkinm.setMadeby(arrivalmMis.getMadeby());
		  	chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
		  	chkinm.setChkinno(chkinno);
		  	chkinm.setVouno(vouno);
		  	chkinm.setMaded(DateFormat.formatDate(arrivalmMis.getMaded(),"yyyy-MM-dd"));
			chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
			Positn positn=new Positn();
			positn.setCode(arrivalmMis.getPk_org());
			chkinm.setPositn(positn);
			Deliver deliver = new Deliver();
			deliver.setCode(arrivalmMis.getPk_supplier());
			chkinm.setDeliver(deliver);
			chkinm.setTyp(DDT.ZCRK);//正常入库
			chkinm.setMemo(null == arrivalmMis.getVmemo()?"直配验货生成入库单,"+arrivalmMis.getVarrbillno():arrivalmMis.getVmemo()+","+arrivalmMis.getVarrbillno());
			chkinm.setInout(DDT.IN);
			chkinmMisMapper.saveChkinm(chkinm);
			for (int j = 0; j < adList.size(); j++) {
				ArrivaldMis dis=(ArrivaldMis)adList.get(j);
				if(dis.getNinsnum() == 0){//如果验货数量为0，则直接跳过这条，继续下次循环
					continue;
				}
				Chkind chkind = new Chkind();//入库单从表
				Supply supply = new Supply();
				supply.setSp_code(dis.getVcode());
				chkind.setChkinno(chkinno);
				chkind.setSupply(supply);
				chkind.setAmount(dis.getNinsnum());//数量为验货数量
				chkind.setPrice(dis.getNprice());
				chkind.setMemo(dis.getVmemo());
				chkind.setDued(null);
				chkind.setInout(DDT.IN);
				chkind.setPound(0);
				chkind.setSp_id(0);
				chkind.setChkstono(arrivalmMis.getChkstono());
				chkind.setAmount1(0);//可以不填 存储过程会算
				chkind.setPcno(null);//保存批次号
				chkind.setChkno(Integer.parseInt(arrivalmMis.getVarrbillno()));//arrivalm的varrbillno
				chkindMisMapper.saveChkind(chkind);
			}
			chkinm.setMonth(commonMISBOHService.getOnlyAccountMonth(chkinm.getMaded(),chkinm.getPositn().getCode()));//  此字段当成月份使用     加入会计月
			chkinm.setChecby(arrivalmMis.getMadeby());
			chkinm.setStatus(DDT.IN);//存储过程中未用
			chkinm.setChk1memo("直配验货生成入库单审核通过");
			chkinmMisMapper.checkChkinm(chkinm);//审核入库单
			//3.修改报货单状态
			inspectionMapper.updateChkstodByArrivalm(arrivalmMis);
			//4.到货单里存入库单号
			arrivalmMis.setIchkinno(chkinno);
			inspectionMapper.updateArrivalm(arrivalmMis);
			return "1";
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}

	/***
	 * 九毛九统配验货根据单据
	 * @param sa
	 * @param page
	 * @return
	 */
	public List<SupplyAcct> listSupplyAcctTP(SupplyAcct sa, Page page) {
		return pageManager1.selectPage(sa, page, InspectionMisMapper.class.getName()+".listSupplyAcctTP");
	}
	
	/***
	 * 九毛九统配验货根据单据 不分页
	 * @param sa
	 * @return
	 */
	public List<SupplyAcct> listSupplyAcctTP(SupplyAcct sa) {
		return inspectionMapper.listSupplyAcctTP(sa);
	}

	/***
	 * 九毛九统配按单据验货
	 * @param sa
	 * @param disList
	 */
	public String checkSupplyAcctTP(SupplyAcct sa, List<SupplyAcct> disList) throws CRUDException {
		try{
			//1.先更新为已验货
			StringBuffer sb = new StringBuffer();
			for(SupplyAcct s : disList){
				sb.append(s.getId()+",");
			}
			HashMap<String, Object> disMap=new HashMap<String, Object>();
			disMap.put("listids", sb.substring(0, sb.length()-1));
			disMap.put("accountName", sa.getMadeby());
			disMap.put("acct", sa.getAcct());
			inspectionMapper.check(disMap);
			
			if("I".equals(DDT.insByArrOrIns)){//如果是入库验货数量的才走这个判断,因为到货数量不可能是0
				//如果验收数量都是空，则没必要生成入库单
				boolean flag = false;//默认为false,如果有一条数据验货数量不是0，则继续进行入库操作
				if(disList.size()!=0){
					for (int j = 0; j < disList.size(); j++) {
						SupplyAcct dis=(SupplyAcct)disList.get(j);
						if(dis.getCntfirm() != 0){
							flag = true;
						}else{//特殊情况 如果验货数量是0（可能是直接审核的极端情况  需要特殊处理一下wjf）
							SupplyAcct supplyAcct_ = new SupplyAcct();
							supplyAcct_.setAcct(sa.getAcct());
							supplyAcct_.setId(dis.getId());
							supplyAcct_.setCntfirm(0);
							supplyAcct_.setCntfirm1(0);
							supplyAcct_.setCntfirmks(dis.getCntout());//门店亏
							supplyAcct_.setCntlogistks(0);
							inspectionMapper.updateAcct(supplyAcct_);
						}
					}
				}
				if(!flag){//如果数量都是0  不生成入库单，直接返回成功
					return "1";
				}
			}
			//2.验收入库
			Chkinm chkinm = new Chkinm();
			String vouno = calChkNum.getNextBytable(DDT.VOUNO_CHKINM,DDT.VOUNO_RK+sa.getFirm()+"-",sa.getMaded());
			int chkinno = chkinmMisMapper.getMaxChkinno();
		  	chkinm.setAcct(sa.getAcct());
		  	chkinm.setMadeby(sa.getMadeby());
		  	chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
		  	chkinm.setChkinno(chkinno);
		  	chkinm.setVouno(vouno);
		  	chkinm.setMaded(DateFormat.formatDate(sa.getMaded(),"yyyy-MM-dd"));
			chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
			Positn positn=new Positn();
			positn.setCode(sa.getFirm());
			chkinm.setPositn(positn);
			//供应商为当前分店所属片区的主直拨库对应的供应商
			Deliver deliver = inspectionMapper.findTpDeliver(positn);
			chkinm.setDeliver(deliver);
			chkinm.setTyp(DDT.ZCRK);//正常入库
			if(disList.size() > 0 && disList.get(0).getCntfirm() < 0){
				chkinm.setTyp(DDT.YHCX);//如果是小于0的单子，单据类型是 验货冲消
			}
			chkinm.setMemo("配送验货生成入库单");
			chkinm.setInout(DDT.IN); 
			chkinmMisMapper.saveChkinm(chkinm);
			for (int j = 0; j < disList.size(); j++) {
				SupplyAcct dis=(SupplyAcct)disList.get(j);
				Chkind chkind=new Chkind();//入库单从表
				Supply supply=new Supply();
				supply.setSp_code(dis.getSp_code());
				chkind.setChkinno(chkinno);
				chkind.setSupply(supply);
				chkind.setAmount(dis.getCntfirm());//数量为门店验货数量
				if("A".equals(DDT.insByArrOrIns))
					chkind.setAmount(dis.getCntout());//数量为门店到货数量
				chkind.setPrice(dis.getPricesale());//售价为0取出库价
				chkind.setMemo(dis.getDes());
				chkind.setDued(dis.getDued());
				chkind.setInout(DDT.IN);
				chkind.setPound(0);
				chkind.setSp_id(dis.getSpno());
				chkind.setChkstono(Integer.parseInt(null == dis.getChkstoNo() ? "0" : dis.getChkstoNo()));
				chkind.setAmount1(dis.getCntfirm1());//数量为门店验货数量
				if("A".equals(DDT.insByArrOrIns))
					chkind.setAmount1(dis.getCntuout());//数量为门店到货数量
				chkind.setPcno(dis.getPcno());//保存批次号
				chkind.setChkno(dis.getChkno());//配送入库 保存supplyacct的chkno
				//如果报货来的，则修改CHKYH = Y ，安全库存报货用
				if(null != dis.getChkstoNo() && !"0".equals(dis.getChkstoNo())){
					inspectionMapper.updateChkstodByChkstonoSpcode(chkind);
				}
				if(chkind.getAmount() == 0){//如果验货数量为0，则直接跳过这条，继续下次循环
					continue;
				}
				chkindMisMapper.saveChkind(chkind);
			}
			chkinm.setMonth(commonMISBOHService.getOnlyAccountMonth(chkinm.getMaded(),chkinm.getPositn().getCode()));//  此字段当成月份使用     加入会计月
			chkinm.setChecby(sa.getMadeby());
			chkinm.setStatus(DDT.IN);//存储过程中未用
			chkinm.setChk1memo("配送验货生成入库单审核通过");
			chkinmMisMapper.checkChkinm(chkinm);//审核入库单
			//3.反写单号
			disMap.put("chkinno", chkinno);
			inspectionMapper.check(disMap);//到货单存入库单号
			return "1";
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
		
	}

	/***
	 * 导出直配验货单 九毛九
	 * @param os
	 * @param arrivalmMis
	 * @param list
	 */
	public void exportArrivaldList(OutputStream os, ArrivalmMis arrivalmMis, List<ArrivaldMis> list) {
		WritableWorkbook workBook = null;
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);
		WritableCellFormat doubleStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES),new NumberFormat("0.00"));//设定带小数数字单元格格式
		try {
			titleStyle.setAlignment(Alignment.CENTRE);
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet("验货单", 0);
			sheet.addCell(new Label(0, 0,arrivalmMis.getTitle(),titleStyle));
			sheet.mergeCells(0, 0, 9, 0);
			sheet.addCell(new Label(0, 1, "物资编码")); 
            sheet.addCell(new Label(1, 1, "物资名称"));   
            sheet.addCell(new Label(2, 1, "规格")); 
            sheet.addCell(new Label(3, 1, "报货数量"));
			sheet.addCell(new Label(4, 1, "到货数量"));
			sheet.addCell(new Label(5, 1, "验货数量"));
			sheet.addCell(new Label(6, 1, "单位"));
			sheet.addCell(new Label(7, 1, "单价"));
			sheet.addCell(new Label(8, 1, "金额"));
			sheet.addCell(new Label(9, 1, "备注"));
            //遍历list填充表格内容
			int index = 1;
            for(int j=0; j<list.size(); j++) {
            	if(j == list.size()){
            		break;
            	}
            	index += 1;
            	sheet.addCell(new Label(0, index, list.get(j).getVcode()));
            	sheet.addCell(new Label(1, index, list.get(j).getVname()));
            	sheet.addCell(new Label(2, index, list.get(j).getSp_desc()));
            	sheet.addCell(new jxl.write.Number(3,index,list.get(j).getNpurnum(),doubleStyle));
        		sheet.addCell(new jxl.write.Number(4,index,list.get(j).getNarrnum(),doubleStyle));
        		sheet.addCell(new jxl.write.Number(5,index,list.get(j).getNinsnum(),doubleStyle));
            	sheet.addCell(new Label(6, index, list.get(j).getPk_unit()));
            	sheet.addCell(new jxl.write.Number(7,index,list.get(j).getNprice(),doubleStyle));
        		sheet.addCell(new jxl.write.Number(8,index,list.get(j).getNmoney(),doubleStyle));
            	sheet.addCell(new Label(9, index, list.get(j).getVmemo()));
	    	}	            
			workBook.write();
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/***
	 * 导出直配验货单
	 * @param os
	 * @param list
	 * @param dis
	 * @return
	 */
	public boolean exportDis(ServletOutputStream os, List<Dis> list, Dis dis) {
		WritableWorkbook workBook = null;
		WritableFont titleFont0 = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle0 = new WritableCellFormat(titleFont0);
		WritableFont titleFont1 = new WritableFont(WritableFont.TIMES, 10,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.GREY_80_PERCENT);
		WritableCellFormat titleStyle1 = new WritableCellFormat(titleFont1);
		WritableFont titleFont2 = new WritableFont(WritableFont.TIMES, 10,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.RED);
		WritableCellFormat titleStyle2 = new WritableCellFormat(titleFont2);
		WritableCellFormat doubleStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES),new NumberFormat("0.00"));//设定带小数数字单元格格式
		try {
			titleStyle0.setAlignment(Alignment.CENTRE);
			titleStyle0.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			titleStyle1.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			titleStyle2.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet(dis.getFirmDes()+"直配验货单", 0);
			sheet.addCell(new Label(0, 0,dis.getFirmDes()+"直配验货单",titleStyle0));
			sheet.mergeCells(0, 0, 15, 0);
			sheet.addCell(new Label(0, 1, "供应商编码",titleStyle1)); 
            sheet.addCell(new Label(1, 1, "供应商名称",titleStyle1));
            sheet.addCell(new Label(2, 1, "到货日期",titleStyle1));
            sheet.addCell(new Label(3, 1, "报货日期",titleStyle1));
            sheet.addCell(new Label(4, 1, "报货单号",titleStyle1));
            sheet.addCell(new Label(5, 1, "凭证号",titleStyle1));
            sheet.addCell(new Label(6, 1, "物资编码",titleStyle1));
            sheet.addCell(new Label(7, 1, "物资名称",titleStyle1));
        	sheet.addCell(new Label(8, 1, "规格",titleStyle1));
        	sheet.addCell(new Label(9, 1, "采购数量",titleStyle1));
        	sheet.addCell(new Label(10, 1, "采购单位",titleStyle1));
        	sheet.addCell(new Label(11, 1, "标准数量",titleStyle2));
        	sheet.addCell(new Label(12, 1, "标准单位",titleStyle1));
    		sheet.addCell(new Label(13, 1, "单价",titleStyle1));
        	sheet.addCell(new Label(14, 1, "金额",titleStyle1));
        	sheet.addCell(new Label(15, 1, "备注",titleStyle1));
        	
            //遍历list填充表格内容
			int index = 1;
            for(int j=0; j<list.size(); j++) {
            	if(j == list.size()){
            		break;
            	}
            	index += 1;
            	sheet.addCell(new Label(0, index, list.get(j).getDeliverCode()));
            	sheet.addCell(new Label(1, index, list.get(j).getDeliverDes()));
            	sheet.addCell(new Label(2, index, DateFormat.getStringByDate(list.get(j).getMaded(),"yyyy-MM-dd")));
            	sheet.addCell(new Label(3, index, DateFormat.getStringByDate(list.get(j).getInd(),"yyyy-MM-dd")));
            	sheet.addCell(new Label(4, index, list.get(j).getChkstoNo()+""));
            	sheet.addCell(new Label(5, index, list.get(j).getVouno()));
            	sheet.addCell(new Label(6, index, list.get(j).getSp_code()));
            	sheet.addCell(new Label(7, index, list.get(j).getSp_name()));
            	sheet.addCell(new Label(8, index, list.get(j).getSp_desc()));
				sheet.addCell(new jxl.write.Number(9,index,list.get(j).getAmount1sto(),doubleStyle));
				sheet.addCell(new Label(10, index, list.get(j).getUnit3()));
            	sheet.addCell(new jxl.write.Number(11,index,list.get(j).getAmountin(),doubleStyle));
            	sheet.addCell(new Label(12, index, list.get(j).getUnit()));
        		sheet.addCell(new jxl.write.Number(13,index,list.get(j).getPricein(),doubleStyle));
            	sheet.addCell(new jxl.write.Number(14,index,list.get(j).getPricein()*list.get(j).getAmountin(),doubleStyle));
            	sheet.addCell(new Label(15, index, list.get(j).getMemo()));
	    	}	            
			workBook.write();
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	/***
	 * 判断档口报货发来的物资是否在档口报货单里存在
	 * @param disMap
	 * @return
	 */
	public int checkChkstomDeptSpcode(HashMap<String, Object> disMap) {
		return inspectionMapper.checkChkstomDeptSpcode(disMap);
	}

	/***
	 * 判断是否已经验货
	 * @param ids
	 * @param sp_codes
	 * @param acct
	 * @return
	 */
	public int findOutCountByIds(String ids, String sp_codes, String acct) throws CRUDException {
		try{
			HashMap<String, Object> disMap = new HashMap<String, Object>();
			disMap.put("listids", ids);
			disMap.put("sp_codes", CodeHelper.replaceCode(sp_codes));
			disMap.put("acct", acct);
			return inspectionMapper.findOutCountByIds(disMap);
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e.getMessage());
		}
	}
}
