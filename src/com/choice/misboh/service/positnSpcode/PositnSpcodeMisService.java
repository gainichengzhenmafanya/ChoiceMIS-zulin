package com.choice.misboh.service.positnSpcode;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletOutputStream;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.misboh.persistence.positnSpcode.PositnSpcodeMisMapper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.PositnSpcode;

@Service
public class PositnSpcodeMisService {
	
	@Autowired
	private PositnSpcodeMisMapper positnSpcodeMisMapper; 
	@Autowired
	private PageManager<PositnSpcode> pageManager;
	
	/**
	 * 根据分店查询物资
	 * @param positnSpcode
	 * @return
	 */
	public List<PositnSpcode> findPositnSpcode(PositnSpcode positnSpcode ,Page page){
		return pageManager.selectPage(positnSpcode, page, PositnSpcodeMisMapper.class.getName()+".findPositnSpcode");
	}
	public List<PositnSpcode> findPositnSpcode(PositnSpcode positnSpcode){
		return positnSpcodeMisMapper.findPositnSpcode(positnSpcode);
	}
	
	/**
	 * 根据分店查询所有物资 九毛九用
	 * @param positnSpcode
	 * @return
	 */
	public List<PositnSpcode> findPositnSpcodeJmj(PositnSpcode positnSpcode ,Page page){
		return pageManager.selectPage(positnSpcode, page, PositnSpcodeMisMapper.class.getName()+".findPositnSpcodeJmj");
	}
	public List<PositnSpcode> findPositnSpcodeJmj(PositnSpcode positnSpcode){
		return positnSpcodeMisMapper.findPositnSpcodeJmj(positnSpcode);
	}
	
	/***
	 * 导出分店物资属性
	 * @param os
	 * @param map
	 * @param list
	 * @return
	 */
	public boolean exportPositnSpcode(ServletOutputStream os, String positnDes,List<PositnSpcode> positnList){
		
		WritableWorkbook workBook = null;
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);
		try {
			titleStyle.setAlignment(Alignment.CENTRE);
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet(positnDes+"分店物资", 0);
			sheet.addCell(new Label(0, 0,positnDes+"分店物资",titleStyle));
			sheet.mergeCells(0, 0, 11, 0);
            sheet.addCell(new Label(0, 1, "物资编码"));   
            sheet.addCell(new Label(1, 1, "物资名称")); 
			sheet.addCell(new Label(2, 1, "规格"));  
			sheet.addCell(new Label(3, 1, "单位"));
			sheet.addCell(new Label(4, 1, "可申购"));
			sheet.addCell(new Label(5, 1, "可盘点"));
			sheet.addCell(new Label(6, 1, "最低验货比"));
			sheet.addCell(new Label(7, 1, "最高验货比"));
			sheet.addCell(new Label(8, 1, "最高库存"));
			sheet.addCell(new Label(9, 1, "最低库存"));
			sheet.addCell(new Label(10, 1, "日均耗用"));
			sheet.addCell(new Label(11, 1, "采购周期"));
			
			for(int i = 0;i < positnList.size(); i++){
				sheet.addCell(new Label(0, (i+2), positnList.get(i).getSp_code()));   
	            sheet.addCell(new Label(1, (i+2), positnList.get(i).getSp_name())); 
				sheet.addCell(new Label(2, (i+2), positnList.get(i).getSp_desc()));  
				sheet.addCell(new Label(3, (i+2), positnList.get(i).getUnit()));
				sheet.addCell(new Label(4, (i+2), positnList.get(i).getYnsto()==null?"":positnList.get(i).getYnsto()+""));
				sheet.addCell(new Label(5, (i+2), positnList.get(i).getYnpd()==null?"":positnList.get(i).getYnpd()+""));
				sheet.addCell(new Label(6, (i+2), positnList.get(i).getSp_min1()+""));
				sheet.addCell(new Label(7, (i+2), positnList.get(i).getSp_max1()+""));
				sheet.addCell(new Label(8, (i+2), positnList.get(i).getYhrate1()+""));
				sheet.addCell(new Label(9, (i+2), positnList.get(i).getYhrate2()+""));
				sheet.addCell(new Label(10, (i+2), positnList.get(i).getCntuse()));
				sheet.addCell(new Label(11, (i+2), positnList.get(i).getDatsto()==null?"":positnList.get(i).getDatsto()+""));
			}
			workBook.write();
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
}
