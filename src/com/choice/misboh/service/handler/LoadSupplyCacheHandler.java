package com.choice.misboh.service.handler;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.shiro.ScmLoginToken;
import com.choice.framework.shiro.handler.Handler;
import com.choice.framework.shiro.tools.SpringUtils;
import com.choice.framework.shiro.tools.UserSpace;
import com.choice.framework.util.CacheUtil;
import com.choice.misboh.domain.costReduction.MisSupply;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.scm.service.SysParamService;

/**
 * usedfor：
 * Created by javahao on 2017/8/9.
 * auth：JavaHao
 */
@Service("loginSuccessHandler")
public class LoadSupplyCacheHandler implements Handler {
	
	private static final Logger LOG = LoggerFactory.getLogger(LoadSupplyCacheHandler.class);

    @Autowired
    private AccountPositnService accountPositnService;//查询当前登录用户的所属分店 wjf
    @Autowired
    private CommonMISBOHService commonMISBOHService;
    @Override
    public boolean execute() {
        ScmLoginToken slt = UserSpace.getLoginToken();
        //查询当前用户所属分店wjf2014.11.21
        AccountPositn accountPositn= null;
        try {
            accountPositn = accountPositnService.findAccountById(slt.getId());
        } catch (CRUDException e) {
            e.printStackTrace();
        }
        if(null != accountPositn.getPositn()){
            //查找该门店使用物资
            String positnCode = accountPositn.getPositn().getCode();
            MisSupply s = new MisSupply();
            s.setAcct("1");
            s.setSp_position(positnCode);
            s.setOrderBy("length(s.sp_init)");
            List<MisSupply> listSupply = commonMISBOHService.findAllSupply(s);
            CacheUtil.getInstance().put("misSupply_cache", slt.getPkGroup()+positnCode, listSupply);
            UserSpace.getSession().setAttribute("misSupply_cache",listSupply);
        }
       
        // ---------------------------------------------------增加用户登录加载门店参数-------------------------------------------------
        // 增加用户登录加载参数（系统、企业、用户）先redis后DB
        try {
            ScmLoginToken scmLoginToken = UserSpace.getLoginToken();
            LOG.debug("登录成功加载用户数据, 企业编码[{}], 用户名[{}]", scmLoginToken.getPkGroup(), scmLoginToken.getUsername());
            SpringUtils.getBean(SysParamService.class).loginCacheInitialized(scmLoginToken.getPkGroup(), scmLoginToken.getUsername());
        } catch (CRUDException e) {
            LOG.error(e.getMessage(), e);
            return false;
        }
        return true;
        
    }
}
