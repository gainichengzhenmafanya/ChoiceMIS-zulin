package com.choice.misboh.service.publish;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.choice.framework.exception.CRUDException;
import com.choice.misboh.commonutil.publish.PublishUtil;
import com.choice.misboh.domain.publish.PublishContent;
import com.choice.misboh.domain.store.Store;
import com.choice.misboh.persistence.publish.PublishMapper;

/**
 * 描述：线程控制数据下发
 * @author 马振
 * 创建时间：2015-8-25 上午11:35:53
 */
@SuppressWarnings("unused")
public class ThreadRun implements Runnable{
	private static Logger log = Logger.getLogger(PublishService.class);
	private PublishMapper publishMapper;
	private Store store;
	private PublishContent pubData;
	private PublishUtil util;
	private PublishService publishService;
	private String dataSource;
	public ThreadRun() {
	}
	
	/**线程构造
	 * @param storeList
	 * @param pubData
	 * @param publishMapper
	 * @param publishService
	 * @param dataSource
	 */
	public ThreadRun(Store store,PublishContent pubData,PublishMapper publishMapper,PublishService publishService,String dataSource) {
		this.publishMapper = publishMapper;
		this.store = store;
		this.pubData = pubData;
		this.publishService = publishService;
		this.dataSource = dataSource;
	}

	@Override
	public void run() {
		try {
			findPublishData();
		} catch (CRUDException e) {
			e.printStackTrace();
		   throw  new RuntimeException(e);   
		}
	}
	
	public PublishUtil getUtil() {
		if(util==null){
			util=new PublishUtil();
		}
		return util;
	}
	
	/**
	 * 描述：查询数据
	 * @author 马振
	 * 创建时间：2015-8-25 上午11:36:36
	 * @return
	 * @throws CRUDException
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String,Object>> findPublishData() throws CRUDException{
		try {
			String executeName = pubData.getExecuteName();
			System.out.println("=========================" + executeName);
			Boolean onlyEnabled = pubData.isOnlyEnabled();
			Boolean isToService = pubData.isToService();
			Map<String,String> storeMap = new HashMap<String, String>(); 
			List<Map<String,Object>> dataList = null;
			
			storeMap.put("pk_store", store.getPk_store());
			
			//是否需要走 service 层
			if(isToService){
				dataList = (List<Map<String, Object>>) PublishService.class.getDeclaredMethod(executeName, List.class, PublishContent.class,String.class).invoke(publishService, store,pubData,dataSource);
			}else{
				dataList = (List<Map<String, Object>>) PublishService.class.getDeclaredMethod(executeName, Map.class, String.class).invoke(publishService, storeMap, dataSource);
			}

			// 生成XML
			getUtil().toXml(dataList, pubData, store.getVcode());
			
			//判断是否需要下载图片！！
			if (pubData.getFileName().equals("choice.xml")) {
				getUtil().getImages();
			}
			return dataList;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
}