package com.choice.misboh.service.publish;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.MisUtil;
import com.choice.misboh.commonutil.publish.DataForPublish;
import com.choice.misboh.commonutil.publish.PublishUtil;
import com.choice.misboh.domain.publish.PublishContent;
import com.choice.misboh.domain.store.Store;
import com.choice.misboh.persistence.publish.PublishMapper;

/**
 * 描述：数据下发
 * @author 马振
 * 创建时间：2015-8-25 下午4:12:30
 */
@Service
public class PublishService {
	private static Logger log = Logger.getLogger(PublishService.class);
	
	@Autowired
	private PublishMapper publishMapper;
	
	private PublishUtil util;

	public PublishUtil getUtil() {
		if (util == null) {
			util = new PublishUtil();
		}
		return util;
	}

	/**
	 * 描述：获取需要下发的数据名（前台界面）
	 * @author 马振
	 * 创建时间：2015-8-25 上午11:30:38
	 * @return
	 * @throws CRUDException
	 */
	public List<PublishContent> listData() throws CRUDException {
		try {
			return readContent();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 描述：删除Publish下的所有文件，判断是否需要下发图片
	 * @author 马振
	 * 创建时间：2015-8-25 上午11:30:53
	 * @param storeList
	 * @param request
	 * @param imgFile
	 * @return
	 * @throws CRUDException
	 */
	public String operatorFilr(Store store, HttpServletRequest request,String imgFile) throws CRUDException {
		try {
			String path = request.getSession().getServletContext().getRealPath("/");// 获取项目 工作路径
			// 下载数据存放路径
			DataForPublish.setPath(path + "Publish");
			PublishUtil publishUtil = new PublishUtil();
			String str = publishUtil.setUpStoreFile(store);// 根据门店CODE创建文件夹
			if(!"true".equals(str)){
				return str;
			}
			if ("ok".equals(imgFile)) {
				PublishUtil.copyImgToStoreFile(store);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
		return "true";
	}

	/**
	 * 描述：下发
	 * @author 马振
	 * 创建时间：2015-8-25 上午11:31:20
	 * @param dataFileName	要下发的文件名
	 * @param storeList		要下发的分店名
	 * @param request
	 * @param dataSource
	 * @return
	 * @throws CRUDException
	 */
	public String toPublish(String dataFileName, Store store,
			HttpServletRequest request, String dataSource) throws CRUDException {
		String result = "ok";
		PublishContent pubData = null;
		try {
			List<String> dataFileNameList = Arrays.asList(dataFileName.split(","));// 选择的数据ID
			//构建线程池
			ExecutorService pool = Executors.newFixedThreadPool(MisUtil.threadSize);
			for (String elementName : dataFileNameList) {
				
				// 判断DataForPublish里是否有数据
				if (DataForPublish.getPublishMap() == null) {
					readContent();// 获取XML数据
				}
				pubData = DataForPublish.getPublishMap().get(elementName);// 根据选中节点获取节点全部信息
				
				// 判断是否有关联数据
				if (null != pubData.getKey()) { 
					for (Map.Entry<String, PublishContent> entry : DataForPublish
							.getPublishMap().entrySet()) {
						if (pubData.getKey().equals(entry.getValue().getKey())) {// 获取关联的数据
							result = entry.getValue().getDes();
							pool.execute(new ThreadRun(store, entry.getValue(), publishMapper, this,dataSource));
							result = "ok";// 重置状态为成功状态
						}
					}
				} else {// 判断是否有关联数据 无关联↓
					result = pubData.getDes();
					pool.execute(new ThreadRun(store, pubData,publishMapper, this, dataSource));
					result = "ok";// 重置状态为成功状态
				}
			}
			pool.shutdown();
			
			//检测线程执行的任务是否已经完结，没有的话检测直到完成
			while(!pool.awaitTermination(1, TimeUnit.MILLISECONDS)){
				result = "false";
			}
			result = "ok";
			return result;
		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
			result = "false";
			return result;
		}
	}

	/**
	 * 查询数据
	 * 
	 * @param storeList
	 * @param publishContent
	 * @return
	 * @throws CRUDException
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> findPublishData(Store store, PublishContent pubData) throws CRUDException {
		try {
			String executeName = pubData.getExecuteName();
			System.out.println("=========================" + executeName);
			Boolean isToService = pubData.isToService();
			Map<String, String> storeMap = new HashMap<String, String>();
			List<Map<String, Object>> dataList = null;
			
			storeMap.put("pk_store", store.getPk_store());
			
			// 是否需要走 service 层
			if (isToService) {
				PublishService.class.getDeclaredMethod(executeName, List.class, PublishContent.class).invoke(this, store, pubData);
			} else {
				dataList = (List<Map<String, Object>>) PublishMapper.class.getDeclaredMethod(executeName, Map.class).invoke(publishMapper, storeMap);
				
				if (pubData.isByFirm()) {
					getUtil().toXml(dataList, pubData, store.getVcode());// 生成XML
				} else {
					getUtil().toXml(dataList, pubData, null);			
				}
			}
			if (pubData.getFileName().equals("choice.xml")) {// 判断是否需要下载图片！！
				getUtil().getImages();
			}
			return dataList;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 描述：读取content内容
	 * @author 马振
	 * 创建时间：2015-8-25 下午1:03:54
	 * @return
	 * @throws ClassNotFoundException
	 * @throws CRUDException
	 */
	public List<PublishContent> readContent() throws ClassNotFoundException, CRUDException {
		SAXReader reader = new SAXReader();
		Map<String, PublishContent> publishContentsMap = new HashMap<String, PublishContent>();
		List<PublishContent> publishContentsList = new ArrayList<PublishContent>();
		
		// 读入文档流
		Document document = null;
		try{
			document = reader.read(this.getClass().getResourceAsStream("/com/choice/misboh/commonutil/publish/config/misboh.xml"));
		} catch (Exception e) {
			e.getStackTrace();
		}
		resolveXml(document, "misboh", publishContentsMap, publishContentsList);	
			
		DataForPublish.setPublishMap(publishContentsMap);
		return publishContentsList;
	}

	/**
	 * 描述：解析xml
	 * @author 马振
	 * 创建时间：2015-8-25 下午1:10:53
	 * @param document
	 * @param type
	 * @param publishContentsMap
	 * @param publishContentsList
	 */
	@SuppressWarnings("rawtypes")
	public void resolveXml(Document document,String type,Map<String, PublishContent> publishContentsMap,List<PublishContent> publishContentsList) {
		// 获取根节点
		Element root = document.getRootElement();
		for (Iterator elem = root.elementIterator(type); elem.hasNext();) {
			Element bohE = (Element) elem.next();
			for (Iterator classI = bohE.elementIterator("classify"); classI.hasNext();) {
				Element classE = (Element) classI.next();
				PublishContent classPub = new PublishContent();	// 选项分类
				classPub.setDes(classE.elementText("des"));		// 选项名称
				classPub.setKey(classE.elementText("key"));
				classPub.setShow(Boolean.valueOf(classE.elementText("show")));// 是否显示
				classPub.setIsmust(null == classE.elementText("ismust") ? false : true);
				List<PublishContent> list = new ArrayList<PublishContent>();
				PublishContent pub;
				// 解析content节点
				for (Iterator content = classE.elementIterator("content"); content.hasNext();) {
					Element contE = (Element) content.next();
					pub = new PublishContent();
					System.out.println(contE.elementText("des"));
					pub.setDes(contE.elementText("des"));
					pub.setMemo(contE.elementText("memo"));
					pub.setMapping(contE.elementText("mapping"));
					pub.setFileName(contE.elementText("fileName"));
					pub.setRootName(contE.elementText("rootName"));
					pub.setExecuteName(contE.elementText("executeName"));
					pub.setElementName(contE.elementText("elementName"));
					pub.setByFirm(Boolean.valueOf(contE.elementText("isByFirm")));
					pub.setOnlyEnabled(Boolean.valueOf(contE.elementText("onlyEnabled")));
					pub.setToService(Boolean.valueOf(contE.elementText("toService")));
					pub.setShow(Boolean.valueOf(contE.elementText("show")));
					pub.setIsmust(null == contE.elementText("ismust") ? false : true);
					pub.setClassName(classE.elementText("des"));
					publishContentsMap.put(contE.elementText("fileName"),pub);
					list.add(pub);
				}
				PublishContent atomPub;
				
				// 解析atom节点
				for (Iterator atom = classE.elementIterator("atom"); atom.hasNext();) {
					Element atomE = (Element) atom.next();
					// 解析 atom 下的 content
					for (Iterator atom_con = atomE.elementIterator("content"); atom_con.hasNext();) {
						Element ele = (Element) atom_con.next();
						atomPub = new PublishContent();
						atomPub.setKey(atomE.elementText("key"));
						atomPub.setDes(ele.elementText("des"));
						atomPub.setMemo(ele.elementText("memo"));
						atomPub.setMapping(ele.elementText("mapping"));
						atomPub.setFileName(ele.elementText("fileName"));
						atomPub.setRootName(ele.elementText("rootName"));
						atomPub.setExecuteName(ele.elementText("executeName"));
						atomPub.setElementName(ele.elementText("elementName"));
						atomPub.setByFirm(Boolean.valueOf(ele.elementText("isByFirm")));
						atomPub.setOnlyEnabled(Boolean.valueOf(ele.elementText("onlyEnabled")));
						atomPub.setToService(Boolean.valueOf(ele.elementText("toService")));
						atomPub.setShow(Boolean.valueOf(ele.elementText("show")));
						atomPub.setClassName(classE.elementText("des"));
						atomPub.setIsmust(null==ele.elementText("ismust")?false:true);
						publishContentsMap.put(ele.elementText("fileName"),atomPub);
						list.add(atomPub);
					}
				}
				classPub.setPublishContentList(list);
				publishContentsList.add(classPub);
			}
		}
	}
	/**
	 * 更新版本号
	 * wangkai 2015-10-15
	 * @param store
	 * @param dataSource
	 * @throws CRUDException
	 */
	public void updateStore(Store store,String dataSource) throws CRUDException {
		DataSourceSwitch.setDataSourceType(dataSource);// 选择数据源
		try {
			Map<String,String> params = new HashMap<String,String>();
			String dataVer = "";
			dataVer = DateFormat.getStringByDate(new Date(), "yyMMdd") + "001";// 数据版本
			params.put("pk_store", store.getPk_store());
			params.put("dataVer", dataVer);
			publishMapper.updateStore(params);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 描述：门店员工
	 * @author 马振
	 * 创建时间：2015-8-25 下午1:14:23
	 * @param map
	 * @param dataSource
	 * @return
	 * @throws Exception
	 */
	public List<Map<String,Object>> toOperator(Map<String, String> map, String dataSource) throws Exception {
		DataSourceSwitch.setDataSourceType(dataSource);
		return publishMapper.toOperator(map);
	}
}