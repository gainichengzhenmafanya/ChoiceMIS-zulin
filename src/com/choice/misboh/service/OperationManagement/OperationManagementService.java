package com.choice.misboh.service.OperationManagement;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.FormatDouble;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.domain.OperationManagement.CashMx;
import com.choice.misboh.domain.OperationManagement.CashSaving;
import com.choice.misboh.domain.OperationManagement.OutLay;
import com.choice.misboh.domain.OperationManagement.SpecialEvent;
import com.choice.misboh.domain.reportMis.PublicEntity;
import com.choice.misboh.persistence.operationManagement.OperationManagementMapper;
import com.choice.misboh.persistence.reportMis.MisBohNewReportMapper;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.scm.domain.Positn;

/**
 * 描述：运营管理：营业外收入、坐支明细、运营情况登记、存款管理
 * @author 马振
 * 创建时间：2015-5-20 上午10:42:12
 */
@Service
public class OperationManagementService {
	
	private static Logger log = Logger.getLogger(OperationManagementService.class);
	
	@Autowired
	private OperationManagementMapper operationManagementMapper;
	
	@Autowired
	private CommonMISBOHService commonMISBOHService; 
	
	@Autowired
	private MisBohNewReportMapper misBohNewReportMapper;
	
	/**
	 * 描述：查询坐支明细
	 * @author 马振
	 * 创建时间：2015-5-18 上午10:30:25
	 * @return
	 * @throws CRUDException
	 */
	public List<OutLay> findOutLay(OutLay outLay,HttpSession session) throws CRUDException{
		try{
			outLay.setPk_store(this.getPkStore(session));
			
			//如果日期为空，则默认当前日期
			if (ValueUtil.IsEmpty(outLay.getDworkdate())) {
				outLay.setDworkdate(DateJudge.getNowDate());
			}
			
			return operationManagementMapper.findOutLay(outLay);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询坐支明细当前数据
	 * @author 马振
	 * 创建时间：2015-5-20 上午11:05:33
	 * @param outLay
	 * @return
	 * @throws CRUDException
	 */
	public List<OutLay> findOutLayNow(OutLay outLay) throws CRUDException{
		try{
			return operationManagementMapper.findOutLayNow(outLay);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询当前门店同一天内的坐支明细总金额
	 * @author 马振
	 * 创建时间：2015-5-18 下午3:36:35
	 * @param outLay
	 * @return
	 */
	public OutLay findPrice(OutLay outLay,HttpSession session) throws CRUDException{
		try{
			outLay.setPk_store(this.getPkStore(session));
			
			//如果日期为空，则默认当前日期
			if (ValueUtil.IsEmpty(outLay.getDworkdate())) {
				outLay.setDworkdate(DateJudge.getNowDate());
			}
			
			if (ValueUtil.IsNotEmpty(operationManagementMapper.findPrice(outLay))) {
				outLay = operationManagementMapper.findPrice(outLay);
			}
			
			return outLay;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询营业外收入历史数据
	 * @author 马振
	 * 创建时间：2015-5-18 上午10:30:25
	 * @return
	 * @throws CRUDException
	 */
	public List<OutLay> findIncome(OutLay outLay,HttpSession session) throws CRUDException{
		try{
			outLay.setPk_store(this.getPkStore(session));
			
			//如果日期为空，则默认当前日期
			if (ValueUtil.IsEmpty(outLay.getDworkdate())) {
				outLay.setDworkdate(DateJudge.getNowDate());
			}
			
			return operationManagementMapper.findIncome(outLay);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询营业外收入当前数据
	 * @author 马振
	 * 创建时间：2015-5-20 上午11:01:46
	 * @param outLay
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<OutLay> findIncomeNow(OutLay outLay) throws CRUDException{
		try{
			return operationManagementMapper.findIncomeNow(outLay);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询当前门店同一天内的营业外收入总金额和总数量
	 * @author 马振
	 * 创建时间：2015-5-18 下午3:36:35
	 * @param outLay
	 * @return
	 */
	public OutLay findPriceAndQuantity(OutLay outLay,HttpSession session) throws CRUDException{
		try{
			outLay.setPk_store(this.getPkStore(session));
			
			//如果日期为空，则默认当前日期
			if (ValueUtil.IsEmpty(outLay.getDworkdate())) {
				outLay.setDworkdate(DateJudge.getNowDate());
			}
			
			if (ValueUtil.IsNotEmpty(operationManagementMapper.findPriceAndQuantity(outLay))) {
				outLay = operationManagementMapper.findPriceAndQuantity(outLay);
			}
			return outLay;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：保存明细录入数据
	 * @author 马振
	 * 创建时间：2015-5-18 上午11:19:37
	 * @param cashMx
	 */
	public void saveCashMx(List<CashMx> listCashMx,HttpSession session) throws CRUDException{
		try{
			List<CashMx> list = new ArrayList<CashMx>();
			
			//将除了合计项之外的数据放入list中
			for (int i = 0; i < listCashMx.size(); i++) {
				if (ValueUtil.IsNotEmpty(listCashMx.get(i).getVitemcode())) {
					list.add(listCashMx.get(i));
				}
			}
			
			//循环新的list新增
			for (int i = 0; i <list.size(); i++) {
				operationManagementMapper.deleteCashMx(listCashMx.get(i));
				
				listCashMx.get(i).setPk_cashmx(CodeHelper.createUUID().substring(0,20).toUpperCase());
				listCashMx.get(i).setNamount(0.00);
				operationManagementMapper.saveCashMx(listCashMx.get(i));
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询特殊历史事件
	 * @author 马振
	 * 创建时间：2015-5-20 下午6:41:20
	 * @param specialEvent
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<SpecialEvent> listSpecialEvent(SpecialEvent specialEvent,HttpSession session) throws CRUDException{
		try{
			specialEvent.setPk_store(this.getPkStore(session));
			
			//如果日期为空，则赋值当前日期
			if (ValueUtil.IsEmpty(specialEvent.getDworkdate())) {
				specialEvent.setDworkdate(DateJudge.getNowDate());
			}
			
			List<SpecialEvent> listSpecialItem = operationManagementMapper.listSpecialEvent(specialEvent);
			for (int i = 0; i < listSpecialItem.size(); i++) {
				List<SpecialEvent> listSelectItemDtl = new ArrayList<SpecialEvent>();
				
				//特殊事件主表主键放入specialEvent中
				specialEvent.setPk_specialitem(listSpecialItem.get(i).getPk_specialitem());
				
				//根据主表主键查询子表有信息,则放入list中
				if (0 != operationManagementMapper.listSelectItemDtl(specialEvent).size()) {
					for (int k = 0; k < operationManagementMapper.listSelectItemDtl(specialEvent).size(); k++) {
						listSelectItemDtl.add(operationManagementMapper.listSelectItemDtl(specialEvent).get(k));
					}
				}
				
				//将list放入特殊事件表中
				listSpecialItem.get(i).setListSelectItemDtl(listSelectItemDtl);
			}
			
			return listSpecialItem;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询现在所用特殊事件(总部主表为主)
	 * @author 马振
	 * 创建时间：2015-5-20 下午6:52:31
	 * @param specialEvent
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<SpecialEvent> listSpecialItem(SpecialEvent specialEvent) throws CRUDException{
		try{
			List<SpecialEvent> listSpecialItem = operationManagementMapper.listSpecialItem();
			for (int i = 0; i < listSpecialItem.size(); i++) {
				
				//特殊事件主表主键放入specialEvent中
				specialEvent.setPk_specialitem(listSpecialItem.get(i).getPk_specialitem());
				
				//根据主表主键查询子表有信息,则放入list中
				if (0 != operationManagementMapper.listSelectItemDtl(specialEvent).size()) {
					List<SpecialEvent> listSelectItemDtl = new ArrayList<SpecialEvent>();
					for (int k = 0; k < operationManagementMapper.listSelectItemDtl(specialEvent).size(); k++) {
						listSelectItemDtl.add(operationManagementMapper.listSelectItemDtl(specialEvent).get(k));
					}
					
					//将list放入特殊事件表中
					listSpecialItem.get(i).setListSelectItemDtl(listSelectItemDtl);
				}
			}
			return listSpecialItem;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 描述：保存数据
	 * @author 马振
	 * 创建时间：2015-5-20 上午9:48:22
	 * @param listSelectItemDtl
	 * @throws CRUDException
	 */
	public void saveSpecialEvent(List<SpecialEvent> listSelectItemDtl) throws CRUDException{
		try{
			for (int i = 0; i < listSelectItemDtl.size(); i++) {
				operationManagementMapper.deleteSpecialEvent(listSelectItemDtl.get(i));
				listSelectItemDtl.get(i).setPk_specialevent(CodeHelper.createUUID().substring(0,20).toUpperCase());
				operationManagementMapper.saveSpecialEvent(listSelectItemDtl.get(i));
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述： 根据门店和日期查询现金存款额
	 * @author 马振
	 * 创建时间：2016年3月22日上午10:59:41
	 * @param cashSaving
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public CashSaving findCashByDateStore(CashSaving cashSaving, HttpSession session) throws CRUDException{
		try{
			PublicEntity condition = new PublicEntity();
			CashSaving cs = new CashSaving();
			condition.setPk_store(FormatDouble.StringCodeReplace(this.getPkStore(session)));
			
			//若营业日为空，则赋值当前系统日期作为开始日期和结束日期
			if (ValueUtil.IsEmpty(cashSaving.getDworkdate())) {
				condition.setBdat(DateJudge.getNowDate());
				condition.setEdat(DateJudge.getNowDate());
			} else {
				condition.setBdat(cashSaving.getDworkdate());
				condition.setEdat(cashSaving.getDworkdate());
			}
			
			Map<String, Object> findNcouponcamt = misBohNewReportMapper.findNcouponcamt(condition);	//应收现金
			Map<String, Object> findShaoamtCal = misBohNewReportMapper.findShaoamtCal(condition);	//少款合计
			Map<String, Object> findDuoamtCal = misBohNewReportMapper.findDuoamtCal(condition);		//多款合计
			
			Double ncoupon = Double.parseDouble(findNcouponcamt.get("NCOUPONCAMT").toString());		//获取应收现金
			Double shaoamt = Double.parseDouble(findShaoamtCal.get("XAMT").toString());				//获取少款
			Double duoamt = Double.parseDouble(findDuoamtCal.get("XAMT").toString());				//获取多款
			
			cs.setNcash(ncoupon - shaoamt + duoamt);
			
			return cs;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询存款
	 * @author 马振
	 * 创建时间：2015-5-20 下午3:19:28
	 * @param cashSaving
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<CashSaving> findCashSaving(CashSaving cashSaving, HttpSession session) throws CRUDException{
		try{
			cashSaving.setPk_store(this.getPkStore(session));
			return operationManagementMapper.findCashSaving(cashSaving);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：按日期查询存款金额
	 * @author 马振
	 * 创建时间：2015-5-26 上午9:34:49
	 * @param cashSaving
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public CashSaving findAmountCount(CashSaving cashSaving, HttpSession session) throws CRUDException{
		try{
			cashSaving.setPk_store(this.getPkStore(session));
			CashSaving cs = new CashSaving();
			
			//若取出的数据为空，则赋值0.00
			if (ValueUtil.IsEmpty(operationManagementMapper.findAmountCount(cashSaving))) {
				cs.setNamount(0.00);
				cs.setNcash(0.00);
				cs.setNdeposit("0.00");
			} else {
				cs = operationManagementMapper.findAmountCount(cashSaving);
			}
			return cs;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询支付类型为币种的支付方式
	 * @author 马振
	 * 创建时间：2015-5-20 下午3:29:45
	 * @return
	 * @throws CRUDException
	 */
	public List<CashSaving> listPayMode() throws CRUDException{
		try{
			return operationManagementMapper.listPayMode();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询账户类型
	 * @author 马振
	 * 创建时间：2015-5-20 下午5:20:45
	 * @return
	 * @throws CRUDException
	 */
	public List<CashSaving> listAccountType() throws CRUDException{
		try{
			return operationManagementMapper.listAccountType();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：根据主键查询存款
	 * @author 马振
	 * 创建时间：2015-5-20 下午4:35:14
	 * @param cashSaving
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public CashSaving getCashSavingByPk(CashSaving cashSaving, HttpSession session) throws CRUDException{
		try{
			cashSaving.setPk_store(this.getPkStore(session));
			return operationManagementMapper.getCashSavingByPk(cashSaving);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：新增存款
	 * @author 马振
	 * 创建时间：2015-5-20 下午4:37:11
	 * @param cashSaving
	 * @param session
	 * @throws CRUDException
	 */
	public void addCashSaving(CashSaving cashSaving, HttpSession session) throws CRUDException{
		try{
			cashSaving.setPk_cashsaving(CodeHelper.createUUID().substring(0,20).toUpperCase());
			cashSaving.setPk_store(this.getPkStore(session));
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			cashSaving.setVscode(thePositn.getCode());
			cashSaving.setVecode(session.getAttribute("accountName").toString());
			operationManagementMapper.addCashSaving(cashSaving);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：修改存款
	 * @author 马振
	 * 创建时间：2015-5-20 下午5:36:49
	 * @param cashSaving
	 * @throws CRUDException
	 */
	public void updateCashSaving(CashSaving cashSaving) throws CRUDException{
		try{
			operationManagementMapper.updateCashSaving(cashSaving);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述： 根据营业日，获取当前营业日当前门店所有的存款金额
	 * @author 马振
	 * 创建时间：2016年3月24日下午3:06:21
	 * @param cashSaving
	 * @return
	 * @throws CRUDException
	 */
	public Double findAmount(CashSaving cashSaving, HttpSession session) throws CRUDException{
		try{
			cashSaving.setPk_store(this.getPkStore(session));
			return operationManagementMapper.findAmount(cashSaving).getNamount();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：  小于上个单据回单时间的数据不能新增
	 * @author ygb
	 * @param cashSaving
	 * @return
	 * @throws CRUDException
	 */
	public String findSavingDate(CashSaving cashSaving, HttpSession session) throws CRUDException{
		try{
			cashSaving.setPk_store(this.getPkStore(session));
			String savdate = operationManagementMapper.findSavingDate(cashSaving).getVsavingdate();
			String rs="1";
			if(savdate.compareTo(cashSaving.getVsavingdate())>0){
				rs="0";
			}
			return rs;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：  回单流水号是否存在
	 * @author ygb
	 * @param cashSaving
	 * @return
	 * @throws CRUDException
	 */
	public String findSavingNo(CashSaving cashSaving, HttpSession session) throws CRUDException{
		try{
			cashSaving.setPk_store(this.getPkStore(session));
			List<CashSaving> list = new ArrayList<CashSaving>();
			list = operationManagementMapper.findSavingNo(cashSaving);
			String rs="1";
			if(list.size()>0){
				rs="0";
			}
			return rs;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：删除数据
	 * @author 马振
	 * 创建时间：2015-5-20 下午6:12:59
	 * @param cashSaving
	 * @throws CRUDException
	 */
	public void deleteCashSaving(CashSaving cashSaving) throws CRUDException{
		try{
			String[] pk_cashsavings = cashSaving.getPk_cashsaving().split(",");
			
			for (int i = 0; i < pk_cashsavings.length; i++) {
				cashSaving.setPk_cashsaving(pk_cashsavings[i]);
				operationManagementMapper.deleteCashSaving(cashSaving);
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：返回当前门店主键
	 * @author 马振
	 * 创建时间：2015-4-7 下午3:58:41
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public String getPkStore(HttpSession session) throws CRUDException{
		try{
			//返回根据当前门店编码获取的当前门店主键
			return commonMISBOHService.getPkStore(session).getPk_store();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}