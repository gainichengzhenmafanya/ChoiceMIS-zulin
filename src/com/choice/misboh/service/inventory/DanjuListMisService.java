package com.choice.misboh.service.inventory;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletOutputStream;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.NumberFormat;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.util.DateFormat;
import com.choice.misboh.domain.inventory.Danju;
import com.choice.misboh.persistence.inventory.DanjuListMisMapper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

@Service
public class DanjuListMisService {

	@Autowired
	private PageManager<Danju> pageManager;
	
	@Autowired
	private DanjuListMisMapper danjuListMisMapper;
	
	/***
	 * 查询单据列表
	 * @param dis
	 * @return
	 */
	public List<Danju> findListDanju(Danju danju,Page page) {
		return pageManager.selectPage(danju, page, DanjuListMisMapper.class.getName()+".findListDanju");
	}
	
	/***
	 * 查询单据列表
	 * @param dis
	 * @return
	 */
	public List<Danju> findListDanju(Danju danju) {
		return danjuListMisMapper.findListDanju(danju);
	}
	
	/***
	 * 导出excel
	 * @param os
	 * @param list
	 * @param positnSupply
	 * @return
	 */
	public boolean exportDanju(ServletOutputStream os, List<Danju> list, Danju danju) {
		WritableWorkbook workBook = null;
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);
		WritableCellFormat doubleStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES),new NumberFormat("0.00"));//设定带小数数字单元格格式
		try {
			titleStyle.setAlignment(Alignment.CENTRE);
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet(danju.getPositnDes()+"单据列表", 0);
			sheet.addCell(new Label(0, 0, danju.getPositnDes()+"单据列表",titleStyle));
			sheet.mergeCells(0, 0, 12, 0);
            sheet.addCell(new Label(0, 1, "单据类型"));   
            sheet.addCell(new Label(1, 1, "单号")); 
			sheet.addCell(new Label(2, 1, "凭证号"));  
			sheet.addCell(new Label(3, 1, "报货日期"));
			sheet.addCell(new Label(4, 1, "报货时间"));
			sheet.addCell(new Label(5, 1, "合计金额"));
			sheet.addCell(new Label(6, 1, "制单人"));
			sheet.addCell(new Label(7, 1, "审核人"));
			sheet.addCell(new Label(8, 1, "仓位"));
			sheet.addCell(new Label(9, 1, "供应商"));
			sheet.addCell(new Label(10, 1, "领用仓位"));
			sheet.addCell(new Label(11, 1, "摘要"));
            //遍历list填充表格内容
			int index = 1;
            for(int j=0; j<list.size(); j++) {
            	if(j == list.size()){
            		break;
            	}
            	index += 1;
            	sheet.addCell(new Label(0, index, list.get(j).getTypDes()));
            	sheet.addCell(new Label(1, index, list.get(j).getChkno().toString()));
            	sheet.addCell(new Label(2, index, list.get(j).getVouno()));
            	sheet.addCell(new Label(3, index, DateFormat.getStringByDate(list.get(j).getMaded(), "yyyy-MM-dd")));
            	sheet.addCell(new Label(4, index, list.get(j).getMadet()));
            	sheet.addCell(new jxl.write.Number(5,index,list.get(j).getTotalamt(),doubleStyle));
            	sheet.addCell(new Label(6, index, list.get(j).getMadeby()));
        		sheet.addCell(new Label(7, index, list.get(j).getChecby()));
            	sheet.addCell(new Label(8, index, list.get(j).getPositnDes()));
        		sheet.addCell(new Label(9, index, list.get(j).getDeliverDes()));
            	sheet.addCell(new Label(10, index, list.get(j).getFirmDes()));
            	sheet.addCell(new Label(11, index, list.get(j).getMemo()));
	    	}
			workBook.write();
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
}
