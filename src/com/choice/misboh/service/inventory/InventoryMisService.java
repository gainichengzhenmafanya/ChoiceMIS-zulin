package com.choice.misboh.service.inventory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpSession;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.NumberFormat;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.domain.costReduction.MisSupply;
import com.choice.misboh.domain.inspection.ArrivalmMis;
import com.choice.misboh.domain.inventory.Chkstoref;
import com.choice.misboh.domain.inventory.Chkstoreo;
import com.choice.misboh.domain.inventory.Inventory;
import com.choice.misboh.domain.inventory.PositnSupply;
import com.choice.misboh.persistence.inout.ChkindMisMapper;
import com.choice.misboh.persistence.inout.ChkinmMisMapper;
import com.choice.misboh.persistence.inout.ChkoutdMisMapper;
import com.choice.misboh.persistence.inout.ChkoutmMisMapper;
import com.choice.misboh.persistence.inventory.InventoryMisMapper;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.ReadProperties;
import com.choice.scm.domain.Acct;
import com.choice.scm.domain.Chkind;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.InventoryDemod;
import com.choice.scm.domain.InventoryDemom;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Product;
import com.choice.scm.domain.Spcodeexm;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.AcctMapper;
import com.choice.scm.persistence.DeliverMapper;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.service.SupplyService;
import com.choice.scm.util.CalChkNum;

@Service
public class InventoryMisService {
	@Autowired
	private InventoryMisMapper inventoryMisMapper;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private ChkoutmMisMapper chkoutmMisMapper;
	@Autowired
	private CommonMISBOHService commonMISBOHService;
	@Autowired
	private ChkoutdMisMapper chkoutdMisMapper;
	@Autowired
	private SupplyService supplyService;
	
	@Autowired
	private ChkinmMisMapper chkinmMisMapper;
	@Autowired
	private ChkindMisMapper chkindMisMapper;
	@Autowired
	private PositnMapper positnMapper;
	@Autowired
	private DeliverMapper deliverMapper;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	@Autowired
	private AcctMapper acctMapper;
	
	@Autowired 
	private PageManager<Chkstoreo> mapPageManager;
	
	private final transient Log log = LogFactory.getLog(InventoryMisService.class);
	
	
	/***
	 * 查询盘点表中有没有数据
	 * @param cf
	 * @return
	 */
	public Chkstoref findChkstoref(Chkstoref cf) {
		return inventoryMisMapper.findChkstoref(cf);
	}
	
	/***
	 * mis 盘点查询分页，未保存，未审核
	 * @author wjf
	 * @param inventory
	 * @return
	 */
	public List<Inventory> findAllInventoryPage(Inventory inventory) {
		return inventoryMisMapper.findAllInventoryPage(inventory);
	}
	
	/***
	 * mis 盘点总数
	 * @param inventory
	 * @return
	 */
	public int findAllInventoryCount(Inventory inventory) {
		return inventoryMisMapper.findAllInventoryCount(inventory);
	}
	
	/**
	 * 查询盘点信息
	 * @param inventory
	 * @return
	 * @throws CRUDException
	 */
	public List<Inventory> findAllInventory(Inventory inventory) throws CRUDException{
		try{
			inventory.setYearr(mainInfoMapper.findYearrList().get(0));
			StringBuffer startNumber = new StringBuffer();
			StringBuffer startNumber1 = new StringBuffer();
			StringBuffer startMoney = new StringBuffer();
			startNumber.append("A.INC0-A.OUTC0+");
			startNumber1.append("A.INCU0-A.OUTCU0+");
			startMoney.append("A.INA0-A.OUTA0+");
			for(int i = 1; i <= inventory.getMonth(); i++){
				startNumber.append("A.INC" + i + "-A.OUTC" + i + "+");
				startNumber1.append("A.INCU" + i + "-A.OUTCU" + i + "+");
				startMoney.append("A.INA" + i + "-A.OUTA" + i + "+");
			}
			inventory.setStartNumber(startNumber.substring(0, startNumber.length()-1));
			inventory.setStartNumber1(startNumber1.substring(0, startNumber1.length()-1));
			inventory.setStartMoney(startMoney.substring(0, startMoney.length()-1));
			return inventoryMisMapper.findAllInventory(inventory);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询物资当月的进出情况
	 * @param inventory
	 * @return
	 * @throws CRUDException
	 */
	public List<Inventory> findSupplyAcctTheMonth(Inventory inventory) throws CRUDException{
		try{
			return inventoryMisMapper.findSupplyAcctTheMonth(inventory);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 查当前的物资结存
	 * @param inventory
	 * @return
	 */
	public Inventory getWzyue(Inventory inventory) {
		return inventoryMisMapper.getWzyue(inventory);
	}
	
	/***
	 * 查当前的物资在positnSupply中的状态
	 * @param inventory
	 * @return
	 */
	public Inventory getPositnSupply(Inventory inventory) {
		return inventoryMisMapper.getPositnSupply(inventory);
	}
	
	/***
	 * 保存盘点
	 * @param inventory
	 * @param accountName
	 * @throws CRUDException
	 */
	public void updateInventory(Inventory inventory,String accountName) throws CRUDException{
		try{
			inventory.setYearr(mainInfoMapper.findYearrList().get(0));
			//0.修改仓位状态，改为已开始盘点（从模板选择的物资会出现仓位还没开始盘点就保存的情况）
			Positn p = new Positn();
			p.setAcct(inventory.getAcct());
			p.setOldcode(inventory.getPositn().getCode());
			p.setPd("N");
			positnMapper.updatePositn(p);
			//1.当天未审核的数据都删除
			Chkstoref cf = new Chkstoref();
			cf.setDept(inventory.getPositn().getCode());//盘点仓位
			cf.setWorkDate(DateFormat.getStringByDate(inventory.getDate(), "yyyy-MM-dd"));//营业日
			cf.setPantyp(inventory.getPantyp());//日盘/周盘/月盘
			cf.setState("0");//未审核的
			inventoryMisMapper.deleteChkstoreo(cf);//删除从表
			inventoryMisMapper.deleteChkstoref(cf);//删除主表
			String cfid = CodeHelper.createUUID();
			cf.setChkstorefid(cfid);
			cf.setFirm(inventory.getFirm());//所属门店编码
			cf.setEcode(accountName);//保存人
			cf.setInputDate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
			cf.setFromtyp(inventory.getFromtyp());
			inventoryMisMapper.insertChkstoref(cf);//插入主表
			List<Chkstoreo> colist = new ArrayList<Chkstoreo>();
			for (int i = 0; i < inventory.getInventoryList().size(); i++) {//保存子表，禁盘的物资也要保存进去
				Inventory in = inventory.getInventoryList().get(i);
				Chkstoreo co = new Chkstoreo();
				co.setChkstorefid(cfid);
				co.setChkstoreoid(CodeHelper.createUUID());
				co.setSp_code(in.getSp_code());
				co.setCnt1(in.getCnt1());
				co.setCnt2(in.getCnt2());
				co.setCnt3(in.getCnt3());
				co.setCnt4(in.getCnt4());
				co.setStockCnt(in.getStockcnt());
				co.setWorkDate(DateFormat.getStringByDate(inventory.getDate(), "yyyy-MM-dd"));
				co.setYnpd(in.getYnpd());
				co.setYnprint(in.getYnprint());
				co.setOrderNum(in.getOrderNum());
				colist.add(co);
//				inventoryMisMapper.insertChkstoreo(co);//插入盘点从表
				//查询positnsupply 表中有没有 新增的物资 没有则新增
//				PositnSupply ps = new PositnSupply();
//				ps.setAcct(inventory.getAcct());
//				ps.setYearr(inventory.getYearr());
//				ps.setPositn(inventory.getPositn().getCode());
//				ps.setSp_code(in.getSp_code());
//				PositnSupply ps1 = inventoryMisMapper.findPositnSupply(ps);
//				ps.setYnpd(in.getYnpd());
//				ps.setOrderNum(in.getOrderNum());
//				if(ps1 == null){
//					inventoryMisMapper.insertPositnSupply(ps);
//				}else{
//					inventoryMisMapper.updatePositnSupply(ps);
//				}
			}
			inventoryMisMapper.insertChkstoreoList(colist);//批量插入盘点从表
			//此次盘点物资余额表里没有的插进去
			inventory.setChkstorefid(cfid);
			inventoryMisMapper.insertPositnSupplyByChkstoreo(inventory);
			//查询顺序号和是否盘点 不一样的物资 更新下
			List<PositnSupply> list = inventoryMisMapper.findChkstoreoByPositnSupply(inventory);
			for(PositnSupply ps : list){
				inventoryMisMapper.updatePositnSupply(ps);
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
		
	}
	
	/***
	 * 保存库存调整
	 * @param inventory
	 * @param accountName
	 * @throws CRUDException
	 */
	public void updatePositnsupplyMod(Inventory inventory,String accountName) throws CRUDException{
		try{
			inventory.setYearr(mainInfoMapper.findYearrList().get(0));
			//1.当天未审核的数据都删除
			Chkstoref cf = new Chkstoref();
			cf.setDept(inventory.getPositn().getCode());//盘点仓位
			cf.setWorkDate(DateFormat.getStringByDate(inventory.getDate(), "yyyy-MM-dd"));//营业日
			cf.setPantyp(inventory.getPantyp());//日盘/周盘/月盘
			cf.setState("0");//未审核的
			inventoryMisMapper.deleteChkstoreo(cf);//删除从表
			inventoryMisMapper.deleteChkstoref(cf);//删除主表
			String cfid = CodeHelper.createUUID();
			cf.setChkstorefid(cfid);
			cf.setFirm(inventory.getFirm());//所属门店编码
			cf.setEcode(accountName);//保存人
			cf.setInputDate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
			cf.setFromtyp(inventory.getFromtyp());
			inventoryMisMapper.insertChkstoref(cf);//插入主表
			List<Chkstoreo> colist = new ArrayList<Chkstoreo>();
			for (int i = 0; i < inventory.getInventoryList().size(); i++) {//保存子表，禁盘的物资也要保存进去
				Inventory in = inventory.getInventoryList().get(i);
				Chkstoreo co = new Chkstoreo();
				co.setChkstorefid(cfid);
				co.setChkstoreoid(CodeHelper.createUUID());
				co.setSp_code(in.getSp_code());
				co.setCnt1(0);
				co.setCnt2(0);
				co.setCnt3(0);
				co.setCnt4(0);
				co.setPrice(in.getStockcnt() == 0?0:in.getStockamt()/in.getStockcnt());
				co.setStockCnt(in.getStockcnt());
				co.setTotalamt(in.getStockamt());
				co.setWorkDate(DateFormat.getStringByDate(inventory.getDate(), "yyyy-MM-dd"));
				co.setYnpd("Y");
				co.setYnprint("Y");
				co.setOrderNum(0);
				colist.add(co);
			}
			inventoryMisMapper.insertChkstoreoList(colist);//批量插入盘点从表
			//此次盘点物资余额表里没有的插进去
			inventory.setChkstorefid(cfid);
			inventoryMisMapper.insertPositnSupplyByChkstoreo(inventory);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 修改盘点单状态
	 * @param cf
	 * @throws CRUDException
	 */
	public void updateChkstoref(Chkstoref cf) throws CRUDException{
		try{
			inventoryMisMapper.updateChkstoref(cf);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 门店半成品分解
	 * @param inventory
	 * @return
	 */
	private List<Inventory> openEx(Inventory inventory){
		List<Inventory> exBom1 = new ArrayList<Inventory>();//存放半成品的原料list
		try{
			Spcodeexm spcodeexm = new Spcodeexm();
			spcodeexm.setItem(inventory.getSp_code());
			spcodeexm.setAcct(inventory.getAcct());
			List<Product> list = inventoryMisMapper.findMaterial(spcodeexm);
			for(Product p : list){
				Inventory in = new Inventory();
				in.setAcct(inventory.getAcct());
				in.setYearr(inventory.getYearr());
				in.setPositn(inventory.getPositn());
				in.setDate(inventory.getDate());
				in.setSp_code(p.getSupply().getSp_code());//物资编码
				in.setStockcnt(p.getCnt()*inventory.getStockcnt());//物资数量
				Supply s = new Supply();
				s.setSp_code(p.getSupply().getSp_code());
				Supply supply = supplyService.findSupplyById(s);
				in.setPricesale(supply.getPricesale());
				in.setSp_price(supply.getSp_price());
				if("Y".equals(supply.getEx())){//还是门店半成品的  则不添加这一条 ，继续分解
					exBom1.addAll(openEx(in));
				}else{
					//查询positnsupply 表中的余额
					Inventory in2 = inventoryMisMapper.getWzyue(in);
					if(in2 == null){
						in.setCntbla(0);
						in.setCntubla(0);
						in.setAmtbla(0);
					}else{
						in.setCntbla(in2.getCntbla());
						in.setCntubla(in2.getCntubla());
						in.setAmtbla(in2.getAmtbla());
					}
					exBom1.add(in);
				}
			}
		
		}catch(Exception e){
			log.error(e);
		}
		return exBom1;
	}
	
	/***
	 * 分解半成品
	 * @param inven
	 * @param listInventory
	 * @return
	 */
	private List<Inventory> splitEx(Inventory inven,List<Inventory> listInventory){
		List<Inventory> noEx = new ArrayList<Inventory>();//存放去掉半成品的list
		List<Inventory> exBom1 = new ArrayList<Inventory>();//存放半成品的原料list
		List<Inventory> exBom2 = new ArrayList<Inventory>();//存放半成品的原料list = exbom1
		boolean flag = false;//判断有没有半成品
		/**分解门店半成品**/
		//1.先把半成品从盘点中分离出去   noEx 只留原材料  exBom1留半成品分解后的物资
		for (int i = 0; i < listInventory.size(); i++) {
			Inventory inventory=listInventory.get(i);
			inventory.setAcct(inven.getAcct());
			inventory.setYearr(inven.getYearr());
			inventory.setPositn(inven.getPositn());
			inventory.setDate(inven.getDate());
			//判断是不是门店半成品，如果是半成品则需要根据bom拆分成原材料   ，可循环拆分(半成品里还有半成品)
			if("Y".equals(inventory.getEx())){
				flag = true;
				exBom1.addAll(openEx(inventory));
			}else{
				noEx.add(inventory);
			}
		}
		exBom2.addAll(exBom1);//此时2等于1
		//2.相同的物资循环合并
		if(flag){
			for(Inventory i1 : noEx){
				for(Inventory i2 : exBom1){
					if(i1.getSp_code().equals(i2.getSp_code())){
						i1.setStockcnt(i1.getStockcnt()+i2.getStockcnt());
						exBom2.remove(i2);//exbom2中删除掉相同的   只剩下不同的 
					}
				}
			}
		}
		//3.将不同的物资也加到noEx上
		if(exBom2.size()>0){//将不同的物资加到noEx上
			noEx.addAll(exBom2);
		}
		return noEx;
	}
	
	/***
	 * 结束盘点(采用倒挤式盘点 只生成盘亏出库)
	 * @param accountName
	 * @param inven
	 * @param listInventory
	 * @param cf
	 * @return
	 * @throws CRUDException
	 */
	public String checkInventory(HttpSession session,String accountName,Inventory inven,List<Inventory> listInventory,Chkstoref cf, String firm) throws CRUDException{
		try{
			List<Inventory> noEx = splitEx(inven, listInventory);//分解之后的
			//4.循环noEx
			List<Inventory> listInventoryChkOut = new ArrayList<Inventory>();//存放真正出库物资的
			List<Inventory> listInventoryNoChkOut = new ArrayList<Inventory>();//存放没生成耗用的
			for (int i = 0; i < noEx.size(); i++) {
				Inventory inventory=noEx.get(i);
				//如果盘点数-结存数 <=-0.01 或者 结存数-盘点数>=0.01 的才生成盘亏出库
				if((inventory.getStockcnt()-inventory.getCntbla() < -0.001 || inventory.getStockcnt()-inventory.getCntbla() > 0.001) && inventory.getStockcnt()-inventory.getCntbla() != 0){
//				if(inventory.getStockcnt()-inventory.getCntbla() != 0){
					listInventoryChkOut.add(inventory);
					continue;
				}
				//特殊情况下：盘点是0,结存是0 但金额不是0的
				if(inventory.getStockcnt() == 0 && (inventory.getCntbla() >= -0.001 && inventory.getCntbla() <= 0.001) && inventory.getAmtbla() != 0){
					listInventoryChkOut.add(inventory);
					continue;
				}
				listInventoryNoChkOut.add(inventory);
			}
			
			ReadProperties rp = new ReadProperties();
			String profitDeliver = rp.getStrByParam("profitDeliver");//获取盘点仓位
			Positn pk = new Positn();
			pk.setCode(profitDeliver);
			pk = positnMapper.findPositnByCode(pk);
			session.setAttribute("time", 58);//此方法执行进度
			if(pk == null){
				throw new CRUDException("未设置盘亏仓位！无法继续盘点，请联系总部进行设置！");
			}
			Deliver pk2 = new Deliver();
			pk2.setCode(profitDeliver);
			pk2 = deliverMapper.findDeliverByCode(pk2);
			session.setAttribute("time", 60);//此方法执行进度
			if(pk2 == null || pk2.getPositn() == null){
				throw new CRUDException("未设置盘亏供应商或者盘亏供应商未对应盘亏仓位！无法继续盘点，请联系总部进行设置！");
			}
			Date date = inven.getDate();
			//生成出库单
			int chkoutno = 0;
			StringBuffer sb = new StringBuffer();//用来回写单价20170119
			if(listInventoryChkOut.size()>0){
				Map<String,Object> map = new HashMap<String,Object>();
				String vouno = calChkNum.getNextBytable(DDT.VOUNO_CHKOUTM,DDT.VOUNO_CK+firm+"-", date);
				chkoutno=chkoutmMisMapper.findNextNo();
				map.put("acct", inven.getAcct());
				map.put("yearr", inven.getYearr());
				map.put("chkoutno", chkoutno);
				map.put("vouno", vouno);
				map.put("maded", date);
				map.put("madet", DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
				map.put("positn", inven.getPositn().getCode());
				map.put("firm", profitDeliver);
				map.put("madeby", accountName);
				map.put("typ", DDT.PKCK);//盘亏出库
				map.put("memo", null);
				chkoutmMisMapper.saveChkoutm(map);
				session.setAttribute("time", 70);
				
				for (int j = 0; j < listInventoryChkOut.size(); j++) {
					Inventory invt=listInventoryChkOut.get(j);
					Map<String,Object> m = new HashMap<String,Object>();
					m.put("chkoutno", chkoutno);///
					m.put("sp_code", invt.getSp_code());
					m.put("amount", invt.getCntbla()-invt.getStockcnt());
					double price = 0.0;
					//特殊情况下：负库存盘成0的
					if(invt.getCntbla() > 0.01 || (invt.getCntbla() < -0.01 && invt.getStockcnt() == 0)){//根据流程图来的 如果结存数量>0.1 则单价=结存金额/结存数量  
//					if(invt.getCntbla() > 0 || (invt.getCntbla() < 0 && invt.getStockcnt() == 0)){//根据流程图来的 如果结存数量>0 或者结存数量小于0，并且盘成了0  则单价=结存金额/结存数量  
						price = invt.getAmtbla()/invt.getCntbla();
					}else{
						//改为取物资在本仓位的最后进价，没有则取标准价
						MisSupply supply = new MisSupply();
						supply.setAcct(inven.getAcct());
						supply.setSp_code(invt.getSp_code());
						supply.setSp_position(firm);
						supply = commonMISBOHService.findSupplyBySpcode(supply);
						price = (null != supply && 0 != supply.getLast_price()) ? supply.getLast_price() : invt.getSp_price();
//						SupplyAcct sa = new SupplyAcct();
//						sa.setAcct(inven.getAcct());
//						sa.setPositn(inven.getPositn().getCode());
//						sa.setSp_code(invt.getSp_code());
//						Double lastPrice = inventoryMisMapper.findLastPriceBySupplyAcct(sa);
//						price = (lastPrice != null && lastPrice != 0) ? lastPrice : invt.getSp_price() ;//有最后进价取最后进价没有取标准价
					}
					//特殊情况下 盘点是0,结存是0 但金额不是0的
					boolean b = false;
					if(invt.getStockcnt() == 0 && (invt.getCntbla() >= -0.001 && invt.getCntbla() <= 0.001) && invt.getAmtbla() != 0){
						b = true;
						if(invt.getAmtbla() < 0){
							m.put("amount", -0.000001);
							price = invt.getAmtbla()/-0.000001;
						}else{
							m.put("amount", 0.000001);
							price = invt.getAmtbla()/0.000001;
						}
					}
//					if(price < 0){//如果价格还是负的，直接取最后进价
//						MisSupply supply = new MisSupply();
//						supply.setAcct(inven.getAcct());
//						supply.setSp_code(invt.getSp_code());
//						supply.setSp_position(firm);
//						supply = commonMISBOHService.findSupplyBySpcode(supply);
//						price = (null != supply && 0 != supply.getLast_price()) ? supply.getLast_price() : invt.getSp_price();
//					}
					m.put("price", price);
					m.put("memo", null);
					m.put("stoid", 0);
					m.put("batchid", 0);
					m.put("deliver", profitDeliver);
					double cntu = 0;
					if(invt.getUnitper4() == 0){
						cntu = invt.getCnt4();
					}
					if(invt.getUnitper3() == 0){
						cntu = invt.getCnt3();
					}
					if(invt.getUnitper2() == 0){
						cntu = invt.getCnt2();
					}
					m.put("amount1", invt.getCntubla()-cntu);//这里只考虑没有转换率的，如果有转换率的存储过程会重算参考单位数量
					m.put("pricesale", 0);//门店出库都不要售价  wjf2015.9.11
					m.put("chkstono", 0);
					m.put("pr", 0);
					chkoutdMisMapper.saveChkoutd(m);
					
					//如果特殊情况下，盘点是0,结存是0 但金额不是0的  多出了0.000001，要出回来
					if(b){
						Map<String,Object> m1 = new HashMap<String,Object>();
						m1.put("chkoutno", chkoutno);///
						m1.put("sp_code", invt.getSp_code());
						m1.put("amount", 0.0 - ((Double)m.get("amount")));
						m1.put("price", 0);
						m1.put("memo", null);
						m1.put("stoid", 0);
						m1.put("batchid", 0);
						m1.put("deliver", profitDeliver);
						m1.put("amount1", 0.0 - ((Double)m.get("amount1")));//这里只考虑没有转换率的，如果有转换率的存储过程会重算参考单位数量
						m1.put("pricesale", 0);//门店出库都不要售价  wjf2015.9.11
						m1.put("chkstono", 0);
						m1.put("pr", 0);
						chkoutdMisMapper.saveChkoutd(m1);
					}
					
					//更新盘点从表价格
//					Chkstoreo co = new Chkstoreo();
//					co.setChkstorefid(cf.getChkstorefid());
//					co.setSp_code(invt.getSp_code());
//					co.setPrice(price);
//					inventoryMisMapper.updateChkstoreo(co);
					sb.append(" select '"+invt.getSp_code()+"' as sp_code,"+price+" as price from dual union all");
				}
				session.setAttribute("time", 80);
				//审核
				map.put("month", commonMISBOHService.getOnlyAccountMonth(date,firm));
				map.put("emp", accountName);
				map.put("sta", "P");
				chkoutmMisMapper.AuditChkout(map);
			}
			session.setAttribute("time", 90);
			//未生成耗用的也要更新价格 20160324
			if(listInventoryNoChkOut.size() > 0){
				for (int j = 0; j < listInventoryNoChkOut.size(); j++) {
					Inventory invt = listInventoryNoChkOut.get(j);
					double price = 0.0;
					if(invt.getCntbla() != 0)
						price = invt.getAmtbla()/invt.getCntbla();
					//更新盘点从表价格
//					Chkstoreo co = new Chkstoreo();
//					co.setChkstorefid(cf.getChkstorefid());
//					co.setSp_code(invt.getSp_code());
//					co.setPrice(price);
//					inventoryMisMapper.updateChkstoreo(co);
					sb.append(" select '"+invt.getSp_code()+"' as sp_code,"+price+" as price from dual union all");
				}
			}
			if(sb.length() > 0){//批量回写价格
				cf.setMemo((sb.substring(0,sb.length()-9).toString()));
				inventoryMisMapper.updateChkstoreoByCf(cf);
			}
			session.setAttribute("time", 95);
			//更新盘点主表 状态和时间 还有出库单号
			cf.setState("1");//已审核的
			cf.setCheckCode(accountName);
			cf.setCheckDate(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
			cf.setChkoutno(chkoutno == 0?null : chkoutno);
			inventoryMisMapper.updateChkstoref(cf);
			session.setAttribute("time", 100);
			return  "1";
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 结束盘点(采用倒挤式盘点 只生成盘亏出库)
	 * 耗用数量    耗用金额     处理方法
	 *   +     +      正常
	 *   -     -      正常
	 *   0     +      先出掉0.000001 出掉金额，再出-0.000001金额0
	 *   0     -      先出掉-0.000001 出掉金额，再出0.000001金额0
	 *   +     0      
	 *   -     0      
	 *   +     -      先出掉-0.000001 出掉金额，再出数量+0.000001 金额0
	 *   -     +      先出掉0.000001 出掉金额，再出数量-0.000001 金额0
	 *   
	 * @param accountName
	 * @param inven
	 * @param listInventory
	 * @param cf
	 * @return
	 * @throws CRUDException
	 */
	public String checkPositnsupplyMod(String accountName,Inventory inven,List<Inventory> listInventory,Chkstoref cf, String firm) throws CRUDException{
		try{
			String result="";
			List<Inventory> noEx = listInventory;
			//4.循环noEx
			List<Inventory> listInventoryChkOut = new ArrayList<Inventory>();//存放真正出库物资的
			for (int i = 0; i < noEx.size(); i++) {
				Inventory inventory=noEx.get(i);
				//如果盘点数-结存数 <=-0.01 或者 结存数-盘点数>=0.01 的才生成盘亏出库
				if(((inventory.getStockcnt()-inventory.getCntbla() < -0.001 || inventory.getStockcnt()-inventory.getCntbla() > 0.001) && inventory.getStockcnt()-inventory.getCntbla() != 0)
					||((inventory.getStockamt()-inventory.getAmtbla() < -0.001 || inventory.getStockamt()-inventory.getAmtbla() > 0.001) && inventory.getStockamt()-inventory.getAmtbla() != 0)	){
					listInventoryChkOut.add(inventory);
					continue;
				}
			}
			
			ReadProperties rp = new ReadProperties();
			String profitDeliver = rp.getStrByParam("profitDeliver");//获取盘点仓位
			Date date = inven.getDate();
			//生成出库单
			int chkoutno = 0;
			if(listInventoryChkOut.size()>0){
				Map<String,Object> map = new HashMap<String,Object>();
				String vouno = calChkNum.getNextBytable(DDT.VOUNO_CHKOUTM,DDT.VOUNO_CK+firm+"-", date);
				chkoutno=chkoutmMisMapper.findNextNo();
				map.put("acct", inven.getAcct());
				map.put("yearr", inven.getYearr());
				map.put("chkoutno", chkoutno);
				map.put("vouno", vouno);
				map.put("maded", date);
				map.put("madet", DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
				map.put("positn", inven.getPositn().getCode());
				map.put("firm", profitDeliver);
				map.put("madeby", accountName);
				map.put("typ", DDT.PKCK);//盘亏出库
				map.put("memo", null);
				chkoutmMisMapper.saveChkoutm(map);
				
				for (int j = 0; j < listInventoryChkOut.size(); j++) {
					Inventory invt=listInventoryChkOut.get(j);
					Map<String,Object> m = new HashMap<String,Object>();
					m.put("chkoutno", chkoutno);///
					m.put("sp_code", invt.getSp_code());
					double amount = invt.getCntbla() - invt.getStockcnt();
					double totalamt = invt.getAmtbla() - invt.getStockamt();
					m.put("amount", amount);
					double price = 0.0;
					boolean a = false;
					boolean b = false;
					if((amount > 0.001 && totalamt >= 0) || (amount < -0.001 && totalamt <= 0)){//正常情况下，耗用是正 ，金额是正；耗用是负，金额是负
						price = totalamt/amount;
					}else{
						if(amount >= -0.001 && amount <= 0.001){//相当于等于0
							a = true;
							if(totalamt < 0){
								m.put("amount", -0.000001);
								price = totalamt/-0.000001;
							}else{
								m.put("amount", 0.000001);
								price = totalamt/0.000001;
							}
						}else{//数量一正一负 先把金额都出掉
							b = true;
							if(totalamt < 0){
								m.put("amount", -0.000001);
								price = totalamt/-0.000001;
							}else{
								m.put("amount", 0.000001);
								price = totalamt/0.000001;
							}
						}
					}
					m.put("price", price);
					m.put("memo", null);
					m.put("stoid", 0);
					m.put("batchid", 0);
					m.put("deliver", profitDeliver);
					m.put("amount1", invt.getCntubla());//没转换率的先都出掉
					m.put("pricesale", 0);//门店出库都不要售价  wjf2015.9.11
					m.put("chkstono", 0);
					m.put("pr", 0);
					chkoutdMisMapper.saveChkoutd(m);
					//如果特殊情况下，盘点是0,结存是0 但金额不是0的  多出了0.000001，要出回来
					if(a){
						Map<String,Object> m1 = new HashMap<String,Object>();
						m1.put("chkoutno", chkoutno);///
						m1.put("sp_code", invt.getSp_code());
						m1.put("amount", 0.0 - ((Double)m.get("amount")));
						m1.put("price", 0);
						m1.put("memo", null);
						m1.put("stoid", 0);
						m1.put("batchid", 0);
						m1.put("deliver", profitDeliver);
						m1.put("amount1", 0.0 - ((Double)m.get("amount1")));//这里只考虑没有转换率的，如果有转换率的存储过程会重算参考单位数量
						m1.put("pricesale", 0);//门店出库都不要售价  wjf2015.9.11
						m1.put("chkstono", 0);
						m1.put("pr", 0);
						chkoutdMisMapper.saveChkoutd(m1);
					}
					//如果特殊情况下，一正一负的，数量没出
					if(b){
						Map<String,Object> m1 = new HashMap<String,Object>();
						m1.put("chkoutno", chkoutno);///
						m1.put("sp_code", invt.getSp_code());
						m1.put("amount", amount - ((Double)m.get("amount")));
						m1.put("price", 0);
						m1.put("memo", null);
						m1.put("stoid", 0);
						m1.put("batchid", 0);
						m1.put("deliver", profitDeliver);
						m1.put("amount1", amount - ((Double)m.get("amount1")));//这里只考虑没有转换率的，如果有转换率的存储过程会重算参考单位数量
						m1.put("pricesale", 0);//门店出库都不要售价  wjf2015.9.11
						m1.put("chkstono", 0);
						m1.put("pr", 0);
						chkoutdMisMapper.saveChkoutd(m1);
					}
				}
				result +="出库单已生成,单号"+chkoutno+" ";
				//审核
				map.put("month", commonMISBOHService.getOnlyAccountMonth(date,firm));
				map.put("emp", accountName);
				map.put("sta", "P");
				chkoutmMisMapper.AuditChkout(map);
			}
			//更新盘点主表 状态和时间 还有出库单号
			cf.setState("1");//已审核的
			cf.setCheckCode(accountName);
			cf.setCheckDate(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
			cf.setChkoutno(chkoutno == 0?null : chkoutno);
			inventoryMisMapper.updateChkstoref(cf);
			return  result;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 删除库存调整
	 * @param inventory
	 * @throws CRUDException
	 */
	public void delPositnsupplyMod(Inventory inventory) throws CRUDException{
		try{
			inventory.setYearr(mainInfoMapper.findYearrList().get(0));
			//1.当天未审核的数据都删除
			Chkstoref cf = new Chkstoref();
			cf.setDept(inventory.getPositn().getCode());//盘点仓位
			cf.setWorkDate(DateFormat.getStringByDate(inventory.getDate(), "yyyy-MM-dd"));//营业日
			cf.setPantyp(inventory.getPantyp());//日盘/周盘/月盘
			cf.setState("0");//未审核的
			inventoryMisMapper.deleteChkstoreo(cf);//删除从表
			inventoryMisMapper.deleteChkstoref(cf);//删除主表
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
		
	}
	
	/**
	 * 查询历史盘点
	 * @param chkstore
	 * @param page
	 * @return
	 */
	public List<Chkstoreo> findMdPandian(Chkstoreo chkstore, Page page) throws CRUDException{
		try {
			String dept = chkstore.getDept();
			chkstore.setDept(CodeHelper.replaceCode(dept));
			List<Chkstoreo> chkstoreoList = mapPageManager.selectPage(chkstore, page, InventoryMisMapper.class.getName()+".findMdPandian");
			chkstore.setDept(dept);
			return chkstoreoList;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}

	/***
	 * 结束盘点(采用总部仓库盘点 生成盘盈入库和盘亏出库)
	 * @param accountName
	 * @param inven
	 * @param listInventory
	 * @param cf
	 * @param firm
	 * @return
	 * @throws CRUDException
	 */
	public String checkInventoryS(String accountName, Inventory inven,List<Inventory> listInventory, Chkstoref cf, String firm) throws CRUDException{
		try{
			String result="";
			List<Inventory> noEx = splitEx(inven, listInventory);//分解之后的
			//4.循环noEx
			ArrayList<Inventory> listInventoryChkIn =new ArrayList<Inventory>();
			ArrayList<Inventory> listInventoryChkOut=new ArrayList<Inventory>();
			for (int i = 0; i < noEx.size(); i++) {
				Inventory inventory=noEx.get(i);
				if((inventory.getStockcnt()-inventory.getCntbla()>0.01)){//盘盈入库
					listInventoryChkIn.add(inventory);
				}else if((inventory.getStockcnt()-inventory.getCntbla()<-0.01)){//盘亏出库
					listInventoryChkOut.add(inventory);
				}
			}
			ReadProperties rp = new ReadProperties();
			String profitDeliver = rp.getStrByParam("profitDeliver");//获取盘亏仓位
			Date date = inven.getDate();//盘点日期
			Deliver deliver=new Deliver();
			deliver.setCode(profitDeliver);
			//生成入库单
			int chkinno = 0;
			if(listInventoryChkIn.size()>0){
				//生成入库单主表
				Chkinm chkinm=new Chkinm();
				String vouno = calChkNum.getNextBytable(DDT.VOUNO_CHKINM,DDT.VOUNO_RK+firm+"-", date);
				chkinno=chkinmMisMapper.getMaxChkinno();
			  	chkinm.setAcct(inven.getAcct());
			  	chkinm.setMadeby(accountName);
			  	chkinm.setYearr(inven.getYearr());
			  	chkinm.setChkinno(chkinno);
			  	chkinm.setVouno(vouno);
			  	chkinm.setDeliver(deliver);
			  	chkinm.setMaded(DateFormat.formatDate(date, "yyyy-MM-dd"));//入库、单据日期为页面选择日期
				chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss "));
				chkinm.setInout(DDT.IN);
				chkinm.setTyp(DDT.PYRK);//盘盈入库
				chkinm.setPositn(inven.getPositn());//仓位
				chkinmMisMapper.saveChkinm(chkinm);
				//生成入库单从表
				for (int i = 0; i < listInventoryChkIn.size(); i++) {
					Inventory invt=listInventoryChkIn.get(i);
					Chkind chkind=new Chkind();
					chkind.setChkinno(chkinm.getChkinno());
					Supply s = new Supply();
					s.setSp_code(invt.getSp_code());
					chkind.setSupply(s);
					chkind.setAmount(invt.getStockcnt()-invt.getCntbla());  //数量
					double price = 0.0;
					if(invt.getCntbla()>0.1){//根据流程图来的 如果结存数量>0.1 则单价=结存金额/结存数量
						price = invt.getAmtbla()/invt.getCntbla();
					}else{
						//改为取物资在本仓位的最后进价，没有则取标准价
						MisSupply supply = new MisSupply();
						supply.setAcct(inven.getAcct());
						supply.setSp_code(invt.getSp_code());
						supply.setSp_position(firm);
						supply = commonMISBOHService.findSupplyBySpcode(supply);
						price = (null != supply && 0 != supply.getLast_price()) ? supply.getLast_price() : invt.getSp_price();
//						SupplyAcct sa = new SupplyAcct();
//						sa.setAcct(inven.getAcct());
//						sa.setPositn(inven.getPositn().getCode());
//						sa.setSp_code(invt.getSp_code());
//						Double lastPrice = inventoryMisMapper.findLastPriceBySupplyAcct(sa);
//						price = (lastPrice != null && lastPrice != 0) ? lastPrice : invt.getSp_price() ;//有最后进价取最后进价没有取标准价
					}
					chkind.setPrice(price);
//					chkind.setMemo("盘盈入库");
					double cntu = 0;
					if(invt.getUnitper4() == 0){
						cntu = invt.getCnt4();
					}
					if(invt.getUnitper3() == 0){
						cntu = invt.getCnt3();
					}
					if(invt.getUnitper2() == 0){
						cntu = invt.getCnt2();
					}
					chkind.setAmount1(cntu - invt.getCntubla());
					chkind.setInout(DDT.IN);   //加入入库标志
					chkindMisMapper.saveChkind(chkind);
					//更新盘点从表价格
					Chkstoreo co = new Chkstoreo();
					co.setChkstorefid(cf.getChkstorefid());
					co.setSp_code(invt.getSp_code());
					co.setPrice(price);
					inventoryMisMapper.updateChkstoreo(co);
				}
				result +="入库单已生成,单号"+chkinno+" ";
				chkinm.setMonth(commonMISBOHService.getOnlyAccountMonth(chkinm.getMaded(),firm));//  此字段当成月份使用     加入会计月
				chkinm.setChecby(accountName);
				chkinm.setStatus("in");
//				chkinm.setChk1memo("盘盈入库审核通过");
				chkinmMisMapper.checkChkinm(chkinm);//审核入库单
			}
			//生成出库单
			int chkoutno = 0;
			if(listInventoryChkOut.size()>0){
				Map<String,Object> map = new HashMap<String,Object>();
				String vouno = calChkNum.getNextBytable(DDT.VOUNO_CHKOUTM,DDT.VOUNO_CK+firm+"-", date);
				chkoutno=chkoutmMisMapper.findNextNo();
				map.put("acct", inven.getAcct());
				map.put("yearr", inven.getYearr());
				map.put("chkoutno", chkoutno);
				map.put("vouno", vouno);
				map.put("maded", date);
				map.put("madet", DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
				map.put("positn", inven.getPositn().getCode());
				map.put("firm", profitDeliver);
				map.put("madeby", accountName);
				map.put("typ", DDT.PKCK);//盘亏出库
				map.put("memo", null);
				chkoutmMisMapper.saveChkoutm(map);
				
				for (int j = 0; j < listInventoryChkOut.size(); j++) {
					Inventory invt=listInventoryChkOut.get(j);
					Map<String,Object> m = new HashMap<String,Object>();
					m.put("chkoutno", chkoutno);///
					m.put("sp_code", invt.getSp_code());
					m.put("amount", invt.getCntbla()-invt.getStockcnt());
					double price = 0.0;
					if(invt.getCntbla()>0.1){//根据流程图来的 如果结存数量>0.1 则单价=结存金额/结存数量
						price = invt.getAmtbla()/invt.getCntbla();
					}else{
						//改为取物资在本仓位的最后进价，没有则取标准价
						MisSupply supply = new MisSupply();
						supply.setAcct(inven.getAcct());
						supply.setSp_code(invt.getSp_code());
						supply.setSp_position(firm);
						supply = commonMISBOHService.findSupplyBySpcode(supply);
						price = (null != supply && 0 != supply.getLast_price()) ? supply.getLast_price() : invt.getSp_price();
//						SupplyAcct sa = new SupplyAcct();
//						sa.setAcct(inven.getAcct());
//						sa.setPositn(inven.getPositn().getCode());
//						sa.setSp_code(invt.getSp_code());
//						Double lastPrice = inventoryMisMapper.findLastPriceBySupplyAcct(sa);
//						price = (lastPrice != null && lastPrice != 0) ? lastPrice : invt.getSp_price() ;//有最后进价取最后进价没有取标准价
					}
					m.put("price", price);
					m.put("memo", null);
					m.put("stoid", 0);
					m.put("batchid", 0);
					m.put("deliver", profitDeliver);
					double cntu = 0;
					if(invt.getUnitper4() == 0){
						cntu = invt.getCnt4();
					}
					if(invt.getUnitper3() == 0){
						cntu = invt.getCnt3();
					}
					if(invt.getUnitper2() == 0){
						cntu = invt.getCnt2();
					}
					m.put("amount1", invt.getCntubla()-cntu);
					m.put("pricesale", 0);//门店出库都不要售价  wjf2015.9.11
					m.put("chkstono", 0);
					m.put("pr", 0);
					chkoutdMisMapper.saveChkoutd(m);
					//更新盘点从表价格
					Chkstoreo co = new Chkstoreo();
					co.setChkstorefid(cf.getChkstorefid());
					co.setSp_code(invt.getSp_code());
					co.setPrice(price);
					inventoryMisMapper.updateChkstoreo(co);
				}
				result +="出库单已生成,单号"+chkoutno+" ";
				//审核
				map.put("month", commonMISBOHService.getOnlyAccountMonth(date,firm));
				map.put("emp", accountName);
				map.put("sta", "A");//传A 存储过程会判断 走Y
				chkoutmMisMapper.AuditChkout(map);
			}
			//更新盘点主表 状态和时间 还有出库单号
			cf.setState("1");//已审核的
			cf.setCheckCode(accountName);
			cf.setCheckDate(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
			cf.setChkoutno(chkoutno == 0?null : chkoutno);
			cf.setChkinno(chkinno == 0?null : chkinno);
			inventoryMisMapper.updateChkstoref(cf);
			return  result;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 仓库盘点导出excel
	 * @param os
	 * @param list
	 * @return
	 */
	public boolean exportInventory(ServletOutputStream os, List<Inventory> list,Inventory inventory) {
		WritableWorkbook workBook = null;
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);
		WritableCellFormat doubleStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES),new NumberFormat("0.00"));//设定带小数数字单元格格式
		try {
			titleStyle.setAlignment(Alignment.CENTRE);
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet(inventory.getPositn().getDes()+"盘点表", 0);//加上要盘点的仓位名称 wjf
			sheet.addCell(new Label(0, 0,inventory.getPositn().getDes()+"盘点表",titleStyle));
			sheet.mergeCells(0, 0, 16, 0);
			sheet.addCell(new Label(0, 1, "物资编码")); 
            sheet.addCell(new Label(1, 1, "物资名称"));   
            sheet.addCell(new Label(2, 1, "规格")); 
            sheet.addCell(new Label(3, 1, "数量|数量"));
			sheet.addCell(new Label(4, 1, "数量|规格1"));
			sheet.addCell(new Label(5, 1, "数量|数量"));
			sheet.addCell(new Label(6, 1, "数量|规格2"));
			sheet.addCell(new Label(7, 1, "数量|数量"));
			sheet.addCell(new Label(8, 1, "数量|规格3"));
			sheet.addCell(new Label(9, 1, "数量|数量"));
			sheet.addCell(new Label(10, 1, "数量|规格4"));
			sheet.addCell(new Label(11, 1, "合计数量"));
			sheet.addCell(new Label(12, 1, "标准单位"));
			sheet.addCell(new Label(13, 1, "是否半成品"));
			sheet.addCell(new Label(14, 1, "顺序号"));
			sheet.addCell(new Label(15, 1, "禁盘"));
			sheet.addCell(new Label(16, 1, "是否打印"));
            //遍历list填充表格内容
			int index = 1;
            for(int j=0; j<list.size(); j++) {
            	if(j == list.size()){
            		break;
            	}
            	index += 1;
            	sheet.addCell(new Label(0, index, list.get(j).getSp_code()));
            	sheet.addCell(new Label(1, index, list.get(j).getSp_name()));
            	sheet.addCell(new Label(2, index, list.get(j).getSp_desc()));
            	sheet.addCell(new jxl.write.Number(3,index,list.get(j).getCnt1(),doubleStyle));
            	sheet.addCell(new Label(4, index, list.get(j).getSpec1()));
            	if(null == list.get(j).getSpec2()||"".equals(list.get(j).getSpec2())){
            		sheet.addCell(new Label(5, index, ""));
            	}else{
            		sheet.addCell(new jxl.write.Number(5,index,list.get(j).getCnt2(),doubleStyle));
            	}
            	sheet.addCell(new Label(6, index, list.get(j).getSpec2()));
            	if(null == list.get(j).getSpec3()||"".equals(list.get(j).getSpec3())){
            		sheet.addCell(new Label(7, index, ""));
            	}else{
            		sheet.addCell(new jxl.write.Number(7,index,list.get(j).getCnt3(),doubleStyle));
            	}
            	sheet.addCell(new Label(8, index, list.get(j).getSpec3()));
            	if(null == list.get(j).getSpec4()||"".equals(list.get(j).getSpec4())){
            		sheet.addCell(new Label(9, index, ""));
            	}else{
            		sheet.addCell(new jxl.write.Number(9,index,list.get(j).getCnt4(),doubleStyle));
            	}
            	sheet.addCell(new Label(10, index, list.get(j).getSpec4()));
            	sheet.addCell(new jxl.write.Number(11,index,list.get(j).getStockcnt(),doubleStyle));
            	sheet.addCell(new Label(12, index, ""+list.get(j).getUnit()));
            	sheet.addCell(new Label(13, index, "Y".equals(list.get(j).getEx()) ? "是":"否"));
            	sheet.addCell(new Label(14, index, list.get(j).getOrderNum()+""));
            	sheet.addCell(new Label(15, index, "Y".equals(list.get(j).getYnpd()) ? "否":"是"));
            	sheet.addCell(new Label(16, index, "Y".equals(list.get(j).getYnprint()) ? "是":"否"));
	    	}	            
			workBook.write();
			os.flush();
		} catch (IOException e) {
			log.error(e);
		} catch (RowsExceededException e) {
			log.error(e);
		} catch (WriteException e) {
			log.error(e);
		} catch (SecurityException e) {
			log.error(e);
		} catch (IllegalArgumentException e) {
			log.error(e);
		}catch (Exception e) {
			log.error(e);
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				log.error(e);
			} catch (IOException e) {
				log.error(e);
			}
		}
		return true;
	}
	
	/***
	 * 仓库盘点导出excel
	 * @param os
	 * @param list
	 * @return
	 */
	public boolean exportInventoryUnit2(ServletOutputStream os, List<Inventory> list,Inventory inventory) {
		WritableWorkbook workBook = null;
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);
		WritableCellFormat doubleStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES),new NumberFormat("0.00"));//设定带小数数字单元格格式
		try {
			titleStyle.setAlignment(Alignment.CENTRE);
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet(inventory.getPositn().getDes()+"盘点表", 0);//加上要盘点的仓位名称 wjf
			sheet.addCell(new Label(0, 0,inventory.getPositn().getDes()+"盘点表",titleStyle));
			sheet.mergeCells(0, 0, 12, 0);
			sheet.addCell(new Label(0, 1, "物资编码")); 
            sheet.addCell(new Label(1, 1, "物资名称"));   
            sheet.addCell(new Label(2, 1, "规格")); 
			sheet.addCell(new Label(3, 1, "盘点数量"));
			sheet.addCell(new Label(4, 1, "盘点单位"));
        	if("Y".equals(DDT.isDistributionUnit)){
        		sheet.addCell(new Label(5, 1, "配送单位数量"));
    			sheet.addCell(new Label(6, 1, "配送单位"));
            }else{
            	sheet.addCell(new Label(5, 1, "采购单位数量"));
    			sheet.addCell(new Label(6, 1, "采购单位"));
            }
			sheet.addCell(new Label(7, 1, "标准单位数量"));
			sheet.addCell(new Label(8, 1, "标准单位"));
			sheet.addCell(new Label(9, 1, "是否半成品"));
			sheet.addCell(new Label(10, 1, "顺序号"));
			sheet.addCell(new Label(11, 1, "禁盘"));
            //遍历list填充表格内容
			int index = 1;
            for(int j=0; j<list.size(); j++) {
            	if(j == list.size()){
            		break;
            	}
            	index += 1;
            	sheet.addCell(new Label(0, index, list.get(j).getSp_code()));
            	sheet.addCell(new Label(1, index, list.get(j).getSp_name()));
            	sheet.addCell(new Label(2, index, list.get(j).getSp_desc()));
            	sheet.addCell(new jxl.write.Number(3,index,list.get(j).getCnt4(),doubleStyle));
            	sheet.addCell(new Label(4, index, ""+list.get(j).getUnit4()));
            	sheet.addCell(new jxl.write.Number(5,index,list.get(j).getCnt3(),doubleStyle));
            	if("Y".equals(DDT.isDistributionUnit)){
            		sheet.addCell(new Label(6, index, null==list.get(j).getDisunit()? " ":(""+list.get(j).getDisunit())));
                }else{
                	sheet.addCell(new Label(6, index, list.get(j).getUnit3()+""));
                }
            	sheet.addCell(new jxl.write.Number(7,index,list.get(j).getCnt1(),doubleStyle));
            	sheet.addCell(new Label(8, index, ""+list.get(j).getUnit()));
            	sheet.addCell(new Label(9, index, "Y".equals(list.get(j).getEx()) ? "是":"否"));
            	sheet.addCell(new Label(10, index, list.get(j).getOrderNum()+""));
            	sheet.addCell(new Label(11, index, "Y".equals(list.get(j).getYnpd()) ? "否":"是"));
	    	}	            
			workBook.write();
			os.flush();
		} catch (IOException e) {
			log.error(e);
		} catch (RowsExceededException e) {
			log.error(e);
		} catch (WriteException e) {
			log.error(e);
		} catch (SecurityException e) {
			log.error(e);
		} catch (IllegalArgumentException e) {
			log.error(e);
		}catch (Exception e) {
			log.error(e);
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				log.error(e);
			} catch (IOException e) {
				log.error(e);
			}
		}
		return true;
	}

	/***
	 * 查询分店能用的模板主表
	 * @param inventoryDemom
	 * @return
	 */
	public List<InventoryDemom> listInventoryDemom(InventoryDemom inventoryDemom) {
		return inventoryMisMapper.listInventoryDemom(inventoryDemom);
	}

	/***
	 * 查询盘点模板子表内容
	 * @param inventoryDemod
	 * @return
	 */
	public List<InventoryDemod> listInventoryDemod(InventoryDemod inventoryDemod) {
		return inventoryMisMapper.listInventoryDemod(inventoryDemod); 
	}
	
	/***
	 * 查询盘点模板1
	 * @param inventoryDemod
	 * @return
	 */
	public List<Inventory> listInventoryDemod1(InventoryDemod inventoryDemod) {
		return inventoryMisMapper.listInventoryDemod1(inventoryDemod);
	}

	/***
	 * 查询盘点结束数据
	 * @param inventory
	 * @return
	 */
	public List<Inventory> findAllInventoryEnd(Inventory inventory) {
		return inventoryMisMapper.findAllInventoryEnd(inventory);
	}
	
	/***
	 * 查询盘点
	 * @param co
	 * @return
	 * @throws CRUDException
	 */
	public List<Inventory> findChkstoreo(Chkstoreo co) throws CRUDException{
		try{
			return inventoryMisMapper.findChkstoreo(co);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	/***
	 * 直配未验货的单据数量
	 * @param arrivalmMis
	 * @return
	 * @throws CRUDException
	 */
	public int findArrilvalmCount(ArrivalmMis arrivalmMis) throws CRUDException{
		try{
			return inventoryMisMapper.findArrilvalmCount(arrivalmMis);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}

	/***
	 * 直配未验货物资数量
	 * @param dis
	 * @return
	 * @throws CRUDException
	 */
	public int findAllDisCount(Dis dis) throws CRUDException{
		try{
			//改为判断是不是非要采购确认 采购审核之后才能查询 wjf
			Acct acct = acctMapper.findAcctById(dis.getAcct());
			if("Y".equals(acct.getYncgqr())){
				dis.setChk1("Y");
			}else{
				dis.setChk1(null);
			}
			if("Y".equals(acct.getYncgsh())){
				dis.setSta("Y");
			}else{
				dis.setSta(null);
			}
			return inventoryMisMapper.findAllDisCount(dis);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}

	/***
	 * 查询配送未验货按单据
	 * @param sa
	 * @return
	 */
	public int findOutBillCount(SupplyAcct sa) throws CRUDException{
		try{
			return inventoryMisMapper.findOutBillCount(sa);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}

	/***
	 * 查询配送未验货安物资
	 * @param sa
	 * @return
	 */
	public int findOutCount(SupplyAcct sa) throws CRUDException{
		try{
			return inventoryMisMapper.findOutCount(sa);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}

	/***
	 * 调拨未验货物资数量
	 * @param sa
	 * @return
	 * @throws CRUDException
	 */
	public int findOutDbCount(SupplyAcct sa) throws CRUDException{
		try{
			return inventoryMisMapper.findOutDbCount(sa);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}

	/***
	 * 未审核入库单数量
	 * @param map
	 * @return
	 * @throws CRUDException
	 */
	public int findChkinmCount(Map<String,Object> map) throws CRUDException{
		try{
			return inventoryMisMapper.findChkinmCount(map);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}

	/***
	 * 未审核出库单
	 * @param map
	 * @return
	 * @throws CRUDException
	 */
	public int findChkoutmCount(Map<String, Object> map) throws CRUDException{
		try{
			return inventoryMisMapper.findChkoutmCount(map);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
//	/**
//	 * 分店盘点  采用档口的情况下（同总部仓位盘点 生成盘亏出库 盘盈入库）
//	 * @param accountName
//	 * @param inven
//	 * @param listInventory
//	 * @return
//	 * @throws CRUDException
//	 */
//	public String endDeptInventory(String accountName,Inventory inven,List<Inventory> listInventory) throws CRUDException{
//		try{
//			ArrayList<Inventory> listInventoryChkIn =new ArrayList<Inventory>();
//			ArrayList<Inventory> listInventoryChkOut=new ArrayList<Inventory>();
//			String result="";
//			for (int i = 0; i < listInventory.size(); i++) {
//				Inventory inventory=listInventory.get(i);
//				if((inventory.getCnttrival()-inventory.getCntbla()>0.01)){//盘盈入库
//					listInventoryChkIn.add(inventory);
//				}else if((inventory.getCnttrival()-inventory.getCntbla()<-0.01)){//盘亏出库
//					listInventoryChkOut.add(inventory);
//				}
//			}
//			ReadProperties rp = new ReadProperties();
//			String profitDeliver = rp.getStrByParam("profitDeliver");//获取盘点仓位
//			Deliver deliver=new Deliver();
//			deliver.setCode(profitDeliver);
//			//拿到当前盘点哪一天
//			Date date = inven.getDate();
//			//生成入库单
//			if(listInventoryChkIn.size()>0){
//				//生成入库单主表
//				Chkinm chkinm=new Chkinm();
//				String vouno=calChkNum.getNext(DDT.VOUNO_RK,date);
//				int chkinno=chkinmMapper.getMaxChkinno();
//			  	chkinm.setAcct(inven.getAcct());
//			  	chkinm.setMadeby(accountName);
//			  	chkinm.setYearr(inven.getYearr());
//			  	chkinm.setChkinno(chkinno);
//			  	chkinm.setVouno(vouno);
//			  	chkinm.setDeliver(deliver);
//			  	chkinm.setMaded(DateFormat.formatDate(date, "yyyy-MM-dd"));//如库、单据日期为页面选择日期
//				chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss "));
//				chkinm.setInout(ChkinmConstants.in);
//				chkinm.setTyp(ChkinmConstants.inVententoryIn);//盘盈入库
//				chkinm.setPositn(listInventory.get(0).getPositn());   //仓位
//				chkinmMapper.saveChkinm(chkinm);
//				//生成入库单从表
//				for (int i = 0; i < listInventoryChkIn.size(); i++) {
//					Inventory invt=listInventoryChkIn.get(i);
//					Chkind chkind=new Chkind();
//					chkind.setChkinno(chkinm.getChkinno());
//					chkind.setSupply(invt.getSupply());
//					chkind.setAmount(invt.getCnttrival()-invt.getCntbla());  //数量
//					chkind.setPrice(invt.getSupply().getLast_price());
//					chkind.setMemo("盘盈入库");
////					chkind.setDued(date);
//					chkind.setAmount1(invt.getCntutrival() - invt.getCntubla());
//					chkind.setInout(ChkinmConstants.in);   //加入入库标志
//					chkindMapper.saveChkind(chkind);
//				}
//				result +="入库单已生成,单号"+chkinno+" ";
//				chkinm.setChecby(accountName);
//				chkinm.setStatus("入库");
//				chkinm.setChk1memo("盘盈入库审核通过");
//				chkinmMapper.checkChkinm(chkinm);//审核入库单
//			}
//			//生成出库单
//			if(listInventoryChkOut.size()>0){
//				Map<String,Object> map = new HashMap<String,Object>();
//				String vouno=calChkNum.getNext(DDT.VOUNO_CK,date);
//				int chkoutno=chkoutmMisMapper.findNextNo();
//				map.put("acct", inven.getAcct());
//				map.put("yearr", inven.getYearr());
//				map.put("chkoutno", chkoutno);
//				map.put("vouno", vouno);
//				map.put("maded", date);
//				map.put("madet", DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
//				map.put("positn", inven.getPositn().getCode());
//				map.put("firm", deliver.getCode());
//				map.put("madeby", accountName);
//				map.put("typ", ChkoutConstants.inVententoryOut);
//				map.put("memo", ChkoutConstants.inVententoryEndOut);
//				chkoutmMisMapper.saveChkoutm(map);
//				for (int j = 0; j < listInventoryChkOut.size(); j++) {
//					Inventory invt=listInventoryChkOut.get(j);
//					Map<String,Object> m = new HashMap<String,Object>();
//					m.put("chkoutno", chkoutno);///
//					m.put("sp_code", invt.getSupply().getSp_code());
//					m.put("amount", invt.getCntbla()-invt.getCnttrival());
//					m.put("price", invt.getSupply().getSp_price());
//					m.put("memo", "盘亏出库");
//					m.put("stoid", 0);
//					m.put("batchid", 0);
//					m.put("deliver", deliver.getCode());
//					m.put("amount1", invt.getCntubla()-invt.getCntutrival());
//					m.put("pricesale", invt.getSupply().getPricesale());
//					m.put("chkstono", 0);//wjf
//					m.put("pr", 0);
//					chkoutdMisMapper.saveChkoutd(m);
//				}
//				result +="出库单已生成,单号"+chkoutno+" ";
//				//审核
//				map.put("month", acctService.getOnlyAccountMonth(date));
//				map.put("emp", accountName);
//				map.put("sta", "Y");
//				chkoutmMisMapper.AuditChkout(map);
//			}
//			/**修改盘点无出库物资时该仓位pd属性不更新Bug*/
//			//本仓位盘点结束 
//			Positn positn=new Positn();
//			positn.setAcct(inven.getAcct());
//			positn.setPd("Y");
//			positn.setOldcode(inven.getPositn().getCode());
//			positnMapper.updatePositn(positn);//更新pd状态
//			return  result;
//		}catch(Exception e){
//			log.error(e);
//			throw new CRUDException(e);
//		}
//		
//	}

}