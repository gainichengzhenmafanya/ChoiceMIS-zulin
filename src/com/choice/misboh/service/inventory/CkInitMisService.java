package com.choice.misboh.service.inventory;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletOutputStream;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.NumberFormat;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.domain.inventory.PositnSupply;
import com.choice.misboh.persistence.inventory.CkInitMisMapper;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Spbatch;

@Service
public class CkInitMisService {

	@Autowired
	private CkInitMisMapper ckInitMisMapper;
	
	
	/***
	 * 查询期初
	 * @param positnSupply
	 * @return
	 */
	public List<PositnSupply> getpositnSupplyList(PositnSupply positnSupply){
		return ckInitMisMapper.getpositnSupplyList(positnSupply);
	}
	
	/***
	 * 获取仓位是否已经初始
	 * @param positn
	 * @return
	 */
	public Positn getQC(Positn positn){
		return ckInitMisMapper.getQC(positn);
	}
	
	/***
	 * 保存期初
	 * @param positnSupply
	 * @throws CRUDException
	 */
	public void updateCkInit(PositnSupply positnSupply) throws CRUDException{
		try{
			//1.先删除此仓位的期初数据
//			ckInitMisMapper.deleteCkInit(positnSupply);
			ckInitMisMapper.deleteSupplyBeginning(positnSupply);
			//2.插入数据
			if(positnSupply.getPositnSupplyList() != null){
				for (int i = 0; i < positnSupply.getPositnSupplyList().size(); i++) {
					PositnSupply ps = positnSupply.getPositnSupplyList().get(i);
					ps.setAcct(positnSupply.getAcct());
//					ps.setYearr(positnSupply.getYearr());
					ps.setPositn(positnSupply.getPositn());
//					ckInitMisMapper.saveCkInit(ps);
					//保存supplybeginning表
//					ps.setId(ckInitMisMapper.findSupplyBeginNextVal());
					ps.setId((i+1)+"");
					ckInitMisMapper.saveSupplyBeginning(ps);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 仓库期初-确认初始
	 * @param positnSupplyList
	 * @param positnSupply
	 * @throws CRUDException
	 */
	public void initation(List<PositnSupply> positnSupplyList, PositnSupply positnSupply) throws CRUDException {
		try {
			ckInitMisMapper.deleteCkInit(positnSupply);//删掉期初数据
			for (int i = 0; i < positnSupplyList.size(); i++) {
				//1.保存positnsupply
				positnSupplyList.get(i).setYearr(positnSupply.getYearr());
				//判断没有转换率的规格是必输项，用来当参考数量，   如果没有这种，则参考数量=标准单位*转换率
				double incu0 = 0;
				positnSupplyList.get(i).setIncu0(0);
				if(positnSupplyList.get(i).getUnitper4() == 0){
					incu0 = positnSupplyList.get(i).getCnt4();
				}
				if(positnSupplyList.get(i).getUnitper3() == 0){
					incu0 = positnSupplyList.get(i).getCnt3();
				}
				if(positnSupplyList.get(i).getUnitper2() == 0){
					incu0 = positnSupplyList.get(i).getCnt2();
				}
				if(incu0 == 0 && positnSupplyList.get(i).getUnitper() != 0){
					positnSupplyList.get(i).setIncu0(positnSupplyList.get(i).getInc0()*positnSupplyList.get(i).getUnitper());
				}
				ckInitMisMapper.saveCkInit(positnSupplyList.get(i));
				//2.保存spbatch
				Spbatch spbatch = new Spbatch();
				spbatch.setAcct(positnSupply.getAcct());
				spbatch.setId(ckInitMisMapper.getMaxId().getId());
				spbatch.setInd(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
				spbatch.setChkno(0);
				spbatch.setSp_code(positnSupplyList.get(i).getSp_code());
				if (positnSupplyList.get(i).getInc0()==0) {
					spbatch.setPrice(0);
					spbatch.setPricesale(0);
				}else {
					spbatch.setPrice(positnSupplyList.get(i).getIna0()/positnSupplyList.get(i).getInc0());
					spbatch.setPricesale(positnSupplyList.get(i).getIna0()/positnSupplyList.get(i).getInc0());
				}
				spbatch.setAmount(positnSupplyList.get(i).getInc0());
				spbatch.setPositn(positnSupply.getPositn());
				spbatch.setStatus(DDT.BEGIN);
				spbatch.setStono(0);
				spbatch.setAmount1(positnSupplyList.get(i).getIncu0());
				spbatch.setDeliver(DDT.profitDeliver);
				ckInitMisMapper.insertSpbatch(spbatch);//插入spbatch表，确认初始
				//更新物资表中的 物资余额记录  
//				Supply supply=new Supply();
//				supply.setAcct(positnSupply.getAcct());	
//				supply.setCnt(positnSupplyList.get(i).getInc0());
//				supply.setCntu(positnSupplyList.get(i).getIncu0());
//				supply.setAmt(positnSupplyList.get(i).getIna0());
//				supply.setSp_code(positnSupplyList.get(i).getSp_code());
//				ckInitMisMapper.updateSupplyCnt(supply);
			}
			//更新仓位状态为已初始
			ckInitMisMapper.updatePositn(positnSupply);
			//更新supplybeginning表
			ckInitMisMapper.updateSupplyBeginning(positnSupply);
			//更新物资最后进价
			ckInitMisMapper.updatePositnspcodePrice(positnSupply);
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 仓库期初导出excel
	 * @param os
	 * @param list
	 * @param positnSupply
	 * @return
	 */
	public boolean exportCangkuInit(ServletOutputStream os, List<PositnSupply> list, PositnSupply positnSupply) {
		WritableWorkbook workBook = null;
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);
		WritableCellFormat doubleStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES),new NumberFormat("0.00"));//设定带小数数字单元格格式
		try {
			titleStyle.setAlignment(Alignment.CENTRE);
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet(positnSupply.getPositnName()+"仓位期初表", 0);//加上要期初的仓位名称 wjf
			sheet.addCell(new Label(0, 0,positnSupply.getPositnName()+"仓位期初表",titleStyle));
			if("Y".equals(positnSupply.getIsTemplete())){
				sheet.mergeCells(0, 0, 11, 0);
			}else{
				sheet.mergeCells(0, 0, 13, 0);
			}
            sheet.addCell(new Label(0, 1, "物资编码"));   
            sheet.addCell(new Label(1, 1, "物资名称")); 
			sheet.addCell(new Label(2, 1, "规格"));  
			sheet.addCell(new Label(3, 1, "数量|数量"));
			sheet.addCell(new Label(4, 1, "数量|规格1"));
			sheet.addCell(new Label(5, 1, "数量|数量"));
			sheet.addCell(new Label(6, 1, "数量|规格2"));
			sheet.addCell(new Label(7, 1, "数量|数量"));
			sheet.addCell(new Label(8, 1, "数量|规格3"));
			sheet.addCell(new Label(9, 1, "数量|数量"));
			sheet.addCell(new Label(10, 1, "数量|规格4"));
			if("Y".equals(positnSupply.getIsTemplete())){
				sheet.addCell(new Label(11, 1, "金额"));
			}else{
				sheet.addCell(new Label(11, 1, "合计数量"));
				sheet.addCell(new Label(12, 1, "标准单位"));
				sheet.addCell(new Label(13, 1, "金额"));
			}
            //遍历list填充表格内容
			int index = 1;
            for(int j=0; j<list.size(); j++) {
            	if(j == list.size()){
            		break;
            	}
            	index += 1;
            	sheet.addCell(new Label(0, index, list.get(j).getSp_code()));
            	sheet.addCell(new Label(1, index, list.get(j).getSp_name()));
            	sheet.addCell(new Label(2, index, list.get(j).getSp_desc()));
            	sheet.addCell(new jxl.write.Number(3,index,list.get(j).getCnt1(),doubleStyle));
            	sheet.addCell(new Label(4, index, list.get(j).getSpec1()));
            	if(null == list.get(j).getSpec2()||"".equals(list.get(j).getSpec2())){
            		sheet.addCell(new Label(5, index, ""));
            	}else{
            		sheet.addCell(new jxl.write.Number(5,index,list.get(j).getCnt2(),doubleStyle));
            	}
            	sheet.addCell(new Label(6, index, list.get(j).getSpec2()));
            	if(null == list.get(j).getSpec3()||"".equals(list.get(j).getSpec3())){
            		sheet.addCell(new Label(7, index, ""));
            	}else{
            		sheet.addCell(new jxl.write.Number(7,index,list.get(j).getCnt3(),doubleStyle));
            	}
            	sheet.addCell(new Label(8, index, list.get(j).getSpec3()));
            	if(null == list.get(j).getSpec4()||"".equals(list.get(j).getSpec4())){
            		sheet.addCell(new Label(9, index, ""));
            	}else{
            		sheet.addCell(new jxl.write.Number(9,index,list.get(j).getCnt4(),doubleStyle));
            	}
            	sheet.addCell(new Label(10, index, list.get(j).getSpec4()));
            	if("Y".equals(positnSupply.getIsTemplete())){
            		sheet.addCell(new jxl.write.Number(11,index,list.get(j).getIna0(),doubleStyle));
    			}else{
    				sheet.addCell(new jxl.write.Number(11,index,list.get(j).getInc0(),doubleStyle));
                	sheet.addCell(new Label(12, index, list.get(j).getUnit()));
                	sheet.addCell(new jxl.write.Number(13,index,list.get(j).getIna0(),doubleStyle));
    			}
	    	}
			workBook.write();
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	/***
	 * 根据分店物资属性去查导出物资
	 * @param positn
	 * @return
	 */
	public List<PositnSupply> getpositnSupplyListByPositnSpcode(Positn positn) {
		return ckInitMisMapper.getpositnSupplyListByPositnSpcode(positn);
	}
	
	/***
	 * 期初状态查询
	 * @param 
	 * @return
	 */
	public List<Positn> qcZtchaxun(Positn positn) {
		return ckInitMisMapper.qcZtchaxun(positn);
	}
	
}
