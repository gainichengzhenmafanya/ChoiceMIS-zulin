package com.choice.misboh.service.inventory;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.springframework.stereotype.Controller;

import com.choice.framework.exception.CRUDException;
import com.choice.misboh.domain.costReduction.MisSupply;
import com.choice.misboh.domain.inventory.Inventory;
import com.choice.misboh.service.common.CommonMISBOHService;


/**
 * 读取excel 和 判断excel中数据是否存在错误信息 并将读取的信息放入对象中
 * 
 * @author 王吉峰
 * 
 */
@Controller
public class ReadInventoryMisExcel {

	public static List<String> errorList;

	/**
	 * List<Supply> 将Supply添加到list中
	 */
	public static List<Inventory> psList = new ArrayList<Inventory>();
	
	private static Set<String> spcodeSet = new HashSet<String>();
	
	private static CommonMISBOHService commonMISBOHService;
	private static String currentFirm;
			
	/**
	 * 对excel中的数据判断 并把错误的信息存放到Map信息中 存在错误信息时返回false 如果不存在错误信息 返回true
	 * 
	 * @param path
	 *            上传的excel路径
	 * @return
	 */
	public static Boolean check(String path, CommonMISBOHService commonService, String curFirm) {
		commonMISBOHService = commonService;
		currentFirm = curFirm;
		Boolean checkResult = true;
		Workbook book = null;
		errorList = new ArrayList<String>();
		try {
			book = Workbook.getWorkbook(new File(path));
			// 获取工作表
			Sheet sheet = book.getSheet(0);
			int colNum = sheet.getColumns();// 列数
			int rowNum = sheet.getRows();// 行数
			if (rowNum > 0) {
				if (!checkSupply(rowNum, colNum, sheet)) {
					checkResult = false;
				}
			} else {
				checkResult = false;
			}
		} catch (Exception e) {
			checkResult = false;
			e.printStackTrace();
		} finally {
			if (book != null) {
				book.close();
				book = null;
			}

		}
		return checkResult;
	}
	
	/**
	 * 检测 存在错误信息时返回false 如果不存在 返回true
	 * @param rowNum
	 * @param colNum
	 * @param sheet
	 * @return
	 */
	private static boolean checkSupply(int rowNum, int colNum, Sheet sheet)
			throws Exception {
		List<Boolean> booleanList = new ArrayList<Boolean>();
		boolean bool = true;
		Cell cell = null;
		psList.clear();
		spcodeSet.clear();
		a:for (int i = 2; i < rowNum; i++) { // 行循环
			Inventory ps = new Inventory();
			for (int col = 0; col < colNum; col++) {// 列循环
				cell = sheet.getCell(col, i);// 读取单元格
				if(col == 0){
					if (isEmpty(cell.getContents())) {
						errorList.add("第"+(i+1)+"行第"+(col+1)+"列"+",物资编码为空");
						bool = false;
						booleanList.add(bool);
						continue;
					}else{
						String regCode = "^[\\d\\w][\\d\\w-]*";
						if(cell.getContents().matches(regCode)){
							if(null != spcodeSet){
								if(spcodeSet.contains(cell.getContents())){//如果有excel中有重复物资，则忽略
									continue a;
								}
							}
							MisSupply supply = checkSupply(cell.getContents());
							if(supply == null){
								errorList.add("第"+(i+1)+"行第"+(col+1)+"列"+"物资编码不存在");
								bool = false;
								booleanList.add(bool);
								continue;
							}else{//，如果编码验证通过，则将里面物资表中的东西都set进去
								spcodeSet.add(supply.getSp_code());
								ps.setSp_code(supply.getSp_code());
								ps.setSp_name(supply.getSp_name());
								ps.setSp_desc(supply.getSp_desc());
								ps.setSp_init(supply.getSp_init());
								ps.setUnit(supply.getUnit());
								ps.setUnit3(supply.getUnit3());
								ps.setUnit4(supply.getUnit4());
								ps.setUnitper3(supply.getUnitper3());
								ps.setUnitper4(supply.getUnitper4());
								ps.setDisunit(supply.getDisunit());
								ps.setDisunitper(supply.getDisunitper());
								ps.setEx(supply.getEx());
								bool = true;
							}
						}else{
							errorList.add("第"+(i+1)+"行第"+(col+1)+"列"+ "物资编码验证不通过");
							bool = false;
							booleanList.add(bool);
							continue;
						}
					}
				}
				double cnt = 0;
				if(col == 5 || col == 7){//采购单位数量和标准单位数量
					try{
						cnt = Double.parseDouble(cell.getContents());
						if(cnt < 0){
							errorList.add("第"+(i+1)+"行第"+(col+1)+"列"+ "数量不能为负数！");
							bool = false;
							booleanList.add(bool);
							continue;
						}
					}catch(Exception e){
						cnt = 0;
					}
					if(col == 5){
						ps.setCnt3(cnt);
					}if(col == 7){
						ps.setCnt1(cnt);
					}
				}
				int ordernum = 0;
				if(col == 10){//顺序号
					try{
						ordernum = Integer.parseInt(cell.getContents());
					}catch(Exception e){
						ordernum = 0;
					}
					ps.setOrderNum(ordernum);
				}
				String ynpd = "Y";
				if(col == 11){//顺序号
					if("Y".equals(cell.getContents()) || "是".equals(cell.getContents())){
						ynpd = "N";
					}
					ps.setYnpd(ynpd);
				}
				booleanList.add(bool);
			}
			if(bool){
				psList.add(ps);
			}
		}
		if(booleanList.size() != 0){
			for(Boolean b :booleanList){
				if(!b){
					return false;
				}
			}
		}
		return true;
	}
	
	/***
	 * 校验是不是存在此物资 wjf
	 * @param code
	 * @param supplyMapper
	 * @return
	 */
	private static MisSupply checkSupply(String code){
		MisSupply ms = new MisSupply();
		ms.setSp_code(code);
		ms.setSp_position(currentFirm);
		try {
			ms = commonMISBOHService.findSupplyBySpcode(ms);
		} catch (CRUDException e) {
			e.printStackTrace();
		}
		return ms;
	}
	
	/**
	 * 判断是否为空
	 * @param contents
	 * @return
	 */
	private static boolean isEmpty(String contents) {
		if ("".equals(contents.trim()))
			return true;
		else
			return false;
	}

}
