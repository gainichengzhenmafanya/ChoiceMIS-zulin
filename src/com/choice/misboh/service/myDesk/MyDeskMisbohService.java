package com.choice.misboh.service.myDesk;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.domain.system.Module;
import com.choice.framework.domain.system.Role;
import com.choice.framework.util.ForResourceFiles;
import com.choice.misboh.domain.BaseRecord.OtherStockDataO;
import com.choice.misboh.domain.inventory.Chkstoref;
import com.choice.misboh.domain.lossmanage.PostionLoss;
import com.choice.misboh.domain.myDesk.GuideConfig;
import com.choice.misboh.persistence.myDesk.MyDeskMisbohMapper;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.ScheduleD;
import com.choice.scm.domain.SupplyAcct;

@Service
public class MyDeskMisbohService {

	@Autowired
	private MyDeskMisbohMapper myDeskMisbohMapper;
	
	/***
	 * 根据门店和日期查询配送班表
	 * @param scheduleD
	 * @return
	 */
	public List<ScheduleD> findSchedulesByPositn(ScheduleD scheduleD) {
		return myDeskMisbohMapper.findSchedulesByPositn(scheduleD);
	}

	/***
	 * 查询直配需要验货的数量 根据供应商分组显示
	 * @param dis
	 * @return
	 */
	public List<Dis> findDireCountGroupbyDeliver(Dis dis) {
		return myDeskMisbohMapper.findDireCountGroupbyDeliver(dis);
	}
	
	/***
	 * 查询统配需要验货的总数量
	 * @param dis
	 * @return
	 */
	public List<SupplyAcct> findOutCount(Dis dis) {
		return myDeskMisbohMapper.findOutCount(dis);
	}
	
	/***
	 * 查询调拨需要验货的总数 根据门店来查
	 * @param dis
	 * @return
	 */
	public List<SupplyAcct> findDbCountGroupByPositn(Dis dis) {
		return myDeskMisbohMapper.findDbCountGroupByPositn(dis);
	}

	/***
	 * 查询我的桌面配置
	 * @return
	 */
	public List<GuideConfig> findGuideConfig() {
		return myDeskMisbohMapper.findGuideConfig();
	}

	/***
	 * 查询直配今日未验货
	 * @return
	 */
	public int findDireCount(Dis dis) {
		if("Y".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "isInsByBill"))){//验货根据单据
			return myDeskMisbohMapper.findDireCountByBill(dis);
		}
		return myDeskMisbohMapper.findDireCount(dis);
	}
	
	/***
	 * 查询统配今日未验货
	 * @return
	 */
	public int findOutCount1(Dis dis) {
		if("Y".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH, "isInsByBill"))){//验货根据单据
			return myDeskMisbohMapper.findOutCountByBill(dis);
		}
		return myDeskMisbohMapper.findOutCount1(dis);
	}

	/***
	 * 查询调拨今日未验货
	 * @param dis
	 * @return
	 */
	public int findDbCount(Dis dis) {
		return myDeskMisbohMapper.findDbCount(dis);
	}

	/***
	 * 查询今日 有没有做成本核减
	 * @param dis
	 * @return
	 */
	public int findCostitemspcodeCount(Dis dis) {
		return myDeskMisbohMapper.findCostitemspcodeCount(dis);
	}

	/***
	 * 查询今日有没有录入水电气
	 * @param oo
	 * @return
	 */
	public List<OtherStockDataO> findOtherStockdataCount(OtherStockDataO oo) {
		return myDeskMisbohMapper.findOtherStockdataCount(oo);
	}

	/***
	 * 查询今日有没有做损耗
	 * @param pl
	 * @return
	 */
	public int findPostionLossCount(PostionLoss pl) {
		return myDeskMisbohMapper.findPostionLossCount(pl);
	}

	/***
	 * 查询今日是否已盘点
	 * @param cf
	 * @return
	 */
	public int findChkstorefCount(Chkstoref cf) {
		return myDeskMisbohMapper.findChkstorefCount(cf);
	}

	/***
	 * 判断今日有没有报货
	 * @param dis
	 * @return
	 */
	public int findChkstomCount(Dis dis) {
		return myDeskMisbohMapper.findChkstomCount(dis);
	}

	/***
	 * 查询positn表营业日
	 * @param positnCode
	 * @return
	 */
	public Date findWorkdateByPositn(String positnCode) {
		return myDeskMisbohMapper.findWorkdateByPositn(positnCode);
	}

	/***
	 * 日结操作
	 * @param positnCode
	 */
	public void updateWorkDateByPositn(String positnCode,Date nextDate) {
		myDeskMisbohMapper.updateWorkDateByPositn(positnCode,nextDate);
	}

	/***
	 * 根据物流的仓位编码查询boh的仓位是否存在
	 * @param positnCode
	 * @return
	 */
	public int findStoreByVcode(String positnCode) {
		return myDeskMisbohMapper.findStoreByVcode(positnCode);
	}

	/***
	 * 查询角色的权限
	 * @param role
	 * @return
	 */
	public List<Module> findRoleOperateList(Role role) {
		return myDeskMisbohMapper.findRoleOperateList(role);
	}
	
	/***
	 * 查询本门店下未盘点仓位
	 * @param cf
	 * @return
	 */
	public List<Positn> findUnInventoryPositn(Chkstoref cf) {
		return myDeskMisbohMapper.findUnInventoryPositn(cf);
	}

	/***
	 * 月结操作
	 * @param positnCode
	 */
	public void updateMonthhByPositn(String positnCode, String monthh) {
		myDeskMisbohMapper.updateMonthhByPositn(positnCode, monthh);
	}

}

