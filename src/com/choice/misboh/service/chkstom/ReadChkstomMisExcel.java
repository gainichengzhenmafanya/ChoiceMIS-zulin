package com.choice.misboh.service.chkstom;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.springframework.stereotype.Controller;

import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.domain.costReduction.MisSupply;
import com.choice.misboh.persistence.common.CommonMISBOHMapper;
import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Chkstom;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Supply;
import com.choice.scm.persistence.PositnMapper;
import com.choice.wsyd.util.DateFormat;


/**
 * 读取excel 和 判断excel中数据是否存在错误信息 并将读取的信息放入对象中
 * 
 * @author 王吉峰
 * 
 */
@Controller
public class ReadChkstomMisExcel {
	
	// Excel表头信息
	static String[] excelColumns = { 
		"物资编码"                              
		,"物资名称"                              
		,"申购数量"                              
		,"到货日期"                              
		,"备注"                              
	};
	// 类中信息
	static String[] supplyColumns = { 
		"sp_code"           
		,"sp_name"           
	    ,"amount1.Double"
		,"hoped"
		,"memo"               
	};
	/**
	 * Map<String, String> 将错误信息存放到map中 用于前台显示使用
	 */
	public static Map<String, String> map = new HashMap<String, String>();
	
	public static List<String> errorList;

	/**
	 * List<Supply> 将Supply添加到list中
	 */
	public static List<Chkstod> chkstodList = new ArrayList<Chkstod>();
	
	public static Chkstom chkstom = new Chkstom();
	
	private static Chkstod chkstod;
	
	//传进来的对象接收下
	private static CommonMISBOHMapper commonMISBOHMapper;
	private static PositnMapper positnMapper;
	private static String currentFirm;
	
	
	/**
	 * 对excel中的数据判断 并把错误的信息存放到Map信息中 存在错误信息时返回false 如果不存在错误信息 返回true
	 * 
	 * @param path
	 *            上传的excel路径
	 * @return
	 */
	public static Boolean check(String path, CommonMISBOHMapper commonMapper, PositnMapper pMapper,String curFirm) {
		commonMISBOHMapper = commonMapper;
		positnMapper = pMapper;
		currentFirm = curFirm;
		Boolean checkResult = true;
		Workbook book = null;
		errorList = new ArrayList<String>();
		try {
			book = Workbook.getWorkbook(new File(path));
			// 获取工作表
			Sheet sheet = book.getSheet(0);
			int colNum = sheet.getColumns();// 列数
			int rowNum = sheet.getRows();// 行数
			if (rowNum > 0) {
				//1.判断日期格式对不对
				Cell cell1 = sheet.getCell(1, 0);
				if(isEmpty(cell1.getContents())){
					map.put("填制日期", "填制日期为空。");
					errorList.add("填制日期为空");
					checkResult = false;
				}else if(!checkDate(cell1.getContents())){
					map.put("填制日期", "填制日期格式不对。");
					errorList.add("填制日期格式不对");
					checkResult = false;
				}else{
					chkstom.setMaded(DateFormat.getDateByString(cell1.getContents(), "yyyy-MM-dd"));
				}
				//2.判断申购仓位有没有
				Cell cell2 = sheet.getCell(1,1);
				if(isEmpty(cell2.getContents())){
					map.put("报货仓位", "报货仓位为空。");
					errorList.add("报货仓位为空");
					checkResult = false;
				}else if(!checkPositn(cell2.getContents()) || !currentFirm.equals(cell2.getContents())){
					map.put("报货仓位", "报货仓位不正确,必须是当前分店");
					errorList.add("报货仓位不正确,必须是当前分店");
					checkResult = false;
				}else{
					chkstom.setFirm(cell2.getContents());
				}
				if (!checkSupply(rowNum, colNum, sheet)) {
					checkResult = false;
				}
			} else {
				checkResult = false;
			}
		} catch (Exception e) {
			checkResult = false;
			e.printStackTrace();
		} finally {
			if (book != null) {
				book.close();
				book = null;
			}

		}
		return checkResult;
	}
	
	
	/***
	 * 判断日期对不对 wjf
	 */
	private static boolean checkDate(String date){
		java.text.SimpleDateFormat format=new java.text.SimpleDateFormat("yyyy-MM-dd");
		try{
			format.parse(date);
		}catch(Exception e){
			return false;
		}
		return true;
	}
	
	/***
	 * 判断是否有此仓位
	 * @param positncode
	 * @return
	 */
	private static boolean checkPositn(String positncode){
		Positn positn = new Positn();
		positn.setCode(positncode);
		Positn positn1 = positnMapper.findPositnByCode(positn);
		if(positn1 == null){
			return false;
		}
		return true;
	}

	/**
	 * 检测 存在错误信息时返回false 如果不存在 返回true
	 * 
	 * @param rowNum
	 * @param colNum
	 * @param sheet
	 * @return
	 */
	private static boolean checkSupply(int rowNum, int colNum, Sheet sheet)
			throws Exception {
		List<Boolean> booleanList = new ArrayList<Boolean>();
		boolean bool = true;
		Cell cell = null;
		chkstodList.clear();
		for (int i = 3; i < rowNum; i++) { // 行循环
			chkstod = new Chkstod();
			for (int col = 0; col < colNum; col++) {// 列循环
				cell = sheet.getCell(col, i);// 读取单元格
				if (col == 0 || col == 3 || col == 9) { // 判断必填字段是否为空
					if (isEmpty(cell.getContents())) {
						map.put((i+1) + "_" + (col + 1), excelColumns[col] + "为空");
						errorList.add("第"+(i+1)+"行第"+(col+1)+"列"+excelColumns[col] + "为空");
						bool = false;
						continue;
					}
				}
				//验证
				if (!("").equals(cell.getContents())) {
					bool = validateColumns(cell.getContents(),i,col);
				}
				booleanList.add(bool);
			}
			if(bool){
				//1.标准数量需要算出来。。
				if("Y".equals(DDT.isDistributionUnit)){
					chkstod.setStomin(0);
					chkstod.setStocnt(0);
					chkstod.setDisunit(chkstod.getSupply().getDisunit());
					chkstod.setDisunitper(chkstod.getSupply().getDisunitper());
					chkstod.setDismincnt(chkstod.getSupply().getDismincnt());
					if(chkstod.getSupply() != null && chkstod.getAmountdis() != 0){
						if(0 != chkstod.getSupply().getDisunitper()){
							chkstod.setAmount(chkstod.getAmountdis()/chkstod.getSupply().getDisunitper());
						}else{
							chkstod.setAmount(0.0);
						}
					}
				}else{
					chkstod.setStomin(chkstod.getSupply().getStomin());
					chkstod.setStocnt(chkstod.getSupply().getStocnt());
					if(chkstod.getSupply() != null && chkstod.getAmount1() != 0){
						if(0 != chkstod.getSupply().getUnitper3()){
							chkstod.setAmount(chkstod.getAmount1()/chkstod.getSupply().getUnitper());
						}else{
							chkstod.setAmount(0.0);
						}
					}
				}
				
				chkstodList.add(chkstod);
			}
		}
		if(booleanList.size() != 0){
			for(Boolean b :booleanList){
				if(!b){
					return false;
				}
			}
		}
		return true;
	}
	private static boolean validateColumns(String cell, int i,int col) {
		boolean bool=true;
		switch (col) {
		case 0:
			//验证编码
			String regCode = "^[\\d\\w][\\d\\w-]*";;
			if(cell.matches(regCode)){
				Supply supply = checkSupply(cell);
				if(supply == null){
					bool = false;
					map.put((i+1) + "_" + (col + 1), excelColumns[col] + "不存在");
					errorList.add("第"+(i+1)+"行第"+(col+1)+"列"+excelColumns[col] + "不存在");
				}else{//，如果编码验证通过，则将里面物资表中的东西都set进去
					chkstod.setSupply(supply);
					bool = true;
				}
			}else{
				map.put((i+1) + "_" + (col + 1), excelColumns[col] + "验证不通过");
				errorList.add("第"+(i+1)+"行第"+(col+1)+"列"+excelColumns[col] + "验证不通过");
				bool = false;
			}
			break;
		case 3:
			//验证报货数量
			try{
				if("Y".equals(DDT.isDistributionUnit)){
					chkstod.setAmountdis(Double.parseDouble(cell));
				}else{
					chkstod.setAmount1(Double.parseDouble(cell));
				}
				bool = true;
				break;
			}catch(Exception e){
				bool = false;
				map.put((i+1) + "_" + (col + 1), excelColumns[col] + "验证不通过");
				errorList.add("第"+(i+1)+"行第"+(col+1)+"列"+excelColumns[col] + "验证不通过");
				break;
			}
		case 9:
			//验证到货日期
			java.text.SimpleDateFormat format=new java.text.SimpleDateFormat("yyyy-MM-dd");
			try{
				format.parse(cell);
				bool = true;
				chkstod.setHoped(cell);
				break;
			}catch(Exception e){
				bool = false;
				map.put((i+1) + "_" + (col + 1), excelColumns[col] + "验证不通过");
				errorList.add("第"+(i+1)+"行第"+(col+1)+"列"+excelColumns[col] + "验证不通过");
				break;
			}
		default:
			break;
		}
		return bool;
	}
	
	/***
	 * 校验是不是存在此物资 wjf
	 * @param code
	 * @return
	 */
	private static Supply checkSupply(String code){
		MisSupply ms = new MisSupply();
		ms.setSp_code(code);
		ms.setSp_position(currentFirm);
		ms = commonMISBOHMapper.findSupplyBySpcode(ms);
		Supply s = new Supply();
		s.setSp_code(ms.getSp_code());
		s.setSp_name(ms.getSp_name());
		s.setSp_desc(ms.getSp_desc());
		s.setSp_price(ms.getSp_price());
		s.setUnit(ms.getUnit());
		s.setUnit3(ms.getUnit3());
		s.setUnitper3(ms.getUnitper3());
		s.setMincnt(ms.getMincnt());
		s.setDisunit(ms.getDisunit());
		s.setDisunitper(ms.getDisunitper());
		s.setDismincnt(ms.getDismincnt());
		s.setStomin(ms.getStomin());
		s.setStocnt(ms.getStocnt());
		return s;
	}
	
	/**
	 * 判断是否为空
	 * 
	 * @param contents
	 * @return
	 */
	private static boolean isEmpty(String contents) {
		if ("".equals(contents.trim()))
			return true;
		else
			return false;
	}

}
