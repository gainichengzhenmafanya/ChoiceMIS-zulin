package com.choice.misboh.service.chkstom;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Workbook;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.NumberFormat;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.LocalUtil;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.domain.chkstom.Purchasedetails;
import com.choice.misboh.domain.inventory.Inventory;
import com.choice.misboh.persistence.chkstom.ChkstomMisMapper;
import com.choice.misboh.persistence.common.CommonMISBOHMapper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Acct;
import com.choice.scm.domain.ChkstoDemod;
import com.choice.scm.domain.ChkstoDemom;
import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Chkstom;
import com.choice.scm.domain.Positn;
import com.choice.scm.persistence.AcctMapper;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.util.FileWorked;

/***
 * 门店报货service
 * @author wjf
 *
 */
@Service
public class ChkstomMisService {

	@Autowired
	private ChkstomMisMapper chkstomMisMapper;
	@Autowired
	private PageManager<Chkstom> pageManager;
	@Autowired
	private PositnMapper positnMapper;
	@Autowired
	private CommonMISBOHMapper commonMISBOHMapper;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	@Autowired
	private AcctMapper acctMapper;
	@Autowired
	private PageManager<Purchasedetails> pdPager;

	private final transient Log log = LogFactory.getLog(ChkstomMisService.class);
	
	/***
	 * 查询当前最大单号
	 * @return
	 * @throws CRUDException
	 */
	public int getMaxChkstono() throws CRUDException{
		try {
			return chkstomMisMapper.getMaxChkstono();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 查询报货单模板主表
	 * @param chkstoDemom
	 * @return
	 * @throws CRUDException
	 */
	public List<ChkstoDemom> listChkstoDemom(ChkstoDemom chkstoDemom) throws CRUDException {
		try{
			return chkstomMisMapper.listChkstoDemom(chkstoDemom);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询报货单模板从表
	 * @param chkstoDemod
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<ChkstoDemod> listChkstoDemod(ChkstoDemod chkstoDemod) throws CRUDException{
		try{
			return chkstomMisMapper.listChkstoDemod(chkstoDemod);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 按单号查询报货单主表
	 * @param chkstom
	 * @return
	 * @throws CRUDException
	 */
	public Chkstom findByChkstoNo(Chkstom chkstom) throws CRUDException{
		try {
			return chkstomMisMapper.findChkstomByChkstoNo(chkstom);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 关键字查找报货单
	 * @param chkstomMap
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkstom> findByKey(HashMap<String,Object> chkstomMap,Page page) throws CRUDException{
		try {
			return pageManager.selectPage(chkstomMap, page, ChkstomMisMapper.class.getName()+".findByKey");			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 根据单号查询 报货单从表
	 * @param chkstod
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkstod> findByChkstoNo(Chkstod chkstod) throws CRUDException {
		try {
			return chkstomMisMapper.findChkstodByChkstoNo(chkstod);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据单号查询
	 * @param chkstod
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkstod> findByChkstoNos(Chkstod chkstod) throws CRUDException
	{
		try {
			return chkstomMisMapper.findByChkstoNos(chkstod);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 添加或修改报货单
	 * @param chkstom
	 * @param sta
	 * @return
	 * @throws CRUDException
	 */
	public String saveOrUpdateChk(Chkstom chkstom, String sta) throws CRUDException {
		String result="1";
		try {
			//定义一个变量计算新增一个报货单时的总金额
			double totalAmt=0;
			chkstom.setYearr(mainInfoMapper.findYearrList().get(0));
			chkstom.setMadet(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			Positn positn=new Positn();
			positn.setCode(chkstomMisMapper.findMainPositnByFirm(chkstom.getFirm()));
			chkstom.setPositn(positn);
			chkstom.setTotalAmt(totalAmt);
			chkstom.setPr(0);
			if(null!=sta && !"".equals(sta) && "add".equals(sta)){
				//添加报货单日志
				log.warn("添加报货单(主单)：\n" + chkstom);
				chkstomMisMapper.saveNewChkstom(chkstom);
			}else{
				HashMap<Object,Object>  map = new HashMap<Object,Object> ();
				Chkstom chkstomN = new Chkstom();
				chkstomN.setChkstoNo(chkstom.getChkstoNo());
				map.put("chkstom", chkstomN);
				chkstom.setVouno(chkstomMisMapper.findByKey(map).get(0).getVouno());
				//修改报货单日志
				log.warn("修改报货单(详单)：\n" + chkstom);
				chkstomMisMapper.updateChkstom(chkstom);
			}
			result=chkstom.getPr().toString();
			if(chkstom.getChkstodList() != null){
				//保存报货单日志
				log.warn("保存报货单(详单)：");
				for(Chkstod chkstod : chkstom.getChkstodList()){
					chkstod.setChkstoNo(chkstom.getChkstoNo());
					//保存报货单日志
					log.warn(chkstod);
					if("Y".equals(DDT.isDistributionUnit)){
						chkstomMisMapper.saveChkstodDisunit(chkstod);
					}else{
						chkstomMisMapper.saveNewChkstod_new(chkstod);
					}
					if(chkstod.getPr()>=1){
						result="1";
					}else if(chkstod.getPr() == -1){
						throw new CRUDException("当前门店所在配送片区未设置主直拨库或设置多个主直拨库，不能继续保存！请联系总部进行设置！");
					}else if(chkstod.getPr() == 0){
						throw new CRUDException(chkstod.getSupply().getSp_code()+"物资不存在或者已被禁用，不能继续保存！请删除物资或者联系总部进行设置！");
					}else if(chkstod.getPr() == -2){
						throw new CRUDException(chkstod.getSupply().getSp_code()+"物资默认仓位未设置对应的虚拟供应商，不能继续保存！请删除物资或者联系总部进行设置！");
					}else{
						throw new CRUDException("报货单保存异常！");
					}
				}
			}
			//更新主表的totalamt
			chkstomMisMapper.updateChkstomTotalamt(chkstom);
			/***这里需要判断一下 ，如果是档口报货来的   需要重新更新档口报货信息wjf***/
			//1.判断是否档口报货来的
			Integer id = chkstom.getChkstoNo();
			List<Chkstom> list = chkstomMisMapper.findChkstonoDept(id);
			if(list.size() > 0){
				//2.是档口报货来的，得到对应的档口报货单号
				StringBuffer idList = new StringBuffer();
				for(Chkstom c : list){
					idList.append(c.getChkstoNo()+",");
				}
				idList.append("0");
				Chkstod chkstod = new Chkstod();
				chkstod.setChkstoNo(chkstom.getChkstoNo());
				chkstod.setChkstoNos(idList.toString());
				chkstod.setBak2(chkstom.getChkstoNo());
				chkstomMisMapper.updateChkstomDept(chkstod);
			}
			return result;
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 需要总部审核
	 * @param chkstom
	 * @return
	 * @throws CRUDException
	 */
	public String checkChkstom(Chkstom chkstom ,String chkstoNoIds) throws CRUDException {
		List<String> ids=null;
		Map<String,String> result = new HashMap<String,String>();
		try {
			if(null!=chkstoNoIds && !"".equals(chkstoNoIds)){
				ids=Arrays.asList(chkstoNoIds.split(","));
			}else{
				ids=Arrays.asList(chkstom.getChkstoNo().toString().split(","));
			}
			for (int i = 0; i < ids.size(); i++) {
				Chkstom c = new Chkstom();
				c.setChkstoNo(Integer.parseInt(ids.get(i)));
				c.setChecby(chkstom.getChecby());
				chkstomMisMapper.checkChkstom(c);
				result.put("pr", "1");	
				chkstomMisMapper.updateChkstomTotalamt(c);//更新主表的totalamt
			}
			result.put("checby", chkstom.getChecby());
			result.put("checbyName", chkstom.getChecbyName());
			//更新引用状态
			Positn positn = new Positn();
			positn.setLocked("Y");
			positn.setOldcode(chkstom.getFirm());
			positnMapper.updatePositn(positn);
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 不需要总部审核
	 * @param chkstom
	 * @param chkstoNoIds
	 * @return
	 * @throws CRUDException
	 */
	public String checkChk(Chkstom chkstom, String chkstoNoIds) throws CRUDException {
		List<String> ids=null;
		Map<String,String> result = new HashMap<String,String>();
		try {
			if(null!=chkstoNoIds && !"".equals(chkstoNoIds)){
				ids=Arrays.asList(chkstoNoIds.split(","));
			}else{
				ids=Arrays.asList(chkstom.getChkstoNo().toString().split(","));
			}
			for (int i = 0; i < ids.size(); i++) {
				Chkstom c = new Chkstom();
				c.setChkstoNo(Integer.parseInt(ids.get(i)));
				//更新主表的totalamt
				chkstomMisMapper.updateChkstomTotalamt(c);
				/**如果是香他她需要加盟商扣款功能**/
				boolean b = false;
				Acct acct = acctMapper.findAcctById(chkstom.getAcct());
				if("香他她".equals(acct.getDes())){
					String vcode = chkstomMisMapper.findFirmtypByScode(chkstom.getFirm());
					if("2".equals(vcode)){//加盟商
						c = chkstomMisMapper.findChkstomByChkstoNo(chkstom);
						Map<String,Object> map = chkstomMisMapper.checkSppriceSale(chkstom.getFirm());
						if(null != map && null != map.get("ISDEDUCT") && null != map.get("MONEY")){
							int iddeduct = Integer.valueOf(map.get("ISDEDUCT").toString());
							if(iddeduct == 1){//需要申购扣款
								double amt = Double.valueOf(map.get("MONEY").toString());
								//查询当日已提交未出库的物资金额
								Map<String,Object> map1 = chkstomMisMapper.findTamount(c);
								double tamount = Double.valueOf(map1.get("TAMOUNT").toString());
								if(amt - tamount -c.getTotalAmt()<0){
									result.put("pr", "-2");//钱不够
									result.put("error", "加盟商预付款余额"+amt+"元，已冻结："+tamount+"元，不足以支付本次订单，提交失败！");
									throw new CRUDException(JSONObject.fromObject(result).toString());
								}else {
									result.put("error", "您的当前余额为"+amt+"元，已冻结："+tamount+"元，本次订单已提交成功，贷款将在出库时扣除。如您提交了多个订单，请确保订单金额合计少于"+(amt-tamount)+"元，否则总仓将无法发货。谢谢！");
								}
								b = true;
							}
						}
					}
				}
				c.setChecby(chkstom.getChecby());
				chkstomMisMapper.checkChkstom(c);
				chkstomMisMapper.checkChk_new(c);
				result.put("pr", c.getPr().toString());	
				if("1".equals(c.getPr().toString())){//审核成功
					if(b){//调用加盟商扣款存储过程
						result.put("pr","2");	
//						chkstomMisMapper.joiningChkstom(c);
//						if(!"1".equals(c.getPr().toString())){
//							result.put("pr", "-3");//加盟商扣款失败
//							result.put("error", "加盟商扣款失败，请联系专业技术人员！");
//							throw new CRUDException(JSONObject.fromObject(result).toString());
//						}
					}
				}else{//审核失败的话 直接返回wjf
					result.put("pr", "-1");//加盟商扣款失败
					throw new CRUDException(JSONObject.fromObject(result).toString());
				}
			}
			result.put("checby", chkstom.getChecby());
			result.put("checbyName", chkstom.getChecbyName());
			//更新引用状态
			Positn positn = new Positn();
			positn.setLocked("Y");
			positn.setOldcode(chkstom.getFirm());
			positnMapper.updatePositn(positn);
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}

	/***
	 * 删除报货单
	 * @param chkstom
	 * @param chkstoNoIds
	 * @return
	 * @throws CRUDException
	 */
	public String deleteChkstom(Chkstom chkstom,String chkstoNoIds) throws CRUDException
	{
		List<String> ids=null;
		Map<String,String> result = new HashMap<String,String>();
		try {
			//删除报货单日志
			log.warn("删除报货单:\n" + chkstom + "\n" + chkstoNoIds);
			if(null!=chkstoNoIds && !"".equals(chkstoNoIds)){
				ids=Arrays.asList(chkstoNoIds.split(","));
			}else{
				ids=Arrays.asList(chkstom.getChkstoNo().toString().split(","));
			}
			for (int i = 0; i < ids.size(); i++) {
				Chkstom c=new Chkstom();
				c.setChkstoNo(Integer.parseInt(ids.get(i)));
				chkstomMisMapper.deleteChk(c);
				chkstomMisMapper.deleteChkM_c(c);
				chkstomMisMapper.deleteChkD_c(c);
				result.put("pr", c.getPr().toString());
			}
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 导出
	 * @param os
	 * @param list
	 * @param chkstom
	 * @return
	 */
	public boolean exportChkstom(ServletOutputStream os, List<Chkstod> list, Chkstom chkstom) {
		WritableWorkbook workBook = null;
		WritableFont titleFont1 = new WritableFont(WritableFont.TIMES, 10,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.GREY_80_PERCENT);
		WritableCellFormat titleStyle1 = new WritableCellFormat(titleFont1);
		WritableFont titleFont2 = new WritableFont(WritableFont.TIMES, 10,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.RED);
		WritableCellFormat titleStyle2 = new WritableCellFormat(titleFont2);
		WritableCellFormat doubleStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES),new NumberFormat("0.00"));//设定带小数数字单元格格式
		try {
			titleStyle1.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			titleStyle2.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet(chkstom.getFirm()+"报货单", 0);
			sheet.addCell(new Label(0, 0, LocalUtil.getI18N("reported_date"),titleStyle2));
			sheet.addCell(new Label(1, 0, DateFormat.getStringByDate(chkstom.getMaded(), "yyyy-MM-dd")));
			sheet.addCell(new Label(3, 0, LocalUtil.getI18N("orders_num"),titleStyle1));
			sheet.addCell(new Label(4, 0, chkstom.getChkstoNo()+""));
			sheet.addCell(new Label(0, 1, LocalUtil.getI18N("positions"),titleStyle2));
			sheet.addCell(new Label(1, 1, chkstom.getFirm()));
			Positn p = new Positn();
			p.setCode(chkstom.getFirm());
			sheet.addCell(new Label(2, 1, positnMapper.findPositnByCode(p).getDes()));
			sheet.addCell(new Label(3, 1, LocalUtil.getI18N("document_number"),titleStyle1));
			sheet.addCell(new Label(4, 1, chkstom.getVouno()));
			
			sheet.addCell(new Label(0, 2, "物资编码",titleStyle2)); 
            sheet.addCell(new Label(1, 2, "物资名称",titleStyle1));
            sheet.addCell(new Label(2, 2, "物资规格",titleStyle1));
            if("Y".equals(DDT.isDistributionUnit)){
            	sheet.addCell(new Label(3, 2, "配送数量",titleStyle2));
                sheet.addCell(new Label(4, 2, "配送单位",titleStyle1));
            }else{
            	sheet.addCell(new Label(3, 2, "采购数量",titleStyle2));
                sheet.addCell(new Label(4, 2, "采购单位",titleStyle1));
            }
            sheet.addCell(new Label(5, 2, "标准数量",titleStyle1));
            sheet.addCell(new Label(6, 2, "标准单位",titleStyle1));
            if("Y".equals(DDT.isNotShowSp_price)){
            	sheet.addCell(new Label(7, 2, "到货日期",titleStyle2));
            	sheet.addCell(new Label(8, 2, "备注",titleStyle1));
            }else{
            	sheet.addCell(new Label(7, 2, "单价",titleStyle1));
            	sheet.addCell(new Label(8, 2, "金额",titleStyle1));
            	sheet.addCell(new Label(9, 2, "到货日期",titleStyle2));
            	sheet.addCell(new Label(10, 2, "备注",titleStyle1));
            }
            //遍历list填充表格内容
			int index = 2;
            for(int j=0; j<list.size(); j++) {
            	if(j == list.size()){
            		break;
            	}
            	index += 1;
            	sheet.addCell(new Label(0, index, list.get(j).getSupply().getSp_code()));
            	sheet.addCell(new Label(1, index, list.get(j).getSupply().getSp_name()));
            	sheet.addCell(new Label(2, index, null == list.get(j).getSupply().getSp_desc()?"":list.get(j).getSupply().getSp_desc()));
            	 if("Y".equals(DDT.isDistributionUnit)){
            		 sheet.addCell(new jxl.write.Number(3,index,list.get(j).getAmountdis(),doubleStyle));
            		 sheet.addCell(new Label(4, index, list.get(j).getDisunit()));
                 }else{
                	 sheet.addCell(new jxl.write.Number(3,index,list.get(j).getAmount1(),doubleStyle));
                	 sheet.addCell(new Label(4, index, list.get(j).getSupply().getUnit3()));
                 }
            	sheet.addCell(new jxl.write.Number(5,index,list.get(j).getAmount(),doubleStyle));
            	sheet.addCell(new Label(6, index, list.get(j).getSupply().getUnit()));
            	if("Y".equals(DDT.isNotShowSp_price)){
            		sheet.addCell(new Label(7, index, list.get(j).getHoped()));
            		sheet.addCell(new Label(8, index, null == list.get(j).getMemo()?"":list.get(j).getMemo()));
                }else{
                	sheet.addCell(new jxl.write.Number(7,index,list.get(j).getSupply().getSp_price(),doubleStyle));
                	sheet.addCell(new jxl.write.Number(8,index,list.get(j).getAmount()*list.get(j).getSupply().getSp_price(),doubleStyle));
                	sheet.addCell(new Label(9, index, list.get(j).getHoped()));
                	sheet.addCell(new Label(10, index, null == list.get(j).getMemo()?"":list.get(j).getMemo()));
                }
	    	}	            
			workBook.write();
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/**
	 * 下载模板信息 wjf
	 * 
	 * @param response
	 * @param request
	 * @throws IOException
	 */
	public void downloadTemplate(HttpServletResponse response,
			HttpServletRequest request,String fileName) throws IOException {
		OutputStream outp = null;
		FileInputStream in = null;
		try {
			String ctxPath = request.getSession().getServletContext()
					.getRealPath("/")
					+ "\\" + "template\\";
			String filedownload = ctxPath + fileName;
			fileName = URLEncoder.encode(fileName, "UTF-8");
			// 要下载的模板所在的绝对路径
			response.reset();
			response.addHeader("Content-Disposition", "attachment; filename="
					+ fileName);
			response.setContentType("application/octet-stream;charset=UTF-8");
			outp = response.getOutputStream();
			in = new FileInputStream(filedownload);
			byte[] b = new byte[1024];
			int i = 0;
			while ((i = in.read(b)) > 0) {
				outp.write(b, 0, i);
			}
			outp.flush();
		} catch (Exception e) {
			System.out.println("Error!");
			e.printStackTrace();
		} finally {
			if (in != null) {
				in.close();
				in = null;
			}
			if (outp != null) {
				outp.close();
				outp = null;
			}
		}
	}
	
	/**
	 * 将文件上传到temp文件夹下wjf
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	public String upload(HttpServletRequest request,
			HttpServletResponse response,String realFileName) throws IOException {
		String realFilePath = "";
		try {
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("file");
			String ctxPath = request.getSession().getServletContext()
					.getRealPath("/")
					+ "temp\\";
			String fileuploadPath = ctxPath;
			File dirPath = new File(fileuploadPath);
			if (!dirPath.exists()) {
				dirPath.mkdir();
			}
			realFilePath = fileuploadPath + realFileName;
			File uploadFile = new File(realFilePath);
			FileCopyUtils.copy(file.getBytes(), uploadFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return realFilePath;
	}
	
	/**
	 * 对execl进行验证
	 */
	public Object check(String path,String accountId,String currentFirm) throws CRUDException {
		boolean checkResult = ReadChkstomMisExcel.check(path, commonMISBOHMapper,positnMapper,currentFirm);
		List<String> errorList = new ArrayList<String>();
		if (checkResult) {
			Chkstom chkstom1 = ReadChkstomMisExcel.chkstom;
			chkstom1.setAcct(accountId);
			List<Chkstod> chkstodList = ReadChkstomMisExcel.chkstodList;
			chkstom1.setChkstodList(chkstodList);
			return chkstom1;
		} else {
			errorList = ReadChkstomMisExcel.errorList;
		}
		FileWorked.deleteFile(path);//删除上传后的的文件
		return errorList;
	}
	
	/**
	 * 返回导入的错误结果信息
	 */
	public List<String> listError(Map<String, String> map) {
		List<String> listError = new ArrayList<String>();
		if (map.size() != 0) {
			for (String key : map.keySet()) {
				listError.add(key);
				listError.add(map.get(key));
			}
		}
		return listError;
	}
	
	/**
	 * 批量提交报货单
	 * @author 文清泉
	 * @param 2015年8月17日 上午9:45:10
	 * @param chkstomList 报货单
	 * @param sta 状态 
	 * @param ynZbChk 是否总部审核
	 * @param autoTyp 门店自动提交类别
	 * @throws CRUDException
	 */
	public void saveOrUpdateChkBatch(List<Chkstom> chkstomList, String sta, String ynZbChk, String autoTyp) throws CRUDException {
		if(chkstomList!=null){
			for (Chkstom chkstom : chkstomList) {
				// 获取最新单号
				chkstom.setChkstoNo(this.getMaxChkstono());
				// 保存订单号
				this.saveOrUpdateChk(chkstom, sta);
				//判断是否需要自动提交
				if(autoTyp!=null&&!autoTyp.equals("0")){
					//设置 -1 全部提交
					if(autoTyp.equals("-1")){
						autoCheckChkstom(ynZbChk,chkstom);
					}else{
						//判断是否设置多个类别
						if(autoTyp.indexOf(";")!=-1){
							String split[] = autoTyp.split(";");
							for (int i = 0; i < split.length; i++) {
								if(chkstom.getManifsttyp().equals(split[i])){
									autoCheckChkstom(ynZbChk,chkstom);
								}
							}
						}else{
							if(chkstom.getManifsttyp().equals(autoTyp)){
								autoCheckChkstom(ynZbChk,chkstom);
							}
						}
					}
				}
				// 判断是否总部审核
			}
		}
	}
	/**
	 * 自动提交
	 * @author 文清泉
	 * @param 2015年8月17日 上午9:44:46
	 * @param ynZbChk 是否总部审核
	 * @param chkstom 单据主表
	 * @throws CRUDException
	 */
	public void autoCheckChkstom(String ynZbChk,Chkstom chkstom)throws CRUDException {
		if(ynZbChk!= null && ynZbChk.equals("Y")){
			this.checkChkstom(chkstom, null);
		}else{
			this.checkChk(chkstom, null);
		}
	}
	
	public String eqOrderTime(String dat,String scode){
		String orderTime = chkstomMisMapper.eqOrderTime(dat,scode);
		String hhmm = DateJudge.HH_mm.format(new Date());
		if(orderTime == null ||orderTime.equals("")){
			return "1";
		}
		if(!DateJudge.timeCompareDate(hhmm, orderTime)){
			if(orderTime.indexOf(":") == -1){//不存在
				hhmm = hhmm.replaceAll(":", "");
				if(ValueUtil.getIntValue(hhmm) > ValueUtil.getIntValue(orderTime)){
					return orderTime;
				}else{
					return "1";
				}
			}else{
				return orderTime;
			}
		}else{
			return "1";
		}
	}

	/***
	 * 供应商邮件查询
	 * @param pd
	 * @param page
	 * @return
	 */
	public List<Purchasedetails> findPdList(Purchasedetails pd, Page page) {
		return pdPager.selectPage(pd, page, ChkstomMisMapper.class.getName()+".findPdList");	
	}

	/***
	 * 查询报货单里 是否有特殊审核的物资
	 * @param c
	 * @return
	 */
	public int findStoCheckByChkstom(Chkstom c) {
		return chkstomMisMapper.findStoCheckByChkstom(c);
	}

	/***
	 * 监测是否已做盘点
	 * @param lastDate
	 * @param firm
	 * @return
	 */
	public int checkInventory(Inventory inventory) throws CRUDException{
		try {
			return chkstomMisMapper.checkInventory(inventory);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
}
