package com.choice.misboh.service.chkstom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DateFormat;
import com.choice.misboh.commonutil.DDT;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.domain.chkstom.ChkstomForecast;
import com.choice.misboh.domain.chkstom.ScheduleD;
import com.choice.misboh.domain.declareGoodsGuide.Declare;
import com.choice.misboh.domain.salesforecast.FirmItemUse;
import com.choice.misboh.domain.salesforecast.ItemPlan;
import com.choice.misboh.domain.salesforecast.SalePlan;
import com.choice.misboh.domain.salesforecast.SpCodeUseMis;
import com.choice.misboh.domain.util.Holiday;
import com.choice.misboh.persistence.chkstom.ChkstomForecastMapper;
import com.choice.misboh.persistence.common.CommonMISBOHMapper;
import com.choice.misboh.persistence.salesforecast.SpCodeUseMisMapper;

/***
 * 报货向导service
 * @author wjf
 *
 */
@Service
public class ChkstomForecastService {
    
    @Autowired
    private ChkstomForecastMapper chkstomForecastMapper;
    
    @Autowired
	private CommonMISBOHMapper commonMISBOHMapper;
    
    @Autowired
	private SpCodeUseMisMapper spCodeUseMisMapper;
    
    private final transient Log log = LogFactory.getLog(ChkstomForecastService.class);
    
    /***
	 * 获取配送班表中可订货类别
	 * @param cf
	 * @return
	 */
	public List<ScheduleD> getCodetypList(ChkstomForecast cf) throws CRUDException{
		try{
			if("Y".equals(cf.getIsuseschedule())){//启用配送班表
				return chkstomForecastMapper.getCodetypListBySchedule(cf);
			}else{
				return chkstomForecastMapper.getCodetypListNoSchedule(cf);
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 根据类别查询报货数量
	 * @param cf
	 * @return
	 */
	public String getChkstomCountByCodetyp(ChkstomForecast cf) throws CRUDException{
		try{
			//获取总共报货次数
			List<Map<String, Object>> list = chkstomForecastMapper.getChkstomCountByCodetyp(cf);
			// 类别订货次数
			Integer dcnt = Integer.parseInt(cf.getDailyGoodsCnt());
			String flag = "";
			for (Map<String, Object> map : list) {
				Integer cnt = ValueUtil.getIntValue(map.get("CNT"));
				if(cnt >= dcnt){
					flag+="【"+ValueUtil.getStringValue(map.get("CODE"))+","+ValueUtil.getStringValue(map.get("DES"))+"】";
				}
			}
			if(flag.equals("")){
				flag = "-1";
			}
			return flag;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 安全库存报货
	 * @param sd
	 * @return
	 */
	public List<Declare> getSafeStockList(ScheduleD sd) throws CRUDException{
		try{
			return chkstomForecastMapper.getSafeStockList(sd);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 周期平均用量
	 * @param sd
	 * @return
	 */
	public List<Declare> getAvgWeekList(ScheduleD sd) throws CRUDException{
		try{
			//得到上周，上上周，上上上周
			sd.setMaded1(DateFormat.getDateBefore(sd.getMaded(), "day", -1, 7));
			sd.setMaded2(DateFormat.getDateBefore(sd.getMaded(), "day", -1, 14));
			sd.setMaded3(DateFormat.getDateBefore(sd.getMaded(), "day", -1, 21));
			//得到这几天的报货量
			if(null != sd.getDept() && !"".equals(sd.getDept()))
				return chkstomForecastMapper.getAvgWeekDeptList(sd);
			return chkstomForecastMapper.getAvgWeekList(sd);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 直接填写报货单物资
	 * @param sd
	 * @return
	 */
	public List<Declare> getAddChkstomList(ScheduleD sd) throws CRUDException{
		try{
			return chkstomForecastMapper.getAddChkstomList(sd);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 调整预估值
	 * @param cf
	 * @return
	 */
	public List<SalePlan> listAdjustTheEstimated(SalePlan salePlan) throws CRUDException{
		try{
			return chkstomForecastMapper.listAdjustTheEstimated(salePlan);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 计算千元用量 
	 * @author 文清泉
	 * @param 2015年4月8日 下午6:21:57
	 * @param category_code 报货类别
	 * @param bdat 参考开始时间
	 * @param edat 参考结束时间
	 * @param SALEVALUE 预估值
	 * @param dept 部门
	 * @param vplantyp 报货方式 
	 * @param scode 店铺编码
	 * @return
	 */
	public List<Declare> calculationThousands(String category_code, String bdat,
			String edat, Object SALEVALUE, String dept, String vplantyp, String scode) {
		List<Map<String,Object>> list =  chkstomForecastMapper.getCalculationMoney(bdat,edat,dept,scode);
		List<Map<String,Object>> spList = chkstomForecastMapper.getCalculationSpcode(bdat,edat,dept,scode,category_code);
		Double _value = 0d;
		// 确认是否存在实际营业额
		if(list!=null && list.size()>0 && list.get(0)!= null){
			if(vplantyp.equals(DDT.AMT)){
				_value = ValueUtil.getDoubleValue(list.get(0).get("MONEY"));
			}else if(vplantyp.equals(DDT.TC)){
				_value =ValueUtil.getDoubleValue(list.get(0).get("TC"));
			}else if(vplantyp.equals(DDT.PAX)){
				_value = ValueUtil.getDoubleValue(list.get(0).get("PEOLENUM"));
			}
		}
		// 计算千用量
		List<Declare> declareList = new ArrayList<Declare>();
		for (Map<String, Object> spmap : spList) {
			Declare declare = new Declare();
			declare.setDailyGoods_Typ(ValueUtil.getStringValue(spmap.get("DAILYGOODS_TYP")));
			declare.setSp_code(ValueUtil.getStringValue(spmap.get("SP_CODE")));
			declare.setSp_name(ValueUtil.getStringValue(spmap.get("SP_NAME")));
			declare.setForecastMoney(ValueUtil.getDoubleValue(SALEVALUE));
			declare.setUnit(ValueUtil.getStringValue(spmap.get("UNIT")));
			declare.setSp_desc(ValueUtil.getStringValue(spmap.get("SP_DESC")));
			declare.setSp_unit(ValueUtil.getStringValue(spmap.get("SP_UNIT")));
			declare.setUnitRate_x(ValueUtil.getDoubleValue(spmap.get("UNITRATE_X")));
			declare.setUnitRate(ValueUtil.getDoubleValue(spmap.get("UNITPER3")));
			// 计算千用量    【实际用量／实际营业额　×1000】
			if(_value ==0d){
				declare.setEstimatedParameters(0d);
			}else{
				declare.setEstimatedParameters(ValueUtil.getDoubleValue(spmap.get("YL"))/_value*1000.0);
			}
			declare.setPeriodUse(declare.getEstimatedParameters() * ValueUtil.getDoubleValue(SALEVALUE)/1000.0);
			declareList.add(declare);
		}
		return declareList;
	}
	
	/***
	 * choice3 千元用量
	 * @param spCodeUse
	 * @return
	 */
	public List<Declare> listSpcodeUse(SpCodeUseMis spCodeUse) {
		return chkstomForecastMapper.listSpcodeUse(spCodeUse);
	}
	
	/***
	 * 计算千元用量choice3
	 * @param spCodeUse
	 * @param typ
	 * @return
	 */
	public List<Declare> calSpcodeUse(SpCodeUseMis spCodeUse,String typ,String vfoodsign,String isDept) {
		SpCodeUseMis spCode = new SpCodeUseMis();//获取营业额和人数总和
		if("2".equals(vfoodsign)){
			spCode = spCodeUseMisMapper.getAmt_boh(spCodeUse);
		}else{
			spCode = spCodeUseMisMapper.getAmtChoice3(spCodeUse);
		}
		if (spCode!=null && (spCode.getPax() != 0 || spCode.getAmt() != 0d)) {
			//判断是用千人还是万元
			if ("PAX".equals(typ)) {
				//千人
				spCodeUse.setLv(spCode.getPax()/1000.0);
			} else if("AMT".equals(typ)) {
				//万元
				spCodeUse.setLv(spCode.getAmt()/10000.0);
			} else if("TC".equals(typ)){
				spCodeUse.setLv(spCode.getTc()/1000.0);
			}
		}
		List<SpCodeUseMis> spList = new ArrayList<SpCodeUseMis>();
		if ("Y".equals(isDept)) {
			spList = spCodeUseMisMapper.listSupplyAcctDept(spCodeUse);
		} else {
			spList = spCodeUseMisMapper.listSupplyAcct(spCodeUse);
		}
		// 计算千用量
		List<Declare> declareList = new ArrayList<Declare>();
		for (SpCodeUseMis s : spList) {
			Declare declare = new Declare();
			declare.setDailyGoods_Typ(spCodeUse.getTyp());
		    declare.setForecastMoney(spCodeUse.getAmt());
			declare.setSp_code(s.getSp_code());
			declare.setSp_name(s.getSp_name());
			declare.setUnit(s.getUnit());
			declare.setSp_desc(s.getSp_desc());
			declare.setSpcal(s.getCntold());
			declare.setSpuse(s.getCnt());
			declare.setPositn(s.getDept());
			declare.setPositndes(s.getDeptdes());
			declare.setPeriodUse(s.getCnt()*spCodeUse.getAmt());
			declareList.add(declare);
		}
		return declareList;
	}
	
	/**
	 * 菜品销售计划与点击率物资用量计算
	 * @author 文清泉
	 * @param 2015年4月25日 下午3:42:34
	 * @param category_code 报货类别
	 * @param bdat 开始时间
	 * @param edat 结束时间
	 * @param dept 部门
	 * @param scode 店铺编码
	 * @param supplyList 
	 * @return
	 */
	public List<Declare> calculationFoodSale(String category_code,String bdat, String edat, String dept,String scode) {
		//物资序列KEY值  supplyList
//		"KCCNT","MINCNT","SP_NAME","SP_DESC","UNIT","UNIT3","UNITPER3","CATEGORY_CODE"
		List<Declare> spList = chkstomForecastMapper.calculationFoodSale(category_code,bdat,edat,dept,scode);
		return spList;
	}

	/***
	 * 查询boh部门编码
	 * @param firm
	 * @param dept
	 * @return
	 */
	public String findPositnFirm(String firm, String dept) {
		return chkstomForecastMapper.findPositnFirm(firm,dept);
	}

	/**
	 * 查询菜品销售计划
	 * @param itemPlan
	 * @return
	 * @throws CRUDException
	 */
	public List<ItemPlan> queryItemPlanByChkstom(ItemPlan itemPlan)throws CRUDException{
		try {
			List<ItemPlan> listItemPlan = chkstomForecastMapper.findPosItemPlan(itemPlan);
			if (listItemPlan.size() <= 0) {
				return null;
			}
			//用来放点击率的数据
			FirmItemUse f = new FirmItemUse();
			f.setFirm(itemPlan.getFirm());
			f.setDept1(itemPlan.getDept1());
			List<FirmItemUse> li = chkstomForecastMapper.findAllItemUse(f);
			
			List<ItemPlan> list = new ArrayList<ItemPlan>();
			List<String> listStr = DateJudge.getDateList(DateJudge.getStringByDate(itemPlan.getStartdate(), "yyyy-MM-dd"), DateJudge.getStringByDate(itemPlan.getEnddate(), "yyyy-MM-dd"));
			for (FirmItemUse firmItemUse : li) {
				ItemPlan ip = new ItemPlan();
				ip.setFirm(firmItemUse.getFirm());
				ip.setDept(firmItemUse.getDept());
				ip.setDeptdes(firmItemUse.getDeptdes());
				ip.setItem(firmItemUse.getItem());
				ip.setItcode(firmItemUse.getItcode());
				ip.setItdes(firmItemUse.getItdes());
				ip.setItunit(firmItemUse.getItunit());
				List<Map<String,Object>> listmap = new ArrayList<Map<String,Object>>();   //修改用
				for (String str : listStr) {
					Map<String,Object> listDateMap = new HashMap<String,Object>();
					for (ItemPlan iplst : listItemPlan) {
						if (!str.equals(DateJudge.getStringByDate(iplst.getDat(), "yyyy-MM-dd"))) {
							continue;
						}
						if (firmItemUse.getItcode().equals(iplst.getItcode())) {
							listDateMap.put("cal", iplst.getCaltotal());
							listDateMap.put("upd", iplst.getUpdtotal());
							listDateMap.put("dat", str);
						}
					}
					listmap.add(listDateMap);
				}
				ip.setItemplanlist(listmap);
				list.add(ip);
			}
			return list;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/***
	 * 查询菜品点击率
	 * @param f
	 * @return
	 */
	public List<FirmItemUse> findAllItemUse(FirmItemUse f) {
		return chkstomForecastMapper.findAllItemUse(f);
	}
	
	/**
	 * 重新计算菜品销售计划
	 * @param isDeclare 
	 * @param salePlan 
	 * @return
	 * @throws CRUDException
	 */
	public void saveCalPlan(ItemPlan posItemPlan, List<SalePlan> salePlanList, List<FirmItemUse> listItemUse) throws CRUDException {
		try {
			//1.删掉这段时间的菜品销售计划
//			chkstomForecastMapper.deletePosItemPlan(posItemPlan);
			List<String> listStr = DateJudge.getDateList(DateJudge.getStringByDate(posItemPlan.getStartdate(), "yyyy-MM-dd"), DateJudge.getStringByDate(posItemPlan.getEnddate(), "yyyy-MM-dd"));
			
			FirmItemUse firmItemUse = new FirmItemUse();
			List<Holiday> holidayList = commonMISBOHMapper.getHoliday(DateFormat.getStringByDate(posItemPlan.getStartdate(),"yyyy-MM-dd"),
					DateFormat.getStringByDate(posItemPlan.getEnddate(),"yyyy-MM-dd"));
			for (int i = 0; i < listItemUse.size(); i++) {//循环所有的点击率
				firmItemUse = listItemUse.get(i);
				ItemPlan itemPlan = new ItemPlan();//菜品销售计划实例
				itemPlan.setFirm(posItemPlan.getFirm());
				itemPlan.setDept(firmItemUse.getDept());//部门
				itemPlan.setDeptdes(firmItemUse.getDeptdes());
				itemPlan.setDept1(firmItemUse.getDept1());//部门
				itemPlan.setDept1des(firmItemUse.getDept1des());
				itemPlan.setItem(firmItemUse.getItem());
				itemPlan.setItcode(firmItemUse.getItcode());//菜品编码
				itemPlan.setItdes(firmItemUse.getItdes());//菜品名称
				itemPlan.setItunit(firmItemUse.getItunit());//单位
				for (int j = 0; j < listStr.size(); j++) {//循环日期列表
					String date = listStr.get(j);
					itemPlan.setDat(DateJudge.getDateByString(date));//营业日
					double adjustmentValue = 0;
					//验证是否报货向导中计算  -  读取预估值
					if(salePlanList!=null){
						for (SalePlan sale : salePlanList) {
							if(itemPlan.getDat().getTime() == sale.getDat().getTime()){
								adjustmentValue = sale.getMoney();
								break;
							}
						}
					}
					int grade = 0;
					//判断是否是节假日
					if(holidayList!=null){
						for (Holiday holiday : holidayList) {
							if(date.equals(holiday.getDholidaydate())){
								grade = holiday.getVholidaygrade();
								break;
							}
						}
					}
					if (grade >= 2) {//法定节假日
						double updtotal = ((firmItemUse.getHcnt2()+firmItemUse.getHcnt3())/2 * adjustmentValue)/1000.0;
						itemPlan.setCaltotal(Math.round(updtotal));
						itemPlan.setUpdtotal(Math.round(updtotal));
					} else {
						double updtotal = ((firmItemUse.getCnt2()+firmItemUse.getCnt3())/2 * adjustmentValue)/1000.0;
						itemPlan.setCaltotal(Math.round(updtotal));
						itemPlan.setUpdtotal(Math.round(updtotal));
					}
					//判断如果存在就更新 不存在就插入
					int updCount = chkstomForecastMapper.updateItemPlan(itemPlan);
					if(updCount == 0)
						chkstomForecastMapper.insertItemPlan(itemPlan);
				}
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

}
