package com.choice.misboh.service.chkstom;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.misboh.domain.chkstom.MISChkstoDemoFirm;
import com.choice.misboh.domain.chkstom.MISChkstoDemod;
import com.choice.misboh.domain.chkstom.MISChkstoDemom;
import com.choice.misboh.persistence.chkstom.ChkstoDemodMisMapper;
import com.choice.misboh.persistence.chkstom.ChkstoDemomMisMapper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * 报货单单据模板主表
 * @author 孙胜彬
 */
@Service
public class ChkstoDemomMisService {
	@Autowired
	private ChkstoDemomMisMapper chkstoDemomMisMapper;
	@Autowired
	private ChkstoDemodMisMapper chkstoDemodMisMapper;
	@Autowired
	private PageManager<MISChkstoDemom> pageManager;
	@Autowired
	private PageManager<MISChkstoDemoFirm> pageManager1;
	private static Logger log = Logger.getLogger(ChkstoDemomMisService.class);
	
	/**
	 * 查询报货单主表信息
	 * @param chkstoDemom
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<MISChkstoDemom> listChkstoDemom(MISChkstoDemom chkstoDemom,Page page) throws CRUDException {
		try{
			return pageManager.selectPage(chkstoDemom, page, ChkstoDemomMisMapper.class.getName()+".listChkstoDemom");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	
	/**
	 * 查询报货单从表
	 * @param chkstoDemod
	 * @param page
	 * @param string 
	 * @return
	 * @throws CRUDException
	 */
	public List<MISChkstoDemod> listChkstoDemod(MISChkstoDemod chkstoDemod,Page page, String scode) throws CRUDException{
		try{
//			return pageManager.selectPage(chkstoDemod, page, ChkstoDemodMisMapper.class.getName()+".listChkstoDemod");
			chkstoDemod.setScode(scode);
			return chkstoDemodMisMapper.listChkstoDemod(chkstoDemod);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	public List<MISChkstoDemom> listChkstoDemom(MISChkstoDemom chkstoDemom) throws CRUDException {
		try{
			return chkstoDemomMisMapper.listChkstoDemom(chkstoDemom);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 新增报货单主表
	 * @param chkstoDemom
	 * @throws CRUDException
	 */
	public MISChkstoDemom saveChkstoDemom(MISChkstoDemom chkstoDemom) throws CRUDException{
		try{
			return chkstoDemomMisMapper.saveChkstoDemom(chkstoDemom);
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 修改报货单主表
	 * @param chkstoDemom
	 * @throws CRUDException
	 */
	public void updateChkstoDemom(MISChkstoDemom chkstoDemom) throws CRUDException{
		try{
			chkstoDemomMisMapper.updateChkstoDemom(chkstoDemom);
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
	}
	
	/**
	 * 删除报货单主表
	 * @param scode 
	 * @param chkstoDemom
	 * @throws CRUDException
	 */
	public void deleteChkstoDemom(String ids, String scode) throws CRUDException{
		try{
			List<String> isList=Arrays.asList(ids.split(","));
			chkstoDemomMisMapper.deleteChkstoDemom(isList,scode);
			chkstoDemomMisMapper.deleteChkstoDemoFirm(isList,scode);
			chkstoDemodMisMapper.deleteChkstoDemod(isList,scode);
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * 新增报货单模板
	 */
	public String saveChkstoDemoms(MISChkstoDemom chkstoDemom,HttpSession session) throws CRUDException{
		String acct=session.getAttribute("ChoiceAcct").toString();
		try{
			if(chkstoDemom !=null){
				Calendar ca = Calendar.getInstance();
			    int year = ca.get(Calendar.YEAR);//获取年份
			    int minute=ca.get(Calendar.MINUTE);//分 
			    int hour=ca.get(Calendar.HOUR_OF_DAY);//小时 
			    int second=ca.get(Calendar.SECOND);//秒
			    String madet=(hour<10?("0"+hour):hour)+":"+(minute<10?("0"+minute):minute)+":"+(second<10?("0"+second):second);
				chkstoDemom.setYearr(String.valueOf(year));
				chkstoDemom.setMaded(new Date());
				chkstoDemom.setMadet(madet);
				chkstoDemom.setAcct(acct);
				chkstoDemom.setTyp("Y");//门店定义
				saveChkstoDemom(chkstoDemom);
				for(int i=0;i<chkstoDemom.getChkstoDemod().size();i++){
					MISChkstoDemod demod=chkstoDemom.getChkstoDemod().get(i);
					demod.setAcct(acct);
					demod.setYearr(String.valueOf(year));
					demod.setChkstodemono(chkstoDemom.getChkstodemono());
					chkstoDemodMisMapper.saveChkstoDemod(demod);
				}
				MISChkstoDemoFirm chkstoDemoFirm = new MISChkstoDemoFirm();
				chkstoDemoFirm.setChkstodemono(chkstoDemom.getChkstodemono());
				chkstoDemoFirm.setFirm(chkstoDemom.getFirm());
				chkstoDemomMisMapper.saveChkstoDemoFirm(chkstoDemoFirm);
				return "1";
			}else{
				return "-1";
			}
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
		return "-1";
	}
	
	/**
	 * 修改报货单模板
	 */
	public int updChkstoDemom(MISChkstoDemom chkstoDemom) throws CRUDException{
		try{
			if(chkstoDemom !=null){
				
//				for(int i=0;i<chkstoDemom.getChkstoDemod().size();i++){
//					ChkstoDemod demod=chkstoDemom.getChkstoDemod().get(i);
//					sum=sum+demod.getPricesale();
//				}
//				chkstoDemom.setTotalamt(sum);
				updateChkstoDemom(chkstoDemom);//修改主表
				List<String> isList=Arrays.asList((chkstoDemom.getChkstodemono()+"").split(","));//先删除从表
				chkstoDemodMisMapper.deleteChkstoDemod(isList,chkstoDemom.getFirm());
				
				Calendar ca = Calendar.getInstance();
			    int year = ca.get(Calendar.YEAR);//获取年份
				
				for(int i=0;i<chkstoDemom.getChkstoDemod().size();i++){
					MISChkstoDemod demod=chkstoDemom.getChkstoDemod().get(i);
					demod.setAcct(chkstoDemom.getAcct());
					demod.setYearr(String.valueOf(year));
					demod.setChkstodemono(chkstoDemom.getChkstodemono());
					chkstoDemodMisMapper.saveChkstoDemod(demod);
//					chkstoDemodMisMapper.updateChkstoDemod(demod);
					
				}
				return 1;
			}else{
				return -1;
			}
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
		return -1;
	}
	
	/**
	 * 查询报货单单据模板适用分店信息
	 * @param chkstoDemom
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<MISChkstoDemoFirm> listChkstoDemoFirm(MISChkstoDemoFirm chkstoDemoFirm,Page page) throws CRUDException {
		try{
			return pageManager1.selectPage(chkstoDemoFirm, page, ChkstoDemomMisMapper.class.getName()+".listChkstoDemoFirm");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 新增报货单单据模板适用分店
	 * @param chkstoDemom
	 * @throws CRUDException
	 */
	public void saveChkstoDemoFirm(MISChkstoDemoFirm chkstoDemoFirm,String chistodemo) throws CRUDException{
		try{
//			deleteChkstoDemoFirm(chistodemo);
			List<String> idList=Arrays.asList(chkstoDemoFirm.getFirm().split(","));
			for(int j=0;j<idList.size();j++){
				chkstoDemoFirm.setFirm(idList.get(j));
				chkstoDemoFirm.setChkstodemono(Integer.parseInt(chistodemo));
				chkstoDemomMisMapper.saveChkstoDemoFirm(chkstoDemoFirm);
			}
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
	}
	

	/**
	 * 查询报货单从表  不分页
	 * @param chkstoDemod
	 * @return
	 * @throws CRUDException
	 */
	public List<MISChkstoDemod> listChkstoDemodd(MISChkstoDemod chkstoDemod) throws CRUDException{
		try{
			return chkstoDemodMisMapper.listChkstoDemod(chkstoDemod);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

 
	
}
