package com.choice.misboh.service.chkstom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.misboh.domain.chkstom.ScheduleD;
import com.choice.misboh.persistence.chkstom.ScheduleMisMapper;

@Service
public class ScheduleMisService {

	@Autowired
	private ScheduleMisMapper scheduleMisMapper;

	/**
	 * 根据配送班表主表查询明细 
	 * @param PositnRole
	 * @throws CRUDException 
	 */
	public Object findScheduleByMonth(ScheduleD scheduleD, String scheduleMonth) throws CRUDException{
		try{
			Map<String,Object> result = new HashMap<String,Object>();
			scheduleD.setOrderDate_NY(scheduleMonth);
			List<ScheduleD> detailsList = scheduleMisMapper.findSchedulesByPositn(scheduleD);
			if (detailsList.size()>0) {
				result.put("data", getScmScheduleD(detailsList));
			}else{
				result.put("data", "");
			}
			scheduleD.setOrderDate_NY(null);
			scheduleD.setReceiveDate_NY(scheduleMonth);
			List<ScheduleD> detailsList_ = scheduleMisMapper.findSchedulesByPositn(scheduleD);
			if (detailsList_.size()>0) {
				result.put("data2", getScmScheduleD(detailsList_));
			}else{
				result.put("data2", "");
			}
			JSONObject rs = JSONObject.fromObject(result);
			return rs;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	private List<com.choice.scm.domain.ScheduleD> getScmScheduleD(List<ScheduleD> detailsList){
		List<com.choice.scm.domain.ScheduleD> list1 = new ArrayList<com.choice.scm.domain.ScheduleD>();
		for(ScheduleD sd : detailsList){
			com.choice.scm.domain.ScheduleD scmSd = new com.choice.scm.domain.ScheduleD();
			scmSd.setScheduleDetailsID(sd.getScheduleDetailsID());
			scmSd.setCategory_Code(sd.getCategory_Code());
			scmSd.setScheduleID(sd.getScheduleID());
			scmSd.setOrderDate(sd.getOrderDate());
			scmSd.setOrderTime(sd.getOrderTime());
			scmSd.setOrderColor(sd.getOrderColor());
			scmSd.setReceiveDate(sd.getReceiveDate());
			scmSd.setReceiveTime(sd.getReceiveTime());
			scmSd.setReceiveColor(sd.getReceiveColor());
			list1.add(scmSd);
		}
		return list1;
	}
}