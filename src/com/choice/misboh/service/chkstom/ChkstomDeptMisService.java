package com.choice.misboh.service.chkstom;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.misboh.persistence.chkstom.ChkstomDeptMisMapper;
import com.choice.misboh.persistence.chkstom.ChkstomMisMapper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Chkstom;
import com.choice.scm.domain.Positn;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.PositnMapper;

/***
 * 档口报货service
 * @author wjf
 *
 */
@Service
public class ChkstomDeptMisService {

	@Autowired
	private ChkstomDeptMisMapper chkstomDeptMisMapper;
	@Autowired
	private PageManager<Chkstom> pageManager;
	@Autowired
	private PositnMapper positnMapper;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	@Autowired
	private ChkstomMisMapper chkstomMisMapper;

	private final transient Log log = LogFactory.getLog(ChkstomDeptMisService.class);
	
	/***
	 * 查询当前最大单号
	 * @return
	 * @throws CRUDException
	 */
	public int getMaxChkstono() throws CRUDException{
		try {
			return chkstomDeptMisMapper.getMaxChkstono();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 按单号查询报货单主表
	 * @param chkstom
	 * @return
	 * @throws CRUDException
	 */
	public Chkstom findByChkstoNo(Chkstom chkstom) throws CRUDException{
		try {
			return chkstomDeptMisMapper.findChkstomByChkstoNo(chkstom);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 添加或修改报货单
	 * @param chkstom
	 * @param sta
	 * @return
	 * @throws CRUDException
	 */
	public String saveOrUpdateChk(Chkstom chkstom, String sta) throws CRUDException{
		String result="1";
		try {
			//定义一个变量计算新增一个报货单时的总金额
			double totalAmt=0;
			chkstom.setYearr(mainInfoMapper.findYearrList().get(0));
			chkstom.setMadet(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			Positn positn=new Positn();
			positn.setCode(chkstomMisMapper.findMainPositnByFirm(chkstom.getFirm()));
			chkstom.setPositn(positn);
			chkstom.setTotalAmt(totalAmt);
			chkstom.setPr(0);
			if(null!=sta && !"".equals(sta) && "add".equals(sta)){
				//添加报货单日志
				log.warn("添加部门报货单(主单)：\n" + chkstom);
				chkstomDeptMisMapper.saveNewChkstom(chkstom);
			}else{
				HashMap<String,Object>  map = new HashMap<String,Object> ();
				Chkstom chkstomN = new Chkstom();
				chkstomN.setChkstoNo(chkstom.getChkstoNo());
				map.put("chkstom", chkstomN);
				chkstom.setVouno(chkstomDeptMisMapper.findByKey(map).get(0).getVouno());
				//修改报货单日志
				log.warn("修改部门报货单(详单)：\n" + chkstom);
				chkstomDeptMisMapper.updateChkstom(chkstom);
			}
			result=chkstom.getPr().toString();
			if(chkstom.getChkstodList() != null){
				//保存报货单日志
				log.warn("保存部门报货单(详单)：");
				for(Chkstod chkstod : chkstom.getChkstodList()){
					chkstod.setChkstoNo(chkstom.getChkstoNo());
					chkstomDeptMisMapper.saveNewChkstod_new(chkstod);
					if(chkstod.getPr()>=1){
						result="1";
					}else if(chkstod.getPr() == -1){
						throw new CRUDException("当前门店所在配送片区未设置主直拨库或设置多个主直拨库，不能继续保存！请联系总部进行设置！");
					}else if(chkstod.getPr() == 0){
						throw new CRUDException(chkstod.getSupply().getSp_code()+"物资不存在或者已被禁用，不能继续保存！请删除物资或者联系总部进行设置！");
					}else if(chkstod.getPr() == -2){
						throw new CRUDException(chkstod.getSupply().getSp_code()+"物资默认仓位未设置对应的虚拟供应商，不能继续保存！请删除物资或者联系总部进行设置！");
					}else{
						throw new CRUDException("报货单保存异常！");
					}
				}
			}
			//更新主表的totalamt
			chkstomDeptMisMapper.updateChkstomTotalamt(chkstom);
			Positn positn1 = new Positn();
			positn1.setOldcode(chkstom.getDept());
			positn1.setLocked("Y");
			positnMapper.updatePositn(positn1);
			return result;
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 删除
	 * @param chkstom
	 * @param chkstoNoIds
	 * @return
	 * @throws CRUDException
	 */
	public String deleteChkstom(Chkstom chkstom,String chkstoNoIds) throws CRUDException {
		List<String> ids=null;
		Map<String,String> result = new HashMap<String,String>();
		try {
			//删除报货单日志
			log.warn("删除报货单:\n" + chkstom + "\n" + chkstoNoIds);
			if(null!=chkstoNoIds && !"".equals(chkstoNoIds)){
				ids=Arrays.asList(chkstoNoIds.split(","));
			}else{
				ids=Arrays.asList(chkstom.getChkstoNo().toString().split(","));
			}
			for (int i = 0; i < ids.size(); i++) {
				Chkstom c=new Chkstom();
				c.setChkstoNo(Integer.parseInt(ids.get(i)));
				chkstomDeptMisMapper.deleteChk(c);
				result.put("pr", c.getPr().toString());
			}
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 关键字查找报货单
	 * @param chkstomMap
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkstom> findByKey(HashMap<String,Object> chkstomMap,Page page) throws CRUDException{
		try {
			return pageManager.selectPage(chkstomMap, page, ChkstomDeptMisMapper.class.getName()+".findByKey");			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 关键字查找报货单 不分页
	 * @param chkstomMap
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkstom> findByKey(HashMap<String,Object> chkstomMap) throws CRUDException{
		try {
			return chkstomDeptMisMapper.findByKey(chkstomMap);		
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 根据单号查询 报货单从表
	 * @param chkstod
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkstod> findByChkstoNo(Chkstod chkstod) throws CRUDException {
		try {
			return chkstomDeptMisMapper.findChkstodByChkstoNo(chkstod);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据单号查询
	 * @param chkstod
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkstod> findByChkstoNos(Chkstod chkstod) throws CRUDException
	{
		try {
			return chkstomDeptMisMapper.findByChkstoNos(chkstod);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 添加或修改报货单(来自档口报货)
	 * @param chkstom
	 * @param idList
	 * @return
	 * @throws CRUDException
	 */
	public String saveOrUpdateChkDept(Chkstom chkstom, String idList) throws CRUDException {
		String result="1";
		try {
			//定义一个变量计算新增一个报货单时的总金额
			double totalAmt=0;
			chkstom.setYearr(mainInfoMapper.findYearrList().get(0));
			chkstom.setMadet(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			Positn positn=new Positn();
			positn.setCode(chkstomMisMapper.findMainPositnByFirm(chkstom.getFirm()));
			chkstom.setPositn(positn);
			chkstom.setTotalAmt(totalAmt);
			chkstom.setPr(0);
			//添加报货单日志
			log.warn("添加报货单(主单)：\n" + chkstom);
			chkstomMisMapper.saveNewChkstom(chkstom);
			result=chkstom.getPr().toString();
			if(chkstom.getChkstodList() != null){
				//保存报货单日志
				log.warn("保存报货单(详单)：");
				for(Chkstod chkstod : chkstom.getChkstodList()){
					chkstod.setChkstoNo(chkstom.getChkstoNo());
					//保存报货单日志
					log.warn(chkstod);
					chkstomMisMapper.saveNewChkstod_new(chkstod);
					if(chkstod.getPr()>=1){
						result="1";
					}else if(chkstod.getPr() == -1){
						throw new CRUDException("当前门店所在配送片区未设置主直拨库或设置多个主直拨库，不能继续保存！请联系总部进行设置！");
					}else if(chkstod.getPr() == 0){
						throw new CRUDException(chkstod.getSupply().getSp_code()+"物资不存在或者已被禁用，不能继续保存！请删除物资或者联系总部进行设置！");
					}else if(chkstod.getPr() == -2){
						throw new CRUDException(chkstod.getSupply().getSp_code()+"物资默认仓位未设置对应的虚拟供应商，不能继续保存！请删除物资或者联系总部进行设置！");
					}else{
						throw new CRUDException("报货单保存异常！");
					}
				}
			}
			chkstomMisMapper.updateChkstomTotalamt(chkstom);
			Chkstod chkstod = new Chkstod();
			chkstod.setChkstoNo(chkstom.getChkstoNo());
			chkstod.setChkstoNos(idList);
			chkstod.setBak2(chkstom.getChkstoNo());
			chkstomDeptMisMapper.updateChkstomDept(chkstod);
			return result;
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e.getMessage());
		}
	}
	
	public void saveOrUpdateChkBatch(List<Chkstom> chkstomList, String sta) throws CRUDException {
		if(chkstomList!=null){
			for (Chkstom chkstom : chkstomList) {
				chkstom.setChkstoNo(this.getMaxChkstono());
				this.saveOrUpdateChk(chkstom, sta);
			}
		}
	}

	/***
	 * 九毛九报货单上传
	 */
	public List<Chkstod> findByChkstoNosJMJ(Chkstod chkstod) {
		return chkstomDeptMisMapper.findByChkstoNosJMJ(chkstod);
	}

	/***
	 * 查询档口报货单状态
	 * @param idList
	 * @return
	 */
	public int findStateByNos(String idList) throws CRUDException {
		try {
			return chkstomDeptMisMapper.findStateByNos(idList);
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e.getMessage());
		}
	}

	/***
	 * 更新门店报货单状态
	 * @param chkstod
	 */
	public void updateChkstomDeptBak1(Chkstod chkstod) throws CRUDException {
		try {
			chkstomDeptMisMapper.updateChkstomDeptBak1(chkstod);
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e.getMessage());
		}
	}
}
