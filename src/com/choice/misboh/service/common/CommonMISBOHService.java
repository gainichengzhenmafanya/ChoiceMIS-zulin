package com.choice.misboh.service.common;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.misboh.commonutil.ValueUtil;
import com.choice.misboh.domain.BaseRecord.ActTyp;
import com.choice.misboh.domain.BaseRecord.Actm;
import com.choice.misboh.domain.BaseRecord.MisBohOperator;
import com.choice.misboh.domain.BaseRecord.Payment;
import com.choice.misboh.domain.BaseRecord.PubItem;
import com.choice.misboh.domain.BaseRecord.PubPackage;
import com.choice.misboh.domain.BaseRecord.PubitemTeam;
import com.choice.misboh.domain.BaseRecord.PubitemTeamDetail;
import com.choice.misboh.domain.common.CommonMethod;
import com.choice.misboh.domain.costReduction.MisSupply;
import com.choice.misboh.domain.inventory.Chkstoref;
import com.choice.misboh.domain.inventory.PositnSupply;
import com.choice.misboh.domain.store.Store;
import com.choice.misboh.domain.store.StoreDept;
import com.choice.misboh.domain.util.Holiday;
import com.choice.misboh.persistence.BaseRecord.BaseRecordMapper;
import com.choice.misboh.persistence.BaseRecord.OperatorMaintenanceMapper;
import com.choice.misboh.persistence.common.CommonMISBOHMapper;
import com.choice.misboh.persistence.inventory.InventoryMisMapper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.MainInfo;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Spprice;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.domain.SupplyUnit;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.PositnMapper;

/**
 * 公共方法
 * @author wangchao
 *
 */
@Service
public class CommonMISBOHService {

	@Autowired
	private CommonMISBOHMapper commonMapper;
	@Autowired
	private PageManager<MisSupply> pageManager;
	@Autowired
	private PageManager<Deliver> pageManager1;
	@Autowired
	private LogsMapper logsMapper;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	@Autowired
	private InventoryMisMapper inventoryMisMapper;
	@Autowired
	private BaseRecordMapper baseRecordMapper;
	@Autowired
	private OperatorMaintenanceMapper operatorMaintenanceMapper;
	@Autowired
	private PositnMapper positnMapper;
	private final transient Log log = LogFactory.getLog(CommonMISBOHService.class);
	
	/***
	 * 新增日志信息
	 * @param logs
	 * @throws CRUDException
	 */
	public void addLogs(Logs logs) throws CRUDException{
		try {
			logsMapper.addLogs(logs);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据物流仓位编码查询当前门店编码
	 * @return
	 */
	public Store getStore(Store store) throws CRUDException{
		try {
			store= commonMapper.getStore(store);
			if(store == null ){
				store = new Store();
				store.setPk_store("~");
				store.setVcode("~");
			}
			return store;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取登录店铺的编码名称
	 * @param accountId
	 * @return
	 * @throws CRUDException
	 */
	public Positn getPositn(HttpSession session)throws CRUDException{
		try {
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			return commonMapper.getPositn(thePositn);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取当前门店下所有的部门
	 * @param positn
	 * @return
	 * @throws CRUDException
	 */
	public List<StoreDept> getAllStoreDept(Store store)throws CRUDException{
		try {
			return commonMapper.getAllStoreDept(store);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据日期获取节假日(判断当前日期是否是节假日)
	 * @param date
	 * @return
	 * @throws CRUDException
	 */
	public Holiday getHolidayByDate(String date)throws CRUDException{
		try {
			Holiday holiday = new Holiday();
			holiday.setDholidaydate(date);
			holiday.setVyear(ValueUtil.getStringValue(date.substring(0, 4)));
			return commonMapper.getHolidayByDate(holiday);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：验证编码是否重复
	 * @author 马振
	 * 创建时间：2015-4-16 下午5:01:29
	 * @param commonMethod
	 * @return
	 */
	public String validateVcode(CommonMethod commonMethod) {
		try {
			return commonMapper.validateVcode(commonMethod);
		} catch (Exception e) {
			log.error(e);
			return "error";
		}
	}
	
	/**
	 * 描述：新增时默认的编码
	 * @author 马振
	 * 创建时间：2015-4-16 下午5:02:08
	 * @param map
	 * @return
	 * @throws CRUDException
	 */
	public String getDefaultVcode(Map<String,String> map) throws CRUDException{
		try {
			String code = commonMapper.getDefaultVcode(map);  //查询最大编码+1的值
			if("1".equals(code)){ //如果等于1，表示此表中还没有值，就进行设置默认值
				code = map.get("defaultvalue");
			}
			return code;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：新增时获取默认的排序
	 * @author 马振
	 * 创建时间：2015-8-25 上午9:03:45
	 * @param commonMethod
	 * @return
	 */
	public String getSortNo(CommonMethod commonMethod) {
		try {
			return commonMapper.getSortNo(commonMethod);
		} catch (Exception e) {
			log.error(e);
			return "error";
		}
	}
	
	/***
	 * 公用获取物资前十条的方法
	 * @author wjf
	 * @param supply
	 * @return
	 */
	public List<MisSupply> findSupplyTop10(MisSupply supply) {
		return commonMapper.findSupplyTop10(supply);
	}
	
	/**
	 * 根据左侧的类别树    查询全部物资编码信息
	 * @param supply
	 * @return
	 * @throws CRUDException
	 */
	public List<MisSupply> findAllSupplyByLeftGrpTyp(MisSupply  supply,String level,String code,Page page) throws CRUDException {
		try { 
			if(null!=supply && null!=supply.getSp_init()){
				supply.setSp_init(supply.getSp_init().toUpperCase());//缩写转为大写
			}
			if (null!=level) {
				if(level.equals("1")){   //大类
					supply.setGrptyp(code);
				}else if(level.equals("2")){//中类
					supply.setGrp(code);
				}else if(level.equals("3")){//小类
					supply.setSp_type(code);
				}	
			}
			return pageManager.selectPage(supply, page, CommonMISBOHMapper.class.getName()+".findAllSupply");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 查询供应商信息
	 * @param key
	 * @param sech
	 * @return
	 * @throws CRUDException
	 */
	public List<Deliver> findDeliver(Deliver deliver,Page page) throws CRUDException {
		try {
			if(null == page)
				return commonMapper.findDeliver(deliver);
			return pageManager1.selectPage(deliver, page, CommonMISBOHMapper.class.getName()+".findDeliver");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：获取当前门店
	 * @author 马振
	 * 创建时间：2015-5-12 上午9:12:15
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public Store getPkStore(HttpSession session) throws CRUDException{
		try{
			//获取当前门店的编码
			String storeCode = "";
			
			Positn thePositn = (Positn)session.getAttribute("accountPositn");
			if(null != thePositn)
				storeCode = thePositn.getCode();
			
			//将编码放入实体类
			Store store = new Store();
			store.setVcode(storeCode);
			
			//返回根据当前门店编码获取的当前门店
			return this.getStore(store);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 专门用来查询物资在某个仓位的余额情况 
	 * @param supplyAcct
	 * @return
	 */
	public List<PositnSupply> findViewPositnSupplyList(SupplyAcct supplyAcct) {
		String year = mainInfoMapper.findYearrList().get(0);//会计年2015.1.5wjf
		supplyAcct.setYearr(year);
		return commonMapper.findViewPositnSupplyList(supplyAcct);
	}

	/***
	 * 专门用来查询物资在某个仓位的余额情况 
	 * @param supplyAcct
	 * @return
	 */
	public PositnSupply findViewPositnSupply(SupplyAcct supplyAcct) {
		String year = mainInfoMapper.findYearrList().get(0);//会计年2015.1.5wjf
		supplyAcct.setYearr(year);
		return commonMapper.findViewPositnSupply(supplyAcct);
	}

	/***
	 * 查询今日是否已做盘点
	 * @param cf
	 * @return
	 */
	public Chkstoref findChkstoref(Chkstoref cf) {
		return inventoryMisMapper.findChkstoref(cf);
	}

	/**
	 * 根据时间段获取节假日数据
	 * @author 文清泉
	 * @param 2015年5月27日 下午4:49:11
	 * @param bdate
	 * @param edate
	 * @return
	 */
	public List<Holiday> getHoliday(String bdate, String edate) {
		return commonMapper.getHoliday(bdate,edate);
	}
	
	/**
	 * 描述：获取班次
	 * @author 马振
	 * 创建时间：2015-6-25 上午11:22:15
	 * @param commonMethod
	 * @return
	 */
	public List<CommonMethod> findAllShiftsft(CommonMethod commonMethod) {
		return commonMapper.findAllShiftsft(commonMethod);
	}
	
	/**
	 * 描述：获取门店班次
	 * @author wangkai
	 * 创建时间：2015-10-16
	 * @param commonMethod
	 * @return
	 */
	public List<CommonMethod> findAllShiftsftByStore(CommonMethod commonMethod, HttpSession session) throws CRUDException {
		if (ValueUtil.IsEmpty(commonMethod.getPk_store())) {
			commonMethod.setPk_store(this.getPkStore(session).getPk_store());
		}
		return commonMapper.findAllShiftsftByStore(commonMethod);
	}
	
	/**
	 * 描述：查找支付方式
	 * @author 马振
	 * 创建时间：2015-6-27 上午10:51:37
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String,Object>> findPayment() throws CRUDException {
		try{
			return commonMapper.findPayment();
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述:查询所有支付方式
	 * 作者:马振
	 * 时间:2016年7月29日下午5:10:59
	 * @param commonMethod
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String,Object>> listPaymode(CommonMethod commonMethod) throws CRUDException {
		try{
			return commonMapper.listPaymode(commonMethod);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有套餐类别信息  YGB 中餐专用
	 * @return
	 * @throws CRUDException
	 */
	public List<CommonMethod> findAllType() throws CRUDException{
		try{
		return commonMapper.findAllType();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询所有套餐信息  ---中餐专用
	 * @author 马振
	 * 创建时间：2015-6-27 下午2:52:32
	 * @param vpacktype
	 * @return
	 * @throws CRUDException
	 */
	public List<PubPackage> findAllPack(CommonMethod commonMethod)throws CRUDException {
		try {
			return commonMapper.findAllPack(commonMethod);
		} catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询集团部门
	 * @author 马振
	 * 创建时间：2015-7-13 下午5:01:03
	 * @return
	 * @throws CRUDException
	 */
	public List<CommonMethod> findAllGroupDept()throws CRUDException {
		try {
			return commonMapper.findAllGroupDept();
		} catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 查找指定物资的规格
	 * @param supplyList
	 * @return
	 */
	public List<SupplyUnit> findAllSupplyUnit(List<MisSupply> supplyList) {
		if(supplyList.size() > 0){
			return commonMapper.findAllSupplyUnit(supplyList);
		}else{
			return null;
		}
		
	}

	/***
	 * 查找门店下的物资的多规格
	 * @param ps
	 * @return
	 */
	public ArrayList<SupplyUnit> findAllSupplyUnitByPositn(PositnSupply ps) {
		if("Y".equals(ps.getYnpd())){//如果是盘点的  则查positnsupply
			return commonMapper.findAllSupplyUnitByPositnSupply(ps);
		}else{//不是盘点则查positnspcode
			return commonMapper.findAllSupplyUnitByPositnSpcode(ps);
		}
		
	}

	/***
	 * 查询分店下的物资
	 * @param supply
	 * @return
	 */
	public List<MisSupply> findAllSupply(MisSupply supply) {
		return commonMapper.findAllSupply(supply);
	}
	
	/***************************************京东接口 start wjf****************************************************/
	
	/***
	 * 查找未上传的入库单
	 * @return
	 */
	public List<SupplyAcct> findUploadIn() throws CRUDException {
		try{
			return commonMapper.findUploadIn();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/***
	 * 更新入库单为已上传状态
	 * @param id
	 */
	public void updateChkindYnupload(int id) throws CRUDException {
		try{
			commonMapper.updateChkindYnupload(id);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/***
	 * 查找未上传的出库单
	 * @return
	 */
	public List<SupplyAcct> findUploadOut() throws CRUDException {
		try{
			return commonMapper.findUploadOut();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/***
	 * 更新出库单为已审核状态
	 * @param id
	 */
	public void updateChkoutdYnupload(int id) throws CRUDException {
		try{
			commonMapper.updateChkoutdYnupload(id);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/***
	 * 查找门店盘点数据上传
	 * @return
	 */
	public List<SupplyAcct> findUploadInventory() throws CRUDException {
		try{
			return commonMapper.findUploadInventory();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/***
	 * 更新盘点上传状态
	 * @param id
	 */
	public void updateInventoryYnupload(String id) throws CRUDException {
		try{
			commonMapper.updateInventoryYnupload(id);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***************************************京东接口  end****************************************************/

	/**
	 * 描述：根据门店餐饮类型查询门店角色
	 * @author 马振
	 * 创建时间：2015-8-24 下午6:38:59
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<CommonMethod> getStoreRole(HttpSession session)throws CRUDException {
		try {
			CommonMethod commonMethod = new CommonMethod();
			commonMethod.setVfoodsign(this.getPkStore(session).getVfoodsign());
			return commonMapper.getStoreRole(commonMethod);
		} catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询当前登陆门店菜品
	 * @author 马振
	 * 创建时间：2015-8-28 下午6:06:16
	 * @param pubitem
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public List<PubItem> listPubitem(PubItem pubitem, HttpSession session) throws CRUDException{
		try{
			pubitem.setPk_store(this.getPkStore(session).getPk_store());
			return baseRecordMapper.listPubitem(pubitem);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：获取菜品类别树
	 * @author 马振
	 * 创建时间：2015-8-28 下午6:24:57
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> listPubitemTree(Map<String,Object> condition) throws CRUDException {
		try{
			return commonMapper.listPubitemTree(condition);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：获取登录用户全部可选菜品列表
	 * @author 马振
	 * 创建时间：2015-8-28 下午6:26:21
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> listPubitemInfo(Map<String,Object> condition) throws CRUDException {
		try{
			return commonMapper.listPubitemInfo(condition);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询菜品组列表
	 * @author 马振
	 * 创建时间：2015-8-28 下午6:27:37
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String,Object>> listPubitemTeamByAccount(Map<String,Object> condition) throws CRUDException {
		try{
			return commonMapper.listPubitemTeamByAccount(condition);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询菜品组明细数据
	 * @author 马振
	 * 创建时间：2015-8-29 上午10:56:50
	 * @param pubitemTeam
	 * @return
	 */
	public List<PubitemTeamDetail> listTeamDetail(PubitemTeam pubitemTeam) throws CRUDException {
		try{
			return commonMapper.listTeamDetail(pubitemTeam);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/***
	 * 根据物资编码查询物资详情  默认会查分店物资属性  如果memo!=null直接查物资表
	 * @param supply
	 * @return
	 */
	public MisSupply findSupplyBySpcode(MisSupply supply) throws CRUDException {
		try {
			return commonMapper.findSupplyBySpcode(supply);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/***
	 * 查询报价
	 * @param spprice
	 * @return
	 */
	public Spprice findBprice(Spprice spprice) throws CRUDException {
		try {
			return commonMapper.findBprice(spprice);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询活动类别
	 * @author 马振
	 * 创建时间：2016-3-1 下午4:05:16
	 * @return
	 * @throws CRUDException
	 */
	public List<ActTyp> findAllActTyp() throws CRUDException {
		try {
			return commonMapper.findAllActTyp();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询支付方式
	 * @author 马振
	 * 创建时间：2016-3-1 下午4:07:35
	 * @return
	 * @throws CRUDException
	 */
	public List<Payment> findAllPayment() throws CRUDException {
		try {
			return commonMapper.findAllPayment();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询活动
	 * @author 马振
	 * 创建时间：2016-3-1 下午4:07:53
	 * @return
	 * @throws CRUDException
	 */
	public List<Actm> findAllActm() throws CRUDException {
		try {
			return commonMapper.findAllActm();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 根据日期得到会计月 可以取之前的会计月
	 * @param maded
	 * @return
	 * @throws CRUDException
	 */
	public int getOnlyAccountMonth2(Date maded) throws CRUDException{
		int month=0;
		try{
			maded = DateFormat.formatDate(maded, "yyyy-MM-dd");
			int year = Integer.parseInt(mainInfoMapper.findYearrList().get(0));
			MainInfo mainInfo= mainInfoMapper.findMainInfo(year);
			for(int i = 1; i <= 12; i++){
				String bdat = "getBdat"+i;
				String edat = "getEdat"+i;
				Method getBdat = MainInfo.class.getDeclaredMethod(bdat);
				Date bdate = (Date)getBdat.invoke(mainInfo);
				Method getEdat = MainInfo.class.getDeclaredMethod(edat);
				Date edate = (Date)getEdat.invoke(mainInfo);
				if(maded.getTime() >= bdate.getTime() && maded.getTime() <= edate.getTime()){
					month = i;
					break;
				}
			}
			if(month==0){
				throw new CRUDException("没有此会计日！请先停止使用，联系辰森技术人员。 ");
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
		return month;
	}
	
	/***
	 * 根据日期得到会计月 不能得到之前的会计月
	 * @param maded
	 * @return
	 * @throws CRUDException
	 */
	public int getOnlyAccountMonth(Date maded,String positnCode) throws CRUDException{
		int month=0;
		try{
			maded = DateFormat.formatDate(maded, "yyyy-MM-dd");
			int year = Integer.parseInt(mainInfoMapper.findYearrList().get(0));
			MainInfo mainInfo= mainInfoMapper.findMainInfo(year);
			for(int i = 1; i <= 12; i++){
				String bdat = "getBdat"+i;
				String edat = "getEdat"+i;
				Method getBdat = MainInfo.class.getDeclaredMethod(bdat);
				Date bdate = (Date)getBdat.invoke(mainInfo);
				Method getEdat = MainInfo.class.getDeclaredMethod(edat);
				Date edate = (Date)getEdat.invoke(mainInfo);
				if(maded.getTime() >= bdate.getTime() && maded.getTime() <= edate.getTime()){
					month = i;
					break;
				}
			}
			if(month==0){
				throw new CRUDException("没有此会计日！请先停止使用，联系辰森技术人员。 ");
			}else{
				String monthh = "1";
				if(!"Y".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH,"ynCenterMonthEnd"))){//如果启用门店月结
					Positn p = new Positn();
					p.setCode(positnCode);
					Positn positn = positnMapper.findPositnByCode(p);
					if(null != positn.getMonthh() && !"".equals(positn.getMonthh())){
						monthh = positn.getMonthh();
					}
				}else{
					monthh = mainInfo.getMonthh();
				}
				if(month < Integer.parseInt(monthh)){
					throw new CRUDException("不能操作当前会计期之前的数据！ ");
				}
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
		return month;
	}

	/***
	 * 根据供应商可供物资查询税率
	 * @param ms
	 * @return
	 */
	public MisSupply findTaxByDeliverSpcode(MisSupply ms) throws CRUDException {
		try{
			return commonMapper.findTaxByDeliverSpcode(ms);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/***
	 * 查询所有会计年
	 * @param acct
	 * @return
	 */
	public Object findAllYears(String acct) throws CRUDException{
		try{
			return mainInfoMapper.findAllYears(acct);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述:查询该门店的门店操作员
	 * 作者:马振
	 * 时间:2016年7月13日下午3:38:52
	 * @param operator
	 * @return
	 * @throws CRUDException
	 */
	public List<MisBohOperator> listStoreOperator(MisBohOperator operator) throws CRUDException {
		try{
			return operatorMaintenanceMapper.listOperatorMaintenance(operator);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

	
	/***
	 * 查询表单是否已预审核
	 * @param string
	 * @param acct
	 * @param chkinno
	 * @return
	 * @throws CRUDException
	 */
	public int findCountByChknos(String table, String column, String acct, String chknos) throws CRUDException {
		try{
			Map<String,String> map = new HashMap<String,String>();
			map.put("acct", acct);
			map.put("table", table);
			map.put("column", column);
			map.put("chknos", chknos);
			return commonMapper.findCountByChknos(map);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/***
	 * 预审核
	 * @param string
	 * @param acct
	 * @param chkinno
	 * @throws CRUDException
	 */
	public void preCheck(String table, String column, String acct, String chknos) throws CRUDException {
		try{
			Map<String,String> map = new HashMap<String,String>();
			map.put("acct", acct);
			map.put("table", table);
			map.put("column", column);
			map.put("chknos", chknos);
			commonMapper.preCheck(map);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 预审核回滚
	 * @param string
	 * @param acct
	 * @param chkinno
	 * @throws CRUDException
	 */
	public void unPreCheck(String table, String column, String acct, String chknos) throws CRUDException {
		try{
			Map<String,String> map = new HashMap<String,String>();
			map.put("acct", acct);
			map.put("table", table);
			map.put("column", column);
			map.put("chknos", chknos);
			commonMapper.unPreCheck(map);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

}