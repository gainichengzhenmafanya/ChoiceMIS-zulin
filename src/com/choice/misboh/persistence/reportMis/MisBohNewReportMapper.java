package com.choice.misboh.persistence.reportMis;

import java.util.List;
import java.util.Map;

import com.choice.misboh.domain.BaseRecord.SiteType;
import com.choice.misboh.domain.reportMis.PublicEntity;
import com.choice.misboh.domain.util.Holiday;

/**
 * 描述：九毛九营业日报表
 * @author 马振
 * 创建时间：2015-7-28 下午5:08:00
 */
public interface MisBohNewReportMapper {
	
	/**
	 * 描述:查询所有已启用的附加项
	 * 作者:马振
	 * 时间:2016年7月28日下午2:33:07
	 * @param condition
	 * @return
	 */
	public List<PublicEntity> findAllRedefine();		
	
	/**
	 * 描述：九毛九营业日报表
	 * @author 马振
	 * 创建时间：2015-7-28 下午5:07:51
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> findPayment(PublicEntity condition);		//支付类型 
	public List<Map<String, Object>> findPaymode(PublicEntity condition);		//支付方式
	public Map<String, Object> findPayCal(PublicEntity condition);				//支付合计
	public List<Map<String, Object>> findMarsaleClass(PublicEntity condition);	//大类 
	public List<Map<String, Object>> findMarsaleClassTwo(PublicEntity condition);//中类
	public Map<String, Object> findMarsaleClassCal(PublicEntity condition);		//大类 合计
	public List<Map<String, Object>> findShaoamt(PublicEntity condition);		//少款 
	public Map<String, Object> findShaoamtCal(PublicEntity condition);			//少款 合计
	public List<Map<String, Object>> findDuoamt(PublicEntity condition);		//多款  
	public Map<String, Object> findDuoamtCal(PublicEntity condition);			//多款  合计
	public Map<String, Object> findNcouponcamt(PublicEntity condition);			//应收现金
	public Map<String, Object> findNcashamt(PublicEntity condition);			//银行卡
	public List<Map<String, Object>> findBillList(PublicEntity condition);		//银行交易记录
	public List<Map<String, Object>> findBillListSign(PublicEntity condition);	//银行交易记录
	public Map<String, Object> findCalBillList(PublicEntity condition);			//银行交易记录 合计
	public Map<String, Object> findStockdata(PublicEntity condition);			//水电气
	public Map<String, Object> findCancel(PublicEntity condition);				//退菜
	public Map<String, Object> findFcnt(PublicEntity condition);				//反结算
	public String findUploadFlag(PublicEntity condition);				        //是否上传
	
	/**
	 * 描述：通过日期查询节假日信息（西贝）
	 * @author 马振
	 * 创建时间：2016-2-22 下午1:14:52
	 * @param date
	 * @return
	 */
	public List<Holiday> fingHolidayByDholidaydate(String date);	
	
	/**
	 * 描述：台位类型显示（西贝）
	 * @author 马振
	 * 创建时间：2016-2-22 下午1:30:57
	 * @param siteType
	 * @return
	 */
	public List<SiteType> listSiteType(SiteType siteType);
	
	/**
	 * 描述：西贝  营收客流分段统计报表
	 * @author 马振
	 * 创建时间：2016-2-22 下午1:38:39
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> findBusinessBurst(PublicEntity condition);
	
	public List<Map<String, Object>> listItemSaleGQ(PublicEntity condition);		//菜品销售沽清表
	public List<Map<String, Object>> listItemSaleGQSum(PublicEntity condition);		//菜品销售沽清表 合计
	
	/**
	 * 描述：西贝  营业时段解析报表
	 * @author 马振
	 * 创建时间：2016-2-24 上午9:03:38
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> findSaleDataByTime(Map<String, Object> map);
	public List<Map<String, Object>> findSaleDataByTimeCal(Map<String, Object> map);
	
	/**
	 * 描述：西贝  菜品见台率统计分析
	 * @author 马振
	 * 创建时间：2016-3-3 上午9:59:17
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findCaiPinJianTaiLv(PublicEntity condition);
	public List<Map<String,Object>> findCalCaiPinJianTaiLv(PublicEntity condition);
	
	/**
	 * 描述:菜品销售明细报表——江边城外
	 * 作者:马振
	 * 时间:2016年7月11日下午4:25:33
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> queryPoscybb_new(PublicEntity condition);
	public List<Map<String,Object>> queryCalForPoscybb_new(PublicEntity condition);
	
	/**
	 * 描述:鱼口味统计报表——江边城外
	 * 作者:马振
	 * 时间:2016年7月28日下午2:41:00
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> queryFishFlavorStatistical(PublicEntity condition);
	public List<Map<String,Object>> queryFishFlavorStatisticalSum(PublicEntity condition);

	/**
	 * 描述:翻台统计表——江边城外
	 * 作者:马振
	 * 时间:2016年7月28日下午3:10:32
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> queryTableStatistics(PublicEntity condition);
	public List<Map<String,Object>> queryTableStatisticsSum(PublicEntity condition);

	/**
	 * 描述:结算方式统计表及其合计
	 * 作者:马振
	 * 时间:2016年7月29日下午1:13:57
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> queryStatementAccounts(PublicEntity condition);
	public List<Map<String,Object>> queryStatementAccountsSum(PublicEntity condition);

	/**
	 * 描述:结算方式明细表及其合计
	 * 作者:马振
	 * 时间:2016年7月29日下午1:14:38
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> queryStatementAccountsDetail(PublicEntity condition);
	public List<Map<String,Object>> queryStatementAccountsDetailSum(PublicEntity condition);
	
	/**
	 * 描述:折扣分析
	 * 作者:马振
	 * 时间:2016年7月29日下午3:57:17
	 * @param paramMap
	 * @return
	 */
	public List<Map<String, Object>> queryDiscountAnalysis(Map<String, Object> paramMap);
	public List<Map<String, Object>> queryDiscountAnalysisSum(Map<String, Object> paramMap);

	/**
	 * 描述：排班报表    权金城
	 * 作者：马振
	 * 时间：2017年5月2日上午10:43:48
	 * @param paramMap
	 * @return
	 */
	public List<Map<String, Object>> queryScheduleReport7(PublicEntity condition);
	public List<Map<String, Object>> queryCalScheduleReport7(PublicEntity condition);
}