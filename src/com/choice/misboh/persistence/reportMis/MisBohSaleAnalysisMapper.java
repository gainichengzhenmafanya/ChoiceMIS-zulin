package com.choice.misboh.persistence.reportMis;

import java.util.List;
import java.util.Map;

import com.choice.misboh.domain.reportMis.PublicEntity;


/**
 * 描述：销售分析下的报表接口
 * @author 马振
 * 创建时间：2015-4-23 下午1:51:40
 */
public interface MisBohSaleAnalysisMapper {

	/**
	 * 描述：大类销售报表
	 * @author 马振
	 * 创建时间：2015-4-23 下午2:04:09
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> listInvoiceType(PublicEntity condition);
	
	/**
	 * 描述：大类销售报表合计
	 * @author 马振
	 * 创建时间：2015-4-23 下午2:04:00
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> listSumInvoiceType(PublicEntity condition);
	
	/**
	 * 描述：中类销售报表
	 * @author 马振
	 * 创建时间：2015-4-23 下午2:03:51
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> listInvoiceGrp(PublicEntity condition);
	
	/**
	 * 描述：中类销售报表合计
	 * @author 马振
	 * 创建时间：2015-4-23 下午2:03:41
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> listSumInvoiceGrp(PublicEntity condition);
	
	/**
	 * 描述：小类销售报表
	 * @author 马振
	 * 创建时间：2015-4-23 下午2:03:30
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> listInvoiceTyp(PublicEntity condition);
	
	/**
	 * 描述：小类销售报表合计
	 * @author 马振
	 * 创建时间：2015-4-23 下午2:03:20
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> listSumInvoiceTyp(PublicEntity condition);
	
	/**
	 * 描述:菜品销售分类报告及其合计—优化后
	 * 作者:马振
	 * 时间:2016年9月29日上午10:49:06
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> listInvoiceType_new(PublicEntity condition);
	public List<Map<String, Object>> listSumInvoiceType_new(PublicEntity condition);
	
	
	/**
	 * 描述：菜品销售汇总
	 * @author 马振
	 * 创建时间：2015-4-23 下午2:20:46
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> queryCpxshz(PublicEntity condition);
	
	/**
	 * 描述：菜品销售汇总合计
	 * @author 马振
	 * 创建时间：2015-4-23 下午2:21:07
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> queryCalForCpxshz(PublicEntity condition);
	
	/**
	 * 描述： 菜品销售明细报表及其合计
	 * @author 马振
	 * 创建时间：2016年4月12日下午8:36:20
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> queryPoscybb(PublicEntity condition);
	public List<Map<String, Object>> queryCalPoscybb(PublicEntity condition);
	
	/**
	 * 描述：退菜明细
	 * @author 马振
	 * 创建时间：2015-4-23 下午3:27:50
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> queryTcmxbb(PublicEntity condition);
	
	/**
	 * 描述：退菜明细汇总
	 * @author 马振
	 * 创建时间：2015-4-23 下午3:28:00
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> CalQueryTcmxbb(PublicEntity condition);
	
	/**
	 * 描述：套餐销售报表
	 * @author 马振
	 * 创建时间：2015-4-28 下午3:18:32
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> listBohSales(PublicEntity condition);
	
	/**
	 * 描述：套餐销售报表合计
	 * @author 马振
	 * 创建时间：2015-4-28 下午3:18:22
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> listSumBohSales(PublicEntity condition);

	/**
	 * 描述：时段营业额报告及合计
	 * @author 马振
	 * 创建时间：2015-7-31 上午11:25:29
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> queryPeriodTurnoverReport(Map<String, Object> map);
	public List<Map<String, Object>> queryPeriodTurnoverReportSUM(Map<String, Object> map);

	/**
	 * 描述：附加项销售统计及其合计
	 * @author 马振
	 * 创建时间：2015-8-15 下午2:45:07
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> queryAdditionalTerm(PublicEntity condition);
	public List<Map<String, Object>> queryAdditionalTermSum(PublicEntity condition);

	/**
	 * 描述：点菜员提成汇总报表
	 * @author 马振
	 * 创建时间：2015-8-18 下午2:42:20
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> queryOrderingMemberCommission(PublicEntity condition);
	public List<Map<String, Object>> queryCalOrderingMemberCommission(PublicEntity condition);
	
	/**
	 * 描述：点菜员提成明细及其合计
	 * @author 马振
	 * 创建时间：2015-8-28 下午3:50:54
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> queryOrderingMemberCommissionDetail(PublicEntity condition);
	public List<Map<String, Object>> queryCalOrderingMemberCommissionDetail(PublicEntity condition);
}