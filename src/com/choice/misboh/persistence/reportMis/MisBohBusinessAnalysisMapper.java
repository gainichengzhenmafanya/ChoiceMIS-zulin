package com.choice.misboh.persistence.reportMis;

import java.util.List;
import java.util.Map;

import com.choice.misboh.domain.reportMis.PublicEntity;

/**
 * 描述：营业分析下的报表接口
 * @author 马振
 * 创建时间：2015-4-16 上午11:15:16
 */
public interface MisBohBusinessAnalysisMapper {

	/**
	 * 描述：现金日报表及其合计
	 * @author 马振
	 * 创建时间：2015-4-16 上午11:16:08
	 * @param paramMap
	 * @return
	 */
	public List<Map<String, Object>> queryXjrReport(Map<String, Object> paramMap);
	public List<Map<String, Object>> queryXjrReport_new(Map<String, Object> paramMap);
	
	/**
	 * 描述：查询存款
	 * @author 马振
	 * 创建时间：2015-4-22 下午1:28:51
	 * @param PublicEntity
	 * @return
	 */
	public List<PublicEntity> findCashSaving(PublicEntity PublicEntity);
	
	/**
	 * 描述：查询存款合计
	 * @author 马振
	 * 创建时间：2015-4-22 下午1:45:02
	 * @param paramMap
	 * @return
	 */
	public PublicEntity findCountCashSaving(PublicEntity PublicEntity);
	
	/**
	 * 描述：查询账单明细表
	 * @author 马振
	 * 创建时间：2015-4-22 上午11:02:07
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> queryFolioDetailInfo(Map<String,Object> map);
	
	/**
	 * 描述：查询账单明细表合计
	 * @author 马振
	 * 创建时间：2015-4-22 上午11:01:56
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> queryCalForFolioDetailInfo(Map<String,Object> map);
	
	/**
	 * 描述：当日查询——账单查询及其合计
	 * @author 马振
	 * 创建时间：2016-3-1 下午2:23:50
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> queryFolioDetailInfoCur(Map<String,Object> map);
	public List<Map<String, Object>> queryCalFolioDetailInfoCur(Map<String,Object> map);
	
	/**
	 * 描述：查询账单明细分析
	 * @author 马振
	 * 创建时间：2015-4-22 下午3:58:56
	 * @param PublicEntity
	 * @return
	 */
	public List<Map<String,Object>> queryZdMxfx(PublicEntity PublicEntity);
	
	/**
	 * 描述：查询账单明细分析总计
	 * @author 马振
	 * 创建时间：2015-4-22 下午3:59:06
	 * @param PublicEntity
	 * @return
	 */
	public List<Map<String,Object>> queryCalForZdMxfx(PublicEntity PublicEntity);
	
	/**
	 * 描述：查询账单支付明细及总计
	 * @author 马振
	 * 创建时间：2015-8-19 下午3:17:10
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> queryZdMxfx_pay(PublicEntity condition);
	public List<Map<String,Object>> queryCalForZdMxfx_pay(PublicEntity condition);
	
	/**
	 * 描述：营运报告
	 * @author 马振
	 * 创建时间：2015-4-23 上午10:20:47
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> queryyybg(Map<String,Object> map);
	
	/**
	 * 描述：当日查询——营运报告
	 * @author 马振
	 * 创建时间：2016-3-3 下午4:18:04
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> queryYybgCur(Map<String,Object> map);
	
	/**
	 * 描述：营业日明细--餐次查询
	 * @author 马振
	 * 创建时间：2015-4-23 上午11:06:21
	 * @param PublicEntity
	 * @return
	 */
	public Map<String, Object> queryYingYeRibaoCanci2(PublicEntity PublicEntity);
	
	/**
	 * 描述：营业日明细
	 * @author 马振
	 * 创建时间：2015-4-23 上午11:07:18
	 * @param PublicEntity
	 * @return
	 */
	public Map<String, Object> queryYingYeRibao(PublicEntity PublicEntity);				//金额查询
	public List<Map<String, Object>> queryYingYeRibaoTd(PublicEntity PublicEntity);		//退单查询
	public List<Map<String, Object>> queryYingYeRibaoWd(PublicEntity PublicEntity);		//外带统计
	public Map<String, Object> queryYingYeRibaoWdSum(PublicEntity PublicEntity);		//外带合计
	public List<Map<String, Object>> queryYingYeRibaoLb(PublicEntity PublicEntity);		//类别统计
	public Map<String, Object> queryYingYeRibaoLbSum(PublicEntity PublicEntity);		//类别合计统计
	public List<Map<String, Object>> queryYingYeRibaoHd(PublicEntity PublicEntity);		//活动统计 
	public Map<String, Object> queryYingYeRibaoHdSum(PublicEntity PublicEntity);		//活动统计 合计
	public List<Map<String, Object>> queryYingYeRibaoZflx(PublicEntity condition);		//应收
	public List<Map<String, Object>> queryYingYeRibaoZflxSum(PublicEntity condition);	//应收合计
	public List<Map<String, Object>> queryYingYeRibaoJsfs(PublicEntity condition);		//实收
	public Map<String, Object> queryYingYeRibaoJsfsSum(PublicEntity condition);			//实收合计
	

	/**
	 * 描述：当日查询——营业日明细
	 * @author 马振
	 * 创建时间：2016-3-4 上午11:41:01
	 * @param PublicEntity
	 * @return
	 */
	public Map<String, Object> queryYingYeRibaoCur(PublicEntity PublicEntity);			//金额查询
	public List<Map<String, Object>> queryYingYeRibaoTdCur(PublicEntity PublicEntity);	//退单查询
	public List<Map<String, Object>> queryYingYeRibaoWdCur(PublicEntity PublicEntity);	//外带统计
	public Map<String, Object> queryYingYeRibaoWdCurSum(PublicEntity PublicEntity);		//外带合计
	public List<Map<String, Object>> queryYingYeRibaoLbCur(PublicEntity PublicEntity);	//类别统计
	public Map<String, Object> queryYingYeRibaoLbCurSum(PublicEntity PublicEntity);		//类别合计统计
	public List<Map<String, Object>> queryYingYeRibaoHdCur(PublicEntity PublicEntity);	//活动统计 
	public Map<String, Object> queryYingYeRibaoHdCurSum(PublicEntity PublicEntity);		//活动统计 合计
	public List<Map<String, Object>> queryYingYeRibaoZffsYs(PublicEntity condition);	//应收
	public List<Map<String, Object>> queryYingYeRibaoZffsYsSum(PublicEntity condition);	//应收合计
	public List<Map<String, Object>> queryYingYeRibaoZffsSs(PublicEntity condition);	//实收
	public Map<String, Object> queryYingYeRibaoZffsSsSum(PublicEntity condition);		//实收合计
	
	/**
	 * 描述：收银员统计
	 * @author 马振
	 * 创建时间：2015-4-24 下午1:09:16
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> queryCashierStatistics(PublicEntity condition);
	
	/**
	 * 描述：收银员统计
	 * @author 马振
	 * 创建时间：2015-4-24 下午1:09:16
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> queryCalCashierStatistics(PublicEntity condition);
	
	/**
	 * 描述：台位活动明细报表及其合计
	 * @author 马振
	 * 创建时间：2015-7-24 下午3:33:51
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> findActmByTbl(PublicEntity condition);
	public List<Map<String, Object>> findActmByTblSUM(PublicEntity condition);
	
	/**
	 * 描述： 支付方式明细及其合计
	 * @author 马振
	 * 创建时间：2016年4月19日下午7:46:24
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> queryPayDetail(PublicEntity condition);
	public List<Map<String,Object>> queryCalPayDetail(PublicEntity condition);
}