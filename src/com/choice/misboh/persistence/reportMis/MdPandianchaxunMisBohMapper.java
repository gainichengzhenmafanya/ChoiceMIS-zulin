package com.choice.misboh.persistence.reportMis;

import java.util.List;
import java.util.Map;

import com.choice.misboh.domain.inventory.Chkstoreo;
import com.choice.scm.domain.SupplyAcct;

public interface MdPandianchaxunMisBohMapper {

	/***
	 * 查询门店历史盘点数据
	 * @param content
	 * @return
	 */
	public List<Map<String, Object>> findMdPandianDetailQuery(Chkstoreo chkstore);

	/***
	 * 查询门店历史盘点汇总
	 * @param content
	 * @return
	 */
	public List<Map<String, Object>> findMdPandianCount(Chkstoreo chkstore);

	/***
	 * 查询门店历史盘点数据
	 * @param content
	 * @return
	 */
	public List<Map<String, Object>> findMdPandianDetailCount(Chkstoreo chkstore);
	
	/***
	 * 查询门店历史盘点数据
	 * @param content
	 * @return
	 */
	public List<Map<String, Object>> findMdPandian(Chkstoreo chkstore);
	
	/***
	 * 查询门店历史盘点汇总
	 * @param content
	 * @return
	 */
	public List<Map<String, Object>> findMdPandianMingxiCount(Chkstoreo chkstore);
	
	/***
	 * 查询门店历史盘点数据
	 * @param content
	 * @return
	 */
	public List<Map<String, Object>> findMdPandianMingxiDetailQuery(Chkstoreo chkstore);

	/**
	 * 门店盘点核减状态查询
	 * @param conditions
	 * @return
	 */
	public List<Map<String, Object>> findMdPdhjState(SupplyAcct conditions);

}
