package com.choice.misboh.persistence.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.framework.exception.CRUDException;
import com.choice.misboh.domain.BaseRecord.ActTyp;
import com.choice.misboh.domain.BaseRecord.Actm;
import com.choice.misboh.domain.BaseRecord.Payment;
import com.choice.misboh.domain.BaseRecord.PubPackage;
import com.choice.misboh.domain.BaseRecord.PubitemTeam;
import com.choice.misboh.domain.BaseRecord.PubitemTeamDetail;
import com.choice.misboh.domain.common.CommonMethod;
import com.choice.misboh.domain.costReduction.MisSupply;
import com.choice.misboh.domain.inventory.PositnSupply;
import com.choice.misboh.domain.store.Store;
import com.choice.misboh.domain.store.StoreDept;
import com.choice.misboh.domain.util.Holiday;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Spprice;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.domain.SupplyUnit;

public interface CommonMISBOHMapper {

	/**
	 * 获取当前门店的预估方式
	 * @return
	 */
	public Store getStore(Store store);
	
	/**
	 * 获取当前门店下所有的部门
	 * @param positn
	 * @return
	 * @throws CRUDException
	 */
	public List<StoreDept> getAllStoreDept(Store store);
	
	/**
	 * 获取登录店铺的编码名称
	 * @param typ 
	 * @param accountId
	 * @return
	 * @throws CRUDException
	 */
	public Positn getPositn(Positn positn);
	
	/**
	 * 根据日期获取节假日(判断当前日期是否是节假日)
	 * @param date
	 * @return
	 * @throws CRUDException
	 */
	public Holiday getHolidayByDate(Holiday holiday);
	
	/**
	 * 描述：编码重复验证
	 * @author 马振
	 * 创建时间：2015-4-16 下午5:05:13
	 * @param commonMethod
	 * @return
	 */
	public String validateVcode(CommonMethod commonMethod);
	
	/**
	 * 描述：新增时默认编码
	 * @author 马振
	 * 创建时间：2015-4-16 下午5:05:45
	 * @param map
	 * @return
	 */
	public String getDefaultVcode(Map<String,String> map);

	/**
	 * 描述：新增时获取默认的排序
	 * @author 马振
	 * 创建时间：2015-8-25 上午9:04:36
	 * @param commonMethod
	 * @return
	 */
	public String getSortNo(CommonMethod commonMethod);
	
	/***
	 * 获取物资前十条的方法
	 * @author wjf
	 * @param supply
	 * @return
	 */
	public List<MisSupply> findSupplyTop10(MisSupply supply);

	/***
	 * 专门用来查询物资在某个仓位的余额情况 
	 * @param supplyAcct
	 * @return
	 */
	public PositnSupply findViewPositnSupply(SupplyAcct supplyAcct);

	/**
	 * 根据时间段获取节假日
	 * @author 文清泉
	 * @param 2015年5月27日 下午4:49:38
	 * @param bdate
	 * @param edate
	 * @return
	 */
	public List<Holiday> getHoliday(@Param(value="bdate")String bdate, @Param(value="edate")String edate);
	
	/**
	 * 描述：获取班次
	 * @author 马振
	 * 创建时间：2015-6-25 上午11:19:53
	 * @param commonMethod
	 * @return
	 */
	public List<CommonMethod> findAllShiftsft(CommonMethod commonMethod);
	public List<CommonMethod> findAllShiftsftByStore(CommonMethod commonMethod);
	
	/**
	 * 描述：查找中餐支付方式
	 * @author 马振
	 * 创建时间：2015-6-27 上午10:52:16
	 * @return
	 */
	public List<Map<String,Object>> findPayment();
	
	/**
	 * 描述:查询所有支付方式
	 * 作者:马振
	 * 时间:2016年7月29日下午5:08:36
	 * @return
	 */
	public List<Map<String,Object>> listPaymode(CommonMethod commonMethod);
	
	/**
	 * 描述：查询所有套餐类别信息  中餐专用
	 * @author 马振
	 * 创建时间：2015-6-27 下午4:00:26
	 * @return
	 */
	public List<CommonMethod> findAllType();
	
	/**
	 * 描述：查询所有套餐信息  ---中餐专用
	 * @author 马振
	 * 创建时间：2015-6-27 下午2:53:01
	 * @param vpacktype
	 * @return
	 */
	public List<PubPackage> findAllPack(CommonMethod commonMethod);
	
	/**
	 * 描述：查询集团部门
	 * @author 马振
	 * 创建时间：2015-7-13 下午4:56:24
	 * @return
	 */
	public List<CommonMethod> findAllGroupDept();
	
	/***
	 * 查找未上传的入库单京东用
	 * @return
	 */
	public List<SupplyAcct> findUploadIn();

	/***
	 * 更新入库单上传状态
	 * @param id
	 * @return
	 */
	public void updateChkindYnupload(int id);

	/***
	 * 查找未上传的出库单 京东用
	 * @return
	 */
	public List<SupplyAcct> findUploadOut();

	/***
	 * 更新出库单为已审核状态
	 */
	public void updateChkoutdYnupload(int id);

	/***
	 * 查找门店盘点未上传数据
	 * @return
	 */
	public List<SupplyAcct> findUploadInventory();

	/***
	 * 更新盘点未已上传状态
	 * @param id
	 */
	public void updateInventoryYnupload(String id);

	/***
	 * 查询物资的规格
	 * @param supply
	 * @return
	 */
	public List<SupplyUnit> findAllSupplyUnit(@Param(value="supplyList")List<MisSupply> supplyList);

	/***
	 * 查询指定门店的物资多规格
	 * @param ps
	 * @return
	 */
	public ArrayList<SupplyUnit> findAllSupplyUnitByPositnSupply(PositnSupply ps);

	/***
	 * 查询指定分店物资属性的多规格
	 * @param ps
	 * @return
	 */
	public ArrayList<SupplyUnit> findAllSupplyUnitByPositnSpcode(PositnSupply ps);

	/***
	 * 查询分店下的所有物资
	 * @param supply
	 * @return
	 */
	public List<MisSupply> findAllSupply(MisSupply supply);
	
	/**
	 * 描述：根据餐饮类型查询门店角色
	 * @author 马振
	 * 创建时间：2015-8-24 下午6:35:41
	 * @param commonMethod
	 * @return
	 */
	public List<CommonMethod> getStoreRole(CommonMethod commonMethod);
	
	/**
	 * 描述：获取菜品类别树
	 * @author 马振
	 * 创建时间：2015-8-28 下午6:24:01
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> listPubitemTree(Map<String, Object> condition);
	
	/**
	 * 描述：获取登录用户全部可选菜品列表
	 * @author 马振
	 * 创建时间：2015-8-28 下午6:27:00
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> listPubitemInfo(Map<String, Object> condition);
	
	/**
	 * 描述：查询菜品组列表
	 * @author 马振
	 * 创建时间：2015-8-28 下午6:32:42
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> listPubitemTeamByAccount(Map<String, Object> condition);

	/**
	 * 描述：查询菜品组明细数据
	 * @author 马振
	 * 创建时间：2015-8-29 上午10:57:33
	 * @param pubitemTeam
	 * @return
	 */
	public List<PubitemTeamDetail> listTeamDetail(PubitemTeam pubitemTeam);

	/***
	 * 根据物资编码查询物资
	 * @param supply
	 * @return
	 */
	public MisSupply findSupplyBySpcode(MisSupply supply);

	/***
	 * 查询报价
	 * @param spprice
	 * @return
	 */
	public Spprice findBprice(Spprice spprice);
	
	/**
	 * 描述：查询活动类别
	 * @author 马振
	 * 创建时间：2016-3-1 下午4:05:52
	 * @return
	 */
	public List<ActTyp> findAllActTyp();

	/**
	 * 描述：查询支付方式
	 * @author 马振
	 * 创建时间：2016-3-1 下午4:06:49
	 * @return
	 */
	public List<Payment> findAllPayment();

	/**
	 * 描述：查询活动
	 * @author 马振
	 * 创建时间：2016-3-1 下午4:07:00
	 * @return
	 */
	public List<Actm> findAllActm();
	
	/**
	 * 描述：获取营运报告预估值
	 * @author 马振
	 * 创建时间：2016-3-3 下午4:57:10
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> getYuGu(Map<String,Object> map);

	/***
	 * 根据供应商可供物资查询税率
	 * @param ms
	 * @return
	 */
	public MisSupply findTaxByDeliverSpcode(MisSupply ms);

	/***
	 * 查找供应商
	 * @param deliver
	 * @return
	 */
	public List<Deliver> findDeliver(Deliver deliver);

	/***
	 * 查询物资余额集合
	 * @param supplyAcct
	 * @return
	 */
	public List<PositnSupply> findViewPositnSupplyList(SupplyAcct supplyAcct);

	/***
	 * 查询表单是否已预审核
	 * @param map
	 * @return
	 */
	public int findCountByChknos(Map<String, String> map);

	/***
	 * 预审核
	 * @param map
	 */
	public void preCheck(Map<String, String> map);
	
	/***
	 * 预审核回滚
	 * @param map
	 */
	public void unPreCheck(Map<String, String> map);

}