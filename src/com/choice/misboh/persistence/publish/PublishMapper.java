package com.choice.misboh.persistence.publish;

import java.util.List;
import java.util.Map;

/**
 * 描述：数据下发
 * @author 马振
 * 创建时间：2015-8-25 上午10:59:33
 */
public interface PublishMapper {

	/**
	 * 描述：门店操作员
	 * @author 马振
	 * 创建时间：2015-8-25 上午10:59:43
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> toOperator(Map<String,String> map);
	/**
	 * 更新门店信息（数据版本、发布时间）
	 * @param map
	 * @return
	 */
	public void updateStore(Map<String,String> map);
}
