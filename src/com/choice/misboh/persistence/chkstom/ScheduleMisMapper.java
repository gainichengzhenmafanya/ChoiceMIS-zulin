package com.choice.misboh.persistence.chkstom;

import java.util.List;

import com.choice.misboh.domain.chkstom.ScheduleD;

public interface ScheduleMisMapper {

	 
	/**
	 * 查询配送班表 list
	 */
	public List<ScheduleD> findSchedulesByPositn(ScheduleD scheduleD);
 
}
