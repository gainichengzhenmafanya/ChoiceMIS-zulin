package com.choice.misboh.persistence.chkstom;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.choice.misboh.domain.chkstom.MISChkstoDemoFirm;
import com.choice.misboh.domain.chkstom.MISChkstoDemom;

/**
 * 报货单单据模板主表
 * @author 孙胜彬
 */
public interface ChkstoDemomMisMapper {
	
	/**
	 * 查询报货单主表信息
	 * @param chkstoDemom
	 * @return
	 */
	public List<MISChkstoDemom> listChkstoDemom(MISChkstoDemom chkstoDemom);
	/**
	 * 新增报货单主表
	 * @param chkstoDemom
	 */
	public MISChkstoDemom saveChkstoDemom(MISChkstoDemom chkstoDemom);
	/**
	 * 修改报货单主表
	 * @param chkstoDemom
	 */
	public void updateChkstoDemom(MISChkstoDemom chkstoDemom);
	/**
	 * 删除报货单主表
	 * @param scode 
	 * @param chkstoDemom
	 */
	public void deleteChkstoDemom(@Param(value="list")List<String> idList, @Param(value="scode")String scode);
	/**
	 * 添加适用分店
	 * @param chkstoDemoFirm
	 */
	public void saveChkstoDemoFirm(MISChkstoDemoFirm chkstoDemoFirm);
	/**
	 * 删除适用分店
	 * @param idList
	 * @param scode 
	 */
	public void deleteChkstoDemoFirm(@Param(value="list")List<String> idList, @Param(value="scode")String scode);
	
	
	
}
