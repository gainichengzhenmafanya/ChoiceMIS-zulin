package com.choice.misboh.persistence.chkstom;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.misboh.domain.chkstom.Purchasedetails;
import com.choice.misboh.domain.inventory.Inventory;
import com.choice.scm.domain.ChkstoDemod;
import com.choice.scm.domain.ChkstoDemom;
import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Chkstom;

public interface ChkstomMisMapper {
	
	/**
	 * 查询当前最大单号
	 * @return
	 */
	public int getMaxChkstono();
	
	/**
	 * 关键字查询
	 * @param chkstomMap
	 * @return
	 */
	public List<Chkstom> findByKey(HashMap<Object, Object> chkstomMap);
	
	/**
	 * 根据单号查询
	 * @param chkstod
	 * @return
	 */
	public List<Chkstod> findByChkstoNos(Chkstod chkstod);
	
	/***
	 * 根据报货单号查明细从表
	 * @param chkstod
	 * @return
	 */
	public List<Chkstod> findChkstodByChkstoNo(Chkstod chkstod);

	/***
	 * 根据报货单号查明细主表
	 * @param chkstom
	 * @return
	 */
	public Chkstom findChkstomByChkstoNo(Chkstom chkstom);

	/***
	 * 门店审核 更新一下时间checd
	 * @param c
	 */
	public void checkChkstom(Chkstom c);

	/***
	 * 保存报货单主表
	 * @param chkstom
	 */
	public void saveNewChkstom(Chkstom chkstom);

	/***
	 * 修改报货单主表
	 * @param chkstom
	 */
	public void updateChkstom(Chkstom chkstom);

	/***
	 * 新增报货单从表
	 * @param chkstod
	 */
	public void saveNewChkstod_new(Chkstod chkstod);

	/***
	 * 更新报货单主表总金额
	 * @param chkstom
	 */
	public void updateChkstomTotalamt(Chkstom chkstom);

	/***
	 * 查询是否档口报货来的
	 * @param id
	 * @return
	 */
	public List<Chkstom> findChkstonoDept(Integer id);

	/***
	 * 更新档口报货单sqlserver专用
	 * @param chkstod
	 */
	public void updateChkstomDept0(Chkstod chkstod);

	/***
	 * 更新档口报货单
	 * @param chkstod
	 */
	public void updateChkstomDept(Chkstod chkstod);

	/***
	 * 删除报货单
	 * @param c
	 */
	public void deleteChk(Chkstom c);

	/***
	 * 删除拆单主表
	 * @param c
	 */
	public void deleteChkM_c(Chkstom c);

	/***
	 * 删除拆单从表
	 * @param c
	 */
	public void deleteChkD_c(Chkstom c);

	/***
	 * 审核报货单
	 * @param c
	 */
	public void checkChk_new(Chkstom c);

	/***
	 * 查询当前分店所在片区的主直拨库
	 * @param firm
	 * @return
	 */
	public String findMainPositnByFirm(String firm);

	/**
	 * 判断订货时间
	 * @author 文清泉
	 * @param 2015年1月1日 上午10:46:30
	 * @param dat 订货日
	 * @param scode 店铺编码
	 * @return
	 */
	public String eqOrderTime(@Param(value="dat")String dat,@Param(value="scode")String scode);

	/***
	 * 查询报货单模板主表
	 * @param chkstoDemom
	 * @return
	 */
	public List<ChkstoDemom> listChkstoDemom(ChkstoDemom chkstoDemom);

	/***
	 * 查询报货单模板从表
	 * @param chkstoDemod
	 * @return
	 */
	public List<ChkstoDemod> listChkstoDemod(ChkstoDemod chkstoDemod);
	
	/***
	 * 供应商邮件查询
	 * @param pd
	 * @return
	 */
	public List<Purchasedetails> findPdList(Purchasedetails pd);

	/***
	 * 查询报货单里 是否有特殊审核的物资
	 * @param c
	 * @return
	 */
	public int findStoCheckByChkstom(Chkstom c);

	/***
	 * 配送单位保存报货单从表
	 * @param chkstod
	 */
	public void saveChkstodDisunit(Chkstod chkstod);

	/***
	 * 直营店还是加盟店
	 * @param firm
	 * @return
	 */
	public String findFirmtypByScode(@Param(value="scode")String scode);
	
	/***
	 * 加盟商金额查询
	 * @param firm
	 * @return
	 */
	public Map<String,Object> checkSppriceSale(@Param(value="firm")String firm);

	/***
	 * 加盟商扣款
	 * @param c
	 */
	public void joiningChkstom(Chkstom chkstom);

	/***
	 * 查询当日 此加盟商 下门店已提交 但未验货的物资总金额
	 * @param c
	 * @return
	 */
	public Map<String, Object> findTamount(Chkstom c);

	/***
	 * 判断是否进行了盘点
	 * @param inventory
	 * @return
	 */
	public int checkInventory(Inventory inventory);

}
