package com.choice.misboh.persistence.chkstom;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Chkstom;

public interface ChkstomDeptMisMapper {
	
	/**
	 * 查询当前最大单号
	 * @return
	 */
	public int getMaxChkstono();
	
	/**
	 * 添加报货单主表
	 * @param chkstom
	 */
	public void saveNewChkstom(Chkstom chkstom);

	/**
	 * 更新报货单
	 * @param chkstomMap
	 */
	public void updateChkstom(Chkstom chkstom);
	
	/**
	 * 关键字查询
	 * @param chkstomMap
	 * @return
	 */
	public List<Chkstom> findByKey(HashMap<String, Object> chkstomMap);
	
	/**
	 * 删除
	 */
	public void deleteChk(Chkstom chkstom);
	
	/**
	 * 根据单号查询
	 * @param chkstod
	 * @return
	 */
	public List<Chkstod> findByChkstoNos(Chkstod chkstod);
	
	/***
	 * 新流程保存档口报货单从表
	 * @param chkstod
	 */
	public void saveNewChkstod_new(Chkstod chkstod);

	/***
	 * 根据报货单号查明细从表
	 * @param chkstod
	 * @return
	 */
	public List<Chkstod> findChkstodByChkstoNo(Chkstod chkstod);

	/***
	 * 根据报货单号查明细主表
	 * @param chkstom
	 * @return
	 */
	public Chkstom findChkstomByChkstoNo(Chkstom chkstom);

	/***
	 * 更新档口报货单
	 * @param chkstod
	 */
	public void updateChkstomDept(Chkstod chkstod);

	/***
	 * 更新档口报货单主表合计
	 * @param chkstom
	 */
	public void updateChkstomTotalamt(Chkstom chkstom);

	/***
	 * 九毛九报货单上传
	 * @param chkstod
	 * @return
	 */
	public List<Chkstod> findByChkstoNosJMJ(Chkstod chkstod);

	/***
	 * 查询档口报货单状态
	 * @param idList
	 * @return
	 */
	public int findStateByNos(@Param(value="idList")String idList);

	/***
	 * 更新报货单状态
	 * @param chkstod
	 */
	public void updateChkstomDeptBak1(Chkstod chkstod);
}
