package com.choice.misboh.persistence.chkstom;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.choice.misboh.domain.chkstom.MISChkstoDemod;

/**
 * 报货单单据模板子表
 * @author 孙胜彬
 */
public interface ChkstoDemodMisMapper {
	
	/**
	 * 报货单从表查询
	 * @param chkstoDemod
	 * @param scode 
	 * @return
	 */
	public List<MISChkstoDemod> listChkstoDemod(MISChkstoDemod chkstoDemod);
	/**
	 * 新增报货单从表
	 * @param chkstoDemod
	 */
	public void saveChkstoDemod(MISChkstoDemod chkstoDemod);
	/**
	 * 修改报货单从表
	 * @param chkstoDemod
	 */
	public void updateChkstoDemod(MISChkstoDemod chkstoDemod);
	/**
	 * 删除报货单从表
	 * @param scode 
	 * @param chkstoDemod
	 */
	public void deleteChkstoDemod(@Param(value="list")List<String> idList, @Param(value="scode")String scode);
	
}
