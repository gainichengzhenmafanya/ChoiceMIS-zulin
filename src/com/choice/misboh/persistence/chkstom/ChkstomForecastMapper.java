package com.choice.misboh.persistence.chkstom;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.misboh.domain.chkstom.ChkstomForecast;
import com.choice.misboh.domain.chkstom.ScheduleD;
import com.choice.misboh.domain.declareGoodsGuide.Declare;
import com.choice.misboh.domain.salesforecast.FirmItemUse;
import com.choice.misboh.domain.salesforecast.ItemPlan;
import com.choice.misboh.domain.salesforecast.SalePlan;
import com.choice.misboh.domain.salesforecast.SpCodeUseMis;

/***
 * 报货向导mapper
 * @author wjf
 *
 */
public interface ChkstomForecastMapper {

	/**
	 * 获取参考计算周期实际营业额
	 * @author 文清泉
	 * @param 2015年4月9日 下午1:31:48
	 * @param bdat 开始时间
	 * @param edat 结束时间
	 * @param dept 部门
	 * @param scode 店铺编码
	 * @return
	 */
	public List<Map<String, Object>> getCalculationMoney(@Param(value="bdat")String bdat,
			@Param(value="edat")String edat, @Param(value="dept")String dept, @Param(value="scode")String scode);

	/**
	 * 获取参考计算物资  实际用量
	 * @author 文清泉
	 * @param 2015年4月9日 下午1:31:48
	 * @param bdat 开始时间
	 * @param edat 结束时间
	 * @param dept 部门
	 * @param scode 店铺编码
	 * @param category_code 报货类别
	 * @return
	 */
	public List<Map<String, Object>> getCalculationSpcode(@Param(value="bdat")String bdat,
			@Param(value="edat")String edat, @Param(value="dept")String dept, @Param(value="scode")String scode, @Param(value="category_code")String category_code);


	/**
	 * 菜品销售计划与点击率物资用量计算
	 * @author 文清泉
	 * @param 2015年4月25日 下午3:42:34
	 * @param category_code 报货类别
	 * @param bdat 开始时间
	 * @param edat 结束时间
	 * @param dept 部门
	 * @param scode 店铺编码
	 * @return
	 */
	public List<Declare> calculationFoodSale(@Param(value="category_code")String category_code,
			@Param(value="bdat")String bdat, @Param(value="edat")String edat, @Param(value="dept")String dept, @Param(value="scode")String scode);

	/***
	 * 启用配送班表 查找报货类别
	 * @param cf
	 * @return
	 */
	public List<ScheduleD> getCodetypListBySchedule(ChkstomForecast cf);
	
	/***
	 * 不启用配送班表 查找报货类别
	 * @param cf
	 * @return
	 */
	public List<ScheduleD> getCodetypListNoSchedule(ChkstomForecast cf);


	/***
	 * 获取单据报货次数
	 * @param cf
	 * @return
	 */
	public List<Map<String, Object>> getChkstomCountByCodetyp(ChkstomForecast cf);

	/***
	 * 安全库存报货
	 * @param sd
	 * @return
	 */
	public List<Declare> getSafeStockList(ScheduleD sd);

	/***
	 * 周期平均用量报货 门店
	 * @param sd
	 * @return
	 */
	public List<Declare> getAvgWeekList(ScheduleD sd);
	
	/***
	 * 周期平均用量 档口
	 * @param sd
	 * @return
	 */
	public List<Declare> getAvgWeekDeptList(ScheduleD sd);

	/***
	 * 直接填写报货单
	 * @param sd
	 * @return
	 */
	public List<Declare> getAddChkstomList(ScheduleD sd);

	/***
	 * 调整预估值
	 * @param salePlan
	 * @return
	 */
	public List<SalePlan> listAdjustTheEstimated(SalePlan salePlan);

	/***
	 * 查询boh部门编码
	 * @param firm
	 * @param dept
	 * @return
	 */
	public String findPositnFirm(@Param(value="firm")String firm, @Param(value="dept")String dept);

	/***
	 * 查询菜品销售计划
	 * @param itemPlan
	 * @return
	 */
	public List<ItemPlan> findPosItemPlan(ItemPlan itemPlan);

	/***
	 * 查询菜品点击率
	 * @param f
	 * @return
	 */
	public List<FirmItemUse> findAllItemUse(FirmItemUse f);

	/***
	 * 删掉菜品销售计划
	 * @param posItemPlan
	 */
	public void deletePosItemPlan(ItemPlan posItemPlan);

	/***
	 * 插入菜品销售计划
	 * @param itemPlan
	 */
	public void insertItemPlan(ItemPlan itemPlan);

	/***
	 * 修改菜品销售计划
	 * @param itemPlan
	 * @return
	 */
	public int updateItemPlan(ItemPlan itemPlan);

	/***
	 * choice3千元用量
	 * @param spCodeUse
	 * @return
	 */
	public List<Declare> listSpcodeUse(SpCodeUseMis spCodeUse);

}
