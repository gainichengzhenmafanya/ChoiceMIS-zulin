package com.choice.misboh.persistence.chineseReport;

import java.util.List;
import java.util.Map;

import com.choice.misboh.domain.reportMis.PublicEntity;

/**
 * 描述：中餐报表-销售分析
 * @author 马振
 * 创建时间：2015-6-25 下午1:14:05
 */
public interface ChineseSalesReportMapper {

	/**
	 * 描述：项目销售统计  
	 * @author 马振
	 * 创建时间：2015-6-27 下午2:38:25
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findProjectSalesStatistics(PublicEntity condition);
	public List<Map<String,Object>> findProjectSalesStatisticsSum(PublicEntity condition);
	
	/**
	 * 描述：菜品销售排行榜
	 * @author 马振
	 * 创建时间：2015-7-14 下午2:35:26
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> findFoodList(Map<String, Object> map);
	
	/**
	 * 描述：客单-时段销售分析报表
	 * @author 马振
	 * 创建时间：2015-6-27 下午3:15:07
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> findSaleTcByTime(Map<String, Object> map);
	public List<Map<String, Object>> findSaleTcByTimeSUM(Map<String, Object> map);
	
	/**
	 * 描述：类别-时段销售分析报表
	 * @author 马振
	 * 创建时间：2015-6-27 下午3:15:17
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> findSaleTypeByTime(Map<String, Object> map);
	public List<Map<String, Object>> findSaleTypeByTimeSUM(Map<String, Object> map);
	
	/**
	 * 描述：套餐明细统计查询及合计
	 * @author 马振
	 * 创建时间：2015-6-27 下午3:14:13
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> queryMxtjb(PublicEntity condition);
	public List<Map<String,Object>> queryCalForMxtjb(PublicEntity condition);
	
	/**
	 * 描述：套餐销售统计查询及合计
	 * @author 马振
	 * 创建时间：2015-6-27 下午3:14:28
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> queryXstjb(PublicEntity condition);
	public List<Map<String,Object>> queryCalForXstjb(PublicEntity condition);
	
	/**
	 * 描述：集团消退菜分析
	 * @author 马振
	 * 创建时间：2015-7-14 下午2:49:36
	 * @param condition
	 * @return
	 */
	public List<Map<String ,Object>> findGroupCRDishes(PublicEntity condition);
	
	/**
	 * 描述：集团消退菜分析合计
	 * @author 马振
	 * 创建时间：2015-7-14 下午2:49:45
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findCalForGroupCRDishes(PublicEntity condition);
	
	/**
	 * 描述：消退菜分析_单店退菜明细
	 * @author 马振
	 * 创建时间：2015-7-14 下午2:49:56
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> findGroupCRDishesByFirm(PublicEntity condition);
	
	/**
	 * 描述：消退菜分析_单店退菜明细合计
	 * @author 马振
	 * 创建时间：2015-7-14 下午2:50:07
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> findGroupCRDishesByFirmTotal(PublicEntity condition);

	/**
	 * 描述：排班报表及其合计
	 * @author 马振
	 * 创建时间：2015-10-30 下午3:23:09
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> createScheduleReport(Map<String, Object> map);
	public List<Map<String, Object>> createScheduleReportSUM(Map<String, Object> map);
}