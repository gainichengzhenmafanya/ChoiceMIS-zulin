package com.choice.misboh.persistence.chineseReport;

import java.util.List;
import java.util.Map;

import com.choice.misboh.domain.reportMis.PublicEntity;

/**
 * 描述：中餐报表-活动分析
 * @author 马振
 * 创建时间：2015-6-25 下午1:14:05
 */
public interface ChineseActivityReportMapper {

	/**
	 * 描述：集团活动分析-按日
	 * @author 马振
	 * 创建时间：2015-7-14 下午1:58:44
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> queryJiTuanHuoDongFenXi1(PublicEntity condition);
	public List<Map<String,Object>> queryJiTuanHuoDongFenXi2(PublicEntity condition);
	public List<Map<String,Object>> queryJiTuanHuoDongFenXi3(PublicEntity condition);
	
	/**
	 * 描述：集团活动分析-按月
	 * @author 马振
	 * 创建时间：2015-7-14 下午1:59:00
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> queryJiTuanHuoDongFenXiByMonth1(PublicEntity condition);
	public List<Map<String,Object>> queryJiTuanHuoDongFenXiByMonth2(PublicEntity condition);
	public List<Map<String,Object>> queryJiTuanHuoDongFenXiByMonth3(PublicEntity condition);
	
	/**
	 * 描述：台位活动明细报表
	 * @author 马振
	 * 创建时间：2015-7-14 上午8:52:06
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> findActmByTbl(PublicEntity condition);
	public List<Map<String, Object>> findActmByTblSUM(PublicEntity condition);
	
	/**
	 * 描述：菜品活动明细报表
	 * @author 马振
	 * 创建时间：2015-7-14 上午8:52:36
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> findActmByItem(PublicEntity condition);
	public List<Map<String, Object>> findActmByItemSUM(PublicEntity condition);
}