package com.choice.misboh.persistence.chineseReport;

import java.util.List;
import java.util.Map;

import com.choice.misboh.domain.reportMis.Business;
import com.choice.misboh.domain.reportMis.PublicEntity;

/**
 * 描述：中餐报表-财务分析
 * @author 马振
 * 创建时间：2015-6-25 下午1:14:05
 */
public interface ChineseFinancialReportMapper {

	/**
	 * 描述：查询数据  收入日结算报表
	 * @author 马振
	 * 创建时间：2015-6-26 下午4:11:04
	 */
	public void deleteTerm();
	public void insertterm(PublicEntity term);
	public void insertDaySumReport(Map<String, Object> map);
	public List<Map<String, String>> findSqlRJS();
	
	/**
	 * 描述：查询数据  营业日报表
	 * @author 马振
	 * 创建时间：2015-6-26 下午5:37:45
	 * @param condition
	 * @return
	 */
	public List<Business> findSqlYYBB(PublicEntity condition);
	
	/**
	 * 查询门店账单分析报表_表格显示及合计
	 * @param condition
	 * @return
	 * @author YGB
	 */
	public List<Map<String,Object>> queryDdzdfx(PublicEntity condition);
	public List<Map<String,Object>> queryCalForDdzdfx(PublicEntity condition);
	
	/**
	 * 描述：根据表格显示serial查询账单详细信息及合计
	 * @author 马振
	 * 创建时间：2015-6-27 上午11:07:24
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findZhangDanMingXi(PublicEntity condition);
	public List<Map<String,Object>> findCalForZhangDanMingXi(PublicEntity condition);

	/**
	 * 查询门店账单分析报表_汇总报表及合计
	 * @param condition
	 * @return
	 * @author YGB
	 */
	public List<Map<String,Object>> queryDdzdfxByHuiZong(PublicEntity condition);
	public List<Map<String,Object>> queryCalForDdzdfxByHuiZong(PublicEntity condition);
	
	/**
	 *  门店反结算分析
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> queryDdfjsfx(PublicEntity condition);
	public List<Map<String, Object>> queryCalForDdfjsfx(PublicEntity condition);
	
	/**
	 * 描述：门店反结算分析明细
	 * @author 马振
	 * 创建时间：2015-7-14 上午8:44:07
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> findDdfjsmx(PublicEntity condition);
	public List<Map<String, Object>> findCalForDdfjsmx(PublicEntity condition);
	
	
	/**
	 * 描述：查询门店收银统计_收银方式统计
	 * @author 马振
	 * 创建时间：2015-7-14 上午8:43:57
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> queryDdsyfstj(PublicEntity condition);
	public List<Map<String, Object>> queryCalForDdsyfstj(PublicEntity condition);
	
	/**
	 * 描述：门店收银统计_结算方式明细
	 * @author 马振
	 * 创建时间：2015-7-14 上午8:43:47
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> queryDdsyjsinfo(PublicEntity condition);
	public List<Map<String, Object>> queryCalForDdsyjsinfo(PublicEntity condition);
}