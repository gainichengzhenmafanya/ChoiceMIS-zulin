package com.choice.misboh.persistence.chineseReport;

import java.util.List;
import java.util.Map;

import com.choice.misboh.domain.reportMis.PublicEntity;

/**
 * 描述：中餐报表-营业分析
 * @author 马振
 * 创建时间：2015-6-25 下午1:14:05
 */
public interface ChineseBusinessReportMapper {

	/**
	 * 描述：门店营业显示分析
	 * @author 马振
	 * 创建时间：2015-7-8 下午2:42:36
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findGroupBusinessShow(PublicEntity condition);
	
    /**
     * 描述：门店营业显示分析-查询支付方式实收明细
     * @author 马振
     * 创建时间：2015-7-8 下午2:42:48
     * @param condition
     * @return
     */
    public List<Map<String,Object>> findGroupPaymentInfo(PublicEntity condition);
    
    /**
     * 描述：门店营业显示分析-类别明细
     * @author 马振
     * 创建时间：2015-7-8 下午2:42:58
     * @param condition
     * @return
     */
    public List<Map<String,Object>> findGroupGrptypInfo(PublicEntity condition);
	
	/**
	 * 描述：门店收入分析按时间段汇总
	 * @author 马振
	 * 创建时间：2015-7-8 下午4:15:21
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findGroupIncomeByDate(Map<String,Object> map);
	
	/**
	 * 描述：门店收入分析按天明细
	 * @author 马振
	 * 创建时间：2015-7-8 下午4:15:35
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findGroupIncomeByDay(Map<String,Object> map);
	
	/**
	 * 描述：门店收入分析，合计
	 * @author 马振
	 * 创建时间：2015-7-8 下午4:15:53
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findCalForGroupIncome(Map<String,Object> map);
    
	/**
	 * 描述：门店销售分析——按类汇总
	 * @author 马振
	 * 创建时间：2015-6-25 下午1:19:35
	 * @param publicEntity
	 * @return
	 */
	public List<Map<String,Object>> findGroupSalesByCate(PublicEntity publicEntity);
	
	/**
	 * 描述：门店销售分析——按菜汇总
	 * @author 马振
	 * 创建时间：2015-6-25 下午1:19:46
	 * @param publicEntity
	 * @return
	 */
	public List<Map<String,Object>> findGroupSalesByFood(PublicEntity publicEntity);
	
	/**
	 * 描述：门店销售查询——按日期汇总查询
	 * @author 马振
	 * 创建时间：2015-6-25 下午1:20:08
	 * @param publicEntity
	 * @return
	 */
	public List<Map<String,Object>> findGroupSalesSumByDate(PublicEntity publicEntity);
	
	/**
	 * 描述：门店销售汇总查询合计   按类汇总、按菜汇总
	 * @author 马振
	 * 创建时间：2015-6-25 下午1:20:25
	 * @param publicEntity
	 * @return
	 */
	public List<Map<String,Object>> findCalForGroupSales(PublicEntity publicEntity);
	
	/**
	 * 描述：查询数据  营业日报区域汇总表 
	 * @author 马振
	 * 创建时间：2015-7-13 下午4:59:35
	 * @param map
	 * @return
	 */
	public List<Map<String, String>> findJTRFX(Map<String, Object> map);
	public List<Map<String, String>> findJTRFXSUM(Map<String, Object> map);
	
	/**
	 * 描述：门店点菜出错率分析及其合计
	 * @author 马振
	 * 创建时间：2015-7-13 下午5:17:35
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findCaiPinChuCuoLv(PublicEntity condition);
	public List<Map<String,Object>> findCalForCaiPinChuCuoLv(PublicEntity condition);
}