package com.choice.misboh.persistence.inventory;

import java.util.List;

import com.choice.misboh.domain.inventory.PositnSupply;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Spbatch;
import com.choice.scm.domain.Supply;

public interface CkInitMisMapper {
	
	/**
	 * 仓库期初
	 * @param positnSupply
	 */
	public List<PositnSupply> getpositnSupplyList(PositnSupply positnSupply);
	
	/**
	 * 获取仓位是否已经初始
	 * @param monthh
	 */
	public Positn getQC(Positn positn);
	
	/***
	 * 删除supplybeginning
	 * @param positnSupply
	 */
	public void deleteSupplyBeginning(PositnSupply positnSupply);
	
	/***
	 * 保存supplyBeginning
	 * @param ps
	 */
	public void saveSupplyBeginning(PositnSupply ps);
	
	/***
	 * 更新supplybeginning表
	 * @param positnSupply
	 */
	public void updateSupplyBeginning(PositnSupply positnSupply);
	
	/***
	 * 得到supplybeginning 的序列值
	 * @return
	 */
	public String findSupplyBeginNextVal();
	
	/**
	 * 保存期初
	 * @param positnSupply
	 * @return
	 */
	public void saveCkInit(PositnSupply positnSupply);
	
	/**
	 * 获取最大id
	 * @return
	 */
	public Spbatch getMaxId();
	
	/**
	 * 确认初始
	 * @param spbatch
	 * @return
	 */
	public void insertSpbatch(Spbatch spbatch);
	
	/**
	 * 更新物资表中的物资余额
	 */
	public void updateSupplyCnt(Supply supply);
	
	/**
	 * 更新仓位状态为已初始
	 * @param positnSupply
	 * @return
	 */
	public void updatePositn(PositnSupply PositnSupply);
	
	/**
	 * 修改期初
	 * @param positnSupply
	 * @return
	 */
	public void updateCkInit(PositnSupply positnSupply);
	/**
	 * 删除期初
	 * @param positnSupply
	 * @return
	 */
	public void deleteCkInit(PositnSupply positnSupply);

	/***
	 * 根据分店物资属性去查导出的物资
	 * @param positn
	 * @return
	 */
	public List<PositnSupply> getpositnSupplyListByPositnSpcode(Positn positn);
	/***
	 * 期初状态查询
	 * @param inventory
	 * @return
	 */
	public List<Positn> qcZtchaxun(Positn positn);

	/***
	 * 
	 * @param positnSupply
	 */
	public void updatePositnspcodePrice(PositnSupply positnSupply);
	
	
	
}
