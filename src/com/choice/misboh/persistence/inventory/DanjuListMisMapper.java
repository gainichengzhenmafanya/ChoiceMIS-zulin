package com.choice.misboh.persistence.inventory;

import java.util.List;

import com.choice.misboh.domain.inventory.Danju;

public interface DanjuListMisMapper {
	
	/***
	 * 查询单据列表
	 * @param dis
	 * @return
	 */
	public List<Danju> findListDanju(Danju danju);
	
}
