package com.choice.misboh.persistence.inventory;


import java.util.List;
import java.util.Map;

import com.choice.misboh.domain.inspection.ArrivalmMis;
import com.choice.misboh.domain.inventory.Chkstoref;
import com.choice.misboh.domain.inventory.Chkstoreo;
import com.choice.misboh.domain.inventory.Inventory;
import com.choice.misboh.domain.inventory.PositnSupply;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.InventoryDemod;
import com.choice.scm.domain.InventoryDemom;
import com.choice.scm.domain.Product;
import com.choice.scm.domain.Spcodeexm;
import com.choice.scm.domain.SupplyAcct;

public interface InventoryMisMapper {
	
	/***
	 * 查询盘点表中有没有当天未审核的数据
	 * @param cf
	 * @return
	 */
	public Chkstoref findChkstoref(Chkstoref cf);
	
	/***
	 * mis 盘点 分页
	 * @author wjf
	 * @param inventory
	 * @return
	 */
	public List<Inventory> findAllInventoryPage(Inventory inventory);
	
	/***
	 * mis 盘点 总数
	 * @param inventory
	 * @return
	 */
	public int findAllInventoryCount(Inventory inventory);

	/***
	 * 盘点  查询不分页
	 * @param inventory
	 * @return
	 */
	public List<Inventory> findAllInventory(Inventory inventory);
	
	/***
	 * 盘点  查询不分页
	 * @param inventory
	 * @return
	 */
	public List<Inventory> findAllInventoryEnd(Inventory inventory);
	
	/***
	 * 得到当前的物资结存
	 * @param inventory
	 * @return
	 */
	public Inventory getWzyue(Inventory inventory);
	
	/***
	 * 删除盘点从表
	 * @param cf
	 */
	public void deleteChkstoreo(Chkstoref cf);
	
	/***
	 * 删除盘点主表
	 * @param cf
	 */
	public void deleteChkstoref(Chkstoref cf);
	
	/***
	 * 插入盘点主表
	 * @param cf
	 */
	public void insertChkstoref(Chkstoref cf);
	
	/***
	 * 插入盘点从表
	 * @param co
	 */
	public void insertChkstoreo(Chkstoreo co);
	
	/***
	 * 批量插入盘点从表
	 * @param colist
	 */
	public void insertChkstoreoList(List<Chkstoreo> colist);
	
	/***
	 * 查找positnsupply
	 * @param ps
	 * @return
	 */
	public PositnSupply findPositnSupply(PositnSupply ps);
	
	/***
	 * 插入positnsupply
	 * @param ps
	 */
	public void insertPositnSupply(PositnSupply ps);
	
	/***
	 * 插入此次盘点物资余额表里没有的物资
	 * @param inventory
	 */
	public void insertPositnSupplyByChkstoreo(Inventory inventory);
	
	/***
	 * 更新positnsupply
	 * @param ps
	 */
	public void updatePositnSupply(PositnSupply ps);
	
	/***
	 * 得到物资在门店的最后一次进价
	 * @param sa
	 * @return
	 */
	public Double findLastPriceBySupplyAcct(SupplyAcct sa);
	
	/***
	 * 更新盘点从表
	 * @param co
	 */
	public void updateChkstoreo(Chkstoreo co);
	
	/***
	 * 更新盘点主表
	 * @param cf
	 */
	public void updateChkstoref(Chkstoref cf);
	
	/***
	 * 查找物资余额表状态
	 * @param inventory
	 * @return
	 */
	public Inventory getPositnSupply(Inventory inventory);

	/***
	 * 查询分店能用的模板主表
	 * @param inventoryDemom
	 * @return
	 */
	public List<InventoryDemom> listInventoryDemom(InventoryDemom inventoryDemom);

	/***
	 * 查询盘点模板子表
	 * @param inventoryDemod
	 * @return
	 */
	public List<InventoryDemod> listInventoryDemod(InventoryDemod inventoryDemod);

	/***
	 * 查询不一样的
	 * @param inventory
	 * @return
	 */
	public List<PositnSupply> findChkstoreoByPositnSupply(Inventory inventory);

	/***
	 * 查询盘点模板从表1
	 * @param inventoryDemod
	 * @return
	 */
	public List<Inventory> listInventoryDemod1(InventoryDemod inventoryDemod);

	/***
	 * 查询九毛九直配未验货单据数量
	 * @param arrivalmMis
	 * @return
	 */
	public int findArrilvalmCount(ArrivalmMis arrivalmMis);

	/***
	 * 直配未验货的数量
	 * @param dis
	 * @return
	 */
	public int findAllDisCount(Dis dis);

	/***
	 * 配送未验货按单据
	 * @param sa
	 * @return
	 */
	public int findOutBillCount(SupplyAcct sa);

	/***
	 * 配送未验货按物资
	 * @param sa
	 * @return
	 */
	public int findOutCount(SupplyAcct sa);

	/***
	 * 调拨未验货
	 * @param sa
	 * @return
	 */
	public int findOutDbCount(SupplyAcct sa);

	/***
	 * 未审核入库单数量
	 * @param map
	 * @return
	 */
	public int findChkinmCount(Map<String, Object> map);

	/***
	 * 未审核出库单
	 * @param map
	 * @return
	 */
	public int findChkoutmCount(Map<String, Object> map);

	/***
	 * 查询盘点详情
	 * @param co
	 * @return
	 */
	public List<Inventory> findChkstoreo(Chkstoreo co);

	/***
	 * 查询物资当月入-出
	 * @param inventory
	 * @return
	 */
	public List<Inventory> findSupplyAcctTheMonth(Inventory inventory);

	/***
	 * 批量修改物资单价
	 * @param cf
	 */
	public void updateChkstoreoByCf(Chkstoref cf);

	/**
	 * 根据code查询原材料
	 * @param spcodeexm
	 * @return
	 */
	public List<Product> findMaterial(Spcodeexm spcodeexm);
	
}
