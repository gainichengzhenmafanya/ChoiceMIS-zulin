package com.choice.misboh.persistence.BaseRecord;

import java.util.List;

import com.choice.misboh.domain.BaseRecord.MisBohOperator;
import com.choice.misboh.domain.OperationManagement.StoreRange;

/**
 * 描述：门店操作员维护
 * @author 马振
 * 创建时间：2015-8-24 下午1:40:13
 */
public interface OperatorMaintenanceMapper {

	/**
	 * 描述：显示全部门店操作员
	 * @author 马振
	 * 创建时间：2015-8-24 下午1:40:41
	 * @param operator
	 * @param session
	 * @return
	 */
	public List<MisBohOperator> listOperatorMaintenance(MisBohOperator operator);
	
	/**
	 * 描述：根据门店操作员主键查询门店操作员信息
	 * @author 马振
	 * 创建时间：2015-8-24 下午2:22:37
	 * @param operator
	 * @return
	 */
	public MisBohOperator getOperatorByPk(MisBohOperator operator);
	
	/**
	 * 描述：新增门店操作员
	 * @author 马振
	 * 创建时间：2015-8-24 下午4:56:41
	 * @param operator
	 * @return
	 */
	public void insertOperator(MisBohOperator operator);
	
	/**
	 * 描述：修改门店操作员
	 * @author 马振
	 * 创建时间：2015-8-24 下午4:57:11
	 * @param operator
	 * @return
	 */
	public void updateOperator(MisBohOperator operator);
	
	/**
	 * 描述：删除门店操作员
	 * @author 马振
	 * 创建时间：2015-8-24 下午4:57:20
	 * @param operator
	 * @return
	 */
	public void deleteOperator(MisBohOperator operator);
	
	/**
	 * 描述：启用、停用门店操作员
	 * @author 马振
	 * 创建时间：2015-8-24 下午4:57:29
	 * @param operator
	 * @return
	 */
	public void ableOperator(MisBohOperator operator);
	
	/**
	 * 描述：删除门店配送范围主表
	 * @author 马振
	 * 创建时间：2016-3-12 下午4:08:18
	 * @param storeRange
	 */
	public int deleteStoreRange(StoreRange storeRange);
	
	/**
	 * 描述：删除门店配送范围坐标
	 * @author 马振
	 * 创建时间：2016-3-12 下午4:08:48
	 * @param storeRange
	 */
	public int deleteRangeCoordi(StoreRange storeRange);
	
	/**
	 * 描述：跟据门店主键查询该门店的配送范围主表信息
	 * @author 马振
	 * 创建时间：2016-3-14 下午1:54:26
	 * @param storeRange
	 */
	public List<StoreRange> listStoreRange(StoreRange storeRange);

	/**
	 * 描述：跟据门店主键查询及区域编号该门店的配送范围子表信息
	 * @author 马振
	 * 创建时间：2016-3-14 下午2:13:32
	 * @param storeRange
	 * @return
	 */
	public List<StoreRange> listRangeCoordi(StoreRange storeRange);
	
	/**
	 * 描述：添加门店配送范围
	 * @author 马振
	 * 创建时间：2016-3-12 下午4:21:11
	 * @param listStoreRange
	 */
	public int addStoreRange(List<StoreRange> listStoreRange);
	
	/**
	 * 描述：添加门店配送范围坐标
	 * @author 马振
	 * 创建时间：2016-3-12 下午4:21:33
	 * @param listStoreRange
	 */
	public int addRangeCoordi(List<StoreRange> listStoreRange);
}