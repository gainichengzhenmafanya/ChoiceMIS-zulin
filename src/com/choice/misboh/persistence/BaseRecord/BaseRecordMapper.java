package com.choice.misboh.persistence.BaseRecord;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.misboh.domain.BaseRecord.ActTyp;
import com.choice.misboh.domain.BaseRecord.Actlck;
import com.choice.misboh.domain.BaseRecord.Actm;
import com.choice.misboh.domain.BaseRecord.ActmFen;
import com.choice.misboh.domain.BaseRecord.ActmVoucher;
import com.choice.misboh.domain.BaseRecord.Actstr;
import com.choice.misboh.domain.BaseRecord.Interval;
import com.choice.misboh.domain.BaseRecord.Item;
import com.choice.misboh.domain.BaseRecord.ItemPkg;
import com.choice.misboh.domain.BaseRecord.Itmdtl;
import com.choice.misboh.domain.BaseRecord.Marsaleclass;
import com.choice.misboh.domain.BaseRecord.OtherStockDataF;
import com.choice.misboh.domain.BaseRecord.OtherStockDataO;
import com.choice.misboh.domain.BaseRecord.PackageDtl;
import com.choice.misboh.domain.BaseRecord.PackageType;
import com.choice.misboh.domain.BaseRecord.Payment;
import com.choice.misboh.domain.BaseRecord.PubItem;
import com.choice.misboh.domain.BaseRecord.PubPackage;
import com.choice.misboh.domain.BaseRecord.StorePos;
import com.choice.misboh.domain.BaseRecord.Wptime;
import com.choice.misboh.domain.reportMis.PublicEntity;

/**
 * 描述： 基础档案：菜品设置、套餐设置、POS定义、门店活动、水电气
 * @author 马振
 * 创建时间：2015-4-2 上午10:42:39
 */
/**
 * 描述:
 * 作者:马振
 * 时间:2016年7月29日下午3:36:12
 */
public interface BaseRecordMapper {
	
	/**
	 * 描述：获取 菜品类别  元数据
	 * @author 马振
	 * 创建时间：2015-4-20 上午11:16:39
	 * @return
	 */
	public List<Map<String, Object>> marsaleclassTree(PubItem pubitem);
	
	/**
	 * 描述：查询菜品信息
	 * @author 马振
	 * 创建时间：2015-4-2 上午10:43:03
	 * @param pubitem
	 * @return
	 */
	public List<PubItem> listPubitem(PubItem pubitem);
	
	/**
	 * 描述： 菜品打印信息
	 * @author 马振
	 * 创建时间：2016年4月19日下午9:58:01
	 * @param pubitem
	 * @return
	 */
	public List<Map<String, Object>> listPubitemPrint(PubItem pubitem);
	
	/**
	 * 描述：根据id查询菜品类别
	 * @author 马振
	 * 创建时间：2015-4-20 下午12:21:39
	 * @param pk_Marsaleclass
	 * @return
	 */
	public Marsaleclass findMarsaleclassById(String pk_Marsaleclass);
	
	/**
	 * 描述：根据中类查找菜品(选择菜品的弹窗)
	 * @author 马振
	 * 创建时间：2015-4-24 下午3:15:48
	 * @param map
	 * @return
	 */
	public List<PubItem> findpubitemByMclassForPub(Map<String,Object> map);
	
	/**
	 * 描述：查询套餐信息选择用
	 * @author 马振
	 * 创建时间：2015-4-24 下午3:33:38
	 * @return
	 */
	public List<Map<String, Object>> findpubpackList();
	
	/**
	 * 描述：查找菜品类别：大、中、小
	 * @author 马振
	 * 创建时间：2015-4-24 下午3:17:17
	 * @param Marsaleclass
	 * @return
	 */
	public List<Marsaleclass> findAllMarsaleclassZero(Marsaleclass Marsaleclass);
    public List<Marsaleclass> findAllMarsaleclassOne(Marsaleclass Marsaleclass);
    public List<Marsaleclass> findAllMarsaleclassTwo(Marsaleclass Marsaleclass);
	
	/**
	 * 描述：查询所有启用的套餐类别
	 * @author 马振
	 * 创建时间：2015-4-20 下午6:26:11
	 * @return
	 */
	public List<PackageType> listPackageType();
	
	/**
	 * 描述：查询套餐信息
	 * @author 马振
	 * 创建时间：2015-4-2 下午1:14:31
	 * @param pubPackage
	 * @return
	 */
	public List<Map<String,Object>> listPubPackage(PubPackage pubPackage);
	
	/**
	 * 描述：根据套餐主键查询套餐明细
	 * @author 马振
	 * 创建时间：2015-4-20 下午2:50:17
	 * @param pk_pubpack
	 * @return
	 */
	public List<Map<String,Object>> listPackageDtl(PackageDtl packageDtl);
	
	/**
	 * 描述：可换菜明细查询
	 * @author 马振
	 * 创建时间：2015-4-20 下午5:30:20
	 * @param itemPkg
	 * @return
	 */
	public List<Map<String,Object>> listItemPkg(ItemPkg itemPkg);
	
	/**
	 * 描述：门店POS
	 * @author 马振
	 * 创建时间：2015-4-2 下午2:25:32
	 * @param storePos
	 * @return
	 */
	public List<StorePos> listStorePos(StorePos storePos);
	
	/**
	 * 描述：获取活动类别元数据树
	 * @author 马振
	 * 创建时间：2015-4-20 下午6:59:55
	 * @return
	 */
	public List<Map<String, Object>> actmTree();
	
	/**
	 * 描述：门店活动
	 * @author 马振
	 * 创建时间：2015-4-2 下午2:25:32
	 * @param storePos
	 * @return
	 */
	public List<Actm> listActm(Actm actm);
	
	/**
	 * 描述：根据活动id查找活动适合的会员卡类型
	 * @author 马振
	 * 创建时间：2015-4-21 上午11:32:26
	 * @param pk_actm
	 * @return
	 */
	public List<Map<String, Object>> findCardTypByActmId(String pk_actm);
	
	/**
	 * 描述：根据编码查询对应的会员卡类型名称
	 * @author 马振
	 * 创建时间：2015-4-21 下午12:24:18
	 * @param cardtyp
	 * @return
	 */
	public String findDesByTyp(String cardtyp);
	
	/**
	 * 描述：门店活动日期限制
	 * @author 马振
	 * 创建时间：2015-4-21 上午9:35:04
	 * @param actm
	 * @return
	 */
	public Actstr getActmStr(Actm actm);
	
	/**
	 * 描述：互斥活动
	 * @author 马振
	 * 创建时间：2015-4-21 下午12:34:27
	 * @param pk_Actm
	 * @return
	 */
	public List<Actlck> findActlckById(Actm actm);
	
	/**
	 * 描述：账单减免--金额减免
	 * @author 马振
	 * 创建时间：2015-4-21 下午3:31:26
	 * @param actm
	 * @return
	 */
	public List<Item> getJianMian(Actm actm);
	
	/**
	 * 描述：账单减免--金额调价
	 * @author 马振
	 * 创建时间：2015-4-21 下午3:31:26
	 * @param actm
	 * @return
	 */
	public List<Item> getDiaoJia(Actm actm);
	
	/**
	 * 描述：消费返券
	 * @author 马振
	 * 创建时间：2015-4-21 下午4:31:48
	 * @param actm
	 * @return
	 */
	public List<ActmVoucher> getFanQuan(Actm actm);
	
	/**
	 * 描述：消费返积分
	 * @author 马振
	 * 创建时间：2015-4-21 下午4:52:39
	 * @param actm
	 * @return
	 */
	public List<ActmFen> getActmFen(Actm actm);
	
	/**
	 * 描述：人数限制
	 * @author 马振
	 * 创建时间：2015-4-21 下午5:14:39
	 * @param actm
	 * @return
	 */
	public Actm getMemberLimit(Actm actm);
	
	/**
	 * 描述：张数限制
	 * @author 马振
	 * 创建时间：2015-4-21 下午5:15:39
	 * @param actm
	 * @return
	 */
	public Actm getTicketLimit(Actm actm);
	
	/**
	 * 描述：金额限制
	 * @author 马振
	 * 创建时间：2015-4-21 下午6:01:02
	 * @param actm
	 * @return
	 */
	public Actm getAmountLimit(Actm actm);
	
	/**
	 * 描述：班次限制
	 * @author 马振
	 * 创建时间：2015-4-21 下午6:01:10
	 * @param actm
	 * @return
	 */
	public Actm getClassesLimit(Actm actm);
	
	/**
	 * 描述：消费返券
	 * @author 马振
	 * 创建时间：2015-4-21 下午12:48:50
	 * @param pk_Actm
	 * @return
	 */
	public List<Map<String, Object>> findActmVoucherById(String pk_actm);
	
	/**
	 * 描述：消费返积分
	 * @author 马振
	 * 创建时间：2015-4-21 下午12:56:22
	 * @param pk_Actm
	 * @return
	 */
	public List<Map<String, Object>> findActmFenById(String pk_actm);
	
	/**
	 * 描述：星期及时段
	 * @author 马振
	 * 创建时间：2015-4-21 上午10:39:29
	 * @param actm
	 * @return
	 */
	public List<Wptime> getWptime(Actm actm);
	
	/**
	 * 描述：产品限制
	 * @author 马振
	 * 创建时间：2015-4-23 下午5:50:49
	 * @param actm
	 * @return
	 */
	public List<Itmdtl> getPubitemLimit(Actm actm);
	
	/**
	 * 描述：新增门店pos
	 * @author 马振
	 * 创建时间：2015-4-7 上午10:54:09
	 * @param storePos
	 */
	public void addStorePos(StorePos storePos);
	
	/**
	 * 描述：修改门店pos
	 * @author 马振
	 * 创建时间：2015-4-7 上午10:54:18
	 * @param storePos
	 */
	public void updateStorePos(StorePos storePos);
	
	/**
	 * 描述：获取水电气主表最大排序号
	 * @author 马振
	 * 创建时间：2015-4-9 下午1:31:41
	 * @return
	 */
	public int getMaxSort();
	
	/**
	 * 描述：显示水
	 * @author 马振
	 * 创建时间：2015-4-8 上午9:10:20
	 * @param pk_store
	 * @return
	 */
	public List<OtherStockDataO> listWater(OtherStockDataO otherStockDataO);
	
	/**
	 * 描述：根据主键查询水电气的总用量
	 * @author 马振
	 * 创建时间：2015-4-9 下午12:41:39
	 * @param pk_otherstockdataf
	 * @return
	 */
	public Double allCount(OtherStockDataF otherStockDataF);
	
	/**
	 * 描述：水主表
	 * @author 马振
	 * 创建时间：2015-4-8 下午2:42:54
	 * @param otherStockDataO
	 * @return
	 */
	public List<OtherStockDataF> waterCount(OtherStockDataF otherStockDataF);
	
	/**
	 * 描述：显示电
	 * @author 马振
	 * 创建时间：2015-4-8 上午9:14:30
	 * @param pk_store
	 * @return
	 */
	public List<OtherStockDataO> listElectric(OtherStockDataO otherStockDataO);
	
	/**
	 * 描述：电主表
	 * @author 马振
	 * 创建时间：2015-4-8 下午2:42:54
	 * @param otherStockDataO
	 * @return
	 */
	public List<OtherStockDataF> electricCount(OtherStockDataF otherStockDataF);
	
	/**
	 * 描述：显示气
	 * @author 马振
	 * 创建时间：2015-4-8 上午9:16:48
	 * @param pk_store
	 * @return
	 */
	public List<OtherStockDataO> listGas(OtherStockDataO otherStockDataO);
	
	/**
	 * 描述：气主表
	 * @author 马振
	 * 创建时间：2015-4-8 下午2:43:44
	 * @param otherStockDataO
	 * @return
	 */
	public List<OtherStockDataF> gasCount(OtherStockDataF otherStockDataF);
	
	/**
	 * 描述：新增水电气
	 * @author 马振
	 * 创建时间：2015-4-8 下午7:16:17
	 * @param otherStockDataF
	 */
	public void addWaterElectricGas(OtherStockDataF otherStockDataF);
	
	/**
	 * 描述：保存修改的水电气主表
	 * @author 马振
	 * 创建时间：2015-5-25 下午3:41:52
	 * @param otherStockDataF
	 */
	public void saveOtherStockDataF(OtherStockDataF otherStockDataF);
	
	/**
	 * 描述：根据主表主键删除子表信息
	 * @author 马振
	 * 创建时间：2015-4-9 下午2:04:39
	 * @param pk_otherstockdatao
	 */
	public void deleteDataO(OtherStockDataF otherStockDataF);
	
	/**
	 * 描述：根据主表主键删除子表信息
	 * @author 马振
	 * 创建时间：2015-4-9 下午2:05:02
	 * @param pk_otherstockdataf
	 */
	public void deleteDataF(OtherStockDataF otherStockDataF);
	
	/**
	 * 描述：根据主键查询主表信息
	 * @author 马振
	 * 创建时间：2015-1-1 下午5:45:30
	 * @param pk_otherstockdataf
	 * @return
	 */
	public OtherStockDataF getOtherStockDataF(String pk_otherstockdataf);
	
	/**
	 * 描述：根据主键查询主表信息
	 * @author 马振
	 * 创建时间：2015-1-1 下午5:45:30
	 * @param pk_otherstockdataf
	 * @return
	 */
	public OtherStockDataF getOtherStockDataFById(OtherStockDataF otherStockDataF);
	
	/**
	 * 描述：新增水电气子表
	 * @author 马振
	 * 创建时间：2015-4-10 下午6:54:31
	 * @param list
	 */
//	public void insertOtherStockDataO(List<OtherStockDataO> list);
	public void insertOtherStockDataO(OtherStockDataO otherStockDataO);
	
	/**
	 * 描述：根据主表主键查询子表信息
	 * @author 马振
	 * 创建时间：2015-4-11 上午10:55:32
	 * @param pk_otherstockdataf
	 * @param edat 
	 * @param bdat 
	 * @return
	 */
	public List<OtherStockDataO> selectAllByIdF(@Param(value="pk_otherstockdataf")String pk_otherstockdataf,@Param(value="bdat") String bdat, @Param(value="edat")String edat);
	
	/**
	 * 描述：查询支付类型
	 * @author 马振
	 * 创建时间：2015-4-16 下午1:20:52
	 * @param payment
	 * @return
	 */
	public List<Payment> findAllPayment(Payment payment);
	
	/**
	 * 描述：查找所有活动分类
	 * @author 马振
	 * 创建时间：2015-4-22 上午10:48:30
	 * @param actTyp
	 * @return
	 */
	public List<ActTyp> findAllActTyp();
	
	/**
	 * 描述：查询所有活动小类
	 * @author 马振
	 * 创建时间：2015-7-14 上午10:54:53
	 * @return
	 */
	public List<PublicEntity> findAllActTypMin();
	
	/**
	 * 描述：营业时段查询
	 * @author 马振
	 * 创建时间：2015-4-23 上午10:10:33
	 * @param interval
	 * @return
	 */
	public List<Interval> listInterval(Interval interval);

	/**
	 * 获取期初值
	 * @author 文清泉
	 * @param 2015年6月2日 下午7:17:28
	 * @param pk_otherstockdataf
	 * @param workdate
	 * @return
	 */
	public OtherStockDataF getOtherStockDataFByWorkdate(@Param(value="pk_otherstockdataf")String pk_otherstockdataf, @Param(value="workdate")String workdate);

	/**
	 * 描述:折扣报表根据市场过滤
	 * 作者:马振
	 * 时间:2016年7月29日下午3:15:35
	 * @param Actm
	 * @return
	 */
	public List<Actm> findAllActmByZheK(Actm Actm); 
	
	/**
	 * 描述:根据活动查询所有大类
	 * 作者:马振
	 * 时间:2016年7月29日下午3:25:19
	 * @param actm
	 * @return
	 */
	public List<ActTyp> findAllActTypByActm(Actm actm);
	
	/**
	 * 描述:根据活动查询所有小类
	 * 作者:马振
	 * 时间:2016年7月29日下午3:32:39
	 * @param actm
	 * @return
	 */
	public List<PublicEntity> findAllActTypMinByActm(Actm actm);
	
	/**
	 * 描述:查询支付方式
	 * 作者:马振
	 * 时间:2016年7月29日下午3:36:15
	 * @param actm
	 * @return
	 */
	public List<PublicEntity> findPaymodeByActm(Actm actm);
}