package com.choice.misboh.persistence.costReduction;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.choice.misboh.domain.costReduction.CostItem;
import com.choice.misboh.domain.costReduction.CostItemSpcode;
import com.choice.misboh.domain.costReduction.MisSupply;
import com.choice.scm.domain.Positn;


/**
 * 描述：成本核减
 * @author 马振
 * 创建时间：2015-4-14 上午9:53:03
 */
public interface CostReductionMapper {

	/**
	 * 描述：显示成本核减信息
	 * @author 马振
	 * 创建时间：2015-4-14 下午1:45:38
	 * @param vcode
	 * @return
	 */
	public List<CostItemSpcode> listCostReduction(CostItem costItem);
	
	/**
	 * 描述：根据日期获取要核减的销售数据
	 * @author 马振
	 * 创建时间：2015-4-15 上午10:14:01
	 * @param costItemSpcode
	 * @return
	 */
	public List<CostItem> getCostItemByDate(CostItemSpcode costItemSpcode);
	
	/**
	 * 描述：根据门店编码、菜品编码、单位获取bom表信息
	 * @author 马振
	 * 创建时间：2015-4-15 上午11:03:05
	 * @param string
	 * @return
	 */
	public int getCostDtl(@Param(value="firm")String firm);
	
	/**
	 * 描述：查询物资信息
	 * @author 马振
	 * 创建时间：2015-4-15 下午1:19:27
	 * @param costItemSpcode
	 * @return
	 */
	public MisSupply getSupply(MisSupply supply);
	
	/**
	 * 描述：将核减后的销售数据添加到核减物资表中
	 * @author 马振
	 * 创建时间：2015-4-15 上午11:04:17
	 * @param costItemSpcode
	 */
	public void saveCostItemSpcode(List<CostItemSpcode> costItemSpcode);
	
	/**
	 * 描述：反结算删除核减物资表中的数据
	 * @author 马振
	 * 创建时间：2015-1-1 下午5:29:54
	 * @param costItemSpcode
	 */
	public void deleteSpcode(CostItemSpcode costItemSpcode);
	
	/**
	 * 描述：反结算删除核减销售表中的数据
	 * @author 马振
	 * 创建时间：2015-1-1 下午5:30:29
	 * @param costItemSpcode
	 */
	public void deleteCostItem(CostItemSpcode costItemSpcode);

	/**
	 * 获取菜品销售情况统计
	 * @author 文清泉
	 * @param 2015年4月28日 下午3:19:23
	 * @param vcode
	 * @param bdat
	 * @param edat
	 * @return
	 */
	public List<java.util.Map<String, Object>> findDatSubtract(CostItem costItem);
	
	/**
	 * 获取菜品销售情况统计 中餐
	 * @author wangkai
	 * @param 2016-1-27
	 * @return
	 */
	public List<java.util.Map<String, Object>> findDatSubtract_cn(CostItem costItem);
	
	/**
	 * 获取菜品销售情况统计 老中餐
	 * @return
	 */
	public List<java.util.Map<String, Object>> findDatSubtract_tele(CostItem costItem);

	/**
	 * 保存菜品销售数据
	 * @author 文清泉
	 * @param 2015年4月28日 下午3:19:47
	 * @param list
	 */
	public void saveCostItem(List<CostItem> list);

	/**
	 * 核减物资使用率
	 * @author 文清泉
	 * @param 2015年4月28日 下午3:37:46
	 * @param list
	 */
	public void excutecostcut(CostItem costItem);

	/***
	 * 得到下一个序列值sqlserver版用
	 * @return
	 */
	public Integer getNextCostItemRec();

	/***
	 * 核减之前判断是否已核减
	 * @param costItemSpcode
	 * @return
	 */
	public List<CostItem> listCostReductionGroupByDate(CostItem costItem);
	
	/**
	 * 根据门店编码查询门店类型
	 * add by wangkai 2016-1-27 
	 * @return
	 */
	public String findVfoodSignByVcode(Positn positn);

	/***
	 * 反核减
	 * @param cost
	 */
	public void excutecostcut_re(CostItem cost);

	/***
	 * sqlserver保存核减主表
	 * @param cost
	 */
	public void saveCostItem(CostItem cost);

	/***
	 * 根据门店得到门店下的档口关系
	 * @param positn
	 * @return
	 */
	public List<Positn> findPositnfirmByPositn(CostItem cost);

}
