package com.choice.misboh.persistence;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.misboh.domain.declareGoodsGuide.Declare;

/**
 * 连接数据库接口层   调用XML文件SQL
 * @author 文清泉
 * @param 2015-4-2 上午9:35:15
 */
public interface DeclareGoodsGuideMapper {

	/**
	 * 获取报货类别
	 * @author 文清泉
	 * @param 2015年4月8日 下午1:43:15
	 * @param dat
	 * @param positnCode
	 * @param isuseschedule 
	 * @return
	 */
    public List<Map<String,Object>> getCategoryList(@Param(value="dat")String dat, @Param(value="positnCode")String positnCode,@Param(value="typ")String typ);

    
	/**
	 * 获取报货类别  - 非配送班表
	 * @author 文清泉
	 * @param 2015年1月1日 下午1:44:45
	 * @return
	 */
	public List<Map<String,Object>> getCategoryNoList(@Param(value="typ")String typ);
	
    /**
     * 获取预估营业额
     * @author 文清泉
     * @param 2015年4月8日 下午1:43:25
     * @param string
     * @param useMaxEndDate
     * @param useMaxEndDate2 
     * @return
     */
	public List<Map<String, Object>> listAdjustTheEstimated(@Param(value="code")String code,
			@Param(value="dat")String dat, @Param(value="useMaxEndDate")String useMaxEndDate);

	
	/**
	 * 获取参考计算周期实际营业额
	 * @author 文清泉
	 * @param 2015年4月9日 下午1:31:48
	 * @param bdat 开始时间
	 * @param edat 结束时间
	 * @param dept 部门
	 * @param scode 店铺编码
	 * @return
	 */
	public List<Map<String, Object>> getCalculationMoney(@Param(value="bdat")String bdat,
			@Param(value="edat")String edat, @Param(value="dept")String dept, @Param(value="scode")String scode);

	/**
	 * 获取参考计算物资  实际用量
	 * @author 文清泉
	 * @param 2015年4月9日 下午1:31:48
	 * @param bdat 开始时间
	 * @param edat 结束时间
	 * @param dept 部门
	 * @param scode 店铺编码
	 * @param category_code 报货类别
	 * @return
	 */
	public List<Map<String, Object>> getCalculationSpcode(@Param(value="bdat")String bdat,
			@Param(value="edat")String edat, @Param(value="dept")String dept, @Param(value="scode")String scode, @Param(value="category_code")String category_code);


	/**
	 * 获取物资对应库存
	 * @author 文清泉
	 * @param 2015年1月1日 下午4:47:12
	 * @param scode
	 * @param dept
	 * @return
	 */
	public List<Map<String, Object>> findSupplyTcurstore(@Param(value="scode")String scode, @Param(value="dept")String dept,@Param(value="yearr")String yearr);

	/**
	 * 菜品销售计划与点击率物资用量计算
	 * @author 文清泉
	 * @param 2015年4月25日 下午3:42:34
	 * @param category_code 报货类别
	 * @param bdat 开始时间
	 * @param edat 结束时间
	 * @param dept 部门
	 * @param scode 店铺编码
	 * @return
	 */
	public List<Map<String, Object>> calculationFoodSale(@Param(value="category_code")String category_code,
			@Param(value="bdat")String bdat, @Param(value="edat")String edat, @Param(value="dept")String dept, @Param(value="scode")String scode);

	/**
	 * 安全库存 计算用量
	 * @author 文清泉
	 * @param 2015年4月25日 下午3:42:34
	 * @param category_code 报货类别
	 * @param dept 部门
	 * @param scode 店铺编码
	 * @return
	 */
	public List<Declare> getSafeStockMap(@Param(value="category_code")String category_code,@Param(value="dept")String dept, @Param(value="scode")String scode, @Param(value="yearr")String yearr);


	/**
	 * 周次平均 --- 平均三周
	 * @author 文清泉
	 * @param 2015年6月9日 下午6:13:15
	 * @param category_code 报货类别
	 * @param dept 部门
	 * @param scode 门店编码 
	 * @param yearr 年份
	 * @param dat 当前订货日
	 * @param bdat 订货日前三周日期
	 * @return
	 */
	public List<Map<String, Object>> getAverageWeeklyMap(@Param(value="category_code")String category_code,@Param(value="dept")String dept, @Param(value="scode")String scode, 
			@Param(value="yearr")String yearr, @Param(value="bdat")String bdat, @Param(value="dat")String dat);


	/**
	 * 获取已报货单据数
	 * @author 文清泉
	 * @param 2015年8月13日 上午11:20:07
	 * @param bdat 报货日期
	 * @param code 店铺编码
	 * @return
	 */
	public List<Map<String, Object>> getcheckChkstomList(@Param(value="dat")String bdat,@Param(value="scode")String code,@Param(value="typ")String typ);
}
