package com.choice.misboh.persistence.positnSpcode;

import java.util.List;

import com.choice.scm.domain.PositnSpcode;

public interface PositnSpcodeMisMapper {
	/**
	 * 根据分店编码查询分店物资
	 * @param positnSpcode
	 * @return
	 */
	public List<PositnSpcode> findPositnSpcode(PositnSpcode positnSpcode);
	
	/**
	 * 根据分店编码查询所有物资 九毛九用
	 * @param positnSpcode
	 * @return
	 */
	public List<PositnSpcode> findPositnSpcodeJmj(PositnSpcode positnSpcode);


}
