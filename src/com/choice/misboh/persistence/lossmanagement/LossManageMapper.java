package com.choice.misboh.persistence.lossmanagement;

import java.util.List;

import com.choice.framework.exception.CRUDException;
import com.choice.misboh.domain.BaseRecord.PubItem;
import com.choice.misboh.domain.costReduction.Costdtl;
import com.choice.misboh.domain.lossmanage.PostionLoss;
import com.choice.misboh.domain.util.ConditionMis;
import com.choice.scm.domain.Chkoutm;
import com.choice.scm.domain.Spcodeexm;
import com.choice.scm.domain.SppriceSale;
import com.choice.scm.domain.Supply;

public interface LossManageMapper {


	/**
	 * 根据营业日期和部门查询损耗单据
	 * @param map
	 * @return
	 * @throws CRUDException
	 */
	public List<PostionLoss> listLossManage(PostionLoss postionLoss);
	
	/**
	 * 自动检索菜品
	 * @param pubitem
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public List<PubItem> queryPubItemmTop10(ConditionMis conditionMis);
	
	/**
	 * 保存损耗单
	 * @param session
	 * @param listPostionLoss
	 * @return
	 * @throws Exception
	 */
	public void saveLossManage(PostionLoss postionLoss);
	
	/**
	 * 删除损耗单
	 * @param session
	 * @param listPostionLoss
	 * @return
	 * @throws Exception
	 */
	public void deleteLossManage(PostionLoss postionLoss);
	
	/**
	 * 根据菜品编码查询BOM
	 * @return
	 */
	public List<Costdtl> queryCostItemByCode(PostionLoss postionLoss);
		
	/**
	 * 查询半成品额BOM
	 * @return
	 */
	public List<Spcodeexm> querySpcodeexmByCode(PostionLoss postionLoss);
	
	/**
	 * 查询物资价格(没有售价就去物资标准价)
	 * @param sppricesale
	 * @return
	 */
	public Supply queryPriceSale(SppriceSale sppricesale);
	
	/**
	 * 损耗生成出库单后修改出库单的价格
	 * @param chkoutm
	 */
	public void updateChkoutm(Chkoutm chkoutm);
	
	/**
	 * 更新确认状态
	 * @param postionLoss
	 */
	public void updatePostionLoss(PostionLoss postionLoss);
	
	/**
	 * 自动检索物资
	 * @param supply
	 * @param session
	 * @param dept
	 * @return
	 * @throws Exception
	 */
	public List<Supply> findSupplyListTopN(ConditionMis conditionMis);
	
	/**
	 * 自动检索半成品
	 * @param supply
	 * @param session
	 * @param dept
	 * @return
	 * @throws Exception
	 */
	public List<Supply> findSupplyListTopNBCP(ConditionMis conditionMis);
	
}
