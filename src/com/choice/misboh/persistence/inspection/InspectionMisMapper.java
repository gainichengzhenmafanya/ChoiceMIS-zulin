package com.choice.misboh.persistence.inspection;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.choice.misboh.domain.inspection.ArrivaldMis;
import com.choice.misboh.domain.inspection.ArrivalmMis;
import com.choice.scm.domain.Chkind;
import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyAcct;


public interface InspectionMisMapper {
	
	/**
	 * 报货分拨 报货验收 总数
	 */
	public int findAllDisCount(Dis dis);

	/**
	 * 报货分拨、报货验收
	 */
	public List<Dis> findAllDis(Dis dis);
	
	/**
	 * 报货分拨、报货验收 分页查询
	 */
	public List<Dis> findAllDisPage(Dis dis);
	
	/***
	 * 是否有多档口报货来的数据
	 * @param ids
	 * @return
	 */
	public int findDeptChkstom(@Param(value="ids")String ids);

	/**
	 * 批量修改      验收入库      验收出库    采购确认   采购审核  的标志字段      的统一 方法
	 */
	public void updateByIds(Dis dis);
	
	/**
	 * 修改
	 */
	public void updateDis(Dis dis);
	
	/**
	 * 查询出入库状态
	 */
	public List<Chkstod> findChkByKey(String id);
	
	/**
	 * 获得收货单数据（分店接受配送中心送货用）
	 */
	public List<SupplyAcct> selectByKey(HashMap<String,Object> disMap);
	
	/**
	 * 更新验货信息
	 * @param supplyAcct
	 */
	public void updateAcct(SupplyAcct supplyAcct);
	
	/**
	 * 更新验货审核信息
	 * @param disMap
	 */
	public void check(HashMap<String, Object> disMap);
	
	/**
	 * 获取入库单数据
	 */
	public List<Dis> findSupplyacctList(Dis dis);
	
	/**
	 * 更新验货信息
	 * @param disMap
	 */
	public void updateYnDept(HashMap<String, Object> disMap);
	
	/***
	 * 
	 * @param dis
	 * @return
	 */
	public List<Dis> findSupplyacctDeptList(Dis dis);

	/***
	 * 当前分店所属片区的主直拨库对应的供应商
	 * @param positn
	 * @return
	 */
	public Deliver findTpDeliver(Positn positn);

	/***
	 * 直配验货查询多档口
	 * @param dis
	 * @return
	 */
	public List<Dis> findChkstodList(Dis dis);

	/***
	 * 更新chkstod_dept chkin 字段为已验货
	 * @param sp_ids
	 */
	public void updateChkstodDeptChkin(@Param(value="sp_ids")String sp_ids);

	/***
	 * 查询调拨出库的门店对应的供应商
	 * @param positn
	 * @return
	 */
	public Deliver findDeliverByPositn(Positn positn);

	/***
	 * 修改报货单为已验货，安全库存报货用
	 * @param chkind
	 */
	public void updateChkstodByChkstonoSpcode(Chkind chkind);

	/***
	 * 根据档口报货单从表更新报货单状态为已验货
	 * @param sp_ids
	 */
	public void updateChkstodByChkstodDeptId(@Param(value="sp_ids")String sp_ids);

	/***
	 * 查询九毛九直配到货单
	 * @param arrivalmMis
	 * @return
	 */
	public List<ArrivalmMis> findArrivalmList(ArrivalmMis arrivalmMis);

	/***
	 * 根据到货单主表 查询到货单从表
	 * @param arrivalmMis
	 * @return
	 */
	public List<ArrivaldMis> findArrivaldList(ArrivalmMis arrivalmMis);

	/***
	 * 修改九毛九直配验货数量
	 * @param ad
	 */
	public void updateArrivald(ArrivaldMis ad);

	/***
	 * 修改到货单状态为已验货
	 * @param arrivalmMis
	 */
	public void updateArrivalm(ArrivalmMis arrivalmMis);

	/***
	 * 修改报货单从表状态为已验货
	 * @param arrivalmMis
	 */
	public void updateChkstodByArrivalm(ArrivalmMis arrivalmMis);
	
	/***
	 * 九毛九统配验货根据单据
	 * @param sa
	 * @return
	 */
	public List<SupplyAcct> listSupplyAcctTP(SupplyAcct sa);

	/***
	 * 判断档口报货发来的物资是否在档口报货单里存在
	 * @param disMap
	 * @return
	 */
	public int checkChkstomDeptSpcode(HashMap<String, Object> disMap);

	/***
	 * 调拨验货查询
	 * @param disMap
	 * @return
	 */
	public List<SupplyAcct> selectByKeyDb(HashMap<String, Object> disMap);

	/***
	 * 审核失败将状态更为未审核
	 * @param disMap
	 */
	public void uncheck(HashMap<String, Object> disMap);

	/***
	 * 查询是否已验货
	 * @param disMap
	 * @return
	 */
	public int findOutCountByIds(HashMap<String, Object> disMap);
}
