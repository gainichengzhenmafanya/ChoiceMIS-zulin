package com.choice.misboh.persistence.operationManagement;

import java.util.List;

import com.choice.misboh.domain.OperationManagement.CashMx;
import com.choice.misboh.domain.OperationManagement.CashSaving;
import com.choice.misboh.domain.OperationManagement.OutLay;
import com.choice.misboh.domain.OperationManagement.SpecialEvent;

/**
 * 描述：运营管理：营业外收入、坐支明细、运营情况登记、存款管理
 * @author 马振
 * 创建时间：2015-5-20 上午10:38:49
 */
public interface OperationManagementMapper {
	
	/**
	 * 描述：查询坐支明细
	 * @author 马振
	 * 创建时间：2015-5-18 上午10:25:56
	 * @return
	 */
	public List<OutLay> findOutLay(OutLay outLay);
	
	/**
	 * 描述：查询坐支明细当前数据
	 * @author 马振
	 * 创建时间：2015-5-20 上午11:06:33
	 * @param outLay
	 * @return
	 */
	public List<OutLay> findOutLayNow(OutLay outLay);
	
	/**
	 * 描述：查询当前门店同一天内的坐支明细总金额
	 * @author 马振
	 * 创建时间：2015-5-18 下午3:36:35
	 * @param outLay
	 * @return
	 */
	public OutLay findPrice(OutLay outLay);
	
	/**
	 * 描述：查询营业外收入历史数据
	 * @author 马振
	 * 创建时间：2015-5-18 上午10:25:56
	 * @return
	 */
	public List<OutLay> findIncome(OutLay outLay);
	
	/**
	 * 描述：查询营业外收入当前数据
	 * @author 马振
	 * 创建时间：2015-5-20 上午11:01:26
	 * @param outLay
	 * @return
	 */
	public List<OutLay> findIncomeNow(OutLay outLay);
	
	/**
	 * 描述：查询当前门店同一天内的营业外收入总金额和总数量
	 * @author 马振
	 * 创建时间：2015-5-18 下午3:36:35
	 * @param outLay
	 * @return
	 */
	public OutLay findPriceAndQuantity(OutLay outLay);
	
	/**
	 * 描述：删除坐支金额
	 * @author 马振
	 * 创建时间：2015-5-18 上午11:32:06
	 */
	public void deleteCashMx(CashMx cashMx);
	
	/**
	 * 描述：保存坐支明细
	 * @author 马振
	 * 创建时间：2015-5-18 上午11:20:41
	 * @param cashMx
	 */
	public void saveCashMx(CashMx cashMx);
	
	/**
	 * 描述：特殊事件
	 * @author 马振
	 * 创建时间：2015-5-20 下午6:38:28
	 * @param specialEvent
	 * @return
	 */
	public List<SpecialEvent> listSpecialEvent(SpecialEvent specialEvent);
	
	/**
	 * 描述：查询特殊事件主表
	 * @author 马振
	 * 创建时间：2015-5-20 下午1:33:30
	 * @param specialItem
	 * @return
	 */
	public List<SpecialEvent> listSpecialItem();
	
	/**
	 * 描述：根据主表主键查询子表信息
	 * @author 马振
	 * 创建时间：2015-5-20 下午7:18:11
	 * @param specialEvent
	 * @return
	 */
	public List<SpecialEvent> listSelectItemDtl(SpecialEvent specialEvent);
	
	/**
	 * 描述：删除已存在的数据
	 * @author 马振
	 * 创建时间：2015-5-20 下午1:45:16
	 * @param specialEvent
	 */
	public void deleteSpecialEvent(SpecialEvent specialEvent);
	
	/**
	 * 描述：保存修改的特殊事件子表
	 * @author 马振
	 * 创建时间：2015-5-20 下午4:05:21
	 * @param specialItem
	 */
	public void saveSpecialEvent(SpecialEvent specialEvent);
	
	/**
	 * 描述：查询存款
	 * @author 马振
	 * 创建时间：2015-5-20 下午3:16:55
	 * @param cashSaving
	 * @return
	 */
	public List<CashSaving> findCashSaving(CashSaving cashSaving);
	
	/**
	 * 描述：按日期查询存款总金额
	 * @author 马振
	 * 创建时间：2015-5-26 上午9:33:36
	 * @param cashSaving
	 * @return
	 */
	public CashSaving findAmountCount(CashSaving cashSaving);
	
	/**
	 * 描述：查询支付类型为币种的支付方式
	 * @author 马振
	 * 创建时间：2015-5-20 下午3:28:58
	 * @return
	 */
	public List<CashSaving> listPayMode();
	
	/**
	 * 描述：查询账户类型
	 * @author 马振
	 * 创建时间：2015-5-20 下午5:20:09
	 * @return
	 */
	public List<CashSaving> listAccountType();
	
	/**
	 * 描述：根据主键查询存款
	 * @author 马振
	 * 创建时间：2015-5-20 下午4:33:14
	 * @param cashSaving
	 * @return
	 */
	public CashSaving getCashSavingByPk(CashSaving cashSaving);
	
	/**
	 * 描述：新增存款
	 * @author 马振
	 * 创建时间：2015-5-20 下午4:32:35
	 * @param cashSaving
	 */
	public void addCashSaving(CashSaving cashSaving);
	
	/**
	 * 描述：修改存款
	 * @author 马振
	 * 创建时间：2015-5-20 下午5:36:06
	 * @param cashSaving
	 */
	public void updateCashSaving(CashSaving cashSaving);
	
	/**
	 * 描述： 根据营业日，获取当前营业日当前门店所有的存款金额
	 * @author 马振
	 * 创建时间：2016年3月24日下午3:09:15
	 * @param cashSaving
	 * @return
	 */
	public CashSaving findAmount(CashSaving cashSaving);
	
	/**
	 * 描述： 最大回单日期记录
	 * @author ygb
	 * @param cashSaving
	 * @return
	 */
	public CashSaving findSavingDate(CashSaving cashSaving);
	
	/**
	 * 描述：  回单流水号是否存在
	 * @author ygb
	 * @param cashSaving
	 * @return
	 */
	public List<CashSaving> findSavingNo(CashSaving cashSaving);
	
	/**
	 * 描述：删除数据
	 * @author 马振
	 * 创建时间：2015-5-20 下午6:08:50
	 * @param cashSaving
	 */
	public void deleteCashSaving(CashSaving cashSaving);
}