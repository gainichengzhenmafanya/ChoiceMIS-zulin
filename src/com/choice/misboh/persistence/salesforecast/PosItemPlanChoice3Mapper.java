package com.choice.misboh.persistence.salesforecast;

import java.util.List;

import com.choice.misboh.domain.salesforecast.ItemPlan;

public interface PosItemPlanChoice3Mapper {

	  
	  
	
	/**
	 *  查找预估菜品销售计划
	 * @return
	 */
	public List<ItemPlan> findPosItemPlan(ItemPlan itemPlan);
	
	/**
	 * 保存修改调整量(预估菜品销售计划)
	 * @param itemPlan
	 * @return
	 */
	public void updateItemPlan(ItemPlan itemPlan);
	
	/**
	 * 删除所有菜品点击率
	 * @param 
	 * @return
	 */
	public void deleteAllPlan(ItemPlan itemPlan);
	
	/**
	 * 添加菜品营业销售计划
	 * @param map
	 * @return
	 */
	public void insertPosItemPlan(ItemPlan itemPlan);
	
	/***
	 * 批量插入
	 * @param list
	 */
	public void insertPosItemPlanBatch(List<ItemPlan> list);
}
