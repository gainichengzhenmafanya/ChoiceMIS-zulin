package com.choice.misboh.persistence.salesforecast;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.choice.misboh.domain.salesforecast.FirmItemUse;

public interface ItemUseChoice3Mapper {

	/**
	 *  查找所有的菜品点击率
	 * @return
	 */
	public List<FirmItemUse> findAllItemUse(FirmItemUse firmItemUse);
	
	/**
	 * 获取点击率表达方式（单数，营业额，人数）
	 * @param acct
	 * @return
	 */
	public String getPlanTyp(@Param("acct")String acct);
	
	/**
	 * 把这道菜的点击率统计信息添加到表
	 * @param firmItemUse
	 * @return
	 */
	public void insertItemUse(FirmItemUse firmItemUse);
	
	/**
	 * 删除所有菜品点击率
	 * @param 
	 * @return
	 */
	public void deleteAll(FirmItemUse firmItemUse);
	
	/**
	 * 获取各班次（人数，营业额，单数）的总数  choice3
	 * @param firmItemUse
	 * @return
	 */
	public List<FirmItemUse> findAllPaxAmtTc(FirmItemUse firmItemUse);
	
	/**
	 * 获取各班次某一段时的菜品销售  choice3
	 * @param firmItemUse
	 * @return
	 */
	public List<FirmItemUse> findAllItem(FirmItemUse firmItemUse);
	
	/**
	 * 保存修改调整量(菜品点击率)
	 * @param firmItemUse
	 * @return
	 */
	public void update(FirmItemUse firmItemUse);

	/***
	 * 删除指定的菜品点击率
	 * @param itemUse
	 */
	public void delete(FirmItemUse itemUse);

	/***
	 * 批量插入
	 * @param list
	 */
	public void insertItemUseBatch(List<FirmItemUse> list);
	
}
