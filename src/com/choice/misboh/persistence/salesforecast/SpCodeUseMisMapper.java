package com.choice.misboh.persistence.salesforecast;

import java.util.List;

import com.choice.misboh.domain.salesforecast.SpCodeUseMis;

public interface SpCodeUseMisMapper {

	/**
	 *  查找千人万元用量预估
	 * @return
	 */
	public List<SpCodeUseMis> listSpCodeUse(SpCodeUseMis spCodeUse);
	
	/**
	 *  查找千人万元用量预估
	 * @return
	 */
	public SpCodeUseMis getAmt(SpCodeUseMis spCodeUse);
	public SpCodeUseMis getAmt_pos(SpCodeUseMis spCodeUse);
	public SpCodeUseMis getAmt_boh(SpCodeUseMis spCodeUse);
	
	/**
	 *  查找预估物资的使用量信息===多部门
	 * @return
	 */
	public List<SpCodeUseMis> listSupplyAcctDept(SpCodeUseMis spCodeUse);
	
	/**
	 *  查找预估物资的使用量信息
	 * @return
	 */
	public List<SpCodeUseMis> listSupplyAcct(SpCodeUseMis spCodeUse);
	
	/**
	 * 把这道菜的点击率统计信息添加到表
	 * @param spCodeUse
	 * @return
	 */
	public void insertSpCodeUse(SpCodeUseMis spCodeUse);
	
	/**
	 * 删除物资用量预估
	 * @param 
	 * @return
	 */
	public void deleteAll(SpCodeUseMis spCodeUse);	
	
	/***
	 * choice3查询营业数据
	 * @param spCodeUse
	 * @return
	 */
	public SpCodeUseMis getAmtChoice3(SpCodeUseMis spCodeUse);

	/***
	 * 修改千人用量
	 * @param spCodeUseMis
	 */
	public int updateSpCodeUse(SpCodeUseMis spCodeUseMis);

	/***
	 * 直接插入所有
	 * @param spCodeUseList
	 */
	public void insertAll(List<SpCodeUseMis> spCodeUseList);
	
}
