package com.choice.misboh.persistence.salesforecast;

import java.util.List;

import com.choice.misboh.domain.salesforecast.ItemPlan;

public interface PosItemPlanMapper {

	/**
	 *  查找预估菜品销售计划
	 * @return
	 */
	public List<ItemPlan> findPosItemPlan(ItemPlan posItemPlan);

	/**
	 * 保存菜品销售计划
	 * @param itemPlan
	 */
	public void insertItemPlan(ItemPlan itemPlan);

	/**
	 * 删除单条销售计划
	 * @param itemPlan
	 */
	public void deleteItemPlan(ItemPlan itemPlan);

	/**
	 * 保存修改调整量(预估菜品销售计划)
	 * @param itemPlan
	 * @return
	 */
	public void updateItemPlan(ItemPlan itemPlan);

}
