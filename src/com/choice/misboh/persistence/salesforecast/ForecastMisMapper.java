package com.choice.misboh.persistence.salesforecast;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.framework.exception.CRUDException;
import com.choice.misboh.domain.BaseRecord.PubItem;
import com.choice.misboh.domain.salesforecast.FirmItemUse;
import com.choice.misboh.domain.salesforecast.ItemPlan;
import com.choice.misboh.domain.salesforecast.SalePlan;
import com.choice.scm.domain.PosItemPlan;

public interface ForecastMisMapper {

	/**
	 *  查找所有的菜品点击率
	 * @return
	 */
	public List<FirmItemUse> findAllItemUse(FirmItemUse itemUse);
	
	/**
	 * 获取点击率表达方式（单数，营业额，人数）
	 * @param acct
	 * @return
	 */
	public String getPlanTyp(@Param("acct")String acct);
	
	/**
	 * 把这道菜的点击率统计信息添加到表
	 * @param itemUse
	 * @return
	 */
	public void insertItemUse(FirmItemUse itemUse);
	
	/**
	 * 删除点击率
	 * @param itemUse
	 */
	public void deleteFirmItemUse(FirmItemUse itemUse);
	
	/**
	 * 删除所有菜品点击率
	 * @param 
	 * @return
	 */
	public void deleteAll(FirmItemUse itemUse);
	
	/**
	 * 保存修改调整量(菜品点击率)
	 * @param itemUse
	 * @return
	 */
	public void update(FirmItemUse itemUse);
	
    /**
     * 查询当前店或者部门下的菜品	
     * @param firmId
     * @return
     * @throws CRUDException
     */
    public List<PubItem> addNewItem(@Param("firmId")String firmId);
	
	/**
	 *  查找营业预估
	 * @return
	 */
	public List<SalePlan> findPosSalePlan(SalePlan posSalePlan);
	
	/**
	 * 删除某日期营业预估
	 * @param posSalePlan
	 * @return
	 */
	public void deletePosSalePlan(SalePlan posSalePlan);
	
	/**
	 * 添加某日期营业预估
	 * @param posSalePlan
	 * @return
	 */
	public void insertPosSalePlan(SalePlan posSalePlan);
	
	/**
	 * 保存修改调整量(营业预估)
	 * @param posSalePlan
	 * @return
	 */
	public void updateSalePlan(SalePlan posSalePlan);
	
	/**
	 *  查找预估菜品销售计划
	 * @return
	 */
	public List<ItemPlan> findPosItemPlan(ItemPlan posItemPlan);
	
	/**
	 * 保存菜品销售计划
	 * @param itemPlan
	 */
	public void insertItemPlan(ItemPlan itemPlan);
	
	/**
	 * 删除单条销售计划
	 * @param itemPlan
	 */
	public void deleteItemPlan(ItemPlan itemPlan);
	
	/**
	 *  查找菜品销售计划天数
	 * @return
	 */
	public List<ItemPlan> findPosItemPlanForDays(ItemPlan posItemPlan);
	
	/**
	 * 保存修改调整量(预估菜品销售计划)
	 * @param itemPlan
	 * @return
	 */
	public void updateItemPlan(ItemPlan itemPlan);
	
	/**
	 * 删除所有菜品点击率
	 * @param 
	 * @return
	 */
	public void deleteAllPlan(ItemPlan posItemPlan);
	
	/**
	 * 添加菜品营业销售计划
	 * @param map
	 * @return
	 */
	public void insertPosItemPlan(PosItemPlan posItemPlan);
	
	/**
	 *  查找菜品营业销售计划
	 * @return
	 */
	public List<SalePlan> findPosSalePlan1(SalePlan posSalePlan);
	
	/**
	 *  查找一段时间内的预估菜品销售计划
	 * @return
	 */
	public List<PosItemPlan> findPosItemPlan1(PosItemPlan posItemPlan);
	
	/**
	 *  查找某日某班次的菜品预估信息
	 * @return
	 */
	public List<PosItemPlan> findPosItemPlan2(PosItemPlan posItemPlan);

	/**
	 * 获取千次点击率  每日销售总和
	 * @author 文清泉
	 * @param 2015年5月27日 上午11:00:53
	 * @param firmId 门店ID
	 * @param bdate 开始
	 * @param edate 结束
	 * @return
	 */
	public List<Map<String, Object>> getCalculateItemuseTotal(@Param(value="firmId")String firmId, @Param(value="bdate")String bdate, @Param(value="edate")String edate);
	public List<Map<String, Object>> getCalculateItemuseTotal3(@Param(value="firmId")String firmId, @Param(value="bdate")String bdate, @Param(value="edate")String edate);

	/**
	 * 获取千次点击率  菜品周次销售情况
	 * @author 文清泉
	 * @param 2015年5月27日 上午11:17:49
	 * @param firmId 门店ID
	 * @param bdate 开始
	 * @param edate 结束
	 * @return
	 */
	public List<Map<String, Object>> getCalculateItemuse(@Param(value="firmId")String firmId, @Param(value="bdate")String bdate, @Param(value="edate")String edate);
	public List<Map<String, Object>> getCalculateItemuse3(@Param(value="firmId")String firmId, @Param(value="bdate")String bdate, @Param(value="edate")String edate);

	/**
	 * 获取千次典籍里  菜品情况
	 * @author 文清泉
	 * @param 2015年5月27日 上午11:42:39
	 * @param firmId
	 * @param bdate
	 * @param edate
	 * @return
	 */
	public List<Map<String, Object>> getCalculatePcode(@Param(value="firmId")String firmId, @Param(value="bdate")String bdate, @Param(value="edate")String edate);
	public List<Map<String, Object>> getCalculatePcode3(@Param(value="firmId")String firmId, @Param(value="bdate")String bdate, @Param(value="edate")String edate);

	/***
	 * 根据店铺编码和菜品编码查询菜品所属的部门
	 * @param firmId
	 * @return
	 */
	public List<Map<String, String>> getDeptByPubitemCode(@Param(value="firmId")String firmId);
}
