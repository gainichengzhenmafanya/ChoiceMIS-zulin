package com.choice.misboh.persistence.salesforecast;

import java.util.List;

import com.choice.misboh.domain.salesforecast.SalePlan;

public interface PosSalePlanChoice3Mapper {
	
	/**
	 *  查找营业预估
	 * @return
	 */
	public List<SalePlan> findPosSalePlan(SalePlan salePlan);
	
	/**
	 * 删除某日期营业预估
	 * @param posSalePlan
	 * @return
	 */
	public void deletePosSalePlan(SalePlan salePlan);
	
	/**
	 * 添加某日期营业预估
	 * @param posSalePlan
	 * @return
	 */
	public void insertPosSalePlan(SalePlan salePlan);
	
	/**
	 * 保存修改调整量(营业预估)
	 * @param posSalePlan
	 * @return
	 */
	public void updateSalePlan(SalePlan salePlan);
	
	/***
	 * 查找营业数据choice3
	 * @param salePlan
	 * @return
	 */
	public List<SalePlan> findPosSaleListChoice3(SalePlan salePlan);
}
