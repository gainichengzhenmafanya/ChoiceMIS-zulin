package com.choice.misboh.persistence.salesforecast;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.framework.exception.CRUDException;
import com.choice.misboh.domain.BaseRecord.PubItem;
import com.choice.misboh.domain.salesforecast.FirmItemUse;

public interface ItemUseMapper {

	/**
	 *  查找所有的菜品点击率
	 * @return
	 */
	public List<FirmItemUse> findAllItemUse(FirmItemUse itemUse);

	/**
	 * 把这道菜的点击率统计信息添加到表
	 * @param itemUse
	 * @return
	 */
	public void insertItemUse(FirmItemUse itemUse);

	/**
	 * 删除所有菜品点击率
	 * @param
	 * @return
	 */
	public void deleteAll(FirmItemUse itemUse);

	/**
	 * 保存修改调整量(菜品点击率)
	 * @param itemUse
	 * @return
	 */
	public void update(FirmItemUse itemUse);

    /**
     * 查询当前店或者部门下的菜品
     * @param firmId
     * @return
     * @throws CRUDException
     */
    public List<PubItem> addNewItem(@Param("firmId") String firmId);

	/**
	 * 获取千次点击率  每日销售总和
	 * @author 文清泉 2015年5月27日 上午11:00:53
	 * @param firmId 门店ID
	 * @param bdate 开始
	 * @param edate 结束
	 * @return
	 */
	public List<Map<String, Object>> getCalculateItemuseTotal(@Param(value = "firmId") String firmId, @Param(value = "bdate") String bdate, @Param(value = "edate") String edate);

	/**
	 * 获取千次点击率  菜品周次销售情况
	 * @author 文清泉 2015年5月27日 上午11:17:49
	 * @param firmId 门店ID
	 * @param bdate 开始
	 * @param edate 结束
	 * @return
	 */
	public List<Map<String, Object>> getCalculateItemuse(@Param(value = "firmId") String firmId, @Param(value = "bdate") String bdate, @Param(value = "edate") String edate);

	/**
	 * 获取千次典籍里  菜品情况
	 * @author 文清泉 2015年5月27日 上午11:42:39
	 * @param firmId
	 * @param bdate
	 * @param edate
	 * @return
	 */
	public List<Map<String, Object>> getCalculatePcode(@Param(value = "firmId") String firmId, @Param(value = "bdate") String bdate, @Param(value = "edate") String edate);

}
