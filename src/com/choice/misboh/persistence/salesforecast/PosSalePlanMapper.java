package com.choice.misboh.persistence.salesforecast;

import java.util.List;
import java.util.Map;

import com.choice.misboh.domain.salesforecast.SalePlan;

public interface PosSalePlanMapper {

	/**
	 *  查找营业预估
	 * @return
	 */
	public List<SalePlan> findPosSalePlan(SalePlan posSalePlan);
	
	/**
	 * 删除某日期营业预估
	 * @param posSalePlan
	 * @return
	 */
	public void deletePosSalePlan(SalePlan posSalePlan);
	
	/**
	 * 添加某日期营业预估
	 * @param posSalePlan
	 * @return
	 */
	public void insertPosSalePlan(SalePlan posSalePlan);
	
	/**
	 * 保存修改调整量(营业预估)
	 * @param posSalePlan
	 * @return
	 */
	public void updateSalePlan(SalePlan posSalePlan);
	
	/***
	 * 营业预估新方法 查询营业数据  
	 * @author wjf 20170730
	 * @param salePlan
	 * @return
	 */
	public List<SalePlan> findBusinessData(SalePlan salePlan);

	/***
	 * 得到预估日期，节假日等数据
	 * @param mapList
	 * @return
	 */
	public List<SalePlan> getCalPosSalePlan(List<Map<String,String>> mapList);

}
