package com.choice.misboh.persistence.myDesk;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.choice.framework.domain.system.Module;
import com.choice.framework.domain.system.Role;
import com.choice.misboh.domain.BaseRecord.OtherStockDataO;
import com.choice.misboh.domain.inventory.Chkstoref;
import com.choice.misboh.domain.lossmanage.PostionLoss;
import com.choice.misboh.domain.myDesk.GuideConfig;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.ScheduleD;
import com.choice.scm.domain.SupplyAcct;

public interface MyDeskMisbohMapper {
	/***
	 * 根据门店和日期查询配送班表
	 * @param scheduleD
	 * @return
	 */
	public List<ScheduleD> findSchedulesByPositn(ScheduleD scheduleD);

	/***
	 * 查询直配需要验货的数量 根据供应商分组显示
	 * @param dis
	 * @return
	 */
	public List<Dis> findDireCountGroupbyDeliver(Dis dis);

	/***
	 * 查询统配需要验货的总数量
	 * @param dis
	 * @return
	 */
	public List<SupplyAcct> findOutCount(Dis dis);

	/***
	 * 查询调拨需要验货的总数 根据门店来查
	 * @param dis
	 * @return
	 */
	public List<SupplyAcct> findDbCountGroupByPositn(Dis dis);

	/***
	 * 查询我的桌面配置
	 * @return
	 */
	public List<GuideConfig> findGuideConfig();

	/***
	 * 查询直配今日未验货
	 * @return
	 */
	public int findDireCount(Dis dis);
	
	/***
	 * 查询统配今日未验货
	 * @param dis
	 * @return
	 */
	public int findOutCount1(Dis dis);

	/***
	 * 查询调拨今日未验货
	 * @param dis
	 * @return
	 */
	public int findDbCount(Dis dis);

	/***
	 * 查询今日有没有做成本核减
	 * @param dis
	 * @return
	 */
	public int findCostitemspcodeCount(Dis dis);

	/***
	 * 查询今日有没有录入水电气
	 * @param oo
	 * @return
	 */
	public List<OtherStockDataO> findOtherStockdataCount(OtherStockDataO oo);

	/***
	 * 查询今日有没有做损耗
	 * @param pl
	 * @return
	 */
	public int findPostionLossCount(PostionLoss pl);

	/***
	 * 查询今日是否已盘点
	 * @param cf
	 * @return
	 */
	public int findChkstorefCount(Chkstoref cf);

	/***
	 * 判断今日有没有报货
	 * @param dis
	 * @return
	 */
	public int findChkstomCount(Dis dis);

	/***
	 * 查询positn表 营业日
	 * @param positnCode
	 * @return
	 */
	public Date findWorkdateByPositn(@Param(value="positnCode")String positnCode);

	/***
	 * 日结操作
	 * @param positnCode
	 * @return
	 */
	public void updateWorkDateByPositn(@Param(value="positnCode")String positnCode,@Param(value="workdate")Date nextDate);

	/***
	 * 根据物流的仓位编码查询boh的仓位是否存在
	 * @param positnCode
	 * @return
	 */
	public int findStoreByVcode(@Param(value="positnCode")String positnCode);

	/***
	 * 直配根据到货单单据查询验货条数 九毛九用
	 * @param dis
	 * @return
	 */
	public int findDireCountByBill(Dis dis);

	/***
	 * 统配根据单据得到验货条数 九毛九用
	 * @param dis
	 * @return
	 */
	public int findOutCountByBill(Dis dis);

	/***
	 * 查询角色的权限
	 * @param role
	 * @return
	 */
	public List<Module> findRoleOperateList(Role role);
	
	/***
	 * 查询未盘点的仓位
	 * @param cf
	 * @return
	 */
	public List<Positn> findUnInventoryPositn(Chkstoref cf);

	/***
	 * 月结操作
	 * @param positnCode
	 */
	public void updateMonthhByPositn(@Param(value="positnCode")String positnCode,@Param(value="monthh")String monthh);

}
