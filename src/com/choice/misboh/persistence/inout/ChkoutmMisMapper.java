package com.choice.misboh.persistence.inout;

import java.util.List;
import java.util.Map;

import com.choice.scm.domain.Chkoutm;
import com.choice.scm.domain.Spbatch;
import com.choice.scm.domain.SupplyAcct;

public interface ChkoutmMisMapper {

	/**
	 * 添加申购单
	 * @param chkoutm
	 */
	public void saveChkoutm(Map<String,Object> chkoutm);
	
	/**
	 * 查询申购单信息（时间段、出库仓位、领用仓位、单号、凭证号）
	 * @param chkoutm
	 */
	public List<Chkoutm> findChkoutm(Map<String,Object> map);
	
	/**
	 * 删除申购单信息
	 * @param chkoutm
	 */
	public void deleteChkoutm(Map<String,Object> chkoutm);
	
	/**
	 * 通过id获取出库单信息
	 * @param chkoutm
	 * @return
	 */
	public Chkoutm findChkoutmById(Chkoutm chkoutm);
	
	/**
	 * 更新出库单信息
	 * @param chkoutm
	 */
	public void updateChkoutm(Map<String,Object> chkoutm);
	
	/**
	 * 获取下一个单号
	 * @return
	 */
	public int findNextNo();
	
	/**
	 * 审核出库单
	 * @param chkoutm
	 */
	public void AuditChkout(Map<String,Object> chkoutm);
	
	/**
	 * 获取冲销数据  非门店
	 * @param chkout
	 */
	public List<Spbatch> findChkoutByCx(Spbatch spbatch);
	/**
	 * 获取冲销数据  门店
	 * @param chkout
	 */
	public List<Spbatch> findChkoutByCx_X(Spbatch spbatch);
	
	/**
	 * 审核出库单后更新总金额（冲销时总金额不正确）
	 * @param chkoutm
	 */
	public void updateChkoutAmount(Chkoutm chkoutm);

	/***
	 * 更新chkks状态  配送差异处理用 2014.12.20wjf
	 * @param supplyAcct
	 */
	public void updateChkks(SupplyAcct supplyAcct);
}
