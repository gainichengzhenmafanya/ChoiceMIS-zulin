package com.choice.misboh.persistence.inout;


import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SpCodeMod;
import com.choice.scm.domain.Spbatch;
import com.choice.scm.domain.Supply;

public interface ChkinmMisMapper {
	
	/**
	 * 查询门店入库单号序列最大值
	 */
	public Integer getMaxChkinno();

	/**
	 * 查询所有门店入库单
	 * @param chkinm
	 * @return
	 */
	public List<Chkinm> findAllChkinm(Chkinm chkinm);
	
	
	/**
	 * 模糊查询
	 * @param map
	 * @return
	 */
	public List<Chkinm> findAllChkinmByinput(Map<String,Object> map);
	
	/**
	 * 模糊查询
	 * @param map
	 * @return
	 */
	public List<Chkinm> findAllChkinmByinputNew(Map<String,Object> map);
	/**
	 * 添加门店入库单
	 * @param chkinm
	 */
	public void saveChkinm(Chkinm chkinm);
	/**
	 * 修改门店入库单
	 * @param chkinm
	 */
	public void updateChkinm(Chkinm chkinm);
	/**
	 * 审核门店入库单
	 * @param chkinm
	 */
	public void checkChkinm(Chkinm chkinm);
	/**
	 * 查询单条门店入库单
	 * @param chkind
	 */
	public  Chkinm  findChkinmByid(Chkinm chkinm);
	/**
	 * 删除门店入库单
	 * @param listId
	 */
	public void deleteChkinm(List<String> listChkinno);
	
	/**
	 * 查询默认仓位
	 * @param chkinm
	 */
	public List<SpCodeMod> getPositn(SpCodeMod spCodeMod);
	public List<Supply> getPositn1(Supply supply);
	
	/**
	 * 获取入库数据
	 * @param chkinm
	 */
	public List<Spbatch> addChkinmByCx(Spbatch spbatch);
	
	/**
	 *  查询入库金额汇总
	* @param map
	* @return
	 */
	public Chkinm findAllChkinmTotal(Map<String, Object> map);

	/**
	 * 根据主键字符串查找数据
	* @param replaceCode
	* @return
	 */
	public List<Chkinm> findChkinmByIds(@Param("chkinno")String replaceCode);

	/***
	 * 获取直发冲消数据
	 * @param spbatch
	 * @return
	 */
	public List<Spbatch> addChkinmzfByCx(Spbatch spbatch);

	/***
	 * 查询档口前十个
	 * @param positn
	 * @return
	 */
	public List<Positn> findAllPositnDeptN(Positn positn);

}
