package com.choice.misboh.autoExec;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimerTask;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.misboh.commonutil.DateJudge;
import com.choice.misboh.domain.costReduction.CostItem;
import com.choice.misboh.service.costReduction.CostReductionService;
import com.choice.scm.domain.Positn;
import com.choice.scm.service.PositnService;

/***
 * 自动核减数据 香港项目
 * 
 * @author 郭洪昌 创建时间：2016-8-11 下午2:44:53
 *
 */
public class CutExamination extends TimerTask {
	@Autowired
	private CostReductionService costReductionService;
	@Autowired
	private PositnService positnService;

	@Override
	public void run() {
		try {
			Date ldate = new Date();
			if (DateJudge.timeCompareDate(DateJudge.HH_mm.format(ldate), "02:00")
					|| !DateJudge.timeCompareDate(DateJudge.HH_mm.format(ldate), "03:00")) {
				return;
			}
			DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
			// 获取所有需要核减的店
			Positn positn1 = new Positn();
			positn1.setTypn("'1203'");
			List<Positn> lyList = positnService.findPositnSuperNOPage(positn1);
			Date date = DateUtils.addDays(new Date(), -1);
			for (Positn positn : lyList) {
				CostItem item = new CostItem();
				item.setAcct(positn.getAcct());
	            item.setBdat(date);
	            item.setEdat(date);
	            item.setFirm(positn.getCode());
	            item.setFirmdes(positn.getDes());
	            item.setYnUseDept(positn.getYnUseDept());
				costReductionService.subtract(item, new HashMap<String,String>());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
