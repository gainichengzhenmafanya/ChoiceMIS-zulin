package com.choice.scm.service;

import static com.choice.assistant.constants.system.SysParamConstants.SBOH;
import static com.choice.assistant.constants.system.SysParamConstants.SCM;
import static com.choice.assistant.constants.system.SysParamConstants.SYS;
import static com.choice.assistant.constants.system.SysParamConstants.USER;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.redis.cache.IMemCache;
import com.choice.framework.redis.cache.MemCacheFactory;
import com.choice.framework.redis.cache.SysParam;
import com.choice.framework.util.DateUtil;
import com.choice.scm.persistence.SysParamMapper;


@Service
public class SysParamService {

	private static final Logger LOG = LoggerFactory.getLogger(SysParamService.class);

	// 系统参数缓存
	private IMemCache sysCache = MemCacheFactory.getSysCache();
	// 企业参数缓存
	private IMemCache scmCache = MemCacheFactory.getScmCache();
	// 企业参数缓存
	private IMemCache sbohCache = MemCacheFactory.getSbohCache();
	// 用户参数缓存
	private IMemCache userCache = MemCacheFactory.getUserCache();

	@Autowired
	private SysParamMapper sysParamMapper;
	
	/***
	 * 从DB获取所有参数, 初期加载用
	 * @return
	 * @throws CRUDException
	 */
	public List<SysParam> getAll() throws CRUDException {
		
		LOG.debug("getAll start !");
		List<SysParam> paramList = sysParamMapper.getAll();
		
		return paramList;
	}
	
	/***
	 * 根据登录用户(企业号、用户名), 获取页面所有参数(系统参数、企业参数、用户参数)
	 * @param param
	 * @return
	 * @throws CRUDException
	 */
	public List<SysParam> getAllParams(SysParam param) throws CRUDException {
		
		LOG.debug("getAllParams start with param, paramCode is [{}], userName is [{}]",param.getVcode(), param.getUserName());
		
		List<SysParam> resultList = new LinkedList<SysParam>();
		
		// 获取系统参数
		List<SysParam> sysList = sysCache.getAllParams(null);
		if(LOG.isTraceEnabled())
			LOG.trace("从缓存获取系统参数, 数据条数[{}]", CollectionUtils.isEmpty(sysList)?0:sysList.size());
		if(CollectionUtils.isEmpty(sysList)){
			SysParam in = new SysParam();
			in.setType(SYS);
			sysList = sysParamMapper.getAllParams(in);
			if(LOG.isTraceEnabled())
				LOG.trace("从DB获取系统参数, 数据条数[{}]", CollectionUtils.isEmpty(sysList)?0:sysList.size());
			sysCache.setParams(sysList);
		}
		
		// 获取企业参数
		List<SysParam> scmList = scmCache.getAllParams(param.getVcode());
		if(LOG.isTraceEnabled())
			LOG.trace("从缓存获取企业参数, 数据条数[{}]", CollectionUtils.isEmpty(scmList)?0:scmList.size());
		if(CollectionUtils.isEmpty(scmList)){
			SysParam in = new SysParam();
			in.setType(SCM);
			in.setVcode(param.getVcode());
			scmList = sysParamMapper.getAllParams(in);
			if(LOG.isTraceEnabled())
				LOG.trace("从DB获取企业参数, 数据条数[{}]", CollectionUtils.isEmpty(scmList)?0:scmList.size());
			scmCache.setParams(scmList);
		}
		
		// 获取门店参数
		List<SysParam> sbohList = sbohCache.getAllParams(param.getVcode());
		if(LOG.isTraceEnabled())
			LOG.trace("从缓存获取门店参数, 数据条数[{}]", CollectionUtils.isEmpty(sbohList)?0:sbohList.size());
		if(CollectionUtils.isEmpty(sbohList)){
			SysParam in = new SysParam();
			in.setType(SBOH);
			in.setVcode(param.getVcode());
			sbohList = sysParamMapper.getAllParams(in);
			if(LOG.isTraceEnabled())
				LOG.trace("从DB获取门店参数, 数据条数[{}]", CollectionUtils.isEmpty(sbohList)?0:sbohList.size());
			sbohCache.setParams(sbohList);
		}
		
		// 获取用户参数
		List<SysParam> userList = userCache.getAllParams(param.getUserName());
		if(LOG.isTraceEnabled())
			LOG.trace("从缓存获取用户参数, 数据条数[{}]", CollectionUtils.isEmpty(userList)?0:userList.size());
		if(CollectionUtils.isEmpty(userList)){
			SysParam in = new SysParam();
			in.setType(USER);
			in.setUserName(param.getUserName());
			userList = sysParamMapper.getAllParams(in);
			if(LOG.isTraceEnabled())
				LOG.trace("从缓存获取用户参数, 数据条数[{}]", CollectionUtils.isEmpty(userList)?0:userList.size());
			userCache.setParams(userList);
		}
		
		// 系统参数结果集添加
		if(!CollectionUtils.isEmpty(sysList)) 
			resultList.addAll(sysList);
		
		// 企业参数结果集添加
		if(!CollectionUtils.isEmpty(scmList)) 
			resultList.addAll(scmList);
		
		// 门店参数结果集添加
		if(!CollectionUtils.isEmpty(sbohList)) 
			resultList.addAll(sbohList);
		
		// 用户参数结果集添加
		if(!CollectionUtils.isEmpty(userList)) 
			resultList.addAll(userList);
		
		LOG.debug("getAllParams end , get data size is [{}]", CollectionUtils.isEmpty(resultList)?0:resultList.size());
		
		return resultList;
	}
	
	/***
	 * 获取系统参数
	 * @return
	 * @throws CRUDException
	 */
	public List<SysParam> getSysParams() throws CRUDException {
		
		LOG.debug("getScmParams start !");
		// 缓存中获取系统参数
		List<SysParam> scmList = sysCache.getAllParams(null);
		// 缓存不存在DB获取
		if(CollectionUtils.isEmpty(scmList)){
			SysParam in = new SysParam();
			in.setType(SYS);
			scmList = sysParamMapper.getAllParams(in);
			sysCache.setParams(scmList);
		}
		
		return scmList;
	}
	
	/***
	 * 根据登录用户(企业号), 获取企业参数
	 * @param param
	 * @return
	 * @throws CRUDException
	 */
	public List<SysParam> getScmParams(SysParam param) throws CRUDException {
		
		LOG.debug("getScmParams start with param, vcode is [{}]", param.getVcode());
		// 缓存中获取企业参数
		List<SysParam> sbohList = scmCache.getAllParams(param.getVcode());
		// 缓存获取不到DB获取
		if(CollectionUtils.isEmpty(sbohList)){
			SysParam in = new SysParam();
			in.setType(SCM);
			in.setVcode(param.getVcode());
			sbohList = sysParamMapper.getAllParams(in);
			scmCache.setParams(sbohList);
		}
		
		return sbohList;
	}
	
	/***
	 * 根据登录用户(企业号), 获取门店参数
	 * @param param
	 * @return
	 * @throws CRUDException
	 */
	public List<SysParam> getSbohParams(SysParam param) throws CRUDException {
		
		LOG.debug("getSbohParams start with param, vcode is [{}]", param.getVcode());
		// 缓存中获取企业参数
		List<SysParam> sbohList = sbohCache.getAllParams(param.getVcode());
		// 缓存获取不到DB获取
		if(CollectionUtils.isEmpty(sbohList)){
			SysParam in = new SysParam();
			in.setType(SBOH);
			in.setVcode(param.getVcode());
			sbohList = sysParamMapper.getAllParams(in);
			sbohCache.setParams(sbohList);
		}
		
		return sbohList;
	}
	
	/***
	 * 根据登录用户(用户名), 获取用户参数
	 * @param param
	 * @return
	 * @throws CRUDException
	 */
	public List<SysParam> getUserParams(SysParam param) throws CRUDException {
		
		LOG.debug("getUserParams start with param, paramCode is [{}], userName is [{}]",param.getVcode(), param.getUserName());
		// 缓存中获取用户参数
		List<SysParam> userList = userCache.getAllParams(param.getUserName());
		// 缓存获取不到DB获取
		if(CollectionUtils.isEmpty(userList)){
			SysParam in = new SysParam();
			in.setType(USER);
			in.setUserName(param.getUserName());
			userList = sysParamMapper.getAllParams(in);
			userCache.setParams(userList);
		}
		
		return userList;
	}
	
	/***
	 * 根据paramCode获取系统参数--先从缓存--再数据库
	 * @param code
	 * @return
	 * @throws CRUDException
	 */
	public SysParam getSysParamByCode(String code) throws CRUDException {
		
		LOG.debug("getSysParamByCode start with param , paramCode is [{}]", code);
		// 缓存中获取系统参数
		String value = sysCache.getParamValueByKey(code);
		// 缓存获取不到DB获取
		if(StringUtils.isEmpty(value)) {
			SysParam in = new SysParam();
			in.setParamCode(code);
			SysParam param = sysParamMapper.getParamByCode(in);
			sysCache.setParam(param);
			return param;
		}
		SysParam cache = new SysParam();
		cache.setParamCode(code);
		cache.setParamValue(value);
		
		return cache;
	}
	
	/***
	 * 根据paramCode获取系统参数--先从缓存--再数据库
	 * @param code
	 * @return
	 * @throws CRUDException
	 */
	public String getSysParamByCodeStr(String code) throws CRUDException {
		
		LOG.debug("getSysParamByCodeStr start with param , paramCode is [{}]", code);
		// 缓存中获取系统参数
		String value = sysCache.getParamValueByKey(code);
		// 缓存中获取不到DB获取
		if(StringUtils.isEmpty(value)) {
			SysParam in = new SysParam();
			in.setParamCode(code);
			SysParam param = sysParamMapper.getParamByCode(in);
			sysCache.setParam(param);
			value = param ==null?"":param.getParamValue();
		}
		
		return value;
	}
	
	/***
	 * 根据paramCode获取系统参数--从缓存
	 * @param code
	 * @return
	 * @throws CRUDException
	 */
	public String getSysParamByCodeFromCache(String code) throws CRUDException {
		LOG.debug("getSysParamByCodeFromCache start with param , paramCode is [{}]", code);
		return sysCache.getParamValueByKey(code);
	}
	
	/***
	 * 根据paramCode、vCode获取企业参数--先从缓存--再数据库
	 * @param vCode
	 * @param code
	 * @return
	 * @throws CRUDException
	 */
	public SysParam getScmParamByCode(String vCode , String code) throws CRUDException {
		
		LOG.debug("getScmParamByCode start with param , vCode is [{}], paramCode is [{}]", vCode, code);
		// 缓存中获取企业参数
		String value = scmCache.getParamValueByKey(vCode, code);
		// 缓存中获取不到DB获取
		if(StringUtils.isEmpty(value)) {
			SysParam in = new SysParam();
			in.setParamCode(code);
			in.setVcode(vCode);
			SysParam param = sysParamMapper.getParamByCode(in);
			scmCache.setParam(param);
			return param;
		}
		SysParam cache = new SysParam();
		cache.setParamCode(code);
		cache.setParamValue(value);
		
		return cache;
	}
	/***
	 * 根据paramCode、vCode获取企业参数--先从缓存--再数据库
	 * @param vCode
	 * @param code
	 * @return
	 * @throws CRUDException
	 */
	public String getScmParamByCodeStr(String vCode , String code) throws CRUDException {
		
		LOG.debug("getScmParamByCodeStr start with param , vCode is [{}], paramCode is [{}]", vCode, code);
		// 缓存中获取企业参数
		String value = scmCache.getParamValueByKey(vCode, code);
		// 缓存中获取不到DB获取
		if(StringUtils.isEmpty(value)) {
			SysParam in = new SysParam();
			in.setParamCode(code);
			in.setVcode(vCode);
			SysParam param = sysParamMapper.getParamByCode(in);
			scmCache.setParam(param);
			value = param ==null?"":param.getParamValue();
		}
		
		return value;
	}
	
	/***
	 *  根据paramCode、vCode获取企业参数--从缓存
	 * @param vCode
	 * @param code
	 * @return
	 * @throws CRUDException
	 */
	public String getScmParamByCodeFromCache(String vCode , String code) throws CRUDException {
		LOG.debug("getScmParamByCodeFromCache start with param , vCode is [{}], paramCode is [{}]", vCode, code);
		return scmCache.getParamValueByKey(vCode, code);
	}
	/***
	 * 根据paramCode、vCode获取门店参数--先从缓存--再数据库
	 * @param vCode
	 * @param code
	 * @return
	 * @throws CRUDException
	 */
	public SysParam getSbohParamByCode(String vCode , String code) throws CRUDException {
		
		LOG.debug("getSbohParamByCode start with param , vCode is [{}], paramCode is [{}]", vCode, code);
		// 缓存中获取门店参数
		String value = sbohCache.getParamValueByKey(vCode, code);
		// 缓存中获取不到DB获取
		if(StringUtils.isEmpty(value)) {
			SysParam in = new SysParam();
			in.setParamCode(code);
			in.setVcode(vCode);
			SysParam param = sysParamMapper.getParamByCode(in);
			sbohCache.setParam(param);
			return param;
		}
		SysParam cache = new SysParam();
		cache.setParamCode(code);
		cache.setParamValue(value);
		
		return cache;
	}
	/***
	 * 根据paramCode、vCode获取企业参数--先从缓存--再数据库
	 * @param vCode
	 * @param code
	 * @return
	 * @throws CRUDException
	 */
	public String getSbohParamByCodeStr(String vCode , String code) throws CRUDException {
		
		LOG.debug("getSbohParamByCodeStr start with param , vCode is [{}], paramCode is [{}]", vCode, code);
		// 缓存中获取门店参数
		String value = sbohCache.getParamValueByKey(vCode, code);
		// 缓存中获取不到DB获取
		if(StringUtils.isEmpty(value)) {
			SysParam in = new SysParam();
			in.setParamCode(code);
			in.setVcode(vCode);
			SysParam param = sysParamMapper.getParamByCode(in);
			sbohCache.setParam(param);
			value = param ==null?"":param.getParamValue();
		}
		
		return value;
	}
	
	/***
	 *  根据paramCode、vCode获取企业参数--从缓存
	 * @param vCode
	 * @param code
	 * @return
	 * @throws CRUDException
	 */
	public String getSbohParamByCodeFromCache(String vCode , String code) throws CRUDException {
		LOG.debug("getSbohParamByCodeFromCache start with param , vCode is [{}], paramCode is [{}]", vCode, code);
		return sbohCache.getParamValueByKey(vCode, code);
	}
	
	/***
	 * 根据paramCode、userName获取用户参数--先从缓存--再数据库
	 * @param userName
	 * @param code
	 * @return
	 * @throws CRUDException
	 */
	public SysParam getUserParamByCode(String userName , String code) throws CRUDException {
		
		LOG.debug("getUserParamByCode start with param , userName is [{}], paramCode is [{}]", userName, code);
		// 缓存中获取用户参数
		String value = userCache.getParamValueByKey(userName, code);
		// 缓存中获取不到DB获取
		if(StringUtils.isEmpty(value)) {
			SysParam in = new SysParam();
			in.setUserName(userName);
			in.setParamCode(code);
			SysParam param = sysParamMapper.getParamByCode(in);
			userCache.setParam(param);
			return param;
		}
		SysParam cache = new SysParam();
		cache.setParamCode(code);
		cache.setParamValue(value);
		
		return cache;
	}
	
	/***
	 * 根据paramCode、userName获取用户参数--先从缓存--再数据库
	 * @param userName
	 * @param code
	 * @return
	 * @throws CRUDException
	 */
	public String getUserParamByCodeStr(String userName , String code) throws CRUDException {
		
		LOG.debug("getUserParamByCodeStr start with param , userName is [{}], paramCode is [{}]", userName, code);
		// 缓存中获取用户参数
		String value = userCache.getParamValueByKey(userName, code);
		// 缓存中获取不到DB获取
		if(StringUtils.isEmpty(value)) {
			SysParam in = new SysParam();
			in.setUserName(userName);
			in.setParamCode(code);
			SysParam param = sysParamMapper.getParamByCode(in);
			userCache.setParam(param);
			value = param ==null? "":param.getParamValue();
		}
		
		return value;
	}
	
	/***
	 * 根据paramCode、userName获取用户参数--从缓存
	 * @param userName
	 * @param code
	 * @return
	 * @throws CRUDException
	 */
	public String getUserParamByCodeFromCache(String userName , String code) throws CRUDException {
		LOG.debug("getUserParamByCodeFromCache start with param , userName is [{}], paramCode is [{}]", userName, code);
		return userCache.getParamValueByKey(userName, code);
	}
	
	/***
	 * 更新单个参数--先更新数据库--再更新缓存
	 * @param param
	 * @return
	 * @throws CRUDException
	 */
	public boolean update(SysParam param) throws CRUDException {
		
		LOG.debug("update start with params, paramCode is [{}], paramValue is [{}]", param.getParamCode(), param.getParamValue());
			// 系统参数的场合
			if(SYS.equals(param.getType())){
				// 要修改的值跟缓存中的值一致（既与数据库一致）  不做操作, 默认成功
				if(param.getParamValue().equals(sysCache.getParamValueByKey(param.getParamCode()))){
					return true;
				}
				boolean res = false;
				param.setUpdateOn(DateUtil.curr());
				res = sysParamMapper.update(param)>0;
				if(res){
					res = sysCache.setParam(param);
				}
				return res;
			// 门店参数的场合
			}else if(SBOH.equals(param.getType())){
				// 要修改的值跟缓存中的值一致（既与数据库一致）  不做操作, 默认成功
				if(param.getParamValue().equals(sbohCache.getParamValueByKey(param.getVcode(), param.getParamCode()))){
					return true;
				}
				boolean res = false;
				param.setUpdateOn(DateUtil.curr());
				res = sysParamMapper.update(param)>0;
				if(res){
					res = sbohCache.setParam(param);
				}
				return res;
			// 企业参数的场合
			}else if(SCM.equals(param.getType())){
				// 要修改的值跟缓存中的值一致（既与数据库一致）  不做操作, 默认成功
				if(param.getParamValue().equals(scmCache.getParamValueByKey(param.getVcode(), param.getParamCode()))){
					return true;
				}
				boolean res = false;
				param.setUpdateOn(DateUtil.curr());
				res = sysParamMapper.update(param)>0;
				if(res){
					res = scmCache.setParam(param);
				}
				return res;
			// 用户参数的场合
			}else if(USER.equals(param.getType())){
				// 要修改的值跟缓存中的值一致（既与数据库一致）  不做操作, 默认成功
				if(param.getParamValue().equals(userCache.getParamValueByKey(param.getUserName(), param.getParamCode()))){
					return true;
				}
				boolean res = false;
				param.setUpdateOn(DateUtil.curr());
				res = sysParamMapper.update(param)>0;
				if(res){
					res = userCache.setParam(param);
				}
				return res;
			} else {
				throw new InternalError("参数类型错误!");
			}

	}
	
	/***
	 * 更新一批参数--先更新数据库--再更新缓存
	 * @param param
	 * @return
	 * @throws CRUDException
	 */
	public boolean updateMany(List<SysParam> param) throws CRUDException {
		
		LOG.debug("updateMany startparamMapper with param, size is [{}]", (param == null|| param.isEmpty())?0:param.size());

		if(param == null ||param.isEmpty()) return false;
		// 修改数据库
		for(int i=0;i<param.size();i++){
			SysParam  paramIn = param.get(i);
			if(paramIn == null){
				param.remove(i);
				continue;
			}
			// 系统参数的场合： 要修改的值跟缓存中的值一致（既与数据库一致）  不做操作, 默认成功
			if(SYS.equals(paramIn.getType()) && 
					paramIn.getParamValue().equals(sysCache.getParamValueByKey(paramIn.getParamCode()))){
				param.remove(i);
				continue;
			// 门店参数的场合： 要修改的值跟缓存中的值一致（既与数据库一致）  不做操作, 默认成功
			}else if(SBOH.equals(paramIn.getType()) 
					&& paramIn.getParamValue().equals(sbohCache.getParamValueByKey(paramIn.getVcode(), paramIn.getParamCode()))){
				param.remove(i);
				continue;
			// 企业参数的场合： 要修改的值跟缓存中的值一致（既与数据库一致）  不做操作, 默认成功
			}else if(SCM.equals(paramIn.getType()) 
					&& paramIn.getParamValue().equals(scmCache.getParamValueByKey(paramIn.getVcode(), paramIn.getParamCode()))){
				param.remove(i);
				continue;
			// 用户参数的场合： 要修改的值跟缓存中的值一致（既与数据库一致）  不做操作, 默认成功
			}else if(USER.equals(paramIn.getType()) 
					&& paramIn.getParamValue().equals(userCache.getParamValueByKey(paramIn.getUserName(), paramIn.getParamCode()))){
				param.remove(i);
				continue;
			}
			paramIn.setUpdateOn(DateUtil.curr());
			if(sysParamMapper.update(paramIn) <1) {
				LOG.error("参数修改DB失败, paramCode is [{}], paramValue is [{}]", paramIn.getParamCode(), paramIn.getParamValue());
				return false;
			}
		}
		// 修改缓存
		for(SysParam paramIn :param){
			// 修改系统参数缓存
			if(SYS.equals(paramIn.getType())){
				sysCache.setParam(paramIn);
			// 修改门店参数缓存
			}else if(SBOH.equals(paramIn.getType())){
				sbohCache.setParam(paramIn);
			// 修改企业参数缓存
			}else if(SCM.equals(paramIn.getType())){
				scmCache.setParam(paramIn);
			// 修改用户参数缓存
			}else if(USER.equals(paramIn.getType())){
				userCache.setParam(paramIn);
			} else {
				LOG.error("参数类型错误!, paramCode is [{}], paramValue is [{}]", paramIn.getParamCode(), paramIn.getParamValue());
				return false;
			}
		}
		
		return true;
	}
	
	/***
	 * 用户登录初始化加载参数（系统、企业、用户）先从redis取, 再从数据库
	 * @param vcode
	 * @param userName
	 * @throws CRUDException
	 */
	public void loginCacheInitialized(String vcode, String userName)throws CRUDException{
		LOG.debug("loginCacheInitialized start with param , vcode is [{}], userName is [{}]", vcode, userName);
		SysParam param = new SysParam();
		param.setUserName(userName);
		param.setVcode(vcode);
		List<?> initCount = getAllParams(param);
		LOG.debug("loginCacheInitialized end, initialized data size is [{}]", CollectionUtils.isEmpty(initCount)?0:initCount.size());
	}
}
