package com.choice.scm.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 分店仓位
 * @author yp
 */

public class Positn implements Serializable {

	private static final long serialVersionUID = 1L;
	private String acct;	//帐套编码
	private String code;	//仓位分店编号
	private String firm;	//对应决策的分店编号
	private String des;		//名称
	private String sta;		//Y启用 N禁用
	private String locked;	//是否已用 Y已用 N未用
	private String qc;		//是否已期初
	private String inout2;	//暂不用
	private int prn;		//排列顺序
	private String des1;	//简写
	private String area;	//区域  编码
	private String pd;		//是否已盘点
	private String init;	//缩写
	private String monthh;	//当前月份，本仓位的当前月份 一般用在分店 
	private String typ;		//类型（主直拨库、基地仓库、加工间、外租库房、基地办公室、分店、分店部门）A/B/C/D…
	private String typn;     // 类型  多选，   比如      typn   in （‘加工间’，‘主直拨库’，'基地仓库'），则   typn          =   ‘加工间’，‘主直拨库’，'基地仓库'
	private String mod2;		//事业群
	private String psarea;	//配送区域
	private String psareacode;//配送片区编码
	private String ip;		//IP地址
	private String sd;		//暂不用
	private String oldcode;	//更新分店仓位是存储，修改前编号
	private String deliverDirect;//是否是供应商直配，供应商直配的时候不包含主直拨库和基地仓库
	private String firmtyp; //分店类别
	private String firmtypdes; //分店类别
	private String ipPing;//ip是否ping通  Y N
	private String ipTime;//ip ping通时间
	private String firmSupply;//是否是分店物资属性用，是的话会对仓位类型做限制，限制为不包括办公室和其他
	private String descode;//分店类型
	private String cwqx;//仓位权限，无筛选0，按照账号仓位权限1
	private String accountId;//仓位权限为1时候的筛选用
	private String ynZbChk;//是否总部审核
	private String ynZbXs;//是否总部虚实
	//用于检索仓位====================
	private String pcode;
	private String pname;
	private String ucode;	//上级编码
	
	//部门属性==============wangjie 2014年10月28日 16:34:31
	private String ynUseDept;//分店是否启用档口 Y表示启用  N表示禁用
	
	private String deptattr;
	private String locale;//语言类型
    //多CODE==============CZH
    private List<String> listCode;
    //采购模块使用
    //供应商编码。订单填制根据供应商选择采购组织
    private String delivercode;
    //物资编码，订单子表会根据分店物资属性选择当前物资可适用的分店
    private String sp_code;
    
    private String vaddr;//地址
    private String vmalladdrid; //商城地址id    
    private String vtel; //联系电话
    private String vcontact; //联系人
    private String vzipcode;//邮政编码
    private String nccode;
    private Date workdate;//营业日 bs门店boh用 
    private List<Deliver> deliverList;
    
    private String costtyp;//成本卡类型
    
    private String vplantyp;//预估方式
    private String visuseschedule;//是否启用配送班表
    
	public String getVplantyp() {
		return vplantyp;
	}
	public void setVplantyp(String vplantyp) {
		this.vplantyp = vplantyp;
	}
	public String getVisuseschedule() {
		return visuseschedule;
	}
	public void setVisuseschedule(String visuseschedule) {
		this.visuseschedule = visuseschedule;
	}
	public String getPsareacode() {
		return psareacode;
	}
	public void setPsareacode(String psareacode) {
		this.psareacode = psareacode;
	}
	public String getCosttyp() {
		return costtyp;
	}
	public void setCosttyp(String costtyp) {
		this.costtyp = costtyp;
	}
	public String getNccode() {
		return nccode;
	}
	public void setNccode(String nccode) {
		this.nccode = nccode;
	}
	public List<Deliver> getDeliverList() {
		return deliverList;
	}
	public void setDeliverList(List<Deliver> deliverList) {
		this.deliverList = deliverList;
	}
	public Date getWorkdate() {
		return workdate;
	}
	public void setWorkdate(Date workdate) {
		this.workdate = workdate;
	}
	public String getVaddr() {
		return vaddr;
	}
	public void setVaddr(String vaddr) {
		this.vaddr = vaddr;
	}
	public String getVmalladdrid() {
		return vmalladdrid;
	}
	public void setVmalladdrid(String vmalladdrid) {
		this.vmalladdrid = vmalladdrid;
	}
	public String getVtel() {
		return vtel;
	}
	public void setVtel(String vtel) {
		this.vtel = vtel;
	}
	public String getVcontact() {
		return vcontact;
	}
	public void setVcontact(String vcontact) {
		this.vcontact = vcontact;
	}
	public String getVzipcode() {
		return vzipcode;
	}
	public void setVzipcode(String vzipcode) {
		this.vzipcode = vzipcode;
	}
	public String getDelivercode() {
		return delivercode;
	}
	public void setDelivercode(String delivercode) {
		this.delivercode = delivercode;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public String getTypn() {
		return typn;
	}
	public void setTypn(String typn) {
		this.typn = typn;
	}
	public String getUcode() {
		return ucode;
	}
	public void setUcode(String ucode) {
		this.ucode = ucode;
	}
	public String getSta() {
		return sta;
	}
	public void setSta(String sta) {
		this.sta = sta;
	}
	public String getPcode() {
		return pcode;
	}
	public void setPcode(String pcode) {
		this.pcode = pcode;
	}
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	//==============================
	
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public String getCwqx() {
		return cwqx;
	}
	public void setCwqx(String cwqx) {
		this.cwqx = cwqx;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getDescode() {
		return descode;
	}
	public void setDescode(String descode) {
		this.descode = descode;
	}
	public String getFirmSupply() {
		return firmSupply;
	}
	public void setFirmSupply(String firmSupply) {
		this.firmSupply = firmSupply;
	}
	public String getIpPing() {
		return ipPing;
	}
	public String getFirmtyp() {
		return firmtyp;
	}
	public void setFirmtyp(String firmtyp) {
		this.firmtyp = firmtyp;
	}
	public String getFirmtypdes() {
		return firmtypdes;
	}
	public void setFirmtypdes(String firmtypdes) {
		this.firmtypdes = firmtypdes;
	}
	public void setIpPing(String ipPing) {
		this.ipPing = ipPing;
	}
	public String getIpTime() {
		return ipTime;
	}
	public void setIpTime(String ipTime) {
		this.ipTime = ipTime;
	}

	public String getDeliverDirect() {
		return deliverDirect;
	}

	public void setDeliverDirect(String deliverDirect) {
		this.deliverDirect = deliverDirect;
	}

	public Positn() {
		super();
	}
	
	public String getAcct() {
		return acct;
	}

	public void setAcct(String acct) {
		this.acct = acct;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public String getInout2() {
		return inout2;
	}

	public void setInout2(String inout2) {
		this.inout2 = inout2;
	}

	public int getPrn() {
		return prn;
	}

	public void setPrn(int prn) {
		this.prn = prn;
	}

	public String getDes1() {
		return des1;
	}

	public void setDes1(String des1) {
		this.des1 = des1;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getInit() {
		return init;
	}

	public void setInit(String init) {
		this.init = init;
	}

	public String getMonthh() {
		return monthh;
	}

	public void setMonthh(String monthh) {
		this.monthh = monthh;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getMod2() {
		return mod2;
	}

	public void setMod2(String mod2) {
		this.mod2 = mod2;
	}

	public String getPsarea() {
		return psarea;
	}

	public void setPsarea(String psarea) {
		this.psarea = psarea;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getSd() {
		return sd;
	}

	public void setSd(String sd) {
		this.sd = sd;
	}

	public String getOldcode() {
		return oldcode;
	}

	public void setOldcode(String oldcode) {
		this.oldcode = oldcode;
	}

	public String getLocked() {
		return locked;
	}

	public void setLocked(String locked) {
		this.locked = locked;
	}

	public String getQc() {
		return qc;
	}

	public void setQc(String qc) {
		this.qc = qc;
	}

	public String getPd() {
		return pd;
	}

	public void setPd(String pd) {
		this.pd = pd;
	}
	public String getYnZbChk() {
		return ynZbChk;
	}
	public void setYnZbChk(String ynZbChk) {
		this.ynZbChk = ynZbChk;
	}
	public String getYnZbXs() {
		return ynZbXs;
	}
	public void setYnZbXs(String ynZbXs) {
		this.ynZbXs = ynZbXs;
	}
	public String getDeptattr() {
		return deptattr;
	}
	public void setDeptattr(String deptattr) {
		this.deptattr = deptattr;
	}

    public List<String> getListCode() {
        return listCode;
    }

    public void setListCode(List<String> listCode) {
        this.listCode = listCode;
    }
	public String getYnUseDept() {
		return ynUseDept;
	}
	public void setYnUseDept(String ynUseDept) {
		this.ynUseDept = ynUseDept;
	}
	public String getLocale() {
		return locale;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
    
}
