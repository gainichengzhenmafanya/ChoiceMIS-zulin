package com.choice.scm.web.controller;

import java.net.URLDecoder;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.DeliverConstants;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Deliver;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.DeliverService;

@Controller
@RequestMapping(value = "deliver")
public class DeliverController {

	@Autowired
	private DeliverService deliverService;
	@Autowired
	private CodeDesService codeDesService;	
	


	/**
	 * 匹配供货商信息
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchAllDeliver")
	public ModelAndView searchAllDeliver(ModelMap modelMap, Deliver deliver,String defaultCode,String defaultName,Page page) throws Exception
	{
		//报货分拨上的供应商查找主页面
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("deliverList", deliverService.findAllPageDelivers(deliver,page));
		modelMap.put("queryDeliver", deliver);
		modelMap.put("defaultCode", defaultCode);
		if(null!=defaultName){
			modelMap.put("defaultName", URLDecoder.decode(defaultName, "UTF-8"));
		}
		modelMap.put("pageobj", page);
		return new ModelAndView(DeliverConstants.SEARCH_ALLDELIVERS,modelMap);
	}
	
	/**
	 * 选择供应商
	 * @param modelMap
	 * @param positn
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectOneDeliver")
	public ModelAndView selectOneDeliver(ModelMap modelMap, Deliver deliver, Page page, String defaultCode, String defaultName) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("defaultCode", defaultCode);
		if(null!=defaultName){
			modelMap.put("defaultName", URLDecoder.decode(defaultName, "UTF-8"));
		}
		//供应商定义主界面
		modelMap.put("delivierListType", deliverService.findDeliverType());
		return new ModelAndView(DeliverConstants.SELECT_ONEDELIVERS2, modelMap);
	}
	
	/**
	 * 
	 * @Title: findAllDelivers2 
	 * @Description: (供货商列表右侧（选择单一供货商使用）) 
	 * @Author：LI Shuai
	 * @date：2014-1-23上午9:50:10
	 * @param modelMap
	 * @param page
	 * @param deliver
	 * @return
	 * @throws Exception  ModelAndView
	 * @throws
	 */
	@RequestMapping(value = "/deliverList2")
	public ModelAndView findAllDelivers2(ModelMap modelMap, Page page, Deliver deliver, HttpSession session) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//供应商定义主界面
		CodeDes codeDes=new CodeDes();
		codeDes.setTyp(String.valueOf(CodeDes.S_DEL_TYPE));//供应商类型
		modelMap.put("codeDesList", codeDesService.findCodeDes(codeDes));
		//供应商权限筛选
		deliver.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
		deliver.setAccountId(session.getAttribute("accountId").toString());
		modelMap.put("deliverList", deliverService.findDeliver(deliver, page));
		modelMap.put("pageobj", page);
		modelMap.put("typCode", deliver.getTypCode());
		modelMap.put("deliver", deliver);
		return new ModelAndView(DeliverConstants.TABLE_DELIVERS2,modelMap);
	}
	
}
