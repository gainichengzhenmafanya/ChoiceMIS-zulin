package com.choice.scm.web.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ForResourceFiles;
import com.choice.misboh.domain.costReduction.MisSupply;
import com.choice.misboh.service.common.CommonMISBOHService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.CostbomConstants;
import com.choice.scm.constants.SupplyConstants;
import com.choice.scm.domain.Pubitem;
import com.choice.scm.domain.Spprice;
import com.choice.scm.domain.SppriceSale;
import com.choice.scm.domain.Supply;
import com.choice.scm.service.CostbomService;
import com.choice.scm.service.GrpTypService;
import com.choice.scm.service.SupplyService;
import com.choice.scm.util.FileWorked;
/**
 * 物资编码
 * @author lehui
 *
 */
@Controller
@RequestMapping(value = "supply")
public class SupplyController {

	@Autowired
	private SupplyService supplyService;
	@Autowired
	private GrpTypService grpTypService;
	@Autowired
	private CostbomService costbomService;
	@Autowired
	private CommonMISBOHService commonMISBOHService;

	/***
	 * 拿到标准产品列表
	 * @param modelMap
	 * @param pubitem
	 * @param page
	 * @param vvcode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchPubitemList")
	public ModelAndView searchPubitemList(ModelMap modelMap,Pubitem pubitem,Page page,String vvcode) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
		if (pubitem == null) {
			modelMap.put("pubitemList", costbomService.findAllPubitem_boh(new Pubitem(),page));//标准产品
		} else {
			modelMap.put("pubitemList", costbomService.findAllPubitem_boh(pubitem,page));//标准产品
		}
		
		//标准产品编码
		modelMap.put("vvcode", vvcode);
		modelMap.put("pageobj", page);
		
		return new ModelAndView(SupplyConstants.SEARCH_ALL_PUBITEM, modelMap);
	}
	
	/**
	 * 打开选择物资类别
	 * @param modelMap
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/selectGrptyp")
	public ModelAndView selectGrptyp(ModelMap modelMap,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));//大类
		modelMap.put("grpList", grpTypService.findAllGrpA(acct,null));//中类
		modelMap.put("typList", grpTypService.findAllTypA(acct,null));//小类
		return new ModelAndView(SupplyConstants.SELECT_SUPPLY, modelMap);
	}
	
	/**
	 * 根据输入的条件查询 前n条记录,用于申购时，可按照分店权限筛选
	 */
	@RequestMapping(value = "/findTop1", method=RequestMethod.POST)
	@ResponseBody
	public  List<Supply> findTop1(Supply supply, HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//帐套
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		supply.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		supply.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//物资账号权限是具体到物资还是具体到小类  wjf
		supply.setAccountId(session.getAttribute("accountId").toString());
	
		return supplyService.findSupplyListTopN(supply);
	}
	
	/**
	 * 根据条件查询售价
	 */
	@RequestMapping(value = "/findSprice", method=RequestMethod.POST)
	@ResponseBody
	public  SppriceSale findSprice(SppriceSale spprice, String typ, HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//帐套
		spprice.setAcct(session.getAttribute("ChoiceAcct").toString());
		double price = 0;
		SppriceSale sprice = supplyService.findSprice(spprice);
		if (sprice == null|| sprice.getPrice() == null) {
			//没有售价就取0，对应保存之后的显示   ,如果硬是要取个价格，没先进先出之前也不一定对 20170623wjf
			price = 0.0;
//			Supply supply = new Supply();
//			supply.setAcct(session.getAttribute("ChoiceAcct").toString());
//			supply.setSp_code(spprice.getSupply().getSp_code());
//			Supply sup=supplyService.findSupplyById(supply);
//			//wangjie 2014年10月28日 09:20:11  取物资的售价
//			if (sup.getPricesale()!=null && sup.getPricesale() != 0) {
//				price = sup.getPricesale();
//			}else{
//				price = sup.getSp_price();
//			}
		}else {
			price = sprice.getPrice().doubleValue();
		}
		spprice.setPrice(price);
		return spprice;
	}

	/**
	 * 根据条件查询报价
	 */
	@RequestMapping(value = "/findBprice", method=RequestMethod.POST)
	@ResponseBody
	public Spprice findBprice(Spprice spprice, HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//帐套
		String acct = session.getAttribute("ChoiceAcct").toString();
		spprice.setAcct(acct);
		//1.查报价
		Spprice sp = supplyService.findBprice(spprice);
		spprice.setPrice(new BigDecimal(0));
		spprice.setSta("N");
		if (null == sp||null == sp.getPrice()) {
			Supply supply = new Supply();
			supply.setAcct(acct);
			supply.setSp_code(spprice.getSp_code());
			supply = supplyService.findSupplyById(supply);
			if (supply.getSp_price()!=null) {
				spprice.setPrice(new BigDecimal(supply.getSp_price()));
			}
		} else {
			spprice.setPrice(sp.getPrice());
			spprice.setSta("Y");
		}
		//2.查税率
		if(null != spprice.getDeliver()){
			MisSupply ms = new MisSupply();
			ms.setAcct(acct);
			ms.setSp_code(spprice.getSp_code());
			ms.setDeliver(spprice.getDeliver());
			ms = commonMISBOHService.findTaxByDeliverSpcode(ms);
			spprice.setTax(ms.getTax());
			spprice.setTaxdes(ms.getTaxdes());
		}
		return spprice;
	}
	
	/**
	 * 下载操作文档
	 */
	@RequestMapping(value = "/downloadScmTemplate")
	public void downloadScmTemplate(HttpServletResponse response,HttpServletRequest request,String fileName) throws IOException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		FileWorked.downloadTemplate(response, request, "\\template\\"+fileName);
	}
	
	/**
	 * 根据sp_code获取物资信息并返回json对象
	 * @param supply
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/findById")
	@ResponseBody
	public Object findById(Supply supply,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		return supplyService.findSupplyById(supply);
	}
	
	/**
	 * 弹出物资选择框                左侧
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectSupplyLeft")
	public ModelAndView selectSupplyLeft(ModelMap modelMap,HttpSession session,String defaultCode, String is_supply_x,String single,String ex1,String sp_position)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));//大类
		modelMap.put("grpList", grpTypService.findAllGrpA(acct,null));//中类
		modelMap.put("typList", grpTypService.findAllTypA(acct,null));//小类
		modelMap.put("defaultCode", defaultCode);//默认选中的编码
		modelMap.put("single", single);//是否单选 默认单选  false多选
		modelMap.put("ex1", ex1);//是否是只查询加工品  wjf
		modelMap.put("sp_position", sp_position);//部分需要根据仓位物资属性进行过滤   2015.6.11   xlh  
		if(null!=is_supply_x && "Y_group".equals(is_supply_x)){//虚拟物料  并且需要分组合并
			modelMap.put("is_supply_x", "Y_group");//虚拟物资编码
			return new ModelAndView(CostbomConstants.SELECT_SUPPLY_X, modelMap);
		}else{
			return new ModelAndView(CostbomConstants.SELECT_SUPPLY, modelMap);
		}
	//	modelMap.put("supplyList", supplyService.findAllSupplyByLeftGrpTyp(null,"1","", new Page()));
	}
	
	/**
	 * 弹出物资选择框                右侧
	 * @param modelMap
	 * @return    is_supply_x   虚拟物资编码
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectSupply")
	public ModelAndView selectSupply(ModelMap modelMap, Supply supply, String level, String code,String single ,Page page,
			HttpSession session, String defaultCode, String is_supply_x)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		if(null!=is_supply_x && "Y_group".equals(is_supply_x)){
			supply.setIs_supply_x("Y_group");//虚拟物料  且是 分组合并
		}
		supply.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		supply.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//物资账号权限是具体到物资还是具体到小类  wjf
		supply.setAccountId(session.getAttribute("accountId").toString());
		modelMap.put("supplyList", supplyService.findAllSupplyByLeftGrpTyp(supply,level,code,page));
		modelMap.put("code", code);
		modelMap.put("level", level);
		modelMap.put("pageobj",page);
		modelMap.put("supply",supply);
		modelMap.put("defaultCode", defaultCode);//默认选中的编码
		//wangjie 2014年11月21日 14:04:40  报货单、出库单填制提交 物资支持多选
		modelMap.put("single", single );//是否单选 默认单选  false多选
		if(null!=is_supply_x && ("Y_group").equals(is_supply_x)){//是否虚拟物料
			modelMap.put("is_supply_x", "Y_group");
			return new ModelAndView(CostbomConstants.SELECT_TABLE_SUPPLY_X, modelMap);
		}else{
			return new ModelAndView(CostbomConstants.SELECT_TABLE_SUPPLY, modelMap);
		}
	}
	
	/**
	 * 查询多条物资    弹出框 下左
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectNSupply")
	public ModelAndView selectNSupply(ModelMap modelMap, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));//大类
		modelMap.put("grpList", grpTypService.findAllGrpA(acct,null));//中类
		modelMap.put("typList", grpTypService.findAllTypA(acct,null));//小类
		return new ModelAndView(SupplyConstants.SELECT_NSUPPLY, modelMap);
	}
	
	/**
	 * 查询多条物资    弹出框 下右
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectTableNSupply")
	public ModelAndView selectTableNSupply(ModelMap modelMap, Supply supply, String level, String code, Page page, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		supply.setAccountId(session.getAttribute("accountId").toString());
		modelMap.put("supplyList", supplyService.findAllSupplyByLeftGrpTyp(supply, level, code, page));
		modelMap.put("pageobj", page);
		modelMap.put("code", code);
		modelMap.put("level", level);
		modelMap.put("supply", supply);//查询记忆
		return new ModelAndView(SupplyConstants.SELECT_TABLENSUPPLY, modelMap);
	}
	
	/**
	*获取物资大类 
	*
	*/
	@RequestMapping(value = "/findDivisionSupply")
	public ModelAndView chooseSupply(ModelMap modelMap,HttpSession session)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//左侧导航树
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));//大类
		return new ModelAndView(SupplyConstants.DIVISIONSUPPLY, modelMap);
	}
	
	/**
	 * 获取物资中类
	 */
	@RequestMapping(value="/findGroupTyp")
	public ModelAndView findGroupTyp(ModelMap modelMap,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//左侧导航树
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpList", grpTypService.findAllGrpA(acct,null));//中类
		return new ModelAndView(SupplyConstants.GROUPSUPPLY, modelMap);
	}
	
	/**
	 * 获取物资小
	 */
	@RequestMapping(value="/findSmallClass")
	public ModelAndView findSmallClass(ModelMap modelMap,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//左侧导航树
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("typList", grpTypService.findAllTypA(acct,null));//小类
		return new ModelAndView(SupplyConstants.CLASSSUPPLY, modelMap);
	}
	/**
	 * 根据大类查询该大类下的所有物资
	 * 
	 */
	@RequestMapping(value="/findGrp")
	@ResponseBody
	public  String findGrp(ModelMap modelMap,HttpSession session,String code,String level)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<Supply> listSupply = grpTypService.findGrp(code,session.getAttribute("ChoiceAcct").toString());
		String codeStr = "";
		String nameStr = "";
		String str = "";
		if(listSupply!=null){
			for(int i=0;i<listSupply.size();i++){
	 			codeStr += listSupply.get(i).getSp_code()+",";
	 			nameStr += listSupply.get(i).getSp_name()+",";
			}
			codeStr = codeStr.substring(0,codeStr.lastIndexOf(","));
			nameStr = nameStr.substring(0,nameStr.lastIndexOf(","));
			str = codeStr+"_"+nameStr;
		}
		return str;
		 
	}
	
	/**
	 * 根据中类查询该中类下的所有物资
	 * 
	 */
	@RequestMapping(value="/findTyp")
	@ResponseBody
	public  String findTyp(ModelMap modelMap,HttpSession session,String code,String level)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<Supply> typList = grpTypService.findTyp(code,session.getAttribute("ChoiceAcct").toString());
		String codeStr = "";
		String nameStr = "";
		String str = "";
		if(typList!=null){
		for(int i=0;i<typList.size();i++){
 			codeStr += typList.get(i).getSp_code()+",";
 			nameStr += typList.get(i).getSp_name()+",";
		}
		codeStr = codeStr.substring(0,codeStr.lastIndexOf(","));
		nameStr = nameStr.substring(0,nameStr.lastIndexOf(","));
		str = codeStr+"_"+nameStr;
		}
		return str;

	}
	
	/**
	 * 根据小类查询该小类下的所有物资 
	 * 
	 */
	@RequestMapping(value="/findSupplyByTyp")
	@ResponseBody
	public  String findSupplyByTyp(ModelMap modelMap,HttpSession session,String code,String level)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		 List<Supply> supplyList = grpTypService.findSupplyByTyp(code,session.getAttribute("ChoiceAcct").toString());
		 String codeStr = "";
			String nameStr = "";
			String str = "";
		if(supplyList!=null){
			for(int i=0;i<supplyList.size();i++){
	 			codeStr += supplyList.get(i).getSp_code()+",";
	 			nameStr += supplyList.get(i).getSp_name()+",";
			}
			codeStr = codeStr.substring(0,codeStr.lastIndexOf(","));
			nameStr = nameStr.substring(0,nameStr.lastIndexOf(","));
			str = codeStr+"_"+nameStr;
		}
			return str;
	}
	
}
