package com.choice.scm.web.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.scm.constants.GrpTypConstants;
import com.choice.scm.service.GrpTypService;

/**
 * 物资类别管理
 * 
 * @author yp
 * 
 */
@Controller
@RequestMapping(value = "grpTyp")
public class GrpTypController {

	@Autowired
	private GrpTypService grpTypService;

	/**
	 * 打开选择物资类别
	 * 
	 * @param modelMap
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping(value = "/selectGrptyp")
	public ModelAndView selectGrptyp(ModelMap modelMap, HttpSession session,
			int level) throws CRUDException {

		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));// 大类
		modelMap.put("grpList", grpTypService.findAllGrpA(acct, null));// 中类
		modelMap.put("typList", grpTypService.findAllTypA(acct, null));// 小类
		modelMap.put("level", level - 1);
		return new ModelAndView(GrpTypConstants.SELECT_SUPPLY, modelMap);
	}

	/**
	 * 获取所有物资中类信息
	 * 
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("/findAllGrp")
	@ResponseBody
	public Object findAllGrp(HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		return grpTypService.findAllGrpA(session.getAttribute("ChoiceAcct")
				.toString(), null);
	}

	/**
	 * 获取所有物资小类信息
	 * 
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("/findAllTyp")
	@ResponseBody
	public Object findAllTyp(HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		return grpTypService.findAllTypA(session.getAttribute("ChoiceAcct")
				.toString(), null);
	}

	/**
	 * 获取所有物资大类信息
	 * 
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("/findAllGrpTyp")
	@ResponseBody
	public Object findAllGrpTyp(HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		return grpTypService.findAllGrpTypA(session.getAttribute("ChoiceAcct")
				.toString());
	}

	/**
	 * 获取所有物资类别信息
	 * 
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping(value = "/selectOneGrpTyp")
	public Object selectOneGrpTyp(ModelMap modelMap, String defaultCode,
			String defaultName) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		modelMap.put("listTyp", grpTypService.findAllTyp(null));
		modelMap.put("defaultCode", defaultCode);
		modelMap.put("defaultName", defaultName);
		return new ModelAndView(GrpTypConstants.SELECT_TYP, modelMap);
	}
	
	/**
	 * 获取所有物资类别信息,多选
	 * 
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author wjf
	 */
	@RequestMapping(value = "/selectMoreGrpTyp")
	public Object selectMoreGrpTyp(ModelMap modelMap, HttpSession session, String defaultCode) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));// 大类
		modelMap.put("grpList", grpTypService.findAllGrpA(acct, null));// 中类
		modelMap.put("typList", grpTypService.findAllTypA(acct, null));// 小类
		modelMap.put("defaultCode", defaultCode);
		return new ModelAndView(GrpTypConstants.SELECT_TYP_MORE, modelMap);
	}

}
