package com.choice.scm.web.controller;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.PositnConstants;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Positn;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.PositnService;

/**
 * 分店管理
 * @author yp
 *
 */
@Controller
@RequestMapping("positn")
public class PositnController {

	@Autowired
	private PositnService positnService;
	@Autowired
	private CodeDesService codeDesService;
	@Autowired
	private AccountPositnService accountPositnService;
	
	
	/**
	 * 查询所有的分店和仓位
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author yp
	 * @param single 判断是单选  还是多选  1单选 0多选
	 * @param typN 仓位
	 */
	@RequestMapping("/searchAllPositn")
	public ModelAndView searchAllPositn(HttpSession session, ModelMap modelMap, Positn positn, String cwqx, String mis,String defaultCode,
			String defaultName,String single,String tagName,String tagId,String callBack) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if ("y".equals(mis)) {
			positn.setTyp("1203");
		}
		Page page = new Page();
		page.setPageSize(Integer.MAX_VALUE);
		CodeDes codeDes = new CodeDes();
		codeDes.setTyp(String.valueOf(CodeDes.S_PS_AREA));
		modelMap.put("psarea", codeDesService.findCodeDes(codeDes, page));
		codeDes.setTyp(String.valueOf(CodeDes.S_AREA));
		//配送片区
		List<CodeDes> listCode = codeDesService.findCodeDes(codeDes, page);
		
		if (cwqx==null || "".equals(cwqx)){
			positn.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		}
		
		if(positn.getCode() != null && !"".equals(positn.getCode())){
			modelMap.put("listPositn1", positnService.selectPositnNew(positn));
		}
		
		positn.setAccountId(session.getAttribute("accountId").toString());
		String locale = session.getAttribute("locale").toString();//语言类型
		positn.setLocale(locale);
		//清空positn中的code值，查询所有仓位
		positn.setCode("");
		List<CodeDes> listCode1 = new ArrayList<CodeDes>();
		for(CodeDes code:listCode){
			positn.setArea(code.getCode());
			if(positnService.findPositnSuperNOPage(positn).size()!=0){
				listCode1.add(code);
			}
		}
		modelMap.put("area", listCode1);
		modelMap.put("listPositn", positnService.findPositnSuperNOPage(positn));
		
		CodeDes codedes = new CodeDes();
		codedes.setTyp("12");
		codedes.setLocale(locale);
		
		if(positn.getTypn() != null && !"".equals(positn.getTypn())){
			codedes.setCode(positn.getTypn());
		}
		modelMap.put("listPositnTyp", codeDesService.findCodeDes(codedes));
		modelMap.put("positn", positn);
		//add wangjie 2014年10月30日 14:31:23
		modelMap.put("single", single);
		modelMap.put("tagName", tagName);
		modelMap.put("tagId", tagId);
		modelMap.put("callBack", callBack);//回调函数名称
		
		modelMap.put("defaultCode", defaultCode);
		if(null!=defaultName){
			modelMap.put("defaultName", URLDecoder.decode(defaultName,"UTF-8"));
		}
		modelMap.put("mis", mis);
		return new ModelAndView(PositnConstants.SEARCH_ALLPOSITN,modelMap);
	}
	
	/**
	 * 查询所有仓位（新公用方法）
	 * @param positn
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/searchAllPositnNew")
	@ResponseBody
	public Object searchAllPositnNew(Positn positn) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		String positntyp = positn.getTyp();
		if(positn.getArea() !=null && "00000000000000000000000000000000".equals(positn.getArea())){
			positn.setArea("");
		}
		if(positn.getTyp() !=null && "00000000000000000000000000000000".equals(positn.getTyp())){
			positn.setTyp("");
		}
		return positnService.findAllPositn(positn);
	}
	
	/**
	 * 
	* @Title: selectPositn 
	* @Description: (选择仓位左侧（选择单一仓位使用）) 
	* @Author：LI Shuai
	* @date：2014-1-23下午4:44:41
	* @param modelMap
	* @param positn
	* @param deliverDirect
	* @param defaultName
	* @param defaultCode
	* @return
	* @throws Exception  ModelAndView
	* @throws
	 */
	@RequestMapping(value = "/selectPositn")
	public ModelAndView selectPositn(ModelMap modelMap,Positn positn, String cwqx, String deliverDirect, String defaultName, String defaultCode, String typ,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if ("firm".equals(typ)) {
			positn.setDes("1203");
		}
		if ("cangku".equals(typ)) {//只查仓库和住直播库 用在直发单填制审核 入库仓位查询 wjf
			positn.setDes("库");
		}
		String locale = session.getAttribute("locale").toString();
		positn.setLocale(locale);
		modelMap.put("listPositnType",positnService.findCodedesType(positn));
		modelMap.put("defaultName", (null==defaultName||"".equals(defaultName))?defaultName:URLDecoder.decode(defaultName, "UTF-8"));
		modelMap.put("defaultCode", defaultCode);
		modelMap.put("queryPositn", positn);
		modelMap.put("cwqx", cwqx);
		return new ModelAndView(PositnConstants.LIST_POSITN_TYPE_FORSE,modelMap);
		
	}
	
	/**
	 * 
	* @Title: findPositnForse 
	* @Description: (选择仓位右侧（选择单一仓位使用）) 
	* @Author：LI Shuai
	* @date：2014-1-23下午5:12:59
	* @param modelMap
	* @param positn
	* @param page
	* @param descode
	* @return
	* @throws Exception  ModelAndView
	* @throws
	 */
	
	@RequestMapping("/listDetailForse")
	public ModelAndView findPositnForse(ModelMap modelMap, HttpSession session, Positn positn, Page page, String descode, String cwqx) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		CodeDes code = new CodeDes();
		Page p = new Page();
		p.setPageSize(Integer.MAX_VALUE);
		code.setTyp(String.valueOf(CodeDes.S_SYQ));
		modelMap.put("listMod", codeDesService.findCodeDes(code, p));
		if (cwqx==null && "".equals(cwqx)){
			positn.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		}
		code.setTyp(String.valueOf(CodeDes.S_AREA));
		modelMap.put("listArea", codeDesService.findCodeDes(code, p));
		positn.setAccountId(session.getAttribute("accountId").toString());
		positn.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		modelMap.put("listPositn", positnService.findPositn(positn,page));
		modelMap.put("listPositnTyp", positnService.findAllPositnTyp());
		modelMap.put("queryPositn", positn);
		modelMap.put("pageobj", page);
		modelMap.put("descode",descode);
		return new ModelAndView(PositnConstants.LIST_POSITN_FORSE,modelMap);
	}
	
	/**
	 * 查询货架
	 * @return
	 * @throws Exception
	 * @author ZGL
	 */
	@RequestMapping("findPositn1")
	@ResponseBody
	public Object findPositn1() throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return positnService.findPositn1();
	}
	
	/**
	 * 根据分店编码code 获取分店信息并返回json对象   wangjie 2014年10月30日 13:25:27
	 * @param Positn
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/findById")
	@ResponseBody
	public Object findById(Positn positn,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return  positnService.findPositnByCode(positn);
	}
	
	/** 
	 * 严禁修改     严禁修改     严禁修改     严禁修改      严禁修改   严禁修改   严禁修改
	 * 查询仓位信息公共 。。。。。。。。。。。。。。。。。。。。。。。。。2015.1.8后 所有方法都可以走 2015.1.8  xlh    严禁修改
	 *  控制typ值就行     比如   查询 基地  仓位和加工间     typ= “typ in （基地仓位，1204）”
	 *  mold  是否多选    one  是多选， 其它为
	 *  iffirm 是否为分店  （mis报表中 查询分店的档口专用 wangjie）
	 */
	@RequestMapping(value = "/findPositnSuper")
	public ModelAndView findPositnSuper(ModelMap modelMap,Positn positn,String mold, String defaultName,String defaultCode, Page page,String iffirm,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		positn.setAccountId(session.getAttribute("accountId").toString());
		positn.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
//		String positninit=positn.getTypn();
		
		if(positn.getTypn().equals("1")){
			positn.setTypn("'1201'");
		}else  if(positn.getTypn().equals("2")){
			positn.setTypn("'1202'");
		}else  if(positn.getTypn().equals("3")){
			positn.setTypn("'1203'");
		}else  if(positn.getTypn().equals("4")){
			positn.setTypn("'1204'");
		}else  if(positn.getTypn().equals("5")){
			positn.setTypn("'1205'");
		}else  if(positn.getTypn().equals("6")){
			positn.setTypn("'1206'");
		}else  if(positn.getTypn().equals("7")){
			positn.setTypn("'1207'");
		}else  if(positn.getTypn().equals("1-2-4")){// 以下照着这个去写
			positn.setTypn("'1201','1202','1204'");
		}else  if(positn.getTypn().equals("1-2-3")){
			positn.setTypn("'1201','1202','1203'");
		}else  if(positn.getTypn().equals("2-4")){
			positn.setTypn("'1202','1204'");
		}else  if(positn.getTypn().equals("3-7")){
			positn.setTypn("'1203','1207'");
		}else  if(positn.getTypn().equals("1-2-3-4-5-6-7")){
			positn.setTypn("'1201','1202','1203','1204','1205','1206','1207'");
		}
		
		List<Positn> positns = new ArrayList<Positn>();
		if(!"".equals(iffirm) && iffirm != null){
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				positn.setUcode(accountPositn.getPositn().getCode());
			}
			Positn positn2 = new Positn();
			positn2.setCode(accountPositn.getPositn().getCode());
			positn2.setTypn("'1203'");
			positn2.setDes(positn.getDes());
			positn2.setInit(positn.getInit());
			List<Positn> list = positnService.findPositnSuper(positn2,page);
			if(list !=null && list.size() == 1){
				positns.add(list.get(0));
			}
		}
		
		CodeDes code = new CodeDes();
		Page p = new Page();
		p.setPageSize(Integer.MAX_VALUE);
		code.setTyp(String.valueOf(CodeDes.S_SYQ));
		modelMap.put("listMod", codeDesService.findCodeDes(code, p));
		code.setTyp(String.valueOf(CodeDes.S_AREA));
		modelMap.put("listArea", codeDesService.findCodeDes(code, p));
		List<Positn> listPositn = positnService.findPositnSuper(positn,page);
		positns.addAll(listPositn);
		modelMap.put("listPositn", positns);
		modelMap.put("listPositnTyp", positnService.findAllPositnTyp());
		modelMap.put("queryPositn", positn);
		if(null!=defaultName){
			modelMap.put("defaultName", URLDecoder.decode(defaultName, "UTF-8"));
		}
		modelMap.put("defaultCode", defaultCode);
		modelMap.put("pageobj", page);
		modelMap.put("iffirm", iffirm);
		if("one".equals(mold)){//选择单条仓位
			return new ModelAndView(PositnConstants.SELECT_ONEPOSITN_SUPER, modelMap);
		}else if("oneTone".equals(mold)){//同一类别仓位单选
			return new ModelAndView(PositnConstants.SELECT_ONE_TO_ONEPOSITN_SUPER, modelMap);
		}else if("oneTmany".equals(mold)){//同一类别仓位多选
			return new ModelAndView(PositnConstants.SELECT_ONE_TO_MANYPOSITN_SUPER, modelMap);
		}else{
			return new ModelAndView(PositnConstants.SELECT_POSITN_SUPER, modelMap);
		}
	}
}
