<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>添加列信息</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<style type="text/css">
				.userInfo,.accountInfo {
					position: relative;
					top: 1px;
					background-color: #E1E1E1;
				}
				
				.userInfo {
					height: 91px;
					line-height: 91px;
				}
				
				.accountInfo {
					height: 91px;
					line-height: 91px;
				}
				
				.accountInfo .form-label{
					width: 40%;
				}
				
			</style>
	</head>
	<body>
	<div class="form">
			<div>
			<form id="columnForm" method="post" action="<%=path %>/columnChoose/${action}">
				<input type="hidden" name="sysName" value="${sysName}" />
				<div class="form-line">
				<div class="form-label"><span class="red">*</span>编号</div>
				<div class="form-input">
					<input type="text" id="id" name="id" class="text" value="${column.id}"/>
				</div>
				<div class="form-label">列名</div>
				<div class="form-input">
					<input type="text" id="columnName" name="columnName" class="text" value="${column.columnName}"/>
				</div>
			</div>
			<div class="form-line">
				<div class="form-label">中文名</div>
				<div class="form-input">
					<input type="text" id="zhColumnName" name="zhColumnName" class="text" value="${column.zhColumnName}"/>
				</div>
				<div class="form-label">属性</div>
				<div class="form-input">
					<input type="text" id="properties" name="properties" class="text" value="${column.properties}"/>
				</div>
			</div>
			<div class="form-line">
				<div class="form-label">宽度</div>
				<div class="form-input">
					<input type="text" id="columnWidth" name="columnWidth" class="text" value="${column.columnWidth}"/>
				</div>
				<div class="form-label">类型</div>
				<div class="form-input">
					<select id="columnType" name="columnType" class="select">
						<option value="text">字符串</option>
						<option value="digital" <c:if test="${column.columnType=='digital'}">selected = "selected"</c:if>>数字</option>
					</select>
				</div>
			</div>
			<div class="form-line">
				<div class="form-label">表名</div>
				<div class="form-input">
					<input type="text" id="tableName" name="tableName" class="text" value="${column.tableName}"/>
				</div>
				<div class="form-label"></div>
				<div class="form-input"></div>
			</div>
			</form>
			</div>
		</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'id',
						validateType:['canNull','num'],
						param:['F','F'],
						error:['编码不能为空！','编码不是有效数字！']
					},{
						type:'text',
						validateObj:'columnWidth',
						validateType:['num'],
						param:['F'],
						error:['宽度必须为数字']
					}]
				});
				
			});
		</script>
	</body>
</html>