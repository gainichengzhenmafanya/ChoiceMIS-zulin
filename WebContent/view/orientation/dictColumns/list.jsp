<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>列选择维护</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<style type="text/css">
			.tr-select{
				background-color: #D2E9FF;
			}
			.leftFrame{
				width : 23%;
			}
			.mainFrame{
				width : 77%
			}
			.condition{
				border: 1px solid #5A7581;
				margin: 0px 0px 0px 0px;
			 	padding: 1px;
			 	padding-left: 0px;
			 	background-color:#CDDCEE;
			}
		</style>
	</head>
	<body>
	<div class="leftFrame">
		<div class="condition">
			<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;当前系统：</span>
			<span>
				<select id="sysName" style="width:80px;margin-left:0px;">
					<option value="scm">物流</option>
					<option value="tele">决策</option>
				</select>
			</span>
		</div>
		<div class="grid">
			<div class="table-head" >
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<td><span style="width:232px;">表名</span></td>
						</tr>
					</thead>
				</table>
			</div>
			<div class="table-body">
				<table cellspacing="0" cellpadding="0" id="tableList">
					<tbody></tbody>
				</table>
			</div>			
		</div>
	</div>
	<div class="mainFrame">
      <div class="tool"></div>
		<div class="grid">
			<div class="table-head" >
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<td class="num" style="width: 25px;">&nbsp;</td>
							<td style="width:30px;">
								<input type="checkbox" id="chkAll"/>
							</td>
							<td><span style="width:40px;">编号</span></td>
							<td><span style="width:150px;">列名</span></td>
							<td><span style="width:150px;">中文名</span></td>
							<td><span style="width:100px;">属性</span></td>
							<td><span style="width:50px;">宽度</span></td>
							<td><span style="width:100px;">类型</span></td>
						</tr>
					</thead>
				</table>
			</div>
			<div class="table-body">
				<table cellspacing="0" cellpadding="0" id="columnsList">
					<tbody></tbody>
				</table>
			</div>
		</div>
   	</div> 
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript">
			var popWindow;
			$(document).ready(function(){
				//初始化页面
				loadToolBar();
				loadContent();
				initPage();
				//添加事件
				$('#sysName').change(function(){
					loadContent();
				});
				
				$('#tableList>tbody>tr').live('click',function(){
					$('.tr-select').removeClass("tr-select");
					$(this).addClass("tr-select");
					loadCols();
				});
			});
			
			
			//加载页面内容
			function loadContent(){
				$('#tableList>tbody').load("<%=path %>/columnChoose/listTable.do",{
					sysName:$('#sysName').val()
				},function(){
					$('#columnsList>tbody').load("<%=path %>/columnChoose/listColumns.do",{
						sysName:$('#sysName').val(),
						tableName:$.trim($('#tableList>tbody>tr>td:first').text())
					});
					$('#tableList>tbody>tr:first').addClass("tr-select");
				});
			}
			//加载工具条按钮
			function loadToolBar(){
				$('.tool').toolbar({
					items: [{
							text: '新增',
							title: '项目新增',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								var url = "<%=path%>/columnChoose/addColumn.do?sysName=" + $('#sysName').val() + "&tableName=" + $.trim($('#tableList>tbody>.tr-select>td').text());
								popWindow = $('body').window({
									id: 'window_saveColumnChoose',
									title: '添加列',
									content: '<iframe id="saveColumnFrame" frameborder="0" src=' + url + '></iframe>',
									width: 650,
									height: 300,
									draggable: true,
									isModal: true,
									topBar: {
										items: [{
												text: '保存',
												title: '添加列',
												icon: {
													url: '<%=path%>/image/Button/op_owner.gif',
													position: ['-80px','-0px']
												},
												handler: function(){
													if(getFrame('saveColumnFrame') && getFrame('saveColumnFrame').validate._submitValidate()){
														submitFrameForm('saveColumnFrame','columnForm');
													}
												}
											},{
												text: '取消',
												title: '取消',
												icon: {
													url: '<%=path%>/image/Button/op_owner.gif',
													position: ['-160px','-100px']
												},
												handler: function(){
													$('.close').click();
												}
											}
										]
									}
								});
							}
						},{
							text: '编辑',
							title: '项目编辑',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								if($(".mainFrame>.grid>.table-body>table>tbody>tr>td>:checkbox:checked").size() != 1){
									alert("请选择单条数据！");
									return;
								}
								var url = "<%=path%>/columnChoose/updateColumn.do?sysName=" + $('#sysName').val() + "&id=" + $(".mainFrame>.grid>.table-body>table>tbody>tr>td>:checkbox:checked").val() + "&tableName=" + $.trim($('#tableList>tbody>.tr-select>td').text());
								popWindow = $('body').window({
									id: 'window_updateColumnChoose',
									title: '编辑列',
									content: '<iframe id="updateColumnFrame" frameborder="0" src=' + url + '></iframe>',
									width: 650,
									height: 300,
									draggable: true,
									isModal: true,
									topBar: {
										items: [{
												text: '保存',
												title: '保存',
												icon: {
													url: '<%=path%>/image/Button/op_owner.gif',
													position: ['-80px','-0px']
												},
												handler: function(){
													if(getFrame('updateColumnFrame') && getFrame('updateColumnFrame').validate._submitValidate()){
														submitFrameForm('updateColumnFrame','columnForm');
													}
												}
											},{
												text: '取消',
												title: '取消',
												icon: {
													url: '<%=path%>/image/Button/op_owner.gif',
													position: ['-160px','-100px']
												},
												handler: function(){
													$('.close').click();
												}
											}
										]
									}
								});
							}
						},{
							text: '删除',
							title: '项目删除',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								var chk = $(".mainFrame>.grid>.table-body>table>tbody>tr>td>:checkbox:checked");
								var val = [];
								for(var i = 0 ; i < chk.size() ; i ++)val.push(chk[i].value);
								var url = "<%=path%>/columnChoose/delColumn.do?sysName=" + $('#sysName').val() + "&id=" + val.join(',');
								$.post(url,function(){
									showMessage({
			  							type: 'success',
			  							msg: '删除成功！',
			  							speed: 1500
									});
									loadCols();
								});
							}
						},{
							text: '退出',
							title: '退出',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.tab-item .button-click',top.document).find('.button-arrow').click();									
							}
						}
					]
				});
			}
			//计算初始化页面布局
			function initPage(){
				$(".condition").height($(".tool").height()-2);
				$(".mainFrame").width($(".mainFrame").width()-3);
				$(".mainFrame").css("margin-left","1px");
				$(".table-body").height($('body').height() - $(".table-head").height() - $('.tool').height() - 30);
				$(".table-body").css("overflow","auto");
			}
			
			function loadCols(){
				$('#columnsList>tbody').load("<%=path %>/columnChoose/listColumns.do",{
					sysName:$('#sysName').val(),
					tableName:$.trim($('#tableList>tbody>.tr-select>td').text())
				});
			}
			
			function pageReload(){
				if(popWindow)popWindow.close();
				loadCols();
			}
		</script>
	</body>
</html>