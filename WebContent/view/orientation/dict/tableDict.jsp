<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="system_encoding" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<style type="text/css">
		.page{margin-bottom: 25px;}
		</style>
	</head>	
	<body>
		<div class="tool"></div>
		<form id="listForm" action="<%=path%>/dict/table.do" method="post">
			<input type="hidden" id="id" name="id" value="${id }"/>
	    	<input type="hidden" id="typ" name="typ" value="${typ }"/>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 25px;"><span style="width:25px;">&nbsp;</span></td>
								<td><span style="width:30px;"><input type="checkbox" id="chkAll"/></span></td>
								<td><span style="width:250px;">参数名称</span></td>
								<td><span style="width:100px;">参数值</span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="dict" items="${dictList}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;"><span style="width:25px;">${status.index+1}</span></td>
									<td><span style="width:30px; text-align: center;">
										<input type="checkbox" name="idList" id="chk_${dict.id}" value="${dict.id}"/></span>
									</td>
									<td><span title="${dict.enum_meaning}"  style="width:250px;">${dict.enum_meaning}</span></td>
									<td><span title="${dict.enum_value}" style="width:100px;">${dict.enum_value}</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>			
		</form>			    
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">		
		//工具栏
			$(document).ready(function(){
				if($("#typ").val()!="root"){
					loadToolBar([false,true,true]);
				}else{
					loadToolBar([true,false,false]);
				}
				//按钮快捷键
				focus() ;//页面获得焦点
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
			 	});
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),30);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
			   $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			   $('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				setElementHeight('.table-body',['.table-head'],'.grid');								
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }
				     else
				     {
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				});
			});	
		
		 	//控制按钮显示
			function loadToolBar(use){
				$('.tool').html('');
				$('.tool').toolbar({
					items: [{
							text: '<fmt:message key="insert" />',
							title: '<fmt:message key="add_systemcoding_information" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')}&&use[0],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){								
								if($("#typ").val()!="root"){	
									alert('<fmt:message key="not_allowed_added_items" />');	
									return;
								}else{
									saveDict('<%=path%>');
								}
							}
						},{
							text: '<fmt:message key="update" />',
							title: '<fmt:message key="modify_systemcoding_information" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[1],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								if($("#typ").val()=="root"){
									alert('<fmt:message key="not_allowed_deleted_items" />');	
									return;
								}else{
									updateDictLeft();
								}
							}
						},{
							text: '<fmt:message key="delete" />',
							title: '<fmt:message key="delete_systemcoding_information" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')}&&use[2],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								if($("#typ").val()=="root"){
									alert('<fmt:message key="not_allowed_deleted_items" />');	
									return;
								}else{
									deleteDictLeft();
								}
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){								
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));	
							}
						},{
							text: '脚本升级',
							title: '脚本升级',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){								
								doSql();
							}
						}
					]
				});
		 	}	
			//新增
			function saveDict(path){
				$('body').window({
					id: 'window_dict',
					title: '新增字典信息',
					content: '<iframe id="saveDictFrame" frameborder="0" src="'+path+'/dict/addLeft.do"></iframe>',
					width: '650px',
					height: '500px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								title: '保存字典信息',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('saveDictFrame')&&window.document.getElementById("saveDictFrame").contentWindow.validate._submitValidate()){
										submitFrameForm('saveDictFrame','DictForm');
									}
								}
							}
						]
					}
				});
			}
			//修改
	  	 	function updateDictLeft(){
	  	 		var id=$('#id').val();
	  	 		var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() ==1){
					var chkValue = [];
					checkboxList.filter(':checked').each(function(){
						chkValue.push($(this).val());
					});
					$('body').window({
						title: '修改字典信息',
						content: '<iframe id="updateDictFrame" frameborder="0" src="<%=path%>/dict/updateLeft.do?id='+id+'"></iframe>',
						width: '650px',
						height: '500px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="update" />',
									title: '修改字典信息',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('updateDictFrame')&&window.document.getElementById("updateDictFrame").contentWindow.validate._submitValidate()){
											submitFrameForm('updateDictFrame','DictForm');
										}
									}
								}
							]
						}
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size() > 1){
					alert('<fmt:message key="please_select_data" />！');
					return ;
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_modify" />！');
					return ;
				}
			}
	  	 	//删除
			function deleteDictLeft(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() > 0){
					if(confirm("是否要删除数据？")){
						var chkValue = [];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
						});
						var action = '<%=path%>/dict/delete.do?id='+chkValue.join(",");
						$('body').window({
							title: '<fmt:message key="delete_systemcoding_information" />',
							content: '<iframe frameborder="0" src='+action+'></iframe>',
							width: '500px',
							height: '245px',
							draggable: true,
							isModal: true
						});
					}
				}else{
					alert('请先选择需要删除的信息！');
					return ;
				}
			}
	  	 	//脚本升级
	  	 	function doSql(){
	  	 		var action = '<%=path%>/teShuCaoZuo/doSql.do';
	  	 		$.ajax({
	  	 			type:'post',
	  	 			url:action,
	  	 			success:function(data){
	  	 				if(data == '0'){
	  	 					alert("执行成功！");
	  	 				}else if(data == '-2'){
	  	 					alert("没有可执行的sql文件。");
	  	 				}else{
	  	 					alert("执行失败！请检查sql文件。");
	  	 				}
	  	 			}
	  	 		});
	  	 	}
		</script>
	</body>
</html>