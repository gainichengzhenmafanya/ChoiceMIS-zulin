<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>学生信息管理</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<style type="text/css">
				.tool{
					position: relative;
					height: 27px;
				}
			</style>
		</head>
	<body>
	  <div class="mainTool" style="float: left;border: 1px solid #999;background-color: #E1E1E1;height: 25px;width: 100%;"> 
			<div class="tool" style="float: left;"></div>
			<div style="float: left;border: 1px solid #999;background-color: #E1E1E1;height: 25px;">
				<select style="WIDTH: 100pt" name="enterState" id="enterState" class="select" onchange="StateChange()">
						<option value="">--请选择报到状态--</option>	
							<option <c:if test="${state eq '0' }">selected="selected"</c:if>  value="0">已报到</option>
							<option <c:if test="${state eq '1' }">selected="selected"</c:if>value="1">未报到</option>
				</select>
			
			</div>
 	</div>
		<form id="listForm" action="<%=path %>/studentInfo/toManage.do" method="post">
			<div class="grid" style="float: left;">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td style="width:30px;">&nbsp;</td>
								<td style="width:30px;">
									<input type="checkbox" id="chkAll"/>
								</td>
								<c:forEach var="dictColumns" items="${dictColumnsListByAccount}">
									<td style="width:${dictColumns.columnWidth}px;"><span>
										${dictColumns.zhColumnName}
										</span>
									</td>
								</c:forEach>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="basicInfo" varStatus="step" items="${listStudent}">
								<tr>
									<td class="num" style="width:30px;">${step.count}</td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" name="idList" id="chk_${basicInfo.id}" value="${basicInfo.id}"/>
									</td>
									<c:forEach var="dictColumns" items="${dictColumnsListByAccount}">
										<td  align="center">
											<span style="width:${dictColumns.columnWidth-10}px;"><span>
											<c:choose> 
											       <c:when test="${elf:append(basicInfo,dictColumns.properties) eq '0'}"> 
											          		   未报到
											       </c:when> 
											       <c:when test="${elf:append(basicInfo,dictColumns.properties) eq '1'}"> 
											          		已报到
											       </c:when> 
											       <c:when test="${elf:append(basicInfo,dictColumns.properties) eq 'F'}"> 
											          		   在校
											       </c:when> 
											       <c:when test="${elf:append(basicInfo,dictColumns.properties) eq 'T'}"> 
											          		已退学
											       </c:when> 
											       <c:otherwise> 
											           	${elf:append(basicInfo,dictColumns.properties)}
											       </c:otherwise> 
											</c:choose> 
											</span></span>
										</td>
									</c:forEach>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<page:page form="campusForm" page="${pageobj}"></page:page>
		<div class="search-div" id="search-div">
		<form id="campusForm" method="post" action="<%=path%>/studentInfo/toManage.do">
			<div class="search-condition">
				<table class="search-table" cellspacing="0" cellpadding="0">
					<tr>
						<td class="c-left">学生姓名：</td>
						<td>
							<input type="text" id="name" name="name" class="text" value="${queryobj.name }" />
						</td>
						<td class="c-left">准考证号：</td>
						<td>
						<input type="text" id="admissionCard" name="admissionCard" class="text" value="${queryobj.admissionCard }" />
						</td>
						<td class="c-left">身份证号：</td>
						<td>
						<input type="text" id="identityId" name="identityId" class="text" value="${queryobj.identityId }" />
						</td>
					</tr>
					<tr>
						<td class="c-left">投档志愿：</td>
						<td>
						<select style="WIDTH: 100pt" name="sendWish.id" id="sendWishId">
						<option value="">--请选择投档志愿--</option>
						<c:forEach var="sendWish" items="${throwWish}">
							<option value="${sendWish.id}"
								<c:if test="${queryobj.sendWish.id eq sendWish.id }">
									selected="selected"
								</c:if>
							>${sendWish.enum_meaning}</option>
						</c:forEach>
					</select>
						</td>
						<td class="c-left">录取批次：</td>
						<td>
						<select style="WIDTH: 100pt" name="ascertain.id" id="ascertainId">
						<option value="">--请选择录取批次--</option>
						<c:forEach var="ascertain" items="${PC}">
							<option value="${ascertain.id}"
								<c:if test="${queryobj.ascertain.id eq ascertain.id }">
									selected="selected"
								</c:if>
							>${ascertain.enum_meaning}</option>
						</c:forEach>
					</select>
						</td>
						<td class="c-left">入学时间：</td>
						<td>
						<input name="enterTime" id="enterTime" type="text"  size="18" class="Wdate" onclick="WdatePicker({dateFmt:'yyyy'});" />
						</td>
					</tr>
				</table>
			</div>
			<div class="search-commit">
				<input type="button" class="search-button" id="search" value="查询" />
				<input type="button" class="search-button" onclick="cleanForm();" value="清空" />
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
	  		<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
			</div>
		</form>
	</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript"  src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			var tool = $('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '查询报考人员信息',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							 $('#search-div').slideToggle(100);
						}
					},{
						text: '<fmt:message key="update" />',
						title: '选择字段',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-18px','0px']
						},
						handler: function(){
							update();
						}
					},{
						text: '<fmt:message key="column_selection" />',
						title: '选择字段',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-100px','-60px']
						},
						handler: function(){
							toSelectColumns();
						}
					},{
						text: '批量修改',
						title: '选择字段',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-100px','-60px']
						},
						handler: function(){
							updateColumns();
						}
					},{
						text: '<fmt:message key="delete" />',
						title: '删除信息',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							deleteBasicInfo();
						}
					},{
						text: 'Excel导入',
						title: '导入信息',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							saveStudent();
						}
					},{
						text: 'DBF导入',
						title: '导入信息',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							importDBF();
						}
					}
				]
			});
			setElementHeight('.grid',['.mainTool'],$(document.body),27);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
			loadGrid();
			
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
			$("#search").bind('click', function() {
				$('.search-div').hide();
				$('#campusForm').submit();
			});
			
		});
		
			function saveStudent(){
				$('body').window({
					title: '保存',
					content: '<iframe id="importEnrollStudentFrame" frameborder="0" src="<%=path%>/studentInfo/initImportBasicInfo.do"></iframe>',
					width: '420px',
					height: '150px',
					draggable: true,
					isModal: true
				});
			}
			
			function importDBF(){
				$('body').window({
					title: '保存',
					content: '<iframe id="importDBFFrame" frameborder="0" src="<%=path%>/studentInfo/initImportDBF.do"></iframe>',
					width: '420px',
					height: '150px',
					draggable: true,
					isModal: true
				});
			}
			
		function cleanForm(){
			$('input[type!="button"]').val('');
			$('select').val('');
		}
		function toSelectColumns(){
			$('body').window({
				title: '列选择',
				content: '<iframe frameborder="0" src="<%=path%>/studentInfo/toColumnsChoose.do"></iframe>',
				width: '460px',
				height: '430px',
				draggable: true,
				isModal: true
				});
		}
		function updateColumns(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList 
					&& checkboxList.filter(':checked').size() >0){
				var chkValue = [];
				checkboxList.filter(':checked').each(function(){
				chkValue.push($(this).val());
				});
				var ids=chkValue.join(",");
			$('body').window({
				title: '修改',
				content: '<iframe frameborder="0" src="<%=path%>/studentInfo/toUpdate.do?ids='+ids+'"></iframe>',
				width: '460px',
				height: '430px',
				draggable: true,
				isModal: true
				});
		}else{
			alert("请选择要修改的信息！");
		}
	}
		function deleteBasicInfo(){
			if(getAllID()){
				if(confirm("是否要删除数据？")){
					var action = '<%=path%>/studentInfo/deleteInfo.do?ids='+getAllID();
					$('#listForm').attr('action',action);
					$('#listForm').submit();
				}
			}else{
				alert('请先选择需要删除的信息！');
				return ;
			}
		}
		function update(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList 
					&& checkboxList.filter(':checked').size() == 1){
				var chkValue = checkboxList.filter(':checked').eq(0).val();
				$('body').window({
					title: '修改',
					content: '<iframe id="updateFrame" frameborder="0" src="<%=path%>/studentInfo/findStudentToUpdate.do?id='+chkValue+'"></iframe>',
					width: 790,
					height: '600px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								title: '保存',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('updateFrame')){
										submitFrameForm('updateFrame','updateInfo');
									}
								}
							}
						]
					}
				});
			}else if(checkboxList 
					&& checkboxList.filter(':checked').size() > 1){
				alert('只能修改一条数据！');
				return ;
			}else{
				alert('请选择数据！');
				return ;
			}
		}
		function StateChange(){
			var id = document.getElementById("enterState").value;
			location.href="<%=path%>/studentInfo/toManage.do?enterState="+id;

		}
	</script>
	</body>
</html>