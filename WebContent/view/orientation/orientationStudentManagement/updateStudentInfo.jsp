<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path=request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新生信息更改</title>
<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		
			<style type="text/css">
				.tool {
					position: relative;
					height: 27px;
				}
				fieldset{
					padding: 5px;
					margin: 10px 6px;
					border: 1px solid #868686;
				}
				
				legend{
					font-size: 13px;
					font-weight: bold;
					font-style: italic;
					cursor: pointer;
					border: 1px solid #868686;
					padding-left: 20px;
				}
				.border_hover{
					border: 2px solid #4F98F6;
				}
				
				.color_hover{
					background-color: #4F98F6;
					color: #FFFFFF;
				}
				
			</style>
		</head>
<body>
	<div class="tool"></div>
	<div id="info" style="overflow: scroll;" align="center">
	<form action="<%=path %>/studentInfo/updateStudentInfo.do" name="updateInfo" id="updateInfo" method="post" >
		  <fieldset>
			 <legend>个 人基 本  信 息（* 为必填项）</legend>
			 <div class="form-line">
			 	<div class="form-label"><span style="color: red;">*</span>姓 名</div>
			 	<div class="form-input">
			 		<input type="text" name="name" id="name" maxLength=5 value="${Student.name}"/>
			 	</div>
			 	<div class="form-label"><span style="color: red;">*</span>性别</div>
				<div class="form-input">
					<select name="sex.id" id="sexId">
						<option value="">--请选择性别--</option>
						<c:forEach var="sex" items="${sexList}">
							<option
							<c:if test="${sex.id eq Student.sex.id }">selected="selected"</c:if>
							 value="${sex.id}">${sex.enum_meaning}</option>
						</c:forEach>
					</select>
				</div>
		 		<div class="form-label"><span style="color: red;">*</span>民 族</div>
				<div class="form-input">
					<select  name="nationality.id" id="nationalityId">
						<option value="">--请选择民族--</option>
						<c:forEach var="nationality" items="${nationalityList}">
							<option
							<c:if test="${nationality.id eq Student.nationality.id}">selected="selected"</c:if>
							 value="${nationality.id}">${nationality.enum_meaning}</option>
						</c:forEach>
					</select>
				</div>
		    </div>
		    <div class="form-line">
		    	<div class="form-label"><span style="color: red;">*</span>身份证号</div>
		    	<div class="form-input">
		    		<input maxLength=18 type="text" name="identityId" id="identityId"  onblur="checkIndentity('identityId')" value="${Student.identityId}"/>
		    	</div>
		    	<div class="form-label">出生年月</div>
		    	<div class="form-input">
					<input type="text" id="birthday" name="birthday" readOnly="readonly" value="${Student.birthday}"/>
		    	</div>
		    	<div class="form-label">籍贯</div>
		    	<div class="form-input">
		    		<input type="text" name="nativePlaceName" id="nativePlaceName" readOnly="readonly" value="${Student.nativePlace.name}"/>
		    	</div>
		    </div>
		      <div class="form-line">
		       <div class="form-label">出生地</div>
		 		<div class="form-input">
						<select style="WIDTH: 110pt" id="birthAddressProvince" name="province" onchange="getTown('birthAddressProvince','birthAddressCity');">
							<option value="">--省--</option>
								<c:forEach var="province" items="${provinceList}">
									<option value="${province.code}"
									<c:if test="${fn:substring(province.code,0,2) eq fn:substring(basicInfo.birthAddress.code,0,2)}">
										selected="selected"
									</c:if>
									>${province.name}</option>
								</c:forEach>
						</select>
					</div>	
			  <div class="form-label">市</div>
					<div class="form-input">
						<select style="WIDTH: 110pt" id="birthAddressCity" name="city" onchange="getTown('birthAddressCity','birthplaceCode');">
							<option value="">--市区--</option>
							<c:forEach var="city" items="${cityList}">
								<option value="${city.code}"
								<c:if test="${fn:substring(city.code,0,4) eq fn:substring(basicInfo.birthAddress.code,0,4)}">
									selected="selected"
								</c:if>
								>${city.name}</option>
							</c:forEach>
						</select>
					</div>
				<div class="form-label">县</div>
					<div class="form-input">	
						<select style="WIDTH: 110pt" id="birthplaceCode" name="birthplace.code">
							<option value="">--县--</option>
							<c:forEach var="town" items="${townList}">
								<option value="${town.code}"
								<c:if test="${town.code eq basicInfo.birthAddress.code}">
									selected="selected"
								</c:if>
								>${town.name}</option>
							</c:forEach>
						</select>
					</div>
		      </div>
		       <div class="form-line">
		      	<div class="form-label"><span style="color: red;">*</span>入学时间</div>
					<div class="form-input">
						<input name="enterTime" id="enterTime" type="text"  size="18" class="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM'});" value="${Student.enterTime}"/>
					</div>
				<div class="form-label"><span style="color: red;">*</span>录取专业</div>
				<div class="form-input">
					<input type="text" id="professionIdName" name="professionId.zymc" class="selectDepartment" value="${Student.professionId.zymc}"/>
					<input type="hidden" id="professionId" name="professionId.id" class="text"  value="${Student.professionId.id}"/>
				</div>
				<div class="form-label">手机号码</div>
			 		<div class="form-input">
			 		<input type="text" name="tel" id="tel" value="${Student.tel}" maxLength=11>
			 		</div>
				</div>
		 </fieldset>
		 <fieldset>
			<legend>入学信息（* 为必填项）</legend>
				<div class="form-line">	
				<div class="form-label">报到状态</div>
				<div class="form-input">
					<select style="WIDTH: 100pt" name="enterState" id="enterState" class="select">
						<option value="">--请选择报到状态--</option>	
							<option <c:if test="${Student.enterState eq '0' }">selected="selected"</c:if>  value="0">已报到</option>
							<option <c:if test="${Student.enterState eq '1' }">selected="selected"</c:if>value="1">未报到</option>
					</select>
				</div>	
				<div class="form-label">通知书编号</div>
				<div class="form-input">
					<input type="text" name="noteId" id="noteId" maxLength=20 value="${Student.noteId}"/>
				</div>
				<div class="form-label">报名表编号</div>
				<div class="form-input">
					<input type="text" name="enterFormId" id="enterFormId" maxLength=20 value="${Student.enterFormId}"/>
				</div>		
			</div>
			<div class="form-line">
				<div class="form-label">学制</div>
				<div class="form-input">
						<select style="WIDTH: 100pt" name="educationSys.id" id="educationSysId">
						<option value="">--请选择学制--</option>
						<c:forEach var="education" items="${educationSys}">
							<option 
							<c:if test="${education.id eq Student.educationSys.id}">selected="selected"</c:if>
							value="${education.id}">${education.enum_meaning}</option>
						</c:forEach>
					</select>
				</div>
				<div class="form-label">考生特征</div>
				<div class="form-input">
				<input type="text" id="examineeCharacter" name="examineeCharacter" value="${Student.examineeCharacter}" maxLength=32 />
				</div>
				<div class="form-label">培养方式</div>
				<div class="form-input">
					<select style="WIDTH: 100pt" name="cultureStyle.id" id="cultureStyleId">
						<option value="">--请选择培养方式--</option>
						<c:forEach var="cultureStyle" items="${CultrueWayList}">
							<option 
							<c:if test="${cultureStyle.id eq Student.cultureStyle.id}">selected="selected"</c:if>
							value="${cultureStyle.id}">${cultureStyle.enum_meaning}</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="form-line">
				<div class="form-label">专业1</div>
				<div class="form-input">
				<input type="text" id="professionIdFirstName" name="professionIdFirst.zymc" value="${Student.professionIdFirst.zyjc}" class="selectDepartment"/>
					<input type="hidden" id="professionIdFirstId" name="professionIdFirst.id" value="${Student.professionIdFirst.id}" class="text"/>
				</div>
				<div class="form-label">专业2</div>
				<div class="form-input">
				<input type="text" id="professionIdSecondName" name="professionIdSecond.zymc" value="${Student.professionIdSecond.zyjc}" class="selectDepartment"/>
					<input type="hidden" id="professionIdSecondId" name="professionIdSecond.id" value="${Student.professionIdSecond.id}" class="text"/>
				</div>
				<div class="form-label">专业3</div>
				<div class="form-input">
				<input type="text" id="professionIdThirdName" name="professionIdThird.zymc" value="${Student.professionIdThird.zyjc}" class="selectDepartment"/>
				<input type="hidden" id="professionIdThirdId" name="professionIdThird.id" value="${Student.professionIdThird.id}" class="text"/>
				</div>
			</div>
			<div class="form-line">
				<div class="form-label">专业4</div>
				<div class="form-input">
				<input type="text" id="professionIdFourthName" name="professionIdFourth.zymc"  value="${Student.professionIdFourth.zyjc}" class="selectDepartment"/>
					<input type="hidden" id="professionIdFourthId" name="professionIdFourth.id" value="${Student.professionIdFourth.id}" class="text"/>
				</div>
				<div class="form-label">专业5</div>
				<div class="form-input">
				<input type="text" id="professionIdFifthName" name="professionIdFifth.zymc" value="${Student.professionIdFifth.zyjc}" class="selectDepartment"/>
					<input type="hidden" id="professionIdFifthId" name="professionIdFifth.id" value="${Student.professionIdFifth.id}" class="text"/>
				</div>
				<div class="form-label">专业6</div>
				<div class="form-input">
				<input type="text" id="professionIdSixName" name="professionIdSix.zymc" value="${Student.professionIdSix.zyjc}" class="selectDepartment"/>
					<input type="hidden" id="professionIdSixId" name="professionIdSix.id" value="${Student.professionIdSix.id}" class="text"/>
				</div>
			</div>
			<div class="form-line">
			
				<div class="form-label">批次</div>
				<div class="form-input">
					<select style="WIDTH: 100pt" name="ascertain.id" id="ascertainId">
						<option value="">--请选择录取批次--</option>
						<c:forEach var="ascertain" items="${PC}">
							<option
							<c:if test="${ascertain.id eq Student.ascertain.id}">selected="selected"</c:if>
							 value="${ascertain.id}">${ascertain.enum_meaning}</option>
						</c:forEach>
					</select>
				</div>
				<div class="form-label">专业科类</div>
				<div class="form-input">
					<select style="WIDTH: 100pt" name="professionCode.id" id="professionCodeId">
						<option value="">--请选择专业科类--</option>
						<c:forEach var="professionCode" items="${specialized}">
							<option 
							<c:if test="${professionCode.id eq Student.professionCode.id}">selected="selected"</c:if>
							value="${professionCode.id}">${professionCode.enum_meaning}</option>
						</c:forEach>
					</select>
				</div>
						<div class="form-label">政治面貌</div>
		 		<div class="form-input">
		 			<select style="WIDTH: 100pt" name="politicsFace.id" id="politicsFaceId">
						<option value="">--请选择政治面貌--</option>
						<c:forEach var="politicsFace" items="${featureList}">
							<option
							<c:if test="${politicsFace.id eq Student.politicsFace.id}">selected="selected"</c:if>
							 value="${politicsFace.id}">${politicsFace.enum_meaning}</option>
						</c:forEach>
					</select>
		 		</div>
			</div>
		</fieldset>
		 <fieldset>
		 	 <legend>联系方式（* 为必填项）</legend>
		 	  <div class="form-line">
		 		<div class="form-label">电子邮箱</div>
		 		<div class="form-input">
		 		<input type="text" name="email" id="email" maxLength=30 value="${Student.email}" onblur="check('email')"/>
		 		</div>
		 		<div class="form-label">邮政编码</div>
		 		<div class="form-input">
		 		<input type="text" name="postCode" id="postCode" value="${Student.postCode}" maxLength=6 onblur="check('postCode')">
		 		</div>
		 			<div class="form-label">婚姻状况</div>
		 		<div class="form-input">
		 			<select style="WIDTH: 100pt" name="marriageState.id" id="marriageStateId">
						<option value="">--请选择婚姻状况--</option>
						<c:forEach var="marriageState" items="${marryState}">
							<option 
							<c:if test="${marriageState.id eq Student.marriageState.id}">selected="selected"</c:if>
							value="${marriageState.id}">${marriageState.enum_meaning}</option>
						</c:forEach>
					</select>
		 		</div>
		   	</div>
		   	 <div class="form-line">
		 		<div class="form-label">邮寄地址</div>
		 		<div class="form-input">
		 		<input type="text" name="mailAddress" id="mailAddress" value="${Student.nowlive}"/>
		 		</div>
		 		<div class="form-label">收件人</div>
		 		<div class="form-input">
		 		<input type="text" name="addressee" id="addressee" maxLength=10 value="${Student.name}"/>
		 		</div>
		   	</div>
		 </fieldset>
		 <fieldset>
		 	<legend>学生信息（* 为必填项）</legend>
		 	<div class="form-line">
		 		<div class="form-label">现住址</div>
		 		<div class="form-input">
		 			<input type="text" name="nowlive" id="nowliveId" value="${Student.nowlive}"/>
		 		</div>
		 		<div class="form-label">户口所在地</div>
		 		<div class="form-input">
		 			<input type="text" name="hkLocal" id="hkLocal" value="${Student.hkLocal}"/>
		 		</div>
		 		<div class="form-label">户口性质</div>
		 		<div class="form-input">
		 			<select style="WIDTH: 100pt" name="hknature.id" id="hknatureId">
					<option value="">--请选择户口性质--</option>
					<c:forEach var="HK" items="${HK_nature}">
						<option
						<c:if test="${HK.id eq Student.hknature.id}">selected="selected"</c:if>
						 value="${HK.id}">${HK.enum_meaning}</option>
					</c:forEach>
				</select>
		 		</div>
		 	</div>
	 		<div class="form-line">
				<div class="form-label">身高</div>
				<div class="form-input">
					<input type="text" id="heightId" name="height" maxLength=3 value="${Student.height}"/>
				</div>
				<div class="form-label">体重</div>
				<div class="form-input">
					<input type="text" id="weightId" name="weight" maxLength=3 value="${Student.weight}"/>
				</div>
					<div class="form-label">健康状况</div>
				<div class="form-input">
					<select style="WIDTH: 100pt" name="healthy.id" id="healthyId">
						<option value="">--请选择健康状况--</option>
						<c:forEach var="healthy" items="${JKZK}">
							<option 
							<c:if test="${healthy.id eq Student.healthy.id}">selected="selected"</c:if>
							value="${healthy.id}">${healthy.enum_meaning}</option>
						</c:forEach>
					</select>
				</div>
			</div>
		 	<div class="form-line">
		 	<div class="form-label">特长</div>
		 	<div class="form-input">
		 		<textarea style="WIDTH:140px;height:20px;"class="textarea textarea-merge-large" id="strongSide" name="strongSide" >
		 		${Student.strongSide}
				</textarea>
			</div>
			<div class="form-label">处罚或奖励</div>
		 	<div class="form-input">
		 		<textarea style="WIDTH:140px;height:20px;"class="textarea textarea-merge-large" id="rewardPunish" name="rewardPunish">
		 		${Student.rewardPunish}
				</textarea>
			</div>	
		 	</div>
		 </fieldset>
		 <fieldset>
			<legend>考试信息（* 为必填项）</legend>
				<div class="form-line">
			<div class="form-label">准考证号</div>
				<div class="form-input">
					<input type="text" maxLength=15 name="admissionCard" id="admissionCard" maxLength=20 value="${Student.admissionCard}"/>
			</div>
			<div class="form-label">会考号</div>
				<div class="form-input">
					<input type="text" name="unionExamId" id="unionExamId" maxLength=12 value="${Student.unionExamId}"/>
			</div>
			<div class="form-label">考生号</div>
				<div class="form-input">
					<input type="text" name="studentId" id="studentId" maxLength=20 value="${Student.studentId}"/>
			</div>
			</div>
			<div class="form-line">
				<div class="form-label">考试类型</div>
				<div class="form-input">
					<select style="WIDTH: 100pt" name="examType.id" id="examTypeId">
						<option value="">--请选择考试类型--</option>
						<c:forEach var="examType" items="${examType}">
							<option
							<c:if test="${examType.id eq Student.examType.id}">selected="selected"</c:if>
							 value="${examType.id}">${examType.enum_meaning}</option>
						</c:forEach>
					</select>
				</div>
				<div class="form-label">应试卷种</div>
				<div class="form-input">
					<select style="WIDTH: 100pt" name="testTaking.id" id="testTakingId">
						<option value="">--请选择应试卷种--</option>
						<c:forEach var="test" items="${paperType}">
							<option 
							<c:if test="${test.id eq Studetn.testTaking.id}">selected="selected"</c:if>
							value="${test.id}">${test.enum_meaning}</option>
						</c:forEach>
					</select>
				</div>
					<div class="form-label">考生类别</div>
				<div class="form-input">
					<select style="WIDTH: 100pt" name="studentType.id" id="studentTypeId">
						<option value="">--请选择考生类别--</option>
						<c:forEach var="studentType" items="${StudentType}">
							<option 
							<c:if test="${studentType.id eq Student.studentType.id}">selected="selected"</c:if>
							value="${studentType.id}">${studentType.enum_meaning}</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="form-line">
				<div class="form-label">外语口试</div>
				<div class="form-input">
				<input type="text" id="oralExaminationId" name="oralExamination" maxLength=3 value="${Student.oralExamination}" onblur="check('oralExaminationId')"/>
				</div>
				<div class="form-label">外语听力</div>
				<div class="form-input">
				<input type="text" id="oralListenerId" name="oralListener" maxLength=3 value="${Student.oralListener}" onblur="check('oralListenerId')"/>
				</div>
				<div class="form-label">外语语种</div>
				<div class="form-input">
					<select style="WIDTH: 100pt" name="foreignLanguage.id" id="foreignLanguageId">
						<option value="">--请选择外语语种--</option>
						<c:forEach var="foreignLanguage" items="${foreignLanguage}">
							<option
							<c:if test="${foreignLanguage.id eq Student.foreignLanguage.id}">selected="selected"</c:if>
							 value="${foreignLanguage.id}">${foreignLanguage.enum_meaning}</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="form-line">
				<div class="form-label">总分</div>
				<div class="form-input">
					<input type="text" name="totalScore" id="totalScore" maxLength=3 value="${Student.totalScore}" onblur="check('totalScore')"/>
				</div>
				<div class="form-label">投档成绩</div>
				<div class="form-input">
				<input type="text" id="sendMark" name="sendMark" maxLength=3 value="${Student.sendMark}"/>
				</div>
				<div class="form-label">投档志愿</div>
				<div class="form-input">
					<select style="WIDTH: 100pt" name="sendWish.id" id="sendWishId">
						<option value="">--请选择投档志愿--</option>
						<c:forEach var="sendWish" items="${throwWish}">
							<option 
							<c:if test="${sendWish.id eq Student.sendWish.id}">selected="selected"</c:if>
							value="${sendWish.id}">${sendWish.enum_meaning}</option>
						</c:forEach>
					</select>
				</div>
			</div>
		</fieldset>
		
		<fieldset>
			<legend>其他信息（* 为必填项）</legend>
				<div class="form-line">
					<div class="form-label">港澳台侨</div>
					<div class="form-input">
					<select style="WIDTH: 100pt" name="emigrant.id" id="emigrantId">
						<option value="">--请选择港澳台侨--</option>
						<c:forEach var="emigrant" items="${GATQ}">
							<option 
							<c:if test="${emigrant.id eq Student.emigrant.id}">selected="selected"</c:if>
							value="${emigrant.id}">${emigrant.enum_meaning}</option>
						</c:forEach>
					</select>
					</div>
					<div class="form-label">中学代码</div>
					<div class="form-input">
					<input type="text" maxLength=8 id="middCodeId" name="middCode" value="${Student.middCode}"/>
					</div>	
					<div class="form-label">中学名称</div>
					<div class="form-input">
					<input type="text" id="middNameId" name="middName" value="${Student.middName}"/>
					</div>	
				</div>
				<div class="form-line">
					<div class="form-label">毕业类别</div>
					<div class="form-input">
						<select style="WIDTH: 100pt" name="graduationType.id" id="graduationTypeId">
						<option value="">--请选择毕业类别--</option>
						<c:forEach var="graduationType" items="${researchDirection}">
							<option 
							<c:if test="${graduationType.id eq Student.graduationType.id}">selected="selected"</c:if>
							value="${graduationType.id}">${graduationType.enum_meaning}</option>
						</c:forEach>
						</select>
					</div>
					<div class="form-label">计划定向</div>
					<div class="form-input">
						<select style="WIDTH: 100pt" name="planNature.id" id="planNatureId">
						<option value="">--请选择计划定向--</option>
						<c:forEach var="planNature" items="${StudentType}">
							<option
							<c:if test="${planNature.id eq Student.planNature.id}">selected="selected"</c:if>
							 value="${planNature.id}">${planNature.enum_meaning}</option>
						</c:forEach>
					</select>
					</div>	
					<div class="form-label">是否订单</div>
					<div class="form-input">
						<select style="WIDTH: 100pt" name="isOrder" id="isOrder">
						<option <c:if test="${Student.isOrder eq '0' }">selected="selected"</c:if> value="0" >是</option>
						<option <c:if test="${Student.isOrder eq '1' }">selected="selected"</c:if> value="1" selected="selected">否</option>
					</select>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label">省市</div>
					<div class="form-input">
						<select style="WIDTH: 110pt" id="provinceId" name="provinceCity.code">
							<option value="">--省--</option>
								<c:forEach var="province" items="${provinceList}">
									<option value="${province.code}"
									<c:if test="${fn:substring(province.code,0,2) eq fn:substring(basicInfo.birthAddress.code,0,2)}">
										selected="selected"
									</c:if>
									>${province.name}</option>
								</c:forEach>
						</select>
					</div>
					<div class="form-label">年度</div>
					<div class="form-input">
						<input name="enterYear" id="enterYear" type="text"  size="18" class="Wdate" onclick="WdatePicker({dateFmt:'yyyy'});" value="${Student.enterYear}"/>
					</div>
					<div class="form-label">地区</div>
					<div class="form-input">
						<input maxLength=6 type="text" id="regionId" name="region.code" value="${Student.region.code}" onblur="check('regionId')"/>
					</div>
				</div>
		</fieldset>
		<input type="hidden" id="nativePlaceCode" name="nativePlace.code" />
		<input type="hidden" id="id" name="id" value="${Student.id}"/>
	</form>
	</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	<script type="text/javascript"  src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript">
		var validate ;
		var FLAG = 'T';
		$(document).ready(function(){
			
			var tool = $('.tool').toolbar({
					items: ['-',{
							text: '<fmt:message key="expandAll" />',
							title: '全部展开账号列表',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-80px']
							},
							handler: function(){
								$('legend').siblings().slideDown('nomal');
							}
						},{
							text: '全部折叠',
							title: '全部折叠账号列表',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif', 
								position: ['-140px','-60px']
							},
							handler: function(){
								$('legend').siblings().slideUp('nomal');
							}
						}
					]
				});
    
		
		//展开或者折叠账号列表
		$('legend').bind('dblclick',function(){
			var $self = $(this);
			if($self.siblings().is(':hidden')){
				$self.siblings().slideDown('normal');
			}else if($self.siblings().is(':visible')){
				$self.siblings().slideUp('normal');
			}
		});
		
		$('legend').bind('click',function(){
			var $self = $(this);
			//设置选中的样式
			$('fieldset').removeClass('border_hover');
			$('legend').removeClass('border_hover color_hover');
			$self.parent().addClass('border_hover');
			$self.addClass('border_hover color_hover');
		});
		//设置列表区域的整体高度
		setElementHeight('#info',['.tool']);
		/**专业选择树**/
		$('#professionIdName').bind('focus.select',function(){
			down('professionIdName','professionId');
		});
		$('#professionIdFirstName').bind('focus.select',function(){
			down('professionIdFirstName','professionIdFirstId');
		});
		$('#professionIdSecondName').bind('focus.select',function(){
			down('professionIdSecondName','professionIdSecondId');
		});
		$('#professionIdThirdName').bind('focus.select',function(){
			down('professionIdThirdName','professionIdThirdId');
		});
		$('#professionIdFourthName').bind('focus.select',function(){
			down('professionIdFourthName','professionIdFourthId');
		});
		$('#professionIdFifthName').bind('focus.select',function(){
			down('professionIdFifthName','professionIdFifthId');
		});
		$('#professionIdSixName').bind('focus.select',function(){
			down('professionIdSixName','professionIdSixId');
		});
	});
		function down(professionIdName,professionId){
			if(!!!top.selectWindow){
				top.selectWin({
					src: '<%=path%>/studentInfo/findProfessionTree.do',
					title: '选择专业',
					offset: getOffset(professionIdName),
					items:[{
						input: $('#'+professionIdName),
						selectInput: '#professionalName'
					},{
						input: $('#'+professionId),
						selectInput: '#professionalId'
					}]
				});
			}
		}
		function getTown(parentId,childId){
			if(parentId==='birthAddressProvince'){
				$('#birthAddress'+' option').remove();
				$('#birthAddress').append('<option value="">--请选择县--</option>');
			}
			if($('#'+parentId).val() != ''){
				$.ajax({
					type:'POST',
					contentType : 'application/json;charset=UTF-8',
					url:'<%=path%>/area/getCity.do',
					data:'{"code":"'+$('#'+parentId).val()+'"}',
					dataType : 'json',
					success:function(data){
						$('#'+childId+' option').remove();
						if(parentId==='birthAddressProvince'){
							$('#birthAddress'+' option').remove();
							$('#birthAddressCity').append('<option value="">--请选择市区--</option>');
							$('#birthAddress').append('<option value="">--请选择县--</option>');
						}
					$('#birthAddress').append('<option value="">--请选择县--</option>');
						for(var i = 0 ; i < data.length ; i++){
  			  $('#'+childId).append('<option value='+data[i].code+'>'+data[i].name+'</option>');
      		}
						$('#'+childId+':first-child:selected');
					}
				});
			}else{
				$('#'+childId+' option').remove();
				$('#'+childId).append('<option>--请选择市--</option>');
				$('#birthAddress option').remove();
				$('#birthAddress').append('<option>--请选择县--</option>');
			}
		}
		
		/*根据身份证号码得到籍贯和生日*/
		function getNativeInfo(sfzh){
			var sfzhm = $('#'+sfzh).val();
			$.ajax({
				type: "POST",
				url: "<%=path%>/studentInfo/isFilledInApplyForm.do",
				data: "identityId="+sfzhm,
				dataType: "json",
				success: function(data){
					if(data == null){
						var nativeplaceCode = sfzhm.substring(0,6);
						var birthday = sfzhm.substring(6,14);
						birthday = birthday.substring(0,4)+"-"+birthday.substring(4,6)+"-"+birthday.substring(6,8);
						$('#birthday').val(birthday);
						$.ajax({
							type: "POST",
							url: "<%=path%>/area/getNativeInfo.do",
							data: "code="+sfzhm,
							dataType: "json",
							success: function(data){
								$('#nativePlaceCode').val(nativeplaceCode);
								$('#nativePlaceName').val(data.nativePlace);
							}
						});
						FLAG = 'T';
				}else{
			FLAG = 'F';
			alert('您的身份证号已经存在了');
				}
				}
			});
		}
		/*验证*/
		function check(tag){
			var checks=$("#"+tag).val();
			if('email'===tag){
			if(checks.length>0){
				if(checks.indexOf("@")==-1){
					alert("输入格式不正确！");
					$("#"+tag).val('');
				}
			}
		}
		if('postCode'===tag||'regionId'===tag){
			var v6 = /^\d\d\d\d\d\d$/;
				if(checks.length>0){
					  if( !checks.match(v6) ){
						   alert("请输入6位数字！");
						   $('#'+tag).val('');
						 }
				}
			}
		if('heightId'===tag||'weightId'===tag||
		'oralExaminationId'===tag||'totalScore'===tag||
		'oralListenerId'===tag||'sendMark'===tag){
			var v6 = /^\d\d\d$/;
				if(checks.length>0){
					  if( !checks.match(v6) ){
						   alert("请输入3位数字！");
						   $('#'+tag).val('');
						 }
				}
			}
		}
		function checkIndentity(sfzh){
			var sfzhm = $('#'+sfzh).val();
			 var v15 = /^\d\d\d\d\d\d\d\d\d\d\d\d\d\d\d$/;
			 var v18 = /^\d\d\d\d\d\d\d\d\d\d\d\d\d\d\d\d\d[1-9x]$/;
			 if( sfzhm.length == 15 ){
				  if( !sfzhm.match(v15) ){
				   alert("15位身份证号码应为数字");
				   $('#'+sfzh).val('');
				   $('#birthday').val('');
				   $('#nativePlaceName').val('');
				  }else{
					  getNativeInfo(sfzh);
				  }
				 }else if( sfzhm.length == 18 ){
				  if( !sfzhm.match(v18) ){
				   alert("18位身份证号码应为数字,最后一位可为x");
				  }else{
					  getNativeInfo(sfzh);
					   $('#'+sfzh).val('');
					   $('#birthday').val('');
					   $('#nativePlaceName').val('');
				  }
				 }else{
				  alert("身份证号码应为15或18位");
				   $('#'+sfzh).val('');
				   $('#birthday').val('');
				   $('#nativePlaceName').val('');
			 }
		}
	</script>
</body>
</html>