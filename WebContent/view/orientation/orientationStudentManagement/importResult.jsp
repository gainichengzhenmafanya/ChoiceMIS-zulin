<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>

<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>导入Excel</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		
	</head>

	<body>
		<div id="result" class="ajaxjiazai" style="display: block;">
			<div id='wBox_loading' align="center">
				<div class="wBox_content" id="wBoxContent">
					<img src='<%=path%>/image/upload/loading.gif' />上传成功，正在加载验证中，请稍后...
				</div>
			</div>
		</div>
		
		<div id='success' class="ajaxjiazai" style="display: none;">
			<div id='wBox_loading' align="center">
				<div class="successContent" id="resultContent">
					<div class="meg" id="successMeg" align="center"></div>
				</div>
			</div>
		</div>
		
		<div id='error' class="ajaxjiazai" style="display: none; width: 100%; overflow: auto;height: 100%;">
			<div id='wBox_loading' align="left">
				<div class="errorContent" id="resultContent">
					<div class="meg" id="errorMeg" align="left" style="height: 100%;">
						<div class="title">错误信息</div>
						<ul id="meg_ul" class="ul"></ul>
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" id="fileName" value="${fileName }" />
		<script type="text/javascript" src="<%=path %>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$.ajax({
					type:"POST",
					contentType : 'application/json;charset=UTF-8',
					url:'<%=path %>/studentInfo/validate.do?fileName='+$('#fileName').val(),
					success:function(data){
						if(data.validateResult != 'ok'){
							$("#result").css("display","none");
							$("#error").css("display","block");
							$("#meg_ul").append(data.validateResult);
						}else{
							if(data.importResult == 'ok'){
								location.href='<%=path%>/view/share/done.jsp';
							}
						}	
					}
				});
			});
		</script>
	</body>
</html>