<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>

<%
	String path = request.getContextPath();

%>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Basic Info</title>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>	
	<style type="text/css">
		.tool {
			position: relative;
			height: 27px;
		}
		.queryTool{
			position: relative;
			height: 30px;
			float: left;
		}
		.queryToolBySfzh{
			position: relative;
			height: 30px;
			float: left;
		}
		.report{
			position: relative;
			width:700px;
			height: 400px;
			margin-top: 10px;
			border: 1px solid gray;
		}
		.report td{
			font-size: 15px;
			text-align: center;
		}
	</style>
</head>
<body>
	<form id="queryForm" method="post" action="<%=path %>/studentInfo/findStudent.do">
		<input type="hidden" id="sfzh" value="${studentInfo.identityId }"/>
		<div class="form-line">
			<c:forEach var="column" items="${listColumns}">
				<c:if test="${condition.columnName eq column.columnName}">
					<div class="form-label">${column.zhColumnName }</div>
					<div class="form-input">
						<c:if test="${column.columnName eq 'SFZH'}">
							<input type="text" class="text" id="sfzh" name="${column.properties}" value="${studentInfo.identityId }"/>
						</c:if>
						<c:if test="${column.columnName ne 'SFZH'}">
							<input type="text" class="text" id="sfzh" name="${column.properties}" value="${elf:append(studentInfo,column.properties) }"/>
						</c:if>
					</div>
				</c:if>
			</c:forEach>
			<c:if test="${condition.columnName eq 'SFZH'}">
				<div class="queryToolBySfzh"></div>
			</c:if>
			<c:if test="${condition.columnName ne 'SFZH'}">
				<div class="queryTool"></div>
			</c:if>
		</div>
		<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
 		<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
	</form>
	<div class="tool"></div>
	<form id="studentInfo" action="" method="post" >
		<div class="grid">
			<div class="table-head">
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<td class="num" style="width: 25px;">&nbsp;</td>
							<td style="width:30px;">
								<input type="checkbox" id="chkAll"/>
							</td>
							<td style="width:80px;">姓名</td>
							<td style="width:140px;">身份证号码</td>
							<td style="width:80px;">性别</td>
							<td style="width:100px;">出生日期</td>
							<td style="width:100px;">民族</td>
							<td style="width:130px;">录取专业</td>
							<td style="width:120px;">学生类别</td>
							<td style="width:60px;">是否报到</td>
						</tr>
					</thead>
				</table>
			</div>
			<div class="table-body">
				<table cellspacing="0" cellpadding="0">
					<tbody>
						<c:forEach var="studentInfo" items="${listStudent}"  varStatus="status">
							<tr>
								<td class="num" style="width: 25px;">${status.index+1}</td>
								<td style="width:30px; text-align: center;">
									<input type="checkbox" id="chk_<c:out value='${studentInfo.id}' />" 
										value="<c:out value='${studentInfo.id}' />"/>
									<input type="hidden" id="${studentInfo.id}_xm" value="${studentInfo.name}"/>
									<input type="hidden" id="${studentInfo.id}_sfzh" value="${studentInfo.identityId}"/>
									<input type="hidden" id="${studentInfo.id}_mz" value="${studentInfo.nationality.enum_meaning}"/>	
									<input type="hidden" id="${studentInfo.id}_xb" value="${studentInfo.sex.enum_meaning}"/>	
									<input type="hidden" id="${studentInfo.id}_proMc" value="${studentInfo.professionId.zymc}"/>
									<input type="hidden" id="${studentInfo.id}_lbmc" value="${studentInfo.studentType.enum_meaning}"/>
								</td>
								<td>
									<span style="width:70px;"><c:out value="${studentInfo.name}" />&nbsp;</span>
								</td>
								<td>
									<span style="width:130px;"><c:out value="${studentInfo.identityId}" />&nbsp;</span>
								</td>
								<td>
									<span style="width:70px;"><c:out value="${studentInfo.sex.enum_meaning}" />&nbsp;</span>
								</td>
								<td>
									<span style="width:90px;"><c:out value="${studentInfo.birthday}" />&nbsp;</span>
								</td>
								<td>
									<span style="width:90px;"><c:out value="${studentInfo.nationality.enum_meaning}" />&nbsp;</span>
								</td>
								<td>
									<span style="width:120px;"><c:out value="${studentInfo.professionId.zymc}" />&nbsp;</span>
								</td>
								<td>
									<span style="width:110px;"><c:out value="${studentInfo.studentType.enum_meaning}" />&nbsp;</span>
								</td>
								<td>
									<c:if test="${studentInfo.enterState eq '1'}">
										<span style="color: green;width: 50px;">已报到&nbsp;</span>
									</c:if>
									<c:if test="${studentInfo.enterState eq '0'}">
										<span style="color: gray;width: 50px;">未报到&nbsp;</span>
									</c:if>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</form>	
	<page:page form="queryForm" page="${pageobj}"></page:page>
	<object  id="LODOP" classid="clsid:2105C259-1E0C-4534-8141-A753534CB4CA" width=0 height=0> 
			<embed id="LODOP_EM" type="application/x-print-lodop" width=0 height=0></embed>
		</object>
	<div id="reportInfo" class="report"  align="center" style="display: none;">
		<table border="1" cellpadding="0" cellspacing="0" style="width: 600px;">
			<tr>
				<td align="center" colspan="4" height="50px;">
					<span style="font-family:'宋体';font-size: 24px;">报到单</span>
				</td>
			</tr>
			<tr>
				<td height="30px;">姓名</td>
				<td width="200px">
					<span id="studentName" style="font-family:'隶书';font-size: 18px;">张三</span>
				</td>
				<td>身份证号码</td>
				<td>
					<span id="studentIDCard" style="font-family:'隶书';font-size:18px;">371121198901131015</span>
				</td>
			</tr>
			<tr>
				<td height="30px;">性别</td>
				<td>
					<span id="studentSex" style="font-family:'隶书';font-size: 18px;">男</span>
				</td>
				<td>民族</td>
				<td>
					<span id="studentMZ" style="font-family:'隶书';font-size: 18px;">汉族</span>
				</td>
			</tr>
			<tr>
				<td height="30px;">录取专业</td>
				<td>
					<span id="studentZY" style="font-family:'隶书';font-size: 18px;"></span>
				</td>
				<td height="30px;">学生类别</td>
				<td>
					<span id="studentLB" style="font-family:'隶书';font-size: 18px;"></span>
				</td>
			</tr>
			<tr>
				<td colspan="4" height="100px">
				</td>
			</tr>
		</table>
	</div>
		
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/LodopFuncs.js"></script>
	<script type="text/javascript">
				$(document).ready(function(){
					var queryTool = $('.queryTool').toolbar({
							items: [{
									text: '<fmt:message key="select" />',
									title: '查询',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif', 
										position: ['0px','-40px']
									},
									handler: function(){
										query();
									}
								}
							]
						});
						
					var shzhTool = $('.queryToolBySfzh').toolbar({
							items: [{
									text: '读卡',
									title: '读卡',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif', 
										position: ['0px','-40px']
									},
									handler: function(){
										read();
									}
								},{
									text: '<fmt:message key="select" />',
									title: '查询',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif', 
										position: ['0px','-40px']
									},
									handler: function(){
										query();
									}
								}
							]
						});
								
					var tool = $('.tool').toolbar({
							items: [{
									text: '报到',
									title: '报到',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['0px','0px']
									},
									handler: function(){
										if(getFirstID()){
											affirmReport(getFirstID(),$('#sfzh').val());
										}else{
											alert('请选择要报到的学生!');
										}
									}
								},{
									text: '打印报到单',
									title: '打印报到单',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-140px','-100px']
									},
									handler: function(){
										if(getFirstID()){
											printForm(getFirstID());
										}else{
											alert('请选择要打印的学生名单!');
										}
										
									}
								}
							]
						});
						
						//添加“回车”事件
						$(window).keydown(function(e){
		  	 			if(e.keyCode === 13){
		  	 				$('#queryForm').click();
		  	 			}
		  	 		});
						
						setElementHeight('.grid',['.form-line','.tool'],$(document.body),27);	//计算.grid的高度
						setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
						loadGrid();
						$('.grid').find('.table-body').find('tr').hover(
							function(){
								$(this).addClass('tr-over');
							},
							function(){
								$(this).removeClass('tr-over');
							}
						);
				});
				/**打印报名单**/
				function printForm(chaValue){
					$('#studentName').html($('#'+chaValue+'_xm').val());
					$('#studentIDCard').html($('#'+chaValue+'_sfzh').val());
					$('#studentSex').html($('#'+chaValue+'_xb').val());
					$('#studentMZ').html($('#'+chaValue+'_mz').val());
					$('#studentZY').html($('#'+chaValue+'_proMc').val());
					$('#studentLB').html($('#'+chaValue+'_lbmc').val());
					print();
				}
				
				function query(){
					$('#queryForm').submit();
				}
				
					/**打印**/
		   function print() {
					CreateOneFormPage();
				};                                           
			  function CreateOneFormPage(){
					LODOP=getLodop(document.getElementById('LODOP'),document.getElementById('LODOP_EM'));
					LODOP.PRINT_INIT("打印控件功能演示_Lodop功能_表单一");
					LODOP.SET_PRINT_STYLE("FontSize",18);
					LODOP.SET_PRINT_STYLE("Bold",1);
					LODOP.ADD_PRINT_HTM(58,104,800,600,document.getElementById("reportInfo").innerHTML);
					LODOP.PRINT_SETUP();
				}; 
				
				
				function affirmReport(chaVlaue,sfzh){
					$('body').window({
						id: 'window_affirm',
						title: '确认报到',
						content: '<iframe id="window_affirmFrame" frameborder="0" src="<%=path%>/studentInfo/affirmReport.do?id='+chaVlaue+'&identityId='+sfzh+'"></iframe>',
						width: 497,
						height: 180,
						isModal: true
					});
				}
				
				function read(){
					location.href = "<%=path%>/studentInfo/read.do";
				}
			</script>
		</body>
</html>