<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>帮助文档列表</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<style type="text/css">
				.grid td span{
					display: inline-block;
				}
				.grid td{
					cursor: default;
				}
				a{
					cursor: pointer;
				}
			</style>
		</head>
	<body>
		<div class="tool">
		</div>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td style="width:30px;"><span>&nbsp;</span></td>
								<td style="width:470px;"><span>文件</span></td>
								<td style="width:80px;"><span>大小</span></td>
								<td style="width:45px;"><span>操作</span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="file" items="${files}" varStatus="status">
								<tr>
									<td style="width:30px;text-align: center;"><span>${status.index+1 }</span></td>
									<td style="width:470px;"><span style="width: 460px;" title="${file['name']}">${file['name']}</span></td>
									<td style="width:80px;"><span style="width: 70px;" title="${file['len']}">${file['len']}</span></td>
									<td style="width:45px;text-align: center;">
										<span style="width:35px;color: blue;">
											<a 
<%-- 												href="<%=path%>/supply/downloadScmTemplate.do?fileName=${file.name }" --%>
													onclick="downloadF('${file.name }')"
											 >下载</a>
										</span>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				// 自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),0);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
					);
			});
			function downloadF(file){
				var form = $('<form method="POST" id="downloads"></form>');
				form.attr('action',"<%=path%>/supply/downloadScmTemplate.do");
				form.append('<input type="text" name="fileName" value="'+file+'"/>');
				$(".grid").append(form);
				$("#downloads").submit();
				$("#downloads").remove();
			}
		</script>
	</body>
</html>