<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title></title>
	    <style type="text/css">
			.xddiv{
				color:#ffffff;width:100px;height:90px;border-radius:5px;text-align:center;line-height:90px;font-size:1.3em;position:relative;
			}
			.redhint{
				position:absolute;
				text-align:center; 
				display:inline-block;
				background-color:red;
/* 				background:url(../../image/wechat/dishNum.png);  */
/* 				background-size:100% 100%;  */
				color:white; 
				clear:both; 
				line-height:15px;
				width:75px;height:15px;
				top:0px;
				right:0px;
				border-radius:10px;
			}
			.greenhint{
				position:absolute;
				text-align:center; 
				display:inline-block;
				background-color:green;
/* 				background:url(../../image/wechat/dishNum.png);  */
/* 				background-size:100% 100%;  */
				color:white; 
				clear:both; 
				line-height:15px;
				width:60px;height:15px;
				top:0px;
				right:0px;
				border-radius:10px;
			}
			
			*{margin:0; padding:0} 
			li{list-style:none} 
			#carousel_container{position:relative; width:95%;height:150px;margin-left:2.7%; overflow:hidden;background: none repeat scroll 0 0 #FBFBFB;border: 1px solid #E2E2E2;} 
			#carousel_inner{width:auto; height:auto; overflow: hidden; position:absolute;left:40px; top:35px; right:40px;} 
			#left_scroll{position: absolute;left:0;top:40px;width:33px;height:77px;cursor: pointer;cursor: hand; background:url(../image/misboh/lgg.gif) no-repeat;} 
			#right_scroll{position: absolute;top:40px;right:0; width: 33px;height: 77px;cursor: pointer;cursor: hand; background: url(../image/misboh/rgg.gif) no-repeat;} 
			#carousel_ul{width:9999px; height:100%; position:relative;} 
			#carousel_ul li{float: left;width:100px;height:90px; display:inline;} 
	    </style>
	</head>
	<body>
		<div id="carousel_container"> 
		<div id="left_scroll"></div> 
		<div id="carousel_inner"> 
		<ul id="carousel_ul">
			<c:forEach var="guideConfig" items="${guideConfigList }" varStatus="status">
				<li>
					<a class="xda" href="javascript:void(0)" onclick="showInfo1('${guideConfig.module_id }','${guideConfig.name }','${guideConfig.url }','${guideConfig.hint }')" style="text-decoration:none;">
						<div class="xddiv" style="background-color:${guideConfig.color };">${guideConfig.name }
						<div id="hint${guideConfig.pk_guideconfig }" class="${guideConfig.hintClass }">${guideConfig.hint }</div>
						</div>
				    </a>
				</li>
				<c:if test="${status.last == false}">
					<li>
						<img src="<%=path %>/image/misboh/jiantou.png" style="width:100px;height:90px;"/>
					</li>
				</c:if>
			</c:forEach>
		</ul> 
		</div> 
		<div id="right_scroll"></div> 
		</div> 
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript">
// 		if('${accountPositn}' == 'N'){
// 			alert('<fmt:message key="The_current_account_does_not_have_a_branch_store_the_store_can_not_use_the_SBoh_node"/>!');
// 		}
		if('${accountStore}' == 'N'){
			alert('<fmt:message key="The_current_store_does_not_correspond_to_the_BOH"/>!');
		}
		if($('#hint11').text() == '已日结'){
			alert('<fmt:message key="Current_date"/>:${nowDate},<fmt:message key="System_date"/>:${nextDate},<fmt:message key="The_current_store_already_end"/>！');
		}
		var SellerScroll = function(options){ 
			this.SetOptions(options); 
			this.lButton = this.options.lButton; 
			this.rButton = this.options.rButton; 
			this.oList = this.options.oList; 
			this.showSum = this.options.showSum; 
			
			this.iList = $("#" + this.options.oList + " > li"); 
			this.iListSum = this.iList.length; 
			this.iListWidth = this.iList.outerWidth(true); 
			this.moveWidth = this.iListWidth * this.showSum; 
			
			this.dividers = Math.ceil(this.iListSum / this.showSum);//Ϊٿ 
			this.moveMaxOffset = (this.dividers - 1) * this.moveWidth; 
			this.LeftScroll(); 
			this.RightScroll(); 
		}; 
		SellerScroll.prototype = { 
			SetOptions: function(options){ 
			this.options = { 
			lButton: "left_scroll", 
			rButton: "right_scroll", 
			oList: "carousel_ul", 
			showSum: 8//һιٸitems 
		}; 
		$.extend(
			this.options, 
			options || {});}, 
			ReturnLeft: function(){ 
				return isNaN(parseInt($("#" + this.oList).css("left"))) ? 0 : parseInt($("#" + this.oList).css("left")); 
			}, 
			LeftScroll: function(){ 
				if(this.dividers == 1) return; 
				var _this = this, currentOffset; 
				$("#" + this.lButton).click(function(){ 
					currentOffset = _this.ReturnLeft(); 
					if(currentOffset == 0){ 
// 						for(var i = 1; i <= _this.showSum; i++){ 
// 							$(_this.iList[_this.iListSum - i]).prependTo($("#" + _this.oList)); 
// 						} 
// 						$("#" + _this.oList).css({ left: -_this.moveWidth }); 
// 						$("#" + _this.oList + ":not(:animated)").animate( { left: "+=" + _this.moveWidth }, { duration: "slow", complete: function(){ 
// 							for(var j = _this.showSum + 1; j <= _this.iListSum; j++){ 
// 								$(_this.iList[_this.iListSum - j]).prependTo($("#" + _this.oList)); 
// 							} 
// 							$("#" + _this.oList).css({ left: -_this.moveWidth * (_this.dividers - 1) }); 
// 						} } ); 
					}else{ 
						$("#" + _this.oList + ":not(:animated)").animate( { left: "+=" + _this.moveWidth }, "slow" ); 
					} 
				}); 
			}, 
			RightScroll: function(){ 
				if(this.dividers == 1) return; 
				var _this = this, currentOffset; 
				$("#" + this.rButton).click(function(){ 
					currentOffset = _this.ReturnLeft(); 
					if(Math.abs(currentOffset) >= _this.moveMaxOffset){ 
// 						for(var i = 0; i < _this.showSum; i++){ 
// 							$(_this.iList[i]).appendTo($("#" + _this.oList)); 
// 						} 
// 						$("#" + _this.oList).css({ left: -_this.moveWidth * (_this.dividers - 2) }); 
// 						$("#" + _this.oList + ":not(:animated)").animate( { left: "-=" + _this.moveWidth }, { duration: "slow", complete: function(){ 
// 							for(var j = _this.showSum; j < _this.iListSum; j++){ 
// 								$(_this.iList[j]).appendTo($("#" + _this.oList)); 
// 							} 
// 							$("#" + _this.oList).css({ left: 0 }); 
// 						} } ); 
					}else{ 
						$("#" + _this.oList + ":not(:animated)").animate( { left: "-=" + _this.moveWidth }, "slow" ); 
					} 
				}); 
			} 
		}; 
		$(document).ready(function(){ 
			var ff = new SellerScroll();
		}); 
		
		//打开明细页签
  		function showInfo1(moduleId,moduleName,moduleUrl,hint){
			if(moduleName == '日结'){
				if(hint == '已日结'){
					alert('<fmt:message key="The_current_store_already_end"/>！');
					return;
				}
				if(confirm('<fmt:message key="Are_you_sure_to_end_the_story_today"/>?')){
					$.ajax({
						url:'<%=path %>/myDesk/endOfDay.do',
						type:'post',
						success:function(msg){
							alert('<fmt:message key="End_success"/>！');
						}
					});
				}
				return;
			}
			parent.showInfo(moduleId,moduleName,moduleUrl);
  	 	}
 		</script>
	</body>
</html>