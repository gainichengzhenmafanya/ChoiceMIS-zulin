<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>

<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="insert"/><fmt:message key="package" /><fmt:message key="set_up_the" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
    	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />	   
	</head>
	<body>
		<div class="tool"></div>
		<div class="form" style="float: left;height: auto;">
			<form id="listForm" method="post" action="<%=path %>/otherStockData/listOtherStockData.do" >
				<div class="easyui-tabs" fit="true" plain="true" id="tabs" style="z-index:88;width:98%;overflow: hidden;height: 540px">
					<div title="<fmt:message key="misboh_baserecord_water" />">
						<input type="hidden" id="flag" value="shui" />
						<div class="grid" style="height:20%;">
							<div class="table-head" >
								<table cellspacing="0" cellpadding="0" style="width:50%;">
									<thead>
										<tr>
											<td style="width:4%;">&nbsp;</td>
											<td style="width:4%;">
<!-- 												<input type="checkbox" id="chkAll"/> -->
											</td>
											<td style="width:46%;"><fmt:message key="project" /><fmt:message key="name" /></td>
											<td style="width:46%;"><fmt:message key="The_total_amount_of" /></td>
										</tr>
									</thead>
								</table>
							</div>
							<div class="table-body" >
								<table cellspacing="0" cellpadding="0" border="0px" frame="void" style="width:50%;">
									<tbody>
										<c:forEach var="wc" items="${waterCount}" varStatus="status">
											<tr>
												<td class="num" style="width:4%;text-align: center;">${status.index+1}</td>
												<td style="width:4%; text-align: center;">
													<input type="checkbox" name="idListWater" value="<c:out value='${wc.pk_otherstockdataf}' />"/>
													<input type="hidden" name="prepayment_shui" id="prepayment_shui" value="${wc.prepayment }" />
												</td>
												<td style="width:46%;"><c:out value="${wc.vname }" /></td>
												<td style="width:46%;text-align: right;"><fmt:formatNumber value="${wc.zcount}" pattern="#00.00"/></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>	
						<div class="grid" style="height:79%;">
							<div class="table-head" style="width: 100%;">
								<table cellspacing="0" cellpadding="0" style="width: 100%;">
									<thead>
										<tr>
											<td style="width: 25px;">&nbsp;</td>
											<td style="width:100px;"><fmt:message key="date" /></td>
											<td style="width:100px;"><fmt:message key="week" /></td>
<%-- 											<td style="width:100px;"><fmt:message key="beginning_of_period" /></td> --%>
<%-- 											<td style="width:100px;"><fmt:message key="The_end_of_the_semester" /></td> --%>
<%-- 											<td style="width:100px;"><fmt:message key="Replenish_stock" /><fmt:message key="quantity" /></td> --%>
											<td style="width:40%;"><fmt:message key="scm_consumption" /></td>
											<td style="width:40%;"><fmt:message key="amount" /></td>
										</tr>
									</thead>
								</table>
							</div>
							<div class="table-body">
								<table cellspacing="0" cellpadding="0" style="width: 100%;">
									<tbody>
										<c:forEach var="water" items="${water}" varStatus="status">
											<tr <c:if test="${'' == water.week }">bgcolor="#D2E9FF"</c:if>>
												<td class="num" style="width: 25px;">${status.index+1}</td>
												<td style="width:100px;text-align: left;"><c:out value="${water.workdate}" /></td>
												<td style="width:100px;"><c:out value="${water.week}" /></td>
<%-- 												<td style="width:100px;text-align: right;"><fmt:formatNumber value="${water.begincount}" pattern="#00.00"/></td> --%>
<%-- 												<td style="width:100px;text-align: right;"><fmt:formatNumber value="${water.endcount}" pattern="#00.00"/></td> --%>
<%-- 												<td style="width:100px;text-align: right;"><fmt:formatNumber value="${water.incount}" pattern="#00.00"/></td> --%>
												<td style="width:40%;text-align: right;"><fmt:formatNumber value="${water.outcount}" pattern="#00.00"/></td>
												<td style="width:40%;text-align: right;"><fmt:formatNumber value="${water.amount}" pattern="#00.00"/></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>	
					</div>
					<div title="<fmt:message key="misboh_baserecord_electric" />">
						<input type="hidden" id="flag" value="dian" />
						<div class="grid" style="height: 20%;">
							<div class="table-head" >
								<table cellspacing="0" cellpadding="0" style="width: 50%;">
									<thead>
										<tr>
											<td style="width:4%;">&nbsp;</td>
											<td style="width:4%;">
<!-- 												<input type="checkbox" id="chkAll"/> -->
											</td>
											<td style="width:31%;"><fmt:message key="project" /><fmt:message key="name" /></td>
											<td style="width:31%;"><fmt:message key="beilv" /></td>
											<td style="width:30%;"><fmt:message key="The_total_amount_of" /></td>
										</tr>
									</thead>
								</table>
							</div>
							<div class="table-body">
								<table cellspacing="0" cellpadding="0" border="0px" frame="void" style="width: 50%;">
									<tbody>
										<c:forEach var="ec" items="${electricCount}" varStatus="status">
											<tr>
												<td class="num" style="width:4%;text-align: center;">${status.index+1}</td>
												<td style="width:4%; text-align: center;">
													<input type="checkbox" name="idListElectric" id="chk_<c:out value='${ec.pk_otherstockdataf}' />" value="<c:out value='${ec.pk_otherstockdataf}' />"/>
													<input type="hidden" name="prepayment_dian" id="prepayment_dian" value="${ec.prepayment }" />
												</td>
												<td style="width:31%;"><c:out value="${ec.vname }" /></td>
												<td style="width:31%;text-align: right;"><fmt:formatNumber value="${ec.meterrate }" pattern="#0.00"/></td>
												<td style="width:30%;text-align: right;"><fmt:formatNumber value="${ec.zcount}" pattern="#0.00"/></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>	
						<div class="grid" id="grid_top2" style="height:79%;">
							<div class="table-head" >
								<table cellspacing="0" cellpadding="0" style="width:100%;">
									<thead>
										<tr>
											<td style="width: 25px;">&nbsp;</td>
											<td style="width:100px;"><fmt:message key="date" /></td>
											<td style="width:100px;"><fmt:message key="week" /></td>
<%-- 											<td style="width:100px;"><fmt:message key="beginning_of_period" /></td> --%>
<%-- 											<td style="width:100px;"><fmt:message key="The_end_of_the_semester" /></td> --%>
<%-- 											<td style="width:100px;"><fmt:message key="Replenish_stock" /><fmt:message key="quantity" /></td> --%>
											<td style="width:40%"><fmt:message key="scm_consumption" /></td>
											<td style="width:40%"><fmt:message key="amount" /></td>
										</tr>
									</thead>
								</table>
							</div>
							<div class="table-body1">
								<table cellspacing="0" cellpadding="0" style="width:100%;">
									<tbody>
										<c:forEach var="electric" items="${electric}" varStatus="status">
											<tr <c:if test="${'' == electric.week }">bgcolor="#D2E9FF"</c:if>>
												<td class="num" style="width: 25px;">${status.index+1}</td>
												<td style="width:100px;text-align: left;"><c:out value="${electric.workdate}" /></td>
												<td style="width:100px;"><c:out value="${electric.week}" /></td>
<%-- 												<td style="width:100px;text-align: right;"><fmt:formatNumber value="${electric.begincount}" pattern="#00.00"/></td> --%>
<%-- 												<td style="width:100px;text-align: right;"><fmt:formatNumber value="${electric.endcount}" pattern="#00.00"/></td> --%>
<%-- 												<td style="width:100px;text-align: right;"><fmt:formatNumber value="${electric.incount}" pattern="#00.00"/></td> --%>
												<td style="width:40%;text-align: right;"><fmt:formatNumber value="${electric.outcount}" pattern="#00.00"/></td>
												<td style="width:40%;text-align: right;"><fmt:formatNumber value="${electric.amount}" pattern="#00.00"/></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>			
					</div>
					<div title='<fmt:message key="misboh_baserecord_gas" />'>
						<input type="hidden" id="flag" value="qi" />
						<div class="grid" style="height:20%;">
							<div class="table-head" >
								<table cellspacing="0" cellpadding="0" style="width: 50%;">
									<thead>
										<tr>
											<td style="width:4%;">&nbsp;</td>
											<td style="width:4%;">
<!-- 												<input type="checkbox" id="chkAll"/> -->
											</td>
											<td style="width:46%;"><fmt:message key="project" /><fmt:message key="name" /></td>
											<td style="width:46%;"><fmt:message key="The_total_amount_of" /></td>
										</tr>
									</thead>
								</table>
							</div>
							<div class="table-body">
								<table cellspacing="0" cellpadding="0" border="0px" frame="void" style="width: 50%;">
									<tbody>
										<c:forEach var="gc" items="${gasCount}" varStatus="status">
											<tr>
												<td class="num" style="width:4%;text-align: center;">${status.index+1}</td>
												<td style="width:4%; text-align: center;">
													<input type="checkbox" name="idListGas" id="chk_<c:out value='${gc.pk_otherstockdataf}' />" value="<c:out value='${gc.pk_otherstockdataf}' />"/>
													<input type="hidden" name="prepayment_qi" id="prepayment_qi" value="${gc.prepayment }" />
												</td>
												<td style="width:46%;"><c:out value="${gc.vname }" /></td>
												<td style="width:46%;text-align: right;"><fmt:formatNumber value="${gc.zcount}" pattern="#00.00"/></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>	
						<div class="grid" style="height:79%;">
							<div class="table-head" >
								<table cellspacing="0" cellpadding="0" style="width: 100%;">
									<thead>
										<tr>
											<td style="width: 25px;">&nbsp;</td>
											<td style="width:100px;"><fmt:message key="date" /></td>
											<td style="width:100px;"><fmt:message key="week" /></td>
<%-- 											<td style="width:100px;"><fmt:message key="beginning_of_period" /></td> --%>
<%-- 											<td style="width:100px;"><fmt:message key="The_end_of_the_semester" /></td> --%>
<%-- 											<td style="width:100px;"><fmt:message key="Replenish_stock" /><fmt:message key="quantity" /></td> --%>
											<td style="width:40%"><fmt:message key="scm_consumption" /></td>
											<td style="width:40%"><fmt:message key="amount" /></td>
										</tr>
									</thead>
								</table>
							</div>
							<div class="table-body">
								<table cellspacing="0" cellpadding="0" style="width: 100%;">
									<tbody>
										<c:forEach var="gas" items="${gas}" varStatus="status">
											<tr <c:if test="${'' == gas.week }">bgcolor="#D2E9FF"</c:if>>
												<td class="num" style="width: 25px;">${status.index+1}</td>
												<td style="width:100px;text-align: left;"><c:out value="${gas.workdate}" /></td>
												<td style="width:100px;"><c:out value="${gas.week}" /></td>
<%-- 												<td style="width:100px;text-align: right;"><fmt:formatNumber value="${gas.begincount}" pattern="#00.00"/></td> --%>
<%-- 												<td style="width:100px;text-align: right;"><fmt:formatNumber value="${gas.endcount}" pattern="#00.00"/></td> --%>
<%-- 												<td style="width:100px;text-align: right;"><fmt:formatNumber value="${gas.incount}" pattern="#00.00"/></td> --%>
												<td style="width:40%;text-align: right;"><fmt:formatNumber value="${gas.outcount}" pattern="#00.00"/></td>
												<td style="width:40%;text-align: right;"><fmt:formatNumber value="${gas.amount}" pattern="#00.00"/></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>			
					</div>
				</div>
				<div class="search-div" style="margin-left: 0px;">
					<div class="search-condition">
						<div class="form-line">
							<div class="form-label"><fmt:message key="select1" /><fmt:message key="months" /></div>
							<div class="form-input">
								<input type="text" id="workdate" name="workdate" class="Wdate text" value="<fmt:formatDate value="${workdate}" pattern="yyyy-MM"/>" />
							</div>
						</div>
					</div>
					<div class="search-commit">
			       		<input type="button" class="search-button" id="search" value='<fmt:message key="enter" />'/>
			       		<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
					</div>
				</div>
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	  	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>		
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			if('${type}'=='2'){ //如果是电就返回电标签
				$('#tabs').tabs('select', '<fmt:message key="misboh_baserecord_electric" />');
			}
			
			if('${type}'=='3'){ //如果是气就返回气标签
				$('#tabs').tabs('select', '<fmt:message key="misboh_baserecord_gas" />');
			}
			
			<%
				session.removeAttribute("type");
			%>
			
			var tool = $('.tool').toolbar({
				items: [{
					text: '<fmt:message key="select" />',
					title: '<fmt:message key="select" />',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-100px','-40px']
					},
					handler: function(){
						$('.search-div').slideToggle(100);
					}
				},'-',{
					text: '<fmt:message key="insert" />表',
					title: '<fmt:message key="new_role_information"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','0px']
					},
					handler: function(){
						add();
					}
				},{
					text: '<fmt:message key="update" />表',
					title: '<fmt:message key="modify_the_role_information"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-18px','0px']
					},
					handler: function(){
						update();
					}
				},{
					text: '录入明细',
					title: '<fmt:message key="modify_the_role_information"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-18px','0px']
					},
					handler: function(){
						updateDetail();
					}
				},{
					text: '<fmt:message key="delete" />表',
					title: '<fmt:message key="delete" />',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-100px','-40px']
					},
					handler: function(){
						deleteAll();
					}
				},"-",{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit" />',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
					}
				}]
			});
			$("#workdate").click(function(){ 
				new WdatePicker({dateFmt:'yyyy-MM'});
			});
			
			$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0).click();
		 		}
		 	});

		});

		$("#search").bind('click', function() {
			$('.search-div').hide();
			$('#listForm').submit();
		});
		
		/* 模糊<fmt:message key="select" />清空 */
		$("#resetSearch").bind('click', function() {
			$('.search-condition input').val('');
		});
		
		//双击弹出修改页面
		$('.grid').find('.table-body').find('tr').live("dblclick", function () {
			$(":checkbox").attr("checked", false);
			$(this).find(':checkbox').attr("checked", true);
			update();
			if($(this).hasClass("show-firm-row"))return;
			$('.grid').find('.table-body').find('tr').removeClass("show-firm-row");
			$(this).addClass("show-firm-row");
		 });
		
		
		function add(){
			var tab = $("#tabs").tabs("getSelected");
			var flag = tab.find("#flag").val();
			switch(flag){
				case "shui": 
					$('body').window({
						id: 'window_saveInceom',
						title: '<fmt:message key="insert" /><fmt:message key="Water_meter" />',
						content: '<iframe id="SaveForm" name="SaveForm" frameborder="0" src="<%=path%>/otherStockData/addWater.do?type=shui"></iframe>',
						width: '600px',
						height: '400px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save_role_of_information"/>',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('SaveForm')&&window.document.getElementById("SaveForm").contentWindow.validate._submitValidate()){
											document.getElementById("SaveForm").contentWindow.document.forms["waterForm"].submit();
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
					break;
				case "dian":
					$('body').window({
						id: 'window_saveInceom',
						title: '<fmt:message key="insert" /><fmt:message key="Electric_meter" />',
						content: '<iframe id="SaveForm" frameborder="0" src="<%=path%>/otherStockData/addWater.do?type=dian"></iframe>',
						width: '600px',
						height: '400px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save_role_of_information"/>',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('SaveForm')){
											document.getElementById("SaveForm").contentWindow.document.forms["waterForm"].submit();
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
					break;
				case "qi": 
					$('body').window({
						id: 'window_saveInceom',
						title: '<fmt:message key="insert" /><fmt:message key="gas_meter" />',
						content: '<iframe id="SaveForm" frameborder="0" src="<%=path%>/otherStockData/addWater.do?type=qi"></iframe>',
						width: '600px',
						height: '400px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save_role_of_information"/>',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('SaveForm')&&window.document.getElementById("SaveForm").contentWindow.validate._submitValidate()){
											document.getElementById("SaveForm").contentWindow.document.forms["waterForm"].submit();
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
					break;
			}
		}
		
		//修改水电气主表
		function update(){
			var tab = $("#tabs").tabs("getSelected");
			var flag = tab.find("#flag").val();
			var checkboxList = [];
			var workdate = $("#workdate").val();
			
			//判断flag的值，如果是”shui“，则取水表标签下的值；若是”dian“，则取电表标签下的值；若是”qi“，则取气表标签下的值
			if("shui" == flag){
				var str=document.getElementsByName("idListWater"); 
				for (var i=0;i<str.length;i++ ){
					if(str[i].checked){
						checkboxList.push(str[i].value);
					}
				}
			}
			
			if ("dian" == flag) {
				var str=document.getElementsByName("idListElectric"); 
				for (var i=0;i<str.length;i++ ){
					if(str[i].checked){
						checkboxList.push(str[i].value);
					}
				}
			}
			
			if ("qi" == flag) {
				var str=document.getElementsByName("idListGas"); 
				for (var i=0;i<str.length;i++ ){
					if(str[i].checked){
						checkboxList.push(str[i].value);
					}
				}
			}

			if(checkboxList.length == 1){
				var action='<%=path%>/otherStockData/updateOtherstockDataF.do?workdate='+workdate+'&type='+flag+'&pk_otherstockdataf='+checkboxList[0];
				$('body').window({
					id: 'window_updateIncome',
					title: '<fmt:message key="update" /><fmt:message key="utility" />',
					content: '<iframe id="SaveForm" frameborder="0" src='+action+'></iframe>',
					width: '600px',
					height: '400px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								title: '<fmt:message key="data" /><fmt:message key="update" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('SaveForm')&&window.document.getElementById("SaveForm").contentWindow.validate._submitValidate()){
										document.getElementById("SaveForm").contentWindow.document.forms["waterForm"].submit();
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}else if(checkboxList.length > 1){
				alert('<fmt:message key="you_can_only_modify_a_data" />！');
				return;
			}else {
				alert('<fmt:message key="please_select_information_you_need_to_modify"/>！');
				return ;
			}
		}
		
		//修改水电气明细
		function updateDetail(){
			var tab = $("#tabs").tabs("getSelected");
			var flag = tab.find("#flag").val();
			var checkboxList = [];
			var prepayments = [];
			var prepayment;
			var workdate = $("#workdate").val();
			//判断flag的值，如果是”shui“，则取水表标签下的值；若是”dian“，则取电表标签下的值；若是”qi“，则取气表标签下的值
			if("shui" == flag){
				var str_prepayment = document.getElementsByName("prepayment_shui");
				var str = document.getElementsByName("idListWater"); 
				for (var i=0;i<str.length;i++ ){
					if(str[i].checked){
						prepayments.push(str_prepayment[i].value);
						checkboxList.push(str[i].value);
					}
				}
			}
			
			if ("dian" == flag) {
				var str = document.getElementsByName("idListElectric"); 
				var str_prepayment = document.getElementsByName("prepayment_dian");
				for (var i=0;i<str.length;i++ ){
					if(str[i].checked){
						prepayments.push(str_prepayment[i].value);
						checkboxList.push(str[i].value);
					}
				}
			}
			
			if ("qi" == flag) {
				var str_prepayment = document.getElementsByName("prepayment_qi");
				var str = document.getElementsByName("idListGas"); 
				for (var i=0;i<str.length;i++ ){
					if(str[i].checked){
						prepayments.push(str_prepayment[i].value);
						checkboxList.push(str[i].value);
					}
				}
			}
			
			//若prepayment为1，则是预付费；若是0，则是后付费
			if (1 == prepayments[0]) {
				prepayment = '<fmt:message key="prepaid" />';
			} else {
				prepayment = '<fmt:message key="After_paying" />';
			}
			
			if(checkboxList.length == 1){
				var action='<%=path%>/otherStockData/updateWaterAndElectricAndGas.do?workdate='+workdate+'&type='+flag+'&pk_otherstockdataf='+checkboxList[0];
				$('body').window({
					id: 'window_updateIncome',
					title: '<fmt:message key="update" /><fmt:message key="utility" /><fmt:message key="the_detail" />('+prepayment+')',
					content: '<iframe id="UpdateForm" name="UpdateForm" frameborder="0" src='+action+'></iframe>',
					width: '800px',
					height: '500px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								title: '<fmt:message key="data" /><fmt:message key="update" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									window.document.getElementById("UpdateForm").contentWindow.saveData();
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}else if(checkboxList.length > 1){
				alert('<fmt:message key="you_can_only_modify_a_data" />！');
				return;
			}else {
				alert('<fmt:message key="please_select_information_you_need_to_modify"/>！');
				return ;
			}
		}
		
		//删除水电气
		function deleteAll(){
			var tab = $("#tabs").tabs("getSelected");
			var flag = tab.find("#flag").val();
			var checkboxList = [];
			var type = "";
			//判断flag的值，如果是”shui“，则取水表标签下的值；若是”dian“，则取电表标签下的值；若是”qi“，则取气表标签下的值
			if("shui" == flag){
				type = 1;
				var str = document.getElementsByName("idListWater"); 
				for (var i=0;i<str.length;i++ ){
					if(str[i].checked){
						checkboxList.push(str[i].value);
					}
				}
			}
			
			if ("dian" == flag) {
				type = 2;
				var str = document.getElementsByName("idListElectric"); 
				for (var i=0;i<str.length;i++ ){
					if(str[i].checked){
						checkboxList.push(str[i].value);
					}
				}
			}
			
			if ("qi" == flag) {
				type = 3;
				var str = document.getElementsByName("idListGas"); 
				for (var i=0;i<str.length;i++ ){
					if(str[i].checked){
						checkboxList.push(str[i].value);
					}
				}
			}
			
			if (1 > checkboxList.join(",").length) {
				alert('<fmt:message key="please_select_information_you_need_to_delete" />!');
				return true;
			} else {
				if (confirm('<fmt:message key="delete_data_confirm" />？')) {
					var action = '<%=path%>/otherStockData/deleteAll.do?id='+checkboxList.join(",")+'&type='+type;
					$('body').window({
						title: '<fmt:message key="delete" /><fmt:message key="data" />',
						content: '<iframe frameborder="0" src='+action+'></iframe>',
						width: '340px',
						height: '255px',
						draggable: true,
						isModal: true
					});
				}
			}
		}
		
		function pageReload(){
			$("#listForm").submit();
		}
		</script>
	</body>
</html>