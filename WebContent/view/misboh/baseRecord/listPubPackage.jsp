<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="package" />主表</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<style type="text/css">
			.separator{
				display:none !important;
			}
			.page{
					margin-bottom: 25px;
				}
			.separator ,div , .pgSearchInfo{
				display: none;
			}
			div[class]{
				display:block;
			}
			.tr-select{
				background-color: #D2E9FF;
			}
		</style>
	</head>
	<body>
		<div class="tool"></div>
			<form id="listForm" action="<%=path%>/baseRecord/listPubPackage.do" method="post">
				<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 20px;"></span></td>
									<td><span  style="width:30px;"></span>
<!-- 										<input type="checkbox" id="chkAll"/> -->
									</td>
									<td><span style="width:60px;"><fmt:message key="coding" /></span></td>
									<td><span style="width:150px;"><fmt:message key="name" /></span></td>
									<td><span style="width:60px;"><fmt:message key="Package_no" /></span></td>
									<td><span style="width:60px;"><fmt:message key="abbreviation" /></span></td>
									<td><span style="width:80px;"><fmt:message key="sales" /><fmt:message key="unit" /></span></td>
									<td><span style="width:100px;"><fmt:message key="Hall_food_prices" /></span></td>
									<td><span style="width:60px;"><fmt:message key="enable_state" /></span></td>
									<td><span style="width:60px;"><fmt:message key="the_AGAR_AGAR_emitting" /></span></td>
									<td><span style="width:100px;"><fmt:message key="package_type" /></span></td>
									<td><span style="width:100px;"><fmt:message key="Package_price_type" /></span></td>
									<td><span style="width:100px;"><fmt:message key="package" /><fmt:message key="break_up_way" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body"  style="overflow:visible;">
						<table cellspacing="0" cellpadding="0" class="datagrid">
							<tbody>
								<c:forEach var="pubpackage" items="${listPubPackage}" varStatus="status">
									<tr>
										<td class="num"><span style="width: 20px;">${status.index+1}</span></td>
										<td>
											<span style="width:30px; text-align: center;">
											<input type="checkbox" name="idList" onclick="CheckBoxCheck(this)"
												id="chk_<c:out value='${pubpackage.PK_PUBPACK }' />" value="${pubpackage.PK_PUBPACK}" />
											</span>
										</td>
										<td><span style="width:60px;">${pubpackage.VCODE}</span></td>
										<td><span style="width:150px;">${pubpackage.VNAME}</span></td>
										<td><span style="width:60px;">${pubpackage.VPACKCODE}</span></td>
										<td><span style="width:60px;">${pubpackage.VINIT}</span></td>
										<td><span style="width:80px;">${pubpackage.VSALEUNIT}</span></td>
										<td><span style="width:100px;text-align: right;"><fmt:formatNumber value="${pubpackage.NRESTPRICE}" pattern="###.00" /></span></td>
										<td>
											<span style="width:60px;" class="${pubpackage.ENABLESTATE}" id="${pubpackage.ENABLESTATE}">
												<c:if test="${pubpackage.ENABLESTATE==1}"><fmt:message key="not_enabled" /></c:if>
												<c:if test="${pubpackage.ENABLESTATE==2}"><fmt:message key="have_enabled" /></c:if>
												<c:if test="${pubpackage.ENABLESTATE==3}"><fmt:message key="stop_enabled" /></c:if>
											</span>
										</td>
										<td><span style="width:60px;text-align: right;"><fmt:formatNumber value="${pubpackage.IMAXFOOD}" pattern="###.00" /></span></td>
										<td><span style="width:100px;">${pubpackage.VGRPTYP}</span></td>
										<td>
											<span style="width:100px;"><c:if test="${pubpackage.VPRICETYP==1}"><fmt:message key="fixed" /><fmt:message key="price" /></c:if>
												<c:if test="${pubpackage.VPRICETYP==2}"><fmt:message key="collect" /><fmt:message key="price" /></c:if>
												<c:if test="${pubpackage.VPRICETYP==3}"><fmt:message key="high_priority" /><fmt:message key="price" /></c:if>
											</span>
										</td>
										<td>
											<span style="width:100px;"><c:if test="${pubpackage.VSPLITTYPE==1}"><fmt:message key="the_proportion_between" /></c:if>
											<c:if test="${pubpackage.VSPLITTYPE==2}"><fmt:message key="percentage_discount" /></c:if>
											<c:if test="${pubpackage.VSPLITTYPE==3}"><fmt:message key="manual_split" /></c:if>
											</span>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
			
			<div class="search-div" style="margin-left: 0px;">
				<div class="search-condition">
					<table class="search-table" cellspacing="0" cellpadding="0" >
						<tr>
							<td class="c-left"><fmt:message key="coding" />：</td>
							<td><input type="text" id="vcode" name="vcode" class="text" value="${pubPackage.vcode }" /></td>
							<td style="width: 20px;"></td>
							<td class="c-left"><fmt:message key="name" />：</td>
							<td><input type="text" id="vname" name="vname" class="text" value="${pubPackage.vname }" onkeyup="getSpInit(this,'vinit');" /></td>
							<td style="width: 20px;"></td>
							<td class="c-left"><fmt:message key="abbreviation" />：</td>
							<td><input type="text" id="vinit" name="vinit" class="text" value="${pubPackage.vinit }" /></td>
							<td style="width: 20px;"></td>
							<td class="c-left"><fmt:message key="category" />：</td>
							<td>
								<select id="vgrptyp" name="vgrptyp" class="select">
									<c:forEach var="packageType" items="${listPackageType}" varStatus="status">
										<option value="${packageType.pk_packagetype }" <c:if test="${pubPackage.vcode == packageType.pk_packagetype }">selected="selected"</c:if> >${packageType.vname }</option>
									</c:forEach>
								</select>
							</td>
						</tr>
					</table>
				</div>
				<div class="search-commit">
		       		<input type="button" class="search-button" id="search" value='<fmt:message key="enter" />'/>
		       		<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
				</div>
			</div>
		</form>
		<div id="wait2" style="display: none;"></div>
    	<div id="wait" style="display: none;">
			<img src="<%=path%>/image/loading_detail.gif" />&nbsp;
			<span style="color:white;font-size:15px;vertical-align: middle;"><fmt:message key="please_wait" />...</span>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript">
			//双击弹出<fmt:message key="update" />
			$('.grid').find('.table-body').find('tr').live("dblclick", function () {
				$(":checkbox").attr("checked", false);
				$(this).find(':checkbox').attr("checked", true);
				pubpackage();
				if($(this).hasClass("show-firm-row"))return;
				$('.grid').find('.table-body').find('tr').removeClass("show-firm-row");
				$(this).addClass("show-firm-row");
			 });
		
			$(document).ready(function(){
				$('.search-div').hide();
	// 			loadGrid();//  自动计算滚动条的js方法
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								$('.search-div').slideToggle(100);
								$('#vcode').focus();
							}
						},'-',{
							text: '<fmt:message key="detail_query" />',
							title: '<fmt:message key="detail_query" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								pubpackage();
							}
						},'-',{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}
					]
				});
			
				//<fmt:message key="select" />按钮
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#listForm').submit();
				});
				
				/* 模糊<fmt:message key="select" />清空 */
				$("#resetSearch").bind('click', function() {
					$('.search-condition input').val('');
				});
	
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),20);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
			});
		
			//实现复选框单选
			function CheckBoxCheck(chk){
			    var obj = $('.grid').find('.table-body').find('input[name=idList]');
			    for(var i = 0; i < obj.length; i++){
			        if(obj[i].type == "checkbox"){
			            obj[i].checked = false;
			        }
			    }
			    chk.checked=true;
			 }
		
			//调转到套餐详细页面
			function pubpackage(){
				var checkboxList = $('.grid').find('.table-body').find('input[name=idList]');
				var chkValue = checkboxList.filter(':checked').eq(0).val();
				var curpage = $("#nowPage").val();
				if(checkboxList && checkboxList.filter(':checked').size() ==1){
					$('body').window({
						title: '<fmt:message key="package_details" />',
						content: '<iframe id="updatePubItemFrame" frameborder="0" src="<%=path%>/baseRecord/updatePubPackage.do?pk_pubpack='+chkValue+'&nowPage='+curpage+'"></iframe>',
						width: '760px',
						height: '500px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="Replaceable_food" /><fmt:message key="select" />',
									title: '<fmt:message key="Replaceable_food" /><fmt:message key="select" />',
									useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['0px','-40px']
									},
									handler: function(){
										window.document.getElementById("updatePubItemFrame").contentWindow.listItemPkg();
									}
								},'-',{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList && checkboxList.filter(':checked').size() > 1){
					alert('<fmt:message key="you_can_only_view_a_data" />！');
					return;
				}else {
					alert('<fmt:message key="please_select_to_see"/>！');
					return ;
				}
			}
			function pageReload(){
				$("#listForm").submit();
		    }
		</script>
	</body>
</html>