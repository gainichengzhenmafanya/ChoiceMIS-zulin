<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="Dishes_set" /><fmt:message key="select1" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
				.grid td span{
					padding:0px;
				}
				.form-line .form-input{
					width: 17%;
					margin-right: 0px;
					padding-left: 0px;
				}
				.form-line .form-input input,.form-line .form-input select{
					width:90%;
				}
				.fieldsetXzZ {
					background-color: #E8F2FE;
					padding-top : 15px;
					position: relative;
					overflow: auto;
				}
				#tagContent {
					overflow:auto;
					BORDER-RIGHT: #aecbd4 1px solid; 
					PADDING-RIGHT: 1px; 
					BORDER-TOP: #aecbd4 1px solid; 
					PADDING-LEFT: 1px; 
					PADDING-BOTTOM: 1px; 
					BORDER-LEFT: #aecbd4 1px solid; 
					PADDING-TOP: 1px; 
					BORDER-BOTTOM: #aecbd4 1px solid; 
					BACKGROUND-COLOR: #fff
				}
				input[type=checkbox]{
					margin-top: 2px;
				}
			</style>
		</head>
	<body>
		<div id='toolbar'></div>
<%-- 		<form id="queryForm" name="queryForm" method="post"> --%>
		<div style="text-align: center;">
			<div  style=" MARGIN-RIGHT: auto; MARGIN-LEFT: auto;">
				<a style="font-size: 15px; color: red;"><fmt:message key="retrieval" /><fmt:message key="pubitem" />：</a>
				<input onkeydown="if(event.keyCode==13){ searchPubitem()}" id="fieldset_product_fHl" /> 
					
				<img class="search" src="<%=path%>/image/themes/icons/search.png" style="margin-top: 0px;"
						onclick="searchPubitem()" alt="<fmt:message key="retrieval" /><fmt:message key="product" />" /> 
			</div>
		</div>
		<div class="grid" style="overflow: auto;" id="pubitemDiv">
			<fieldset class="fieldsetXzZ" id="indexfieldset"  style="margin-left: 10px;margin-right: 10px;">
			<legend><input type="checkbox" id="chkAll"/><fmt:message key="future_generations" /></legend>
				<c:forEach var="mcone" items="${MarsaleclassList0}" varStatus="status">
					<fieldset class="fieldsetXzZ"  style="margin-left: 10px;margin-right: 10px;margin-bottom: 2px;border: 2px solid #CCD8FF;background-color: #E8F2FE;">
					<legend style="margin-left: 20px;border: 2px;"><input type="checkbox" name="idList" value="${mcone.PK_MARSALECLASS }" onclick="setchildren(this)"/>${mcone.NAME }</legend>
						<c:forEach var="mctwo" items="${MarsaleclassList1}" varStatus="status">
							<c:if test="${mctwo.PK_FATHER==mcone.PK_MARSALECLASS }">
								<fieldset class="fieldsetXzZ"  style="margin-left: 10px;margin-right: 10px;margin-top: 3px;">
								<legend style="margin-left: 30px"><input type="checkbox" name="idList" value="${mctwo.PK_MARSALECLASS }" onclick="setchildren(this)"/>${mctwo.NAME }</legend>
									<c:forEach var="pubitemtype" items="${pubItemList}" varStatus="status">
										<c:if test="${pubitemtype['PK_ID'] == mctwo.PK_MARSALECLASS }">
												<c:forEach var="pubitemflg" items="${pubitemtype['VALUELIST']}" varStatus="status">
													<div style="float: left; width: 200px;"><span title="${pubitemflg.vname }" style="width: 200px;">
														<input type="checkbox" name="idList" id="vname" value="${pubitemflg.vname }" style="margin-left: 20px;"/>${pubitemflg.vname }</span>
														<input type="hidden" id="vcode" value="${pubitemflg.vcode }"/>
														<input type="hidden" id="pk_pubitem" value="${pubitemflg.pk_pubitem }"/>
														<input type="hidden" id="vgrptyp" value="${pubitemflg.vgrptyp }"/>
														<input type="hidden" id="pk_vgrptyp" value="${pubitemflg.pk_vgrptyp }"/>
														<input type="hidden" id="vgrp" value="${pubitemflg.vgrp }"/>
														<input type="hidden" id="pk_vgrp" value="${pubitemflg.pk_vgrp }"/>
														<input type="hidden" id="vtyp" value="${pubitemflg.vtyp }"/>
														<input type="hidden" id="pk_vtyp" value="${pubitemflg.pk_vtyp }"/></div>
												</c:forEach>
										</c:if>
									</c:forEach>
								</fieldset>
							</c:if>
						</c:forEach>
					</fieldset>
				</c:forEach>
			</fieldset>
		</div>
<%-- 		</form> --%>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/boh/common/teleFunc-${sessionScope.locale}.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/boh/BoxSelect.js"></script>
		<script src="<%=path%>/js/gaoliang.js" type="text/javascript"></script>
		<script type="text/javascript">
		var selected=[];
			$(document).ready(function(){
				//<fmt:message key="future_generations" />
			     $("#chkAll").click(function(){
			    	 var fla=false;
			    	 if($('#chkAll').attr('checked')=="checked"){fla=true;}
					 //所有checkbox跟着<fmt:message key="future_generations" />的checkbox走。
					 $('[name=idList]:checkbox').each(function(){
					    if($(this).attr('disabled')){return;}//跳过已经<fmt:message key="select1" />的数据 
						$(this).attr("checked", fla);
					 });
				 });
				 $('[name=idList]:checkbox').click(function(){
							//定义一个临时变量，避免重复使用同一个<fmt:message key="select1" />器<fmt:message key="select1" />页面中的元素，提升程序效率。
							var $tmp=$('[name=idList]:checkbox');
							//用filter方法筛选出选中的复选框。并直接给chkAll赋值。
							$('#chkAll').each(function(){
								if($(this).attr('disabled')){return;}//跳过已经<fmt:message key="select1" />的数据 
								$(this).attr('checked',$tmp.length==$tmp.filter(':checked').length);
							});

						/*
							//一行做过多的事情需要写更多注释。复杂<fmt:message key="select1" />器还可能影响效率。因此不推荐如下写法。
							$('#chkAll').attr('checked',!$('[name=idList]:checkbox').filter(':not(:checked)').length);
						*/
				 });
				 setElementHeight('.grid',['#toolbar'],$(document.body),50);//计算.treePanel的高度
				 loadGrid();//  自动计算滚动条的js方法
				$("#toolbar").toolbar({
						items: [{
							text: '<fmt:message key="enter"/>',
							useable:true,
							handler: function(){
								var checkboxList = $('#indexfieldset').find(':checkbox');
								var data = {show:[],code:[],mod:[],entity:[]};
								checkboxList.filter(':checked').each(function(){
									if($(this).attr('disabled')){return;}//跳过已经<fmt:message key="select1" />的数据 
									if($(this).attr('id') != 'vname') {return;}//跳过不<fmt:message key="be" /><fmt:message key="pubitem" />的复选框
									var div = $(this).closest('div');
									data.code.push(div.find('#pk_pubitem').val());
// 									data.show.push($.trim($(this).val()));
									data.show.push(div.find('#vname').val());
									var entity = {};
									entity.vcode = $.trim(div.find('#vcode').val());
									entity.vname = $.trim(div.find('#vname').val());
									entity.pk_vgrptyp = $.trim(div.find('#pk_vgrptyp').val());
									entity.vgrptyp = $.trim(div.find('#vgrptyp').val());//<fmt:message key="a_class" />别
									entity.pk_vgrp = $.trim(div.find('#pk_vgrp').val());
									entity.vgrp = $.trim(div.find('#vgrp').val());//<fmt:message key="two_classes" />别
									entity.pk_vtyp = $.trim(div.find('#pk_vtyp').val());
									entity.vtyp = $.trim(div.find('#vtyp').val());//<fmt:message key="c" /><fmt:message key="category" />
									entity.pk_pubitem = $.trim(div.find('#pk_pubitem').val());
									
									data.entity.push(entity);
								});
								if("${domId}" == "selected"){
									parent.selected = data.code;
								}
								parent['${callBack}'](data);
								$(".close",parent.document).click();
							}
						},{
							text: '<fmt:message key="cancel"/>',
							useable: true,
							handler: function(){
								$(".close",parent.document).click();
							}
						}
					]
				});
				
				//获取父窗体已选<fmt:message key="pubitem" />id，若在父页面中存在，则使勾选框禁用
				if(typeof(parent.$('#vpcode')) != "undefined"){
					var idlist=$('#vpcode',parent.document).val().split(",");
					if(idlist)selected=idlist;
				}
				var checkboxList = $('#indexfieldset').find(':checkbox').each(function(){
					if($(this).attr('id') != 'vname') {return true;}//跳过不<fmt:message key="be" /><fmt:message key="pubitem" />的复选框
					var div = $(this).closest('div');
					var id=div.find('#pk_pubitem').val();
					if($.inArray(id,selected) >= 0){//若大于等于零，则这个id已存在父页面中，不<fmt:message key="allow" />再次勾选 
						$(this).attr('checked','checked');
// 						$(this).attr('disabled','disabled');
					}
				});
			
			});
			//<fmt:message key="set_up_the" />子集复选框，<fmt:message key="future_generations" />还<fmt:message key="be" />不<fmt:message key="future_generations" />
			function setchildren(flag){
				if($(flag).attr('checked')=="checked"){
					$(flag).closest('fieldset').find('input[name=idList]:checkbox').each(function(){
						if($(this).attr('disabled')){return;}//跳过已经<fmt:message key="select1" />的数据 
						$(this).attr("checked", true);	
					});
				}else{
					$(flag).closest('fieldset').find('input[name=idList]:checkbox').each(function(){
						if($(this).attr('disabled')){return;}//跳过已经<fmt:message key="select1" />的数据 
						$(this).attr("checked", false);
					});
				}
			}
			//搜索菜品
			function searchPubitem(divId,inputId){
				pre_fHl(pubitemDiv, fieldset_product_fHl.value);
				//将滚动条置顶
				document.documentElement.scrollTop=-10;
			}
		</script>
	</body>
</html>