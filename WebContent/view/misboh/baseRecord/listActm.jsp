<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="activity" /><fmt:message key="plan" /><fmt:message key="set_up_the" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		
		<style type="text/css">
			.separator{
				display:none !important;
			}
			.page{
				margin-bottom: 28px;
			}
			.separator ,div , .pgSearchInfo{
				display: none;
			}
			div[class]{
				display:block;
			}
			.tr-select{
				background-color: #D2E9FF;
			}
		</style>
	</head>
	<body>
		<div class="tool">
		
		</div>
		<form id="listForm" action="<%=path%>/baseRecord/listActm.do" method="post">
				<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 20px;"></span></td>
									<td style="width:40px;">
<!-- 										<input type="checkbox" id="chkAll"/> -->
									</td>
									<td><span style="width:60px;"><fmt:message key="coding" /></span></td>
									<td><span style="width:140px;"><fmt:message key="name" /></span></td>
									<td><span style="width:50px;"><fmt:message key="enable_state" /></span></td>
									<td><span style="width:60px;"><fmt:message key="date" /><fmt:message key="limit" /></span></td>
									<td><span style="width:60px;"><fmt:message key="time" /><fmt:message key="limit" /></span></td>
									<td><span style="width:60px;"><fmt:message key="flight_restrictions" /></span></td>
									<td><span style="width:60px;"><fmt:message key="amount_limitation" /></span></td>
									<td><span style="width:60px;"><fmt:message key="product" /><fmt:message key="limit" /></span></td>
<%-- 									<td><span style="width:60px;"><fmt:message key="bill_breaks" /></span></td> --%>
<%-- 									<td><span style="width:60px;"><fmt:message key="enable" /><fmt:message key="pricing" /></span></td> --%>
									<td><span style="width:80px;"><fmt:message key="perform_activities" /></span></td>
									<td><span style="width:60px;"><fmt:message key="number_of_people" /><fmt:message key="limit" /></span></td>
									<td><span style="width:60px;"><fmt:message key="a_number_of" /><fmt:message key="limit" /></span></td>
									<td><span style="width:60px;"><fmt:message key="incompatible_activities" /></span></td>
									<td><span style="width:80px;">PAD<fmt:message key="whether_useful" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0" class="datagrid">
							<tbody>
								<c:forEach var="Actm" items="${listActm}" varStatus="status">
									<tr>
										<td class="num"><span style="width: 20px;">${status.index+1}</span></td>
										<td>
											<span style="width:30px; text-align: center;">
											<input type="checkbox" name="idList" onclick="CheckBoxCheck(this)"
												id="chk_<c:out value='${Actm.pk_actm }' />" value="${Actm.pk_actm}" />
											</span>
										</td>
										<td><span style="width:60px;">${Actm.vcode}</span></td>
										<td><span style="width:140px;">${Actm.vname}</span></td>
										<td><span style="width:50px;">
											<c:if test="${Actm.enablestate==1}"><fmt:message key="not_enabled" /></c:if> 
											<c:if test="${Actm.enablestate==2}"><fmt:message key="have_enabled" /></c:if>
											<c:if test="${Actm.enablestate==3}"><fmt:message key="stop_enabled" /></c:if> 
										</span></td>
										<td><span style="width:60px; text-align: center;">
											<c:if test="${Actm.bisdate == 'Y'}"><fmt:message key="be" /></c:if>
											<c:if test="${Actm.bisdate != 'Y'}"><fmt:message key="no1" /></c:if>
										</span></td>
										<td><span style="width:60px; text-align: center;">
											<c:if test="${Actm.bistime == 'Y'}"><fmt:message key="be" /></c:if>
											<c:if test="${Actm.bistime != 'Y'}"><fmt:message key="no1" /></c:if>
										</span></td>
										<td><span style="width:60px; text-align: center;">
											<c:if test="${Actm.bisclasses == 'Y'}"><fmt:message key="be" /></c:if>
											<c:if test="${Actm.bisclasses != 'Y'}"><fmt:message key="no1" /></c:if>
										</span></td>
										<td><span style="width:60px; text-align: center;">
											<c:if test="${Actm.bisamount == 'Y'}"><fmt:message key="be" /></c:if>
											<c:if test="${Actm.bisamount != 'Y'}"><fmt:message key="no1" /></c:if>
										</span></td>
										<td><span style="width:60px; text-align: center;">
											<c:if test="${Actm.bispubitem == 'Y'}"><fmt:message key="be" /></c:if>
											<c:if test="${Actm.bispubitem != 'Y'}"><fmt:message key="no1" /></c:if>
										</span></td>
<!-- 										<td><span style="width:60px; text-align: center;"> -->
<%-- 											<c:if test="${Actm.bremit == 'Y'}"><fmt:message key="be" /></c:if> --%>
<%-- 											<c:if test="${Actm.bremit != 'Y'}"><fmt:message key="no1" /></c:if> --%>
<!-- 										</span></td> -->
<!-- 										<td><span style="width:60px; text-align: center;"> -->
<%-- 											<c:if test="${Actm.badjust == 'Y'}"><fmt:message key="be" /></c:if> --%>
<%-- 											<c:if test="${Actm.badjust != 'Y'}"><fmt:message key="no1" /></c:if> --%>
<!-- 										</span></td> -->
										<td><span style="width:80px; text-align: left;">
											<c:if test="${Actm.bremit == 'Y'}"><fmt:message key="bill_breaks" /></c:if>
											<c:if test="${Actm.badjust == 'Y'}"><fmt:message key="enable" /><fmt:message key="pricing" /></c:if>
											<c:if test="${Actm.voucherback == 'Y'}"><fmt:message key="Consumption_coupons" /></c:if>
											<c:if test="${Actm.vfenback == 'Y'}"><fmt:message key="Consumer_return_integral" /></c:if>
										</span></td>
										<td><span style="width:60px; text-align: center;">
											<c:if test="${Actm.biscount == 'Y'}"><fmt:message key="be" /></c:if>
											<c:if test="${Actm.biscount != 'Y'}"><fmt:message key="no1" /></c:if>
										</span></td>
										<td><span style="width:60px; text-align: center;">
											<c:if test="${Actm.bisticket == 'Y'}"><fmt:message key="be" /></c:if>
											<c:if test="${Actm.bisticket != 'Y'}"><fmt:message key="no1" /></c:if>
										</span></td>
										<td><span style="width:60px; text-align: center;">
											<c:if test="${Actm.bishchd == 'Y'}"><fmt:message key="be" /></c:if>
											<c:if test="${Actm.bishchd != 'Y'}"><fmt:message key="no1" /></c:if>
										</span></td>
										<td><span style="width:80px; text-align: center;">
											<c:if test="${Actm.showinpad == 'Y'}"><fmt:message key="be" /></c:if>
											<c:if test="${Actm.showinpad != 'Y'}"><fmt:message key="no1" /></c:if>
										</span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>			
				</div>
			<div class="search-div" style="margin-left: 0px;">
				<div class="search-condition">
					<table class="search-table" cellspacing="0" cellpadding="0" >
						<tr>
							<td class="c-left"><fmt:message key="coding" />：</td>
							<td><input type="text" id="vcode" name="vcode" class="text" value="${actm.vcode }" /></td>
							<td style="width: 20px;"></td>
							<td class="c-left"><fmt:message key="name" />：</td>
							<td><input type="text" id="vname" name="vname" class="text" value="${actm.vname }" /></td>
							<td style="width: 20px;"></td>
						</tr>
					</table>
				</div>
				<div class="search-commit">
		       		<input type="button" class="search-button" id="search" value='<fmt:message key="enter" />'/>
		       		<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
				</div>
			</div>
		</form>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		
 		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
			//双击弹出<fmt:message key="update" />
			$('.grid').find('.table-body').find('tr').live("dblclick", function () {
				$(":checkbox").attr("checked", false);
				$(this).find(':checkbox').attr("checked", true);
				actmDetail();
				if($(this).hasClass("show-firm-row"))return;
				$('.grid').find('.table-body').find('tr').removeClass("show-firm-row");
				$(this).addClass("show-firm-row");
			 });
			
			$(document).ready(function(){
				$('.search-div').hide();
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								$('.search-div').slideToggle(100);		
								$("#vcode").focus();
							}
						},'-',{
							text: '<fmt:message key="detail_query" />',
							title: '<fmt:message key="detail_query" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								actmDetail();
							}
						},'-',{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}
					]
				});
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#listForm').submit();
				});
				
				/* 模糊<fmt:message key="select" />清空 */
				$("#resetSearch").bind('click', function() {
					$('.search-condition input').val('');
				});
// 				自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),10);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				
				changeTh();//可以手动拉伸表格大小
			});
			function pageReload(){
				$('#listForm').submit();
			}
			
			//实现复选框单选
			function CheckBoxCheck(chk){
			    var obj = $('.grid').find('.table-body').find('input[name=idList]');
			    for(var i = 0; i < obj.length; i++){
			        if(obj[i].type == "checkbox"){
			            obj[i].checked = false;
			        }
			    }
			    chk.checked=true;
			}
			
			//活动显示详细
			function actmDetail(){
				var checkboxList = $('.grid').find('.table-body').find('input[name=idList]');
				var chkValue = checkboxList.filter(':checked').eq(0).val();
				if(checkboxList && checkboxList.filter(':checked').size() ==1){
					$('body').window({
						title: '<fmt:message key="activity" /><fmt:message key="the_detail" />',
						content: '<iframe id="updatePubItemFrame" frameborder="0" src="<%=path%>/baseRecord/actmDetail.do?pk_actm='+chkValue+'"></iframe>',
						width: '750px',
						height: '500px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList && checkboxList.filter(':checked').size() > 1){
					alert('<fmt:message key="you_can_only_modify_a_data" />！');
					return;
				}else {
					alert('<fmt:message key="please_select_information_you_need_to_modify"/>！');
					return ;
				}
			}
		</script>
	</body>
</html>
