<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>

<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="insert"/><fmt:message key="package" /><fmt:message key="set_up_the" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
    	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>	   
		<style type="text/css">
			.table-head{
				width: 800px;
			}
			.table-body{
				width: 750px;
			}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<div class="form" style="float: left;height: auto;">
			<form id="listForm" method="post" action="<%=path %>/otherStockData/listOtherStockData.do" >
				<div class="form-line">
					<div class="form-label"><fmt:message key="project" /><fmt:message key="name" />：</div>
					<div class="form-input">
						<input type="hidden" id="meterrate" name="meterrate" class="text" value="${dataF.meterrate }"/>
						<input type="hidden" id="pk_otherstockdataf" name="pk_otherstockdataf" class="text" value="${dataF.pk_otherstockdataf }"/>
						<input type="hidden" id="pk_store" name="pk_store" class="text" value="${dataF.pk_store }"/>
						<input type="hidden" id="ecode" name="ecode" class="text" value="${dataF.ecode }"/>
						<input type="text" id="vname" name="vname" class="text" value="${dataF.vname }" disabled="disabled"/>
					</div>
					<div class="form-label"><fmt:message key="scm_reference" /><fmt:message key="price" />：</div>
					<div class="form-input">
						<input type="text" id="referencePrice" name="referencePrice" class="text" value="<fmt:formatNumber value="${dataF.price }" pattern="###0.00"/>" disabled="disabled"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="unit" />：</div>
					<div class="form-input">
						<input type="text" id="unit" name="unit" class="text" disabled="disabled"
							<c:if test="${statistical == 'shui' }">value='<fmt:message key="dun" />'</c:if>
							<c:if test="${statistical == 'dian' }">value='<fmt:message key="du" />'</c:if>
							<c:if test="${statistical == 'qi' }">value='<fmt:message key="lifangmi" />'</c:if> />
					</div>
					<div class="form-label"><fmt:message key="unit_price" />：</div>
					<div class="form-input">
						<input type="text" id="price" name="price" onblur="updatePrice()" class="text" value="<fmt:formatNumber value="${dataF.price }" pattern="##0.00"/>" />
					</div>
				</div>
				<div class="grid">
					<div class="table-head">
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td style="width: 25px;">&nbsp;</td>
									<td style="width:80px;"><fmt:message key="date" /></td>
									<td style="width:50px;"><fmt:message key="week" /></td>
									<td style="width:75px;"><fmt:message key="beginning_of_period" /></td>
									<td style="width:133px;"><fmt:message key="The_end_of_the_semester" /></td>
									<td style="width:133px;"><fmt:message key="buy" /><fmt:message key="quantity" /></td>
									<td style="width:75px;"><fmt:message key="scm_consumption" /></td>
									<td style="width:133px;"><fmt:message key="unit_price" /></td>
									<td style="width:80px;"><fmt:message key="amount" /></td>
									<td style="visibility: hidden;"></td>
									<td style="visibility: hidden;"></td>
									<td style="visibility: hidden;"></td>
									<td style="visibility: hidden;"></td>
									<td style="visibility: hidden;"></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0" style="width: 1500px;" id="tableBody">
							<tbody>
								<c:forEach var="allData" items="${allDataO}" varStatus="status">
									<tr <c:if test="${'' == allData.week }">bgcolor="#D2E9FF"</c:if>>
										<td class="num" style="width: 25px;">${status.index+1}</td>
										<td style="width:80px;text-align: left;" id="workdate${status.index+1}">
											<span>${allData.workdate}
											<input type="hidden" value="${allData.flag}" id="flag${status.index+1}"/>
											</span>
										</td>
										<td style="width:50px;" id="week${status.index+1}">
											<c:if test="${allData.week != ''}">
												<span><c:out value="${allData.week}" /></span>
											</c:if>
											<c:if test="${allData.week == ''}">
												<span style="visibility: hidden;"><c:out value="1" /></span>
											</c:if>
										</td>
										<td style="width:75px;text-align: right;" id="begincount${status.index+1}">
											<span><c:out value="${allData.begincount}" /></span>
										</td>
										<td style="width:100px;">
											<c:if test="${allData.week != '' }">
												<input type="text" id="endcount${status.index+1}" name="endcount" class="text" 
<%-- 													onblur="endCount(${status.index+1})" --%>
													value="<fmt:formatNumber value="${allData.endcount}" pattern="##0.00"/>" style="text-align: right;"
<%-- 													<c:if test="${allData.flag == 'false' }">disabled="disabled"</c:if>  --%>
												/>
											</c:if>
										</td>
										<td style="width:80px;">
											<c:if test="${allData.week != '' }">
												<input type="text" name="incount" id="incount${status.index+1}" class="text" onblur="updateAmount(${status.index+1})"
													   value="<fmt:formatNumber value="${allData.incount}" pattern="##0.00"/>" style="text-align: right;"
													<c:if test="${dataF.prepayment == 0 }">disabled="disabled"</c:if> 
													<c:if test="${allData.flag == 'false' }">disabled="disabled"</c:if>/>
											</c:if>
										</td>
										<td style="width:75px;text-align: right;" id="outcount${status.index+1}">
											<span><fmt:formatNumber value="${allData.outcount}" pattern="##0.00"/></span>
										</td>
										<td style="width:80px;">
											<c:if test="${allData.week != '' }">
												<input type="text" id="price${status.index+1}" name="price" class="text" onblur="updateAmount(${status.index+1})"
													   value="<fmt:formatNumber value="${allData.price}" pattern="##0.00"/>" style="text-align: right;"
												<c:if test="${allData.flag == 'false' }">disabled="disabled"</c:if>/>
											</c:if>
										</td>
										<td style="width:80px;text-align: right;" id="amount${status.index+1}">
											<fmt:formatNumber value="${allData.amount}" pattern="##0.00" />
										</td>
										<td style="visibility: hidden;">
											<span><c:out value="${dataF.pk_otherstockdataf}" /></span>
										</td>
										<td style="visibility: hidden;">
											<span><c:out value="${dataF.ecode}" /></span>
										</td>
										<td style="visibility: hidden;">
											<span><c:out value="${dataF.pk_store}" /></span>
										</td>
										<td style="visibility: hidden;" id="prepayment${status.index+1}">
											<span><c:out value="${dataF.prepayment}" /></span>
										</td>
										<td style="visibility: hidden;" id="type">
											<span><c:out value="${dataF.type}" /></span>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>			
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	  	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/boh/common/teleFunc-${sessionScope.locale}.js"></script>		
		<script type="text/javascript" src="<%=path%>/js/boh/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/colorpicker/jscolor.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				// 自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),41);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
			});

			var saveData = function(){
				var flag = true;
				$.ajaxSetup({async:false});
				var data = {};
				var type = $("#type").text();
				$("#tableBody").find("tr").each(function(i){
					if (updateAmount(i+1)) {
						var begincount = $("#begincount"+(i+1)).text();						//期初
						var endcount = $.trim($(this).find("td:eq(4)").find("input").val());//期末
						var incount = $.trim($(this).find("td:eq(5)").find("input").val());	//购入量
						var prepayment = $("#prepayment"+(i+1)).text(); 					//付费方式
						var outcount = 0;													//用量
					
						//判断付费类型计算用量，如果是预付费，则用量=期初+购入-期末；如果是后付费，则用量=期末-期初
						if (1 == prepayment) {
							outcount = Number(begincount) + Number(incount) - Number(endcount);
						}
						if (0 == prepayment) {
							outcount = Number(endcount) - Number(begincount);
						}
						data["stockDataList["+i+"].workdate"]=$.trim($(this).find("td:eq(1)").find("span").text());
						data["stockDataList["+i+"].week"]=$.trim($(this).find("td:eq(2)").find("span").text());
						data["stockDataList["+i+"].begincount"]= $("#begincount"+(i+1)).text();
	                    data["stockDataList["+i+"].endcount"]= $.trim($(this).find("td:eq(4)").find("input").val());
		                data["stockDataList["+i+"].incount"]= $.trim($(this).find("td:eq(5)").find("input").val());
	                    // 验证是否最后一天数据（当前录入）
	                    if(Number(outcount) < 0){
	                    	outcount= 0.00;
	                    }
	                    data["stockDataList["+i+"].outcount"]= outcount;
	                    data["stockDataList["+i+"].price"]=$.trim($(this).find("td:eq(7)").find("input").val());
	                    data["stockDataList["+i+"].pk_otherstockdataf"]=$.trim($(this).find("td:eq(9)").find("span").text());
	                    data["stockDataList["+i+"].ecode"]=$.trim($(this).find("td:eq(10)").find("span").text());
	                    data["stockDataList["+i+"].pk_store"]=$.trim($(this).find("td:eq(11)").find("span").text());
	                    data["stockDataList["+i+"].type"]=$.trim($(this).find("td:eq(13)").find("span").text());
					} else {
						flag = false;
					}
				});
				if (flag) { 
					$.post("<%=path%>/otherStockData/saveOtherStockData.do?type="+type,data,function(){
						location.href="<%=path%>/baseRecord/toList.do";
					});
				}
			};
			
			//当修改表格顶上的单价时，表格中所有行的单价都变为输入的那个值
			function updatePrice(){
				var price1 = $("#price").val();
				
				$("#tableBody").find("tr").each(function(i){
					i= i+1;
					if($("#flag"+i).val() == 'true'){
	    				var outcount = $("#outcount"+i).text();		//用量
    	                $.trim($(this).find("td:eq(7)").find("input").val(price1));
    					$("#amount"+i).text((Number(price1) * outcount).toFixed(2));//为计算的金额赋值
					}
				});
				total();
			}
			
			//输入期末后，以后所有的期初变为这个值
			function endCount(i){
				flag = true;
				
				// 计算用量  金额 验证  期末数量是否符合规则
				if (updateAmount(i)) {
					flag = false;
// 					return;
				}
				
				var endcount = $("#endcount"+i).val();
				
				if (typeof(endcount) != "undefined") {
					for (var k = i; k < '${allDataOCount}' - 2; k++) {
						var week = $("#week"+(k+1)).text();
						if (1 != week) {
							$("#begincount"+(k+1)).text(Number(endcount).toFixed(2));
						} else {
							$("#begincount"+(k+1)).text();
						}
					}
				}
				return flag;
			}
			
			//改变购入数量和单价时，用量和金额也改变
			function updateAmount(i){
				var flag = true;
				var dat = $.trim($("#workdate"+i).text());
				var outcount = 0;							//定义用量
				var begincount = $("#begincount"+i).text();	//期初
				var endcount = $("#endcount"+i).val();		//期末
				var incount = $("#incount"+i).val(); 		//购入数量
				var price = $("#price"+i).val();     		//单价
				var prepayment = $("#prepayment"+i).text(); //付费方式
				var meterrate = $("#meterrate").val();
				//判断付费类型计算用量，如果是预付费，则用量=期初+购入-期末；如果是后付费，则用量=期末-期初
				if (1 == prepayment) {
					outcount = Number(begincount) + Number(incount) - Number(endcount);
				}else {
					outcount = Number(endcount) - Number(begincount);
				}
				
				if (outcount < 0 && dat <= '${workdate}') {
					if(1 == prepayment){
						flag = false;
						alert("【"+dat+"】期末不能大于购入加期初！");
						$("#endcount"+i).focus();
					}else{
						flag = false;
						alert("【"+dat+"】期末不能小于期初！");
						$("#endcount"+i).focus();
					}
					return;
				}
				if(meterrate!=0){
					outcount = meterrate*outcount;
				}
				$("#outcount"+i).text(Number(outcount).toFixed(2));	  		//为计算的用量赋值
				$("#amount"+i).text((Number(price) * outcount).toFixed(2));	//为计算的金额赋值
				
				total();
				return flag;
			}
			function total(){
				
				var count = 0;								//定义一个变量用于计算周计的用量
				var weekCount = 0;							//定义一个变量用于计算周计的用量
				var amount = 0;								//定义金额
				var weekAmount = 0;							//定义一个变量用于计算周计的金额
				
				// 设定月合计
				var monthcnt = 0;
				var monthamt = 0;
				var ts = $("#tableBody").find("tr").length;
				//循环计算周计
				$("#tableBody").find("tr").each(function(a){
					if (1 == $("#week"+a).find("span").text()) {
 						$("#outcount"+a).text("00.00");
						$("#amount"+a).text("00.00");
						count = count + Number($("#outcount"+a).text());
						amount = amount + Number($("#amount"+a).text());
						
						if (isNaN(Number(amount) - Number(weekAmount))) {
							$("#amount"+a).text("00.00");
						}else{
							$("#amount"+a).text((Number(amount) - Number(weekAmount)).toFixed(2));
						}
						if (isNaN(Number(count) - Number(weekCount))) {
							$("#outcount"+a).text("00.00");
						}else{
							$("#outcount"+a).text((Number(count) - Number(weekCount)).toFixed(2));
						}
						weekCount = count;
						weekAmount = amount;
					}else{
						count = count + Number($("#outcount"+a).text());
						amount = amount + Number($("#amount"+a).text());
						monthcnt = monthcnt + Number($("#outcount"+a).text());
						monthamt = monthamt + Number($("#amount"+a).text());
					}
				});
					$("#outcount"+ts).text("00.00");
					$("#amount"+ts).text("00.00");
					$("#amount"+ts).text((Number(monthamt)).toFixed(2));
					$("#outcount"+ts).text((Number(monthcnt)).toFixed(2));
			}
		</script>
	</body>
</html>