<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="Dishes_set" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<style type="text/css">
			.separator{
				display:none !important;
			}
			.page{
				margin-bottom: 45px;
			}
			.separator ,div , .pgSearchInfo{
				display: none;
			}
			div[class]{
				display:block;
			}
			.tr-select{
				background-color: #D2E9FF;
			}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<form id="listForm" action="<%=path%>/baseRecord/listPubitemPrint.do" method="post">
			<div class="grid" style="overflow:auto">
				<div class="table-head" style="overflow:visible;">
					<table cellspacing="0" cellpadding="0" id="headTable">
						<thead>
							<tr>
								<td class="num"><span style="width: 50px;"></span></td>
								<td><span style="width:70px;"><fmt:message key="scm_pubitem_code" /></span></td>
								<td><span style="width:120px;"><fmt:message key="scm_pubitem_name" /></span></td>
								<td><span style="width:70px;"><fmt:message key="Department_stores" /></span></td>
								<td><span style="width:70px;"><fmt:message key="print_num" /></span></td>
								<td><span style="width:70px;"><fmt:message key="main_print_machine" /></span></td>
								<td><span style="width:70px;"><fmt:message key="secondary_print_machine" /></span></td>
								<td><span style="width:70px;"><fmt:message key="main_print_machine3" /></span></td>
								<td><span style="width:70px;"><fmt:message key="main_print_machine4" /></span></td>
								<td><span style="width:70px;"><fmt:message key="main_print_machine5" /></span></td>
								<td><span style="width:70px;"><fmt:message key="main_print_machine6" /></span></td>
								<td><span style="width:70px;"><fmt:message key="total_print_machine" /></span></td>
								<td><span style="width:70px;"><fmt:message key="total_print_machine2" /></span></td>
								<td><span style="width:70px;"><fmt:message key="total_print_machine3" /></span></td>
								<td><span style="width:70px;"><fmt:message key="total_print_machine4" /></span></td>
								<td><span style="width:70px;"><fmt:message key="spare_print_machine" /></span></td>
								<td><span style="width:70px;"><fmt:message key="secondary_spare_print_machine" /></span></td>
								<td><span style="width:70px;"><fmt:message key="vorder" /><fmt:message key="print_machine" /></span></td>
								<td><span style="width:70px;">是否KVS显示</span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body" style="overflow:visible;">
					<table cellspacing="0" cellpadding="0" class="datagrid">
						<tbody>
							<c:forEach var="pubitemObj" items="${listPubitemPrint}" varStatus="status">
								<tr>
									<td class="num"><span style="width: 50px;">${status.index+1}</span></td>
									<td><span style="width:70px;" title="${pubitemObj['VCODE']}">${pubitemObj['VCODE']}</span></td>
									<td><span style="width:120px;" title="${pubitemObj['VNAME']}">${pubitemObj['VNAME']}</span></td>
									<td><span style="width:70px;" title="${pubitemObj['VSTOREDEPTNAME']}">${pubitemObj['VSTOREDEPTNAME']}</span></td>
									<td><span style="width:70px; text-align:right;" title="${pubitemObj['IPRNCNT']}">${pubitemObj['IPRNCNT']}</span></td>
									<td><span style="width:70px;" title="${pubitemObj['VDEPTPRN1NAME']}">${pubitemObj['VDEPTPRN1NAME']}</span></td>
									<td><span style="width:70px;" title="${pubitemObj['VDEPTPRN2NAME']}">${pubitemObj['VDEPTPRN2NAME']}</span></td>
									<td><span style="width:70px;" title="${pubitemObj['VDEPTPRN3NAME']}">${pubitemObj['VDEPTPRN3NAME']}</span></td>
									<td><span style="width:70px;" title="${pubitemObj['VDEPTPRN4NAME']}">${pubitemObj['VDEPTPRN4NAME']}</span></td>
									<td><span style="width:70px;" title="${pubitemObj['VDEPTPRN5NAME']}">${pubitemObj['VDEPTPRN5NAME']}</span></td>
									<td><span style="width:70px;" title="${pubitemObj['VDEPTPRN6NAME']}">${pubitemObj['VDEPTPRN6NAME']}</span></td>
									<td><span style="width:70px;" title="${pubitemObj['VTOTALPRNNAME']}">${pubitemObj['VTOTALPRNNAME']}</span></td>
									<td><span style="width:70px;" title="${pubitemObj['VTOTALPRN2NAME']}">${pubitemObj['VTOTALPRN2NAME']}</span></td>
									<td><span style="width:70px;" title="${pubitemObj['VTOTALPRN3NAME']}">${pubitemObj['VTOTALPRN3NAME']}</span></td>
									<td><span style="width:70px;" title="${pubitemObj['VTOTALPRN4NAME']}">${pubitemObj['VTOTALPRN4NAME']}</span></td>
									<td><span style="width:70px;" title="${pubitemObj['VBAKPRN1NAME']}">${pubitemObj['VBAKPRN1NAME']}</span></td>
									<td><span style="width:70px;" title="${pubitemObj['VBAKPRN2NAME']}">${pubitemObj['VBAKPRN2NAME']}</span></td>
									<td><span style="width:70px;" title="${pubitemObj['VORDPRNNAME']}">${pubitemObj['VORDPRNNAME']}</span></td>
									<td><span style="width:70px;" title="${pubitemObj['VSHOWKVS']}">${pubitemObj['VSHOWKVS']}</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>			
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />	
			
			<div class="search-div" style="margin-left: 0px;">
				<div class="search-condition">
					<table class="search-table" cellspacing="0" cellpadding="0" >
						<tr>
							<td class="c-left"><fmt:message key="coding" />：</td>
							<td><input type="text" id="vcode" name="vcode" class="text" value="${pubitem.vcode }" /></td>
							<td style="width: 20px;"></td>
							<td class="c-left"><fmt:message key="name" />：</td>
							<td><input type="text" id="vname" name="vname" class="text" value="${pubitem.vname }" onkeyup="getSpInit(this,'vinit');" /></td>
							<td style="width: 20px;"></td>
						</tr>
					</table>
				</div>
				<div class="search-commit">
		       		<input type="button" class="search-button" id="search" value='<fmt:message key="enter" />'/>
		       		<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
				</div>
			</div>
		</form>
		<div id="wait2" style="display: none;"></div>
    	<div id="wait" style="display: none;">
			<img src="<%=path%>/image/loading_detail.gif" />&nbsp;
			<span style="color:white;font-size:15px;vertical-align: middle;"><fmt:message key="please_wait" />...</span>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				var tool = $('.tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							$('.search-div').slideToggle(100);
							$('#vcode').focus();//点击查询按钮，聚焦查询框
						}
					},'-',{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}
					]
				});
					
				$('.search-div').hide();
					
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#listForm').submit();
				});
				
				/* 模糊<fmt:message key="select" />清空 */
				$("#resetSearch").bind('click', function() {
					$('.search-condition input').val('');
				});
	
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body), 30);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
			});
			function pageReload(){
				$("#listForm").submit();
			}
		</script>
	</body>
</html>