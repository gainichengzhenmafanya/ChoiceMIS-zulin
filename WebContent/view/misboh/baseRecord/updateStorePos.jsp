<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="branches_and_positions_information" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<style type="text/css">
			.form {
				position: relative;
				width: 90%;
				padding-top: 30px;
			}
			.store{
				width:200px;
				background-image:url("<%=path%>/image/themes/icons/searchmul1.png");
				background-position:right;
				background-repeat:no-repeat;
				cursor: pointer;
			}
		</style>
	</head>
	<body>
		<div class="form">
			<form id="storePosUpdateForm" method="post" action="<%=path %>/baseRecord/updateStorePos.do">
				<div class="form-line">
					<input type="hidden" id="pk_store" name="pk_store" value="${storePos.pk_store }"/>
					<input type="hidden" value="${storePos.pk_pos }" id="pk_pos" name="pk_pos"/>
					<div class="form-label"><span class="red">*</span><fmt:message key="coding" />:</div>
					<div class="form-input">
						<input type="text" id="icode" name="icode" class="text" value="${storePos.icode }" readonly="readonly" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="name" /></div>
					<div class="form-input">
						<input type="text" class="text" id="vmemo" name="vmemo" value="${storePos.vmemo }" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span>PosIP:</div>
					<div class="form-input">
						<input type="text" id="posip" name="posip" class="text" value="${storePos.posip }" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="enable_state" />:</div>
					<div class="form-input">
						<select id="enablestate" name="enablestate" class="select">
							<option value="1" <c:if test="${storePos.enablestate == '1' }">selected="selected"</c:if> ><fmt:message key="not_enabled" /></option>
							<option value="2" <c:if test="${storePos.enablestate == '2' }">selected="selected"</c:if> ><fmt:message key="have_enabled" /></option>
							<option value="3" <c:if test="${storePos.enablestate == '3' }">selected="selected"</c:if> ><fmt:message key="stop_enabled" /></option>
						</select>
					</div>
				</div>
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'posip',
						validateType:['canNull','ip'],
						param:['F','F'],
						error:['IP<fmt:message key="cannot_be_empty" />!','IP<fmt:message key="incorrect_format" />!']
					},{
						type:'text',
						validateObj:'vmemo',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="name" /><fmt:message key="cannot_be_empty" />!']
					}]
				});
			});
		function storePosUpdateForm(){
			$('#storePosUpdateForm').submit();
		}
		</script>
	</body>
</html>