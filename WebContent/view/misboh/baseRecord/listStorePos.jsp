<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>POS<fmt:message key="set_up_the" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			
			<style type="text/css">
				.page{
					margin-bottom: 30px;
				}
				.store{
				background-image:url("<%=path%>/image/themes/icons/searchmul1.png");
				background-position:right;
				background-repeat:no-repeat;
				cursor: pointer;
			}
			</style>
		</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/baseRecord/listStorePos.do" method="post">
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td style="width: 25px;">&nbsp;</td>
								<td style="width:30px;">
<!-- 									<input type="checkbox" id="chkAll"/> -->
								</td>
								<td style="width:100px;"><fmt:message key="coding" /></td>
								<td style="width:200px;"><fmt:message key="name" /></td>
								<td style="width:150px;">IP</td>
								<td style="width:60px;"><fmt:message key="enable_state" /></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="storePos" items="${listStorePos}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" name="idList" onclick="CheckBoxCheck(this)"
										id="chk_<c:out value='${storePos.pk_pos}' />" value="<c:out value='${storePos.pk_pos}' />"/>
										<input type="hidden" name="pk_store" id="pk_store" value="${storePos.pk_store }" />
									</td>
									<td style="width:100px;text-align: left;"><span style="width: 90px;"><c:out value="${storePos.icode}" /></span></td>
									<td style="width:200px;"><span style="width: 190px;"><c:out value="${storePos.vmemo}" /></span></td>
									<td style="width:150px;text-align: left;"><span style="width: 90px;"><c:out value="${storePos.posip}" /></span></td>
									<td style="width:50px;text-align: center;">
										<span style="width: 50px;">
											<c:if test="${storePos.enablestate=='1'}"><fmt:message key="not_enabled" /></c:if>
											<c:if test="${storePos.enablestate=='2'}"><fmt:message key="have_enabled" /></c:if>
											<c:if test="${storePos.enablestate=='3'}"><fmt:message key="stop_enabled" /></c:if>
										</span>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
			<div class="search-div" style="margin-left: 0px;">
				<div class="search-condition">
					<table class="search-table" cellspacing="0" cellpadding="0" >
						<tr>
							<td class="c-left"><fmt:message key="coding" />：</td>
							<td><input type="text" id="icode" name="icode" class="text" value="${storePos.icode }" /></td>
							<td style="width: 20px;"></td>
							<td class="c-left"><fmt:message key="name" />：</td>
							<td><input type="text" id="vmemo" name="vmemo" class="text" value="${storePos.vmemo }" /></td>
							<td style="width: 20px;"></td>
							<td class="c-left"><fmt:message key="enable_state" />：</td>
							<td>
								<select id="enablestate" name="enablestate" class="select" style="height: 22px;margin-top: 3px; border: 1px solid #999999;">
									<option value="" <c:if test="${storePos.enablestate==''}">selected="selected"</c:if> ><fmt:message key="all" /></option>
									<option value="1" <c:if test="${storePos.enablestate=='1'}">selected="selected"</c:if> ><fmt:message key="not_enabled" /></option>
									<option value="2" <c:if test="${storePos.enablestate=='2'}">selected="selected"</c:if> ><fmt:message key="have_enabled" /></option>
									<option value="3" <c:if test="${storePos.enablestate=='3'}">selected="selected"</c:if> ><fmt:message key="stop_enabled" /></option>
								</select>
							</td>
						</tr>
					</table>
				</div>
			<div class="search-commit">
	       		<input type="button" class="search-button" id="search" value='<fmt:message key="enter" />'/>
	       		<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
			</div>
		</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>	
		<script type="text/javascript">

		//双击弹出<fmt:message key="update" />
		$('.grid').find('.table-body').find('tr').live("dblclick", function () {
			$(":checkbox").attr("checked", false);
			$(this).find(':checkbox').attr("checked", true);
			updateStorePos();
			if($(this).hasClass("show-firm-row"))return;
			$('.grid').find('.table-body').find('tr').removeClass("show-firm-row");
			$(this).addClass("show-firm-row");
		 });
		
			$(document).ready(function(){
				
				/* 模糊<fmt:message key="select" />提交 */
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#listForm').submit();
				});
				
				/* 模糊<fmt:message key="select" />清空 */
				$("#resetSearch").bind('click', function() {
					$('#listForm input[type=text]').val('');
					$('#listForm option:nth-child(1)').attr('selected','selected');
				});
				
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								$('.search-div').slideToggle(100);
								$('#icode').focus();//点击查询按钮，聚焦查询框
							}
						},'-',{
							text: '<fmt:message key="insert" />',
							title: '<fmt:message key="new_role_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								addStorePos();	
							}
						},{
							text: '<fmt:message key="update" />',
							title: '<fmt:message key="modify_the_role_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								updateStorePos();	
							}
						},'-',{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}
					]
				});
				
				// 自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),60);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
			});
			
			//实现复选框单选
			function CheckBoxCheck(chk){
			    var obj = $('.grid').find('.table-body').find('input[name=idList]');
			    for(var i = 0; i < obj.length; i++){
			        if(obj[i].type == "checkbox"){
			            obj[i].checked = false;
			        }
			    }
			    chk.checked=true;
			}
			
			//新增门店pos
			function addStorePos(){
				$('body').window({
					id: 'window_saveStorePos',
					title: '<fmt:message key="store" />POS<fmt:message key="insert" />',
					content: '<iframe id="SaveForm" frameborder="0" src="<%=path%>/baseRecord/add.do?pk_store=${storePos.pk_store}"></iframe>',
					width: '350px',
					height: '300px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								title: '<fmt:message key="save_role_of_information"/>',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('SaveForm')&&window.document.getElementById("SaveForm").contentWindow.validate._submitValidate()){
										document.getElementById("SaveForm").contentWindow.document.forms["storePosSaveForm"].submit();
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			
			//修改门店pos
			function updateStorePos(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() ==1){
					var action='<%=path%>/baseRecord/update.do?pk_store='+$("#pk_store").val()+'&pk_pos='+checkboxList.filter(':checked').val();
					$('body').window({
						id: 'window_selectTargetStore',
						title: '<fmt:message key="store" />POS<fmt:message key="update" />',
						content: '<iframe id="UpdateForm" frameborder="0" src='+action+'></iframe>',
						width: '350px',
						height: '300px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="store" />pos<fmt:message key="update" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){ 
										if(getFrame('UpdateForm')&&window.document.getElementById("UpdateForm").contentWindow.validate._submitValidate()){
											window.document.getElementById("UpdateForm").contentWindow.storePosUpdateForm();
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList && checkboxList.filter(':checked').size()>1){
					alert('<fmt:message key="you_can_only_modify_a_data" />!');
				}else{
					alert('<fmt:message key="please_select_data" />!');
				}
			}
			
			function pageReload(){
				$('#listForm').submit();
			}
		</script>
	</body>
</html>