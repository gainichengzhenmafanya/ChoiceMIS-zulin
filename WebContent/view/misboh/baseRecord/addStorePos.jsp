<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="branches_and_positions_information" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<style type="text/css">
			.form {
				position: relative;
				width: 90%;
				padding-top: 30px;
			}
			.store{
				width:200px;
				background-image:url("<%=path%>/image/themes/icons/searchmul1.png");
				background-position:right;
				background-repeat:no-repeat;
				cursor: pointer;
			}
		</style>
	</head>
	<body>
		<div class="form">
			<form id="storePosSaveForm" method="post" action="<%=path %>/baseRecord/addStorePos.do">
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="coding" /></div>
					<div class="form-input">
						<input type="text" id="icode" name="icode" class="text" value="${icode}" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="name" /></div>
					<div class="form-input">
						<input type="text" class="text" id="vmemo" name="vmemo" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span>POSIP</div>
					<div class="form-input">
						<input type="text" id="posip" name="posip" class="text" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="enable_state" />:</div>
					<div class="form-input">
						<select id="enablestate" name="enablestate" class="select">
							<option value="1"><fmt:message key="not_enabled" /></option>
							<option value="2" selected="selected"><fmt:message key="have_enabled" /></option>
							<option value="3"><fmt:message key="stop_enabled" /></option>
						</select>
					</div>
				</div>
				<input type="hidden" id="pk_store" name="pk_store" value="${storePos.pk_store}"/>
			</form>
		</div>
		<input type="hidden" id="subflag" value="0" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>
		<script type="text/javascript">
		var validate;
		$(document).ready(function(){
			getDefaultVcode("<%=path%>","CBOH_STOREPOS_3CH","icode","icode","101");
			
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'icode',
						validateType:['canNull','intege2','maxLength'],
						param:['F','F',8],
						error:['<fmt:message key="coding" /><fmt:message key="cannot_be_empty" />!','<fmt:message key="coding" /><fmt:message key="only_start_with_nozero" />!','<fmt:message key="the_maximum_length" />8']
					},{
						type:'text',
						validateObj:'icode',
						validateType:['handler'],
						handler:function(){
								var result = true;
								var commonMethod = new CommonMethod();
								commonMethod.vcode = $("#icode").val();
								commonMethod.vcodename = "icode";
								commonMethod.vtablename = "CBOH_STOREPOS_3CH";
								commonMethod.pk_store = $("#pk_store").val();
								var returnvalue = validateVcode("<%=path %>",commonMethod);
								if (returnvalue == "1") {
									result = false;
								}else{
									result=true;
								}
							return result;
						},
						param:['F'],
						error:['<fmt:message key="coding" /><fmt:message key="already_exists" />,<fmt:message key="please_re_enter" />!']
					},{
						type:'text',
						validateObj:'posip',
						validateType:['handler'],
						handler:function(){
								var result = true;
								var commonMethod = new CommonMethod();
								commonMethod.vcode = $("#posip").val();
								commonMethod.vcodename = "posip";
								commonMethod.vtablename = "CBOH_STOREPOS_3CH";
								commonMethod.pk_store = $("#pk_store").val();
								var returnvalue = validateVcode("<%=path %>",commonMethod);
								if (returnvalue == "1") {
									result = false;
								}else{
									result=true;
								}
							return result;
						},
						param:['F'],
						error:['IP<fmt:message key="already_exists" />,<fmt:message key="please_re_enter" />!']
					},{
						type:'text',
						validateObj:'posip',
						validateType:['canNull','ip'],
						param:['F','F'],
						error:['IP<fmt:message key="cannot_be_empty" />!','IP<fmt:message key="incorrect_format" />!']
					},{
						type:'text',
						validateObj:'vmemo',
						validateType:['canNull','maxLength'],
						param:['F',150],
						error:['<fmt:message key="name" /><fmt:message key="cannot_be_empty" />!','<fmt:message key="the_maximum_length" />150']
					},{
						type:'text',
						validateObj:'vmemo',
						validateType:['handler'],
						handler:function(){
								var result = true;
								var commonMethod = new CommonMethod();
								commonMethod.vcode = $("#vmemo").val();
								commonMethod.vcodename = "vmemo";
								commonMethod.vtablename = "CBOH_STOREPOS_3CH";
								commonMethod.enablestate = "1";
								commonMethod.pk_store = $("#pk_store").val();
								var returnvalue = validateVcode("<%=path %>",commonMethod);
								if (returnvalue == "1") {
									result = false;
								}else{
									result=true;
								}
							return result;
						},
						param:['F'],
						error:['<fmt:message key="name" /><fmt:message key="already_exists" />,<fmt:message key="please_re_enter" />!']
					}]
				});
				
				tabArray=['icode','vmemo','posip','enablestate'];
				//定义加载后定位在第一个输入框上          
				$('#'+tabArray[0]).focus();            
				$('.text,.select').not($('input[type="hidden"],input:disabled')).keydown(function(e) { 
					//使用jquery的这种获取事件方法，不必再指定event.srcElement或者event.target  
					var event = $.event.fix(e);
					//判断如果按键事件的按键代码是回车，则从数组中获取下一个元素的ID,并设置焦点                       
					if (event.keyCode == 13) {
						if($('#subflag').val()=='0'){
						var index = $.inArray($.trim($(event.target).attr("id")), tabArray);//
						if(index == tabArray.length-1){
							if(validate._submitValidate()){
								$('#subflag').val('1');
								$('#storePosSaveForm').submit();//切换到最后一个控件后，执行修改
								return;
							}
						}
						$('#'+tabArray[index]).blur();
						index=index+1;
						if(index==tabArray.length) index=0;
						$('#'+tabArray[index]).focus();	
						}
					}
				});
			});
		</script>
	</body>
</html>