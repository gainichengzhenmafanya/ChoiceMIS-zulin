<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="java.util.Calendar"   %>

<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>门店活动详细显示页面</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<script src="<%=path%>/js/gaoliang.js" type="text/javascript"></script>
		<style type="text/css">
			table.gridtable {
				font-family: verdana,arial,sans-serif;
				font-size:11px;
				color:#333333;
				border-width: 1px;
				border-color: #666666;
				border-collapse: collapse;
			}
			table.gridtable th {
				border-width: 1px;
				padding: 8px;
				border-style: solid;
				border-color: #666666;
				background-color: #dedede;
			}
			table.gridtable td {
				border-width: 1px;
				padding: 8px;
				border-style: solid;
				border-color: #666666;
				background-color: #ffffff;
			}
			.panel-body {
				overflow:hidden;
				border-right:1px solid #004771;
				border-top-width:0px;
			}
		</style> 
	</head>
	<body>
		<div style="padding-left:70px;">
			<!-- 基本信息 -->
			<div class="form-line">
				<div class="form-label"><fmt:message key="coding" />:</div>
				<div class="form-input"><input type="text" id="vcode" name="vcode" disabled="disabled" class="text" value="${actm.vcode }"/></div>
				<div class="form-label"><fmt:message key="name" />:</div>
				<div class="form-input"><input type="text" id="vname" name="vname" class="text" value="${actm.vname }" disabled="disabled"/></div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="enable_state" />:</div>
				<div class="form-input">
					<select id="enablestate" name="enablestate" class="select" disabled="disabled">
						<option value="1"  <c:if test="${actm.enablestate==1}"> selected="selected" </c:if> ><fmt:message key="not_enabled" /></option>
						<option value="2"  <c:if test="${actm.enablestate==2}"> selected="selected" </c:if> ><fmt:message key="have_enabled" /></option>
						<option value="3"  <c:if test="${actm.enablestate==3}"> selected="selected" </c:if> ><fmt:message key="stop_enabled" /></option>
					</select>
				</div>
				<div class="form-label"><fmt:message key="third_party" /><fmt:message key="coding" />:</div>
				<div class="form-input">
					<input type="text" class="text" id="vthirdcode" name="vthirdcode" value="${actm.vthirdcode}" disabled="disabled"/>
				</div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="activity" /><fmt:message key="manage" /><fmt:message key="bigClass" />:</div>
				<div class="form-input">
					<input type="text" class="text" value="${actm.pk_acttyp }" disabled="disabled"/>
				</div>
				<div class="form-label"><fmt:message key="activity" /><fmt:message key="manage" /><fmt:message key="smallClass" />:</div>
				<div class="form-input">
					<input type="text" class="text" value="${actm.pk_acttypmin }" disabled="disabled"/>
				</div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="font_type" />:</div>
				<div class="form-input">
					<input type="text" class="text" value="${actm.vfont }" disabled="disabled"/>
				</div>
				<div class="form-label"><fmt:message key="the_font_size" />:</div>
				<div class="form-input">
					<input type="text" id="ifontsize" name="ifontsize" class="text" value="${actm.ifontsize }" disabled="disabled"/>
				</div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="the_font_color" />:</div>
				<div class="form-input">
					<input type="text" id="vfontcolor" name="vfontcolor" class="text color" value="${actm.vfontcolor }" disabled="disabled"/>
				</div>
				<div class="form-label"><fmt:message key="the_button_height" />:</div>
				<div class="form-input">
					<input type="text" id="ibackheight" name="ibackheight"  class="text" value="${actm.ibackheight }" disabled="disabled"/>
				</div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="button" /><fmt:message key="width" />:</div>
				<div class="form-input">
					<input type="text" id="ibackwidth" name="ibackwidth" disabled="disabled" class="text" value="${actm.ibackwidth }" />
				</div>			
				<div class="form-label"><fmt:message key="the_button_color" />:</div>
				<div class="form-input">
					<input type="text" id="vbackgroud" name="vbackgroud" class="text color" value="${actm.vbackgroud }" disabled="disabled"/>
				</div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="could_you_input_amount" />:</div>
				<div class="form-input">
					<select id="visinputmoney" name="visinputmoney" class="select" disabled="disabled">
						<option value="N"  <c:if test="${actm.visinputmoney=='N' || empty actm.visinputmoney}"> selected="selected" </c:if> ><fmt:message key="no1" /></option>
						<option value="Y"  <c:if test="${actm.visinputmoney=='Y'}"> selected="selected" </c:if> ><fmt:message key="be" /></option>
					</select>
				</div>
				<div class="form-label"><fmt:message key="whether_valid" />:</div>
				<div class="form-input">
					<select id="isvalidate" name="isvalidate" class="select" disabled="disabled">
						<option value="N"  <c:if test="${actm.isvalidate=='N'}"> selected="selected" </c:if> ><fmt:message key="no1" /></option>
						<option value="Y"  <c:if test="${actm.isvalidate=='Y'}"> selected="selected" </c:if> ><fmt:message key="be" /></option>
					</select>
				</div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="limited" />:</div>
				<div class="form-input">
					<select id="vproducttype" name="vproducttype" class="select" disabled="disabled">
						<option value="0" <c:if test="${actm.vproducttype=='0'}"> selected="selected" </c:if>><fmt:message key="close" /></option>
						<option value="1" <c:if test="${actm.vproducttype=='1'}"> selected="selected" </c:if> ><fmt:message key="open" /></option>
						<option value="2" <c:if test="${actm.vproducttype=='2'}"> selected="selected" </c:if> ><fmt:message key="execute_every_day" /></option>
					</select>
				</div>
				<div class="form-label"><fmt:message key="remark" />:</div>
				<div class="form-input">
					<textarea rows="3" cols="12" disabled="disabled" style="width: 128px;height: 15px;max-height: 45px;"id="vmemo" name="vmemo" class="textarea">${actm.vmemo }</textarea>
				</div>
			</div>
		</div>
		<div class="easyui-tabs" fit="false" plain="true" id="tabs" style="margin-top:20px;margin-left:20px;width:700px;">
			
			<!-- 日期 -->		
			<div title="<fmt:message key="date" /><fmt:message key="limit" />" >
				<div class="form-line">
					<div class="form-label"><span><fmt:message key="startdate" /><fmt:message key="limit" />1</span>:</div>
					<div class="form-input">
						<input type="text" class="text" value="${str.mdbegintime1 }" disabled="disabled" style="text-align: right;" />
					</div>
					<div class="form-label"><span><fmt:message key="enddate" /><fmt:message key="limit" />1</span>:</div>
					<div class="form-input">
						<input type="text" class="text" value="${str.mdendtime1 }" disabled="disabled" style="text-align: right;" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span><fmt:message key="startdate" /><fmt:message key="limit" />2</span>:</div>
					<div class="form-input">
						<input type="text" class="text" value="${str.mdbegintime2 }" disabled="disabled" style="text-align: right;" />
					</div>
					<div class="form-label"><span><fmt:message key="enddate" /><fmt:message key="limit" />2</span>:</div>
					<div class="form-input">
						<input type="text" class="text" value="${str.mdendtime2 }" disabled="disabled" style="text-align: right;" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span><fmt:message key="startdate" /><fmt:message key="limit" />3</span>:</div>
					<div class="form-input">
						<input type="text" class="text" value="${str.mdbegintime3 }" disabled="disabled" style="text-align: right;" />
					</div>
					<div class="form-label"><span><fmt:message key="enddate" /><fmt:message key="limit" />3</span>:</div>
					<div class="form-input">
						<input type="text" class="text" value="${str.mdendtime3 }" disabled="disabled" style="text-align: right;"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span><fmt:message key="startdate" /><fmt:message key="limit" />4</span>:</div>
					<div class="form-input">
						<input type="text" class="text" value="${str.mdbegintime4 }" disabled="disabled" style="text-align: right;" />
					</div>
					<div class="form-label"><span><fmt:message key="enddate" /><fmt:message key="limit" />4</span>:</div>
					<div class="form-input">
						<input type="text" class="text" value="${str.mdendtime4 }" disabled="disabled" style="text-align: right;" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span><fmt:message key="startdate" /><fmt:message key="limit" />5</span>:</div>
					<div class="form-input">
						<input type="text" class="text" value="${str.mdbegintime5 }" disabled="disabled" style="text-align: right;" />
					</div>
					<div class="form-label"><span><fmt:message key="enddate" /><fmt:message key="limit" />5</span>:</div>
					<div class="form-input">
						<input type="text" class="text" value="${str.mdendtime5 }" disabled="disabled" style="text-align: right;" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span><fmt:message key="startdate" /><fmt:message key="limit" />6</span>:</div>
					<div class="form-input">
						<input type="text" class="text" value="${str.mdbegintime6 }" disabled="disabled" style="text-align: right;" />
					</div>
					<div class="form-label"><span><fmt:message key="enddate" /><fmt:message key="limit" />6</span>:</div>
					<div class="form-input">
						<input type="text" class="text" value="${str.mdendtime6 }" disabled="disabled" style="text-align: right;" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span><fmt:message key="startdate" /><fmt:message key="limit" />7</span>:</div>
					<div class="form-input">
						<input type="text" class="text" value="${str.mdbegintime7 }" disabled="disabled" style="text-align: right;" />
					</div>
					<div class="form-label"><span><fmt:message key="enddate" /><fmt:message key="limit" />7</span>:</div>
					<div class="form-input">
						<input type="text" class="text" value="${str.mdendtime7 }" disabled="disabled" style="text-align: right;" />
					</div>
				</div>
			</div>
			
			<!-- 星期及时段 -->
			<div title='<fmt:message key="week_period_in_time" /><fmt:message key="limit" />'>
<%-- 				<c:if test="${actm.bistime == 'Y' }"> --%>
					<div class="grid">
						<div class="table-head">
							<table cellspacing="0" cellpadding="0">
								<thead>
									<tr>
										<td><span style="width:50px;"><fmt:message key="week" /></span></td>
										<td><span style="width:70px;"><fmt:message key="starttime" />1</span></td>
										<td><span style="width:70px;"><fmt:message key="endtime" />1</span></td>
										<td><span style="width:70px;"><fmt:message key="starttime" />2</span></td>
										<td><span style="width:70px;"><fmt:message key="endtime" />2</span></td>
										<td><span style="width:70px;"><fmt:message key="starttime" />3</span></td>
										<td><span style="width:70px;"><fmt:message key="endtime" />3</span></td>
										<td><span style="width:70px;"><fmt:message key="starttime" />4</span></td>
										<td><span style="width:70px;"><fmt:message key="endtime" />4</span></td>
									</tr>
								</thead>
							</table>
						</div>
						<div class="table-body" style="height:155px;">
							<table id="tblGrid" cellspacing="0" cellpadding="0">
								<tbody>
									<c:forEach var="time" items="${time}" varStatus="status">
										<tr><td><span style="width:50px;text-align: left">
													<c:if test="${time.weekly==1 }"><fmt:message key="week1" /></c:if>
													<c:if test="${time.weekly==2 }"><fmt:message key="week2" /></c:if>
													<c:if test="${time.weekly==3 }"><fmt:message key="week3" /></c:if>
													<c:if test="${time.weekly==4 }"><fmt:message key="week4" /></c:if>
													<c:if test="${time.weekly==5 }"><fmt:message key="week5" /></c:if>
													<c:if test="${time.weekly==6 }"><fmt:message key="week6" /></c:if>
													<c:if test="${time.weekly==7 }"><fmt:message key="week7" /></c:if>
												</span>
											</td>
											<td><span style="width:70px;text-align: right"><c:out value="${time.vbegintime1 }" /></span></td>
											<td><span style="width:70px;text-align: right"><c:out value="${time.vendtime1 }" /></span></td>
											<td><span style="width:70px;text-align: right"><c:out value="${time.vbegintime2 }" /></span></td>
											<td><span style="width:70px;text-align: right"><c:out value="${time.vendtime2 }" /></span></td>
											<td><span style="width:70px;text-align: right"><c:out value="${time.vbegintime3 }" /></span></td>
											<td><span style="width:70px;text-align: right"><c:out value="${time.vendtime3 }" /></span></td>
											<td><span style="width:70px;text-align: right"><c:out value="${time.vbegintime4 }" /></span></td>
											<td><span style="width:70px;text-align: right"><c:out value="${time.vendtime4 }" /></span></td>
										</tr>					
									</c:forEach>
								</tbody>
							</table>
						</div>	
					</div>
<%-- 				</c:if> --%>
			</div>
			
			<!-- 产品限制 -->
			<div title="<fmt:message key="product" /><fmt:message key="limit" />">
<%-- 				<c:if test="${actm.xztj == 'Y' }"> --%>
					<span style="color: red;"><fmt:message key="product_fraction_relationship" />：${pubitem[0].rdo }</span>
					<div class="grid">
						<div class="table-head" >
							<table cellspacing="0" cellpadding="0">
								<thead>
									<tr>
										<td style="width: 25px;">&nbsp;</td>
										<td style="width:80px;"><fmt:message key="scm_pubitem_code" /></td>
										<td style="width:80px;"><fmt:message key="scm_pubitem_name" /></td>
										<td style="width:80px;"><fmt:message key="unit" /></td>
										<td style="width:80px;"><fmt:message key="quantity" /></td>
									</tr>
								</thead>
							</table>
						</div>
						<div class="table-body" style="height:139px;">
							<table id="tblGrid" cellspacing="0" cellpadding="0">
								<tbody>
									<c:forEach var="pi" items="${pubitem}" varStatus="status">
										<tr>
											<td class="num" style="width: 25px;"><span>${status.index+1}</span></td>
											<td style="width:80px;"><span><c:out value="${pi.vcode}" /></span></td>
											<td style="width:80px;"><span><c:out value="${pi.vname}" /></span></td>
											<td style="width:80px;text-align: center;"><span><c:out value="${pi.unitname}" /></span></td>
											<td style="width:80px;text-align: right;"><span><c:out value="${pi.inum}" /></span></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>	
					</div>
<%-- 				</c:if> --%>
			</div>
			
			<!-- 限制条件（金额限制、人数限制、张数限制、班次限制） -->
			<div title="<fmt:message key="limit" /><fmt:message key="condition" />">
				<c:if test="${actm.bisamount == 'Y' }">
					<div class="form-line" style="height:175px;">
						<div class="form-label"><span><fmt:message key="caps_of_amt" /></span>：</div>
						<div class="form-input">
							<input type="text" class="text" value="<fmt:formatNumber value="${amount.iamount }" pattern="#,###.00" />" disabled="disabled" style="text-align: right;" />
						</div>
						<div class="form-label"><span><fmt:message key="limit_of_amt" /></span>：</div>
						<div class="form-input">
							<input type="text" class="text" value="<fmt:formatNumber value="${amount.idownamount }" pattern="#,###.00" />" disabled="disabled" style="text-align: right;" />
						</div>
					</div>
				</c:if>
				<c:if test="${actm.biscount == 'Y' }">
					<div class="form-line" style="height:175px;">
						<div class="form-label"><span><fmt:message key="number_of_people" /><fmt:message key="scm_ceiling" /></span>：</div>
						<div class="form-input">
							<input type="text" class="text" value="${member.itopnumber }" disabled="disabled" style="text-align: right;" />
						</div>
						<div class="form-label"><span><fmt:message key="number_of_people" /><fmt:message key="The_lower_limit" /></span>：</div>
						<div class="form-input">
							<input type="text" class="text" value="${member.idownnumber }" disabled="disabled" style="text-align: right;" />
						</div>
					</div>
				</c:if>
				<c:if test="${actm.bisticket == 'Y' }">
					<div class="form-line" style="height:175px;">
						<div class="form-label"><span><fmt:message key="a_number_of" /></span>：</div>
						<div class="form-input">
							<input type="text" class="text" value="${ticket.iticketnum }" disabled="disabled" style="text-align: right;" />
						</div>
					</div>
				</c:if>
				<c:if test="${actm.bisclasses == 'Y' }">
					<div class="form-line" style="height:175px;">
						<div class="form-label"><span><fmt:message key="limit" /><fmt:message key="scm_flight" /></span>：</div>
						<div class="form-input">
							<c:if test="${actm.bisoneclass == 'Y' }">
								<input type="text" class="text" value="<fmt:message key="shift1" />" disabled="disabled" style="text-align: left;" />
							</c:if>
							<c:if test="${actm.bistwoclass == 'Y' }">
								<input type="text" class="text" value="<fmt:message key="shift2" />" disabled="disabled" style="text-align: left;" />
							</c:if>
							<c:if test="${actm.bisthreeclass == 'Y' }">
								<input type="text" class="text" value="<fmt:message key="shift3" />" disabled="disabled" style="text-align: left;" />
							</c:if>
							<c:if test="${actm.bisfourclass == 'Y' }">
								<input type="text" class="text" value="<fmt:message key="shift4" />" disabled="disabled" style="text-align: left;" />
							</c:if>
						</div>
					</div>
				</c:if>
			</div>
			
			<!-- 执行活动 （账单减免、启用调价、消费返券、消费返积分）-->
			<div title='<fmt:message key="perform_activities" />'>
					<c:if test="${actm.bremit == 'Y' }">
						<div class="grid">
							<div class="table-head" >
								<table cellspacing="0" cellpadding="0">
									<thead>
										<tr>
											<td style="width: 25px;">&nbsp;</td>
											<td style="width:80px;"><fmt:message key="coding" /></td>
											<td style="width:106px;"><fmt:message key="name" /></td>
											<td style="width:80px;"><fmt:message key="unit" /></td>
											<td style="width:80px;"><fmt:message key="Reduction" /><fmt:message key="type" /></td>
											<td style="width:80px;"><fmt:message key="Reduction" /><fmt:message key="amount" /><fmt:message key="type" /></td>
											<td style="width:80px;"><fmt:message key="Reduction" /><fmt:message key="amount" /></td>
											<td style="width:80px;"><fmt:message key="Reduction" /><fmt:message key="quantity" /></td>
										</tr>
									</thead>
								</table>
							</div>
							<div class="table-body" style="height:155px;">
								<table id="tblGrid" cellspacing="0" cellpadding="0">
									<tbody>
										<c:forEach var="jm" items="${jianMian}" varStatus="status">
											<tr>
												<td class="num" style="width: 25px;"><span>${status.index+1}</span></td>
												<td style="width:80px;text-align: left;"><span><c:out value="${jm.vcode}" /></span></td>
												<td style="width:106px;"><span><c:out value="${jm.vname}" /></span></td>
												<td style="width:80px;"><span><c:out value="${jm.unitname}" /></span></td>
												<td style="width:80px;"><span><c:out value="${jm.jmttyp}" /></span></td>
												<td style="width:80px;"><span><c:out value="${jm.jmrdo}" /></span></td>
												<td style="width:80px;text-align: right;"><span><fmt:formatNumber value="${jm.nderatenum}" pattern="#,##0.00" /></span></td>
												<td style="width:80px;text-align: right;"><span><c:out value="${jm.inum}" /></span></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>	
						</div>
					</c:if>
					<c:if test="${actm.badjust == 'Y' }">
						<div class="grid">
							<div class="table-head" >
								<table cellspacing="0" cellpadding="0">
									<thead>
										<tr>
											<td style="width:25px;">&nbsp;</td>
											<td style="width:80px;"><fmt:message key="coding" /></td>
											<td style="width:80px;"><fmt:message key="name" /></td>
											<td style="width:80px;"><fmt:message key="unit" /></td>
											<td style="width:80px;"><fmt:message key="pricing" /><fmt:message key="way" /></td>
											<td style="width:80px;"><fmt:message key="enable" /><fmt:message key="pricing" /><fmt:message key="type" /></td>
											<td style="width:80px;"><fmt:message key="pricing" /><fmt:message key="amount" /></td>
											<td style="width:80px;"><fmt:message key="pricing" /><fmt:message key="quantity" /></td>
										</tr>
									</thead>
								</table>
							</div>
							<div class="table-body" style="height:155px;">
								<table id="tblGrid" cellspacing="0" cellpadding="0">
									<tbody>
										<c:forEach var="dj" items="${diaoJia}" varStatus="status">
											<tr>
												<td class="num" style="width: 25px;">${status.index+1}</td>
												<td style="width:80px;text-align: left;"><span><c:out value="${dj.vcode}" /></span></td>
												<td style="width:80px;"><span><c:out value="${dj.vname}" /></span></td>
												<td style="width:80px;"><span><c:out value="${dj.unitname}" /></span></td>
												<td style="width:80px;"><span><c:out value="${dj.jmrdo}" /></span></td>
												<td style="width:80px;"><span><c:out value="${dj.jmttyp}" /></span></td>
												<td style="width:80px;text-align: right;"><span><fmt:formatNumber value="${dj.nderatenum}" pattern="#,##0.00" /></span></td>
												<td style="width:80px;text-align: right;"><span><c:out value="${dj.inum}" /></span></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>	
						</div>
					</c:if>
					<c:if test="${actm.voucherback == 'Y' }">
						<div class="grid">
							<div class="table-head" >
								<table cellspacing="0" cellpadding="0">
									<thead>
										<tr>
											<td style="width:25px;">&nbsp;</td>
											<td style="width:80px;"><fmt:message key="coding" /></td>
											<td style="width:80px;"><fmt:message key="name" /></td>
											<td style="width:80px;"><fmt:message key="quantity" /></td>
											<td style="width:80px;"><fmt:message key="voucher" /><fmt:message key="type" /></td>
											<td style="width:180px;"><fmt:message key="remark" /></td>
										</tr>
									</thead>
								</table>
							</div>
							<div class="table-body" style="height:155px;">
								<table id="tblGrid" cellspacing="0" cellpadding="0">
									<tbody>
										<c:forEach var="fq" items="${fanQuan}" varStatus="status">
											<tr>
												<td class="num" style="width: 25px;"><span>${status.index+1}</span></td>
												<td style="width:80px;text-align: left;"><span><c:out value="${fq.vvouchercode}" /></span></td>
												<td style="width:80px;"><span><c:out value="${fq.vname}" /></span></td>
												<td style="width:80px;text-align: right;"><span><fmt:formatNumber value="${fq.ivouchernum}" pattern="#,##0.00" /></span></td>
												<td style="width:80px;"><span><c:out value="${fq.typname}" /></span></td>
												<td style="width:180px;"><span><c:out value="${fq.vmemo}" /></span></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>	
						</div>
					</c:if>
					<c:if test="${actm.vfenback == 'Y' }">
						<div class="grid">
							<div class="table-head" >
								<table cellspacing="0" cellpadding="0">
									<thead>
										<tr>
											<td style="width: 25px;">&nbsp;</td>
											<td style="width:80px;"><fmt:message key="limit_of_amt" /></td>
											<td style="width:80px;"><fmt:message key="caps_of_amt" /></td>
											<td style="width:80px;"><fmt:message key="type" /></td>
											<td style="width:180px;"><fmt:message key="The_proportion" /><fmt:message key="or" /><fmt:message key="integral" /><fmt:message key="quantity" /></td>
										</tr>
									</thead>
								</table>
							</div>
							<div class="table-body" style="height:155px;">
								<table id="tblGrid" cellspacing="0" cellpadding="0">
									<tbody>
										<c:forEach var="jf" items="${jiFen}" varStatus="status">
											<tr>
												<td class="num" style="width: 25px;"><span>${status.index+1}</span></td>
												<td style="width:80px;text-align: left;"><span><c:out value="${jf.ifloormoney}" /></span></td>
												<td style="width:80px;text-align: right;"><span><c:out value="${jf.iceilmoney}" /></span></td>
												<td style="width:80px;"><span><c:out value="${jf.vfentyp}" /></span></td>
												<td style="width:180px;"><span><c:out value="${jf.ifennum}" /></span></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>	
						</div>
					</c:if>
				</div>
				
				<!-- 互斥活动 -->
				<div title="<fmt:message key="incompatible_activities" />">
<%-- 					<c:if test="${actm.bishchd == 'Y' }"> --%>
						<div class="grid">
							<div class="table-head" >
								<table cellspacing="0" cellpadding="0">
									<thead>
										<tr>
											<td style="width: 25px;">&nbsp;</td>
											<td style="width:100px;"><fmt:message key="coding" /></td>
											<td style="width:200px;"><fmt:message key="name" /></td>
										</tr>
									</thead>
								</table>
							</div>
							<div class="table-body" style="height:155px;">
								<table id="tblGrid" cellspacing="0" cellpadding="0">
									<tbody>
										<c:forEach var="lck" items="${lck}" varStatus="status">
											<tr>
												<td class="num" style="width: 25px;">${status.index+1}</td>
												<td style="width:100px;text-align: left;"><span style="width: 90px;"><c:out value="${lck.vcode}" /></span></td>
												<td style="width:200px;"><span style="width: 190px;"><c:out value="${lck.vname}" /></span></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>	
						</div>
<%-- 					</c:if> --%>
				</div>
			</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>		
	 	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/colorpicker/jscolor.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/FontFacesMenu.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/StringBuilder.js"></script>
	</body>
</html>