<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="package" /><fmt:message key="Replaceable_food" /><fmt:message key="the_detail" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<style type="text/css">
			.separator{
				display:none !important;
			}
			.page{
				margin-bottom: 0px;
			}
			.separator ,div , .pgSearchInfo{
				display: none;
			}
			div[class]{
				display:block;
			}
			.tr-select{
				background-color: #D2E9FF;
			}
		</style>
	</head>
	<body>
		<div class="tool">
		
		</div>
		<form id="listForm" action="<%=path%>/pubpackage/listItemPkg.do" method="post">
		<div class="grid" style="overflow: auto;">
			<div class="table-head" >
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr><td class="num"><span style="width: 20px;"></span></td>
							<td><span style="width:100px;"><fmt:message key="pubitem_code" /></span></td>
							<td><span style="width:100px;"><fmt:message key="pubitem_name" /></span></td>
							<td><span style="width:100px;"><fmt:message key="unit" /><fmt:message key="select1" /></span></td>
							<td><span style="width:60px;"><fmt:message key="unit" /></span></td>
							<td><span style="width:60px;"><fmt:message key="unit_price" /></span></td>
							<td><span style="width:60px;"><fmt:message key="package" /><fmt:message key="price" /></span></td>
							<td><span style="width:60px;"><fmt:message key="buy_price" /></span></td>
							<td><span style="width:60px;"><fmt:message key="the_discount_value" /></span></td>
							<td><span style="width:60px;"><fmt:message key="Minimum_quantity" /></span></td>
							<td><span style="width:60px;"><fmt:message key="The_largest_number" /></span></td>
							<td><span style="width:60px;"><fmt:message key="quantity" /></span></td>
							<td><span style="width:60px;"><fmt:message key="order_no" /></span></td>
						</tr>
					</thead>
				</table>
			</div>
			<div class="table-body" id="itemPkgTable">
				<table cellspacing="0" cellpadding="0" class="datagrid">
					<tbody>
						<c:forEach var="itempkg" items="${listItemPkg}" varStatus="status">
							<tr><td class="num"><span style="width: 20px;">${status.index+1}</span></td>
								<td><span style="width:100px;">${itempkg.PUBITEMCODE}</span></td>
								<td><span style="width:100px;">${itempkg.VNAME}</span></td>
								<td><span style="width:100px;">
									<c:choose>
										<c:when test="${itempkg.UNITINDEX=='2'}"><fmt:message key="The_second" /><fmt:message key="unit" /><input type="hidden" value='2' /></c:when>
										<c:when test="${itempkg.UNITINDEX=='3'}"><fmt:message key="the_third" /><fmt:message key="unit" /><input type="hidden" value='3' /></c:when>
										<c:when test="${itempkg.UNITINDEX=='4'}"><fmt:message key="the_fourth" /><fmt:message key="unit" /><input type="hidden" value='4' /></c:when>
										<c:when test="${itempkg.UNITINDEX=='5'}"><fmt:message key="the_fifth" /><fmt:message key="unit" /><input type="hidden" value='5' /></c:when>
										<c:when test="${itempkg.UNITINDEX=='6'}"><fmt:message key="the_sixth" /><fmt:message key="unit" /><input type="hidden" value='6' /></c:when>
										<c:otherwise><fmt:message key="the_first" /><fmt:message key="unit" /><input type="hidden" value='1' /></c:otherwise>
									</c:choose>
								</span></td>
								<td><span style="width:60px;">${itempkg.VUNIT}</span></td>
								<td align="right"><span style="width:60px;"><fmt:formatNumber value="${itempkg.NPRICE}" pattern="##0.00" /></span></td>
								<td align="right"><span style="width:60px;"><fmt:formatNumber value="${itempkg.NPACKAGEPRICE}" pattern="##0.00" /></span></td>
								<td align="right"><span style="width:60px;"><fmt:formatNumber value="${itempkg.NADJUSTPRICE}" pattern="##0.00" /></span></td>
								<td align="right"><span style="width:60px;"><fmt:formatNumber value="${itempkg.NDISCOUNTPRICE}" pattern="##0.00" /></span></td>
								<td align="right"><span style="width:60px;"><fmt:formatNumber value="${itempkg.IMINCOUNT}" pattern="##0.00" /></span></td>
								<td align="right"><span style="width:60px;"><fmt:formatNumber value="${itempkg.IMAXCOUNT}" pattern="##0.00" /></span></td>
								<td align="right"><span style="width:60px;"><fmt:formatNumber value="${itempkg.NCNT}" pattern="##0.00" /></span></td>
								<td align="right"><span style="width:60px;">${itempkg.ISORTNO}</span></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>			
		</div>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/mis/autoTable.js"></script>	
	 	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-60px']
							},
							handler: function(){
								parent.$('.close').click();									
							}
						}
					]
				});
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),5);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				initpk_id();
				
			});
		</script>
	</body>
</html>