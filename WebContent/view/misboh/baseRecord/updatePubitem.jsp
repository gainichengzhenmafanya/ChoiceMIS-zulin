<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><fmt:message key="update" /><fmt:message key="pubitem" /><fmt:message key="set_up_the" /></title>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	<style type="text/css">
		/* input.maxCount,input.minCount{
			width:100%;height:100%;border:none;text-align:center;background:#fff;
		} */
		.file-box{ position:relative;width:340px}
		.txt{ height:20px; border:1px solid #cdcdcd; width:130px;}
		.btn{ background-color:#FFF; border:1px solid #CDCDCD;height:24px; width:50px;}
		.file{ position:absolute; top:0; right:-100px;margin-top: 2px;; height:24px; filter:alpha(opacity:0);opacity: 0;width:250px }
	</style>
</head>
<body>
			<div class="easyui-tabs" fit="false" plain="true" id="tabs">
				<div title="<fmt:message key="basic_information" />">
					<input type="hidden" id="flag" value="jbxx" />
					<div class="form-line">
						<div class="form-label">
							<fmt:message key="coding" />：
						</div>
						<div class="form-input">
							<c:out value="${pubitem.vcode}" />
						</div>
						<div class="form-label">
							<fmt:message key="name" />：
						</div>
						<div class="form-input">
							<c:out value="${pubitem.vname}" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label">
							<fmt:message key="abbreviation" />：
						</div>
						<div class="form-input">
							<c:out value="${pubitem.vinit}" />
						</div>
						<div class="form-label"><fmt:message key="The_alias" />：</div>
						<div class="form-input">
							<c:out value="${pubitem.vdese}" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label">
							<fmt:message key="order_no" />：
						</div>
						<div class="form-input">
							<c:out value="${pubitem.isortno}" />
						</div>
					<div class="form-label"><fmt:message key="enable_state" />：</div>
						<div class="form-input">
							<c:if test="${pubitem.enablestate==1}"><fmt:message key="not_enabled" /></c:if> 
							<c:if test="${pubitem.enablestate==2}"><fmt:message key="have_enabled" /></c:if> 
							<c:if test="${pubitem.enablestate==3}"><fmt:message key="stop_enabled" /></c:if> 
						</div> 
						<div class="form-label">
							<fmt:message key="a_class" />：
						</div>
						<div class="form-input">
								<c:out value="${pubitem.vgrptyp}" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label">
							<fmt:message key="two_classes" />：
						</div>
						<div class="form-input">
							<c:out value="${pubitem.vgrp}" />
						</div>
						<div class="form-label">
							<fmt:message key="c" /><fmt:message key="category" />：
						</div>
						<div class="form-input">
							<c:out value="${pubitem.vtyp}" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label">
							<fmt:message key="the_first" /><fmt:message key="unit" />：
						</div>
						<div class="form-input">
							<c:out value="${pubitem.vunit}" />
						</div>
						<div class="form-label"><fmt:message key="statistical" /><fmt:message key="category" />：</div>
						<div class="form-input">
							<c:out value="${pubitem.vsptyp}" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="The_second" /><fmt:message key="unit" />：</div>
						<div class="form-input">
							<c:out value="${pubitem.units}" />
						</div>
						
						<div class="form-label">PAD<fmt:message key="category" />：</div>
						<div class="form-input">
							<c:out value="${pubitem.vclass}" />
						</div>
					</div>
					<div class="form-line">
					<div class="form-label">
							<fmt:message key="the_third" /><fmt:message key="unit" />：
						</div>
						<div class="form-input">
							<c:out value="${pubitem.vunit3}" />
						</div>
						<div class="form-label"><fmt:message key="activity" /><fmt:message key="category" />：</div>
						<div class="form-input">
							<c:out value="${pubitem.dishdisctyp}" />
						</div>
					</div>
					
					<div class="form-line">
						<div class="form-label">
							<fmt:message key="the_fourth" /><fmt:message key="unit" />：
						</div>
						<div class="form-input">
							<c:out value="${pubitem.vunit4}" />
						</div>
						<div class="form-label"><fmt:message key="auxiliary" /><fmt:message key="category" />：</div>
						<div class="form-input">
							<c:out value="${pubitem.vtypoth}" />
						</div>
					</div>
					<div class="form-line">
					<div class="form-label">
							<fmt:message key="the_fifth" /><fmt:message key="unit" />：
						</div>
						<div class="form-input">
							<c:out value="${pubitem.vunit5}" />
						</div>
						<div class="form-label"><fmt:message key="sales" /><fmt:message key="material" /><fmt:message key="coding" />：</div>
						<div class="form-input">
							<c:out value="${pubitem.salesSpCode}" />
							<c:if test="${'' != pubitem.salesSpCode} }">(<fmt:message key="docking" />ERP)</c:if>
						</div>
						</div>
						<div class="form-line">
						<div class="form-label">
							<fmt:message key="the_sixth" /><fmt:message key="unit" />：
						</div>
						<div class="form-input">
							<c:out value="${pubitem.vunit6}" />
						</div>
						<div class="form-label"><fmt:message key="additional" /><fmt:message key="product" />：</div>
						<div class="form-input">
							<c:out value="${pubitem.visaddprod}" />
						</div>
					</div>
					<div class="form-line" style="margin-bottom: 20px;">
						<div class="form-label">
							<fmt:message key="whether_the_present" />：
						</div>
						<div class="form-input">
							<c:out value="${pubitem.vcomp}" />
						</div>
					</div>
				</div>
				<div title='<fmt:message key="price" /><fmt:message key="information" />'>
					<input type="hidden" id="flag" value="jgxx" />
					<div class="form-line">
						<div class="form-label">
							<fmt:message key="price" />(<fmt:message key="Hall_food_prices" />)：
						</div>
						<div class="form-input">
							<fmt:formatNumber value="${pubitem.nrestprice}" pattern="#,###.00" />
						</div>
						<div class="form-label"><fmt:message key="price" />2(<fmt:message key="Plus_the_price_of" />)：</div>
						<div class="form-input">
							<fmt:formatNumber value="${pubitem.noutprice}" pattern="#,###.00" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="price" />3(<fmt:message key="Other_price" />)：</div>
						<div class="form-input">
							<fmt:formatNumber value="${pubitem.notherprice}" pattern="#,###.00" />
						</div>
						<div class="form-label"><fmt:message key="price" />4(<fmt:message key="member_price" />)：</div>
						<div class="form-input">
							<fmt:formatNumber value="${pubitem.nsecrestprice}" pattern="#,###.00" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="price" />5(<fmt:message key="unit" />2<fmt:message key="price" />)：</div>
						<div class="form-input">
							<fmt:formatNumber value="${pubitem.nsecoutprice}" pattern="#,###.00" />
						</div>
						<div class="form-label"><fmt:message key="price" />6(<fmt:message key="unit" />3<fmt:message key="price" />)：</div>
						<div class="form-input">
							<fmt:formatNumber value="${pubitem.nsecotherprice}" pattern="#,###.00" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="price" />7(<fmt:message key="unit" />4<fmt:message key="price" />)：</div>
						<div class="form-input">
							<fmt:formatNumber value="${pubitem.price7}" pattern="#,###.00" />
						</div>
						<div class="form-label"><fmt:message key="price" />8(<fmt:message key="unit" />5<fmt:message key="price" />)：</div>
						<div class="form-input">
							<fmt:formatNumber value="${pubitem.price8}" pattern="#,###.00" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="price" />9(<fmt:message key="unit" />6<fmt:message key="price" />)：</div>
						<div class="form-input">
							<fmt:formatNumber value="${pubitem.price9}" pattern="#,###.00" />
						</div>
						<div class="form-label"><fmt:message key="price" />10：</div>
						<div class="form-input">
							<fmt:formatNumber value="${pubitem.price10}" pattern="#,###.00" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="price" />11：</div>
						<div class="form-input">
							<fmt:formatNumber value="${pubitem.price11}" pattern="#,###.00" />
						</div>
					</div>
				</div>
				<div title="<fmt:message key="the_recipe" /><fmt:message key="set_up_the" />">
					<div class="form-line">
						<div class="form-input">
							<input type="checkbox" disabled="disabled" readonly="readonly" id="vquick" name="vquick" class="text" disabled="disabled"
								<c:if test="${pubitem.vquick=='Y'}">checked="checked"</c:if>/><fmt:message key="The_core_food" />
						</div>
						<div class="form-input">
							<input type="checkbox" disabled="disabled" readonly="readonly" id="vspecialprice" name="vspecialprice" disabled="disabled"
								class="text"
								<c:if test="${pubitem.vspecialprice=='Y'}">checked="checked"</c:if>/><fmt:message key="Star_food" />
						</div>
						<div class="form-input">
							<input type="checkbox" disabled="disabled" readonly="readonly" id="vhighprice" name="vhighprice"
								class="text"
								<c:if test="${pubitem.vhighprice=='Y'}">checked="checked"</c:if>
								/><fmt:message key="Optional_dish" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-input">
							<input type="checkbox" disabled="disabled" readonly="readonly" id="vyheigh" name="vyheigh" class="text"
								<c:if test="${pubitem.vyheigh=='Y'}">checked="checked"</c:if>
								/><fmt:message key="Statistical_food" />
						</div>
						<div class="form-input">
							<input type="checkbox" disabled="disabled" readonly="readonly" id="vyttl" name="vyttl" class="text"
								<c:if test="${pubitem.vyttl=='Y'}">checked="checked"</c:if>
								 /><fmt:message key="High-grade_food" />
						</div>
						<div class="form-input">
							<input type="checkbox" disabled="disabled" readonly="readonly" id="vplusd" name="vplusd" class="text"
								<c:if test="${pubitem.vplusd=='Y'}">checked="checked"</c:if>
								/><fmt:message key="Only_a_few" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-input">
							<input type="checkbox" disabled="disabled" readonly="readonly" id="vyrapid" name="vyrapid" class="text"
								<c:if test="${pubitem.vyrapid=='Y'}">checked="checked"</c:if>
								/><fmt:message key="Top_fish" />
						</div>
						<div class="form-input">
							<input type="checkbox" disabled="disabled" readonly="readonly" id="vpprntyp" name="vpprntyp" class="text"
								<c:if test="${pubitem.vpprntyp=='Y'}">checked="checked"</c:if>
								/><fmt:message key="Total_single_type" />
						</div>
						<div class="form-input">
							<input type="checkbox" disabled="disabled" readonly="readonly" readonly="readonly" id="vpriormth" name="vpriormth"
								class="text"
								<c:if test="${pubitem.vpriormth=='Y'}">checked="checked"</c:if>
								/><fmt:message key="allow" /><fmt:message key="update" /><fmt:message key="price" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-input">
							<input type="checkbox" disabled="disabled" readonly="readonly" id="vnodisc" name="vnodisc" class="text"
								<c:if test="${pubitem.vnodisc=='Y'}">checked="checked"</c:if>
								/><fmt:message key="allow" /><fmt:message key="At_a_discount" />
						</div>
						<div class="form-input">
							<input type="checkbox" disabled="disabled" readonly="readonly" id="vyprinttimely" name="vyprinttimely"
								class="text"
								<c:if test="${pubitem.vyprinttimely=='Y'}">checked="checked"</c:if>
								 /><fmt:message key="Print_the_post_it_note" />
						</div>
						<div class="form-input">
							<input type="checkbox" disabled="disabled" readonly="readonly" id="vhsta" name="vhsta" class="text"
								<c:if test="${pubitem.vhsta=='Y'}">checked="checked"</c:if>
								/><fmt:message key="According_to_the_order_a" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-input">
							<input type="checkbox" disabled="disabled" readonly="readonly" id="vhsta2" name="vhsta2" class="text"
								<c:if test="${pubitem.vhsta2=='Y'}">checked="checked"</c:if>
								 /><fmt:message key="According_to_the_order_a" />2
						</div>
						<div class="form-input">
							<input type="checkbox" disabled="disabled" readonly="readonly" id="vhsta3" name="vhsta3" class="text"
								<c:if test="${pubitem.vhsta3=='Y'}">checked="checked"</c:if>
								/><fmt:message key="According_to_the_order_a" />3
						</div>
						<div class="form-input">
							<input type="checkbox" disabled="disabled" readonly="readonly" id="vistemp" name="vistemp" class="text" <c:if test="${pubitem.vistemp=='Y'}">checked="checked"</c:if> />
								<fmt:message key="The_temporary_food" />
						</div>
					</div>
				</div>
				<div title='<fmt:message key="other_information" />'>
					<input type="hidden" id="flag" value="qtxx" />
					<div id="selectImgDIV" style="position: absolute; width:315px;height: 268px; z-index: 999;display: none;">
	                    <iframe id="selectImg" width="100%" height="85%" src="">
					   	</iframe>
	                </div>
					
					<div class="form-line">
						<div class="form-label"><fmt:message key="The_current_sales_terms" /><fmt:message key="unit" />：</div>
						<div class="form-input">
							<c:if test="${pubitem.vunitprice==1}"><fmt:message key="the_first" /><fmt:message key="unit" /></c:if>
							<c:if test="${pubitem.vunitprice==2}"><fmt:message key="The_second" /><fmt:message key="unit" /></c:if>
						</div>
						<div class="form-label"><fmt:message key="pubitem" /><fmt:message key="unit" /><fmt:message key="The_weight_of_the" />：</div>
						<div class="form-input">
							<c:out value="${pubitem.nunitprice}" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="Safety_stock" />：</div>
						<div class="form-input">
							<c:out value="${pubitem.nsafestock}" />
						</div>
						<div class="form-label"><fmt:message key="Package_price_type" />：</div>
						<div class="form-input">
							<c:if test="${pubitem.vpricetyp==1}"><fmt:message key="fixed" /><fmt:message key="price" /></c:if>
							<c:if test="${pubitem.vpricetyp==2}"><fmt:message key="collect" /><fmt:message key="price" /></c:if>
							<c:if test="${pubitem.vpricetyp==3}"><fmt:message key="high_priority" /><fmt:message key="price" /></c:if>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="The_default" /><fmt:message key="the_picture" />：</div>
						<div class="form-input">
							<c:out value="${pubitem.vpap}" />
						</div>
						<div class="form-label"  style="width:50px;margin-left:60px;"><fmt:message key="insets" />：</div>
						<div class="form-input">
							<c:out value="${pubitem.vpicsmall}" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="A_larger_version" />：</div>
						<div class="form-input">
							<c:out value="${pubitem.vpicbig}" />
						</div>
						<div class="form-label"><fmt:message key="Sales_order" />：</div>
						<div class="form-input">
							<c:out value="${pubitem.isalfoodorderid}" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="Auxiliarynumber" />：</div>
						<div class="form-input">
							<c:out value="${pubitem.naidnum}" />
						</div>
						<div class="form-label"><fmt:message key="Print_water_single_food_order" />：</div>
						<div class="form-input">
							<c:out value="${pubitem.ipntbillorderid}" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="After_cooking_dishes_to_print_order" />：</div>
						<div class="form-input">
							<c:out value="${pubitem.ikchnptderid}" />
						</div>
						<div class="form-label"><fmt:message key="the_font_color" />：</div>
						<div class="form-input">
							<input type="text" id="vfontcolor" name="vfontcolor" disabled="disabled"
								class="color {pickerClosable:true} text"
								value="<c:out value='${pubitem.vfontcolor}' /> "/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="the_foreground_color" />：</div>
						<div class="form-input">
							<input type="text" id="vbgcolor" name="vbgcolor" disabled="disabled"
								class="color {pickerClosable:true} text"
								value="<c:out value='${pubitem.vbgcolor}' /> " />
						</div>
						<div class="form-label"><fmt:message key="the_background_color" />：</div>
						<div class="form-input">
							<input type="text" id="vrealbgcolor" name="vrealbgcolor" disabled="disabled"
								class="color {pickerClosable:true} text"
								value="<c:out value='${pubitem.vrealbgcolor}' />" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="background_size_(wide)" />：</div>
						<div class="form-input">
							<c:out value="${pubitem.ibgsizew}" />
						</div>
						<div class="form-label"><fmt:message key="background_size_(high)" />：</div>
						<div class="form-input">
							<c:out value="${pubitem.ibgsizeh}" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="font_type" />：</div>
						<div class="form-input">
							<c:out value="${pubitem.vfonttype}" />
						</div>
						<div class="form-label"><fmt:message key="Additional_item_number" />：</div>
						<div class="form-input">
							<c:out value="${pubitem.iadditemnum}" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="the_font_size" />：</div>
						<div class="form-input">
							<c:out value="${pubitem.ifontsize}" />
						</div>
						<div class="form-label"><fmt:message key="The_mutex_relationship" />：</div>
						<div class="form-input">
							<c:out value="${pubitem.vmsobject}" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="print" /><fmt:message key="classification" />：</div>
						<div class="form-input">
							<c:out value="${pubitem.dept}" />
						</div>
						<div class="form-label"><fmt:message key="The_theory_of_cost" />：</div>
						<div class="form-input">
							<c:out value="${pubitem.ncost}" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="remark" />：</div>
						<div class="form-input">
							<c:out value="${pubitem.vrememo}" />
						</div>
					</div>
				</div>
			</div>
	<input type="hidden" id="subflag" value="0" />
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>	
	<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/boh/common/teleFunc-${sessionScope.locale}.js"></script>		
	<script type="text/javascript" src="<%=path%>/js/boh/autoTable.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
	<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	<script type="text/javascript" src="<%=path%>/js/custom.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mis/colorpicker/jscolor.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mis/screen/buttonObject.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
</body>
</html>