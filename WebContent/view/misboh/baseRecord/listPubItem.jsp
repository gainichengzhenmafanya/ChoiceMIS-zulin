<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="Dishes_set" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<style type="text/css">
			.separator{
				display:none !important;
			}
			.separator ,div , .pgSearchInfo{
				display: none;
			}
			div[class]{
				display:block;
			}
			.tr-select{
				background-color: #D2E9FF;
			}
		</style>
	</head>
	<body>
		<div class="tool">
		
		</div>
		<form id="listForm" action="<%=path%>/baseRecord/listPubitems.do" method="post">
				<div class="grid" style="overflow:auto">
					<div class="table-head" style="overflow:visible;">
						<table cellspacing="0" cellpadding="0" id="headTable">
							<thead>
								<tr>
									<td class="num"><span style="width: 20px;"></span></td>
									<td style="width:30px;">
<!-- 										<input type="checkbox" id="chkAll"/> -->
									</td>
									<td><span style="width:70px;"><fmt:message key="coding" /></span></td>
									<td><span style="width:100px;"><fmt:message key="name" /></span></td>
									<td><span style="width:40px;"><fmt:message key="unit" /></span></td>
									<td><span style="width:50px;"><fmt:message key="price" />1</span></td>
									<td><span style="width:75px;"><fmt:message key="abbreviation" /></span></td>
									<td><span style="width:70px;"><fmt:message key="a_class" /></span></td>
									<td><span style="width:70px;"><fmt:message key="two_classes" /></span></td>
									<td><span style="width:70px;"><fmt:message key="level_3_class" /></span></td>
									<td><span style="width:50px;"><fmt:message key="enable_state" /></span></td>
									<td><span style="width:70px;">PAD<fmt:message key="category" /></span></td>
									<td><span style="width:70px;"><fmt:message key="activity" /><fmt:message key="category" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body" style="overflow:visible;">
						<table cellspacing="0" cellpadding="0" class="datagrid">
							<tbody>
								<c:forEach var="pubitemObj" items="${listPubitem}" varStatus="status">
									<tr>
										<td class="num"><span style="width: 20px;">${status.index+1}</span></td>
										<td>
											<span style="width:20px; text-align: center;">
											<input type="checkbox" name="idList" onclick="CheckBoxCheck(this)"
											id="chk_<c:out value='${pubitemObj.pk_pubitem }' />" value="${pubitemObj.pk_pubitem}" 
											 <c:if test="${pubitemObj.pk_pubitem eq pubitem.pk_pubitem}"> checked="checked" </c:if> />
											</span>
										</td>
										<td><span style="width:70px;" title="${pubitemObj.vcode}">${pubitemObj.vcode}</span></td>
										<td><span style="width:100px;" title="${pubitemObj.vname}">${pubitemObj.vname}</span></td>
										<td><span style="width:40px;">${pubitemObj.vunit}</span></td>
										<td><span style="width:50px;text-align: right;"><fmt:formatNumber value="${pubitemObj.nrestprice}" pattern="0.00" /></span></td>
										<td><span style="width:75px;">${pubitemObj.vinit}</span></td>
										<td><span style="width:70px;">${pubitemObj.vgrptyp}</span></td>
										<td><span style="width:70px;">${pubitemObj.vgrp}</span></td>
										<td><span style="width:70px;">${pubitemObj.vtyp}</span></td>
										<td><span style="width:50px;" class="${pubitemObj.enablestate }">
											<c:if test="${pubitemObj.enablestate==1}"><fmt:message key="not_enabled" /></c:if>
											<c:if test="${pubitemObj.enablestate==2}"><fmt:message key="have_enabled" /></c:if>
											<c:if test="${pubitemObj.enablestate==3}"><fmt:message key="stop_enabled" /></c:if>
											</span>
										</td>
										<td><span style="width:70px;">${pubitemObj.vclass}</span></td>
										<td><span style="width:70px;">${pubitemObj.dishdisctyp}</span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>			
				</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />	
			<input type="hidden" name ="pk_pubitemid"  id="pk_pubitemid" value="${pubitem.pk_pubitem }" />		
			
			<div class="search-div" style="margin-left: 0px;">
				<div class="search-condition">
					<table class="search-table" cellspacing="0" cellpadding="0" >
						<tr>
							<td class="c-left"><fmt:message key="coding" />：</td>
							<td><input type="text" id="vcode" name="vcode" class="text" value="${pubitem.vcode }" /></td>
							<td style="width: 20px;"></td>
							<td class="c-left"><fmt:message key="name" />：</td>
							<td><input type="text" id="vname" name="vname" class="text" value="${pubitem.vname }" onkeyup="getSpInit(this,'vinit');" /></td>
							<td style="width: 20px;"></td>
							<td class="c-left"><fmt:message key="abbreviation" />：</td>
							<td><input type="text" id="vinit" name="vinit" class="text" value="${pubitem.vinit }" /></td>
						</tr>
						<tr style="display: none;">
							<td class="c-left"><fmt:message key="a_class" />：</td>
							<td>
								<select id="pk_vgrptyp" name="pk_vgrptyp" class="select"  disabled="disabled">
									<option value="" selected="selected"></option>
									<c:forEach var="father" items="${MarsaleclassList0}" varStatus="status">
										<option value="${father.PK_MARSALECLASS }"
											<c:if test="${father.PK_MARSALECLASS==pubitem.pk_vgrptyp }"> selected="selected" </c:if>>${father.NAME}</option>
									</c:forEach>
								</select>
								<input type="hidden" value="${pubitem.pk_vgrptyp }" name="pk_vgrptyp"/>
							</td>
							<td class="c-left"><fmt:message key="two_classes" />：</td>
							<td>	
								<select id="pk_vgrp" name="pk_vgrp" class="select" disabled="disabled">
									<option value="" selected="selected"></option>
									<c:forEach var="father" items="${MarsaleclassList1}" varStatus="status">
										<option value="${father.PK_MARSALECLASS }"
											<c:if test="${father.PK_MARSALECLASS==pubitem.pk_vgrp }"> selected="selected" </c:if>>${father.NAME}</option>
									</c:forEach>
								</select>
								<input type="hidden" value="${pubitem.pk_vgrp }" name="pk_vgrp"/>
							</td>
							<td class="c-left"><fmt:message key="level_3_class" />：</td>
							<td>
								<select id="pk_vtyp" name="pk_vtyp" class="select"  disabled="disabled">
									<option value="" selected="selected"></option>
									<c:forEach var="father" items="${MarsaleclassList2}" varStatus="status">
										<option value="${father.PK_MARSALECLASS }"
											<c:if test="${father.PK_MARSALECLASS==pubitem.pk_vtyp }"> selected="selected" </c:if>>${father.NAME}</option>
									</c:forEach>
								</select>
								<input type="hidden" value="${pubitem.pk_vtyp }" name="pk_vtyp"/>
							</td>
						</tr>
					</table>
				</div>
				<div class="search-commit">
		       		<input type="button" class="search-button" id="search" value='<fmt:message key="enter" />'/>
		       		<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
				</div>
			</div>
		</form>
		<div id="wait2" style="display: none;"></div>
    	<div id="wait" style="display: none;">
			<img src="<%=path%>/image/loading_detail.gif" />&nbsp;
			<span style="color:white;font-size:15px;vertical-align: middle;"><fmt:message key="please_wait" />...</span>
		</div>
		<input type="hidden" id="pk_vgrptyp" name="pk_vgrptyp" value="${pubitem.pk_vgrptyp}" />
		<input type="hidden" id="pk_vgrp" name="pk_vgrp" value="${pubitem.pk_vgrp}" />
		<input type="hidden" id="pk_vtyp" name="pk_vtyp" value="${pubitem.pk_vtyp}" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript">

			//双击弹出<fmt:message key="update" />
			$('.grid').find('.table-body').find('tr').live("dblclick", function () {
				$(":checkbox").attr("checked", false);
				$(this).find(':checkbox').attr("checked", true);
				pubitem();
				if($(this).hasClass("show-firm-row"))return;
				$('.grid').find('.table-body').find('tr').removeClass("show-firm-row");
				$(this).addClass("show-firm-row");
			 });
			$(document).ready(function(){
				var tool = $('.tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							$('.search-div').slideToggle(100);
							$('#vcode').focus();//点击查询按钮，聚焦查询框
						}
					},'-',{
						text: '<fmt:message key="detail_query" />',
						title: '<fmt:message key="detail_query" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							pubitem();
						}
					},'-',{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}
					]
				});
					
				$('.search-div').hide();
					
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#listForm').submit();
				});
				
				/* 模糊<fmt:message key="select" />清空 */
				$("#resetSearch").bind('click', function() {
					$('.search-condition input').val('');
				});
	
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),20);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
			});
			
			//实现复选框单选
			function CheckBoxCheck(chk){
			    var obj = $('.grid').find('.table-body').find('input[name=idList]');
			    for(var i = 0; i < obj.length; i++){
			        if(obj[i].type == "checkbox"){
			            obj[i].checked = false;
			        }
			    }
			    chk.checked=true;
			 }
			
			//调转到菜品详细页面
			function pubitem(){
				var checkboxList = $('.grid').find('.table-body').find('input[name=idList]');
				var chkValue = checkboxList.filter(':checked').eq(0).val();
				var curpage = $("#nowPage").val();
				if(checkboxList && checkboxList.filter(':checked').size() ==1){
					$('body').window({
						title: '<fmt:message key="pubitem" /><fmt:message key="the_detail" />',
						content: '<iframe id="updatePubItemFrame" frameborder="0" src="<%=path%>/baseRecord/updatePubitem.do?pk_pubitem='+chkValue+'&nowPage='+curpage+'"></iframe>',
						width: '650px',
						height: '500px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList && checkboxList.filter(':checked').size() > 1){
					alert('<fmt:message key="you_can_only_view_a_data" />！');
					return;
				}else {
					alert('<fmt:message key="please_select_to_see"/>！');
					return ;
				}
			}
			function pageReload(){
				$("#listForm").submit();
			}
		</script>
	</body>
</html>