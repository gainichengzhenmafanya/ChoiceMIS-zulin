<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="branches_and_positions_information" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<style type="text/css">
			.form{
				margin-top:50px;
			}
		</style>
	</head>
	<body>
		<div class="form">
			<form id="waterForm" method="post" action="<%=path %>/otherStockData/saveOtherstockDataF.do">
				<div class="form-line">
					<div class="form-label">
						<span class="red">*</span><fmt:message key="name" />：
					</div>
					<div class="form-input">
						<input type="text" id="vname" name="vname" class="text" value="${dataF.vname }" />
					</div>
					<div class="form-label"><fmt:message key="scm_reference" /><fmt:message key="unit_price" />：</div>
					<div class="form-input">
						<input type="text" id="price" name="price" class="text" value="${dataF.price }"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="beginning_of_period" /><fmt:message key="value" />：</div>
					<div class="form-input">
						<input type="text" id="begincount" name="begincount" class="text" value="${dataF.begincount }"/>
					</div>
					<div class="form-label"><span class="red">*</span><fmt:message key="unit" />：</div>
					<div class="form-input">
						<input type="text" id="unit" name="unit" class="text" value="${dataF.unit }" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="beginning_of_period" /><fmt:message key="date" />：</div>
					<div class="form-input">
						<input type="text" id="begindate" name="begindate" class="Wdate text" />
					</div>
					<div class="form-label"><span class="red">*</span><fmt:message key="no" />：</div>
					<div class="form-input">
						<input type="text" id="sort" name="sort" class="text" value="${dataF.sort }" readonly="readonly"/>
					</div>
				</div>
				<div class="form-line" <c:if test="${statistical != 'dian' }">style="display: none;"</c:if> >
					<div class="form-label"><span class="red">*</span><fmt:message key="beilv" />：</div>
					<div class="form-input">
						<input type="text" id="meterrate" name="meterrate" class="text" value="${dataF.meterrate }"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="payment" />：</div>
					<div class="form-input">
						<select id="prepayment" class="select" name="prepayment">
							<option value="1" <c:if test="${1 == dataF.prepayment }" >selected="selected"</c:if>><fmt:message key="prepaid" /></option>
							<option value="0" <c:if test="${0 == dataF.prepayment }" >selected="selected"</c:if>><fmt:message key="After_paying" /></option>
						</select>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="remark" />：</div>
					<div class="form-input">
						<textarea rows="4" style="white-space:wrap;margin-top:3px;" cols="55" id="memo" name="memo">${dataF.memo }</textarea>
					</div>
				</div>
				<input type="hidden" id="type" name="type" value="${type }" />
				<input type="hidden" id="pk_otherstockdataf" name="pk_otherstockdataf" value="${dataF.pk_otherstockdataf }" />
			</form>
		</div>
		<input type="hidden" id="subflag" value="0" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
		<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			$("#begindate").htmlUtils("setDate","now");
			$("#begindate").click(function(){
	  			new WdatePicker({minDate:'#F{$dp.$D(\'begindate\')}'});
	  		});
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'begincount',
						validateType:['canNull','num2'],
						param:['F','F'],
						error:['<fmt:message key="beginning_of_period" /><fmt:message key="value" /><fmt:message key="cannot_be_empty" />！',
						       '<fmt:message key="beginning_of_period" /><fmt:message key="value" /><fmt:message key="must_be_numeric" />！']
					},{
						type:'text',
						validateObj:'meterrate',
						validateType:['canNull','num2'],
						param:['F','F'],
						error:['<fmt:message key="beilv" /><fmt:message key="cannot_be_empty" />！',
						       '<fmt:message key="beilv" /><fmt:message key="must_be_numeric" />！']
					},{
						type:'text',
						validateObj:'vname',
						validateType:['canNull','maxLength','withOutSpecialChar'],
						param:['F',50,'F'],
						error:['<fmt:message key="name" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="the_maximum_length" />50','<fmt:message key="no_contain_special_char" />！']
					},{
						type:'text',
						validateObj:'unit',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="unit" /><fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'sort',
						validateType:['canNull','intege2','maxValue'],
						param:['F','F','999999'],
						error:['<fmt:message key="no" /><fmt:message key="cannot_be_empty" />',
						       '<fmt:message key="enter_the_number_of_non_zero" /><fmt:message key="coding" />！',
						       '<fmt:message key="sort_column_is_too_large" />999999']
					},{
						type:'text',
						validateObj:'memo',
						validateType:['maxLength'],
						param:[150],
						error:['<fmt:message key="the_maximum_length" />150']
					}]
				});
			});
		</script>
	</body>
</html>