<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>POS<fmt:message key="set_up_the" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/operatorMaintenance/listOperatorMaintenance.do" method="post">
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width: 20px;"></span></td>
								<td style="width:30px; text-align: center;">
									<input type="checkbox" id="chkAll"/>
								</td>
								<td><span style="width:60px;"><fmt:message key="employees" /><fmt:message key="coding" /></span></td>
								<td><span style="width:60px;"><fmt:message key="name" /></span></td>
								<td><span style="width:100px;"><fmt:message key="Card_number" /></span></td>
								<td><span style="width:60px;"><fmt:message key="sector" /></span></td>
								<td><span style="width:100px;"><fmt:message key="role" /></span></td>
								<td><span style="width:100px;"><fmt:message key="use" /><fmt:message key="screen" /></span></td>
								<td><span style="width:50px;"><fmt:message key="abbreviation" /></span></td>
								<td><span style="width:100px;"><fmt:message key="allowed_error" /><fmt:message key="count" /></span></td>
								<td><span style="width:50px;"><fmt:message key="enable_state" /></span></td>
								<td><span style="width:50px;"><fmt:message key="no" /></span></td>
								<td><span style="width:200px;"><fmt:message key="remark" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" class="datagrid">
						<tbody>
							<c:forEach var="operator" items="${listOperatorMaintenance}" varStatus="status">
								<tr>
									<td class="num"><span style="width: 20px;">${status.index+1}</span></td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" name="idList" id="chk_<c:out value='${operator.pk_operator}' />" value="<c:out value='${operator.pk_operator}' />"/>
									</td>
									<td><span style="width:60px;">${operator.vcode}</span></td>
									<td><span style="width:60px;">${operator.vname}</span></td>
									<td><span style="width:100px;">${operator.vcardno}</span></td>
									<td><span style="width:60px;">${operator.deptname}</span></td>
									<td><span style="width:100px;">${operator.rolename}</span></td>
									<td><span style="width:100px;">
										<c:if test="${operator.vusescreen =='1'}"><fmt:message key="the_manager" /></c:if>
										<c:if test="${operator.vusescreen =='2'}"><fmt:message key="choose_food_staff" /></c:if>
										<c:if test="${operator.vusescreen =='3'}"><fmt:message key="cashier" /></c:if>
								 		<c:if test="${operator.vusescreen =='4'}"><fmt:message key="sys_manager" /></c:if>
										<c:if test="${operator.vusescreen =='5'}"><fmt:message key="delivery" /></c:if>
										<c:if test="${operator.vusescreen =='6'}"><fmt:message key="usher" /></c:if>
										<c:if test="${operator.vusescreen =='7'}"><fmt:message key="Assistant_Restaurant" /></c:if>
										<c:if test="${operator.vusescreen =='8'}"><fmt:message key="Company_salesman" /></c:if>
									</span></td>
									<td><span style="width:50px;">${operator.vinit}</span></td>
									<td><span style="width:100px;">${operator.iretrycount}</span></td>
									<td><span style="width:50px;">
										<c:if test="${operator.enablestate == '1'}"><fmt:message key="not_enabled" /></c:if>
										<c:if test="${operator.enablestate == '2'}"><fmt:message key="have_enabled" /></c:if>
										<c:if test="${operator.enablestate == '3'}"><fmt:message key="stop_enabled" /></c:if>
									</span></td>
									<td><span style="width:50px;">${operator.isortno}</span></td>
									<td><span style="width:200px;">${operator.vmemo}</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>			
			</div>
			<div class="search-div" style="margin-left: 0px;">
				<div class="search-condition">
					<table class="search-table" cellspacing="0" cellpadding="0" >
						<tr>
							<td><fmt:message key="coding" />：</td>
							<td><input type="text" id="vcode" name="vcode" class="text" /></td>
							<td><fmt:message key="name" />：</td>
							<td><input type="text" id="vname" name="vname" class="text" /></td>
							<td><fmt:message key="abbreviation" />：</td>
							<td><input type="text" id="vinit" name="vinit" class="text" /></td>
							<td><fmt:message key="enable_state" />：</td>
							<td>
								<select id="enablestate" name="enablestate" class="select" style="height: 22px;margin-top: 3px; border: 1px solid #999999;">
									<option value="" ><fmt:message key="all" /></option>
									<option value="1" ><fmt:message key="not_enabled" /></option>
									<option value="2" selected="selected"><fmt:message key="have_enabled" /></option>
									<option value="3" ><fmt:message key="stop_enabled" /></option>
								</select>
							</td>
						</tr>
					</table>
				</div>
			<div class="search-commit">
	       		<input type="button" class="search-button" id="search" value='<fmt:message key="enter" />'/>
	       		<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
			</div>
		</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>	
		<script type="text/javascript">
			$(document).ready(function(){
				/* 模糊<fmt:message key="select" />提交 */
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#listForm').submit();
				});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								$('.search-div').slideToggle(100);
							}
						},'-',{
							text: '<fmt:message key="insert" />',
							title: '<fmt:message key="insert"/><fmt:message key="data"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								addCashSaving();	
							}
						},{
							text: '<fmt:message key="update" />',
							title: '<fmt:message key="update"/><fmt:message key="data"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								updateCashSaving();	
							}
						},{
							text: '<fmt:message key="enable" />',
							title: '<fmt:message key="enable" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','-40px']
							},
							handler: function(){
								enable();
							}
						},{
							text: '<fmt:message key="disable1" />',
							title: '<fmt:message key="disable1" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-40px']
							},
							handler: function(){
								disable();
							}
						},{
							text: '<fmt:message key="publish" />',
							title: '<fmt:message key="publish"/><fmt:message key="data"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								checkPublishData();	
							}
						},{
							text: '<fmt:message key="delete" />',
							title: '<fmt:message key="delete"/><fmt:message key="data"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								deleteCashSaving();	
							}
						},'-',{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}
					]
				});
				
				// 自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),30);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
			});

			//双击弹出<fmt:message key="update" />
			$('.grid').find('.table-body').find('tr').live("dblclick", function () {
				$(":checkbox").attr("checked", false);
				$(this).find(':checkbox').attr("checked", true);
				updateCashSaving();
				if($(this).hasClass("show-firm-row"))return;
				$('.grid').find('.table-body').find('tr').removeClass("show-firm-row");
				$(this).addClass("show-firm-row");
			 });
			
			//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     }
			     else
			     {
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			     }
			 });
			
			//新增存款
			function addCashSaving(){
				$('body').window({
					id: 'window_savecashSaving',
					title: '<fmt:message key="insert" /><fmt:message key="data" />',
					content: '<iframe id="SaveForm" frameborder="0" src="<%=path%>/operatorMaintenance/edit.do?iflag=add"></iframe>',
					width: '600px',
					height: '300px',
					draggable: true,
					isModal: true,
					position : {
						left : 260
					},
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								title: '<fmt:message key="save_role_of_information"/>',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('SaveForm')&&window.document.getElementById("SaveForm").contentWindow.validate._submitValidate()){
										document.getElementById("SaveForm").contentWindow.document.forms["listForm"].submit();
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});	
			}
			
			//修改存款
			function updateCashSaving(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() ==1){
					var action='<%=path%>/operatorMaintenance/edit.do?iflag=update&pk_operator='+checkboxList.filter(':checked').val();
					$('body').window({
						id: 'window_selectTargetStore',
						title: '<fmt:message key="update" /><fmt:message key="data" />',
						content: '<iframe id="UpdateForm" frameborder="0" src='+action+'></iframe>',
						width: '600px',
						height: '300px',
						draggable: true,
						isModal: true,
						position : {
							left : 260
						},
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="update" /><fmt:message key="data" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){ 
										if(getFrame('UpdateForm')&&window.document.getElementById("UpdateForm").contentWindow.validate._submitValidate()){
											document.getElementById("UpdateForm").contentWindow.document.forms["listForm"].submit();
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList && checkboxList.filter(':checked').size()>1){
					alert('<fmt:message key="you_can_only_modify_a_data" />!');
				}else{
					alert('<fmt:message key="please_select_data" />!');
				}
			}
			
			//启用数据
			function enable(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() > 0){
					if(confirm("<fmt:message key='want_to_enable' />？")){
						var chkValue = [];
						var codeValue = [];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
							codeValue.push($(this).closest('td').next().find('span').text());
						});
						var action = '<%=path%>/operatorMaintenance/save.do?iflag=enable&pk_operator='+chkValue.join(",")+'&vcode='+codeValue.join(",");
						$('body').window({
							title: '<fmt:message key="enable" />',
							content: '<iframe id="updateButtontypeFrame" frameborder="0" src="'+action+'"></iframe>',
							width: '300px',
							height: '200px',
							draggable: true,
							isModal: true
						});
					}
				}else{
					alert("<fmt:message key='select_you_want_to_enable' />");
					return ;
				}
			}
			
			//停用数据
			function disable(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() > 0){
					if(confirm("<fmt:message key='want_to_disable' />？")){
						var chkValue = [];
						var codeValue = [];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
							codeValue.push($(this).closest('td').next().find('span').text());
						});
						
						var action = '<%=path%>/operatorMaintenance/save.do?iflag=disable&pk_operator='+chkValue.join(",")+'&vcode='+codeValue.join(",");
						$('body').window({
							title: '<fmt:message key="disable1" />',
							content: '<iframe id="updateButtontypeFrame" frameborder="0" src="'+action+'"></iframe>',
							width: '300px',
							height: '200px',
							draggable: true,
							isModal: true
						});
					}
				}else{
					alert("<fmt:message key='select_you_want_to_disable' />！");
					return ;
				}
			}
			
			//删除数据
			function deleteCashSaving(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() > 0){
					if(confirm('<fmt:message key="delete_data_confirm" />？')){
						var chkValue = [];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
						});
						var action = '<%=path%>/operatorMaintenance/save.do?iflag=delete&pk_operator='+chkValue.join(",");
						$('body').window({
							title: '<fmt:message key="delete" /><fmt:message key="data" />',
							content: '<iframe frameborder="0" src='+action+'></iframe>',
							width: '340px',
							height: '255px',
							draggable: true,
							isModal: true
						});
					}
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				}
			}
			
			//数据发布
			function checkPublishData(){
				$.ajaxSetup({async:false});
				$.ajax({
					type : 'POST',
					data : {bohElement:'employee.xml'},
					url : '<%=path%>/publish/toPublish.do',
					success : function(message) {
						$('#wait2',document).css("visibility","hidden");
						$('#wait',document).css("visibility","hidden");
						if(message == "ok"){
							showMessage({
							type: 'success',
							msg: '<fmt:message key="publish_sussess" />!',
							speed: 2000
							});
						}else{
							showMessage({
							type: 'error',
							msg: '<fmt:message key="publish_failer" />--'+message+'<fmt:message key="create_failer" />!',
							speed: 2000
							});
						}
					},
					error:function(e){
						alert(e);
					}
				});
			}
		</script>
	</body>
</html>