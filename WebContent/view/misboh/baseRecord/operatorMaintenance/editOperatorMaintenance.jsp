<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="store" /><fmt:message key="operator" /><fmt:message key="edit" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<style type="text/css">
 			.separator{ 
 				display:none !important; 
			} 
 			.page{ 
 				margin-bottom: 28px; 
			} 
 			div[class]{ 
 				display:block; 
 			} 
			.tr-select{ 
				background-color: #D2E9FF; 
			} 
		</style>
	</head>
	<body>
		<div style="margin:0 auto;">
			<form id="listForm" action="<%=path%>/operatorMaintenance/save.do" method="post" style="float: left; width: 99.9%;margin: 0 auto;">
				<div class="form-line aligninput">
					<div class="form-label"><span class="red">*</span><fmt:message key="coding" />:</div>
					<div class="form-input">
						<input type="text" id="vcode" name="vcode" class="text" value="${operator.vcode }" 
							<c:if test="${null != operator }">readonly="readonly"</c:if> />
					</div>
					<div class="form-label"><span class="red">*</span><fmt:message key="name" />:</div>
					<div class="form-input">
						<input type="text" id="vname" name="vname" class="text" value="${operator.vname}" onblur="getSpInit(this,'vinit');"/>
					</div>
				</div>
				<div class="form-line aligninput">
					<div class="form-label "><span class="red">*</span><fmt:message key="abbreviation" />:</div>
					<div class="form-input">
						<input type="text" id="vinit" name="vinit" class="text" value="${operator.vinit}"/>
					</div>
					<div class="form-label"><span class="red">*</span><fmt:message key="no" />:</div>
					<div class="form-input">
						<input type="text" maxlength="7" id="isortno" name="isortno" class="text" value="${operator.isortno}"/>
					</div>
				</div>
				<div class="form-line aligninput">
					<div class="form-label"><span class="red">*</span><fmt:message key="password" />:</div>
					<div class="form-input">
						<input type="text" id="vpassword" name="vpassword" class="text" value="${operator.vpassword}"/>
					</div>
					<div class="form-label"><span class="red">*</span><fmt:message key="allowed_error" /><fmt:message key="count" />:</div>
					<div class="form-input">
						<input type="text" id="iretrycount" name="iretrycount" class="text" value="${operator.iretrycount}"/>
					</div>
				</div>
				<div class="form-line aligninput">
					<div class="form-label"><span class="red">*</span><fmt:message key="enable_state" />:</div>
					<div class="form-input">
						<select id="enablestate" name="enablestate" class="select" style="width:133px;">
								<option value="2"  <c:if test="${operator.enablestate==2}"> selected="selected" </c:if> ><fmt:message key="have_enabled" /></option>
								<option value="1"  <c:if test="${operator.enablestate==1}"> selected="selected" </c:if> ><fmt:message key="not_enabled" /></option>
								<option value="3"  <c:if test="${operator.enablestate==3}"> selected="selected" </c:if> ><fmt:message key="stop_enabled" /></option>
						</select>
					</div>
					<div class="form-label"><span class="red">*</span><fmt:message key="role" />:</div>
					<div class="form-input">
						<select id="pk_role" name="pk_role" class="select">
							<c:forEach var="role" items="${listRole }">
								<option value="${role.pk_role }" <c:if test="${operator.pk_role == role.pk_role}">selected="selected"</c:if>>${role.vname}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-line aligninput">
					<div class="form-label"><span class="red">*</span><fmt:message key="sector" />:</div>
					<div class="form-input">
						<select id="pk_dept" name="pk_dept" class="select">
							<c:forEach var="storeDept" items="${listStoreDept }">
								<option value="${storeDept.pk_storedept }" <c:if test="${operator.pk_dept == storeDept.pk_storedept}">selected="selected"</c:if>>${storeDept.vname}</option>
							</c:forEach>
						</select>
					</div>
					<div class="form-label"><fmt:message key="Card_number" />:</div>
					<div class="form-input">
						<input type="text" id="vcardno" name="vcardno" maxlength="50" class="text" value="${operator.vcardno}"  />
					</div>
				</div>
				<div class="form-line aligninput">
					<div class="form-label"><fmt:message key="use" /><fmt:message key="screen" />:</div>
					<div class="form-input">
						<select id="vusescreen" name="vusescreen" class="select">
							<option value="1" <c:if test="${operator.vusescreen =='1'}">selected="selected"</c:if> ><fmt:message key="the_manager" /></option>
							<option value="2" <c:if test="${operator.vusescreen =='2'}">selected="selected"</c:if> ><fmt:message key="choose_food_staff" /></option>
							<option value="3" <c:if test="${operator.vusescreen =='3'}">selected="selected"</c:if> ><fmt:message key="cashier" /></option>
							<option value="4" <c:if test="${operator.vusescreen =='4'}">selected="selected"</c:if> ><fmt:message key="misboh_sys_manager" /></option>
							<option value="5" <c:if test="${operator.vusescreen =='5'}">selected="selected"</c:if> ><fmt:message key="misboh_delivery" /></option>
							<option value="6" <c:if test="${operator.vusescreen =='6'}">selected="selected"</c:if> ><fmt:message key="misboh_usher" /></option>
							<option value="7" <c:if test="${operator.vusescreen =='7'}">selected="selected"</c:if> ><fmt:message key="misboh_Assistant_Restaurant" /> </option>
							<option value="8" <c:if test="${operator.vusescreen =='8'}">selected="selected"</c:if> ><fmt:message key="misboh_Company_salesman" /></option>
						</select>
					</div>
					<div class="form-label"><fmt:message key="remark" />:</div>
					<div class="form-input">
						<input type="text" id="vmemo" name="vmemo" class="text" value="${operator.vmemo}"  />
					</div>
				</div>
				<input type="hidden" id="pk_operator" name="pk_operator" value="${operator.pk_operator}"/>
				<input type="hidden" id="pk_store" name="pk_store" value="${pk_store}"/>
				<input type="hidden" id="iflag" name="iflag" value="${iflag}"/>
			</form>
		</div>
		<input type="hidden" id="subflag" value="0" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript">
			var validate;
			function pageReload(){
				$('#listForm').submit();
			}
			$(document).ready(function(){
				//${operator}为空时为新增，添加默认编码、排序、允许错误次数
				if (null == '${operator}' || '' == '${operator}') {
					getDefaultVcode("<%=path%>","CBOH_OPERATOR_3CH","vcode","vcode","101");
					getSortNoMethod();
					$("#iretrycount").val(6);
				}
				//验证参数
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'deptname',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="cannot_be_empty" />!']
					},{
						type:'text',
						validateObj:'rolename',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="role" /><fmt:message key="cannot_be_empty" />!']
					},{
						type:'text',
						validateObj:'vcode',
						validateType:['canNull','intege2','maxLength'],
						param:['F','F',8],
						error:['<fmt:message key="coding" /><fmt:message key="cannot_be_empty" />!','<fmt:message key="coding" /><fmt:message key="only_start_with_nozero" />!','<fmt:message key="the_maximum_length" />8!']
					},{
						type:'text',
						validateObj:'vname',
						validateType:['canNull','maxLength','withOutSpecialChar'],
						param:['F',50,'F'],
						error:['<fmt:message key="name" /><fmt:message key="cannot_be_empty" />!','<fmt:message key="the_maximum_length" />50!','<fmt:message key="no_contain_special_char" />！']
					},{
						type:'text',
						validateObj:'vinit',
						validateType:['canNull','maxLength'],
						param:['F',50],
						error:['<fmt:message key="abbreviation" /><fmt:message key="cannot_be_empty" />!','<fmt:message key="the_maximum_length" />50!']
					},{
						type:'text',
						validateObj:'vpassword',
						validateType:['canNull','num1'],
						param:['F','F'],
						error:['<fmt:message key="password" /><fmt:message key="cannot_be_empty" />!','<fmt:message key="password" />必须为数字!']
					},{
						type:'text',
						validateObj:'iretrycount',
						validateType:['canNull','num1','maxLength'],
						param:['F','F',9],
						error:['<fmt:message key="allowed_error" /><fmt:message key="count" /><fmt:message key="cannot_be_empty" />!','<fmt:message key="allowed_error" /><fmt:message key="count" />必须为数字!','<fmt:message key="the_maximum_length" />9']
					},{
						type:'text',
						validateObj:'isortno',
						validateType:['canNull','intege2'],
						param:['F','F'],
						error:['<fmt:message key="no" /><fmt:message key="cannot_be_empty" />!','<fmt:message key="no" /><fmt:message key="only_start_with_nozero" />!']
					},{
						type:'text',
						validateObj:'vmemo',
						validateType:['maxLength'],
						param:[200],
						error:['<fmt:message key="the_maximum_length" />200!']
					}]
				});
				
				//自动实现滚动条
	// 			setElementHeight('.grid',['.tool'],$(document.body),25);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
			});
			
			//<fmt:message key="store" /><fmt:message key="role" /><fmt:message key="select1" />
			$("#selectStoreRole").click(function(){
				selectStoreRole({
		 				basePath:'<%=path%>',
		 				domId:'pk_role',
		 				single:true,
		 				width:500,
		 				height:400,
		 				vfoodsign:$("#vfoodsign").val()
		 			});
			});
			
			function setStoreRole(data){
				$("#pk_role").val(data.code);   
				$("#rolename").val(data.show); 
			}
			
			//选择门店部门
			$("#selectStoreDept").click(function(){
				var pk_store=$("#pk_store").val();
				selectStoreDept({
		 				basePath:'<%=path%>',
		 				single:true,
		 				pk_id:$("#pk_store").val(),
		 				width:500,
		 				height:400
		 			});
			});
			
			//选择门店部门
// 			function selectStoreDept(params) {
// 				var basePath = params.basePath;
// 				var url = basePath + "/operator/toChooseStoreDept.do";
// 				var callBack = params.callBack ? params.callBack : "setStoreDept";
// 				url += ("?callBack=" + callBack);
// 				url += params.domId ? '&domId=' + params.domId : '&domId=selected';
// 				if (params.single)
// 					url += '&single=true';
//				pk_id列表界面的集合，在列表界面将不会显示这些id的数据，以便数据不会重复选择
// 				url += '&pk_id=' + (params.pk_id ? params.pk_id : '');
// 				url += '&pk_store=' + (params.pk_store ? params.pk_store : '');
// 				var data = params.param;
// 				for ( var i in data) {
// 					url += ('&' + i + '=' + data[i]);
// 				}
// 				return $('body').window({
// 					id : 'window_chooseStoreDept',
// 					title : '<fmt:message key="select1" /><fmt:message key="store" /><fmt:message key="sector" />',
// 					content : '<iframe id="chooseStoreDept" frameborder="0" src='
// 							+ url + '></iframe>',
// 					width : params.width ? params.width : 500,
// 					height : params.height ? params.height : 450,
// 					confirmClose : false,
// 					draggable : true,
// 					isModal : true
// 				});
// 			}
			
			function setStoreDept(data){
				$("#pk_dept").val(data.code);   
				$("#deptname").val(data.show); 
			}
			// 获取<fmt:message key="no" />值
			function getSortNoMethod(){
				var commonMethod = new CommonMethod();
				
				commonMethod.isortno = "ISORTNO"; // <fmt:message key="no" />字段名
				commonMethod.vtablename = "CBOH_OPERATOR_3CH"; //对应表名
				var returnvalue = getSortNo("<%=path %>",commonMethod);
				
				$("#isortno").val(returnvalue);
			}
		</script>
	</body>
</html>