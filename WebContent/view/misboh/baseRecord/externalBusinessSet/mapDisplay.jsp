<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%String path = request.getContextPath();%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
		<script charset="utf-8" src="http://map.qq.com/api/js?v=2.exp"></script>
		<title>外送商圈设定</title>
		<link rel="shortcut icon" type="image/x_icon" href="">
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
		<link rel="stylesheet" href="http://jqueryui.com/resources/demos/style.css">
		<style type="text/css">
			#container{
			  min-width:700px;
			  min-height:800px;
			  }
		</style>
	</head>
	<body onload="init()">
		<form id="listForm" action="<%=path%>/operatorMaintenance/mapDisplay.do" method="post">
			<input type="hidden" id="pk_store" value="${pk_store }">
		    <input type="hidden" id="color" name="color" />
		    <input type="hidden" id="iareaseqs" name="iareaseqs" />
		</form>
	    <div id="dialog" hidden="hidden" style="text-align: center;padding-top: 20px;font-size: 14px;color: red;" title="<fmt:message key='tips' />"></div>
	    <div id="container"></div>
	    
		<script src="//code.jquery.com/jquery-1.9.1.js"></script>
		<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
		<script type="text/javascript" src="<%=path%>/js/StringBuilder.js"></script>
	    
		<script type="text/javascript">
			var citylocation, map, path, path1 = null; 
			var polygon1, polygon2, polygon3, polygon4, polygon5 = null;
      	  	var data = {};
			var listMapInfo = [];
			$.ajaxSetup({async:false});
			
			var init = function() {
			    var center = new qq.maps.LatLng(39.916527, 116.397128);
			    var map = new qq.maps.Map(document.getElementById("container"), {
			    	center: center,
			    	zoom: 13
			    });
				
			  	//获取城市列表接口设置中心点
			    citylocation = new qq.maps.CityService({
			        complete : function(result){
			            map.setCenter(result.detail.latLng);
			            var lat = result.detail.latLng.getLat();	//获取当前纬度值
			            var lng = result.detail.latLng.getLng();	//获取当前经度值
		            	var colors = $("#color").val();
			            var iareaseqs = $("#iareaseqs").val();
			            
	            		path1 = [
    	    		            new qq.maps.LatLng(lat, lng),
    	    		            new qq.maps.LatLng(lat + 0.014143, lng - 0.010792),
    	    		            new qq.maps.LatLng(lat + 0.018343, lng + 0.016143),
    	    		            new qq.maps.LatLng(lat, lng + 0.019146)
	    	    		        ];
			            if (0 != '${count}') {
			            	<c:forEach items="${listMapInfo}" varStatus="status" var="item"> 
			            		var color = '${item.vcolor}';		//获取各区域颜色
			            		var iareaseq = '${item.iareaseq}';	//获取各区域编号
			            		
			            		path = [
			            		        //循环获取各区域坐标
					            		<c:forEach items="${item.listRangeCoordi}" varStatus="status" var="latlng"> 
					            			new qq.maps.LatLng('${latlng.vlat}', '${latlng.vlng}'),
					            		</c:forEach>
		    	    		        ];
					            
			            		if (1 == iareaseq) {
			            			polygon1 = new qq.maps.Polygon({
				    		        	path: path,
				    		            map: map,
				    		            editable: true,
				    		            clickable: true,
				    		            zIndex: 1,
				       		            strokeColor: qq.maps.Color.fromHex(color, 1),
				    		            fillColor: qq.maps.Color.fromHex(color, 0.5),
				    		        });

									deleteData(polygon1, 1, '${count}');	//删除区域1
			            		}
			            		
			            		if (2 == iareaseq) {
			            			polygon2 = new qq.maps.Polygon({
				    		        	path: path,
				    		            map: map,
				    		            editable: true,
				    		            clickable: true,
				    		            zIndex: 2,
				       		            strokeColor: qq.maps.Color.fromHex(color, 1),
				    		            fillColor: qq.maps.Color.fromHex(color, 0.5),
				    		        });

									deleteData(polygon2, 2, '${count}');	//删除区域2
			            		}
			            		
			            		if (3 == iareaseq) {
			            			polygon3 = new qq.maps.Polygon({
				    		        	path: path,
				    		            map: map,
				    		            editable: true,
				    		            clickable: true,
				    		            zIndex: 3,
				       		            strokeColor: qq.maps.Color.fromHex(color, 1),
				    		            fillColor: qq.maps.Color.fromHex(color, 0.5),
				    		        });

									deleteData(polygon3, 3, '${count}');	//删除区域3
			            		}
			            		
			            		if (4 == iareaseq) {
			            			polygon4 = new qq.maps.Polygon({
				    		        	path: path,
				    		            map: map,
				    		            editable: true,
				    		            clickable: true,
				    		            zIndex: 4,
				       		            strokeColor: qq.maps.Color.fromHex(color, 1),
				    		            fillColor: qq.maps.Color.fromHex(color, 0.5),
				    		        });

									deleteData(polygon4, 4, '${count}');	//删除区域4
			            		}

			            		if (5 == iareaseq) {
			            			polygon5 = new qq.maps.Polygon({
				    		        	path: path,
				    		            map: map,
				    		            editable: true,
				    		            clickable: true,
				    		            zIndex: 5,
				       		            strokeColor: qq.maps.Color.fromHex(color, 1),
				    		            fillColor: qq.maps.Color.fromHex(color, 0.5),
				    		        });

									deleteData(polygon5, 5, '${count}');	//删除区域5
			            		}
			            		colors = "" == colors ? color : colors + "," + color;
			            		iareaseqs = "" == iareaseqs ? iareaseq : iareaseqs + "," + iareaseq;
		            		</c:forEach>
		            		$("#color").val(colors);
		            		$("#iareaseqs").val(iareaseqs);
			            } else {
				            polygon1 = new qq.maps.Polygon({
		    		        	path: path1,
		    		            map: map,
		    		            editable: true,
		    		            clickable: true,
		    		            zIndex: 1,
		       		            strokeColor: qq.maps.Color.fromHex("#F27157", 1),
		    		            fillColor: qq.maps.Color.fromHex("#F27157", 0.5)
		    		        });
				            $("#color").val("#F27157");
				            $("#iareaseqs").val(1);
				            deleteData(polygon1, 1, 1);	//删除区域1
			            }
			        }
			    });
				
			  	//输入错误区号，返回提示
			    citylocation.setError(function() {
			    	tishi("出错了，请输入正确的城市区号！！！");
		        });
			  	
			    citylocation.searchLocalCity();				//调用searchLocalCity()
// 			    citylocation.searchCityByAreaCode("0531");	//根据用户IP查询城市信息。
			    
	            //创建左上角边框div，以放置保存、返回按钮
		        var customZoomDiv1 = document.createElement("div");
		        customZoomDiv1.id = "anniu";
		        customZoomDiv1.style.cssText = "margin-left:60px;margin-top:30px;height:35px;width:50px;";
		        customZoomDiv1.index = 1; //设置在当前布局中的位置
	            customZoomDiv1.innerHTML = "<input type='button' id='save' onclick='save(0, 0)' value='保存' style='border:0px;color:white;background:black;height:35px;width:60px;' />" +
	            						   "<input type='button' id='save' onclick='back()' value='返回' style='margin-left:10px;border:0px;color:white;background:black;height:35px;width:60px;' />"	;
	            map.controls[qq.maps.ControlPosition.TOP_CENTER].push(customZoomDiv1);
	            
			    //创建右上角边框div，以放置添加配送区按钮及添加的区域div
		        var customZoomDiv = document.createElement("div");
		        customZoomDiv.id = "area";
		        customZoomDiv.style.cssText = "margin-left:750px;margin-top:30px;padding:5px;border:2px solid #86ACF2;background:#FFFFFF;height:270px;width:250px;";
		        customZoomDiv.index = 2; //设置在当前布局中的位置
	            customZoomDiv.innerHTML = "<span style='font-size:15px;margin-left:10px;'><B>配送区域</B></span> " +
	            						  "<input type='button' id='addArea' onclick='addArea()' value='添加配送区' style='margin-left:85px;border:0px;background:blue;height:20px;width:80px;' />";
	            map.controls[qq.maps.ControlPosition.TOP_CENTER].push(customZoomDiv);
	     
	            //若传过来的'${count}'不为0，则说明数据库中有数据，循环显示；否则直接加载
	            if (0 != '${count}') {
	            	<c:forEach items="${listMapInfo}" varStatus="status" var="item"> 
	            		if ('${status.index}' > 2) {
	            			customZoomDiv.style.height = 87 * '${status.index + 1}' + "px";
	            		}

	            		qsj = 0 == '${item.nstartprice}' ? "0 元" : '${item.ndistributfee}';		//起送价
	            		psf = 0 == '${item.ndistributfee}' ? "0 元" : '${item.ndistributfee}';	//配送费
	            		
	            		//默认创建一个含table的区域div
			        	var newDiv = document.createElement("div");
			        	newDiv.style.cssText = "margin-top:10px;padding-top:10px;border-top: 2px solid #F3F2F7;";
			        	newDiv.innerHTML = "<table cellspacing='2' cellpadding='2'>" +
			        					   "<tr><td width='120px' rowspan='2' bgcolor='${item.vcolor}'></td><td width='120px' rowspan='2'>区域${item.iareaseq}</td><td>起送价：<input type='text' id='qsj${item.iareaseq}' name='qsj${item.iareaseq}' value='" + qsj + "' style='width:60px;height:25px;border:1px solid #86acf2;text-align:right;color:#cccccc;' /></td></tr>" +
										   "<tr><td>配送费：<input type='text' id='psf${item.iareaseq}' name='psf${item.iareaseq}' value='" + psf + "' style='width:60px;height:25px;border:1px solid #86acf2;text-align:right;color:#cccccc;' /></td></tr></table>";
						customZoomDiv.appendChild(newDiv);
	            	</c:forEach>
	            } else {
					//默认创建一个含table的区域div
		        	var newDiv = document.createElement("div");
		        	newDiv.style.cssText = "margin-top:10px;padding-top:10px;border-top: 2px solid #F3F2F7;";
		        	newDiv.innerHTML = "<table cellspacing='2' cellpadding='2'>" +
		        					   "<tr><td width='120px' rowspan='2' bgcolor='#F27157'></td><td width='120px' rowspan='2'>区域1</td><td>起送价：<input type='text' id='qsj1' name='qsj1' value='0 元' onfocus='clearText(this)' style='width:60px;height:25px;border:1px solid #86acf2;text-align:right;color:#cccccc;' /></td></tr>" +
									   "<tr><td>配送费：<input type='text' id='psf1' value='0 元' name='psf1' onfocus='clearText(this)' style='width:60px;height:25px;border:1px solid #86acf2;text-align:right;color:#cccccc;' /></td></tr></table>";
					customZoomDiv.appendChild(newDiv);
	            }
			}
			
			//添加区域
	        function addArea(){
				var color = getRandomColor();						//获取随机生成的颜色值
				var strokeColor = qq.maps.Color.fromHex(color, 1);	//多边形区域变形颜色及透明度
				var fillColor = qq.maps.Color.fromHex(color, 0.5);	//多边形区域填充颜色及透明度
				var map = polygon1.getMap();						//多边形所在地图
				
				//获取已添加的含table的div个数+1
				var size = document.getElementById("area").getElementsByTagName("div").length + 1;	
				
				if (size > 5) {
					tishi("<fmt:message key='Up_to_5_zones_can_be_set_up' />！");	//显示提示信息
				} else {
					$("#color").val($("#color").val() + "," + color);
					$("#iareaseqs").val($("#iareaseqs").val() + "," + size);
					
					//当添加含table的动态div超过3个时，动态改变上一级div的高度
					if (size > 3) {
						document.getElementById("area").style.height = 87 * size + "px";
					}
					
					//创建含table的div
		        	var newDiv = document.createElement("div");
		        	newDiv.style.cssText = "margin-top:10px;padding-top:10px;border-top: 2px solid #F3F2F7;";
		        	newDiv.innerHTML = "<table cellspacing='2' cellpadding='2'>" +
									   "<tr><td width='120px' rowspan='2' bgcolor='" + color + "'></td><td width='120px' rowspan='2'>区域" + size + "</td><td>起送价：<input type='text' id='qsj" + size + "' name='qsj" + size + "' value='0 元' onfocus='clearText(this)' style='width:60px;height:25px;border:1px solid #86acf2;text-align:right;color:#cccccc;' /></td></tr>" +
									   "<tr><td>配送费：<input type='text' id='psf" + size + "' name='psf" + size + "' value='0 元' onfocus='clearText(this)' style='width:60px;height:25px;border:1px solid #86acf2;text-align:right;color:#cccccc;' /></td></tr></table>";
					document.getElementById("area").appendChild(newDiv);
					
					//创建一个区域div的同时在地图上创建一个多边形覆盖区域
					if (2 == size) {
						polygon2 = new qq.maps.Polygon({
		   		        	path: path1,
		   		            map: map,
		   		            editable: true,
	    		            clickable: true,
		   		         	zIndex: 2,
		   		            strokeColor: strokeColor,
				            fillColor: fillColor,
		   		        });

						deleteData(polygon2, 2, 2);	//删除添加的第二个区域
					}
					
					if (3 == size) {
						polygon3 = new qq.maps.Polygon({
		   		        	path: path1,
		   		            map: map,
		   		            editable: true,
	    		            clickable: true,
		   		         	zIndex: 3,
		   		            strokeColor: strokeColor,
				            fillColor: fillColor,
		   		        });

						deleteData(polygon3, 3, 3);	//删除添加的第三个区域
					}
					
					if (4 == size) {
						polygon4 = new qq.maps.Polygon({
		   		        	path: path1,
		   		            map: map,
		   		            editable: true,
	    		            clickable: true,
		   		         	zIndex: 4,
		   		            strokeColor: strokeColor,
				            fillColor: fillColor,
		   		        });

						deleteData(polygon4, 4, 4);	//删除添加的第四个区域
					}
					
					if (5 == size) {
						polygon5 = new qq.maps.Polygon({
		   		        	path: path1,
		   		            map: map,
		   		            editable: true,
	    		            clickable: true,
		   		         	zIndex: 5,
		   		            strokeColor: strokeColor,
				            fillColor: fillColor,
		   		        });
						
						deleteData(polygon5, 5, 5);	//删除添加的第五个区域
					}
				}
	        }

			//光标进入文本框时，若里面为0 元则清除掉
	        function clearText(obj) {
	        	if ("0 元" == obj.value) {
	            	obj.value = "";
	        	}
	        }
			
			//保存按钮执行，iareaseq为区域编号，flag为操作类型：0为保存，1为删除
			function save(num, flag){
				var colors = document.getElementById("color").value;
				var iareaseqs = $("#iareaseqs").val();
				var size = colors.split(",").length;	//获取添加的多边形总个数
				var str = new StringBuilder();
		
				for (var i = 1; size >= i; i++) {
					var result = "";
					color = colors.split(",")[i - 1];//获取区域号对应的颜色
					iareaseq = iareaseqs.split(",")[i - 1];
					
					var qsj = $("#qsj" + iareaseq).val();
					var psf = $("#psf" + iareaseq).val();
					
					if ("" == qsj || "0 元" == qsj) {
						qsj = 0;
					}
					
					if ("" == psf || "0 元" == psf) {
						psf = 0;
					}

					//区域1
					if (1 == iareaseq) {	
						var length = polygon1.getPath().getLength();					//获取第一个区域多边形的坐标点个数
						
						//循环多边形坐标点数组
						for (var j = 0; length > j; j++) {
							result = result + polygon1.getPath().getAt(j) + ";";		//获取第一个j索引处的坐标
							result = result.replace(", ", "-");							//将获取的坐标的","换成"-"
						}
						
						str.append(color + ":" + result + i + ";" + qsj + ";" + psf + ",");
					}
				
					//区域2
					if (2 == iareaseq) {	
						var length = polygon2.getPath().getLength();
						for (var j = 0; length > j; j++) {
							result = result + polygon2.getPath().getAt(j) + ";";
							result = result.replace(", ", "-");
						}

						str.append(color + ":" + result + i + ";" + qsj + ";" + psf + ",");
					}

					//区域3
					if (3 == iareaseq) {	
						var length = polygon3.getPath().getLength();
						for (var j = 0; length > j; j++) {
							result = result + polygon3.getPath().getAt(j) + ";";
							result = result.replace(", ", "-");
						}

						str.append(color + ":" + result + i + ";" + qsj + ";" + psf + ",");
					}

					//区域4
					if (4 == iareaseq) {	
						var length = polygon4.getPath().getLength();
						for (var j = 0; length > j; j++) {
							result = result + polygon4.getPath().getAt(j) + ";";
							result = result.replace(", ", "-");
						}

						str.append(color + ":" + result + i + ";" + qsj + ";" + psf + ",");
					}

					//区域5
					if (5 == iareaseq) {	
						var length = polygon5.getPath().getLength();
						for (var j = 0; length > j; j++) {
							result = result + polygon5.getPath().getAt(j) + ";";
							result = result.replace(", ", "-");
						}

						str.append(color + ":" + result + i + ";" + qsj + ";" + psf + ",");
					}
				}
				
				data["content"] = str.toString().substr(0, str.toString().length - 1);
				data["pk_store"] = $("#pk_store").val();
				data["iareaseq"] = num;
				$.post("<%=path%>/operatorMaintenance/saveContent.do", data, function(re){
					var result = "<fmt:message key='delete_fail' />！";
					
					//根据操作类型和返回值，赋予不同的提示信息
					if ("0" == flag && "Y" == re) {
						result = "<fmt:message key='save_successful' />！";
					} else if ("0" == flag && "N" == re) {
						result = "<fmt:message key='save_fail' />！";
					} else if ("1" == flag && "Y" == re) {
						result = "<fmt:message key='successful_deleted' />！";
					}
				
					//提示信息显示
					document.getElementById("dialog").innerHTML = result;
					$(function() {
						$("#dialog").dialog({
					    	resizable: false,
					      	height:140,
					      	modal: true,
					      	buttons: {
					      	  	"<fmt:message key='enter' />": function() {
					        		$(this).dialog("close");
									$("#listForm").submit();
					        	}
					      	}
					    });
					});
				});
			}

			//删除操作
			var deleteData = function(polygon, iareaseq, count) {
				if (1 == count) {
					qq.maps.event.addListener(polygon, "rightclick", function() {
						tishi("该店仅有这一个配送区域不可删除！");
					});
				} else {
	    			qq.maps.event.addListener(polygon, "rightclick", function() {
	        			document.getElementById("dialog").innerHTML = "是否要删除区域" + iareaseq + "？";
						$(function() {
							$("#dialog").dialog({
						    	resizable: false,
						      	height:140,
						      	modal: true,
						      	buttons: {
						      	  	"<fmt:message key='enter' />": function() {
	    								$(this).dialog("close");
	    								save(iareaseq, "1");
						        	},
						        	"<fmt:message key='cancel' />": function() {
						        		$(this).dialog("close");
						        	}
						      	}
						    });
						});
	    			});
				}
			}
			
			//提示信息显示
			var tishi = function(info) {
				document.getElementById("dialog").innerHTML = info;
				$(function() {
					$("#dialog").dialog({
				    	resizable: false,
				      	height:140,
				      	modal: true,
				      	buttons: {
				      	  	"<fmt:message key='enter' />": function() {
				        		$(this).dialog("close");
				        	}
				      	}
				    });
				});
			} 
			
			//点击返回按钮，重新初始化地图
			function back() {
				$("#listForm").submit();
			}
			
			//随机获取颜色值
			var getRandomColor = function() {
				return '#' + ('00000' + (Math.random() * 0x1000000 << 0).toString(16)).substr(-6).toUpperCase();
			}
		</script>
	</body>
</html>