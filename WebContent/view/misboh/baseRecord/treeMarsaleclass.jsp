<%@ page import="com.choice.orientationSys.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
	String rootId = StringConstant.ROOT_ID;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="food_category" />列表</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
			
			</style>
		</head>
	<body>
		<div class="leftFrame" style="width: 20%;float:left;">
        	<div id="toolbar"></div>
	    	<div class="treePanel" style=" overflow-y: scroll;">
		        <script src="<%=path%>/js/tree/MzTreeView10.js" type="text/javascript"></script>
	        	<script type="text/javascript">
	          		var tree = new MzTreeView("tree");
	          
	          		tree.nodes['0_~'] = 'text:<fmt:message key="all" />;method:changeUrl("0","~","<fmt:message key="all" />")';
	          		<c:forEach var="marsale" items="${listMarsaleclassTree}" varStatus="status">
	          			tree.nodes["${marsale['PARENTID']}_${marsale['CHILDRENID']}"] 
		          		= "text:${marsale['CODE']}-${marsale['NAME']}; method:changeUrl('${marsale['ORDERID']}','${marsale['CHILDRENID']}','${marsale['NAME']}','${marsale['PARENTID']}')";
	          		</c:forEach>
	          		 tree.setIconPath("<%=path%>/image/tree/none/");
	          		document.write(tree.toString());
	        	</script>
	    	</div>
    	</div>
    	<div  class="mainFrame" style="width:80%;" id="dataList">
    		<iframe src="" id="listFrame" frameborder="0" scrolling="no"></iframe>
    	</div>
	    
	    <input type="hidden" id="treeNodeName" name="treeNodeName" />
	    <input type="hidden" id="pk_father" name="pk_father" />
	    <input type="hidden" id="pk_id" name="pk_id" value="${pk_id }" />
		
		<script type="text/javascript" src="<%=path%>/js/system/department.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>	
		<script type="text/javascript">
			function changeUrl(orderid,pk_id,name,father){
				$('#pk_father').val(father);
				//$("#type").val(orderid ? orderid : '0');
				$("#treeNodeName").val(name);
				tree.focus(pk_id);
				loadDataList(orderid,pk_id,father);
				$('#pk_id').val(pk_id);
			}
			//加载右侧数据
			function loadDataList(orderid,pk_id,father){
				var action = "<%=path%>/baseRecord/listPubitems.do";
				if(orderid=='1'){//点击一级类别
					action+= "?pk_vgrptyp="+pk_id;
				}else if(orderid=='2'){//点击二级类别
					action+= "?pk_vgrp="+pk_id+"&pk_vgrptyp="+father;
				}else if(orderid=='3'){//点击三级类别
					action+= "?pk_vtyp="+pk_id+"&pk_vgrp="+father;
				}
				$('#listFrame').attr('src',action);
			}
			
			$(document).ready(function(){
				var expanFlag = true;
				var toolbar = $('#toolbar').toolbar({
					items: [{
							text: '<fmt:message key="expandAll" />',
							title: '<fmt:message key="expandAll"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-80px']
							},
							handler: function(){
								tree.expandAll();
							}
						}
					]
				});
				
				setElementHeight('.treePanel',['#toolbar'],$(document.body),29);//计算.treePanel的高度
				setElementHeight('#dataList',['#toolbar'],$(document.body),0);//计算右侧的高度
				/* tree.focus('${department.id}');*/
				changeUrl('0','${pk_id}',''); //初次加载全部
			});
			
		</script>

	</body>
</html>