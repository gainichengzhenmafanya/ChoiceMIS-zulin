<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="insert"/><fmt:message key="package" /><fmt:message key="set_up_the" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
    	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>	
		<style type="text/css">
			table.gridtable {
				font-family: verdana,arial,sans-serif;
				color:#333333;
				border-width: 1px;
				border-color: #666666;
				border-collapse: collapse;
			}
			table.gridtable th {
				border-width: 1px;
				padding: 6px;
				border-style: solid;
				border-color: #666666;
			}
			table.gridtable td {
				border-width: 1px;
				padding: 6px;
				border-style: solid;
				border-color: #666666;
			}
			.tableTr {
				background-color: #B9D4ED;
				text-align: center;
				border: 1px;
			}
			.tabs-panels {
				width:750px;
			}
		</style> 
	</head>
	<body>
		<div class="tool"></div>
		<div class="form" style="float: left;height: auto;">
		 <form id="pubPackageForm" method="post" action="<%=path %>/pubpackage/savePubPackAge.do" >
		 	<input type="hidden" id="pk_pubpack" name="pk_pubpack" value="${pubPackage.pk_pubpack }" class="text" />
			<div class="easyui-tabs" fit="true" plain="true" id="tabs" style="z-index:88;width:750px;overflow: hidden;height: 415px;">
				<div title="<fmt:message key="basic_information" />">
					<div class="form-line">
						<div class="form-label"><fmt:message key="coding"/>：</div>
						<div class="form-input"><c:out value="${pubPackage.VCODE }" /></div>
						<div class="form-label"><fmt:message key="name"/>：</div>
						<div class="form-input"><c:out value="${pubPackage.VNAME }" /></div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="Package_no" />：</div>
						<div class="form-input"><c:out value="${pubPackage.VPACKCODE }" /></div>
						<div class="form-label"><fmt:message key="abbreviation" />：</div>
						<div class="form-input"><c:out value="${pubPackage.VINIT }" /></div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="package_type" />：</div>
						<div class="form-input"><c:out value="${pubPackage.VGRPTYP }" /></div>
						<div class="form-label">PAD<fmt:message key="category" />：</div>
						<div class="form-input"><c:out value="${pubPackage.VCLASS }" /></div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="enable_state"/>：</div>
						<div class="form-input">
							<c:if test="${pubPackage.ENABLESTATE=='2'}"> <fmt:message key="have_enabled"/></c:if>
							<c:if test="${pubPackage.ENABLESTATE=='1'}"> <fmt:message key="not_enabled"/></c:if>
							<c:if test="${pubPackage.ENABLESTATE=='3'}"> <fmt:message key="stop_enabled"/></c:if>
						</div>
						<div class="form-label"><fmt:message key="no"/>：</div>
						<div class="form-input"><c:out value="${pubPackage.ISORTNO }" /></div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="the_biggest" /><fmt:message key="pubitem" />：</div>
						<div class="form-input"><c:out value="${pubPackage.IMAXFOOD }" /></div>
						<div class="form-label"><fmt:message key="sales" /><fmt:message key="unit" />：</div>
						<div class="form-input"><c:out value="${pubPackage.VSALEUNIT }" /></div>
					</div>
					<div class="form-line">
						<div class="form-label">
							<fmt:message key="package" /><fmt:message key="price" /><fmt:message key="type" />：
						</div>
						<div class="form-input">
							<c:if test="${pubPackage.VPRICETYP==1 }"> <fmt:message key="fixed" /><fmt:message key="price" /> </c:if>
							<c:if test="${pubPackage.VPRICETYP==2 }"> <fmt:message key="collect" /><fmt:message key="price" /> </c:if>
							<c:if test="${pubPackage.VPRICETYP==3 }"> <fmt:message key="high_priority" /><fmt:message key="price" /> </c:if>
						</div>
						<div class="form-label"><fmt:message key="package" /><fmt:message key="break_up_way" />：</div>
						<div class="form-input">
							<c:if test="${pubPackage.VSPLITTYPE==1 }"> <fmt:message key="the_proportion_between" /> </c:if>
							<c:if test="${pubPackage.VSPLITTYPE==2 }"> <fmt:message key="percentage_discount" /> </c:if>
							<c:if test="${pubPackage.VSPLITTYPE==3 }"> <fmt:message key="manual_split" /> </c:if>
						</div>
					</div>
				</div>
				<div title="<fmt:message key="price" /><fmt:message key="information" />">
					<div class="form-line">
						<div class="form-label" ><fmt:message key="Hall_food_prices" />：</div>
						<div class="form-input">
							<fmt:formatNumber value="${pubPackage.NRESTPRICE}" pattern="#,###.00" />
						</div>
						<div class="form-label"><fmt:message key="The_theory_of_cost" />：</div>
						<div class="form-input">
							<fmt:formatNumber value="${pubPackage.NTHEORYPRICE}" pattern="#,###.00" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label" ><fmt:message key="Plus_the_price_of" />：</div>
						<div class="form-input">
							<fmt:formatNumber value="${pubPackage.NOUTPRICE}" pattern="#,###.00" />
						</div>
						<div class="form-label" ><fmt:message key="Other_price" />： </div>
						<div class="form-input">
							<fmt:formatNumber value="${pubPackage.NOTHERPRICE}" pattern="#,###.00" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label">
							<fmt:message key="The_second" /><fmt:message key="unit" /><fmt:message key="Hall_food_prices" />：
						</div>
						<div class="form-input">
							<fmt:formatNumber value="${pubPackage.NSECRESTPRICE}" pattern="#,###.00" />
						</div>
						<div class="form-label">
							<fmt:message key="The_second" /><fmt:message key="unit" /><fmt:message key="Plus_the_price_of" />：
						</div>
						<div class="form-input">
							<fmt:formatNumber value="${pubPackage.NSECOUTPRICE}" pattern="#,###.00" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label">
							<fmt:message key="The_second" /><fmt:message key="unit" /><fmt:message key="Other_price" />：
						</div>
						<div class="form-input">
							<fmt:formatNumber value="${pubPackage.NSECOTHERPRICE}" pattern="#,###.00" />
						</div>
					</div>
				</div>
				<div title="<fmt:message key="other_information" />">
					<div class="form-line">
						<div class="form-label">
							<fmt:message key="whether" /><fmt:message key="allow" /><fmt:message key="update"/><fmt:message key="price" />：
						</div>
						<div class="form-input" >
							<input type="checkbox" style="margin-top:6px;" disabled="disabled" id="bmodifyprice" name="bmodifyprice" <c:if test="${pubPackage.BMODIFYPRICE=='Y'}">checked="checked"</c:if> value="${pubPackage.BMODIFYPRICE }" class="text"/>
						</div>
						<div class="form-label">
							<fmt:message key="whether" /><fmt:message key="can" /><fmt:message key="At_a_discount" />：
						</div>
						<div class="form-input">
							<input type="checkbox" style="margin-top:6px;" disabled="disabled" id="bdiscount" name="bdiscount" <c:if test="${pubPackage.BDISCOUNT=='Y'}">checked="checked"</c:if> value="${pubPackage.BDISCOUNT }" class="text" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label">
							<fmt:message key="whether" /><fmt:message key="free_service_charge" />：
						</div>
						<div class="form-input">
							<input type="checkbox" style="margin-top:6px;" disabled="disabled" id="bcancelfee" name="bcancelfee" <c:if test="${pubPackage.BCANCELFEE=='Y'}">checked="checked"</c:if> value="${pubPackage.BCANCELFEE }" class="text" />
						</div>
						<div class="form-label">
							<fmt:message key="whether" /><fmt:message key="Print_the_post_it_note" />：
						</div>
						<div class="form-input">
							<input type="checkbox" style="margin-top:6px;" disabled="disabled" id="vyprinttimely" name="vyprinttimely" <c:if test="${pubPackage.VYPRINTTIMELY=='Y'}">checked="checked"</c:if> value="${pubPackage.VYPRINTTIMELY }" class="text" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="print_water_single" />：</div>
						<div class="form-input"><c:out value="${pubPackage.IPTBILLOID }" /></div>
						<div class="form-label"><fmt:message key="After_cooking_dishes_to_print_order" />：</div>
						<div class="form-input"><c:out value="${pubPackage.IKTCHDERID }" /></div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="the_font_color"/>：</div>
						<div class="form-input">
							<input type="text" id="vfontcolor" name="vfontcolor" disabled="disabled" value="${pubPackage.VFONTCOLOR }" class="color {pickerClosable:true} text" />
						</div>
						<div class="form-label"><fmt:message key="the_background_color" />：</div>
						<div class="form-input">
							<input type="text" id="vbgcolor" name="vbgcolor" disabled="disabled" value="<c:out value='${pubPackage.VBGCOLOR }' default='#1F8707'/>" class="color {pickerClosable:true} text" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="background_size_(wide)" />：</div>
						<div class="form-input"><c:out value="${pubPackage.IBGSIZEW }" /></div>
						<div class="form-label"><fmt:message key="background_size_(high)" />：</div>
						<div class="form-input"><c:out value="${pubPackage.IBGSIZEH }" /></div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="the_font_size" />：</div>
						<div class="form-input"><c:out value="${pubPackage.IFONTSIZE }" /></div>
						<div class="form-label"><fmt:message key="font_type"/>：</div>
						<div class="form-input"><c:out value="${pubPackage.VFONTTYPE }" /></div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="the_mutex_type" />：</div>
						<div class="form-input"><c:out value="${pubPackage.VMSOBJEC }" /></div>
						<div class="form-label">
							<fmt:message key="additional_items" /><fmt:message key="quantity" />：
						</div>
						<div class="form-input"><c:out value="${pubPackage.IEXTCOUNT }" /></div>
					</div>
					<div class="form-line">
						<div class="form-label">
							<fmt:message key="print_water_single" /><fmt:message key="pubitem" /><fmt:message key="order" />：
						</div>
						<div class="form-input"><c:out value="${pubPackage.IPRINTORDER }" /></div>
						<div class="form-label">
							<fmt:message key="pubitem" /><fmt:message key="unit" /><fmt:message key="The_weight_of_the" />：
						</div>
						<div class="form-input"><c:out value="${pubPackage.NITEMWEIGHT }" /></div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="Safety_stock" />：</div>
						<div class="form-input"><c:out value="${pubPackage.ISAFESTOCK }" /></div>
						<div class="form-label">
							<fmt:message key="sales" /><fmt:message key="pubitem" /><fmt:message key="the_order" />：
						</div>
						<div class="form-input"><c:out value="${pubPackage.NSALFODORDID }" /></div>
					</div>
					<div class="form-line">
						<div class="form-label" style="width:50px;margin-left:60px;"><fmt:message key="insets" />：</div>
						<div class="form-input"><c:out value="${pubPackage.VPICSMALL }" /></div>
						<div class="form-label"><fmt:message key="A_larger_version" />：</div>
						<div class="form-input"><c:out value="${pubPackage.VPICBIG }" /></div>
					</div>
				</div>
				<div title="<fmt:message key="package_details" />">
					<input type="hidden" id="flag" value="detail" />
					<div class="gridDiv">
								<table class="gridtable" cellspacing="0" cellpadding="0">
									<tr class="tableTr">
											<td class="num"><span style="width: 20px;"></span></td>
											<td style="width:40px;">
											</td>
											<td><span style="width:100px;"><fmt:message key="pubitem_code" /></span></td>
											<td><span style="width:100px;"><fmt:message key="pubitem_name" /></span></td>
											<td><span style="width:60px;"><fmt:message key="Replaceable_food" /></span></td>
											<td><span style="width:100px;"><fmt:message key="unit" /><fmt:message key="select1" /></span></td>
											<td><span style="width:60px;"><fmt:message key="unit" /></span></td>
											<td><span style="width:60px;"><fmt:message key="pubitem" /><fmt:message key="unit_price" /></span></td>
											<td><span style="width:60px;"><fmt:message key="Inside_the_package" /><fmt:message key="price" /></span></td>
											<td><span style="width:60px;"><fmt:message key="discount" /></span></td>
											<td><span style="width:60px;"><fmt:message key="the_discount_value" /></span></td>
											<td><span style="width:60px;"><fmt:message key="Minimum_quantity" /></span></td>
											<td><span style="width:60px;"><fmt:message key="The_largest_number" /></span></td>
											<td><span style="width:60px;"><fmt:message key="quantity" /></span></td>
											<td><span style="width:60px;"><fmt:message key="optional1" /><fmt:message key="quantity" /></span></td>
											<td><span style="width:60px;"><fmt:message key="order_no" /></span></td>
										</tr>		
									<c:forEach var="packagedtl" items="${listPackageDtl}" varStatus="status">
										<tr><td class="num"><span style="width: 20px;">${status.index+1}</span></td>
												<td style="width:24px; text-align: center;" >
													<span><input type="checkbox" name="idList" onclick="CheckBoxCheck(this)" id="chk_<c:out value='${packagedtl.PK_PACKAGE}' />" 
<%-- 													<c:if test="${packagedtl.BCHANGE==0}">disabled="disabled"</c:if> --%>
													value="<c:out value='${packagedtl.PK_PACKAGE}' />"/></span>
												</td>
												<td><span style="width:100px;">${packagedtl.PUBITEMCODE}</span></td>
												<td><span style="width:100px;">${packagedtl.PUBITEMNAME}</span></td>
												<td>
													<span style="width:60px;">
														<c:if test="${packagedtl.BCHANGE==1}">
															<fmt:message key="be" />
														</c:if>
														<c:if test="${packagedtl.BCHANGE==0}">
															<fmt:message key="no1" />
														</c:if>
													</span>
												</td>
												<td><span style="width:100px;">
													<c:choose>
														<c:when test="${packagedtl.UNITINDEX=='2'}"><fmt:message key="The_second" /><fmt:message key="unit" /><input type="hidden" value='2' /></c:when>
														<c:when test="${packagedtl.UNITINDEX=='3'}"><fmt:message key="the_third" /><fmt:message key="unit" /><input type="hidden" value='3' /></c:when>
														<c:when test="${packagedtl.UNITINDEX=='4'}"><fmt:message key="the_fourth" /><fmt:message key="unit" /><input type="hidden" value='4' /></c:when>
														<c:when test="${packagedtl.UNITINDEX=='5'}"><fmt:message key="the_fifth" /><fmt:message key="unit" /><input type="hidden" value='5' /></c:when>
														<c:when test="${packagedtl.UNITINDEX=='6'}"><fmt:message key="the_sixth" /><fmt:message key="unit" /><input type="hidden" value='6' /></c:when>
														<c:otherwise><fmt:message key="the_first" /><fmt:message key="unit" /><input type="hidden" value='1' /></c:otherwise>
													</c:choose>
													</span></td>
												<td><span style="width:60px;">${packagedtl.VUNIT}</span></td>
												<td align="right"><span style="width:60px;">
													<fmt:formatNumber value="${packagedtl.NPRICE}" pattern="##0.00" />
													</span></td>
												<td align="right"><span style="width:60px;"><fmt:formatNumber value="${packagedtl.NPACKAGEPRICE}" pattern="##0.00" /></span></td>
												<td align="right"><span style="width:60px;"><fmt:formatNumber value="${packagedtl.NPROPORTION}" pattern="##0.00" /></span></td>
												<td align="right"><span style="width:60px;"><fmt:formatNumber value="${packagedtl.NDISCOUNTPRICE}" pattern="##0.00" /></span></td>
												<td align="right"><span style="width:60px;"><fmt:formatNumber value="${packagedtl.IMINCOUNT}" pattern="##0.00" /></span></td>
												<td align="right"><span style="width:60px;"><fmt:formatNumber value="${packagedtl.IMAXCOUNT}" pattern="##0.00" /></span></td>
												<td align="right"><span style="width:60px;"><fmt:formatNumber value="${packagedtl.ICNT}" pattern="##0.00" /></span></td>
												<td align="right"><span style="width:60px;"><fmt:formatNumber value="${packagedtl.IMAXGCNT}" pattern="##0.00" /></span></td>
												<td align="right"><span style="width:60px;">${packagedtl.ISORTNO}</span></td>
											</tr>
									</c:forEach>
							</table>
						</div>
					</div>
				</div>
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>		
	 	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/colorpicker/jscolor.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>
		<script type="text/javascript">
			//双击弹出<fmt:message key="update" />
			$('.gridDiv').find('.gridtable').find('tr').live("dblclick", function () {
				$(":checkbox").attr("checked", false);
				$(this).find(':checkbox').attr("checked", true);
				listItemPkg();
				if($(this).hasClass("show-firm-row"))return;
				$('.grid').find('.table-body').find('tr').removeClass("show-firm-row");
				$(this).addClass("show-firm-row");
			 });
			
			//实现复选框单选
			function CheckBoxCheck(chk){
			    var obj = $('.gridDiv').find('.gridtable').find('input[name=idList]');
			    for(var i = 0; i < obj.length; i++){
			        if(obj[i].type == "checkbox"){
			            obj[i].checked = false;
			        }
			    }
			    chk.checked=true;
			}
			
			//显示可换菜明细
			function listItemPkg(){
				var tab = $("#tabs").tabs("getSelected");
				var flag = tab.find("#flag").val();
				var checkboxList = $('.gridDiv').find('.gridtable').find('input[name=idList]');
				
				if ("detail" != flag) {
					alert('<fmt:message key="misboh_only_detail" />！');
					return true;
				} else {
				
					if(checkboxList && checkboxList.filter(':checked').size() == 1){
						
						var chkValue = checkboxList.filter(':checked').eq(0).val();
						if(chkValue=='on' || chkValue=="") {
							alert('<fmt:message key="choose_package_details_is_not_saved" />！');
							return;
						}
						
						inputObject=checkboxList.filter(':checked').eq(0).closest('tr').find('td').eq(3).find('span').find('input');
						$.post("<%=path%>/baseRecord/findPackageDtlById.do?pk_package="+chkValue,null,function(data){
							 ndiscountprice = data['packageDtl'].NDISCOUNTPRICE;
							 pubitemcode = data['packageDtl'].PUBITEMCODE;
							 pubitemname = data['packageDtl'].PUBITEMNAME;
							 vunitdes = data['packageDtl'].VUNIT == 'undefined'? '': data['packageDtl'].VUNIT;
							 nprice = data['packageDtl'].NPRICE;
							 imaxcnt = data['packageDtl'].IMAXCOUNT;
							 npackageprice = data['packageDtl'].NPACKAGEPRICE;
							 vpk_pubpack = data['packageDtl'].PK_PUBPACK;
							 vpk_package = data['packageDtl'].PK_PACKAGE;
							 vpk_pubitem = data['packageDtl'].PK_PUBITEM;
							 bchange = data['packageDtl'].BCHANGE;
							 unit_index = data['packageDtl'].UNITINDEX;
							 unit_code = data['packageDtl'].UNITCODE;
							var pk_pubpack = $("#pk_pubpack").val();
							if (0 == bchange) {
								alert('<fmt:message key="there_is_no" /><fmt:message key="Replaceable_food" />！');
								return;
							}
							$('body').window({
								title: '<span class="blue" style="font-size: 15px;"><fmt:message key="Replaceable_food" /><fmt:message key="coding" />:'+pubitemcode+'&nbsp;&nbsp;<fmt:message key="Replaceable_food" /><fmt:message key="name" />：'+ pubitemname+'</span>',
								content: '<iframe id="updatePubPackageFrame" frameborder="0" src="<%=path%>/baseRecord/listItemPkg.do?pk_package='+chkValue+'&pk_pubpack='+pk_pubpack+'&ndiscountprice='+ndiscountprice+'&vpk_pubitem='+vpk_pubitem+'"></iframe>',
								width: '740px',
								height: '430px',
								draggable: true,
								isModal: true
							});
						});	
					}else if(checkboxList && checkboxList.filter(':checked').size() > 1) {
						alert('<fmt:message key="you_can_only_view_a_data" />！');
						return;
					}else {
						alert('<fmt:message key="please_select_to_see" />！');
						return ;
					}
				}
			}
		</script>
	</body>
</html>