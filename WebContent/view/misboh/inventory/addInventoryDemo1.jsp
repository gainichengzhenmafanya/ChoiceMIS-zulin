<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="purchase_the_template" /></title>
<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
<style type="text/css">
.blueBGColor{background-color:	#F1F1F1;}
.redBGColor{background-color:	#F1F1F1;}
.onEdit{
	background:lightblue;
	border:1px solid;
	border-bottom-color: blue;
	border-top-color: blue;
	border-left-color: blue;
	border-right-color: blue;
}
.input{
	background:transparent;
	border:1px solid;
}
.redClass{
	color:red;
}
</style>		
</head>
<body>
	<div class="tool"></div>
	<form id="listForm" action="<%=path%>/inventoryMis/addInventoryDemo1.do" method="post">
		<div class="form-line">	
			<div class="form-label"><fmt:message key="title" /></div>
			<div class="form-input">
				<input type="hidden" id="status" name="status" value="${status}"/>
				<input type="hidden" id="firm" name="firm" value="${firm}"/>
				<select class="select" id="chkstodemono" name="chkstodemono" onchange="findByTitle(this);">
					<c:forEach var="chkstodemo" items="${listTitle}" varStatus="status">
						<option
							<c:if test="${chkstodemo.chkstodemono==chkstodemono}"> selected="selected" </c:if> value="${chkstodemo.chkstodemono}">${chkstodemo.title}
						</option>
					</c:forEach>				
				</select>
			</div>
		</div>
	
		<div class="grid">		
			<div class="table-head" >
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<td class="num" rowspan="2"><span style="width: 30px;">&nbsp;</span></td>
							<td rowspan="2"><span style="width:70px;"><fmt:message key="supplies_code"/></span></td>
							<td rowspan="2"><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
							<td rowspan="2"><span style="width:80px;"><fmt:message key="supplies_specifications"/></span></td>
							<td colspan="8"><span><fmt:message key="quantity"/></span></td>
							<td rowspan="2"><span style="width:60px;"><fmt:message key="scm_pandian"/><fmt:message key="quantity"/></span></td>
							<td rowspan="2"><span style="width:40px;"><fmt:message key="scm_pandian"/><br/><fmt:message key="unit"/></span></td>
						</tr>
						<tr>
							<td><span style="width:60px;"><fmt:message key="quantity"/></span></td>
							<td><span style="width:40px;"><fmt:message key="specifications"/><fmt:message key="scm_one"/></span></td>
							<td><span style="width:60px;"><fmt:message key="quantity"/></span></td>
							<td><span style="width:40px;"><fmt:message key="specifications"/><fmt:message key="scm_two"/></span></td>
							<td><span style="width:60px;"><fmt:message key="quantity"/></span></td>
							<td><span style="width:40px;"><fmt:message key="specifications"/><fmt:message key="scm_three"/></span></td>
							<td><span style="width:60px;"><fmt:message key="quantity"/></span></td>
							<td><span style="width:40px;"><fmt:message key="specifications"/><fmt:message key="scm_four"/></span></td>
						</tr>
					</thead>
				</table>
			</div>				
			<div class="table-body">
				<table cellspacing="0" cellpadding="0">
					<tbody>
						<c:forEach var="inventory" items="${chkstodemoList}" varStatus="status">
							<tr data-unitper1="${inventory.unitper1}" data-unitper2="${inventory.unitper2}"
								data-unitper3="${inventory.unitper3}" data-unitper4="${inventory.unitper4}" data-ex="${inventory.ex}">
								<td><span title="${status.index+1}" style="width:30px;">${status.index+1}</span></td>
								<td><span title="${inventory.sp_code}" style="width:70px;" data-sp_name="${inventory.sp_name}">${inventory.sp_code}</span></td>
								<td><span title="${inventory.sp_name}" style="width:100px;">${inventory.sp_name}</span></td>
								<td><span title="${inventory.sp_desc}" style="width:80px;">${inventory.sp_desc}</span></td>
								<td><span title="${inventory.cnt1}" style="width:60px;text-align: right;"><fmt:formatNumber value="${inventory.cnt1}" pattern="##.##" minFractionDigits="2"/></span></td>
								<td><span title="${inventory.spec1}" style="width:40px;">${inventory.spec1}</span></td>
								<td>
									<span title="${inventory.cnt2}" style="width:60px;text-align: right;">
										<c:if test="${!empty inventory.spec2 }">
											<fmt:formatNumber value="${inventory.cnt2}" pattern="##.##" minFractionDigits="2"/>
										</c:if>
									</span>
								</td>
								<td><span title="${inventory.spec2}" style="width:40px;">${inventory.spec2}</span></td>
								<td>
									<span title="${inventory.cnt3}" style="width:60px;text-align: right;">
										<c:if test="${!empty inventory.spec3 }">
											<fmt:formatNumber value="${inventory.cnt3}" pattern="##.##" minFractionDigits="2"/>
										</c:if>
									</span>
								</td>
								<td><span title="${inventory.spec3}" style="width:40px;">${inventory.spec3}</span></td>
								<td>
									<span title="${inventory.cnt4}" style="width:60px;text-align: right;">
										<c:if test="${!empty inventory.spec4 }">
											<fmt:formatNumber value="${inventory.cnt4}" pattern="##.##" minFractionDigits="2"/>
										</c:if>
									</span>
								</td>
								<td><span title="${inventory.spec4}" style="width:40px;">${inventory.spec4}</span></td>
								<td><span title="${inventory.stockcnt}" style="width:60px;text-align: right;"><fmt:formatNumber value="${inventory.stockcnt}" pattern="##.##" minFractionDigits="2"/></span></td>
								<td><span title="${inventory.unit}" style="width:40px;">${inventory.unit}</span></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</form>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
	<script type="text/javascript">
	//ajax同步设置
	$.ajaxSetup({
		async: false
	});
	var validate;
	//工具栏
	$(document).ready(function(){
		//按钮快捷键
		focus() ;//页面获得焦点
		
		//屏蔽鼠标右键
	 	$(document).bind('keyup',function(e){
	 		if(e.keyCode==27){
	 			parent.$('.close').click();
	 		}
	 		//表格数量一变，校验一下，价格就接着变
	 		if($(e.srcElement).is("input")){//对表格内的输入框进行判读，延迟600毫秒
	 			var index=$(e.srcElement).closest('td').index();
	 			if($(e.srcElement).is("input")){
		    		validator($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
		    	}
	    	}
		});
	 	
	   $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
	   $('.grid').find('.table-body').find('tr').hover(
			function(){
				$(this).addClass('tr-over');
			},
			function(){
				$(this).removeClass('tr-over');
			}
		);
		
		//自动实现滚动条
		setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
		setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
		loadGrid();//  自动计算滚动条的js方法	
		editCells();
		$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),4);
	});
	
	//动态下拉列表框
	function findByTitle(e){
		$('#listForm').submit();
	}
	//确认修改
	function enterUpdate() {
		var checkboxList = $('.grid').find('.table-body').find('tr');
		if(checkboxList.size() < 1){
			alert('<fmt:message key="no_data"/>。');
			return;
		}
		if(parent.$(".table-body").find('tr').size() > 0){
			alert('<fmt:message key="Select_template_will_only_take_the_material_of_this_template"/>！');
			parent.$(".table-body").find('tr').remove();
		}
		parent.editCells();
		parent.loadToolBar([false,false,false,true]);
		$('#wait').show();
		$('#wait2').show();
		checkboxList.each(function(i){
			if(parent.$(".table-body").find("tr:last").find("td:eq(1)").find('span').text() != ''){
				parent.$(".table-body").autoGrid.addRow();
			}
			var unitper1 = $(this).data('unitper1');
			var unitper2 = $(this).data('unitper2');
			var unitper3 = $(this).data('unitper3');
			var unitper4 = $(this).data('unitper4');
			var cnt1 = Number($(this).find('td:eq(4) span').attr('title'));
			var cnt2 = $(this).find('td:eq(7) span').text()?Number($(this).find('td:eq(6) span').attr('title')):0;
			var cnt2t = $(this).find('td:eq(7) span').text()?Number(cnt2).toFixed(2):'';
			var cnt3 = $(this).find('td:eq(9) span').text()?Number($(this).find('td:eq(8) span').attr('title')):0;
			var cnt3t = $(this).find('td:eq(9) span').text()?Number(cnt3).toFixed(2):'';
			var cnt4 = $(this).find('td:eq(11) span').text()?Number($(this).find('td:eq(10) span').attr('title')):0;
			var cnt4t = $(this).find('td:eq(11) span').text()?Number(cnt4).toFixed(2):'';
			var stockcnt = Number($(this).find('td:eq(12) span').attr('title'));
			parent.$(".table-body").find("tr:last").find("td:eq(0)").find('span').attr('style','width:30px;');
			parent.$(".table-body").find("tr:last").find("td:eq(1)").find('span').text($(this).find('td:eq(1) span').text()).data('sp_name',$(this).find('td:eq(2) span').text());
			parent.$(".table-body").find("tr:last").find("td:eq(2)").find('span').text($(this).find('td:eq(2) span').text());
			parent.$(".table-body").find("tr:last").find("td:eq(3)").find('span').text($(this).find('td:eq(3) span').text());
			parent.$(".table-body").find("tr:last").find("td:eq(4)").find('span').text(Number(cnt1).toFixed(2)).attr('title',cnt1).css("text-align","right");
			parent.$(".table-body").find("tr:last").find("td:eq(5)").find('span').text($(this).find('td:eq(5)').text());
			parent.$(".table-body").find("tr:last").find("td:eq(6)").find('span').text(cnt2t).attr('title',cnt2).css("text-align","right");
			parent.$(".table-body").find("tr:last").find("td:eq(7)").find('span').text($(this).find('td:eq(7)').text());
			parent.$(".table-body").find("tr:last").find("td:eq(8)").find('span').text(cnt3t).attr('title',cnt3).css("text-align","right");
			parent.$(".table-body").find("tr:last").find("td:eq(9)").find('span').text($(this).find('td:eq(9)').text());
			parent.$(".table-body").find("tr:last").find("td:eq(10)").find('span').text(cnt4t).attr('title',cnt4).css("text-align","right");
			parent.$(".table-body").find("tr:last").find("td:eq(11)").find('span').text($(this).find('td:eq(11)').text());
			parent.$(".table-body").find("tr:last").find("td:eq(12)").find('span').text(Number(stockcnt).toFixed(2)).attr('title',stockcnt).css("text-align","right");
			parent.$(".table-body").find("tr:last").find("td:eq(13)").find('span').text($(this).find('td:eq(13)').text());
			parent.$(".table-body").find("tr:last").find("td:eq(14)").find('span').text($(this).data('ex') == 'Y'?'是':'否');
			parent.$(".table-body").find("tr:last").find("td:eq(15)").find('span').text(i+1);
// 			var html = "<select style='width:40px;height:18px;'><option value='Y'>否</option><option value='N'>是</option></select>";
// 			parent.$(".table-body").find("tr:last").find("td:eq(16)").find('span').html(html);
			parent.$(".table-body").find("tr:last").find("td:eq(16)").find('span').html('<input type="checkbox"/>').css("text-align","center");
			parent.$(".table-body").find("tr:last").find("td:eq(17)").find('span').html('<input type="checkbox"/>').css("text-align","center");
			parent.$(".table-body").find("tr:last").data("unitper1",unitper1);
			parent.$(".table-body").find("tr:last").data("unitper2",unitper2);
			parent.$(".table-body").find("tr:last").data("unitper3",unitper3);
			parent.$(".table-body").find("tr:last").data("unitper4",unitper4);
			if(Number(stockcnt)>0){
				parent.$(".table-body").find("tr:last").addClass('redClass');
			}
		});
		$('#wait').hide();
		$('#wait2').hide();
		parent.$('#fromtyp').val('template');//来自模板
	    //关闭弹出窗口
		parent.$('.close').click();
	}
		
	//编辑表格
		function editCells() {
			if($(".table-body").find('tr').length == 0){
				return;
			}
			$(".table-body").autoGrid({
				initRow:1,
				colPerRow:14,
				widths:[40,80,110,90,70,50,70,50,70,50,70,50,70,50],
				colStyle:['','',{background:"#F1F1F1"},'',{background:"#F1F1F1"},'',{background:"#F1F1F1"},'',{background:"#F1F1F1"},'',{background:"#F1F1F1"},'','',''],
				VerifyEdit:{verify:true,enable:function(cell,row){
					return row.find('td').index(cell) == 2 || (row.find("td:eq(2)").text() == null || $.trim(row.find("td:eq(2)").text()) != '' ) ? true : false;
				}},
				onEdit:$.noop,
				editable:[4,6,8,10],
				onLastClick:$.noop,
				onEnter:function(data){
					$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.ovalue) ;
				},
				cellAction:[{
					index:4,
					action:function(row,data2){
						if(Number(data2.value) < 0){
							alert('<fmt:message key="number_cannot_be_negative"/>！');
							row.find("td:eq(4)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,4);
						}else if(isNaN(data2.value)){
							alert('<fmt:message key="number_be_not_number"/>！');
							row.find("td:eq(4)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,4);
						}else{
							row.find("td:eq(4)").find('span').attr('title',data2.value);
							getSumCnt(row);
							if(row.find("td:eq(7) span").text() == ''){
								$.fn.autoGrid.setCellEditable(row.next(),4);
							}else{
								$.fn.autoGrid.setCellEditable(row,6);
							}
						}
					}
				},{
					index:6,
					action:function(row,data2){
						if(Number(data2.value) < 0){
							alert('<fmt:message key="number_cannot_be_negative"/>！');
							row.find("td:eq(6)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,6);
						}else if(isNaN(data2.value)){
							alert('<fmt:message key="number_be_not_number"/>！');
							row.find("td:eq(6)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,6);
						}else{
							row.find("td:eq(6)").find('span').attr('title',data2.value);
							//如果转换率是0的并且规格一有数量的，这里则必输
							if(Number(row.data('unitper2')) == 0 && Number(row.find("td:eq(4)").find('span').text()) != 0){
								if(data2.value == 0){
									alert('<fmt:message key="There_is_no_conversion_specification_will_not_null"/>！');//没有转换率的规格是必输项
									$.fn.autoGrid.setCellEditable(row,6);
									return;
								}
							}
							getSumCnt(row);
							if(row.find("td:eq(9) span").text() == ''){
								$.fn.autoGrid.setCellEditable(row.next(),4);
							}else{
								$.fn.autoGrid.setCellEditable(row,8);
							}
						}
					}
				},{
					index:8,
					action:function(row,data2){
						if(Number(data2.value) < 0){
							alert('<fmt:message key="number_cannot_be_negative"/>！');
							row.find("td:eq(8)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,8);
						}else if(isNaN(data2.value)){
							alert('<fmt:message key="number_be_not_number"/>！');
							row.find("td:eq(8)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,8);
						}else{
							row.find("td:eq(8)").find('span').attr('title',data2.value);
							//如果转换率是0的并且规格一有数量的，这里则必输
							if(Number(row.data('unitper3')) == 0 && Number(row.find("td:eq(4)").find('span').text()) != 0){
								if(data2.value == 0){
									alert('<fmt:message key="There_is_no_conversion_specification_will_not_null"/>！');//没有转换率的规格是必输项
									$.fn.autoGrid.setCellEditable(row,8);
									return;
								}
							}
							getSumCnt(row);
							if(row.find("td:eq(11) span").text() == ''){
								$.fn.autoGrid.setCellEditable(row.next(),4);
							}else{
								$.fn.autoGrid.setCellEditable(row,10);
							}
						}
					}
				},{
					index:10,
					action:function(row,data2){
						if(Number(data2.value) < 0){
							alert('<fmt:message key="number_cannot_be_negative"/>！');
							row.find("td:eq(10)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,10);
						}else if(isNaN(data2.value)){
							alert('<fmt:message key="number_be_not_number"/>！');
							row.find("td:eq(10)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,10);
						}else{
							row.find("td:eq(10)").find('span').attr('title',data2.value);
							//如果转换率是0的并且规格一有数量的，这里则必输
							if(Number(row.data('unitper4')) == 0 && Number(row.find("td:eq(4)").find('span').text()) != 0){
								if(data2.value == 0){
									alert('<fmt:message key="There_is_no_conversion_specification_will_not_null"/>！');//没有转换率的规格是必输项
									$.fn.autoGrid.setCellEditable(row,10);
									return;
								}
							}
							getSumCnt(row);
							$.fn.autoGrid.setCellEditable(row.next(),4);
						}
					}
				}]
			});
		}
		
		//得到总数量的方法
	  function getSumCnt(row){
		  var sumCnt = Number(row.find("td:eq(4) span").attr('title'));
		  if(row.find("td:eq(7) span").text() != '' && Number(row.data('unitper2')) != 0){
			  sumCnt += Number(row.find("td:eq(6) span").attr('title'))/Number(row.data('unitper2'));
		  }
		  if(row.find("td:eq(9) span").text() != '' && Number(row.data('unitper3')) != 0){
			  sumCnt += Number(row.find("td:eq(8) span").attr('title'))/Number(row.data('unitper3'));
		  }
		  if(row.find("td:eq(11) span").text() != '' && Number(row.data('unitper4')) != 0){
			  sumCnt += Number(row.find("td:eq(10) span").attr('title'))/Number(row.data('unitper4'));
		  }
		  row.find("td:eq(12) span").text(sumCnt.toFixed(2)).attr('title',sumCnt).css('text-align','right');
		  if(sumCnt != 0){
			  row.addClass('redClass');
		  }else{
			  row.removeClass('redClass');
		  }
	  }
	
		//校验
	  function validator(weiz,row,data){
		  row.find("input").data("ovalue",data.value);
			if(weiz=="4"){
				if(Number(data.value) < 0){
					alert('<fmt:message key="number_cannot_be_negative"/>！');
					row.find("td:eq(4)").find('span').text(data.ovalue);
					$.fn.autoGrid.setCellEditable(row,4);
				}else if(isNaN(data.value)){
					alert('<fmt:message key="number_be_not_number"/>！');
					row.find("td:eq(4)").find('span').text(data.ovalue);
					$.fn.autoGrid.setCellEditable(row,4);
				}else{
					row.find("td:eq(4)").find('span').attr('title',data.value);
					getSumCnt(row);
				}
			}else if(weiz=="6"){
				if(row.find("td:eq(7) span").text() != ''){
					if(Number(data.value) < 0){
						alert('<fmt:message key="number_cannot_be_negative"/>！');
						row.find("td:eq(6)").find('span').text(data.ovalue);
						$.fn.autoGrid.setCellEditable(row,6);
					}else if(isNaN(data.value)){
						alert('<fmt:message key="number_be_not_number"/>！');
						row.find("td:eq(6)").find('span').text(data.ovalue);
						$.fn.autoGrid.setCellEditable(row,6);
					}else{
						row.find("td:eq(6)").find('span').attr('title',data.value);
						getSumCnt(row);
					}
				}else{
					row.find("td:eq(6)").find('span').text(data.ovalue);
					$.fn.autoGrid.setCellEditable(row.next(),4);
				}
			}else if(weiz=="8"){
				if(row.find("td:eq(9) span").text() != ''){
					if(Number(data.value) < 0){
						alert('<fmt:message key="number_cannot_be_negative"/>！');
						row.find("td:eq(8)").find('span').text(data.ovalue);
						$.fn.autoGrid.setCellEditable(row,8);
					}else if(isNaN(data.value)){
						alert('<fmt:message key="number_be_not_number"/>！');
						row.find("td:eq(8)").find('span').text(data.ovalue);
						$.fn.autoGrid.setCellEditable(row,8);
					}else{
						row.find("td:eq(8)").find('span').attr('title',data.value);
						getSumCnt(row);
					}
				}else{
					row.find("td:eq(8)").find('span').text(data.ovalue);
					$.fn.autoGrid.setCellEditable(row.next(),4);
				}
			}else if(weiz=="10"){
				if(row.find("td:eq(11) span").text() != ''){
					if(Number(data.value) < 0){
						alert('<fmt:message key="number_cannot_be_negative"/>！');
						row.find("td:eq(10)").find('span').text(data.ovalue);
						$.fn.autoGrid.setCellEditable(row,10);
					}else if(isNaN(data.value)){
						alert('<fmt:message key="number_be_not_number"/>！');
						row.find("td:eq(10)").find('span').text(data.ovalue);
						$.fn.autoGrid.setCellEditable(row,10);
					}else{
						row.find("td:eq(10)").find('span').attr('title',data.value);
						getSumCnt(row);
					}
				}else{
					row.find("td:eq(10)").find('span').text(data.ovalue);
					$.fn.autoGrid.setCellEditable(row.next(),4);
				}
			}
		}

	</script>
</body>
</html>