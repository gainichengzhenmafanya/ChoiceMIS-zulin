<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>月结</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/view/tableMain.css" />
			<style type="text/css">
				#myRemind li{
					font-size:15px;
				}
				#wait4 { 
					filter:alpha(opacity=80);
					-moz-opacity:0.8;
					-khtml-opacity: 0.8;
					opacity: 0.8;
					background-color:black;
					width:100%;
					height:100%;
					position:absolute;
					z-index:998;
					top:0px;
					left:0px;
				}			
				#wait3 { 
					position:absolute; 
					top:40%; 
					left:40%; 
					margin-left:-80px; 
					margin-top:-30px; 
					z-index:999;
				}
			</style>
		</head>
	<body>
		<input type="hidden" id="isControPdYj" value="${isControPdYj }"/>
		<input type="hidden" id="month" value="${month }"/>
		<input type="hidden" id="nextMonth" value="${nextMonth }"/>
		<form id="listForm" action="<%=path%>/monthEndMis/tableMonthEnd.do" method="post">
			<input type="hidden" id="action" name="action" value="${action }"/>
		</form>
		<div class="tool"></div>
		<div class="bj_head">
			<div class="form-line">
				<div class="form-label"><fmt:message key="MonthClose_from" />${month }<fmt:message key="month" />:</div>
				<div class="form-input" style="width:200px;">
					<input type="text" id="bdate" class="Wdate text" value="${bdate }" readonly="readonly" style="width:100px;"/>-
					<input type="text" id="edate" class="Wdate text" value="${edate }" readonly="readonly" style="width:100px;"/>
				</div>
				<div class="form-label"><fmt:message key="MonthClose_to" />${nextMonth }<fmt:message key="month" />:</div>
				<div class="form-input" style="width:300px;">
					<input type="text" id="nextBdate" class="Wdate text" value="${nextBdate }" readonly="readonly" style="width:100px;"/>-
					<input type="text" id="nextEdate" class="Wdate text" value="${nextEdate }" readonly="readonly" style="width:100px;"/>
				</div>
			</div>
		</div>
		<div class="module" style="width:98%;padding-bottom:10px;">
			<h4 class="moduleHeader">
			    <span class="title"><fmt:message key="MonthClose_reminder" /></span>
			</h4>
			<div>
				<c:choose>
					<c:when test="${empty action }">
						<div style="font-size:30px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;提示：进行月结之前，请先点击查询。</div>
					</c:when>
					<c:otherwise>
						<ul id="myRemind" style="height:100%;padding:5px 0px;">
							<li><fmt:message key="inspection" /><!-- 验货 -->：</li>
							<c:choose>
								<c:when test="${direCount == 0 && outCount == 0 && dbCount == 0 }">
									<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="In_the_month_you_have_no_inspection_documents" /><!-- 当月您没有未验货的单据 -->。</li>
								</c:when>
								<c:otherwise>
									<input type="hidden" id="tishi" value="1"/>
									<c:if test="${direCount != 0 }">
										<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="In_the_month_you_have" /><!-- 当月您有 -->
											<span style="cursor: pointer;color: red;" onclick="openUnChecked('dire')">${direCount }</span>
											<fmt:message key="dire_goods_without_inspection" /><!-- 条直配物资未验货 -->！
											&nbsp;&nbsp;&nbsp;&nbsp;<span style="cursor: pointer;color: red;" onclick="openUnChecked('dire')"><fmt:message key="Clicktoview"/>>></span>
										</li>
									</c:if>
									<c:if test="${outCount != 0 }">
										<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="In_the_month_you_have" /><!-- 当月您有 -->
											<span style="cursor: pointer;color: red;" onclick="openUnChecked('out')">${outCount }</span>
											<fmt:message key="out_goods_without_inspection" /><!-- 条配送单据未验货 -->！
											&nbsp;&nbsp;&nbsp;&nbsp;<span style="cursor: pointer;color: red;" onclick="openUnChecked('out')"><fmt:message key="Clicktoview"/>>></span>
										</li>
									</c:if>
									<c:if test="${dbCount != 0 }">
										<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="In_the_month_you_have" /><!-- 当月您有 -->
											<span style="cursor: pointer;color: red;" onclick="openUnChecked('db')">${dbCount }</span>
											<fmt:message key="transfer_goods_without_inspection" /><!-- 条调拨物资未验货 -->！
											&nbsp;&nbsp;&nbsp;&nbsp;<span style="cursor: pointer;color: red;" onclick="openUnChecked('db')"><fmt:message key="Clicktoview"/>>></span>
										</li>
									</c:if>
								</c:otherwise>
							</c:choose>
							<li><fmt:message key="In_out_order" /><!-- 出入库 -->：</li>
							<c:choose>
								<c:when test="${chkinmCount == 0 && chkinmzfCount == 0 && chkoutmCount == 0 && chkoutmdbCount == 0}">
									<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="In_the_month_you_have_no_uncheck_inout_orders" /><!-- 当月您没有未审核的出入库单据 -->。</li>
								</c:when>
								<c:otherwise>
									<input type="hidden" id="tishi" value="1"/>
									<c:if test="${chkinmCount != 0 }">
										<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="In_the_month_you_have" /><!-- 当月您有 -->
											<span style="cursor: pointer;color: red;" onclick="openUnChecked('chkinm')">${chkinmCount }</span>
											<fmt:message key="article"/><fmt:message key="In_orders"/><fmt:message key="unchecked"/>！
											&nbsp;&nbsp;&nbsp;&nbsp;<span style="cursor: pointer;color: red;" onclick="openUnChecked('chkinm')"><fmt:message key="Clicktoview"/>>></span>
										</li>
									</c:if>
									<c:if test="${chkinmzfCount != 0 }">
										<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="In_the_month_you_have" /><!-- 当月您有 -->
											<span style="cursor: pointer;color: red;" onclick="openUnChecked('chkinmzf')">${chkinmzfCount }</span>
											<fmt:message key="article"/><fmt:message key="Inout_order"/><fmt:message key="unchecked"/>！
											&nbsp;&nbsp;&nbsp;&nbsp;<span style="cursor: pointer;color: red;" onclick="openUnChecked('chkinmzf')"><fmt:message key="Clicktoview"/>>></span>
										</li>
									</c:if>
									<c:if test="${chkoutmCount != 0 }">
										<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="In_the_month_you_have" /><!-- 当月您有 -->
											<span style="cursor: pointer;color: red;" onclick="openUnChecked('chkoutm')">${chkoutmCount }</span>
											<fmt:message key="article"/><fmt:message key="Out_orders"/><fmt:message key="unchecked"/>！
											&nbsp;&nbsp;&nbsp;&nbsp;<span style="cursor: pointer;color: red;" onclick="openUnChecked('chkoutm')"><fmt:message key="Clicktoview"/>>></span>
										</li>
									</c:if>
									<c:if test="${chkoutmdbCount != 0 }">
										<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="In_the_month_you_have" /><!-- 当月您有 -->
											<span style="cursor: pointer;color: red;" onclick="openUnChecked('chkoutmdb')">${chkoutmdbCount }</span>
											<fmt:message key="article"/><fmt:message key="Transfer_order"/><fmt:message key="unchecked"/>！
											&nbsp;&nbsp;&nbsp;&nbsp;<span style="cursor: pointer;color: red;" onclick="openUnChecked('chkoutmdb')"><fmt:message key="Clicktoview"/>>></span>
										</li>
									</c:if>
								</c:otherwise>
							</c:choose>
							<li><fmt:message key="scm_pandian"/><!-- 盘点 -->：</li>
							<c:choose>
								<c:when test="${empty unInventoryPositn }">
									<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="In_the_monthEnd_all_positions_have_inventoried" /><!-- 当月最后一天所有仓位都已盘点 -->。</li>
								</c:when>
								<c:otherwise>
									<input type="hidden" id="tishi" value="1"/>
									<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="In_the_monthEnd_these_positions" /><fmt:message key="have_no_inventoried" /><!-- 当月最后一天您有以下仓位 未进行盘点 -->：<br/>
										<table style="width:350px;margin-left:50px;">
											<tr>
											<c:forEach var="positn" items="${unInventoryPositn}" varStatus="status">
												<td style="cursor: pointer;color:red;">${positn.code},${positn.des }</td>
												<c:if test="${(status.index + 1) % 2 == 0 }"></tr><tr></c:if>
											</c:forEach>
										</table>
<%-- 										<c:forEach var="positn" items="${unInventoryPositn}"> --%>
<%-- 											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="cursor: pointer;color: red;">${positn.code},${positn.des }</span><br/> --%>
<%-- 										</c:forEach> --%>
									</li>
								</c:otherwise>
							</c:choose>
						</ul>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
		<div id="wait4" style="display:none;"></div> 
		<div id="wait3" style="display:none;color:white;font-size:15px;"><img src="../image/loading_detail.gif" />&nbsp;
		<span id="endTime">系统正在进行月结之前的检查，请耐心等待！LOADING...</span><br/>
		<div id="p" class="easyui-progressbar" style="margin:0 auto;width: 400px;"></div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		
		<script type="text/javascript">
			
			//判断是不是第13个月了
			if($('#month').val() == 13){
				alert('<fmt:message key="The_current_month_can_not_continue_to_close_the_month_need_to_close_the_year_please_contact_the_center" />！');//当前月份不能继续月结，需要年结，请联系总部
				invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
			}
		
			//ajax同步设置
			$.ajaxSetup({
				async: false
			});
			$(document).ready(function(){
				if($('#action').val() != 'select'){
					loadToolBar(false);
				}else{
					loadToolBar(true);
				}
			});
			
			//控制按钮显示
			function loadToolBar(b){
				$('.tool').html('');
				$('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								$('#action').val('select');
								$('#listForm').submit();
								$('#wait').hide();
								$('#wait2').hide();
								$('#wait3').show();
								$('#wait4').show();
								timerId = window.setInterval(getForm,500);
// 								$('#wait').css('left','18%').find('span').text('系统正在检查当月是否有未验货或未审核的单据，以及是否有未盘点的仓位，检查过程可能需要几分钟，请您耐心等待！LOADING...');
// 								$("#wait2").show();
// 								$("#wait").show();
							}
						},{
							text: '<fmt:message key="MonthClose" />',
							title: '<fmt:message key="MonthClose" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')} && b,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								var isControPdYj = $('#isControPdYj').val();
								if($('#tishi').val() == 1){
									if(isControPdYj == 'Y'){
										alert('<fmt:message key="Please_check_the_MonthClose_reminder_you_can_not_continue_to_close_the_month" />！');//请检查月结提醒！不能进行月结
										return;
									}else{
										if(!confirm('警告！！！请检查月结提醒，您有未完成的操作，继续月结会对系统数据产生影响！！！确定继续进行月结吗（此操作不可逆）？')){
											return;
										}
									}
								}
								if(!confirm('<fmt:message key="Prudent_operation_Are_you_sure_you_have_a_monthly_closing_operation_for_the_store" />？')){//谨慎操作！确定进行门店月结操作吗
									return;
								}
								$.ajax({
									url:'<%=path %>/monthEndMis/checkMonthEnd.do?monthh='+$("#nextMonth").val(),
									type:'post',
									success:function(msg){
										alert('<fmt:message key="Month_close_successful_please_relogin" />！');//月结成功，请重新登录
										top.location="<%=path%>/login/loginOut.do";//添加自动跳转重新登录功能
									}
								});
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
							}
						}
					]
				});
			}
			
			//打开提醒里边的信息
			function openUnChecked(type){
				var bdat = $('#bdate').val();
				var edat = $('#edate').val();
				if(type=='dire'){
					showInfo('84CAE7674FB841A3ABBB734B336A21A0','<fmt:message key="Dire_inspection"/>','/inspectionDire/tableCheck.do?yndo=NO&bdat='+bdat+'&edat='+edat);
				}else if(type=='out'){
					showInfo('83F8F27E01F742308A0538342AD0CE9F','<fmt:message key="Out_inspection"/>','/inspectionOut/dispatch.do?mold=select&check=2&bdate='+bdat+'&edate='+edat);
				}else if(type=='db'){
					showInfo('8F1421D5867E45B099C31CF580BD0CD2','<fmt:message key="Transfer_inspection"/>','/inspectionDb/dispatchDb.do?mold=select&check=2&bdate='+bdat+'&edate='+edat);
				}else if(type=='chkstom'){
					showInfo('04948F998FC54E15B049488A6DC8037D','<fmt:message key="Report_order"/><fmt:message key="Fill_in"/>','/chkstomMis/chkstomTable.do');
					searchUncheck('iframe_04948F998FC54E15B049488A6DC8037D');
				}else if(type=='chkinm'){
					showInfo('D0EDA324850B4CC9BDED565B8D902FFF','<fmt:message key="In_orders"/><fmt:message key="Fill_in"/>','/chkinmMis/add.do?action=init');
					searchUncheck('iframe_D0EDA324850B4CC9BDED565B8D902FFF');
				}else if(type=='chkinmzf'){
					showInfo('96CE49A2774145A3B742EE394A5AF762','<fmt:message key="Inout_order"/><fmt:message key="Fill_in"/>','/chkinmZbMis/addzb.do?action=init');
					searchUncheck('iframe_96CE49A2774145A3B742EE394A5AF762');
				}else if(type=='chkoutm'){
					showInfo('AD216F61A31B4E85803C15A37857465A','<fmt:message key="Out_orders"/><fmt:message key="Fill_in"/>','/chkoutMis/addChkout.do?action=init');
					searchUncheck('iframe_AD216F61A31B4E85803C15A37857465A');
				}else if(type=='chkoutmdb'){
					showInfo('C5B73F18F2A54057BA47DDFB0B3B082E','<fmt:message key="Transfer_order"/><fmt:message key="Fill_in"/>','/chkoutDbMis/addChkout.do?action=init');
					searchUncheck('iframe_C5B73F18F2A54057BA47DDFB0B3B082E');
				}
			};
	  	 	function showInfo(moduleId,moduleName,moduleUrl){
	  	 		window.parent.tabMain.addItem([{
		  				id: 'tab_'+moduleId,
		  				text: moduleName,
		  				title: moduleName,
		  				closable: true,
		  				content: '<iframe id="iframe_'+moduleId+'" name="iframe_'+moduleId+'" frameborder="0" src="<%=path%>'+moduleUrl+'"></iframe>'
		  			}
		  		]);
	  	 		window.parent.tabMain.show('tab_'+moduleId);
	  	 	}
	  	 	
	  	 	function searchUncheck(id){
	  	 		var bdate = $('#bdate').val();
				var edate = $('#edate').val();
	  	 		window.setTimeout(function(){
					var frame = parent.document.getElementById(id);
					if(frame && frame.contentWindow)
						frame.contentWindow.searchUncheck(bdate,edate);
				},1000);
	  	 	}
	  	 	
	  	 	function getForm(){
				//使用JQuery从后台获取JSON格式的数据
				$.ajax({
					type : "post",//请求方式
					url : "<%=path%>/monthEndMis/monthEndTime.do",//发送请求地址
					timeout : 30000,//超时时间：30秒
					//请求成功后的回调函数 data为json格式
					success : function(data) {
						var msg = eval('('+data+')');
						if (msg.time >= 100) {
							window.clearInterval(timerId);
						}
						$('#p').progressbar('setValue', msg.time);
						$('#endTime').text(msg.des);
					},
					//请求出错的处理
					error : function() {
						window.clearInterval(timerId);
						alert("请求出错");
					}
				});
			}

		</script>
	</body>
</html>