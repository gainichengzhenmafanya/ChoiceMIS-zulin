<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>期初状态查询</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.bgBlue{
				background: #D2E9FF;
			}
		</style>
	</head>
	<body>
		<input type="hidden" name="sta" id="sta" value="select"/>
		<div class="tool"></div>
			<form id="listForm" action="<%=path%>/chkstodemomMis/listChkstoDemom.do" method="post">
				<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 25px;"></span></td>						
									<td><span style="width:80px;"><fmt:message key="gyszmbm"/></span></td>
									<td><span style="width:80px;"><fmt:message key="gyszmmcs"/></span></td>
									<td><span style="width:80px;"><fmt:message key="beginning_of_period"/><fmt:message key="positn_state"/></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0" class="datagrid">
							<tbody>
								<c:forEach var="positn" items="${positnList}" varStatus="status">
									<tr>
										<td class="num" ><span style="width: 25px;">${status.index+1}</span></td>
										<td><span style="width:80px;">${positn.code}</span></td>
										<td><span style="width:80px;">${positn.des}</span></td>
										<td><span style="width:80px;">
										    <c:if test="${positn.qc=='Y' }">
										    <fmt:message key="already"/><fmt:message key="beginning_of_period"/>
										    </c:if>
										    <c:if test="${positn.qc=='N' }">
										    <fmt:message key="not"/><fmt:message key="beginning_of_period"/>
										    </c:if>
										    </span>
										</td>
										
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</form>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
// 			if($.browser.msie){ 
// 				if ($.browser.version == 8.0) {
// 					setElementHeight('.grid',['.tool'],$(document.body),390);	//计算.grid的高度--适用IE8
// 				} else {
// 					setElementHeight('.grid',['.tool'],$(document.body),495);	//计算.grid的高度 -- 适用IE8以上版本
// 				}
// 			};
			setElementHeight('.grid',['.tool'],$(document.body),30);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid',20);	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			var tool = $('.tool').toolbar({
				items: [ {
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							$(".close",parent.document).click();								
						}
					} 
				]
			});
			
			
			//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     } else {
			    	 $('.grid').find(":checkbox").attr("checked", false);
			         $('.bgBlue').removeClass("bgBlue");
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			     }
			});
		});
		
		
		</script>
	</body>
</html>