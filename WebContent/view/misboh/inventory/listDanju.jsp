<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>单据列表</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.page{margin-bottom: 25px;}
		</style>					
	</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/danjuListMis/listDanju.do" method="post">
			<div class="form-line">
				<div class="form-label"><fmt:message key="startdate"/>：</div>
				<div class="form-input"><input type="text" id="bdate" name="bdate" class="Wdate text" value="<fmt:formatDate value="${danju.bdate}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edate\')}'})"/></div>
				<div class="form-label"><fmt:message key="suppliers"/>：</div>
				<div class="form-input">
				<input type="text"  id="deliver_name"  name="deliverDes" readonly="readonly" value="${danju.deliverDes}"/>
				<input type="hidden" id="delivercode" name="deliver" value="${danju.deliver}"/>
				<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_suppliers"/>' />
				</div>
				<div class="form-label"><fmt:message key="orders_num"/>：</div>
				<div class="form-input"><input type="text" id="chkno" name="chkno" class="text" value="${danju.chkno}" onkeyup="this.value=this.value.replace(/\D/g,'')"/></div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="enddate"/>：</div>
				<div class="form-input"><input type="text" id="edate" name="edate" class="Wdate text" value="<fmt:formatDate value="${danju.edate}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdate\')}'})"/></div>
				<div class="form-label"><fmt:message key="positions"/>：</div>
				<div class="form-input">
					<input type="text" name="positnDes" id="positn_name" class="text" value="<c:out value="${danju.positnDes}" />"/>
					<img id="seachDept" class="search" src="<%=path%>/image/themes/icons/search.png" alt='查询仓位' />
					<input type="hidden" id="positn" name="positn" value="${danju.positn }"/>
				</div>
				<div class="form-label"><fmt:message key="document_number"/>：</div>
				<div class="form-input"><input type="text" id="vouno" name="vouno" class="text" value="${danju.vouno}"/></div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="document_types"/>：</div>
				<div class="form-input">
					<select name="typ" style="width:135px;">
						<option></option>
						<option value="0" <c:if test="${danju.typ == 0 && danju.typ != ''}"> selected="selected"</c:if>>报货单</option>
						<c:forEach items="${danjuTyp }" var="typ">
							<option value="${typ.code}" <c:if test="${danju.typ == typ.code }"> selected="selected"</c:if>>${typ.des }</option>
						</c:forEach>
					</select></div>
				<div class="form-label"><fmt:message key="requisitioned_positions"/>：</div>
				<div class="form-input">
				<input type="text"  id="positn_name1"  name="firmDes" readonly="readonly" value="${danju.firmDes}"/>
				<img id="seachPositn1" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
				<input type="hidden" id="firm" name="firm" value="${danju.firm}"/>
				</div>
				<div class="form-input" style="width:165px;margin-left:100px;">
					<font size="3">[</font><input type="radio" name="checby" value="" checked="checked"/><fmt:message key="all"/>
					<input type="radio" <c:if test="${danju.checby=='checked' }"> checked="checked"</c:if> name="checby" value="checked"/><fmt:message key="checked"/>
					<input type="radio" <c:if test="${danju.checby=='unchecked' }"> checked="checked"</c:if> name="checby" value="unchecked"/><fmt:message key="unchecked"/><font size="3">]</font>
				</div>
				<div class="form-input" style="margin-left:50px;">
					<font size="3">[</font><input type="checkbox" name="id" value="5" <c:if test="${fn:contains(danju.id,5) }"> checked="checked"</c:if>/><fmt:message key="reports"/>
					<input type="checkbox" name="id" value="1" <c:if test="${fn:contains(danju.id,1) }"> checked="checked"</c:if>/><fmt:message key="storage"/>
					<input type="checkbox" name="id" value="2" <c:if test="${fn:contains(danju.id,2) }"> checked="checked"</c:if>/><fmt:message key="straight_hair"/>
					<input type="checkbox" name="id" value="3" <c:if test="${fn:contains(danju.id,3) }"> checked="checked"</c:if>/><fmt:message key="library"/>
					<input type="checkbox" name="id" value="4" <c:if test="${fn:contains(danju.id,4) }"> checked="checked"</c:if>/><fmt:message key="Transfer"/><font size="3">]</font>
				</div>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width:25px;"></span></td>
								<td><span style="width:60px;"><fmt:message key="document_types"/></span></td>
								<td><span style="width:50px;"><fmt:message key="orders_num"/></span></td>
								<td><span style="width:120px;"><fmt:message key="document_number"/></span></td>
								<td><span style="width:80px;"><fmt:message key="date_of_the_system_alone"/></span></td>
								<td><span style="width:130px;"><fmt:message key="time_of_the_system_alone"/></span></td>
								<td><span style="width:70px;"><fmt:message key="total_amount1"/></span></td>
								<td><span style="width:60px;"><fmt:message key="orders_maker"/></span></td>
								<td><span style="width:60px;"><fmt:message key="orders_audit"/></span></td>
								<td><span style="width:80px;"><fmt:message key="positions"/></span></td>
								<td><span style="width:100px;"><fmt:message key="suppliers"/></span></td>
								<td><span style="width:80px;"><fmt:message key="requisitioned_positions"/></span></td>
								<td><span style="width:100px;"><fmt:message key="summary"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="dj" items="${danjuList }" varStatus="status">
								<tr data-typ="${dj.id }" data-id="${dj.chkno }" data-checby="${dj.checby }">
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td><span style="width:60px;" title="${dj.typDes }">${dj.typDes }</span></td>
									<td><span style="width:50px;" title="${dj.chkno }">${dj.chkno }</span></td>
									<td><span style="width:120px;" title="${dj.vouno }">${dj.vouno }</span></td>
									<td><span style="width:80px;" title="<fmt:formatDate value="${dj.maded}" pattern="yyyy-MM-dd" type="date"/>"><fmt:formatDate value="${dj.maded}" pattern="yyyy-MM-dd" type="date"/></span></td>
									<td><span style="width:130px;" title="${dj.madet }">${dj.madet}</span></td>
									<td><span style="width:70px;text-align: right;" title="<fmt:formatNumber value="${dj.totalamt }" pattern="##.#" minFractionDigits="2" />"><fmt:formatNumber value="${dj.totalamt }" pattern="##.#" minFractionDigits="2" /></span></td>
									<td><span style="width:60px;" title="${dj.madeby }">${dj.madeby }</span></td>
									<td><span style="width:60px;" title="${dj.checby }">${dj.checby }</span></td>
									<td><span style="width:80px;" title="${dj.positnDes }">${dj.positnDes }</span></td>
									<td><span style="width:100px;" title="${dj.deliverDes }">${dj.deliverDes }</span></td>
									<td><span style="width:80px;" title="${dj.firmDes }">${dj.firmDes }</span></td>
									<td><span style="width:100px;" title="${dj.memo }">${dj.memo }</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>				
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
		<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">
		var detailWin;
		var validate;
		$(document).ready(function(){
			/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'chkno',
					validateType:['canNull','intege'],
					param:['T','T'],
					error:['','<fmt:message key="single_number_is_digital_or_empty"/>！']
				}]
			});
			//双击
			$('.grid .table-body tr').dblclick(function(){
				var typ = $(this).attr('data-typ');
				var id = $(this).attr('data-id');
				var checby = $(this).attr('data-checby');
				if(checby && checby != ''){//已审核
					if(typ == 1){//入库
						detailWin = $('body').window({
							id: 'checkedChkinmDetail',
							title: '<fmt:message key="In_order_detail"/>',
							content: '<iframe id="checkedChkinmDetailFrame" frameborder="0" src="<%=path%>/chkinmMis/update.do?chkinno='+id+'&isCheck=isCheck"></iframe>',
							width: $(document.body).width()-20/* '1050px' */,
							height: '460px',
							draggable: true,
							isModal: true,
							confirmClose:false
						});
					}else if(typ == 2){//直发
						detailWin = $('body').window({
							id: 'checkedChkinmZbDetail',
							title: '<fmt:message key="Inout_order_detail"/>',
							content: '<iframe id="checkedChkinmZbDetailFrame" frameborder="0" src="<%=path%>/chkinmZbMis/update.do?chkinno='+id+'&isCheck=isCheck&inout=zf"></iframe>',
							width: $(document.body).width()-20/* '1050px' */,
							height: '460px',
							draggable: true,
							isModal: true,
							confirmClose:false
						});
					}else if(typ == 3){//出库
						detailWin = $('body').window({
							id: 'checkedChkoutmDetail',
							title: '<fmt:message key="Out_order_detail"/>',
							content: '<iframe id="checkedChkoutDetailFrame" frameborder="0" src="<%=path%>/chkoutMis/searchCheckedChkout.do?chkoutno='+id+'"></iframe>',
							width: $(document.body).width()-20,
							height: '460px',
							draggable: true,
							isModal: true,
							confirmClose:false
						});
					}else if(typ == 4){//调拨
						detailWin = $('body').window({
							id: 'checkedChkoutDbDetail',
							title: '<fmt:message key="Transfer_out_order_detail"/>',
							content: '<iframe id="checkedChkoutDbDetailFrame" frameborder="0" src="<%=path%>/chkoutDbMis/searchCheckedChkout.do?chkoutno='+id+'"></iframe>',
							width: $(document.body).width()-20,
							height: '460px',
							draggable: true,
							isModal: true,
							confirmClose:false
						});
					}else if(typ == 5){//报货
						detailWin = $('body').window({
							id: 'checkedChkstomDetail',
							title: '<fmt:message key="chkstom_detail" />',
							content: '<iframe id="checkedChkstomDetailFrame" frameborder="0" src="<%=path%>/chkstomMis/findDetail.do?chkstoNo='+id+'"></iframe>',
							width: $(document.body).width()-20,
							height: '460px',
							draggable: true,
							isModal: true,
							confirmClose:false
						});
					}
				}else{//未审核
					if(typ == 1){
						showInfo('D0EDA324850B4CC9BDED565B8D902FFF','<fmt:message key="In_orders"/><fmt:message key="Fill_in"/>','/chkinmMis/add.do?action=init');
						window.setTimeout(function(){
							var frame = parent.document.getElementById('iframe_D0EDA324850B4CC9BDED565B8D902FFF');
							if(frame && frame.contentWindow)
								frame.contentWindow.location.href = '<%=path%>/chkinmMis/update.do?chkinno='+id;
						},500);
					}else if(typ == 2){
						showInfo('96CE49A2774145A3B742EE394A5AF762','<fmt:message key="Inout_order"/><fmt:message key="Fill_in"/>','/chkinmZbMis/addzb.do?action=init');
						window.setTimeout(function(){
							var frame = parent.document.getElementById('iframe_96CE49A2774145A3B742EE394A5AF762');
							if(frame && frame.contentWindow)
								frame.contentWindow.location.href = '<%=path%>/chkinmZbMis/update.do?inout=zf&chkinno='+id;
						},500);
					}else if(typ == 3){
						showInfo('AD216F61A31B4E85803C15A37857465A','<fmt:message key="Out_orders"/><fmt:message key="Fill_in"/>','/chkoutMis/addChkout.do?action=init');
						window.setTimeout(function(){
							var frame = parent.document.getElementById('iframe_AD216F61A31B4E85803C15A37857465A');
							if(frame && frame.contentWindow)
								frame.contentWindow.location.href = '<%=path%>/chkoutMis/updateChkout.do?chkoutno='+id;
						},500);
					}else if(typ == 4){
						showInfo('C5B73F18F2A54057BA47DDFB0B3B082E','<fmt:message key="Transfer_order"/><fmt:message key="Fill_in"/>','/chkoutDbMis/addChkout.do?action=init');
						window.setTimeout(function(){
							var frame = parent.document.getElementById('iframe_C5B73F18F2A54057BA47DDFB0B3B082E');
							if(frame && frame.contentWindow)
								frame.contentWindow.location.href = '<%=path%>/chkoutDbMis/updateChkout.do?chkoutno='+id;
						},500);
					}else if(typ == 5){
						showInfo('04948F998FC54E15B049488A6DC8037D','<fmt:message key="Report_order"/><fmt:message key="Fill_in"/>','/chkstomMis/chkstomTable.do');
						window.setTimeout(function(){
							var frame = parent.document.getElementById('iframe_04948F998FC54E15B049488A6DC8037D');
							if(frame && frame.contentWindow)
								frame.contentWindow.location.href = '<%=path%>/chkstomMis/findChk.do?chkstoNo='+id;
						},500);
					}
				}
			});
			//按钮快捷键
			focus() ;//页面获得焦点
		 	$('.tool').toolbar({
				items: [{
					text: '<fmt:message key="select" />',
					title: '<fmt:message key="select"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						if(validate._submitValidate()){
							$("#listForm").submit();
						}
					}
				},{
					text: '<fmt:message key="export" />',
					title: '<fmt:message key="export"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-140px','-100px']
					},
					handler: function(){
						if(validate._submitValidate()){
							exportDanju();
						}
					}
				},{
// 					text: '<fmt:message key="print" />',
// 					title: '<fmt:message key="print"/>',
// 					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
// 					icon: {
<%-- 						url: '<%=path%>/image/Button/op_owner.gif', --%>
// 						position: ['-140px','-100px']
// 					},
// 					handler: function(){
						
// 					}
// 				},{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
					}
				}]
			});
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		    $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),100);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			changeTh();
			
// 			$("#seachPositn1").click(function(){
// 				chooseStoreSCM({
<%-- 					basePath:'<%=path%>', --%>
// 					width:600,
// 					firmId:$("#firm").val(),
// 					single:false,
// 					tagName:'positn_name1',
// 					tagId:'firm',
// 					title:'<fmt:message key="please_select_positions"/>'
// 				});
// 			});
			/*弹出树*/
			$('#seachPositn1').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#firm').val();
					var defaultName = $('#positn_name1').val();
					//alert(defaultCode+"==="+defaultName);
					var offset = getOffset('positn');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/misbohcommon/findPositnSuper.do?sta=all&typn=3-6&mold=oneTmany&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positn_name1'),$('#firm'),'760','520','isNull');
				}
			});
  	 		$('#seachDeliver').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $("#delivercode").val();
					var offset = getOffset('bdate');
					top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/deliver/searchAllDeliver.do?defaultCode='+defaultCode),offset,$('#deliver_name'),$('#delivercode'),'900','500','isNull');
				}
			});
  	 		/*弹出树*/
			$('#seachDept').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#positn').val();
					var defaultName = $('#positn_name').val();
					//alert(defaultCode+"==="+defaultName);
					var offset = getOffset('positn');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/misbohcommon/findPositnSuper.do?typn=7&iffirm=1&mold=oneTmany&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positn_name'),$('#positn'),'760','520','isNull');
				}
			});
		});
		
		//导出excel
		function exportDanju(){
			$("#wait2").val('NO');//不用等待加载
			$("#listForm").attr("action","<%=path%>/danjuListMis/exportDanju.do");
			$('#listForm').submit();
			$("#wait2 span").html("数据导出中，请稍后...");
			$("#listForm").attr("action","<%=path%>/danjuListMis/listDanju.do");
			$("#wait2").val('');//等待加载还原
	    }
		
		function showInfo(moduleId,moduleName,moduleUrl){
  	 		window.parent.tabMain.addItem([{
	  				id: 'tab_'+moduleId,
	  				text: moduleName,
	  				title: moduleName,
	  				closable: true,
	  				content: '<iframe id="iframe_'+moduleId+'" name="iframe_'+moduleId+'" frameborder="0" src="<%=path%>'+moduleUrl+'"></iframe>'
	  			}
	  		]);
  	 		window.parent.tabMain.show('tab_'+moduleId);
  	 	}
		</script>				
	</body>
</html>