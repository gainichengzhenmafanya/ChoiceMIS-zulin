<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>仓库期初</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
				form .form-line .form-label{
					width: 10%;
				}
				form .form-line .form-input{
					width: 20%;
				}
			</style>
			<script type="text/javascript">
				var path="<%=path%>";
			</script>								
		</head>
	<body>
		<div class="tool">
		</div>
		<input type="hidden" id="firm" value="${positnSupply.firm }" />
		<form id="listForm" name="listForm" action="<%=path%>/ckInitMis/ckInit.do"method="post">
		<!--status是否已盘点 y  n -->
		<input type="hidden" id="status" name="status" value="<c:out value="${status}" default="init"/>" />
		<input type="hidden" id="positn" name="positn" value="${positnSupply.positn }"/>
		<div class="form-line">
			<c:if test="${!empty positnSupply.firm }">
				<div class="form-label"><span style="color:red;">*</span><fmt:message key="positions"/>：</div>
				<div class="form-input">
					<div class="form-input">
						<input type="text" name="positnName" id="positn_name" style="width: 136px;margin-top: -3px;" class="text" value="<c:out value="${positnSupply.positnName}" />"/>
						<img id="seachDept" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
					</div>
				</div>
			</c:if>
			<div class="form-label"><fmt:message key="retrieval"/>：</div>
			<div class="form-input">
				<input type="text" id="search" style="margin-top:2px;"/>
		    </div>
		</div>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" rowspan="2"><span style="width: 16px;">&nbsp;</span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="supplies_code"/></span></td>
								<td rowspan="2"><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="supplies_specifications"/></span></td>
								<td colspan="8"><span><fmt:message key="quantity"/></span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="total"/><fmt:message key="quantity"/></span></td>
								<td rowspan="2"><span style="width:40px;"><fmt:message key="unit"/></span></td>
								<td rowspan="2"><span style="width:100px;"><fmt:message key="amount"/></span></td>
							</tr>
							<tr>
								<td><span style="width:60px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:40px;"><fmt:message key="specifications"/><fmt:message key="scm_one"/></span></td>
								<td><span style="width:60px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:40px;"><fmt:message key="specifications"/><fmt:message key="scm_two"/></span></td>
								<td><span style="width:60px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:40px;"><fmt:message key="specifications"/><fmt:message key="scm_three"/></span></td>
								<td><span style="width:60px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:40px;"><fmt:message key="specifications"/><fmt:message key="scm_four"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<c:set var="sum_num" value="${0}"/>  <!-- 物资条数 -->
				<c:set var="sum_amount" value="${0}"/>  <!-- 总数量 -->
				<c:set var="sum_totalamt" value="${0}"/>  <!-- 总金额 -->
				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" id="table-body">
						<tbody>
							<c:forEach var="positnSupply" items="${positnSupplyList}" varStatus="status">
								<tr data-unitper1="${positnSupply.unitper1}" data-unitper2="${positnSupply.unitper2}"
									data-unitper3="${positnSupply.unitper3}" data-unitper4="${positnSupply.unitper4}"
									data-sp_init="${positnSupply.sp_init }">
									<td class="num" ><span style="width:16px;">${status.index+1}</span></td>
									<td><span title="${positnSupply.sp_code}" style="width:70px;" data-sp_name="${positnSupply.sp_name}">${positnSupply.sp_code}</span></td>
									<td><span title="${positnSupply.sp_name}" style="width:100px;">${positnSupply.sp_name}</span></td>
									<td><span title="${positnSupply.sp_desc}" style="width:80px;">${positnSupply.sp_desc}</span></td>
									<td><span title="${positnSupply.cnt1}" style="width:60px;text-align: right;"><fmt:formatNumber value="${positnSupply.cnt1}" pattern="##.##" minFractionDigits="2"/></span></td>
									<td><span title="${positnSupply.spec1}" style="width:40px;">${positnSupply.spec1}</span></td>
									<td>
										<span title="${positnSupply.cnt2}" style="width:60px;text-align: right;">
											<c:if test="${!empty positnSupply.spec2 }">
												<fmt:formatNumber value="${positnSupply.cnt2}" pattern="##.##" minFractionDigits="2"/>
											</c:if>
										</span>
									</td>
									<td><span title="${positnSupply.spec2}" style="width:40px;">${positnSupply.spec2}</span></td>
									<td>
										<span title="${positnSupply.cnt3}" style="width:60px;text-align: right;">
											<c:if test="${!empty positnSupply.spec3 }">
												<fmt:formatNumber value="${positnSupply.cnt3}" pattern="##.##" minFractionDigits="2"/>
											</c:if>
										</span>
									</td>
									<td><span title="${positnSupply.spec3}" style="width:40px;">${positnSupply.spec3}</span></td>
									<td>
										<span title="${positnSupply.cnt4}" style="width:60px;text-align: right;">
											<c:if test="${!empty positnSupply.spec4 }">
												<fmt:formatNumber value="${positnSupply.cnt4}" pattern="##.##" minFractionDigits="2"/>
											</c:if>
										</span>
									</td>
									<td><span title="${positnSupply.spec4}" style="width:40px;">${positnSupply.spec4}</span></td>
									
									<td><span title="${positnSupply.inc0}" style="width:80px;text-align: right;"><fmt:formatNumber value="${positnSupply.inc0}" pattern="##.##" minFractionDigits="2"/></span></td>
									<td><span title="${positnSupply.unit}" style="width:40px;">${positnSupply.unit}</span></td>
									<td><span title="${positnSupply.ina0}" style="width:100px;text-align: right;"><fmt:formatNumber value="${positnSupply.ina0}" pattern="##.##" minFractionDigits="2"/></span></td>
								</tr>
								<c:set var="sum_num" value="${status.index+1}"/>
								<c:set var="sum_amount" value="${sum_amount + positnSupply.inc0}"/>
								<c:set var="sum_totalamt" value="${sum_totalamt + positnSupply.ina0}"/>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div style="height: 10px">	
					<table cellspacing="0" cellpadding="0" style="height: 10px">
						<thead>
							<tr>
								<td style="width:106px;"><fmt:message key="total"/></td>
								<td style="width:110px;"><fmt:message key="material_number"/>:<u><label id="sum_num">${sum_num}</label></u></td>
								<td style="width:530px"></td>
								<td style="width:140px;"><fmt:message key="total_number"/>:<u><label id="sum_amount"><fmt:formatNumber value="${sum_amount}" pattern="##.##" minFractionDigits="2" /></label></u></td>
								<td style="width:50px"></td>
								<td style="width:110px;"><fmt:message key="total_amount"/>:
									<u><label id="sum_totalamt"><fmt:formatNumber value="${sum_totalamt}" pattern="##.##" minFractionDigits="2" /></label>元</u>
								</td>
							</tr>
						</thead>
					</table>
			   </div>	
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/autoTable.js"></script>
		
		<script type="text/javascript">
		//如果导入报货单报错了。。。wjf
		var importError = '<c:out value="${importError}"/>';
		if(importError != null && importError != '' && importError != 'undefined'){
			alert('<fmt:message key="Import_failed_The_reason"/>：\n'+importError);
		}
			$(document).ready(function(){
				function validator(weiz,row,data){
					row.find("input").data("ovalue",data.value);
					if(weiz=="4"){
						if(Number(data.value) < 0){
							alert('<fmt:message key="number_cannot_be_negative"/>！');
							row.find("td:eq(4)").find('span').text(data.ovalue);
							$.fn.autoGrid.setCellEditable(row,4);
						}else if(isNaN(data.value)){
							alert('<fmt:message key="number_be_not_number"/>！');
							row.find("td:eq(4)").find('span').text(data.ovalue);
							$.fn.autoGrid.setCellEditable(row,4);
						}else{
							row.find("td:eq(4)").find('span').attr('title',data.value);
							getSumCnt(row);
						}
// 						getTotalSum();
					}else if(weiz=="6"){
						if(row.find("td:eq(7) span").text() != ''){
							if(Number(data.value) < 0){
								alert('<fmt:message key="number_cannot_be_negative"/>！');
								row.find("td:eq(6)").find('span').text(data.ovalue);
								$.fn.autoGrid.setCellEditable(row,6);
							}else if(isNaN(data.value)){
								alert('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(6)").find('span').text(data.ovalue);
								$.fn.autoGrid.setCellEditable(row,6);
							}else{
								row.find("td:eq(6)").find('span').attr('title',data.value);
								getSumCnt(row);
							}
						}else{
							row.find("td:eq(6)").find('span').text(data.ovalue);
							$.fn.autoGrid.setCellEditable(row,14);
						}
// 						getTotalSum();
					}else if(weiz=="8"){
						if(row.find("td:eq(9) span").text() != ''){
							if(Number(data.value) < 0){
								alert('<fmt:message key="number_cannot_be_negative"/>！');
								row.find("td:eq(8)").find('span').text(data.ovalue);
								$.fn.autoGrid.setCellEditable(row,8);
							}else if(isNaN(data.value)){
								alert('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(8)").find('span').text(data.ovalue);
								$.fn.autoGrid.setCellEditable(row,8);
							}else{
								row.find("td:eq(8)").find('span').attr('title',data.value);
								getSumCnt(row);
							}
						}else{
							row.find("td:eq(8)").find('span').text(data.ovalue);
							$.fn.autoGrid.setCellEditable(row,14);
						}
// 						getTotalSum();
					}else if(weiz=="10"){
						if(row.find("td:eq(11) span").text() != ''){
							if(Number(data.value) < 0){
								alert('<fmt:message key="number_cannot_be_negative"/>！');
								row.find("td:eq(10)").find('span').text(data.ovalue);
								$.fn.autoGrid.setCellEditable(row,10);
							}else if(isNaN(data.value)){
								alert('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(10)").find('span').text(data.ovalue);
								$.fn.autoGrid.setCellEditable(row,10);
							}else{
								row.find("td:eq(10)").find('span').attr('title',data.value);
								getSumCnt(row);
							}
						}else{
							row.find("td:eq(10)").find('span').text(data.ovalue);
							$.fn.autoGrid.setCellEditable(row,14);
						}
// 						getTotalSum();
					}else if(weiz=="14"){
						if(Number(data.value) < 0){
							alert('<fmt:message key="number_cannot_be_negative"/>！');
							row.find("td:eq(14)").find('span').text(data.ovalue);
							$.fn.autoGrid.setCellEditable(row,14);
						}else if(isNaN(data.value)){
							alert('<fmt:message key="number_be_not_number"/>！');
							row.find("td:eq(14)").find('span').text(data.ovalue);
							$.fn.autoGrid.setCellEditable(row,14);
						}
						row.find("td:eq(14)").find('span').attr('title',data.value);
					}
// 					getTotalSum();
				}
			 	//键盘事件绑定
			 	$(document).bind('keyup',function(e){
			 		if($(e.srcElement).is("input")){
			    		validator($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
			    	}
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit"/>').click();
			 		}
				}); 
		 		//定义加载后定位在第一个输入框上     
		 		//$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),2);
			 	//判断工具栏状态
		 		var status = $("#status").val();
			 	if(status == 'select'){
			 		loadToolBar([true,false]);
			 	}else if(status == 'init'){
			 		loadToolBar([false,false]);
			 	}else if(status == 'import'){
			 		$("#status").val('select');
			 		loadToolBar([false,true]);
			 		editCells();
			 	}else{
			 		loadToolBar([true,true]);
			 	} 
				function loadToolBar(use){
					$('.tool').html('');
					var tool = $('.tool').toolbar({
						items: [{
								text: '<fmt:message key="select" />',
								title: '<fmt:message key="select"/>',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['0px','-40px']
								},
								handler: function(){
									if($('#positn').val() == ''){
										alert('<fmt:message key="please_select_positions"/>！');//请选择仓位
										return;
									}
									$("#status").val('select');
									$('#listForm').submit();
								}
							},'-',{
								text: '<fmt:message key="update" />',
								title: '<fmt:message key="update" />',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['0px','0px']
								},
								handler: function(){
									loadToolBar([false,true]);
									editCells();
								}
							},{
								text: '<fmt:message key="save" />',
								title: '<fmt:message key="save" />',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[1],
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){ 
									resolve();
								}
							},{
								text: '<fmt:message key="initial_recognition"/>',
								title: '<fmt:message key="initial_recognition"/>',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'selete')}&&use[0],
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-20px']
								},
								handler: function(){
									initSpatch();
								}
							},{
								text: '<fmt:message key="view" /><fmt:message key="total" />',
								title: '<fmt:message key="view" /><fmt:message key="total" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-20px']
								},
								handler: function(){
									getTotalSum();
								}
							},{
								text: '<fmt:message key="beginning_of_period" /><fmt:message key="positn_state" /><fmt:message key="select" />',
								title: '<fmt:message key="beginning_of_period" /><fmt:message key="positn_state" /><fmt:message key="select" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-20px']
								},
								handler: function(){
									qcZtChaxun();
								}
							},'-',{
								text: '<fmt:message key="import" />',
								title: '<fmt:message key="import" />',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'import')}&&use[0],
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-20px']
								},
								handler: function(){
									importCangkuInit();
								}
							},{
								text: '<fmt:message key="export" />',
								title: '<fmt:message key="export" />',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-20px']
								},
								handler: function(){
									exportCangkuInit();
								}
							},{
								text: '<fmt:message key="quit" />',
								title: '<fmt:message key="quit"/>',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
								}
							}
						]
					});
				}
				setElementHeight('.grid',['.tool'],$(document.body),55);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid',20);				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				});
				
				/*弹出部门选择树*/
				$('#seachDept').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#positn').val();
						var defaultName = $('#positn_name').val();
						//alert(defaultCode+"==="+defaultName);
						var offset = getOffset('positn');
						top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/misbohcommon/findPositnSuper.do?typn=7&iffirm=1&mold=one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positn_name'),$('#positn'),'760','520','isNull',handle);
					}
				});
				
				function handle(code){
					$("#status").val('select');
					$('#listForm').submit();
				}
				
// 				new tabTableInput("table-body","text"); //input  上下左右移动
// 				$('.edit').bind('click', function(){
// 					var tdObj = $(this);
// 					$(this).html('<input id="editText" text="text" size="10" value="'+$(this).text()+'" name="name" onblur="onBlurMethod(this)"/>');
// 			    	$('#editText').focus();
// 				});
				
				//检索
				$('#search').blur(function(){
					$('.table-body').find('tr').css('color','black');
					if($(this).val() != ''){
						$('.table-body').find('tr').each(function(){
							if($(this).find('td:eq(1) span').text().indexOf($('#search').val()) != -1 
									|| $(this).find('td:eq(2) span').text().indexOf($('#search').val()) != -1 
									|| $(this).data('sp_init').indexOf($('#search').val().toUpperCase()) != -1){//存在
								$(this).css('color','red');
							}
						});
					}
				});
				
				//初始进入页面或者没查询到数据时出现滚动条
				var headScrollWidth = $('.table-head')[0].scrollWidth;
				$('.table-head').width(headScrollWidth);
			});
			
// 		    function onBlurMethod(inputObj){
// 		    	$(inputObj).parent('span').text($(inputObj).val());
// 	    	}
		    
			//保存修改数据
			function resolve(){
				var selected = {};
				var sp_name = '';
				var flag1 = true;//有数量没金额
				var flag2 = true;//有金额没数量
				var flag3 = 0;//判断没有转换率的物资 规格是必输项
				if($('.grid').find('.table-body').find('tr').size() > 0 && $('.grid').find('.table-body').find('tr').find('td:eq(1) span').text()!=""){
					selected['positn'] = $('#positn').val();
					var i = 0;
					$('.grid').find('.table-body').find('tr').each(function(){
						if($.trim($(this).find('td:eq(1) span').text())!=null && $.trim($(this).find('td:eq(1) span').text()) !='') {
							sp_name = $(this).find('td:eq(2) span').text()?$(this).find('td:eq(2) span').text():$(this).find('td:eq(2) span').find('input').val();
							var inc0 = Number($(this).find('td:eq(12) span').attr('title'));//数量
							var ina0 = Number($(this).find('td:eq(14) span').attr('title'));
							if(inc0 != 0 && ina0 == 0){//有数量没金额
								flag1 = false;
								return false;
							}
							if(ina0 != 0 && inc0 == 0){//有金额没数量
								flag2 = false;
								return false;
							}
							if(inc0 == 0 && ina0 == 0){//都为0的过滤掉
								return true;
							}
							var cnt1 = Number($(this).find('td:eq(4) span').attr('title'));
							var cnt2 = $(this).find('td:eq(7) span').text()?Number($(this).find('td:eq(6) span').attr('title')):0;
							var cnt3 = $(this).find('td:eq(9) span').text()?Number($(this).find('td:eq(8) span').attr('title')):0;
							var cnt4 = $(this).find('td:eq(11) span').text()?Number($(this).find('td:eq(10) span').attr('title')):0;
							if(inc0 != 0){
		 						if(Number($(this).data('unitper2')) == 0 && Number(cnt2) == 0 && $(this).find('td:eq(7) span').text()){
		 							flag3 = 2;
		 							return false;
								}else if(Number($(this).data('unitper3')) == 0 && Number(cnt3) == 0 && $(this).find('td:eq(9) span').text()){
									flag3 = 3;
		 							return false;
								}else if(Number($(this).data('unitper4')) == 0 && Number(cnt4) == 0 && $(this).find('td:eq(11) span').text()){
									flag3 = 4;
		 							return false;
								}
							}
							selected['positnSupplyList['+i+'].sp_code'] = $.trim($(this).find('td:eq(1) span').text());
							selected['positnSupplyList['+i+'].inc0'] = inc0;
							selected['positnSupplyList['+i+'].ina0'] = ina0;
							//转换率为0的才存入参考数量的值，转换率都不为0，参考数量为0
// 							if(Number($(this).data('unitper4')) == 0){
// 								selected['positnSupplyList['+i+'].incu0'] = $(this).find('td:eq(10) span').text()?Number($(this).find('td:eq(10) span').text()):Number($(this).find('td:eq(10) span').find('input').val());
// 							}else if(Number($(this).data('unitper3')) == 0){
// 								selected['positnSupplyList['+i+'].incu0'] = $(this).find('td:eq(8) span').text()?Number($(this).find('td:eq(8) span').text()):Number($(this).find('td:eq(8) span').find('input').val());
// 							}else if(Number($(this).data('unitper2')) == 0){
// 								selected['positnSupplyList['+i+'].incu0'] = $(this).find('td:eq(6) span').text()?Number($(this).find('td:eq(6) span').text()):Number($(this).find('td:eq(6) span').find('input').val());
// 							}else{
// 								selected['positnSupplyList['+i+'].incu0'] = 0;
// 							}
							selected['positnSupplyList['+i+'].cnt1'] = cnt1;
							selected['positnSupplyList['+i+'].cnt2'] = cnt2;
							selected['positnSupplyList['+i+'].cnt3'] = cnt3;
							selected['positnSupplyList['+i+'].cnt4'] = cnt4;
							selected['positnSupplyList['+i+'].sp_price'] = inc0 == 0?0:ina0/inc0;
							selected['positnSupplyList['+i+'].firm'] = $('#firm').val()?$('#firm').val():$('#positn').val();
							i++;
						}
					});
					if(!flag1){
						alert('<fmt:message key="supplies"/>:['+sp_name+']<fmt:message key="has_quantity_no_amount"/>！');
						return;
					}
					if(!flag2){
						alert('<fmt:message key="supplies"/>:['+sp_name+']<fmt:message key="has_amount_no_quantity"/>！');
						return;
					}
					if(flag3 != 0){
						alert('<fmt:message key="supplies"/>:['+sp_name+']<fmt:message key="There_is_no_conversion_specification"/>'+flag3+'<fmt:message key="will_not_null"/>！');
						return;
					}
					$('#wait').show();
					$('#wait2').show();
					$.post('<%=path%>/ckInitMis/updateCkInit.do',selected,function(data){
						$('#wait').hide();
						$('#wait2').hide();
					 	if(data=="success"){
							showMessage({
								type: 'success',
								msg: '<fmt:message key="success_to_save_data"/>！',
								speed: 1000
							});	
							pageReload('parent');
					 	}else{
					 		showMessage({
								type: 'error',
								msg: '<fmt:message key="failed_to_save_data"/>！',
								speed: 1000
							});	
					 	}
					});
				}else{
					if(confirm('<fmt:message key="No_material_detail_data_Whether_or_not_to_continue"/>?')){//无物资明细数据,是否继续
						$('#listForm').submit();
					}
				}
			}
			
		    function pageReload(par){
		    	if('parent'==par){
		    		alert('<fmt:message key="operation_successful"/>！');
		    	}
		    	$('#listForm').submit();
		    }
		    
		  //确认初始
			function initSpatch(){
				var selected = {};
				var flag = true;
				if($('.grid').find('.table-body').find('tr').size() == 0 || $('.grid').find('.table-body').find('tr').first().find('td:eq(1) span').text()==""){
					flag = confirm('<fmt:message key="No_material_detail_data_Whether_or_not_to_continue"/>?');
				}
				if(flag){
// 					var msg = '确定要确认当前门店的期初数据吗?确认后不能再进行修改!';
					var msg = '<fmt:message key="Are_you_sure_to_confirm"/><fmt:message key="the_current_store"/>'+
								'<fmt:message key="Initial_data_Confirm_and_cannot_be_modified"/>!';
					if($('#firm').val()){
// 						msg = '确定要确认['+$("#positn_name").val()+']的期初数据吗?确认后不能再进行修改!';
						msg = '<fmt:message key="Are_you_sure_to_confirm"/>['+$("#positn_name").val()+']'+
						'<fmt:message key="Initial_data_Confirm_and_cannot_be_modified"/>!';
					}
					if(confirm(msg)){
						$("#wait2").css('display','');
		  	 			$("#wait").css('display','');
						selected['positn'] = $('#positn').val();
						$.post('<%=path%>/ckInitMis/initation.do',selected,function(data){
							$("#wait2").css('display','none');
			  	 			$("#wait").css('display','none');
						 	//弹出提示信息
						 	alert('<fmt:message key="Initialized_to_complete_cannot_modify"/>！');//已初始化完成,不能再修改
							$("#status").val('init');
							pageReload();
						});
					}
				}
			}
		  //编辑表格
			function editCells() {
				$('.table-body').find('tr:first').find("td:eq(2) span").focus().select();
				$(".table-body").autoGrid({
					initRow:1,
					colPerRow:15,
					widths:[26,80,110,90,70,50,70,50,70,50,70,50,90,50,110],
					colStyle:['','',{background:"#F1F1F1"},'',{background:"#F1F1F1"},'',{background:"#F1F1F1"},'',{background:"#F1F1F1"},'',{background:"#F1F1F1"},'','','',{background:"#F1F1F1"}],
					VerifyEdit:{verify:true,enable:function(cell,row){
						return row.find('td').index(cell) == 2 || (row.find("td:eq(2)").text() == null || $.trim(row.find("td:eq(2)").text()) != '' ) ? true : false;
					}},
					onEdit:$.noop,
					editable:[2,4,6,8,10,14],
					onLastClick:function(row){
// 						getTotalSum();
					},
					onEnter:function(data){
						var pos = data.curobj.closest('tr').find('td').index(data.curobj.closest('td'));
						if(pos == 2){
							if($.trim(data.curobj.closest('td').prev().text())){
								data.curobj.find('span').html(data.curobj.closest('td').prev().find('span').data('sp_name'));
								return;
							}
						 	else if(!data.actionobj){
								$.fn.autoGrid.setCellEditable(data.curobj.closest('tr'),2);
								return;
							} 
						}
						$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.ovalue) ;
					},
					cellAction:[{
						index:2,
						action:function(row){
							$.fn.autoGrid.setCellEditable(row,4);
						},
						onCellEdit:function(event,data,row){
							data['url'] = '<%=path%>/misbohcommon/findSupplyTop10.do?ex=N&spec1=Y';//不能查门店半成品
							//if(!isNaN(data.value))
								data['key'] = 'sp_code';
							//else
							//	data['key'] = 'sp_init';
							$.fn.autoGrid.ajaxEdit(data,row);
						},
						resultFormat:function(data){
							return data.sp_init+'-'+data.sp_code+'-'+data.sp_name;
						},
						afterEnter:function(data2,row){
							var num=0;
							$('.grid').find('.table-body').find('tr').each(function(){
								if($(this).find("td:eq(1)").text()==data2.sp_code){
									num=1;
								}
							});
							if(num==1){
								showMessage({
	 								type: 'error',
	 								msg: '<fmt:message key="added_supplies_remind"/>！',
	 								speed: 1000
	 							});
								return;
							}
							row.find("td:eq(1) span").text(data2.sp_code).data('sp_name',data2.sp_name);
							row.find("td:eq(2) span input").val(data2.sp_name).focus();
							row.find("td:eq(3) span").text(data2.sp_desc);
							row.find("td:eq(4) span").text('0.00').attr('title',0).css("text-align","right");
							row.find("td:eq(5) span").text(data2.spec1);
							row.find("td:eq(6) span").text(data2.spec2?'0.00':'').attr('title',0).css("text-align","right");
							row.find("td:eq(7) span").text(data2.spec2);
							row.find("td:eq(8) span").text(data2.spec3?'0.00':'').attr('title',0).css("text-align","right");
							row.find("td:eq(9) span").text(data2.spec3);
							row.find("td:eq(10) span").text(data2.spec4?'0.00':'').attr('title',0).css("text-align","right");
							row.find("td:eq(11) span").text(data2.spec4);
							row.find("td:eq(12) span").text('0.00').attr('title',0).css("text-align","right");
							row.find("td:eq(13) span").text(data2.unit);
							row.find("td:eq(14) span").text('0.00').attr('title',0).css("text-align","right");
							row.data("unitper1",data2.specUnitper1);
							row.data("unitper2",data2.specUnitper2);
							row.data("unitper3",data2.specUnitper3);
							row.data("unitper4",data2.specUnitper4);
							row.data("sp_init",data2.sp_init);
						}
					},{
						index:4,
						action:function(row,data2){
							if(Number(data2.value) < 0){
								alert('<fmt:message key="number_cannot_be_negative"/>！');
								row.find("td:eq(4)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,4);
							}else if(isNaN(data2.value)){
								alert('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(4)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,4);
							}else{
								row.find("td:eq(4)").find('span').attr('title',data2.value);
								getSumCnt(row);
								if(row.find("td:eq(7) span").text() == ''){
									$.fn.autoGrid.setCellEditable(row,14);
								}else{
									$.fn.autoGrid.setCellEditable(row,6);
								}
// 								getTotalSum();
							}
						}
					},{
						index:6,
						action:function(row,data2){
							if(Number(data2.value) < 0){
								alert('<fmt:message key="number_cannot_be_negative"/>！');
								row.find("td:eq(6)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,6);
							}else if(isNaN(data2.value)){
								alert('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(6)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,6);
							}else{
								row.find("td:eq(6)").find('span').attr('title',data2.value);
								//如果转换率是0的并且规格一有数量的，这里则必输
								if(Number(row.data('unitper2')) == 0 && Number(row.find("td:eq(4)").find('span').text()) != 0){
									if(data2.value == 0){
										alert('<fmt:message key="There_is_no_conversion_specification_will_not_null"/>！');//没有转换率的规格是必输项
										$.fn.autoGrid.setCellEditable(row,6);
										return;
									}
								}
								getSumCnt(row);
								if(row.find("td:eq(9) span").text() == ''){
									$.fn.autoGrid.setCellEditable(row,14);
								}else{
									$.fn.autoGrid.setCellEditable(row,8);
								}
// 								getTotalSum();
							}
						}
					},{
						index:8,
						action:function(row,data2){
							if(Number(data2.value) < 0){
								alert('<fmt:message key="number_cannot_be_negative"/>！');
								row.find("td:eq(8)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,8);
							}else if(isNaN(data2.value)){
								alert('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(8)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,8);
							}else{
								row.find("td:eq(8)").find('span').attr('title',data2.value);
								//如果转换率是0的并且规格一有数量的，这里则必输
								if(Number(row.data('unitper3')) == 0 && Number(row.find("td:eq(4)").find('span').text()) != 0){
									if(data2.value == 0){
										alert('<fmt:message key="There_is_no_conversion_specification_will_not_null"/>！');//没有转换率的规格是必输项
										$.fn.autoGrid.setCellEditable(row,8);
										return;
									}
								}
								getSumCnt(row);
								if(row.find("td:eq(11) span").text() == ''){
									$.fn.autoGrid.setCellEditable(row,14);
								}else{
									$.fn.autoGrid.setCellEditable(row,10);
								}
// 								getTotalSum();
							}
						}
					},{
						index:10,
						action:function(row,data2){
							if(Number(data2.value) < 0){
								alert('<fmt:message key="number_cannot_be_negative"/>！');
								row.find("td:eq(10)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,10);
							}else if(isNaN(data2.value)){
								alert('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(10)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,10);
							}else{
								row.find("td:eq(10)").find('span').attr('title',data2.value);
								//如果转换率是0的并且规格一有数量的，这里则必输
								if(Number(row.data('unitper4')) == 0 && Number(row.find("td:eq(4)").find('span').text()) != 0){
									if(data2.value == 0){
										alert('<fmt:message key="There_is_no_conversion_specification_will_not_null"/>！');//没有转换率的规格是必输项
										$.fn.autoGrid.setCellEditable(row,10);
										return;
									}
								}
								getSumCnt(row);
								$.fn.autoGrid.setCellEditable(row,14);
// 								getTotalSum();
							}
						}
					},{
						index:14,
						action:function(row,data2){
							if(Number(data2.value) < 0){
								alert('<fmt:message key="number_cannot_be_negative"/>！');
								row.find("td:eq(14)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,14);
							}else if(isNaN(data2.value)){
								alert('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(14)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,14);
							}else{
								row.find("td:eq(14)").find('span').attr('title',data2.value);
								if(!row.next().html())
									$.fn.autoGrid.addRow();
								$.fn.autoGrid.setCellEditable(row.next(),2);
// 								getTotalSum();
							}
						}
					}]
				});
			}
		  
		  //得到总数量的方法
		  function getSumCnt(row){
			  var sumCnt = Number(row.find("td:eq(4) span").attr('title'));
			  if(row.find("td:eq(7) span").text() != '' && Number(row.data('unitper2')) != 0){
				  sumCnt += Number(row.find("td:eq(6) span").attr('title'))/Number(row.data('unitper2'));
			  }
			  if(row.find("td:eq(9) span").text() != '' && Number(row.data('unitper3')) != 0){
				  sumCnt += Number(row.find("td:eq(8) span").attr('title'))/Number(row.data('unitper3'));
			  }
			  if(row.find("td:eq(11) span").text() != '' && Number(row.data('unitper4')) != 0){
				  sumCnt += Number(row.find("td:eq(10) span").attr('title'))/Number(row.data('unitper4'));
			  }
			  row.find("td:eq(12) span").text(sumCnt.toFixed(2)).attr('title',sumCnt).css('text-align','right');
		  }
		  //计算统计数据
		  function getTotalSum(){
				var sum_amount = 0; 
				var sum_totalamt = 0;
				$('.table-body').find('tr').each(function (){
					if($(this).find('td:eq(1)').text()!=''){//非空行
						var amount = $(this).find('td:eq(12) span').text();
						sum_amount += Number(amount);
						var totalamt = $(this).find('td:eq(14) span').text()?$(this).find('td:eq(14) span').text():$(this).find('td:eq(14)').find("input:eq(0)").val();
						sum_totalamt += Number(totalamt);
					}
				});
				$('#sum_num').text($(".table-body").find('tr').length);//总行数
				$('#sum_amount').text(Number(sum_amount).toFixed(2));//总数量
				$('#sum_totalamt').text(Number(sum_totalamt).toFixed(2));//总金额
			}
		  //导出
		    function exportCangkuInit(){
		    	var positnCode=$('#positn').val();
				if(positnCode==''){
					alert('<fmt:message key="please_select_positions"/>！');//please_select_positions
					return;
				}else{
					$("#wait2").val('NO');//不用等待加载
					$("#listForm").attr("action","<%=path%>/ckInitMis/exportCangkuInit.do");
					$('#listForm').submit();
					$("#wait2 span").html("数据导出中，请稍后...");
					$("#listForm").attr("action","<%=path%>/ckInitMis/ckInit.do");
					$("#wait2").val('');//等待加载还原
				}
		    }
		  
		  //导入
		  function importCangkuInit(){
			  var positn = $("#positn").val();
			  $('body').window({
					id: 'window_importInit',
					title: '<fmt:message key="import" /><fmt:message key="beginning_of_period" />',
					content: '<iframe id="importInit" frameborder="0" src="<%=path%>/ckInitMis/importInit.do?positn='+positn+'"></iframe>',
					width: '400px',
					height: '200px',
					draggable: true,
					isModal: true
				});
		  }
			  
		 
		  //期初状态查询
		  function qcZtChaxun(){
				var curwindow = $('body').window({
					id: 'window_searchChkinm',
					title: '<fmt:message key="beginning_of_period"/><fmt:message key="positn_state"/><fmt:message key="select"/>',
					content: '<iframe id="searchChkinmFrame" frameborder="0" src="<%=path%>/ckInitMis/qcZtchaxun.do"></iframe>',
					width: 480,
					height: 300,
					draggable: true,
					isModal: true
				});
			}
		</script>
	</body>
</html>