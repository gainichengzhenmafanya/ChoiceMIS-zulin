<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Basic Info</title>
<link type="text/css" rel="stylesheet"
	href="<%=path%>/css/lib.ui.core.css" />
<link type="text/css" rel="stylesheet"
	href="<%=path%>/css/widget/lib.ui.button.css" />
<link type="text/css" rel="stylesheet"
	href="<%=path%>/css/widget/lib.ui.toolbar.css" />
<style type="text/css">
.tool {
	position: relative;
	height: 27px;
}
</style>
</head>
<body>
	<div class="tool"></div>
	<form action="" id="import" method="post" enctype="multipart/form-data">
		<input type="hidden" id="positn" name="positn" value="${positn }"/>
		<div align="center" id="divImport">
			<FONT color=red>* </FONT>
			<fmt:message key="select_imported_file" />
			： <input type="file" style="WIDTH: 250px" maxLength=5 name="file"
				id="file" />
		</div>
	</form>
	<br>
	----------------------------------------------------------------------
	<br>
	<FONT color=red>导入注意事项： </FONT>
	<br> 1.导入格式不要变动!<FONT color=red>注意:如果是导出的格式,务必删掉合计数量和单位列! </FONT>
	<br> 2.只支持导入.xls格式的excel!

	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript"
		src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript"
		src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript">
			var tool;
			$(document).ready(function(){
				tool	= _tool();
				});
			
			var _tool=function(){
				return $('.tool').toolbar({
					items: [
					 {
						text: '<fmt:message key="import" />',
						title: '<fmt:message key="import" />期初数据',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							handle();
						}
					},
					{
						text: '<fmt:message key="template_download" />(数据来自分店物资属性)',
						title: '<fmt:message key="template_download" />(数据来自分店物资属性)',
						icon: {
							url: '<%=path%>/image/Button/excel.bmp'
						},
						handler: function(){
							$("#import").attr('action','<%=path%>/ckInitMis/exportCangkuInit.do?isTemplete=Y').submit();
						}
					}
				]
				});
				}
			function handle(){
				var file = $("#file").val();
				if(file == ''){
					alert('<fmt:message key="please_select_the_imported" />');
					return false;
				}
				 var pos = file.lastIndexOf(".");
				 var lastname = file.substring(pos,file.length);
				
			   if (!(lastname.toLowerCase()==".xls" || lastname.toLowerCase()==".xlsx")){
				     alert('<fmt:message key="type_of_file_uploaded" />'+lastname+'，<fmt:message key="type_of_file" />');
				     return false;
			   }else{
					$("#import").attr('action','<%=path%>/ckInitMis/loadExcel.do').submit();
			}
		}
	</script>
</body>
</html>