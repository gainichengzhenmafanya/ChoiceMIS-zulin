<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>选择盘点</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
	</head>
	<body>
		<div class="tool"></div>
		<form id="listForm" action="<%=path%>/inventoryMis/chooseInventory.do" method="post">
			<input  type="hidden" name="firm" id="firm" value="${chkstore.firm}"/>
			<input  type="hidden" name="dept" id="dept" value="${chkstore.dept}"/>
			<div class="form-line">
				<div class="form-label"><fmt:message key="startdate"/></div>
				<div class="form-input"><input type="text" id="bdate" name="bdate" class="Wdate text" value="${chkstore.bdate }"/></div>
				<div class="form-label"><fmt:message key="enddate"/></div>
				<div class="form-input"><input type="text" id="edate" name="edate" class="Wdate text" value="${chkstore.edate }"/></div>
				<div class="form-label"><fmt:message key="inventory_type"/></div>
				<div class="form-input">
					<select id="pantyp" name="pantyp" style="margin-top:3px;">
						<option value="" <c:if test="${empty chkstore.pantyp}">selected="selected"</c:if>><fmt:message key="all"/></option>
						<option value="daypan" <c:if test="${chkstore.pantyp == 'daypan'}">selected="selected"</c:if>><fmt:message key="daypan"/></option>
						<option value="weekpan" <c:if test="${chkstore.pantyp == 'weekpan'}">selected="selected"</c:if>><fmt:message key="weekpan"/></option>
						<option value="monthpan" <c:if test="${chkstore.pantyp == 'monthpan'}">selected="selected"</c:if>><fmt:message key="monthpan"/></option>
					</select>
				</div>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:16px;">&nbsp;</span></td>
								<td><span style="width:20px;"></span></td>
								<td><span style="width:100px;"><fmt:message key="positions"/></span></td>
								<td><span style="width:80px;">盘点日期</span></td>
								<td><span style="width:60px;"><fmt:message key="inventory_type"/></span></td>
								<td><span style="width:80px;"><fmt:message key="the_inventorier"/></span></td>
								<td><span style="width:130px;"><fmt:message key="inventory_time"/></span></td>
								<td><span style="width:80px;"><fmt:message key="the_auditor"/></span></td>
								<td><span style="width:130px;"><fmt:message key="audit_time"/></span></td>
								<td><span style="width:80px;">是否来自模板</span></td>
							</tr>
						</thead>
					</table>
				</div> 
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="co" varStatus="step" items="${chkstoreList}">
								<tr>
									<td class="num" ><span style="width:16px;">${step.count}</span></td>
									<td style="width:25px; text-align: center;">
										<span style="width:20px;"><input type="checkbox" name="idList" id="chk_${co.chkstorefid}" value="${co.chkstorefid}"/></span>
									</td>
									<td><span title="${co.dept}" style="width:100px;">${co.dept}&nbsp;</span></td>
									<td><span title="${co.workDate}" style="width:80px;">${co.workDate}&nbsp;</span></td>
									<td><span title="${co.pantyp}" style="width:60px;">${co.pantyp}&nbsp;</span></td>
									<td><span title="${co.ecode }" style="width:80px;">${co.ecode }&nbsp;</span></td>
									<td><span title="${co.inputDate}" style="width:130px;">${co.inputDate}&nbsp;</span></td>
									<td><span title="${co.checkCode }" style="width:80px;">${co.checkCode }&nbsp;</span></td>
									<td><span title="${co.checkDate}" style="width:130px;">${co.checkDate}&nbsp;</span></td>
									<td><span title="${co.fromtyp}" style="width:80px;">
										<c:choose>
											<c:when test="${co.fromtyp == 'template' }">是</c:when>
											<c:otherwise>否</c:otherwise>											
										</c:choose>&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		var validate;
			$(document).ready(function(){
				$('#bdate').bind('click',function(){
					new WdatePicker();
				});
				$('#edate').bind('click',function(){
					new WdatePicker();
				});
				setElementHeight('.grid',['.tool'],$(document.body),60);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$('.grid').find('.table-body').find('tr').click(function(){
					$('input[type=checkbox]').attr('checked',false);
					$(this).find('input[type=checkbox]').attr('checked',true);
				});
				//双击直接选择
				$('.grid').find('.table-body').find('tr').dblclick(function(){
					var fromtyp = $(this).find('td:eq(9)').find('span').attr('title');
					var confmsg = '是否确定使用选中的盘点为基础进行本次盘点？';
					if(fromtyp == 'template'){
						confmsg += '\n注意：如果来源是模板的盘点单，则只会盘点模板中的物资。';
					}
					if(confirm(confmsg)){
						var chkstorefid = $(this).filter(':checked').first().val();
						parent.selectChkstoreo(chkstorefid,fromtyp);
					}
				});
			});	
			var tool = $('.tool').toolbar({
				items: [{
					text: '<fmt:message key="select" />',
					title: '<fmt:message key="select"/>',
					useable: true,
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-20px']
					},
					handler: function(){
						$('#listForm').submit();
					}
				},{
					text: '<fmt:message key="enter" /><fmt:message key="select1" />',
					title: '<fmt:message key="enter"/><fmt:message key="select1" />',
					useable: true,
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-20px']
					},
					handler: function(){
						selectChkstoreo();
					}
				},{
					text: '<fmt:message key="cancel"/>',
					title: '<fmt:message key="cancel"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						parent.$('.close').click();
					}
				}]
			});
			
			//选择此次盘点
			function selectChkstoreo(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() == 1){
					var fromtyp = $(checkboxList.filter(':checked')).parents('tr').find('td:eq(9)').find('span').attr('title');
					var confmsg = '是否确定使用选中的盘点为基础进行本次盘点？';
					if(fromtyp == 'template'){
						confmsg += '\n注意：如果来源是模板的盘点单，则只会盘点模板中的物资。';
					}
					if(confirm(confmsg)){
						var chkstorefid = checkboxList.filter(':checked').first().val();
						parent.selectChkstoreo(chkstorefid,fromtyp);
					}
				}else{
					alert('<fmt:message key="please_select_single_message"/>！');
					return ;
				}
			}
			function pageReload(){
			    $('#listForm').submit();
			}
		</script>
	</body>
</html>