<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="purchase_the_template" /></title>
<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
<style type="text/css">
.blueBGColor{background-color:	#F1F1F1;}
.redBGColor{background-color:	#F1F1F1;}
.onEdit{
	background:lightblue;
	border:1px solid;
	border-bottom-color: blue;
	border-top-color: blue;
	border-left-color: blue;
	border-right-color: blue;
}
.input{
	background:transparent;
	border:1px solid;
}
.redClass{
	color:red;
}
</style>		
</head>
<body>
	<div class="tool"></div>
	<form id="listForm" action="<%=path%>/inventoryMis/addInventoryDemo.do" method="post">
		<div class="form-line">	
			<div class="form-label"><fmt:message key="title" /></div>
			<div class="form-input">
				<input type="hidden" id="status" name="status" value="${status}"/>
				<input type="hidden" id="firm" name="firm" value="${firm}"/>
				<select class="select" id="chkstodemono" name="chkstodemono" onchange="findByTitle(this);">
					<c:forEach var="chkstodemo" items="${listTitle}" varStatus="status">
						<option
							<c:if test="${chkstodemo.chkstodemono==chkstodemono}"> selected="selected" </c:if> value="${chkstodemo.chkstodemono}">${chkstodemo.title}
						</option>
					</c:forEach>				
				</select>
			</div>
		</div>
	
		<div class="grid">		
			<div class="table-head" >
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<td rowspan="2" class="num"><span style="width: 25px;"></span></td>
							<td rowspan="2"><span style="width:80px;"><fmt:message key="supplies_code" /></span></td>
							<td rowspan="2"><span style="width:120px;"><fmt:message key="supplies_name" /></span></td>
							<td rowspan="2"><span style="width:80px;"><fmt:message key="specification" /></span></td>
							<td colspan="2"><span><fmt:message key="scm_pandian"/></span></td>
							<td colspan="4"><span>录入</span></td>
						</tr>
						<tr>
							<td><span style="width:60px;"><fmt:message key="scm_pandian"/><fmt:message key="quantity"/></span></td>
							<td><span style="width:40px;"><fmt:message key="scm_pandian"/><br/><fmt:message key="unit"/></span></td>
							<td><span style="width:60px;"><fmt:message key="quantity"/></span></td>
							<td><span style="width:40px;"><fmt:message key="procurement_unit_br"/></span></td>
							<td><span style="width:60px;"><fmt:message key="quantity"/></span></td>
							<td><span style="width:40px;"><fmt:message key="standard_unit_br"/></span></td>
						</tr>
					</thead>
				</table>
			</div>				
			<div class="table-body">
				<table cellspacing="0" cellpadding="0">
					<tbody>
						<c:forEach var="chkstodemo" items="${chkstodemoList}" varStatus="status">
							<tr data-unitper3="${chkstodemo.supply.unitper3 }" data-unitper4="${chkstodemo.supply.unitper4 }" data-ex="${chkstodemo.supply.ex }"
								data-stockcnt="0">
								<td class="num"><span style="width: 25px;">${status.index+1}</span></td>
								<td title="${chkstodemo.supply.sp_code}"><span  style="width:80px;">${chkstodemo.supply.sp_code}</span></td>
								<td title="${chkstodemo.supply.sp_name}"><span  style="width:120px;">${chkstodemo.supply.sp_name}</span></td>
								<td title="${chkstodemo.supply.sp_desc}"><span  style="width:80px;">${chkstodemo.supply.sp_desc}</span></td>
								<td><span style="width:60px;text-align:right;" title="0">0.00</span></td>
								<td><span style="width:40px;">${chkstodemo.supply.unit4}</span></td>
								<td><span style="width:60px;text-align:right;" title="0">0.00</span></td>
								<td><span style="width:40px;">${chkstodemo.supply.unit3}</span></td>
								<td><span style="width:60px;text-align:right;" title="0">0.00</span></td>
								<td><span style="width:40px;">${chkstodemo.supply.unit}</span></td>
								
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</form>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
	<script type="text/javascript">
	//ajax同步设置
	$.ajaxSetup({
		async: false
	});
	var validate;
	//工具栏
	$(document).ready(function(){
		//按钮快捷键
		focus() ;//页面获得焦点
		
		//屏蔽鼠标右键
	 	$(document).bind('keyup',function(e){
	 		if(e.keyCode==27){
	 			parent.$('.close').click();
	 		}
	 		//表格数量一变，校验一下，价格就接着变
	 		if($(e.srcElement).is("input")){//对表格内的输入框进行判读，延迟600毫秒
	 			var index=$(e.srcElement).closest('td').index();
	 			if($(e.srcElement).is("input")){
		    		validator($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
		    	}
	    	}
		});
	 	
	   $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
	   $('.grid').find('.table-body').find('tr').hover(
			function(){
				$(this).addClass('tr-over');
			},
			function(){
				$(this).removeClass('tr-over');
			}
		);
		
		//自动实现滚动条
		setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
		setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
		loadGrid();//  自动计算滚动条的js方法	
		editCells();
		$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),6);
	});
	
	//动态下拉列表框
	function findByTitle(e){
		$('#listForm').submit();
	}
	//确认修改
	function enterUpdate() {
		if(parent.$(".table-body").find('tr').size() > 0){
			alert('选择模板只会盘点此模板的物资！');
			parent.$(".table-body").find('tr').remove();
		}
		parent.editCells();
		parent.loadToolBar([false,false,false,true]);
		var checkboxList = $('.grid').find('.table-body').find('tr');
		checkboxList.each(function(i){
			if(parent.$(".table-body").find("tr:last").find("td:eq(1)").find('span').text() != ''){
				parent.$(".table-body").autoGrid.addRow();
			}
			var unitper3 = $(this).data('unitper3');
			var unitper4 = $(this).data('unitper4');
			var stockcnt = $(this).data('stockcnt');
			var cnt4 = Number($(this).find('td:eq(4) span').attr('title'));
			var cnt3 = Number($(this).find('td:eq(6) span').attr('title'));
			var cnt1 = Number($(this).find('td:eq(8) span').attr('title'));
			parent.$(".table-body").find("tr:last").find("td:eq(0)").find('span').attr('style','width:30px;');
			parent.$(".table-body").find("tr:last").find("td:eq(1)").find('span').text($(this).find('td:eq(1) span').text()).data('sp_name',$(this).find('td:eq(2) span').text());
			parent.$(".table-body").find("tr:last").find("td:eq(2)").find('span').text($(this).find('td:eq(2) span').text());
			parent.$(".table-body").find("tr:last").find("td:eq(3)").find('span').text($(this).find('td:eq(3) span').text());
			parent.$(".table-body").find("tr:last").find("td:eq(4)").find('span').text(Number(cnt4).toFixed(2)).attr('title',cnt4).css("text-align","right");
			parent.$(".table-body").find("tr:last").find("td:eq(5)").find('span').text($(this).find('td:eq(5)').text());
			parent.$(".table-body").find("tr:last").find("td:eq(6)").find('span').text(Number(cnt3).toFixed(2)).attr('title',cnt3).css("text-align","right");
			parent.$(".table-body").find("tr:last").find("td:eq(7)").find('span').text($(this).find('td:eq(7)').text());
			parent.$(".table-body").find("tr:last").find("td:eq(8)").find('span').text(Number(cnt1).toFixed(2)).attr('title',cnt1).css("text-align","right");
			parent.$(".table-body").find("tr:last").find("td:eq(9)").find('span').text($(this).find('td:eq(9)').text());
			parent.$(".table-body").find("tr:last").find("td:eq(10)").find('span').text($(this).data('ex') == 'Y'?'是':'否');
			parent.$(".table-body").find("tr:last").find("td:eq(11)").find('span').text(i+1);
			var html = "<select style='width:40px;height:18px;'><option value='Y'>否</option><option value='N'>是</option></select>";
			parent.$(".table-body").find("tr:last").find("td:eq(12)").find('span').html(html);
			parent.$(".table-body").find("tr:last").data("unitper3",unitper3);
			parent.$(".table-body").find("tr:last").data("unitper4",unitper4);
			parent.$(".table-body").find("tr:last").data("stockcnt",stockcnt);
			if(Number(cnt4)>0){
				parent.$(".table-body").find("tr:last").addClass('redClass');
			}
		});
	    //关闭弹出窗口
		parent.$('.close').click();
	}
	//编辑表格
	function editCells(){
		if($(".table-body").find('tr').length == 0){
			return;
		}
		$(".table-body").autoGrid({
			initRow:1,
			colPerRow:10,
			widths:[26,80,120,80,70,50,70,50,70,50],
			colStyle:['','','','',{background:"#F1F1F1"},''],
			VerifyEdit:{verify:true,enable:function(cell,row){
				return row.find('td').index(cell) == 2 || (row.find("td:eq(2)").text() == null || $.trim(row.find("td:eq(2)").text()) != '' ) ? true : false;
			}},
			onEdit:$.noop,
			editable:[6,8],//能输入的位置
			onLastClick:$.noop,
			onEnter:function(data){
				$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.value) ;
			},
			cellAction:[{
				index:6,
				action:function(row,data2){
					if(Number(data2.value) < 0){
						alert('<fmt:message key="number_cannot_be_negative"/>！');
						row.find("td:eq(6)").find('span').text(data2.ovalue);
						$.fn.autoGrid.setCellEditable(row,6);
					}else if(isNaN(data2.value)){
						alert('<fmt:message key="number_be_not_number"/>！');
						row.find("td:eq(6)").find('span').text(data2.ovalue);
						$.fn.autoGrid.setCellEditable(row,6);
					}else{
						row.find("td:eq(6)").find('span').attr('title',data2.value);
						getSumCnt(row);
						$.fn.autoGrid.setCellEditable(row,8);
					}
				}
			},{
				index:8,
				action:function(row,data2){
					if(Number(data2.value) < 0){
						alert('<fmt:message key="number_cannot_be_negative"/>！');
						row.find("td:eq(8)").find('span').text(data2.ovalue);
						$.fn.autoGrid.setCellEditable(row,8);
					}else if(isNaN(data2.value)){
						alert('<fmt:message key="number_be_not_number"/>！');
						row.find("td:eq(8)").find('span').text(data2.ovalue);
						$.fn.autoGrid.setCellEditable(row,8);
					}else{
						row.find("td:eq(8)").find('span').attr('title',data2.value);
						getSumCnt(row);
						$.fn.autoGrid.setCellEditable(row.next(),6);
					}
				}
			}]
		});
	}
	//得到总数量的方法
	  function getSumCnt(row){
		  var sumCnt = Number(row.find("td:eq(8) span").attr('title'));//标准单位
		  var cnt3 = Number(row.find("td:eq(6) span").attr('title'));//采购单位
		  if(Number(row.data('unitper3')) != 0){
			  sumCnt += cnt3/Number(row.data('unitper3'));
		  }
		  row.data("stockcnt",sumCnt);//标准单位
		  row.find("td:eq(4) span").text((sumCnt*Number(row.data('unitper4'))).toFixed(2)).attr(sumCnt*Number(row.data('unitper4')));
		  if(sumCnt != 0){
			  row.addClass('redClass');
		  }else{
			  row.removeClass('redClass');
		  }
	  }
	
		//校验
	  function validator(weiz,row,data){
		  row.find("input").data("ovalue",data.value);
			if(weiz=="6"){
				if(Number(data.value) < 0){
					alert('<fmt:message key="number_cannot_be_negative"/>！');
					row.find("td:eq(6)").find('span').text(data.ovalue);
					$.fn.autoGrid.setCellEditable(row,6);
				}else if(isNaN(data.value)){
					alert('<fmt:message key="number_be_not_number"/>！');
					row.find("td:eq(6)").find('span').text(data.ovalue);
					$.fn.autoGrid.setCellEditable(row,6);
				}else{
					row.find("td:eq(6)").find('span').attr('title',data.value);
					getSumCnt(row);
				}
			}else if(weiz=="8"){
				if(Number(data.value) < 0){
					alert('<fmt:message key="number_cannot_be_negative"/>！');
					row.find("td:eq(8)").find('span').text(data.ovalue);
					$.fn.autoGrid.setCellEditable(row,8);
				}else if(isNaN(data.value)){
					alert('<fmt:message key="number_be_not_number"/>！');
					row.find("td:eq(8)").find('span').text(data.ovalue);
					$.fn.autoGrid.setCellEditable(row,8);
				}else{
					row.find("td:eq(8)").find('span').attr('title',data.value);
					getSumCnt(row);
				}
			}
		}

	</script>
</body>
</html>