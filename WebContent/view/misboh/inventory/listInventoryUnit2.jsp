<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>inventory Info 盘点</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<style type="text/css">
				.search{
					margin-top:3px;
					cursor: pointer;
				}
				.page{
					margin-bottom: 25px;
				}
				.seachSupply{
					margin-top:3px;
					cursor: pointer;
				}
				.redClass{
					color:red;
				}
			</style>						
		</head>
	<body>
		<div class="tool">
		</div>
		<input type="hidden" id="guideConfig" value="${guideConfig }"/>
		<input type="hidden" id="save" value="${inventory.save }"/>
		<form id="listForm" name="listForm" action="<%=path%>/inventoryMis/list.do"method="post">
		<input type="hidden" id="firm" name="firm" value="${inventory.firm }" />
<%-- 		<input type="hidden" id="type" name="type" value="<c:out value="${type}" default="init"/>" /> --%>
		<input type="hidden" id="status" name="status" value="<c:out value="${inventory.positn.pd}" default="init"/>" /><!--status是否已盘点 y  n -->
		<input type="hidden" id="yearr" name="yearr" value="${inventory.yearr}" /><!-- 会计年 -->
		<input type="hidden" id="monthpan" name="monthpan" value="${inventory.monthpan}" /><!-- 是否月盘 -->
		<div class="form-line">
			<div class="form-label"><fmt:message key="date"/>：</div>
			<div class="form-input">
				<input type="text" id="date" name="date" class="Wdate text" 
					<c:if test="${action != 'init' }">readonly="readonly"</c:if> <c:if test="${action == 'init' }">onfocus="WdatePicker();"</c:if>
				value="<fmt:formatDate value="${inventory.date}" pattern="yyyy-MM-dd"/>" />
			</div>
			<input type="hidden" id="positn" name="positn.code" value="${inventory.positn.code}"/>
			<c:if test="${!empty inventory.firm }">
				<div class="form-label"><span style="color:red;">*</span><fmt:message key="positions"/>：</div>
				<div class="form-input">
					<div class="form-input">
						<input type="text" name="positn.des" id="positn_name" class="text" readonly="readonly" value="<c:out value="${inventory.positn.des}" />" style="width:120px;margin-bottom:5px;"/>
						<img id="seachDept" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
					</div>
				</div>
			</c:if>
			<div class="form-label"><span style="color:red;">*</span><fmt:message key="scm_pandian"/><fmt:message key="type"/>：</div>
			<div class="form-input" style="width:50px;">
				<select id="pantyp" name="pantyp" style="margin-top:3px;" <c:if test="${inventory.monthpan == 'Y' }">disabled="disabled"</c:if>>
					<option value="daypan" <c:if test="${inventory.pantyp == 'daypan' }">selected="selected"</c:if>>日盘</option>
					<option value="weekpan" <c:if test="${inventory.pantyp == 'weekpan' }">selected="selected"</c:if>>周盘</option>
					<option value="monthpan" <c:if test="${inventory.pantyp == 'monthpan' }">selected="selected"</c:if>>月盘</option>
				</select>
			</div>
			<div class="form-label" ><input type="checkbox" name="ynpd" value="N" <c:if test="${!empty inventory.ynpd }">checked="checked"</c:if>/><fmt:message key="display"/><fmt:message key="The_forbidden_dish"/></div><!-- 显示禁盘 -->
			<input type="hidden" name="ynprint" value="N" />
			<div class="form-label" ><input type="checkbox" id="onlyQuery" name="onlyQuery" value="Y" <c:if test="${!empty inventory.onlyQuery }">checked="checked"</c:if>/>仅<fmt:message key="select"/></div><!-- 显示禁盘 -->
		</div>
		<c:if test="${!empty chkstoref}">
			<div class="bj_head">
				<div class="form-line">
					<div class="form-label">是否审核：</div>
					<div class="form-input" style="width:120px;">${chkstoref.state == '1'?'已审核':'未审核'}</div>
					<div class="form-label">盘点人：</div>
					<div class="form-input" style="width:120px;">${chkstoref.ecodeName }</div>
					<div class="form-label">盘点时间：</div>
					<div class="form-input" style="width:120px;">${chkstoref.inputDate }</div>
				</div>
				<div class="form-line">
					<div class="form-label"></div>
					<div class="form-input" style="width:120px;"></div>
					<div class="form-label">审核人：</div>
					<div class="form-input" style="width:120px;">${chkstoref.checkCodeName }</div>
					<div class="form-label">审核时间：</div>
					<div class="form-input" style="width:120px;">${chkstoref.checkDate }</div>
				</div>
			</div>
		</c:if>
		<!-- 修改ie8下条件一行显示不全，换行显示 wjf-->
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" rowspan="2"><span style="width: 30px;">&nbsp;</span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="supplies_code"/></span></td>
								<td rowspan="2"><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="supplies_specifications"/></span></td>
								<td colspan="2"><span><fmt:message key="scm_pandian"/></span></td>
								<td colspan="4"><span>盘点录入数量(要货+标准)</span></td>
								<td rowspan="2"><span style="width:40px;"><fmt:message key="whether_semi"/></span></td><!-- 半成品 -->
								<td rowspan="2"><span style="width:40px;"><fmt:message key="serial_number"/></span></td><!-- 顺序号 -->
								<td rowspan="2"><span style="width:40px;"><fmt:message key="The_forbidden_dish"/></span></td><!-- 禁盘  -->
							</tr>
							<tr>
								<td><span style="width:60px;"><fmt:message key="total_number"/></span></td>
								<td><span style="width:40px;"><fmt:message key="scm_pandian"/><br/><fmt:message key="unit"/></span></td>
								<td><span style="width:60px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:40px;">要货<br/>单位</span></td>
								<td><span style="width:60px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:40px;"><fmt:message key="standard_unit_br"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" id="table-body">
						<tbody>
							<c:forEach var="inventory" items="${inventoryList}" varStatus="status">
								<tr data-unitper3="${inventory.unitper3}" data-unitper4="${inventory.unitper4}" data-stockcnt="${inventory.stockcnt}"
									<c:if test="${inventory.cnt4 != 0 }">class="redClass"</c:if>>
									<td class="num" ><span title="${status.index+1}" style="width:30px;">${status.index+1}</span></td>
									<td><span title="${inventory.sp_code}" style="width:70px;" data-sp_name="${inventory.sp_name}">${inventory.sp_code}</span></td>
									<td edit="false"><span title="${inventory.sp_name}" style="width:100px;">${inventory.sp_name}</span></td>
									<td><span title="${inventory.sp_desc}" style="width:80px;">${inventory.sp_desc}</span></td>
<%-- 									<td><span title="${inventory.cnt1}" style="width:60px;text-align: right;"><fmt:formatNumber value="${inventory.cnt1}" pattern="##.##" minFractionDigits="2"/></span></td> --%>
<%-- 									<td><span title="${inventory.spec1}" style="width:40px;">${inventory.spec1}</span></td> --%>
<!-- 									<td> -->
<%-- 										<span title="${inventory.cnt2}" style="width:60px;text-align: right;"> --%>
<%-- 											<c:if test="${!empty inventory.spec2 }"> --%>
<%-- 												<fmt:formatNumber value="${inventory.cnt2}" pattern="##.##" minFractionDigits="2"/> --%>
<%-- 											</c:if> --%>
<!-- 										</span> -->
<!-- 									</td> -->
<%-- 									<td><span title="${inventory.spec2}" style="width:40px;">${inventory.spec2}</span></td> --%>
<!-- 									<td> -->
<%-- 										<span title="${inventory.cnt3}" style="width:60px;text-align: right;"> --%>
<%-- 											<c:if test="${!empty inventory.spec3 }"> --%>
<%-- 												<fmt:formatNumber value="${inventory.cnt3}" pattern="##.##" minFractionDigits="2"/> --%>
<%-- 											</c:if> --%>
<!-- 										</span> -->
<!-- 									</td> -->
<%-- 									<td><span title="${inventory.spec3}" style="width:40px;">${inventory.spec3}</span></td> --%>
<!-- 									<td> -->
<%-- 										<span title="${inventory.cnt4}" style="width:60px;text-align: right;"> --%>
<%-- 											<c:if test="${!empty inventory.spec4 }"> --%>
<%-- 												<fmt:formatNumber value="${inventory.cnt4}" pattern="##.##" minFractionDigits="2"/> --%>
<%-- 											</c:if> --%>
<!-- 										</span> -->
<!-- 									</td> -->
<%-- 									<td><span title="${inventory.spec4}" style="width:40px;">${inventory.spec4}</span></td> --%>
									<td><span title="${inventory.cnt4}" style="width:60px;text-align: right;"><fmt:formatNumber value="${inventory.cnt4}" pattern="##.##" minFractionDigits="2"/></span></td>
									<td><span title="${inventory.unit4}" style="width:40px;">${inventory.unit4}</span></td>
									<td><span title="${inventory.cnt3}" style="width:60px;text-align: right;"><fmt:formatNumber value="${inventory.cnt3}" pattern="##.##" minFractionDigits="2"/></span></td>
									<td><span title="${inventory.unit3}" style="width:40px;">${inventory.unit3}</span></td>
									<td><span title="${inventory.cnt1}" style="width:60px;text-align: right;"><fmt:formatNumber value="${inventory.cnt1}" pattern="##.##" minFractionDigits="2"/></span></td>
									<td><span title="${inventory.unit}" style="width:40px;">${inventory.unit}</span></td>
									<td>
										<span title="${inventory.ex}" style="width:40px;">
											<c:choose>
												<c:when test="${inventory.ex == 'Y'}">
													<fmt:message key="be"/><!-- 是 -->
												</c:when>
												<c:otherwise>
													<fmt:message key="no1"/><!-- 否 -->
												</c:otherwise>
											</c:choose>
										</span></td>
									<td><span title="${inventory.orderNum}" style="width:40px;">${inventory.orderNum}</span></td>
									<td>
										<span title="${inventory.ynpd}" style="width:40px;">
											<select style='width:40px;height:18px;'>
												<option value='Y' <c:if test="${inventory.ynpd == 'Y' }">selected="selected"</c:if>><fmt:message key="no1"/><!-- 否 --></option>
												<option value='N' <c:if test="${inventory.ynpd == 'N' }">selected="selected"</c:if>><fmt:message key="be"/><!-- 是 --></option>
											</select>
										</span>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<div class="form-line">
				<div class="form-label" style="width:50px;"><fmt:message key="smallClass"/>：</div>
				<div class="form-input" style="margin-top:-4px;">
					<input type="text" id="typDes" class="text"  name="typdes" readonly="readonly" value="${inventory.typdes}"/>
					<input type="hidden" id="sp_type" name="sp_type" value="${inventory.sp_type}"/>
					<img id="seachTyp" style="margin-top:1px;" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_category"/>' />
			    </div>
				<div class="form-label" style="width:50px;"><fmt:message key="coding"/>：</div>
				<div class="form-input" style="margin-top:-4px;">
					<input type="text" class="text" id="sp_code" name="sp_code" value="${inventory.sp_code}" />
	                <img id="seachSupply" class="seachSupply" src="<%=path%>/image/themes/icons/search.png" alt="<fmt:message key="query_supplies"/>" /><!-- 查询物资 -->
				</div>
				<div class="form-input" style="color:red;">
					【盘点总数量 = 要货数量 + 标准数量 ，单位之间转换，程序自动计算，无需人工计算】
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/autoTableOnlyAdd.js"></script>
		<script type="text/javascript">
		var guideConfig = $('#guideConfig').val();
		if(null != guideConfig && '' != guideConfig){
			alert('当前门店不能进行盘点,原因如下:\n'+guideConfig);
			invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
		}
		//如果导入报货单报错了。。。wjf
		var importError = '<c:out value="${importError}"/>';
		if(importError != null && importError != '' && importError != 'undefined'){
			alert('<fmt:message key="Import_failed_The_reason"/>：\n'+importError);
		}
		var defaultCode = '';
		var defaultName = '';
		//ajax同步设置
// 		$.ajaxSetup({
// 			async: false
// 		});
		var nScrollHeight=0;
		var nScrollTop=0;
		var returnInfo = true;
		var pageSize = 0;
		
		var height = 80;
			$(document).ready(function(){
			 	//键盘事件绑定
			 	$(document).bind('keyup',function(e){
			 		if($(e.srcElement).is("input")){
			    		validator($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
			    	}
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit"/>').click();
			 		}
				}); 
				
				focus() ;//页面获得焦点
				if($("#onlyQuery").attr('checked') == 'checked'){
					loadToolBar([false,false,false,false,false]);
					$("#date").click(function(){
		  	 			new WdatePicker();
		  	 		});
					if('${!empty chkstoref}' == 'true'){
						height = 140;
					}
				}else{
				 	var status = $("#status").val();
				 	if(status == 'Y'){
						loadToolBar([false,true,false,false,true]);
				 	}else if(status == 'N'){
				 		if($('#save').val() == 'save'){//有未审核的
				 			loadToolBar([true,false,true,false,true]);
				 			if(confirm('查看当天盘点应产率?')){
				 				showYCL();
				 			}
				 		}else{
				 			loadToolBar([false,false,true,false,true]);
				 		}
				 	}else{
				 		loadToolBar([false,false,false,false,true]);
				 	}
				}
				if($('#save').val() == 'imp'){
		 			loadToolBar([false,false,false,true,true]);
		 			editCells();
		 		}
				
				setElementHeight('.grid',['.tool'],$(document.body),height);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				
				/*弹出部门选择树*/
				$('#seachDept').bind('click.custom',function(e){
					if(!!!top.customWindow){
						defaultCode = $('#positn').val();
						defaultName = $('#positn_name').val();
						//alert(defaultCode+"==="+defaultName);
						var offset = getOffset('positn');
						top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/misbohcommon/findPositnSuper.do?typn=7&iffirm=1&mold=one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positn_name'),$('#positn'),'760','520','isNull',select);
					}
				});
				/*弹出树*/
				$('#seachTyp').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode1 = $('#sp_type').val();
						var offset = getOffset('positn');
						top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/misbohcommon/selectMoreGrpTyp.do?defaultCode='+defaultCode1),offset,$('#typDes'),$('#sp_type'),'320','460','isNull');
					}
				});
				$('#seachSupply').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode1 = $('#sp_code').val();
						top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/misbohcommon/selectSupplyLeft.do?positn=1&defaultCode='+defaultCode1,$('#sp_code'));	
					}
				});
			});
			
			function loadToolBar(use){
				$('.tool').html('');
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							
							handler: function(){
								select();
							}
						},{
							text: '<fmt:message key="template" />',
							title: '<fmt:message key="template"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[4],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							
							handler: function(){
// 								if(!checkBeforeInventory())//盘点前判断
// 									return;
								addInventoryDemo();
							}
						},'-',{
							text: '<fmt:message key="scm_start_pandian"/>',
							title: '<fmt:message key="scm_start_pandian"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[1],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
// 								if(!checkBeforeInventory())//盘点前判断
// 									return;
								$('#status').val('start');//开始盘点
								$('#listForm').submit();
							}
						},{
							text: '<fmt:message key="edit"/>',
							title: '<fmt:message key="edit"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[2],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								editCells();
								loadToolBar([false,false,false,true,true]);
								setTimeout('keepSessionAjax()',300000);
							}
						},{
							text: '<fmt:message key="save"/>',
							title: '<fmt:message key="save"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[3],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-80px']
							},
							handler: function(){ 
								resolveInventory();
							}
						},{
							text: '结束<fmt:message key="scm_pandian"/>',//结束盘点
							title: '结束<fmt:message key="scm_pandian"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')}&&use[0],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
// 								if(!checkBeforeInventory())//盘点前判断
// 									return;
								endInventory();
							}
						},'-',{
							text: '<fmt:message key="import" />',
							title: '<fmt:message key="import" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'import')}&&use[4],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-20px']
							},
							handler: function(){
// 								if(!checkBeforeInventory())//盘点前判断
// 									return;
								importInventory();
							}
						},{
							text: '<fmt:message key="export" />',
							title: '<fmt:message key="export" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-20px']
							},
							handler: function(){
								exportInventory();
							}
						},{
							text: '<fmt:message key="print" />',
							title: '<fmt:message key="print" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-20px']
							},
							handler: function(){
								printInventory();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}
					]
				});
			}
			
			function select(){
				var positnCode=$('#positn').val();
				if(positnCode==''){
					alert('<fmt:message key="please_select_positions"/>！');//请选择仓位
					return;
				}
				//判断有没有期初
				$.ajax({
					url:"<%=path%>/misbohcommon/checkQC.do?code="+positnCode,
					type:"post",
					success:function(data){
						if(data){
// 							if(!checkBeforeInventory())//盘点前判断
// 								return;
							$('#listForm').submit();
						}else{
							var msg = '本门店未做期初!';
							if($('#firm').val()){
								msg = '['+$("#positn_name").val()+']未做期初！';
							}
							alert(msg);//该门店还未做期初！
							$('#positn').val(defaultCode);
							$("#positn_name").val(defaultName);
							return;
						}
					}
				});
			}
			
			//盘点前判断
			function checkBeforeInventory(){
				$('#wait').show();
				$('#wait2').show();
				var flag = false;
				var date = $('#date').val();
				$.ajax({
					url:'<%=path%>/inventoryMis/checkBeforeInventory.do?edate='+date,
					type:"post",
					async:false,
					success:function(data){
						var rs = eval('('+data+')');
						if(rs.pr == 1){
							flag = true;
						}else{
							flag = false;
							alert(rs.error);
						}
						$('#wait').hide();
						$('#wait2').hide();
					}
				});
				return flag;
			}
			
			//编辑表格
			function editCells() {
				$(".table-body").autoGrid({
					initRow:1,
					colPerRow:13,
					widths:[40,80,110,90,70,50,70,50,70,50,50,50,50],
					colStyle:['','',{background:"#F1F1F1"},'','','',{background:"#F1F1F1"},'',{background:"#F1F1F1"},'','',{background:"#F1F1F1"},{background:"#F1F1F1"}],
					VerifyEdit:{verify:true,enable:function(cell,row){
						return row.find('td').index(cell) == 2 || (row.find("td:eq(2)").text() == null || $.trim(row.find("td:eq(2)").text()) != '' ) ? true : false;
					}},
					onEdit:$.noop,
					editable:[2,6,8,11],
					onLastClick:function(row){
// 						getTotalSum();
					},
					onEnter:function(data){
						var pos = data.curobj.closest('tr').find('td').index(data.curobj.closest('td'));
						if(pos == 2){
							if($.trim(data.curobj.closest('td').prev().text())){
								data.curobj.find('span').html(data.curobj.closest('td').prev().find('span').data('sp_name'));
								return;
							}
						 	else if(!data.actionobj){
								$.fn.autoGrid.setCellEditable(data.curobj.closest('tr'),2);
								return;
							} 
						}
						$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.ovalue) ;
					},
					cellAction:[{
						index:2,
						action:function(row){
							$.fn.autoGrid.setCellEditable(row,6);
						},
						onCellEdit:function(event,data,row){
							data['url'] = '<%=path%>/misbohcommon/findSupplyTop10.do?ynpd=Y&spec1=Y';
							//if(!isNaN(data.value))
								data['key'] = 'sp_code';
							//else
							//	data['key'] = 'sp_init';
							$.fn.autoGrid.ajaxEdit(data,row);
						},
						resultFormat:function(data){
							return data.sp_init+'-'+data.sp_code+'-'+data.sp_name;
						},
						afterEnter:function(data2,row){
							var num=0;
							$('.grid').find('.table-body').find('tr').each(function(){
								if($(this).find("td:eq(1)").text()==data2.sp_code){
									num=1;
								}
							});
							if(num==1){
								showMessage({
	 								type: 'error',
	 								msg: '<fmt:message key="added_supplies_remind"/>！',
	 								speed: 1000
	 							});
								$.fn.autoGrid.setCellEditable(row,2);
								return;
							}
							row.find("td:eq(1) span").text(data2.sp_code).data('sp_name',data2.sp_name);
							row.find("td:eq(2) span input").val(data2.sp_name).focus();
							row.find("td:eq(3) span").text(data2.sp_desc);
							row.find("td:eq(4) span").text('0.00').attr('title',0).css("text-align","right");
							row.find("td:eq(5) span").text(data2.unit4);
							row.find("td:eq(6) span").text('0.00').attr('title',0).css("text-align","right");
							row.find("td:eq(7) span").text(data2.unit3);
							row.find("td:eq(8) span").text('0.00').attr('title',0).css("text-align","right");
							row.find("td:eq(9) span").text(data2.unit);
							row.find("td:eq(10) span").text(data2.ex == 'Y'?'是':'否');
							row.data("unitper3",data2.unitper3);
							row.data("unitper4",data2.unitper4);
							row.data("stockcnt",0);//标准单位
 							//判断此物资是否需要盘点以及顺序号
							var url = '<%=path%>/inventoryMis/getPositnSupply.do?yearr='+$("#yearr").val()+'&date='+$("#date").val()+'&positn.code='+$("#positn").val()+'&sp_code='+data2.sp_code;
							$.ajax({
								url:url,
								type: "POST",
								success:function(inventory){
									row.find("td:eq(11) span").text(inventory.orderNum?inventory.orderNum:0);
									var html = "<select style='width:40px;height:18px;'><option value='Y' ";
									if(inventory.ynpd == 'Y' || !inventory.ynpd){
										 html += "selected='selected'";
									}
									html += ">否</option><option value='N' ";
									if(inventory.ynpd == 'N'){
										html += "selected='selected'";
									}
									html += ">是</option></select>";
									row.find("td:eq(12) span").html(html);
								}
							});
						}
					},{
						index:6,
						action:function(row,data2){
							if(Number(data2.value) < 0){
								alert('<fmt:message key="number_cannot_be_negative"/>！');
								row.find("td:eq(6)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,6);
							}else if(isNaN(data2.value)){
								alert('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(6)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,6);
							}else{
								row.find("td:eq(6)").find('span').attr('title',data2.value);
								getSumCnt(row);
								$.fn.autoGrid.setCellEditable(row,8);
							}
						}
					},{
						index:8,
						action:function(row,data2){
							if(Number(data2.value) < 0){
								alert('<fmt:message key="number_cannot_be_negative"/>！');
								row.find("td:eq(8)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,8);
							}else if(isNaN(data2.value)){
								alert('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(8)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,8);
							}else{
								row.find("td:eq(8)").find('span').attr('title',data2.value);
								getSumCnt(row);
								$.fn.autoGrid.setCellEditable(row,11);
							}
						}
					},{
						index:11,
						action:function(row,data2){
							if(!data2.value.match("^([0-9]|[1-9][0-9])*$")) {
								alert('顺序号必须为正整数！');
								row.find("td:eq(11)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,11);
							}else{
								if(!row.next().html()){
									$.fn.autoGrid.addRow();
									$.fn.autoGrid.setCellEditable(row.next(),2);
								}else{
									if(row.next().find("td:eq(1) span").text() != ''){
										$.fn.autoGrid.setCellEditable(row.next(),6);
									}else{
										$.fn.autoGrid.setCellEditable(row.next(),2);
									}
								}
							}
						}
					}]
				});
				$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),6);
			}
			
			//得到总数量的方法
		  function getSumCnt(row){
			  var sumCnt = Number(row.find("td:eq(8) span").attr('title'));//标准单位
			  var cnt3 = Number(row.find("td:eq(6) span").attr('title'));//采购单位
			  if(Number(row.data('unitper3')) != 0){
				  sumCnt += cnt3/Number(row.data('unitper3'));
			  }
			  row.data("stockcnt",sumCnt);//标准单位
			  row.find("td:eq(4) span").text((sumCnt*Number(row.data('unitper4'))).toFixed(2)).attr('title',sumCnt*Number(row.data('unitper4')));
			  if(sumCnt != 0){
				  row.addClass('redClass');
			  }else{
				  row.removeClass('redClass');
			  }
		  }
		
			//校验
		  function validator(weiz,row,data){
		  		row.find("input").data("ovalue",data.value);
				if(weiz=="6"){
					if(Number(data.value) < 0){
						alert('<fmt:message key="number_cannot_be_negative"/>！');
						row.find("td:eq(6)").find('span').text(data.ovalue);
						$.fn.autoGrid.setCellEditable(row,6);
					}else if(isNaN(data.value)){
						alert('<fmt:message key="number_be_not_number"/>！');
						row.find("td:eq(6)").find('span').text(data.ovalue);
						$.fn.autoGrid.setCellEditable(row,6);
					}else{
						row.find("td:eq(6)").find('span').attr('title',data.value);
						getSumCnt(row);
					}
				}else if(weiz=="8"){
					if(Number(data.value) < 0){
						alert('<fmt:message key="number_cannot_be_negative"/>！');
						row.find("td:eq(8)").find('span').text(data.ovalue);
						$.fn.autoGrid.setCellEditable(row,8);
					}else if(isNaN(data.value)){
						alert('<fmt:message key="number_be_not_number"/>！');
						row.find("td:eq(8)").find('span').text(data.ovalue);
						$.fn.autoGrid.setCellEditable(row,8);
					}else{
						row.find("td:eq(8)").find('span').attr('title',data.value);
						getSumCnt(row);
					}
				}else if(weiz=="11"){
					if(!data.value.match("^([0-9]|[1-9][0-9])*$")) {
						alert('顺序号必须为正整数！');
						row.find("td:eq(11)").find('span').text(data.ovalue);
						$.fn.autoGrid.setCellEditable(row,11);
					}
				}
			}
			
			//保存盘点
			function resolveInventory(){
				var selected = {};
				var checkboxList = $('.grid').find('.table-body').find('tr');
				if(checkboxList && checkboxList.size() > 0){
					if(confirm('确定保存?')){
						$('#wait').show();
						$('#wait2').show();
						selected['positn.code'] = $('#positn').val();
						selected['firm'] = $('#firm').val()?$('#firm').val():$('#positn').val();
						selected['yearr'] = $('#yearr').val();
						selected['pantyp'] = $('#pantyp').val();
						selected['date'] = $('#date').val();
						var i = 0;
						var sp_name = '';
						var flag1 = 0;//判断没有转换率的物资 规格是必输项
						checkboxList.each(function(){
							if($.trim($(this).find('td:eq(1) span').text())!=null && $.trim($(this).find('td:eq(1) span').text()) !='') {
								selected['inventoryList['+i+'].sp_code'] = $.trim($(this).find('td:eq(1) span').text());
								selected['inventoryList['+i+'].stockcnt'] = Number($(this).data('stockcnt'));
								var cnt4 = Number($(this).find('td:eq(4) span').attr('title'));
								selected['inventoryList['+i+'].cnt4'] = cnt4;
								var cnt3 = Number($(this).find('td:eq(6) span').attr('title'));
								selected['inventoryList['+i+'].cnt3'] = cnt3;
								var cnt1 = Number($(this).find('td:eq(8) span').attr('title'));
								selected['inventoryList['+i+'].cnt1'] = cnt1;
								
								selected['inventoryList['+i+'].ynpd'] = $(this).find('td:eq(12) span').find('select').val();
								selected['inventoryList['+i+'].orderNum'] = $(this).find('td:eq(11) span').text()?Number($(this).find('td:eq(11) span').text()):Number($(this).find('td:eq(11) span').find('input').val());
								i++;
							}
						});
						if(flag1 != 0){
							$('#wait').hide();
							$('#wait2').hide();
							alert('物资:['+sp_name+']没有转换率的规格'+flag1+'是必输项！');
							return;
						}
						$.post('<%=path%>/inventoryMis/updateInventory.do',selected,function(data){
							$('#wait').hide();
							$('#wait2').hide();
						 	if(data=="success"){
								showMessage({
									type: 'success',
									msg: '数据已保存！',
									speed: 1000
								});	
								$('#listForm').submit();
						 	}else{
						 		showMessage({
									type: 'error',
									msg: '数据保存失败！',
									speed: 1000
								});	
						 	}
						});
					}
				}else{
					alert('没有数据！');
					return ;
				}
			}
			//结束盘点
			function endInventory(){
				if($('.grid').find('.table-body').find('tr').size() == 0 || $('.grid').find('.table-body').find('tr').first().find('td:eq(1) span').text()==""){
					alert('没有数据！');
					return;
				}
				var msg = '谨慎操作！结束盘点会操作当前所有物资！结束后生成盘亏出库单,确定结束本门店的盘点吗!';
				if('' != $('#firm').val()){
					msg = '谨慎操作！结束盘点会操作当前所有物资！结束后生成盘亏出库单,确定结束['+$("#positn_name").val()+']的盘点吗!';
				}
				if(confirm(msg)){
					//结束盘点谨慎操作，防止网络延迟，造成重复点击产生垃圾数据 wangjie 1.30
					$("body").find(".button:eq(5)").unbind();
					$('#wait').show();
					$('#wait2').show();
					var selected = {};
					selected['yearr'] = $('#yearr').val();
					selected['positn.code'] = $('#positn').val();
					selected['date'] = $('#date').val();
					selected['pantyp'] = $('#pantyp').val();
					selected['firm'] = $('#firm').val();
					$.post('<%=path%>/inventoryMis/endInventory.do',selected,function(data){
						$('#wait').hide();
						$('#wait2').hide();
						if(data.indexOf('Exception') != -1){
							alert(data);
							return;
						}
						var msg_ ="本门店相应的盘亏出库单已生成，请在已审核出库单中查询,本门店盘点结束！";
						if('' != $('#firm').val()){
							msg = '['+$("#positn_name").val()+']相应的盘亏出库单已生成，请在已审核出库单中查询,部门盘点结束！';
						}
						alert(msg_);
						$('#listForm').submit();
					});	
				}
			}
			    
			    //导出excel
			    function exportInventory(){
			    	var positnCode=$('#positn').val();
					if(positnCode==''){
						alert('<fmt:message key="please_select_positions"/>！');//please_select_positions
						return;
					}else{
						$("#wait2").val('NO');//不用等待加载
						$("#listForm").attr("action","<%=path%>/inventoryMis/exportInventory.do");
						$('#listForm').submit();
						$("#wait2 span").html("数据导出中，请稍后...");
						$("#listForm").attr("action","<%=path%>/inventoryMis/list.do");
						$("#wait2").val('');//等待加载还原
					}
			    }
			    
			    //打印盘点表
			    function printInventory(){
			    	var positnCode=$('#positn').val();
					if(positnCode==''){
						alert('<fmt:message key="please_select_positions"/>！');//please_select_positions
						return;
					}else{
						var blank = confirm('是否打印空白盘点表?');
						$("#wait2").val('NO');//不用等待加载
						$('#listForm').attr('target','report');
						window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
						var action="<%=path%>/inventoryMis/printInventory.do?blank="+blank;
						$('#listForm').attr('action',action);
						$('#listForm').submit();
						$("#listForm").attr("action","<%=path%>/inventoryMis/list.do");
						$('#listForm').attr('target','');
						$("#wait2").val('');//等待加载还原
					}
			    }
			    
			    //调用模板
				function addInventoryDemo(){
					var selected = {};
					selected['positn.code'] = $('#positn').val();
					selected['date'] = $('#date').val();
					selected['pantyp'] = $('#pantyp').val();
			    	//1.先判断有没有盘点保存但未结束的数据，如果有则提示覆盖或者继续操作原来的数据，没有则继续
			    	$.post('<%=path%>/inventoryMis/checkIsSaveData.do',selected,function(data){
					 	if(data==1){
							if(confirm('存在已保存但未结束的盘点数据,点击确定选择已保存的数据,点击取消将删除已存在的数据并继续选择新模板')){
								select();
							}else{
								addInventoryDemoNext();
							}
					 	}else{
					 		addInventoryDemoNext();
					 	}
					});
				}
			    
			    function addInventoryDemoNext(){
			    	var pantyp = $('#pantyp').val();
			    	if(pantyp == 'monthpan'){
			    		pantyp = '';
			    	}
			    	var action = "<%=path%>/inventoryMis/addInventoryDemo.do?firm="+$('#positn').val()+"&status="+pantyp;//调用模板的时候传申购门店
					$('body').window({
						id: 'window_addInventoryDemo',
						title: '盘点模板',
						content: '<iframe id="addInventoryDemoFrame" name="addInventoryDemoFrame" frameborder="0" src='+action+'></iframe>',
						width: '80%',//'1000px'
						height: '420px',
						draggable: true,
						isModal: true,
						confirmClose: false,
						topBar: {
							items: [{
									text: '确定',
									title: '确定',
									useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['0px','-40px']
									},
									handler: function(){
										if(getFrame('addInventoryDemoFrame')){
											window.frames["addInventoryDemoFrame"].enterUpdate();
										}
									}
								},{
									text: '取消',
									title: '取消',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}]
						}
					});
			    }
			    
			    var win;
				function showYCL(){
					var date = $('#date').val();
					win=$('body').window({
						id: 'window_showYCL',
						title: '测试应产率',
						content: '<iframe id="window_showYCLFrame" name="window_showYCLFrame" frameborder="0" src="<%=path%>/YclFenxiMisBoh/toPtivityAnaTest.do?date='+date+'"></iframe>',
						width: '98%',
						height: '90%',
						draggable: true,
						isModal: true
					});
				}
				
			//导入
			function importInventory(){
				var selected = {};
				selected['positn.code'] = $('#positn').val();
				selected['date'] = $('#date').val();
				selected['pantyp'] = $('#pantyp').val();
		    	//1.先判断有没有盘点保存但未结束的数据，如果有则提示覆盖或者继续操作原来的数据，没有则继续
		    	$.post('<%=path%>/inventoryMis/checkIsSaveData.do',selected,function(data){
				 	if(data==1){
						if(confirm('存在已保存但未结束的盘点数据,点击确定选择已保存的数据,点击取消将删除已存在的数据并继续进行导入')){
							select();
						}else{
							importInventoryNext();
						}
				 	}else{
				 		importInventoryNext();
				 	}
				});
			}
			function importInventoryNext(){
			  var positnCode=$('#positn').val();
			  $('body').window({
					id: 'window_importInit',
					title: '<fmt:message key="import" />',
					content: '<iframe id="importInit" frameborder="0" src="<%=path%>/inventoryMis/importInventory.do?positn='+positnCode+'"></iframe>',
					width: '400px',
					height: '200px',
					draggable: true,
					isModal: true
				});
			}
			
			//新增和编辑状态下 保持session
			function keepSessionAjax(){
				$.ajax({
					url:'<%=path%>/misbohcommon/keepSessionAjax.do',
					type:"post",
					success:function(data){}
				});
				setTimeout('keepSessionAjax()',300000);
			}
		</script>
	</body>
</html>