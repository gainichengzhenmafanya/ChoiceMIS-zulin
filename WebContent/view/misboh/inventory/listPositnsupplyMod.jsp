<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>inventory Info 盘点</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<style type="text/css">
				.search{
					margin-top:3px;
					cursor: pointer;
				}
				.page{
					margin-bottom: 25px;
				}
				.seachSupply{
					margin-top:3px;
					cursor: pointer;
				}
				.redClass{
					color:red;
				}
			</style>						
		</head>
	<body>
		<div class="tool">
		</div>
		<input type="hidden" id="save" value="${inventory.save }"/>
		<input type="hidden" id="firmOutWay" value="${firmOutWay }"/>
		<input type="hidden" id="positnTyp" value="${positnTyp }"/>
		<input type="hidden" id="nowDate" value="<fmt:formatDate value="${nowDate }" pattern="yyyy-MM-dd" type="date"/>"/><!-- 服务器当前时间 -->
		<form id="listForm" name="listForm" action="<%=path%>/positnsupplyMod/listPositnsupplyMod.do"method="post">
		<input type="hidden" id="firm" name="firm" value="${inventory.firm }" />
		<input type="hidden" id="yearr" name="yearr" value="${inventory.yearr}" /><!-- 会计年 -->
		<input type="hidden" id="pantyp" name="pantyp" value="adjustment" />
		<div class="form-line">
			<div class="form-label"><fmt:message key="date"/>：</div>
			<div class="form-input">
				<input type="text" id="date" name="date" class="Wdate text" 
					<c:if test="${action != 'init' }">readonly="readonly"</c:if> <c:if test="${action == 'init' }">onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'nowDate\')}'});"</c:if>
				value="<fmt:formatDate value="${inventory.date}" pattern="yyyy-MM-dd"/>" />
			</div>
			<input type="hidden" id="positn" name="positn.code" value="${inventory.positn.code}"/>
			<c:if test="${!empty inventory.firm }">
				<div class="form-label"><span style="color:red;">*</span><fmt:message key="positions"/>：</div>
				<div class="form-input">
					<div class="form-input">
						<input type="text" name="positn.des" id="positn_name" class="text" readonly="readonly" value="<c:out value="${inventory.positn.des}" />" style="width:120px;margin-bottom:5px;"/>
						<img id="seachDept" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
					</div>
				</div>
			</c:if>
			<div class="form-input" style="width:60px;">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="ynpd" value="N" <c:if test="${!empty inventory.ynpd }">checked="checked"</c:if>/><fmt:message key="show_uninventoriable"/><!-- 显示禁盘 -->
				&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="ynprint" value="N" <c:if test="${!empty inventory.ynprint}">checked="checked"</c:if>/><fmt:message key="Show_no_print"/><!-- 是否需打印 -->
				&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" id="onlyQuery" name="onlyQuery" value="Y" <c:if test="${!empty inventory.onlyQuery }">checked="checked"</c:if>/><fmt:message key="only_query"/><!-- 僅查詢 -->
			</div>
		</div>
		<c:if test="${!empty chkstoref}">
			<div class="bj_head">
				<div class="form-line">
					<div class="form-label"><fmt:message key="misboh_whether_checked"/>：</div>
					<div class="form-input" style="width:120px;">
						<c:choose>
							<c:when test="${chkstoref.state == '1'}">
								<fmt:message key="checked"/>
							</c:when>
							<c:otherwise>
								<fmt:message key="unchecked"/>
							</c:otherwise>
						</c:choose>
					</div>
					<div class="form-label"><fmt:message key="The_entry_of_people"/>：</div>
					<div class="form-input" style="width:120px;">${chkstoref.ecodeName }</div>
					<div class="form-label"><fmt:message key="The_entry_time"/>：</div>
					<div class="form-input" style="width:120px;">${chkstoref.inputDate }</div>
				</div>
				<div class="form-line">
					<div class="form-label"></div>
					<div class="form-input" style="width:120px;"></div>
					<div class="form-label"><fmt:message key="the_auditor"/>：</div>
					<div class="form-input" style="width:120px;">${chkstoref.checkCodeName }</div>
					<div class="form-label"><fmt:message key="audit_time"/>：</div>
					<div class="form-input" style="width:120px;">${chkstoref.checkDate }</div>
				</div>
			</div>
		</c:if>
		<!-- 修改ie8下条件一行显示不全，换行显示 wjf-->
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" rowspan="2"><span style="width: 30px;">&nbsp;</span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="supplies_code"/></span></td>
								<td rowspan="2"><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="supplies_specifications"/></span></td>
								<td rowspan="2"><span style="width:40px;"><fmt:message key="unit"/></span></td>
								<td rowspan="2"><span style="width:60px;">最后进价</span></td>
								<td colspan="2"><span>当前结存</span></td>
								<td colspan="2"><span>调整</span></td>
							</tr>
							<tr>
								<td><span style="width:60px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:60px;"><fmt:message key="amount"/></span></td>
								<td><span style="width:60px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:60px;"><fmt:message key="amount"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" id="table-body">
						<tbody>
							<c:forEach var="inventory" items="${inventoryList}" varStatus="status">
								<c:choose>
									<c:when test="${inventory.cntbla <= 0.001 && inventory.cntbla >= -0.001 }">
										<c:set var="cntbla" value="0"/>
									</c:when>
									<c:otherwise>
										<c:set var="cntbla" value="${inventory.cntbla }"/>
									</c:otherwise>
								</c:choose>
								<c:choose>
									<c:when test="${inventory.amtbla <= 0.001 && inventory.amtbla >= -0.001 }">
										<c:set var="amtbla" value="0"/>
									</c:when>
									<c:otherwise>
										<c:set var="amtbla" value="${inventory.amtbla }"/>
									</c:otherwise>
								</c:choose>
								<tr data-ex="${inventory.ex}" <c:choose>
										<c:when test="${(inventory.cntbla > 0 && inventory.amtbla > 0) || (inventory.cntbla == 0 && inventory.amtbla == 0) 
										|| (inventory.cntbla <= 0.001 && inventory.cntbla >= -0.001 && inventory.amtbla <= 0.001 && inventory.amtbla >= -0.001)}"></c:when>
										<c:otherwise>class="redClass"</c:otherwise>
									</c:choose>>
									<td class="num" ><span title="${status.index+1}" style="width:30px;">${status.index+1}</span></td>
									<td><span title="${inventory.sp_code}" style="width:70px;" data-sp_name="${inventory.sp_name}">${inventory.sp_code}</span></td>
									<td><span title="${inventory.sp_name}" style="width:100px;">${inventory.sp_name}</span></td>
									<td><span title="${inventory.sp_desc}" style="width:80px;">${inventory.sp_desc}</span></td>
									<td><span title="${inventory.unit}" style="width:40px;">${inventory.unit}</span></td>
									<td><span title="${inventory.last_price}" style="width:60px;text-align: right;"><fmt:formatNumber value="${inventory.last_price}" pattern="##.##" minFractionDigits="2"/></span></td>
									<td><span title="${cntbla}" style="width:60px;text-align: right;"><fmt:formatNumber value="${cntbla}" pattern="##.##" minFractionDigits="2"/></span></td>
									<td><span title="${amtbla}" style="width:60px;text-align: right;"><fmt:formatNumber value="${amtbla}" pattern="##.##" minFractionDigits="2"/></span></td>
									<td><span title="${inventory.stockcnt}" style="width:60px;text-align: right;"><fmt:formatNumber value="${inventory.stockcnt}" pattern="##.##" minFractionDigits="2"/></span></td>
									<td><span title="${inventory.stockamt}" style="width:60px;text-align: right;"><fmt:formatNumber value="${inventory.stockamt}" pattern="##.##" minFractionDigits="2"/></span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="smallClass"/>：</div>
				<div class="form-input" style="margin-top:-4px;">
					<input type="text" id="typDes" class="text"  name="typdes" readonly="readonly" value="${inventory.typdes}"/>
					<input type="hidden" id="sp_type" name="sp_type" value="${inventory.sp_type}"/>
					<img id="seachTyp" style="margin-top:1px;" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_category"/>' />
			    </div>
				<div class="form-label"><fmt:message key="coding"/>：</div>
				<div class="form-input" style="margin-top:-4px;">
					<input type="text" class="text" id="sp_code" name="sp_code" value="${inventory.sp_code}" />
	                <img id="seachSupply" class="seachSupply" src="<%=path%>/image/themes/icons/search.png" alt="<fmt:message key="query_supplies"/>" /><!-- 查询物资 -->
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/autoTable.js"></script>
		
		<script type="text/javascript">
		
		var defaultCode = '';
		var defaultName = '';
		//ajax同步设置
// 		$.ajaxSetup({
// 			async: false
// 		});
		var nScrollHeight=0;
		var nScrollTop=0;
		var returnInfo = true;
		var pageSize = 0;
		
		var height = 80;
			$(document).ready(function(){
			 	//键盘事件绑定
			 	$(document).bind('keyup',function(e){
			 		if($(e.srcElement).is("input")){
			    		validator($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
			    	}
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit"/>').click();
			 		}
				}); 
				
				focus() ;//页面获得焦点
				if($("#onlyQuery").attr('checked') == 'checked'){
					loadToolBar([false,false,false,false,false]);
					$("#date").click(function(){
		  	 			new WdatePicker({maxDate:$('#nowDate').val()});
		  	 		});
					if('${!empty chkstoref}' == 'true'){
						height = 140;
					}
				}else{
					if('${action}' == 'init'){
			 			loadToolBar([false,false,false,false,false]);
					}else{
				 		if($('#save').val() == 'save'){//有未审核的
				 			loadToolBar([true,false,true,false,true]);
				 		}else{
				 			loadToolBar([false,false,true,false,true]);
				 		}
					}
				}
		 		
				setElementHeight('.grid',['.tool'],$(document.body),height);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				
				/*弹出部门选择树*/
				$('#seachDept').bind('click.custom',function(e){
					if(!!!top.customWindow){
						defaultCode = $('#positn').val();
						defaultName = $('#positn_name').val();
						//alert(defaultCode+"==="+defaultName);
						var offset = getOffset('positn');
						top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/misbohcommon/findPositnSuper.do?typn=7&iffirm=1&mold=one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positn_name'),$('#positn'),'760','520','isNull',select);
					}
				});
				/*弹出树*/
				$('#seachTyp').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode1 = $('#sp_type').val();
						var offset = getOffset('positn');
						top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/misbohcommon/selectMoreGrpTyp.do?defaultCode='+defaultCode1),offset,$('#typDes'),$('#sp_type'),'320','460','isNull');
					}
				});
				$('#seachSupply').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode1 = $('#sp_code').val();
						top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/misbohcommon/selectSupplyLeft.do?positn=1&defaultCode='+defaultCode1,$('#sp_code'));	
					}
				});
			});
			
			
				function loadToolBar(use){
					$('.tool').html('');
					var tool = $('.tool').toolbar({
						items: [{
								text: '<fmt:message key="select" />',
								title: '<fmt:message key="select"/>',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['0px','-40px']
								},
								
								handler: function(){
									select();
								}
							},{
								text: '<fmt:message key="edit"/>',
								title: '<fmt:message key="edit"/>',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[2],
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['0px','0px']
								},
								handler: function(){
									editCells();
									loadToolBar([false,false,false,true]);
									setTimeout('keepSessionAjax()',300000);
								}
							},{
								text: '<fmt:message key="save"/>',
								title: '<fmt:message key="save"/>',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[3],
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-80px']
								},
								handler: function(){ 
									saveInventory();
								}
							},{
								text: '<fmt:message key="check"/>',//结束盘点
								title: '<fmt:message key="check"/>',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')}&&use[0],
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									var date = $('#date').val();
									$('#wait').show();
									$('#wait2').show();
									$.ajax({
										url:'<%=path%>/inventoryMis/checkBeforeInventory.do?edate='+date,
										type:"post",
										success:function(data){
											$('#wait').hide();
											$('#wait2').hide();
											var rs = eval('('+data+')');
											if(rs.pr == 1){
												endInventory();
											}else{
												alert(rs.error);
											}
										}
									});
								}
							},{
								text: '<fmt:message key="delete"/>',
								title: '<fmt:message key="delete"/>',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')}&&use[0],
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									delPositnsupplyMod();
								}
							},{
								text: '调整等于结存',
								title: '调整等于结存',
								useable: use[3],
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
 									copySupply();
								}
							},{
								text: '自动过滤没问题的物资',
								title: '自动过滤没问题的物资',
								useable: use[3],
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
 									deleteGoodSupply();
								}
							},{
								text: '<fmt:message key="quit" />',
								title: '<fmt:message key="quit"/>',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
								}
							}
						]
					});
				}
			
			function select(){
				var positnCode=$('#positn').val();
				if(positnCode==''){
					alert('<fmt:message key="please_select_positions"/>！');//请选择仓位
					return;
				}
				//判断有没有期初
				$.ajax({
					url:"<%=path%>/misbohcommon/checkQC.do?code="+positnCode,
					type:"post",
					success:function(data){
						if(data){
							$('#listForm').submit();
						}else{
							var msg = '<fmt:message key="The_store_isnot_Initialized"/>!';
							if($('#firm').val()){
								msg = '['+$("#positn_name").val()+']<fmt:message key="no_Initialized"/>！';
							}
							alert(msg);//该门店还未做期初！
							$('#positn').val(defaultCode);
							$("#positn_name").val(defaultName);
							return;
						}
					}
				});
			}
			
			//盘点前判断
			function checkBeforeInventory(){
				var flag = false;
				var date = $('#date').val();
				$('#wait').show();
				$('#wait2').show();
				$.ajax({
					url:'<%=path%>/inventoryMis/checkBeforeInventory.do?edate='+date,
					type:"post",
					async:false,
					success:function(data){
						$('#wait').hide();
						$('#wait2').hide();
						var rs = eval('('+data+')');
						if(rs.pr == 1){
							flag = true;
						}else{
							flag = false;
							alert(rs.error);
						}
					}
				});
				return flag;
			}
			
			//编辑表格
			function editCells() {
				$('.table-body').find('tr:first').find("td:eq(2) span").focus().select();
				$(".table-body").autoGrid({
					initRow:1,
					colPerRow:9,
					widths:[40,80,110,90,50,70,70,70,70,70],
					colStyle:['','','','','','','','',{background:"#F1F1F1"},{background:"#F1F1F1"}],
					VerifyEdit:{verify:true,enable:function(cell,row){
						return row.find('td').index(cell) == 2 || (row.find("td:eq(2)").text() == null || $.trim(row.find("td:eq(2)").text()) != '' ) ? true : false;
					}},
					onEdit:$.noop,
					editable:[2,8,9],
					onLastClick:function(row){
// 						getTotalSum();
					},
					onEnter:function(data){
						var pos = data.curobj.closest('tr').find('td').index(data.curobj.closest('td'));
						if(pos == 2){
							if($.trim(data.curobj.closest('td').prev().text())){
								data.curobj.find('span').html(data.curobj.closest('td').prev().find('span').data('sp_name'));
								return;
							}
						 	else if(!data.actionobj){
								$.fn.autoGrid.setCellEditable(data.curobj.closest('tr'),2);
								return;
							} 
						}
						$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.ovalue) ;
					},
					cellAction:[{
						index:2,
						action:function(row){
							$.fn.autoGrid.setCellEditable(row,8);
						},
						onCellEdit:function(event,data,row){
							data['url'] = '<%=path%>/misbohcommon/findSupplyTop10.do?ynpd=Y';
							//if(!isNaN(data.value))
								data['key'] = 'sp_code';
							//else
							//	data['key'] = 'sp_init';
							$.fn.autoGrid.ajaxEdit(data,row);
						},
						resultFormat:function(data){
							return data.sp_init+'-'+data.sp_code+'-'+data.sp_name;
						},
						afterEnter:function(data2,row){
							var num=0;
							$('.grid').find('.table-body').find('tr').each(function(){
								if($(this).find("td:eq(1)").text()==data2.sp_code){
									num=1;
								}
							});
							if(num==1){
								showMessage({
	 								type: 'error',
	 								msg: '<fmt:message key="added_supplies_remind"/>！',
	 								speed: 1000
	 							});
								return;
							}
							row.find("td:eq(1) span").text(data2.sp_code).data('sp_name',data2.sp_name);
							row.find("td:eq(2) span input").val(data2.sp_name).focus();
							row.find("td:eq(3) span").text(data2.sp_desc);
							row.find("td:eq(4) span").text(data2.unit);
							row.find("td:eq(5) span").text(data2.last_price.toFixed(2)).attr('title',data2.last_price).css("text-align","right");
							row.data("ex",data2.ex);
							//判断此物资是否需要盘点以及顺序号
							var url = '<%=path%>/inventoryMis/getWzyue.do?yearr='+$("#yearr").val()+'&date='+$("#date").val()+'&positn.code='+$("#positn").val()+'&sp_code='+data2.sp_code;
							$.ajax({
								url:url,
								type: "POST",
								success:function(inventory){
									var cntbla = 0;
									var amtbla = 0;
									if(inventory && inventory.cntbla){
										cntbla = inventory.cntbla;
										amtbla = inventory.amtbla;
									}
									row.find("td:eq(6) span").text(cntbla.toFixed(2)).attr('title',cntbla).css("text-align","right");
									row.find("td:eq(7) span").text(amtbla.toFixed(2)).attr('title',amtbla).css("text-align","right");
									row.find("td:eq(8) span").text('0.00').attr('title',0).css("text-align","right");
									row.find("td:eq(9) span").text('0.00').attr('title',0).css("text-align","right");
								}
							});
						}
					},{
						index:8,
						action:function(row,data2){
							if(Number(data2.value) < 0){
								alert('<fmt:message key="number_cannot_be_negative"/>！');
								row.find("td:eq(8)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,8);
							}else if(isNaN(data2.value)){
								alert('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(8)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,8);
							}else{
								row.find("td:eq(8)").find('span').attr('title',data2.value);
								$.fn.autoGrid.setCellEditable(row,9);
							}
						}
					},{
						index:9,
						action:function(row,data2){
							if(Number(data2.value) < 0){
								alert('<fmt:message key="number_cannot_be_negative"/>！');
								row.find("td:eq(9)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,9);
							}else if(isNaN(data2.value)){
								alert('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(9)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,9);
							}else{
								row.find("td:eq(9)").find('span').attr('title',data2.value);
								if(!row.next().html()){
									$.fn.autoGrid.addRow();
									$.fn.autoGrid.setCellEditable(row.next(),2);
								}else{
									if(row.next().find("td:eq(1) span").text() != ''){
										$.fn.autoGrid.setCellEditable(row.next(),8);
									}else{
										$.fn.autoGrid.setCellEditable(row.next(),2);
									}
								}
							}
						}
					}]
				});
			}
			
			//校验
		  function validator(weiz,row,data){
			  row.find("input").data("ovalue",data.value);
				if(weiz=="8"){
					if(Number(data.value) < 0){
						alert('<fmt:message key="number_cannot_be_negative"/>！');
						row.find("td:eq(8)").find('span').text(data.ovalue);
						$.fn.autoGrid.setCellEditable(row,8);
					}else if(isNaN(data.value)){
						alert('<fmt:message key="number_be_not_number"/>！');
						row.find("td:eq(8)").find('span').text(data.ovalue);
						$.fn.autoGrid.setCellEditable(row,8);
					}else{
						row.find("td:eq(8)").find('span').attr('title',data.value);
					}
				}else if(weiz=="9"){
					if(Number(data.value) < 0){
						alert('<fmt:message key="number_cannot_be_negative"/>！');
						row.find("td:eq(9)").find('span').text(data.ovalue);
						$.fn.autoGrid.setCellEditable(row,9);
					}else if(isNaN(data.value)){
						alert('<fmt:message key="number_be_not_number"/>！');
						row.find("td:eq(9)").find('span').text(data.ovalue);
						$.fn.autoGrid.setCellEditable(row,9);
					}else{
						row.find("td:eq(9)").find('span').attr('title',data.value);
					}
				}
			}
			
			//保存盘点
			function saveInventory(){
				var selected = {};
				var checkboxList = $('.grid').find('.table-body').find('tr');
				if(checkboxList && checkboxList.size() > 0 && checkboxList.first().find('td:eq(1) span').text() != ''){
					if(confirm('<fmt:message key="Are_you_sure_you_want_to_save"/>?')){//您确定要保存吗
						$('#wait').show();
						$('#wait2').show();
						selected['positn.code'] = $('#positn').val();
						selected['firm'] = $('#firm').val()?$('#firm').val():$('#positn').val();
						selected['yearr'] = $('#yearr').val();
						selected['pantyp'] = $('#pantyp').val();
						selected['date'] = $('#date').val();
						var i = 0;
						var sp_name = '';
						checkboxList.each(function(){
							if($.trim($(this).find('td:eq(1) span').text())!=null && $.trim($(this).find('td:eq(1) span').text()) !='' 
									&& $(this).data('ex') == 'N') {
								selected['inventoryList['+i+'].sp_code'] = $.trim($(this).find('td:eq(1) span').text());
								selected['inventoryList['+i+'].stockcnt'] = Number($(this).find('td:eq(8) span').attr('title'));
								selected['inventoryList['+i+'].stockamt'] = Number($(this).find('td:eq(9) span').attr('title'));
								i++;
							}
						});
						$.post('<%=path%>/positnsupplyMod/updatePositnsupplyMod.do',selected,function(data){
							$('#wait').hide();
							$('#wait2').hide();
						 	if(data=="success"){
								showMessage({
									type: 'success',
									msg: '<fmt:message key="success_to_save_data"/>！',
									speed: 1000
								});	
								$('#listForm').submit();
						 	}else{
						 		showMessage({
									type: 'error',
									msg: '<fmt:message key="failed_to_save_data"/>！',
									speed: 1000
								});	
						 	}
						});
					}
				}else{
					alert('<fmt:message key="no_data"/>！');
					return ;
				}
			}
			//结束盘点
			function endInventory(){
				if($('.grid').find('.table-body').find('tr').size() == 0 || $('.grid').find('.table-body').find('tr').first().find('td:eq(1) span').text()==""){
					alert('<fmt:message key="no_data"/>！');
					return;
				}
				var msg = '谨慎操作！审核后生成盘亏出库单,确定进行本门店的库存调整吗!';
				if('' != $('#firm').val()){
					msg = '谨慎操作！审核后生成盘亏出库单,确定进行['+$("#positn_name").val()+']的库存调整吗!';
				}
				if($('#positnTyp').val() == '1203'){
					if($('#firmOutWay').val() == '1'){//所有门店都生成盘盈盘亏的
// 						msg = '谨慎操作！结束盘点会操作当前所有物资！结束后生成盘盈入库和盘亏出库单,确定结束本门店的盘点吗!';
						msg = '<fmt:message key="Be_careful_End_inventory_will_deal_all_the_current_materials"/>!'+
						'<fmt:message key="End_inventory_will_generate_Inventory_surplus_order_and_dish_deficient_outbound_order"/>,'+
						'<fmt:message key="Are_you_sure_to_end"/><fmt:message key="the_current_store"/><fmt:message key="s_inventory"/>!';
						if('' != $('#firm').val()){
// 							msg = '谨慎操作！结束盘点会操作当前所有物资！结束后生成盘盈入库和盘亏出库单,确定结束['+$("#positn_name").val()+']的盘点吗!';
							msg = '<fmt:message key="Be_careful_End_inventory_will_deal_all_the_current_materials"/>!'+
							'<fmt:message key="End_inventory_will_generate_Inventory_surplus_order_and_dish_deficient_outbound_order"/>,'+
							'<fmt:message key="Are_you_sure_to_end"/>['+$("#positn_name").val()+']<fmt:message key="s_inventory"/>!';
						}
					}
					if($('#firmOutWay').val() == '2'){//只有启用档口才生成盘盈盘亏
						if('' != $('#firm').val()){
// 							msg = '谨慎操作！结束盘点会操作当前所有物资！结束后生成盘盈入库和盘亏出库单,确定结束['+$("#positn_name").val()+']的盘点吗!';
							msg = '<fmt:message key="Be_careful_End_inventory_will_deal_all_the_current_materials"/>!'+
							'<fmt:message key="End_inventory_will_generate_Inventory_surplus_order_and_dish_deficient_outbound_order"/>,'+
							'<fmt:message key="Are_you_sure_to_end"/>['+$("#positn_name").val()+']<fmt:message key="s_inventory"/>!';
						}
					}
				}
				if(confirm(msg)){
					//结束盘点谨慎操作，防止网络延迟，造成重复点击产生垃圾数据 wangjie 1.30
					$("body").find(".button:eq(5)").unbind();
					$('#wait').show();
					$('#wait2').show();
					var selected = {};
					selected['yearr'] = $('#yearr').val();
					selected['positn.code'] = $('#positn').val();
					selected['date'] = $('#date').val();
					selected['pantyp'] = $('#pantyp').val();
					selected['firm'] = $('#firm').val();
					$.post('<%=path%>/positnsupplyMod/checkPositnsupplyMod.do',selected,function(data){
						$('#wait').hide();
						$('#wait2').hide();
						if(data.indexOf('Exception') != -1){
							alert(data);
							return;
						}
						var msg1 ="本门店相应的盘亏出库单已生成，请在已审核出库单中查询！";
						if('' != $('#firm').val()){
							msg1 = '['+$("#positn_name").val()+']相应的盘亏出库单已生成，请在已审核出库单中查询！';
						}
						if($('#positnTyp').val() == '1203'){
							if($('#firmOutWay').val() == '1'){//所有门店都生成盘盈盘亏的
// 								msg1 = '本门店相应的盘盈入库单和盘亏出库单已生成，请在已审核出入库单中查询,本门店盘点结束！';
								msg1 ='<fmt:message key="the_current_store"/><fmt:message key="Inventory_surplus_order_and_The_corresponding_dish_deficient_outbound_order_has_been_generated"/>,'+
								'<fmt:message key="Please_have_the_audit_for_the_single_query_the_end_of_the_store_inventory"/>!';
								if('' != $('#firm').val()){
// 									msg1 = '['+$("#positn_name").val()+']相应的盘盈入库单和盘亏出库单已生成，请在已审核出入库单中查询,本门店盘点结束！';
									msg1 = '['+$("#positn_name").val()+']<fmt:message key="Inventory_surplus_order_and_The_corresponding_dish_deficient_outbound_order_has_been_generated"/>,'+
									'<fmt:message key="Please_have_the_audit_for_the_single_query_the_end_of_the_store_inventory"/>!';
								}
							}
							if($('#firmOutWay').val() == '2'){//只有启用档口才生成盘盈盘亏
								if('' != $('#firm').val()){
// 									msg1 = '['+$("#positn_name").val()+']相应的盘盈入库单和盘亏出库单已生成，请在已审核出入库单中查询,本门店盘点结束！';
									msg1 = '['+$("#positn_name").val()+']<fmt:message key="Inventory_surplus_order_and_The_corresponding_dish_deficient_outbound_order_has_been_generated"/>,'+
									'<fmt:message key="Please_have_the_audit_for_the_single_query_the_end_of_the_store_inventory"/>!';
								}
							}
						}
						alert(msg1);
						$('#listForm').submit();
					});	
				}
			}
			
			//删除库存调整
			function delPositnsupplyMod(){
				if(!confirm('是否删除当天的库存调整单？'))return;
				$('#wait').show();
				$('#wait2').show();
				var selected = {};
				selected['yearr'] = $('#yearr').val();
				selected['positn.code'] = $('#positn').val();
				selected['date'] = $('#date').val();
				selected['pantyp'] = $('#pantyp').val();
				selected['firm'] = $('#firm').val();
				$.ajax({
					url:'<%=path%>/positnsupplyMod/delPositnsupplyMod.do',
					type:"post",
					data:selected,
					success:function(data){
						$('#wait').hide();
						$('#wait2').hide();
					 	if(data=="success"){
							alert('<fmt:message key="successful_deleted"/>!');
							$('#listForm').submit();
					 	}else{
							alert('<fmt:message key="delete_fail"/>!');
					 	}
					}
				});
			}
			
			//一键复制调整等于结存
			function copySupply(){
				var checkboxList = $('.grid').find('.table-body').find('tr');
				if(checkboxList && checkboxList.size() > 0){
					checkboxList.each(function(i){
						var cnt = $(this).find('td:eq(6)').find('span').attr('title');
						var amt = $(this).find('td:eq(7)').find('span').attr('title');
						$(this).find('td:eq(8) span').text(Number(cnt).toFixed(2)).attr('title',cnt);
						$(this).find('td:eq(9) span').text(Number(amt).toFixed(2)).attr('title',amt);
					});
				}
			}
			
			//删掉没问题的物资
			function deleteGoodSupply(){
				var checkboxList = $('.grid').find('.table-body').find('tr');
				if(checkboxList && checkboxList.size() > 0){
					checkboxList.each(function(i){
						var cnt = $(this).find('td:eq(6)').find('span').text();
						var amt = $(this).find('td:eq(7)').find('span').text();
						var ex = $(this).data('ex');
						if((cnt > 0 && amt > 0) || (cnt == 0 && amt == 0) || ex == 'Y'){
							$(this).remove();
						}
					});
				}
			}
			
			//新增和编辑状态下 保持session
			function keepSessionAjax(){
				$.ajax({
					url:'<%=path%>/misbohcommon/keepSessionAjax.do',
					type:"post",
					success:function(data){}
				});
				setTimeout('keepSessionAjax()',300000);
			}
		</script>
	</body>
</html>