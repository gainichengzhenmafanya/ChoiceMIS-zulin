<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>	
		<style type="text/css">
			.onEdit{
				border:1px solid;
				border-bottom-color: blue;
				border-top-color: blue;
				border-left-color: blue;
				border-right-color: blue;
			}
			.input{
				background:transparent;
				border:0px solid;
			}
			a.l-btn-plain{
				border:1px solid #7eabcd; 
				height:22px;
			}
			.search{
				margin-top:-2px;
				cursor: pointer;
			}
			.textInput span {
				padding:0px;
			}
			.textInput input {
				border:0px;
				width:55px;
			}
		</style>
		<script type="text/javascript">
			var path="<%=path%>";
		</script>					
	</head>
	<body>
		<div class="tool"></div>
		<%--当前登录用户 --%>	
		<input type="hidden" id="lv" value="${spCodeUse.lv}"/><!-- 可以用来判断是不是重新计算了 -->
		<input type="hidden" id="yysj" value="${yysj}"/>
		<form id="listForm" action="<%=path%>/spCodeUseMis/list.do" method="post">	
			<input type="hidden" id="ynUseDept" name="ynUseDept" value="${spCodeUse.ynUseDept}"/>
			<div class="form-line">	
				<div class="form-label">&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="the_reference_date"/>(仅计算用):</div>
				<div class="form-input" style="width:250px;">
					<font style="color:blue;">从:</font>
					<input type="text" style="width:90px;margin-top: -3px;" id="bdat" name="bdat" value="<fmt:formatDate value="${spCodeUse.bdat}" pattern="yyyy-MM-dd"/>" class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/>
					<font style="color:blue;"><fmt:message key="to"/>:</font>
					<input type="text" style="width:90px;margin-top: -3px;" id="edat" name="edat" value="<fmt:formatDate value="${spCodeUse.edat}" pattern="yyyy-MM-dd"/>" class="Wdate text" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/>
				</div>
				<c:if test="${spCodeUse.ynUseDept == 'Y'}">
				 	<div class="form-label"><fmt:message key="sector"/></div>
					<div class="form-input" style="width:205px;">
						<input style="width:95px;" type="hidden" id="dept" name="dept" value="${spCodeUse.dept }"/>
						<input style="width:95px;" type="text" id="deptName" name="deptdes" value="${spCodeUse.deptdes }"/>
						<img id="seachDept" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="misboh_DeclareGoodsGuide_dept" />' />
					</div>
				</c:if>
			</div>	
		   	<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width:30px;">&nbsp;</span></td>
								<c:if test="${spCodeUse.ynUseDept == 'Y'}">
									<td><span style="width:100px;"><fmt:message key="sector"/></span></td>
								</c:if>
								<td><span style="width:100px;"><fmt:message key="supplies_code"/></span></td>
								<td><span style="width:170px;"><fmt:message key="supplies_name"/></span></td>
								<td><span style="width:90px;"><fmt:message key="supplies_specifications"/></span></td>
								<td><span style="width:50px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:50px;"><fmt:message key="misboh_forecastreference"/></span></td>
								<td><span style="width:70px;"><fmt:message key="adjustment"/></span></td>
								<td><span style="width:200px;"><fmt:message key="remark"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>	
							<c:forEach var="spuse" items="${spCodeUseList}" varStatus="status">
								<tr>
									<td class="num"><span style="width:30px;">${status.index+1}</span>
									<input type="hidden" name="show_id"   value="${spuse.sp_code}"/>
									</td>
									<c:if test="${spCodeUse.ynUseDept == 'Y'}">
										<td><span title="${spuse.dept}" style="width:100px;">${spuse.deptdes}</span></td>
									</c:if>
									<td><span title="${spuse.sp_code}" style="width:100px;">${spuse.sp_code}</span></td>
									<td><span title="${spuse.sp_name}" style="width:170px;">${spuse.sp_name}</span></td>
									<td><span title="${spuse.sp_desc}" style="width:90px;">${spuse.sp_desc}</span></td>
									<td><span title="${spuse.unit}" style="width:50px;">${spuse.unit}</span></td>
									<td><span title='<fmt:formatNumber type="currency" pattern="#0.00" value="${spuse.cntold}"></fmt:formatNumber>' style="width:50px;text-align: right;"><fmt:formatNumber type="currency" pattern="#0.00" value="${spuse.cntold}"></fmt:formatNumber></span></td>
									<td class="textInput"><span title='<fmt:formatNumber type="currency" pattern="#0" value="${spuse.cnt}"/>' style="width:70px;">
										<input type="text" value='<fmt:formatNumber type="currency" pattern="#0.00" value="${spuse.cnt}"/>' style="width:70px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
									<td class="textInput"><span title="${spuse.memo}" style="width:200px;">
										<input type="text" value="${spuse.memo}" style="width:200px;text-align: right;padding: 0px;" onfocus="this.select()"/></span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>		
		<div class="form-line">
			<div class="form-input" style="color:red;">
				【根据配置,当前预估用量为：
				<c:choose>
					<c:when test="${spCodeUse.typ == 'AMT' }">
						万元用量&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						计算逻辑：物资用量(参考)=参考日期内物资耗用/参考日期内的营业额*10000&nbsp;】
					</c:when>
					<c:when test="${spCodeUse.typ == 'TC' }">
						千单用量&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						计算逻辑：物资用量(参考)=参考日期内物资耗用/参考日期内的单数*1000&nbsp;】
					</c:when>
					<c:otherwise>
						千人用量&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						计算逻辑：物资用量(参考)=参考日期内物资耗用/参考日期内的客流*1000&nbsp;】
					</c:otherwise>
				</c:choose>
				 
			</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			focus() ;//页面获得焦点
			if ($('#yysj').val()=='0'){
				alert("<fmt:message key='lack_of_business_data'/>！");
			}
			if($('.grid').find('.table-body').find('.num').size()>0){
				loadToolBar([true,true,true]);
			}else {
				loadToolBar([true,true,false]);
			}
			
		 	$(document).bind('keydown',function(e){//按钮快捷键
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
			});

			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
			function loadToolBar(use){
				$('.tool').html('');
				$('.tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							$("#listForm").submit();
						}
					},'-',{
						text: '<fmt:message key="calculate" />',
						title: '<fmt:message key="calculate" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'calculate')}&&use[1],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							calculate();
						}
					},{
						text: '<fmt:message key="save" />',
						title: '<fmt:message key="save" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[2],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							saveUpdate();
						}
					},'-',{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}]
				});
			}
			
			//自动实现滚动条 				
			setElementHeight('.grid',['.tool'],$(document.body),80);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();
			
			/*弹出树*/
			$('#seachDept').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#dept').val();
					var defaultName = $('#deptName').val();
					//alert(defaultCode+"==="+defaultName);
					var offset = getOffset('bdat');
					top.cust('<fmt:message key="please_select_positions" />',encodeURI('<%=path%>/misbohcommon/findPositnSuper.do?typn=7&iffirm=0&mold=oneTmany&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deptName'),$('#dept'),'760','520','isNull');
				}
			});
		});	
		
		//保存登记
		function saveUpdate(){
			var selected = {};
			selected["lv"] = $('#lv').val();
			selected["dept"] = $('#dept') ? $('#dept').val() : '';
			var checkList = $("input[name='show_id']");
			if($('#ynUseDept').val() == 'Y'){
				checkList.each(function(i){
// 					selected['spCodeUseList['+i+'].firm'] = $('#firm').val();
					selected['spCodeUseList['+i+'].dept'] = $(this).parents('tr').find('td:eq(1)').find('span').attr('title');
					selected['spCodeUseList['+i+'].sp_code'] = $(this).parents('tr').find('td:eq(2)').find('span').attr('title');
					selected['spCodeUseList['+i+'].cnt'] = $(this).parents('tr').find('td:eq(7)').find('input').val();
					selected['spCodeUseList['+i+'].change'] = $(this).parents('tr').find('td:eq(7)').find('input').data('change');
					selected['spCodeUseList['+i+'].cntold'] = $(this).parents('tr').find('td:eq(6)').find('span').attr('title');
					selected['spCodeUseList['+i+'].memo'] = $(this).parents('tr').find('td:eq(8)').find('input').val();
				});
			} else{
				checkList.each(function(i){
// 					selected['spCodeUseList['+i+'].firm'] = $('#firm').val();
					selected['spCodeUseList['+i+'].sp_code'] = $(this).parents('tr').find('td:eq(1)').find('span').attr('title');
					selected['spCodeUseList['+i+'].cnt'] = $(this).parents('tr').find('td:eq(6)').find('input').val();
					selected['spCodeUseList['+i+'].change'] = $(this).parents('tr').find('td:eq(6)').find('input').data('change');
					selected['spCodeUseList['+i+'].cntold'] = $(this).parents('tr').find('td:eq(5)').find('span').attr('title');
					selected['spCodeUseList['+i+'].memo'] = $(this).parents('tr').find('td:eq(7)').find('input').val();
				});
			}
			$('#wait').show();
			$('#wait2').show();
			$.post('<%=path%>/spCodeUseMis/update.do',selected,function(data){
				$('#wait').hide();
				$('#wait2').hide();
				if(data ==1){
					alert('<fmt:message key="operation_successful" />!');
					$("#listForm").submit();
				}else{
					alert('<fmt:message key="operation_failed" />!');
				}
			});
		}
		
		function calculate(){
			if (confirm('重新计算会清除本店所有数据！<fmt:message key="to_confirm_the_computation_estimation_Mody"/>？')) {
				var a =  new Date($('#edat').val().replace(/-/g,"/")).getTime()- new Date($('#bdat').val().replace(/-/g,"/")).getTime();
				if(a/(24*60*60*1000)<6) {
					alert('<fmt:message key="please_select"/><fmt:message key="Is_greater_than"/><fmt:message key="misboh_week1"/><fmt:message key="date"/>');
					return;
				}
				if(!$('#bdat').val()) {
					alert('<fmt:message key="please_enter"/><fmt:message key="scm_reference"/><fmt:message key="startdate"/>！');
					return;
				}
				if(!$('#edat').val()) {
					alert('<fmt:message key="please_enter"/><fmt:message key="scm_reference"/><fmt:message key="enddate"/>！');
					return;
				}
				var action = "<%=path%>/spCodeUseMis/calculate.do";
				$('#listForm').attr('action',action);
				$('#listForm').submit(); 
				$('#wait').show();
				$('#wait2').show();
			}
		}
		// 检查最高最低库存是否有效数字
		function checkNum(inputObj){
			if(isNaN(inputObj.value)){
				alert("<fmt:message key='invalid_number'/>！");
				inputObj.focus();
				return false;
			}
			$(inputObj).val(Number($(inputObj).val()).toFixed(2));//保留两位小数
			$(inputObj).attr('data-change','Y');
		}
		</script>
	</body>
</html>