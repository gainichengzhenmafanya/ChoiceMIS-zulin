<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>分店物资</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			
			<style type="text/css">
				.page{
					margin-bottom: 25px;
				}
			</style>
		</head>
	<body>
		<div class="tool"></div>
		<form action="<%=path%>/positnSpcodeMis/table.do" id="listForm" name="listForm" method="post">
			<div class="bj_head">
				<div class="form-line">
					<div class="form-label"><fmt:message key="supplies_code"/>:</div>
					<div class="form-input">
						<input type="text" id="sp_code" name="sp_code" value="${positnSpcode.sp_code }"/>
					</div>
					<div class="form-label"><fmt:message key="supplies_name"/>:</div>
					<div class="form-input">
						<input type="text" id="sp_name" name="sp_name" class="text" value="${positnSpcode.sp_name}" />
					</div>
					<div class="form-label"><fmt:message key="supplies_abbreviations"/>:</div>
					<div class="form-input">
						<input type="text" id="sp_init" name="sp_init" class="text" style="text-transform:uppercase;" value="${positnSpcode.sp_init}" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-input" style="margin-left:100px;">
						[<input type="radio" name="ynsto" value="" checked="checked"/><fmt:message key="all"/>
						<input type="radio" name="ynsto" value="Y" <c:if test="${positnSpcode.ynsto == 'Y' }">checked="checked"</c:if> /><fmt:message key="reportable"/>
						<input type="radio" name="ynsto" value="N" <c:if test="${positnSpcode.ynsto == 'N' }">checked="checked"</c:if> /><fmt:message key="unreported"/>]
					</div>
					<div class="form-input" style="margin-left:100px;">
						[<input type="radio" name="ynpd" value="" checked="checked"/><fmt:message key="all"/>
						<input type="radio" name="ynpd" value="Y" <c:if test="${positnSpcode.ynpd == 'Y' }">checked="checked"</c:if> /><fmt:message key="inventoriable"/>
						<input type="radio" name="ynpd" value="N" <c:if test="${positnSpcode.ynpd == 'N' }">checked="checked"</c:if> /><fmt:message key="uninventoried"/>]
					</div>
					<div <c:if test="${isPositnSpcodeJmj=='N' }"> style="display:none;"</c:if> class="form-input" style="margin-left:100px;">
						[<input type="radio" name="positnCode" value="" checked="checked"/><fmt:message key="all"/>
						<input type="radio" name="positnCode" value="0" <c:if test="${positnSpcode.positnCode == '0'}">checked="checked"</c:if> />已下发
						<input type="radio" name="positnCode" value="1" <c:if test="${positnSpcode.positnCode == '1'}">checked="checked"</c:if> />未下发]
					</div>
				</div>
			</div>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2"><span style="width:30px;"></span></td>
								<td colspan="7"><fmt:message key="supplies"/></td>
								<td <c:choose><c:when test="${isDistributionUnit == 'Y' }">colspan="7"</c:when><c:otherwise>colspan="7"</c:otherwise></c:choose>>
								<fmt:message key="branche"/><fmt:message key="attribute"/></td>
							</tr>
							<tr>
								<td><span style="width:80px;"><fmt:message key="coding"/></span></td>
								<td><span style="width:120px;"><fmt:message key="name"/></span></td>
								<td><span style="width:60px;"><fmt:message key="supplies_abbreviations"/></span></td>
								<td><span style="width:80px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:50px;"><fmt:message key="standard_unit_br"/></span></td>
								<td><span style="width:50px;"><fmt:message key="reports"/><br/><fmt:message key="unit"/></span></td>
								<td><span style="width:50px;"><fmt:message key="the_conversion_rate"/></span></td>
								<td><span style="width:50px;"><fmt:message key="reportable"/></span></td>
								<td><span style="width:50px;"><fmt:message key="inventoriable"/></span></td>
								<c:choose>
									<c:when test="${isDistributionUnit == 'Y' }">
										<td><span style="width:50px;"><fmt:message key="Distribution_Unit"/></span></td>
										<td><span style="width:50px;"><fmt:message key="the_conversion_rate"/></span></td>
										<td><span style="width:50px;"><fmt:message key="Purchase_multiple"/></span></td>
										<td><span style="width:90px;"><fmt:message key="status"/></span></td>
									</c:when>
									<c:otherwise>
										<td <c:if test="${isPositnSpcodeJmj=='Y' }"> style="display:none;"</c:if>><span style="width:60px;"><fmt:message key="minimum_amount_of_purchase"/></span></td>
										<td <c:if test="${isPositnSpcodeJmj=='Y' }"> style="display:none;"</c:if>><span style="width:50px;"><fmt:message key="Purchase_multiple"/></span></td>
										<td <c:if test="${isPositnSpcodeJmj=='N' }"> style="display:none;"</c:if>><span style="width:90px;"><fmt:message key="status"/></span></td>
									</c:otherwise>
								</c:choose>
								<td><span style="width:120px;"><fmt:message key="modifedtime"/></span></td>
<!-- 								<td><span style="width:50px;">当前售价</span></td> -->
								<!-- <td><span style="width:60px;"><fmt:message key="the_lowest_ratio_of_inspection"/></span></td>
								<td><span style="width:60px;"><fmt:message key="the_highest_ratio_of_inspection"/></span></td>
								<td><span style="width:70px;"><fmt:message key="highest_inventory"/></span></td>
								<td><span style="width:70px;"><fmt:message key="minimum_inventory"/></span></td>
								<td><span style="width:60px;"><fmt:message key="average_daily_consumption"/></span></td>
								<td><span style="width:60px;"><fmt:message key="procurement_cycle"/></span></td> -->
							</tr>
						</thead>
					</table>
				</div>
				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="firmSupply" items="${listFirmSupply}" varStatus="status">
								<tr <c:if test="${empty firmSupply.positn or firmSupply.ynsto == 'N'}">style="color:#B7B7B7;"</c:if>
									<%-- <c:choose>
										<c:when test="${empty firmSupply.positn or firmSupply.ynsto == 'N'}">style="color:#B7B7B7;"</c:when>
										<c:otherwise>
											<c:if test="${isDistributionUnit == 'Y' and empty firmSupply.disunit }">style="color:red;"</c:if>
										</c:otherwise>
									</c:choose> --%>
									>
									<td><span title="${status.index+1}" style="width:30px;text-align:center;">${status.index+1}</span></td>
									<td><span style="width:80px;" title="${firmSupply.sp_code}">${firmSupply.sp_code}&nbsp;</span></td>
									<td><span style="width:120px;" title="${firmSupply.sp_name}">${firmSupply.sp_name}&nbsp;</span></td>
									<td><span style="width:60px;"title="${firmSupply.sp_init}">${firmSupply.sp_init}&nbsp;</span></td>
									<td><span style="width:80px;" title="${firmSupply.sp_desc}">${firmSupply.sp_desc}&nbsp;</span></td>
									<td><span style="width:50px;" title="${firmSupply.unit}">${firmSupply.unit}&nbsp;</span></td>
									<td><span style="width:50px;" title="${firmSupply.supply.unit3}">${firmSupply.supply.unit3}&nbsp;</span></td>
									<td><span style="width:50px;text-align:right;" title="${firmSupply.supply.unitper3}">${firmSupply.supply.unitper3}&nbsp;</span></td>
									<td><span style="width:50px;" title="${firmSupply.ynsto}">
										<c:choose>
											<c:when test="${firmSupply.ynsto == 'Y'}"><fmt:message key="be"/></c:when>
											<c:otherwise><fmt:message key="no1"/></c:otherwise>
										</c:choose>
									&nbsp;</span></td>
									<td><span style="width:50px;" title="${firmSupply.ynpd}">
										<c:choose>
											<c:when test="${firmSupply.ynpd == 'Y'}"><fmt:message key="be"/></c:when>
											<c:otherwise><fmt:message key="no1"/></c:otherwise>
										</c:choose>
									&nbsp;</span></td>
									<c:choose>
										<c:when test="${isDistributionUnit == 'Y' }">
											<td><span style="width:50px;" title="${firmSupply.disunit }">${firmSupply.disunit }</span></td>
											<td><span style="width:50px;text-align:right;" title="${firmSupply.disunitper }">${firmSupply.disunitper }</span></td>
											<td><span style="width:50px;text-align:right;" title="${firmSupply.dismincnt }">${firmSupply.dismincnt }</span></td>
											<td><span style="width:90px;">
												<c:choose>
													<c:when test="${empty firmSupply.positn }">未下发</c:when>
													<c:when test="${!empty firmSupply.positn && firmSupply.ynsto == 'N'}">已下发不可报货</c:when>
													<c:otherwise>已下发可报货</c:otherwise>
												</c:choose>
											</span></td>
										</c:when>
										<c:otherwise>
											<td <c:if test="${isPositnSpcodeJmj=='Y' }"> style="display:none;"</c:if>><span style="width:60px;text-align: right;" title="${firmSupply.stomin }"><fmt:formatNumber value="${firmSupply.stomin}" pattern="##.##"/>&nbsp;</span></td>
											<td <c:if test="${isPositnSpcodeJmj=='Y' }"> style="display:none;"</c:if>><span style="width:50px;text-align: right;" title="${firmSupply.stocnt }"><fmt:formatNumber value="${firmSupply.stocnt}" pattern="##.##"/>&nbsp;</span></td>
											<td <c:if test="${isPositnSpcodeJmj=='N' }"> style="display:none;"</c:if>><span style="width:90px;">
												<c:choose>
													<c:when test="${empty firmSupply.positn }">未下发</c:when>
													<c:when test="${!empty firmSupply.positn && firmSupply.ynsto == 'N'}">已下发不可报货</c:when>
													<c:otherwise>已下发可报货</c:otherwise>
												</c:choose>
											</span></td>
										</c:otherwise>
									</c:choose>
									<td><span style="width:120px;" title="${firmSupply.ts }">${firmSupply.ts }</span></td>
<%-- 									<td><span style="width:50px;" title="${firmSupply.sp_price}">${firmSupply.sp_price}&nbsp;</span></td> --%>
								<!--  	<td><span style="width:60px;text-align: right;" title="${firmSupply.sp_min1}">${firmSupply.sp_min1}&nbsp;</span></td>
									<td><span style="width:60px;text-align: right;" title="${firmSupply.sp_max1}">${firmSupply.sp_max1}&nbsp;</span></td>
									<td><span style="width:70px;text-align: right;" title="${firmSupply.yhrate1}">${firmSupply.yhrate1}&nbsp;</span></td>
									<td><span style="width:70px;text-align: right;" title="${firmSupply.yhrate2}">${firmSupply.yhrate2}&nbsp;</span></td>
									<td><span style="width:60px;text-align: right;" title="<fmt:formatNumber value="${firmSupply.cntuse}" pattern="##.##"/>">
										<fmt:formatNumber value="${firmSupply.cntuse}" pattern="##.##"/>
										&nbsp;</span></td>
									<td><span style="width:60px;text-align: right;" title="${firmSupply.datsto}">${firmSupply.datsto}&nbsp;</span></td>-->
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		
		<script type="text/javascript">
			//ajax同步设置
			$.ajaxSetup({
				async: false
			});
			$(document).ready(function(){
				var tool = $('.tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							 $('#listForm').submit();
						}
					},{
							text: '<fmt:message key="export"/>',
							title: '<fmt:message key="export"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								$("#wait2").val('NO');//不用等待加载
								$("#listForm").attr("action","<%=path%>/positnSpcodeMis/exportPositnSpcode.do");
								$("#listForm").submit();
								$("#wait2 span").html("数据导出中，请稍后...");
								$("#listForm").attr("action","<%=path%>/positnSpcodeMis/table.do");
								$("#wait2").val('');
							}
						},{
							text: '<fmt:message key="quit"/>',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '/Choice/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}
					]
				});
				setElementHeight('.grid',['.tool'],$(document.body),120);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
// 				changeTh();
				$('.grid').find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
					);
				$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
			 	});
				//如果全选按钮选中的话，table背景变色
				$("#chkAll").click(function() {
	                if (!!$("#chkAll").attr("checked")) {
	                	$('.grid').find('.table-body').find('tr').addClass("bgBlue");
	                }else{
	                	$('.grid').find('.table-body').find('tr').removeClass("bgBlue");
                	      }
	            });
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }else{
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				 });
			});		
		</script>
	</body>
</html>