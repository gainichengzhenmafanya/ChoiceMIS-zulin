<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>POS<fmt:message key="set_up_the" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		</head>
	<body>
		<div class="form">
			<form id="listForm" action="<%=path%>/operationManagement/saveData.do" method="post">
				<div class="form-line" style="margin-top:10px;">
					<div class="form-label"><span class="red">*</span>存款交易流水号:</div>
					<div class="form-input">
						<input type="text" name="vsavingno" id="vsavingno" class="text" value="${cashSaving.vsavingno }" />
					</div>
					<div class="form-label"><fmt:message key="hr_Transfer_to_the_account" />:</div>
					<div class="form-input">
						<select id="vsavingtypeid" name="vsavingtypeid" class="select" style="width:133px;">
							<c:forEach var="accountType" items="${listAccountType }">
								<option value="${accountType.pk_accounttype }" <c:if test="${accountType.pk_accounttype == cashSaving.vsavingtypeid }">selected="selected"</c:if>>${accountType.vname }</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="business_day" />:</div>
					<div class="form-input">
						<input autocomplete="off" type="text" id="dworkdate" name="dworkdate" class="Wdate text" value="${dworkdate }" onchange="findNcash(this.value)"/>
					</div>
					<div class="form-label">应上缴现金金额:</div>
					<div class="form-input">
						<input type="text" name="ncash" id="ncash" class="text" value="${ncash }" readonly="readonly" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="Deposit_receipt_date" />:</div>
					<div class="form-input">
						<input autocomplete="off" type="text" id="vsavingdate" name="vsavingdate" class="Wdate text" value="${vsavingdate }" onchange="changeSavingdate(this.value)"/>
					</div>
					<div class="form-label"><span class="red">*</span><fmt:message key="bank" /><fmt:message key="The_date_of_the_deposit" />:</div>
					<div class="form-input">
						<input type="text" name="namount" id="namount" class="text" maxlength="10"
							   onkeyup="this.value=this.value.replace(/^(\-)*(\d+)\.(\d\d\d).*$/,'$1$2.$3')" 
							   onchange="amountChange(this.value)" value="${cashSaving.namount }" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="currencys" />:</div>
					<div class="form-input">
						<select id="vcurrencytypeid" name="vcurrencytypeid" class="select" style="width:133px;">
							<c:forEach var="paymode" items="${listPayMode }">
								<option value="${paymode.pk_paymode }" <c:if test="${paymode.pk_paymode == cashSaving.vcurrencytypeid }">selected="selected"</c:if>>${paymode.vname }</option>
							</c:forEach>
						</select>
					</div>
					<div class="form-label"><fmt:message key="Deposit_difference" />:</div>
					<div class="form-input">
						<input type="text" name="ndeposit" id="ndeposit" class="text" value="<fmt:formatNumber value='${cashSaving.ndeposit }' pattern='###0.00'/>" readonly="readonly" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="remark" />:</div>
					<div class="form-input">
						<textarea id="vcomments" name="vcomments" style="width:400px;height:80px;margin-top:3px;" maxlength="250">${cashSaving.vcomments }</textarea>
					</div>
				</div>
				<div style="margin-top:70px;margin-left: 97px;"><span style="color: red;">注:&nbsp;&nbsp;一次只能录入一个存款单的信息</span></div>
				<input type="hidden" name="flag" id="flag" value="${flag }" />
				<input type="hidden" name="isgo" id="isgo" value="no" />
				<input type="hidden" name="nowDate" id="nowDate" value="${dworkdate }" />
				<input type="hidden" name="nowDate1" id="nowDate1" value="${vsavingdate }" />
				<input type="hidden" name="pk_cashsaving" id="pk_cashsaving" value="${cashSaving.pk_cashsaving }" />
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>	
		<script type="text/javascript">
			$("#vsavingdate").click(function(){
	  			new WdatePicker({maxDate:'#F{$dp.$D(\'nowDate1\')}',dateFmt:'yyyy-MM-dd HH:mm:ss'});
	  		});
			$("#dworkdate").click(function(){
	  			new WdatePicker({maxDate:'#F{$dp.$D(\'nowDate\')}'});
	  		});
			$("#dworkdate,#vsavingdate").change(function(){
				if($("#dworkdate").val()>$("#vsavingdate").val()){
					alert("营业日不可大于存款回单日期");
					$("#dworkdate").val($("#nowDate").val());
					$("#vsavingdate").val($("#nowDate1").val());
				}
			});
			
			$(document).ready(function(){
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'namount',
						validateType:['canNull','num2'],
						param:['F','F'],
						error:['<fmt:message key="amount" /><fmt:message key="cannot_be_empty" />!','<fmt:message key="amount" /><fmt:message key="must_be_numeric" />!']
					},{
						type:'text',
						validateObj:'vsavingdate',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="Deposit_receipt_date" /><fmt:message key="cannot_be_empty" />!']
					},{
						type:'text',
						validateObj:'dworkdate',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="business_day" /><fmt:message key="cannot_be_empty" />!']
					},{
						type:'text',
						validateObj:'vsavingno',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="The_deposit_receipt" /><fmt:message key="cannot_be_empty" />!']
					},{
						type:'text',
						validateObj:'vsavingno',
						validateType:['handler'],
						handler:function(){
							$.ajaxSetup({ 
								  async: false 
						  	});
							var result = true;
							var data = {};
							data["vsavingno"] = $("#vsavingno").val();
							$.post("<%=path%>/operationManagement/checkSavingno.do",data,function(returndata){
								if (returndata['savno'] != "1") {
									result = false;
								}
							});
							return result;
						},
						param:['F'],
						error:['存款交易流水号已存在！']
					},{
						type:'text',
						validateObj:'vsavingno',
						validateType:['handler'],
						handler:function(){
							$.ajaxSetup({ 
								  async: false 
						  	});
							var result = true;
							var data = {};
							data["edat"] = $("#dworkdate").val();
							$.post("<%=path%>/operationManagement/checkUpFlag.do",data,function(returndata){
								if (returndata['uploadFlag'] != "1") {
									result = false;
								}
							});
							return result;
						},
						param:['F'],
						error:['数据为实时数据！']
					},{
						type:'text',
						validateObj:'vsavingdate',
						validateType:['handler'],
						handler:function(){
							$.ajaxSetup({ 
								  async: false 
						  	});
							var result = true;
							var data = {};
							data["vsavingdate"] = $("#vsavingdate").val();
							$.post("<%=path%>/operationManagement/checkSavingDate.do",data,function(returndata){
								if (returndata['savdate'] != "1") {
									result = false;
								}
							});
							return result;
						},
						param:['F'],
						error:['小于上个单据回单时间的数据不能新增！']
					}]
				});
			});
			
			var data = {};
			
			//离开银行存款金额焦点后，获取当前营业日、当前门店所有的存款金额再加上当前输入的金额减去营业日现金合计金额，赋值给存款差异
			var amountChange = function(amount) {
				data["dworkdate"] = $("#dworkdate").val();
				var ncash = $("#ncash").val();
				$.post("<%=path%>/operationManagement/findAmount.do", data, function(re){
					$('#ndeposit').val((Number(amount) + Number(re) - Number(ncash)).toFixed(2));
				});
			};
			
			//若选择当前日期之前的日期作为营业日，则在离开营业日焦点后，获取选择的营业日下的存款金额
			var findNcash = function(dworkdate) {
				data["dworkdate"] = dworkdate;
				var namount = $("#namount").val();	//输入的银行存款金额
				$.post("<%=path%>/operationManagement/findNcash.do", data, function(re){
					var cash = re['cash'];			//所选营业日下的现金金额
					var amount = re['amount'];		//所选营业日下的存款金额总和
					$('#ncash').val(cash.toFixed(2))
					
					//若输入的存款金额不为空，则重新为存款差异赋值
					if ('' != namount) {
						$('#ndeposit').val((Number(namount) + Number(amount) - Number(cash)).toFixed(2));
					}
				});
			};
			
			var changeSavingdate = function(vsavingdate) {};
		</script>
	</body>
</html>