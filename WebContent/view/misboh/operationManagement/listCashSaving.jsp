<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>POS<fmt:message key="set_up_the" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		</head>
	<body>
		<div id="dialog" hidden="hidden" style="text-align: center;padding-top: 20px;font-size: 14px;color: red;" title="<fmt:message key='tips' />"></div>
	    <div class="tool"></div>
		<form id="listForm" action="<%=path%>/operationManagement/findDepositManagement.do" method="post">
			<div class="form-line" style="margin-top:5px;">
				<div class="form-label"><fmt:message key="startdate" /></div>
				<div class="form-input">
					<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" value="${bdat }" />
				</div>
				<div class="form-label"><fmt:message key="enddate" /></div>
				<div class="form-input">
					<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" value="${edat }" />
				</div>
				<div class="form-input" style="width:200px;">
				<input type="radio" name="queryMod" id="H" value="H" /><label for="H">按回单日期</label>
				<input type="radio" name="queryMod" id="Y" value="Y" /><label for="Y">按营业日</label>
			</div>
			</div>
			<div class="grid" >
				<div class="table-head" style="width:1500px;">
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td style="width: 25px;">&nbsp;</td>
								<td style="width:30px;">
									<input type="checkbox" id="chkAll"/>
								</td>
								<td style="width:100px;"><fmt:message key="business_day" /></td>
								<td style="width:130px;"><fmt:message key="Deposit_receipt_date" /></td>
								<td style="width:130px;">日报表应上缴现金金额</td>
								<td style="width:130px;"><fmt:message key="The_date_of_the_deposit" /></td>
								<td style="width:130px;"><fmt:message key="Deposit_difference" /></td>
								<td style="width:130px;">存款交易流水号</td>
								<td style="width:250px;"><fmt:message key="remark" /></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body" style="width:1500px;">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="cashSaving" items="${listCashSving}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td style="width:30px;text-align: center;">
										<input type="checkbox" name="idList" 
										id="chk_<c:out value='${cashSaving.pk_cashsaving}' />" value="<c:out value='${cashSaving.pk_cashsaving}' />"/>
										<input type="hidden" name="pk_store" id="pk_store" value="${cashSaving.pk_store }" />
										<input type="hidden" name="nowDate" id="nowDate" value="${nowDate }" />
										<input type="hidden" name="savingdate" id="savingdate" value="${bdat }" />
									</td>
									<td style="width:100px;text-align:center;"><c:out value="${cashSaving.dworkdate}" /></td>
									<td style="width:130px;text-align:center;"><c:out value="${cashSaving.vsavingdate}" /></td>
									<td style="width:130px;text-align:right;"><fmt:formatNumber value="${cashSaving.ncash}" pattern="###0.00"/>&nbsp;</td>
									<td style="width:130px;text-align:right;"><fmt:formatNumber value="${cashSaving.namount}" pattern="###0.00"/>&nbsp;</td>
									<td style="width:130px;text-align:right;"><fmt:formatNumber value="${cashSaving.ndeposit}" pattern="###0.00"/>&nbsp;</td>
									<td style="width:130px;">&nbsp;<c:out value="${cashSaving.vsavingno}" /></td>
									<td style="width:250px;"><c:out value="${cashSaving.vcomments}" /></td>
								</tr>
							</c:forEach>
							<tr>
									<td class="num" style="width:157px;text-align: right" colspan="3"><fmt:message key="total" />：</td>
									<td style="width:130px;"></td>
									<td style="width:130px;text-align: right"><fmt:formatNumber value="${cs.ncash}" pattern="###0.00"/>&nbsp;</td>
									<td style="width:130px;text-align:right"><fmt:formatNumber value="${cs.namount}" pattern="###0.00"/>&nbsp;</td>
									<td style="width:130px;"></td>
									<td style="width:130px;"></td>
									<td style="width:250px;"></td>
								</tr>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>	
		<script type="text/javascript">
			$(document).ready(function(){
				var mod='${queryMod}';
				if(mod=='Y'){
					$("#Y").attr("checked", true);
				}else{
					$("#H").attr("checked", true);
				}
				
				$("#bdat").click(function(){
		  			new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});
		  		});
				$("#edat").click(function(){
		  			new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});
		  		});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								$("#listForm").submit();
							}
						},'-',{
							text: '<fmt:message key="insert" />',
							title: '<fmt:message key="insert"/><fmt:message key="data"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								addCashSaving();	
							}
						},
// 						{
// 							text: '<fmt:message key="update" />',
// 							title: '<fmt:message key="update"/><fmt:message key="data"/>',
// 							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
// 							icon: {
<%-- 								url: '<%=path%>/image/Button/op_owner.gif', --%>
// 								position: ['-18px','0px']
// 							},
// 							handler: function(){
// 								updateCashSaving();	
// 							}
// 						},
						{
							text: '<fmt:message key="delete" />',
							title: '<fmt:message key="delete"/><fmt:message key="data"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								deleteCashSaving();	
							}
						},'-',{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}
					]
				});
				
				$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0).click();
			 		}
			 	});
				
				// 自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),60);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
			});

			//双击弹出<fmt:message key="update" />
			$('.grid').find('.table-body').find('tr').live("dblclick", function () {
				$(":checkbox").attr("checked", false);
				$(this).find(':checkbox').attr("checked", true);
// 				updateCashSaving();
				if($(this).hasClass("show-firm-row"))return;
				$('.grid').find('.table-body').find('tr').removeClass("show-firm-row");
				$(this).addClass("show-firm-row");
			 });
			
			//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     }
			     else
			     {
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			     }
			 });
			
			//新增存款
			function addCashSaving(){
				var nowDate = '${nowDate}';
				var dworkdate = $("#bdat").val();
				if (nowDate > dworkdate) {
					alert('开始日期超过当前日期一个星期，不可进行<fmt:message key="insert" /><fmt:message key="operation" />！');
				} else {
					$('body').window({
						id: 'window_savecashSaving',
						title: '<fmt:message key="insert" /><fmt:message key="data" />',
						content: '<iframe id="SaveForm" frameborder="0" src="<%=path%>/operationManagement/addData.do?flag=add"></iframe>',
						width: '600px',
						height: '300px',
						draggable: true,
						isModal: true,
						position : {
							left : 260
						},
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save_role_of_information"/>',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('SaveForm')&&window.document.getElementById("SaveForm").contentWindow.validate._submitValidate()){
											if(confirm("是否还需录入下一张存款单信息？")){
												 document.getElementById("SaveForm").contentWindow.document.forms["listForm"].isgo.value="yes";
											}
											document.getElementById("SaveForm").contentWindow.document.forms["listForm"].submit();
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});	
				}
			}
			
			//修改存款
			function updateCashSaving(){
				var nowDate = '${nowDate}';
				var dworkdate = $("#bdat").val();
				if (nowDate > dworkdate) {
					alert('开始日期超过当前日期一个星期，不可进行<fmt:message key="update" /><fmt:message key="operation" />！');
				} else {
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					if(checkboxList && checkboxList.filter(':checked').size() ==1){
						var action='<%=path%>/operationManagement/addData.do?flag=update&pk_cashsaving='+checkboxList.filter(':checked').val();
						$('body').window({
							id: 'window_selectTargetStore',
							title: '<fmt:message key="update" /><fmt:message key="data" />',
							content: '<iframe id="UpdateForm" frameborder="0" src='+action+'></iframe>',
							width: '600px',
							height: '300px',
							draggable: true,
							isModal: true,
							position : {
								left : 260
							},
							topBar: {
								items: [{
										text: '<fmt:message key="save" />',
										title: '<fmt:message key="update" /><fmt:message key="data" />',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-80px','-0px']
										},
										handler: function(){ 
											window.document.getElementById("UpdateForm").contentWindow.document.forms["listForm"].submit();
										}
									},{
										text: '<fmt:message key="cancel" />',
										title: '<fmt:message key="cancel" />',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-160px','-100px']
										},
										handler: function(){
											$('.close').click();
										}
									}
								]
							}
						});
					}else if(checkboxList && checkboxList.filter(':checked').size()>1){
						alert('<fmt:message key="you_can_only_modify_a_data" />!');
					}else{
						alert('<fmt:message key="please_select_data" />!');
					}
				}
			}
			
			//删除数据
			function deleteCashSaving(){
				var nowDate = '${nowDate}';
				var dworkdate = $("#bdat").val();
				if (nowDate > dworkdate) {
					alert('开始日期超过当前日期一个星期，不可进行<fmt:message key="delete" /><fmt:message key="operation" />！');
				} else {
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					if(checkboxList && checkboxList.filter(':checked').size() > 0){
						if(confirm('<fmt:message key="delete_data_confirm" />？')){
							var chkValue = [];
							checkboxList.filter(':checked').each(function(){
								chkValue.push($(this).val());
							});
							var action = '<%=path%>/operationManagement/saveData.do?flag=delete&pk_cashsaving='+chkValue.join(",");
							$('body').window({
								title: '<fmt:message key="delete" /><fmt:message key="data" />',
								content: '<iframe frameborder="0" src='+action+'></iframe>',
								width: '340px',
								height: '255px',
								draggable: true,
								isModal: true
							});
						}
					}else{
						alert('<fmt:message key="please_select_information_you_need_to_delete" />！');
						return ;
					}
				}
			}
		</script>
	</body>
</html>