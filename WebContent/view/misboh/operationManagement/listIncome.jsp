<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>POS<fmt:message key="set_up_the" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/operationManagement/findOutLay.do?flag=income" method="post">
			<div class="form-line" style="margin-top:5px;">
				<div class="form-lable" style="padding-left:30px;">
					<fmt:message key="business_day" />:
					<input autocomplete="off" type="text" id="dworkdate" name="dworkdate" class="Wdate text" value="${dworkdate }"/>
				</div>
			</div>
			<div class="grid" style="margin-top:5px;">
				<div class="table-head" style="width:1500px;">
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td style="width: 25px;">&nbsp;</td>
								<td style="width:151px;"><fmt:message key="project" /><fmt:message key="name" /></td>
								<td style="width:100px;"><fmt:message key="quantity" /></td>
								<td style="width:150px;"><fmt:message key="amount" /></td>
								<td style="width:260px;"><fmt:message key="remark" /></td>
								<td style="visibility: hidden; "></td>
								<td style="visibility: hidden; "></td>
								<td style="visibility: hidden; "></td>
								<td style="visibility: hidden; "></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body" style="width:1500px;">
					<table cellspacing="0" cellpadding="0" id="tableBody">
						<tbody>
							<c:forEach var="income" items="${listIncome}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td style="width:100px;text-align: left;">
										<span><c:out value="${income.vname}" /></span>
									</td>
									<td style="width:100px;">
										<input type="text" class="text" id="iquantity${status.index+1}" name="iquantity" <c:if test="${false == timeCompare }">disabled="disabled"</c:if> onblur="checkPrice(${status.index+1})" style="text-align: right;width:98px;" value="${income.iquantity}" />
									</td>
									<td style="width:150px;">
										<input type="text" class="text" id="nprice${status.index+1}" name="nprice" 
											   <c:if test="${false == timeCompare }">disabled="disabled"</c:if> 
											   onblur="checkPrice(${status.index+1})" style="text-align: right;width:148px;" 
											   value="<fmt:formatNumber value="${income.nprice}" pattern="###0.00"/>" />
									</td>
									<td style="width:260px;text-align: left;">
										<input type="text" class="text" id="vcomments${status.index+1}" name="vcomments" <c:if test="${false == timeCompare }">disabled="disabled"</c:if> style="width:258px;" value="${income.vcomments}" />
									</td>
									<td style="visibility: hidden; "><span>${income.pk_outlay }</span></td>
									<td style="visibility: hidden; "><span>${pk_store }</span></td>
									<td style="visibility: hidden; "><span>${vscode }</span></td>
									<td style="visibility: hidden; "><span>${vecode }</span></td>
									<td style="visibility: hidden; "><span>${dworkdate }</span></td>
								</tr>
							</c:forEach>
								<tr>
									<td class="num"><span style="width: 15px;"></span></td>
									<td>
										<span style="width:141px;text-align: right;"><fmt:message key="total" />：</span>
									</td>
									<td id="countQuantity" style="text-align: right;">
										<span style="width:90px;">${iquantity}</span>
									</td>
									<td id="countPrice" style="text-align: right;">
										<span style="width:140px;text-align: right;"><fmt:formatNumber value="${nprice}" pattern="###0.00"/></span>
									</td>
									<td><span style="width:250px;text-align: left;"></span></td>
								</tr>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>	
		<script type="text/javascript">
			$(document).ready(function(){
				$("#iquantity1").focus();
				$("#dworkdate").click(function(){
		  			new WdatePicker();
		  		});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								location.href="<%=path%>/operationManagement/findOutLay.do?flag=income&dworkdate="+$('#dworkdate').val();
							}
						},'-',{
							text: '<fmt:message key="save" />',
							title: '<fmt:message key="save" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								saveData();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}
					]
				});
				
				$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0).click();
			 		}
			 	});
				
				// 自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),20);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				
				//定义数字并加入字段
				var tabArray = [];
				for (var i = 1; '${number}' >= i; i++) {
					tabArray.push('iquantity'+i);
					tabArray.push('nprice'+i);
					tabArray.push('vcomments'+i);
				}
					
				//定义加载后定位在第一个输入框末尾上  
				var id = document.getElementById(tabArray[0]);
				$('#'+tabArray[0]).focus();
				id.value = id.value;
				
				$('.text').not($('input[type="hidden"],input:disabled')).keydown(function(e) { 
					
					//使用jquery的这种获取事件方法，不必再指定event.srcElement或者event.target  
					var event = $.event.fix(e);
					
					//判断如果按键事件的按键代码是回车，则从数组中获取下一个元素的ID,并设置焦点                       
					if (event.keyCode == 13) {
						var index = $.inArray($.trim($(event.target).attr("id")), tabArray);
						if(index == tabArray.length-1){
							saveData(); //切换到最后一个控件后，执行新增
							return;
						}
						index=index+1;
						if(index==tabArray.length) index=0;
						
						var idIndex = document.getElementById(tabArray[index]);
						$('#'+tabArray[index]).focus();	
						idIndex.value = idIndex.value;
					}
				});
			});
			
			function checkPrice(i){
				var countPrice = 0.00;
				var countQuantity = 0;
				if(!$("#nprice"+i).val().match("^\\d+(\\.\\d+)?$")) {
					alert("<fmt:message key="amount" /><fmt:message key="only_digits" />");
					$("#nprice"+i).val('0.00');
					$("#nprice"+i).focus();
					return true;
				}
				
				if(!$("#iquantity"+i).val().match("^\\d+(\\.\\d+)?$")) {
					alert("<fmt:message key="quantity" /><fmt:message key="only_digits" />");
					$("#iquantity"+i).val(0);
					$("#iquantity"+i).focus();
					return true;
				}
				
				for (var k = 1; 100 > k; k++) {
					if(!$("#nprice"+k).val().match("^\\d+(\\.\\d+)?$")) {
							
					}else{
						countPrice = Number(countPrice) + Number($("#nprice"+k).val());
					}
					if(!$("#iquantity"+k).val().match("^\\d+(\\.\\d+)?$")) {
						
					}else{
						countQuantity = Number(countQuantity) + Number($("#iquantity"+k).val());
					}
					$("#countPrice").text(countPrice.toFixed(2));
					$("#countQuantity").text(countQuantity);
				}
			}
			
			var saveData = function(){
				var data = {};
				$.ajaxSetup({async:false});
				$("#tableBody").find("tr").each(function(i){
                    data["listCashMx["+i+"].iquantity"]= $.trim($(this).find("td:eq(2)").find("input").val());
                    data["listCashMx["+i+"].nprice"]= $.trim($(this).find("td:eq(3)").find("input").val());
                    data["listCashMx["+i+"].vcomments"]=$.trim($(this).find("td:eq(4)").find("input").val());
					data["listCashMx["+i+"].vitemcode"]=$.trim($(this).find("td:eq(5)").find("span").text());
					data["listCashMx["+i+"].vmxcode"]=$.trim($(this).find("td:eq(5)").find("span").text());
					data["listCashMx["+i+"].pk_store"]= $.trim($(this).find("td:eq(6)").find("span").text());
                    data["listCashMx["+i+"].vscode"]= $.trim($(this).find("td:eq(7)").find("span").text());
                    data["listCashMx["+i+"].vecode"]= $.trim($(this).find("td:eq(8)").find("span").text());
                    data["listCashMx["+i+"].dworkdate"]= $.trim($(this).find("td:eq(9)").find("span").text());
				});
				
				$.post("<%=path%>/operationManagement/saveCashMx.do",data,function(){
					$('body').window({
						id: 'window_saveStorePos',
						title: '<fmt:message key="save" /><fmt:message key="data" />',
						content: '<iframe id="SaveForm" frameborder="0" src="<%=path%>/operationManagement/toList.do?flag=income"></iframe>',
						width: '350px',
						height: '300px',
						draggable: true,
						isModal: true,
						position : {
							left : 260
						}
					});
				});
			};
		</script>
	</body>
</html>