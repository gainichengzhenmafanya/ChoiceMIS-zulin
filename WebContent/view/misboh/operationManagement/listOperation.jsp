<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>POS<fmt:message key="set_up_the" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/baseRecord/listspecialItem.do" method="post">
			<div class="form-line" style="margin-top:5px;">
				<div class="form-lable" style="padding-left:30px;">
					<fmt:message key="business_day" />:
					<input autocomplete="off" type="text" id="dworkdate" name="dworkdate" class="Wdate text" value="${dworkdate }"/>
				</div>
			</div>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td style="width: 25px;">&nbsp;</td>
								<td style="width:100px;"><fmt:message key="coding" /></td>
								<td style="width:200px;"><fmt:message key="name" /></td>
								<td style="width:202px;"><fmt:message key="content" /></td>
								<td style="visibility: hidden;"></td>
								<td style="visibility: hidden;"></td>
								<td style="visibility: hidden;"></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" id="tableBody">
						<tbody>
							<c:forEach var="specialItem" items="${listSpecialItem}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td style="width:100px;text-align: left;"><span style="width: 90px;"><c:out value="${specialItem.vcode}" /></span></td>
									<td style="width:200px;"><span style="width: 190px;"><c:out value="${specialItem.vname}" /></span></td>
									<td style="width:150px;text-align: left;">
										<c:if test="${'1' == specialItem.vitemtyp }">
											<select id="vcontent${status.index}" name="vcontent${status.index+1}" style="width:202px;" class="select" <c:if test="${false == timeCompare }">disabled="disabled"</c:if>>
												<c:forEach var="dtl" items="${specialItem.listSelectItemDtl }">
													<option value="${dtl.vname }" <c:if test="${dtl.vname == specialItem.vcontent }">selected="selected"</c:if>>${dtl.vname }</option>
												</c:forEach>
											</select>
										</c:if>
										<c:if test="${'2' == specialItem.vitemtyp }">
											<input type="text" id="vcontent${status.index}" name="vcontent${status.index+1}" <c:if test="${false == timeCompare }">disabled="disabled"</c:if> value="${specialItem.vcontent }" style="width:200px;" class="text" />
										</c:if>
									</td>
									<td style="visibility: hidden;">
										<input type="hidden" name="pk_store" id="pk_store" value="${pk_store }" />
									</td>
									<td style="visibility: hidden;">
										<input type="hidden" name="dworkdate" id="dworkdate" value="${dworkdate }" />
									</td>
									<td style="visibility: hidden;">
										<input type="hidden" name="vitemtyp" id="vitemtyp" value="${specialItem.vitemtyp }" />
									</td>
									<td style="visibility: hidden;">
										<input type="hidden" name="pk_specialitem" id="pk_specialitem" value="${specialItem.pk_specialitem }" />
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
		</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>	
	 	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>	
		<script type="text/javascript">
			$(document).ready(function(){
				$("#dworkdate").click(function(){
		  			new WdatePicker();
		  		});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								location.href="<%=path%>/operationManagement/findOperation.do?dworkdate="+$('#dworkdate').val();
							}
						},'-',{
							text: '<fmt:message key="save" />',
							title: '<fmt:message key="save" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								saveData();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}
					]
				});
				
				$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0).click();
			 		}
			 	});
				
				// 自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),80);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
			});
			
			//实现复选框单选
			function CheckBoxCheck(chk){
			    var obj = $('.grid').find('.table-body').find('input[name=idList]');
			    for(var i = 0; i < obj.length; i++){
			        if(obj[i].type == "checkbox"){
			            obj[i].checked = false;
			        }
			    }
			    chk.checked=true;
			}
			
			var saveData = function(){
				var data = {};
				$.ajaxSetup({async:false});
				$("#tableBody").find("tr").each(function(i){
                    data["listSelectItemDtl["+i+"].vcode"]= $.trim($(this).find("td:eq(1)").find("span").text());
                    data["listSelectItemDtl["+i+"].vname"]= $.trim($(this).find("td:eq(2)").find("span").text());
                    data["listSelectItemDtl["+i+"].vcontent"]=$("#vcontent"+i).val();
					data["listSelectItemDtl["+i+"].pk_store"]=$.trim($(this).find("td:eq(4)").find("input").val());
					data["listSelectItemDtl["+i+"].dworkdate"]= $.trim($(this).find("td:eq(5)").find("input").val());
                    data["listSelectItemDtl["+i+"].vitemtyp"]= $.trim($(this).find("td:eq(6)").find("input").val());
                    data["listSelectItemDtl["+i+"].pk_specialitem"]= $.trim($(this).find("td:eq(7)").find("input").val());
				});
				
// 				if (data.length > 0) {
					$.post("<%=path%>/operationManagement/saveSelectItemDtl.do",data,function(){
						$('body').window({
							id: 'window_saveOutlay',
							title: '<fmt:message key="save" /><fmt:message key="data" />',
							content: '<iframe id="SaveForm" frameborder="0" src="<%=path%>/operationManagement/toList.do"></iframe>',
							width: '650px',
							height: '300px',
							draggable: true,
							isModal: true,
						});
					});
// 				} else {
// 					alert('<fmt:message key="misboh_No_data_saved" />!');
// 				}
			};
		</script>
	</body>
</html>