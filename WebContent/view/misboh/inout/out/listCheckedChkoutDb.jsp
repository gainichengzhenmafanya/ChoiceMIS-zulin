<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>positn Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<style type="text/css">
				.page{
					margin-bottom: 25px;
				}
				.tool {
					position: relative;
					height: 27px;
				}
				.condition {
					position: relative;
					top: 1px;
					height: 31px;
					line-height: 31px;
				}
				.grid td span{
					padding:0px;
				}
				.search{
					margin-top:3px;
					cursor: pointer;
				}
				.form-input input[type=text],.form-input select{
					width: 150px;
				}
				form .form-line .form-label{
					width: 60px;
					margin-left: 25px;
				}
				.table-head td span{
					white-space: normal;
				}
			</style>
		</head>
	<body>
		<div class="tool"></div>
		<form action="<%=path%>/chkoutDbMis/list.do?checby=c" id="queryForm" name="queryForm" method="post">
					<div class="form-line">
					<div class="form-label"><fmt:message key="startdate"/></div>
					<div class="form-input"><input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/></div>
<%-- 					<div class="form-label"><fmt:message key="library_positions"/></div> --%>
<!-- 					<div class="form-input"> -->
<%-- 						<input type="text"  id="positn"  name="positn.des" readonly="readonly" value="${chkoutm.positn.des}"/> --%>
<%-- 						<input type="hidden" id="positn_select" name="positn.code" value="${chkoutm.positn.code}"/> --%>
<%-- 						<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' /> --%>
<!-- 					</div> -->
					<div class="form-label"><fmt:message key="orders_num"/></div>
					<div class="form-input"><input type="text" id="chkoutno" name="chkoutno" class="text" value="<c:out value="${chkoutm.chkoutno}" />"/></div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="enddate"/></div>
					<div class="form-input"><input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd"/>" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/></div>
					<div class="form-label"><fmt:message key="document_number"/></div>
					<div class="form-input"><input type="text" id="vouno" name="vouno" class="text" value="<c:out value="${chkoutm.vouno}" />"/></div>
					<div class="form-label"><fmt:message key="Transferin"/><fmt:message key="positions"/></div>
					<div class="form-input">
						<input type="text"  id="firm"  name="firm.des" readonly="readonly" value="${chkoutm.firm.des}"/>
						<input type="hidden" id="firm_select" name="firm.code" value="${chkoutm.firm.code}"/>
						<img id="seachOnePositn1" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>'/>
					</div>
				</div>

			<div class="grid" style="overflow: auto;">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width: 25px">&nbsp;</span></td>
								<td><span style="width:30px;"></span></td>
								<td><span style="width:50px;"><fmt:message key="storehouse_no"/></span></td>
								<td><span style="width:100px;"><fmt:message key="document_number"/></span></td>
								<td><span style="width:80px;"><fmt:message key="document_types"/></span></td>
								<td><span style="width:80px;"><fmt:message key="date"/></span></td>
								<td><span style="width:70px;"><fmt:message key="time"/></span></td>
								<td><span style="width:80px;"><fmt:message key="library_positions"/></span></td>
								<td><span style="width:80px;"><fmt:message key="Transferin"/><fmt:message key="positions"/></span></td>
								<td><span style="width:80px;"><fmt:message key="total_amount1"/></span></td>
								<td><span style="width:80px;"><fmt:message key="orders_maker"/></span></td>
								<td><span style="width:80px;"><fmt:message key="orders_audit"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="chkout" items="${chkoutList}" varStatus="status">
								<tr>
									<td class="num"><span style="width: 25px;">${status.index+1}</span></td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox"  name="idList" id="chk_<c:out value='${chkout.chkoutno}' />" value="<c:out value='${chkout.chkoutno}' />"/>
									</td>
									<td><span style="width:50px;"><c:out value="${chkout.chkoutno}" />&nbsp;</span></td>
									<td><span style="width:100px;"><c:out value="${chkout.vouno}" />&nbsp;</span></td>
									<td><span style="width:80px;"><c:out value="${chkout.typ}" />&nbsp;</span></td>
									<td><span style="width:80px;"><fmt:formatDate value="${chkout.maded}" pattern="yyyy-MM-dd"/>&nbsp;</span></td>
									<td align="center">
										<span style="width:70px;">
											<!-- 查询已审核出库单，日期的格式最好显示一致。 wangjie 2014年12月12日 15:03:32 -->
											<c:choose>
												<c:when test="${fn:length(chkout.madet) > 8}">
													${fn:substring(chkout.madet,11,19) }&nbsp;
												</c:when>
												<c:otherwise>
													${chkout.madet }&nbsp;
												</c:otherwise>
											</c:choose>
										</span>
									</td>
									<td><span style="width:80px;"><c:out value="${chkout.positn.des}" />&nbsp;</span></td>
									<td><span style="width:80px;"><c:out value="${chkout.firm.des}" />&nbsp;</span></td>
									<td align="right"><span style="width:80px;"><c:out value="${chkout.totalamt}" />&nbsp;</span></td>
									<td><span style="width:80px;"><c:out value="${chkout.madebyName}" />&nbsp;</span></td>
									<td><span style="width:80px;"><c:out value="${chkout.checbyName}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="queryForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			function pageReload(){
				$('#queryForm').submit();
			}
			function clearQueryForm(){
				$('#queryForm input[type="text"]').val('');
				$('#queryForm select option').removeAttr("selected");;
				$('#queryForm select option[value=""]').attr("selected","selected");;
			}
			function execQuery(){
				$('#queryForm').submit();
			}
			$(document).ready(function(){
				focus() ;//页面获得焦点
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit"/>').click();
			 		}
			 	});
				if(!$("#bdat").val())
			 		$("#bdat,#edat").htmlUtils("setDate","now");
				//双击事件
				$('.grid .table-body tr').live('dblclick',function(){
					$(":checkbox").attr("checked", false);
					$(this).find(":checkbox").attr("checked", true);
					toCheckedChkoutm();
				});
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'chkoutno',
						validateType:['canNull','intege'],
						param:['T','T'],
						error:['','<fmt:message key="single_number_is_digital_or_empty"/>']
					}]
				});
				$('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />(<u>F</u>)',
							title: '<fmt:message key="query_storehouse_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								if(validate._submitValidate()){
									execQuery();
								}
							}
						},{
							text: '<fmt:message key="view" />(<u>V</u>)',
							title: '<fmt:message key="query_storehouse_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								toCheckedChkoutm();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}
					]
				});
				
				$('#positn_select').bind('keyup',function(){
			          $("#positn").find('option')
			                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
			                    .attr('selected','selected');
			       });
				$('#firm_select').bind('keyup',function(){
			          $("#firm").find('option')
			                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
			                    .attr('selected','selected');
			       });
				
				$("#positn").focus(function(){
		 			this.size = this.length;
		 			$('#positn').css('height','100px');
		 			$('#positn').parent('.form-input').css('z-index','3');
		 			})
		 		.blur(function(){
		 			this.size = 1;
		 			$('#positn').css('height','20px');
		 			$('#positn_select').val($(this).val());
	 			})
	 			.dblclick(function() {
 					$('#positn_select').val($(this).val());
 					$('#positn').blur();
 				});
		 		$("#firm").focus(function(){
		 			this.size = this.length;
		 			$('#firm').css('height','100px');
		 			$('#firm').parent('.form-input').css('z-index','2');
		 			})
		 		.blur(function(){
		 			this.size = 1;
		 			$('#firm').css('height','20px');
		 			$('#firm_select').val($(this).val());
	 			})
	 			.dblclick(function() {
 					$('#firm_select').val($(this).val());
 					$('#firm').blur();
 				});
				
				
				$('#search').click(function(){
					$("#queryForm").submit();
				});
				$('#resetSearch').click(function(){
					clearQueryForm();
				});
				
				setElementHeight('.grid',['.tool'],$(document.body),100);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();
				$('.grid').find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
					);
					
					//单击每行选中前面的checkbox
					$('.grid').find('.table-body').find('tr').live("click", function () {
						if($(this).find(':checkbox')[0].checked){
							$(":checkbox").attr("checked", false);
						}else{
							$(":checkbox").attr("checked", false);
							$(this).find(':checkbox').attr("checked", true);
						}
					 });
					$('#seachOnePositn1').bind('click.custom',function(e){
						if(!!!top.customWindow){
							var defaultCode = $('#firm_select').val();
							var defaultName = $('#firm').val();
							var offset = getOffset('bdat');
							top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/misbohcommon/findPositnSuper.do?sta=all&typn=3-6&mold=one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#firm'),$('#firm_select'),'760','520','isNull');
						}
					});
			});
			
			//跳转到已审核详细信息
			var detailWin;
			function toCheckedChkoutm(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
// 				var  library_positions= $(".table-body").find("td").eq(7).text();
// 				var  requisitioned_units= $('.table-body').find("td").eq(8).text();
				if(checkboxList&& checkboxList.filter(':checked').size() ==1){
					var chkValue = checkboxList.filter(':checked').val();
					detailWin = $('body').window({
						id: 'checkedChkoutmDetail',
						title: '<fmt:message key="Transfer_out_order_detail"/>',
						content: '<iframe id="checkedChksoutDetailFrame" frameborder="0" src="<%=path%>/chkoutDbMis/searchCheckedChkout.do?chkoutno='+chkValue+'"></iframe>',
						width: '1050px',
						height: '460px',
						draggable: true,
						isModal: true,
						confirmClose:false
					});
						
				}else{
					alert('<fmt:message key="please_select_data_to_view"/>！');
					return ;
				}
			}
			
			function getSelected(){
				var chks = $('.grid .table-body :checkbox').filter(':checked');
				switch(chks.length){
				case 0:
					alert('<fmt:message key="please_select_data_need_to_be_operated"/>！');
					break;
				case 1:
					return $('.grid .table-body :checkbox').filter(':checked').val();
					default:
						alert('<fmt:message key="please_select_data"/>！');
				}
			}
			
		</script>
	</body>
</html>