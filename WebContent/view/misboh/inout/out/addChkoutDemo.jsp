<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="purchase_the_template" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<style type="text/css">
		.blueBGColor{background-color:	#F1F1F1;}
		.redBGColor{background-color:	#F1F1F1;}
		.onEdit{
			background:lightblue;
			border:1px solid;
			border-bottom-color: blue;
			border-top-color: blue;
			border-left-color: blue;
			border-right-color: blue;
		}
		.input{
			background:transparent;
			border:1px solid;
		}
		</style>		
</head>
<body>
	<div class="tool"></div>
	<input type="hidden" id="subSta" name="subSta"/>
	<form id="listForm" action="<%=path%>/chkoutYgcMis/addChkoutDemo.do" method="post">
		<div class="form-line">	
			<div class="form-label"><fmt:message key="title" /></div>
			<div class="form-input">
				<input type="hidden" id="firm" name="firm" value="${firm}"/><!-- 适用分店wjf -->
				<select class="select" id="chkstodemono" name="chkstodemono" onchange="findByTitle(this);">
<%-- 					<option value=""><fmt:message key="all" /></option>				 --%>
					<c:forEach var="chkstodemo" items="${listTitle}" varStatus="status">
						<option
							<c:if test="${chkstodemo.chkstodemono==chkstodemono}"> selected="selected" </c:if> value="${chkstodemo.chkstodemono}">${chkstodemo.title}
						</option>
					</c:forEach>				
				</select>
			</div>
		</div>
	
		<div class="grid">		
			<div class="table-head" >
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<td class="num"><span style="width: 25px;"></span></td>
							<td><span style="width:80px;"><fmt:message key="supplies_code" /></span></td>
							<td><span style="width:120px;"><fmt:message key="supplies_name" /></span></td>
							<td><span style="width:80px;"><fmt:message key="specification" /></span></td>
							<td style="display:none;"><span style="width:80px;"><fmt:message key="price" /></span></td>
							<td><span style="width:80px;"><fmt:message key="quantity" /></span></td>
							<td><span style="width:60px;"><fmt:message key="standard_unit" /></span></td>
							<td><span style="width:80px;"><fmt:message key="quantity" /></span></td>
							<td><span style="width:60px;"><fmt:message key="reference_unit" /></span></td>
							<td><span style="width:80px;"><fmt:message key="inventory"/></span></td>
						</tr>
					</thead>
				</table>
			</div>				
			<div class="table-body">
				<table cellspacing="0" cellpadding="0">
					<tbody>
						<c:forEach var="chkstodemo" items="${chkstodemoList}" varStatus="status">
							<tr data-unitper="${chkstodemo.supply.unitper }">
								<td class="num"><span style="width: 25px;">${status.index+1}</span></td>
								<td><span style="width:80px;" title="${chkstodemo.supply.sp_code}">${chkstodemo.supply.sp_code}</span></td>
								<td><span style="width:120px;" title="${chkstodemo.supply.sp_name}">${chkstodemo.supply.sp_name}</span></td>
								<td><span style="width:80px;" title="${chkstodemo.supply.sp_desc}">${chkstodemo.supply.sp_desc}</span></td>
								<td style="display:none;"><span style="width:80px;text-align:right;" title="${chkstodemo.supply.sp_price}"><fmt:formatNumber value="${chkstodemo.supply.sp_price}" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:80px;text-align:right;" title="0">0.00</span></td>
								<td><span style="width:60px;">${chkstodemo.supply.unit}</span></td>
								<td><span style="width:80px;text-align:right;" title="0">0.00</span></td>
								<td><span style="width:60px;">${chkstodemo.supply.unit1}</span></td>
								<td><span style="width:80px;text-align:right;" title="${chkstodemo.supply.cnt}"><fmt:formatNumber value="${chkstodemo.supply.cnt}" type="currency" pattern="0.00"/></span></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
<%-- 		<page:page form="listForm" page="${pageobj}"></page:page> --%>
<%-- 		<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" /> --%>
<%-- 		<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />		 --%>
<!-- 	</form>				 -->
	</form>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
	<script type="text/javascript">
	var validate;
	//工具栏
	$(document).ready(function(){
		//按钮快捷键
		focus() ;//页面获得焦点
		
		//屏蔽鼠标右键
	 	$(document).bind('keyup',function(e){
	 		if(e.keyCode==27){
	 			parent.$('.close').click();
	 		}
	 		//表格数量一变，校验一下，价格就接着变
	 		if($(e.srcElement).is("input")){//对表格内的输入框进行判读，延迟600毫秒
	 			var index=$(e.srcElement).closest('td').index();
	    		if(index=="5" || index == "7"){
		 			$(e.srcElement).unbind('blur').blur(function(e){
		 				validateByMincnt($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
		 			});
		    		validator($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
	    		}
	    	}
		});
	 	
	   $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
	   $('.grid').find('.table-body').find('tr').hover(
			function(){
				$(this).addClass('tr-over');
			},
			function(){
				$(this).removeClass('tr-over');
			}
		);
		
		//自动实现滚动条
		setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
		setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
		loadGrid();//  自动计算滚动条的js方法	
		editCells();
		$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),5);
	});
	
	//动态下拉列表框
	function findByTitle(e){
		$('#listForm').submit();
	}
	//确认修改
	function enterUpdate() {
		var checkboxList = $('.grid').find('.table-body').find('tr');
		var data = {};
		var i = 0;
		var totalAmt = 0;
		var totalPrice = 0;
		checkboxList.each(function(){
			//1.如果数量是0的过滤掉
			var amount = $(this).find('td:eq(5) span').attr('title');
			if(Number(amount) == 0  || amount == null || amount == ''){
				return true;
			}
			//2.如果已经存在的物资过滤掉
			var sp_code = $(this).find('td:eq(1) span').text();
			var flag = true;
			parent.$('.table-body').find('tr').each(function (){
				if($(this).find("td:eq(1)").text() == sp_code){
					flag = false;
					return false;
				}
			});
			if(!flag){
				return true;
			}
			if(parent.$(".table-body").find("tr:last").find("td:eq(1)").find('span').text() != ''){
				i++;
				parent.$(".table-body").autoGrid.addRow();
			}
			var unitper = $(this).data('unitper');
			var price = $(this).find('td:eq(4)').text();
			parent.$(".table-body").find("tr:last").find("td:eq(1)").find('span').text(sp_code);
			parent.$(".table-body").find("tr:last").find("td:eq(2)").find('span').text($(this).find('td:eq(2) span').text());
			parent.$(".table-body").find("tr:last").find("td:eq(3)").find('span').text($(this).find('td:eq(3) span').text());
			parent.$(".table-body").find("tr:last").find("td:eq(4)").find('span').text($(this).find('td:eq(6) span').text());
			parent.$(".table-body").find("tr:last").find("td:eq(5)").find('span').text(Number(amount).toFixed(2)).attr('title',amount).css("text-align","right");
			var amount1 = 0;
			if(Number(unitper) != 0){
				amount1 = Number(amount)*Number(unitper);
			}
			parent.$(".table-body").find("tr:last").find("td:eq(6)").find('span').text(Number(price).toFixed(2)).attr('title',price).css("text-align","right");
			parent.$(".table-body").find("tr:last").find("td:eq(7)").find('span').text((Number(amount)*Number(price)).toFixed(2)).attr('title',Number(amount)*Number(price)).css("text-align","right");
			parent.$(".table-body").find("tr:last").find("td:eq(8)").find('span').text($(this).find('td:eq(8) span').text());
			parent.$(".table-body").find("tr:last").find("td:eq(9)").find('span').text(Number(amount1).toFixed(2)).attr('title',Number(amount1)).css("text-align","right");
			parent.$(".table-body").find("tr:last").find("td:eq(10)").find('span').text(0).attr('title',0);
			parent.$(".table-body").find("tr:last").find("td:eq(11)").find('span').text(0).attr('title',0);
			parent.$(".table-body").find("tr:last").find("td:eq(12)").find('span').text($(this).find('td:eq(9) span').text()).attr('title',$(this).find('td:eq(9) span').attr('title')).css("text-align","right");
			parent.$(".table-body").find("tr:last").find("td:eq(13)").find('span').text("");
			parent.$(".table-body").find("tr:last").data("unitper",unitper);
			totalAmt = Number(totalAmt) + Number(amount);
			totalPrice = Number(totalPrice) + (Number(amount)*Number(price));
		});
		parent.$('#sum_num').text(Number(parent.$('#sum_num').text()) + i);
		parent.$('#sum_amount').text((Number(parent.$('#sum_amount').text()) + totalAmt).toFixed(2));
		parent.$('#sum_totalamt').text((Number(parent.$('#sum_totalamt').text()) + totalPrice).toFixed(2));
	    //关闭弹出窗口
		parent.$('.close').click();
	}
	//编辑表格
	function editCells(){
		if($(".table-body").find('tr').length == 0){
			return;
		}
		$(".table-body").autoGrid({
			initRow:1,
			colPerRow:10,
			widths:[26,80,120,80,80,80,60,80,60,80],
			colStyle:['','','','','',{background:"#F1F1F1"},'',{background:"#F1F1F1"},'',''],
			VerifyEdit:{verify:true,enable:function(cell,row){
				return row.find('td').index(cell) == 2 || (row.find("td:eq(2)").text() == null || $.trim(row.find("td:eq(2)").text()) != '' ) ? true : false;
			}},
			onEdit:$.noop,
			editable:[5,7],//能输入的位置
			onLastClick:$.noop,
			onEnter:function(data){
				$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.value) ;
			},
			cellAction:[{
				index:5,
				action:function(row,data2){
					if(Number(data2.value) < 0){
						alert('<fmt:message key="number_cannot_be_negative"/>！');
						row.find("td:eq(5)").find('span').text(data2.ovalue);
						$.fn.autoGrid.setCellEditable(row,5);
					}else if(isNaN(data2.value)){
						alert('<fmt:message key="number_be_not_number"/>！');
						row.find("td:eq(5)").find('span').text(data2.ovalue);
						$.fn.autoGrid.setCellEditable(row,5);
					}else if(data2.value.length > 6){
						alert('<fmt:message key="the_maximum_length_not"/>6<fmt:message key="digit"/>！');
						row.find("td:eq(5)").find('span').text(data2.ovalue);
						$.fn.autoGrid.setCellEditable(row,5);
					}else {
						row.find("td:eq(5) span").attr('title',data2.value);
						row.find("td:eq(7) span").text((Number(data2.value)*Number(row.data("unitper"))).toFixed(2)).attr('title',Number(data2.value)*Number(row.data("unitper")));//设置参考数量
						$.fn.autoGrid.setCellEditable(row.next(),5);
					}
				}
			},{
				index:7,
				action:function(row,data2){
					if(Number(data2.value) < 0){
						alert('<fmt:message key="number_cannot_be_negative"/>！');
						row.find("td:eq(7)").find('span').text(data2.ovalue);
						$.fn.autoGrid.setCellEditable(row,7);
					}else if(isNaN(data2.value)){
						alert('<fmt:message key="number_be_not_number"/>！');
						row.find("td:eq(7)").find('span').text(data2.ovalue);
						$.fn.autoGrid.setCellEditable(row,7);
					}else {
						row.find("td:eq(7) span").attr('title',data2.value);
						if(Number(row.data("unitper")) != 0){//如果转换率不为0的  才修改  否则会报错wjf
							row.find("td:eq(5) span").text(Number(Number(data2.value)/Number(row.data("unitper"))).toFixed(2)).attr('title',Number(Number(data2.value)/Number(row.data("unitper"))));//设置标准数量
						}
						$.fn.autoGrid.setCellEditable(row.next(),5);
					}
				}
			}]
		});
	}
	
	function validateByMincnt(index,row,data2){//最小申购量判断
		if(index=="5"){
			if(isNaN(data2.value)||Number(data2.value) < 0){
				alert('<fmt:message key="please_enter_positive_integer"/>！');
				row.find("input").focus().select();
			}else{
				row.find("td:eq(5)").find('span').text(Number(data2.value).toFixed(2));
			}
		}
	}
	function validator(index,row,data2){//输入框验证
		row.find("input").data("ovalue",data2.value);
		if(index=="5"){
			if(isNaN(data2.value)||Number(data2.value) < 0){
				data2.value=0;
				return;
			}
			row.find("td:eq(5)").find('span').attr('title',data2.value);
			row.find("td:eq(7) span").text((Number(row.find("td:eq(5)").text())*Number(row.data("unitper"))).toFixed(2)).attr('title',Number(row.find("td:eq(5)").text())*Number(row.data("unitper")));//设置参考数量
		}
		if(index=="7"){
			if(isNaN(data2.value)||Number(data2.value) < 0){
				data2.value=0;
				return;
			}
			row.find("td:eq(7)").find('span').attr('title',data2.value);
			if(Number(row.data("unitper")) != 0){//如果转换率不为0的  才修改  否则会报错wjf
				row.find("td:eq(5) span").text(Number(Number(data2.value)/Number(row.data("unitper"))).toFixed(2)).attr('title',Number(Number(data2.value)/Number(row.data("unitper"))));//设置标准数量
			}
		}
	}
	</script>
</body>
</html>