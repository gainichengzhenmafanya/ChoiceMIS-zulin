<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="storehouse_fill_in_audit"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
				.memoClass{border:0px;background:none;}
			</style>
	</head>
	<body>
	<div class="tool"></div>
	<input type="hidden" id="importFlag" name="importFlag" value="${importFlag}"/><!-- 导入判断 wjf -->
	<form action="<%=path%>/chkoutMis/saveByAdd.do" id="listForm" name="listForm" method="post">
		<div class="bj_head">
			<input type="hidden" id="curChkout" name="chkoutno" disabled="disabled" value="${chkoutm.chkoutno}"/>
			<%-- curStatus 0:init;1:edit;2:add;3:show --%>
			<input type="hidden" id="curStatus" value="<c:out value="${curStatus}" default="init"/>" />
			<div class="form-line" style="z-index:13">
				<div class="form-label"><fmt:message key="document_types"/>:</div>
				<div class="form-input" style="width:370px;">
					<select disabled="disabled" name="typ" id="typ" class="select" style="width:366px;">
						<c:forEach items="${billType }" var="codedes">
							<option value="${codedes.code }">${codedes.des }</option>
						</c:forEach>
					</select>
				</div>
				<div class="form-label" style="width:100px;"><fmt:message key="document_number"/>:</div>
				<div class="form-input">
					<c:if test="${chkoutm.vouno!=null}"><c:out value="${chkoutm.vouno}"></c:out></c:if>		
					<input type="hidden" name="vouno" id="vouno" value="${chkoutm.vouno }"/>
	<%-- 				<input disabled="disabled" class="text" id="vouno" value="${chkoutm.vouno }"/> --%>
				</div>
<!-- 				<div class="form-label" style="width:100px;">条形码：</div> -->
<!-- 				<div class="form-input" style="width:150px;"> -->
<!-- 					<input type="text" name="barCode" id="barCode" class="text"/>  -->
<!-- 				</div> -->
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="date_of_the_system_alone"/>:</div>
				<div class="form-input" style="width:370px;">
					<input style="width:364px;" disabled="disabled" class="Wdate text" id="maded" onfocus="WdatePicker({onpicked:function(){pickedFunc()}})" name="maded" value="<fmt:formatDate type="Date" pattern="yyyy-MM-dd" value="${chkoutm.maded }"/>"/>
				</div>
				<div class="form-label" style="width:100px;"><fmt:message key="orders_num"/>:</div>
				<div class="form-input">
					<c:if test="${chkoutm.chkoutno!=null}"><c:out value="${chkoutm.chkoutno}"></c:out></c:if>
					<input type="hidden" id="chkoutno" name="chkoutno" value="${chkoutm.chkoutno }"/>
	<%-- 				<input class="text" disabled="disabled" value="${chkoutm.chkoutno }"/> --%>
				</div>
			</div>
			<div class="form-line" style="z-index:12">
				<div class="form-label"><fmt:message key="positions"/>:</div>
				<div class="form-input" style="width:370px;">
					<input type="text" class="text" disabled="disabled" id="positn_select" onfocus="this.select()" style="width:40px;margin-top:4px;vertical-align:top;" value="${chkoutm.positn.code}"/>
						<select disabled="disabled" name="positn.code" id="positn" class="select" style="width:320px;">
<!-- 							<option value="">--</option> -->
							<c:forEach items="${positnOut}" var="positn">
								<option value="${positn.code}" title="${positn.des}"
								<c:if test="${positn.code == chkoutm.positn.code }">
									selected="selected"
								</c:if>
								id="${positn.code}" value="${positn.code}">${positn.code}-${positn.init}-${positn.des}
								</option>
							</c:forEach>
						</select>
<%-- 						<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' /> --%>
				</div>
				<div class="form-label" style="width:100px;"><fmt:message key="make_orders"/>:</div>
				<div class="form-input">
					<c:if test="${chkoutm.madeby!=null}"><c:out value="${chkoutm.madebyName}"></c:out></c:if>
					<input type="hidden" name="madeby" value="${chkoutm.madeby }"/>
	<%-- 				<input readonly="readonly" class="text" disabled="disabled" value="${chkoutm.madeby }"/> --%>
				</div>
			</div>
			<div class="form-line" style="z-index:11">
				<div class="form-label"><fmt:message key="requisitioned_positions"/>:</div>
				<div class="form-input" style="width:370px;">
					<input type="text" class="text" disabled="disabled" id="firm_select" onfocus="this.select()" style="width:40px;margin-top:4px;vertical-align:top" value="${chkoutm.firm.code}"/>
						<select disabled="disabled" name="firm.code" id="firm" class="select" style="width:320px;">
							<c:forEach items="${positnIn}" var="positn">
								<option value="${positn.code}" title="${positn.des}"
								<c:if test="${positn.code == chkoutm.firm.code }">
									selected="selected"
								</c:if>
								id="${positn.code}" value="${positn.code}">${positn.code}-${positn.init}-${positn.des}
								</option>
							</c:forEach>
						</select>
<%-- 						<img id="seachOnePositn1" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' /> --%>
				</div>
				<div class="form-label" style="width:100px;"><fmt:message key="accounting"/>:</div>
				<div class="form-input" id="checby">
					<c:if test="${chkoutm.checby!=null}"><c:out value="${chkoutm.checby}"></c:out></c:if>
					<input type="hidden" name="checby" value="${chkoutm.checby }"/>
	<%-- 				<input class="text" disabled="disabled" name="checby" value="${chkoutm.checby }"/> --%>
				</div>	
					
			</div>
			<div class="form-line">	
				<div class="form-label"><fmt:message key="summary"/>:</div>
				<div class="form-input" style="width:370px;">
					<input readonly="readonly" type="text" style="width:364px;" id="memo" class="text" name="memo" value=""/>
				</div>
				<div class="form-label" style="width:100px;"><fmt:message key="audit_remarks"/>:</div>
				<div class="form-input">
					<input type="hidden" name="chk1memo" id="chk1memo" class="text" value="${chkoutm.chk1memo}" />				
					<input type="text" name="chk1memo2" id="chk1memo2" class="text" disabled="disabled" value="${chkoutm.chk1memo}" />
				</div>	
				<div class="form-label" style="width:100px;"><fmt:message key="please_select_materials"/>:</div>
				<div class="form-input" style="width:80px;">
					<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="please_select_materials"/>' />
				</div>
			</div>
		</div>
		<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 26px;">&nbsp;</td>
								<td colspan="3"><fmt:message key="supplies"/></td>
								<td colspan="4"><fmt:message key="standard_unit"/></td>
								<td colspan="2"><fmt:message key="reference_unit"/></td>
								<td colspan="2" style="display:none;"><fmt:message key="scm_sales"/></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="inventory"/></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="remark"/></span></td>
							</tr>
							<tr>
								<td class="num"><span style="width: 16px;">&nbsp;</span></td>
								<td><span style="width:70px;"><fmt:message key="coding"/></span></td>
								<td><span style="width:140px;"><fmt:message key="name"/></span></td>
								<td><span style="width:70px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:30px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:70px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:40px;"><fmt:message key="unit_price"/></span></td>
								<td><span style="width:70px;"><fmt:message key="amount"/></span></td>
								<td><span style="width:30px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:70px;"><fmt:message key="quantity"/></span></td>
								<td style="display:none;"><span style="width:70px;"><fmt:message key="scm_sale_price"/></span></td>
								<td style="display:none;"><span style="width:70px;"><fmt:message key="amount"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body" style="height:100%;overflow: auto;">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:set var="sum_num" value="${0}"/>  <!-- 物资条数 -->
							<c:set var="sum_amount" value="${0}"/>  <!-- 总数量 -->
							<c:set var="sum_totalamt" value="${0}"/>  <!-- 总金额 -->
							<c:set var="sum_priceamt" value="${0}"/>  <!-- 总陈本金额 -->
							<c:forEach var="chkoutd" items="${chkoutm.chkoutd}" varStatus="status">
								<c:set var="sum_num" value="${status.index+1}"/>  
								<c:set var="sum_amount" value="${sum_amount + chkoutd.amount}"/>  
								<c:set var="sum_totalamt" value="${sum_totalamt + chkoutd.totalamt}"/>  
								<c:set var="sum_priceamt" value="${sum_priceamt + chkoutd.amount*chkoutd.pricesale}"/>  
								<tr data-unitper="${chkoutd.sp_code.unitper}" data-batchno="${chkoutd.batchno }">
									<td class="num"><span style="width:16px;">${status.index+1}</span></td>
									<td><span style="width:70px;" data-sp_name="${chkoutd.sp_code.sp_name }"><c:out value="${chkoutd.sp_code.sp_code }"/></span></td>
									<td><span style="width:140px;"><c:out value="${chkoutd.sp_code.sp_name }"/></span></td>
									<td><span style="width:70px;"><c:out value="${chkoutd.sp_code.sp_desc }"/></span></td>
									<td><span style="width:30px;"><c:out value="${chkoutd.sp_code.unit }"/></span></td>
									<td><span style="width:70px;text-align: right;" title="${chkoutd.amount }"><fmt:formatNumber value="${chkoutd.amount }" pattern="##.##"/></span></td>
									<td><span style="width:40px;text-align: right;" title="${chkoutd.price }"><input type="hidden" value="${chkoutd.price }"/><fmt:formatNumber value="${chkoutd.price }" pattern="##.##"/></span></td>
									<td><span style="width:70px;text-align: right;" title="${chkoutd.totalamt }"><fmt:formatNumber value="${chkoutd.totalamt }" pattern="##.##"/></span></td>
									<td><span style="width:30px;"><c:out value="${chkoutd.sp_code.unit1 }"/></span></td>
									<td><span style="width:70px;text-align: right;" title="${chkoutd.amount1 }"><fmt:formatNumber value="${chkoutd.amount1 }" pattern="##.##"/></span></td>
									<td style="display:none;"><span style="width:70px;text-align: right;" title="${chkoutd.pricesale}"><fmt:formatNumber value="${chkoutd.pricesale}" pattern="##.##"/></span></td>
									<td style="display:none;"><span style="width:70px;text-align: right;" title="${chkoutd.pricesale * chkoutd.amount }"><fmt:formatNumber value="${chkoutd.pricesale * chkoutd.amount }" pattern="##.##"/></span></td>
									<td><span style="width:70px;text-align: right;" title="${chkoutd.sp_code.cnt }"><fmt:formatNumber value="${chkoutd.sp_code.cnt }" pattern="##.##" minFractionDigits="2" /></span></td>
									<td><span style="width:70px;"><c:out value="${chkoutd.memo }"/></span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<div style="height: 10px">	
				<table cellspacing="0" cellpadding="0" style="margin-top:0;z-index:0;height: 10px">
					<thead>
						<tr>
							<td style="width: 26px;">&nbsp;</td>
							<td style="width:80px;"><fmt:message key="total"/>:</td>
							<td style="width:170px;"><fmt:message key="material_number"/>：<u>&nbsp;&nbsp;<label id="sum_num">${sum_num}</label>&nbsp;&nbsp;</u></td>
							<td style="width:180px;"><fmt:message key="total_number"/>：<u>&nbsp;&nbsp;<label id="sum_amount"><fmt:formatNumber value="${sum_amount}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber></label>&nbsp;&nbsp;</u></td>
							<td style="width:240px;"><fmt:message key="total_amount"/>:
								<u>&nbsp;&nbsp;<label id="sum_totalamt"><fmt:formatNumber value="${sum_totalamt}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber></label>元&nbsp;&nbsp;</u>
							</td>
							<td style="width:240px;display:none;"><fmt:message key="scm_total_sale"/>:
								<u>&nbsp;&nbsp;<label id="sum_priceamt"><fmt:formatNumber value="${sum_priceamt}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber></label>元&nbsp;&nbsp;</u>
							</td>
						</tr>
					</thead>
				</table>
		   </div>	
	</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">		
		//如果导入出库单报错了。。。wjf
		var importError = '<c:out value="${importError}"/>';
		if(importError != null && importError != '' && importError != 'undefined'){
			alert("<fmt:message key='import'/><fmt:message key='failure'/>！<fmt:message key='reason'/>：\n"+importError);
		}
		//ajax同步设置
// 		$.ajaxSetup({
// 			async: false
// 		});
	 	$('#seachSupply').bind('click.custom',function(e){
	 		if($("#curStatus").val() != "edit" && $("#curStatus").val() != "add"){//必须是新增或者编辑的情况下才能选择物资  2015.1.2wjf
	 			alert('<fmt:message key="must_be_dded_or_edited_in_order_to_select_materials"/>！');
	 			return;
	 		}
	 		if($('#positn_select').val()==''){
				showMessage({
					type: 'error',
					msg: '<fmt:message key="please_select_positions"/>！',
					speed: 1000
				});
				return;
			}
	 		if(!!!top.customWindow){
				top.customSupply('<fmt:message key="please_select_materials" />',encodeURI('<%=path%>/misbohcommon/selectSupplyLeft.do?single=false&positn='+$('#positn').val()),$('#sp_code'),null,null,$('#unit1'),$('.unit'),$('.unit1'),handler2);
			}
		});
	 	function handler2(sp_codes){
	 		var codes = '';//存放页面上的编码，用来判断是否存在
	 		$('.grid').find('.table-body').find('tr').each(function (){
	 			codes += $(this).find('td:eq(1)').text()+",";
			});
			var sp_code_arry = new Array();
	 		sp_code_arry = sp_codes.split(",");
	 		
			for(var i=0; i<sp_code_arry.length; i++){
				var sp_code = sp_code_arry[i];
				if(codes.indexOf(sp_code) != -1 ){//如果已存在，则继续下次循环
					continue;
				}
				//调拨的价格直接取移动平均 = 结存金额/结存数量    结存数量是0价格 就是0
				$.ajax({
					type: "POST",
					url: "<%=path%>/misbohcommon/findViewPositnSupply.do?sp_code="+sp_code+"&positn="+$('#positn_select').val(),
					dataType: "json",
					async:false,
					success:function(data1){
						var price = 0;
						var endNumber = 0;
						if(data1){
							price = data1.sp_price;
							endNumber = data1.inc0;
						}
						$.ajax({//查询物资
							type: "POST",
							url: "<%=path%>/supply/findById.do",
							data: "sp_code="+sp_code,
							dataType: "json",
							async:false,
							success:function(supply){
								if(!$(".table-body").find("tr:last").find("td:eq(1)").find('span').text()==''){
									$.fn.autoGrid.addRow();
								}
								var row = $(".table-body").find("tr:last");
								row.find("td:eq(1) span").text(supply.sp_code).data('sp_name',supply.sp_name);
								row.find("td:eq(2) span").text(supply.sp_name);
								row.find("td:eq(3) span").text(supply.sp_desc==null?"":supply.sp_desc);
								row.find("td:eq(4) span").text(supply.unit);
								row.find("td:eq(5) span").text(1).attr('title',1).css("text-align","right");
							    row.find("td:eq(6) span").text(price.toFixed(2)).attr('title',price).css("text-align","right");//前面取售价  显示  2015.4.23wjf 
								row.find("td:eq(7) span").text(price.toFixed(2)).attr('title',price).css("text-align","right");
								row.find("td:eq(8) span").text(supply.unit1);
								row.find("td:eq(9) span").text((supply.unitper).toFixed(2)).attr('title',supply.unitper).css("text-align","right");
								row.find("td:eq(10) span").text(price.toFixed(2)).attr('title',price).css("text-align","right");
								row.find("td:eq(11) span").text(price.toFixed(2)).attr('title',price).css("text-align","right");
								row.find("td:eq(12) span").text(Number(endNumber).toFixed(2)).attr('title',endNumber).css("text-align","right");
								row.find("td:eq(13) span").text("");
								row.data("unitper",supply.unitper);
								row.data("price",price);
								//计算总金额
								getTotalSum();
							}
						});
					}
				});
			}
 		}
			$(document).ready(function(){
				$("#positn_select").val($("#positn").val());
				$("#firm_select").val($("#firm").val());
				/*过滤*/
				$('#positn_select').bind('keyup',function(){
			          $("#positn").find('option')
			                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
			                    .attr('selected','selected');
			       });
				$('#firm_select').bind('keyup',function(){
			          $("#firm").find('option')
			                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
			                    .attr('selected','selected');
			       });
				//按钮快捷键
				focus() ;//页面获得焦点
			 	$(document).bind('keyup',function(e){
			 		if($(e.srcElement).is("input")){//对表格内的输入框进行判读，延迟600毫秒
			 			var index=$(e.srcElement).closest('td').index();
			    		if(index=="5"||index=="6"||index=="7"||index=="9"){
			    			$(e.srcElement).unbind('blur').blur(function(e){
				 				validateByMincnt($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
				 			});
				    		validator($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
			    		}
			    	}
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit"/>').click();
			 		}
			 		if(e.altKey ==false)return;
			 		switch (e.keyCode)
		            {
		                case 70: $('#autoId-button-101').click(); break;
		                case 65: $('#autoId-button-102').click(); break;
		                case 68: $('#autoId-button-103').click(); break;
		                case 69: $('#autoId-button-104').click(); break;
						case 80: $('#autoId-button-105').click(); break;
		                case 83: $('#autoId-button-106').click(); break;
		                case 67: $('#autoId-button-107').click(); break;
		            }
				});  
			 	$('input').filter(':disabled').addClass('textDisable');
			 	//回车换焦点start       
		 	    //定义需要做切换的input输入框，最后可以放一个提交按钮，这样最好一个input点击回车后可以直接触发按钮的点击       
		 	    array = ['typ','maded','positn_select','positn','firm_select','firm','memo','chk1memo2'];  
		 	   $('select,input[type="text"]').not($("#barCode")).keydown(function(e) {                
			 		//使用jquery的这种获取事件方法，不必再指定event.srcElement或者event.target  
			 		var event = $.event.fix(e);                
			 		//判断如果按键事件的按键代码是回车，则从数组中获取下一个元素的ID,并设置焦点                       
			 		if (event.keyCode == 13) {                
			 			var index = $.inArray($.trim($(event.target).attr("id")), array);//alert(index)
			 				if(index==3 && $('#'+array[index+1]).attr('disabled')=='disabled'){
			 					++index;
			 				}
			 				$('#'+array[++index]).focus();
			 				if(index==7){
			 					$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),2);
			 				} 
			 		}
		 		});    
		 		$("#typ").focus(function(){
		 			this.size = this.length;
		 			$('#typ').css('height','20px');
		 			$('#typ').parent('.form-input').css('z-index','99');
		 		})
	 			.blur(function(){
		 			this.size = 1;
		 			$('#typ').css('height','20px');
	 			})
	 			.dblclick(function() {
 					$('#typ').blur();
 				});
		 		$("#positn").focus(function(){
		 			this.size = this.length;
		 			$('#positn').css('height','100px');
		 			$('#positn').parent('.form-input').css('z-index','3');
		 			})
		 		.blur(function(){
		 			this.size = 1;
		 			$('#positn').css('height','20px');
		 			$('#positn_select').val($(this).val());
	 			})
	 			.dblclick(function() {
 					$('#positn_select').val($(this).val());
 					$('#positn').blur();
 				});
		 		$("#firm").focus(function(){
		 			this.size = this.length;
		 			$('#firm').css('height','20px');
		 			$('#firm').parent('.form-input').css('z-index','2');
		 			})
		 		.blur(function(){
		 			this.size = 1;
		 			$('#firm').css('height','20px');
		 			$('#firm_select').val($(this).val());
	 			})
	 			.dblclick(function() {
 					$('#firm_select').val($(this).val());
 					$('#firm').blur();
 				});
	 			//回车换焦点end
				var status = $("#curStatus").val();
				if(status == 'add'&& !$("#firm").val()){
					alert("总部未设置1999盘亏仓位,不能新增,请联系总部！");
					return;
				}
				if(status == 'add')setEditable();
				
				<%-- curStatus 0:init;1:edit;2:add;3:show --%>
				//判断按钮的显示与隐藏
				if(status == 'add'){
					loadToolBar([true,true,false,false,false,true,false]);
					$('#chk1memo2').addClass('memoClass');
				}else if(status == 'show'){//查询页面双击返回
					loadToolBar([true,true,true,true,true,false,true]);
					$('#chk1memo2').attr('disabled',false);
				}else{//init
					loadToolBar([true,true,false,false,false,false,false]);
					$('#chk1memo2').addClass('memoClass');
				}
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),180);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'typ',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="document_types"/><fmt:message key="cannot_be_empty"/>！']
					},{
						type:'text',
						validateObj:'maded',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="date_of_the_system_alone"/><fmt:message key="cannot_be_empty"/>！']
					},{
						type:'text',
						validateObj:'firm',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="library_positions"/><fmt:message key="cannot_be_empty"/>！']
					},{
						type:'text',
						validateObj:'positn',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="positions"/><fmt:message key="cannot_be_empty"/>！']
					},{
						type:'text',
						validateObj:'chkoutno',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="document_number"/><fmt:message key="cannot_be_empty"/>！']
					}]
				});
				
				$("select[name='typ'] option[value='<c:out value="${chkoutm.typ}"/>']").attr("selected","selected");
				<c:if test="${tableFrom=='table'}">
					$('#autoId-button-101').click();
				</c:if>
			});
			
			
			//打印
			function printChkout(){
				window.open ("<%=path%>/chkoutMis/printChkout.do?chkoutno="+$("#curChkout").val(),'newwindow','height='+window.screen.height+',width='+window.screen.width+',top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');
			}
			function setEditable(){
				//新增的时候才+1，修改的时候不用wjf
				if($('#curStatus').val()=='add'){
					//如果导入成功，则不加1 wjf
					var importFlag = $('#importFlag').val();
					if(importFlag != 'OK'){
						$('#sum_num').text(Number($('#sum_num').text())+1);//总行数
					}
				}
				$('select').removeAttr("disabled");
				$('#typ').removeAttr("disabled");
				$('#positn').removeAttr("disabled");
				$('#firm').removeAttr("disabled");
				$('input').removeAttr("readonly");
				$('#maded').removeAttr("disabled");
				$('#firm_select').removeAttr("disabled");
				$('#positn_select').removeAttr("disabled").focus();
				$('input').removeClass('textDisable');
				$('#editable').val('true');
				var endNumber = 0;
				$(".table-body").autoGrid({
					initRow:1,
					colPerRow:14,
					VerifyEdit:{verify:true,enable:function(cell,row){
						return row.find('td').index(cell) == 2 || (row.find("td:eq(2)").text() == null || $.trim(row.find("td:eq(2)").text()) != '' ) ? true : false;
					}},
					widths:[26,80,150,80,40,80,50,80,40,80,80,80,80,80,0],
					colStyle:['','',{background:"#F1F1F1"},'','',{background:"#F1F1F1"},'',{background:"#F1F1F1"},'',{background:"#F1F1F1"},{display:"none"},{display:"none"},'','',''],
					editable:[2,5,7,9,13],
					onLastClick:function(row){
						getTotalSum();
					},
					onEnter:function(data){
						if(data.curobj.closest('tr').find('td').index(data.curobj.closest('td')) == 2){
							if($.trim(data.curobj.closest('td').prev().text())){
								data.curobj.find('span').html(data.curobj.closest('td').prev().find('span').data('sp_name'));
								return;
							}else if(!data.actionobj){
								$.fn.autoGrid.setCellEditable(data.curobj.closest('tr'),2);
								return;
							}
						}
						$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.ovalue) ;
					},
					cellAction:[{
						index:2,
						action:function(row){
							$.fn.autoGrid.setCellEditable(row,5);
						},
						onCellEdit:function(event,data,row){
							data['url'] = '<%=path%>/misbohcommon/findSupplyTop10.do';
							//if(!isNaN(data.value)){
								data['key'] = 'sp_code';
							//}else{
							//	data['key'] = 'sp_init';
							//}	
							data['sp_position']=$('#positn_select').val();
							$.fn.autoGrid.ajaxEdit(data,row);
						},
						resultFormat:function(data){
							var desc = "";
							if(data.sp_desc!=null){
								desc = "-"+data.sp_desc;
							}
							return data.sp_code+'-'+data.sp_init+'-'+data.sp_name + desc;
						},
						afterEnter:function(data,row){
							var num=0;
							$('.grid').find('.table-body').find('tr').each(function (){
								if($(this).find("td:eq(1)").text()==data.sp_code){
									num=1;
								}
							});
							if(num==1){
								showMessage({
	 								type: 'error',
	 								msg: '<fmt:message key="added_supplies_remind"/>！',
	 								speed: 1000
	 							});
								$.fn.autoGrid.setCellEditable(row,2);
								return;
							}
							if($('#positn_select').val()==''){
								showMessage({
									type: 'error',
									msg: '<fmt:message key="please_select_positions"/>！',
									speed: 1000
								});
								return;
							}
							//调拨的价格直接取移动平均 = 结存金额/结存数量    结存数量是0价格 就是0    
							$.ajax({
								type: "POST",
								url: "<%=path%>/misbohcommon/findViewPositnSupply.do?sp_code="+data.sp_code+"&positn="+$('#positn_select').val(),
								dataType: "json",
								success:function(data1){
									var price = 0;
									var endNumber = 0;
									if(data1){
										price = data1.sp_price;
										endNumber = data1.inc0;
									}
									row.find("td:eq(1) span").text(data.sp_code).data('sp_name',data.sp_name);
									row.find("td:eq(2) span input").val(data.sp_name).focus();
									row.find("td:eq(3) span").text(data.sp_desc==null?"":data.sp_desc);
									row.find("td:eq(4) span").text(data.unit);
									row.find("td:eq(5) span").text(0).attr('title',0).css("text-align","right");
								    row.find("td:eq(6) span").text(price.toFixed(2)).attr('title',price).css("text-align","right");
									row.find("td:eq(7) span").text(0).attr('title',0).css("text-align","right");
									row.find("td:eq(8) span").text(data.unit1);
									row.find("td:eq(9) span").text(0).attr('title',0).css("text-align","right");
									row.find("td:eq(10) span").text(price.toFixed(2)).attr('title',price).css("text-align","right");
									row.find("td:eq(11) span").text(0).attr('title',0).css("text-align","right");
									row.find("td:eq(12) span").text(Number(endNumber).toFixed(2)).attr('title',endNumber).css("text-align","right");
									row.data("unitper",data.unitper);
								}
							});
						}
					},{
						index:5,
						action:function(row,data2){
							if(Number(data2.value) == 0){
								alert('<fmt:message key="number_cannot_be_zero"/>！');
								row.find("td:eq(5)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,5);
							}else if(Number(data2.value) < 0){
								alert('<fmt:message key="number_cannot_be_negative"/>！');
								row.find("td:eq(5)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,5);
							}else if(isNaN(data2.value)){
								alert('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(5)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,5);
							}else if(data2.value.length > 6){
								alert('<fmt:message key="the_maximum_length_not"/>6<fmt:message key="digit"/>！');
								row.find("td:eq(5)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,5);
							}else{
								//标准数量变 要同步5,7,9,12
								row.find("td:eq(5)").find('span').attr('title',data2.value);
								setValue(row,5);//设置本行其他值
								var realPrice = Number(row.find("td:eq(6) span").attr('title')?row.find("td:eq(6) span").attr('title'):row.find("td:eq(6)").text());
								if(Number(realPrice) > 0){
									if(row.data("unitper") == 0){
										$.fn.autoGrid.setCellEditable(row,9);
									}else{
										$.fn.autoGrid.setCellEditable(row,13);
									}
								}else{
									$.fn.autoGrid.setCellEditable(row,6);
								}
								getTotalSum();
							}
						}
					},{
						index:6,
						action:function(row,data2){
							if(Number(data2.value) < 0){
								alert('<fmt:message key="number_cannot_be_negative"/>！');
								row.find("td:eq(6)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,6);
							}else if(isNaN(data2.value)){
								alert('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(6)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,6);
							}else{
								//单价变 要同步6,7,11,12
								row.find("td:eq(6)").find('span').attr('title',data2.value);
								setValue(row,6);//设置本行其他值
								$.fn.autoGrid.setCellEditable(row,7);
								getTotalSum();
							}
						}
					},{
						index:7,
						action:function(row,data2){
							if(Number(data2.value) < 0){
								alert('<fmt:message key="number_cannot_be_negative"/>！');
								row.find("td:eq(7)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,7);
							}else if(isNaN(data2.value)){
								alert('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(7)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,7);
							}else{
								//金额变要变 7,6,11,12
								row.find("td:eq(7)").find('span').attr('title',data2.value);
								var amount = row.find("td:eq(5) span").attr('title')?row.find("td:eq(5) span").attr('title'):row.find("td:eq(5)").text();
								var realPrice = Number(data2.value)/Number(amount);
								row.find("td:eq(6)").find('span').text(realPrice.toFixed(2)).attr('title',realPrice);
								setValue(row,7);//设置本行其他值
								if(row.data("unitper") == 0){
									$.fn.autoGrid.setCellEditable(row,9);
								}else{
									$.fn.autoGrid.setCellEditable(row,13);
								}
								getTotalSum();
							}
						}
					},{
						index:9,
						action:function(row,data2){
							if(Number(data2.value) == 0){
								alert('<fmt:message key="number_cannot_be_zero"/>！');
								row.find("td:eq(9)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,9);
							}else if(Number(data2.value) < 0){
								alert('<fmt:message key="number_cannot_be_negative"/>！');
								row.find("td:eq(9)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,9);
							}else if(isNaN(data2.value)){
								alert('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(9)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,9);
							}else if(data2.value.length > 6){
								alert('<fmt:message key="the_maximum_length_not"/>6<fmt:message key="digit"/>！');
								row.find("td:eq(9)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,9);
							}else{
								//参考数量变5,7,9,12要变
								row.find("td:eq(9)").find('span').attr('title',data2.value);
								if(Number(row.data("unitper")) != 0){//如果转换率不为0的  才修改  否则会报错wjf
									row.find("td:eq(5) span").text((Number(data2.value)/Number(row.data("unitper"))).toFixed(2)).attr('title',Number(data2.value)/Number(row.data("unitper")));//参考数量改变时改变标准数量
									setValue(row,9);//设置本行其他值
									getTotalSum();
								}
								$.fn.autoGrid.setCellEditable(row,13);
							}
						}
					},{
						index:13,
						action:function(row,data){
							if(!row.next().html())$.fn.autoGrid.addRow();
							$.fn.autoGrid.setCellEditable(row.next(),2);
							$('#sum_num').text(Number($('#sum_num').text())+1);//总行数
						}
					}]
				});
			}
			
			//设置一行的值
			function setValue(row,index){
				var amount = Number(row.find("td:eq(5) span").attr('title')?row.find("td:eq(5) span").attr('title'):row.find("td:eq(5)").text());
				var realPrice = Number(row.find("td:eq(6) span").attr('title')?row.find("td:eq(6) span").attr('title'):row.find("td:eq(6)").text());
				var unitper = Number(row.data("unitper"));
				if(index != 7){
					row.find("td:eq(7) span").text((amount*realPrice).toFixed(2)).attr('title',amount*realPrice);
				}
				if(index != 9){
					row.find("td:eq(9) span").text((amount*unitper).toFixed(2)).attr('title',amount*unitper);
				}
				var pricesale = Number(row.find("td:eq(10) span").attr('title')?row.find("td:eq(10) span").attr('title'):row.find("td:eq(10)").text());
				row.find("td:eq(11) span").text((pricesale*amount).toFixed(2)).attr('title',pricesale*amount);
			}
			//冲消用
			function setEditable2(){
				$(".table-body").autoGrid({
					initRow:1,
					colPerRow:14,
					widths:[26,80,150,80,40,80,50,80,40,80,80,80,80,80,0],
					colStyle:['','',{background:"#F1F1F1"},'','',{background:"#F1F1F1"},'',{background:"#F1F1F1"},'',{background:"#F1F1F1"},{display:"none"},{display:"none"},'','',''],
					editable:[]
				});
			}
			function loadToolBar(use){
				$('.tool').html('');
				$('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />(<u>F</u>)',
							title: '<fmt:message key="select"/><fmt:message key="storehouse_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								searchChkout();
							}
						},'-',{
							text: '<fmt:message key="insert" />(<u>A</u>)',
							title: '<fmt:message key="insert"/><fmt:message key="storehouse_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')}&&use[1],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								var status = $('#curStatus').val();
								if(status == 'add' || status == 'edit')
									if(!confirm('<fmt:message key="data_unsaved_whether_to_continue"/>)'))return;
								window.location.replace("<%=path%>/chkoutYgcMis/addChkout.do");
							}
						},{
							text: '<fmt:message key="template" />',
							title: '<fmt:message key="template" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')}&&use[5],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								getChkoutDemo();
							}
						},{
							text: '<fmt:message key="edit" />(<u>E</u>)',
							title: '<fmt:message key="update"/><fmt:message key="storehouse_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[3],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								var status = $('#curStatus').val();
								if(status == 'init')return;
								if($('#typ').val() == '9913' || $('#typ').val() == '9914'){
									alert('<fmt:message key="Out_back_or_Out_offset_cannot_edit"/>！');//出库退货或者出库冲消类型不能进行编辑
									return;
								}
								if(status == 'add' || status == 'edit')
									if(!confirm('<fmt:message key="data_unsaved_whether_to_continue"/>)'))return;
								$("#curStatus").val("edit");
								setEditable();
								$('#positn').attr("disabled",false);
								$('#firm').attr("disabled",false);
								$('#positn_select').attr("disabled",false);
								$('#firm_select').attr("disabled",false);
								$('#typ').attr("disabled",false);
								$('#maded').attr("disabled",false);
								loadToolBar([true,true,true,false,true,true,false]);
							}
						},{
							text: '<fmt:message key="save" />(<u>S</u>)',
							title: '<fmt:message key="save"/>',
							useable: use[5],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								if($("#curStatus").val()=='add' || $("#curStatus").val()=='edit'){
									if(validate._submitValidate()){
										//先判断出库仓位有没有期初
										$.ajax({
											url:"<%=path%>/misbohcommon/checkQC.do?code="+$('#positn').val(),
											type:"post",
											success:function(data){
												if(data){
													//再判断领用仓位有没有期初
													var firm = $('#firm_select').val();
													if(firm != '1999'){
														$.ajax({
															url:"<%=path%>/misbohcommon/checkQC.do?code="+firm,
															type:"post",
															success:function(data){
																if(data){
																	saveChkout();
																}else{
																	alert(firm+'<fmt:message key="The_use_positn_isnot_Initialized"/>！<fmt:message key="can_not"/><fmt:message key="save"/>！');
																	return;
																}
															}
														});
													}else{
														saveChkout();
													}
												}else{
													alert($('#positn').val()+'<fmt:message key="no_Initialized"/>！<fmt:message key="can_not"/><fmt:message key="save"/>！');
													return;
												}
											}
										});
									}
								}else{
									alert('<fmt:message key="no_edited_documents_to_be_saved"/>！');
								}
							}
						},{
							text: '<fmt:message key="check" />(<u>C</u>)',
							title: '<fmt:message key="check"/><fmt:message key="storehouse_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')}&&use[6],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-58px','-240px']
							},
							handler: function(){
								auditChkout();
							}
						},{
							text: '<fmt:message key="delete" />(<u>D</u>)',
							title: '<fmt:message key="delete"/><fmt:message key="storehouse_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')}&&use[2],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								deleteChkout();
							}
						},'-',{
// 							text: '<fmt:message key="import" />',
// 							title: '<fmt:message key="import" />',
// 							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'import')},
// 							icon: {
<%-- 								url: '<%=path%>/image/Button/excel.bmp', --%>
// 								position: ['2px','2px']
// 							},
// 							handler: function(){
// 								importChkout();
// 							}
// 						},{
							text: '<fmt:message key="print" />(<u>P</u>)',
							title: '<fmt:message key="print"/><fmt:message key="storehouse_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[4],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								printChkout();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								var status = $("#curStatus").val();
								if(status == 'add' || status == 'edit')
									if(!confirm('<fmt:message key="data_unsaved_whether_to_exit"/>？'))return;
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}
					]
				});
				
			}
			function saveChkout(){
				var numNull=0;//默认0代表成功，1代表0值，2代表非数字
				var isNull=0;//空单据
				var sp_name = '';
				$('.table-body').find('tr').each(function(i){
					if($(this).find('td:eq(1) span').text()!=''){
						isNull=1;
						sp_name = $(this).find('td:eq(2) span input').val() ? $(this).find('td:eq(2) span input').val() : $(this).find('td:eq(2)').text();
						var amount=$(this).find('td:eq(5) span').attr('title');
						var amount1=$(this).find('td:eq(9) span').attr('title');//数量1
						var price=$(this).find('td:eq(6) span').attr('title');
						var amt=$(this).find('td:eq(7) span').attr('title');
						if(amount==0 || amount==0.0 ||amount==0.00 || amount1 == 0 || amount1==0.0 ||amount1==0.00){//参考数量也不能为0  2015.1.5wjf
							numNull=1;
							return false;
						}
						if(amount=="" || amount==null || isNaN(amount) || amount1=="" || amount1==null || isNaN(amount1)){
							numNull=2;//2代表数量为空或者非数字
							return false;
						}
						if(price=="" || price==null || isNaN(price)){
							numNull=3;//3代表单价为空或者非数字
							return false;
						}
						if(amt=="" || amt==null || isNaN(amt)){
							numNull=4;//4代表金额为空或者非数字
							return false;
						}
					}
				});
				if(Number(isNull)==0){
					alert('<fmt:message key="empty_document_unallowed_please_select_supplies"/>！');
					return;
				}
				if(Number(numNull)==1){//数量不为0
					alert('<fmt:message key="supplies"/>:['+sp_name+']<fmt:message key="number_cannot_be_zero"/>！');
					return;
				}
				if(Number(numNull)==2){//数量为空或字母
					alert('<fmt:message key="supplies"/>:['+sp_name+']<fmt:message key="quantity"/><fmt:message key="not_a_valid_number"/>！');
					return;
				}
				if(Number(numNull)==3){//单价为空或字母
					alert('<fmt:message key="supplies"/>:['+sp_name+']<fmt:message key="price"/><fmt:message key="not_a_valid_number"/>！');
					return;
				}
				if(Number(numNull)==4){//金额为空或字母
					alert('<fmt:message key="supplies"/>:['+sp_name+']<fmt:message key="amount"/><fmt:message key="not_a_valid_number"/>！');
					return;
				}
				//检测要保存的单据是否已被审核
				var dataS = {},showM = "yes";
				dataS['vouno'] = $("#vouno").val();
				dataS['chkoutno'] = $("#chkoutno").val();
				dataS['active'] = 'edit';
				if($("#curStatus").val()=='edit')
					$.ajax({url:'<%=path%>/chkoutMis/chkChect.do',type:'POST',
						data:dataS,async:false,success:function(data){
						if ("YES"!=data) {
							showM='no';
							alert(data);
						}
					}});
				if('no' == showM){
					return;
				}
				var keys = ["sp_code.sp_code","sp_code.sp_name","sp_code.sp_desc","sp_code.unit", "amount","price","totalamt","sp_code.unit1", "amount1","pricesale","sp_code.cnt","memo","batchno"];
				var data = {};
				data['scrapped'] = $('#scrapped').val();
				data['scrappedname'] = $('#scrappedname').val();
				var i = 0;
				if($("#firm").val() == $("#positn").val()){
					alert('<fmt:message key="library_positions_cannot_be_requisitioned_positions"/>！');
					return;
				}
				$("#listForm *[name]").each(function(){
					var name = $(this).attr("name"); 
					if(name)data[name] = $(this).val();
				});
				var rows = $(".grid .table-body table tr").not(function(){
					if($(this).find('td:eq(1)').find('span').html() == ''){
						return $(this);
					}
				});
				if((!rows.length > 0) || !$.trim($(rows[0]).find("td:eq(2)").text())){
					alert('<fmt:message key="please_add_data"/>！');
					return;
				}
				$('#wait').show();
				$('#wait2').show();
				for(i=0;i<rows.length;i++){
					cols = $(rows[i]).find("td");
					var j = 0;
					for(j=1;j <= keys.length;j++){
						var value = $.trim($(rows[i]).find("td:eq("+j+") span").attr('title'))?$.trim($(rows[i]).find("td:eq("+j+") span").attr('title')):$.trim($(rows[i]).find("td:eq("+j+")").text());
						value = value ? value : $.trim($(rows[i]).find("td:eq("+j+") input").val());
						if(value)
							data["chkoutd["+i+"]."+keys[j-1]] = value.replace(/&nbsp;/g,'');
						if(j == 12){//备注 wjf修改
							value = $.trim($(rows[i]).find("td:eq(13)").text()) ? $.trim($(rows[i]).find("td:eq(13)").text()) : $.trim($(rows[i]).find("td:eq(13) input").val());
							data["chkoutd["+i+"]."+keys[j-1]] = value;
						}
						if(j==13){
							data["chkoutd["+i+"]."+keys[j-1]] = $.trim($(rows[i]).data("batchno"));
						}
					}
				}
				$.ajax({
					url:"<%=path%>/chkoutMis/saveOrUpdate.do?curStatus="+$("#curStatus").val(),
					data:data,
					type:"POST",
					success:function(data){
						$('#wait').hide();
						$('#wait2').hide();
						var state='<fmt:message key="save_successful"/>!';
						if($("#curStatus").val() != 'add'){
							state='<fmt:message key="update_successful"/>!';
						}
						var rs = eval('('+data+')');
						switch(Number(rs.pr)){
						case 0:
							alert('<fmt:message key="update_fail"/>！');
							break;
						case 1:
							alert(state);
							window.location.replace("<%=path%>/chkoutYgcMis/updateChkout.do?chkoutno="+$("#chkoutno").val());
							break;
						case -1:
							alert('<fmt:message key="material_error_update_failed"/>！');
							/* showMessage({
								type: 'error',
								msg: '<fmt:message key="material_error_update_failed"/>！',
								speed: 1000
								}); */
							break;
						case -2:
							alert('<fmt:message key="storehouse_be_audited_cannot_be_updated"/>！');
							/* showMessage({
								type: 'error',
								msg: '<fmt:message key="storehouse_be_audited_cannot_be_updated"/>！',
								speed: 1000
								}); */
							break;
						case -3:
							alert('此会计日不在本会计年中，不能保存！');
							/* showMessage({
								type: 'error',
								msg: '<fmt:message key="storehouse_be_audited_cannot_be_updated"/>！',
								speed: 1000
								}); */
							break;
								
						}
					},
					error:function(){
						alert('<fmt:message key="update_fail"/>！');
						/* showMessage({
							type: 'error',
							msg: '<fmt:message key="update_fail"/>！',
							speed: 1000
							}); */
					}
				});
			}
			
			//审核
			function auditChkout(){
				if($("#curStatus").val() != 'show'){
					alert('<fmt:message key="no_document_or_documents_being_edited_cannot_be_audited"/>！');
					return;
				}
				
				//检测要保存的单据是否已被审核
				var dataS = {},showM = "yes";
				dataS['vouno'] = $("#vouno").val();
				dataS['chkoutno'] = $("#chkoutno").val();
				dataS['active'] = 'edit';
				$('#wait').show();
				$('#wait2').show();
				if($("#curStatus").val()=='edit')
					$.ajax({url:'<%=path%>/chkoutMis/chkChect.do',type:'POST',
						data:dataS,async:false,success:function(data){
						$('#wait').hide();
						$('#wait2').hide();
						if ("YES"!=data) {
							showM='no';
							alert(data);
						}
					}});
				if('no' == showM){
					return;
				}
				
				//判断是否盘点2015.5.27wjf
				var msg = 0;
				var data = {};
				data['maded'] = $('#maded').val();
				data['positn'] = $('#positn').val();
				$('#wait').show();
				$('#wait2').show();
				$.ajax({url:'<%=path%>/misbohcommon/chkYnInout.do',type:'POST',
						data:data,async:false,success:function(data){
					$('#wait').hide();
					$('#wait2').hide();
					msg = data;
				}});
				if(1 == msg){//提示已做盘点，是否继续
					if(!confirm('<fmt:message key="positions"/>:['+$("#positn").find("option:selected").attr('title')+']<fmt:message key="at"/>'+$('#maded').val()+'<fmt:message key="have_inventoried_Whether_to_continue_for_the_in-out_warehouse_operation"/>?')){return;};
				}else if(2 == msg){
					alert('<fmt:message key="positions"/>:['+$("#positn").find("option:selected").attr('title')+']<fmt:message key="at"/>'+$('#maded').val()+'<fmt:message key="have_inventoried_Whether_to_cannot_for_the_in-out_warehouse_operation"/>!');
					return;
				}
				
				var msg = {};
				msg['chk1memo'] = $('#chk1memo2').val();
				if($("#curChkout").val()){
					$('#wait').show();
					$('#wait2').show();
					$.ajax({
						url:"<%=path%>/chkoutMis/auditChkout.do?chkoutno="+$("#curChkout").val(),
						data:msg,
						type:"POST",
						success:function(data){
							$('#wait').hide();
							$('#wait2').hide();
							try{
								var rs = eval("("+data+")");
	// 							var rs = $.parseJSON(data);
								if(Number(rs.pr) == 1){
									showMessage({
										type: 'success',
										msg: '<fmt:message key="check_successful"/>！',
										speed: 3000
										});
									$("#checby").text(rs.checby);
									loadToolBar([true,true,false,false,true,false,false]);//点保存后把按钮状态改为 init
								}else if(Number(rs.pr)==9){//出库数量大于库存数量审核失败
									showMessage({
										type: 'error',
										msg: rs.rss,
										speed: 3000
										});
									loadToolBar([true,true,false,true,true,false,true]);//点保存后把按钮状态改为 init  审核失败提供编辑功能
								}
								else if(!rs.msg.match(/^\{\d|\D*\}$/)){
									showMessage({
										type: 'error',
										msg: rs.msg,
										speed: 1000
										});
									loadToolBar([true,true,false,true,true,false,true]);//点保存后把按钮状态改为 init  审核失败提供编辑功能
								}
							}catch(e){
								alert(data);
							}
							$("#curStatus").val('show');    //修改状态
						},
						error:function(){
							alert('<fmt:message key="check_fail"/>！');
							/* showMessage({
								type: 'error',
								msg: '<fmt:message key="check_fail"/>！',
								speed: 1000
								}); */
						}
					});
				}
			}
			
			function openChkout(chkoutno){
				window.location.replace("<%=path%>/chkoutYgcMis/updateChkout.do?chkoutno="+chkoutno);
			}
			function deleteChkout(){
				if($("#curChkout").val() && confirm("<fmt:message key='suredel'/>？")){
					//检测要保存的单据是否已被审核
					var dataS = {},showM = "yes";
					dataS['vouno'] = $("#vouno").val();
					dataS['chkoutno'] = $("#chkoutno").val();
					dataS['active'] = 'delete';
					$.ajax({url:'<%=path%>/chkoutMis/chkChect.do',type:'POST',
						data:dataS,async:false,success:function(data){
						if ("YES"!=data) {
							showM='no';
						}
					}});
					if('no' == showM){
						return;
					}
					$('#wait').show();
					$('#wait2').show();
					 $.ajax({
						url:"<%=path%>/chkoutMis/deleteChkout.do?vouno="+$("#curChkout").val(),
						type:"POST",
						success:function(data){
							$('#wait').hide();
							$('#wait2').hide();
							var rs = eval('('+data+')');
							if(rs.pr == 0)
								alert('<fmt:message key="audited_data_cannot_be_deleted"/>！');
							/* showMessage({
								type: 'error',
								msg: '<fmt:message key="audited_data_cannot_be_deleted"/>！',
								speed: 1000
								}); */
							if(rs.pr == 1){
								showMessage({
									type: 'success',
									msg: '<fmt:message key="successful_deleted"/>！',
									speed: 3000
								});
								setTimeout(initPage,3000);
							}
						}
					});
				}
			}
			function initPage(){
				window.location.replace("<%=path%>/chkoutYgcMis/addChkout.do?action=init");
			}
			function searchChkout(){
				var curwindow = $('body').window({
					id: 'window_searchChkout',
					title: '<fmt:message key="query_storehouse"/>',
					content: '<iframe id="listChkoutFrame" frameborder="0" src="<%=path%>/chkoutYgcMis/list.do?action=init"></iframe>',
					width: 780,
					height: 480,
					confirmClose: true,
					draggable: true,
					isModal: true
				});
				curwindow.max();
			}
			function pickedFunc(){
				var cur=$('#maded').val();
				var uri = "<%=path%>/chkoutMis/getVouno.do?maded=" + cur;
				$.get(uri,function(data){
					$("input[name='vouno']").val(data);
				});
			}
			function validateByMincnt(index,row,data2){//最小申购量判断
				if(Number(data2.value) < 0){
					alert('<fmt:message key="number_cannot_be_negative"/>！');
					row.find("td:eq("+index+")").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,index);
					return;
				}else if(isNaN(data2.value)){
					alert('<fmt:message key="number_be_not_number"/>！');
					row.find("td:eq("+index+")").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,index);
					return;
				}
			}
			function validator(index,row,data2){//输入框验证
				row.find("input").data("ovalue",data2.value);
				if(Number(data2.value) < 0){
					alert('<fmt:message key="number_cannot_be_negative"/>！');
					row.find("td:eq("+index+")").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,index);
					return;
				}else if(isNaN(data2.value)){
					alert('<fmt:message key="number_be_not_number"/>！');
					row.find("td:eq("+index+")").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,index);
					return;
				}
				if(index=="5"){
					row.find("td:eq(5)").find('span').attr('title',data2.value);
					setValue(row,5);//设置本行其他值
				}else if(index=="6"){
					row.find("td:eq(6)").find('span').attr('title',data2.value);
					setValue(row,6);//设置本行其他值
				}else if(index=="7"){
					row.find("td:eq(7)").find('span').attr('title',data2.value);
					var amount = row.find("td:eq(5) span").attr('title')?row.find("td:eq(5) span").attr('title'):row.find("td:eq(5)").text();
					var realPrice = Number(data2.value)/Number(amount);
					row.find("td:eq(6)").find('span').text(realPrice.toFixed(2)).attr('title',realPrice);
					setValue(row,7);//设置本行其他值
				}else if(index=="9"){
					row.find("td:eq(9)").find('span').attr('title',data2.value);
					if(Number(row.data("unitper")) != 0){//如果转换率不为0的  才修改  否则会报错wjf
						row.find("td:eq(5) span").text((Number(data2.value)/Number(row.data("unitper"))).toFixed(2)).attr('title',Number(data2.value)/Number(row.data("unitper")));//参考数量改变时改变标准数量
						setValue(row,9);//设置本行其他值
					}
				}
				getTotalSum();
			}
			function getTotalSum(){//计算统计数据
				var sum_amount = 0; 
				var sum_totalamt = 0;
				var sum_priceamt = 0;
				$('.table-body').find('tr').each(function (){
					if($(this).find('td:eq(1)').text()!=''){//非空行
						var amount = $(this).find('td:eq(5) span').attr('title');
						sum_amount += Number(amount);
						var price  = $(this).find('td:eq(7) span').attr('title');
						sum_totalamt += Number(price);
						var pricesale = $(this).find('td:eq(11) span').attr('title');
						sum_priceamt += pricesale;
					}
				});
				$('#sum_num').text($(".table-body").find('tr').length);//总行数
				$('#sum_amount').text(Number(sum_amount).toFixed(2));//总数量
				$('#sum_totalamt').text(Number(sum_totalamt).toFixed(2));//总金额
				$('#sum_priceamt').text(Number(sum_priceamt).toFixed(2));//总成本金额			
			}
			
			//导入excel出库单
			function importChkout(){
		    	$('body').window({
					id: 'window_importSupply',
					title: '<fmt:message key="import_out_order" />',
					content: '<iframe id="importChkout" frameborder="0" src="<%=path%>/chkoutMis/importChkout.do"></iframe>',
					width: '400px',
					height: '200px',
					draggable: true,
					isModal: true
				});
		    }
			
			// 查找添加盘点模板
			function getChkoutDemo(){
		    	var action = "<%=path%>/chkoutYgcMis/addChkoutDemo.do?firm="+$('#positn').val();
				$('body').window({
					id: 'window_addChkstoDemo',
					title: '<fmt:message key="template"/>',
					content: '<iframe id="addChkstoDemoFrame" name="addChkstoDemoFrame" frameborder="0" src='+action+'></iframe>',
					width: '80%',//'1000px'
					height: '420px',
					draggable: true,
					isModal: true,
					confirmClose: false,
					topBar: {
						items: [{
								text: '<fmt:message key="enter"/>',
								title: '<fmt:message key="enter"/>',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['0px','-40px']
								},
								handler: function(){
									if(getFrame('addChkstoDemoFrame')){
										window.frames["addChkstoDemoFrame"].enterUpdate();
									}
								}
							},{
								text: '<fmt:message key="cancel"/>',
								title: '<fmt:message key="cancel"/>',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}]
					}
				});
			}
		</script>
	</body>
</html>
