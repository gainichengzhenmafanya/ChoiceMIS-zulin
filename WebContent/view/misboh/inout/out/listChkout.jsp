<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>positn Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<style type="text/css">
			.tool {
				position: relative;
				height: 27px;
			}
			.page{
				margin-bottom: 25px;
			}
			.condition {
				position: relative;
				top: 1px;
				height: 31px;
				line-height: 31px;
			}
			.grid td span{
				padding:0px;
			}
			.search{
				margin-top:3px;
				cursor: pointer;
			}
			.form-input input[type=text],.form-input select{
				width: 150px;
			}
			form .form-line .form-label{
				width: 60px;
				margin-left: 25px;
			}
			.table-head td span{
				white-space: normal;
			}
		</style>					
	</head>
	<body>
		<div class="tool"></div>
		<form action="<%=path%>/chkoutMis/list.do" id="queryForm" name="queryForm" method="post">
					<div class="form-line">
					<div class="form-label"><fmt:message key="startdate"/></div>
					<div class="form-input"><input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/></div>
<%-- 					<div class="form-label"><fmt:message key="library_positions"/></div> --%>
<!-- 					<div class="form-input"> -->
<%-- 						<input type="text"  id="positn"  name="positn.des" readonly="readonly" value="${chkoutm.positn.des}"/> --%>
<%-- 						<input type="hidden" id="positn_select" name="positn.code" value="${chkoutm.positn.code}"/> --%>
<%-- 						<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' /> --%>
<%-- 					<input type="hidden" name="checby" value="${chkoutm.checby}"/>  --%>
<!-- 					</div> -->
					<div class="form-label"><fmt:message key="orders_num"/></div>
					<div class="form-input"><input type="text" id="chkoutno" name="chkoutno" class="text" value="<c:out value="${chkoutm.chkoutno}" />"/></div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="enddate"/></div>
					<div class="form-input"><input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd"/>" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/></div>
					<div class="form-label"><fmt:message key="document_number"/></div>
					<div class="form-input"><input type="text" id="vouno" name="vouno" class="text" value="<c:out value="${chkoutm.vouno}" />"/></div>
					<div class="form-label"><fmt:message key="requisitioned_positions"/></div>
					<div class="form-input">
						<input type="text"  id="firm"  name="firm.des" readonly="readonly" value="${chkoutm.firm.des}"/>
						<input type="hidden" id="firm_select" name="firm.code" value="${chkoutm.firm.code}"/>
						<img id="seachOnePositn1" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
					</div>
				</div>

			<div class="grid" style="overflow: auto;">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width: 25px"></span></td>
								<td ><span style="width:30px;"><input type="checkbox" id="chkAll"/></span></td>
								<td ><span style="width:50px;"><fmt:message key="storehouse_no"/></span></td>
								<td ><span style="width:100px;"><fmt:message key="document_number"/></span></td>
								<td ><span style="width:80px;"><fmt:message key="document_types"/></span></td>
								<td ><span style="width:80px;"><fmt:message key="date"/></span></td>
								<td ><span style="width:150px;"><fmt:message key="time"/></span></td>
								<td ><span style="width:80px;"><fmt:message key="library_positions"/></span></td>
								<td ><span style="width:120px;"><fmt:message key="requisitioned_positions"/></span></td>
								<td ><span style="width:80px;"><fmt:message key="total_amount1"/></span></td>
								<td ><span style="width:80px;"><fmt:message key="orders_maker"/></span></td>
<%-- 								<td ><span style="width:80px;"><fmt:message key="orders_audit"/></span></td> --%>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="chkout" items="${chkoutList}" varStatus="status">
								<tr data-positn="${chkout.positn.code}">
									<td  class="num"><span style="width: 25px;">${status.index+1}</span></td>
									<td >
										<span style="width:30px; text-align: center;"><input type="checkbox" name="idList" id="chk_<c:out value='${chkout.chkoutno}' />" value="<c:out value='${chkout.chkoutno}' />"/></span>
									</td>
									<td><span style="width:50px;"><c:out value="${chkout.chkoutno}" />&nbsp;</span></td>
									<td><span style="width:100px;"><c:out value="${chkout.vouno}" />&nbsp;</span></td>
									<td><span style="width:80px;"><c:out value="${chkout.typ}" />&nbsp;</span></td>
									<td><span style="width:80px;" title="<fmt:formatDate value="${chkout.maded}" pattern="yyyy-MM-dd"/>"><fmt:formatDate value="${chkout.maded}" pattern="yyyy-MM-dd"/>&nbsp;</span></td>
									<td><span style="width:150px;"><c:out value="${chkout.madet}"/>&nbsp;</span></td>
									<td><span style="width:80px;"><c:out value="${chkout.positn.des}" />&nbsp;</span></td>
									<td><span style="width:120px;"><c:out value="${chkout.firm.des}" />&nbsp;</span></td>
									<td><span style="width:80px;text-align: right;"><fmt:formatNumber type="number" value="${chkout.totalamt}" pattern="#0.00"/>&nbsp;</span></td>
									<td><span style="width:80px;"><c:out value="${chkout.madebyName}" />&nbsp;</span></td>
<%-- 									<td><span style="width:80px;"><c:out value="${chkout.checby}" />&nbsp;</span></td> --%>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="queryForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			function pageReload(){
				$('#queryForm').submit();
			}
			function clearQueryForm(){
				$('#queryForm input[type="text"]').val('');
				$('#queryForm select option').removeAttr("selected");;
				$('#queryForm select option[value=""]').attr("selected","selected");;
			}
			function execQuery(){
				$('#queryForm').submit();
			}
			$(document).ready(function(){
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'chkoutno',
						validateType:['canNull','intege'],
						param:['T','T'],
						error:['','<fmt:message key="single_number_is_digital_or_empty"/>']
					}]
				});
				$('#positn_select').bind('keyup',function(){
			          $("#positn").find('option')
			                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
			                    .attr('selected','selected');
			       });
				$('#firm_select').bind('keyup',function(){
			          $("#firm").find('option')
			                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
			                    .attr('selected','selected');
			       });
				
				$("#positn").focus(function(){
		 			this.size = this.length;
		 			$('#positn').css('height','100px');
		 			$('#positn').parent('.form-input').css('z-index','3');
		 			})
		 		.blur(function(){
		 			this.size = 1;
		 			$('#positn').css('height','20px');
		 			$('#positn_select').val($(this).val());
	 			})
	 			.dblclick(function() {
 					$('#positn_select').val($(this).val());
 					$('#positn').blur();
 				});
		 		$("#firm").focus(function(){
		 			this.size = this.length;
		 			$('#firm').css('height','100px');
		 			$('#firm').parent('.form-input').css('z-index','2');
		 			})
		 		.blur(function(){
		 			this.size = 1;
		 			$('#firm').css('height','20px');
		 			$('#firm_select').val($(this).val());
	 			})
	 			.dblclick(function() {
 					$('#firm_select').val($(this).val());
 					$('#firm').blur();
 				});
				
				focus() ;//页面获得焦点
			 	$(document).bind('keyup',function(e){
			 		if(e.keyCode==27){
			 			parent.$('.close').click();
			 		}
			 		if(e.altKey ==false)return;
			 		switch (e.keyCode)
		            {
		                case 70: parent.$('.<fmt:message key="query_storehouse"/>').click(); break;
		                case 67: parent.$('.<fmt:message key="audit_storehouse"/>').click(); break;
		                case 68: parent.$('.<fmt:message key="delete_storehouse"/>').click(); break;
		            }
				});  
				
				$('#search').click(function(){
					$("#queryForm").submit();
				});
				$('#resetSearch').click(function(){
					clearQueryForm();
				});
				$('.grid .table-body tr').live('dblclick',function(){
					var status = $(window.parent.document).find("#curStatus").val();
					if(status == 'add' || status == 'edit'){
						if(!confirm('<fmt:message key="data_unsaved_whether_to_exit"/>')){
							return;
						}
					}
					var chkoutno = $(this).find("input:checkbox").val();
					chkoutno ? window.parent.openChkout(chkoutno) && window.close() : alert("error!");
					
				});
				
// 				var audit = $('<select name="checby" class="select" style="width: 70px;float: left;" ></select>');
// 				audit.append('<option value="" ><fmt:message key="unchecked"/></option>');
// 				audit.append('<option value="a" ><fmt:message key="checked"/></option>');
// 				audit.change(function(){location.replace('<%=path%>/chkoutMis/list.do?checby='+audit.val());}); 
// 				if('<c:out value="${chkoutList[0].checby}"/>')audit.find('option:last').attr('selected','selected');
// 				$('.tool').prepend(audit);
// 				$('.toolbar').css('position','static');
				
				setElementHeight('.grid',['.tool'],$(document.body),100);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();
				$('.grid').find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
					);
					
					//如果全选按钮选中的话，table背景变色
					$("#chkAll").click(function() {
		                if (!!$("#chkAll").attr("checked")) {
		                	$('.grid').find('.table-body').find('tr').addClass("bgBlue");
		                }else{
		                	$('.grid').find('.table-body').find('tr').removeClass("bgBlue");
	                	}
		            });
					//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
					$('.grid').find('.table-body').find('tr').live("click", function () {
					     if ($(this).hasClass("bgBlue")) {
					         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
					     }else{
					         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
					     }
					 });
					$("#bdat").focus(function(){
						new WdatePicker();
					});
					$("#edat").focus(function(){
						new WdatePicker();
					});
					$('#seachOnePositn1').bind('click.custom',function(e){
						if(!!!top.customWindow){
							var defaultCode = $('#firm_select').val();
							var defaultName = $('#firm').val();
							var offset = getOffset('bdat');
							top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/misbohcommon/findPositnSuper.do?sta=all&typn=3-6&mold=one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#firm'),$('#firm_select'),'760','520','isNull');
						}
					});
			});
			var tool = $('.tool').toolbar({
				items: [{
					text: '<fmt:message key="select" />(<u>F</u>)',
					title: '<fmt:message key="query_storehouse"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						var bdat = $('#queryForm').find("#bdat").val().toString();
						var edat= $('#queryForm').find("#edat").val().toString();
						if(validate._submitValidate()){
							if(bdat<=edat)
								execQuery();
							else
								//alert("<fmt:message key="starttime"/><fmt:message key="not_less_than"/><fmt:message key="endtime"/>");
								alert("<fmt:message key='starttime'/><fmt:message key='can_not'/><fmt:message key='not_less_than'/><fmt:message key='endtime'/>");
						}
					}
				},{
					text: '<fmt:message key="enter" />',
					title: '<fmt:message key="enter"/>',
					useable: true,
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-20px']
					},
					handler: function(){
						onEnter();
					}
				},{
					text: '<fmt:message key="delete" />(<u>D</u>)',
					title: '<fmt:message key="delete_storehouse"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-38px','0px']
					},
					handler: function(){
						deleteChkout();
					}
				},{
					text: '<fmt:message key="check" />(<u>C</u>)',
					title: '<fmt:message key="check"/><fmt:message key="storehouse_information"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-58px','-240px']
					},
					handler: function(){
						auditChkout();
					}
				},{
					text: '<fmt:message key="quit"/>',
					title: '<fmt:message key="quit"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						parent.$('.close').click();
					}
				}]
			});								
			
			function getSelected(){
				var data = {};
				var chks = $('.grid .table-body :checkbox').filter(':checked');
				switch(chks.length){
				case 0:
					alert('<fmt:message key="please_select_data_need_to_be_operated"/>！');
					break;
				case 1:
					data['chkoutno'] = $('.grid .table-body :checkbox').filter(':checked').val();
					data['checby'] = $.trim($('.grid .table-body :checkbox').filter(':checked').parent().parent().find('td:last').text());
					return data;
					default:
						alert('<fmt:message key="please_select_data"/>！');
				}
			}
			
			function deleteChkout(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if($("select[name='checby']").val()){
					alert('<fmt:message key="audited_data_cannot_be_deleted"/>！');
					return;
				}
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					if(confirm('<fmt:message key="delete_data_confirm"/>？')){
						var chkValue = [];
						
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
						});
						
						var action = '<%=path%>/chkoutMis/deleteChkout.do?vouno='+chkValue.join(",");
						
						$.get(action,function(data){
							var rs = eval('('+data+')');
							if(rs.pr == 0)alert('<fmt:message key="delete_fail"/>！');
							if(rs.pr == 1){//重新刷新 wjf
								$('body').window({
									title: '<fmt:message key="delete_storehouse"/>',
									content: '<iframe frameborder="0" src=<%=path%>/chkoutMis/success.do></iframe>',
									width: 500,
									height: '245px',
									draggable: true,
									isModal: true
								});
							};
						});
					}
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_delete"/>！');
					return ;
				}
			}
			function auditChkout(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if($("select[name='checby']").val()){
					alert('<fmt:message key="unaudited_data_cannot_be_deleted"/>！');
					return;
				}
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					if(confirm('<fmt:message key="whether_audit_data"/>？')){
						var chkValue = [];
						
						checkboxList.filter(':checked').each(function(){
							var maded = $(this).parents('tr').find('td:eq(5) span').attr('title');
							var positn = $(this).parents('tr').attr('data-positn');
							var positnDes = $(this).parents('tr').find('td:eq(7) span').text();
							var msg = 0;
							var data = {};
							data['maded'] = maded;
							data['positn'] = positn;
							$.ajax({url:'<%=path%>/misbohcommon/chkYnInout.do',type:'POST',
									data:data,async:false,success:function(data){
								msg = data;
							}});
							if(1 == msg){//提示已做盘点，是否继续
								if(!confirm('<fmt:message key="In_order"/>:['+$(this).val()+'],<fmt:message key="the_current_store"/>'+maded+'<fmt:message key="have_inventoried_Whether_to_continue_check_this_order"/>?')){
									return true;
								}
							}else if(2 == msg){
								alert('<fmt:message key="In_order"/>:['+$(this).val()+'],<fmt:message key="the_current_store"/>'+maded+'<fmt:message key="have_inventoried_already_filter_this_order"/>！');
								return true;
							}
							chkValue.push($(this).val());
						});
						
						if(chkValue.length == 0){
							alert('<fmt:message key="No_orders_to_be_checked"/>！');
							return;
						}
						var action = '<%=path%>/chkoutMis/auditChkout.do?chkoutnos='+chkValue.join(",");
						$.post(action,function(data){
							try{
								var rs = eval("("+data+")");
								if(rs.pr == 1)
									$('body').window({
										title: '<fmt:message key="audit_storehouse"/>',
										content: '<iframe frameborder="0" src=<%=path%>/chkoutMis/success.do></iframe>',
										width: 500,
										height: '245px',
										draggable: true,
										isModal: true
									});
								else
									if(!rs.msg.match(/^\{\d|\D*\}$/))
										showMessage({
											type: 'error',
											msg: rs.msg.replace(rs.msg.match(/^\d|\D*:/),''),
											speed: 1000
											});
							}catch(e){
								alert(data);
							}
						});
					}
				}else{
					alert('<fmt:message key="please_select_reviewed_information"/>！');
					return ;
				}
			}
			
			function onEnter(){
				var selected = $('.grid').find('.table-body').find(':checkbox').filter(':checked'); 
				if(selected.size() != 1){
					alert('<fmt:message key="please_select_data"/>');
					return;
				}
				selected.trigger("dblclick");
			}
		</script>
	</body>
</html>