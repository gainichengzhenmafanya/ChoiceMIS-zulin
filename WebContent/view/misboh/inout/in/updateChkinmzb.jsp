<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Chkinm Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<style type="text/css">
				.memoClass{border:0px;background:none;}
				.searchFendian{cursor: pointer;}
			</style>
	</head>
	<body>

		<div class="tool">
		</div>
		<input type="hidden" id="isChkinPriceNot0" value="${isChkinPriceNot0}"/><!-- 是否入库价格可以为0 -->
		<form id="listForm" action="<%=path%>/chkinmZbMis/saveChkinm.do" method="post">
		<!-- 加入隐藏内容 -->
		<div class="bj_head">
		<%-- curStatus 0:init;1:edit;2:add;3:show --%>
		<input type="hidden" id="curStatus" value="<c:out value="${curStatus}" default="init"/>" />
		<input type="hidden" id="zb" name="zb" value="zb" />
		<div class="form-line" style="z-index:13">
			<div class="form-label"><fmt:message key="document_types"/>:</div>
			<div class="form-input" style="width:330px;">
				<select id="typ" name="typ" value="${chkinm.typ}" class="select" style="width:329px;">
					<c:forEach items="${billType }" var="codedes">
						<option value="${codedes.code }">${codedes.des }</option>
					</c:forEach>
				</select>
			</div>
			<div class="form-label" style="width:100px;"><fmt:message key="document_number"/>:</div>
			<div class="form-input">
				<c:if test="${chkinm.vouno!=null}"><c:out value="${chkinm.vouno}"></c:out></c:if>
				<input type="hidden" name="vouno" id="vouno" value="${chkinm.vouno }"/>
<%-- 				<input type="text" disabled="disabled" class="text" name="vouno" id="vouno" value="${chkinm.vouno }"/> --%>
			</div>
		</div>
		<div class="form-line" style="z-index:12">
			<div class="form-label"><fmt:message key="date_of_the_system_alone"/>:</div>
			<div class="form-input" style="width:330px;">
			<input type="text" id="maded" name="maded" style="width:327px;" class="Wdate text" value="<fmt:formatDate value="${chkinm.maded}" pattern="yyyy-MM-dd"/>" onfocus="WdatePicker({onpicked:function(){pickedFunc()}})" />
			</div>
			<div class="form-label" style="width:100px;"><fmt:message key="orders_num"/>:</div>
			<div class="form-input">
				<c:if test="${chkinm.chkinno!=null}"><c:out value="${chkinm.chkinno}"></c:out></c:if>
				<input type="hidden" name="chkinno" id="chkinno" value="${chkinm.chkinno }"/>				
<%-- 				<input type="text" disabled="disabled" class="text" name="chkinno" id="chkinno" value="${chkinm.chkinno }"/> --%>
			</div>												
		</div>
		<div class="form-line" style="z-index:11">	
			<div class="form-label"><fmt:message key="storage_positions"/>:</div>
			<div class="form-input" style="width:330px;">
				<input type="text" class="text"  id="positn_select" onfocus="this.select()" style="width:40px;margin-top:4px;vertical-align:top" value="${chkinm.positn.code}"/>
				<select class="select" id="positn" name="positn" style="width:284px;" onchange="findPositnByDes(this);">
					<c:forEach var="positn" items="${positnList}" varStatus="status">
						<option 
							<c:if test="${positn.code==chkinm.positn.code}"> 
									   	selected="selected"
							</c:if> 
							id="${positn.code}" value="${positn.code}">${positn.code}-${positn.init}-${positn.des}
						</option>
					</c:forEach>
				</select>
<%-- 				<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' /> --%>
			</div>
			<div class="form-label" style="width:100px;"><fmt:message key="orders_maker"/>:</div>
			<div class="form-input">
				<c:if test="${chkinm.madeby!=null}"><c:out value="${chkinm.madebyName}"></c:out></c:if>
				<input type="hidden" name="madeby" id="madeby" class="text" value="${chkinm.madeby}" />			
<%-- 				<input type="text" class="text" value="${chkinm.madeby}" disabled="disabled"/> --%>
			</div>								
		</div>
		<div class="form-line" style="z-index:10">	
			<div class="form-label"><fmt:message key="suppliers"/>:</div>
			<div class="form-input" style="width:330px;">
				<input type="text" class="text" id="deliver_select" onfocus="this.select()" style="width:40px;margin-top:4px;vertical-align:top" value="${chkinm.deliver.code}"/>
				<select class="select" id="deliver" name="deliver" style="width:284px;" onchange="findDeliverByDes(this);">
					<c:forEach var="deliver" items="${deliverList}" varStatus="status">
						<option 
							<c:if test="${deliver.code==chkinm.deliver.code}"> 
									   	selected="selected"
							</c:if> 
							id="${deliver.code}" value="${deliver.code}">${deliver.code}-${deliver.init}-${deliver.des}
						</option>
					</c:forEach>
				</select>
<%-- 				<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' /> --%>
			</div>
			<div class="form-label" style="width:100px;"><fmt:message key="accounting"/>:</div>
				<input type="hidden" name="checby" id="checby" class="text" value="${chkinm.checby}" />			
				<div class="form-input" id="showChecby"><c:if test="${chkinm.checby!=null}"><c:out value="${chkinm.checby}"></c:out></c:if>
<%-- 				<input type="text" class="text" value="${chkinm.checby}" disabled="disabled"/> --%>
			</div>	
		</div>
		<div class="form-line" style="z-index:9">
			<div class="form-label"><fmt:message key="summary"/>:</div>
			<div class="form-input" style="width:330px;">
				<input type="text" name="memo" id="memo" class="text" value="${chkinm.memo}"  style="width:327px"/>
			</div>
			<div class="form-label" style="width:100px;"><fmt:message key="audit_remarks"/>:</div>
			<div class="form-input">
				<c:if test="${chkinm.chk1memo!=null}"><c:out value="${chkinm.chk1memo }"></c:out></c:if>
				<input type="hidden" name="chk1memo" id="chk1memo" class="text" value="${chkinm.chk1memo}" />				
				<input type="text" name="chk1memo2" id="chk1memo2" class="text" disabled="disabled" value="${chkinm.chk1memo}" />
			</div>	
			<div class="form-label" style="width:100px;"><fmt:message key="please_select_materials"/>:</div>
			<div class="form-input" style="width:20px;">
				<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="please_select_materials"/>' />
			</div>		
		</div>
		</div>
			<div class="grid">		
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2"><span class="num" style="width: 26px;padding:0px;">&nbsp;</span></td>
 								<td colspan="3"><span><fmt:message key="supplies"/></span></td>
								<td colspan="4"><span><fmt:message key="standard_unit"/></span></td>
								<td colspan="2"><span><fmt:message key="reference_unit"/></span></td>
								<td colspan="2"><span><fmt:message key="scm_dept"/></span></td>
								<td colspan="3"><fmt:message key="scm_taxes_pre"/></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="production_date"/></span></td>
								<td rowspan="2"><span style="width:90px;"><fmt:message key="pc_no"/></span></td>
								<td rowspan="2"><span style="width:100px;"><fmt:message key="remark"/></span></td>
							</tr>
							<tr>
								<td><span style="width:70px;"><fmt:message key="coding"/></span></td>
								<td><span style="width:100px;"><fmt:message key="name"/></span></td>
 								<td><span style="width:70px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:50px;"><fmt:message key="unit"/></span></td>
 								<td><span style="width:50px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:50px;"><fmt:message key="unit_price"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="amount"/></span></td>
								<td><span style="width:50px;"><fmt:message key="unit"/></span></td>
 								<td><span style="width:50px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:60px;"><fmt:message key="coding"/></span></td>
								<td><span style="width:100px;"><fmt:message key="name"/></span></td>
								<td><span style="width:55px;"><fmt:message key="tax_rate"/></span></td>
								<td><span style="width:50px;"><fmt:message key="unit_price"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="amount"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<c:set var="sum_num" value="${0}"/>  <!-- 物资条数 -->
				<c:set var="sum_amount" value="${0}"/>  <!-- 总数量 -->
				<c:set var="sum_totalamt" value="${0}"/>  <!-- 总金额 -->
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" border="0">
						<tbody>
							<c:forEach var="chkind"  items="${chkinm.chkindList}" varStatus="status" >
								<tr data-unitper="${chkind.supply.unitper}" data-tax="${chkind.supply.tax}"
									data-sp_per1="${chkind.supply.sp_per1}" data-ynbatch="${chkind.supply.ynbatch }"
									data-sp_id="${chkind.sp_id }">
									<td><span class="num" style="width: 16px;">${status.index+1}</span></td>
									<td><span style="width:70px;" data-sp_name="${chkind.supply.sp_name}">${chkind.supply.sp_code}</span></td>
									<td><span style="width:100px;">${chkind.supply.sp_name}</span></td>
	 								<td><span style="width:70px;">${chkind.supply.sp_desc}</span></td>
									<td><span style="width:50px;">${chkind.supply.unit}</span></td>
	 								<td><span style="width:50px;text-align:right;" title="${chkind.amount }"><fmt:formatNumber value="${chkind.amount}" type="currency" pattern="0.00"/></span></td>
									<td><span style="width:50px;text-align:right;" title="${chkind.price*(1+chkind.supply.tax)}" a="${chkind.price*(1+chkind.supply.tax)}">
										<fmt:formatNumber value="${chkind.price*(1+chkind.supply.tax)}" type="currency" pattern="0.00"/></span></td>
	 								<td><span style="width:60px;text-align:right;" title="${chkind.totalamt*(1+chkind.supply.tax)}">
	 									<fmt:formatNumber value="${chkind.totalamt*(1+chkind.supply.tax)}" type="currency" pattern="0.00"/></span></td>
									<td><span style="width:50px;">${chkind.supply.unit1}</span></td>
	 								<td><span style="width:50px;text-align:right;" title="${chkind.amount1}"><fmt:formatNumber value="${chkind.amount1}" type="currency" pattern="0.00"/></span></td>
									<td><span style="width:60px;">${chkind.indept}</span></td>
									<td><span style="width:100px;">${chkind.des}</span></td>
									<td><span style="width:55px;">${chkind.supply.taxdes}</span></td>
	 								<td><span style="width:50px;text-align:right;" title="${chkind.price}"><fmt:formatNumber value="${chkind.price}" type="currency" pattern="0.00"/></span></td>
	 								<td><span style="width:60px;text-align:right;" title="${chkind.totalamt}"><fmt:formatNumber value="${chkind.price*chkind.amount}" type="currency" pattern="0.00"/></span></td>
	 								<td><span style="width:80px;"><fmt:formatDate value="${chkind.dued}" pattern="yyyy-MM-dd"/></span></td>
	 								<td><span style="width:90px;">${chkind.pcno}</span></td>
	 								<td><span style="width:100px;">${chkind.memo}</span></td>
<%-- 	 								<td><input type="hidden" name="unitper" value="${chkind.supply.unitper}"></input></td> --%>
<%-- 	 								<td><input type="hidden" name="tax" value="${chkind.supply.tax}"></input></td> --%>
<%-- 	 								<td><input type="hidden" name="sp_id" value="${chkind.sp_id }"></input></td> --%>
								</tr>
								<c:set var="sum_num" value="${status.index+1}"/>
								<c:set var="sum_amount" value="${sum_amount + chkind.amount}"/>  
								<c:set var="sum_totalamt" value="${sum_totalamt + chkind.totalamt*(1+chkind.supply.tax)}"/>  
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<div style="height: 10px">	
				<table cellspacing="0" cellpadding="0" style="margin-top:0;z-index:0;height: 10px">
					<thead>
						<tr>
							<td style="width: 25px;">&nbsp;</td>
							<td style="width:80px;"><fmt:message key="total"/>:</td>
							<td style="width:170px;"><fmt:message key="material_number"/>：<u>&nbsp;&nbsp;<label id="sum_num">${sum_num}</label>&nbsp;&nbsp;</u></td>
							<td style="width:180px;"><fmt:message key="total_number"/>：<u>&nbsp;&nbsp;<label id="sum_amount"><fmt:formatNumber value="${sum_amount}" pattern="##.##" minFractionDigits="2" /></label>&nbsp;&nbsp;</u></td>
							<td style="width:240px;"><fmt:message key="total_amount"/>:
								<u>&nbsp;&nbsp;<label id="sum_totalamt"><fmt:formatNumber value="${sum_totalamt}" pattern="##.##" minFractionDigits="2" /></label>元&nbsp;&nbsp;</u>
							</td>
						</tr>
					</thead>
				</table>
		   </div>	
		</form>
		<form id="reportForm" method="post">
		<input type="hidden" id="chkinno" name="chkinno"/>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/Chkin.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		//ajax同步设置
// 		$.ajaxSetup({
// 			async: false
// 		});
		var inselected=[];//存储已经录过的物资
		var trList = $('.grid').find('.table-body').find('tr');
		trList.each(function(){
			inselected.push($(this).find('td:eq(1)').text());
		});
			var validate;
			$(document).ready(function(){
				$("#positn_select").val($("#positn").val());
				$("#deliver_select").val($("#deliver").val());
				/*过滤*/
				$('#deliver_select').bind('keyup',function(){
			          $("#deliver").find('option')
			                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
			                    .attr('selected','selected');
			       });
				$('#positn_select').bind('keyup',function(){
			          $("#positn").find('option')
			                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
			                    .attr('selected','selected');
			       });
				//按钮快捷键
				focus() ;//页面获得焦点
				$(document).bind('keydown',function(e){
					var input = $(e.srcElement)?$(e.srcElement):$(e.target);
			 		if(input.is("input")){//对表格内的输入框进行判读，延迟600毫秒
			 			var index=input.closest('td').index();
		 				if(index == "6" || index == "7"){
		 					var ynprice = input.closest('tr').data('ynprice');//判断有没有做报价
							if(!ynprice){
								ynprice = getYnprie(input.closest('tr'));
							}
							if(Number(input.val()) > 0 && ynprice == 'Y'){
			 					$.fn.autoGrid.setCellEditable(input.closest('tr'),17);
			 					return;
			 				}
			 			}
			 		}
				});
			 	$(document).bind('keyup',function(e){
			 		if($(e.srcElement).is("input")){//对表格内的输入框进行判读，延迟600毫秒
			 			var index=$(e.srcElement).closest('td').index();
			    		if(index=="5"||index=="6"||index=="7"||index=="9"){
			    			$(e.srcElement).unbind('blur').blur(function(e){
				 				validateByMincnt($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
				 			});
				    		validator($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
			    		}
			 		}
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit"/>').click();
			 		}
			 		if(e.altKey ==false)return;
			 		switch (e.keyCode)
		            {
		                case 70: $('#autoId-button-101').click(); break;
		                case 65: $('#autoId-button-102').click(); break;
		                case 68: $('#autoId-button-103').click(); break;
		                case 69: $('#autoId-button-104').click(); break;
						case 80: $('#autoId-button-105').click(); break;
		                case 83: $('#autoId-button-106').click(); break;
		                case 67: $('#autoId-button-107').click(); break;
		            }
				}); 
			 	
			 	$('#seachDeliver').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#deliver_select').val();
						var defaultName = $('#deliver').val();
						var offset = getOffset('maded');
						top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/deliver/selectOneDeliver.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deliver'),$('#deliver_select'),'900','500','isNull',handler1);
						$("#deliver").find('option').filter(":contains('"+($("#deliver").val())+"')").attr('selected','selected');
					}
				});
			 	function handler1(a){
			 		 $("#deliver").find("option[value='"+a+"']").attr('selected','selected');
			 	}
			 	$('#seachSupply').bind('click.custom',function(e){
			 		if($("#curStatus").val() != "edit" && $("#curStatus").val() != "add"){//必须是新增或者编辑的情况下才能选择物资  2015.1.2wjf
			 			alert('<fmt:message key="must_add_or_edit_can_select"/>！');
			 			return;
			 		}
			 		if(!!!top.customWindow){
						top.customSupply('<fmt:message key="please_select_materials" />',encodeURI('<%=path%>/misbohcommon/selectSupplyLeft.do?single=false&positn='+$('#positn').val()),$('#sp_code'),null,null,$('#unit1'),$('.unit'),$('.unit1'),handler2);
					}
				});
			 	function handler2(sp_codes){
			 		var codes = '';//存放页面上的编码，用来判断是否存在
			 		$('.grid').find('.table-body').find('tr').each(function (){
			 			codes += $(this).find('td:eq(1)').text()+",";
					});
			 		var price;
			 		var sp_code;
					var sp_code_arry = new Array();
			 		sp_code_arry = sp_codes.split(",");
			 		//wangjie  支持物资多选   遍历所选择的物资编码 sp_code 2014年11月21日 14:04:11
					for(var i=0; i<sp_code_arry.length; i++){
						sp_code = sp_code_arry[i];
						if(codes.indexOf(sp_code) != -1 ){//如果已存在，则继续下次循环
							continue;
						}
						var url ="<%=path%>/misbohcommon/findBprice.do";
						var data1 = "sp_code="+sp_code+"&area="+$('#positn_select').val()+"&madet="+$('#maded').val()+"&deliver="+$('#deliver').val();
						$.ajax({
							type: "POST",
							url: url,
							data: data1,
							dataType: "json",
							async: false,
							success:function(spprice){
								price = spprice.price;//保留两位小数吧，防止有些小数显示不了问题  wjf
								$.ajax({
									type: "POST",
									url: "<%=path%>/supply/findById.do",
									data: "sp_code="+sp_code,
									dataType: "json",
									async: false,
									success:function(supply){		
										if(!$(".table-body").find("tr:last").find("td:eq(1)").find('span').text()==''){
											$.fn.autoGrid.addRow();
										}
										$(".table-body").find("tr:last").find("td:eq(1)").find('span').text(supply.sp_code).data('sp_name',supply.sp_name);
										$(".table-body").find("tr:last").find("td:eq(2)").find('span').text(supply.sp_name);
										$(".table-body").find("tr:last").find("td:eq(3)").find('span').text(supply.sp_desc);
										$(".table-body").find("tr:last").find("td:eq(4)").find('span').text(supply.unit);
										$(".table-body").find("tr:last").find("td:eq(5)").find('span').text(1).attr('title',1).css("text-align","right");
										$(".table-body").find("tr:last").find("td:eq(6)").find('span').text(price.toFixed(2)).attr('title',price).css("text-align","right");
										$(".table-body").find("tr:last").find("td:eq(7)").find('span').text(price.toFixed(2)).attr('title',price).css("text-align","right");
										$(".table-body").find("tr:last").find("td:eq(8)").find('span').text(supply.unit1);
										$(".table-body").find("tr:last").find("td:eq(9)").find('span').text((supply.unitper).toFixed(2)).attr('title',supply.unitper).css("text-align","right");
										$(".table-body").find("tr:last").find("td:eq(12)").find('span').text(spprice.taxdes);
										var pricesq = Number(price)/(1+Number(spprice.tax));
										$(".table-body").find("tr:last").find("td:eq(13)").find('span').text(pricesq.toFixed(2)).attr('title',pricesq).css("text-align","right");
										$(".table-body").find("tr:last").find("td:eq(14)").find('span').text(pricesq.toFixed(2)).attr('title',pricesq).css("text-align","right");
										$(".table-body").find("tr:last").find("td:eq(15)").find('span').text($('#maded').val());
										$(".table-body").find("tr:last").data("unitper",supply.unitper);
										$(".table-body").find("tr:last").data("tax",spprice.tax);
										$(".table-body").find("tr:last").data("sp_per1",supply.sp_per1);
										$(".table-body").find("tr:last").data("ynbatch",supply.ynbatch);
										$(".table-body").find("tr:last").data("ynprice",spprice.sta);
									}
								});
							}
						});
						if(i == sp_code_arry.length -1){//如果是最后一个，焦点放到记录的分店列上
							$(".table-body").find("tr:last").find("td:eq(10)").find('span').focus();
						}
					}
			 	}
			 	//回车换焦点start
				    var array = new Array();        
			 	    //定义需要做切换的input输入框，最后可以放一个提交按钮，这样最好一个input点击回车后可以直接触发按钮的点击       
			 	    array = ['typ','maded','positn_select', 'positn', 'deliver_select','deliver','memo'];        
			 		//定义加载后定位在第一个输入框上          
			 		$('#'+array[2]).focus();            
			 		$('select,input[type="text"]').keydown(function(e) {                  
				 		//使用jquery的这种获取事件方法，不必再指定event.srcElement或者event.target  
				 		var event = $.event.fix(e);                
				 		//判断如果按键事件的按键代码是回车，则从数组中获取下一个元素的ID,并设置焦点                       
				 		if (event.keyCode == 13) {                
				 			var index = $.inArray($.trim($(event.target).attr("id")), array);//alert(index)
				 				if(index==3 && $('#'+array[index+1]).attr('disabled')=='disabled'){
				 					++index;
				 				}
				 				$('#'+array[++index]).focus();
				 				if(index==5){
				 					$('#deliver_select').val($('#deliver').val());
				 				} 
				 				if(index==3){
				 					$('#positn_select').val($('#positn').val());
				 				} 
				 				if(index==7){
				 					$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),2);
				 				} 
				 		}
			 		});    
			 		document.getElementById("typ").onfocus=function(){
			 			this.size = this.length;
			 			$('#typ').css('height','155px');
			 			$('#typ').parent('.form-input').css('z-index','3');
			 			};
		 			document.getElementById("typ").onblur = function(){
			 			this.size = 1;
			 			$('#typ').css('height','20px');
		 			};
		 			$('#typ').bind('dblclick', function() {
	 					$('#typ').blur();
	 				});
		 			$('#typ').change(function(){//onchange方法  2014.10.20wjf
		 				if($(this).val() == '9920' || $(this).val() == '9919'){
		 					$('.table-body').find('tr').remove();
		 					$('#sum_num').text('0');
		 				}else{
		 					setEditable();
		 				}
		 			});
			 		document.getElementById("positn").onfocus=function(){
			 			this.size = this.length;
			 			$('#positn').css('height','20px');
			 			$('#positn').parent('.form-input').css('z-index','2');
			 		};
		 			document.getElementById("positn").onblur = function(){
			 			this.size = 1;
			 			$('#positn').css('height','20px');
		 			};
			 		$('#positn').bind('dblclick', function() {
	 					$('#positn_select').val($(this).val());
	 					$('#positn').blur();
	 				});
			 		document.getElementById("deliver").onfocus=function(){
			 			this.size = this.length;
			 			$('#deliver').css('height','330px');
			 			$('#deliver').parent('.form-input').css('z-index','1');
			 			};
		 			document.getElementById("deliver").onblur = function(){
			 			this.size = 1;
			 			$('#deliver').css('height','20px');
		 			};
		 			$('#deliver').bind('dblclick', function() {
	 					$('#deliver_select').val($(this).val());
	 					$('#deliver').blur();
	 				});
		 		//回车换焦点end
				//新增
				var status = $("#curStatus").val();
				if(status == 'add'&& !$("#deliver").val()){
					alert("总部未设置分店供应商,不能新增,请联系总部！");
					return;
				}
				if(status == 'add')setEditable();
				<%-- curStatus 0:init;1:edit;2:add;3:show --%>
				//判断按钮的显示与隐藏
				if(status == 'add'){
					loadToolBar([true,true,false,false,false,true,false]);
					$('#chk1memo2').addClass('memoClass');
				}else if(status == 'show'){//查询页面双击返回
					loadToolBar([true,true,true,true,true,false,true]);
					$('#positn').attr("disabled","disabled");
					$('#deliver').attr("disabled","disabled");
					$('#positn_select').attr("disabled","disabled");
					$('#deliver_select').attr("disabled","disabled");
					$('#typ').attr("disabled","disabled");
					$('#maded').attr("disabled","disabled");
					$('#chk1memo2').attr('disabled',false);
				}else{//init
					loadToolBar([true,true,false,false,false,false,false]);
					$('#positn').attr("disabled","disabled");
					$('#deliver').attr("disabled","disabled");
					$('#positn_select').attr("disabled","disabled");
					$('#deliver_select').attr("disabled","disabled");
					$('#typ').attr("disabled","disabled");
					$('#maded').attr("disabled","disabled");
					$('#chk1memo2').addClass('memoClass');
				}
			
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'typ',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="document_types"/><fmt:message key="cannot_be_empty"/>！']
					},{
						type:'text',
						validateObj:'vouno',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="document_number"/><fmt:message key="cannot_be_empty"/>！']
					},{
						type:'text',
						validateObj:'maded',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="date_of_the_system_alone"/><fmt:message key="cannot_be_empty"/>！']
					},{
						type:'text',
						validateObj:'chkinno',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="orders_num"/><fmt:message key="cannot_be_empty"/>！']
					},{
						type:'text',
						validateObj:'positn',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="purchase_positions"/><fmt:message key="cannot_be_empty"/>！']
					},{
						type:'text',
						validateObj:'deliver',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="suppliers"/><fmt:message key="cannot_be_empty"/>！']
					}]
				});
				$("select[name='typ'] option[value='<c:out value="${chkinm.typ}"/>']").attr("selected","selected");
				$('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
				setElementHeight('.grid',['.tool'],$(document.body),180);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
			});
			function loadToolBar(use){
				$('.tool').html('');
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />(<u>F</u>)',
							title: '<fmt:message key="select"/><fmt:message key="direct_dial_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								var status = $('#curStatus').val();
								if(status == 'add' || status == 'edit')
									if(!confirm('<fmt:message key="data_unsaved_whether_to_continue"/>？'))return;
								searchChkinm();
							}
						},'-',{
							text: '<fmt:message key="insert" />(<u>A</u>)',
							title: '<fmt:message key="insert"/><fmt:message key="direct_dial_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')}&&use[1],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								var status = $('#curStatus').val();
								if(status == 'add' || status == 'edit')
									if(!confirm('<fmt:message key="data_unsaved_whether_to_continue"/>？'))return;
								window.location.replace("<%=path%>/chkinmZbMis/addzb.do");
							}
						},{
							text: '<fmt:message key="template" />',
							title: '<fmt:message key="template" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')}&&use[5],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								getChkinDemo();
							}
						},{
							text: '<fmt:message key="edit" />(<u>E</u>)',
							title: '<fmt:message key="edit"/><fmt:message key="direct_dial_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[3],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								var status = $('#curStatus').val();
								if(status == 'init')return;
								if(status == 'add' || status == 'edit')
									if(!confirm('<fmt:message key="data_unsaved_whether_to_continue"/>？'))return;
								$("#curStatus").val("edit");
								loadToolBar([true,true,true,false,true,true,false]);
								if($('#typ').val() == '9920' || $('#typ').val() == '9919'){
									setEditable2();
									$('#maded').attr('disabled',false);//设置只可以编辑时间
								}else{
									setEditable();
									$('#positn').attr("disabled",false);
									$('#deliver').attr("disabled",false);
									$('#positn_select').attr("disabled",false);
									$('#deliver_select').attr("disabled",false);
									$('#typ').attr("disabled",false);
									$('#maded').attr("disabled",false);
									$('#positn_select').focus();
								}
							}
						},{
							text: '<fmt:message key="select"/><fmt:message key="reversal"/>',
							title: '<fmt:message key="select"/><fmt:message key="reversal"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'writeoff')}&&use[5],
// 							useable: use[5],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								if ($('#typ').val()=="9920"||$('#typ').val()=="9919") {
									var deliver = $('#deliver_select').val();
									var action="<%=path%>/chkinmZbMis/addChkinmzfByCx.do?deliver="+deliver+"&action=init";
									$('body').window({
										id: 'window_a',
										title: '<fmt:message key="select"/><fmt:message key="reversal"/>',
										content: '<iframe id="chkstomFrame" frameborder="0" src='+action+'></iframe>',
										width: '90%',
										height: '90%',
										draggable: true,
										isModal: false
									});
								}else {
									alert('<fmt:message key="please_select"/><fmt:message key="document_types"/><fmt:message key="reversal"/><fmt:message key="or"/><fmt:message key="returns"/>！');
								}
							}
						},{
							text: '<fmt:message key="save" />(<u>S</u>)',
							title: '<fmt:message key="save"/>',
							useable: use[5],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								//先判断仓库有没有期初
								$.ajax({
									url:"<%=path%>/misbohcommon/checkQC.do?code="+$('#positn').val(),
									type:"post",
									success:function(data){
										if(data){
											if($("#curStatus").val()=='add' || $("#curStatus").val()=='edit'){
												if(validate._submitValidate()){
			// 										$('#chk1memo').focus();
													saveOrUpdateChkinm();
												}
											}else{
												alert('<fmt:message key="no_edited_documents_to_be_saved"/>！');
											}
										}else{
											alert($('#positn').val()+'<fmt:message key="the_storage_positions_do_not_at_the_beginning_of_the_period"/>！<fmt:message key="can_not"/><fmt:message key="save"/>！');
											return;
										}
									}
								});
							}
						},{
							text: '<fmt:message key="check" />(<u>C</u>)',
							title: '<fmt:message key="check"/><fmt:message key="direct_dial_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')}&&use[6],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-58px','-240px']
							},
							handler: function(){
								checkChkinm();
							}
						},{
							text: '<fmt:message key="delete" />(<u>D</u>)',
							title: '<fmt:message key="delete"/><fmt:message key="direct_dial_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')}&&use[2],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								deleteChkinm();
							}
						},'-',{
							text: '<fmt:message key="print" />(<u>P</u>)',
							title: '<fmt:message key="print"/><fmt:message key="direct_dial_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[4],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								printChkinm();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								var status = $("#curStatus").val();
								if(status == 'add' || status == 'edit')
									if(!confirm('<fmt:message key="data_unsaved_whether_to_exit"/>？'))return;
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}
					]
				});
			}	
			function findPositnByDes(inputObj){
				$('#positn_select').val($(inputObj).val());
	        }
			function findDeliverByDes(inputObj){
				$('#deliver_select').val($(inputObj).val());
	        }
			function pickedFunc(){
				var cur=$('#maded').val();
				var uri = "<%=path%>/chkinmZbMis/getVouno.do?maded=" + cur;
				$.get(uri,function(data){
					$("input[name='vouno']").val(data);
				});
			}
			function openChkinm(chkinno){
				window.location.replace("<%=path%>/chkinmZbMis/update.do?inout=zf&chkinno="+chkinno);
			}
			function setEditable(){
				if($('#curStatus').val()=='add'){
					$('#sum_num').text(Number($('#sum_num').text())+1);//总行数
				}
				$(".table-body").autoGrid({
					initRow:1,
					colPerRow:18,
					VerifyEdit:{verify:true,enable:function(cell,row){
						return row.find('td').index(cell) == 2 || (row.find("td:eq(2)").text() == null || $.trim(row.find("td:eq(2)").text()) != '' ) ? true : false;
					}},
					widths:[25,80,110,80,60,60,60,70,60,60,70,110,65,60,70,90,100,110],
					colStyle:['','','','','',{background:"#F1F1F1"},{background:"#F1F1F1"},{background:"#F1F1F1"},'',{background:"#F1F1F1"},'','','','','','','',''],
					editable:[2,5,6,7,9,11,15,16,17],
					onLastClick:function(row){
						var sp_code = row.find('td:eq(1) span').text();
						if(sp_code && $.inArray(sp_code,inselected) >= 0)//如果本物资存在，则先在集合删除
							inselected.splice($.inArray(sp_code,inselected),1);
						getTotalSum();
					},
					onEnter:function(data){
						if(data.curobj.closest('tr').find('td').index(data.curobj.closest('td')) == 2){
							if($.trim(data.curobj.closest('td').prev().text())){
								data.curobj.find('span').html(data.curobj.closest('td').prev().find('span').data('sp_name'));
								return;
							}else if(!data.actionobj){
								$.fn.autoGrid.setCellEditable(data.curobj.closest('tr'),2);
								return;
							}
						}
						$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.ovalue) ;
					},
					cellAction:[{
						index:2,
						action:function(row){
							$.fn.autoGrid.setCellEditable(row,5);
						},
						onCellEdit:function(event,data,row){
							$(".table-body").scrollLeft(0);
							data['url'] = '<%=path%>/misbohcommon/findSupplyTop10.do';
							//if(!isNaN(data.value))
								data['key'] = 'sp_code';
							//else
							//	data['key'] = 'sp_init';
							data.sp_position = $('#positn_select').val();
							$.fn.autoGrid.ajaxEdit(data,row);
						},
						resultFormat:function(data){
							var desc = "";
							if(data.sp_desc!=null){
								desc = "-"+data.sp_desc;
							}
							return data.sp_code+'-'+data.sp_init+'-'+data.sp_name + desc;
						},
						afterEnter:function(data,row){
							var sp_code = row.find('td:eq(1) span').text();
							if(sp_code && $.inArray(sp_code,inselected) >= 0)//如果本物资存在，则先在集合删除
								inselected.splice($.inArray(sp_code,inselected),1);
							//将新物资添加到已存在的集合
							if($.inArray(data.sp_code,inselected)>=0){
								showMessage({
									type: 'error',
									msg: '<fmt:message key="added_supplies_remind"/>！',
									speed: 1000
								});
								//把原来的再放回去
								inselected.push(sp_code);
								$.fn.autoGrid.setCellEditable(row,2);
								return;
							}else{
								inselected.push(data.sp_code);
							}
							//查询bao价
							$.ajax({
								type: "POST",
								url: "<%=path%>/misbohcommon/findBprice.do",
								data: "sp_code="+data.sp_code+"&area="+$('#positn_select').val()+"&madet="+$('#maded').val()+"&deliver="+$('#deliver').val(),
								dataType: "json",
								success:function(spprice){
									row.find("td:eq(1)").find('span').text(data.sp_code).data('sp_name',data.sp_name);
									row.find("td:eq(2)").find('span input').val(data.sp_name).focus();
									row.find("td:eq(3)").find('span').text(data.sp_desc==null?"":data.sp_desc);
									row.find("td:eq(4)").find('span').text(data.unit);
									row.find("td:eq(5)").find('span').text(0).attr('title',0).css("text-align","right");
									row.find("td:eq(6)").find('span').text(Number(spprice.price).toFixed(2)).attr('title',Number(spprice.price)).css("text-align","right");
									row.find("td:eq(7)").find('span').text(0).attr('title',0).css("text-align","right");
									row.find("td:eq(8)").find('span').text(data.unit1);
									row.find("td:eq(9)").find('span').text(0).attr('title',0).css("text-align","right");
									row.find("td:eq(12)").find('span').text(spprice.taxdes);
									row.find("td:eq(13)").find('span').text((Number(spprice.price)/(1+Number(spprice.tax))).toFixed(2)).attr('title',Number(spprice.price)/(1+Number(spprice.tax))).css("text-align","right");
									row.find("td:eq(14)").find('span').text(0).attr('title',0).css("text-align","right");
									row.data("tax",spprice.tax);
									row.data("sp_per1",data.sp_per1);
									row.data("unitper",data.unitper);
									row.data("ynbatch",data.ynbatch);//是否批次管理
									row.data("ynprice",spprice.sta);
								}
							});
							//查询售价结束
						}
					},{
						index:5,
						action:function(row,data2){
							if(Number(data2.value) == 0){
								alert('<fmt:message key="number_cannot_be_zero"/>！');
								row.find("td:eq(5)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,5);
							}else if(Number(data2.value) < 0){
								alert('<fmt:message key="number_cannot_be_negative"/>！');
								row.find("td:eq(5)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,5);
							}else if(isNaN(data2.value)){
								alert('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(5)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,5);
							}else if(data2.value.length > 6){
								alert('<fmt:message key="the_maximum_length_not"/>6<fmt:message key="digit"/>！');
								row.find("td:eq(5)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,5);
							}else{
								//标准数量变 要同步5,7,9,14
								row.find("td:eq(5)").find('span').attr('title',data2.value);
								setValue(row,5);//设置本行其他值
								var realPrice = Number(row.find("td:eq(6) span").attr('title')?row.find("td:eq(6) span").attr('title'):row.find("td:eq(6)").text());
								var ynprice = row.data('ynprice');//判断有没有做报价
								if(!ynprice){
									ynprice = getYnprie(row);
								}
								if(Number(realPrice) > 0 && ynprice == 'Y'){
									if(row.data("unitper") == 0){
										$.fn.autoGrid.setCellEditable(row,9);
									}else{
										$.fn.autoGrid.setCellEditable(row,11);
									}
								}else{
									$.fn.autoGrid.setCellEditable(row,6);
								}
								getTotalSum();
							}
						}
					},{
						index:6,
						action:function(row,data2){
							if(Number(data2.value) < 0){
								alert('<fmt:message key="number_cannot_be_negative"/>！');
								row.find("td:eq(6)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,6);
							}else if(isNaN(data2.value)){
								alert('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(6)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,6);
							}else{
								//单价变 要同步6,7,11,12
								row.find("td:eq(6)").find('span').attr('title',data2.value);
								setValue(row,6);//设置本行其他值
								$.fn.autoGrid.setCellEditable(row,7);
								getTotalSum();
							}
						}
					},{
						index:7,
						action:function(row,data2){
							if(Number(data2.value) < 0){
								alert('<fmt:message key="number_cannot_be_negative"/>！');
								row.find("td:eq(7)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,7);
							}else if(isNaN(data2.value)){
								alert('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(7)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,7);
							}else{
								//金额变要变 7,6,11,12
								row.find("td:eq(7)").find('span').attr('title',data2.value);
								var amount = row.find("td:eq(5) span").attr('title')?row.find("td:eq(5) span").attr('title'):row.find("td:eq(5)").text();
								var realPrice = Number(data2.value)/Number(amount);
								row.find("td:eq(6)").find('span').text(realPrice.toFixed(2)).attr('title',realPrice);
								setValue(row,7);//设置本行其他值
								if(row.data("unitper") == 0){
									$.fn.autoGrid.setCellEditable(row,9);
								}else{
									$.fn.autoGrid.setCellEditable(row,11);
								}
								getTotalSum();
							}
						}
					},{
						index:9,
						action:function(row,data2){
							if(Number(data2.value) == 0){
								alert('<fmt:message key="number_cannot_be_zero"/>！');
								row.find("td:eq(9)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,9);
							}else if(Number(data2.value) < 0){
								alert('<fmt:message key="number_cannot_be_negative"/>！');
								row.find("td:eq(9)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,9);
							}else if(isNaN(data2.value)){
								alert('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(9)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,9);
							}else if(data2.value.length > 6){
								alert('<fmt:message key="the_maximum_length_not"/>6<fmt:message key="digit"/>！');
								row.find("td:eq(9)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,9);
							}else{
								//参考数量变5,7,9,12要变
								row.find("td:eq(9)").find('span').attr('title',data2.value);
								if(Number(row.data("unitper")) != 0){//如果转换率不为0的  才修改  否则会报错wjf
									row.find("td:eq(5) span").text((Number(data2.value)/Number(row.data("unitper"))).toFixed(2)).attr('title',Number(data2.value)/Number(row.data("unitper")));//参考数量改变时改变标准数量
									setValue(row,9);//设置本行其他值
									getTotalSum();
								}
								$.fn.autoGrid.setCellEditable(row,11);
							}
						}
					},{
						index:11,
						action:function(row,data){
							if(data.value && row.find('td:eq(10)').text() != ''){
								if(row.data("sp_per1")!='' && row.data("sp_per1")!='0.0' && row.data("sp_per1")!='0'){
									$.fn.autoGrid.setCellEditable(row,15);
								}else if(row.data("ynbatch")=='Y'){
									$.fn.autoGrid.setCellEditable(row,16);
								}else{
									$.fn.autoGrid.setCellEditable(row,17);
								}
							}else{
								$.fn.autoGrid.setCellEditable(row,11);
							}
						},
						onCellEdit:function(event,data,row){
							data['url'] = '<%=path%>/chkinmZbMis/findAllPositnDeptN.do';
							data['key'] = 'init';
							$.fn.autoGrid.ajaxEdit(data,row);
						},
						resultFormat:function(data){
							return data.code+'-'+data.init+'-'+data.des;
						},
						afterEnter:function(data,row){
							row.find("td:eq(10)").find('span').text(data.code);
							row.find("td:eq(11)").find('span input').val(data.des).focus();
						}
					},{
						index:15,
						action:function(row,data){
							$.fn.autoGrid.setCellEditable(row,16);
						},CustomAction:function(event,data){
							var input = data.curobj.find('span').find('input');
							input.addClass("Wdate text");
							input.bind('click',function(){
								input.unbind('keyup');
								new WdatePicker({
									startDate:'%y-%M-{%d+1}',
									el:'input',
									onpicked:function(dp){
										data.curobj.find('span').html(dp.cal.getNewDateStr());
										$.fn.autoGrid.setCellEditable(data.row,16);
									},
									oncleared:function(dp){ 
										if(row.data("sp_per1")!='' && row.data("sp_per1")!='0.0' && row.data("sp_per1")!='0'){
											alert('<fmt:message key="supplies"/><fmt:message key="production_date"/><fmt:message key="cannot_be_empty"/>!');
										}else{
											data.curobj.find('span').html("");
											if(row.data("ynbatch")=='Y'){
												$.fn.autoGrid.setCellEditable(row,16);
											}else{
												$.fn.autoGrid.setCellEditable(row,17);
											}
										};
									}
								});
							});
							if(Number(row.data("sp_per1"))!=0){
								input.trigger('click'); 
							}
						}
					},{
						index:16,
						action:function(row,data){
							if(row.data('ynbatch') == 'Y' && data.value == ''){
								alert('<fmt:message key="supplies"/><fmt:message key="pc_no"/><fmt:message key="cannot_be_empty"/>!');
								$.fn.autoGrid.setCellEditable(row,16);
							}else{
								$.fn.autoGrid.setCellEditable(row,17);
							}
						}
					},{
						index:17,
						action:function(row,data){
							if(!row.next().html())$.fn.autoGrid.addRow();
							//add wangjie 2014年10月30日 13:09:06
							var pre_row_num = row.index();
							var code = row.find("td:eq(10)").find("span").text();
							var postion = row.find("td:eq(11)").find("span").text();
							$(".table-body").find("tr:eq("+(pre_row_num+1)+")").find("td:eq(10)").find('span').text(code);
							$(".table-body").find("tr:eq("+(pre_row_num+1)+")").find("td:eq(11)").find('span').text(postion);
							
							$.fn.autoGrid.setCellEditable(row.next(),2);
							$('#sum_num').text(Number($('#sum_num').text())+1);//总行数
						}
					}]
				});
			}
			
			//设置一行的值
			function setValue(row,index){
				var amount = Number(row.find("td:eq(5) span").attr('title')?row.find("td:eq(5) span").attr('title'):row.find("td:eq(5)").text());
				var realPrice = Number(row.find("td:eq(6) span").attr('title')?row.find("td:eq(6) span").attr('title'):row.find("td:eq(6)").text());
				var unitper = Number(row.data("unitper"));
				var tax = Number(row.data("tax"));
				if(index != 7){
					row.find("td:eq(7) span").text((amount*realPrice).toFixed(2)).attr('title',amount*realPrice);
				}
				if(index != 9){
					row.find("td:eq(9) span").text((amount*unitper).toFixed(2)).attr('title',amount*unitper);
				}
				row.find("td:eq(13) span").text((realPrice/(1+ tax)).toFixed(2)).attr('title',realPrice/(1+ tax));
				row.find("td:eq(14) span").text((amount*realPrice/(1+tax)).toFixed(2)).attr('title',amount*realPrice/(1+tax));
			}
			
			//冲消用
			function setEditable2(){
				$(".table-body").autoGrid({
					initRow:1,
					colPerRow:18,
					widths:[25,80,110,80,60,60,60,70,60,60,70,110,65,60,70,90,100,110],
					colStyle:['','','','','',{background:"#F1F1F1"},{background:"#F1F1F1"},{background:"#F1F1F1"},'',{background:"#F1F1F1"},'','','','','','','',''],
					editable:[]
				});
			}
			function searchChkinm(){
				var curwindow = $('body').window({
					id: 'window_searchChkinm',
					title: '<fmt:message key="query_direct_dial_single"/>',
					content: '<iframe id="searchChkinmFrame" frameborder="0" src="<%=path%>/chkinmZbMis/list.do?checkOrNot=unCheck&action=init&inout=zf"></iframe>',
					width: 780,
					height: 480,
					draggable: true,
					isModal: true
				});
				curwindow.max();
			}
			
			//查找未审核单据
			function searchUncheck(bdate,edate){
				var action = "<%=path%>/chkinmZbMis/list.do?checkOrNot=unCheck&inout=zf&maded="+bdate+"&madedEnd="+edate;
				var curwindow = $('body').window({
					id: 'window_searchChkinm',
					title: '<fmt:message key="query_direct_dial_single"/>',
					content: '<iframe id="searchChkinmFrame" frameborder="0" src='+action+'></iframe>',
					width: 780,
					height: 480,
					draggable: true,
					isModal: true
				});
				curwindow.max();
			}
			
			//增加 或者 修改
			function saveOrUpdateChkinm(){
				//检测要保存的单据是否已被审核
				var dataS = {},showM = "yes";
				dataS['vouno'] = $("#vouno").val();
				dataS['chkinno'] = $("#chkinno").val();
				dataS['active'] = 'edit';
				if($("#curStatus").val()=='edit')
					$.ajax({url:'<%=path%>/chkinmZbMis/chkChect.do',type:'POST',
						data:dataS,async:false,success:function(data){
						if ("YES"!=data) {
							alert(data);
							showM='no';
						}
					}});
				if('no' == showM){
					return false;
				}
				
				var selected = {};
				selected['typ'] = $('#typ').val();
				selected['vouno'] = $('#vouno').val();
				selected['maded'] = $('#maded').val();
				selected['madeby'] = $('#madeby').val();
				selected['chkinno'] = $('#chkinno').val();
				selected['positn.code'] = $('#positn').val();
				selected['deliver.code'] = $('#deliver').val();
				selected['checby'] = $('#checby').val();
				selected['memo'] = $('#memo').val();
				//table 数组
				var numNull=0;//默认0代表成功，1代表0值，2代表非数字
				var isNull=0;
				var sp_name = "";//用来判断有问题的物资
				var flag = true;//判断档口是否期初
				var deptList = [];
				$('.table-body').find('tr').each(function(i){
					if($(this).find('td:eq(1)').find('span').html()!=''){
						isNull=1;
// 						$(this).find('td:eq(12)').click();
						sp_name = $(this).find('td:eq(2) span input').val() ? $(this).find('td:eq(2) span input').val() : $(this).find('td:eq(2)').text();
						var sp_per1 = $(this).data("sp_per1");//判断是否需要生成日期
						var ynbatch = $(this).data('ynbatch');//判断是否需要批次号
						var amount=$(this).find('td:eq(5) span').attr('title');
						var amount1=$(this).find('td:eq(9) span').attr('title');//数量1
						var price=$(this).find('td:eq(6) span').attr('title');
						var totalamt=$(this).find('td:eq(7) span').attr('title');
						var dued = $(this).find('td:eq(15) span input').val() ? $(this).find('td:eq(15) span input').val() : $(this).find('td:eq(15)').text();
						var pcno = $(this).find('td:eq(16) span input').val() ? $(this).find('td:eq(16) span input').val() : $(this).find('td:eq(16)').text();
						var indept=$(this).find('td:eq(10)').text();
						if(amount==0 || amount==0.0 ||amount==0.00 || amount1 == 0 || amount1==0.0 ||amount1==0.00){//参考数量也不能为0  2015.1.5wjf
							numNull=1;
							return false; 
						}
						if(amount=="" || amount==null || isNaN(amount) || amount1=="" || amount1==null || isNaN(amount1)){
							numNull=2;//2代表数量为空或者非数字
							return false; 
						}
						if(price=="" || price==null || isNaN(price)){
							numNull=3;//3代表单价为空或者非数字
							return false; 
						}
						if($('#isChkinPriceNot0').val() == 'Y' && Number(price) == 0){
							numNull=5;//5代表不能是0
							return false; 
						}
						if(totalamt=="" || totalamt==null || isNaN(totalamt)){
							numNull=4;//4代表金额为空或者非数字
							return false; 
						}
						if(indept==''){//直发分店不能为空
							isNull=5;
							return false; 
						}
						if(!isNaN(sp_per1) && Number(sp_per1) != 0 && dued == ''){//生产<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>
							isNull=6;
							return false; 
						}
						if('Y' == ynbatch && pcno == ''){//批次号不能为空
							isNull=7;
							return false; 
						}
						//判断有没有做期初
						if($.inArray(indept,deptList)<0){//不存在才查询期初
							deptList.push(indept);
							var deptDes = $(this).find('td:eq(11)').text();
							$.ajax({
								url:"<%=path%>/misbohcommon/checkQC.do?code="+indept,
								type:"post",
								async:false,
								success:function(data){
									if(!data){
										flag = false;
										alert(deptDes+'<fmt:message key="the_storage_positions_do_not_at_the_beginning_of_the_period"/>!<fmt:message key="can_not"/><fmt:message key="storage"/>!');
									}
								}
							});
						}
						selected['chkindList['+i+'].supply.sp_code'] = $(this).find('td:eq(1)').text();
						selected['chkindList['+i+'].supply.sp_name'] = sp_name;
						selected['chkindList['+i+'].supply.sp_desc'] = $(this).find('td:eq(3)').text();
						selected['chkindList['+i+'].supply.unit'] = $(this).find('td:eq(4)').text();//单位
						selected['chkindList['+i+'].amount'] = Number(amount);//数量
						selected['chkindList['+i+'].price'] = Number(price);//单价
						selected['chkindList['+i+'].totalamt'] = Number(totalamt);//金额
						selected['chkindList['+i+'].amount1'] = Number(amount1);//数量1
						selected['chkindList['+i+'].indept'] = indept;//分店代码
						selected['chkindList['+i+'].dued'] = dued;//生产日期
						selected['chkindList['+i+'].pcno'] = pcno;//批次
						var value = $.trim($(this).find("td:eq(17)").text()) ? $.trim($(this).find("td:eq(17)").text()) : $.trim($(this).find("td:eq(17) input").val());
						selected['chkindList['+i+'].memo'] = value;//备注  2014.11.19 wjf
						selected['chkindList['+i+'].sp_id'] = $(this).data("sp_id");
					}
				});
				if(!flag){
					return;
				}
				if(isNull==0){
					alert('<fmt:message key="empty_document_unallowed_please_select_supplies"/>！');
					return;
				}
				if(Number(numNull)==1){//数量不为0
					alert('<fmt:message key="supplies"/>:['+sp_name+']<fmt:message key="number_of_standards"/><fmt:message key="number_cannot_be_zero"/>！');
					return;
				}
				if(Number(numNull)==2){//数量为空或字母
					alert('<fmt:message key="supplies"/>:['+sp_name+']<fmt:message key="quantity"/><fmt:message key="not_a_valid_number"/>！');
					return;
				}
				if(Number(numNull)==3){//单价为空或字母
					alert('<fmt:message key="supplies"/>:['+sp_name+']<fmt:message key="price"/><fmt:message key="not_a_valid_number"/>！');
					return;
				}
				if(Number(numNull)==4){//金额为空或字母
					alert('<fmt:message key="supplies"/>:['+sp_name+']<fmt:message key="amount"/><fmt:message key="not_a_valid_number"/>！');
					return;
				}
				if(Number(numNull)==5){//价格为0
					alert('<fmt:message key="supplies"/>:['+sp_name+']单价不能为0！');
					return;
				}
				if(isNull==5){
					alert('<fmt:message key="supplies"/>:['+sp_name+']<fmt:message key="scm_dept"/><fmt:message key="cannot_be_empty"/>！');
					return;
				}
				if(isNull==6){
					alert('<fmt:message key="supplies"/>:['+sp_name+']<fmt:message key="production_date"/><fmt:message key="cannot_be_empty"/>！');
					return;
				}
				if(isNull==7){
					alert('<fmt:message key="supplies"/>:['+sp_name+']<fmt:message key="pc_no"/><fmt:message key="cannot_be_empty"/>！');
					return;
				}
				$('#wait').show();
				$('#wait2').show();
				$.post('<%=path%>/chkinmZbMis/updateChkinmzb.do',selected,function(data){
					$('#wait').hide();
					$('#wait2').hide();
					var rs = eval('('+data+')');
					//判断状态
// 					var status = $("#curStatus").val();
// 					var state='<fmt:message key="edit_direct_dial_single"/>';
// 					if(status=="add"){
// 						state='<fmt:message key="add_direct_dial_single"/>';
// 						loadToolBar([true,true,true,true,true,false,true]);//点保存后把按钮状态改为 show
// 						$("#curStatus").val('show');    //修改状态
// 						$('#chk1memo2').attr('disabled',false);//审核备注可用  wjf
// 						$('#chk1memo2').removeClass('memoClass');
// 					}
					if(rs=="1"){
// 						showMessage({
// 							type: 'success',
// 							msg: state+'<fmt:message key="successful"/>！',
// 							speed: 3000
// 						});	
// 						loadToolBar([true,true,true,true,true,false,true]);//点保存成功后把按钮状态改为 show
						$("#curStatus").val('show');    //修改状态
						alert('<fmt:message key="save_successful"/>！');
						var  chkinno=$('#chkinno').val();
						window.location.replace("<%=path%>/chkinmZbMis/update.do?inout=zf&chkinno="+chkinno);
// 						$('#chk1memo2').attr('disabled',false);//审核备注可用  wjf
// 						$('#chk1memo2').removeClass('memoClass');
					}else if(rs == -9){
						alert('此会计日不在本会计年中，不能保存！');
						return;
					}else{
						showMessage({
							type: 'error',
							msg: state+'<fmt:message key="failure"/>！',
							speed: 1000
						});	
					}
				});
			}
			//审核
			function checkChkinm(){
				//判断是否盘点2015.5.27wjf
				var msg = 0;
				var data = {};
				data['maded'] = $('#maded').val();
				data['positn'] = $('#positn').val();
				$('#wait').show();
				$('#wait2').show();
				$.ajax({url:'<%=path%>/misbohcommon/chkYnInout.do',type:'POST',
						data:data,async:false,success:function(data){
					$('#wait').hide();
					$('#wait2').hide();
					msg = data;
				}});
				if(1 == msg){//提示已做盘点，是否继续
					if(!confirm('<fmt:message key="the_current_store"/><fmt:message key="at"/>'+$('#maded').val()+'<fmt:message key="have_inventoried_Whether_to_continue_for_the_in-out_warehouse_operation"/>?')){return;};
				}else if(2 == msg){//已经盘点不能继续
					alert('<fmt:message key="the_current_store"/><fmt:message key="at"/>'+$('#maded').val()+'<fmt:message key="have_inventoried_Whether_to_cannot_for_the_in-out_warehouse_operation"/>!');
					return;
				}
				var selected = {};
				selected['typ'] = $('#typ').val();
				selected['vouno'] = $('#vouno').val();
				selected['maded'] = $('#maded').val();
				selected['madeby'] = $('#madeby').val();
				selected['chkinno'] = $('#chkinno').val();
				selected['positn.code'] = $('#positn').val();
				selected['deliver.code'] = $('#deliver').val();
				selected['checby'] = $('#checby').val();
				selected['memo'] = $('#memo').val();
				selected['chk1memo'] = $('#chk1memo2').val();//审核备注
				//table 数组
// 				$('.table-body').find('tr').each(function(i){
// 					if($(this).find('td:eq(1)').find('span').html()!=''){
// 						selected['chkindList['+i+'].supply.sp_code'] = $(this).find('td:eq(1)').text();
// 						selected['chkindList['+i+'].supply.sp_name'] = $(this).find('td:eq(2) span input').val() ? $(this).find('td:eq(2) span input').val() : $(this).find('td:eq(2)').text();
// 						selected['chkindList['+i+'].supply.sp_desc'] = $(this).find('td:eq(3)').text();
// 						selected['chkindList['+i+'].supply.unit'] = $(this).find('td:eq(4)').text();//单位
// 						selected['chkindList['+i+'].amount'] = $(this).find('td:eq(5) span input').val() ? $(this).find('td:eq(5) span input').val() : $(this).find('td:eq(5)').text();//数量
// 						selected['chkindList['+i+'].price'] = $(this).find('td:eq(6) span input').val() ? $(this).find('td:eq(6) span input').val() : $(this).find('td:eq(6)').text();//单价
// 						selected['chkindList['+i+'].totalamt'] = $(this).find('td:eq(7) span input').val() ? $(this).find('td:eq(7) span input').val() : $(this).find('td:eq(7)').text();//金额
// 						selected['chkindList['+i+'].amount1'] = $(this).find('td:eq(9) span input').val() ? $(this).find('td:eq(9) span input').val() : $(this).find('td:eq(9)').text();//数量1
// 						selected['chkindList['+i+'].indept'] = $(this).find('td:eq(10)').text();//分店编码
// 						selected['chkindList['+i+'].dued'] = $(this).find('td:eq(15) span input').val() ? $(this).find('td:eq(15) span input').val() : $(this).find('td:eq(15)').text();// 
// 						selected['chkindList['+i+'].pcno'] = $(this).find('td:eq(16) span input').val() ? $(this).find('td:eq(16) span input').val() : $(this).find('td:eq(16)').text();// 
// 						selected['chkindList['+i+'].memo'] = $(this).find('td:eq(17) span input').val() ? $(this).find('td:eq(17) span input').val() : $(this).find('td:eq(17)').text();//备注
// 					}
// 				});
				$('#wait').show();
				$('#wait2').show();
				$.ajax({
					url:'<%=path%>/chkinmZbMis/checkChkinm.do?zb=zb',
					data:selected,
					type:'post',
					success:function(data){
						$('#wait').hide();
						$('#wait2').hide();
						try{
							var rs = eval('('+data+')');
							switch(Number(rs.pr)){
							case 1:
								$('#showChecby').text(rs.checby);
								//弹出提示信息
								alert('<fmt:message key="check_successful"/>！');
								loadToolBar([true,true,false,false,true,false,false]);//点保存后把按钮状态改为 init
								$("#curStatus").val('init');    //修改状态
								break;
							case 0:
								alert('<fmt:message key="cannot_repeat_audit"/>！');
								break;
							case -1:
								alert('审核失败，冲消(退货)的物资数量大于入库数量！请检查！');
								break;
							case -3:
								alert('审核失败，冲消(退货)的物资找不到对应的入库记录！请检查是否已被冲消(退货)！');
								break;
							default:
								alert('<fmt:message key="check"/><fmt:message key="failure"/>！');
								break;
							}
						}catch(e){
							alert(data);
						}
					},
					error:function(msg){
						$('#wait').hide();
						$('#wait2').hide();
						alert('error!');
					}
				});
			}
			//删除
			function deleteChkinm(){
				if(!confirm('<fmt:message key="delete_data_confirm"/>？'))return;
				//检测要保存的单据是否已被审核
				var dataS = {},showM = "yes";
				dataS['vouno'] = $("#vouno").val();
				dataS['chkinno'] = $("#chkinno").val();
				dataS['active'] = 'delete';
				$.ajax({url:'<%=path%>/chkinmZbMis/chkChect.do',type:'POST',
					data:dataS,async:false,success:function(data){
					if ("YES"!=data) {
						alert(data);
						showM='no';
					}
				}});
				if('no' == showM){
					return false;
				}
				var action = '<%=path%>/chkinmZbMis/delete.do?action=init&flag=zb&chkinnoids='+$('#chkinno').val();
				$('#deliver').attr('name','deliver.code');
				$('#positn').attr('name','positn.code');
				$('#listForm').attr('action',action);
				$('#listForm').submit();
			}
			//打印
			function printChkinm(){
				$('#reportForm').find('#chkinno').attr('value',$('#listForm').find('#chkinno').val());
				$('#reportForm').attr('target','report');
				window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
				var action = '<%=path%>/chkinmZbMis/viewChkinm.do';	
				$('#reportForm').attr('action',action);
				$('#reportForm').submit();
				$('#reportForm').attr('target','');
			}
			//判断可不可以修改价格
			function getYnprie(row){
				var sp_code = row.find('td:eq(1)').text();
				var ynprice = null;
				//查询bao价
				$.ajax({
					type: "POST",
					url: "<%=path%>/misbohcommon/findBprice.do",
					data: "sp_code="+sp_code+"&area="+$('#positn_select').val()+"&madet="+$('#maded').val()+"&deliver="+$('#deliver').val(),
					dataType: "json",
					success:function(spprice){
						row.data("ynprice",spprice.sta);
						ynprice = spprice.sta;
					}
				});
				return ynprice;
			}
			function validateByMincnt(index,row,data2){//最小申购量判断
				if(Number(data2.value) < 0){
					alert('<fmt:message key="number_cannot_be_negative"/>！');
					row.find("td:eq("+index+")").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,index);
					return;
				}else if(isNaN(data2.value)){
					alert('<fmt:message key="number_be_not_number"/>！');
					row.find("td:eq("+index+")").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,index);
					return;
				}
			}
			function validator(index,row,data2){//输入框验证
				row.find("input").data("ovalue",data2.value);
				if(Number(data2.value) < 0){
					alert('<fmt:message key="number_cannot_be_negative"/>！');
					row.find("td:eq("+index+")").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,index);
					return;
				}else if(isNaN(data2.value)){
					alert('<fmt:message key="number_be_not_number"/>！');
					row.find("td:eq("+index+")").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,index);
					return;
				}
				if(index=="5"){
					row.find("td:eq(5)").find('span').attr('title',data2.value);
					setValue(row,5);//设置本行其他值
				}else if(index=="6"){
					row.find("td:eq(6)").find('span').attr('title',data2.value);
					setValue(row,6);//设置本行其他值
				}else if(index=="7"){
					row.find("td:eq(7)").find('span').attr('title',data2.value);
					var amount = row.find("td:eq(5) span").attr('title')?row.find("td:eq(5) span").attr('title'):row.find("td:eq(5)").text();
					var realPrice = Number(data2.value)/Number(amount);
					row.find("td:eq(6)").find('span').text(realPrice.toFixed(2)).attr('title',realPrice);
					setValue(row,7);//设置本行其他值
				}else if(index=="9"){
					row.find("td:eq(9)").find('span').attr('title',data2.value);
					if(Number(row.data("unitper")) != 0){//如果转换率不为0的  才修改  否则会报错wjf
						row.find("td:eq(5) span").text((Number(data2.value)/Number(row.data("unitper"))).toFixed(2)).attr('title',Number(data2.value)/Number(row.data("unitper")));//参考数量改变时改变标准数量
						setValue(row,9);//设置本行其他值
					}
				}
				getTotalSum();
			}
			function getTotalSum(){//计算统计数据
				var sum_amount = 0; 
				var sum_totalamt = 0;
				$('.table-body').find('tr').each(function (){
					if($(this).find('td:eq(1)').text()!=''){//非空行
						var amount = $(this).find('td:eq(5) span').attr('title');
						sum_amount += Number(amount);
						var price  = $(this).find('td:eq(7) span').attr('title');
						sum_totalamt += Number(price);
					}
				});
				$('#sum_num').text($(".table-body").find('tr').length);//总行数
				$('#sum_amount').text(sum_amount.toFixed(2));//总数量
				$('#sum_totalamt').text(Number(sum_totalamt).toFixed(2));//总金额
			}
			
			//判断仓位有没有期初
			function checkQC(positn){
				$.ajax({
					url:"<%=path%>/misbohcommon/checkQC.do?code="+positn,
					type:"post",
					success:function(data){
						return data;
					}
				});
			}
			
			// 查找添加盘点模板
			function getChkinDemo(){
		    	var action = "<%=path%>/chkinmZbMis/addChkinzbDemo.do?madet="+$('#maded').val()+"&deliver="+$('#deliver').val();
				$('body').window({
					id: 'window_addChkstoDemo',
					title: '<fmt:message key="template"/>',
					content: '<iframe id="addChkstoDemoFrame" name="addChkstoDemoFrame" frameborder="0" src='+action+'></iframe>',
					width: '80%',//'1000px'
					height: '420px',
					draggable: true,
					isModal: true,
					confirmClose: false,
					topBar: {
						items: [{
								text: '<fmt:message key="enter"/>',
								title: '<fmt:message key="enter"/>',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['0px','-40px']
								},
								handler: function(){
									if(getFrame('addChkstoDemoFrame')){
										window.frames["addChkstoDemoFrame"].enterUpdate();
									}
								}
							},{
								text: '<fmt:message key="cancel"/>',
								title: '<fmt:message key="cancel"/>',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}]
					}
				});
			}
		</script>
	</body>
</html>