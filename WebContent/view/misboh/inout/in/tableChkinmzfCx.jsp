<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>冲消查询</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			
		<style type="text/css">
		.page{
			margin-bottom: 25px;
		}
		.test{
			color:blue;
		}
		.textInput span{
			padding:0px;
		}
		.textInput input{
			border:0px;
			background: #F1F1F1;
		}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<%--存放一个状态 判断是何种操作类型 --%>
		
		<form id="listForm" action="<%=path%>/chkinmZbMis/addChkinmzfByCx.do" method="post">
			<input type="hidden" id="deliver" name="deliver" value="${spbatch.deliver}"/>
			<div class="form-line">
				<div class="form-label"><fmt:message key="startdate"/>：</div>
				<div class="form-input">
					<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${spbatch.bdat}" pattern="yyyy-MM-dd"/>" />
				</div>	
				<div class="form-label" style="margin-left: 17px;width: 93px;"><fmt:message key="supplies_code"/>：</div>
				<div class="form-input">
					<input type="text" style="margin-top:0px;vertical-align:middle;" id="sp_code" name="sp_code" value="${spbatch.sp_code }" />
					<img id="seachSupply" class="search" style="margin-left: 5px;" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
				</div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="enddate"/>：</div>
				<div class="form-input">
					<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${spbatch.edat}" pattern="yyyy-MM-dd"/>" />
				</div>
<!-- 				<div class="form-label">部门：</div> -->
<!-- 				<div class="form-input"> -->
<%-- 					<input type="text"  id="positndes"  name="positndes" readonly="readonly" value="${spbatch.positndes}"/> --%>
<%-- 					<input type="hidden" id="positn" name="positn" value="${spbatch.positn}"/> --%>
<%-- 					<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' /> --%>
<!-- 				</div> -->
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2"><span class="num" style="width: 25px;">&nbsp;</span></td>
								<td rowspan="2"><span style="width:20px;">
									<input type="checkbox" id="chkAll"/></span>
								</td>
								<td rowspan="2"><span style="width:50px;"><fmt:message key="positions"/><fmt:message key="coding"/></span></td>
								<td rowspan="2"><span style="width:65px;"><fmt:message key="positions"/><fmt:message key="name"/></span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="date"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="scm_document_no"/></span></td>
								<td rowspan="2"><span style="width:65px;"><fmt:message key="supplies_code"/></span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="supplies_name"/></span></td>
								<td colspan="4"><span style="width:180px;"><fmt:message key="standard_unit"/></span></td>
								<td colspan="3"><span style="width:140px;"><fmt:message key="reference_unit"/></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="production_date"/></span></td>
								<td rowspan="2"><span style="width:65px;"><fmt:message key="pc_no"/></span></td>
							</tr>
							<tr>						
								<td><span style="width:40px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:50px;"><fmt:message key="price"/></span></td>
								<td><span style="width:50px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:60px;"><fmt:message key="scm_input_num"/></span></td>
								<td><span style="width:40px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:50px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:60px;"><fmt:message key="scm_input_num"/></span></td>								
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body" style="height: 100%">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>
						<c:forEach var="spbatch" items="${spbatchList}" varStatus="status">
							<tr>
								<td align="center"><span style="width:25px;">${status.index+1}</span></td>
								<td >
									<span style="width:20px;text-align: center;">
										<input type="checkbox" name="idList" id="chk_${spbatch.chkno}" value="${spbatch.id}"/>
									</span>
								</td>
								<td><span title="${spbatch.positn }" style="width:50px;">${spbatch.positn }</span></td>
								<td><span title="${spbatch.positndes }" style="width:65px;">${spbatch.positndes }</span></td>
								<td><span title="<fmt:formatDate value="${spbatch.ind }" pattern="yyyy-MM-dd" type="date"/>" style="width:80px;"><fmt:formatDate value="${spbatch.ind }" pattern="yyyy-MM-dd" type="date"/></span></td>
								<td><span title="${spbatch.chkno }" style="width:60px;">${spbatch.chkno }</span></td>
								<td><span title="${spbatch.sp_code }" style="width:65px;">${spbatch.sp_code }</span></td>
								<td><span title="${spbatch.sp_name }" style="width:80px;">${spbatch.sp_name }</span></td>
								<td><span title="${spbatch.unit }" style="width:40px;">${spbatch.unit }</span></td>
								<td><span title="${spbatch.price }" style="width:50px;text-align: right;"><fmt:formatNumber value="${spbatch.price }" type="currency" pattern="0.00"/></span></td>
								<td><span title="${spbatch.amount }" style="width:50px;text-align: right;"><fmt:formatNumber value="${spbatch.amount }" type="currency" pattern="0.00"/></span></td>
								<td class="textInput">
									<span style="width:60px;" title="0">
										<input type="text" onfocus="this.select()" value="0" onblur="getAmt1(this,'${spbatch.unitper}')" onkeyup="validate(this);ajaxSearch(this);" style="width:60px;text-align: right;padding: 0px;" />
									</span>
								</td>
								<td><span title="${spbatch.unit1 }" style="width:40px;">${spbatch.unit1 }</span></td>
								<td><span title="${spbatch.amount1 }" style="width:50px;text-align: right;"><fmt:formatNumber value="${spbatch.amount1 }" type="currency" pattern="0.00"/></span></td>
								<td class="textInput">
									<span style="width:60px;text-align: right;" title="0">
										<input type="text" onfocus="this.select()" value="0" onblur="getAmt(this,'${spbatch.unitper}')" onkeyup="validate(this);ajaxSearch(this);" style="width:60px;text-align: right;padding: 0px;" />
									</span>
								</td>
								<td style="display:none;"><span><input type="hidden" value="${spbatch.unitper }"/></span></td>
								<td style="display:none;"><span><input type="hidden" value="${spbatch.sp_desc }"/></span></td>
								<td style="display:none;"><span><input type="hidden" value="${spbatch.pricesale }"/></span></td>
								<td style="display:none;"><span><input type="hidden" value="${spbatch.tax }"/></span></td>		
								<td><span title="<fmt:formatDate value="${spbatch.dued}" pattern="yyyy-MM-dd"/>" style="width:70px;"><fmt:formatDate value="${spbatch.dued}" pattern="yyyy-MM-dd"/></span></td>															
								<td><span title="${spbatch.pcno }" style="width:65px;">${spbatch.pcno}</span></td>
							</tr>
						</c:forEach>
						</tbody>
					</table>					
				</div>
			</div>
		</form>	
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		//工具栏
		$(document).ready(function(){
			//按钮快捷键
			focus() ;//页面获得焦点
			//键盘事件绑定
		 	$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode)
	            {
	                case 70: $('#autoId-button-102').click(); break;//查询
	                case 83: $('#autoId-button-105').click(); break;//保存
	            }
			}); 
			
		 	//解决谷歌浏览器单击不选中的问题
			$('.textInput').find('input').live('click',function(event){
				var self = this;
				setTimeout(function(){
					self.select();
				},10);
			});
			$('tbody input[type="text"]').attr('disabled',false);
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		   
		    //自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
		});
		$('#bdat').bind('click',function(){
			new WdatePicker();
		});
		$('#edat').bind('click',function(){
			new WdatePicker();
		});
		
		$('#seachSupply').bind('click.custom',function(e){
			if(!!!top.customWindow){
				var defaultCode = $('#sp_code').val();
				top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/misbohcommon/selectSupplyLeft.do?positn=1&defaultCode='+defaultCode,$('#sp_code'));	
			}
		});
		//控制按钮显示
		$('.tool').toolbar({
			items: [{
					text: '<fmt:message key="select" />(<u>F</u>)',
					title: '<fmt:message key="select"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						var a =  new Date($('#edat').val().replace(/-/g,"/")).getTime()- new Date($('#bdat').val().replace(/-/g,"/")).getTime();
						if(a/(24*60*60*1000)>2) {
							alert("<fmt:message key='select'/><fmt:message key='time'/><fmt:message key='can_not_be_greater_than'/>2<fmt:message key='day'/>!");
							return;
						}
// 						if(''==$('#positn').val()){
// 							alert('<fmt:message key="please_select_branche"/>！');
// 							return;
// 						}
						$('#listForm').submit();
					}
				},{
					text: '<fmt:message key="enter" />',
					title: '<fmt:message key="enter" />',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-80px','-0px']
					},
					handler: function(){
						selectSupply();
					}
				},{
					text: '<fmt:message key="just_copy" />',
					title: '<fmt:message key="just_copy" />',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-80px','-0px']
					},
					handler: function(){
						copyAmount();
					}
				},{
					text: '<fmt:message key="cancel" />',
					title: '<fmt:message key="cancel" />',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						parent.$('.close').click();
					}
				}
			]
		});
		function selectSupply(){  	 		
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() > 0){
				//var selected = {}; 
				var isnull=0;
				var sp_name = '';
				checkboxList.filter(':checked').each(function(i){
					sp_name = $(this).parents('tr').find('td:eq(7)').find('span').text();
					var amount = Number($(this).closest('tr').find('td:eq(11) span').attr('title'));
					var amount1 = Number($(this).closest('tr').find('td:eq(14) span').attr('title'));
					var amountold = Number($(this).closest('tr').find('td:eq(10) span').attr('title'));
					var amountold1 = Number($(this).closest('tr').find('td:eq(13) span').attr('title'));
					if (amount == 0 || amount1 == 0) {
							isnull = 1;
							return false;
					}else if ((amount>amountold) || (amount1>amountold1)) {
						isnull=2;
						return false;
					}else if (amount<0||amount1<0) {
						isnull=3;
						return false;
					}
				});	
				if (isnull=='1') {
					alert("<fmt:message key='supplies'/>["+sp_name+"]<fmt:message key='_the_number'/>！");
					return;
				}
				if (isnull=='2') {
					alert("<fmt:message key='supplies'/>["+sp_name+"]<fmt:message key='scm_input_num'/><fmt:message key='More_than'/><fmt:message key='storage'/><fmt:message key='quantity'/>！");
					return;
				}
				if (isnull=='3') {
					alert("<fmt:message key='supplies'/>["+sp_name+"]<fmt:message key='number_cannot_be_negative'/>！");
					return;
				}
				var num=0;
				var first=0;
				checkboxList.filter(':checked').each(function(i){
					if (first==1) {
						parent.$(".table-body").autoGrid.addRow();
					}else if(first == 0){//2014.10.20 wjf
						parent.setEditable2(); 
						if(parent.$(".table-body").find("tr:last").find("td:eq(1)").text()!=""){//豪享来  第二次选择时候需要多加一行   2015.6.4  xlh
							parent.$(".table-body").autoGrid.addRow();
						}
					}
					first=1;
					var amount = Number($(this).closest('tr').find('td:eq(11) span').attr('title'));
					var amount1 = Number($(this).closest('tr').find('td:eq(14) span').attr('title'));
					var realPrice = Number($(this).closest('tr').find('td:eq(9)').find('span').attr('title'));
					var tax = Number($(this).closest('tr').find('td:eq(18)').find('input').val());
					parent.$(".table-body").find("tr:last").find("td:eq(1)").find('span').text($(this).closest('tr').find('td:eq(6)').text());
					parent.$(".table-body").find("tr:last").find("td:eq(2)").find('span').text($(this).closest('tr').find('td:eq(7)').text());
					parent.$(".table-body").find("tr:last").find("td:eq(3)").find('span').text($(this).closest('tr').find('td:eq(16)').find('input').val());
					parent.$(".table-body").find("tr:last").find("td:eq(4)").find('span').text($(this).closest('tr').find('td:eq(8)').text());
					parent.$(".table-body").find("tr:last").find("td:eq(5)").find('span').text((amount*(-1)).toFixed(2)).attr('title',amount*(-1)).css("text-align","right");
					parent.$(".table-body").find("tr:last").find("td:eq(6)").find('span').text(realPrice.toFixed(2)).attr('title',realPrice).css("text-align","right");
					parent.$(".table-body").find("tr:last").find("td:eq(7)").find('span').text((realPrice*(-1)*amount).toFixed(2)).attr('title',realPrice*(-1)*amount).css("text-align","right");
					parent.$(".table-body").find("tr:last").find("td:eq(8)").find('span').text($(this).closest('tr').find('td:eq(12)').text());
					parent.$(".table-body").find("tr:last").find("td:eq(9)").find('span').text((amount1*(-1)).toFixed(2)).attr('title',amount1*(-1)).css("text-align","right");
					parent.$(".table-body").find("tr:last").find("td:eq(10)").find('span').text($(this).closest('tr').find('td:eq(2)').text());
					parent.$(".table-body").find("tr:last").find("td:eq(11)").find('span').text($(this).closest('tr').find('td:eq(3)').text());
					parent.$(".table-body").find("tr:last").find("td:eq(12)").find('span').text(tax);//解决税后价格等不显示问题  wjf
					parent.$(".table-body").find("tr:last").find("td:eq(13)").find('span').text((realPrice*(1+tax)).toFixed(2)).attr('title',realPrice*(1+tax)).css("text-align","right");
					parent.$(".table-body").find("tr:last").find("td:eq(14)").find('span').text((realPrice*(-1)*amount*(1+tax)).toFixed(2)).attr('title',realPrice*(-1)*amount*(1+tax)).css("text-align","right");
					parent.$(".table-body").find("tr:last").find("td:eq(15)").find('span').text($(this).closest('tr').find('td:eq(19)').text());  //生产日期
					parent.$(".table-body").find("tr:last").find("td:eq(16)").find('span').text($(this).closest('tr').find('td:eq(20)').text());  //批次
					parent.$(".table-body").find("tr:last").data("unitper",$(this).closest('tr').find('td:eq(15)').find('input').val());
					parent.$(".table-body").find("tr:last").data("sp_id",$(this).closest('tr').find('td:eq(1)').find('input').val());
					num++;
				});
				parent.getTotalSum();//重新计算行数，数量，总计等  wjf
				parent.$('#positn').attr("disabled",true);
				parent.$('#deliver').attr("disabled",true);
				parent.$('#positn_select').attr("disabled",true);
				parent.$('#deliver_select').attr("disabled",true);
				/* parent.$('#maded').attr("disabled",true); */
				parent.$('#typ').attr("disabled",true);
				parent.$('#seachSupply').remove();  //不让再选物资了     xlh
				parent.$('.close').click();	
			}else{
				alert('<fmt:message key="please_select"/><fmt:message key="reversal"/><fmt:message key="supplies"/>！');
				return ;
			}
			
		}
		/*弹出部门选择树*/
		$('#seachOnePositn').bind('click.custom',function(e){
			if(!!!top.customWindow){
				var defaultCode = $('#positn').val();
				var defaultName = $('#positndes').val();
				//alert(defaultCode+"==="+defaultName);
				var offset = getOffset('positn');
				top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/misbohcommon/findPositnSuper.do?typn='+'7&iffirm=2&mold='+'oneTone&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positndes'),$('#positn'),'760','520','isNull');
			}
		});
		
		//一键复制
		function copyAmount(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() > 0){
				checkboxList.filter(':checked').each(function(i){
					var amount = $(this).closest('tr').find('td:eq(10)').find('span').attr('title');
					var amount1 = $(this).closest('tr').find('td:eq(13)').find('span').attr('title');
					$(this).closest('tr').find('td:eq(11)').find('input').val(Number(amount).toFixed(2));
					$(this).closest('tr').find('td:eq(11) span').attr('title',amount);
					$(this).closest('tr').find('td:eq(14)').find('input').val(Number(amount1).toFixed(2));
					$(this).closest('tr').find('td:eq(14) span').attr('title',amount1);
				});
			}else{
				alert('<fmt:message key="please_select_materials"/>！');
				return;
			}
		}
		
		//计算数量1的值
		function getAmt1(e,f) {
			//1.标准数量赋值
			$(e).closest('td').find('span').attr('title',e.value);
			//2.参考数量赋值
			$(e).parents('tr').find('td:eq(14) span').find('input').val(Number(f*e.value).toFixed(2));
			$(e).parents('tr').find('td:eq(14) span').attr('title',f*e.value);
		}
 		
		//计算数量的值
		function getAmt(e,f) {
			$(e).closest('td').find('span').attr('title',e.value);
			if(f==0 || f==0.0){
				return;
			}
			$(e).parents('tr').find('td:eq(11) span').find('input').val(Number(e.value/f).toFixed(2));
			$(e).parents('tr').find('td:eq(11) span').attr('title',e.value/f);
		}
		
		function ajaxSearch(e){
			if (event.keyCode == 13){	
				$(e).blur();
			} 
		}
		
		//焦点离开检查输入是否合法 
		function validate(e){
			if(null==e.value || ""==e.value){
				e.value=e.defaultValue;
				showMessage({
							type: 'error',
							msg: '<fmt:message key="cannot_be_empty"/>！',
							speed: 1000
							});
				$(e).focus();
				return;
			}
			if(Number(e.value)<0 || isNaN(e.value)){
				e.value=e.defaultValue;
				showMessage({
							type: 'error',
							msg: '<fmt:message key="not_a_valid_number"/>！',
							speed: 1000
							});
				$(e).focus();
				return;
			}
		}
		</script>			
	</body>
</html>