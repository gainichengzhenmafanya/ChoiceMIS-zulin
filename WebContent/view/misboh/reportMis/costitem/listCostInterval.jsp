<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>菜品成本区间分析</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	</head>
	<body>
		<input type="hidden" id="itcode" value="${itcode }"/>
		<input type="hidden" id="bdat" value="${bdat }"/>
		<input type="hidden" id="edat" value="${edat }"/>
		<input type="hidden" id="onlycbk" value="${onlycbk }"/>
		<div class="tool">
		</div>
		<div class="easyui-tabs" id="tt" fit="false" plain="false" style="height:392px;">
			<div title='列表'>
				<div class="grid" style="height:343px;">
					<div class="table-head">
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 20px;">&nbsp;</span></td>
									<td><span style="width:70px;">区间</span></td>
									<td><span style="width:70px;">成本</span></td>
									<td><span style="width:70px;">营业额</span></td>
									<td><span style="width:70px;">百分比</span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0">
							<tbody>
							<c:forEach var="item" items="${costIntervalList }" varStatus="status">
								<tr>
									<td align="center"><span style="width:20px;">${status.index+1}</span></td>
									<td><span style="width:70px;">${item.QUJIAN}</span></td>
									<td><span style="width:70px;text-align:right;">${item.COSTHEJI }</span></td>
									<td><span style="width:70px;text-align:right;">${item.INFASHENGE }</span></td>
									<td><span style="width:70px;text-align:right;">${item.BAIFENBI}</span></td>
								</tr>
							</c:forEach>
							</tbody>
						</table>					
					</div>
				</div>
			</div>
			<div title="图表">
				<div id="tubiao" class="grid" style="height:343px;">
				</div>
			</div>
		</div>		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
 		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script language="JavaScript" src="<%=path%>/Charts/FusionCharts.js"></script> 	
		<script type="text/javascript">
			$(document).ready(function(){
				//按钮快捷键
				focus() ;//页面获得焦点
			 	$(document).bind('keyup',function(e){
			 		if(e.keyCode==27){
			 			parent.$('.close').click();
			 		}
			 	});
			 	//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),65);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.tool').toolbar({
					items: [{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								parent.$('.close').click();
							}
						}
					]
				});
				$('#tt').tabs({   
					border:false,   
					  onSelect:function(title){   
						if(title == "图表"){
							loadChart();
						}
					  }   
				 });  
			});
			//图表展示
			function loadChart(){
				var itcode=$('#itcode').val();
	  	 		var bdat=$('#bdat').val();
	  	 		var edat=$('#edat').val();
	  	 		var onlycbk=$('#onlycbk').val();
				var chartSwf = "Doughnut2D.swf";
				var w = 960;
				var h = 350;
				//图表
				var chart = new FusionCharts("<%=path%>/Charts/"+chartSwf, "菜品成本区间分析", w, h);
				chart.setDataURL(escape("<%=path%>/CpLirunMingxiMisBoh/findXmlCostInterval.do?itcode="+itcode+"&bdat="+bdat+"&edat="+edat+"&onlycbk="+onlycbk));
				chart.render("tubiao");

			}
		</script>
	</body>
</html>