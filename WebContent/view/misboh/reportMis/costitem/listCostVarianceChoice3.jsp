<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title><fmt:message key="misboh_reportChengbenchayi"/></title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<style type="text/css">
		.text{
			font-size:12px,border:0px,line-height:20px,height:20px,padding:0px,*height:18px,*line-height:18px,_height:18px,_line-height:18px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 7%;
		}
		form .form-line .form-input{
			width: 16%;
		}
		.form-line .form-input input[type=text] , .form-line .form-input select{
			width:80%;
		}
	</style>
  </head>	
  <body>
  	<div class="tool" id="tool"></div>
  	<input id="firstLoad" type="hidden"/>
  	<input id="chooseSp_code" type="hidden"/>
  	<input id="chooseSp_name" type="hidden"/>
  	<input id="chooseSp_desc" type="hidden"/>
  	<input id="chooseUnit" type="hidden"/>
  	<input id="choosePositn" type="hidden"/>
  	<input id="choosePositndes" type="hidden"/>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
		<div class="form-label"><fmt:message key="startdate"/></div>
		<div class="form-input"><input type="text" id="bdat" name="bdat" class="Wdate text" value="${condition.bdat}" /></div>
		<div class="form-label"><fmt:message key="supplies_code"/></div>
		<div class="form-input">
			<input type="text" style="margin-bottom:4px;vertical-align:middle;" class="text" id="sp_code" name="sp_code" />
			<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
		</div>
		<div class="form-label"><fmt:message key="category"/></div>
		<div class="form-input">
			<input type="hidden" id="typ" name="typ"/>
			<input type="text"  id="typdes" name="typdes" readonly="readonly" class="text"/>
			<img id="seachTyp" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_category"/>' />
		</div>
	</div>
	<div class="form-line">
		<div class="form-label"><fmt:message key="enddate"/></div>
		<div class="form-input"><input type="text" id="edat" name="edat" class="Wdate text" value="${condition.edat}" /></div>
		<div class="form-label"><fmt:message key="sector"/></div>
		<div class="form-input">
			<input type="hidden" id="code" name="code" value="${condition.code }"/>
			<input type="text" id="deptName" value="${condition.des }"/>
			<img id="seachDept" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="misboh_DeclareGoodsGuide_dept" />' />
		</div>
<%-- 		<div class="form-label"><fmt:message key="bigClass"/></div> --%>
<!-- 		<div class="form-input"> -->
<%-- 			<select id="grptyp" name="grptyp" url="<%=path %>/grpTyp/findAllGrpTyp.do" class="select" select="true"></select> --%>
<!-- 		</div>			 -->
<%-- 		<div class="form-label"><fmt:message key="middleClass"/></div> --%>
<!-- 		<div class="form-input"> -->
<%-- 			<select id="grp" name="grp" url="<%=path %>/grpTyp/findAllGrp.do" class="select" select="true"></select> --%>
<!-- 		</div> -->
<%-- 		<div class="form-label"><fmt:message key="smallClass"/></div> --%>
<!-- 		<div class="form-input"> -->
<%-- 			<select id="typ" name="typ" url="<%=path %>/grpTyp/findAllTyp.do" class="select" select="true"></select> --%>
<!-- 		</div> -->
	</div>
		<input type="hidden" id="reportName" value="MISCostVarianceChoice3"/>
		<input type="hidden" id="serviceName" name="serviceName" value="com.choice.misboh.service.reportMis.WzChengbenChayiMisBohService"/>
		<input type="hidden" id="excelUrl" value="<%=path%>/WzChengbenChayiMisBoh/exportReport.do"/>
		<input type="hidden" id="printUrl" value="<%=path%>/WzChengbenChayiMisBoh/printReport.do"/>
		<input type="hidden" id="dataUrl" value="<%=path%>/WzChengbenChayiMisBoh/findCostVarianceChoice3.do"/>
</form>
 	<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
//   	 		$("#grptyp").htmlUtils("select");
//   	 		$("#grp").htmlUtils("select");
//   	 		$("#typ").htmlUtils("select");
  	 		if($('#bdat').val() == '')
  	 			$("#bdat,#edat").htmlUtils("setDate","now");
  	 		
  	 		/*弹出树*/
			$('#seachDept').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#code').val();
					var defaultName = $('#deptName').val();
					//alert(defaultCode+"==="+defaultName);
					var offset = getOffset('bdat');
					top.cust('<fmt:message key="please_select_positions" />',encodeURI('<%=path%>/misbohcommon/findPositnSuper.do?typn=7&iffirm=0&mold=oneTmany&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deptName'),$('#code'),'760','520','isNull');
				}
			});
  	 		
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/misbohcommon/selectSupplyLeft.do?sta=all&positn=1&defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
			
			$('#seachTyp').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#typ').val();
					var offset = getOffset('bdat');
					top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/misbohcommon/selectMoreGrpTyp.do?brackets=Y&defaultCode='+defaultCode),offset,$('#typdes'),$('#typ'),'320','460','isNull');
				}
			});
			$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
		 	});
			initTable(true);
  	 		$("#firstLoad").val("true");
  	 		$("#bdat,#edat").focus(function(){
  	 			new WdatePicker();
  	 		});
  	 		$(".panel-tool").remove();
  	 	});
  	 	// 加载表格
  	 	function initTable(rebuild){
  	 		var excelUrl = $("#excelUrl").val();
  	 		var printUrl = $("#printUrl").val();
  	 		var reportName = $("#reportName").val();
  	 		var dataUrl = $("#dataUrl").val();
  	 		var grid = $("#datagrid");
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp : true,//导出时从前台取列名
  	 			verifyFun:function(){
  	 				//验证查询条件 不满足return false;
	 				return true;
  	 			},
  	 			excelUrl:excelUrl+'?reportName='+reportName,
  	 			printUrl:printUrl+'?reportName='+reportName,
  	 			colsChooseUrl:'<%=path%>/WzChengbenChayiMisBoh/toColumnsChoose.do?reportName='+reportName,
  	 			toolbar:['search',excel,'option',hjmx,wzmxz,'exit']
  	 		});
  	 		
  	 		if(rebuild || grid.css('display') != 'none'){
 	  			firstLoad = true;//重置第一次加载标志
	  	 		builtTable({
	  	 			headUrl:"<%=path%>/WzChengbenChayiMisBoh/findHeaders.do?reportName="+reportName,
	  	 			dataUrl:dataUrl,
	  	 			title:'<fmt:message key="misboh_reportChengbenchayi"/>',
	  	 			grid:grid,
	  	 			alignCols:['qccnt','qcamt','rkcnt','rkamt','dbrkcnt','dbrkamt','dbckcnt',		
	 		  	 		       'dbckamt','bsckcnt','bsckamt','sjckcnt','sjckamt',
	 		  	 		       'llcnt','llamt','sjjccnt','sjjcamt','cycnt','cyamt',
	                            'cyl','pdckcnt','pdckamt'],//右对齐
	  	 			numCols:['qccnt','qcamt','rkcnt','rkamt','dbrkcnt','dbrkamt','dbckcnt',		
	 		  	 		       'dbckamt','bsckcnt','bsckamt','sjckcnt','sjckamt',
	 		  	 		       'llcnt','llamt','sjjccnt','sjjcamt','cycnt','cyamt',
	                            'cyl','pdckcnt','pdckamt'],//右对齐
	  	 			decimalDigitF:2,//几位小数
	  	 			onClickRow:function(a,b){
						$('#chooseSp_code').val(b.SP_CODE);
						$('#chooseSp_name').val(b.SP_NAME);
						$('#chooseSp_desc').val(b.SP_DESC);
						$('#chooseUnit').val(b.UNIT);
						$('#choosePositn').val(b.CODE);
						$('#choosePositndes').val(b.DES);
					}
	  	 		});
 	  		}
  	 		
  	 		//列选择后页面重新加载
  	  		function pageReload(){
  	  			closeColChooseWin();
  	  			initTab(false);
  	  		}
  	 	}
  	 	var excel = {
  				text: 'Excel',
  				title: 'Excel',
  				icon: {
  					url: '<%=path%>/image/Button/op_owner.gif',
  					position: ['0px','0px']
  				},
  				handler: function(){
  					$('#queryForm').attr('action','<%=path%>/WzChengbenChayiMisBoh/exportCostVarianceChoice3.do');
					$('#queryForm').submit();
  				}
  			};
  	 	
  	 		var hjmx = {
  				text: '核减明细',
  				title: '核减明细',
  				icon: {
  					url: '<%=path%>/image/Button/op_owner.gif',
  					position: ['0px','0px']
  				},
  				handler: function(){
  					var sp_code = $('#chooseSp_code').val();
  					if(sp_code == ''){
  						alert('请选择要查看的物资。');
  						return;
  					}
  					var positn = $('#choosePositn').val();
  					var positndes = $('#choosePositndes').val();
  					var params = {"bdat":$("#bdat").val(),"edat":$("#edat").val(),"positn":positn,"positndes":positndes,sp_code:sp_code};
  					openTag("hjMingxi","<fmt:message key="Check_list" />","<%=path%>/hjMingxiChaxunMisBoh/toHeJian.do",params);
  				}
  			};
  	 		
  	 		var wzmxz = {
  				text: '物资明细账',
  				title: '物资明细账',
  				icon: {
  					url: '<%=path%>/image/Button/op_owner.gif',
  					position: ['0px','0px']
  				},
  				handler: function(){
  					var sp_code = $('#chooseSp_code').val();
  					if(sp_code == ''){
  						alert('请选择要查看的物资。');
  						return;
  					}
  					var positn = $('#choosePositn').val();
  					var positndes = $('#choosePositndes').val();
  					var sp_name = $('#chooseSp_name').val();
  					var sp_desc = $('#chooseSp_desc').val();
  					var unit = $('#chooseUnit').val();
  					var params = {"bdat":$("#bdat").val(),"edat":$("#edat").val(),"positn":positn,"positndes":positndes,
  							sp_code:sp_code,sp_name:sp_name,sp_desc:sp_desc,unit:unit,withdat:1};
  					openTag("wzMingxizhang","<fmt:message key="material_ledger" />","<%=path%>/WzMingxiZhangMisBoh/toSupplyDetailsInfo.do",params);
  				}
  			};

  	 </script>
  </body>
</html>
