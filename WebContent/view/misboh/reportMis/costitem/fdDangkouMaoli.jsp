<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>分店档口毛利</title>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	     	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
			<style type="text/css">
				.page{
					margin-bottom: 25px;
				}
				.datagrid-sort-icon{
					background:none; 
				}
				.panel-body{}
				.redtd{
					background:red;
				}
			</style>
  </head>	
  <body>
  	<div class="tool"></div>
  		<input id="firstLoad" type="hidden"/>
  		<input id="positn" type="hidden"/>
  		<input id="positndes" type="hidden"/>
  		<form id="queryForm" name="queryForm" method="post">
			<div class="form-line">
				<div class="form-label"><fmt:message key="startdate"/></div>
				<div class="form-input"><input type="text" autocomplete="off" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${costProfit.bdat }" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/></div>
				<div class="form-label"><fmt:message key="sector"/></div>
				<div class="form-input" style="width:205px;">
					<input style="width:95px;" type="hidden" id="dept" name="dept"/>
					<input style="width:95px;" type="text" id="deptName"/>
					<img id="seachDept" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="misboh_DeclareGoodsGuide_dept" />' />
				</div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="enddate"/></div>
				<div class="form-input"><input type="text" autocomplete="off" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${costProfit.edat }" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/></div>
				<div class="form-label">
					<input type="radio" name="showtyp" value="2" <c:if test="${showtyp == 2 }"> checked="checked"</c:if>/><fmt:message key="week"/>
	                <input type="radio" name="showtyp" value="3" <c:if test="${showtyp == 3 }"> checked="checked"</c:if>/><fmt:message key="misboh_month"/>
				</div>
	            <div class="form-input">
<%-- 	                <input type="radio" name="showtyp" value="1" checked="checked"/><fmt:message key="misboh_day"/> --%>
	                <input type="radio" name="chengben" value="sj" <c:if test="${chengben == 'sj' }"> checked="checked"</c:if>/><fmt:message key="actual"/><fmt:message key="Cost_scm"/>
	                <input type="radio" name="chengben" value="ll" <c:if test="${chengben == 'll' }"> checked="checked"</c:if>/><fmt:message key="The_theory_of_cost"/>
	            </div>
			</div>
		</form>
		<div id="datagrid"></div>
		<input type="hidden" id="parentId"/>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	 
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
			//按钮快捷键
			focus() ;//页面获得焦点			
			$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							var form = $("#queryForm").find("*[name]");
							form = form.filter(function(index){
								var cur = form[index];
								if($(cur).attr("name")){
									if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
										if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
											if($("input[name='"+$(cur).attr("name")+"']:checked").length){
												params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
											}else{
												params[$(cur).attr("name")] = undefined;
											}
										}else{
											params[$(cur).attr("name")] = $(cur).val();
										}
									}
								}
								
							});
							
							var dept = $("#dept").val();
							var tableContent1 = {};
				  	 		//表头（多行），其中元素为columns
				  	 		head = [];
							$.ajax({url:"<%=path%>/fdDangkouMaoliMisBoh/findFdDangkouMaoliHeaders.do?dept="+dept,
			  	 				async:false,
			  	 				success:function(data){
			  	 					tableContent1 = data;
			  	 				}
			  	 			});
							//解析获取的数据
					 		var col11 = [];
					 		var col12 = [];
							for(var i in tableContent1){
				  	 			col11.push({title:tableContent1[i].des,colspan:3,align:'center'});
				  	 			col12.push({field:'A_'+tableContent1[i].code,title:'<fmt:message key="scm_sales"/>',width:60,align:'right'});
				  	 			col12.push({field:'C_'+tableContent1[i].code,title:'<fmt:message key="Cost_scm"/>',width:60,align:'right'});
				  	 			col12.push({field:'M_'+tableContent1[i].code,title:'<fmt:message key="Gross_profit"/>',width:60,align:'right'});
				  	 		}
							head.push(col11);
							head.push(col12);
// 							if (document.all){//ie
// 								$("#datagrid").datagrid("reload");
// 							}else{
								$("#datagrid").datagrid({columns:head});
// 							}
								$('.datagrid-header').find('.datagrid-cell').click(function(){
									$('.datagrid-header').find('.datagrid-cell').removeClass('redtd');
					  	 			$(this).addClass('redtd');
					  	 			var code = $(this).parents('td').attr('field');
					  	 			code = code.substring(2,code.length);
					  	 			$('#positn').val(code);
					  	 			var index = $(this).closest('td').index();
					  	 			index = Math.round((index - 1)/3);
					  	 			var des = $(this).parents('tr').prev('tr').find('td:eq('+index+')').text();
					  	 			$('#positndes').val(des);
					  	 		});
						}
					},{
						text: 'Excel',
						title: 'Excel',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$('#queryForm').attr('action',"<%=path%>/fdDangkouMaoliMisBoh/exportFirmDangKouProfit.do");
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="scm_wzcbcy" />',
 						title: '<fmt:message key="scm_wzcbcy" />',
 						useable:true,
 						icon: {
 							url: '<%=path%>/image/Button/op_owner.gif',
 							position: ['-100px','-60px']
 						},
 						handler: function(){
 							toWzChengbenChayi();
 						}
 					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							$(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').click();
						}
					}
				]
			});
			var tableHeight = $(".main",top.document).parent("div[region='center']").height() - $(".tab-control",top.document).height() - $(".tool").height() - $("#queryForm").height();
// 			$("#bdat,#edat").htmlUtils("setDate","now");
  	 		//收集form表单数据的对象
  	 		var params = {};
  	 		//Controller传来的map对象，包含所需要显示的报表所有列的dictColumns对象，和需要固定在左侧的col的index（用，分割）
  	 		var tableContent = {};
  	 		//表头（多行），其中元素为columns
  	 		var head = [];
  	 		//需要固定在左侧的列的表头（单行）
  	 		var frozenHead = [];
  	 		//需要固定在左侧的列的表头（多行），元素为frozenHead
  	 		var frozenColumns = [];
  	 		//ajax获取报表表头
  	 		$.ajax({url:"<%=path%>/fdDangkouMaoliMisBoh/findFdDangkouMaoliHeaders.do",
  	 				async:false,
  	 				success:function(data){
  	 					tableContent = data;
  	 				}
  	 			});
  	 		//解析获取的数据
	 		var col1 = [];
	 		var col2 = [];
			for(var i in tableContent){
				col1.push({title:tableContent[i].des,colspan:3,align:'center'});
  	 			col2.push({field:'A_'+tableContent[i].code,title:'<fmt:message key="scm_sales"/>',width:60,align:'right'});
  	 			col2.push({field:'C_'+tableContent[i].code,title:'<fmt:message key="Cost_scm"/>',width:60,align:'right'});
  	 			col2.push({field:'M_'+tableContent[i].code,title:'<fmt:message key="Gross_profit"/>',width:60,align:'right'});
  	 		}
  	 		frozenColumns.push({field:'WEEKNO',title:'<fmt:message key="Weekly" />',width:80,align:'left'});
  	 		frozenColumns.push({field:'DES',title:'<fmt:message key="Starting_date" />',width:120,align:'left'});
			head.push(col1);
			head.push(col2);
  	 		frozenHead.push(frozenColumns);
  	 		
  	 		//生成报表数据表格
  	 		$("#datagrid").datagrid({
  	 			title:'<fmt:message key="scm_fddkml" />',
  	 			width:'100%',
  	 			height:tableHeight,
  	 			nowrap: true,
				striped: true,
				singleSelect:true,
				collapsible:true,
				//对从服务器获取的数据进行解析格式化
	 			dataFilter:function(data,type){
	 				var rs = eval("("+data+")");
	 				var modifyRows = [];
	 				var modifyFooter = [];
	 				var rows = rs.rows;
	 				var footer = rs.footer;
	 				if(!rows || rows.length <= 0)$('#datagrid').datagrid('loadData',{total:0,rows:[],footer:[]});
	 				for(var i in rows){
	 					var curRow = {};
	 					curRow["WEEKNO"] = eval("rows["+i+"].WEEKNO");
	 					curRow["DES"] = eval("rows["+i+"].DES");
	 					for(var j in tableContent){
	 						try{
	 							var currentCol = tableContent[j].code;
	 							curRow["A_"+currentCol] =  eval("rows["+i+"].A_"+currentCol)?eval("rows["+i+"].A_"+currentCol).toFixed(2):'0.00';
	 							curRow["C_"+currentCol] =  eval("rows["+i+"].C_"+currentCol)?eval("rows["+i+"].C_"+currentCol).toFixed(2):'0.00';
	 							curRow["M_"+currentCol] =  eval("rows["+i+"].M_"+currentCol)?eval("rows["+i+"].M_"+currentCol).toFixed(2):'0.00';
	 						}catch(e){
	 							alert('Exception');
	 						}
	 					}
	 					modifyRows.push(curRow);
	 				}
	 				for(var i in footer){
	 					var curRow = {};
	 					curRow["WEEKNO"] = eval("footer["+i+"].WEEKNO");
	 					for(var j in tableContent){
	 						try{
	 							var currentCol = tableContent[j].code;
	 							curRow["A_"+currentCol] =  eval("footer["+i+"].A_"+currentCol)?eval("footer["+i+"].A_"+currentCol).toFixed(2):'0.00';
	 							curRow["C_"+currentCol] =  eval("footer["+i+"].C_"+currentCol)?eval("footer["+i+"].C_"+currentCol).toFixed(2):'0.00';
	 							curRow["M_"+currentCol] =  eval("footer["+i+"].M_"+currentCol)?eval("footer["+i+"].M_"+currentCol).toFixed(2):'0.00';
	 						}catch(e){
	 							alert('Exception');
	 						}
	 					}
	 					modifyFooter.push(curRow);
	 				}
	 				rs.rows = modifyRows;
	 				rs.footer = modifyFooter;
	 				//非固定列，第一列显示数据总行数
// 	 				rs.footer[0][columns[0].field] = rs.total;
	 				return $.toJSON(rs);
	 			},
				url:"<%=path%>/fdDangkouMaoliMisBoh/findDangKouProfit.do",
				remoteSort: true,
				//页码选择项
				pageList:[20,30,40,50],
				frozenColumns:frozenHead,
				columns:head,
				queryParams:params,
				showFooter:true,
				rowStyler:function(){
					return 'line-height:11px';
				},
				pagination:true,
				rownumbers:true,
				onBeforeLoad:function(){
					if(!$("#firstLoad").val())
						return false;
				}
  	 		});
  	 		$("#firstLoad").val("true");
  	 		
  	  	 	$(".panel-tool").remove();
  	 		
  	 		/*弹出树*/
			$('#seachDept').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#dept').val();
					var defaultName = $('#deptName').val();
					//alert(defaultCode+"==="+defaultName);
					var offset = getOffset('bdat');
					top.cust('<fmt:message key="please_select_positions" />',encodeURI('<%=path%>/misbohcommon/findPositnSuper.do?typn=7&iffirm=0&mold=oneTmany&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deptName'),$('#dept'),'760','520','isNull');
				}
			});
  	 		
			$('.datagrid-header').find('.datagrid-cell').click(function(){
				$('.datagrid-header').find('.datagrid-cell').removeClass('redtd');
  	 			$(this).addClass('redtd');
  	 			var code = $(this).parents('td').attr('field');
  	 			code = code.substring(2,code.length);
  	 			$('#positn').val(code);
  	 			var index = $(this).closest('td').index();
  	 			index = Math.round((index - 1)/3);
  	 			var des = $(this).parents('tr').prev('tr').find('td:eq('+index+')').text();
  	 			$('#positndes').val(des);
  	 		});
  	 		
  	 	});
  	 	
  	 	function toWzChengbenChayi(){
  	 		var positn = $('#positn').val();
  	 		if(positn == ''){
  	 			alert('<fmt:message key="Please_choose_a_position" />');
  	 			return;
  	 		}
			var params = {"bdat":$("#bdat").val(),"edat":$("#edat").val(),"code":positn,"des":$('#positndes').val()};
			openTag("wzChengbenChayi","<fmt:message key="scm_wzcbcy" />","<%=path%>/WzChengbenChayiMisBoh/toReport.do?reportName=MISCostVarianceChoice3",params);
  	 	}
  	 </script>
  </body>
</html>
