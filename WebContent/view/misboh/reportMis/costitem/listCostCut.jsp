<%@ page import="com.choice.orientationSys.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="reduction_in_the_theoretical_cost"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<style type="text/css">
			.moduleInfo {
				//background-color: #E1E1E1;
			}
			.leftFrame{
			width:20%;
			}
			.mainFrame{
			width:80%;
			}
		</style>
	</head>
	<body>
		<div class="leftFrame">
		<form id="listForm" method="post" action="">
		<input type="hidden" id="mis" name="mis" value="${mis}"/>
      		<div class="moduleInfo">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr >
							<td>&nbsp;&nbsp;&nbsp;
							 	<fmt:message key="startdate"/>：<input  style="width:100px;"  type="text" id="startdate" name="startdate"  class="Wdate text" value="<fmt:formatDate value="${queryChkout.dat}" pattern="yyyy-MM-dd"/>" />
							</td>
						</tr>
						<tr>
							<td>&nbsp;&nbsp;&nbsp;
								<fmt:message key="enddate"/>：<input  style="width:100px;" type="text" id="enddate" name="enddate"  class="Wdate text" value="<fmt:formatDate value="${queryChkout.dat}" pattern="yyyy-MM-dd"/>" />
							</td>
						</tr>
						<tr>  
							<td height="3"></td>
						</tr>   
						<tr>
							<td width="200">&nbsp;&nbsp;&nbsp;
					        	&nbsp;&nbsp;&nbsp;<fmt:message key="abbreviation"/>：<input type="text" value="${init}" id="init" name="init" style="width:100px;text-transform:uppercase;margin-bottom:5px;" onkeyup="changeSelect(this.value);" onkeydown="if(event.keyCode==32){return false;};"/>
					        </td>
					    </tr>
					</table>
			</div>
			<div class="grid" style="width: 100%;">
				<div class="table-head">
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span class="num" style="width: 16px;">&nbsp;</span></td>
								<td><span style="width:30px;text-align: center;">
									<input type="checkbox"  checked="checked" id="chkAll"/></span>
								</td>
								<td><span style="width:40px;"><fmt:message key="number"/>&nbsp;</span></td>
								<td><span style="width:108px;"><fmt:message key="branches_or_positions"/>&nbsp;</span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="positn" items="${positnList}" varStatus="status">
								<tr>
									<td><span class="num" style="width: 16px;">${status.index+1}</span></td>
									<td><span style="width:30px;text-align: center;">
										<input type="checkbox" checked="checked" name="idList" id="chk_${positn.code}" value="${positn.code}"/></span>
									</td>
									<td><span title="${positn.code}" style="width:40px;">${positn.code}&nbsp;</span></td>
									<td><span title="${positn.des}" style="width:108px;">${positn.des}&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			</form>
		</div>
    	<div class="mainFrame">
			<iframe src="<%=path%>/costcut/table.do?mis=1" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
    	</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">	
		var spcode=null;
		$(document).ready(function(){
			//自动实现滚动条
			setElementHeight('.grid',['.moduleInfo'],'.leftFrame',1);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			changeTh();
			if ($("#mis").val()=="1") {
				$(".checkbox").attr("checked",true);
				$(".checkbox").attr("disabled",true);
			}
			//loadGrid();//  自动计算滚动条的js方法
			$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);

			//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     }
			     else
			     {
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			     }
			 });
			
			$('#startdate').bind('click',function(){
				new WdatePicker();
			});
			$('#enddate').bind('click',function(){
				new WdatePicker();
			});
		});
		
		//当缩写码的值发生改变时执行
		function changeSelect(e) {
			$.ajax({
				type: "POST",
				url: '<%=path%>/costcut/findByKeyAjax.do',
				data: "key="+$("#init").val(),
				dataType: 'json',
				success:function(positnList){
					$('.table-body').find('table').find('tbody').empty();
					for(var i=0;i<positnList.length;i++){
						$('.table-body').find('table').find('tbody').append('<tr><td class="num" style="width: 25px;"><span style="width:16px;">'
							+(i+1)+'&nbsp;</span></td><td><span style="width:25px; text-align: center;"><input type="checkbox" name="idList" id="chk_'
							+positnList[i].code+'" value="'+positnList[i].code+'" />&nbsp;</span></td><td title="'+positnList[i].code+'"><span style="width:40px;">'
							+positnList[i].code+'&nbsp;</span></td><td><span title="'+positnList[i].des+'" style="width:108px;">'
							+positnList[i].des+'&nbsp;</span></td></tr>');
					}
				},
				error: function(){
					alert('<fmt:message key="server_busy_please_try_again"/>');
					}
				});
			  }
		</script>
	</body>
</html>