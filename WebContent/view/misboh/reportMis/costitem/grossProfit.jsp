<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <title>毛利查询</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<style type="text/css">
		.text{
			font-size:12px,border:0px,line-height:20px,height:20px,padding:0px,*height:18px,*line-height:18px,_height:18px,_line-height:18px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 7%;
		}
		form .form-line .form-input{
			width: 16%;
		}
		.form-line .form-input input[type=text] , .form-line .form-input select{
			width:80%;
		}
	</style>
  </head>	
  <body>
  	<div class="tool" id="tool"></div>
  	<input id="firstLoad" type="hidden"/>
  	<form id="queryForm" name="queryForm" method="post">
				<div class="form-line">
					<div class="form-label"><fmt:message key="startdate"/></div>
					<div class="form-input"><input type="text" autocomplete="off" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/></div>
					<div class="form-label"><fmt:message key="enddate"/></div>
					<div class="form-input"><input type="text" autocomplete="off" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/></div>
				</div>
					<input type="hidden" id="reportName" value="GrossProfit"/>
					<input type="hidden" id="serviceName" name="serviceName" value="com.choice.misboh.service.reportMis.MlChaxunMisBohService"/>
					<input type="hidden" id="excelUrl" value="<%=path%>/MlChaxunMisBoh/exportReport.do"/>
					<input type="hidden" id="printUrl" value="<%=path%>/MlChaxunMisBoh/printReport.do"/>
					<input type="hidden" id="dataUrl" value="<%=path%>/MlChaxunMisBoh/findGrossProfit.do"/>
			</form>
 	<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
  	 <script type="text/javascript">
  		
  	 	$(document).ready(function(){
			$("#bdat,#edat").htmlUtils("setDate","now");
		 	$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
		 	});
  	 		initTable(true);
  	 		$("#firstLoad").val("true");  	 		
  	 		$(".panel-tool").remove();
  	 	});
  	 	
  		// 加载表格
  	 	function initTable(rebuild){
  	 		var excelUrl = $("#excelUrl").val();
  	 		var printUrl = $("#printUrl").val();
  	 		var reportName = $("#reportName").val();
  	 		var dataUrl = $("#dataUrl").val();
  	 		var grid = $("#datagrid");
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp : true,//导出时从前台取列名
  	 			verifyFun:function(){
  	 				//验证查询条件 不满足return false;
	 				return true;
  	 			},
  	 			excelUrl:excelUrl+'?reportName='+reportName,
  	 			printUrl:printUrl+'?reportName='+reportName,
  	 			colsChooseUrl:'<%=path%>/MlChaxunMisBoh/toColumnsChoose.do?reportName='+reportName,
  	 			toolbar:['search','excel','print','option','exit']
  	 		});
  	 		
  	 		if(rebuild || grid.css('display') != 'none'){
 	  			firstLoad = true;//重置第一次加载标志
	  	 		builtTable({
	  	 			headUrl:"<%=path%>/MlChaxunMisBoh/findHeaders.do?reportName="+reportName,
	  	 			dataUrl:dataUrl,
	  	 			title:'<fmt:message key="Sales_profit_analysis" />',
	  	 			grid:grid,
	  	 			alignCols:['sale_totalamt','true_cost','mao_rate','theory_cost','dif_cost','theory_mao_rate','dif_rate'],//右对齐
	  	 			numCols:['sale_totalamt','true_cost','mao_rate','theory_cost','dif_cost','theory_mao_rate','dif_rate'],//右对齐
	  	 			decimalDigitF:2,//几位小数
	  	 			pagination:false
	  	 		});
 	  		}
  		}
  	 		//列选择后页面重新加载
  	  		function pageReload(){
  	  			closeColChooseWin();
  	  			initTab(false);
  	  		}
  	 	
  	 </script>
  </body>
</html>
