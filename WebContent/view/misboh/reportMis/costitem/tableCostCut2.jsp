<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="subtract_the_theoretical_cost_data"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
	</head>	
	<body>
		<div class="tool">
		</div>
		<input type="hidden" name ="status"  id="status" value="${status}" />
		<input type="hidden" name ="date"  id="date" value="${date}" />
		<input type="hidden" name ="firm"  id="firm" value="${firm}" />
		
		<form action="" id="listForm" name="listForm" method="post">
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:110px;"><fmt:message key="branche"/></span></td>
								<td><span style="width:110px;"><fmt:message key="sector"/></span></td>
								<td><span style="width:50px;"><fmt:message key="coding"/></span></td>
								<td><span style="width:110px;"><fmt:message key="name"/></span></td>
								<td><span style="width:30px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:50px;"><fmt:message key="standard_price"/></span></td>
								<td><span style="width:50px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:50px;"><fmt:message key="amount"/></span></td>
								<td><span style="width:50px;"><fmt:message key="discount"/></span></td>
								<td><span style="width:50px;"><fmt:message key="service_charge"/></span></td>
								<td><span style="width:50px;"><fmt:message key="taxes"/></span></td>
								<td><span style="width:50px;"><fmt:message key="free_entry"/></span></td>
								<td><span style="width:50px;"><fmt:message key="package_deals"/></span></td>
								<td><span style="width:50px;"><fmt:message key="quantity"/>2</span></td>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</form>
			    
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/orderTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">

		//工具栏
			$(document).ready(function(){
				focus() ;//页面获得焦点
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit"/>').click();
			 		}
			 	});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="subtract_the_theoretical_cost_data"/>(<u>C</u>)',
							title: '<fmt:message key="subtract_the_theoretical_cost_data"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-20px']
							},
							handler: function(){
								auditChkout();
							}
						},{
							text: '<fmt:message key="subtract_situation_queries"/>(<u>F</u>)',
							title: '<fmt:message key="editing_materials_with_material_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								searchChkout();
								
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}]
					});
					//自动实现滚动条
					setElementHeight('.grid',['.tool'],$(document.body),0);	//计算.grid的高度
					setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
					loadGrid();//  自动计算滚动条的js方法
					
					if($('#status').val()=="ok") {
						alert('<fmt:message key="subtract_completed"/>！');
					}else if($('#status').val()=="no"){
						alert($('#firm').val()+$('#date').val()+':<fmt:message key="subtract_cannot_continue_please_dont_select_this_branch"/>！');
					}else if($('#status').val()=="nodata"){
						alert('<fmt:message key="the_current_conditions_no_data_needs_to_be_reduced"/>！');
					}
				});
			function searchChkout(){
				$('body').window({
					id: 'window_searchChkout',
					title: '<fmt:message key="subtract_dates_and_stores"/>',
					content: '<iframe id="searchChkoutFrame" frameborder="0" src="<%=path%>/costcut/search1.do"></iframe>',
					width: 400,
					height: '480px',
					confirmClose: false,
					draggable: true,
					isModal: true
				});
			}
			
			function auditChkout(){
				var checkboxList = parent.$('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 0){
					alert('<fmt:message key="please_select_one_branch"/>！');
					return;
				};
				if(!parent.$("#startdate").val()) {
					alert('<fmt:message key="please_select_the_startdate"/>！');
					return;
				};
				if(!parent.$("#enddate").val()) {
					alert('<fmt:message key="please_select_the_enddate"/>！');
					return;
				};
				if(parent.$("#startdate").val() > parent.$("#enddate").val()) {
					alert('<fmt:message key="startdate_littler_than_enddate"/>！');
					return;
				};
				var chkValue = [];
				checkboxList.filter(':checked').each(function(){
					chkValue.push($(this).val());
				});
				var action = "<%=path%>/costcut/excutecostcut.do?mis=2&firmid="+chkValue.join(',')+"&bdate="+parent.$("#startdate").val()+"&edate="+parent.$("#enddate").val();
				$('#listForm').attr('action',action);
				$('#listForm').submit();
			}
		</script>
	</body>
</html>