<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>菜品成本明细</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	</head>
	<body>
		<div class="tool">
		</div>
		<div class="easyui-tabs" fit="false" plain="true" style="height:392px;">
			<div title='主列表'>
				<div class="grid" style="height:343px;">
					<div class="table-head">
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 20px;">&nbsp;</span></td>
									<td><span style="width:90px;">物资编码</span></td>
									<td><span style="width:100px;">物资名称</span></td>
									<td><span style="width:70px;">规格</span></td>
									<td><span style="width:50px;">品牌</span></td>
									<td><span style="width:50px;">取料率</span></td>
									<td><span style="width:50px;">净用量</span></td>
									<td><span style="width:50px;">毛用量</span></td>
									<td><span style="width:50px;">成本单位</span></td>
									<td><span style="width:50px;">毛用量1</span></td>
									<td><span style="width:40px;">单位</span></td>
									<td><span style="width:40px;">单价</span></td>
									<td><span style="width:40px;">金额</span></td>
									<td><span style="width:40px;">数量2</span></td>
									<td><span style="width:40px;">单位2</span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0">
							<tbody>
							<c:forEach var="item" items="${costdtlmList }" varStatus="status">
								<tr>
									<td align="center"><span style="width:20px;">${status.index+1}</span></td>
									<td><span style="width:90px;">${item.SP_CODE }</span></td>
									<td><span style="width:100px;">${item.SP_NAME }</span></td>
									<td><span style="width:70px;">${item.SP_DESC }</span></td>
									<td><span style="width:50px;">${item.SP_MARK }</span></td>
									<td><span style="width:50px;text-align:right;">${item.EXRATE }</span></td>
									<td><span style="width:50px;text-align:right;">${item.EXCNT }</span></td>
									<td><span style="width:50px;text-align:right;">${item.CNT2 }</span></td>
									<td><span style="width:50px;">${item.UNIT2 }</span></td>
									<td><span style="width:50px;text-align:right;">${item.CNT }</span></td>
									<td><span style="width:40px;">${item.UNIT }</span></td>
									<td><span style="width:40px;text-align:right;">${item.SP_PRICE }</span></td>
									<td><span style="width:40px;text-align:right;">${item.TOTALAMT }</span></td>
									<td><span style="width:40px;text-align:right;">${item.CNT1 }</span></td>
									<td><span style="width:40px;">${item.UNIT1 }</span></td>
								</tr>
							</c:forEach>
							</tbody>
						</table>					
					</div>
				</div>
			</div>
			<div title="明细">
				<div class="grid" style="height:343px;">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 20px;">&nbsp;</span></td>
									<td><span style="width:90px;">物资编码</span></td>
									<td><span style="width:100px;">物资名称</span></td>
									<td><span style="width:70px;">规格</span></td>
									<td><span style="width:50px;">品牌</span></td>
									<td><span style="width:50px;">取料率</span></td>
									<td><span style="width:50px;">净用量</span></td>
									<td><span style="width:50px;">毛用量</span></td>
									<td><span style="width:50px;">成本单位</span></td>
									<td><span style="width:50px;">毛用量1</span></td>
									<td><span style="width:40px;">单位</span></td>
									<td><span style="width:40px;">单价</span></td>
									<td><span style="width:40px;">金额</span></td>
									<td><span style="width:40px;">数量2</span></td>
									<td><span style="width:40px;">单位2</span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0">
							<tbody>
							<c:forEach var="item" items="${costdtlList }" varStatus="status">
								<tr>
									<td align="center"><span style="width:20px;">${status.index+1}</span></td>
									<td><span style="width:90px;">${item.SP_CODE }</span></td>
									<td><span style="width:100px;">${item.SP_NAME }</span></td>
									<td><span style="width:70px;">${item.SP_DESC }</span></td>
									<td><span style="width:50px;">${item.SP_MARK }</span></td>
									<td><span style="width:50px;text-align:right;">${item.EXRATE }</span></td>
									<td><span style="width:50px;text-align:right;">${item.EXCNT }</span></td>
									<td><span style="width:50px;text-align:right;">${item.CNT2 }</span></td>
									<td><span style="width:50px;">${item.UNIT2 }</span></td>
									<td><span style="width:50px;text-align:right;">${item.CNT }</span></td>
									<td><span style="width:40px;">${item.UNIT }</span></td>
									<td><span style="width:40px;text-align:right;">${item.SP_PRICE }</span></td>
									<td><span style="width:40px;text-align:right;">${item.TOTALAMT }</span></td>
									<td><span style="width:40px;text-align:right;">${item.CNT1 }</span></td>
									<td><span style="width:40px;">${item.UNIT1 }</span></td>
								</tr>
							</c:forEach>
							</tbody>
						</table>					
					</div>
				</div>
			</div>
		</div>		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
 		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script> 		
		<script type="text/javascript">
			$(document).ready(function(){
				//按钮快捷键
				focus() ;//页面获得焦点
			 	$(document).bind('keyup',function(e){
			 		if(e.keyCode==27){
			 			parent.$('.close').click();
			 		}
			 	});
				$('.tool').toolbar({
					items: [{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								parent.$('.close').click();
							}
						}
					]
				});	
			});
		</script>
	</body>
</html>