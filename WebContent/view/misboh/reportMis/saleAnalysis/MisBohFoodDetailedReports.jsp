<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title><fmt:message key="retreat_food" /><fmt:message key="the_detail" /><fmt:message key="the_report" /></title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
		.leftDiv{
			float: left;
			height: 100%;
			width: 120px;
			padding-left: 20px;
			line-height: 30px;
			padding-top: 30px;
		}
		.chartdiv{
			height: 100%;
		}
		.form-label2{
			width: 40px;
			height: 25px;
			position: relative;
			margin: 0;
			float: left;
			text-align: right;
			vertical-align: middle;
			line-height: 25px;
			margin-right: 5px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 6.5%;
		}
		form .form-line .form-input{
			width: 13%;
		}
		form .form-line .form-input input[type=text]{
			width: 85%;
			margin-top:3px;
		}
	</style>
  </head>	
  <body>
  	<%@ include file="../share/permission.jsp"%>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label"><fmt:message key="startdate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" />
			</div>
			<div class="form-label"><fmt:message key="enddate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd"/>" />
			</div>
			<div class="form-label"><fmt:message key="bill_no" /></div>
			<div class="form-input">
				<input type="text" id="vbcode" name="vbcode" value="" />
				<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }" />
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="pubitem_name" /></div>
			<div class="form-input">
				<input type="text" id="vpname" name="vpname" value="" />
			</div>
			<div class="form-label"><fmt:message key="Operator" /></div>
			<div class="form-input">
				<input type="text" id="vename" name="vename" value="" />
			</div>
			<div class="form-label"><fmt:message key="authorizer" /></div>
			<div class="form-input">
				<input type="text" id="vjename" name="vjename" value="" />
			</div>
		</div>
	</form>
	
	<input type="hidden" id="reportName" value="tcmxbb"/>
	<input type="hidden" id="excelUrl" value="<%=path%>/misSaleAnalysis/exportReport.do"/>
	<input type="hidden" id="printUrl" value="<%=path%>/misSaleAnalysis/printReport.do"/>
	<input type="hidden" id="dataUrl" value="<%=path%>/misSaleAnalysis/queryTcmxbb.do"/>
	<input type="hidden" id="title" value="<fmt:message key="retreat_food" /><fmt:message key="the_detail" />"/>
	<div id="datagrid"></div>
	
	<script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript">
		var tab;
  	 	$(document).ready(function(){
			//默认<fmt:message key="time" />
  	 		$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
  	 		$("#bdat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
		    });
		    $("#edat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
		    });
 		
			//初始化<fmt:message key="selected" />标签<fmt:message key="content" />
			initTab(false); 	 		
  	 	});
  	 	
  		//初始化<fmt:message key="selected" />标签<fmt:message key="content" />
  	 	function initTab(rebuild){
  	 		var excelUrl = $("#excelUrl").val();
  	 		var printUrl = $("#printUrl").val();
  	 		var reportName = $("#reportName").val();
  	 		var dataUrl = $("#dataUrl").val();
  	 		var title = $("#title").val();
  	 		var grid = $("#datagrid");

  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp:true,
  	 			excelUrl:excelUrl+'?reportName='+reportName,
  	 			printUrl:printUrl+'?reportName='+reportName,
  	 			colsChooseUrl:'<%=path%>/misSaleAnalysis/toColumnsChoose.do?reportName='+reportName,
  	 			toolbar:['search','excel','exit'],
  	 			searchFun:function(){
  	 				$('#datagrid').datagrid('load',getParam($("#queryForm")));
  	 			}
  	 		});
  	 		if(rebuild || grid.css('display') != 'none'){
  	  			firstLoad = true;//重置第一<fmt:message key="secondary" />加载标志
		  	 	builtTable({
		  	 		dataUrl:dataUrl,
		  	 		title:title,
		  	 		grid:grid,
		  	 		createHeader:function(data,head,frozenHead){
		  	 			var col1 = [];
		  	 			col1.push({field:"DWORKDATE",width:90,title:'<fmt:message key="date" />',align:'left'});
			  	 		col1.push({field:"DIVISION",width:100,title:'<fmt:message key="pubitem" /><fmt:message key="middleClass" />',align:'left'});
			  	 		col1.push({field:"SUBDIVISION",width:100,title:'<fmt:message key="pubitem" /><fmt:message key="smallClass" />',align:'left'});
			  	 		col1.push({field:"VBCODE",width:150,title:'<fmt:message key="In_a_single_number" />',align:'left'});
						col1.push({field:"VPNAME",width:130,title:'<fmt:message key="pubitem_name" />',align:'left'});
			   			col1.push({field:"RNUM",width:100,title:'<fmt:message key="retreat_food" /><fmt:message key="quantity" />',align:'right'});
			  	 		col1.push({field:"RMONEY",width:100,title:'<fmt:message key="retreat_food" /><fmt:message key="amount" />',align:'right'});
			  	 		col1.push({field:"RREASON",width:100,title:'<fmt:message key="reason_for_withdrawal_vegetables" />',align:'left'});
			  	 		col1.push({field:"ORTIME",width:100,title:'<fmt:message key="waste" /><fmt:message key="time" />',align:'left'});
// 			  	 		col1.push({field:"VENAME",width:100,title:'<fmt:message key="Operator" />',align:'left'});
			  	 		col1.push({field:"VJENAME",width:100,title:'<fmt:message key="authorizer" />',align:'left'});
		  	 			head.push(col1);
		  	 		}
		  	 	});
  	  		}
  	 	}
  	 	
  		//列<fmt:message key="select1" />后页面重新加载
  		function pageReload(){
  			closeColChooseWin();
  			initTab(true);
  		}
  	 </script>
  </body>
</html>