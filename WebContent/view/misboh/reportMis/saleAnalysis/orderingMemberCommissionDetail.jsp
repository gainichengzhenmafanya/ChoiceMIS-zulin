<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title><fmt:message key="bill_detail_analysis" /></title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
		.leftDiv{
			float: left;
			height: 100%;
			width: 120px;
			padding-left: 20px;
			line-height: 30px;
			padding-top: 30px;
		}
		.chartdiv{
			height: 100%;
		}
		.form-label2{
			width: 40px;
			height: 25px;
			position: relative;
			margin: 0;
			float: left;
			text-align: right;
			vertical-align: middle;
			line-height: 25px;
			margin-right: 5px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 6.5%;
		}
		form .form-line .form-input{
			width: 13%;
		}
		form .form-line .form-input input[type=text]{
			width: 85%;
		}
	</style>
  </head>	
  <body>
  	<%@ include file="../share/permission.jsp"%>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label"><fmt:message key="startdate" /></div>
			<div class="form-input">
				<input type="text" id="bdat" name="bdat" class="text" value="${bdat}" readonly="readonly"/>
			</div>
			<div class="form-label"><fmt:message key="enddate" /></div>
			<div class="form-input">
				<input type="text" id="edat" name="edat" class="text" value="${bdat}" readonly="readonly"/>
			</div>
			<div class="form-label"><fmt:message key="choose_food_staff" /><fmt:message key="coding" /></div>
			<div class="form-input">
				<input type="text" id="vcode" name="vcode" class="text" value="${vcode }" readonly="readonly" />
			</div>
		</div>
		<input type="hidden" id="iflag" name="iflag" value='${iflag }'/>
	</form>
	<input type="hidden" id="reportName" value="dcytcmx"/>
	<input type="hidden" id="excelUrl" value="<%=path%>/misSaleAnalysis/exportReport.do"/>
	<input type="hidden" id="printUrl" value="<%=path%>/misSaleAnalysis/printReport.do"/>
	<input type="hidden" id="dataUrl" value="<%=path%>/misSaleAnalysis/queryOrderingMemberCommissionDetail.do"/>
	<input type="hidden" id="title" value='<fmt:message key="Order_the_royalty_schedule" />'/>
	<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 var code = "";
  	 	$(document).ready(function(){
  	 		//默认<fmt:message key="time" />
//   	 	$("#bdat,#edat").htmlUtils("setDate","now");
   	 		
// 			$("#bdat").click(function(){
// 	  			new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});
// 	  		});
// 	  		$("#edat").click(function(){
// 	  			new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});
// 	  		});
  	 		
			//初始化<fmt:message key="selected" />标签<fmt:message key="content" />
			initTab(false);
            initone();
          //账单查询跳转到此
  	 		if('${vcode}'){
  	 			debugger;
  	 			$('#bdat').val('${bdat}');
  	 			$('#edat').val('${edat}');
  	 			$('#vcode').val('${vcode}');
  	 			$('#datagrid').datagrid('load',getParam($("#queryForm")));
  	 		}
//   	 		setElementHeight('.gridShow',['.tool'],$(document.body),500);	//计算.grid的高度
// 			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
  	 	});
  	 	
  		//初始化<fmt:message key="selected" />标签<fmt:message key="content" />
  	 	function initTab(rebuild){
  	 		var excelUrl = $("#excelUrl").val();
  	 		var printUrl = $("#printUrl").val();
  	 		var reportName = $("#reportName").val();
  	 		var dataUrl = $("#dataUrl").val();
  	 		var title = $("#title").val();
  	 		var grid = $("#datagrid");
  	 		
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			width:'96%',
  	 			height:'100%',
  	 			exportTyp:true,
  	 			//confirmClose:false,
  	 			excelUrl:excelUrl+'?reportName='+reportName,
  	 			printUrl:printUrl+'?reportName='+reportName,
  	 			colsChooseUrl:'<%=path%>/misSaleAnalysis/toColumnsChoose.do?reportName='+reportName,
  	 			toolbar:['excel','exit'],
  	 			searchFun:function(){
  	 				$('#datagrid').datagrid('load',getParam($("#queryForm")));
  	 			}
  	 		});
  	 		
  	 		if(rebuild || grid.css('display') != 'none'){
  	  				firstLoad = true;//重置第一<fmt:message key="secondary" />加载标志
		  	 		builtTable({
		  	 			dataUrl:dataUrl,
		  	 			title:title,
		  	 			grid:grid,
// 		  	 			height:'500px',
		  	 			settimeout:1,
		  	 			createHeader:function(data,head,frozenHead){
	  	  	 				var col1 = [];
	  		  	 			col1.push({field:"DWORKDATE",width:100,title:'<fmt:message key="date" />',align:'left'});
	  		  	 			col1.push({field:"VBCODE",width:170,title:'<fmt:message key="bill_no" />',align:'left'});
	  		  	 			col1.push({field:"VPCODE",width:100,title:'<fmt:message key="scm_pubitem_code" />',align:'left'});
	  		  	 			col1.push({field:"VPNAME",width:100,title:'<fmt:message key="scm_pubitem_name" />',align:'left'});
		  	 				col1.push({field:"NCOUNT",width:80,title:'<fmt:message key="sell_num" />',rowspan:1,colspan:1,align:'right'});
		  	 				col1.push({field:"NMONEY",width:80,title:'<fmt:message key="sell_amt" />',rowspan:1,colspan:1,align:'right'});
		  	 				col1.push({field:"TCMONEY",width:80,title:'<fmt:message key="commission" /><fmt:message key="amount" />',rowspan:1,colspan:1,align:'right'});
		  	 				col1.push({field:"COMMTYPE",width:100,title:'<fmt:message key="commission" /><fmt:message key="way" />',align:'left'});
		  	 				head.push(col1);
	  	  	 			}
		  	 		});
  	  			}
  	 	}
  		//列<fmt:message key="select1" />后页面重新加载
  		function pageReload(){
  			closeColChooseWin();
  			initTab(true);
	  		//重新<fmt:message key="select" />
  			$("#datagrid").datagrid('reload');
  		}
  	 </script>
  </body>
</html>