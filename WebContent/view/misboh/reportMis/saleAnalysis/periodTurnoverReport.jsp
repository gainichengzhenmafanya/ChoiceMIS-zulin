<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>时段销售分析</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
		
	</style>
	<%@ include file="../share/permission.jsp"%>
  </head>	
  <body>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label"><fmt:message key="startdate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text"
					   value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" />
			</div>
			<div class="form-label" style="width:70px;"><fmt:message key="enddate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text"
					   value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" />
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="starttime" /></div>
			<div class="form-input">
				<input type="text" id="binterval" name="binterval" class="text" value="00:00" 
					   style="width:50px;margin-top:1px;" onblur="isTime(this)"/>
				<span style="color: red;margin-left:6px;">(<fmt:message key="format_for" />08:00)</span>
			</div>
			<div class="form-label" style="width:70px;"><fmt:message key="endtime" /></div>
			<div class="form-input">
				<input type="text" id="einterval" name="einterval" class="text" value="24:00" 
					   style="width:50px;margin-top:1px;" onblur="isTime(this)"/>
				<span style="color: red;margin-left:6px;">(<fmt:message key="format_for" />10:00)</span>
			</div>
			<div class="form-label"><fmt:message key="the_time_interval" /></div>
			<div class="form-input">
				<input type="text" id="interval" name="interval" class="text" style="margin-top:0px;" value="30"
					   onkeyup="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}" 
					   onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}" />
				<fmt:message key="minutes" />
			</div>
		</div>
		<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }"/>
	</form>
	<input type="hidden" id="reportName" value="periodTurnoverReport"/>
	<input type="hidden" id="excelUrl" value="<%=path%>/misSaleAnalysis/exportReport.do"/>
	<input type="hidden" id="dataUrl" value="<%=path%>/misSaleAnalysis/queryPeriodTurnoverReport.do"/>
	<input type="hidden" id="decimal" value="2" />
	<input type="hidden" id="title" value="<fmt:message key="turnover_time" />"/>
	<div id="datagrid" ></div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
		 	//默认<fmt:message key="time" />
		 	$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
  	 		$("#bdat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
		    });
		    $("#edat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
		    });
  	  		
  	  		initTab(false);
  	 	});
  	 	
  	 	function checkTime(vals){
			var start = $("#binterval").val();
			var end = $("#einterval").val();
			
			if (start > end) {
				alert("<fmt:message key='starttime' /><fmt:message key='can_not_be_greater_than' /><fmt:message key='endtime' />!");
				vals.value="";
				vals.focus();
			}
  	 	}
  	 	
  	 	function isTime(vals){
			if(vals.value.match('^([0-1][0-9]|[2][0-3]):([0-5][0-9])$')){
				checkTime(vals);
				return true;
			}else if(vals.value.match('^([0-1][0-9]|[2][0-4])$')){
				checkTime(vals);
				vals.value=vals.value+":00";
				return true;
			}else if(vals.value.match('^([0-9])$')){
				checkTime(vals);
				vals.value="0"+vals.value+":00";
				return true;
			}else if(vals.value.match('^([2][4]):([0][0])$')){
				checkTime(vals);
				return true;
			}else{
				vals.value="";
				alert("<fmt:message key='time' /><fmt:message key='incorrect_format' />，<fmt:message key='please_re_enter' />!");
				vals.focus();
				return false;
			}
		}
  	 	
  		//初始化选中标签内容
  	 	function initTab(rebuild){
  	 		var excelUrl = $("#excelUrl").val();
  	 		var reportName = $("#reportName").val();
  	 		var dataUrl = $("#dataUrl").val();
  	 		var title = $("#title").val();
  	 		var grid = $("#datagrid");
  	 		
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp:true,
  	 			excelUrl:excelUrl+'?reportName='+reportName,
  	 			colsChooseUrl:'<%=path%>/misSaleAnalysis/toColumnsChoose.do?reportName='+reportName,
  	 			toolbar:['search','excel','exit'],
  	 			searchFun:function(){
  	 				$('#datagrid').datagrid('load',getParam($("#queryForm")));
  	 			}
  	 		});
  	 		if(rebuild || grid.css('display') != 'none'){
  	  				firstLoad = true;//重置第一次加载标志
		  	 		builtTable({
		  	 			dataUrl:dataUrl,
		  	 			title:title,
		  	 			grid:grid,
		  	 			decimalDigitF:$('#decimal').val(),
		  	 			alignCols:['tc','cnt','cntl','cntzb','cntlzb','amt','amtl','amtzb','amtlzb'],
		  	 			createHeader:function(data,head,frozenHead){
			  	 			var col1 = [];
// 			  	 			col1.push({field:"DWORKDATE",width:90,title:'<fmt:message key="date" />',align:'left'});
				  	 		col1.push({field:"DAT",width:100,title:'<fmt:message key="bucket" />',align:'left'});
				  	 		col1.push({field:"NSERVICETIME",width:115,title:'<fmt:message key="Service_time_statistics" />(<fmt:message key="minutes" />)',align:'right'});
				  	 		col1.push({field:"AVGTIME",width:140,title:'<fmt:message key="On_average" /><fmt:message key="Service_time_statistics" />(<fmt:message key="minutes" />)',align:'right'});
				  	 		col1.push({field:"NMONEY",width:90,title:'<fmt:message key="Total_operating_income" />',align:'right'});
							col1.push({field:"NYMONEY",width:90,title:'<fmt:message key="actual" /><fmt:message key="turnover" />',align:'right'});
				   			col1.push({field:"TC",width:90,title:'<fmt:message key="singular" />(TC)',align:'right'});
				   			col1.push({field:"JZCOUNT",width:90,title:'结账单数',align:'right'});
				  	 		col1.push({field:"AVGTC",width:80,title:'<fmt:message key="Tables_are" />',align:'right'});
				  	 		col1.push({field:"PAX",width:90,title:'<fmt:message key="visitor_numbers" />',align:'right'});
				  	 		col1.push({field:"PEOPLEAVG",width:80,title:'<fmt:message key="Per_capita" />',align:'right'});
				  	 		col1.push({field:"NMONEYLJ",width:90,title:'<fmt:message key="The_cumulative" /><fmt:message key="business_income" />',align:'right'});
				  	 		col1.push({field:"TCBFB",width:100,title:'<fmt:message key="open_the_numbers" /><fmt:message key="The_percentage" />(%)',align:'right'});
				  	 		col1.push({field:"AMOUNTBFB",width:100,title:'<fmt:message key="amount" /><fmt:message key="The_percentage" />(%)',align:'right'});
			  	 			head.push(col1);
			  	 		}
		  	 		});
  	  			}
  	 	}
  		//列选择后页面重新加载
  		function pageReload(){
  			closeColChooseWin();
  			initTab(true);
  		}
  	 </script>
  </body>
</html>