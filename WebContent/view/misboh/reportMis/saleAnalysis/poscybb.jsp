<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title><fmt:message key="pubitem" />销售明细表</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
		.leftDiv{
			float: left;
			height: 100%;
			width: 120px;
			padding-left: 20px;
			line-height: 30px;
			padding-top: 30px;
		}
		.chartdiv{
			height: 100%;
		}
		.form-label2{
			width: 40px;
			height: 25px;
			position: relative;
			margin: 0;
			float: left;
			text-align: right;
			vertical-align: middle;
			line-height: 25px;
			margin-right: 5px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 6.5%;
		}
		form .form-line .form-input{
			width: 13%;
		}
		form .form-line .form-input input[type=text]{
			width: 85%;
		}
	</style>
  </head>	
  <body>
  	<%@ include file="../share/permission.jsp"%>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label"><fmt:message key="startdate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text"  value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" />
			</div>
			<div class="form-label"><fmt:message key="enddate" /></div>
			<div class="form-input">	
				<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" />
			</div>
			<div class="form-label"><fmt:message key="pubitem" /></div>
			<div class="form-input">
				<input type="hidden" id="pk_pubitem" name="pk_pubitem" value="" />
				<input type="hidden" id="vpcode" name="vpcode"  value="" />
				<input type="text" id="vpname" name="vpname" class="text" value="" />
				<img id="searchPubitem" class="search" src="<%=path%>/image/themes/icons/search.png" style="margin-top: 0px;"  />
			</div>
		</div>
	</form>
	<input type="hidden" id="reportName" value="poscybb"/>
	<input type="hidden" id="excelUrl" value="<%=path%>/misSaleAnalysis/exportReport.do"/>
	<input type="hidden" id="dataUrl" value="<%=path%>/misSaleAnalysis/queryPoscybb.do"/>
	<input type="hidden" id="title" value="<fmt:message key="pubitem" /><fmt:message key="sales" /><fmt:message key="detail_query" />"/>
	<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
			$("#searchPubitem").click(function(){
				choosePubitem({
	 				basePath:'<%=path%>',
	 				width:600,
	 				domId:$("#pk_pubitem").val()
	 			});
			});
  	 		//默认<fmt:message key="time" />
  	 		$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
  	 		$("#bdat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
		    });
		    $("#edat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
		    });
  	 		
			//初始化<fmt:message key="selected" />标签<fmt:message key="content" />
			initTab(false);
  	 		initone();
  	 	});
  	 	
  	 	//<fmt:message key="pubitem" /><fmt:message key="select1" />回调函数
  	 	function setPubitem(data){
  	 		$("#pk_pubitem").val(data.code);
  	 		$("#vpname").val(data.show);
  	 		$("#vpcode").val(data.mod);
  	 	}
  	 	
  		//初始化<fmt:message key="selected" />标签<fmt:message key="content" />
  	 	function initTab(rebuild){
  	 		var excelUrl = $("#excelUrl").val();
  	 		var reportName = $("#reportName").val();
  	 		var dataUrl = $("#dataUrl").val();
  	 		var title = $("#title").val();
  	 		var grid = $("#datagrid");

  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp:true,
  	 			excelUrl:excelUrl+'?reportName='+reportName,
  	 			colsChooseUrl:'<%=path%>/misSaleAnalysis/toColumnsChoose.do?reportName='+reportName,
  	 			toolbar:['search','excel','exit'],
  	 			searchFun:function(){
  	 				$('#datagrid').datagrid('load',getParam($("#queryForm")));
  	 			}
  	 		});
  	 		if(rebuild || grid.css('display') != 'none'){
  				firstLoad = true;//重置第一<fmt:message key="secondary" />加载标志
	  	 		builtTable({
	  	 			headUrl:"<%=path%>/misSaleAnalysis/findHeaders.do?reportName="+reportName,
	  	 			dataUrl:dataUrl,
	  	 			title:title,
	  	 			grid:grid,
	  	 			createHeader:function(data,head,frozenHead){
	  	 				var col1 = [];
	  	 				frozenHead.push([{field:'MCNAME',width:75,title:'<fmt:message key="product" /><fmt:message key="category" />',align:'left'},
  	 				                 {field:'VPNAME',width:170,title:'<fmt:message key="product" /><fmt:message key="name" />',align:'left'}]);
	  	 				col1.push({field:"NYCOUNT",width:70,title:'<fmt:message key="sell_num" />',align:'right'});
	  	 				col1.push({field:"NFZCOUNT",width:70,title:'<fmt:message key="Auxiliarynumber" />',align:'right'});
	  	 				col1.push({field:"NPRICE",width:70,title:'<fmt:message key="unit_price" />',align:'right'});
	  	 				col1.push({field:"NMONEY",width:70,title:'<fmt:message key="amount" />',align:'right'});

	  	 				col1.push({field:"NTCOUNT",width:70,title:'<fmt:message key="retreat_food" /><fmt:message key="quantity" />',align:'right'});
	  	 				col1.push({field:"NTMONEY",width:70,title:'<fmt:message key="retreat_food" /><fmt:message key="amount" />',align:'right'});
	  	 				
	  	 				col1.push({field:"NZSCOUNT",width:70,title:'<fmt:message key="gift_num" />',align:'right'});
	  	 				col1.push({field:"NZSMONEY",width:70,title:'<fmt:message key="gift_amt" />',align:'right'});
	  	 				
	  	 				col1.push({field:"NZCOUNT",width:70,title:'<fmt:message key="discount" /><fmt:message key="quantity" />',align:'right'});
	  	 				col1.push({field:"NZMONEY",width:70,title:'<fmt:message key="discount_amt" />',align:'right'});
	  	 				col1.push({field:"NBZERO",width:70,title:'<fmt:message key="maLing" />',align:'right'});
	  	 				col1.push({field:"JAMT",width:70,title:'<fmt:message key="the_actual_turnover" />',align:'right'});
	  	 				
	  	 				head.push(col1);
	  	 			}
	  	 		});
  			}
  	 	}
  		//列<fmt:message key="select1" />后页面重新加载
  		function pageReload(){
  			closeColChooseWin();
  			initTab(true);
  			if($('#firmdes').val() && $('#firmid').val())	
  	  			$('#tabs').tabs('getSelected').panel("body").find("#datagrid").datagrid('reload');
			else {
				//alerterror('<fmt:message key="please_select_branche"/>');
			}
  		}
  		
  		 $("#selectPos").click(function(){
             var pk_store=$("#pk_store").val();
             if(pk_store=='' || pk_store.indexOf(',')!=-1){
            	 alerterror('<fmt:message key="Please_select_a_store"/>');return;
             }
			 var vposcode = $("#vposcode").val();
             selectStorePos({
                 basePath:"<%=path%>",
                 domId:$('#pk_storearear').val()==''?'selected':'vposcode',     //记住已<fmt:message key="select1" />的记录
                 single:false,     //<fmt:message key="whether" />单选
                 pk_store:pk_store,
                 isreport:'true',//是否报表，是的话，选择pos界面不显示新增按钮
                 height:300,
                 width:500
             },pk_store);
         });
  		 

		 function setStorePos(data){
            if(data==null){
                return;
            };
            $("#vposname").val(data.showname);
            $("#vposcode").val(data.show);
        }
  	 </script>
  </body>
</html>