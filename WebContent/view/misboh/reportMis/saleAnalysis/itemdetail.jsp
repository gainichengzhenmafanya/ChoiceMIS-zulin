<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>菜品销售明细查询表</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
	</style>
  </head>	
  <body>
  	<%@ include file="../share/permission.jsp"%>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label"><fmt:message key="startdate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text"  value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" />
			</div>
  			<div class="form-label"><fmt:message key="enddate" /></div>
			<div class="form-input">	
				<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" />
			</div>
			<div class="form-label"><fmt:message key="starttime" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="binterval" name="binterval" class="Wdate text" />
			</div>	
			<div class="form-label"><fmt:message key="endtime" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="einterval" name="einterval" class="Wdate text" />
			</div>	
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="flight" /></div>
			<div class="form-input">
				<select id="sft" name="sft" style="width: 133px;" class="select">
					<option value="0"><fmt:message key="all_day" /></option>
					<c:forEach var="sft" items="${sftList}">
						<option value="${sft.vcode}" <c:if test="${sft.vcode==condition.sft}"> selected="selected" </c:if> >${sft.vname}</option>
					</c:forEach>				
				</select>
			</div>	
			<div class="form-label"><fmt:message key="pubitem" /></div>
			<div class="form-input">
				<input type="hidden" id="pk_pubitem" name="pk_pubitem"  value="" />
				<input type="text" id="vpname" name="vpname" class="text" style="margin-top: 0px;" value=""/>
				<img id="searchPubitem" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" style="margin-top: 0px;"  />
			</div>
			<div class="form-label"><fmt:message key="bill" /><fmt:message key="type" /></div>
			<div class="form-input">
				<select id="vorclass" name="vorclass" style="width: 133px;" class="select">
					<option value="0"><fmt:message key="all" /></option>
					<option value="1" <c:if test="${1==condition.vorclass}"> selected="selected" </c:if> >1--整单OK</option>
					<option value="2" <c:if test="${2==condition.vorclass}"> selected="selected" </c:if> >2--整单撤销</option>
					<option value="3" <c:if test="${3==condition.vorclass}"> selected="selected" </c:if> >3--单品撤销</option>
					<option value="4" <c:if test="${4==condition.vorclass}"> selected="selected" </c:if> >4--废弃</option>
					<option value="5" <c:if test="${5==condition.vorclass}"> selected="selected" </c:if> >5--膳食</option>
					<option value="6" <c:if test="${6==condition.vorclass}"> selected="selected" </c:if> >6--折扣</option>
					<option value="7" <c:if test="${7==condition.vorclass}"> selected="selected" </c:if> >7--反结算</option>
				</select>
			</div>	
		</div>
		<div class="form-line">
  			<div class="form-label">账单号</div>
			<div class="form-input">	
				<input type="text" id="vbcode" name="vbcode" class="text" />
			</div>
			<div class="form-label">桌台号</div>
			<div class="form-input">	
				<input type="text" id="tbldes" name="tbldes" class="text" />
			</div>
			<div class="form-label">点菜员</div>
			<div class="form-input">	
				<input type="text" id="vlastemp" name="vlastemp" class="text" />
			</div>
		</div>
	</form>
	<input type="hidden" id="reportName" value="itemdetail"/>
	<input type="hidden" id="excelUrl" value="<%=path%>/misSaleAnalysis/exportReport.do"/>
	<input type="hidden" id="dataUrl" value="<%=path%>/misSaleAnalysis/queryItemDetail.do"/>
	<input type="hidden" id="title" value="菜品销售明细查询表"/>
	<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
			$("#searchPubitem").click(function(){
				choosePubitem({
	 				basePath:'<%=path%>',
	 				width:600,
	 				domId:$("#pk_pubitem").val()
	 			});
			});
  	 		//默认<fmt:message key="time" />
  	 		$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
  	 		$("#bdat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
		    });
		    $("#edat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
		    });
	  		$("#binterval").click(function(){
	  			new WdatePicker({maxDate:'#F{$dp.$D(\'einterval\')}',dateFmt:'HH:mm:ss'});
	  		});
	  		$("#einterval").click(function(){
	  			new WdatePicker({minDate:'#F{$dp.$D(\'binterval\')}',dateFmt:'HH:mm:ss'});
	  		});
	  		
			//初始化<fmt:message key="selected" />标签<fmt:message key="content" />
			initTab();
  	 	});
  	 	
  	 	//<fmt:message key="pubitem" /><fmt:message key="select1" />回调函数
  	 	function setPubitem(data){
	 		$("#pk_pubitem").val(data.code);
	 	  	$("#vpname").val(data.show);
  	 	}
  	 	
  		//初始化<fmt:message key="selected" />标签<fmt:message key="content" />
  	 	function initTab(){
  	 		var excelUrl = $("#excelUrl").val();
  	 		var reportName = $("#reportName").val();
  	 		var dataUrl = $("#dataUrl").val();
  	 		var title = $("#title").val();
  	 		var grid = $("#datagrid");
  	 		queryParams = getParam($("#queryForm"));
  	 		
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp:true,
  	 			excelUrl:excelUrl+'?reportName='+reportName,
  	 			toolbar:['search','excel','exit'],
  	 			searchFun:function(){
  	 				initTab();
  	 			}
  	 		});
  	 		
  	 		builtTable({
  	 			dataUrl:dataUrl,
  	 			title:title,
  	 			grid:grid,
  	 			createHeader:function(data,head,frozenHead){
  	 				var col1 = [];
  	 				frozenHead.push([{field:'VSCODE',width:70,title:'分店编码',align:'left',sortable:true},
  	 				                 {field:'VSNAME',width:120,title:'分店名称',align:'left',sortable:true},
  	 						         {field:'DWORKDATE',width:70,title:'营业日',align:'center',sortable:true},
  	 						   		 {field:'VPCODE',width:60,title:'菜品编码',align:'left',sortable:true},
  	 						         {field:'VPNAME',width:100,title:'菜品名称',align:'left',sortable:true},]);
  	 					
					col1.push({field:'NPRICE',width:60,title:'单价',align:'right',sortable:true});
					col1.push({field:'VUNITS',width:60,title:'单位',align:'left',sortable:true});
  	 				col1.push({field:'NCOUNT',width:60,title:'售卖数量',align:'right',sortable:true});
  	 				col1.push({field:'NMONEY',width:60,title:'售卖金额',align:'right',sortable:true});
  	 				col1.push({field:'NYCOUNT',width:60,title:'实售数量',align:'right',sortable:true});
  	 				col1.push({field:'NYMONEY',width:60,title:'实售金额',align:'right',sortable:true});
  	 				col1.push({field:'NZCOUNT',width:60,title:'优惠数量',align:'right',sortable:true});
  	 				col1.push({field:'NZMONEY',width:60,title:'优惠金额',align:'right',sortable:true});
  	 				col1.push({field:'NSVR',width:60,title:'服务费',align:'right',sortable:true});
  	 				col1.push({field:'NBZERO',width:60,title:'抹零',align:'right',sortable:true});
  	 				col1.push({field:'VBCODE',width:150,title:'账单号',align:'left',sortable:true});
  	 				col1.push({field:'VTABLENUM',width:50,title:'台位号',align:'left',sortable:true});
  	 				col1.push({field:'ICHANGETABLE',width:50,title:'班次',align:'left',sortable:true});
  	 				col1.push({field:'VORCLASS',width:60,title:'账单类型',align:'left',sortable:true});
  	 				col1.push({field:'VRECODE',width:60,title:'点菜员编码',align:'left',sortable:true});
  	 				col1.push({field:'VRENAME',width:70,title:'点菜员名称',align:'left',sortable:true});
  	 				col1.push({field:'VECODE',width:60,title:'收银员编号',align:'left',sortable:true});
  	 				col1.push({field:'VENAME',width:70,title:'收银员名称',align:'left',sortable:true});
  	 				col1.push({field:'VJECODE',width:60,title:'经理编号',align:'left',sortable:true});
  	 				col1.push({field:'VJENAME',width:70,title:'经理名称',align:'left',sortable:true});
 	 				col1.push({field:'DBRTIME',width:120,title:'点菜时间',align:'center',sortable:true});
 	 				col1.push({field:'VRETURNTIME',width:120,title:'退菜时间',align:'center',sortable:true});
 	 				col1.push({field:'DGIVETIME',width:120,title:'赠菜时间',align:'center',sortable:true});
 	 				col1.push({field:'DERTIME',width:120,title:'结算时间',align:'center',sortable:true});
  	 				col1.push({field:'DBTABLETIME',width:120,title:'开单时间',align:'center',sortable:true});
  	 				col1.push({field:'DETABLETIME',width:120,title:'封单时间',align:'center',sortable:true});
  	 				
  	 				head.push(col1);
  	 			}
  	 		});
  	 	}
  		//列<fmt:message key="select1" />后页面重新加载
  		function pageReload(){
  			initTab();
  	  		$('#tabs').tabs('getSelected').panel("body").find("#datagrid").datagrid('reload');
  		}
  	 </script>
  </body>
</html>