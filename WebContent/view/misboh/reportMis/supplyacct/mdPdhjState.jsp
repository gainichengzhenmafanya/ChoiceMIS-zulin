<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.tele.domain.Condition" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>门店盘点核减状态查询</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
	<style type="text/css">
		.select{
			margin-top: 2px;
		}	
	</style>
  </head>	
  <body>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label"><fmt:message key ="startdate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd" type="date"/>"/>
			</div>
			<div class="form-label"><fmt:message key="positions"/></div>
			<div class="form-input">
				<input type="text" name="positn_name" id="positn_name" readonly="readonly"/>
				<img id="seachDept" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
				<input type="hidden" id="positn" name="positn"/>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key ="enddate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd" type="date"/>"
				onfocus="var date=limitMonthDate();WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:date})"/>
			</div>
			<div class="form-label"></div>
			<div class="form-input">
				<input type="radio" id="bill" name="bill" value="0" checked="checked" /><fmt:message key="scm_pandian"/>
				<input type="radio" id="bill" name="bill" value="1" /><fmt:message key="Subtract"/>
			</div>
			<div class="form-label" style="margin-left:10px;width:220px;">
				<div style="float:left;width:18px;height:18px;background-color:red"></div><div style="float:left;width:40px;text-align:left;"><fmt:message key="Not_to_do"/></div>
				<div style="float:left;width:18px;height:18px;background-color:yellow"></div><div style="float:left;width:50px;text-align:left;"><fmt:message key="unchecked"/></div>
				<div style="float:left;width:18px;height:18px;background-color:green"></div><div style="float:left;width:30px;text-align:left;"><fmt:message key="Have_to_do"/></div>
			</div>
		</div>
	</form>
	<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script language="JavaScript" src="<%=path%>/Charts/FusionCharts.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		//选择分店
			$("#searchFirm").click(function(){
				selectFirm({
  	 				basePath:'<%=path%>'
   	 			});
			});
			/*弹出树*/
			$('#seachDept').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#positn').val();
					var defaultName = $('#positn_name').val();
					//alert(defaultCode+"==="+defaultName);
					var offset = getOffset('positn');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/misbohcommon/findPositnSuper.do?sta=all&typn=7&iffirm=1&mold=oneTmany&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positn_name'),$('#positn'),'760','520','isNull');
				}
			});
  	 		//默认时间
  	 		$("#bdat,#edat").htmlUtils("setDate","now");
			$("#bdat,#edat").click(function(){
	  			new WdatePicker();
	  		});
  	 		//生成工具栏
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			verifyFun:function(){
  	 				var edatMax = limitMonthDate();
					var edat = $('#edat').val();
					if(new Date(edatMax).getTime() < new Date(edat).getTime()){
						alert('开始日期，结束日期必须是同一个月份，请返回修改。');
						return false;
					}
					return true;
  	 			},
  	 			gridId:'datagrid',
  	 			exportTyp:true,
  	 			excelUrl:'',
  	 			toolbar:['search','exit'],
  	 			searchFun:function(grid,form){
  	 				getParam(form);
  	 				creatGrid();
  	 			}
  	 		});
  	 		creatGrid();
  	 	});
		function creatGrid(){
	 		//生成表格
	  		builtTable({
	 			headUrl:"<%=path%>/MdPdhjZhuangtaiChaxunMisBoh/findFreetimeHeader.do",
	 			dataUrl:"<%=path%>/MdPdhjZhuangtaiChaxunMisBoh/findMdPdhjState.do",
	 			title:'<fmt:message key ="Store_state_query" />',
	 			id:'datagrid',
	 			pagination:false,
	 			createHeader:function(data,head,frozenHead){
  	 				var colFirst = [];
  	 				var colFrozen = [];
  	 				colFrozen.push({field:'FIRMCODE',width:100,title:'<fmt:message key="branches_encoding" />'});
  	 				colFrozen.push({field:'FIRMDES',width:150,title:'<fmt:message key="branches_name" />'});
  	 				frozenHead.push(colFrozen);
  	 				for(var i in data){
  	 					colFirst.push({field:data[i],title:data[i],width:20,align:'right',styler:function(value){
  	 						if(value && value == 1){
  	 							return "color:green;background:green";
  	 						}else if(value == 0){
  	 							return "color:yellow;background:yellow";
  	 						}else{
  	 							return "color:red;background:red";
  	 						}
  	 							
  	 					}});
  	 				}
  	 				head.push(colFirst);
  	 			}
	 		});
		}
		//方法延时执行
		var delay = function(t,func){
			var self = this;
			if(self.curTime)
				clearTimeout(self.curTime);
			self.curTime = setTimeout(function(){
				func.apply(self);
				},t*1000);
		};
		//生成工具栏
		function builtToolBar(params){
			var form = $('#'+params.formId);//页面formid
			var grid = params.gridId ? $('#'+params.gridId) : params.grid;//表格所在div
			var basePath = params.basePath;
			var curtoolbar = params.toolbar;//需要的工具按钮,可能的值search,excel,print,option,exit
			var searchFun = params.searchFun; //自定义查询方法
			var verifyFun = params.verifyFun;
			var exportTyp = params.exportTyp;//excel导出时获取表头的方式，默认为从数据库查询。设置为true时从页面获取
			var items = [];
			if(grid)grid.data("verifyFun",verifyFun);
			var toolbar = {search:{
				text: $.messager.defaults.search,
				title: $.messager.defaults.search,
				icon: {
					url: basePath+'/image/Button/op_owner.gif',
					position: ['0px','-40px']
				},
				handler: function(){
					delay(0.5,function(){
						if(!(verifyFun ? verifyFun() : true))return;
						searchFun ? searchFun(grid,form) :
						grid.datagrid("load",getParam(form));
					});
				}
			},
			excel:{
				text: $.messager.defaults.excel,
				title: $.messager.defaults.excel,
				icon: {
					url: basePath+'/image/Button/op_owner.gif',
					position: ['-40px','-20px']
				},
				handler: function(){
					if(!(verifyFun ? verifyFun() : true))return;
					var headers = [];
					if(exportTyp){
						var panel = grid.datagrid('getPanel');
						var content = panel.panel('body');
						function clearHead(head){
							head.find('table').removeAttr('border').removeAttr('cellspacing').removeAttr('cellpadding');
							head.find('td').each(function(){
								if($(this).css('display') == 'none'){
									$(this).remove();
								}else{
									$(this).removeAttr('class');
									$(this).children('div').html($.trim($(this).text()));
									$(this).children('div').removeAttr('class');
								}
							});
							return head.html();
						}
						headers.push(clearHead(content.find('.datagrid-view').find('.datagrid-view1').find('.datagrid-header-inner').clone()));
						headers.push(clearHead(content.find('.datagrid-view').find('.datagrid-view2').find('.datagrid-header-inner').clone()));
						headers.push("<fieldMap>"+$.toJSON(fieldMap)+"</fieldMap>");
					}
					headers = headers.join("");
					var rs = headers.match(/\w+\s*=\w+/g);
					for(var s in rs){
						var string = String(rs[s]);
						string.match(/(\w+)$/g);
						headers = headers.replace(string,string.replace(RegExp.$1,'"'+RegExp.$1+'"'));
					}
					var head = $("<input type='hidden' name='headers'/>");
					form.find("input[name='headers']").remove();
					head.val(headers.replace(/\r\n/g,""));
					head.appendTo(form);
					form.attr('action',params.excelUrl);
					form.submit();
				}
			},
			print:{
				text: $.messager.defaults.print,
				title: $.messager.defaults.print,
				icon: {
					url: basePath+'/image/Button/op_owner.gif',
					position: ['-140px','-100px']
				},
				handler: function(){
					if(!(verifyFun ? verifyFun() : true))return;
					form.attr('target','report');
					window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
					var action=params.printUrl;
					form.attr('action',action);
					form.submit();
				}
			},
			option:{
				text: $.messager.defaults.option,
				title: $.messager.defaults.option,
				icon: {
					url: basePath+'/image/Button/op_owner.gif',
					position: ['-100px','-60px']
				},
				handler: function(){
					toColsChoose(params.colsChooseUrl);
				}
			},
			exit:{
				text: $.messager.defaults.exit,
				title: $.messager.defaults.exit,
				useable:true,
				icon: {
					url: basePath+'/image/Button/op_owner.gif',
					position: ['-160px','-100px']
				},
				handler: function(){
					invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
				}
			}
			};
			
			for(var i in curtoolbar){
				if(typeof(curtoolbar[i]) == 'string')
					items.push(toolbar[curtoolbar[i]]);
				else
					items.push(curtoolbar[i]);
			}
			$('#'+params.toolbarId).html('');
			$('#'+params.toolbarId).toolbar({
				items:items
			});
			bodyHeight = $(".layout-panel-center",top.document).children('div[region="center"]').height() - $(".tab-control",top.document).height();
			tableHeight = bodyHeight - $("#tool").height() - $("#queryForm").height() - $(".tabs-header").height();
			$('body').height(bodyHeight);
		}
		//解析获取表单数据
		function getParam(form){
			form = form.find("*[name]").filter(function(){
				return $.inArray($(this).attr('type') ? $(this).attr('type').toLowerCase() : undefined ,['button','submit','reset','image','file']) < 0 && $(this).val() 
				&& !$(this).attr('disabled');
			});
			var mul = ['radio','checkbox'];
			var temp = {};
			var param = {};
			form.each(function(){
				this.tagName.toLowerCase() == 'input' ? (temp[$(this).attr('name')] = $(this).attr('type') ? $(this).attr('type') : 'text') : temp[$(this).attr('name')] = this.tagName.toLowerCase();
			});
			for(var i in temp){
				$.inArray(temp[i],mul) < 0 ? param[i] = form.filter(temp[i]+'[name="'+i+'"]').val() ? param[i] = form.filter(temp[i]+'[name="'+i+'"]').val() : form.filter('input[name="'+i+'"]').val()
						: param[i] = form.filter('input[name="'+i+'"]:checked').val();
			}
			queryParams = param;
			return param;
		}
		//限制开始和结束时间为同一个月  
  	    function limitMonthDate() {  
  	        var DateString;  
            var beginDate = $dp.$("bdat").value;  
            if (beginDate != "" && beginDate != null) {  
                var limitDate = new Date(beginDate);  
                limitDate.setDate(new Date(limitDate.getFullYear(), limitDate  
                        .getMonth() + 1, 0).getDate()); //获取此月份的天数  
                DateString = limitDate.getFullYear() + '-'  
                        + (limitDate.getMonth() + 1 < 10 ? '0' + (limitDate.getMonth() + 1) : (limitDate.getMonth() + 1)) + '-'  
                        + limitDate.getDate();  
                return DateString;  
            }  
  	    }
  	 </script>
  </body>
</html>
