<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <title>物资余额查询</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<style type="text/css">
		.text{
			font-size:12px,border:0px,line-height:20px,height:20px,padding:0px,*height:18px,*line-height:18px,_height:18px,_line-height:18px;
		}
		input[type=radio] , input[type=checkbox]{
			height:13px; 
			vertical-align:text-top; 
			margin-top:1px;
			margin-right: 3px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 6.2%;
		}
		form .form-line .form-input{
			width: 15%;
		}
		form .form-line .form-input input[type=text]{
			width: 80%;
		}
		form .form-line .form-input select{
			width: 82%;
		}
	</style>
  </head>	
  <body>
  	<div class="tool"></div>
  	<input id="firstLoad" type="hidden"/>
  	<form id="queryForm" name="queryForm" method="post">
				<div class="form-line"  style="z-index:10;">
					<div class="form-label"><fmt:message key="positions"/></div>
					<div class="form-input">
						<input type="text" name="positn_name" id="positn_name" style="margin-top: -3px;" autocomplete="off" class="text" value="<c:out value="${supplyAcct.positn}" />"/>
						<img id="seachDept" class="search" src="<%=path%>/image/themes/icons/search.png" alt='查询仓位' />
						<input type="hidden" id="positn" name="positn" value="${supplyAcct.positn }"/>
<%-- 						<input type="hidden" id="positn1" name="positn1" value="${supplyAcct.positn }"/> --%>
					</div>
					<div class="form-label"><fmt:message key="Accounting_year"/></div>
					<div class="form-input">
						<select id="yearr" name="yearr" key="yearr" data="yearr" url="<%=path %>/misbohcommon/findAllYears.do"  class="select" single="true" code="" des=""></select>
					</div>
					<div class="form-label"><fmt:message key="months"/></div>
					<div class="form-input">
						<select id="month" name="month" class="select">
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
						</select>
					</div>
				</div>
				<div class="form-line"  style="z-index:9;">
					<div class="form-label" <c:if test='${type!=null}'>style="display:none;"</c:if>><fmt:message key="shelf"/></div>
					<div class="form-input" <c:if test='${type!=null}'>style="display:none;"</c:if>>
						<select id="positn1" name="positn1" key="POSITN1" data="POSITN1" url="<%=path %>/positn/findPositn1.do"  class="select"></select>
					</div>
					<div class="form-label" <c:if test='${type!=null}'>style="display:none;"</c:if>><fmt:message key="coding"/></div>
					<div class="form-input" <c:if test='${type!=null}'>style="display:none;"</c:if> style="margin-top: -2px;">
						<input type="text" id="sp_code" name="sp_code" value="${sp_code}" class="text" />
						<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
					</div>
					<div class="form-label"><fmt:message key="category"/></div>
					<div class="form-input">
						<input type="hidden" id="typ" name="typ" value="${supplyAcct.typ}"/>
						<input type="text"  id="typdes" name="typdes" readonly="readonly" value="${supplyAcct.typdes}" class="text" style="margin-top: -3px;"/>
						<img id="seachTyp" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_category"/>' />
					</div>
				</div>
				<div class="form-line"   style="z-index:8;">
					<div class="form-label"></div>
					<div class="form-input">
						<input  type="checkbox" id="without0" name="without0"  value="1"/><label for="without0"><fmt:message key="filter_zero_value"/></label>
					</div>
					<div class="form-label"></div>
					<div class="form-input">
						[<input type="radio" id="folio1" checked="checked" name="folio" value="1" /><label for="folio1"><fmt:message key="all"/></label>
						<input type="radio" id="folio2" name="folio" value="2" /><label for="folio2"><fmt:message key="there_is_a_balance"/></label>]
					</div>
					<div class="form-label"></div>
					<div class="form-input">
						[<input type="radio" id="bill2" checked="checked" name="bill" value="2" /><label for="bill2"><fmt:message key="only_the_library"/></label>
							<input type="radio" id="bill3" name="bill" value="3" /><label for="bill3"><fmt:message key="only_subtract"/></label>]
					</div>
				</div>
			</form>
 	<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
  	 <script type="text/javascript">
  		$('#yearr').attr('code',new Date().getFullYear());//会计年默认当前年
		$('#yearr').attr('des',new Date().getFullYear());
  	 	$(document).ready(function(){
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							var form = $("#queryForm").find("*[name]");
							form = form.filter(function(index){
								var cur = form[index];
								if($(cur).attr("name")){
									if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
										if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
											if($("input[name='"+$(cur).attr("name")+"']:checked").length){
												params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
											}else{
												params[$(cur).attr("name")] = undefined;
											}
										}else{
											params[$(cur).attr("name")] = $(cur).val();
										}
									}
								}
								
							});
							$("#datagrid").datagrid("load");
						}
					},{
						text: 'Excel',
						title: 'Excel',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$('#queryForm').attr('action',"<%=path%>/WzYueChaxunMisBoh/exportSupplyBalance.do");
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="print" />',
						title: '<fmt:message key="print" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							$('#queryForm').attr('target','report');
							window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
							var action="<%=path%>/WzYueChaxunMisBoh/printSupplyBalance.do";
							$('#queryForm').attr('action',action);
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="column_selection" />',
						title: '<fmt:message key="column_selection" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-100px','-60px']
						},
						handler: function(){
							toColsChoose();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							$(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').click();
						}
					}
				]
			});
  	 		$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/misbohcommon/selectSupplyLeft.do?sta=all&positn=1&defaultCode='+defaultCode,$('#sp_code'));	
				}
			});

  	 		$('#seachDept').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#positn').val();
					var defaultName = $('#positn_name').val();
					//alert(defaultCode+"==="+defaultName);
					var offset = getOffset('positn');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/misbohcommon/findPositnSuper.do?sta=all&typn=7&iffirm=1&mold=oneTmany&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positn_name'),$('#positn'),'760','520','isNull');
				}
			});
			$('#seachTyp').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#typ').val();
					var offset = getOffset('positn');
					top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/misbohcommon/selectMoreGrpTyp.do?brackets=Y&defaultCode='+defaultCode),offset,$('#typdes'),$('#typ'),'320','460','isNull');
				}
			});
  	 	  	var m = new Date().getMonth()+1;
	 		$("#month > option[value="+m+"]").attr("selected","selected");
  	 		var tableHeight = $(".main",top.document).parent("div[region='center']").height() - $(".tab-control",top.document).height() - $(".tool").height() - $("#queryForm").height();
  	 		$("select").not("#month").each(function(){
  	 			$(this).htmlUtils("select");
  	 		});
  	 		var des = $('#des'); 
  	 		$('#des').parent().html("").append(des);
  	 		$('#des').htmlUtils('select',[{key:'<fmt:message key="unqualified"/>',value:'<fmt:message key="unqualified"/>'}]);
  	 		//定义类型为Date的列，js将根据此变量解析Data类型数据默认解析成yyyy-MM-dd样式
  	 		var dateCols = ['dat'];
  	 		//数字列
  	 		var numCols = ['price','sp_price','startnumber','startmoney','innumber','inmoney','outnumber','outmoney',
  	 		               'endnumber','endmoney','lilunnumber','lilunmoney','inventorynumber','inventorymoney','sp_max','sp_max2','sp_min','sp_cxl','amountin','neednumber'];
  	 		var numRowCols = ['price','sp_price','startnumber','startmoney','innumber','inmoney','outnumber','outmoney',
  	  	 		               'endnumber','endmoney','lilunnumber','lilunmoney','inventorynumber','inventorymoney','sp_max','sp_max2','sp_min','amountin'];
  	 		var numFootCols = ['startnumber','startmoney','innumber','inmoney','outnumber','outmoney','endnumber','endmoney','lilunnumber','lilunmoney','inventorynumber','inventorymoney'];
  	 		//收集form表单数据的对象
  	 		var params = {};
  	 		//Controller传来的map对象，包含所需要显示的报表所有列的dictColumns对象，和需要固定在左侧的col的index（用，分割）
  	 		var tableContent = {};
  	 		//表头行（单行）
  	 		var columns = [];
  	 		//表头（多行），其中元素为columns
  	 		var head = [];
  	 		//需要固定在左侧的列的表头（单行）
  	 		var frozenHead = [];
  	 		//需要固定在左侧的列的表头（多行），元素为frozenHead
  	 		var frozenColumns = [];
  	 		//物资余额报货单生成所需列表
			var createBillRows = {};
  	 		//ajax获取报表表头
  	 		$.ajax({url:"<%=path%>/WzYueChaxunMisBoh/findSupplyBalanceHeaders.do",
  	 				async:false,
  	 				success:function(data){
  	 					tableContent = data;
  	 				}
  	 			});
  	 		//解析获取的数据
  	 		var frozenIndex = tableContent.frozenColumns.split(',');
  	 		var Cols = [];
  	 		var colsSecond = [];
			var prev = '';
			var temp;
  	 		for(var i in tableContent.columns)Cols.push(tableContent.columns[i].zhColumnName);
  	 		var t = Cols.toString().match(/,([\d\D]+?)\|[\d\D]+?(?=,)/g);
  	 		if(t && !t.length){
  	 			for(var i in tableContent.columns){
  	 				var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
	  	 			if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
	  	 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
	  	 			else
	  	 				columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
	  	 		}
				head.push(columns);
	  	 		frozenHead.push(frozenColumns);
  	 		}else{
  	 			for(var i in tableContent.columns){
  	 				var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
  	 				if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
	  	 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
  	 				else{
  	 					var cur = tableContent.columns[i].zhColumnName.match(/^([\d\D]+)\|[\d\D]+$/g);
  	 					if(cur && cur.length){
  	 						var cur = tableContent.columns[i].zhColumnName;
  	 						if(cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1") == prev){
  	 							temp.colspan ++;
  	 						}else{
  	 							temp = {title:cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1"),colspan:1};
  	 							columns.push(temp);
  	 							prev = cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1");
  	 						}
  	 						colsSecond.push({field:tableContent.columns[i].columnName.toUpperCase(),title:cur.replace(/^([\d\D]+)\|([\d\D]+)$/g,"$2"),width:tableContent.columns[i].columnWidth,sortable:true,colspan:1,align:align});
  	 					}else{
  	 						if(tableContent.columns[i].columnName)
  	 							columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
  	 					}
  	 				}
  	 			}
  	 			head.push(columns);
  	 			head.push(colsSecond);
  	 			frozenHead.push(frozenColumns);
  	 		}
  	 		
  	 		
  	 		//生成报表数据表格
  	 		$("#datagrid").datagrid({
  	 			title:'<fmt:message key="material_balance_inquiry"/>',
  	 			width:'100%',
  	 			height:tableHeight,
  	 			nowrap: true,
				striped: true,
				singleSelect:true,
				collapsible:true,
				//对从服务器获取的数据进行解析格式化
	 			dataFilter:function(data,type){
	 				 var rs = eval("("+data+")");
		 				var modifyRows = [];
		 				var modifyFoot = [];
		 				var rows = rs.rows;
		 				var footer = rs.footer;
		 				var keys = ["supply.sp_code","supply.sp_name","supply.sp_desc","supply.unit","amount",
		 				           "supply.unit1","amount1","hoped","memo"];
		 				var n = 0;
		 				if(!rows || rows.length <= 0)$('#datagrid').datagrid('loadData',{total:0,rows:[],footer:[]});
		 				for(var i in rows){
		 					var cols = tableContent.columns;
							var curRow = {};
		 					for(var j in cols){
		 						try{
		 							var value = eval("rows["+i+"]."+cols[j].properties.toUpperCase());
		 							value = $.inArray(cols[j].properties,numRowCols) >=0 ? (value ? value.toFixed(2) : '0.00') : (value ? ($.inArray(cols[j].properties,dateCols) >= 0 ? convertDate(value) : value):'');
		 							curRow[cols[j].columnName.toUpperCase()] = value;
		 						}catch(e){
		 							alert('<fmt:message key="wrong"/>'+"rows["+i+"]."+cols[j].properties);
		 						}
		 					}
		 					if(eval("rows["+i+"].NEEDNUMBER")>0){
		 						createBillRows["chkstod["+n+"]."+keys[0]]=eval("rows["+i+"].SP_CODE");
		 						createBillRows["chkstod["+n+"]."+keys[1]]=eval("rows["+i+"].SP_NAME");
		 						createBillRows["chkstod["+n+"]."+keys[2]]=eval("rows["+i+"].SP_DESC");
		 						createBillRows["chkstod["+n+"]."+keys[3]]=eval("rows["+i+"].UNIT");
		 						createBillRows["chkstod["+n+"]."+keys[4]]=eval("rows["+i+"].NEEDNUMBER");
		 						createBillRows["chkstod["+n+"]."+keys[7]]=new Date().getFullYear()+"-"+(new Date().getMonth()+1)+"-"+new Date().getDate();
			 					n++;
		 					}		 					
		 					modifyRows.push(curRow);
		 				}
		 				rs.rows = modifyRows;
		 				for(var i in footer){
		 					var cols = tableContent.columns;
							var curRow = {};
		 					for(var j in cols){
		 						try{
		 							var value = eval("footer["+i+"]."+cols[j].properties.toUpperCase()) ;
		 							value = $.inArray(cols[j].properties,numFootCols) >=0 ? (value ? value.toFixed(2) : '0.00') : (value ? ($.inArray(cols[j].properties,dateCols) >= 0 ? convertDate(value) : value):'');
		 							curRow[cols[j].columnName.toUpperCase()] = value;
		 						}catch(e){
		 							alert('<fmt:message key="wrong"/>'+"footer["+i+"]."+cols[j].properties);
		 						}
		 					}
		 					modifyFoot.push(curRow);
		 				}
		 				rs.footer = modifyFoot;
		 				return $.toJSON(rs);
	 			},
				url:"<%=path%>/WzYueChaxunMisBoh/findSupplyBalance.do",
				remoteSort: true,
				//页码选择项
				pageList:[20,30,40,50],
				frozenColumns:frozenHead,
				columns:head,
				queryParams:params,
				rowStyler:function(){
					return 'line-height:11px';
				},
				showFooter:true,
				pagination:true,
				rownumbers:true,
				onBeforeLoad:function(){
					if(!$("#firstLoad").val())
						return false;
				}
  	 		});
  	 		$("#firstLoad").val("true");
  	 		
  	 		$("#bdat,#edat").focus(function(){
  	 			new WdatePicker();
  	 		});
  	 		
  	 		$(".panel-tool").remove();

  	 	//判断仓位选取个数&提交报货单createBillRows
			$("#createbill").click(function(){
				if(!$("input:hidden[name='positn']").val() || $("input:hidden[name='positn']").val().split(",").length!=1){
					alert('<fmt:message key="please_select_one_positions"/>');
			  	 	/* showMessage({
						type: 'error',
						msg: '<fmt:message key="please_select_one_positions"/>',
						speed: 1500
					}); */
					return;	
				}else if(!createBillRows === {}){
					//提交并返回状态
					createBillRows["Positn.code"] = $("input:hidden[name='positn']").val();
					$.post("<%=path%>/WzYueChaxunMisBoh/saveBill.do",createBillRows,function(data){
						var rs = eval('('+data+')');
						switch(Number(rs)){
						case -1:
							alert('<fmt:message key="generated_newspaper_manifest"/><fmt:message key="save_fail"/>！');
							/* showMessage({
										type: 'error',
										msg: '<fmt:message key="generated_newspaper_manifest"/><fmt:message key="save_fail"/>！',
										speed: 2000
										}); */
							break;
						case 1:
							showMessage({
										type: 'success',
										msg: '<fmt:message key="generated_newspaper_manifest"/><fmt:message key="save_successful"/>！',
										speed: 2000
										});
							break;
						}
					});	
				}else{
					alert('<fmt:message key="there_is_no_need_to_buy_goods"/>！');
					return;
				}
			});
  	 	});
  	 	  	 	
  	 	function toColsChoose(){
  	 		$('body').window({
				title: '<fmt:message key="column_selection"/>',
				content: '<iframe frameborder="0" src="<%=path%>/SupplyAcctMisBoh/toColumnsChoose.do?reportName=${reportName}"></iframe>',
				width: '460px',
				height: '430px',
				draggable: true,
				isModal: true
			});
  	 	}
  	 </script>
  </body>
</html>
