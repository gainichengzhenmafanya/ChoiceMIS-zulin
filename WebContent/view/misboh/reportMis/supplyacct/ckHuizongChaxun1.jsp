<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<style type="text/css">
		.text{
			font-size:12px,border:0px,line-height:20px,height:20px,padding:0px,*height:18px,*line-height:18px,_height:18px,_line-height:18px;
		}
		input[type=radio] , input[type=checkbox]{
			height:13px; 
			vertical-align:text-top; 
			margin-top:1px;
			margin-right: 3px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 6%;
		}
		form .form-line .form-input{
			width: 17%;
		}
		.form-line .form-input input[type=text] , .form-line .form-input select{
			width: 80%;
		}
	</style>
  </head>	
  <body>
  	<div class="tool"></div>
  	<input id="firstLoad" type="hidden"/>
  	<form id="queryForm" name="queryForm" method="post">
  		<div class="form-line"  style="z-index:10;">
			<div class="form-label"><fmt:message key="startdate"/></div>
			<div class="form-input"><input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" /></div>
			<div class="form-label hidden" style="display: none;"><fmt:message key="startdate"/></div>
			<div class="form-input hidden" style="display: none;"><input autocomplete="off" disabled="disabled" type="text" id="bdatprev" name="bdatprev" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" /></div>
			<div class="form-label"><fmt:message key="coding"/></div>
			<div class="form-input">
			<input type="text" name="sp_code" id="sp_code" autocomplete="off" class="text" value="<c:out value="${chkoutm.chkoutno}" />" style="margin-top: -3px;"/>
			<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
			</div>
		</div>
		<div class="form-line"  style="z-index:10;">
			<div class="form-label"><fmt:message key="enddate"/></div>
			<div class="form-input"><input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd"/>"/></div>
			<div class="form-label hidden" style="display: none;"><fmt:message key="enddate"/></div>
			<div class="form-input hidden" style="display: none;"><input autocomplete="off" disabled="disabled" type="text" id="edatprev" name="edatprev" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd"/>"/></div>  		
<%-- 					<div class="form-label" <c:if test='${type!=null}'>style="display: none;"</c:if>><fmt:message key="positions"/></div> --%>
<%-- 					<div class="form-input" <c:if test='${type!=null}'>style="display: none;"</c:if>> --%>
<!-- 					<input type="text"  id="positn_name"  name="positn_name" readonly="readonly" value=""/> -->
<!-- 					<input type="hidden" id="positn" name="positn" value=""/> -->
<%-- 					<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' /> --%>
<!-- 					</div> -->
			<div class="form-label"><fmt:message key="category"/></div>
			<div class="form-input">
				<input type="hidden" id="typ" name="typ" value="${supplyAcct.typ}"/>
				<input type="text"  id="typdes" name="typdes" readonly="readonly" value="${supplyAcct.typdes}" class="text" style="margin-top: -3px;"/>
				<img id="seachTyp" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_category"/>' />
			</div>
		</div>	
		<div class="form-line"  style="z-index:9;">
			<div class="form-label"><fmt:message key="library_positions"/></div>
			<div class="form-input">
				<input type="text" name="positn_name" id="positn_name" class="text" value="<c:out value="${supplyAcct.positn}" />"/>
				<img id="seachDept" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
				<input type="hidden" id="positn" name="positn" value="${supplyAcct.positn }"/>
				<input type="hidden" id="positn1" name="positn1" value="${supplyAcct.positn }"/>
			</div>
			<div class="form-label"><fmt:message key="document_types"/></div>
			<div class="form-input"><select id="chktyp" name="chktyp" url="<%=path %>/misbohcommon/findAllBillType.do?codetyp=CK,ZF" class="select"></select></div>
<%-- 			<div class="form-label"><fmt:message key="bigClass"/></div> --%>
<!-- 			<div class="form-input"> -->
<%-- 				<select id="grptyp" name="grptyp" url="<%=path %>/grpTyp/findAllGrpTyp.do"  class="select"></select> --%>
<!-- 			</div> -->
<%-- 			<div class="form-label"><fmt:message key="middleClass"/></div> --%>
<%-- 			<div class="form-input"><select id="grp" name="grp" url="<%=path %>/grpTyp/findAllGrp.do"   class="select"></select></div> --%>
<%-- 			<div class="form-label"><fmt:message key="smallClass"/></div> --%>
<%-- 			<div class="form-input"><select id="typ" name="typ" url="<%=path %>/grpTyp/findAllTyp.do"  class="select"></select></div> --%>
		</div>
		<div class="form-line"   style="z-index:8;" <c:if test='${type!=null}'>style="display: none;"</c:if>>
			<div class="form-label" <c:if test='${type!=null}'>style="display: none;"</c:if>><fmt:message key="requisitioned_positions"/></div>
			<div class="form-input" <c:if test='${type!=null}'>style="display: none;"</c:if>>
			<input type="text"  id="positn_name1"  name="positn_name1" readonly="readonly" value=""/>
			<input type="hidden" id="firm" name="firm" value=""/>
			<img id="seachPositn1" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
			</div>
			<div class="form-label"></div>
			<div class="form-input">
				<input type="radio" checked="checked" name="chktag" value="0"/><fmt:message key="all"/>
				<input type="radio" name="chktag" value="2"/><fmt:message key="only_the_library"/>
				<input type="radio" name="chktag" value="3"/><fmt:message key="only_direct_dial"/>
			</div>
			<div class="form-label"></div>
			<div class="form-input">
				[
				<input type="radio" name="showtyp" value="0" checked="checked" />
				按领用仓位
				<input type="radio" name="showtyp" value="1" />
				按耗用仓位 ]
			</div>
		</div>
	</form>
 	<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							params = {};
							var form = $("#queryForm").find("*[name]");
							form = form.filter(function(index){
								var cur = form[index];
								if($(cur).attr("name")){
									if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
										if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
											if($("input[name='"+$(cur).attr("name")+"']:checked").length){
												params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
											}else{
												params[$(cur).attr("name")] = undefined;
											}
										}else{
											params[$(cur).attr("name")] = $(cur).val();
										}
									}
								}
								
							});
							$.ajax({url:"<%=path%>/CkHuizongChaxunMisBoh/findChkoutSumQuery1Headers.do",
			  	 				async:false,
			  	 				data:params,
			  	 				success:function(data){
			  	 					tableContent = data;
			  	 				}
			  	 			});
			  	 		//解析获取的数据
			  	 		var frozenIndex = tableContent.frozenColumns.split(',');
			  	 		var Cols = [];
			  	 		var colsSecond = [];
						var prev = '';
						var temp;
							head=[];
							columns=[];
							frozenHead=[];
							for(var i in tableContent.columns)Cols.push(tableContent.columns[i].zhColumnName);
				  	 		var t = Cols.toString().match(/,([\d\D]+?)\|[\d\D]+?(?=,)/g);
				  	 		if(t && !t.length){
				  	 			for(var i in tableContent.columns){
				  	 				var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
					  	 			if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
					  	 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
					  	 			else
					  	 				columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
					  	 		}
								head.push(columns);
					  	 		frozenHead.push(frozenColumns);
				  	 		}else{
				  	 			for(var i in tableContent.columns){
				  	 				var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
				  	 				if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
					  	 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
				  	 				else{
				  	 					var cur = tableContent.columns[i].zhColumnName.match(/^([\d\D]+)\|[\d\D]+$/g);
				  	 					if(cur && cur.length){
				  	 						var cur = tableContent.columns[i].zhColumnName;
				  	 						if(cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1") == prev){
				  	 							temp.colspan ++;
				  	 						}else{
				  	 							temp = {title:cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1"),colspan:1};
				  	 							columns.push(temp);
				  	 							prev = cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1");
				  	 						}
				  	 						colsSecond.push({field:tableContent.columns[i].columnName.toUpperCase(),title:cur.replace(/^([\d\D]+)\|([\d\D]+)$/g,"$2"),width:tableContent.columns[i].columnWidth,sortable:true,colspan:1,align:align});
				  	 					}else{
				  	 						if(tableContent.columns[i].columnName)
				  	 							columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
				  	 					}
				  	 				}
				  	 			}
				  	 			head.push(columns);
				  	 			head.push(colsSecond);
				  	 			frozenHead.push(frozenColumns);
				  	 		}
				  	 		
				  	 		
				  	 		//生成报表数据表格
				  	 		$("#datagrid").datagrid({
				  	 			title:'<fmt:message key="aggregate_query_library"/>',
				  	 			width:'100%',
				  	 			height:tableHeight,
				  	 			nowrap: true,
								striped: true,
								singleSelect:true,
								collapsible:true,
								//对从服务器获取的数据进行解析格式化
					 			dataFilter:function(data,type){
					 				var rs = eval("("+data+")");
					 				var modifyRows = [];
					 				var rows = rs.rows;
					 				if(!rows || rows.length <= 0)$('#datagrid').datagrid('loadData',{total:0,rows:[],footer:[]});
					 				for(var i in rows){
					 					var cols = tableContent.columns;
										var curRow = {};
					 					for(var j in cols){
					 						try{
					 							var value = eval("rows["+i+"]."+cols[j].properties.toUpperCase()) ;
					 							value = value ? ($.inArray(cols[j].properties,dateCols) >= 0 ? convertDate(value) : value):($.inArray(cols[j].properties,numCols) < 0 ? '':0);
					 							curRow[cols[j].columnName.toUpperCase()] = value;
					 						}catch(e){
					 							alert('Exception'+"rows["+i+"]."+cols[j].properties);
					 						}
					 					}
					 					modifyRows.push(curRow);
					 				}
					 				rs.rows = modifyRows;
					 				//非固定列，第一列显示数据总行数
					 				rs.footer[0][columns[0].field] = '<fmt:message key="total"/>';
					 				return $.toJSON(rs);
					 			},
								url:"<%=path%>/CkHuizongChaxunMisBoh/findChkoutSumQuery1.do",
								remoteSort: true,
								//页码选择项
								pageList:[20,30,40,50],
								frozenColumns:frozenHead,
								columns:head,
								queryParams:params,
								rowStyler:function(){
									return 'line-height:11px';
								},
								showFooter:true,
								pagination:true,
								rownumbers:true
				  	 		});
				  	 		$(".panel-tool").remove();
						}
					},{
						text: 'Excel',
						title: 'Excel',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$('#queryForm').attr('action',"<%=path%>/CkHuizongChaxunMisBoh/exportChkoutSumQuery1.do");
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="print" />',
						title: '<fmt:message key="print" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							$('#queryForm').attr('target','report');
							window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
							var action="<%=path%>/CkHuizongChaxunMisBoh/printChkoutSumQuery1.do";
							$('#queryForm').attr('action',action);
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="column_selection" />',
						title: '<fmt:message key="column_selection" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-100px','-60px']
						},
						handler: function(){
							toColsChoose();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							$(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').click();
						}
					}
				]
			});
  	 		var tableHeight = $(".main",top.document).parent("div[region='center']").height() - $(".tab-control",top.document).height() - $(".tool").height() - $("#queryForm").height();
  	 		$("#bycomp").click(function(){
  	 			if($(this).attr("checked"))
  	 				$(".hidden").css("display","block") && $(".hidden").find("input").removeAttr("disabled");
  	 			else
  	 				$(".hidden").css("display","none") && $(".hidden").find("input").attr("disabled","disabled");
  	 		});
  	 		$("select").each(function(){
  	 			$(this).htmlUtils("select");
  	 		});
  	 		var comp = [136,137,138,139];
  	 		var monthMax = [140,141];
  	 		$("#bdat,#edat,#edatprev,#bdatprev").htmlUtils("setDate","now");
  	 		var des = $('#des'); 
  	 		$('#des').parent().html("").append(des);
  	 		$('#des').htmlUtils('select',[{key:'<fmt:message key="unqualified"/>',value:'<fmt:message key="unqualified"/>'}]);
  	 		//定义类型为Date的列，js将根据此变量解析Data类型数据默认解析成yyyy-MM-dd样式
  	 		var dateCols = ['dat'];
  	 		//数字列
  	 		var numCols = ['cntout','amtout','priceout','cntoutprev','amtoutprev','priceoutprev','subvalue'];
  	 		//收集form表单数据的对象
  	 		var params = {};
  	 		//Controller传来的map对象，包含所需要显示的报表所有列的dictColumns对象，和需要固定在左侧的col的index（用，分割）
  	 		var tableContent = {};
  	 		//表头行（单行）
  	 		var columns = [];
  	 		//表头（多行），其中元素为columns
  	 		var head = [];
  	 		//需要固定在左侧的列的表头（单行）
  	 		var frozenHead = [];
  	 		//需要固定在左侧的列的表头（多行），元素为frozenHead
  	 		var frozenColumns = [];
  	 		//ajax获取报表表头
  	 		var form = $("#queryForm").find("*[name]");
							form = form.filter(function(index){
								var cur = form[index];
								if($(cur).attr("name")){
									if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
										if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
											if($("input[name='"+$(cur).attr("name")+"']:checked").length){
												params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
											}
										}else{
											params[$(cur).attr("name")] = $(cur).val();
										}
									}
								}
								
							});
  	 		$.ajax({url:"<%=path%>/CkHuizongChaxunMisBoh/findChkoutSumQuery1Headers.do",
  	 				async:false,
  	 				data:params,
  	 				success:function(data){
  	 					tableContent = data;
  	 				}
  	 			});
  	 		//解析获取的数据
  	 		var frozenIndex = tableContent.frozenColumns.split(',');
  	 		var Cols = [];
  	 		var colsSecond = [];
			var prev = '';
			var temp;
  	 		for(var i in tableContent.columns)Cols.push(tableContent.columns[i].zhColumnName);
  	 		var t = Cols.toString().match(/,([\d\D]+?)\|[\d\D]+?(?=,)/g);
  	 		if(t && !t.length){
  	 			for(var i in tableContent.columns){
  	 				var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
	  	 			if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
	  	 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
	  	 			else
	  	 				columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
	  	 		}
				head.push(columns);
	  	 		frozenHead.push(frozenColumns);
  	 		}else{
  	 			for(var i in tableContent.columns){
  	 				var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
  	 				if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
	  	 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
  	 				else{
  	 					var cur = tableContent.columns[i].zhColumnName.match(/^([\d\D]+)\|[\d\D]+$/g);
  	 					if(cur && cur.length){
  	 						var cur = tableContent.columns[i].zhColumnName;
  	 						if(cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1") == prev){
  	 							temp.colspan ++;
  	 						}else{
  	 							temp = {title:cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1"),colspan:1};
  	 							columns.push(temp);
  	 							prev = cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1");
  	 						}
  	 						colsSecond.push({field:tableContent.columns[i].columnName.toUpperCase(),title:cur.replace(/^([\d\D]+)\|([\d\D]+)$/g,"$2"),width:tableContent.columns[i].columnWidth,sortable:true,colspan:1,align:align});
  	 					}else{
  	 						if(tableContent.columns[i].columnName)
  	 							columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
  	 					}
  	 				}
  	 			}
  	 			head.push(columns);
  	 			head.push(colsSecond);
  	 			frozenHead.push(frozenColumns);
  	 		}
  	 		
  	 		
  	 		//生成报表数据表格
  	 		$("#datagrid").datagrid({
  	 			title:'<fmt:message key="aggregate_query_library"/>',
  	 			width:'100%',
  	 			height:tableHeight,
  	 			nowrap: true,
				striped: true,
				singleSelect:true,
				collapsible:true,
				//对从服务器获取的数据进行解析格式化
	 			dataFilter:function(data,type){
	 				var rs = eval("("+data+")");
	 				var modifyRows = [];
	 				var rows = rs.rows;
	 				if(!rows || rows.length <= 0)$('#datagrid').datagrid('loadData',{total:0,rows:[],footer:[]});
	 				for(var i in rows){
	 					var cols = tableContent.columns;
						var curRow = {};
	 					for(var j in cols){
	 						try{
	 							var value = eval("rows["+i+"]."+cols[j].properties.toUpperCase()) ;
	 							value = $.inArray(cols[j].properties,numCols) >=0 ? (value ? value.toFixed(2) : '0.00') : (value ? ($.inArray(cols[j].properties,dateCols) >= 0 ? convertDate(value) : value):'');
	 							curRow[cols[j].columnName.toUpperCase()] = value;
	 						}catch(e){
	 							alert('Exception'+"rows["+i+"]."+cols[j].properties);
	 						}
	 					}
	 					modifyRows.push(curRow);
	 				}
	 				rs.rows = modifyRows;
	 				//非固定列，第一列显示数据总行数
	 				rs.footer[0][columns[0].field] = rs.total;
	 				return $.toJSON(rs);
	 			},
				url:"<%=path%>/CkHuizongChaxunMisBoh/findChkoutSumQuery1.do",
				remoteSort: true,
				//页码选择项
				pageList:[20,30,40,50],
				frozenColumns:frozenHead,
				columns:head,
				queryParams:params,
				showFooter:true,
				rowStyler:function(){
					return 'line-height:11px';
				},
				pagination:true,
				rownumbers:true,
				onBeforeLoad:function(){
					if(!$("#firstLoad").val())
						return false;
				},
  	 			onDblClickRow:function(index,data){
  	 				var chktypval = typeof($("#chktyp").data("checkedVal"))!="undefined"?$("#chktyp").data("checkedVal"):"";
  	 				var chktypname = typeof($("#chktyp").data("checkedName"))!="undefined"?$("#chktyp").data("checkedName"):"";
  	 				var parameters = {"sp_code":data['SP_CODE'],"bdat":$("#bdat").val(),"edat":$("#edat").val(),
  	 						"positn":$("#positn").val(),"positndes":$("#positn_name").val(),
  	 						"firmcode":$("#firm").val(),"firmdes":$("#positn_name1").val(),
  	 						"typ":$("#typ").val(),"typdes":$('#typdes').val(),
  	 						"chktyp":chktypval,"chktypdes":chktypname};
    	 	  		openTag("ChkoutDetailQuery","出库明细查询","<%=path%>/CkMingxiChaxunMisBoh/toChkoutDetailQuery.do",parameters);
  	 			}
  	 		});
  	 		$("#firstLoad").val("true");
  	 		$(".panel-tool").remove();
  	 		$("#bdat,#edat,#bdatprev,#edatprev").focus(function(){
  	 			new WdatePicker();
  	 		});
  	 		
  	 		$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/misbohcommon/selectSupplyLeft.do?sta=all&positn=1&defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
  	 		
  	 		/*弹出树*/
			$('#seachPositn1').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#firm').val();
					var defaultName = $('#positn_name1').val();
					//alert(defaultCode+"==="+defaultName);
					var offset = getOffset('positn');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/misbohcommon/findPositnSuper.do?sta=all&typn=3-6&mold=oneTmany&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positn_name1'),$('#firm'),'760','520','isNull');
				}
			});
  	 		
  	 		/*弹出树*/
			$('#seachDept').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#positn').val();
					var defaultName = $('#positn_name').val();
					//alert(defaultCode+"==="+defaultName);
					var offset = getOffset('positn');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/misbohcommon/findPositnSuper.do?sta=all&typn=7&iffirm=1&mold=oneTmany&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positn_name'),$('#positn'),'760','520','isNull');
				}
			});
  	 		
			/*弹出树*/
			$('#seachTyp').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#typ').val();
					var offset = getOffset('bdat');
					top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/misbohcommon/selectMoreGrpTyp.do?brackets=Y&defaultCode='+defaultCode),offset,$('#typdes'),$('#typ'),'320','460','isNull');
				}
			});
  	 	});
  	 	
  	 	function toColsChoose(){
  	 		$('body').window({
				title: '<fmt:message key="column_selection"/>',
				content: '<iframe frameborder="0" src="<%=path%>/SupplyAcctMisBoh/toColumnsChoose.do?reportName=${reportName}"></iframe>',
				width: '460px',
				height: '430px',
				draggable: true,
				isModal: true
			});
  	 	}
  	 </script>
  </body>
</html>
