<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<style type="text/css">
		.text{
			font-size:12px,border:0px,line-height:20px,height:20px,padding:0px,*height:18px,*line-height:18px,_height:18px,_line-height:18px;
		}
		input[type=radio] , input[type=checkbox]{
			height:13px; 
			vertical-align:text-top; 
			margin-top:1px;
			margin-right: 3px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 5%;
		}
		form .form-line .form-input{
			width: 13%;
		}
		form .form-line .form-input input[type=text]{
			width: 80%;
		}
		form .form-line .form-input select{
			width: 82%;
		}
	</style>
  </head>	
  <body>
  	<div class="tool"></div>
  	<input id="firstLoad" type="hidden"/>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line"  style="z-index:10;">
			<div class="form-label"><fmt:message key="startdate"/></div>
			<div class="form-input"><input type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${supplyAcct.bdat}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/></div>
			<div class="form-label"><fmt:message key="positions"/></div>
			<div class="form-input">
				<input type="text" name="positn_name" id="positn_name" style="margin-top: -3px;" autocomplete="off" class="text" value="<c:out value="${supplyAcct.positn}" />"/>
				<img id="seachDept" class="search" src="<%=path%>/image/themes/icons/search.png" alt='查询仓位' />
				<input type="hidden" id="positn" name="positn" value="${supplyAcct.positn }"/>
			</div>
			<div class="form-label"><fmt:message key="category"/></div>
			<div class="form-input">
				<input type="hidden" id="typ" name="typ" value="${supplyAcct.typ}"/>
				<input type="text"  id="typdes" name="typdes" readonly="readonly" value="${supplyAcct.typdes}" class="text" />
				<img id="seachTyp" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_category"/>' />
			</div>
		</div>
		<div class="form-line"  style="z-index:9;">
			<div class="form-label"><fmt:message key="enddate"/></div>
			<div class="form-input"><input type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${supplyAcct.edat}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/></div>
			<div class="form-label"><fmt:message key="suppliers"/></div>
			<div class="form-input">
			<input type="text"  id="deliver_name"  name="deliverdes" readonly="readonly" value="${supplyAcct.deliverdes }"/>
			<input type="hidden" id="delivercode" name="delivercode" value="${supplyAcct.delivercode }"/>
			<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_suppliers"/>' />
			<%-- <select  name="delivercode" url="<%=path %>/deliver/findAllDeliver.do"   class="select"></select> --%>
			</div>
			<div class="form-label"></div>
			<div class="form-input">
				<input type="checkbox" name="tax" value="1"/><fmt:message key="Whether_tax"/>
			</div>
			<div class="form-label"></div>
			<div class="form-input">
				<input type="checkbox" id="showtyp" name="showtyp" value="1"/><fmt:message key="Contain_transfer"/>
			</div>
		</div>
	</form>
 	<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							var form = $("#queryForm").find("*[name]");
							form = form.filter(function(index){
								var cur = form[index];
								if($(cur).attr("name")){
									if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
										if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
											if($("input[name='"+$(cur).attr("name")+"']:checked").length){
												params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
											}else{
												params[$(cur).attr("name")] = undefined;
											}
										}else{
											params[$(cur).attr("name")] = $(cur).val();
										}
									}
								}
								
							});
							
							var typ = $("#typ").val();
							var grpTypList1 = {};
				  	 		//表头（多行），其中元素为columns
				  	 		head = [];
							$.ajax({url:"<%=path%>/GysLeibieHuizongMisBoh/findGysLeibieHuizongHeaders3.do?typ="+typ,
			  	 				async:false,
			  	 				success:function(data){
			  	 					grpTypList1 = data;
			  	 				}
			  	 			});
							//解析获取的数据
					 		var col11 = [];
				  	 		var col12 = [];
				  	 		var col13 = [];
				  	 		
							for(var i in grpTypList1){
								var grpList = grpTypList1[i].grpList;
								var gtLen = 0;
				  	 			for(var j in grpList){
				  	 				gtLen += 1;
				  	 				var typList = grpList[j].typList;
				  	 				col12.push({title:grpList[j].des,colspan:typList.length + 1,align:'center'});
				  	 				for(var k in typList){
				  	 					gtLen += 1;
				  	 					col13.push({field:'T_'+typList[k].code,title:typList[k].des,width:60,align:'right'});
					  	 			}
				  	 				col13.push({field:'XJ_'+grpList[j].code,title:'<fmt:message key="subtotal" />',width:60,align:'right'});
				  	 			}
				  	 			col11.push({title:grpTypList1[i].des,colspan:gtLen + 1,align:'center'});
				  	 			col12.push({field:'HJ_'+grpTypList1[i].code,rowspan:2,title:'<fmt:message key="total" />',width:60,align:'right'});
				  	 		}
							head.push(col11);
							head.push(col12);
							head.push(col13);
							$("#datagrid").datagrid({columns:head});
						}
					},{
						text: 'Excel',
						title: 'Excel',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$('#queryForm').attr('action',"<%=path%>/GysLeibieHuizongMisBoh/exportDeliverCategorySum3.do");
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							$(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').click();
						}
					}
				]
			});
  	 		var tableHeight = $(".main",top.document).parent("div[region='center']").height() - $(".tab-control",top.document).height() - $(".tool").height() - $("#queryForm").height();
  	 		//收集form表单数据的对象
  	 		var params = {};
  	 		//Controller传来的map对象，包含所需要显示的报表所有列的dictColumns对象，和需要固定在左侧的col的index（用，分割）
  	 		var grpTypList = {};
  	 		//表头（多行），其中元素为columns
  	 		var head = [];
  	 		//需要固定在左侧的列的表头（单行）
  	 		var frozenHead = [];
  	 		//需要固定在左侧的列的表头（多行），元素为frozenHead
  	 		var frozenColumns = [];
  	 		//ajax获取报表表头
  	 		$.ajax({url:"<%=path%>/GysLeibieHuizongMisBoh/findGysLeibieHuizongHeaders3.do",
  	 				async:false,
  	 				success:function(data){
  	 					grpTypList = data;
  	 				}
  	 			});
			//解析获取的数据
	 		var col1 = [];
  	 		var col2 = [];
  	 		var col3 = [];
  	 		
			for(var i in grpTypList){
				var grpList = grpTypList[i].grpList;
				var gtLen = 0;
  	 			for(var j in grpList){
  	 				gtLen += 1;
  	 				var typList = grpList[j].typList;
  	 				col2.push({title:grpList[j].des,colspan:typList.length + 1,align:'center'});
  	 				for(var k in typList){
  	 					gtLen += 1;
  	 					col3.push({field:'T_'+typList[k].code,title:typList[k].des,width:60,align:'right'});
	  	 			}
  	 				col3.push({field:'XJ_'+grpList[j].code,title:'<fmt:message key="subtotal" />',width:60,align:'right'});
  	 			}
  	 			col1.push({title:grpTypList[i].des,colspan:gtLen + 1,align:'center'});
  	 			col2.push({field:'HJ_'+grpTypList[i].code,title:'<fmt:message key="total" />',width:60,rowspan:2,align:'right'});
  	 		}
			head.push(col1);
			head.push(col2);
			head.push(col3);
  	 		frozenColumns.push({field:'DELIVERCODE',title:'<fmt:message key="suppliers_coding"/>',width:80,rowspan:3,align:'left'});
  	 		frozenColumns.push({field:'DELIVERDES',title:'<fmt:message key="suppliers_name"/>',width:120,rowspan:3,align:'left'});
			frozenColumns.push({field:'ZJ',title:'<fmt:message key="Total1" />',width:80,rowspan:3,align:'right'});
  	 		frozenHead.push(frozenColumns);
  	 		
  	 		//生成报表数据表格
  	 		$("#datagrid").datagrid({
  	 			title:'<fmt:message key="supplier_category_summary"/>1',
  	 			width:'100%',
  	 			height:tableHeight,
  	 			nowrap: true,
				striped: true,
				singleSelect:true,
				collapsible:true,
				//对从服务器获取的数据进行解析格式化
	 			dataFilter:function(data,type){
	 				var rs = eval("("+data+")");
	 				var modifyRows = [];
	 				var modifyFooter = [];
	 				var rows = rs.rows;
	 				var footer = rs.footer;
	 				if(!rows || rows.length <= 0)$('#datagrid').datagrid('loadData',{total:0,rows:[],footer:[]});
	 				
	 				for(var r in rows){
	 					var curRow = {};
	 					curRow["DELIVERCODE"] = eval("rows["+r+"].DELIVERCODE");
	 					curRow["DELIVERDES"] = eval("rows["+r+"].DELIVERDES");
	 					curRow["ZJ"] =  eval("rows["+r+"].ZJ")?eval("rows["+r+"].ZJ").toFixed(2):'0.00';
	 					for(var i in grpTypList){
		 					var grpList = grpTypList[i].grpList;
		 	  	 			for(var j in grpList){
		 	  	 				var typList = grpList[j].typList;
		 	  	 				for(var k in typList){
		 	  	 					var currentCol = typList[k].code;
		 	  	 					curRow["T_"+currentCol] =  eval("rows["+r+"].T_"+currentCol)?eval("rows["+r+"].T_"+currentCol).toFixed(2):'0.00';
		 		  	 			}
		 	  	 				curRow["XJ_"+grpList[j].code] =  eval("rows["+r+"].XJ_"+grpList[j].code)?eval("rows["+r+"].XJ_"+grpList[j].code).toFixed(2):'0.00';
		 	  	 			}
		 	  	 			curRow["HJ_"+grpTypList[i].code] =  eval("rows["+r+"].HJ_"+grpTypList[i].code)?eval("rows["+r+"].HJ_"+grpTypList[i].code).toFixed(2):'0.00';
		 	  	 		}
	 					modifyRows.push(curRow);
	 				}
	 				for(var r in footer){
	 					var curRow = {};
	 					curRow["DELIVERCODE"] = eval("footer["+r+"].DELIVERCODE");
	 					curRow["DELIVERDES"] = eval("footer["+r+"].DELIVERDES");
	 					curRow["ZJ"] =  eval("footer["+r+"].ZJ")?eval("footer["+r+"].ZJ").toFixed(2):'0.00';
	 					for(var i in grpTypList){
		 					var grpList = grpTypList[i].grpList;
		 	  	 			for(var j in grpList){
		 	  	 				var typList = grpList[j].typList;
		 	  	 				for(var k in typList){
		 	  	 					var currentCol = typList[k].code;
		 	  	 					curRow["T_"+currentCol] =  eval("footer["+r+"].T_"+currentCol)?eval("footer["+r+"].T_"+currentCol).toFixed(2):'0.00';
		 		  	 			}
		 	  	 				curRow["XJ_"+grpList[j].code] =  eval("footer["+r+"].XJ_"+grpList[j].code)?eval("footer["+r+"].XJ_"+grpList[j].code).toFixed(2):'0.00';
		 	  	 			}
		 	  	 			curRow["HJ_"+grpTypList[i].code] =  eval("footer["+r+"].HJ_"+grpTypList[i].code)?eval("footer["+r+"].HJ_"+grpTypList[i].code).toFixed(2):'0.00';
		 	  	 		}
	 					modifyFooter.push(curRow);
	 				}
	 				rs.rows = modifyRows;
	 				rs.footer = modifyFooter;
	 				//非固定列，第一列显示数据总行数
// 	 				rs.footer[0][columns[0].field] = rs.total;
	 				return $.toJSON(rs);
	 			},
				url:"<%=path%>/GysLeibieHuizongMisBoh/findDeliverCategorySum3.do",
				remoteSort: true,
				//页码选择项
				pageList:[20,30,40,50],
				frozenColumns:frozenHead,
				columns:head,
				queryParams:params,
				showFooter:true,
				rowStyler:function(){
					return 'line-height:11px';
				},
				pagination:true,
				rownumbers:true,
				onBeforeLoad:function(){
					if(!$("#firstLoad").val())
						return false;
				}
  	 		});
  	 		
	  	  	$("#firstLoad").val("true");
  	  	 	$(".panel-tool").remove();

  	  		$("#bdat,#edat").htmlUtils("setDate","now");
	 		$("#bdat,#edat").focus(function(){
	 			new WdatePicker();
	 		});
	 		
	 		$('#seachDeliver').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $("#delivercode").val();
// 					var defaultName = $("#deliver_name").val();
					var offset = getOffset('bdat');
					top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/deliver/searchAllDeliver.do?defaultCode='+defaultCode),offset,$('#deliver_name'),$('#delivercode'),'900','500','isNull');
				}
			});
	 		$('#seachDept').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#positn').val();
					var defaultName = $('#positn_name').val();
					//alert(defaultCode+"==="+defaultName);
					var offset = getOffset('positn');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/misbohcommon/findPositnSuper.do?sta=all&typn=7&iffirm=1&mold=oneTmany&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positn_name'),$('#positn'),'760','520','isNull');
				}
			});
		  	/*弹出树*/
			$('#seachTyp').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#typ').val();
					var offset = getOffset('bdat');
					top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/misbohcommon/selectMoreGrpTyp.do?defaultCode='+defaultCode),offset,$('#typdes'),$('#typ'),'320','460','isNull');
				}
			});
  	 	});

  	 </script>
  </body>
</html>
