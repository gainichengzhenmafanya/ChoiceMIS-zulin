<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
  	<style type="text/css">
  		.btn {
  			width: 120px;
  			height: 50px;
  		}
  		.easyui-linkbutton{
		  	width: 110px;
  			height: 0px;
  			margin-top: 10px;
  			margin-left: 5px;
  			
  		}
  		a.l-btn-plain{
			border:1px solid #7eabcd; 
		}
  	</style>
  </head>	
  <body>
  	<div style="width: 630px;">
  		<a href="<%=path %>/JgYujingBizhongMis/toPriceWarning.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="scm_jgyjhbz"/></a>
  		<a href="<%=path %>/CgJiageYidongMis/toPurchasePriceChange.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="scm_cgjgyd"/></a>
<%--   		<a href="<%=path %>/JhJiageBijiaoMis/toSupplyInPriceCompare.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo">进货价格比较</a> --%>
<%--   		<a href="<%=path %>/RkChengbenFenxiMis/toReceiptCostAnalysis.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo">入库成本分析</a> --%>
  	</div>
  	<div style="width: 760px;">
<%--   		<a href="<%=path %>/KcZhouzhuanLvMis/toKuCunZhouZhuanLv.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo">库存周转率</a> --%>
<%--   		<a href="<%=path %>/KlZongheFenxiMis/toInventoryAgingSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo">库龄综合分析</a> --%>
<%--   		<a href="<%=path %>/WzKulingMingxiMis/toSupplyKuLingDetail.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo">物资库龄明细</a> --%>
  	</div>
  	
  	
  	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
  	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
  	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
  	<script type="text/javascript">
  		/* function show(url){
  			window.location.href = url;
  		} */
  		$('a').click(function(event){
  			var self = $(this);
  			if(!self.attr("tabid")){
  				self.attr("tabid",self.text()+new Date().getTime());
  			}
  			event.preventDefault();
  			top.showInfo(self.attr("tabid"),self.text(),self.attr("href").replace("<%=path %>",""));
  		});
  	</script>
  </body>
</html>
