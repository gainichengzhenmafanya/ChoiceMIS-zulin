<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<style type="text/css">
		.text{
			font-size:12px,border:0px,line-height:20px,height:20px,padding:0px,*height:18px,*line-height:18px,_height:18px,_line-height:18px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		a.l-btn-plain{
			border:1px solid #7eabcd; 
		}
	</style>
  </head>	
  <body>
  	<div class="tool"></div>
  	<input id="firstLoad" type="hidden"/>
  	<input id="itcode" type="hidden"/>
  	<form id="queryForm" name="queryForm" method="post">
					<div class="form-line" style="z-index:9;">
					<div class="form-label"><fmt:message key="startdate"/></div>
					<div class="form-input"><input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/></div>
					<div class="form-label">分店</div>
					<div class="form-input">
						<input type="text"  id="positn_name"  name="firmDes" readonly="readonly"/>
						<input type="hidden" id="positn" name="firm"/>
						<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
					</div>
					<div class="form-label">类别</div>
					<div class="form-input">
<%-- 						<select name="typ" url="<%=path %>/grpTyp/findAllTyp.do" class="select"></select> --%>
						<select key="pubgrp" data="pgrpdes" id="payment" name="typ" url="<%=path%>/pubGrp/findPubGrp.do" class="select" select="true"></select>
					</div>
				</div>
				<div class="form-line" style="z-index:8;">
					<div class="form-label"><fmt:message key="enddate"/></div>
					<div class="form-input"><input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd"/>" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/></div>
					<div class="form-label"><fmt:message key="supplies_code"/></div>
					<div class="form-input">
						<input type="text" style="margin-top:0px;vertical-align:middle;" id="sp_code" name="sp_code" />
<%-- 						<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' /> --%>
					</div>
					<div class="form-label" >菜名</div>
					<div class="form-input">
						<input type="text" name="food" id="food" />
					</div>
				</div>
				<div class="form-line" style="z-index:7;">
					<div class="form-label" style="width:256px;">
						<input type="checkbox" name="exp0" value="exp0"/>过滤零值
						<input type="checkbox" id="onlycbk" name="onlycbk" value="onlycbk"/>仅做成本卡
						<input type="checkbox" name="doublefood" value="doublefood"/>双单位菜品
					</div>
					<div class="form-label" style="width:60px;"></div>
					<div class="form-input">
						<input type="radio" name="foodType" value="all" checked="checked"/>全部
						<input type="radio" name="foodType" value="normal" />正常
						<input type="radio" name="foodType" value="tuicai" />退菜
						<input type="radio" name="foodType" value="mianxiang" />免项菜
						<input type="radio" name="foodType" value="zengsong" />赠送菜
					</div>
				</div>
				<%--<div class="form-line" style="z-index:7;">
					<div class="form_label">
						<a style="width:110px;text-align: center;" id="titleKey" plain="true" iconCls="" class="easyui-linkbutton" onclick="javascript:findCostCard();">查看成本卡</a>
						<a style="width:110px;text-align: center;" id="titleKey" plain="true" iconCls="" class="easyui-linkbutton" onclick="javascript:findCostDetail();">菜品成本组成物资</a>
						<a style="text-align: center;" id="titleKey" plain="true" iconCls="" class="easyui-linkbutton" onclick="javascript:findSaleCostProfitTrends();">菜品销售成本利润走势分析</a>
						<a style="width:110px;text-align: center;" id="titleKey" plain="true" iconCls="" class="easyui-linkbutton" onclick="javascript:findCostInterval();">菜品成本区间分析</a>
						<a style="width:110px;text-align: center;" id="titleKey" plain="true" iconCls="" class="easyui-linkbutton" onclick="javascript:findMaolilvInterval();">菜品毛利率区间分析</a>
					</div>
				</div>--%>
			</form>
 	 <div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
  	 <script type="text/javascript">
  	 	var checkMis='${checkMis}';
  	 	var operateMap = '${operateMap}';
  	 	$(document).ready(function(){
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							var form = $("#queryForm").find("*[name]");
							form = form.filter(function(index){
								var cur = form[index];
								if($(cur).attr("name")){
									if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
										if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
											if($("input[name='"+$(cur).attr("name")+"']:checked").length){
												params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
											}else{
												params[$(cur).attr("name")] = undefined;
											}
										}else{
											params[$(cur).attr("name")] = $(cur).val();
										}
									}
								}
								
							});
							$("#datagrid").datagrid("reload");
						}
					},{
						text: 'Excel',
						title: 'Excel',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$('#queryForm').attr('action',"<%=path%>/firmMis/exportFirmFoodProfit.do");
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="print" />',
						title: '<fmt:message key="print" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							$('#queryForm').attr('target','report');
							window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
							var action="<%=path%>/firmMis/printFirmFoodProfit.do";
							$('#queryForm').attr('action',action);
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="column_selection" />',
						title: '<fmt:message key="column_selection" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-100px','-60px']
						},
						handler: function(){
							toColsChoose();
						}
					},{
                        text: '报表选项',
                        title: '报表选项',
                        useable:true,
                        icon: {
                            url: '<%=path%>/image/Button/op_owner.gif',
                            position: ['-100px','-60px']
                        },
                        items:[{
                            text: '查看成本卡',
                            title: '查看成本卡',
                            useable:true,
                            icon: {
                                url: '<%=path%>/image/Button/op_owner.gif',
                                position: ['-160px','-100px']
                            },
                            handler: function(){
                                findCostCard();
                            }
                        },{
                            text: '菜品成本组成物资',
                            title: '菜品成本组成物资',
                            useable:true,
                            icon: {
                                url: '<%=path%>/image/Button/op_owner.gif',
                                position: ['-160px','-100px']
                            },
                            handler: function(){
                                findCostDetail();
                            }
                        },{
                            text: '菜品销售成本利润走势分析',
                            title: '菜品销售成本利润走势分析',
                            useable:true,
                            icon: {
                                url: '<%=path%>/image/Button/op_owner.gif',
                                position: ['-160px','-100px']
                            },
                            handler: function(){
                                findSaleCostProfitTrends();
                            }
                        },{
                            text: '菜品成本区间分析',
                            title: '菜品成本区间分析',
                            useable:true,
                            icon: {
                                url: '<%=path%>/image/Button/op_owner.gif',
                                position: ['-160px','-100px']
                            },
                            handler: function(){
                                findCostInterval();
                            }
                    },{
                        text: '菜品毛利率区间分析',
                        title: '菜品毛利率区间分析',
                        useable:true,
                        icon: {
                            url: '<%=path%>/image/Button/op_owner.gif',
                            position: ['-160px','-100px']
                        },
                        handler: function(){
                            findMaolilvInterval();
                        }
                    }]

                },{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							//window.location.replace("<%=path%>/SupplyAcct/toWareReport.do?checkMis=mis");
							$(window.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').click();
						}
					}
				]
			});
  	 		var tableHeight = $(".main",top.document).parent("div[region='center']").height() - $(".tab-control",top.document).height() - $(".tool").height() - $("#queryForm").height();
  	 		$("select").each(function(){
  	 			$(this).htmlUtils("select");
  	 		});
  	 		$("#bdat,#edat").htmlUtils("setDate","now");
  	 		var des = $('#des'); 
  	 		$('#des').parent().html("").append(des);
  	 		$('#des').htmlUtils('select',[{key:'<fmt:message key="unqualified"/>',value:'<fmt:message key="unqualified"/>'}]);
  	 		//定义类型为Date的列，js将根据此变量解析Data类型数据默认解析成yyyy-MM-dd样式
  	 		var dateCols = [''];
  	 		//数字列
  	 		var numCols = ['incount',
						'junjia',
						'incount2',
						'infashenge',
						'inzhekou',
						'infuwufei',
						'inshuijin',
						'inmianxiang',
						'intaocan',
						'injine',
						'costmingxi',
						'costtiaoliao',
						'costfujia',
						'costheji',
						'maolimaoli',
						'maoliyuanmaoli',
						'maolimaolilv',
						'maoliyuanmaolilv',
						'maoliunitcost',
						'maoliunitmaoli'];
  	 		//收集form表单数据的对象
  	 		var params = {};
  	 		//Controller传来的map对象，包含所需要显示的报表所有列的dictColumns对象，和需要固定在左侧的col的index（用，分割）
  	 		var tableContent = {};
  	 		//表头行（单行）
  	 		var columns = [];
  	 		//表头（多行），其中元素为columns
  	 		var head = [];
  	 		//需要固定在左侧的列的表头（单行）
  	 		var frozenHead = [];
  	 		//需要固定在左侧的列的表头（多行），元素为frozenHead
  	 		var frozenColumns = [];
  	 		//ajax获取报表表头
  	 		$.ajax({url:"<%=path%>/firmMis/findFirmFoodProfitHeaders.do",
  	 				async:false,
  	 				success:function(data){
  	 					tableContent = data;
  	 				}
  	 			});
  	 	//解析获取的数据
  	 		var frozenIndex = [];
  	 		var Cols = [];
  	 		var colsSecond = [];
			var prev = '';
			var temp;
  	 		for(var i in tableContent.columns)Cols.push(tableContent.columns[i].zhColumnName);
  	 		var t = Cols.toString().match(/,([\d\D]+?)\|[\d\D]+?(?=,)/g);
  	 		if(t && !t.length){
  	 			for(var i in tableContent.columns){
  	 				var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
	  	 			if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
	  	 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
	  	 			else
	  	 				columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
	  	 		}
				head.push(columns);
	  	 		frozenHead.push(frozenColumns);
  	 		}else{
  	 			for(var i in tableContent.columns){
  	 				var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
  	 				if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
	  	 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
  	 				else{
  	 					var cur = tableContent.columns[i].zhColumnName.match(/^([\d\D]+)\|[\d\D]+$/g);
  	 					if(cur && cur.length){
  	 						var cur = tableContent.columns[i].zhColumnName;
  	 						if(cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1") == prev){
  	 							temp.colspan ++;
  	 						}else{
  	 							temp = {title:cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1"),colspan:1};
  	 							columns.push(temp);
  	 							prev = cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1");
  	 						}
  	 						colsSecond.push({field:tableContent.columns[i].columnName.toUpperCase(),title:cur.replace(/^([\d\D]+)\|([\d\D]+)$/g,"$2"),width:tableContent.columns[i].columnWidth,sortable:true,colspan:1,align:align});
  	 					}else{
  	 						if(tableContent.columns[i].columnName)
  	 							columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
  	 					}
  	 				}
  	 			}
  	 			head.push(columns);
  	 			head.push(colsSecond);
  	 			frozenHead.push(frozenColumns);
  	 		}
  	 		//生成报表数据表格
  	 		$("#datagrid").datagrid({
  	 			title:'分店菜品利润表',
  	 			width:'100%',
  	 			height:tableHeight,
  	 			nowrap: true,
				striped: true,
				singleSelect:true,
				collapsible:true,
				onDblClickRow:function(a,b){
					$('#itcode').val(b.SP_CODE);
					findCostCard();
				},
				onClickRow:function(a,b){
					$('#itcode').val(b.SP_CODE);
				},
				url:"<%=path%>/firmMis/findFirmFoodProfit.do",
				remoteSort: true,
				//页码选择项
				pageList:[20,30,40,50],
				frozenColumns:frozenHead,
				columns:head,
				queryParams:params,
				showFooter:true,
				rowStyler:function(){
					return 'line-height:11px';
				},
				pagination:true,
				rownumbers:true,
				onBeforeLoad:function(){
					if(!$("#firstLoad").val())
						return false;
				}
  	 		});
  	 		$("#firstLoad").val("true");
  	 		$(".panel-tool").remove();
  	 		$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/misbohcommon/selectSupplyLeft.do?sta=all&positn=1&defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
  	 		$('#seachPositn').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $("#positn").val();
					var defaultName = $("#positn_name").val();
					var offset = getOffset('bdat');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/addPositnBatch.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positn_name'),$('#positn'),'750','500','isNull');
				}
			});
  	 	});
  	 	//列选择
  	 	function toColsChoose(){
  	 		$('body').window({
				title: '<fmt:message key="column_selection"/>',
				content: '<iframe frameborder="0" src="<%=path%>/firmMis/toColChooseFirmFoodProfit.do"></iframe>',
				width: '460px',
				height: '430px',
				draggable: true,
				isModal: true
			});
  	 	}
  	 	//查看成本卡
  	 	function findCostCard(){
  	 		var itcode=$('#itcode').val();
  	 		if(null!=itcode && ""!=itcode){
	  	 		$('body').window({
					id: 'window_findCostCard',
					title: '菜品成本明细',
					content: '<iframe id="findCostCardFrame" frameborder="0" src="'+"<%=path%>/firmMis/findCostCard.do?itcode="+itcode+'"></iframe>',
					width: '1000px',
					height: '450px',
					draggable: true,
					isModal: true,
					confirmClose: false
				});
  	 		}else{
  	 			alert('请选择要查看的物资！');		
  	 		}
  	 	}
  	 	//菜品成本组成物资明细
  	 	function findCostDetail(){
  	 		var itcode=$('#itcode').val();
  	 		var bdat=$('#bdat').val();
  	 		var edat=$('#edat').val();
  	 		if(null!=itcode && ""!=itcode){
	  	 		$('body').window({
					id: 'window_findCostDetail',
					title: '项目成本明细',
					content: '<iframe id="findCostDetailFrame" frameborder="0" src="'+"<%=path%>/firmMis/findCostDetail.do?itcode="+itcode+"&bdat="+bdat+"&edat="+edat+'"></iframe>',
					width: '700px',
					height: '450px',
					draggable: true,
					isModal: true,
					confirmClose: false
				});
  	 		}else{
  	 			alert('请选择要查看的物资！');		
  	 		}
  	 	}
  	 	
  	 	//菜品销售成本利润走势分析
  	 	function findSaleCostProfitTrends(){
  	 		var itcode=$('#itcode').val();
  	 		var bdat=$('#bdat').val();
  	 		var edat=$('#edat').val();
  	 		var onlycbk='';
  	 		if($('#onlycbk').attr('checked')=='checked'){
  	 			onlycbk='onlycbk';
  	 		}
  	 		if(null!=itcode && ""!=itcode){
	  	 		$('body').window({
					id: 'window_findSaleCostProfitTrends',
					title: '菜品销售成本利润走势分析',
					content: '<iframe id="findSaleCostProfitTrendsFrame" frameborder="0" src="'+"<%=path%>/firmMis/findSaleCostProfitTrends.do?itcode="+itcode+"&bdat="+bdat+"&edat="+edat+"&onlycbk="+onlycbk+'"></iframe>',
					width: '1000px',
					height: '450px',
					draggable: true,
					isModal: true,
					confirmClose: false
				});
  	 		}else{
  	 			alert('请选择要查看的物资！');		
  	 		}
  	 	}
  	 	//菜品成本区间分析
  	 	function findCostInterval(){
  	 		var itcode=$('#itcode').val();
  	 		var bdat=$('#bdat').val();
  	 		var edat=$('#edat').val();
  	 		var onlycbk='';
  	 		if($('#onlycbk').attr('checked')=='checked'){
  	 			onlycbk='onlycbk';
  	 		}
  	 		if(null!=itcode && ""!=itcode){
	  	 		$('body').window({
					id: 'window_findCostInterval',
					title: '菜品成本区间分析',
					content: '<iframe id="findCostIntervalFrame" frameborder="0" src="'+"<%=path%>/firmMis/findCostInterval.do?itcode="+itcode+"&bdat="+bdat+"&edat="+edat+"&onlycbk="+onlycbk+'"></iframe>',
					width: '1000px',
					height: '450px',
					draggable: true,
					isModal: true,
					confirmClose: false
				});
  	 		}else{
  	 			alert('请选择要查看的物资！');		
  	 		}
  	 	}
  	 	//菜品毛利率区间分析
  	 	function findMaolilvInterval(){
  	 		var itcode=$('#itcode').val();
  	 		var bdat=$('#bdat').val();
  	 		var edat=$('#edat').val();
  	 		var onlycbk='';
  	 		if($('#onlycbk').attr('checked')=='checked'){
  	 			onlycbk='onlycbk';
  	 		}
  	 		if(null!=itcode && ""!=itcode){
	  	 		$('body').window({
					id: 'window_findMaolilvInterval',
					title: '菜品毛利率区间分析',
					content: '<iframe id="findMaolilvIntervalFrame" frameborder="0" src="'+"<%=path%>/firmMis/findMaolilvInterval.do?itcode="+itcode+"&bdat="+bdat+"&edat="+edat+"&onlycbk="+onlycbk+'"></iframe>',
					width: '1000px',
					height: '450px',
					draggable: true,
					isModal: true,
					confirmClose: false
				});
  	 		}else{
  	 			alert('请选择要查看的物资！');		
  	 		}
  	 	}
  	 </script>
  </body>
</html>
