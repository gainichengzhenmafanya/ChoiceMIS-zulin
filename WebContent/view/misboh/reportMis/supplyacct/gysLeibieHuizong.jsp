<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<style type="text/css">
		.text{
			font-size:12px,border:0px,line-height:20px,height:20px,padding:0px,*height:18px,*line-height:18px,_height:18px,_line-height:18px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 7%;
		}
		form .form-line .form-input{
			width: 16%;
		}
		form .form-line .form-input input[type=text]{
			width: 80%;
		}
		form .form-line .form-input select{
			width: 82%;
		}
	</style>
  </head>	
  <body>
  	<div class="tool"></div>
  	<input id="firstLoad" type="hidden"/>
  	<form id="queryForm" name="queryForm" method="post">
					<div class="form-line"  style="z-index:10;">
					<div class="form-label"><fmt:message key="startdate"/></div>
					<div class="form-input"><input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" /></div>
<%-- 					<div class="form-label"><fmt:message key="positions"/></div> --%>
<!-- 					<div class="form-input"> -->
<!-- 					</div> -->
					<div class="form-label"><fmt:message key="positions"/></div>
					<div class="form-input">
						<input type="text" name="positn_name" id="positn_name" style="margin-top: -3px;" autocomplete="off" class="text" value="<c:out value="${supplyAcct.positn}" />"/>
						<img id="seachDept" class="search" src="<%=path%>/image/themes/icons/search.png" alt='查询仓位' />
						<input type="hidden" id="positn" name="positn" value="${supplyAcct.positn }"/>
					</div>
					<div class="form-label"><fmt:message key="bigClass"/></div>
					<div class="form-input">
						<select id="grptyp" name="grptyp" url="<%=path %>/grpTyp/findAllGrpTyp.do"  class="select"></select>
					</div>
					<div class="form-input" style="width:210px;">
						<input type="radio" checked="checked" name="grpdes" value="grptyp"/><fmt:message key="a"/>
						<input type="radio" name="grpdes" value="grp"/><fmt:message key="b"/>
						<input type="radio" name="grpdes" value="typ"/><fmt:message key="c"/>
						<input type="checkbox" id="showtyp" name="showtyp" value="1"/><fmt:message key="Contain_transfer"/>
					</div>
				</div>
				<div class="form-line"  style="z-index:9;">
					<div class="form-label"><fmt:message key="enddate"/></div>
					<div class="form-input"><input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd"/>"/></div>
					<div class="form-label"><fmt:message key="suppliers"/></div>
						<div class="form-input">
						<input type="text"  id="deliver_name"  name="deliver_name" readonly="readonly" value="" class="text" style="margin-top: -3px;"/>
						<input type="hidden" id="delivercode" name="delivercode" value=""/>
						<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_suppliers"/>' />
						<%-- <select  name="delivercode" url="<%=path %>/deliver/findAllDeliver.do"   class="select"></select> --%>
					</div>
					<div class="form-label"><fmt:message key="middleClass"/></div>
					<div class="form-input"><select id="grp" name="grp" url="<%=path %>/grpTyp/findAllGrp.do"   class="select"></select></div>
					<div class="form-label"><fmt:message key="document_types"/></div>
					<div class="form-input"><select id="chktyp" name="chktyp" url="<%=path %>/misbohcommon/findAllBillType.do?codetyp=RK,ZF" class="select"></select></div>
				</div>
			</form>
 	<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  		
  	 	$(document).ready(function(){

  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							var form = $("#queryForm").find("*[name]");
							form = form.filter(function(index){
								var cur = form[index];
								if($(cur).attr("name")){
									if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
										if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
											if($("input[name='"+$(cur).attr("name")+"']:checked").length){
												params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
											}else{
												params[$(cur).attr("name")] = undefined;
											}
										}else{
											params[$(cur).attr("name")] = $(cur).val();
										}
									}
								}
								
							});
							$("#datagrid").datagrid("load");
						}
					},{
						text: 'Excel',
						title: 'Excel',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$('#queryForm').attr('action',"<%=path%>/GysLeibieHuizongMisBoh/exportDeliverCategorySum.do");
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="print" />',
						title: '<fmt:message key="print" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							$('#queryForm').attr('target','report');
							window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
							var action="<%=path%>/GysLeibieHuizongMisBoh/printDeliverCategorySum.do";
							$('#queryForm').attr('action',action);
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="column_selection" />',
						title: '<fmt:message key="column_selection" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-100px','-60px']
						},
						handler: function(){
							toColsChoose();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							$(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').click();
						}
					}
				]
			});
  	 		var tableHeight = $(".main",top.document).parent("div[region='center']").height() - $(".tab-control",top.document).height() - $(".tool").height() - $("#queryForm").height();
  	 		$("select").each(function(){
  	 			$(this).htmlUtils("select");
  	 		});
  	 		$("#bdat,#edat").htmlUtils("setDate","now");
  	 		var des = $('#des'); 
  	 		$('#des').parent().html("").append(des);
  	 		$('#des').htmlUtils('select',[{key:'<fmt:message key="unqualified"/>',value:'<fmt:message key="unqualified"/>'}]);
  	 		//定义类型为Date的列，js将根据此变量解析Data类型数据默认解析成yyyy-MM-dd样式
  	 		var dateCols = ['dat'];
  	 		//数字列
  	 		var numCols = ['amtin','amtintax'];
  	 		//收集form表单数据的对象
  	 		var params = {};
  	 		//Controller传来的map对象，包含所需要显示的报表所有列的dictColumns对象，和需要固定在左侧的col的index（用，分割）
  	 		var tableContent = {};
  	 		//表头行（单行）
  	 		var columns = [];
  	 		//表头（多行），其中元素为columns
  	 		var head = [];
  	 		//需要固定在左侧的列的表头（单行）
  	 		var frozenHead = [];
  	 		//需要固定在左侧的列的表头（多行），元素为frozenHead
  	 		var frozenColumns = [];
  	 		//ajax获取报表表头
  	 		$.ajax({url:"<%=path%>/GysLeibieHuizongMisBoh/findDeliverCategorySumHeaders.do",
  	 				async:false,
  	 				success:function(data){
  	 					tableContent = data;
  	 				}
  	 			});
  	 		//解析获取的数据
  	 		var frozenIndex = tableContent.frozenColumns.split(',');
  	 		var Cols = [];
  	 		var colsSecond = [];
			var prev = '';
			var temp;
  	 		for(var i in tableContent.columns)Cols.push(tableContent.columns[i].zhColumnName);
  	 		var t = Cols.toString().match(/,([\d\D]+?)\|[\d\D]+?(?=,)/g);
  	 		if(t && !t.length){
  	 			for(var i in tableContent.columns){
  	 				var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
	  	 			if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
	  	 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
	  	 			else
	  	 				columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
	  	 		}
				head.push(columns);
	  	 		frozenHead.push(frozenColumns);
  	 		}else{
  	 			for(var i in tableContent.columns){
  	 				var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
  	 				if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
	  	 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
  	 				else{
  	 					var cur = tableContent.columns[i].zhColumnName.match(/^([\d\D]+)\|[\d\D]+$/g);
  	 					if(cur && cur.length){
  	 						var cur = tableContent.columns[i].zhColumnName;
  	 						if(cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1") == prev){
  	 							temp.colspan ++;
  	 						}else{
  	 							temp = {title:cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1"),colspan:1};
  	 							columns.push(temp);
  	 							prev = cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1");
  	 						}
  	 						colsSecond.push({field:tableContent.columns[i].columnName.toUpperCase(),title:cur.replace(/^([\d\D]+)\|([\d\D]+)$/g,"$2"),width:tableContent.columns[i].columnWidth,sortable:true,colspan:1,align:align});
  	 					}else{
  	 						if(tableContent.columns[i].columnName)
  	 							columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
  	 					}
  	 				}
  	 			}
  	 			head.push(columns);
  	 			head.push(colsSecond);
  	 			frozenHead.push(frozenColumns);
  	 		}
  	 		//收集表单参数
  	 		var form = $("#queryForm").find("*[name]");
			form = form.filter(function(index){
				var cur = form[index];
				if($(cur).attr("name")){
					if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
						if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
							if($("input[name='"+$(cur).attr("name")+"']:checked").length){
								params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
							}
						}else{
							params[$(cur).attr("name")] = $(cur).val();
						}
					}
				}
				
			});
						  	 		
  	 		
  	 		//生成报表数据表格
  	 		$("#datagrid").datagrid({
  	 			title:'<fmt:message key="supplier_category_summary"/>',
  	 			width:'100%',
  	 			height:tableHeight,
  	 			nowrap: true,
				striped: true,
				singleSelect:true,
				collapsible:true,
				//对从服务器获取的数据进行解析格式化
	 			dataFilter:function(data,type){
	 				var rs = eval("("+data+")");
	 				var modifyRows = [];
	 				var rows = rs.rows;
	 				if(!rows || rows.length <= 0)$('#datagrid').datagrid('loadData',{total:0,rows:[],footer:[]});
	 				for(var i in rows){
	 					var cols = tableContent.columns;
						var curRow = {};
	 					for(var j in cols){
	 						try{
	 							var value = eval("rows["+i+"]."+cols[j].properties.toUpperCase()) ;
	 							value = value ? ($.inArray(cols[j].properties,dateCols) >= 0 ? convertDate(value) : value):($.inArray(cols[j].properties,numCols) < 0 ? '':0);
	 							curRow[cols[j].columnName.toUpperCase()] = value;
	 						}catch(e){
	 							alert('Exception'+"rows["+i+"]."+cols[j].properties);
	 						}
	 					}
	 					modifyRows.push(curRow);
	 				}
	 				rs.rows = [];
	 				//当前列delivercode
	 				var currentCol = '';
	 				//当前delivercode小计
	 				var currentTotal = 0;
	 				var currentTotaltax = 0;
	 				for(var i in modifyRows){
	 					var cur = modifyRows[i].DELIVERCODE;
	 					modifyRows[i].AMTIN = modifyRows[i].AMTIN.toFixed(2);
	 					modifyRows[i].AMTINTAX = modifyRows[i].AMTINTAX.toFixed(2);
	 					if(cur != currentCol){
	 						if(currentTotal){
	 							rs.rows.push({
	 								DELIVERCODE:'<fmt:message key="subtotal"/>',
	 								AMTIN:currentTotal.toFixed(2),
	 								AMTINTAX:currentTotaltax.toFixed(2)
	 							});
	 							currentTotal = 0;
	 							currentTotaltax = 0;
	 						}
	 						rs.rows.push(modifyRows[i]);
	 						currentTotal += modifyRows[i].AMTIN ? Number(modifyRows[i].AMTIN) : 0;
	 						currentTotaltax += modifyRows[i].AMTINTAX ? Number(modifyRows[i].AMTINTAX) : 0;
	 						currentCol = cur;
	 					}else{
	 						modifyRows[i].DELIVERCODE = '';
	 						modifyRows[i].DELIVERDES = '';
	 						currentTotal += modifyRows[i].AMTIN ? Number(modifyRows[i].AMTIN) : 0;
	 						currentTotaltax += modifyRows[i].AMTINTAX ? Number(modifyRows[i].AMTINTAX) : 0;
	 						rs.rows.push(modifyRows[i]);
	 					}
	 					if(i == modifyRows.length-1)rs.rows.push({
	 						DELIVERCODE:'<fmt:message key="subtotal"/>',
							AMTIN:currentTotal.toFixed(2),
							AMTINTAX:currentTotaltax.toFixed(2)
	 					});
	 				}
	 				//非固定列，第一列显示数据总行数
	 				rs.footer[0][columns[0].field] = '<fmt:message key="total"/>';
	 				return $.toJSON(rs);
	 			},
				url:"<%=path%>/GysLeibieHuizongMisBoh/findDeliverCategorySum.do",
				remoteSort: true,
				//页码选择项
				pageList:[20,30,40,50],
				frozenColumns:frozenHead,
				columns:head,
				queryParams:params,
				rowStyler:function(){
					return 'line-height:11px';
				},
				showFooter:true,
				rownumbers:true,
				onBeforeLoad:function(){
					if(!$("#firstLoad").val())
						return false;
				},
				onDblClickRow:function(index,data){
					if(data['DELIVERCODE'] == "小计") return false;
  	 				var delivercode = data['DELIVERCODE'];
  	 				delivercode = getData("datagrid",index,delivercode);
  	 				var deliverdes = data['DELIVERDES'];
  	 				deliverdes = getData2("datagrid",index,deliverdes);
  	 				
  	 				var grptyp = data['GRP'];
  	 				grptyp = getData("datagrid",index,grptyp);
  	 				var grptypdes = data['GRPDES'];
  	 				grptypdes = getData2("datagrid",index,grptypdes);
  	 				
  	 				var dl = '',dldes = '',zl = '',zldes = '',xl = '',xldes = '';
  	 				var grpdes = $('input[name="grpdes"]:checked').val();
  	 				if(grpdes == 'grptyp'){
  	 					dl = grptyp;
  	 					dldes = grptypdes;
  	 				}else if(grpdes == 'grp'){
  	 					zl = grptyp;
  	 					zldes = grptypdes;
  	 				}else if(grpdes == 'typ'){
  	 					xl = grptyp;
  	 					xldes = grptypdes;
  	 				}
  	 				
					var grpname = typeof($("#grp").data("checkedName"))!="undefined"?$("#grp").data("checkedName"):"";
  	 				var chktypname = typeof($("#chktyp").data("checkedName"))!="undefined"?$("#chktyp").data("checkedName"):"";
					var params = {"delivercode":delivercode,"deliverdes":deliverdes,"bdat":$("#bdat").val(),"edat":$("#edat").val(),
							"positn":$('#positn').val(),"positndes":$('#positn_name').val(),"chktyp":chktypname,
							"grptyp":dl,"grptypdes":dldes,"grp":zl,"grpdes":zldes,"typ":xl,"typdes":xldes};
					openTag("chkindetailS","<fmt:message key='warehousing_detail_query'/>","<%=path%>/RkMingxiChaxunMisBoh/toChkinDetailS.do",params);
  	 			}
  	 		});
  	 		$("#firstLoad").val("true");
  	 		
  	 		$("#bdat,#edat").focus(function(){
  	 			new WdatePicker();
  	 		});
  	 		
  	 		$(".panel-tool").remove();
  	 		$('#seachDeliver').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $("#delivercode").val();
					var defaultName = $("#deliver_name").val();
					var offset = getOffset('bdat');
					top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/deliver/searchAllDeliver.do?defaultCode='+defaultCode),offset,$('#deliver_name'),$('#delivercode'),'900','500','isNull');
				}
			});
  	 		
  	 		$('#seachDept').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#positn').val();
					var defaultName = $('#positn_name').val();
					//alert(defaultCode+"==="+defaultName);
					var offset = getOffset('positn');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/misbohcommon/findPositnSuper.do?sta=all&typn=7&iffirm=1&mold=oneTmany&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positn_name'),$('#positn'),'760','520','isNull');
				}
			});
  	 	});
  	 	
  	 	function toColsChoose(){
  	 		$('body').window({
				title: '<fmt:message key="column_selection"/>',
				content: '<iframe frameborder="0" src="<%=path%>/SupplyAcctMisBoh/toColumnsChoose.do?reportName=${reportName}"></iframe>',
				width: '460px',
				height: '430px',
				draggable: true,
				isModal: true
			});
  	 	}
  		//一列多行相同时除第一个外剩余行都隐藏，选择时获取第一个行此列不为空的值
		function getData(id,index,dat){
				var start = index;
				//如果双击的行key列为空，向上找直到找到第一个key列不为空的行
				while(index>=0 && !dat){
					$("#"+id).datagrid("selectRow",index);
					dat = $("#"+id).datagrid("getSelected")['DELIVERCODE'];
 	 				index--;
				}
 	  			//返回到被双击的行
	  			$("#"+id).datagrid("selectRow",start);
 	  		return dat;
		}
		//一列多行相同时除第一个外剩余行都隐藏，选择时获取第一个行此列不为空的值
		function getData2(id,index,dat){
				var start = index;
				//如果双击的行key列为空，向上找直到找到第一个key列不为空的行
				while(index>=0 && !dat){
					$("#"+id).datagrid("selectRow",index);
					dat = $("#"+id).datagrid("getSelected")['DELIVERDES'];
 	 				index--;
				}
 	  			//返回到被双击的行
	  			$("#"+id).datagrid("selectRow",start);
 	  		return dat;
		}
  	 </script>
  </body>
</html>
