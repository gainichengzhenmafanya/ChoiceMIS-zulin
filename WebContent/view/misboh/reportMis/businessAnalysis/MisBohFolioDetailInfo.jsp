<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title><fmt:message key="bill_detail_report" /></title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
		.form-line .form-label{
			width: 8%;
		}
		.select{
			margin-top: 2px;
			width: 89%;
		}
	</style>
  </head>	
  <body>
  	<%@ include file="../share/permission.jsp"%>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }"/>
			<div class="form-label"><fmt:message key="startdate" /></div>
			<div class="form-input"><input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" pattern="yyyy-MM-dd"/></div>
			<div class="form-label"><fmt:message key="enddate" /></div>
			<div class="form-input"><input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text"/></div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="bill_no" /></div>
			<div class="form-input">
				<input type="text" id="vbcode" name="vbcode" value="" class="text" />
			</div>
			<div class="form-label"><fmt:message key="employees" /><fmt:message key="number" /></div>
			<div class="form-input">
				<input type="text" id="vecode" name="vecode" value="" class="text" />
			</div>
			<div class="form-label"><fmt:message key="bill" /><fmt:message key="type" /></div>
			<div class="form-input">
				<select name="vorclass" id="vorclass" class="select">
					<option></option>
					<option value="1"><fmt:message key="The_whole_list" />OK</option>
					<option value="2"><fmt:message key="The_entire_single_back" /></option>
					<option value="3"><fmt:message key="Single_product_return" /></option>
					<option value="4"><fmt:message key="waste" /></option>
					<option value="5"><fmt:message key="Dietary" /></option>
					<option value="6"><fmt:message key="discount" /></option>
					<option value="7"><fmt:message key="return_money" /></option>
				</select>
			</div>
		</div>
	</form>
	
	<input type="hidden" id="reportName" value="zdmxdet"/>
	<input type="hidden" id="excelUrl" value="<%=path%>/MISBOHbusinessAnalysis/exportReport.do"/>
	<input type="hidden" id="printUrl" value="<%=path%>/MISBOHbusinessAnalysis/printReport.do"/>
	<input type="hidden" id="dataUrl" value="<%=path%>/MISBOHbusinessAnalysis/queryFolioDetailInfo.do"/>
	<input type="hidden" id="title" value='<fmt:message key="bill_detail_report" />'/>
	<div id="datagrid"></div>
	
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script><!-- 兼容浏览器ie 用1.7报错参数无效 -->
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script language="JavaScript" src="<%=path%>/Charts/FusionCharts.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		//默认<fmt:message key="time" />
  	 		$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
  	 		$("#bdat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
		    });
		    $("#edat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
		    });
  	 		//为tab添加<fmt:message key="select1" /><fmt:message key="event" />
			initTab(false);
			$("#mode").change(function(){
				$("#firmid,#firmdes").val("");
			});
  	 	});
  		//初始化<fmt:message key="selected" />标签<fmt:message key="content" />
  	 	function initTab(rebuild){
  	 		var excelUrl = $("#excelUrl").val();
  	 		var printUrl = $("#printUrl").val();
  	 		var reportName = $("#reportName").val();
  	 		var dataUrl = $("#dataUrl").val();
  	 		var title = $("#title").val();
  	 		var grid = $("#datagrid");
  	 		
  	 		//生成工具栏
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp:true,
  	 			excelUrl:excelUrl+'?reportName='+reportName,
  	 			printUrl:printUrl+'?reportName='+reportName,
  	 			toolbar:['search','excel','exit'],
  	 			verifyFun:function(){
  	 				var flag = true;
  	 				return flag;
  	 			},
  	 			searchFun:function(grid,form){
  	 				getParam(form);
  	 				initTab(true);
  	 				grid.datagrid('load');
  	 			}
  	 		});
//   	 		getParam($('#queryForm'));
  	 		//生成表格
  	 		if(rebuild || grid.css('display') != 'none'){
  	  			firstLoad = true;//重置第一<fmt:message key="secondary" />加载标志
  	  			var numcloss = ['nmoney','nbzero','nzmoney','nymoney'];
	  	  		builtTable({
	  	 			headUrl:"<%=path%>/MISBOHbusinessAnalysis/findHeaderForZdmx.do",
	  	 			dataUrl:dataUrl,
	  	 			title:title,
	  	 			grid:grid,
	  	 			numCols:numcloss,
	  	 			createHeader:function(data,head,frozenHead){
	  	 				var actTyp = data.actTyp;
	  	 				var payment = data.payment;
// 	  	 				alert($.toJSON(payment));
	  	 				var col1 = [];
	  	 				var col2 = [];
	  	 				var yhname = '<fmt:message key="preferential" /><fmt:message key="category" />';
	  	 				var curCol1 = undefined;
	  	 				var mod = undefined;
// 	  	 				{field:'CANBIE',width:50,title:'开始市别 ',align:'left',rowspan:2},
	  	 				frozenHead.push([{field:'DWORKDATE',width:75,title:'<fmt:message key="date" />',align:'left',rowspan:2},
	  	 				                 {field:'VBCODE',width:230,title:'<fmt:message key="bill_no" />',align:'left',rowspan:2}]);
	  	 				col1.push({field:'DBRTIME',width:60,title:'<fmt:message key="creation_time" />',align:'left',rowspan:2});
	  	 				col1.push({field:'DORTIME',width:60,title:'<fmt:message key="checkout_time" />',align:'left',rowspan:2});
// 						col1.push({field:'AREARNAME',width:60,title:'消费<fmt:message key="area" />',align:'left',rowspan:2});
						col1.push({field:'SITDES',width:60,title:'<fmt:message key="desk_no" />',align:'left',rowspan:2});
						col1.push({field:'IPEOLENUM',width:60,title:'<fmt:message key="eat_people_num" />',align:'right',rowspan:2});
						col1.push({field:'NMONEY',width:60,title:'<fmt:message key="item_amt" />',align:'right',rowspan:2});
						col1.push({field:'NBZERO',width:60,title:'<fmt:message key="small_change" />',align:'right',rowspan:2});
						col1.push({field:'NZMONEY',width:60,title:'<fmt:message key="discount_deals" />',align:'right',rowspan:2});
	  	 				
	  	 				for(var cur in actTyp){
	  	 					if(mod == yhname){
	  	 						curCol1.colspan += 1;
	  	 					}else{
	  	 						curCol1 = {title:yhname,colspan:1};
	  							col1.push(curCol1);
	  	 						mod = yhname;
	  	 					}
	  	 					col2.push({field:"AMT"+actTyp[cur].vcode,width:60,title:actTyp[cur].vname,align:'right'});
	  	 				}
	  	 				for(var pay in payment){
							mod='<fmt:message key="payment" />';	
							if(payment[pay].ivalue==1000) {
								continue;////如果是币种跳出本次循环
							}else{
		  	 					if(mod == yhname){
		  	 						curCol1.colspan += 1;
		  	 					}else{
		  	 						yhname=mod;
		  	 						curCol1 = {title:yhname,colspan:1};
		  							col1.push(curCol1);
		  	 					}
		  	 					col2.push({field:"PAYMENT"+payment[pay].vcode,width:60,title:payment[pay].vname,align:'right'});
							}
	  	 				}
		  	 			col1.push({field:"ZSCOUNT",width:60,title:'<fmt:message key="gift_num" />',rowspan:2,align:'right'});
		  	 			col1.push({field:"ZSMONEY",width:60,title:'<fmt:message key="gift_amt" />',rowspan:2,align:'right'});
		  	 			col1.push({field:"NYMONEY",width:60,title:'<fmt:message key="real_total" />',rowspan:2,align:'right'});
		  	 			col1.push({field:"OPENINVOICE",width:70,title:'<fmt:message key="whether_invoicing" />',rowspan:2,align:'left'});
		  	 			col1.push({field:"VNAME",width:60,title:'<fmt:message key="operator" />',rowspan:2,align:'left'});
		  	 			col1.push({field:"VANTICLASS",width:60,title:'<fmt:message key="return_money" />',rowspan:2,align:'left'});
		  	 			col1.push({field:"NMONEYV",width:80,title:'<fmt:message key="before_return_money" />',rowspan:2,align:'right'});
		  	 			col1.push({field:"FANWEISHOUQUAN",width:60,title:'<fmt:message key="authorizer" />',rowspan:2,align:'left'});
		  	 			col1.push({field:"JDCOUNT",width:60,title:'<fmt:message key="add_bill_num" />',rowspan:2,align:'right'});
		  	 			col1.push({field:"JDMONEY",width:60,title:'<fmt:message key="add_bill_amt" />',rowspan:2,align:'right'});
		  	 			col1.push({field:"TDCOUNT",width:60,title:'<fmt:message key="back_bill_num" />',rowspan:2,align:'right'});
		  	 			col1.push({field:"TDMONEY",width:60,title:'<fmt:message key="back_bill_amt" />',rowspan:2,align:'right'});
		  	 			col1.push({field:"QIANDANJL",width:60,title:'<fmt:message key="manager" />',rowspan:2,align:'left'});
	  	 				head.push(col1);
	  	 				head.push(col2);
	  	 			},
	  	 			onDblClickRow:function(index,data){
	  	 				var vbcode =data["VBCODE"]; //账单号
	  	 				var DWORKDATE =data["DWORKDATE"]; //账单号
	  	 			//	var bdat = $("#bdat").val(); //日期
	  	 				//var edat = $("#edat").val();
	  	 				if(window.parent.tabMain.getItem('tab_FolioDetail')){
	  	 					window.parent.tabMain.close('tab_FolioDetail');
	  	 				}
	  	 				showInfo('FolioDetail','<fmt:message key="bill_detail_analysis" />','/MISBOHbusinessAnalysis/toReport.do?reportName=zdmxfx&vbcode='+vbcode+'&bdat='+DWORKDATE+'&edat='+DWORKDATE);
	  	 			}
	  	 			
	  	 		});
  	 		}
	 	 		
  	 	}
  		
  	 	function showInfo(moduleId,moduleName,moduleUrl){
  	 		window.parent.tabMain.addItem([{
	  				id: 'tab_'+moduleId,
	  				text: moduleName,
	  				title: moduleName,
	  				closable: true,
	  				content: '<iframe id="iframe_'+moduleId+'" name="iframe_'+moduleId+'" frameborder="0" src="<%=path%>'+moduleUrl+'"></iframe>'
	  			}
	  		]);
  	 		window.parent.tabMain.show('tab_'+moduleId);
  	 	}
  	 	
  	 	//列<fmt:message key="select1" />后页面重新加载
  		function pageReload(){
  			closeColChooseWin();
  			initTab(true);
//   			$("#datagrid").datagrid('reload');
  		}
  		
  		
  	 </script>
  </body>
</html>
