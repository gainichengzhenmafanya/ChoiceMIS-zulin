<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title><fmt:message key="bill_detail_analysis" /></title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
		.leftDiv{
			float: left;
			height: 100%;
			width: 120px;
			padding-left: 20px;
			line-height: 30px;
			padding-top: 30px;
		}
		.chartdiv{
			height: 100%;
		}
		.form-label2{
			width: 40px;
			height: 25px;
			position: relative;
			margin: 0;
			float: left;
			text-align: right;
			vertical-align: middle;
			line-height: 25px;
			margin-right: 5px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 6.5%;
		}
		form .form-line .form-input{
			width: 13%;
		}
		form .form-line .form-input input[type=text]{
			width: 85%;
		}
	</style>
  </head>	
  <body>
  	<%@ include file="../share/permission.jsp"%>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label"><fmt:message key="business_day" /></div>
			<div class="form-input">
				<input type="text" id="bdat" name="bdat" class="text" value="${bdat }" readonly="readonly" />
			</div>
			<div class="form-label"><fmt:message key="bill_no" /></div>
			<div class="form-input">
				<input type="text" id="vbcode" name="vbcode" class="text" value="${vcode }" style="width:150px" readonly="readonly" />
			</div>
		</div>
	</form>
	<input type="hidden" id="reportName" value="zdmxfx"/>
	<input type="hidden" id="excelUrl" value="<%=path%>/MISBOHbusinessAnalysis/exportReport.do"/>
	<input type="hidden" id="printUrl" value="<%=path%>/MISBOHbusinessAnalysis/printReport.do"/>
	<input type="hidden" id="dataUrl" value="<%=path%>/MISBOHbusinessAnalysis/queryZdMxfx.do"/>
	<input type="hidden" id="title" value='<fmt:message key="bill_detail_analysis" />'/>
	<div id="gridShow">
		<div id="datagrid"></div>
		<div id="payShow"></div>
	</div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 var code = "";
  	 	$(document).ready(function(){
  	 		
          	//账单查询跳转到此
  	 		if('${vcode}'&&code==''){
  	 			$('#bdat').val('${bdat}');
  	 			$('#edat').val('${edate}');
  	 			$('#vbcode').val('${vcode}');
  	 			initTab(true);
  	 		}
  	 	});
  	 	
  		//初始化<fmt:message key="selected" />标签<fmt:message key="content" />
  	 	function initTab(rebuild){
  	 		var excelUrl = $("#excelUrl").val();
  	 		var printUrl = $("#printUrl").val();
  	 		var reportName = $("#reportName").val();
  	 		var dataUrl = $("#dataUrl").val();
  	 		var title = $("#title").val();
  	 		var grid = $("#datagrid");
  	 		var heightDiv = $("#gridShow").height()==0?516:$("#gridShow").height();
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp:true,
  	 			excelUrl:excelUrl+'?reportName='+reportName,
  	 			printUrl:printUrl+'?reportName='+reportName,
  	 			colsChooseUrl:'<%=path%>/MISBOHbusinessAnalysis/toColumnsChoose.do?reportName='+reportName,
  	 			toolbar:['excel','exit'],
  	 			searchFun:function(){
  	 				$('#datagrid').datagrid('load',getParam($("#queryForm")));
  	 			}
  	 		});
  	 		
  	 		builtTable({
  	 			headUrl:"<%=path%>/MISBOHbusinessAnalysis/findHeaders.do?reportName="+reportName,
  	 			dataUrl:dataUrl,
  	 			title:title,
  	 			grid:grid,
  	 			settimeout:1,
  	 			height:heightDiv/2,
  	 			alignCols:['serial','pax','foliono','sft','cnt','name','code','prncnt'],
  	 			numCols:['nprice','ncount','nmoney','nzmoney','nymoney','nbzero']
  	 		});
  	 		$('#datagrid').datagrid('load',getParam($("#queryForm")));
  	 		$('#datagrid').datagrid({
  	 			onLoadSuccess:function(data){
  	 				for(var i=1; data.rows.length >= i; i++){
  	 					if(i == data.rows.length){
  	 						initTab2();
  	 					}
  	 				}
  	 			}
  	 		});
	  	}
  	 	
  	 	function initTab2(){
  	 		builtTable({
  	 			dataUrl:'<%=path%>/MISBOHbusinessAnalysis/queryZdMxfx_pay.do',
  	 			title:'支付明细',
  	 			grid: $("#payShow"),
  	 			height:250,
  	 			createHeader:function(data,head,frozenHead){
 	  	 				var col = [];
	  	 				frozenHead.push([{field:'DWORKDATE',width:80,title:'日期',align:'center'},
	  	 				            	 {field:'VOPERATE',width:60,title:'编码',align:'left'},
	  	 				                 {field:'VNAME',width:120,title:'支付名称',align:'left'}]);
	  	 				col.push({field:"NMONEY",width:70,title:'金额',align:'right'});
	  	 				col.push({field:"NYMONEY",width:70,title:'实收',align:'right'});
	  	 				col.push({field:"NOVERCASH",width:70,title:'超收',align:'right'});
	  					col.push({field:"NPOUNDAGE",width:70,title:'手续费',align:'right'});
	  					col.push({field:"TCTIME",width:130,title:'结账时间',align:'center'});
	  					col.push({field:"VECODE",width:80,title:'结账员工',align:'left'});
	  					col.push({field:"VRCODE",width:130,title:'卡号',align:'left'});
 		  				head.push(col);
 	  	 			}
  	 		});
  	 	}
  	 	
  		//列<fmt:message key="select1" />后页面重新加载
  		function pageReload(){
  			closeColChooseWin();
  			initTab(true);
  		}
  	 </script>
  </body>
</html>