<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title><fmt:message key="bill_detail_analysis" /></title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
		.leftDiv{
			float: left;
			height: 100%;
			width: 120px;
			padding-left: 20px;
			line-height: 30px;
			padding-top: 30px;
		}
		.chartdiv{
			height: 100%;
		}
		.form-label2{
			width: 40px;
			height: 25px;
			position: relative;
			margin: 0;
			float: left;
			text-align: right;
			vertical-align: middle;
			line-height: 25px;
			margin-right: 5px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 6.5%;
		}
		form .form-line .form-input{
			width: 13%;
		}
		form .form-line .form-input input[type=text]{
			width: 85%;
		}
	</style>
  </head>	
  <body>
  	<%@ include file="../share/permission.jsp"%>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label"><fmt:message key="startdate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" pattern="yyyy-MM-dd"/>'
			</div>
			<div class="form-label"><fmt:message key="enddate" /></div>
			<div class="form-input">	
				<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" />
			</div>
  			<div class="form-label" ><fmt:message key="package" /><fmt:message key="decomposition" /></div>
			<div class="form-input">
				&nbsp;<input type="checkbox" id="iflag" name="iflag" value="1"/>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="pubitem" /></div>
			<div class="form-input">
				<input type="hidden" id="pk_pubitem" name="pk_pubitem"  value="" />
				<input type="hidden" id="vpcode" name="vpcode"  value="" />
				<input type="text" id="vpname" name="vpname" class="text" value=""/>
				<img id="searchPubitem" class="search" src="<%=path%>/image/themes/icons/search.png" style="margin-top: 0px;"  />
			</div>
			<div class="form-label"><span style="color: red">*</span><fmt:message key="bill_no" /></div>
			<div class="form-input">
				<input type="text" id="vbcode" name="vbcode" class="text" value="${vbcode }" style="width:230px" />
			</div>
		</div>
		<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }"/>
	</form>
	<input type="hidden" id="reportName" value="zdmxfx"/>
	<input type="hidden" id="excelUrl" value="<%=path%>/MISBOHbusinessAnalysis/exportReport.do"/>
	<input type="hidden" id="printUrl" value="<%=path%>/MISBOHbusinessAnalysis/printReport.do"/>
	<input type="hidden" id="dataUrl" value="<%=path%>/MISBOHbusinessAnalysis/queryZdMxfx.do"/>
	<input type="hidden" id="title" value='<fmt:message key="bill_detail_analysis" />'/>
	<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
			$("#searchPubitem").click(function(){
  	 			selectPubItemBOHV2({
  	 				basePath:'<%=path%>',
  	 				width:820,
  	 				pk_id:'pk_pubitem',
  	 				showPubpack:!$("#iflag").attr("checked"),
  	 				pk_parentId:''
   	 			});
			});
  	 		//默认<fmt:message key="time" />
  	 		$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
  	 		$("#bdat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
		    });
		    $("#edat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
		    });
  	 		
			//初始化<fmt:message key="selected" />标签<fmt:message key="content" />
			initTab(false);
            initone();
          //账单查询跳转到此
  	 		if('${vbcode}'){
  	 			debugger;
  	 			$('#bdat').val('${bdate}');
  	 			$('#edat').val('${edate}');
  	 			$('#vbcode').val('${vbcode}');
  	 			$('#datagrid').datagrid('load',getParam($("#queryForm")));
  	 		}
  	 	});
  	 	
  	 	function setPubItem(data){
  	 		$("#vpcode").val(data.code);
  	 		$("#vpname").val(data.show);
  	 	}
  		//初始化<fmt:message key="selected" />标签<fmt:message key="content" />
  	 	function initTab(rebuild){
  	 		var excelUrl = $("#excelUrl").val();
  	 		var printUrl = $("#printUrl").val();
  	 		var reportName = $("#reportName").val();
  	 		var dataUrl = $("#dataUrl").val();
  	 		var title = $("#title").val();
  	 		var grid = $("#datagrid");

  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp:true,
  	 			excelUrl:excelUrl+'?reportName='+reportName,
  	 			printUrl:printUrl+'?reportName='+reportName,
  	 			colsChooseUrl:'<%=path%>/MISBOHbusinessAnalysis/toColumnsChoose.do?reportName='+reportName,
  	 			toolbar:['search','excel','exit'],
  	 			searchFun:function(){
  	 	  	 		var vbcode = $("#vbcode").val();
  	 				if ("" == vbcode) {
  	 					alert('<fmt:message key="bill_no" /><fmt:message key="cannot_be_empty" />！');
  	 					return true;
  	 				} else {
  	 					$('#datagrid').datagrid('load',getParam($("#queryForm")));
  	 				}
  	 			}
  	 		});
  	 		if(rebuild || grid.css('display') != 'none'){
  	  				firstLoad = true;//重置第一<fmt:message key="secondary" />加载标志
		  	 		builtTable({
		  	 			headUrl:"<%=path%>/MISBOHbusinessAnalysis/findHeaders.do?reportName="+reportName,
		  	 			dataUrl:dataUrl,
		  	 			title:title,
		  	 			grid:grid,
		  	 			alignCols:['serial','pax','foliono','sft','cnt','name','code','prncnt'],
		  	 			numCols:['nprice','ncount','nmoney','nzmoney','nymoney']
		  	 		});
  	  			}
  	 	}
  		//列<fmt:message key="select1" />后页面重新加载
  		function pageReload(){
  			closeColChooseWin();
  			initTab(true);
	  		//重新<fmt:message key="select" />
  			$("#datagrid").datagrid('reload');
  		}
  	 </script>
  </body>
</html>