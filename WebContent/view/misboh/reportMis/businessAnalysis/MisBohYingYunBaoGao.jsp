<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>营运报表日汇总</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
		.leftDiv{
			float: left;
			height: 100%;
			width: 120px;
			padding-left: 20px;
			line-height: 30px;
			padding-top: 30px;
		}
		.chartdiv{
			height: 100%;
		}
		.form-label2{
			width: 40px;
			height: 25px;
			position: relative;
			margin: 0;
			float: left;
			text-align: right;
			vertical-align: middle;
			line-height: 25px;
			margin-right: 5px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 6.5%;
		}
		form .form-line .form-input{
			width: 13%;
		}
		form .form-line .form-input input[type=text]{
			width: 85%;
		}
	</style>
  </head>	
  <body>
  	<%@ include file="../share/permission.jsp"%>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label" style="width:60px;"><fmt:message key="months" /></div>
			<div class="form-input" style="width:150px;">
				<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text"  value="<fmt:formatDate value="${bdat}" pattern="yyyy—MM"/>" />
			</div>
			<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }"/>
		</div>
	</form>
	<input type="hidden" id="reportName" value="yybg_yk"/>
	<input type="hidden" id="excelUrl" value="<%=path%>/MISBOHbusinessAnalysis/exportReport.do"/>
	<input type="hidden" id="printUrl" value="<%=path%>/MISBOHbusinessAnalysis/printReport.do"/>
	<input type="hidden" id="dataUrl" value="<%=path%>/MISBOHbusinessAnalysis/queryYybg.do"/>
	<input type="hidden" id="title" value='<fmt:message key="trading_day_summary_report_java" />'/>
	<div id="datagrid"></div>
	
	<script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 <script language="JavaScript" src="<%=path%>/Charts/FusionCharts.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
			$("#bdat").click(function(){ new WdatePicker({dateFmt:'yyyy-MM'});});
  	 		//默认<fmt:message key="time" />
  	 		$("#bdat").htmlUtils("setDate","curYearMonth",'${newdate}');
	  		
			//初始化<fmt:message key="selected" />标签<fmt:message key="content" />
			initTab(false);
			initone();
  	 	});
  	 	
  		//初始化<fmt:message key="selected" />标签<fmt:message key="content" />
  	 	function initTab(rebuild){
  	 		var excelUrl = $("#excelUrl").val();
  	 		var printUrl = $("#printUrl").val();
  	 		var reportName = $("#reportName").val();
  	 		var dataUrl = $("#dataUrl").val();
  	 		var title = $("#title").val();
  	 		var grid = $("#datagrid");

  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp:true,
  	 			excelUrl:excelUrl+'?reportName='+reportName,
  	 			printUrl:printUrl+'?reportName='+reportName,
  	 			colsChooseUrl:'<%=path%>/MISBOHbusinessAnalysis/toColumnsChoose.do?reportName='+reportName,
  	 			toolbar:['search','excel','exit'],
  	 			searchFun:function(){
  	 				initTab(true);
  	 				$('#datagrid').datagrid('load',getParam($("#queryForm")));
  	 			}
  	 		});
  	 		if(rebuild || grid.css('display') != 'none'){
  	  				firstLoad = true;//重置第一<fmt:message key="secondary" />加载标志
		  	 		builtTable({
		  	 			headUrl:"<%=path%>/MISBOHbusinessAnalysis/findHeaderForYybg.do",
		  	 			dataUrl:dataUrl,
		  	 			title:title,
		  	 			pagination:false,
		  	 			showFooter:false,
		  	 			grid:grid,
			  	 		createHeader:function(data,head,frozenHead){
		  	 				var col2 = [];
		  	 				var col1 = [];
		  	 				frozenHead.push([{field:"DWORKDATE",width:80,title:'<fmt:message key="date" />',rowspan:2,align:'left'},
		  	 				              	{field:"XQ",width:40,title:'<fmt:message key="week" />',rowspan:2,align:'left'}
							]);
// 		  	 				col1.push({width:880,title:'<fmt:message key="business_income" />',colspan:7,align:'center'});
// 		  	 				col1.push({width:400,title:'<fmt:message key="open_the_numbers" />',colspan:4,align:'center'});
// 		  	 				col1.push({width:200,title:'<fmt:message key="Table_of_all_consumption" />',colspan:2,align:'center'});
// 		  	 				col1.push({width:200,title:'<fmt:message key="number_of_people" />',colspan:2,align:'center'});
// 		  	 				col1.push({width:200,title:'<fmt:message key="per_capita_consumption" />',colspan:2,align:'center'});
// 		  	 				//冻结列
// 							sb1.append("NMONEY,NMONEYLJ,NYMONEY,NYMONEYLJ,TNYMONEY,TNYMONEYLJ,TC,TCLJ,TCL,TCLLJ,
// 							PTC,PTCLJ,IPEOLENUM,IPEOLENUMLJ,IPL,IPLLJ");
							// sb1.append("NMONEY"+y+",MONEYL"+y+",TC"+y+",PTC"+y+",");
							// sb1.append("YK,YKLJ,ZSMONEY,DQMONEY,SHOUMAI");
		  	 				
		  	 				col1.push({width:880,title:'<fmt:message key="business_income" />',colspan:7,align:'center'});
		  	 				col2.push({field:"NMONEY",width:100,title:'<fmt:message key="business_income" />',align:'right'});
		  	 				col2.push({field:"NMONEYLJ",width:100,title:'<fmt:message key="The_cumulative" />',align:'right'});
		  	 				col2.push({field:"NYMONEY",width:100,title:'<fmt:message key="Net_turnover" />',align:'right'});
		  	 				col2.push({field:"NYMONEYLJ",width:100,title:'<fmt:message key="The_cumulative" />',align:'right'});
		  	 				col2.push({field:"TNYMONEY",width:100,title:'<fmt:message key="In_the_same_period_last_year" />',align:'right'});
		  	 				col2.push({field:"TNYMONEYL",width:60,title:'<fmt:message key="Compared_with_last_year" />',align:'right'});
		  	 				col2.push({field:"TNYMONEYLJ",width:100,title:'<fmt:message key="The_cumulative" /><fmt:message key="The_budget_to_complete" />(%)',align:'right'});
		  	 				//<fmt:message key="open_the_numbers" />
		  	 				col1.push({width:400,title:'<fmt:message key="open_the_numbers" />',colspan:4,align:'center'});
		  	 				col2.push({field:"TC",width:50,title:'<fmt:message key="everyday" />',align:'right'});
		  	 				col2.push({field:"TCLJ",width:50,title:'<fmt:message key="The_cumulative" />',align:'right'});
		  	 				col2.push({field:"TCL",width:60,title:'<fmt:message key="Founding_rate" />(%)',align:'right'});
		  	 				col2.push({field:"PTCLJ",width:85,title:'<fmt:message key="The_cumulative" /><fmt:message key="Founding_rate" />(%)',align:'right'});
		  	 				//桌均消费
		  	 				col1.push({width:200,title:'<fmt:message key="Table_of_all_consumption" />',colspan:2,align:'center'});
		  	 				col2.push({field:"AVGTC",width:50,title:'<fmt:message key="everyday" />',align:'right'});
		  	 				col2.push({field:"AVGTCLJ",width:50,title:'<fmt:message key="The_cumulative" />',align:'right'});
		  	 				//<fmt:message key="number_of_people" />
		  	 				col1.push({width:200,title:'<fmt:message key="number_of_people" />',colspan:2,align:'center'});
		  	 				col2.push({field:"IPEOLENUM",width:50,title:'<fmt:message key="everyday" />',align:'right'});
		  	 				col2.push({field:"IPEOLENUMLJ",width:50,title:'<fmt:message key="The_cumulative" />',align:'right'});
		  	 				//<fmt:message key="per_capita_consumption" /> 
		  	 				col1.push({width:200,title:'<fmt:message key="per_capita_consumption" />',colspan:2,align:'center'});
		  	 				col2.push({field:"IPL",width:50,title:'<fmt:message key="everyday" />',align:'right'});
		  	 				col2.push({field:"IPLLJ",width:50,title:'<fmt:message key="The_cumulative" />',align:'right'});
		  	 				
		  	 				//时段<fmt:message key="turnover" />
							for ( var index in data.listinter) {
								var interval=data.listinter[index];
								col1.push({width:260,title:interval.vname+interval.vstarttime+"~"+interval.vendtime,colspan:4,align:'center'});
								col2.push({field:"NMONEY"+interval.vcode,width:80,title:"<fmt:message key="turnover" />",align:'right'});
								col2.push({field:"MONEYL"+interval.vcode,width:80,title:"<fmt:message key="turnover" />%",align:'right'});
								col2.push({field:"TC"+interval.vcode,width:50,title:"<fmt:message key="open_the_numbers" />",align:'right'});
								col2.push({field:"PTC"+interval.vcode,width:50,title:'<fmt:message key="Founding_rate" />',align:'right'});
							}
							col1.push({width:200,title:'<fmt:message key="The_positive_and_negative" /><fmt:message key="Profit_and_loss" />',colspan:2,align:'center'});
		  	 				col2.push({field:"YK",width:100,title:'<fmt:message key="Profit_and_loss" /><fmt:message key="amount" />',align:'right'});
		  	 				col2.push({field:"YKLJ",width:100,title:'<fmt:message key="Profit_and_loss" /><fmt:message key="The_cumulative" /><fmt:message key="amount" />',align:'right'});
 		  	 				//col1.push({width:200,title:'<fmt:message key="giving" />',colspan:2,align:'center'});
		  	 				col1.push({field:"ZSMONEY",width:80,title:'<fmt:message key="giving" />',rowspan:2,align:'right'});
		  	 				col1.push({field:"DQMONEY",width:80,title:'<fmt:message key="Discarding_the_amount" />',rowspan:2,align:'right'});
		  	 				col1.push({field:"SSMONEY",width:80,title:'<fmt:message key="Dietary_amount" />',rowspan:2,align:'right'});
		  	 				
		  	 				head.push(col1);
		  	 				head.push(col2);
		  	 			}
		  	 		});
  	  			}
  	 	}
  		//列<fmt:message key="select1" />后页面重新加载
  		function pageReload(){
  			closeColChooseWin();
  			initTab(true);
  			if($('#firmdes').val() && $('#firmid').val())	
  	  			$('#tabs').tabs('getSelected').panel("body").find("#datagrid").datagrid('reload');
  		}
  		
  		var BTIME;
  		var ETIME;
  		//<fmt:message key="set_up_the" /><fmt:message key="time" />段
  		function setTimeSpace(params){
  			var beginSelect = params.beginSelect ? params.beginSelect : "beginSelect";
  			var endSelect = params.endSelect ? params.endSelect : "endSelect";
  			//默认<fmt:message key="select" /><fmt:message key="starttime" />
  			var beginTime = params.beginTime ? parseInt(params.beginTime) : ($("#"+beginSelect).val() ? parseInt($("#"+beginSelect).val()) : 10);
  			//默认<fmt:message key="select" /><fmt:message key="endtime" />
  			var endTime = params.endTime ? parseInt(params.endTime) : ($("#"+endSelect).val() ? parseInt($("#"+endSelect).val()) : 23);		

  			if(!BTIME){
  				BTIME = beginTime;
  			}
  			if(!ETIME){
  				ETIME = endTime;
  			}
  			
  			var bSize = $("#"+beginSelect).find("option").size();
			var eSize = $("#"+endSelect).find("option").size();
			
			for(var i=0;i<bSize;i++){
				$("#"+beginSelect).find("option:eq(0)").remove();
			}
			
			for(var i=0;i<eSize;i++){
				$("#"+endSelect).find("option:eq(0)").remove();
			}
			
  			for(var i=BTIME; i<endTime;i++){
  				if(i==beginTime){
  					$("#"+beginSelect).append("<option value='" + i +"' selected='selected'>" + i + ":00</option>");  //添加一项option
  				}else {
  					$("#"+beginSelect).append("<option value='" + i +"'>" + i + ":00</option>");  //添加一项option
  				}
  			}
  			for(var i=beginTime+1;i<=ETIME;i++){
  				if(i==endTime){
  					$("#"+endSelect).append("<option value='" + i +"' selected='selected'>" + i + ":00</option>");  //添加一项option	
  				}else {
					$("#"+endSelect).append("<option value='" + i +"'>" + i + ":00</option>");  //添加一项option
  				}
  			}
  		}
  	 </script>
  </body>
</html>