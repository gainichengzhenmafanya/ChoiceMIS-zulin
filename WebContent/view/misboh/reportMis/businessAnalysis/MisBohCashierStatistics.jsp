<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title><fmt:message key="retreat_food" /><fmt:message key="the_detail" /><fmt:message key="the_report" /></title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
		.leftDiv{
			float: left;
			height: 100%;
			width: 120px;
			padding-left: 20px;
			line-height: 30px;
			padding-top: 30px;
		}
		.chartdiv{
			height: 100%;
		}
		.form-label2{
			width: 40px;
			height: 25px;
			position: relative;
			margin: 0;
			float: left;
			text-align: right;
			vertical-align: middle;
			line-height: 25px;
			margin-right: 5px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 6.5%;
		}
		form .form-line .form-input{
			width: 13%;
		}
		form .form-line .form-input input[type=text]{
			width: 85%;
			margin-top:3px;
		}
	</style>
  </head>	
  <body>
  	<%@ include file="../share/permission.jsp"%>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label"><fmt:message key="startdate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" />
			</div>
			<div class="form-label"><fmt:message key="enddate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd"/>" />
			</div>
			<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }" />
		</div>
	</form>
	
	<input type="hidden" id="reportName" value="syytj"/>
	<input type="hidden" id="excelUrl" value="<%=path%>/MISBOHbusinessAnalysis/exportReport.do"/>
	<input type="hidden" id="printUrl" value="<%=path%>/MISBOHbusinessAnalysis/printReport.do"/>
	<input type="hidden" id="dataUrl" value="<%=path%>/MISBOHbusinessAnalysis/queryCashierStatistics.do"/>
	<input type="hidden" id="title" value="<fmt:message key="cashier" /><fmt:message key="statistical" />"/>
	<div id="datagrid"></div>
	
	<script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script><!-- 兼容浏览器ie 用1.7报错参数无效 -->
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script language="JavaScript" src="<%=path%>/Charts/FusionCharts.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript">
		var tab;
  	 	$(document).ready(function(){
			//默认<fmt:message key="time" />
  	 		$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
  	 		$("#bdat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
		    });
		    $("#edat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
		    });
 		
			//初始化<fmt:message key="selected" />标签<fmt:message key="content" />
			initTab(false); 	 		
  	 	});
  	 	
  		//初始化<fmt:message key="selected" />标签<fmt:message key="content" />
  	 	function initTab(rebuild){
  	 		var excelUrl = $("#excelUrl").val();
  	 		var printUrl = $("#printUrl").val();
  	 		var reportName = $("#reportName").val();
  	 		var dataUrl = $("#dataUrl").val();
  	 		var title = $("#title").val();
  	 		var grid = $("#datagrid");

  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp:true,
  	 			excelUrl:excelUrl+'?reportName='+reportName,
  	 			printUrl:printUrl+'?reportName='+reportName,
  	 			colsChooseUrl:'<%=path%>/MISBOHbusinessAnalysis/toColumnsChoose.do?reportName='+reportName,
  	 			toolbar:['search','excel','exit'],
  	 			searchFun:function(){
  	 				$('#datagrid').datagrid('load',getParam($("#queryForm")));
  	 			}
  	 		});
  	 		if(rebuild || grid.css('display') != 'none'){
  	  			firstLoad = true;//重置第一<fmt:message key="secondary" />加载标志
		  	 	builtTable({
		  	 		dataUrl:dataUrl,
		  	 		title:title,
		  	 		grid:grid,
		  	 		createHeader:function(data,head,frozenHead){
		  	 			var col1 = [];
		  	 			var col2 = [];
//		  	 				//冻结列
	  	 				frozenHead.push([
							{field:"USER_NAME",width:120,title:'<fmt:message key="employee_name" />',colspan:1,rowspan:1,align:"left"},
							{field:"WORKDATE",width:120,title:'<fmt:message key="business_day" />',colspan:1,rowspan:1,align:"left"},
		  	 				{field:"SHANGJICISHU",width:120,title:'<fmt:message key="Times" />',colspan:1,rowspan:1,align:"right"},
							{field:"JIAOYICISHU",width:120,title:'<fmt:message key="The_number_of_transactions" />',colspan:1,rowspan:1,align:"right"}
	  	 				]);
	  	 				
// 	  	 				现金短缺
	  	 				col1.push({field:"",width:320,title:'<fmt:message key="A_shortage_of_cash" />',colspan:4,rowspan:1,align:"right"});
	  	 				col2.push({field:"XIANJINDUANQUEJINE",width:60,title:'<fmt:message key="amount" />',colspan:1,rowspan:1,align:"right"});
	  	 				col2.push({field:"XIANJINDUANQUECISHU",width:60,title:'<fmt:message key="count" />',colspan:1,rowspan:1,align:"right"});
	  	 				col2.push({field:"XJDQSJCS",width:100,title:'%(<fmt:message key="According_to_the" /><fmt:message key="Times" />)',colspan:1,rowspan:1,align:"right"});
	  	 				col2.push({field:"XJDQJYCS",width:100,title:'%(<fmt:message key="According_to_the" /><fmt:message key="The_number_of_transactions" />)',colspan:1,rowspan:1,align:"right"});
	  	 				
// 	  	 				赔款
	  	 				col1.push({field:"",width:320,title:'<fmt:message key="reparations" />',colspan:4,rowspan:1,align:"right"});
	  	 				col2.push({field:"PEIKUANJINE",width:60,title:'<fmt:message key="amount" />',colspan:1,rowspan:1,align:"right"});
	  	 				col2.push({field:"PEIKUANCISHU",width:60,title:'<fmt:message key="count" />',colspan:1,rowspan:1,align:"right"});
	  	 				col2.push({field:"PKSJCS",width:100,title:'%(<fmt:message key="According_to_the" /><fmt:message key="Times" />)',colspan:1,rowspan:1,align:"right"});
	  	 				col2.push({field:"PKJYCS",width:100,title:'%(<fmt:message key="According_to_the" /><fmt:message key="The_number_of_transactions" />)',colspan:1,rowspan:1,align:"right"});
	  	 				
// 	  	 				整单退
	  	 				col1.push({field:"",width:320,title:'<fmt:message key="The_entire_single_back" />',colspan:4,rowspan:1,align:"right"});
	  	 				col2.push({field:"ZHENGDANTUIJINE",width:60,title:'<fmt:message key="amount" />',colspan:1,rowspan:1,align:"right"});
	  	 				col2.push({field:"ZHENGDANTUICISHU",width:60,title:'<fmt:message key="count" />',colspan:1,rowspan:1,align:"right"});
	  	 				col2.push({field:"ZDTSJCS",width:100,title:'%(<fmt:message key="According_to_the" /><fmt:message key="Times" />)',colspan:1,rowspan:1,align:"right"});
	  	 				col2.push({field:"ZDTJYCS",width:100,title:'%(<fmt:message key="According_to_the" /><fmt:message key="The_number_of_transactions" />)',colspan:1,rowspan:1,align:"right"});
	  	 				
// 	  	 				单品退
	  	 				col1.push({field:"",width:320,title:'<fmt:message key="Single_product_return" />',colspan:4,rowspan:1,align:"right"});
	  	 				col2.push({field:"DANPINTUIJINE",width:60,title:'<fmt:message key="amount" />',colspan:1,rowspan:1,align:"right"});
	  	 				col2.push({field:"DANPINTUICISHU",width:60,title:'<fmt:message key="count" />',colspan:1,rowspan:1,align:"right"});
	  	 				col2.push({field:"DPTSJCS",width:100,title:'%(<fmt:message key="According_to_the" /><fmt:message key="Times" />)',colspan:1,rowspan:1,align:"right"});
	  	 				col2.push({field:"DPTJYCS",width:100,title:'%(<fmt:message key="According_to_the" /><fmt:message key="The_number_of_transactions" />)',colspan:1,rowspan:1,align:"right"});
	  	 				
// 	  	 				账单取消
	  	 				col1.push({field:"",width:320,title:'<fmt:message key="bill" /><fmt:message key="cancel" />',colspan:4,rowspan:1,align:"right"});
	  	 				col2.push({field:"QUXIAOJINE",width:60,title:'<fmt:message key="amount" />',colspan:1,rowspan:1,align:"right"});
	  	 				col2.push({field:"QUXIAOCISHU",width:60,title:'<fmt:message key="count" />',colspan:1,rowspan:1,align:"right"});
	  	 				col2.push({field:"QXSJCS",width:100,title:'%(<fmt:message key="According_to_the" /><fmt:message key="Times" />)',colspan:1,rowspan:1,align:"right"});
	  	 				col2.push({field:"QXJYCS",width:100,title:'%(<fmt:message key="According_to_the" /><fmt:message key="The_number_of_transactions" />)',colspan:1,rowspan:1,align:"right"});
	  	 				
// 	  	 				封帐前/后删除
	  	 				col1.push({field:"",width:320,title:'<fmt:message key="A_bill_before_after_delete" />',colspan:4,rowspan:1,align:"right"});
	  	 				col2.push({field:"FENGZHANGQIANHOUJINE",width:60,title:'<fmt:message key="amount" />',colspan:1,rowspan:1,align:"right"});
	  	 				col2.push({field:"FENGZHANGQIANHOUCISHU",width:60,title:'<fmt:message key="count" />',colspan:1,rowspan:1,align:"right"});
	  	 				col2.push({field:"FZQHSJCS",width:100,title:'%(<fmt:message key="According_to_the" /><fmt:message key="Times" />)',colspan:1,rowspan:1,align:"right"});
	  	 				col2.push({field:"FZQHJYCS",width:100,title:'%(<fmt:message key="According_to_the" /><fmt:message key="The_number_of_transactions" />)',colspan:1,rowspan:1,align:"right"});
	  	 				
// 	  	 				反结算
	  	 				col1.push({field:"",width:320,title:'<fmt:message key="return_money" />',colspan:4,rowspan:1,align:"right"});
	  	 				col2.push({field:"FANJIESUANJINE",width:60,title:'<fmt:message key="amount" />',colspan:1,rowspan:1,align:"right"});
	  	 				col2.push({field:"FANJIESUANCISHU",width:60,title:'<fmt:message key="count" />',colspan:1,rowspan:1,align:"right"});
	  	 				col2.push({field:"FJSSJCS",width:100,title:'%(<fmt:message key="According_to_the" /><fmt:message key="Times" />)',colspan:1,rowspan:1,align:"right"});
	  	 				col2.push({field:"FJSJYCS",width:100,title:'%(<fmt:message key="According_to_the" /><fmt:message key="The_number_of_transactions" />)',colspan:1,rowspan:1,align:"right"});
	  	 				
// 	  	 				开抽屉次数
	  	 				col1.push({field:"",width:260,title:'<fmt:message key="open_the_cash_drawer" /><fmt:message key="count" />',colspan:3,rowspan:1,align:"right"});
	  	 				col2.push({field:"KAIQIANXIANGCISHU",width:60,title:'<fmt:message key="count" />',colspan:1,rowspan:1,align:"right"});
	  	 				col2.push({field:"KQXSJCS",width:100,title:'%(<fmt:message key="According_to_the" /><fmt:message key="Times" />)',colspan:1,rowspan:1,align:"right"});
	  	 				col2.push({field:"KQXJYCS",width:100,title:'%(<fmt:message key="According_to_the" /><fmt:message key="The_number_of_transactions" />)',colspan:1,rowspan:1,align:"right"});
	  	 				
	  	 				head.push(col1);
	  	 				head.push(col2);
	  	 			}

		  	 	});
  	  		}
  	 	}
  	 	
  		//列<fmt:message key="select1" />后页面重新加载
  		function pageReload(){
  			closeColChooseWin();
  			initTab(true);
  		}
  	 </script>
  </body>
</html>