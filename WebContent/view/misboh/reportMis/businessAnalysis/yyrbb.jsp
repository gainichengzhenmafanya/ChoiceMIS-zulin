<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
    String path = request.getContextPath();
	int len=1;
	int num=1;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>九毛九营业日报表</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <style type="text/css">
    	.table{
 			width: 550px; 
			margin-left: 10px;
 			margin-top: 10px; 
			border-width: 1px;
			border-style: solid;
			border-color: #B1C3D9;
		}
		.table tr td{ 
			border-width: 1px;
			border-color: #B1C3D9;
			border-style: solid;
			height: 20px;
			text-align: center;
		}
    </style>
    <%@ include file="../share/permission.jsp" %>
</head>
<body>
<div id="tool"></div>
<form id="queryForm" action="<%=path%>/MISBOHNewreport/queryYYRBB.do" method="post">
    <div class="form-line">
        <div class="form-label"><fmt:message key="startdate" /></div>
		<div class="form-input">
			<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" value="${condition.bdat}" />
		</div>
		 <div class="form-label"><fmt:message key="enddate" /></div>
		<div class="form-input">
			<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" value="${condition.edat}" />
		</div>
		<div class="form-input">
			<b>${condition.edat}营业日为
			<c:if test="${uploadFlag=='0'}"><span style="color: red;">实时数据</span></c:if>
			<c:if test="${uploadFlag=='1'}"><span style="color: green;">打烊后上传数据</span></c:if>
			<c:if test="${uploadFlag=='2'}"><span style="color: red;">未营业</span></c:if>
			</b>
		</div>
		<input type="hidden" id="pk_store" name="pk_store" value="${pk_store}"/>
    </div>
    <div style="width: 100%;height: 480px;overflow: auto;">
	    <div style="display: inline-table;">
		    <table class="table" style="float:left;" cellspacing="0">
			    <tr bgcolor="#c0c0c0">
			    	<td colspan="7" style="font-weight: bold;"><b>营业结算方式</b></td>
			    </tr>
		    	<tr>
		    		<td style="width: 22%">项目</td>
		    		<td style="width: 15%">笔数/张数</td>
		    		<td style="width: 12%">原价金额</td>
		    		<td style="width: 12%">优惠金额</td>
		    		<td style="width: 15%">收入确认金额</td>
		    		<td style="width: 12%">手续费</td>
		    		<td style="width: 12%">实收金额</td>
		    	</tr>
		    	<c:forEach items="${paymentList}" var="payment" varStatus="Status">
			    	<tr>
						<%
						    len=1;
						%>
			    		<td style="width: 22%;text-align: left;">&nbsp;${Status.index+1}、${payment.VNAME}</td>
			    		<td style="width: 15%;text-align: right;">${payment.CNT}</td>
			    		<td style="width: 12%;text-align: right;">${payment.NMONEY}</td>
			    		<td style="width: 12%;text-align: right;">${payment.NPAIDMONEY}</td>
			    		<td style="width: 15%;text-align: right;">${payment.AMT}</td>
			    		<td style="width: 12%;text-align: right;">${payment.NPOUNDAGE}</td>
			    		<td style="width: 12%;text-align: right;">${payment.JAMT}</td>
			    	</tr>
			    	<c:forEach items="${paymodeList}" var="paymode">
				    	<c:if test="${payment.VOPERATE==paymode.IGROUPID}">
				    		<tr>
					    		<td style="width: 22%;text-align: left;">&nbsp;&nbsp;&nbsp;&nbsp;${Status.index+1}.<%=len++%>&nbsp;${paymode.VNAME}</td>
					    		<td style="width: 15%;text-align: right;">${paymode.CNT}</td>
					    		<td style="width: 12%;text-align: right;">${paymode.NMONEY}</td>
					    		<td style="width: 12%;text-align: right;">${paymode.NPAIDMONEY}</td>
					    		<td style="width: 15%;text-align: right;">${paymode.AMT}</td>
					    		<td style="width: 12%;text-align: right;">${paymode.NPOUNDAGE}</td>
					    		<td style="width: 12%;text-align: right;">${paymode.JAMT}</td>
					    	</tr>
				    	</c:if>
			    	</c:forEach>
			    </c:forEach>
			    <c:if test="${payCal.JAMT!=marsaleClassCal.NYMONEY}">
			    <tr bgcolor="red">
			    	<td style="width: 22%"><b>${payCal.VNAME}</b></td>
		    		<td style="width: 15%;text-align: right;"><b>${payCal.CNT}</b></td>
		    		<td style="width: 12%;text-align: right;"><b>${payCal.NMONEY}</b></td>
		    		<td style="width: 12%;text-align: right;"><b>${payCal.NPAIDMONEY}</b></td>
		    		<td style="width: 15%;text-align: right;"><b>${payCal.AMT}</b></td>
		    		<td style="width: 12%;text-align: right;"><b>${payCal.NPOUNDAGE}</b></td>
		    		<td style="width: 12%;text-align: right;"><b>${payCal.JAMT}</b></td>
		    	</tr>
			    </c:if>
			    <c:if test="${payCal.JAMT==marsaleClassCal.NYMONEY}">
			    <tr bgcolor="#ffff00">
		    		<td style="width: 22%"><b>${payCal.VNAME}</b></td>
		    		<td style="width: 15%;text-align: right;"><b>${payCal.CNT}</b></td>
		    		<td style="width: 12%;text-align: right;"><b>${payCal.NMONEY}</b></td>
		    		<td style="width: 12%;text-align: right;"><b>${payCal.NPAIDMONEY}</b></td>
		    		<td style="width: 15%;text-align: right;"><b>${payCal.AMT}</b></td>
		    		<td style="width: 12%;text-align: right;"><b>${payCal.NPOUNDAGE}</b></td>
		    		<td style="width: 12%;text-align: right;"><b>${payCal.JAMT}</b></td>
		    	</tr>
		    	</c:if>
		    </table>
		    <table class="table" style="float: left;" cellspacing="0">
			    <tr bgcolor="#c0c0c0">
			    	<td colspan="7" style="font-weight: bold;"><b>收入大类</b></td>
			    </tr>
		    	<tr>
		    		<td colspan="2" style="width: 25%">项目</td>
		    		<td style="width: 15%">原价金额</td>
		    		<td style="width: 15%">优惠金额</td>
		    		<td style="width: 15%">含税收入</td>
		    		<td style="width: 15%">销项税</td>
		    		<td style="width: 15%">不含税收入</td>
		    	</tr>
		    	<c:forEach items="${marsaleClassList}" var="mar" varStatus="Status">
		    		<%
					    num=1;
					%>
		    		<tr>
		    			<td style="width: 10%">${mar.TYPENAME}</td>
		    			<td style="width: 15%">${mar.RN}、${mar.NAME}</td>
			    		<td style="width: 15%;text-align: right;">${mar.NMONEY}</td>
			    		<td style="width: 15%;text-align: right;">${mar.NZMONEY}</td>
			    		<td style="width: 15%;text-align: right;">${mar.NYMONEY}</td>
			    		<td style="width: 15%;text-align: right;">${mar.NTAX}</td>
			    		<td style="width: 15%;text-align: right;">${mar.JAMT}</td>
			    	</tr>
			    	<c:forEach items="${marsaleClassTwoList}" var="mart">
				    	<c:if test="${mar.ID==mart.ID && mar.SORTS==mart.SORTS}">
				    		<tr>
				    			<td style="width: 10%">${mart.TYPENAME}</td>
					    		<td style="width: 15%">&nbsp;&nbsp;${mar.RN}.<%=num++%>&nbsp;${mart.NAME}</td>
					    		<td style="width: 15%;text-align: right;">${mart.NMONEY}</td>
					    		<td style="width: 15%;text-align: right;">${mart.NZMONEY}</td>
					    		<td style="width: 15%;text-align: right;">${mart.NYMONEY}</td>
					    		<td style="width: 15%;text-align: right;">${mart.NTAX}</td>
					    		<td style="width: 15%;text-align: right;">${mart.JAMT}</td>
					    	</tr>
				    	</c:if>
			    	</c:forEach>
			    </c:forEach>
			    <c:if test="${payCal.JAMT!=marsaleClassCal.NYMONEY}">
		    	<tr bgcolor="red">
		    		<td colspan="2" style="width: 25%"><b>${marsaleClassCal.NAME}</b></td>
		    		<td style="width: 15%;text-align: right;"><b>${marsaleClassCal.NMONEY}</b></td>
		    		<td style="width: 15%;text-align: right;"><b>${marsaleClassCal.NZMONEY}</b></td>
		    		<td style="width: 15%;text-align: right;"><b>${marsaleClassCal.NYMONEY}</b></td>
		    		<td style="width: 15%;text-align: right;"><b>${marsaleClassCal.NTAX}</b></td>
		    		<td style="width: 15%;text-align: right;"><b>${marsaleClassCal.JAMT}</b></td>
		    	</tr>
		    	</c:if>
		    	<c:if test="${payCal.JAMT==marsaleClassCal.NYMONEY}">
		    	<tr bgcolor="#ffff00">
		    		<td colspan="2" style="width: 25%"><b>${marsaleClassCal.NAME}</b></td>
		    		<td style="width: 15%;text-align: right;"><b>${marsaleClassCal.NMONEY}</b></td>
		    		<td style="width: 15%;text-align: right;"><b>${marsaleClassCal.NZMONEY}</b></td>
		    		<td style="width: 15%;text-align: right;"><b>${marsaleClassCal.NYMONEY}</b></td>
		    		<td style="width: 15%;text-align: right;"><b>${marsaleClassCal.NTAX}</b></td>
		    		<td style="width: 15%;text-align: right;"><b>${marsaleClassCal.JAMT}</b></td>
		    	</tr>
		    	</c:if>
		    </table>
	    </div>
	    <div>
		    <table class="table" style="float:left;" cellspacing="0">
			    <tr bgcolor="#c0c0c0">
			    	<td colspan="3" style="font-weight: bold;"><b>其他少款</b></td>
			    </tr>
		    	<tr>
		    		<td style="width: 40%">项目</td>
		    		<td style="width: 30%">现金</td>
		    		<td style="width: 30%">刷卡</td>
		    	</tr>
		    	<c:forEach items="${shaoamtList}" var="shaoamt">
			    	<tr>
			    		<td style="width: 40%;text-align: left;">&nbsp;${shaoamt.NAME}</td>
			    		<td style="width: 30%;text-align: right;">${shaoamt.XAMT}</td>
			    		<td style="width: 30%;text-align: right;">${shaoamt.SAMT}</td>
			    	</tr>
			    </c:forEach>
		    	<tr bgcolor="#ffff00">
		    		<td style="width: 40%"><b>${shaoamtCal.NAME}</b></td>
		    		<td style="width: 30%;text-align: right;"><b>${shaoamtCal.XAMT}</b></td>
		    		<td style="width: 30%;text-align: right;"><b>${shaoamtCal.SAMT}</b></td>
		    	</tr>
		    </table>
		    <table class="table" style="float: left;" cellspacing="0">
			    <tr bgcolor="#c0c0c0">
			    	<td colspan="3" style="font-weight: bold;"><b>其他多款</b></td>
			    </tr>
		    	<tr>
		    		<td style="width: 40%">项目</td>
		    		<td style="width: 30%">现金</td>
		    		<td style="width: 30%">刷卡</td>
		    	</tr>
		    	<c:forEach items="${duoamtList}" var="duoamt">
		    		<tr>
		    			<td style="width: 40%;text-align: left;">&nbsp;${duoamt.NAME}</td>
			    		<td style="width: 30%;text-align: right;">${duoamt.XAMT}</td>
			    		<td style="width: 30%;text-align: right;">${duoamt.SAMT}</td>
			    	</tr>
			    </c:forEach>
		    	<tr bgcolor="#ffff00">
		    		<td style="width: 40%"><b>${duoamtCal.NAME}</b></td>
		    		<td style="width: 30%;text-align: right;"><b>${duoamtCal.XAMT}</b></td>
		    		<td style="width: 30%;text-align: right;"><b>${duoamtCal.SAMT}</b></td>
		    	</tr>
		    </table>
	    </div>
	    <div>
		    <table class="table" style="width: 1110px;float: left;" cellspacing="0">
			    <tr>
			    	<td colspan="4" style="text-align: left">（三）应上缴现金合计（账存现金金额-少款现金+多款现金）</td>
			    	<td colspan="2" style="text-align: right">${ncouponcamt.NCOUPONCAMT-shaoamtCal.XAMT+duoamtCal.XAMT}</td>
			    </tr>
			    <tr>
			    	<td colspan="4" style="text-align: left">（四）当日刷卡合计（账存刷卡金额-少款刷卡+多款刷卡）</td>
			    	<td colspan="2" style="text-align: right">${ncashamt.NCASHAMT-shaoamtCal.SAMT+duoamtCal.SAMT}</td>
			    </tr>
			    <tr>
			    	<td colspan="6" style="text-align: left">（五）现金存银行明细信息</td>
			    	</tr>
			    <tr>
			    	<td>序号</td>
			    	<td>现金所属营业日期</td>
			    	<td>应存营业现金金额</td>
			    	<td>存款回单日期</td>
			    	<td>存款交易流水号</td>
			    	<td>现金存款金额</td>
			    </tr>
		    	<c:forEach items="${billList}" var="bill">
		    		<tr>
		    			<td>${bill.ID}</td>
			    		<td>${bill.DWORKDATE}</td>
			    		<td>${bill.NCASH}</td>
			    		<td>${bill.VSAVINGDATE}</td>
			    		<td>${bill.VSAVINGNO}</td>
			    		<td>${bill.NAMOUNT}</td>
			    	</tr>
			    </c:forEach>
			    <tr>
	    			<td></td>
		    		<td>合计</td>
		    		<td>${billListCal.NCASH}</td>
		    		<td></td>
		    		<td></td>
		    		<td>${billListCal.NAMOUNT}</td>
			    </tr>
			    <tr>
			    	<td colspan="4" style="text-align: left">（六）存款差异（应存营业现金金额合计-现金存款金额合计）</td>
			    	<td colspan="2" style="text-align: right">${billListCal.NCASH-billListCal.NAMOUNT}</td>
			    </tr>
			    <tr>
			    	<td>水</td>
			    	<td>昨日数:</td>
			    	<td style="text-align: right">${stockdata.SBCNT}</td>
			    	<td>今日数:</td>
			    	<td style="text-align: right">${stockdata.SECNT}</td>
			    	<td style="text-align: left">&nbsp;本日用量:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${stockdata.SCNT}</td>
			    </tr>
			    <tr>
			    	<td>电</td>
			    	<td>昨日数:</td>
			    	<td style="text-align: right">${stockdata.DBCNT}</td>
			    	<td>今日数:</td>
			    	<td style="text-align: right">${stockdata.DECNT}</td>
			    	<td style="text-align: left">&nbsp;本日用量:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${stockdata.DCNT}</td>
			    </tr>
			    <tr>
			    	<td>燃气</td>
			    	<td>昨日数:</td>
			    	<td style="text-align: right">${stockdata.QBCNT}</td>
			    	<td>今日数:</td>
			    	<td style="text-align: right">${stockdata.QECNT}</td>
			    	<td style="text-align: left">&nbsp;本日用量:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${stockdata.QCNT}</td>
			    </tr>
			    <tr>
			    	<td width="15%">退菜数量</td>
			    	<td  style="width:15%; text-align: right">${cancel.NTCOUNT}</td>
			    	<td width="15%">退菜金额</td>
			    	<td  style="width:15%; text-align: right">${cancel.NTMONEY}</td>
			    	<td width="15%">反结账次数</td>
			    	<td style="width:25%; text-align: right">${fcnt.FCNT}</td>
			    </tr>
		    </table>
	    </div>
	</div>
</form>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
    	//查询等待start
		$("#queryForm").submit( function () {
			$("#wait2").css("visibility","visible");
			$("#wait").css("visibility","visible");
		});
		//查询等待end
		
 		//默认<fmt:message key="time" />
 	 	$("#bdat").click(function(){
	        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
	    });
	    $("#edat").click(function(){
	        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
	    });
        
        $('#tool').toolbar({
			items: [{
					text: '<fmt:message key="select" />',
					title: '<fmt:message key="select" />',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						$('#queryForm').submit();
					}
				},{
					text: '<fmt:message key="export" />',
					title: '<fmt:message key="exporting_reports" />',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
					icon: {
						url: '<%=path%>/image/Button/excel.bmp',
						position: ['2px','2px']
					},
					handler: function(){
						$('#queryForm').attr('action',"<%=path%>/MISBOHNewreport/exportYYRBBReport.do");
						$('#queryForm').submit();
						$('#queryForm').attr('action',"<%=path%>/MISBOHNewreport/queryYYRBB.do");
						
						$("#wait2").css("visibility","hidden");
						$("#wait").css("visibility","hidden");
					}
				},{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit" />',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
					}
				}
			]
		});
    });
</script>
</body>
</html>
