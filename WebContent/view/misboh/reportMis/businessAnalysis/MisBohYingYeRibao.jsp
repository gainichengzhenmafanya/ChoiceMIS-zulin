<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>营业日报</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	<style type="text/css">
		.table{
			margin-left: 10px;
			border-width: 1px;
			border-style: solid;
			border-color: #B1C3D9;
		}
		.table thead tr td{ 
			text-align: center;
		}
		.table tr td{ 
			border-width: 1px;
			border-color: #B1C3D9;
			border-style: solid;
			height: 20px;
		}
	</style>
  </head>	
  <body>
  	<%@ include file="../share/permission.jsp"%>
  <div id="tool"></div>
  	<form id="queryForm" name="queryForm" action="<%=path%>/MISBOHbusinessAnalysis/queryYingYeRibao.do" method="post">
		<div class="form-line">
			<input type="hidden" id="pk_store" name="pk_store"  value="${searchCondition.pk_store }"/>
			<div class="form-label"><fmt:message key="startdate" /></div>
			<div class="form-input">
				<input  type="text" id="bdat" name="bdat" class="Wdate text" />
			</div>
			<div class="form-label"><fmt:message key="enddate" /></div>
			<div class="form-input">	
				<input  type="text" id="edat" name="edat" class="Wdate text" />
			</div>
		</div>
	</form>
	<div style="width:100%; margin-top: 20px;height: 470px;overflow: auto">
	   <div style="width:20%;float: left;padding-bottom: 20px;padding-right: 5px;">
		   <table class="table table1" width="200px" cellspacing="0" cellpadding="0" >
		     <thead>
		     	 <tr bgcolor="#c0c0c0">
		     	 	<td colspan="2" align="center"><fmt:message key="amount" /><fmt:message key="select" /></td>
		     	 </tr>
			     <tr>
				     <td width="100px"><fmt:message key="project" /></td>
				     <td><fmt:message key="amount" /></td>
			     </tr>
		     </thead>
		     <tbody>
		         <tr>
				     <td><fmt:message key="scm_sales" /><fmt:message key="Total_income" /></td>
				     <td align="right">${xsMap.AMT1 }</td>
			     </tr>
		         <tr>
				     <td>+<fmt:message key="other_operating_income" /></td>
				     <td align="right">${xsMap.QTYWSR }</td>
			     </tr>
		         <tr>
				     <td>=<fmt:message key="main_business_income" /></td>
				     <td align="right">${xsMap.ZYYWSR }</td>
			     </tr>
		         <tr>
				     <td>-<fmt:message key="discount_amt" /></td>
				     <td align="right">${xsMap.ZKMONEY }</td>
			     </tr>
		         <tr>
				     <td>-<fmt:message key="maLing" /></td>
				     <td align="right">${xsMap.NBZERO }</td>
			     </tr>
		         <tr>
				     <td>=<fmt:message key="Tax_income" /></td>
				     <td align="right">${xsMap.ZYYWSR-xsMap.ZKMONEY-xsMap.NBZERO }</td>
			     </tr>
		         <tr>
				     <td>-<fmt:message key="service_fees_javapro" /></td>
				     <td align="right">${xsMap.NSVR }</td>
			     </tr>
		         <tr>
				     <td>=<fmt:message key="real_total" /></td>
				     <td align="right">${xsMap.ZYYWSR-xsMap.ZKMONEY-xsMap.NBZERO-xsMap.NSVR }</td>
			     </tr>
		         <tr>
				     <td>+<fmt:message key="operating_income" /></td>
				     <td align="right">${xsMap.YYWSR }</td>
			     </tr>
		         <tr>
				     <td>=<fmt:message key="Total_income" /></td>
				     <td align="right">${xsMap.ZYYWSR-xsMap.ZKMONEY-xsMap.NBZERO-xsMap.NSVR+xsMap.YYWSR }</td>
			     </tr>
		     </tbody>
		   </table>
	   </div>
	    <div style="width:21%;float: left;padding-right: 5px;">
		     <table class="table table2" cellspacing="0" cellpadding="0">
			     <thead>
			     	 <tr bgcolor="#c0c0c0">
			     	 	<td colspan="3" align="center"><fmt:message key="single_back" /><fmt:message key="statistical" /></td>
			     	 </tr>
				     <tr>
					     <td width="80px"><fmt:message key="project" /></td>
					     <td width="80px"><fmt:message key="quantity" /></td>
					     <td width="80px"><fmt:message key="amount" /></td>
				     </tr>
			     </thead>
		     	<tbody>
		     		<c:forEach items="${tdMap }" var="map">
		     			<tr>
				     		<td>${map['VNAME'] }</td>
				    		<td align="right">${map['CNT'] }</td>
				     		<td align="right">${map['NYMONEY'] }</td>
			     		</tr>
		     		</c:forEach>
		    	</tbody>
		   	</table>
	   </div>
	     <div style="width:58%;float:left;">
		   <table class="table table3" cellpadding="0" cellspacing="0">
		     <thead>
		     	 <tr bgcolor="#c0c0c0">
		     	 	<td colspan="6" align="center"><fmt:message key="bucket" /><fmt:message key="statistical" /></td>
		     	 </tr>
			     <tr>
				     <td width="90px"><fmt:message key="Operating_time" /></td>
				     <td width="80px"><fmt:message key="singular" /></td>
				     <td width="90px"><fmt:message key="amount" /></td>
				     <td width="80px"><fmt:message key="List_all" /></td>
				     <td width="100px"><fmt:message key="Reach" /><fmt:message key="scm_estimated_date" />%</td>
				     <td width="120px"><fmt:message key="Reach" /><fmt:message key="last_year" />%</td>
			     </tr>
		     </thead>
		     <tbody>
		     	<c:forEach items="${ccMap }" var="canci" varStatus="statues">
		     		<tr>
		     			<td>${canci.CANCI }</td>
		     			<td align="right">${canci.NUMB }</td>
		     			<td align="right"><fmt:formatNumber value="${canci.MONEY }" pattern="##########0.00" /></td>
		     			<td align="right"><fmt:formatNumber value="${canci.DANJUN }" pattern="##########0.00" /></td>
		     			<td align="right">${canci.DCYG }</td>
		     			<td align="right">${canci.DCQN }</td>
		     		</tr>
		     	</c:forEach>
		     </tbody>
		   </table>
	   </div>
	
	<div style="width:100%;">
	  <table class="table table4" width="99%" cellpadding="0" cellspacing="0">
		     <thead>
		     	 <tr bgcolor="#c0c0c0">
		     	 	<td colspan="9" align="center"><fmt:message key="way" /><fmt:message key="statistical" /></td>
		     	 </tr>
			     <tr>
			     	<td width="80px"><fmt:message key="project" /></td>
				    <td width="80px"><fmt:message key="scm_sales" /><fmt:message key="income" /></td>
				    <td width="63px"><fmt:message key="accounted_for" />(%)</td>
				    <td width="80px"><fmt:message key="bill_num" /></td>
					<td width="80px"><fmt:message key="accounted_for" />(%)</td>
					<td width="80px"><fmt:message key="List_all" /></td>
					<td width="90px"><fmt:message key="number_of_people" /></td>
					<td width="80px"><fmt:message key="per_capita" /></td>
					<td width="88px"><fmt:message key="List_all" /><fmt:message key="service" /><fmt:message key="time" />(<fmt:message key="minutes" />)</td>
				</tr>
		     </thead>
		     <tbody>
		    	 <c:forEach items="${wdMap.listRow }" var="wdMap" varStatus="statues">
			     	 <tr>
				        <td>${wdMap.VNAME }</td>
					    <td align="right">${wdMap.TSNMONEY }</td>
					    <td align="right">${wdMap.MONEYZB }</td>
					    <td align="right">${wdMap.TSNCOUNT }</td>
						<td align="right">${wdMap.SLZB }</td>
						<td align="right">${wdMap.DJ }</td>
						<td align="right">${wdMap.IPEOLENUM }</td>
						<td align="right">${wdMap.RJ }</td>
						<td align="right">${wdMap.DJFWSJ }</td>
					</tr>
		     	</c:forEach>
				<tr bgcolor="yellow">
			        <td><fmt:message key="total" /></td>
				    <td align="right">${wdMap.wdMapSum.TSNMONEY }</td>
				    <td align="right"> </td>
				    <td align="right">${wdMap.wdMapSum.TSNCOUNT }</td>
					<td align="right"> </td>
					<td align="right">${wdMap.wdMapSum.DJ }</td>
					<td align="right">${wdMap.wdMapSum.IPEOLENUM }</td>
					<td align="right">${wdMap.wdMapSum.RJ }</td>
					<td align="right"> </td>
				</tr>
		     </tbody>
		  </table>
	</div>
	
	<div style="width:100%;">
	    <div style="width:25%;float:left;padding-top: 20px;padding-right: 5px;">
		   <table class="table table5" width="98%" cellpadding="0" cellspacing="0">
		     <thead>
		     	 <tr bgcolor="#c0c0c0">
		     	 	 <td colspan="3" align="center"><fmt:message key="category" />(<fmt:message key="middleClass" />)<fmt:message key="statistical" /></td>
		     	 </tr>
			     <tr>
				     <td width="75px"><fmt:message key="project" /></td>
				     <td width="75px" align="right"><fmt:message key="quantity" /></td>
				     <td align="right"><fmt:message key="amount" /></td>
			     </tr>
		     </thead>
		     <tbody>
			     <c:forEach items="${lbList}" var="lbMap" >
			          <tr>
					     <td width="75px">${lbMap.NAME }</td>
					     <td width="75px" align="right">${lbMap.NYCOUNT }</td>
					     <td width="75px" align="right">${lbMap.NYMONEY }</td>
				     </tr>
				  </c:forEach>
				  <tr bgcolor="yellow">
				     <td width="75px"><fmt:message key="total" /></td>
				     <td width="75px" align="right">${lbSumMap.NYCOUNT }</td>
				     <td width="75px" align="right">${lbSumMap.NYMONEY }</td>
				 </tr>
		     </tbody>
		   </table>
	   </div>
	    <div style="width:25%;float:left;padding-top: 20px;padding-right: 5px;">
		   <table class="table table6" width="98%" cellpadding="0" cellspacing="0">
		     <thead>
		     	 <tr bgcolor="#c0c0c0">
		     	 	 <td colspan="3" align="center"><fmt:message key="activity" /><fmt:message key="statistical" /></td>
		     	 </tr>
			     <tr>
				     <td width="75px"><fmt:message key="project" /></td>
				     <td width="75px"><fmt:message key="quantity" /></td>
			     <td width="60px"><fmt:message key="amount" /></td></tr>
		     </thead>
		     <tbody>
		          <c:forEach items="${hdList}" var="hdMap" >
			          <tr>
					     <td width="75px">${hdMap.VNAME }</td>
					     <td width="75px" align="right">${hdMap.SLCOUNT }</td>
					     <td width="60px" align="right">${hdMap.NMONEY }</td>
				     </tr>
				  </c:forEach>
				    <tr bgcolor="yellow">
					     <td width="75px"><fmt:message key="total" /></td>
					     <td width="75px" align="right">${hdSumMap.SLCOUNT }</td>
					     <td width="60px" align="right">${hdSumMap.NMONEY }</td>
				     </tr>
		     </tbody>
		   </table>
	   </div>
	    <div style="width:25%;float:left;padding-top: 20px;padding-right: 5px;">
		   <table class="table" width="98%" cellpadding="0" cellspacing="0">
		     <thead>
		     	 <tr bgcolor="#c0c0c0">
		     	 	 <td colspan="3" align="center"><fmt:message key="payment_types" /><fmt:message key="statistical" /></td>
		     	 </tr>
			     <tr>
				     <td><fmt:message key="project" /></td>
				     <td><fmt:message key="quantity" /></td>
			     <td><fmt:message key="amount" /></td></tr>
		     </thead>
		     <tbody>
		     	<c:forEach items="${jsfsList}" var="jsfsMap" >
			          <tr>
					     <td>${jsfsMap.VNAME }</td>
					     <td align="right">${jsfsMap.CNT }</td>
					     <td align="right">${jsfsMap.NMONEY }</td>
				     </tr>
				</c:forEach>
			    <tr bgcolor="yellow">
				     <td><fmt:message key="total" /></td>
				     <td align="right">${jsfsSumMap.CNT }</td>
				     <td align="right">${jsfsSumMap.NMONEY }</td>
			     </tr>
		     </tbody>
		   </table>
	   </div>
	    <div style="width:23%;float:left;padding-top: 20px;">
		   <table class="table" width="98%" cellpadding="0" cellspacing="0">
		     <thead>
		     	 <tr bgcolor="#c0c0c0">
		     	 	 <td colspan="3" align="center"><fmt:message key="settlement_java" /><fmt:message key="statistical" /></td>
		     	 </tr>
			     <tr>
				     <td><fmt:message key="project" /></td>
				     <td><fmt:message key="quantity" /></td>
				     <td><fmt:message key="amount" /></td>
				 </tr>
		     </thead>
		     <tbody>
		         <c:forEach items="${xfList}" var="xfsMap" >
			     	<tr>
						<td>${xfsMap.VNAME }</td>
					    <td align="right">${xfsMap.NNUM }</td>
					    <td align="right">${xfsMap.NMONEY }</td>
				    </tr>
				 </c:forEach>
				 <tr bgcolor="yellow">
				     <td><fmt:message key="total" /></td>
				     <td align="right">${tdSumMap.NNUM }</td>
				     <td align="right">${tdSumMap.NMONEY }</td>
			     </tr>
		     </tbody>
		   </table>
	   </div>
	 </div>
	 
	</div>
	
	<div id="wait2" style="visibility:hidden"></div>
    	<div id="wait" style="visibility:hidden;">
			<img src="<%=path%>/image/loading_detail.gif" />&nbsp;
			<span style="color:white;font-size:15px;vertical-align: middle;">正在查询，请稍后...</span>
		</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/boh/common/teleFunc-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 <script language="JavaScript" src="<%=path%>/Charts/FusionCharts.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		
  	 		
  	 		var tool = $('#tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable: true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position:['0px','0px']
						},
						handler: function(){
							$('#wait2').css("visibility","visible");
							$('#wait').css("visibility","visible");
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
						}
					}
				]
			});
  	 		
  	 	    //默认<fmt:message key="time" />
  	 	    if('${searchCondition.edat}'==''){
	  	 		$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
  	 	    }
	 		$("#bdat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
		    });
		    $("#edat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
		    });
  	 	});

		$('#btnSub').click(function(){
			$('#wait2').css("visibility","visible");
			$('#wait').css("visibility","visible");
			$('#queryForm').submit();
		});
  		
  	 </script>
  </body>
</html>