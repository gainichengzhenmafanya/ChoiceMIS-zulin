<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title><fmt:message key="actmbytbl"/></title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
	</style>
	<%@ include file="../share/permission.jsp"%>
  </head>	
  <body>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label"><fmt:message key="startdate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" />
			</div>
			<div class="form-label"><fmt:message key="enddate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" />
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="activity"/><fmt:message key="bigClass"/></div>
			<div class="form-input">
				<select id="pubgrptyp" name="pubgrptyp" class="select" style="width: 132px;">
					<option value=""><fmt:message key="all" /></option>
					<c:forEach var="acttyp" items="${acttypList}" varStatus="status">
						<option value="${acttyp.pk_ActTyp }" <c:if test="${condition.pubgrptyp==acttyp.pk_ActTyp }">selected="selected"</c:if> >${acttyp.vname }</option>
					</c:forEach>
				</select>
			</div>
			<div class="form-label"><fmt:message key="activity"/><fmt:message key="smallClass"/></div>
			<div class="form-input">
				<select id="pubgrp" name="pubgrp" class="select" style="width: 132px;">
					<option value=""><fmt:message key="all" /></option>
					<c:forEach var="acttypmin" items="${acttypminList}" varStatus="status">
						<option value="${acttypmin.pk_acttypmin }" <c:if test="${condition.pubgrp==acttypmin.pk_acttypmin }"> selected="selected" </c:if>>${acttypmin.vname }</option>
					</c:forEach>
				</select>
			</div>
			<div class="form-label"><fmt:message key="activity"/><fmt:message key="name"/></div>
			<div class="form-input">
				<input type="text" id="vname" name="vname" class="text"/>
			</div>
		</div>
		<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }"/>
	</form>
	<input type="hidden" id="reportName" value="actmbytbl"/>
	<input type="hidden" id="excelUrl" value="<%=path%>/MISBOHbusinessAnalysis/exportReport.do"/>
	<input type="hidden" id="dataUrl" value="<%=path%>/MISBOHbusinessAnalysis/queryActmByTbl.do"/>
	<input type="hidden" id="title" value="<fmt:message key="actmbytbl"/>"/>
	<div id="datagrid"></div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		
		 	//默认<fmt:message key="time" />
		 	$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
  	 		$("#bdat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
		    });
		    $("#edat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
		    });
  	  		
  	 		initTab();
  	 	});
  	 	
  		//初始化选中标签内容
  	 	function initTab(){
  	 		var excelUrl = $("#excelUrl").val();
  	 		var reportName = $("#reportName").val();
  	 		var dataUrl = $("#dataUrl").val();
  	 		var title = $("#title").val();
  	 		var grid = $("#datagrid");
  	 		queryParams = getParam($("#queryForm"));
  	 		
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp:true,
  	 			excelUrl:excelUrl+'?reportName='+reportName,
  	 			toolbar:['search','excel','exit'],
  	 			searchFun:initTab
  	 		});
  	 		
  	 		builtTable({
  	 			dataUrl:dataUrl,
  	 			title:title,
  	 			grid:grid,
  	 			createHeader:function(data,head,frozenHead){
  	 				var colFirst = [];
  	 				
//   	 				colFirst.push({field:'VNAME',width:120,title:'<fmt:message key="branches_name"/>',align:'left'});
  	 				colFirst.push({field:'DWORKDATE',width:80,title:'<fmt:message key="date"/>',align:'center'});
  	 				colFirst.push({field:'VBCODE',width:150,title:'<fmt:message key="bill_no"/>',align:'left'});
  	 				colFirst.push({field:'VACTCODE',width:100,title:'<fmt:message key="activity"/><fmt:message key="coding"/>',align:'left'});
  	 				colFirst.push({field:'VPAYMENTDES',width:150,title:'<fmt:message key="activity"/><fmt:message key="name"/>',align:'left'});
  	 				colFirst.push({field:'VTBLDES',width:70,title:'<fmt:message key="desk_no"/>',align:'left'});
  	 				colFirst.push({field:'IPEOLENUM',width:80,title:'<fmt:message key="number_of_people"/>',align:'right'});
  	 				colFirst.push({field:'NMONEY',width:80,title:'<fmt:message key="bill"/><fmt:message key="paid_in"/>',align:'right'});
  	 				colFirst.push({field:'NYMONEY',width:80,title:'<fmt:message key="bill"/><fmt:message key="accounts_receivable"/>',align:'right'});
  	 				colFirst.push({field:'ACTYM',width:80,title:'<fmt:message key="Preferential_remission_activities"/>',align:'right'});
  	 				colFirst.push({field:'ACTJS',width:80,title:'<fmt:message key="poundage"/>',align:'right'});
  	 				colFirst.push({field:'NOVERMONEY',width:80,title:'<fmt:message key="novermoney"/>',align:'right'});
  	 				colFirst.push({field:'DBRTIME',width:100,title:'<fmt:message key="operating_time"/>',align:'left'});
  	 				
  	 				head.push(colFirst);
  	 			}
  	 		});
	  	}
  		
  		//列选择后页面重新加载
  		function pageReload(){
  			closeColChooseWin();
  			initTab();
  		}
  	 </script>
  </body>
</html>