<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	  	<title>支付活动明细查询表</title>
	    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	</head>	
  	<body>
  		<%@ include file="../share/permission.jsp"%>
  		<div id="tool"></div>
	  	<form id="queryForm" name="queryForm" method="post">
			<div class="form-line">
				<div class="form-label"><fmt:message key="startdate" /></div>
				<div class="form-input">
					<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text"  value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" />
				</div>
	  			<div class="form-label"><fmt:message key="enddate" /></div>
				<div class="form-input">	
					<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" />
				</div>
				<div class="form-label"><fmt:message key="starttime" /></div>
				<div class="form-input">
					<input autocomplete="off" type="text" id="binterval" name="binterval" class="Wdate text" />
				</div>	
				<div class="form-label"><fmt:message key="endtime" /></div>
				<div class="form-input">
					<input autocomplete="off" type="text" id="einterval" name="einterval" class="Wdate text" />
				</div>	
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="flight" /></div>
				<div class="form-input">
					<select id="sft" name="sft" style="width: 133px;" class="select">
						<option value="0"><fmt:message key="all_day" /></option>
						<c:forEach var="sft" items="${listSft}">
							<option value="${sft.vcode}" <c:if test="${sft.vcode==condition.sft}"> selected="selected" </c:if> >${sft.vname}</option>
						</c:forEach>				
					</select>
				</div>	
				<div class="form-label"><fmt:message key="bill" /><fmt:message key="type" /></div>
				<div class="form-input">
					<select id="vorclass" name="vorclass" style="width: 133px;" class="select">
						<option value="0"><fmt:message key="all" /></option>
						<option value="1" <c:if test="${1==condition.vorclass}"> selected="selected" </c:if> >1--整单OK</option>
						<option value="2" <c:if test="${2==condition.vorclass}"> selected="selected" </c:if> >2--整单撤销</option>
						<option value="3" <c:if test="${3==condition.vorclass}"> selected="selected" </c:if> >3--单品撤销</option>
						<option value="4" <c:if test="${4==condition.vorclass}"> selected="selected" </c:if> >4--废弃</option>
						<option value="5" <c:if test="${5==condition.vorclass}"> selected="selected" </c:if> >5--膳食</option>
						<option value="6" <c:if test="${6==condition.vorclass}"> selected="selected" </c:if> >6--折扣</option>
						<option value="7" <c:if test="${7==condition.vorclass}"> selected="selected" </c:if> >7--反结算</option>
					</select>
				</div>	
				<div class="form-label"><fmt:message key="pay" /><fmt:message key="activity" /></div>
				<div class="form-input">
					<input type="text" id="paymentname" name="paymentname" class="text" />
				</div>
	  			<div class="form-label"><fmt:message key="bill_no" /></div>
				<div class="form-input">	
					<input type="text" id="vbcode" name="vbcode" class="text" />
				</div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="Table_number" /></div>
				<div class="form-input">	
					<input type="text" id="tbldes" name="tbldes" class="text" />
				</div>
				<div class="form-label"><fmt:message key="cashier" /></div>
				<div class="form-input">	
					<input type="text" id="vlastemp" name="vlastemp" class="text" />
				</div>
			</div>
			<input type="hidden" id="pk_store" name="pk_store" value="${pk_store}" />
		</form>
		<input type="hidden" id="reportName" value="paydetail"/>
		<input type="hidden" id="excelUrl" value="<%=path%>/MISBOHbusinessAnalysis/exportReport.do"/>
		<input type="hidden" id="dataUrl" value="<%=path%>/MISBOHbusinessAnalysis/queryPayDetail.do"/>
		<input type="hidden" id="title" value="<fmt:message key='pay' /><fmt:message key='activity' /><fmt:message key='detail_query' />"/>
		<div id="datagrid"></div>
	 	<script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  		<script type="text/javascript">
	  	 	$(document).ready(function(){
	  	 		//默认<fmt:message key="time" />
	  	 		$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
	  	 		$("#bdat").click(function(){
			        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
			    });
			    $("#edat").click(function(){
			        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
			    });
		  		$("#binterval").click(function(){
		  			new WdatePicker({maxDate:'#F{$dp.$D(\'einterval\')}',dateFmt:'HH:mm:ss'});
		  		});
		  		$("#einterval").click(function(){
		  			new WdatePicker({minDate:'#F{$dp.$D(\'binterval\')}',dateFmt:'HH:mm:ss'});
		  		});
		  		
				//初始化<fmt:message key="selected" />标签<fmt:message key="content" />
				initTab();
	  	 	});
  	 	
	  		//初始化<fmt:message key="selected" />标签<fmt:message key="content" />
	  	 	function initTab(){
	  	 		var excelUrl = $("#excelUrl").val();
	  	 		var reportName = $("#reportName").val();
	  	 		var dataUrl = $("#dataUrl").val();
	  	 		var title = $("#title").val();
	  	 		var grid = $("#datagrid");
	  	 		queryParams = getParam($("#queryForm"));
	  	 		
	  	 		builtToolBar({
	  	 			basePath:"<%=path%>",
	  	 			toolbarId:'tool',
	  	 			formId:'queryForm',
	  	 			grid:grid,
	  	 			exportTyp:true,
	  	 			excelUrl:excelUrl+'?reportName='+reportName,
	  	 			toolbar:['search','excel','exit'],
	  	 			searchFun:function(){
	  	 				initTab();
	  	 			}
	  	 		});
	  	 		
	  	 		builtTable({
	  	 			dataUrl:dataUrl,
	  	 			title:title,
	  	 			grid:grid,
	  	 			createHeader:function(data,head,frozenHead){
	  	 				var col1 = [];
	  	 				var col2 = [];
	  	 				
	  	 				frozenHead.push([{field:'DWORKDATE',width:100,title:'<fmt:message key="business_day" />',align:'left',rowspan:2,sortable:true},	//营业日
		  	 							 {field:'VBCODE',width:190,title:'<fmt:message key="bill_no" />',align:'left',rowspan:2,sortable:true},			//账单号
	  	 				 				 {field:'VTABLENUM',width:80,title:'<fmt:message key="desk_no" />',align:'left',rowspan:2,sortable:true}]);		//台位号
	  	 				
	  	 				//支付方式
	  	 				col2.push({title:'<fmt:message key="payment" />',align:'center',colspan:3});
	  	 				col1.push({field:'VOPERATE',width:80,title:'<fmt:message key="coding" />',align:'left',sortable:true});	//编码
	  	 				col1.push({field:'PAYNAME',width:100,title:'<fmt:message key="name" />',align:'left',sortable:true});	//名称
						col1.push({field:'NMONEY',width:80,title:'<fmt:message key="amount" />',align:'right',sortable:true});	//金额
						
						//券
						col2.push({title:'<fmt:message key="voucher" />',align:'center',colspan:4});
						col1.push({field:'ICASHCOUNT',width:80,title:'<fmt:message key="quantity" />',align:'right',sortable:true});		//数量
	  	 				col1.push({field:'NCASHONE',width:80,title:'<fmt:message key="denomination" />',align:'right',sortable:true});		//面额
	  	 				col1.push({field:'NOVERCASH',width:80,title:'<fmt:message key="novermoney" />',align:'right',sortable:true});		//超收
	  	 				col1.push({field:'NPAIDMONEY',width:80,title:'<fmt:message key="preferential" />',align:'right',sortable:true});	//优惠
	  	 				
	  	 				col2.push({field:'NPOUNDAGE',width:80,title:'<fmt:message key="poundage" />',align:'right',rowspan:2,sortable:true});//手续费
	  	 				
	  	 				//收银员
	  	 				col2.push({width:150,title:'<fmt:message key="cashier" />',align:'center',colspan:2});
	  	 				col1.push({field:'VECODE',width:80,title:'<fmt:message key="coding" />',align:'left',sortable:true});				//编码
	  	 				col1.push({field:'VENAME',width:100,title:'<fmt:message key="name" />',align:'left',sortable:true});					//名称
	  	 				
	  	 				//账单
	  	 				col2.push({width:150,title:'<fmt:message key="bill" />',align:'center',colspan:5});
	  	 				col1.push({field:'VORCLASS',width:80,title:'<fmt:message key="type" />',align:'left',sortable:true});				//类型
	  	 				col1.push({field:'IPEOLENUM',width:80,title:'<fmt:message key="number_of_people" />',align:'right',sortable:true});	//人数
	  	 				col1.push({field:'AMT',width:80,title:'<fmt:message key="accounts_receivable" />',align:'right',sortable:true});	//应收
	  	 				col1.push({field:'JAMT',width:80,title:'<fmt:message key="paid_in" />',align:'right',sortable:true});				//实收
	  	 				col1.push({field:'YMAMT',width:80,title:'<fmt:message key="preferential" />',align:'right',sortable:true});			//优惠
	  	 				
	  	 				col2.push({field:'TCTIME',width:130,title:'<fmt:message key="operating_time" />',align:'left',rowspan:2,sortable:true});	//操作时间
	  	 				col2.push({field:'ICHANGETABLE',width:80,title:'<fmt:message key="flight" />',align:'left',rowspan:2,sortable:true});		//班次
	  	 				col2.push({field:'NSVR',width:80,title:'<fmt:message key="service_fees_javapro" />',align:'right',rowspan:2,sortable:true});//服务费		
	  	 				col2.push({field:'NBZERO',width:80,title:'<fmt:message key="maLing" />',align:'right',rowspan:2,sortable:true});			//抹零
	  	 				
	  	 				head.push(col2);
	  	 				head.push(col1);
	  	 			}
	  	 		});
	  	 	}
	  		//列<fmt:message key="select1" />后页面重新加载
	  		function pageReload(){
	  			initTab();
	  	  		$('#tabs').tabs('getSelected').panel("body").find("#datagrid").datagrid('reload');
	  		}
  		</script>
	</body>
</html>