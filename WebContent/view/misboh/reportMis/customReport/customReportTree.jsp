<%@ page import="com.choice.orientationSys.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
	String rootId = StringConstant.ROOT_ID;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>自定义报表</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<style type="text/css">
			.treePanel{
				width:99%;
			}
			</style>
		</head>
	<body>
		<div class="leftFrame" style="width: 16%;">
        	
        	<div id="toolbar"></div>
	    	<div class="treePanel" style="height: 475px;">
		        <script src="<%=path%>/js/tree/MzTreeView10.js" type="text/javascript"></script>
	        	<script type="text/javascript">
	          		var tree = new MzTreeView("tree");
	          		tree.nodes['0_00000000000000000000000000000000'] = 'text:<fmt:message key="all" />;method:changeUrl("0","00000000000000000000000000000000","0","0")';
	          		<c:forEach var="store" items="${listCustomReportTree}" varStatus="status">
	          		tree.nodes["${store['PARENTID']}_${store['PK_CUSTOMREPORT']}"] 
		          		= "text:${store['VNAME']}; method:changeUrl('${store['PK_CUSTOMREPORT']}','${store['CHILDRENID']}','${store['VNAME']}','${store['ORDERID']}')";
	          		</c:forEach>
	          		 tree.setIconPath("<%=path%>/image/tree/none/");
	          		document.write(tree.toString());
	        	</script>
	    	</div>
    	</div>
    	
	    <div class="mainFrame" style="width:84%;height:90%;">
	    <div id="toolbar2"></div>
<%-- 	      	<form id="listForm" action="<%=path%>/customReportMisBoh/exportFile.do" method="post"> --%>
<!-- 	      		<input type="hidden" id="orderid" name="orderid" value="0" /> -->
<!-- 	      		<input type="hidden" id="pk_customReport" name="pk_customReport" /> -->
<!-- 	      		<input type="hidden" id="typ" name="typ"/> -->
<!-- 	      		<input type="hidden" id="pk_store" name="pk_store" /> -->
<!-- 			    <input type="hidden" id="bdat" name="bdat" /> -->
<!-- 			    <input type="hidden" id="edat" name="edat" /> -->
<!-- 			    <input type="hidden" id="vname" name="vname" /> -->
<!-- 	      	</form> -->
	  		<form id="listForm" name="listForm" method="post" action="<%=path%>/customReportMisBoh/exportFile.do">
	  			<input type="hidden" id="orderid" name="orderid" value="0" />
	  			<input type="hidden" id="pk_customReport" name="pk_customReport" />
	  			<input type="hidden" id="reportPath" name="reportPath" />
	  			<input type="hidden" id="vname" name="vname" />
	  			<input type="hidden" id="type" name="type"/>
	  			<div class="bj_head" style="display:none;">
				<div class="form-line">
					<div class="form-label week"><fmt:message key="Weekly"/></div>
					<div class="form-input week">
						<input type="text" id="week" name="week" class="Wdate text" onclick="new WdatePicker({isShowWeek:true,onpicked:function() {$dp.$('d122_1').value=$dp.cal.getP('W','W');$dp.$('d122_2').value=$dp.cal.getP('W','WW');}});"/>
					</div>
					<div class="form-label startDateFlag"><fmt:message key="startdate"/></div>
					<div class="form-input startDateFlag"><input type="text" id="bdat" name="bdat" class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/></div>
					<div class="form-label storeFlag"><fmt:message key="positions" /></div>
					<div class="form-input storeFlag">
						<input type="text"  id="positn_name" class="text" style="margin-bottom: 4px;" name="firmDes" readonly="readonly"/>
						<input type="hidden" id="positn" name="positn"/>
						<img id="seachDept" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="branches_selection"/>' />
					</div>
					<div class="form-label typ"><fmt:message key="smallClass"/></div>
					<div class="form-input typ" style="margin-top:-4px;">
						<input type="text" id="typdes" class="text"  name="typdes" readonly="readonly"/>
						<input type="hidden" id="typ" name="typ"/>
						<img id="seachTyp" style="margin-top:1px;" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_category"/>' />
				    </div>
				</div>
				<div class="form-line">
					<div class="form-label endDateFlag"><fmt:message key="enddate"/></div>
					<div class="form-input endDateFlag"><input type="text" id="edat" name="edat" class="Wdate text" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/></div>
					<div class="form-label sp_code"><fmt:message key="supplies_code"/></div>
					<div class="form-input sp_code">
						<input type="text" style="margin-top:0px;margin-bottom:4px;vertical-align:middle;" class="text" id="sp_code" name="sp_code" />
						<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
					</div>
				</div>
				</div>
			</form>
	      <iframe frameborder="0" name="mainFrame" id="mainFrame" style="width:100%;height:88%;overflow:auto;"></iframe>
	    </div>
       <div id="wait2" style="visibility: hidden;"></div>
    	<div id="wait" style="visibility:hidden;">
			<img src="<%=path %>/image/loading_detail.gif" />&nbsp;
		</div>
       
		<script type="text/javascript" src="<%=path%>/js/system/department.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
			function changeUrl(pk_customReport,reportPath,reportName,orderid){
				$('.bj_head').find('div:not(".form-line")').hide();
				window.mainFrame.location.href = 'about:blank';
				$('#pk_customReport').val(pk_customReport);
				$('#orderid').val(orderid);
				$('#reportPath').val(reportPath);
				$('#vname').val(reportName);
				if(orderid=='2'){
					$.ajax({
						url:'<%=path %>/customReportMisBoh/findReport.do?pk_customReport='+pk_customReport,
						type:'post',
						success:function(custom){
							var flags = custom.flags;
							for(var i in flags){
								var flag = flags[i];
								$('.'+flag).show();
							}
							//日期默认
							$('#bdat').val(custom.startDateFlag);
							$('#edat').val(custom.startDateFlag);
							$('#week').val(custom.startDateFlag);
							//仓位默认门店
							$('#positn').val(custom.positnCode);
							$('#positn_name').val(custom.positnDes);
							$('.bj_head').show();
						}
					});
				}
			}
			
		    function refreshTree(){
				window.location.href = '<%=path%>/customReportMisBoh/customReportTree.do';
		    }
		    
		    function pageReload(){
		    	window.location.href = '<%=path%>/customReportMisBoh/customReportTree.do';
		    }
	    
			$(document).ready(function(){
				var toolbar = $('#toolbar').toolbar({
					items: [{
							text: '<fmt:message key="expand" />',
							title: '<fmt:message key="expandAll"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-80px']
							},
							handler: function(){
								tree.expandAll();
							}
						},{
							text: '<fmt:message key="refresh" />',
							title: '<fmt:message key="refresh"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','0px']
							},
							handler: function(){
								refreshTree();
							}
						},{
							text: '<fmt:message key="insert" />',
							title: '<fmt:message key="insert" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','0px']
							},
							handler: function(){
								var orderid=$('#orderid').val();
								if(orderid=='1'){
									saveReport();
								}else if(orderid=='0'){
									saveFirstMenu();
								}
							}
						},{
							text: '<fmt:message key="edit" />',
							title: '<fmt:message key="edit" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','0px']
							},
							handler: function(){
								var orderid=$('#orderid').val();
								if(orderid=='2'){
									//editReport();
									alert('<fmt:message key="Not_support_to_modified_the_second_level_nodes_now" />!');
								}else if(orderid=='1'){
									editFirstMenu();
								}
							}
						},{
							text: '<fmt:message key="delete" />',
							title: '<fmt:message key="delete" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','0px']
							},
							handler: function(){
								var orderid=$('#orderid').val();
								if(orderid!='0'){
									if(confirm('<fmt:message key="suredel" />？')){
										  $.ajaxSetup({async:false});
											$.post("<%=path %>/customReportMisBoh/deleteCustomReport.do?pk_customReport="+$('#pk_customReport').val(),function(data){
												alert('<fmt:message key="successful_deleted" />');
												refreshTree();
											});
									}
								}else{
									alert('<fmt:message key="The_node_cannot_delete" />');
								}
							}
						}
					]
				});//end toolbar
				var toolbar2 = $('#toolbar2').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-80px']
							},
							handler: function(){
								if($('#bdat').val()!='' && $('#edat').val()!=''){
									$('#wait2').css("visibility","visible");
									$('#wait').css("visibility","visible");
									var reportPath = $('#reportPath').val();
									var vname = $('#vname').val();
									var bdat = $('#bdat').val();
									var edat = $('#edat').val();
		 							var positn = $('#positn').val();
									var sp_code = $('#sp_code').val();
									var typ = $('#typ').val();
									var week = $('#week').val();
									$.ajax({
										url:'<%=path %>/customReportMisBoh/toReport.do?bdat='+bdat+'&edat='+edat+'&positn='+positn+'&typ='+typ+'&sp_code='+sp_code+'&reportPath='+reportPath+'&vname='+encodeURI(encodeURI(vname))+'&week='+week,
										type:'post',
										success:function(msg){
											$('#wait2').css("visibility","hidden");
											$('#wait').css("visibility","hidden");
											var date = new Date();
											window.mainFrame.location.href = "<%=path %>/report/misboh/customReport/"+reportPath+".html?date="+date;
										}
									});
								}else{
									alert('<fmt:message key="date" /><fmt:message key="cannot_be_empty" />！');
								}
							}
						},{
						text: '<fmt:message key="export" />',
						title: '<fmt:message key="export" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-60px','0px']
						},
						items:[
								{
									text: 'EXCEL',
									title: 'EXCEL',
									useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-60px','0px']
									},
									handler: function(){
										var orderid=$('#orderid').val();
										if(orderid=='2'){
											if($('#bdat').val()!='' && $('#edat').val()!=''){
												$('#type').val("excel");
												$('#listForm').submit();
											}
										}else{
											alert('<fmt:message key="The_node_cannot_be_exported" />!');
										}
									}
								},{
									text: 'PDF',
									title: 'PDF',
									useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-60px','0px']
									},
									handler: function(){
										var orderid=$('#orderid').val();
										if(orderid=='2'){
											if($('#bdat').val()!='' && $('#edat').val()!=''){
												$('#type').val("pdf");
												$('#listForm').submit();
											}
										}else{
											alert('<fmt:message key="The_node_cannot_be_exported" />!');
										}
									}
								},{
									text: 'Word',
									title: 'Word',
									useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-60px','0px']
									},
									handler: function(){
										var orderid=$('#orderid').val();
										if(orderid=='2'){
											if($('#bdat').val()!='' && $('#edat').val()!=''){
												$('#type').val("word");
												$('#listForm').submit();
											}
										}else{
											alert('<fmt:message key="The_node_cannot_be_exported" />!');
										}
									}
								}
							]
						},{
							text: '<fmt:message key="print" />',
							title: '<fmt:message key="print" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','0px']
							},
							handler: function(){
								var orderid=$('#orderid').val();
								if(orderid=='2'){
									$('#listForm').attr('target','report');
									window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
									var action="<%=path%>/customReportMisBoh/printReport.do";
									$('#listForm').attr('action',action);
									$('#listForm').submit();
									$("#listForm").attr("action","<%=path%>/customReportMisBoh/exportFile.do");
									$('#listForm').attr('target','');
								}else{
									alert('<fmt:message key="The_node_cannot_print" />!');
								}
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$(window.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').click();
							}
						}
					]
				});//end toolbar
				
				$('#seachSupply').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#sp_code').val();
						top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/misbohcommon/selectSupplyLeft.do?sta=all&positn=1&defaultCode='+defaultCode,$('#sp_code'));	
					}
				});
	  	 		
	  	 		/*弹出树*/
				$('#seachTyp').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode1 = $('#typ').val();
						var offset = getOffset('typ');
						top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/misbohcommon/selectMoreGrpTyp.do?defaultCode='+defaultCode1),offset,$('#typdes'),$('#typ'),'320','500','isNull');
					}
				});
	  	 		
				/*弹出树*/
				$('#seachDept').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#positn').val();
						var defaultName = $('#positn_name').val();
						//alert(defaultCode+"==="+defaultName);
						var offset = getOffset('positn');
						top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/findPositnSuper.do?typn='+'7&iffirm=1&mold='+'oneTone&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positn_name'),$('#positn'),'760','520','isNull');
					}
				});
			});
			

			function saveFirstMenu(){
				$('body').window({
					id: 'window_saveInceom',
					title: '<fmt:message key="Add_first_level_node" />',
					content: '<iframe id="SaveForm" frameborder="0" src="<%=path%>/customReportMisBoh/add.do?type=1"></iframe>',
					width: '400px',
					height: '100px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								title: '<fmt:message key="save" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('SaveForm')&&window.document.getElementById("SaveForm").contentWindow.validate._submitValidate()){
										submitFrameForm('SaveForm','customReportForm');
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			function saveReport(){
				$('body').window({
					id: 'window_saveInceom',
					title: '<fmt:message key="Add_custom_report" />',
					content: '<iframe id="SaveForm" frameborder="0" src="<%=path%>/customReportMisBoh/add.do?type=2&parentid='+$('#pk_customReport').val()+'"></iframe>',
					width: '650px',
					height: '400px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								title: '<fmt:message key="save" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('SaveForm')&&window.document.getElementById("SaveForm").contentWindow.validate._submitValidate()){
										submitFrameForm('SaveForm','customReportForm');
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			function editFirstMenu(){
				$('body').window({
					id: 'window_saveInceom',
					title: '<fmt:message key="update_first_level_node" />',
					content: '<iframe id="SaveForm" frameborder="0" src="<%=path%>/customReportMisBoh/edit.do?type=1&pk_customReport='+$('#pk_customReport').val()+'"></iframe>',
					width: '400px',
					height: '100px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								title: '<fmt:message key="save" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('SaveForm')&&window.document.getElementById("SaveForm").contentWindow.validate._submitValidate()){
										submitFrameForm('SaveForm','customReportForm');
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			function editReport(){
				$('body').window({
					id: 'window_saveInceom',
					title: '<fmt:message key="update_custom_report" />',
					content: '<iframe id="SaveForm" frameborder="0" src="<%=path%>/customReportMisBoh/edit.do?type=2&pk_customReport='+$('#pk_customReport').val()+'"></iframe>',
					width: '650px',
					height: '400px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								title: '<fmt:message key="save" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('SaveForm')&&window.document.getElementById("SaveForm").contentWindow.validate._submitValidate()){
										submitFrameForm('SaveForm','customReportForm');
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			
			$("#resetSearch").bind('click', function() {
				$('.search-condition input').val('');
			});
			
		</script>

	</body>
</html>