<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="branches_and_positions_information" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<style type="text/css">
			.form {
				position: relative;
				width: 90%;
			}
		</style>
	</head>
	<body>
		<div class="form">
			<form id="customReportForm" method="post" action="<%=path %>/customReportMisBoh/saveCustomReport.do">
				
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="report_name" />：</div>
					<div class="form-input">
						<input type="text" id="vname" name="vname" class="text" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="File_name" />：</div>
					<div class="form-input">
					    <input type="hidden" value="${parentid}" name="parentid"/>
						<input type="text" id="filePath" name="filePath" class="text" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="Conditional_choice" />：</div>
<!-- 					<div class="form-input"> -->
<!-- 						<input type="checkbox" id="storeFlag" name="storeFlag" class="text" />门店 -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 				<div class="form-line"> -->
<!-- 					<div class="form-label">&nbsp;</div> -->
<!-- 					<div class="form-input"> -->
<!-- 						<input type="checkbox" id="actmFlag" name="actmFlag" class="text" />活动 -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 				<div class="form-line"> -->
<!-- 					<div class="form-label">&nbsp;</div> -->
<!-- 					<div class="form-input"> -->
<!-- 						<input type="checkbox" id="paymodeFlag" name="paymodeFlag" class="text" />支付方式 -->
<!-- 					</div> -->
<!-- 				</div> -->
					<div class="form-input">
						<input type="checkbox" id="startDateFlag" name="startDateFlag" class="text" /><fmt:message key="startdate" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label">&nbsp;</div>
					<div class="form-input">
						<input type="checkbox" id="endDateFlag" name="endDateFlag" class="text" /><fmt:message key="enddate" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label">&nbsp;</div>
					<div class="form-input">
						<input type="checkbox" id="typ" name="typ" class="text" /><fmt:message key="smallClass"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label">&nbsp;</div>
					<div class="form-input">
						<input type="checkbox" id="sp_code" name="sp_code" class="text" /><fmt:message key="supplies_code"/>
					</div>
				</div>
			</form>
			
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		
		<script type="text/javascript">
		$('option[value=${servicefeeset.enablestate }]').attr("selected","selected");
		$(document).ready(function(){
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'vname',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'filePath',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="cannot_be_empty" />！']
					}
					]
				});
				
			});
			
		</script>
	</body>
</html>