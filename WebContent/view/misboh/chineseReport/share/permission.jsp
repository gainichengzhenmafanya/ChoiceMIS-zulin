<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<script type="text/javascript">
	var report_search_perm_tele = ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')};
	var report_export_perm_tele = ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')};
	var report_print_perm_tele = ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')};
	var ROLE_TIME_beforeDay = <c:out value="${roleTimeMap.beforeDay}" default="30"/>;
	var ROLE_TIME_afterDay = <c:out value="${roleTimeMap.afterDay}" default="30"/>;
</script>
<div id="wait2" style="visibility: hidden;"></div>
    	<div id="wait" style="visibility: hidden;">
		<img src="<%=request.getContextPath() %>/image/loading_detail.gif" />&nbsp;
		<span style="color:white;font-size:15px;vertical-align: middle;"><fmt:message key="being" /><fmt:message key="export" /><fmt:message key="data" />,<fmt:message key="please_wait" />...</span>
  	</div>