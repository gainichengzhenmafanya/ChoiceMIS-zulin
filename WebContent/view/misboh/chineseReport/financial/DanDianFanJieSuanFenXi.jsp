<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>单店反结算分析</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
		.leftDiv{
			float: left;
			height: 100%;
			width: 120px;
			padding-left: 20px;
			line-height: 30px;
			padding-top: 30px;
		}
		.chartdiv{
			height: 100%;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		.datagrid{
			width: 30%;
		}
	</style>
	<%@ include file="../share/permission.jsp"%>
  </head>	
  <body>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label"><fmt:message key="startdate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" />
			</div>
			<div class="form-label"><fmt:message key="enddate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" />
			</div>
			<div class="form-input"><input type="hidden" name="vbcode" id="vbcode"/></div>
		</div>
		<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }"/>
	</form>
	<div class="easyui-tabs" fit="false" plain="true" id="tabs">
		<div title="<fmt:message key="bill" /><fmt:message key="the_detail" /><fmt:message key="list" />">
			<input type="hidden" id="reportName" value="ddfjsfx"/>
			<input type="hidden" id="excelUrl" value="<%=path%>/chineseFinancialReport/exportReport.do"/>
			<input type="hidden" id="dataUrl" value="<%=path%>/chineseFinancialReport/queryDdfjsfx.do"/>
			<input type="hidden" id="decimal" value="2" />
			<input type="hidden" id="title" value="<fmt:message key="bill" /><fmt:message key="the_detail" /><fmt:message key="list" />"/>
				<div id="datagrid"></div>
		</div>
		<div  id="zdmx" title="<fmt:message key="return_money" /><fmt:message key="the_detail" /><fmt:message key="list" />">
			<input type="hidden" id="reportName" value="ddfjsfxbymx"/>
			<input type="hidden" id="excelUrl" value="<%=path%>/chineseFinancialReport/exportReport.do"/>
			<input type="hidden" id="dataUrl" value="<%=path%>/chineseFinancialReport/findDdfjsmx.do"/>
			<input type="hidden" id="title" value="<fmt:message key="return_money" /><fmt:message key="the_detail" /><fmt:message key="list" />"/>
				<div id="gridShow"><div id="datagrid"></div></div>
		</div>
	</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		//默认<fmt:message key="time" />
		 	$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
  	 		$("#bdat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
		    });
		    $("#edat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
		    });
  	 		$("#tabs").tabs({onSelect:function(){
	 				//初始化选中标签内容
					initTab(false);
  	 			}
  	 		});
  	 	});
  	 	
  		//初始化选中标签内容
  	 	function initTab(rebuild){
  			tab = $('#tabs').tabs('getSelected');//获取选中的面板
	 		var content = tab.panel("body");
  	 		var reportName = content.find("#reportName").val();
  	 		var excelUrl = content.find("#excelUrl").val();
  	 		var dataUrl = content.find("#dataUrl").val();
  	 		var title = content.find("#title").val();
  	 		var grid = content.find("#datagrid");
  	 		
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp:true,
  	 			excelUrl:excelUrl+'?reportName='+reportName,
  	 			colsChooseUrl:'<%=path%>/chineseFinancialReport/toColumnsChoose.do?reportName='+reportName,
  	 			toolbar:['search','excel','option','exit'],
  	 			searchFun:function(){
//   	 				if(reportName=="ddfjsfxbymx")	return false;
  	 				content.find('#datagrid').datagrid('load',getParam($("#queryForm")));
  	 			}
  	 		});
  	 		if(rebuild || grid.css('display') != 'none'){
  	  				firstLoad = true;//重置第一次加载标志
		  	 		builtTable({
		  	 			headUrl:"<%=path%>/chineseFinancialReport/findHeaders.do?reportName="+reportName,
		  	 			dataUrl:dataUrl,
		  	 			title:title,
		  	 			grid:grid,
		  	 			numCols:['nmoney','actymamt','nsvcchg','nsvcitem','nbzero','earnamt'],
		  	 			decimalDigitF:$('#decimal').val(),
		  	 			onDblClickRow:function(index,data){
		  	 				if(reportName!="ddfjsfx") return;
		  	 				var serials = {};
		  	 				$('#tabs').tabs("select",'<fmt:message key="return_money" /><fmt:message key="the_detail" /><fmt:message key="list" />');
		  	 				serials['vbcode'] = data['VBCODE'];
		  	 				serials['pk_store'] = $('#pk_store').val();
		  	 				serials['bdat'] = data['DWORKDATE'];
		  	 				$('#vbcode').val(data['VBCODE']);
		  	 				$('#bdat').val(data['DWORKDATE']);
		  	 				$('#edat').val(data['DWORKDATE']);
		  	 					$('#tabs').tabs('getSelected').panel("body").find("#datagrid").datagrid('reload',serials);
		  	 			}
		  	 		});
  	  			}
  	 	}
  		//列选择后页面重新加载
  		function pageReload(){
  			closeColChooseWin();
  			initTab(true);
  	  		$('#tabs').tabs('getSelected').panel("body").find("#datagrid").datagrid('reload');
  		}
  	 </script>
  </body>
</html>