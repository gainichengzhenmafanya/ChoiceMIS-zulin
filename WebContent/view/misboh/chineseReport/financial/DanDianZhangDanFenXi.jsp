<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>单店账单分析</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
		.form-label2{
			width: 40px;
			height: 25px;
			position: relative;
			margin: 0;
			float: left;
			text-align: right;
			vertical-align: middle;
			line-height: 25px;
			margin-right: 5px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 6.5%;
		}
		form .form-line .form-input{
			width: 13%;
		}
		form .form-line .form-input input[type=text]{
			width: 85%;
		}
	</style>
	<%@ include file="../share/permission.jsp"%>
  </head>	
  <body>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label2"></div>
			<div class="form-input" style="width:250px;">
				<input type="radio" name="vorclass" id="all" value=""/><label for="all"><fmt:message key="all" /></label>
				<input type="radio" name="vorclass" id="C" value="C" checked="checked" /><label for="C"><fmt:message key="closed" /></label>
				<input type="radio" name="vorclass" id="A" value="A" /><label for="A"><fmt:message key="outstanding" /></label>
				<input type="radio" name="vorclass" id="D" value="D" /><label for="D"><fmt:message key="cancel" /></label>
				<input type="radio" name="vorclass" id="R" value="R" /><label for="R"><fmt:message key="vorder" /></label>
			</div>
			<div class="form-input" style="width:200px;margin-left: 15px;">
				<input type="checkbox" name="repayment" id="repayment" value="1" /><label for="repayment"><fmt:message key="Only_anti_settlement" /></label>
				<input type="checkbox" name="ncomp" id="ncomp" value="1" /><label for="ncomp"><fmt:message key="only_server" /></label>
				<input type="checkbox" name="ntmptsfr" id="ntmptsfr" value="1" /><label for="ntmptsfr"><fmt:message key="only_acct" /></label>
			</div>
			<div id="djyclable" class="form-label" style="width:155px;display: none;">
				<input type="checkbox" name="pubgrp" id="pubgrp" value="1" /><fmt:message key="only_unusual_price" />
			</div>
		</div>
		<div class="form-line">
			<div class="form-label" style="margin-left:23px;"><fmt:message key="startdate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" />
			</div>
			<div class="form-label2" style="width: 10px;margin-left:18px;"></div>
			<div class="form-label"><fmt:message key="enddate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" />
			</div>
			<div class="form-label2" style="width: 10px;"></div>
			<div class="form-label"><fmt:message key="flight" /></div>
			<div class="form-input">
				<select id="sft" name="sft" class="select" style="width:130px;">
					<option value="0"><fmt:message key="all_day" /></option>
					<c:forEach var="sft" items="${sftList}">
						<option value="${sft.vcode}" <c:if test="${sft.vcode==condition.sft}"> selected="selected" </c:if> >${sft.vname}</option>
					</c:forEach>					
				</select>
			</div>			
		</div>
		<div class="form-line">
			<div class="form-label2" style="width:98px;"><fmt:message key="Taiwan" /> </div>
			<div class="form-label" style="margin-left:7px;">
				<input type="text" name="tbldes" class="text" style="width:128px;" />
			</div>
			<div class="form-label2" style="margin-left:139px;"><fmt:message key="The_cashier" /></div>
			<div class="form-label" style="margin-left:5px;">
				<input type="text" name="vlastemp" class="text" style="width:129px;" />
			</div>
			<div class="form-label2" style="margin-left:122px;"><fmt:message key="bill_no" /></div>
			<div class="form-label" style="margin-left:6px;">
				<input type="text" name="vbcode" class="text" style="width:128px;" />
			</div>
<%-- 			<div class="form-label2" style="margin-left:100px;"><fmt:message key="serial_number" /></div> --%>
<!-- 			<div class="form-label"> -->
<!-- 				<input type="text" name="iclasses" class="text" style="width:128px;" /> -->
<!-- 			</div> -->
		</div>
		<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }"/>
	</form>
	<div class="easyui-tabs" fit="false" plain="true" id="tabs">
		<div title="<fmt:message key="The_table_shows" />">
			<input type="hidden" id="reportName" value="ddzdfx_bytable"/>
			<input type="hidden" id="excelUrl" value="<%=path%>/chineseFinancialReport/exportReport.do"/>
			<input type="hidden" id="dataUrl" value="<%=path%>/chineseFinancialReport/queryDdzdfx.do"/>
			<input type="hidden" id="title" value="<fmt:message key="The_table_shows" />"/>
				<div id="gridShow"><div id="datagrid"></div></div>
		</div>	
		<div  id="zdmx" title="<fmt:message key="bill" /><fmt:message key="the_detail" />">
			<input type="hidden" id="reportName" value="ddzdfx_byinfo"/>
			<input type="hidden" id="excelUrl" value="<%=path%>/chineseFinancialReport/exportReport.do"/>
			<input type="hidden" id="dataUrl" value="<%=path%>/chineseFinancialReport/findZhangDanMingXi.do"/>
			<input type="hidden" id="title" value="<fmt:message key="bill" /><fmt:message key="the_detail" />"/>
				<div id="gridShow"><div id="datagrid"></div></div>
		</div>
		<div title="<fmt:message key="collect" /><fmt:message key="Report_form" />">
			<input type="hidden" id="reportName" value="ddzdfx_byall"/>
			<input type="hidden" id="excelUrl" value="<%=path%>/chineseFinancialReport/exportReport.do"/>
			<input type="hidden" id="dataUrl" value="<%=path%>/chineseFinancialReport/queryDdzdfxByHuiZong.do"/>
			<input type="hidden" id="title" value="<fmt:message key="collect" /><fmt:message key="Report_form" />"/>
				<div id="gridShow"><div id="datagrid"></div></div>
		</div>
	</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		
		 	//默认<fmt:message key="time" />
		 	$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
  	 		$("#bdat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
		    });
		    $("#edat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
		    });
  	  		
	  	  	$("#tabs").tabs({onSelect:function(){
					//初始化选中标签内容
					initTab(false);
				}
			});
  	 		
  	 	});
  	 	
  		//初始化选中标签内容
  	 	function initTab(){
  	 		tab = $('#tabs').tabs('getSelected');//获取选中的面板
  	 		var content = tab.panel("body");
  	 		var excelUrl = content.find("#excelUrl").val();
  	 		var reportName = content.find("#reportName").val();
  	 		var headUrl = "";
  	 		var dataUrl = content.find("#dataUrl").val();
  	 		var title = content.find("#title").val();
  	 		var grid = content.find("#datagrid");
  	 		queryParams = getParam($("#queryForm"));
  	 		var tool = "";
  	 		
  	 		if(reportName == "ddzdfx_byall"){
  	 			headUrl = "<%=path%>/chineseFinancialReport/findHeaders.do?reportName="+reportName;
  	 			tool = ['search','excel','option','exit'];
  	 		}else{
  	 			headUrl = "<%=path%>/chineseFinancialReport/findHeaders.do?reportName="+reportName;
  			}
  			if(reportName == "ddzdfx_byinfo"){
  				headUrl = "<%=path%>/chineseFinancialReport/findHeaders.do?reportName="+reportName;
  	 			tool = ['search','excel','option','exit'];
  	 			$('#djyclable').css("display","block");
  	 		}else{
  	 			headUrl = "<%=path%>/chineseFinancialReport/findHeaders.do?reportName="+reportName;
  	 			$('#djyclable').css("display","none");
  	 		}
  			if(reportName == "ddzdfx_bytable"){
  	 			headUrl = "<%=path%>/chineseFinancialReport/toDdzdfxHeader.do";
  	 			$('#djyclable').css("display","block");
  	 			tool = ['search','excel','exit'];
  	 		}
  			
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp:true,
  	 			excelUrl:excelUrl+'?reportName='+reportName,
  	 			colsChooseUrl:'<%=path%>/chineseFinancialReport/toColumnsChoose.do?reportName='+reportName,
  	 			toolbar:tool
  	 		});

  	 		builtTable({
  	 			headUrl:headUrl,
  	 			dataUrl:dataUrl,
  	 			title:title,
  	 			grid:grid,
  	 			onDblClickRow:function(index,data){
  	 				if(reportName!="ddzdfx_bytable") return;
  	 				var serials = {};
  	 				$('#tabs').tabs("select",'<fmt:message key="bill" /><fmt:message key="the_detail" />');
  	 				serials['vbcode'] = data['VBCODE'];
  	 				serials['bdat'] = data['DWORKDATE'];
  	 				serials['pk_store'] = $("#pk_store").val();
  	 				$('#vbcode').val(data['VBCODE']);
  	 				tab = $('#tabs').tabs('getSelected');//获取选中的面板
  	 	  	 		var content = tab.panel("body");
  	 	  	 		var reportNameS = content.find("#reportName").val();
  	 				if(reportNameS=="ddzdfx_byinfo") {
  	 					$('#tabs').tabs('getSelected').panel("body").find("#datagrid").datagrid('reload',serials);
  	 				}
  	 				
  	 			},
  	 			createHeader:function(data,head,frozenHead){
  	 				if(reportName == "ddzdfx_bytable"){
	  	 				var pays = data.pays;
	  	 				var typs = data.typs;
	  	 				var col1 = [];
	  	 				var col2 = [];

	  	 				frozenHead.push([//{field:'VNAME',width:150,rowspan:2,title:'<fmt:message key="branches_name" />',align:'left'},
	  	 				                 {field:'VBCODE',width:60,rowspan:2,title:'<fmt:message key="bill_no" />',align:'left'},
	  	 				                 {field:'VTBLDES',width:60,rowspan:2,title:'<fmt:message key="the_same" />',align:'left'},
	  	 				                 {field:'DWORKDATE',width:80,rowspan:2,title:'<fmt:message key="date" />',align:'center'},
	  	 				                 {field:'IPEOLENUM',width:60,rowspan:2,title:'<fmt:message key="number_of_people" />',align:'right'}]);
	  	 				
  	 					col1.push({title:'<fmt:message key="The_amount_incurred" />',colspan:typs.length+1});
	  	 				for(var cur in typs){
	  	 					col2.push({field:"TYP" + typs[cur].VCNCODEONE,width:70,title:typs[cur].NAME,align:'right'});
	  	 				}
  	 					col2.push({field:"FAMT",width:80,title:'<fmt:message key="total" />',align:'right'});
  	 					
  	 					col1.push({title:'<fmt:message key="ZengShouE" />',colspan:4});
  	 					col2.push({field:"NSVCCHG",width:80,title:'<fmt:message key="service_charge" />',align:'right'});
  	 					col2.push({field:"NROOMAMT",width:80,title:'<fmt:message key="BaoJianFei" />',align:'right'});
  	 					col2.push({field:"NSVCITEM",width:80,title:'<fmt:message key="An_activity" />',align:'right'});
  	 					col2.push({field:"ZAMT",width:80,title:'<fmt:message key="total" />',align:'right'});
  	 					
  	 					col1.push({field:"AMT",width:60,rowspan:2,title:'<fmt:message key="business_income" />',align:'right'});
  	 					
  	 					col1.push({title:'<fmt:message key="Optimal_from_the_forehead" />',colspan:4});
  	 					col2.push({field:"ACTYMAMT",width:90,title:'<fmt:message key="Preferential_remission_activities" />',align:'right'});
  	 					col2.push({field:"NREFUNDAMT",width:110,title:'<fmt:message key="Payment_of_the_paid_in_amount" />',align:'right'});
  	 					col2.push({field:"NBZERO",width:80,title:'<fmt:message key="maLing" />',align:'right'});
  	 					col2.push({field:"YMAMT",width:90,title:'<fmt:message key="total" />',align:'right'});
  	 					
  	 					col1.push({title:'<fmt:message key="the_actual_turnover" />',colspan:pays.length+2});
	  	 				for(var cur in pays){
	  	 					col2.push({field:"PAY" + pays[cur]['VCODE'],width:70,title:pays[cur]['VNAME'],align:'right'});
	  	 				}
  	 					col2.push({field:"ACTJAMT",width:80,title:'<fmt:message key="Activity_paid_in" />',align:'right'});
  	 					col2.push({field:"JAMT",width:80,title:'<fmt:message key="total" />',align:'right'});
  	 					
	  	 				col1.push({field:"DISCPER",width:60,rowspan:2,title:'<fmt:message key="After_the_discount_per_capita" />',align:'right'});
	  	 				col1.push({field:"ICLASSES",width:60,rowspan:2,title:'<fmt:message key="serial_number" />',align:'left'});
	  	 				col1.push({field:"VSERIALNO",width:60,rowspan:2,title:'<fmt:message key="document_no" />',align:'left'});
	  	 				col1.push({field:"VORCLASS",width:60,rowspan:2,title:'<fmt:message key="bill" /><fmt:message key="status" />',align:'center'});
	  	 				col1.push({field:"VMEMO",width:60,rowspan:2,title:'<fmt:message key="bill" /><fmt:message key="remark" />',align:'left'});
	  	 				col1.push({field:"VSETUPBY",width:70,rowspan:2,title:'<fmt:message key="Merchandiser" />',align:'left'});
	  	 				col1.push({field:"DBTABLETIME",width:130,rowspan:2,title:'<fmt:message key="billing_time" />',align:'left'});
	  	 				col1.push({field:"VLASTEMP",width:70,rowspan:2,title:'<fmt:message key="The_cashier" />',align:'left'});
	  	 				col1.push({field:"DETABLETIME",width:130,rowspan:2,title:'<fmt:message key="checkout_time" />',align:'left'});
	  	 				col1.push({field:"NINVOICEMONEY",width:80,rowspan:2,title:'<fmt:message key="The_invoice_amount" />',align:'right'});
		  	 			
	  	 				head.push(col1);
	  	 				head.push(col2);
	  	 			}else{
	  	 				var frozenColumns = [];
	  	 				var columns = [];
	  	 				var frozenIndex = data.frozenColumns ? data.frozenColumns.split(',') : [];
	  	 				for(var i in data.columns){
	  	 					if(!data.columns[i].properties)continue;
	  	 					if($.inArray(data.columns[i].id,frozenIndex) >= 0){
	  	 						frozenColumns.push({field:data.columns[i].properties.toUpperCase(),title:data.columns[i].zhColumnName,width:data.columns[i].columnWidth,sortable:true});
	  	 					}else{
	  	 						if("CNT"==data.columns[i].properties.toUpperCase()||"NPRICE"==data.columns[i].properties.toUpperCase()||"NYCOUNT"==data.columns[i].properties.toUpperCase()||"NYMONEY"==data.columns[i].properties.toUpperCase()||"NTAX"==data.columns[i].properties.toUpperCase()||"ACTYMAMT"==data.columns[i].properties.toUpperCase()||"BZEROAMT"==data.columns[i].properties.toUpperCase()||"NYMPAY"==data.columns[i].properties.toUpperCase()||"JAMT"==data.columns[i].properties.toUpperCase()){
	  	 							columns.push({field:data.columns[i].properties.toUpperCase(),title:data.columns[i].zhColumnName,width:data.columns[i].columnWidth,sortable:true,align:'right'});
	  	 						}else{
	  	 							columns.push({field:data.columns[i].properties.toUpperCase(),title:data.columns[i].zhColumnName,width:data.columns[i].columnWidth,sortable:true});
	  	 						}
	  	 					}
	  	 				}
	  	 				head.push(columns);
	  	 				frozenHead.push(frozenColumns);

	  	 			}
 	 				}
  	 		});
  	 	}
  		//列选择后页面重新加载
  		function pageReload(){
  			closeColChooseWin();
  			initTab();
  		}
  	 </script>
  </body>
</html>