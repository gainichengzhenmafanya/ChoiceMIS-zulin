<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>单店收银统计</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
		.leftDiv{
			float: left;
			height: 100%;
			width: 120px;
			padding-left: 20px;
			line-height: 30px;
			padding-top: 30px;
		}
		.chartdiv{
			height: 100%;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		.datagrid{
			width: 30%;
		}
		.form-line .form-label{
			width: 13%;
		}
	</style>
	<%@ include file="../share/permission.jsp"%>
  </head>	
  <body>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label"><fmt:message key="startdate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" />
			</div>
			<div class="form-label"><fmt:message key="flight" /></div>
			<div class="form-input">
				<select id="sft" name="sft" style="width: 123px;" class="select">
					<option value="0"><fmt:message key="all_day" /></option>
					<c:forEach var="sft" items="${sftList}">
						<option value="${sft.vcode}" <c:if test="${sft.vcode==condition.sft}"> selected="selected" </c:if> >${sft.vname}</option>
					</c:forEach>					
				</select>
			</div>	
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="enddate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" />
			</div>
			<div id="statuslabel" class="form-label"></div>
			<div id="statusdiv" class="form-input">
				<input type="radio" name="charts" value="1" id="all" onclick="changUrl(1)"/><label for="all"><fmt:message key="The_bar_chart" /></label>
				<input type="radio" name="charts" value="2" id="C" onclick="changUrl(2)" /><label for="C"><fmt:message key="The_bar_chart" />2</label>
				<input type="radio" name="charts" value="3" id="A" checked="checked" onclick="changUrl(3)"/><label for="A"><fmt:message key="The_pie_chart" /></label>
			</div>
			<div class="form-label"><fmt:message key="The_work_number" /></div>
			<div class="form-input">
				<input type="text" name="vlastemp" class="text" style="width:80%;" />
			</div>
		</div>
		<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }"/>
	</form>
	<div class="easyui-tabs" fit="false" plain="true" id="tabs">
		<div title="<fmt:message key="The_checkout_method" />" style="position:absolute;">
			<input type="hidden" id="reportName" value="ddsyfstj"/>
			<input type="hidden" id="excelUrl" value="<%=path%>/chineseFinancialReport/exportReport.do"/>
			<input type="hidden" id="dataUrl" value="<%=path%>/chineseFinancialReport/queryDdsyfstj.do"/>
			<input type="hidden" id="chartUrl" value="<%=path%>/chineseFinancialReport/buildChart.do"/>
			<input type="hidden" id="chartUrl2" value="<%=path%>/chineseFinancialReport/buildChart2.do"/>
			<input type="hidden" id="swfurl" value="<%=path%>/Charts/Bar2D.swf"/>
			<input type="hidden" id="decimal" value="2" />
			<input type="hidden" id="title" value="<fmt:message key="The_checkout_method" />"/>
			<input type="hidden" id="chartDiv1" value="chart1" />
			<input type="hidden" id="chartDiv2" value="chart2" />
			<div id="datadiv" style="width:30%;float:left;"><div id="datagrid"></div></div>
			<div id="chartdivs" style="width:70%;height:100%;float:right;">
				<div  id="chart1" style="width:100%;height:50%;"></div>
				<div  id="chart2" style="width:100%;height:50%;"></div>
			</div>
		</div>
		<div  id="zdmx" title="<fmt:message key="checkout_type_detail" />">
			<input type="hidden" id="reportName" value="ddsyjsinfo"/>
			<input type="hidden" id="excelUrl" value="<%=path%>/chineseFinancialReport/exportReport.do"/>
			<input type="hidden" id="dataUrl" value="<%=path%>/chineseFinancialReport/queryDdsyjsinfo.do"/>
			<input type="hidden" id="decimal" value="2" />
			<input type="hidden" id="title" value="<fmt:message key="checkout_type_detail" />"/>
				<div id="datagrid"></div>
		</div>
	</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script language="JavaScript" src="<%=path%>/Charts/FusionCharts.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		//默认<fmt:message key="time" />
		 	$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
  	 		$("#bdat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
		    });
		    $("#edat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
		    });
  	 		$("#tabs").tabs({onSelect:function(){
	 				//初始化选中标签内容
					initTab(false);
  	 			}
  	 		});
  	 	});
  	 	
  		//初始化选中标签内容
  	 	function initTab(rebuild){
  	 		tab = $('#tabs').tabs('getSelected');//获取选中的面板
  	 		var content = tab.panel("body");
  	 		var excelUrl = content.find("#excelUrl").val();
  	 		var reportName = content.find("#reportName").val();
  	 		var dataUrl = content.find("#dataUrl").val();
  	 		var title = content.find("#title").val();
  	 		var grid = content.find("#datagrid");
  	 		var chartUrl = content.find("#chartUrl").val();
  	 		var chartDiv1 = content.find("#chartDiv1").val();
  	 		var chartUrl2 = content.find("#chartUrl2").val();
  	 		var chartDiv2 = content.find("#chartDiv2").val();
  	 		var widthDiv = content.find("#chartdivs").width();
  	 		
  	 		
  	 		if(reportName=='ddsyjsinfo'){
  	 			$('#statusdiv').css('display','none');
  	 			$('#statuslabel').css('display','none');
  	 		}else{
  	 			$('#statusdiv').css('display','block');
  	 			$('#statuslabel').css('display','block');
  	 		}
  	 		
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp:true,
  	 			excelUrl:excelUrl+'?reportName='+reportName,
  	 			colsChooseUrl:'<%=path%>/chineseFinancialReport/toColumnsChoose.do?reportName='+reportName,
  	 			toolbar:['search','excel','option','exit'],
  	 			searchFun:function(){
  	 	  	 		var value = $(':radio[name="charts"]').filter(":checked").val();
  	 	  	 		if(value==1)
  	 	  				$('#swfurl').val("<%=path%>/Charts/Bar2D.swf");
  	 	  			else if(value==2)
  	 	  				$('#swfurl').val("<%=path%>/Charts/Column3D.swf");
  	 	  			else if(value==3)
  	 	  				$('#swfurl').val("<%=path%>/Charts/Pie3D.swf");
  	 	  			else
  	 	  				$('#swfurl').val("<%=path%>/Charts/Bar2D.swf");
	  	 	  	 	var swfurl = content.find("#swfurl").val();
	  	 	  	 	var heightDiv = content.find("#datadiv").height();
		 			
	  	 	  	 	if(reportName=='ddsyfstj'){
	  	 				buildChart({
	  	 	  	 			swf:swfurl,
	  	 	  	 			width:widthDiv,
	  	 	  	 			height:heightDiv/2,
	  	 	  	 			url:chartUrl,
	  	 	  	 			div:chartDiv1,
	  	 	  	 			form:$("#queryForm")
	  	 	  	 		});
	  	 				buildChart({
	  	 	  	 			swf:swfurl,
	  	 	  	 			width:widthDiv,
	  	 	  	 			height:heightDiv/2,
	  	 	  	 			url:chartUrl2,
	  	 	  	 			div:chartDiv2,
	  	 	  	 			form:$("#queryForm")
	  	 	  	 		});
	  	 	  	 	}
	  	 	  		content.find('#datagrid').datagrid('load',getParam($("#queryForm")));
  	 			}
  	 		});
  	 		if(rebuild || grid.css('display') != 'none'){
  	  				firstLoad = true;//重置第一次加载标志
		  	 		builtTable({
		  	 			headUrl:"<%=path%>/chineseFinancialReport/findHeaders.do?reportName="+reportName,
		  	 			dataUrl:dataUrl,
		  	 			title:title,
		  	 			grid:grid,
		  	 			width:'30%',
		  	 			alignCols:['cnt'],
		  	 			numCols:['amt'],
		  	 			decimalDigitF:content.find('#decimal').val()
		  	 		});
  	  			}
  	 	}
  		//列选择后页面重新加载
  		function pageReload(){
  			closeColChooseWin();
  			initTab(true);
  		}
  		//改变swf
  		function changUrl(id){
  			if(id==1)
  				$('#swfurl').val("<%=path%>/Charts/Bar2D.swf");
  			else if(id==2)
  				$('#swfurl').val("<%=path%>/Charts/Column3D.swf");
  			else if(id==3)
  				$('#swfurl').val("<%=path%>/Charts/Pie3D.swf");
  			else
  				$('#swfurl').val("<%=path%>/Charts/Bar2D.swf");

  	 		tab = $('#tabs').tabs('getSelected');//获取选中的面板
  	 		var content = tab.panel("body");
  	 		var chartUrl = content.find("#chartUrl").val();
  	 		var chartDiv1 = content.find("#chartDiv1").val();
  	 		var swfurl = content.find("#swfurl").val();
  	 		var chartUrl2 = content.find("#chartUrl2").val();
  	 		var chartDiv2 = content.find("#chartDiv2").val();
  	 		var widthDiv = content.find("#chartdivs").width();
  	 		var heightDiv = content.find("#datadiv").height();
  	 		if($('#firmdes').val() && $('#pk_store').val()){
  	 			content.find('#datagrid').datagrid('load',getParam($("#queryForm")));
  	 			buildChart({
	  	 			swf:swfurl,
 	  	 			width:widthDiv,
 	  	 			height:heightDiv/2,
	  	 			url:chartUrl,
	  	 			div:chartDiv1,
	  	 			form:$("#queryForm")
	  	 		});
				buildChart({
	  	 			swf:swfurl,
 	  	 			width:widthDiv,
 	  	 			height:heightDiv/2,
	  	 			url:chartUrl2,
	  	 			div:chartDiv2,
	  	 			form:$("#queryForm")
	  	 		});
  	 		}
  		}
  	 </script>
  </body>
</html>