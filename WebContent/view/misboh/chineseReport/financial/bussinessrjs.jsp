<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>收入日结算报表</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<%@ include file="../share/permission.jsp"%>
  </head>	
  <body>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label"><fmt:message key="startdate" /></div>
			<div class="form-input"><input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" /></div>
			<div class="form-label" style="width:60px;"><fmt:message key="enddate" /></div>
			<div class="form-input"><input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" /></div>
			<div class="form-label" style="width:40px;"><fmt:message key="flight" /></div>
			<div class="form-input">
				<select id="sft" name="sft" class="select" style="width:134px;margin-top:3px;">
					<option value="0"><fmt:message key="all_day" /></option>
					<c:forEach var="sft" items="${sftList}">
						<option value="${sft.vcode}" <c:if test="${sft.vcode==condition.sft}"> selected="selected" </c:if> >${sft.vname}</option>
					</c:forEach>				
				</select>
			</div>
			<div class="form-label" style="width:150px;">
				<input type="radio" id="mon1" name="mon" value="1"/><fmt:message key="This_month" />
				<input type="radio" id="mon2" name="mon" value="2"/><fmt:message key="Last_month" />
				<input type="radio" id="mon3" name="mon" value="3"/><fmt:message key="Next_month" />
			</div>
		</div>
		<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }"/>
	</form>
	<div id="datagrid"></div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		$(":radio").click(function(){
  	 			var dat = new Date();
  	 			$("#bdat").val(getDatebyB($(this).val(),dat.getFullYear(),dat.getMonth()+1));
  	 			$("#edat").val(getDatebyF($(this).val(),dat.getFullYear(),dat.getMonth()+1));
  	 		});
	 		//默认<fmt:message key="time" />
	 		$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
  	 		$("#bdat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
		    });
		    $("#edat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
		    });
// 	  		$("#bdat").val($("#edat").val().substr(0,8)+"01");
	  		
  	 		initTable();
  	 	});
  	 	
  	 	function changebdat(){
  	 		if($("#bdat").val().substr(5,2)!=$("#edat").val().substr(5,2)){
  	 			if($("#bdat").val().substr(5,1)=='0'){
  	 				$("#edat").val(getDatebyF(1,$("#bdat").val().substr(0,4),parseInt($("#bdat").val().substr(5,2),16)));
  	 			}else{
  	 				$("#edat").val(getDatebyF(1,$("#bdat").val().substr(0,4),$("#bdat").val().substr(5,2)));
  	 			}
  	 		}
  	 	}
  	 	
  	 	function changeedat(){
  	 		if($("#bdat").val().substr(5,2)!=$("#edat").val().substr(5,2)){
  				$("#bdat").val($("#edat").val().substr(0,8)+"01");
  	 		}
  	 	}
  	 	
  	 	function initTable(){
  	 		queryParams = getParam($("#queryForm"));
  	 		
  	  	 	//生成工具栏
  	  	 		builtToolBar({
  	  	 			basePath:"<%=path%>",
  	  	 			toolbarId:'tool',
  	  	 			formId:'queryForm',
  	  	 			grid:$("#datagrid"),
  	  	 			exportTyp:true,
  	  	 		    excelUrl:'<%=path%>/chineseFinancialReport/exportReportRJS.do',
  	  	 		    toolbar:['search','excel','exit'],
  	  	 			searchFun:initTable
  	  	 		});
  	  	 	
  	  	 		//生成表格
  	  	  		builtTable({
  	  	 			dataUrl:"<%=path%>/chineseFinancialReport/getDataRJS.do",
  	  	 			title:'<fmt:message key="Income_settlement_report"/>',
  	  	 			grid:$("#datagrid"),
  	  	 			pagination:false,
	  	 			showFooter:false,
  	  	 			createHeader:function(data,head,frozenHead){
  	  	 				var col1 = [];
  	  	 				var col2 = [];

  	  	 				frozenHead.push([{field:'CODE',width:55,title:'<fmt:message key="subjects"/><fmt:message key="Code"/>',align:'left',rowspan:2,styler:function(value){
	  	 						if(value.substr(0,1) == 'B'||value.substr(0,1) == 'C'){flag=true;return "background:#FBEC88";}else{flag=false;}}},
	  	 						{field:'DES',width:100,title:'<fmt:message key="subjects"/><fmt:message key="name"/>',align:'left',rowspan:2,styler:function(){if(flag){return "background:#FBEC88";}}},
  	  	 						{field:"Q0",width:80,title:'<fmt:message key="business_income"/><fmt:message key="total"/>',align:'right',rowspan:2,styler:function(){if(flag){return "background:#FBEC88";}}},
  	  	 						{field:"H0",width:80,title:'<fmt:message key="Net_income"/><fmt:message key="total"/>',align:'right',rowspan:2,styler:function(){if(flag){return "background:#FBEC88";}}}]);
  	  	 			
  	  	 				var a=$("#bdat").val().substr(8,10);
  	  	 				if(a.substr(0,1) ==0){
  	  	 					a=a.substr(1,2);
  	  	 				}
  	  	 				var b=$("#edat").val().substr(8,10);
  	  	 				if(b.substr(0,1) ==0){
	  	 					b=b.substr(1,2);
	  	 				}
  	  	 				a=parseInt(a);
	  	 				b=parseInt(b);
	  	 				for(var i=a;i<=b;i++){
	  	 					col1.push({title:i+"号("+getXq($("#bdat").val().substr(0,4),$("#bdat").val().substr(5,2),i)+")",colspan:2});
	  	  	 				col2.push({field:"Q"+i,width:75,title:'<fmt:message key="business_income"/>',align:'right'});
	  	  	 				col2.push({field:"H"+i,width:75,title:'<fmt:message key="Net_income"/>',align:'right'});
	  	 				}
	  	 				
  	  	 				head.push(col1);
  	  	 				head.push(col2);
  	  	 			}
  	  	 		});
  	  	 	}
  		//根据日期获得星期几
  		function getXq(year,month,day){
  		    var ssdate=new Date(year,month-1,day);  
  		    switch(ssdate.getDay()){
  		    case 1: return '<fmt:message key="Monday"/>'; break;
  		    case 2: return '<fmt:message key="Tuesday"/>'; break;
  		    case 3: return '<fmt:message key="Wednesday"/>'; break;
  		    case 4: return '<fmt:message key="Thursday"/>'; break;
  		    case 5: return '<fmt:message key="Friday"/>'; break;
  		    case 6: return '<fmt:message key="Saturday"/>'; break;
  		    case 7: return '<fmt:message key="Sunday"/>'; break;
  		    default : return '<fmt:message key="Sunday"/>'; break;
  		    }
  		}
  	 	function getDatebyB(mon,year,month){
  	 		if(mon==1){
  	 			month = month<10?"0"+month:month;
  	 			return year+"-"+month+"-01";
  	 		}
			if(mon==2){
				if(month-1==0){
					return (year-1)+"-12-01";
				}else{
					month = (month-1)<10?"0"+(month-1):(month-1);
					return year+"-"+month+"-01";
				}			
			}
			if(mon==3){
				if(month+1>12){
					return (year+1)+"-01-01";
				}else{
					month = (month+1)<10?"0"+(month+1):(month+1);
					return year+"-"+month+"-01";
				}	
			}
		}
  	 	function getDatebyF(mon,year,month){
			var days = 0;
			var month1={1:31,3:31,5:31,7:31,8:31,10:31,12:31,4:30,6:30,9:30,11:30};
			
			if(mon==1){
				if(month1[month]){
					days = month1[month];
				}else if(month==2){
					days =year%400 == 0 || (year%4==0 && year%100!=0)?29:28;
				}
				month = month<10?"0"+month:month;
				return year+"-"+month+"-"+days;
			}
			if(mon==2){
				if(month-1==0){
					return (year-1)+"-12-31";
				}else{
					if(month1[month-1]){
						days = month1[month-1];
					}else if(month-1==2){
						days =year%400 == 0 || (year%4==0 && year%100!=0)?29:28;
					}
					month = (month-1)<10?"0"+(month-1):(month-1);
					return year+"-"+month+"-"+days;
				}	
			}
			if(mon==3){
				if(month+1>12){
					return (year+1)+"-01-31";
				}else{
					if(month1[month+1]){
						days = month1[month+1];
					}else if(month+1==2){
						days =year%400 == 0 || (year%4==0 && year%100!=0)?29:28;
					}
					month = (month+1)<10?"0"+(month+1):(month+1);
					return year+"-"+month+"-"+days;
				}
			}
		}
  	 </script>
  </body>
</html>
