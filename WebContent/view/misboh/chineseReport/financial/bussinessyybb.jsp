<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>营业日报表</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
		.leftDiv{
			float: left;
			height: 100%;
			width: 120px;
			padding-left: 20px;
			line-height: 30px;
			padding-top: 30px;
		}
		.chartdiv{
			height: 100%;
		}
	</style>
	<%@ include file="../share/permission.jsp"%>
  </head>	
  <body>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label" style="width:60px;"><fmt:message key="date" /></div>
			<div class="form-input"><input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" /></div>
		</div>
		<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }"/>
	</form>
	<div id="datagrid"></div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	<script type="text/javascript">
  	 	$(document).ready(function(){
			
			//默认时间
  	 		$("#edat").htmlUtils("setDate","yes",'${newdate}');
  	 		$("#edat").click(function(){
  	 			new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
	  		});
  	 		initTable();
  	 	});
  		
  	 	function initTable(){
  	 		queryParams = getParam($("#queryForm"));
  	 		
  	  	 	//生成工具栏
  	  	 		builtToolBar({
  	  	 			basePath:"<%=path%>",
  	  	 			toolbarId:'tool',
  	  	 			formId:'queryForm',
  	  	 			grid:$("#datagrid"),
  	  	 			exportTyp:true,
  	  	 		    excelUrl:'<%=path%>/chineseFinancialReport/exportReportYYBB.do',
  	  	 		    toolbar:['search','excel','exit']
  	  	 		});
  	  	 	
  	  	 		//生成表格
  	  	  		builtTable({
  	  	  			headUrl:"<%=path%>/chineseFinancialReport/findHeaderForYYBB.do",
  	  	 			dataUrl:"<%=path%>/chineseFinancialReport/getDataYYBB.do",
  	  	 			title:'<fmt:message key="business_day_report" />',
  	  	 			grid:$("#datagrid"),
  	  	 			createHeader:function(data,head,frozenHead){
  	  	 				var sft = data.sfts;
  	  	 				var col1 = [];
  	  	 				var col2 = [];
  	  	 				var col3 = [];
  	  	 				var col4 = [];
  	  	 				
  	  	 				frozenHead.push([{field:'CODE',width:50,title:'<fmt:message key="subjects"/><fmt:message key="Code"/>',align:'left',rowspan:4,styler:function(value){
  	 						if(value.substr(0,1) == 'B'||value.substr(0,1) == 'C'){flag=true;return "background:#FBEC88";}else{flag=false;}}},
  	 						{field:'DES',width:100,title:'<fmt:message key="subjects"/><fmt:message key="name"/>',align:'left',rowspan:4,styler:function(){if(flag){return "background:#FBEC88";}}}]);
  	  	 				col1.push({title:'<fmt:message key="This_day" />',colspan:sft.length*2+2});
  	  	 				col1.push({title:'<fmt:message key="Early_today" />',colspan:sft.length*2+2});
  	  	 				
  	  	 				col2.push({title:'<fmt:message key="all_day" /><fmt:message key="total" />',colspan:2,rowspan:2});
  	  	 				col2.push({title:'<fmt:message key="flight" />',colspan:sft.length*2});
  	  	 				col2.push({title:'<fmt:message key="all_day" /><fmt:message key="total" />',colspan:2,rowspan:2});
	  	 				col2.push({title:'<fmt:message key="flight" />',colspan:sft.length*2});
	  	 				
	  	 				col4.push({field:"DM0",width:70,title:'<fmt:message key="amount" />',align:'right'});
  	  	 				col4.push({field:"DB0",width:55,title:'<fmt:message key="The_proportion" />(%)',align:'right'});
	  	  	 			for(var cur in sft){
	  	 					col3.push({title:sft[cur].vname,colspan:2});
	  	  	 				col4.push({field:"DM"+sft[cur].vcode,width:70,title:'<fmt:message key="amount" />',align:'right'});
	  	  	 				col4.push({field:"DB"+sft[cur].vcode,width:55,title:'<fmt:message key="The_proportion" />(%)',align:'right'});
	  	 				}
	  	  	 				
	  	 				col4.push({field:"MM0",width:70,title:'<fmt:message key="amount" />',align:'right'});
	  	 				col4.push({field:"MB0",width:55,title:'<fmt:message key="The_proportion" />(%)',align:'right'});
	  	 				for(var cur in sft){
	  	 					col3.push({title:sft[cur].vname,colspan:2});
	  	  	 				col4.push({field:"MM"+sft[cur].vcode,width:70,title:'<fmt:message key="amount" />',align:'right'});
	  	  	 				col4.push({field:"MB"+sft[cur].vcode,width:55,title:'<fmt:message key="The_proportion" />(%)',align:'right'});
	  	 				}
	  	 				
  	  	 				head.push(col1);
  	  	 				head.push(col2);
  	  	 				head.push(col3);
  	  	 				head.push(col4);
  	  	 			}
  	  	 		});
  	  	 	}
  	 </script>
  </body>
</html>
