<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>菜品销售排行榜</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
		.search{
			margin-top:3px;
			cursor: pointer;
		}
	</style>
	<%@ include file="../share/permission.jsp"%>
  </head>	
  <body>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label"><fmt:message key="date" /></div>
			<div class="form-input"><input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd"/>" /></div>
			<div class="form-label"><fmt:message key="Ranking_number" /></div>
			<div class="form-input">
				<input type="text" id="interval" name="interval" class="text" onkeyup="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}" onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}" style="margin-top: 2px;" value="15"/>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="bigClass" /></div>
			<div class="form-input">
				<select name="pubgrptyp" id="pubgrptyp" style="width: 133px;" class="select" key="VCNCODEONE" data="NAME" select="true"></select>
			</div>
			<div class="form-label"><fmt:message key="smallClass" /></div>
			<div class="form-input">
				<select name="pubgrp" id="pubgrp" style="width: 133px;" class="select" key="VCNCODETWO" data="NAME" select="true"></select>
			</div>
			<div class="form-label"><fmt:message key="sector" /></div>
			<div class="form-input">
				<select name="idept" id="idept" style="width: 133px;" class="select" key="vcode" data="vname" select="true"></select>
			</div>
		</div>
		<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }"/>
	</form>
	<input type="hidden" id="reportName" value="foodList"/>
	<input type="hidden" id="excelUrl" value="<%=path%>/chineseSalesReport/exportReport.do"/>
	<input type="hidden" id="dataUrl" value="<%=path%>/chineseSalesReport/queryFoodList.do"/>
	<input type="hidden" id="title" value="<fmt:message key="Food_sales_charts" />"/>
	<div id="datagrid"></div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	<script type="text/javascript">
  	 	$(document).ready(function(){
  	 		
			//默认<fmt:message key="time" />
		 	$("#edat").htmlUtils("setDate","yes",'${newdate}');
  	  		$("#edat").click(function(){
  	  			new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
  	  		});
			
  	 		//初始化选中标签内容
  	 		initTab();
  	 		
  	 		$('#pubgrptyp').htmlUtils("select");
	 		//页面加载时默认查询小类信息
	 		$('#pubgrptyp').attr("url","<%=path%>/chineseBusinessReport/findAllMarsaleclassOne.do");
	 		$('#pubgrptyp').siblings().remove();//把已经存在的select去掉
	 		$('#pubgrptyp').htmlUtils("select");
	 		
	 		$('#pubgrp').htmlUtils("select");
	 		//页面加载时默认查询小类信息
	 		$('#pubgrp').attr("url","<%=path%>/chineseBusinessReport/findAllMarsaleclassTwo.do");
	 		$('#pubgrp').siblings().remove();//把已经存在的select去掉
	 		$('#pubgrp').htmlUtils("select");
	 		
			$('#idept').htmlUtils("select");
	 		//页面加载时默认查询部门信息
			$('#idept').attr("url","<%=path%>/misbohcommon/findAllGroupDept.do");
  	 		$('#idept').siblings().remove();//把已经存在的select去掉
  	 		$('#idept').htmlUtils("select");
  	 	});
  	 	
  		//初始化选中标签内容
  	 	function initTab(){
  	 		var excelUrl = $("#excelUrl").val();
  	 		var reportName = $("#reportName").val();
  	 		var dataUrl = $("#dataUrl").val();
  	 		var title = $("#title").val();
  	 		var grid = $("#datagrid");
  	 		
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp:true,
  	 			excelUrl:excelUrl+'?reportName='+reportName,
  	 			colsChooseUrl:'<%=path%>/chineseSalesReport/toColumnsChoose.do?reportName='+reportName,
  	 			toolbar:['search','excel','option','exit'],
  	 			searchFun:function(){
  	 				grid.datagrid('load',getParam($("#queryForm")));
  	 			}
  	 		});
  	 		builtTable({
  	 			headUrl:"<%=path%>/chineseSalesReport/findHeaders.do?reportName="+reportName,
  	 			dataUrl:dataUrl,
  	 			title:title,
  	 			grid:grid,
  	 			numCols:['scnt','samt','snprice','cnt','amt','nprice',]
  	 		});
  	 	}
  		//列选择后页面重新加载
  		function pageReload(){
  			closeColChooseWin();
  			initTab(true);
  		}
  	 </script>
  </body>
</html>