<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>时段销售分析</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
		
	</style>
	<%@ include file="../share/permission.jsp"%>
  </head>	
  <body>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label"><fmt:message key="startdate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" />
			</div>
			<div class="form-label"><fmt:message key="period_of_time" /></div>
			<div class="form-input">
				<input type="text" id="interval" name="interval" style="margin-top: 3px;" class="text" onkeyup="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}" onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}" style="margin-top: 0px;" value="30"/>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="enddate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" />
			</div>
			<div class="form-label"><fmt:message key="category" /></div>
			<div class="form-input">
				<select name="pubgrp" id="pubgrp" style="width: 133px;" class="select" key="VCNCODETWO" data="NAME" select="true"></select>
			</div>
			<div class="form-label"><fmt:message key="statistical" /><fmt:message key="category" /></div>
			<div class="form-input">
				<input type="radio" name="detailMod" id="all" value="1" checked="checked" onclick="changUrl(1)"/><label for="all"><fmt:message key="According_to_the" /><fmt:message key="food_and_beverage" /><fmt:message key="smallClass" /></label>
				<input type="radio" name="detailMod" id="C" value="2" onclick="changUrl(2)" /><label for="C"><fmt:message key="According_to_the" /><fmt:message key="food_and_beverage" /><fmt:message key="bigClass" /></label>
<%-- 				<input type="radio" name="detailMod" id="A" value="3" onclick="changUrl(3)"/><label for="A" ><fmt:message key="According_to_the" /><fmt:message key="food_and_beverage" /><fmt:message key="sector" /></label> --%>
			</div>
		</div>
		<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }"/>
	</form>
	<div class="easyui-tabs" fit="false" plain="true" id="tabs">
		<div title="<fmt:message key="Analysis_of_single_time_customer_sales" />">
			<input type="hidden" id="reportName" value="saletcbytime"/>
			<input type="hidden" id="excelUrl" value="<%=path%>/chineseSalesReport/exportReport.do"/>
			<input type="hidden" id="dataUrl" value="<%=path%>/chineseSalesReport/querySaleTcByTime.do"/>
			<input type="hidden" id="decimal" value="2" />
			<input type="hidden" id="title" value="<fmt:message key="Analysis_of_single_time_customer_sales" />"/>
			<div><div id="datagrid" ></div></div>
		</div>
		<div title="<fmt:message key="Analysis_of_categories_time_sales" />">
			<input type="hidden" id="reportName" value="saletypebytime"/>
			<input type="hidden" id="excelUrl" value="<%=path%>/chineseSalesReport/exportReport.do"/>
			<input type="hidden" id="dataUrl" value="<%=path%>/chineseSalesReport/querySaleTypeByTime.do"/>
			<input type="hidden" id="decimal" value="2" />
			<input type="hidden" id="title" value="<fmt:message key="Analysis_of_categories_time_sales" />"/>
				<div id="datagrid"></div>
		</div>
	</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
		 	//默认<fmt:message key="time" />
		 	$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
  	 		$("#bdat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
		    });
		    $("#edat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
		    });
  	  		
  	 		$("#tabs").tabs({onSelect:function(){
  	 				//初始化选中标签内容
  	 				initTab(false);
  	 			}
  	 		});

  	 		$('#pubgrp').htmlUtils("select");
  	 		//页面加载时默认查询小类信息
  	 		$('#pubgrp').attr("url","<%=path%>/chineseBusinessReport/findAllMarsaleclassTwo.do");
  	 		$('#pubgrp').siblings().remove();//把已经存在的select去掉
  	 		$('#pubgrp').htmlUtils("select");
  	 	});
  	 	
  		//初始化选中标签内容
  	 	function initTab(rebuild){
  	 		tab = $('#tabs').tabs('getSelected');//获取选中的面板
  	 		var content = tab.panel("body");
  	 		var excelUrl = content.find("#excelUrl").val();
  	 		var reportName = content.find("#reportName").val();
  	 		var dataUrl = content.find("#dataUrl").val();
  	 		var title = content.find("#title").val();
  	 		var grid = content.find("#datagrid");
  	 		
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp:true,
  	 			excelUrl:excelUrl+'?reportName='+reportName,
  	 			colsChooseUrl:'<%=path%>/chineseSalesReport/toColumnsChoose.do?reportName='+reportName,
  	 			toolbar:['search','excel','exit'],
  	 			searchFun:function(){
  	 				content.find('#datagrid').datagrid('load',getParam($("#queryForm")));
  	 			}
  	 		});
  	 		if(rebuild || grid.css('display') != 'none'){
  	  				firstLoad = true;//重置第一次加载标志
		  	 		builtTable({
		  	 			headUrl:"<%=path%>/chineseSalesReport/findHeaders.do?reportName="+reportName,
		  	 			dataUrl:dataUrl,
		  	 			title:title,
		  	 			grid:grid,
		  	 			decimalDigitF:content.find('#decimal').val(),
		  	 			alignCols:['tc','cnt','cntl','cntzb','cntlzb','amt','amtl','amtzb','amtlzb']
		  	 		});
  	  			}
  	 	}
  		//列选择后页面重新加载
  		function pageReload(){
  			closeColChooseWin();
  			initTab(true);
  		}
  		
  	//改变类别
  		function changUrl(id){
  			if(id==1){
  				$('#pubgrp').attr("key","VCNCODETWO");
  				$('#pubgrp').attr("data","NAME");
  				$('#pubgrp').attr("url","<%=path%>/chineseBusinessReport/findAllMarsaleclassTwo.do");
  	  	 		$('#pubgrp').siblings().remove();//把已经存在的select去掉
  	  	 		$('#pubgrp').htmlUtils("select");
  			}
  			else if(id==2){
  				$('#pubgrp').attr("key","VCNCODEONE");
  				$('#pubgrp').attr("data","NAME");
  				$('#pubgrp').attr("url","<%=path%>/chineseBusinessReport/findAllMarsaleclassOne.do");
  	  	 		$('#pubgrp').siblings().remove();//把已经存在的select去掉
  	  	 		$('#pubgrp').htmlUtils("select");
  			}
  		}
  	 </script>
  </body>
</html>