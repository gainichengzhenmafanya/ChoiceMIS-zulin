<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>排班报表</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
		
	</style>
	<%@ include file="../share/permission.jsp"%>
  </head>	
  <body>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label"><fmt:message key="startdate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" />
			</div>
			<div class="form-label"><fmt:message key="enddate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" />
			</div>
			<div class="form-label"><fmt:message key="period_of_time" /></div>
			<div class="form-input">
				<input type="text" id="interval" name="interval" style="margin-top: 3px;" class="text" onkeyup="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}" onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}" style="margin-top: 0px;" value="30"/>
			</div>
		</div>
		<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }"/>
	</form>
	<input type="hidden" id="reportName" value="scheduleReport"/>
	<input type="hidden" id="excelUrl" value="<%=path%>/chineseSalesReport/exportReport.do"/>
	<input type="hidden" id="dataUrl" value="<%=path%>/chineseSalesReport/createScheduleReport.do"/>
	<input type="hidden" id="title" value="<fmt:message key="misboh_Schedule_report" />"/>
	<div id="datagrid" ></div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
		 	//默认<fmt:message key="time" />
		 	$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
  	 		$("#bdat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
		    });
		    $("#edat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
		    });
  	  		
  	  		//初始化选中标签内容
			initTab(false);
  	 	});
  	 	
  		//初始化选中标签内容
  	 	function initTab(rebuild){
  	 		var excelUrl = $("#excelUrl").val();
  	 		var reportName = $("#reportName").val();
  	 		var dataUrl = $("#dataUrl").val();
  	 		var title = $("#title").val();
  	 		var grid = $("#datagrid");

  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp:true,
  	 			excelUrl:excelUrl+'?reportName='+reportName,
  	 			colsChooseUrl:'<%=path%>/misSaleAnalysis/toColumnsChoose.do?reportName='+reportName,
  	 			toolbar:['search','excel','exit'],
  	 			searchFun:function(){
  	 				initTab(true);
  	 				$('#datagrid').datagrid('load',getParam($("#queryForm")));
  	 			}
  	 		});
  	 		if(rebuild || grid.css('display') != 'none'){
  	  				firstLoad = true;//重置第一<fmt:message key="secondary" />加载标志
		  	 		builtTable({
		  	 			dataUrl:dataUrl,
		  	 			title:title,
		  	 			grid:grid,
			  	 		createHeader:function(data,head,frozenHead){
			  	 			var col1 = [];
// 		  	 				col1.push({field:"CNO",width:80,title:'<fmt:message key="line" /><fmt:message key="code" />',rowspan:1,colspan:1,align:'left'});
		  	 				col1.push({field:"DAT",width:100,title:'<fmt:message key="bucket" />',rowspan:1,colspan:1,align:'left'});
		  	 				col1.push({field:"NCOUNT",width:80,title:'<fmt:message key="open_the_numbers" />',rowspan:1,colspan:1,align:'right'});
		  	 				col1.push({field:"NMONEY",width:120,title:'<fmt:message key="sell_amt" />',rowspan:1,colspan:1,align:'right'});
		  	 				col1.push({field:"JZCOUNT",width:80,title:'<fmt:message key="misboh_Check_the_number" />',rowspan:1,colspan:1,align:'right'});
		  	 				col1.push({field:"JZMONEY",width:120,title:'结账金额',rowspan:1,colspan:1,align:'right'});
		  	 				col1.push({field:"ZTCOUNT",width:80,title:'<fmt:message key="misboh_In_units" />',rowspan:1,colspan:1,align:'right'});
		  	 				head.push(col1);
		  	 			}
		  	 		});
  	  			}
  	 	}
  		//列选择后页面重新加载
  		function pageReload(){
  			closeColChooseWin();
  			initTab(true);
  		}
  	 </script>
  </body>
</html>