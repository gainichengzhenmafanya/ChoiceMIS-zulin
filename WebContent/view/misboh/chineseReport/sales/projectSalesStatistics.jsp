<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>项目销售统计</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		.radio{
			line-height: 20px;
			width:80px;
			display: inline-block;
		}
		.form-line .form-label{
			width: 12%;
		}
		.diy_select{
			margin-top: 2px;
		}
	</style>
	<%@ include file="../share/permission.jsp"%>
  </head>
  <body>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
  		<input type="hidden" id="vpcode" name="vpcode"/>
  		<div style="width: 803px;float: left;">
			<div class="form-line">
				<div class="form-label"><fmt:message key="startdate" /></div>
				<div class="form-input">
					<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" />
				</div>
				<div class="form-label"><fmt:message key="bigClass" /></div>
				<div class="form-input">
					<select name="pubgrptyp" id="pubgrptyp" style="width: 120px;" class="select" key="VCNCODEONE" data="NAME" select="true"></select>
				</div>		
				<div class="form-label"><fmt:message key="smallClass" /></div>
				<div class="form-input">
					<select name="pubgrp" id="pubgrp" style="width: 120px;" class="select" key="VCNCODETWO" data="NAME" select="true"></select>
				</div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="enddate" /></div>
				<div class="form-input">
					<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" />
				</div>
				<div class="form-label"><fmt:message key="flight" /></div>
				<div class="form-input">
					<select id="sft" name="sft" class="select" style="width: 120px;" >
						<option value="0"><fmt:message key="all_day" /></option>
						<c:forEach var="sft" items="${sftList}">
							<option value="${sft.vcode}" <c:if test="${sft.vcode==condition.sft}"> selected="selected" </c:if> >${sft.vname}</option>
						</c:forEach>				
					</select>
				</div>
				<div class="form-label" style="margin-top:3px;"><fmt:message key="select" /><fmt:message key="way" /></div>
				<div class="form-input" style="margin-top:3px;">
					<input type="radio" id="all" value="0" checked="checked" name="queryMod" style="margin-top:2px;"/><label for="all"><fmt:message key="all" /></label>
					<input type="radio" id="normal" value="2" name="queryMod" style="margin-top:2px;"/><label for="normal"><fmt:message key="Zero_food" /></label>
					<input type="radio" id="pkg" value="1" name="queryMod" style="margin-top:3px;"/><label for="pkg"><fmt:message key="Package_food" /></label>
				</div>
			</div>
		</div>
		<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }"/>
	</form>
	<div class="easyui-tabs" fit="false" plain="true" id="tabs" style="clear: both;">
		<div  id="xstjb" title="<fmt:message key="Statistics_of_sales" />">
			<input type="hidden" id="reportName" value="projectSalesStatistics"/>
			<input type="hidden" id="excelUrl" value="<%=path%>/chineseSalesReport/exportReport.do?sumMod=<%=PublicEntity.BYFIRM%>"/>
			<input type="hidden" id="headUrl" value="<%=path%>/chineseSalesReport/findHeaders.do"/>
			<input type="hidden" id="dataUrl" value="<%=path%>/chineseSalesReport/findProjectSalesStatistics.do?sumMod=<%=PublicEntity.BYFIRM%>"/>
			<input type="hidden" id="decimal" value="2" />
			<input type="hidden" id="title" value="<fmt:message key="Statistics_of_sales" />"/>
				<div id="datagrid"></div>
		</div>
		<div  id="mxtjb" title="<fmt:message key="Statistical_tables" />">
			<input type="hidden" id="reportName" value="projectSalesStatisticsDetail"/>
			<input type="hidden" id="excelUrl" value="<%=path%>/chineseSalesReport/exportReport.do?sumMod=<%=PublicEntity.BYFOOD%>"/>
			<input type="hidden" id="headUrl" value="<%=path%>/chineseSalesReport/findHeaders.do"/>
			<input type="hidden" id="dataUrl" value="<%=path%>/chineseSalesReport/findProjectSalesStatistics.do?sumMod=<%=PublicEntity.BYFOOD%>"/>
			<input type="hidden" id="decimal" value="2" />
			<input type="hidden" id="title" value="<fmt:message key="Statistical_tables" />"/>
				<div id="datagrid"></div>
		</div>
	</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		
		 	//默认<fmt:message key="time" />
		 	$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
  	 		$("#bdat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
		    });
		    $("#edat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
		    });
  	  		
	  	  	$("#tabs").tabs({onSelect:function(){
					//初始化选中标签内容
					initTab(false);
				}
			});
	  	  	
	  	    $('#pubgrptyp').htmlUtils("select");
	 		//页面加载时默认查询小类信息
	 		$('#pubgrptyp').attr("url","<%=path%>/chineseBusinessReport/findAllMarsaleclassOne.do");
	 		$('#pubgrptyp').siblings().remove();//把已经存在的select去掉
	 		$('#pubgrptyp').htmlUtils("select");
	 		
	 		$('#pubgrp').htmlUtils("select");
  	 		//页面加载时默认查询小类信息
  	 		$('#pubgrp').attr("url","<%=path%>/chineseBusinessReport/findAllMarsaleclassTwo.do");
  	 		$('#pubgrp').siblings().remove();//把已经存在的select去掉
  	 		$('#pubgrp').htmlUtils("select");
  	 	});
  		//初始化选中标签内容
  	 	function initTab(rebuild){
  	 		tab = $('#tabs').tabs('getSelected');//获取选中的面板
  	 		var content = tab.panel("body");
  	 		var excelUrl = content.find("#excelUrl").val();
  	 		var headUrl = content.find("#headUrl").val();
  	 		var reportName = content.find("#reportName").val();
  	 		var dataUrl = content.find("#dataUrl").val();
  	 		var title = content.find("#title").val();
  	 		var grid = content.find("#datagrid");
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp:true,
  	 			excelUrl:excelUrl+'&reportName='+reportName,
  	 			colsChooseUrl:'<%=path%>/chineseSalesReport/toColumnsChoose.do?reportName='+reportName,
  	 			toolbar:['search','excel','option','exit'],
		  	 	searchFun:function(){
  	 				grid.datagrid("reload",getParam($('#queryForm')));
				}
  	 		});

  	 		if(rebuild || grid.css('display') != 'none'){
  	  				firstLoad = true;//重置第一次加载标志
					builtTable({
						headUrl:headUrl+'?reportName='+reportName,
		  	 			dataUrl:dataUrl,
		  	 			title:title,
		  	 			grid:grid,
		  	 			height:tableHeight-50,
		  	 			decimalDigitF:content.find('#decimal').val(),
		  	 			alignCols:['num','zcnt'],
		  	 			numCols:['zamt','damt','zdisc'],
		  	 			onDblClickRow:function(index,data){
// 		  	 			    if(reportName=="projectSalesStatisticsDetail") return;
		  	 				var serials = {};
		  	 				$('#tabs').tabs("select",'<fmt:message key="Statistical_tables" />');
		  	 				$('#vpcode').val(data['PITCODE']);
		  	 				serials['vpcode'] = data['PITCODE'];
		  	 				serials['bdat'] = $("#bdat").val();
		  	 				serials['edat'] = $("#edat").val();
		  	 				serials['pk_store'] = $("#pk_store").val();
		  	 				serials['sft'] = $("#sft").val();
		  	 				serials['pubgrptyp'] = $("#pubgrptyp").val()==null?'':$("#pubgrptyp").val();
		  	 				serials['pubgrp'] = $("#pubgrp").val()==null?'':$("#pubgrp").val();
		  	 				serials['queryMod'] = $("input[name=queryMod]:checked").val();
		  	 				tab = $('#tabs').tabs('getSelected');//获取选中的面板
		  	 	  	 		var content = tab.panel("body");
		  	 				content.find("#datagrid").datagrid('reload',serials);
		  	 			}
		  	 		});
  	  		}
  	 	}

  		//列选择后页面重新加载
  		function pageReload(){
  			closeColChooseWin();
  			initTab(true);
  		}
  	 </script>
  </body>
</html>