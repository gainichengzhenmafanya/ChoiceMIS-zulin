<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>集团消退菜分析</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
		.leftDiv{
			float: left;
			height: 100%;
			width: 120px;
			padding-left: 20px;
			line-height: 30px;
			padding-top: 30px;
		}
		.chartdiv{
			height: 100%;
		}
	</style>
	<%@ include file="../share/permission.jsp"%>
  </head>	
  <body>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label"><fmt:message key="startdate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" />
			</div>
			<div class="form-label" style="width:70px;"><fmt:message key="Regression_of_dish_type" /></div>
			<div class="form-input">
				<input type="radio" id="return" value="<%=PublicEntity.RETURN %>" checked="checked" name="refundTyp"/><label for="return"><fmt:message key="retreat_food" /></label>
				<input type="radio" id="cancel" value="<%=PublicEntity.CANCEL %>" name="refundTyp"/><label for="cancel"><fmt:message key="cancel" /></label>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="enddate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" />
			</div>
			<div class="form-label"><fmt:message key="The_circumstances_of_the_loss" /></div>
			<div class="form-input">
				<select id="status" name="status" style="width: 133px;" class="select">
					<option value=""><fmt:message key="all" /></option>
					<option value="Y"><fmt:message key="The_damaged" /></option>
					<option value="N"><fmt:message key="No_harm" /></option>
				</select>
			</div>
			<div class="form-label" style="width:70px;"><fmt:message key="select" /><fmt:message key="way" /></div>
			<div class="form-input">
				<input type="radio" id="dept" value="1" checked="checked" name="queryMod"/><label for="dept"><fmt:message key="sector" /></label>
				<input type="radio" id="grp" value="2" name="queryMod"/><label for="grp"><fmt:message key="smallClass" /></label>
				<input type="radio" id="grptyp" value="3" name="queryMod"/><label for="grptyp"><fmt:message key="bigClass" /></label>
			</div>
		</div>
		<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }"/>
	</form>
	<div class="easyui-tabs" fit="false" plain="true" id="tabs">
		<div title="<fmt:message key="Regression_analysis_of_group_of_food" />">
			<input type="hidden" id="reportName" value="groupcrdishes"/>
			<input type="hidden" id="excelUrl" value="<%=path%>/chineseSalesReport/exportReport.do"/>
			<input type="hidden" id="headUrl" value="<%=path%>/chineseSalesReport/findGroupCRDishesHeader.do"/>
			<input type="hidden" id="dataUrl" value="<%=path%>/chineseSalesReport/findGroupCRDishes.do"/>
			<input type="hidden" id="title" value="<fmt:message key="Regression_analysis_of_group_of_food" />"/>
				<div id="gridShow"><div id="datagrid"></div></div>
		</div>	
		<div title="<fmt:message key="Single_retreat_food_list" />">
			<input type="hidden" id="reportName" value="groupcrdishes_firm"/>
			<input type="hidden" id="excelUrl" value="<%=path%>/chineseSalesReport/exportReport.do"/>
			<input type="hidden" id="headUrl" value="<%=path%>/chineseSalesReport/findHeaders.do?reportName=groupcrdishes_firm"/>
			<input type="hidden" id="dataUrl" value="<%=path%>/chineseSalesReport/findGroupCRDishesByFirm.do"/>
			<input type="hidden" id="title" value="<fmt:message key="Single_retreat_food_list" />"/>
				<div id="gridShow"><div id="datagrid"></div></div>
		</div> 
	</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		
  	 		//默认<fmt:message key="time" />
		 	$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
  	 		$("#bdat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
		    });
		    $("#edat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
		    });
		  	$("#tabs").tabs({onSelect:function(){
  	 				//初始化选中标签内容
  	 				initTab(false);
  	 			}
  	 		});
  	 	});
  	 	
  		//初始化选中标签内容
  	 	function initTab(rebuild){
//   	 		if(load)firstLoad = true;//重置第一次加载标志
  			var tab = $('#tabs').tabs('getSelected');//获取选中的面板
  	 		var content = tab.panel("body");
  	 		var headUrl = content.find("#headUrl").val();
  	 		var reportName = content.find("#reportName").val();
  	 		var excelUrl = content.find("#excelUrl").val();
  	 		var dataUrl = content.find("#dataUrl").val();
  	 		var title = content.find("#title").val();
  	 		var grid = content.find("#datagrid");
  	 		queryParams = getParam($("#queryForm"));
  	 		
  	 		//生成工具栏 
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp:true,
  	 			excelUrl:excelUrl+'?reportName='+reportName,
  	 			toolbar:['search','excel','exit'],
  	 			searchFun:function(){
  	 				content.find('#datagrid').datagrid('load',getParam($("#queryForm")));
  	 				if(reportName == "groupcrdishes"){
	 					initTab(true);
  	 				}
  	 			}
  	 		});
  	 		if(rebuild || grid.css('display') != 'none'){
	  	 		firstLoad = true;//重置第一次加载标志
	  	 		//生成表格
				if(reportName == "groupcrdishes")
		  	 		builtTable({
		  	 			headUrl:headUrl+"?pk_store="+$("#pk_store").val(),
		  	 			dataUrl:dataUrl,
		  	 			title:title,
		  	 			grid:grid,
		  	 			height:tableHeight,
		  	 			pagination:false,
		 	 			createHeader:function(tableContent,head,frozenHead){
		 	 				columns = [];
		 	 	  			columnsSecond = [];
		 	 	  			frozenColumns = [];
		 	 	  			
		 	 	  			var flag = $("[type='radio'][name='queryMod']:checked").val();
		 	 	 			var dept='<fmt:message key="sector" />';
		 	 	 			if(flag==2){
		 	 	 				dept='<fmt:message key="smallClass" />';
		 	 	 			} else if(flag==3){
		 	 	 				dept='<fmt:message key="bigClass" />';
		 	 	 			} else{
		 	 	 				dept='<fmt:message key="sector" />';
		 	 	 			}
		 	 				frozenColumns.push({field:'DEPT',title:dept,width:70,align:'left',rowspan:2});
		 	 				frozenColumns.push({field:'VDATAGED',title:'<fmt:message key="The_circumstances_of_the_loss" />',width:70,align:'left',rowspan:2});
		 	 				frozenColumns.push({field:'VNAME',title:'<fmt:message key="reason_for_withdrawal_vegetables" />',width:70,align:'left',rowspan:2});
		 	 	  	 		
		 	 	  			for(var i in tableContent){
			 	 	  			columns.push({colspan:2,title:tableContent[i].vname});
			 	 	  			columnsSecond.push({field:'CNT'+tableContent[i].vcode,title:'<fmt:message key="quantity" />',width:80,align:'right'});
			 	 	  			columnsSecond.push({field:'AMT'+tableContent[i].vcode,title:'<fmt:message key="amount" />',width:80,align:'right'});
							}
							head.push(columns);
							head.push(columnsSecond);
							frozenHead.push(frozenColumns);
		  	 			}
		  	 		});
				else
					builtTable({
						headUrl:headUrl,
		  	 			dataUrl:dataUrl,
		  	 			title:title,
		  	 			grid:grid,
		  	 			alignCols:['cnt'],
		  	 			numCols:['amt']
		  	 		});
  	 		}
  	 	}
  		//列选择后页面重新加载
  		function pageReload(){
  			closeColChooseWin();
  			initTab(false);
  		}
  	 </script>
  </body>
</html>
