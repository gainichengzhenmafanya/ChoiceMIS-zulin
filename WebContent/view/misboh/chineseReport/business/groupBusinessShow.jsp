<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>集团营业显示分析</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <style type="text/css">
        .content {
            border: 1px solid #B1C3D9;
            width: 230px;
            margin: 10px;
            float: left;
            height: 400px;
            overflow: auto;
        }

        .content > div {
            height: 20px;
            margin: 10px;
        }

        .content label {
            font-size: 14px;
            width: 80px;
            text-align: right;
            display: inline-block;
            float: left;
            *display: inline;
            zoom: 1;
        }

        .content span {
            font-size: 14px;
            width: 120px;
            text-align: right;
            display: inline-block;
            *display: inline;
            zoom: 1;
        }
    </style>
    <%@ include file="../share/permission.jsp"%>
</head>
<body>
<div id="tool"></div>
<form id="queryForm" name="queryForm" method="post">
    <div class="form-line">
       <div class="form-label"><fmt:message key="startdate" /></div>
		<div class="form-input">
			<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" />
		</div>
        <div class="form-label"><fmt:message key="enddate" /></div>
		<div class="form-input">
			<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" />
		</div>
        <div class="form-label"><fmt:message key="flight" /></div>
		<div class="form-input">
			<select id="sft" name="sft" style="width: 133px;" class="select">
				<option value="0"><fmt:message key="all_day" /></option>
				<c:forEach var="sft" items="${sftList}">
					<option value="${sft.vcode}" <c:if test="${sft.vcode==condition.sft}"> selected="selected" </c:if> >${sft.vname}</option>
				</c:forEach>				
			</select>
		</div>			
    </div>
	<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }"/>
</form>
<div>
    <div class="content">
        <div>
            <label class="contentie7"><fmt:message key="business_income" />：</label>
            <span id="AMT">￥0.00</span>
        </div>
        <div>
            <label></label>
            <span></span>
        </div>
        <div>
            <label class="contentie7"><fmt:message key="Paid_in_amount" />：</label>
            <span id="JAMT">￥0.00</span>
        </div>
        <div>
            <label class="contentie7"><fmt:message key="Optimal_from_the_forehead" />：</label>
            <span id="YMAMT">￥0.00</span>
        </div>
        <div>
            <label></label>
            <span></span>
        </div>
        <div>
            <label class="contentie7"><fmt:message key="The_amount_incurred" />：</label>
            <span id="FAMT">￥0.00</span>
        </div>
        <div>
            <label class="contentie7"><fmt:message key="ZengShouE" />：</label>
            <span id="ZAMT">￥0.00</span>
        </div>
    </div>
    <div class="content">
        <div>
            <label class="contentie7"><fmt:message key="Paid_in_amount" />：</label>
            <span id="PAYMENTJAMT">￥0.00</span>
        </div>
        <div id="payment_positn">
            <label class="contentie7"><fmt:message key="Activity_paid_in" />：</label>
            <span id="ACTJAMT">￥0.00</span>
        </div>
        <div>
            <label></label>
            <span></span>
        </div>
        <hr></hr>
        <div>
            <label><fmt:message key="Optimal_from_the_forehead" />：</label>
            <span id="DYMAMT">￥0.00</span>
        </div>
        <div>
            <label><fmt:message key="Preferential_remission_activities" />：</label>
            <span id="ACTYMAMT">￥0.00</span>
        </div>
        <div>
            <label><fmt:message key="Payment_of_the_paid_in_amount" />：</label>
            <span id="NREFUNDAMT">￥0.00</span>
        </div>
        <div>
            <label><fmt:message key="maLing" />：</label>
            <span id="BZEROAMT">￥0.00</span>
        </div>
    </div>
    <div class="content">
        <div id="grptyp_positn">
            <label><fmt:message key="The_amount_incurred" />：</label>
            <span id="GRPTYPFAMT">￥0.00</span>
        </div>
        <div>
            <label></label>
            <span></span>
        </div>
        <hr></hr>
        <div>
            <label><fmt:message key="ZengShouE" />：</label>
            <span id="DZAMT">￥0.00</span>
        </div>
        <div>
            <label><fmt:message key="BaoJianFei" />：</label>
            <span id="ROOMAMT">￥0.00</span>
        </div>
        <div>
            <label><fmt:message key="service_charge" />：</label>
            <span id="NSVCCHG">￥0.00</span>
        </div>
        <div>
            <label><fmt:message key="An_activity" />：</label>
            <span id="NSVCITEM">￥0.00</span>
        </div>
    </div>
    <div class="content">
        <div>
            <label><fmt:message key="The_highest_consumption" />：</label>
            <span id="MAXFOLIO">￥0.00</span>
        </div>
        <div>
            <label><fmt:message key="open_the_numbers" />：</label>
            <span id="TBLS">0</span>
        </div>
        <div>
            <label><fmt:message key="turn_sets_the_rate" />：</label>
            <span id="TBLSRATE">0％</span>
        </div>
        <div>
            <label><fmt:message key="dining" /><fmt:message key="number_of_people" />：</label>
            <span id="PAX">0</span>
        </div>
        <div>
            <label><fmt:message key="attendance" />：</label>
            <span id="PAXRATE">0％</span>
        </div>
        <div>
            <label><fmt:message key="Preferential_remission_rate" />：</label>
            <span id="YMRATE">0％</span>
        </div>
        <div>
            <label><fmt:message key="Every_single" />：</label>
            <span id="TBLSAVG">0</span>
        </div>
        <div>
            <label><fmt:message key="The_number_of_each_table" />：</label>
            <span id="PAXAVG">0</span>
        </div>
        <div>
            <label><fmt:message key="Fold_the_former_per_capita" />：</label>
            <span id="AMTAVG">0</span>
        </div>
        <div>
            <label><fmt:message key="After_the_discount_per_capita" />：</label>
            <span id="DISCAVG">0</span>
        </div>
    </div>
</div>
<div id="wait2" style="visibility: hidden;"></div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
	 	//默认<fmt:message key="time" />
	 	$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
 	 	$("#bdat").click(function(){
	        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
	    });
	    $("#edat").click(function(){
	        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
	    });
        //生成工具栏
        builtToolBar({
            basePath: "<%=path%>",
            toolbarId: 'tool',
            formId: 'queryForm',
            excelUrl: '<%=path%>/chineseBusinessReport/exportReport.do?reportName=groupBusinessShow',
            toolbar: ['search','exit'],
            curName: 'TELE-JTYYXXFX',
            searchFun: function () {
                $.ajax({
                    url: "<%=path%>/chineseBusinessReport/findGroupBusinessShow.do",
                    type: "POST",
                    data: getParam($('#queryForm')),
                    success: function (data) {
                        var rows =  data['rows'];
                        data =rows[0];
                        for (var key in data) {
                            $('#' + key).text(data[key]);
                        }
                        $(".dynamic").remove();
                        var flag = 1;
                        for(var i = 1 ; i < rows.length ; i ++){
                        	if(rows[i].DES=="实收明细-大类统计"){
                        		flag = 2;
                        		continue;
                        	}
                        	if(flag==1){
                        		$("#payment_positn").after($("<div><label>" + rows[i].DES+ "：</label><span>" + rows[i].AMT+ "</span></div>").addClass("dynamic"));
                        	}else if(flag==2){
                        		$("#grptyp_positn").after($("<div><label>" + rows[i].DES+ "：</label><span>" + rows[i].AMT+ "</span></div>").addClass("dynamic"));
                        	}
                        }
                    },
                    beforeSend: function () {
                        $('#wait2,#wait').css("visibility", "visible");
                    },
                    complete: function () {
                        $('#wait2,#wait').css("visibility", "hidden");
                    }
                });
            }
        });
    });
</script>
</body>
</html>
