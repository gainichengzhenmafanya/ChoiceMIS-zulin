<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>集团销售分析-汇总查询</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
		.leftDiv{
			float: left;
			height: 100%;
			width: 120px;
			padding-left: 20px;
			line-height: 25px;
			padding-top: 10px;
			clear: none;
		}
		.chartdiv{
			height: 100%;
			display: none;
			float: left;
		}
	</style>
	<%@ include file="../share/permission.jsp"%>
  </head>	
  <body>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label" style="width:60px;"><fmt:message key="startdate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" />
			</div>
			<div class="form-label" style="width:60px;"><fmt:message key="pubitem_name" /></div>
			<div class="form-input"><input type="text" id="vpname" name="vpname" class="text"/></div>
			<div class="form-label" style="width:60px;"><fmt:message key="pubitem" /><fmt:message key="abbreviation" /></div>
			<div class="form-input"><input type="text" id="vinit" name="vinit" class="text"/></div>
		</div>
		<div class="form-line">
			<div class="form-label" style="width:60px;"><fmt:message key="enddate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" />
			</div>
			<div class="form-label" style="width:60px;"><fmt:message key="bigClass" /></div>
			<div class="form-input">
				<select name="pubgrptyp" id="pubgrptyp" style="width: 133px;" class="select" key="VCNCODEONE" data="NAME" select="true"></select>
			</div>
			<div class="form-label" style="width:60px;"><fmt:message key="smallClass" /></div>
			<div class="form-input">
				<select name="pubgrp" id="pubgrp" style="width: 133px;" class="select" key="VCNCODETWO" data="NAME" select="true"></select>
			</div>
			<div class="form-label byDate" style="width:60px;"><fmt:message key="period_of_time" /></div>
			<div class="form-input byDate">
				<select name="sumMod" id="sumModByDate" class="select">
					<option value="3"><fmt:message key="According_to_the_aggregate" /></option>
					<option value="4"><fmt:message key="A_monthly_summary" /></option>
					<option value="5"><fmt:message key="The_quarterly_summary" /></option>
					<option value="9"><fmt:message key="Half_a_year_summary" /></option>
					<option value="6"><fmt:message key="According_to_the_annual_summary" /></option>
					<option value="8"><fmt:message key="According_to_the_week" /></option>
					<option value="7"><fmt:message key="Weekly_summary" /></option>
				</select>
			</div>
		</div>
		<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }"/>
	</form>
	<div class="easyui-tabs" fit="false" plain="true" id="tabs">
		<div title="<fmt:message key="By_the_summary" />">
			<input type="hidden" id="reportName" value="sales_sum_byfood"/>
			<input type="hidden" id="excelUrl" value="<%=path%>/chineseBusinessReport/exportReport.do?sumMod=<%=PublicEntity.BYFOOD%>"/>
			<input type="hidden" id="dataUrl" value="<%=path%>/chineseBusinessReport/findGroupSales.do?sumMod=<%=PublicEntity.BYFOOD%>"/>
			<input type="hidden" id="title" value="<fmt:message key="According_to_the_food_sales_summary_list" />"/>
				<div id="gridShow"><div id="datagrid"></div></div>
		</div>
		<div title="<fmt:message key="According_to_the_kind_of_summary" />">
			<input type="hidden" id="reportName" value="sales_sum_bycategory"/>
			<input type="hidden" id="excelUrl" value="<%=path%>/chineseBusinessReport/exportReport.do?sumMod=<%=PublicEntity.BYCATEGORY%>"/>
			<input type="hidden" id="dataUrl" value="<%=path%>/chineseBusinessReport/findGroupSales.do?sumMod=<%=PublicEntity.BYCATEGORY%>"/>
			<input type="hidden" id="title" value="<fmt:message key="According_to_the_sales_summary_list" />"/>
				<div id="gridShow"><div id="datagrid"></div></div>
		</div>
		<div title="<fmt:message key="According_to_the_time_period_summary" />">
			<input type="hidden" id="byDate" value="true"/>
			<input type="hidden" id="reportName" value="groupsales_sum_bydate"/>
			<input type="hidden" id="excelUrl" value="<%=path%>/chineseBusinessReport/exportReport.do"/>
			<input type="hidden" id="dataUrl" value="<%=path%>/chineseBusinessReport/findGroupSales.do"/>
			<input type="hidden" id="title" value="<fmt:message key="By_the_time_the_summary_list" />"/>
				<div id="gridShow"><div id="datagrid"></div></div>
		</div>
	</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
		 	//默认<fmt:message key="time" />
		 	$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
  	 		$("#bdat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
		    });
		    $("#edat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
		    });
  	 		//为tab添加选择事件
  	 		$("#tabs").tabs({onSelect:function(){
  	 				//初始化选中标签内容
  	 				initTab();
  	 			}
  	 		});
  	 		
  	 	 	$('#pubgrptyp').htmlUtils("select");
	 		//页面加载时默认查询小类信息
	 		$('#pubgrptyp').attr("url","<%=path%>/chineseBusinessReport/findAllMarsaleclassOne.do");
	 		$('#pubgrptyp').siblings().remove();//把已经存在的select去掉
	 		$('#pubgrptyp').htmlUtils("select");
	 		
	 		$('#pubgrp').htmlUtils("select");
	 		//页面加载时默认查询小类信息
	 		$('#pubgrp').attr("url","<%=path%>/chineseBusinessReport/findAllMarsaleclassTwo.do");
	 		$('#pubgrp').siblings().remove();//把已经存在的select去掉
	 		$('#pubgrp').htmlUtils("select");
  	 	});
  		
  		//初始化选中标签内容
  	 	function initTab(){
  	 		tab = $('#tabs').tabs('getSelected');//获取选中的面板
  	 		var content = tab.panel("body");
  	 		var excelUrl = content.find("#excelUrl").val();
  	 		var reportName = content.find("#reportName").val();
  	 		var dataUrl = content.find("#dataUrl").val();
  	 		var title = content.find("#title").val();
  	 		var grid = content.find("#datagrid");
  	 		if(content.find("#byDate").val()){
  	 			$('.byDate').show();
  	 			$('#sumModByDate').removeAttr('disabled');
  	 		}else{
  	 			$('.byDate').hide();
  	 			$('#sumModByDate').attr('disabled','disabled');
  	 		};
  	 		
  	 		//生成工具栏
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp:true,
  	 			excelUrl:excelUrl+'&reportName='+reportName,
  	 			colsChooseUrl:'<%=path%>/chineseBusinessReport/toColumnsChoose.do?reportName='+reportName,
  	 			toolbar:['search','excel','exit'],
  	 			searchFun:function(){
  	 				grid.datagrid("load",getParam($("#queryForm")));
  	 			}
  	 		});
  	 		//生成表格
  	 		if(!grid.data('loaded')){
  	 			builtTable({
	  	 			headUrl:"<%=path%>/chineseBusinessReport/findHeaders.do?reportName="+reportName,
	  	 			dataUrl:dataUrl,
	  	 			title:title,
	  	 			grid:grid,
	  	 			alignCols:['cnt'],
	  	 			numCols:['amt','ymamt','jamt','famt']
	  	 		});
  	 			grid.data('loaded',true);
  	 		}
  	 	}
  		//列选择后页面重新加载
  		function pageReload(){
  			closeColChooseWin();
  			initTab();
  			$('#tabs').tabs('getSelected').panel("body").find("#datagrid").datagrid('reload');
  		}
  	 </script>
  </body>
</html>
