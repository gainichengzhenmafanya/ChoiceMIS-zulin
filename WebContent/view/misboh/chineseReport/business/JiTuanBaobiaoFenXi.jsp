<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>营业日报区域汇总表</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
		.leftDiv{
			float: left;
			height: 100%;
			width: 120px;
			padding-left: 20px;
			line-height: 30px;
			padding-top: 30px;
		}
		.chartdiv{
			height: 100%;
		}
	</style>
	<%@ include file="../share/permission.jsp"%>
  </head>	
  <body>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label" style="width:60px;"><fmt:message key="date" /></div>
			<div class="form-input"><input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" /></div>
			<div class="form-label" style="width:20px;"><fmt:message key="to2" /></div>
			<div class="form-input"><input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" /></div>
			<div class="form-label" style="width:60px;"><fmt:message key="statistical" /><fmt:message key="way" /></div>
			<div class="form-input">
				<select id="sumMod" name="sumMod" class="select" style="width:134px;margin-top: 0px;">
					<option value="1"><fmt:message key="floor" /></option>
					<option value="2"><fmt:message key="area" /></option>
				</select>
			</div>
			<div class="form-label" style="width:150px;">
				<input type="radio" id="mon1" name="mon" value="1"/><fmt:message key="This_month" />
				<input type="radio" id="mon2" name="mon" value="2"/><fmt:message key="Last_month" />
				<input type="radio" id="mon3" name="mon" value="3"/><fmt:message key="Next_month" />
			</div>
		</div>
		<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }"/>
	</form>
	<div id="datagrid"></div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	<script type="text/javascript">
  	 	$(document).ready(function(){
  	 		$(":radio").click(function(){
  	 			var dat = new Date();
  	 			$("#bdat").val(getDatebyB($(this).val(),dat.getFullYear(),dat.getMonth()+1));
  	 			$("#edat").val(getDatebyF($(this).val(),dat.getFullYear(),dat.getMonth()+1));
  	 		});
			
  	 		//默认<fmt:message key="time" />
	 		$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
  	 		$("#bdat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
		    });
		    $("#edat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
		    });
// 	  		$("#bdat").val($("#edat").val().substr(0,8)+"01");
	  		
  	 		initTable();
  	 	});
  	 	
  	 	function initTable(){
  	 		queryParams = getParam($("#queryForm"));
  	 		
  	  	 	//生成工具栏
  	  	 		builtToolBar({
  	  	 			basePath:"<%=path%>",
  	  	 			toolbarId:'tool',
  	  	 			formId:'queryForm',
  	  	 			grid:$("#datagrid"),
  	  	 			exportTyp:true,
  	  	 		    excelUrl:'<%=path%>/chineseBusinessReport/exportReportJTRFX.do',
  	  	 		    toolbar:['search','excel','exit'],
  	  	 			searchFun:initTable
  	  	 		});
  	  	 	
  	  	 		//生成表格
  	  	  		builtTable({
  	  	 			headUrl:"<%=path%>/chineseBusinessReport/findHeaderForJTRFX.do",
  	  	 			dataUrl:"<%=path%>/chineseBusinessReport/findJTRFX.do",
  	  	 			title:'<fmt:message key="Business_daily_area_summary_table"/>',
  	  	 			grid:$("#datagrid"),
  	  	 			pagination:false,
	  	 			showFooter:false,
  	  	 			createHeader:function(data,head,frozenHead){
  	  	 				var pay = data.pay;
  	  	 				var dept = data.dept;
  	  	 				var col1 = [];
  	  	 				var col2 = [];

  	  	 				frozenHead.push([{field:'SFT',width:50,title:'<fmt:message key="flight"/>',align:'left',rowspan:2},
	  	 								{field:'AREA',width:90,title:'<fmt:message key="area"/>(<fmt:message key="The_Numbers"/>)',align:'left',rowspan:2}]);
  	  	 			
  	  	 			    col1.push({title:"<fmt:message key='The_cumulative'/>("+$("#bdat").val().substr(5,10)+"~"+$("#edat").val().substr(5,10)+")",colspan:7});
  	  	 				col2.push({field:"PAXL",width:75,title:'<fmt:message key="number_of_people"/>',align:'right'});
  	  	 				col2.push({field:"ACL",width:75,title:'<fmt:message key="After_the_discount_per_capita"/>',align:'right'});
  	  	 				col2.push({field:"NUML",width:75,title:'<fmt:message key="open_the_numbers"/>',align:'right'});
  	  	 				col2.push({field:"SZLL",width:75,title:'<fmt:message key="attendance"/>',align:'right'});
  	  	 				col2.push({field:"TCL",width:80,title:'<fmt:message key="List_all_consumption"/>',align:'right'});
  	  	 				col2.push({field:"AMTL",width:90,title:'<fmt:message key="Paid_in_amount"/><fmt:message key="total"/>',align:'right'});
  	  					col2.push({field:"BILLAMTL",width:75,title:'<fmt:message key="The_invoice_amount"/>',align:'right'});
  	  	 			    col1.push({title:"<fmt:message key='This_day'/>",colspan:7});
  	  	 				col2.push({field:"PAX",width:75,title:'<fmt:message key="number_of_people"/>',align:'right'});
	  	 				col2.push({field:"AC",width:75,title:'<fmt:message key="After_the_discount_per_capita"/>',align:'right'});
	  	 				col2.push({field:"NUM",width:75,title:'<fmt:message key="open_the_numbers"/>',align:'right'});
	  	 				col2.push({field:"SZL",width:75,title:'<fmt:message key="attendance"/>',align:'right'});
	  	 				col2.push({field:"TC",width:80,title:'<fmt:message key="List_all_consumption"/>',align:'right'});
	  	 				col2.push({field:"AMT",width:90,title:'<fmt:message key="Paid_in_amount"/><fmt:message key="total"/>',align:'right'});
	  					col2.push({field:"BILLAMT",width:75,title:'<fmt:message key="The_invoice_amount"/>',align:'right'});
	  					
	  					col1.push({title:"<fmt:message key='The_classification_of_settlement_subsidiary'/>",colspan:pay.length+1});
	  					col2.push({field:"ACTJAMT",width:75,title:'<fmt:message key="Activity_paid_in"/>',align:'right'});
	  					for(var cur in pay){
	  	 					col2.push({field:"PAY"+pay[cur]['VCODE'],width:75,title:pay[cur]['VNAME'],align:'right'});
	  	 				}
	  					col1.push({title:"<fmt:message key='Revenue_classification_details'/>",colspan:dept.length});
	  					for(var cur in dept){
	  	 					col2.push({field:"DEPT"+dept[cur].vcode,width:75,title:dept[cur].vname,align:'right'});
	  	 				}
	  					
  	  	 				head.push(col1);
  	  	 				head.push(col2);
  	  	 			}
  	  	 		});
  	  	 	}
  	 	
  	 	function getDatebyB(mon,year,month){
  	 		if(mon==1){
  	 			month = month<10?"0"+month:month;
  	 			return year+"-"+month+"-01";
  	 		}
			if(mon==2){
				if(month-1==0){
					return (year-1)+"-12-01";
				}else{
					month = (month-1)<10?"0"+(month-1):(month-1);
					return year+"-"+month+"-01";
				}			
			}
			if(mon==3){
				if(month+1>12){
					return (year+1)+"-01-01";
				}else{
					month = (month+1)<10?"0"+(month+1):(month+1);
					return year+"-"+month+"-01";
				}	
			}
		}
  	 	function getDatebyF(mon,year,month){
			var days = 0;
			var month1={1:31,3:31,5:31,7:31,8:31,10:31,12:31,4:30,6:30,9:30,11:30};
			
			if(mon==1){
				if(month1[month]){
					days = month1[month];
				}else if(month==2){
					days =year%400 == 0 || (year%4==0 && year%100!=0)?29:28;
				}
				month = month<10?"0"+month:month;
				return year+"-"+month+"-"+days;
			}
			if(mon==2){
				if(month-1==0){
					return (year-1)+"-12-31";
				}else{
					if(month1[month-1]){
						days = month1[month-1];
					}else if(month-1==2){
						days =year%400 == 0 || (year%4==0 && year%100!=0)?29:28;
					}
					month = (month-1)<10?"0"+(month-1):(month-1);
					return year+"-"+month+"-"+days;
				}				
			}
			if(mon==3){
				if(month+1>12){
					return (year+1)+"-01-31";
				}else{
					if(month1[month+1]){
						days = month1[month+1];
					}else if(month+1==2){
						days =year%400 == 0 || (year%4==0 && year%100!=0)?29:28;
					}
					month = (month+1)<10?"0"+(month+1):(month+1);
					return year+"-"+month+"-"+days;
				}
			}
		}
  	 </script>
  </body>
</html>
