<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>集团点菜出错率分析</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<%@ include file="../share/permission.jsp"%>
  </head>	
  <body>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label" style="width:60px;"><fmt:message key="startdate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" />
			</div>
		</div>
		<div class="form-line">
			<div class="form-label" style="width:60px;"><fmt:message key="enddate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" />
			</div>
			<div class="form-label" style="width:60px;"><fmt:message key="retreat_food" /><fmt:message key="sector" /></div>
			<div class="form-input">
				<select name="idept" id="idept" style="width: 133px;" url="<%=path%>/groupDept/findAllGroupDept.do" class="select" key="vcode" data="vname" select="true"></select>
			</div>
		</div>
		<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }"/>
	</form>
	<div id="datagrid"></div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		$('#idept').htmlUtils("select");
  	 		
		 	//默认<fmt:message key="time" />
		 	$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
  	 		$("#bdat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
		    });
		    $("#edat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
		    });
  	 		//生成工具栏
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			gridId:'datagrid',
  	 			exportTyp:true,
  	 			excelUrl:'<%=path%>/chineseBusinessReport/exportReport.do?reportName=JiTuanDianCaiChuCuoLv',
  	 			colsChooseUrl:'<%=path%>/chineseBusinessReport/toColumnsChoose.do?reportName=JiTuanDianCaiChuCuoLv',
  	 			toolbar:['search','excel','option','exit'],
  	 		});
  	 		//生成表格
  	  		builtTable({
  	 			headUrl:"<%=path%>/chineseBusinessReport/findHeaders.do?reportName=JiTuanDianCaiChuCuoLv",
  	 			dataUrl:"<%=path%>/chineseBusinessReport/findCaiPinChuCuoLv.do",
  	 			title:'<fmt:message key="Analysis_of_the_rate_of_the_group_order_error"/>',
  	 			id:'datagrid',
  	 			numCols:['ttlcnt','ttlamt','cnt','amt','cntrate','amtrate']
  	 		});
  	 	});
  	 </script>
  </body>
</html>
