<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>集团收入分析</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
		.leftDiv{
			float: left;
			height: 100%;
			width: 120px;
			padding-left: 20px;
			line-height: 25px;
			padding-top: 10px;
			clear: none;
		}
		.chartdiv{
			height: 100%;
			display: none;
			float: left;
		}
	</style>
	<%@ include file="../share/permission.jsp"%>
  </head>	
  <body>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label"><fmt:message key="startdate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" />
			</div>
			<div class="form-label"><fmt:message key="enddate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" />
			</div>
			<div class="form-label"><fmt:message key="flight" /></div>
			<div class="form-input">
				<select id="sft" name="sft" style="width: 133px;" class="select">
					<option value="0"><fmt:message key="all_day" /></option>
					<c:forEach var="sft" items="${sftList}">
						<option value="${sft.vcode}" <c:if test="${sft.vcode==condition.sft}"> selected="selected" </c:if> >${sft.vname}</option>
					</c:forEach>					
				</select>
			</div>	
			<div class="form-label byDate" style="width:60px;"><fmt:message key="period_of_time" /></div>
			<div class="form-input byDate">
				<select name="sumMod" id="sumModByDate" class="select">
					<option value="3"><fmt:message key="According_to_the_aggregate" /></option>
					<option value="4"><fmt:message key="A_monthly_summary" /></option>
					<option value="5"><fmt:message key="The_quarterly_summary" /></option>
					<option value="9"><fmt:message key="Half_a_year_summary" /></option>
					<option value="6"><fmt:message key="According_to_the_annual_summary" /></option>
					<option value="8"><fmt:message key="According_to_the_week" /></option>
					<option value="7"><fmt:message key="Weekly_summary" /></option>
				</select>
			</div>
		</div>
		<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }"/>
	</form>
	<div class="easyui-tabs" fit="false" plain="true" id="tabs">
		<div title="<fmt:message key="According_to_the_section_of_the_summary" />">
			<input type="hidden" id="byDate" value="true"/>
			<input type="hidden" id="reportName" value="groupIncome_ByDate"/>
			<input type="hidden" id="excelUrl" value="<%=path%>/chineseBusinessReport/exportReport.do"/>
			<input type="hidden" id="dataUrl" value="<%=path%>/chineseBusinessReport/findGroupIncome.do"/>
			<input type="hidden" id="title" value="<fmt:message key="According_to_the_period_of_the_sales_summary_list" />"/>
				<div id="gridShow"><div id="datagrid"></div></div>
		</div>
		<div title="<fmt:message key="On_a_daily_basis_detailed" />">
			<input type="hidden" id="reportName" value="groupIncome_ByDay"/>
			<input type="hidden" id="excelUrl" value="<%=path%>/chineseBusinessReport/exportReport.do?sumMod=<%=PublicEntity.DETAILBYDAY%>"/>
			<input type="hidden" id="dataUrl" value="<%=path%>/chineseBusinessReport/findGroupIncome.do?sumMod=<%=PublicEntity.DETAILBYDAY%>"/>
			<input type="hidden" id="title" value="<fmt:message key="Daily_sales_detailed_list" />"/>
				<div id="gridShow"><div id="datagrid"></div></div>
		</div>
	</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		
		 	//默认<fmt:message key="time" />
		 	$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
  	 		$("#bdat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
		    });
		    $("#edat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
		    });
  	 		//为tab添加选择事件
  	 		$("#tabs").tabs({onSelect:function(){
  	 				//初始化选中标签内容
  	 				initTab();
  	 			}
  	 		});
  	 	});
  	 	
  		//初始化选中标签内容
  	 	function initTab(){
  	 		tab = $('#tabs').tabs('getSelected');//获取选中的面板
  	 		var content = tab.panel("body");
  	 		var excelUrl = content.find("#excelUrl").val();
  	 		var reportName = content.find("#reportName").val();
  	 		var dataUrl = content.find("#dataUrl").val();
  	 		var title = content.find("#title").val();
  	 		var grid = content.find("#datagrid");
  	 		var pagination = true;
  	 		excelUrl.indexOf('?') >= 0 ? excelUrl += '&' : excelUrl += '?';
  	 		if(content.find("#byDate").val()){
  	 			$('.byDate').show();
  	 			$('#sumModByDate').removeAttr('disabled');
  	 		}else{
  	 			$('.byDate').hide();
  	 			$('#sumModByDate').attr('disabled','disabled');
  	 		};
  	 		//生成工具栏
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp:true,
  	 			excelUrl:excelUrl+'reportName='+reportName,
  	 			colsChooseUrl:'<%=path%>/chineseBusinessReport/toColumnsChoose.do?reportName='+reportName,
  	 			toolbar:['search','excel','exit'],
  	 			searchFun:function(){
  	 				grid.datagrid("load",getParam($("#queryForm")));
  	 			}
  	 		});
  	 		//生成表格
  	 		if(grid.css('display') != 'none'){
	 	 		builtTable({
	  	 			headUrl:"<%=path%>/chineseBusinessReport/findHeaderForGroupIncome.do",
	  	 			dataUrl:dataUrl,
	  	 			title:title,
	  	 			grid:grid,
	  	 			createHeader:function(data,head,frozenHead){
	  	 				var colFirst = [];
	  	 				var colSecond = [];
	  	 				
	  	 				if("groupIncome_ByDate" == reportName){
	  	 					frozenHead.push([{field:'DWORKDATE',width:130,rowspan:2,title:'<fmt:message key="period_of_time"/>'}]);
	  	 				}else{
	  	 					frozenHead.push([{field:'VNAME',width:130,rowspan:2,title:'<fmt:message key="branches_name"/>'},
	  	 					              {field:'DWORKDATE',width:90,rowspan:2,title:'<fmt:message key="date"/>',align:'center'}]);
	  	 				}
	  	 				
	  	 				colFirst.push({field:'PAX',width:60,rowspan:2,title:'<fmt:message key="number_of_people"/>',align:'right'});
	  	 				colFirst.push({field:'TBLS',width:60,rowspan:2,title:'<fmt:message key="open_the_numbers"/>',align:'right'});
	  	 				colFirst.push({field:'FAMT',width:80,rowspan:2,title:'<fmt:message key="The_amount_incurred"/>',align:'right'});
	  	 				
	  	 				colFirst.push({colspan:4,title:'<fmt:message key="ZengShouE"/>'});
	  	 				colSecond.push({field:'NSVCCHG',width:70,title:'<fmt:message key="service_charge"/>',align:'right'});
	  	 				colSecond.push({field:'ROOMAMT',width:70,title:'<fmt:message key="BaoJianFei"/>',align:'right'});
	  	 				colSecond.push({field:'NSVCITEM',width:70,title:'<fmt:message key="An_activity"/>',align:'right'});
	  	 				colSecond.push({field:'ZAMT',width:80,title:'<fmt:message key="total"/>',align:'right'});
	  	 				
	  	 				colFirst.push({field:'AMT',width:80,rowspan:2,title:'<fmt:message key="business_income"/>',align:'right'});
	  	 				
	  	 				colFirst.push({colspan:4,title:'<fmt:message key="Optimal_from_the_forehead"/>'});
	  	 				colSecond.push({field:'ACTYMAMT',width:70,title:'<fmt:message key="Preferential_remission_activities"/>',align:'right'});
	  	 				colSecond.push({field:'NREFUNDAMT',width:100,title:'<fmt:message key="Payment_of_the_paid_in_amount"/>',align:'right'});
	  	 				colSecond.push({field:'BZEROAMT',width:70,title:'<fmt:message key="maLing"/>',align:'right'});
	  	 				colSecond.push({field:'YMAMT',width:80,title:'<fmt:message key="total"/>',align:'right'});
	  	 				
	  	 				colFirst.push({colspan:data.length+2,title:'<fmt:message key="the_actual_turnover"/>'});
	  	 				for(var cur in data){
	  	 					colSecond.push({field:'PAY'+data[cur]['VCODE'],width:70,title:data[cur]['VNAME'],align:'right'});
	  	 				}
	  	 				colSecond.push({field:'ACTJAMT',width:70,title:'<fmt:message key="Activity_paid_in"/>',align:'right'});
	  	 				colSecond.push({field:'JAMT',width:80,title:'<fmt:message key="total"/>',align:'right'});
	  	 				
	  	 				colFirst.push({field:'DISCAVG',width:80,rowspan:2,title:'<fmt:message key="After_the_discount_per_capita"/>',align:'right'});
	  	 				colFirst.push({field:'TBLSAVG',width:80,rowspan:2,title:'<fmt:message key="Fold_the_sheet_after_all"/>',align:'right'});
	  	 				colFirst.push({field:'MAXFOLIO',width:80,rowspan:2,title:'<fmt:message key="The_highest_consumption"/>',align:'right'});
	  	 				colFirst.push({field:'NINVOICEMONEY',width:80,rowspan:2,title:'<fmt:message key="The_invoice_amount"/>',align:'right'});
	  	 				
	  	 				head.push(colFirst);
	  	 				head.push(colSecond);
	  	 			}
	  	 		});
  	 		}
  	 	}
  		//列选择后页面重新加载
  		function pageReload(){
  			closeColChooseWin();
  			initTab();
  		}
  	 </script>
  </body>
</html>
