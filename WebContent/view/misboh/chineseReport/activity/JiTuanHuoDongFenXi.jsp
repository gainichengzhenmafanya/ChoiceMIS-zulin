<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>集团<fmt:message key="activity" />分析</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
		.leftDiv{
			float: left;
			height: 100%;
			width: 120px;
			padding-left: 20px;
			line-height: 30px;
			padding-top: 30px;
		}
		.chartdiv{
			height: 100%;
		}
		.form-label2{
			width: 40px;
			height: 25px;
			position: relative;
			margin: 0;
			float: left;
			text-align: right;
			vertical-align: middle;
			line-height: 25px;
			margin-right: 5px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 6.5%;
		}
		form .form-line .form-input{
			width: 13%;
		}
		form .form-line .form-input input[type=text]{
			width: 85%;
		}
	</style>
  </head>	
  <body>
	<%@ include file="../share/permission.jsp"%>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="year" >
				<div class="form-label"><fmt:message key="Analysis_of_the" /><fmt:message key="years" /></div>
				<div class="form-input">
					<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" />
				</div>
			</div>
			<div class="month" >
				<div class="form-label"><fmt:message key="date" /></div>
				<div class="form-input">
					<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" />
				</div>
			</div>			
		</div>
		<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }"/>
	</form>
	<div class="easyui-tabs" fit="true" plain="true" id="tabs" >
		<div title='<fmt:message key="Daily_analysis" />'>
			<input type="hidden" id="reportName" value="jituanhuodongfenxi"/>
			<input type="hidden" id="excelUrl" value="<%=path%>/chineseActivityReport/exportReport.do"/>
			<input type="hidden" id="printUrl" value="<%=path%>/chineseActivityReport/printReport.do"/>
			<input type="hidden" id="dataUrl" value="<%=path%>/chineseActivityReport/queryJiTuanHuoDongFenXi.do"/>
			<input type="hidden" id="title" value="<fmt:message key="Group_activity_analysis" />"/>
			<div id="datagrid"></div>
		</div>
		<div title='<fmt:message key="Monthly_analysis" />' >
			<input type="hidden" id="reportName" value="jituanhuodongfenxibymonth"/>
			<input type="hidden" id="excelUrl" value="<%=path%>/chineseActivityReport/exportReport.do"/>
			<input type="hidden" id="printUrl" value="<%=path%>/chineseActivityReport/printReport.do"/>
			<input type="hidden" id="dataUrl" value="<%=path%>/chineseActivityReport/queryJiTuanHuoDongFenXiByMonth.do"/>
			<input type="hidden" id="title" value="<fmt:message key="Group_activity_analysis" />"/>
			<div id="datagrid"></div>
		</div>
	</div>
	
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
			
			$("#bdat").click(function(){ 
				new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
			});
  	 		//默认<fmt:message key="time" />
  	 		$("#bdat").htmlUtils("setDate","yes",'${newdate}');
  	 		
  	 		$("#edat").click(function(){ 
  	  	 		new WdatePicker({dateFmt:'yyyy'});
  	  	 	});
  	 		//默认<fmt:message key="time" />
  	 		$("#edat").htmlUtils("setDate","curYear",'${newdate}');
  	 		
  	 		//为tab添加选择事件
  	 		$("#tabs").tabs({onSelect:function(){
  				//初始化选中标签内容
  					//tab = $('#tabs').tabs('getSelected');//获取选中的面板
  					initTab(true);
  	 			}
  	 		});
  	 	});
  	
  		//初始化<fmt:message key="selected" />标签<fmt:message key="content" />
  	 	function initTab(rebuild){
  	 		tab = $('#tabs').tabs('getSelected');//获取选中的面板
  	 		var content = tab.panel("body");
  	 		
  	 		var excelUrl = content.find("#excelUrl").val();
  	 		var reportName = content.find("#reportName").val();
  	 		var headUrl = "";
  	 		var dataUrl = content.find("#dataUrl").val();
//   	 		alert(dataUrl);
  	 		var title = content.find("#title").val();
  	 		var grid = content.find("#datagrid");
//   	 		queryParams = getParam($("#queryForm"));
  	 		if(reportName == "jituanhuodongfenxi"){
  	 			headUrl = "<%=path%>/chineseActivityReport/findHeaderForJiTuanHuoDongFenXi.do?bdate="+$("#bdat").val();
  	 			$(".year").css("display","none");
				$(".month").css("display","block");
  	 		}else if(reportName == "jituanhuodongfenxibymonth"){
  	 			headUrl="";
  	 			$(".year").css("display","block");
				$(".month").css("display","none");
  			}
			
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp:true,
  	 			excelUrl:excelUrl+'?reportName='+reportName,
  	 			printUrl:printUrl+'?reportName='+reportName,
  	 			colsChooseUrl:'<%=path%>/chineseActivityReport/toColumnsChoose.do?reportName='+reportName,
  	 			toolbar:['search','excel','exit'],
  	 			searchFun:function(){
  	 				grid.datagrid('load',getParam($("#queryForm")));
//   	 				grid.datagrid('load',queryParams);
  	 				initTab(true);
  	 			}
  	 		});

  	 		if(rebuild || grid.css('display') != 'none'){
  	  				firstLoad = true;//重置第一<fmt:message key="secondary" />加载标志
		  	 		builtTable({
		  	 			headUrl:headUrl,
		  	 			dataUrl:dataUrl,
		  	 			title:title,
		  	 			grid:grid,
		  	 			pagination:false,
// 		  	 			showFooter:false,
		  	 			mergeCellsFun:function(data){
		  	 				if(reportName=='jituanhuodongfenxi'){
		  	 					//获取所有的记录数
		  	 					var size = data.rows.length;//
		  	 					//将记录的最后两条数据按列合并
								for (var j=size-2;j <=size;j++){
									//一个月按三十一天算
									for(var i=1;i<=31;i++){
										grid.datagrid('mergeCells', {
					  						index: j,
					  						field: 'CNT'+i,
					  						colspan: 4,
					  						type: 'body'
					  					});
				  	 				}
								}
		  	 				}else {
		  	 					//
		  	 					var size = data.rows.length;
								for (var j=size-2;j <=size;j++){
									//一年按12月算
									for(var i=1;i<=12;i++){
										grid.datagrid('mergeCells', {
					  						index: j,
					  						field: 'CNT'+i,
					  						colspan: 4,
					  						type: 'body'
					  					});
				  	 				}
								}
		  	 				}
			  	 				
		  	 			},
		  	 			//decimal控制保留小数位数 
		  	 			//保留两位小数
		  	 			createHeader:function(data,head,frozenHead){
		  	 				day = data.DAYS;
		  	 				var col1 = [];
		  	 				var col2 = [];
		  	 				//冻结列
		  	 				frozenHead.push([{field:'DX',width:100,title:'<fmt:message key="activity" /><fmt:message key="classification" />',align:'left',rowspan:1},
		  	 								 {field:'WORKDATE',width:120,title:'<fmt:message key="activity" /><fmt:message key="name" />',align:'left',colspan:1,rowspan:1}
// 		  	 								 {field:'WORKDATE1',width:100,title:'开业日',align:'left',rowspan:1}
		  	 								 ]);
							if(reportName=='jituanhuodongfenxi'){
								for ( var index = 1; index <= day+1; index++) {
								    var show="";
									if(index<=9){
										show = "-0" + index;
									}else{
										show = "-" + index;
									}
									col1.push({field:"",title:$("#bdat").val().substring(0,7)+ show,colspan:4,align:'center'});
									col2.push(
											{field:"CNT"+index,width:80,title:'<fmt:message key="quantity" />',colspan:1,align:'right'},
											{field:"HDJS"+index,width:80,title:'<fmt:message key="activity" /><fmt:message key="Income" />',colspan:1,align:'right'},
											{field:"YMJE"+index,width:80,title:'<fmt:message key="Excellent_free" /><fmt:message key="amount" />',colspan:1,align:'right'},
											{field:"YMZB"+index,width:80,title:'<fmt:message key="Excellent_free" /><fmt:message key="accounted_for" />',colspan:1,align:'right'}
									);							
								}
								col1.push({field:"",title:'<fmt:message key="total" />',colspan:3,align:'center'});
								col2.push({field:"CNTHJ",width:80,title:'<fmt:message key="quantity" />',colspan:1,align:'right'});
								col2.push({field:"YMJEHJ",width:80,title:'<fmt:message key="Excellent_free" /><fmt:message key="amount" />',colspan:1,align:'right'});
								col2.push({field:"HDJSHJ",width:80,title:'<fmt:message key="An_activity" />',colspan:1,align:'right'});
								
			  	 				head.push(col1);
			  	 				head.push(col2);
							}else{
								day = 12;
								for ( var index = 1; index < day+1; index++) {
								    var show="";
									if(index<=9){
										show = "-0" + index;
									}else{
										show = "-" + index;
									}
									col1.push({field:"",title:$("#edat").val()+ show,colspan:4,align:'center'});
									col2.push(
											{field:"CNT"+index,width:80,title:'<fmt:message key="quantity" />',colspan:1,align:'right'},
											{field:"HDJS"+index,width:80,title:'<fmt:message key="activity" /><fmt:message key="Income" />',colspan:1,align:'right'},
											{field:"YMJE"+index,width:80,title:'<fmt:message key="Excellent_free" /><fmt:message key="amount" />',colspan:1,align:'right'},
											{field:"YMZB"+index,width:80,title:'<fmt:message key="Excellent_free" /><fmt:message key="accounted_for" />',colspan:1,align:'right'}
									);							
								}
								col1.push({field:"",title:'<fmt:message key="total" />',colspan:3,align:'center'});
								col2.push({field:"CNTHJ",width:80,title:'<fmt:message key="quantity" />',colspan:1,align:'right'});
								col2.push({field:"YMJEHJ",width:80,title:'<fmt:message key="amount" />',colspan:1,align:'right'});
								col2.push({field:"HDJSHJ",width:80,title:'<fmt:message key="An_activity" />',colspan:1,align:'right'});
			  	 				head.push(col1);
			  	 				head.push(col2);
							}
							
		  	 			}
		  	 		});
  	  		}
  	 	}
  	 </script>
  </body>
</html>