<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			var _dat = window.parent.dat;
			if(_dat!=null && _dat!=''){
				$("#bdat").val(_dat);
			}
			// 加载时间选择
			$("#bdat").focus(function(){
	 			new WdatePicker({minDate:new Date().format()});
	 		});
			// 加载界面默认值
			_sel = window.parent.tableList;
			if(_sel!=null){
				for(var s =0;s<_sel.length;s++){
					checkCode(_sel[s]);
				}
			}
			
			var sta = $("#sta").val();
			if(sta =='1'){
				document.getElementsByName("CODETYP").value="1";	
			}else if(sta =='2'){
				document.getElementsByName("CODETYP").value="6";
			}
			
			/*弹出树*/
			$('#seachDept').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#positn').val();
					var defaultName = $('#dept').val();
					//alert(defaultCode+"==="+defaultName);
					var offset = getOffset('positn');
					top.cust('<fmt:message key="please_select_positions" />',encodeURI('<%=path%>/misbohcommon/findPositnSuper.do?typn=7&iffirm=1&mold=one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#dept'),$('#positn'),'760','520','isNull');
				}
			});
			
			setElementHeight('.grid',['.tool'],$(document.body),85);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
		});
		// 获取系统当前时间
		Date.prototype.format = function(){	
			var yy = String(this.getFullYear());
			var mm = String(this.getMonth() + 1);
			var dd = String(this.getDate());
			if(mm<10){
				mm = ''+0+mm;
			}
			if(dd<10){
				dd = ''+0+dd;
			}
			var str = yy+"-"+mm+"-"+dd;
			return str;
		};
		
		
		// 上一步调整 将历史数据显示
		function checkCode(_list){
			var check = $("input[name='CATEGORY_CODE']");
			 for(var c = 0;c<check.length;c++){
				 if(_list.CATEGORY_CODE == $(check[c]).val()){
					 _id = $(check[c]).attr("id");
					 $(check[c]).attr("checked","checked");
					 $("#chk_RECEIVEDATE"+_id).val(_list.RECEIVEDATE);
					 $("#chk_NEXTRECEIVEDATE"+_id).val(_list.NEXTRECEIVEDATE);
					 return;
				 }
			 }
		}
		
		// 设置下次到货日
		function setNEXTRECEIVEDATE(_RECEIVEDATE,_index){
			var _value=$('#'+_RECEIVEDATE).val();
			new WdatePicker({minDate:_value});
			$("#"+_index).attr("checked",true);
		}
		// 设置到货日
		function setRECEIVEDATE(_index){
	 			new WdatePicker({minDate:$('#bdat').val()});
	 			$("#"+_index).attr("checked",true);
		}
		var _sel;
		var _codetyp ='';
		// 获取界面表格显示值
		function getTableList(){
			var isuseschedule = $("input[name='isuseschedule']").val();
			var code = '';
			if(isuseschedule == 'Y'){
				code = $("input[name='CATEGORY_CODE']");
			}else{
				 code = $("input[name='CATEGORY_CODE']:checked");
			}
				var _id;
				_sel = new Array;
				var _rol;
				for(var k = 0;k<code.length;k++){
					_rol = {};
					_id = $(code[k]).attr("id");
					_rol["CATEGORY_CODE"]=$(code[k]).val();
					_rol["RECEIVEDATE"]=$("#chk_RECEIVEDATE"+_id).val();
					_rol["NEXTRECEIVEDATE"]=$("#chk_NEXTRECEIVEDATE"+_id).val();
					_rol["DAYS"]=$("#chk_DAYS"+_id).val();
					_rol["CODETYP"]=$("#chk_CODETYP"+_id).val();
					_rol["DES"]=$("#chk_DES"+_id).attr("title");
					_codetyp = _codetyp +"###"+_rol["CODETYP"];
					if(_rol["RECEIVEDATE"] == '' || _rol["RECEIVEDATE"] == null
							||_rol["NEXTRECEIVEDATE"] == '' || _rol["NEXTRECEIVEDATE"] == null){
						alert($("#chk_DES"+_id).attr("title")+":<fmt:message key='misboh_DeclareGoodsGuide_error3' />");
						return -1;
					}else{
						_sel.push(_rol);
					}
				}
				return 1;
		}
		
		// 获取订货日 
		function getDdat(){
			return $("#bdat").val(); 
		}
		
		// 获取部门
		function getDept(){
			return $("#positn").val(); 
		}
		// 获取报货单据数量控制
		function getFlag(){
			return $("#flag").val(); 
		}
		// 查看配送班表
		var addScheduleMis;
		function getScheduleMis(){
	    	var action = "<%=path%>/scheduleMis/list.do?sta=1";
	    	addScheduleMis= $('body').window({
				id: 'window_addScheduleMis',
				title: '<fmt:message key="The_shipping_schedule" />',
				content: '<iframe id="addscheduleMisFrame" name="addscheduleMisFrame" frameborder="0" src='+action+'></iframe>',
				width: $(document.body).width()-20,//'1000px'
				height: $(document.body).height()-200,
				draggable: true,
				isModal: true,
				confirmClose: false
			});
		}
		function updatDate(_dat){
			 window.parent.dat = _dat;
			$("#mainFrame").submit();
		}
		</script>
	</head>
	<body>
		<form id="mainFrame" method="post" action="<%=path%>/declareGoodsGuide/listRightDeclareGoodsGuide.do">
		<input type="hidden" name ="isdept" value="${isdept}"/> 
		<input type="hidden" name ="flag" id="flag" value="${flag}"/> 
		<input type="hidden" name ="isuseschedule" value="${isuseschedule}"/> 
		<input type="hidden" id="positn" name="positn" value=""/>
		 <div class="moduleInfo">
				<div class="form-line">
					<div class="form-label"><fmt:message key="misboh_DeclareGoodsGuide_date"/>：</div>
					<div class="form-input" style="width:100px;text-align: center;">
						<c:if test="${OrderDayControl == 1 }">
							${bdat}<input  style="width:100px;"  type="hidden" id="bdat" name="bdat"  value="${bdat}" />
						</c:if>
						<c:if test="${OrderDayControl != 1 }">
							<input  style="width:100px;"  type="text" id="bdat" name="bdat" class="Wdate text" value="${bdat}"  onchange="updatDate(this.value)"/>
						</c:if>
						<!-- 判断是否启用配送班表   Y 启用  N 禁用 -->
					</div>
					<div class="form-input" style="width:80px;bottom: -3px;">
						<c:if test ="${isuseschedule == 'Y'}">
								<input type="button" value="<fmt:message key="misboh_DeclareGoodsGuide_ckpsbb"/>" onclick="getScheduleMis()"/>
						</c:if>
					</div>
				<c:if test ="${isdept == 'Y'}">
						<div class="form-label"><fmt:message key="sector"/>：</div>
						<div class="form-input" style="width:205px;">
							<input style="width:95px;" type="text" id="dept" name="dept"/>
							<img id="seachDept" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="misboh_DeclareGoodsGuide_dept" />' />
						</div>
				</c:if>
				</div>
			</div>
			<div class="grid">
				<div class="table-head">
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:15px;"></span></td>
								<td><span style="width:240px;"><fmt:message key="Daily_goods_category"/></span></td>
								<td><span style="width:150px;"><fmt:message key="arrival_date1"/></span></td>
								<td><span style="width:150px;"><fmt:message key="misboh_DeclareGoodsGuide_nextArrival_date"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" id="CategoryTable">
						<tbody>
							<!-- 是否启用配送班表   N 禁用 Y 启用 -->
							<c:if  test ="${isuseschedule =='Y' }">
								<c:forEach var="list" items="${categoryList}" varStatus="sta">
									<tr>
										<td><span  style="width:15px;"> 
												<input type="checkbox"  name="CATEGORY_CODE"
												<c:if test="${list.NEXTRECEIVEDATE !=null }">
													checked="checked" 
												</c:if>
													id="${sta.index}"  value="${list.CATEGORY_CODE}" 
												 disabled="disabled"
												 /></span> 
										</td>
										<td><span title="${list.DES}" id="chk_DES${sta.index}"  style="width:240px;">${list.DES}&nbsp;
										</span></td>
										<td><span title="${list.ReceiveDate}" style="width:150px;">${list.RECEIVEDATE}
											<input  type="hidden" id="chk_RECEIVEDATE${sta.index}" name="RECEIVEDATE" value="${list.RECEIVEDATE}" />
										</span></td>
										<td><span title="${list.nextReceiveDate}" style="width:150px;">${list.NEXTRECEIVEDATE}
											<input  type="hidden" id="chk_NEXTRECEIVEDATE${sta.index}" name="NEXTRECEIVEDATE" value="${list.NEXTRECEIVEDATE}" />
											<input  type="hidden" id="chk_CODETYP${sta.index}" name="CODETYP" value="${list.CODETYP}" />
											<input  type="hidden" id="chk_DAYS${sta.index}" name="DAYS" value="${list.DAYS}" />
										</span></td>
									</tr>
								</c:forEach>
							</c:if>
							<c:if  test ="${isuseschedule !='Y' }">
								<c:forEach var="list" items="${categoryList}" varStatus="sta">
									<tr>
										<td><span  style="width:15px;"> 
											<input type="checkbox"
												 name="CATEGORY_CODE" id="${sta.index}" value="${list.CATEGORY_CODE}"/></span> 
												 <input  type="hidden" id="chk_CODETYP${sta.index}" name="CODETYP" value="${list.CODETYP}" />
												 <input  type="hidden" id="chk_DAYS${sta.index}" name="DAYS" value="${list.DAYS}" />
										</td>
										<td><span title="${list.DES}" style="width:240px;" id="chk_DES${sta.index}">${list.DES}&nbsp;
										</span></td>
										<td><span title="${list.ReceiveDate}" style="width:150px;">
											<input  type="text" id="chk_RECEIVEDATE${sta.index}"  style="width:120px;" class="Wdate text" name="RECEIVEDATE"   onclick="setRECEIVEDATE('${sta.index}')" onfocus="setRECEIVEDATE('${sta.index}')" />
										</span></td>
										<td><span title="${list.nextReceiveDate}" style="width:150px;">
											<input  type="text" id="chk_NEXTRECEIVEDATE${sta.index}"  onclick="setNEXTRECEIVEDATE('chk_RECEIVEDATE${sta.index}','${sta.index}')" onfocus="setNEXTRECEIVEDATE('chk_RECEIVEDATE${sta.index}','${sta.index}')" style="width:120px;" class="Wdate text" name="NEXTRECEIVEDATE"  />
										</span></td>
									</tr>
								</c:forEach>
							</c:if>
						</tbody>
					</table>
				</div>
			</div>
			</form>
	</body>
</html>