<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			var bool = 1;
			// 设置界面跳转
			function setChecked(index){
				/*$("input[id='chk_"+index+"'").each(function(){
					$(this).attr("checked", true);
				});*/
				if(index==null || index =='undefined'){
					index = 0;
				}
				var v = $("input[name='idList']");
				v[index].checked = true;
				if(index == (v.length-1)){
					bool = 2;
				}else{
					bool = 1;
				}
				return v[index].value;
			}
			function getSta(){
				return $("#sta").val(); 
			}
			
			$(document).ready(function(){
				setChecked(window.parent._index);
			});
			
			function setLeftFrom(_codetyp,url){
				$("#codetyp").val(_codetyp);
				$("#leftForm").submit();
			}
		</script>
	</head>
	<body>
	<form id="leftForm" method="post" action="<%=path%>/declareGoodsGuide/listLeftDeclareGoodsGuide.do">
			<input name="codetyp" id="codetyp" type="hidden"/>
			<input name="sta" id="sta" type="hidden" value="${sta}"/>
			<div class="grid" style="width: 100%;">
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="list" items="${leftList}" varStatus="sta">
								<tr>
									<td><span style="width:25px;">
											<input type="radio" 
										<c:if test="${sta.index == 0 }">
												checked="checked" 
										</c:if>
											 name="idList" id="chk_${sta.index}" value="${list[1]}" disabled="disabled"/></span> 
									</td>
									<td><span title="${list[0]}" style="width:108px;">${list[0]}&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
	</form>
	</body>
</html>