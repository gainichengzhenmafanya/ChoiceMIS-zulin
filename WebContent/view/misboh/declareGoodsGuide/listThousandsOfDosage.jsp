<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		
		$(document).ready(function(){
			loadDate();
			load();
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),85);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
		});
		
		// 加载日期选择控件
		function loadDate(){
			$("#bThousandsDat,#eThousandsDat").focus(function(){
	 			new WdatePicker();
	 		});
		}
		var thousList = new Array;
		var declareGoods;
		function load(){
			thousList = new Array;
			declareGoods = window.parent.declareGoods;
			var plantyp = $('#plantyp').val();
			var tablewidth = (window.frames["mainForm"].scrollWidth - 400)/3;
			var show ='<div class="table-head">'+
			'<table cellspacing="0" cellpadding="0">'+
			'<thead> <tr>'+
					'<td><span style="width:100px;"><fmt:message key="supplies_code"/></span></td>'+
					'<td><span style="width:150px;"><fmt:message key="supplies_name"/></span></td>'+
					'<td><span style="width:60px;"><fmt:message key="supplyunit"/></span></td>';
			if(plantyp =='PAX'){
				show = show+'<td><span style="width:'+tablewidth+'px;"><fmt:message key="misboh_DeclareGoodsGuide_ThousandsPeopleOfDosage"/></span></td>'+
				'<td><span style="width:'+tablewidth+'px;"><fmt:message key="misboh_DeclareGoodsGuide_ThousandsPeopleOfDosage_edit"/></span></td>'+
				'<td><span style="width:'+tablewidth+'px;"><fmt:message key="misboh_DeclareGoodsGuide_Periodic_quantity"/></span></td>';
			}else if(plantyp =='AMT'){
				show = show+'<td><span style="width:'+tablewidth+'px;"><fmt:message key="misboh_DeclareGoodsGuide_ThousandsYuanOfDosage"/></span></td>'+
				'<td><span style="width:'+tablewidth+'px;"><fmt:message key="misboh_DeclareGoodsGuide_ThousandsYuanOfDosage_edit"/></span></td>'+
				'<td><span style="width:'+tablewidth+'px;"><fmt:message key="misboh_DeclareGoodsGuide_Periodic_quantity"/></span></td>';
			}else if(plantyp =='TC'){
				show = show+'<td><span style="width:'+tablewidth+'px;"><fmt:message key="misboh_DeclareGoodsGuide_ThousandsTimeOfDosage"/></span></td>'+
				'<td><span style="width:'+tablewidth+'px;"><fmt:message key="misboh_DeclareGoodsGuide_ThousandsTimeOfDosage"/></span></td>'+
				'<td><span style="width:'+tablewidth+'px;"><fmt:message key="misboh_DeclareGoodsGuide_Periodic_quantity"/></span></td>';
			}
			show = show+'</tr></thead></table></div>';
			var thousandsList = declareGoods.thousandsList;
			var _oldThousandsList = declareGoods.thousList;
			if(_oldThousandsList!=null){
					var _thous = {};
					show = show+'<div class="table-body"> <table cellspacing="0" cellpadding="0"> <tbody>';
						for(var t=0;t<_oldThousandsList.length;t++){
							 _thous = {};
							show=show+' <tr>';
							show=show+'<td><span style="width:100px;">'+_oldThousandsList[t].SPCODE+'</span></td>';
							show=show+'<td><span style="width:150px;">'+_oldThousandsList[t].SPNAME+'</span></td>';
							show=show+'<td><span style="width:60px;">'+_oldThousandsList[t].UNIT+'</span></td>';
							show=show+'<td><span style="width:'+tablewidth+'px;text-align: right;" align="right">'+_oldThousandsList[t].OLDTHOUSCNT+'</span></td>';
							show=show+'<td><span style="width:'+tablewidth+'px;text-align: right;" align="right">';
							show=show+'<input style="width:'+tablewidth+'px;text-align:right;" pattern="[0-9]" onkeyup="eqEnter('+t+','+(_oldThousandsList.length-1)+')" onblur="gclick('+_oldThousandsList[t].SPCODE+',this.value,'+t+')" type="text" id="dz'+t+'" name="dz" value="'+_oldThousandsList[t].THOUSCNT+'"/>';
							show=show+'</span></td>';
							show=show+'<td><span id = "zq'+t+'" style="width:'+tablewidth+'px;text-align: right;" align="right">'+_oldThousandsList[t].ZQCNT+'</span></td>';
							show=show+' </tr>';
							_thous["SPCODE"]=_oldThousandsList[t].SPCODE;
							_thous["SPNAME"]=_oldThousandsList[t].SPNAME;
							_thous["UNIT"]=_oldThousandsList[t].UNIT;
							_thous["UNITPER"]=_oldThousandsList[t].UNITPER;
							_thous["DAILYGOODS_TYP"]=_oldThousandsList[t].DAILYGOODS_TYP;
							_thous["OLDTHOUSCNT"]=_oldThousandsList[t].OLDTHOUSCNT;
							_thous["THOUSCNT"]="dz"+t;
							_thous["ZQCNT"]="zq"+t;
							thousList.push(_thous);
							
						}
					show = show+'</tbody></table></div>';
			}else{
				if(thousandsList!=null){
					var _thous = {};
					show = show+'<div class="table-body"> <table cellspacing="0" cellpadding="0"> <tbody>';
						for(var t=0;t<thousandsList.length;t++){
							 _thous = {};
							show=show+' <tr>';
							show=show+'<td><span style="width:100px;">'+thousandsList[t][0]+'</span></td>';
							show=show+'<td><span style="width:150px;">'+thousandsList[t][1]+'</span></td>';
							show=show+'<td><span style="width:60px;">'+thousandsList[t][2]+'</span></td>';
							show=show+'<td><span style="width:'+tablewidth+'px;text-align: right;" align="right">'+thousandsList[t][3]+'</span></td>';
							show=show+'<td><span style="width:'+tablewidth+'px;text-align: right;" align="right">';
							show=show+'<input style="width:'+tablewidth+'px;text-align:right;" pattern="[0-9]" onkeyup="eqEnter('+t+','+(thousandsList.length-1)+')" onblur="gclick('+thousandsList[t][0]+',this.value,'+t+')" type="text" id="dz'+t+'" name="dz" value="'+thousandsList[t][4]+'"/>';
							show=show+'</span></td>';
							show=show+'<td><span id = "zq'+t+'" style="width:'+tablewidth+'px;text-align: right;" align="right">'+thousandsList[t][5]+'</span></td>';
							show=show+' </tr>';
							_thous["SPCODE"]=thousandsList[t][0];
							_thous["SPNAME"]=thousandsList[t][1];
							_thous["UNIT"]=thousandsList[t][2];
							_thous["UNITPER"]=thousandsList[t][6];
							_thous["OLDTHOUSCNT"]=thousandsList[t][3];
							_thous["DAILYGOODS_TYP"]=thousandsList[t][7];
							_thous["THOUSCNT"]="dz"+t;
							_thous["ZQCNT"]="zq"+t;
							thousList.push(_thous);
						}
					show = show+'</tbody></table></div>';
				}
			}
			$('#show').html(show);
			$('#bThousandsDat').val(declareGoods.bThousandsDat);
			$('#eThousandsDat').val(declareGoods.eThousandsDat);
		}
		function calculate(){
			var param = {};
			param['tableList'] = window.parent.tableList;
			param['bThousandsDat'] = $('#bThousandsDat').val();
			param['eThousandsDat'] = $('#eThousandsDat').val();
			param['dept'] = window.parent.dept;
			param['dat'] = window.parent.dat;
			param['sta'] = window.parent.sta;
			param['salePlanList'] = declareGoods.salePlanList;
			$.post('<%=path%>/declareGoodsGuide/calculationThousands.do',param,function(data){
				window.parent.declareGoods = data;
				load();
			});
			
		}
		function getThousands(){
			for(var sa=0;sa<thousList.length;sa++){
				thousList[sa].THOUSCNT=	$('#'+thousList[sa].THOUSCNT).val();
				thousList[sa].ZQCNT=	$('#'+thousList[sa].ZQCNT).text();
			}
		}
		
		function gclick(_code,_value,_index){
			var declareMap = declareGoods.declareMap[_code];
			if(declareMap!=null){
				declareMap.estimatedParameters = _value;
				$("#zq"+_index).text(Number(declareMap.estimatedParameters*declareMap.forecastMoney).toFixed(declareGoods.moneyLength));
			}
		}
		
		function eqEnter(_index,_max){
			if(event.keyCode == 13){
				if(_index == _max){
					_index = 0;
				}else{
					_index ++;
				}
				$('#dz'+_index).focus();
			}
		}
			
		</script>
	</head>
	<body>
		<form id="mainForm" method="post" action="">
		 <div class="moduleInfo">
		 	<span style="width:180px;">
				<fmt:message key="startdate"/>：
				<input style="width:100px;"  type="text" id="bThousandsDat" name="bThousandsDat" class="Wdate text" />
				<input style="width:100px;"  type="hidden" id="plantyp" name="plantyp"  value="${plantyp}" />
			</span>
			<span style="width:180px;">
				<fmt:message key="enddate"/>：
				<input  style="width:100px;"  type="text" id="eThousandsDat" name="eThousandsDat" class="Wdate text"/>
			</span>
			<span style="width:50px;">
				<input type="button" value="  <fmt:message key="calculate"/>  " onclick="calculate()"/>
			</span>
		</div>
			<div class="grid" id="show">
			</div>
			</form>
	</body>
</html>