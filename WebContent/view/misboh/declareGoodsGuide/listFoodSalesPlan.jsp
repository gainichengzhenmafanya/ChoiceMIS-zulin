<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		
		$(document).ready(function(){
			loadDate();
		});
		
		// 加载日期选择控件
		function loadDate(){
			$("#bdat").focus(function(){
	 			new WdatePicker();
	 		});
		}
		
		
		var addScheduleMis;
		// 查看配送班表
		function getScheduleMis(){
	    	var action = "<%=path%>/scheduleMis/list.do?sta=1";//调用模板的时候传申购门店
	    	addScheduleMis= $('body').window({
				id: 'window_addScheduleMis',
				title: '报货单模板',
				content: '<iframe id="addscheduleMisFrame" name="addscheduleMisFrame" frameborder="0" src='+action+'></iframe>',
				width: $(document.body).width()-20,//'1000px'
				height: '620px',
				draggable: true,
				isModal: true,
				confirmClose: false
			});
		}
		
		</script>
	</head>
	<body>
		<form id="mainFrame" method="post" action="">
		 <div class="moduleInfo">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td align="right">
							 	<fmt:message key="misboh_DeclareGoodsGuide_date"/>：
							</td>
							<td align="left" style="width:100px;">
							 	<input  style="width:100px;"  type="text" id="bdat" name="bdat" class="Wdate text" value="${bdat}" />
							</td>
							<td align="left">
							 	<input type="button" value="<fmt:message key="misboh_DeclareGoodsGuide_ckpsbb"/>" onclick="getScheduleMis()"/>
							</td>
						</tr>
						<tr>
							<td align="right">
							 	<fmt:message key="sector"/>：
							</td>
							<td align="left" style="width:100px;">
							 	<input style="width:95px;" type="text" id="enddate" name="enddate"/>
								<img id="seachDept" class="search" src="<%=path%>/image/themes/icons/search.png" alt='查询部门' />
							</td>
							<td align="left">
							</td>
						</tr>
						<tr>  
							<td height="3"></td>
						</tr>   
					</table>
			</div>
			<div class="grid" style="width: 100%;">
				<div class="table-head">
					<table cellspacing="0" cellpadding="0"  style="width: 100%;">
						<thead>
							<tr>
								<td><span style="width:10px;"></span></td>
								<td><span style="width:300px;"><fmt:message key="Daily_goods_category"/></span></td>
								<td><span style="width:120px;"><fmt:message key="arrival_date1"/></span></td>
								<td><span style="width:120px;"><fmt:message key="misboh_DeclareGoodsGuide_nextArrival_date"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0"  style="width: 100%;">
						<tbody>
								<c:forEach var="list" items="${categoryList}" varStatus="sta">
								<tr>
									<td><span style="width:10px;">
											<input type="checkbox" 
											<c:if test="${list.nextReceiveDate !=null }">
												checked="checked" 
											</c:if>
											 name="Category_Code" id="chk_Category_Code${sta.index}" value="${list.Category_Code}" disabled="disabled"/>&nbsp;</span> 
									</td>
									<td><span title="${list.DES}"  style="width:300px;">${list.DES}&nbsp;
									</span></td>
									<td><span title="${list.ReceiveDate}" style="width:120px;">${list.RECEIVEDATE}
										<input  type="hidden" id="chk_RECEIVEDATE${sta.index}" name="RECEIVEDATE" value="${list.RECEIVEDATE}" />
									</span></td>
									<td><span title="${list.nextReceiveDate}" style="width:120px;">${list.NEXTRECEIVEDATE}
										<input  type="hidden" id="chk_NEXTRECEIVEDATE${sta.index}" name="NEXTRECEIVEDATE" value="${list.NEXTRECEIVEDATE}" />
										<input  type="hidden" id="chk_CODETYP${sta.index}" name="CODETYP" value="${list.CODETYP}" />
										<input  type="hidden" id="chk_DAYS${sta.index}" name="DAYS" value="${list.DAYS}" />
									</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			</form>
	</body>
</html>