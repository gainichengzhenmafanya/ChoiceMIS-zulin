<%@page import="com.choice.misboh.commonutil.DateJudge"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			load();
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),85);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
		});
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		var saleList = new Array;
		var declareGoods=null;
		// 加载表格显示值
		function load(){
			var old_saleList =  null
			if(window.parent.declareGoods != null){
				old_saleList = window.parent.declareGoods.salePlanList;
			}
			var salePlanList = window.parent.salePlanList;
			var width = window.frames["mainForm"].scrollWidth;
			var show='<div class="table-head">'+
				'<table cellspacing="0" cellpadding="0">'+
				'<thead>'+
				'<tr>'+
				'<td><span style="width:100px;"><fmt:message key="business_day"/></span></td>'+
				'<td><span style="width:50px;"><fmt:message key="week"/></span></td>'+
				'<td><span style="width:100px;"><fmt:message key="misboh_DeclareGoodsGuide_yg1" /></span></td>'+
				'<td><span style="width:100px;"><fmt:message key="misboh_DeclareGoodsGuide_yg2" /></span></td>'+
				'</tr>'+
				'</thead>'+
				'</table>'+
				'</div>'+
				'<div class="table-body">'+
				'<table cellspacing="0" cellpadding="0">'+
				'<tbody>';
				if(old_saleList!=null){
					var _val='';
					var _sale = {};
					for(var s=0;s<old_saleList.length;s++){
						_sale = {};
						show=show+'<tr><td><span style="width:100px;">'+old_saleList[s].DAT+'</span></td>'+
						'<td><span style="width:50px;">'+old_saleList[s].WEEK+'</span></td>'+
						'<td><span style="width:100px;text-align: right;" align="right">'+Number(old_saleList[s].SALEVALUE).toFixed(2)+'</span></td>'+
						'<td><span style="width:100px;">'+
						'<input style="width:100px;text-align:right;" pattern="[0-9]"  type="text" id="dz'+s+'" name="dz" value="'+Number(old_saleList[s].SALEVALUE).toFixed(2)+'"/>'+
						'</span></td>'+
						'</tr>';
						_sale['DAT']=old_saleList[s].DAT;
						_sale['WEEK']=old_saleList[s].WEEK;
						_sale['SALEVALUE']='dz'+s;
						saleList.push(_sale);
					}
				}else{
					if(salePlanList!=null){
						var _val='';
						var _sale = {};
						for(var s=0;s<salePlanList.length;s++){
							_sale = {};
							if($("#plantyp").val()=='PAX'){
								_val=salePlanList[s].PEOPLE_T;
							}else if($("#plantyp").val()=='AMT'){
								_val=salePlanList[s].MONEY_T;
							}else if($("#plantyp").val()=='TC'){
								_val=salePlanList[s].BILLS_T;
							}
							show=show+'<tr><td><span style="width:100px;">'+salePlanList[s].DAT+'</span></td>'+
							'<td><span style="width:50px;">'+salePlanList[s].WEEK+'</span></td>'+
							'<td><span style="width:100px;text-align: right;" align="right">'+_val+'</span></td>'+
							'<td><span style="width:100px;">'+
							'<input style="width:100px;text-align:right;" pattern="[0-9]"  type="text" id="dz'+s+'" name="dz" value="'+_val+'"/>'+
							'</span></td>'+
							'</tr>';
							_sale['DAT']=salePlanList[s].DAT;
							_sale['SALEVALUE']='dz'+s;
							_sale['WEEK']=salePlanList[s].WEEK;
							saleList.push(_sale);
						}
					}
				}
				
				$('#estimated').html(show+'</tbody></table></div>');
		}
		function getSalePlanValue(){
			var param = {};
			var bool = 0;
			param['tableList'] = window.parent.tableList;
			param['dat'] = window.parent.dat;
			param['dept'] = window.parent.dept;
			param['sta'] = window.parent.sta;
			for(var sa=0;sa<saleList.length;sa++){
				saleList[sa].SALEVALUE=	$('#'+saleList[sa].SALEVALUE).val();
			}
			param['salePlanList'] = saleList;
			$.post('<%=path%>/declareGoodsGuide/calculationThousands.do',param,function(data){
				declareGoods = data;
				declareGoods.saleList = saleList;
				bool = 1;
			});
			declareGoods.saleList = saleList;
			return bool;
		}
		
		</script>
		
	</head>
	<body>
		<form id="mainForm" method="post" action="">
		<input type="hidden" id="plantyp" value="${plantyp}" />
		 <div class="moduleInfo">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td align="left" style="height:5px;">
								<span style="height:10px;">预估单位：
								<c:if test="${plantyp=='PAX'}">
									千人
								</c:if>
								<c:if test="${plantyp=='AMT'}">
									千元
								</c:if>
								<c:if test="${plantyp=='TC'}">
									千次
								</c:if>
								</span>
							</td>
						</tr>
					</table>
			</div>
			<div class="grid" id="estimated">
			</div>
			</form>
	</body>
</html>