<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="misboh_DeclareGoodsGuide"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<style type="text/css">
			.moduleInfo {
				//background-color: #E1E1E1;
			}
			.leftFrame{
				width:20%;
			}
			.mainFrame{
				width:80%;
			}
		</style>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				loadBtn(false,true,false);
			});
			//ajax同步设置
			$.ajaxSetup({
				async: false
			});
			var _index = 0;
			function loadBtn(last,next,_ok){
				$('.tool').text("");
				$('.tool').toolbar({
					items: [{
							text: '<fmt:message key="misboh_DeclareGoodsGuide_last" />',
							title: '<fmt:message key="misboh_DeclareGoodsGuide_last" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&last,
							handler: function(){
								$("#wait2").css("visibility","visible");
								$("#wait").css("visibility","visible");
								_index = _index-1;
								var url = window.frames["leftFrame"].setChecked(_index);
								window.frames["mainFrame"].location= "<%=path%>"+url;
								//按钮控制
								if(_index==0){
									loadBtn(false,true,false);
								}else{
									loadBtn(true,true,false);
								}
								stopVisibility();
							}
						},
						{
							text: '<fmt:message key="misboh_DeclareGoodsGuide_next" />',
							title: '<fmt:message key="misboh_DeclareGoodsGuide_next" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&next,
							handler: function(){
								// 执行点击下一步参数
								generateReport();
							}
						},{
							text: '<fmt:message key="confirm" />',
							title: '<fmt:message key="confirm" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&_ok,
							handler: function(){	
								confirmation();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							useable:true,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}
					]
				});
			}
			var tableList;
			var dat;
			var dept;
			var declareGoods;
			var sta;
			var flag;
			// 调整 生成报货单界面
			function generateReport(){
				$("#wait2").css("visibility","visible");
				$("#wait").css("visibility","visible");
				var url = window.frames["mainFrame"].location.href;
				sta = window.frames["leftFrame"].getSta();
				// 验证下一步操作是否调整预估值
				if(url.indexOf('listRightDeclareGoodsGuide.do')!= -1){
					//校验订货单报货次数
					flag = window.frames["mainFrame"].getFlag();
					if(flag !='-1'){
						alert("以下报货类别已存在报货单"+flag+",请在报货单填制中查询！");
						stopVisibility();
						return;
					}
					dat = window.frames["mainFrame"].getDdat();
					// 验证订货时间
					if(checkOrderTime()){
						stopVisibility();
						return;
					}
					// 获取表格显示数据
					var _sta = window.frames["mainFrame"].getTableList();
					tableList = window.frames["mainFrame"]._sel;
					dept = window.frames["mainFrame"].getDept();
					// 验证数据是否正确
					if(_sta == 1){
						if(tableList==null || tableList.length <= 0 ||tableList == 'undefined'){
							stopVisibility();
							alert('<fmt:message key="misboh_DeclareGoodsGuide_The_current_order" />【'+dat+'】<fmt:message key="misboh_DeclareGoodsGuide_The_current_order1" />');
							return;
						}
					}else{
						stopVisibility();
						//  alert('<fmt:message key="misboh_DeclareGoodsGuide_The_current_order" />【'+dat+'】<fmt:message key="misboh_DeclareGoodsGuide_The_current_order1" />');
						return;
					}
					// 验证是否安全库存
					if(sta == '3' || sta == '4'){//如果是安全库存报货或者周期平均报货
						stopVisibility();
						execLoad();
						return;
					}
					// 验证是否存在营业额预估
					if(checkSalePlan(tableList)){
						stopVisibility();
						return ;
					};
					//刷新界面
					window.frames["leftFrame"].setLeftFrom(window.frames["mainFrame"]._codetyp);
					//window.frames["leftFrame"].codetyp.value=window.frames["mainFrame"]._codetyp;
					//window.frames["leftFrame"].leftForm.action = "<%=path%>/declareGoodsGuide/listLeftDeclareGoodsGuide.do";
					//window.frames["leftFrame"].leftForm.submit();
				}else if(url.indexOf('listAdjustTheEstimated.do')!= -1){
					var _sta = window.frames["mainFrame"].getSalePlanValue();
					if(_sta == 0){
						stopVisibility();
						return;
					}
					 declareGoods = window.frames["mainFrame"].declareGoods;
				}else if(url.indexOf('listThousandsOfDosage.do')!=-1){
					window.frames["mainFrame"].getThousands();
					declareGoods.thousList = window.frames["mainFrame"].thousList;
				}else if(url.indexOf('/forecastMis/calculate.do')!= -1 || url.indexOf('/forecastMis/list.do')!= -1 ){
				 	var bool = window.frames["mainFrame"].saveUpdate();
				 	if(!bool){
				 		stopVisibility();
				 		return;
				 	}
				}else if(url.indexOf('/forecastMis/planList.do')!= -1 || url.indexOf('/forecastMis/calPlan.do')!= -1){
					var bool = window.frames["mainFrame"].saveUpdate();
				 	if(!bool){
				 		stopVisibility();
				 		return;
					}
				}
				// 执行界面调整 与按钮控制
				execLoad();
				stopVisibility();
			}
			function stopVisibility(){
				$("#wait2").attr("style","visibility:hidden;");
				$("#wait").attr("style","visibility:hidden;");
			}
			var yg = 0;
			function execLoad(){
				_index = _index+1;
				var url = window.frames["leftFrame"].setChecked(_index);
				if(_index == 1 && (window.frames["mainFrame"]._codetyp == '###8' || window.frames["mainFrame"]._codetyp == '###4')){//如果是报货向导，并且是安全库存或者周期平均报货的
					url = "/declareGoodsGuide/generateReportList.do";
					var error='';
					for(var k=0;k<tableList.length;k++){
						error= error+tableList[k].CATEGORY_CODE+',';
					}
					loadBtn(true,false,true);
					 $("#errorLog").val(error);
					 window.frames["mainFrame"].location = "<%=path%>"+url+"?errorLog="+error;
					 return;
				}
				// 按钮控制
				if(window.frames["leftFrame"].bool==2){
					var error='';
					for(var k=0;k<tableList.length;k++){
						error= error+tableList[k].CATEGORY_CODE+',';
					}
					loadBtn(true,false,true);
					 $("#errorLog").val(error);
					 window.frames["mainFrame"].location = "<%=path%>"+url+"?errorLog="+error;
					
				}else{
					loadBtn(true,true,false);
					if(url.indexOf('/forecastMis/list.do')!= -1){ // 菜品点击率
						window.frames["mainFrame"].location="<%=path%>"+url;
						window.frames["mainFrame"].$("#listForm").submit();
					}else if(url.indexOf('/forecastMis/planList.do')!= -1){// 菜品销售计划
						if(dept ==null || dept == 'undefined'){
							dept = '';
						}
						 window.frames["mainFrame"].location="<%=path%>"+url+"&bdate="+declareGoods.dat+"&edate="+declareGoods.useMaxEndDate+"&dept="+dept;
						 yg = 1;
					}else{
						window.frames["mainFrame"].location="<%=path%>"+url+"?sta="+$("#sta").val();
					}
				}
			}			
			// 检测预估营业额
			var salePlanList;
			function checkSalePlan(){
				var param = {};
				var bool = false;
				param['tableList'] = tableList;
				param['dat'] = dat;
				$.post('<%=path%>/declareGoodsGuide/checkSalePlan.do',param,function(data){
					var errorLog = data.errorLog;
					if(errorLog!=null && errorLog!=''){
						alert('<fmt:message key="misboh_DeclareGoodsGuide_salePlanError"/>'+errorLog);
						bool = true;
					}
					salePlanList = data.salePlanList;
				});
				return bool;
			}
			// 检测报货时间
			function checkOrderTime(){
				var param = {};
				var bool = false;
				param['dat'] = dat;
				$.post('<%=path%>/declareGoodsGuide/eqOrderTime.do',param,function(data){
					var errorLog = data.errorLog;
					if(errorLog!=null && errorLog!='1'){
						alert('<fmt:message key="misboh_DeclareGoodsGuide_orderTimeError"/>【'+errorLog+'】<fmt:message key="misboh_DeclareGoodsGuide_orderTimeError1"/>');
						bool = true;
					}
				});
				return bool;
			}
			
			//确认生成订货单
			function confirmation(){
				$("#wait2").css("visibility","visible");
				$("#wait").css("visibility","visible");
				window.frames["mainFrame"].getOrdrSupply();
				var param = {};
				if(declareGoods){
					param['bThousandsDat'] = declareGoods.bThousandsDat;
					param['eThousandsDat'] = declareGoods.eThousandsDat;
					param['salePlanList'] = declareGoods.salePlanList;
				}
				param['tableList'] = tableList;
				param['dept'] = dept;
				param['dat'] = dat;
				param['sta'] = sta;
				param['typList'] = window.frames["mainFrame"].typList;
				$.post('<%=path%>/declareGoodsGuide/confirmationSupplyOrder.do',param,function(data){
					if(data!=null && data.errorLog !=null && data.errorLog == 'OK'){
						alert("<fmt:message key="misboh_DeclareGoodsGuide_error1"/>");
						window.location="<%=path%>/declareGoodsGuide/findDeclareGoodsGuide.do?sta="+sta;
					}else{
						alert("<fmt:message key="misboh_DeclareGoodsGuide_error2"/> ");
					}
					stopVisibility();
				});
			}
		</script>
	</head>
	<body>
		<div class="tool">
		</div>
		<input type="hidden" id="errorLog" name="errorLog" />
		<div class="leftFrame">
			<iframe src="<%=path%>/declareGoodsGuide/listLeftDeclareGoodsGuide.do?sta=${sta}" frameborder="0" name="leftFrame" id="leftFrame"></iframe>
    	</div>
    	<div class="mainFrame">
			<iframe src="<%=path%>/declareGoodsGuide/listRightDeclareGoodsGuide.do?sta=${sta}" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
    	</div>
	</body>
</html>