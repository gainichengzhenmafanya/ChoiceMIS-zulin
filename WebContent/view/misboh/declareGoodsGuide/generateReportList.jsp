<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
				<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<style type="text/css">
			#tblGrid td{
				border-right: 1px solid #999999;
				border-bottom:1px solid #999999; 
			}
		</style>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript">
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		//订货向导对象
		var declareGoods;
		// 保存界面绘画JSP界面
		var showdata = new Array;
		$(document).ready(function(){
			declareGoods = window.parent.declareGoods;
			load();
			 $('#tabs1').tabs( {onSelect:function(){
				 loadTabs();
			 }});
		});

		// 界面加载
		function load(){
			var show = '';
			var param = {};
			param['tableList'] = window.parent.tableList;
			param['dept'] = window.parent.dept;
			param['dat'] = window.parent.dat;
			if(declareGoods){
				param['sta'] = declareGoods.sta;
				param['salePlanList'] = declareGoods.salePlanList;
				if(declareGoods.thousList != null){
					param['thousList'] = declareGoods.thousList;
				}
			}else{
				param['sta'] = window.parent.sta;
			}
			$.post('<%=path%>/declareGoodsGuide/calculationSupplyOrder.do',param,function(data){
					$(".grid").each(function(){
						var _id = this.id;
						// 确认是否安全库存报货界面加载
						if(window.parent.sta == 3 || window.parent.sta == 4){
							editHtml2(data,_id);
						}else if(window.parent.sta == 0){
							if($("#s"+_id).val() == 4 || $("#s"+_id).val() == 8){
								editHtml2(data,_id);								
							}else{
								editHtml(data,_id);
							}
						}else{
							editHtml(data,_id);
						}
				});
			});
		}
	
		// 页面加载
		function editHtml(_data,_id){
				var supplyordr = _data.supplyordr;
				var tablewidth = (window.frames["mainForm"].scrollWidth - 680);
				var showTitle = '<div class="table-head">'+
				'<table cellspacing="0" cellpadding="0">'+
				'<thead> <tr> <td><span style="width:100px;"><fmt:message key="supplies_code" /></span></td>'+
						'<td><span style="width:150px;"><fmt:message key="supplies_name" /></span></td>'+
						'<td><span style="width:60px;"><fmt:message key="supplyunit" /></span></td>'+
						'<td><span style="width:80px;">最小报货量</span></td>'+
						'<td><span style="width:80px;"><fmt:message key="misboh_DeclareGoodsGuide_no1" /></span></td>'+
						'<td><span style="width:80px;"><fmt:message key="misboh_DeclareGoodsGuide_no2" /></span></td>'+
						'<td><span style="width:80px;"><fmt:message key="misboh_DeclareGoodsGuide_no3" /></span></td>'+
						'<td><span style="width:80px;"><fmt:message key="misboh_DeclareGoodsGuide_no4" /></span></td>'+
					'</tr> </thead> </table> </div>'+
					'<div class="table-body">'+
					'<table cellspacing="0" cellpadding="0">'+
						'<tbody>';
				if(supplyordr[_id]!=null && supplyordr[_id].length>0){
					for(var s =0;s<supplyordr[_id].length;s++){
						showTitle = showTitle+'<tr>'+
							'<td><span style="width:100px;">'+supplyordr[_id][s].SPCODE+
							'</span></td>'+
							'<td><span style="width:150px;">'+supplyordr[_id][s].SPNAME+'</span></td>'+
							'<td><span style="width:60px;">'+supplyordr[_id][s].UNIT+'</span></td>'+
							'<td><span style="width:80px;text-align: right;" align="right">'+supplyordr[_id][s].MINCNT+'</span></td>'+
							'<td><span style="width:80px;text-align: right;" align="right">'+supplyordr[_id][s].SPWAY+'</span></td>'+
							'<td><span style="width:80px;text-align: right;" align="right">'+supplyordr[_id][s].SPNOW+'</span></td>'+
							'<td><span style="width:80px;text-align: right;" align="right">'+supplyordr[_id][s].SPUSE+'</span></td>'+
							'<td><span style="width:80px;">'+
							'<input style="width:80px;text-align:right;"  onkeyup="validate(this,'+supplyordr[_id][s].MINCNT+','+s+','+(supplyordr[_id].length-1)+','+_id+')" onblur="validate2(this,'+supplyordr[_id][s].MINCNT+')"  pattern="[0-9]" type="text" id="'+(_id+"_"+s)+'" name="dz'+_id+'" value="'+supplyordr[_id][s].SPUSE+'"/>'+
							'<input type="hidden"   id="spcode'+(_id+"_"+s)+'" value="'+supplyordr[_id][s].SPCODE+'"/>'+
							'<input type="hidden"   id="UNITPER'+(_id+"_"+s)+'" value="'+supplyordr[_id][s].UNITPER+'"/>'+
							'<input type="hidden"   id="MINCNT'+(_id+"_"+s)+'" value="'+supplyordr[_id][s].MINCNT+'"/>'+
							'</span></td></tr>';
					}
				}
				showTitle = showTitle+'	</tbody></table></div>';
				showdata[_id] = showTitle;
				// $("#"+_id).html(showTitle);
		}
		
		// 页面加载
		function editHtml2(_data,_id){
			var supplyordr = _data.supplyordr;
			var tablewidth = (window.frames["mainForm"].scrollWidth - 680);
				var showTitle = '<div class="table-head">'+
				'<table cellspacing="0" cellpadding="0">'+
				'<thead> <tr> <td><span style="width:80px;"><fmt:message key="supplies_code" /></span></td>'+
						'<td><span style="width:100px;"><fmt:message key="supplies_name" /></span></td>'+
						'<td><span style="width:60px;"><fmt:message key="supplies_specifications" /></span></td>'+
						'<td><span style="width:60px;"><fmt:message key="supplyunit" /></span></td>'+
						'<td><span style="width:80px;">最小报货量</span></td>'+//最小报货量
						'<td><span style="width:80px;"><fmt:message key="scm_safety_stock" /></span></td>'+//最低库存
						'<td><span style="width:80px;"><fmt:message key="current_inventory" /></span></td>'+//当前库存
						'<td><span style="width:80px;"><fmt:message key="ontheway" /></span></td>'+//在途
						'<td><span style="width:80px;"><fmt:message key="misboh_DeclareGoodsGuide_no3" /></span></td>'+//计算量
						'<td><span style="width:60px;"><fmt:message key="procurement_unit" /></span></td>'+//采购单位
						'<td><span style="width:80px;"><fmt:message key="misboh_DeclareGoodsGuide_no4" /></span></td>'+//调整量
					'</tr> </thead> </table> </div>'+
					'<div class="table-body">'+
					'<table cellspacing="0" cellpadding="0">'+
						'<tbody>';
				if(supplyordr[_id]!=null && supplyordr[_id].length>0){
					for(var s =0;s<supplyordr[_id].length;s++){
						showTitle = showTitle+'<tr data-mincnt='+supplyordr[_id][s].SPMIN+'>'+
							'<td><span style="width:80px;">'+supplyordr[_id][s].SPCODE+
							'</span></td>'+
							'<td><span style="width:100px;">'+supplyordr[_id][s].SPNAME+'</span></td>'+
							'<td><span style="width:60px;">'+supplyordr[_id][s].SPDESC+'</span></td>'+
							'<td><span style="width:60px;">'+supplyordr[_id][s].SPUNIT+'</span></td>'+
							'<td><span style="width:80px;text-align: right;" align="right">'+supplyordr[_id][s].MINCNT+'</span></td>'+
							'<td><span style="width:80px;text-align: right;" align="right">'+supplyordr[_id][s].SPMIN+'</span></td>'+
							'<td><span style="width:80px;text-align: right;" align="right">'+supplyordr[_id][s].SPNOW+'</span></td>'+
							'<td><span style="width:80px;text-align: right;" align="right">'+supplyordr[_id][s].SPWAY+'</span></td>'+
							'<td><span style="width:80px;text-align: right;" align="right">'+supplyordr[_id][s].SPUSE+'</span></td>'+
							'<td><span style="width:60px;">'+supplyordr[_id][s].UNIT+'</span></td>'+
							'<td><span style="width:80px;">'+
							'<input onfocus="this.select()" onkeyup="validate(this,'+supplyordr[_id][s].MINCNT+','+s+','+(supplyordr[_id].length-1)+','+_id+')" onblur="validate2(this,'+supplyordr[_id][s].MINCNT+')" style="width:80px;text-align:right;" pattern="[0-9]" type="text" id="'+(_id+"_"+s)+'" name="dz'+_id+'" value="'+supplyordr[_id][s].SPUSE+'"/>'+
							'<input type="hidden"   id="spcode'+(_id+"_"+s)+'" value="'+supplyordr[_id][s].SPCODE+'"/>'+
							'<input type="hidden"   id="UNITPER'+(_id+"_"+s)+'" value="'+supplyordr[_id][s].UNITPER+'"/>'+
							'<input type="hidden"   id="MINCNT'+(_id+"_"+s)+'" value="'+supplyordr[_id][s].MINCNT+'"/>'+
							'</span></td></tr>';
					}
				}
				showTitle = showTitle+'	</tbody></table></div>';
				showdata[_id] = showTitle;
				 // $("#"+_id).html(showTitle);
		}
		
		var typList = new Array;
		function getOrdrSupply(){
			typList = new Array;
			$(".grid").each(function(){
				$("input[name=dz"+this.id+"]").each(function(){
					var _supply = {};
					var split = this.id.split("_");
					_supply["CATEGORY_CODE"]=split[0];
					_supply["SPCODE"]=$("#spcode"+this.id).val();
					_supply["UNITPER"]=$("#UNITPER"+this.id).val();
					_supply["MINCNT"]=$("#MINCNT"+this.id).val();
					_supply["AMOUNT"]=this.value;
					typList.push(_supply);
				});
			});
		}
		
		// 鼠标离开时检查数量输入格式是否合法
		function validate(e,spmin,index,max,_id){
			if(Number(e.value)<0){
	    		alert('<fmt:message key="number_be_not_valid_number"/>！');//数量不能是负数
				e.value=e.defaultValue;
				//$(e).focus();
				return;
			}else if(isNaN(e.value)){
				alert('<fmt:message key="number_be_not_valid_number"/>！');
				e.value=e.defaultValue;
				//$(e).focus();
				return;
			}else if(Number(e.value)%spmin != 0 && spmin>0 ){
// 				alert('该物质最低申购数量为'+$(e).parents('tr').data("mincnt")+"，申购量必须是最小申购量的整数倍!");
				alert('<fmt:message key="the_minimum_purchase_quantity_of_material"/>【'+spmin+'】<fmt:message key="subscriptions_must_be_an_integer_multiple_of_the_minimum_purchase_amount"/>!')
				e.value=e.defaultValue;
				//$(e).focus();
				return;
			}
			eqEnter(index,max,_id);
		}
		
		
		function eqEnter(_index,_max,_id){
			if(event.keyCode == 13){
				if(_index == _max){
					_index = 0;
				}else{
					_index ++;
				}
				$('#'+_id+'_'+_index).focus();
			}
		}
		
		// 鼠标离开时检查数量输入格式是否合法
		function validate2(e,spmin){
	    	if(Number(e.value)<0){
	    		alert('<fmt:message key="number_be_not_valid_number"/>！');//数量不能是负数
				e.value=e.defaultValue;
				//$(e).focus();
				return;
			}else if(isNaN(e.value)){
				alert('<fmt:message key="number_be_not_valid_number"/>！');
				e.value=e.defaultValue;
				//$(e).focus();
				return;
			}else if(Number(e.value)%spmin != 0 && spmin>0 ){
// 				alert('该物质最低申购数量为'+$(e).parents('tr').data("mincnt")+"，申购量必须是最小申购量的整数倍!");
				alert('<fmt:message key="the_minimum_purchase_quantity_of_material"/>【'+spmin+'】<fmt:message key="subscriptions_must_be_an_integer_multiple_of_the_minimum_purchase_amount"/>!')
				e.value=e.defaultValue;
				//$(e).focus();
				return;
			}
		}
		
		// 控制界面标签页加载
		function loadTabs(){
			var tab = $('#tabs1').tabs('getSelected');
			var content = tab.panel("body");
			var flag = content.find('#flags').val();
			$("#"+flag).html(showdata[flag]);
			setGd(flag);
		}
		
		function setGd(flag){
			var $grid = $("#"+flag);
			$thead = $grid.find('.table-head');
			$tbody = $grid.find('.table-body');
			$grid.height($(document.body).height() - 90);
			$grid.find('.table-body').height($grid.height() - $thead.height());
			
			docWidth = $(document.body).width();
			headWidth = $thead.find('table').outerWidth();
			bodyWidth = $tbody.find('table').outerWidth();
			headWidth = (headWidth > docWidth || headWidth + 55 > docWidth)
				? headWidth + 55 
				: docWidth;
			bodyWidth = (bodyWidth > docWidth || bodyWidth + 55 > docWidth)
				? bodyWidth + 55 
				: docWidth;
			$thead.width(headWidth);
			$tbody.width(bodyWidth);
			if(headWidth > docWidth){
				$grid.width(docWidth).css('overflow-x','auto');
				$tbody.height($tbody.height() - 17);
				$thead.width($tbody.width());
			}else{
				$grid.width(docWidth).css('overflow-x','hidden');		
			}
		}
		</script>
	</head>
	<body>
		<form id="mainForm" method="post" action="">
		<div class="easyui-tabs"  fit="false" plain="true"  style="z-index:88;" id="tabs1" >
			<c:forEach var="list" items="${categoryList}" varStatus="sta">
				<input type="hidden" id="s${list.CATEGORY_CODE}" name="showCode" value="${list.CODETYP}"/>
			 	<div title='${list.DES}' style="width: 100%;"  class="grid" id="${list.CATEGORY_CODE}">
				<input type="hidden" id="flags"  value="${list.CATEGORY_CODE}" />
				</div>
			</c:forEach>		 	
		</div>
		</form>
	</body>
</html>