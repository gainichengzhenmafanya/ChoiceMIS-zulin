<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="materials_list" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	    <style type="text/css">
			#tblGrid td{
				border-right: 1px solid #999999;
				border-bottom:1px solid #999999; 
			}
			.form{
				height: 80%;
			}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<div class="form">
			<input type="hidden" id="state" value="show"/>
			<form id="LossManageFormWZ" method="post" action="<%=path%>/lossManage/listLossManage.do">
			<input type="hidden" id="nowDate" name="nowDate" value="${nowDate}"/>
			<input type="hidden" id="ynusedept" name="ynusedept" value="${ynusedept}"/>
			<input type="hidden" id="typ" name="typ" value="${typ}"/>
			<input type="hidden" id="dept" name="dept" value="${dept }"/>
			<div class="form-line">	
				<div class="form-label"><fmt:message key="business_day"/>:</div>
				<div class="form-input" style="width:120px;">
					<input type="text" style="width:90px;" id="workdate" name="workdate" value="<fmt:formatDate value="${workdate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edate\')}'});"/>
				</div>
				<c:if test="${ynusedept == 'Y'}">
				 	<div class="form-label"><span style="color:red;">*</span><fmt:message key="positions"/>：</div>
					<div class="form-input">
						<div class="form-input">
							<input type="text" name="deptDes" id="deptDes" style="width: 136px;margin-top: -3px;" autocomplete="off" class="text" value="<c:out value="${deptDes}" />"/>
							<img id="seachDept" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
						</div>
					</div>
				</c:if>
				<div class="form-label">
					<fmt:message key="please_select_materials"/>:
				</div>
				<div class="form-input">
					<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
				</div>
			</div>
				<div class="grid">		
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num" style="width: 26px;"><span></span></td>
									<td><span style="width:80px;"><fmt:message key="supplies_code" /></span></td>
									<td><span style="width:120px;"><fmt:message key="supplies_name" /></span></td>
									<td><span style="width:60px;"><fmt:message key="standard_unit" /></span></td>
									<td><span style="width:60px;"><fmt:message key="quantity" /></span></td>
									<td><span style="width:60px;"><fmt:message key="reference_unit" /></span></td>
									<td><span style="width:60px;"><fmt:message key="reference_number" /></span></td>
									<td><span style="width:80px;"><fmt:message key="The_entry_of_people" /></span></td>
									<td><span style="width:120px;"><fmt:message key="The_entry_time" /></span></td>
									<td><span style="width:80px;"><fmt:message key="status" /></span></td>
									<td><span style="width:120px;"><fmt:message key="remark" /></span></td>
								</tr>
							</thead>
						</table>
					</div>				
					<div class="table-body">
						<table id="tblGrid" cellspacing="0" cellpadding="0">
							<tbody>
								<c:forEach var="postionLoss" items="${listPostionLoss}" varStatus="status">
									<tr data-state=${postionLoss.state } data-unitper=${postionLoss.unitper }>
										<td class="num" style="width: 26px;"><span>${status.index+1}</span></td>
										<td><span style="width:80px;" title="${postionLoss.pk_postionloss}">${postionLoss.pcode}</span></td>
										<td title="${postionLoss.pname}"><span title="${postionLoss.pname}" style="width:120px;">${postionLoss.pname}</span></td>
										<td title="${postionLoss.unit}"><span  style="width:60px;">${postionLoss.unit}</span></td>
										<td><span style="width:60px;text-align:right;" title="${postionLoss.cnt}"><fmt:formatNumber value="${postionLoss.cnt}" pattern="##.##" minFractionDigits="2"/></span></td>
										<td title="${postionLoss.unit1}"><span  style="width:60px;">${postionLoss.unit1}</span></td>
										<td><span style="width:60px;text-align:right;" title="${postionLoss.cnt1}"><fmt:formatNumber value="${postionLoss.cnt1}" pattern="##.##" minFractionDigits="2"/></span></td>
										<td><span style="width:80px;" title="${postionLoss.inputby}">${postionLoss.inputuser}</span></td>
										<td><span style="width:120px;">${postionLoss.inputtime}</span></td>
										<td><span style="width:80px;" title="${postionLoss.state}">
											<c:if test="${postionLoss.state == 1}">未确认</c:if>
											<c:if test="${postionLoss.state == 2}">已确认</c:if>
										</span></td>
										<td>
											<span style="width:120px;">
												<c:choose>
													<c:when test="${postionLoss.state == 1}">
														<select style='width:120px;height:18px;'>
															<c:forEach var='codeDes' items='${codeDesList }'>
																<option value='${codeDes.des }' <c:if test="${codeDes.des == postionLoss.memo }">selected = "selected"</c:if>>${codeDes.des }</option>
															</c:forEach>
														</select>
													</c:when>
													<c:otherwise>
														${postionLoss.memo}
													</c:otherwise>
												</c:choose>
											</span>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/autoTable.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				var savelock = false;
				focus() ;//页面获得焦点
				loadToolBar([true,true,false,true,true,true]);
				$('#workdate').bind('click',function(){
					new WdatePicker();
				});
				
				//
				var defSelectionHtml="";
				$.ajax({
					url:'<%=path%>/lossManage/getScrappedname.do',
					type: "POST",
					async: true,
					success:function(data){
						defSelectionHtml = "<select style='width:120px;height:18px;'>";
						for(i in data){
							var code = data[i];
							defSelectionHtml += '<option value="'+code.des+'">'+code.des+'</option>';
						}
						defSelectionHtml += '</select>';
					}
				});
				
				//查找物资
			 	$('#seachSupply').bind('click.custom',function(e){
			 		if($("#state").val() != "edit" && $("#state").val() != "add"){//必须是编辑的情况下才能选择物资  2015.1.2wjf
			 			alert('必须是编辑的情况下才能选择物资！');
			 			return;
			 		}
			 		if(!!!top.customWindow){
						top.customSupply('<fmt:message key="please_select_materials" />',encodeURI('<%=path%>/misbohcommon/selectSupplyLeft.do?ynsto=Y&single=false&positn='+$('#firm').val()),$('#sp_code'),null,$('#unit1'),$('.unit'),$('.unit1'),null,handler2);
					}
				});
			 	function handler2(sp_codes){
			 		var codes = '';//存放页面上的编码，用来判断是否存在
			 		$('.grid').find('.table-body').find('tr').each(function (){
			 			codes += $(this).find('td:eq(1)').text()+",";
					});
			 		var sp_code_arry = sp_codes.split(",");
					for(var i=0; i<sp_code_arry.length; i++){
						var sp_code = sp_code_arry[i];
						if(codes.indexOf(sp_code) != -1 ){//如果已存在，则继续下次循环
							continue;
						}
						$.ajax({
							type: "POST",
							url: "<%=path%>/misbohcommon/findSupplyBySpcode.do",
							data: "sp_code="+sp_code,
							dataType: "json",
							success:function(supply){
								if(!$(".table-body").find("tr:last").find("td:eq(1)").find('span').text()==''){
									$.fn.autoGrid.addRow();
								}
								$(".table-body").find("tr:last").find("td:eq(1) span").text(supply.sp_code);
								$(".table-body").find("tr:last").find("td:eq(1) span").attr('title','');
								$(".table-body").find("tr:last").find("td:eq(2) span input").val(supply.sp_name).focus();
								$(".table-body").find("tr:last").find("td:eq(2) span").attr('title',supply.sp_name);
								$(".table-body").find("tr:last").find("td:eq(2) span").text(supply.sp_name);
								$(".table-body").find("tr:last").find("td:eq(3) span").text(supply.unit);
								$(".table-body").find("tr:last").find("td:eq(4) span").text(0.00).attr('title',0).css("text-align","right");
								$(".table-body").find("tr:last").find("td:eq(5) span").text(supply.unit1);
								$(".table-body").find("tr:last").find("td:eq(6) span").text(0.00).attr('title',0).css("text-align","right");
								$(".table-body").find("tr:last").find("td:eq(7) span").text('');
								$(".table-body").find("tr:last").find("td:eq(8) span").text('');
								$(".table-body").find("tr:last").find("td:eq(9) span").text('');
								$(".table-body").find("tr:last").data('unitper', supply.unitper);
								$(".table-body").find("tr:last").data('state','1');
								$(".table-body").find("tr:last").find("td:eq(10) span").html(defSelectionHtml);
							
							}
						});
					}
			 	}
				
				//屏蔽鼠标右键
			 	$(document).bind('keyup',function(e){
			 		//表格数量一变，校验一下，价格就接着变
			 		if($(e.srcElement).is("input")){//对表格内的输入框进行判读，延迟600毫秒
			 			var index=$(e.srcElement).closest('td').index();
			    		if(index=="4" || index == "6"){
				 			$(e.srcElement).unbind('blur').blur(function(e){
				 				validateByMincnt($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
				 			});
				    		validator($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
			    		}
			    	}
				});
				
				//控制按钮显示
				function loadToolBar(use){
					$('.tool').html('');
					$('.tool').toolbar({
						items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								$("#LossManageFormWZ").submit();
							}
						},"-",{
							text: '<fmt:message key="edit" />',
							title: '<fmt:message key="edit" />',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[1],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								editCells();
								$('#state').val('edit');
								loadToolBar([true,false,true,true,true,true]);
							}
						},{
							text: '<fmt:message key="save" />',
							title: '<fmt:message key="save" />',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[2],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								saveDate();
								$('#state').val('show');
							}
						},{
							text: '<fmt:message key="delete" />',
							title: '<fmt:message key="delete" />',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')}&&use[3],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								deleteData();
							}
						},{
							text: '<fmt:message key="confirm" />',
							title: '<fmt:message key="confirm" />',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								enterData();
							}
						},"-",{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}]
					});
			 	}
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),26);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				/*弹出部门选择树*/
				$('#seachDept').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#dept').val();
						var defaultName = $('#deptDes').val();
						//alert(defaultCode+"==="+defaultName);
						var offset = getOffset('dept');
						top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/misbohcommon/findPositnSuper.do?typn=7&iffirm=1&mold=one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deptDes'),$('#dept'),'760','520','isNull',handle);
					}
				});
				
				function handle(code){
					$("#LossManageFormWZ").submit();
				}
			});
			//编辑表格
			function editCells(){
				var dept = $('#dept').val();
				$(".table-body").autoGrid({
					initRow:1,
					delSta:2,
					colPerRow:11,
					widths:[35,90,130,70,70,70,70,90,130,90,130],
					colStyle:['','',{background:"#F1F1F1"},'',{background:"#F1F1F1"},'',{background:"#F1F1F1"},'','','',{background:"#F1F1F1"}],
					VerifyEdit:{verify:true,enable:function(cell,row){
						return row.find('td').index(cell) == 2 || (row.find("td:eq(2)").text() == null || $.trim(row.find("td:eq(2)").text()) != '' ) ? true : false;
					}},
					onEdit:$.noop,
					editable:[2,4,6],//能输入的位置
					onLastClick:$.noop,
					onEnter:function(data){
// 						$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.ovalue) ;
						var pos = data.curobj.closest('tr').find('td').index(data.curobj.closest('td'));
						if(pos == 2){
							if($.trim(data.curobj.closest('td').prev().text())){
								data.curobj.find('span').html(data.curobj.closest('td').find('span').attr('title'));
								return;
							}
						 	else if(!data.actionobj){
								$.fn.autoGrid.setCellEditable(data.curobj.find('span').closest('tr'),2);
								return;
							} 
						}
						$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.ovalue) ;
					},
					cellAction:[{
						index:2,
						action:function(row){
							$.fn.autoGrid.setCellEditable(row,4);
						},
						onCellEdit:function(event,data,row){
							data['url'] = '<%=path%>/misbohcommon/findSupplyTop10.do?ex=N';//不能查门店半成品
							/*if (data.value.split(".").length>2) {
								data['key'] = 'sp_code';
							}else if(!isNaN(data.value)){
								data['key'] = 'sp_code';
							}else if((/[\u4e00-\u9fa5]+/).test(data.value)){
								data['key'] = 'sp_name';
							}else{
								data['key'] = 'sp_init';
							}*/
							data['key'] = 'sp_code';
							$.fn.autoGrid.ajaxEdit(data,row);
						},
						resultFormat:function(data){
							return data.sp_init+'-'+data.sp_code+'-'+data.sp_name;
						},
						afterEnter:function(data2,row){
							if(!$(".table-body").find("tr:last").find("td:eq(1)").find('span').text()==''){
								$.fn.autoGrid.addRow();
							}
							row.find("td:eq(1) span").text(data2.sp_code);
							row.find("td:eq(1) span").attr('title','');
							row.find("td:eq(2) span input").val(data2.sp_name).focus();
							row.find("td:eq(2) span").attr('title',data2.sp_name);
							row.find("td:eq(3) span").text(data2.unit);
							row.find("td:eq(4) span").text(0.00).attr('title',0).css("text-align","right");
							row.find("td:eq(5) span").text(data2.unit1);
							row.find("td:eq(6) span").text(0.00).attr('title',0).css("text-align","right");
							row.find("td:eq(7) span").text('');
							row.find("td:eq(8) span").text('');
							row.find("td:eq(9) span").text('');
							row.data('unitper',data2.unitper);
							row.data('state','1');
							$.ajax({
								url:'<%=path%>/lossManage/getScrappedname.do',
								type: "POST",
								success:function(data){
									var html = "<select style='width:120px;height:18px;'>";
									for(i in data){
										var code = data[i];
										html += '<option value="'+code.des+'">'+code.des+'</option>';
									}
									html += '</select>';
									row.find("td:eq(10) span").html(html);
								}
							});
						}
					},{
						index:4,
						action:function(row,data2){
							if(Number(data2.value) < 0){
								alert('数量不能为负数！');
								row.find("td:eq(4)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,4);
							} else if(isNaN(data2.value)){
								alert('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(4)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,4);
							} else {
								row.find("td:eq(4)").find('span').attr('title',data2.value);
								row.find("td:eq(6) span").text((Number(data2.value) * Number(row.data('unitper'))).toFixed(2)).attr('title',Number(data2.value) * Number(row.data('unitper'))).css("text-align","right");
								$.fn.autoGrid.setCellEditable(row,6);
							}
						}
					},{
						index:6,
						action:function(row,data2){
							if(Number(data2.value) < 0){
								alert('数量不能为负数！');
								row.find("td:eq(6)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,6);
							} else if(isNaN(data2.value)){
								alert('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(6)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,6);
							} else {
								row.find("td:eq(6)").find('span').attr('title',data2.value);
								if(Number(row.data("unitper")) != 0){//如果转换率不为0的  才修改  否则会报错wjf
									row.find("td:eq(4) span").text(Number(Number(data2.value)/Number(row.data("unitper"))).toFixed(2)).attr('title',Number(data2.value)/Number(row.data("unitper")));//设置标准数量
								}
								if(!row.next().html())$.fn.autoGrid.addRow();
								$.fn.autoGrid.setCellEditable(row.next(),2);
							}
						}
					}]
				});
			}
			function checkNum(inputObj){
				if(isNaN(inputObj.value)){
					alert("无效数字！");
					inputObj.focus();
					return false;
				}
			}
			
			//保存数据
			function saveDate(){
				var data = {};
				var flag = false;
				var checkboxList = $('.grid').find('.table-body').find('tr');
				if(checkboxList.size() > 0){
					if(confirm('数据将被更新，是否继续保存？')){
						checkboxList.each(function(i){
							if($.trim($(this).find('td:eq(1)').find('span').text()) != null && $.trim($(this).find('td:eq(1)').find('span').text()) != ''){
								if($.trim($(this).find('td:eq(9)').find('span').text()) != "已确认"){
									flag = true;
									var pname = $(this).find('td:eq(2)').find('input').val()==null?$(this).find('td:eq(2)').find('span').text():$(this).find('td:eq(2)').find('input').val();
									var cnt = $(this).find('td:eq(4) span').attr('title');
									var cnt1 = $(this).find('td:eq(6) span').attr('title');
									var unitper = $(this).data('unitper');
									data['listPostionLoss['+i+'].pcode'] = $(this).find('td:eq(1)').find('span').text();
									data['listPostionLoss['+i+'].unit'] = $(this).find('td:eq(3)').find('span').text();
									data['listPostionLoss['+i+'].unit1'] = $(this).find('td:eq(5)').find('span').text();
									data['listPostionLoss['+i+'].pname'] = pname;
									data['listPostionLoss['+i+'].cnt'] = cnt;
									data['listPostionLoss['+i+'].cnt1'] = cnt1;
									data['listPostionLoss['+i+'].dept'] = $("#dept").val();
									data['listPostionLoss['+i+'].typ'] = $("#typ").val();
									data['listPostionLoss['+i+'].workdate'] = $("#workdate").val();
									data['listPostionLoss['+i+'].pk_postionloss'] = $(this).find('td:eq(1)').find('span').attr('title');
									data['listPostionLoss['+i+'].state'] = $(this).find('td:eq(9)').find('span').attr('title');
									data['listPostionLoss['+i+'].memo'] = $(this).find('td:eq(10) span').find('select').val();
									data['listPostionLoss['+i+'].unitper'] = unitper;
								}
							}
						});
						data["dept"] = $("#dept").val();
						data["typ"] = $("#typ").val();
						data["workdate"] = $("#workdate").val();
					}
				}
				if(!flag){
					alert('<fmt:message key="no_edited_documents_to_be_saved"/>！');
					return;
				}
				$.ajaxSetup({async:false});
				$.post("<%=path%>/lossManage/saveLossManage.do",data,function(data){
					var rs = data;
                    if(isNaN(rs)){
                    	alert('<fmt:message key="save_fail"/>！');
                        return;
                    }
					switch(Number(rs)){
					case -1:
						alert('<fmt:message key="save_fail"/>！');
						savelock = false;
						break;
					case 1:
						alert('<fmt:message key="save_successful"/>！');
							savelock = true;
							reloadPage();
						break;
					}
				});	
			}
			//刷新页面
			function reloadPage(){
				var action = "<%=path %>/lossManage/listLossManage.do";
				$('#LossManageFormWZ').attr('action',action);
				$('#LossManageFormWZ').submit(); 
			}
			//清空报损数据
			function deleteData(){
				if(confirm('是否删除所有未确认损耗记录？')){
					$.ajaxSetup({async:false});
					$.post("<%=path %>/lossManage/deleteLossManage.do",{workdate:$('#workdate').val(),typ:$('#typ').val(),dept:$('#dept').val()},function(data){
						if(data == '1'){
							alert('删除成功!');
							reloadPage();
						} else {
							alert('删除失败！');
						}
					});
// 					$('#LossManageFormWZ').attr('action',action);
// 					$('#LossManageFormWZ').submit(); 
				}
			}
			//确认
			function enterData(){
				if($('#state').val() == 'edit'){
					alert('<fmt:message key="please_save_the_unsaved_data"/>');
					return;
				}
				if($('.grid').find('.table-body').find('tr').size() > 0){
// 					saveDate();
<%-- 					var action = "<%=path %>/lossManage/enterLossManage.do"; --%>
					$.ajaxSetup({async:false});
					$.post("<%=path %>/lossManage/enterLossManage.do",{workdate:$("#workdate").val(),typ:$("#typ").val(),dept:$("#dept").val()},function(data){
						alert(data);
						reloadPage();
// 						if(data != null){
							
// 							if(data == '0'){
// 								alert('本单据早已被审核，不能再审核或者是本仓位没有定义类型！');
// 							}else if(data == '1'){
// 								alert('确认成功！');
// 							}else if(data == '-2'){
// 								alert('');
// 							}else if(data == '-3'){
// 								alert('退库冲消单据数量不能大于0！');
// 							}else if(data == '-4'){
// 								alert('库存不足！');
// 							}else if(data == '-5'){
// 								alert('本仓位无次物资！');
// 							}else{
// 								alert('确认失败！');
// 							}
// 						}else{
// 							alert('确认失败！');
// 						}
					});
				} else{
					alert("当前没有可确认的数据！");
				}
			}
			
			function validateByMincnt(index,row,data2){//最小申购量判断
				if(index=="4"){
					if(isNaN(data2.value)||Number(data2.value) < 0){
						alert('<fmt:message key="please_enter_positive_integer"/>！');
						row.find("input").focus().select();
					}else{
						row.find("td:eq(4)").find('span').text(Number(data2.value).toFixed(2));
					}
				}
			}
			function validator(index,row,data2){//输入框验证
				row.find("input").data("ovalue",data2.value);
				if(index=="4"){
					if(isNaN(data2.value)||Number(data2.value) < 0){
						data2.value=0;
						return;
					}
					row.find("td:eq(4)").find('span').attr('title',data2.value);
					row.find("td:eq(6) span").text((Number(row.find("td:eq(4)").text())*Number(row.data("unitper"))).toFixed(2)).attr('title',Number(data2.value)*Number(row.data("unitper")));//设置参考数量
				}
				if(index=="6"){
					if(isNaN(data2.value)||Number(data2.value) < 0){
						data2.value=0;
						return;
					}
					row.find("td:eq(6)").find('span').attr('title',data2.value);
					if(Number(row.data("unitper")) != 0){//如果转换率不为0的  才修改  否则会报错wjf
						row.find("td:eq(4) span").text(Number(Number(data2.value)/Number(row.data("unitper"))).toFixed(2)).attr('title',Number(data2.value)/Number(row.data("unitper")));//设置标准数量
					}
				}
			}
			
			//获取系统时间
			function getInd(){
				var myDate=new Date(new Date().getTime() + 24*60*60*1000);  
				var yy=myDate.getFullYear();
				var MM=myDate.getMonth()+1;
				var dd=myDate.getDate();
				if(MM<10)
					MM="0"+MM;
				if(dd<10)
					dd="0"+dd;
				return fullDate=yy+"-"+MM+"-"+dd;
			}
			
		</script>
	</body>
</html>