<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="materials_list" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	    <style type="text/css">
			#tblGrid td{
				border-right: 1px solid #999999;
				border-bottom:1px solid #999999; 
			}
			.form{
				height: 80%;
			}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<div class="form">
			<input type="hidden" id="state" value="show"/>
			<form id="LossManageForm" method="post" action="<%=path %>/lossManage/listLossManage.do">
			<input type="hidden" id="nowDate" name="nowDate" value="${nowDate}"/>
			<input type="hidden" id="ynusedept" name="ynusedept" value="${ynusedept}"/>
			<input type="hidden" id="typ" name="typ" value="${typ}"/>
			<input type="hidden" id="dept" name="dept" value="${dept }"/>
			<div class="form-line">	
				<div class="form-label"><fmt:message key="business_day"/>:</div>
				<div class="form-input" style="width:120px;">
					<input type="text" style="width:90px;" id="workdate" name="workdate" value="<fmt:formatDate value="${workdate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edate\')}'});"/>
				</div>
				<c:if test="${ynusedept == 'Y'}">
				 	<div class="form-label"><span style="color:red;">*</span><fmt:message key="positions"/>：</div>
					<div class="form-input">
						<div class="form-input">
							<input type="text" name="deptDes" id="deptDes" style="width: 136px;margin-top: -3px;" autocomplete="off" class="text" value="<c:out value="${deptDes}" />"/>
							<img id="seachDept" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
						</div>
					</div>
				</c:if>
<!-- 				<div class="form-label">请选择菜品</div> -->
<!-- 				<div class="form-input"> -->
<!-- 					<input type="hidden" id="pk_pubitem" name="pk_pubitem" value="" /> -->
<!-- 					<input type="hidden" id="vpcode" name="vpcode"  value="" /> -->
<!-- 					<input type="hidden" id="vpname" name="vpname" class="text" value="" /> -->
<!-- 					<input type="hidden" id="vunitname" name="vunitname" class="text" value="" /> -->
<%-- 					<img id="searchPubitem" class="search" src="<%=path%>/image/themes/icons/search.png" style="margin-top: 0px;"  /> --%>
<!-- 				</div> -->
			</div>
			<div class="grid">		
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 26px;"><span></span></td>
								<td><span style="width:80px;"><fmt:message key="scm_pubitem_code" /></span></td>
								<td><span style="width:120px;"><fmt:message key="scm_pubitem_name" /></span></td>
								<td><span style="width:60px;"><fmt:message key="unit" /></span></td>
								<td><span style="width:60px;"><fmt:message key="quantity" /></span></td>
								<td><span style="width:80px;"><fmt:message key="The_entry_of_people" /></span></td>
								<td><span style="width:120px;"><fmt:message key="The_entry_time" /></span></td>
								<td><span style="width:80px;"><fmt:message key="status" /></span></td>
								<td><span style="width:120px;"><fmt:message key="remark" /></span></td>
							</tr>
						</thead>
					</table>
				</div>				
				<div class="table-body">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="postionLoss" items="${listPostionLoss}" varStatus="status">
								<tr data-state=${postionLoss.state }>
									<td class="num" style="width: 26px;"><span>${status.index+1}</span></td>
									<td><span style="width:80px;" title="${postionLoss.pk_postionloss}">${postionLoss.pcode}</span></td>
									<td title="${postionLoss.pname}"><span title="${postionLoss.pname}" style="width:120px;">${postionLoss.pname}</span></td>
									<td title="${postionLoss.unit}"><span  style="width:60px;">${postionLoss.unit}</span></td>
									<td><span style="width:60px;text-align:right;"><fmt:formatNumber value="${postionLoss.cnt}" pattern="##.##" minFractionDigits="2"/></span></td>
									<td><span style="width:80px;" title="${postionLoss.inputby}">${postionLoss.inputuser}</span></td>
									<td><span style="width:120px;">${postionLoss.inputtime}</span></td>
									<td><span style="width:80px;" title="${postionLoss.state}">
										<c:if test="${postionLoss.state == 1}">未确认</c:if>
										<c:if test="${postionLoss.state == 2}">已确认</c:if>
									</span></td>
									<td>
										<span style="width:120px;">
											<c:choose>
												<c:when test="${postionLoss.state == 1}">
													<select style='width:120px;height:18px;'>
														<c:forEach var='codeDes' items='${codeDesList }'>
															<option value='${codeDes.des }' <c:if test="${codeDes.des == postionLoss.memo }">selected = "selected"</c:if>>${codeDes.des }</option>
														</c:forEach>
													</select>
												</c:when>
												<c:otherwise>
													${postionLoss.memo}
												</c:otherwise>
											</c:choose>
										</span>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/autoTable.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
		
		<script type="text/javascript">
		//
		var defSelectionHtml="";

		$(document).ready(function(){
				var savelock = false;
				focus() ;//页面获得焦点
				loadToolBar([true,true,false,true,true,true]);
				$('#workdate').bind('click',function(){
					new WdatePicker();
				});
				//点击checkbox改变
// 				$('.grid').find('.table-body').find('tr').find(':checkbox').bind("change", function () {
// 					if ($(this)[0].checked) {
// 					    $(this).attr("checked", false);
// 					}else{
// 						$(this).attr("checked", true);
// 					}
// 				});
				// 当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
// 				$('.grid').find('.table-body').find('tr').bind("click", function () {
// 					if ($(this).find(':checkbox')[0].checked) {
// 						$(this).find(':checkbox').attr("checked", false);
// 					}else{
// 						$(this).find(':checkbox').attr("checked", true);
// 					}
// 				});
				//---------------------------
//	 			全选
// 				$("#chkAll").click(function() {
// 			    	if (!!$("#chkAll").attr("checked")) {
// 			    		$('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",true);
// 			    	}else{
// 			            $('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",false);
// 		            }
// 			    });

				$.ajax({
					url:'<%=path%>/lossManage/getScrappedname.do',
					type: "POST",
					async: true,
					success:function(data){
						defSelectionHtml = "<select style='width:120px;height:18px;'>";
						for(i in data){
							var code = data[i];
							defSelectionHtml += '<option value="'+code.des+'">'+code.des+'</option>';
						}
						defSelectionHtml += '</select>';
					}
				});
	  		
				$("#searchPubitem").click(function(){
			 		if($("#state").val() != "edit" && $("#state").val() != "add"){//必须是编辑的情况下才能选择物资  2015.1.2wjf
			 			alert('必须是编辑状态下才能选择菜品！');//<fmt:message key="must_add_or_edit_can_select"/>！
			 			return;
			 		}
					choosePubitem({
		 				basePath:'<%=path%>',
		 				width:600,
		 				domId:$("#pk_pubitem").val()
		 			});
				});
				//控制按钮显示
				function loadToolBar(use){
					$('.tool').html('');
					$('.tool').toolbar({
						items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								$("#LossManageForm").submit();
							}
						},"-",{
							text: '<fmt:message key="edit" />',
							title: '<fmt:message key="edit" />',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[1],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								editCells();
								$('#state').val('edit');
								loadToolBar([true,false,true,true,true,true]);
							}
						},{
							text: '<fmt:message key="save" />',
							title: '<fmt:message key="save" />',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[2],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								saveDate();
								$('#state').val('show');
							}
						},{
							text: '<fmt:message key="delete" />',
							title: '<fmt:message key="delete" />',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')}&&use[3],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								deleteData();
							}
						},{
							text: '<fmt:message key="confirm" />',
							title: '<fmt:message key="confirm" />',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								enterData();
							}
						},"-",{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}]
					});
			 	}
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),80);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
// 				if($('.grid').find('.table-body').find('.num').size()==0){
// 					editCells();
// 					loadToolBar([true,false,false,true,true,true]);
// 				}
// 				if($('.grid').find('.table-body').find('.num').size() > 0){
// 					loadToolBar([true,false,false,true,true,true]);
// 				} else{
// 					loadToolBar([true,true,false,true,true,true]);
// 				}
				/*弹出部门选择树*/
				$('#seachDept').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#dept').val();
						var defaultName = $('#deptDes').val();
						//alert(defaultCode+"==="+defaultName);
						var offset = getOffset('dept');
						top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/misbohcommon/findPositnSuper.do?typn=7&iffirm=1&mold=one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deptDes'),$('#dept'),'760','520','isNull',handle);
					}
				});
				
				function handle(code){
					$("#LossManageForm").submit();
				}
			});
			//编辑表格
			function editCells(){
				var dept = $('#dept').val();
				$(".table-body").autoGrid({
					initRow:1,
					delSta:2,
					colPerRow:9,
					widths:[35,90,130,70,70,90,130,90,130],
					colStyle:['','',{background:"#F1F1F1"},'',{background:"#F1F1F1"},'','','',{background:"#F1F1F1"}],
					VerifyEdit:{verify:true,enable:function(cell,row){
						return row.find('td').index(cell) == 2 || (row.find("td:eq(2)").text() == null || $.trim(row.find("td:eq(2)").text()) != '' ) ? true : false;
					}},
					onEdit:$.noop,
					editable:[2,4],//能输入的位置
					onLastClick:$.noop,
					onEnter:function(data){
// 						$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.ovalue) ;
						var pos = data.curobj.closest('tr').find('td').index(data.curobj.closest('td'));
						if(pos == 2){
							if($.trim(data.curobj.closest('td').prev().text())){
								data.curobj.find('span').html(data.curobj.closest('td').find('span').attr('title'));
								return;
							}
						 	else if(!data.actionobj){
								$.fn.autoGrid.setCellEditable(data.curobj.find('span').closest('tr'),2);
								return;
							} 
						}
						$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.ovalue) ;
					},
					cellAction:[{
						index:2,
						action:function(row){
							$.fn.autoGrid.setCellEditable(row,4);
						},
						onCellEdit:function(event,data,row){
							data['url'] = '<%=path%>/lossManage/queryPubItemmTop10.do';
							if (data.value.split(".").length>2) {
								data['key'] = 'vcode';
							}else if(!isNaN(data.value)){
								data['key'] = 'vcode';
							}else if((/[\u4e00-\u9fa5]+/).test(data.value)){
								data['key'] = 'vname';
							}else{
								data['key'] = 'vinit';
							}
							$.fn.autoGrid.ajaxEdit(data,row);
						},
						resultFormat:function(data){
							return data.vcode+'-'+data.vinit+'-'+data.vname;
						},
						afterEnter:function(data2,row){
// 							var num=0;
// 							$('.grid').find('.table-body').find('tr').each(function(){
// 								if($(this).find("td:eq(1)").text()==data2.vcode){
// 									num=1;
// 								}
// 							});
// 							if(num==1){
// 								showMessage({
// 	 								type: 'error',
// 	 								msg: '<fmt:message key="added_supplies_remind"/>！',
// 	 								speed: 1000
// 	 							});
// 								return;
// 							}
							row.find("td:eq(1) span").text(data2.vcode);
							row.find("td:eq(1) span").attr('title','');
							row.find("td:eq(2) span input").val(data2.vname).focus();
							row.find("td:eq(2) span").attr('title',data2.vname);
							row.find("td:eq(3) span").text(data2.vunit);
							row.find("td:eq(4) span").text(0.00).css("text-align","right");
							row.find("td:eq(5) span").text('');
							row.find("td:eq(6) span").text('');
							row.find("td:eq(7) span").text('').attr('title','1');;
							row.data('state','1');
							$.ajax({
								url:'<%=path%>/lossManage/getScrappedname.do',
								type: "POST",
								success:function(data){
									var html = "<select style='width:120px;height:18px;'>";
									for(i in data){
										var code = data[i];
										html += '<option value="'+code.des+'">'+code.des+'</option>';
									}
									html += '</select>';
									row.find("td:eq(8) span").html(html);
								}
							});
						}
					},{
						index:4,
						action:function(row,data2){
							if(Number(data2.value) < 0){
								alert('数量不能为负数！');
								row.find("td:eq(4)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,4);
							} else if(isNaN(data2.value)){
								alert('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(4)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,4);
							} else {
								if(!row.next().html())$.fn.autoGrid.addRow();
								$.fn.autoGrid.setCellEditable(row.next(),2);
							}
						}
					}]
				});
			}
			function checkNum(inputObj){
				if(isNaN(inputObj.value)){
					alert("无效数字！");
					inputObj.focus();
					return false;
				}
			}
			
			//保存数据
			function saveDate(){
				var data = {};
				var flag = false;
				var checkboxList = $('.grid').find('.table-body').find('tr');
				if(checkboxList.size() > 0){
					if(confirm('数据将被更新，是否继续保存？')){
						checkboxList.each(function(i){
							if($.trim($(this).find('td:eq(1)').find('span').text()) != null && $.trim($(this).find('td:eq(1)').find('span').text()) != ''){
								if($.trim($(this).find('td:eq(7)').find('span').text()) != "已确认"){
									flag = true;
									var pname = $(this).find('td:eq(2)').find('input').val()==null?$(this).find('td:eq(2)').find('span').text():$(this).find('td:eq(2)').find('input').val();
									var cnt = $(this).find('td:eq(4)').find('input').val()==null?$(this).find('td:eq(4)').find('span').text():$(this).find('td:eq(4)').find('input').val();
									data['listPostionLoss['+i+'].pcode'] = $(this).find('td:eq(1)').find('span').text();
									data['listPostionLoss['+i+'].unit'] = $(this).find('td:eq(3)').find('span').text();
									data['listPostionLoss['+i+'].pname'] = pname;
									data['listPostionLoss['+i+'].cnt'] = cnt;
									data['listPostionLoss['+i+'].dept'] = $("#dept").val();
									data['listPostionLoss['+i+'].typ'] = $("#typ").val();
									data['listPostionLoss['+i+'].workdate'] = $("#workdate").val();
									data['listPostionLoss['+i+'].pk_postionloss'] = $(this).find('td:eq(1)').find('span').attr('title');
									data['listPostionLoss['+i+'].state'] = $(this).find('td:eq(7)').find('span').attr('title');
									data['listPostionLoss['+i+'].memo'] = $(this).find('td:eq(8) span').find('select').val();
								}
							}
						});
						data["dept"] = $("#dept").val();
						data["typ"] = $("#typ").val();
						data["workdate"] = $("#workdate").val();
					}
				}
				if(!flag){
					alert('<fmt:message key="no_edited_documents_to_be_saved"/>！');
					return;
				}
				$.ajaxSetup({async:false});
				$.post("<%=path%>/lossManage/saveLossManage.do",data,function(data){
					var rs = data;
                    if(isNaN(rs)){
                        alerterror('<fmt:message key="save_fail"/>！');
                        return;
                    }
					switch(Number(rs)){
					case -1:
						alerterror('<fmt:message key="save_fail"/>！');
						savelock = false;
						break;
					case 1:
						alert('<fmt:message key="save_successful"/>！');
						savelock = true;
						reloadPage();
						break;
					}
				});	
			}
			//刷新页面
			function reloadPage(){
				var action = "<%=path%>/lossManage/listLossManage.do";
				$('#LossManageForm').attr('action',action);
				$('#LossManageForm').submit(); 
			}
			//清空报损数据
			function deleteData(){
				if(confirm('是否删除所有未确认损耗记录？')){
					$.ajaxSetup({async:false});
					$.post("<%=path %>/lossManage/deleteLossManage.do",{workdate:$("#workdate").val(),typ:$("#typ").val(),dept:$("#dept").val()},function(data){
						if(data == '1'){
							alert('删除成功!');
							reloadPage();
						} else {
							alert('删除失败！');
						}
					});
<%-- 					var action = "<%=path %>/lossManage/deleteLossManage.do?typ=1"; --%>
// 					$('#LossManageForm').attr('action',action);
// 					$('#LossManageForm').submit(); 
				}
// 				reloadPage();
			}
			//确认
			function enterData(){
				if($('#state').val() == 'edit'){
					alert('<fmt:message key="please_save_the_unsaved_data"/>');
					return;
				}
				if($('.grid').find('.table-body').find('tr').size() > 0){
// 					saveDate();
<%-- 					var action = "<%=path %>/lossManage/enterLossManage.do?typ=1"; --%>
					$.ajaxSetup({async:false});
					$.post("<%=path %>/lossManage/enterLossManage.do",{workdate:$("#workdate").val(),typ:$("#typ").val(),dept:$("#dept").val()},function(data){
// 						if(!data)result = false;
						alert(data);
						reloadPage();
					});
// 					reloadPage();
				} else{
					alert("当前没有可确认的数据！");
				}
			}
	  	 	
	  	 	function setPubitem(data){
		 		var codes = '';//存放页面上的编码，用来判断是否存在
		 		$('.grid').find('.table-body').find('tr').each(function (){
		 			codes += $(this).find('td:eq(1)').text()+",";
				});

		 		for(var i=0; i<data.entity.length; i++){
					var sp_pubitem = data.entity[i].pk_store;
					var sp_vpname = data.entity[i].vname;
					var sp_vpcode = data.entity[i].vcode;
					var sp_vunitname = data.entity[i].vunitname;
					if(codes.indexOf(sp_pubitem) != -1 ){//如果已存在，则继续下次循环
						continue;
					}
	
					if(!$(".table-body").find("tr:last").find("td:eq(1)").find('span').text()==''){
						$.fn.autoGrid.addRow();
					}

					$(".table-body").find("tr:last").find("td:eq(1) span").text(sp_vpcode);
					$(".table-body").find("tr:last").find("td:eq(1) span").attr('title','');
					$(".table-body").find("tr:last").find("td:eq(2) span input").val(sp_vpname).focus();
					$(".table-body").find("tr:last").find("td:eq(2) span").attr('title',sp_vpname);
					$(".table-body").find("tr:last").find("td:eq(2) span").text(sp_vpname);
					$(".table-body").find("tr:last").find("td:eq(3) span").text(sp_vunitname);
					$(".table-body").find("tr:last").find("td:eq(4) span").text(0.00).css("text-align","right");
					$(".table-body").find("tr:last").find("td:eq(5) span").text('');
					$(".table-body").find("tr:last").find("td:eq(6) span").text('');
					$(".table-body").find("tr:last").find("td:eq(7) span").text('').attr('title','1');;
					$(".table-body").find("tr:last").data('state','1');
					$(".table-body").find("tr:last").find("td:eq(8) span").html(defSelectionHtml);
					
							
				}
	  	 	}
	  		</script>
	</body>
</html>