<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="reported_acceptance"/>--营业预估</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>	
		<style type="text/css">
			.page{margin-bottom: 25px;}
			.onEdit{
				border:1px solid;
				border-bottom-color: blue;
				border-top-color: blue;
				border-left-color: blue;
				border-right-color: blue;
			}
			.input{
				background:transparent;
				border:0px solid;
			}
			.textInput span {
				padding:0px;
			}
			.textInput input {
				border:0px;
				width:55px;
			}
		</style>
		<script type="text/javascript">
			var path="<%=path%>";
		</script>					
	</head>
	<body>
		<div class="tool"></div>
		<%--当前登录用户 --%>	
		<form id="listForm" action="<%=path%>/posSalePlanChoice3/listPosSalePlan.do" method="post">
			<div class="form-line">	
				<div class="form-label"><fmt:message key="scm_estimated_date"/><fmt:message key="date"/>:</div>
				<div class="form-input" style="width:300px;">
					<input type="text" style="width:90px;margin-bottom:5px;" id="startdate" name="startdate" value="<fmt:formatDate value="${salePlan.startdate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'enddate\')}'});"/>
					<font style="color:blue;"><fmt:message key="to"/></font>
					<input type="text" style="width:90px;margin-bottom:5px;" id="enddate" name="enddate" value="<fmt:formatDate value="${salePlan.enddate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'startdate\')}'});"/>
				</div>
			</div>
			<div class="form-line">	
				<div class="form-label"><fmt:message key="the_reference_date"/>:</div>
				<div class="form-input" style="width:300px;">
					<input type="text" style="width:90px;margin-bottom:5px;" id="bdate" name="bdate" value="<fmt:formatDate value="${salePlan.bdate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edate\')}'});"/>
					<font style="color:blue;"><fmt:message key="to"/></font>
					<input type="text" style="width:90px;margin-bottom:5px;" id="edate" name="edate" value="<fmt:formatDate value="${salePlan.edate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdate\')}'});"/>
				</div>
			</div>	
		   	<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num"><span style="width:15px;">&nbsp;</span></td>
								<td rowspan="2"><span style="width:20px;">
									<input type="checkbox" id="chkAll"/></span>
								</td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="date"/></span></td>
								<td rowspan="2"><span style="width:50px;"><fmt:message key="scm_weeks"/></span></td>
								<td rowspan="2"><span style="width:40px;"><fmt:message key="scm_holidays"/></span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="special_events"/></span></td>
<!-- 								<td colspan="2"><span>一班</span></td> -->
 								<td colspan="2"><span>午餐</span></td>
 								<td colspan="2"><span>晚餐</span></td>
<!--  								<td colspan="2"><span>四班</span></td> -->
 								<td colspan="2"><span><fmt:message key="total"/></span></td>
							</tr>
							<tr>
<%-- 								<td><span style="width:60px;"><fmt:message key="scm_estimated_date"/></span></td> --%>
<%--  								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td> --%>
 								<td><span style="width:60px;"><fmt:message key="scm_estimated_date"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="scm_estimated_date"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td>
<%--  								<td><span style="width:60px;"><fmt:message key="scm_estimated_date"/></span></td> --%>
<%--  								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td> --%>
 								<td><span style="width:60px;"><fmt:message key="scm_estimated_date"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<c:set var="sum_dat" value="${0}"/>  <!-- 日期条数 -->
				<c:set var="sum_pax2old" value="${0}"/>
				<c:set var="sum_pax2" value="${0}"/>
				<c:set var="sum_pax3old" value="${0}"/>
				<c:set var="sum_pax3" value="${0}"/>
				<c:set var="sum_totalold" value="${0}"/>
				<c:set var="sum_total" value="${0}"/>
				<div class="table-body">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>	
							<c:forEach var="posSalePlan" items="${posSalePlanList}" varStatus="status">
								<tr <c:if test="${salePlan.dat.time > posSalePlan.dat.time }">style="color:#B7B7B7;"</c:if>>
									<td class="num"><span style="width:15px;">${status.index+1}</span></td>
									<td>
										<span style="width:20px; text-align: center;">
											<input type="checkbox" name="idList" id="chk_${posSalePlan.dat}" value="<fmt:formatDate value="${posSalePlan.dat}" pattern="yyyy-MM-dd"/>"/>
										</span>
									</td>
									<td><span title="<fmt:formatDate value="${posSalePlan.dat}" pattern="yyyy-MM-dd"/>" style="width:80px;"><fmt:formatDate value="${posSalePlan.dat}" pattern="yyyy-MM-dd"/></span></td>
									<td><span title="${posSalePlan.week }" style="width:50px;">${posSalePlan.week }</span></td>
									<td><span title="${posSalePlan.isHoliday }" style="width:40px;">${posSalePlan.isHoliday}</span></td>
									<td class="textInput"><span style="width:80px;">
										<c:choose>
											<c:when test="${salePlan.dat.time <= posSalePlan.dat.time }">
												<input type="text" value="${posSalePlan.memo }" style="width:75px;text-align: middle;padding: 0px;" />
											</c:when>
											<c:otherwise>
												${posSalePlan.memo }
											</c:otherwise>
										</c:choose>
										</span></td>
<%-- 									<td><span title="${posSalePlan.pax1old }" style="width:60px;text-align: right;">${posSalePlan.pax1old }</span></td> --%>
<%-- 									<td class="textInput"><span title="${posSalePlan.pax1 }" style="width:60px;"> --%>
<%-- 										<input tiaozheng=tiaozheng type="text" value="${posSalePlan.pax1 }" onchange="changeNum(7,${status.index})"; style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td> --%>
									<td><span title="${posSalePlan.pax2old }" style="width:60px;text-align: right;">${posSalePlan.pax2old }</span></td>
									<td class="textInput"><span title="${posSalePlan.pax2 }" style="width:60px; text-align: right;">
										<c:choose>
											<c:when test="${salePlan.dat.time <= posSalePlan.dat.time }">
												<input tiaozheng=tiaozheng type="text" value="${posSalePlan.pax2 }" onchange="changeNum(7,${status.index})"; style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/>
											</c:when>
											<c:otherwise>
												${posSalePlan.pax2 }
											</c:otherwise>
										</c:choose>
										</span></td>
									<td><span title="${posSalePlan.pax3old }" style="width:60px;text-align: right;">${posSalePlan.pax3old }</span></td>
									<td class="textInput"><span title="${posSalePlan.pax3 }" style="width:60px; text-align: right;">
										<c:choose>
											<c:when test="${salePlan.dat.time <= posSalePlan.dat.time }">
												<input tiaozheng=tiaozheng type="text" value="${posSalePlan.pax3 }" onchange="changeNum(9,${status.index})"; style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/>
											</c:when>
											<c:otherwise>
												${posSalePlan.pax3 }
											</c:otherwise>
										</c:choose>
										</span></td>
<%-- 									<td><span title="${posSalePlan.pax4old }" style="width:60px;text-align: right;">${posSalePlan.pax4old }</span></td> --%>
<%-- 									<td class="textInput"><span title="${posSalePlan.pax4 }" style="width:60px;"> --%>
<%-- 										<input tiaozheng=tiaozheng type="text" value="${posSalePlan.pax4 }" onchange="changeNum(13,${status.index})"; style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td> --%>
									<td><span title="${posSalePlan.totalold }" style="width:60px;text-align: right;">${posSalePlan.totalold }</span></td>
									<td><span span=span title="${posSalePlan.total }" style="width:60px;text-align: right;">${posSalePlan.total }</span></td>
								</tr>
								<c:set var="sum_dat" value="${status.index+1}"/>
								<c:set var="sum_pax2old" value="${sum_pax2old + posSalePlan.pax2 }"/>
								<c:set var="sum_pax2" value="${sum_pax2 + posSalePlan.pax2old }"/>
								<c:set var="sum_pax3old" value="${sum_pax3old + posSalePlan.pax3old}"/>
								<c:set var="sum_pax3" value="${sum_pax3 + posSalePlan.pax3}"/>
								<c:set var="sum_totalold" value="${sum_totalold + posSalePlan.totalold}"/>
								<c:set var="sum_total" value="${sum_total + posSalePlan.total}"/>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<div style="height:10px;">	
				<table border="0" cellspacing="0" cellpadding="0" style="margin-top:0;z-index:0;height:10px">
					<thead>
						<tr>
							<td style="width:55px;"></td>
							<td style="width:90px;"><fmt:message key="total"/></td>
							<td style="width:60px;">${sum_dat }</td>
							<td style="width:170px;"></td>
							<td style="width:70px;">${sum_pax2old }</td>
							<td style="width:70px;" id="sum_pax2">${sum_pax2 }</td>
							<td style="width:70px;">${sum_pax3old }</td>
							<td style="width:70px;" id="sum_pax3">${sum_pax3 }</td>
							<td style="width:70px;">${sum_totalold }</td>
							<td style="width:70px;" id="sum_total">${sum_total }</td>
						</tr>
					</thead>
				</table>
		   </div>
<%-- 			<page:page form="listForm" page="${pageobj}"></page:page> --%>
<%-- 			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" /> --%>
<%-- 			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" /> --%>
		</form>		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			focus() ;//页面获得焦点
			
			if($('.grid').find('.table-body').find('.num').size()>0){
				loadToolBar([true,true,true,true]);
			}else {
				loadToolBar([true,false,false,false]);
			}
			
		 	$(document).bind('keydown',function(e){//按钮快捷键
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
			});
		 	//编辑到货数量和单价时，按回车可以跳到下一行的同一列
			$('tbody').find('input[tiaozheng="tiaozheng"]').live('keydown',function(e){
		 		if(e.keyCode==13){
		 			var lie = $(this).parent().parent().prevAll().length;
					var hang= $(this).parent().parent().parent().prevAll().length + 1;
					$('tbody').find('tr:eq('+hang+')').find('td:eq('+lie+')').find('span').find('input').focus();
					if(hang == $('.table-body').find('tr').length){
						$('tbody').find('tr:eq(0)').find('td:eq('+lie+')').find('span').find('input').focus();
					}
		 		}
			});

			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				});
						
			function loadToolBar(use){
				$('.tool').html('');
				$('.tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							$("#listForm").submit();
						}
					},'-',{
						text: '<fmt:message key="calculate" />',
						title: '<fmt:message key="calculate" />',
// 						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[1],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							calculate();
						}
					},{
						text: '<fmt:message key="save" />',
						title: '<fmt:message key="save" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[1],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							saveUpdate();
						}
					},'-',{
						text: 'Excel',
						title: 'Excel',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')}&&use[3],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$("#wait2").val('NO');//不用等待加载
							$('#listForm').attr('action','<%=path%>/posSalePlanChoice3/exportCast.do');
							$('#listForm').submit();
							$('#listForm').attr('target','');
							$('#listForm').attr('action','<%=path%>/posSalePlanChoice3/listPosSalePlan.do');
							$("#wait2").val('');//等待加载还原
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}]
				});
			}
			
			//自动实现滚动条 				
			setElementHeight('.grid',['.tool'],$(document.body),100);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();		
		});	
		//保存登记
		function saveUpdate(){
			var selected = {};
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() > 0){
				if(confirm('<fmt:message key="only_checked_saved_whether_continue" />!')){
					checkboxList.filter(':checked').each(function(i){
						selected['SalePlanList['+i+'].dat'] = $(this).parents('tr').find('td:eq(2)').find('span').attr('title');
						selected['SalePlanList['+i+'].memo'] = $(this).parents('tr').find('td:eq(5)').find('input').val();
						selected['SalePlanList['+i+'].pax2'] = $(this).parents('tr').find('td:eq(7)').find('input').val();
						selected['SalePlanList['+i+'].pax3'] = $(this).parents('tr').find('td:eq(9)').find('input').val();
					});
					$('#wait').show();
					$('#wait2').show();
					$.post('<%=path%>/posSalePlanChoice3/updateSalePlan.do',selected,function(data){
						$('#wait').hide();
						$('#wait2').hide();
						if(data == 1){
							alert('<fmt:message key="save_successful" />！');
						}else{
							alert('<fmt:message key="save_fail" />！');
						}
						$("#listForm").submit();
					});
				}
			}else{
				alert('<fmt:message key="please_select_options_you_need_save" />！');
				return ;
			}
		}
		
		function calculate(){
			if (confirm('<fmt:message key="hours_period_values_were_recalculated_it"/>？')) {
				if(!$('#bdate').val()) {
					alert('<fmt:message key="please_enter"/><fmt:message key="scm_reference"/><fmt:message key="startdate"/>！');
					return;
				}
				if(!$('#edate').val()) {
					alert('<fmt:message key="please_enter"/><fmt:message key="scm_reference"/><fmt:message key="enddate"/>！');
					return;
				}
				if(!$('#startdate').val()) {
					alert('<fmt:message key="please_enter"/><fmt:message key="scm_estimated_date"/><fmt:message key="startdate"/>！');
					return;
				}
				if(!$('#enddate').val()) {
					alert('<fmt:message key="please_enter"/><fmt:message key="scm_estimated_date"/><fmt:message key="enddate"/>！');
					return;
				}
				var startdate = $('#startdate').val();
				var enddate = $('#enddate').val();
				//预估日期不能大于31天
				if(DateDiff(enddate,startdate)>30){
					alert('预估日期不能大于30天！');
					return;
				}
				var bdate = $('#bdate').val();
				var edate = $('#edate').val();
				//参考日期必须大于30天
				if(DateDiff(edate,bdate) < 6){
					alert('参考日期不能小于一周！');
					return;
				}
				//参考日期不能大于90天
				if(DateDiff(edate,bdate) > 90){
					alert('参考日期不能大于90天！');
					return;
				}
				$('#wait').show();
				$('#wait2').show();
				var action = "<%=path%>/posSalePlanChoice3/calPosSalePlan.do";
				$('#listForm').attr('action',action);
				$('#listForm').submit(); 
			}
		}
		function DateDiff(sDate1, sDate2){ 
		    var aDate, oDate1, oDate2, iDays;
		    aDate = sDate1.split("-");
		    oDate1 = new Date(aDate[1] + '/' + aDate[2] + '/' + aDate[0]); //转换为12-18-2002格式
		    aDate = sDate2.split("-");
		    oDate2 = new Date(aDate[1] + '/' + aDate[2] + '/' + aDate[0]);
		    iDays = parseInt((oDate1 - oDate2) / 1000 / 60 / 60 /24); //把相差的毫秒数转换为天数
		    return iDays;
		}
		// 检查最高最低库存是否有效数字
		function checkNum(inputObj){
			if(isNaN(inputObj.value)){
				alert("<fmt:message key='invalid_number'/>！");
				inputObj.focus();
				return false;
			}
		}
		//计算列与行合计
		function changeNum(n,b){
			var numSum = 0;
			//列合计
			$("#tblGrid").find("tbody").find("tr").each(function (){
				var num1 = $(this).find("td:eq("+n+")").find('input').val()?$(this).find("td:eq("+n+")").find('input').val():$(this).find("td:eq("+n+") span").text();
				numSum += Number(num1);
			})
			if(n == 7){
				$('#sum_pax2').text(numSum);
				$('#sum_total').text(numSum+Number($('#sum_pax3').text()));
			}
			if(n == 9){
				$('#sum_pax3').text(numSum);
				$('#sum_total').text(numSum+Number($('#sum_pax2').text()));
			}
			//行合计
			var tousNum = 0;
			$("#tblGrid").find("tbody").find("tr:eq("+b+")").find('td').find('[tiaozheng=tiaozheng]').each(function (){
				tousNum+=Number($(this).val());
				$("#tblGrid").find("tbody").find("tr:eq("+b+")").find('[span=span]').html(tousNum);
				$("#tblGrid").find("tbody").find("tr:eq("+b+")").find('[span=span]').attr('title',tousNum);
			});
		}
		</script>
	</body>
</html>