<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title> 新预估--菜品销售计划</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>	
			<style type="text/css">	
			.page{margin-bottom: 25px;}		
			.onEdit{
				border:1px solid;
				border-bottom-color: blue;
				border-top-color: blue;
				border-left-color: blue;
				border-right-color: blue;
			}
			.input{
				background:transparent;
				border:0px solid;
			}
			a.l-btn-plain{
				border:1px solid #7eabcd; 
				height:22px;
			}
			.search{
				margin-top:-2px;
				cursor: pointer;
			}
			.textInput span {
				padding:0px;
			}
			.textInput input {
				border:0px;
				width:60px;
			}
			</style>
	</head>
	<body> 
		<div class="tool"></div>
		<%--当前登录用户 --%>	
		<form id="listForm" action="<%=path%>/posItemPlanChoice3/planList.do" method="post">	
		<input type="hidden" id="msg" name="msg" value="${msg}"/>	
		<input type="hidden" id="listStrSize" name="listStrSize" value="${listStrSize}"/>
		<input type="hidden" id="isDeclare" name="isDeclare" value="${isDeclare}"/>	
			<div class="form-line">	
				<div class="form-label"><fmt:message key="scm_estimated_date"/><fmt:message key="date"/>:</div>
				<div class="form-input">
					<input type="text" style="width:100px;margin-bottom:5px;" id="bdate" name="bdate" value="<fmt:formatDate value="${itemPlan.bdate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text"/>
					<font style="color:blue;"><fmt:message key="to"/></font>
					<input type="text" style="width:100px;margin-bottom:5px;" id="edate" name="edate" value="<fmt:formatDate value="${itemPlan.edate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdate\')}'});"/>
				</div>
<!-- 			 	<div class="form-label">仓位</div> -->
<!-- 				<div class="form-input"> -->
<%-- 					<input type="text" name="deptdes" id="deptdes" style="width: 136px;margin-top: -3px;" autocomplete="off" class="text" value="<c:out value="${itemPlan.deptdes}" />"/> --%>
<%-- 					<img id="seachDept" class="search" src="<%=path%>/image/themes/icons/search.png" alt='查询仓位' /> --%>
<!-- 					<input type="hidden" id="dept" name="dept" value=""/> -->
<!-- 				</div> -->
<!-- 				<div class="form-label"></div> -->
<!-- 				<div class="form-input"> -->
<%-- 					<input type="button" style="width:60px;margin-top:2px;" id="calculate" name="calculate" value='<fmt:message key="calculate"/>'/> --%>
<!-- 				</div> -->
			</div>	
		   	<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num"><span style="width:25px;">&nbsp;</span></td>
								<td rowspan="2"><span style="width:20px;">
									<input type="checkbox" id="chkAll"/></span>
								</td>
								<td rowspan="2"><span style="width:50px;"><fmt:message key="coding"/></span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="name"/></span></td>
								<td rowspan="2"><span style="width:25px;"><fmt:message key="unit"/></span></td>
								<c:forEach var="dat" items="${dateList}" varStatus="status">
									<td colspan="4"><span>${dat}</span></td>
								</c:forEach>
<!-- 								<td colspan="2"><span>一班</span></td> -->
<!--  								<td colspan="2"><span>午餐</span></td> -->
<!--  								<td colspan="2"><span>晚餐</span></td> -->
<!--  								<td colspan="2"><span>四班</span></td> -->
								<td rowspan="2"><span style="width:80px;"><fmt:message key="sector"/></span></td>
							</tr>
						   <tr>
								<c:forEach var="dat" items="${dateList}" varStatus="status">
									<td colspan=""><span style="width:60px;">午餐<fmt:message key="calculate"/></span></td>
	 								<td colspan=""><span style="width:60px;">午餐<fmt:message key="after_the_reform_of"/></span></td>
	 								<td colspan=""><span style="width:60px;">晚餐<fmt:message key="calculate"/></span></td>
	 								<td colspan=""><span style="width:60px;">晚餐<fmt:message key="after_the_reform_of"/></span></td>
								</c:forEach>
							</tr>
							<tr>
<%-- 								<td><span style="width:60px;"><fmt:message key="calculate"/></span></td> --%>
<%--  								<td><span style="width:60px;"><fmt:message key="after_the_reform_of"/></span></td> --%>
<%--  								<td><span style="width:60px;"><fmt:message key="calculate"/></span></td> --%>
<%--  								<td><span style="width:60px;"><fmt:message key="after_the_reform_of"/></span></td> --%>
<%--  								<td><span style="width:60px;"><fmt:message key="calculate"/></span></td> --%>
<%--  								<td><span style="width:60px;"><fmt:message key="after_the_reform_of"/></span></td> --%>
<%--  								<td><span style="width:60px;"><fmt:message key="calculate"/></span></td> --%>
<%--  								<td><span style="width:60px;"><fmt:message key="after_the_reform_of"/></span></td> --%>
							</tr>
						</thead>
					</table>
				</div>
				<c:set var="sum_dat" value="${0}"/>  <!-- 日期条数 -->
				<c:set var="sum_cnt2" value="${0}"/>
				<c:set var="sum_cnt2old" value="${0}"/>
				<c:set var="sum_cnt3" value="${0}"/>
				<c:set var="sum_cnt3old" value="${0}"/>
				<c:set var="sum_caltotal" value="${0}"/>
				<c:set var="sum_updtotal" value="${0}"/>
				<div class="table-body">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>	
							<c:forEach var="ips" items="${itemPlanList}" varStatus="status">
								<tr>
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td>
										<span style="width:20px; text-align: center;">
											<input type="checkbox" name="idList" id="chk_${ips.item}" value="${ips.item}"/>
										</span>
									</td>
									<td><span title="${ips.itcode}" style="width:50px;">${ips.itcode}</span></td>
									<td><span title="${ips.itdes }" style="width:80px;">${ips.itdes }</span></td>
									<td><span title="${ips.itunit }" style="width:25px;">${ips.itunit }</span></td>
 									<c:forEach var="ip" items="${ips.itemplanlist}" varStatus="status1">
 										<c:choose>
 											<c:when test="${ip['dat'] < itemPlan.date}">
		 								   		<td><span title="${ip['dat']}" style="width:60px;text-align: right;"><fmt:formatNumber value="${ip['cnt2old']}" pattern="#0.#"/></span></td>
												<td class="textInput"><span title="${ip['cnt2']}" style="width:60px;">
													<input type="text" value="<fmt:formatNumber value="${ip['cnt2']}" pattern="#0.#"/>" style="width:60px;text-align: right;padding: 0px;" disabled="disabled"/></span></td>
												<td><span title="${ip['dat']}" style="width:60px;text-align: right;"><fmt:formatNumber value="${ip['cnt3old']}" pattern="#0.#"/></span></td>
												<td class="textInput"><span title="${ip['cnt3']}" style="width:60px;">
													<input type="text" value="<fmt:formatNumber value="${ip['cnt3']}" pattern="#0.#"/>" style="width:60px;text-align: right;padding: 0px;" disabled="disabled"/></span></td>
 											</c:when>
 											<c:otherwise>
		 								   		<td><span title="${ip['dat']}" style="width:60px;text-align: right;"><fmt:formatNumber value="${ip['cnt2old']}" pattern="#0.#"/></span></td>
												<td class="textInput"><span title="${ip['cnt2']}" style="width:60px;">
													<input type="text" value="<fmt:formatNumber value="${ip['cnt2']}" pattern="#0.#"/>" style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
												<td><span title="${ip['dat']}" style="width:60px;text-align: right;"><fmt:formatNumber value="${ip['cnt3old']}" pattern="#0.#"/></span></td>
												<td class="textInput"><span title="${ip['cnt3']}" style="width:60px;">
													<input type="text" value="<fmt:formatNumber value="${ip['cnt3']}" pattern="#0.#"/>" style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
 											</c:otherwise>
 										</c:choose>
					
<%-- 									<td><span title="${itemplan['dat']}" style="width:60px;text-align: right;">${itemplan['cal']}</span></td> --%>
<%-- 										<td class="textInput"><span title="${itemplan['upd']}" style="width:60px;"> --%>
<%-- 											<input type="text" value="${itemplan['upd']}" style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td> --%>
									</c:forEach>
								   <td><span title="${ips.deptdes}" style="width:80px;">${ips.deptdes}</span></td>
								</tr>
								<c:set var="sum_dat" value="${status.index+1}"/>
								<c:set var="sum_cnt2old" value="${sum_cnt2old + ips.cnt2old }"/>
								<c:set var="sum_cnt2" value="${sum_cnt2 + ips.cnt2 }"/>
								<c:set var="sum_cnt3old" value="${sum_cnt3old + ips.cnt3old}"/>
								<c:set var="sum_cnt3" value="${sum_cnt3 + ips.cnt3}"/>
								<c:set var="sum_caltotal" value="${sum_caltotal + ips.caltotal}"/>
								<c:set var="sum_updtotal" value="${sum_updtotal + ips.updtotal}"/>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		   <div style="height:10px;">	
				<table border="0" cellspacing="0" cellpadding="0" style="margin-top:0;z-index:0;height:10px">
					<thead>
						<tr>
							<td style="width:55px;"></td>
							<td style="width:90px;"><fmt:message key="total"/></td>
							<td style="width:60px;">${sum_dat }</td>
							<td style="width:80px;"></td>
<%-- 							<td style="width:70px;"><fmt:formatNumber value="${sum_cnt2old }" pattern="#0.#"/></td> --%>
<%-- 							<td style="width:70px;"><label id="sum_cnt2"><fmt:formatNumber value="${sum_cnt2 }" pattern="#0.#"/></label></td> --%>
<%-- 							<td style="width:70px;"><fmt:formatNumber value="${sum_cnt3old }" pattern="#0.#"/></td> --%>
<%-- 						    <td style="width:70px;"><label id="sum_cnt3"><fmt:formatNumber value="${sum_cnt3 }" pattern="#0.#"/></label></td> --%>
<%-- 							<td style="width:70px;">${sum_caltotal }</td> --%>
<%-- 							<td style="width:70px;">${sum_updtotal }</td> --%>
						</tr>
					</thead>
				</table>
		   </div>
<%-- 			<page:page form="listForm" page="${pageobj}"></page:page> --%>
<%-- 			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" /> --%>
<%-- 			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" /> --%>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){	
			focus() ;//页面获得焦点
			
			if($('.grid').find('.table-body').find('.num').size()>0){
				loadToolBar([true,true,true,true,true]);
			}else {
				loadToolBar([true,false,false,false,false]);
			}
			
			if ("a"!=$("#msg").val()) {
				alert($("#msg").val());
			}
			//合计触发
			$('.grid').find('.table-body').find('tr').bind('keyup',function(){
				getTotalSum();
			});
		 	$(document).bind('keydown',function(e){//按钮快捷键
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
		 		if(window.event && window.event.keyCode == 118) { 
			 		window.event.keyCode = 505; 
		 		} 
		 		if(window.event && window.event.keyCode == 505){
		 			window.event.returnValue=false; 
		 		}; 
		 		if(e.altKey ==false){
		 			return;
		 		}
		 		switch (e.keyCode) {
	                case 70: $('#autoId-button-101').click(); break;
	                case 69: $('#autoId-button-102').click(); break;
	                case 83: $('#autoId-button-103').click(); break;
	                case 67: $('#autoId-button-104').click(); break;
	                case 68: $('#autoId-button-105').click(); break;
					case 80: $('#autoId-button-106').click(); break;
	            }
			});
		 	//排序结束
			$('#bdate').bind('click',function(){
				new WdatePicker();
			});

			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
		 	//控制按钮显示
			function loadToolBar(use){
				$('.tool').html('');
				$('.tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							if(DateDiff($("#edate").val(),$("#bdate").val())>7){
								alert('销售计划日期不能大于7天！');
								return;
							}
							$("#listForm").submit();
						}
					},'-',{
						text: '<fmt:message key="calculate" />',
						title: '<fmt:message key ="calculate" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'calculate')}&&use[0],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							calculate();
						}
					},{
						text: '<fmt:message key="save" />',
						title: '<fmt:message key ="save" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[1],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							saveUpdate();
						}
					},{
						text: '<fmt:message key="delete" />',
						title: '<fmt:message key ="delete" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')}&&use[2],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							if ($('.grid').find('.table-body').find(':checkbox').size()==0) {
								alert("请先查询，或者该日销售计划为空，请确认！");
							}else {
								if(confirm('确定删除当前所选营业日销售计划吗？')){
									$('#wait').show();
									$('#wait2').show();
									var selected = {};
									selected["bdate"] = $('#bdate').val();
									selected["edate"] = $('#edate').val();
									$.post('<%=path%>/posItemPlanChoice3/deletePlan.do',selected,function(data){
										$('#wait').hide();
										$('#wait2').hide();
										if(data ==1){
											alert('<fmt:message key="operation_successful" />!');
											$('#listForm').submit(); 
										}else{
											alert('<fmt:message key="operation_failed" />!');
										}
									});
								}
							}
						}
					},'-',{
						text: 'Excel',
						title: 'Excel',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')}&&use[4],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							if(DateDiff($("#edate").val(),$("#bdate").val())>7){
								alert('销售计划日期不能大于7天！');
								return;
							}
							$("#wait2").val('NO');//不用等待加载
							$('#listForm').attr('action','<%=path%>/posItemPlanChoice3/exportPlan.do');
							$('#listForm').submit();
							$('#listForm').attr('target','');
							$('#listForm').attr('action','<%=path%>/posItemPlanChoice3/planList.do');
							$("#wait2").val('');//等待加载还原
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}]
				});
		 	}
			
			//自动实现滚动条 				
			setElementHeight('.grid',['.tool'],$(document.body),79);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();		
  	 		/*弹出树*/
			$('#seachDept').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#dept').val();
					var defaultName = $('#deptdes').val();
					//alert(defaultCode+"==="+defaultName);
					var offset = getOffset('dept');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/findPositnSuper.do?typn='+'7&iffirm=1&mold='+'oneTmany&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deptdes'),$('#dept'),'760','520','isNull');
				}
			});
		
		});	
		
		
		//保存登记
		function saveUpdate(){
			var len = $('#listStrSize').val();
			var selected = {};
			var checkList = $("input[name='idList']").filter(':checked');
			if(checkList && checkList.size() > 0){
				// if(confirm('<fmt:message key="only_checked_saved_whether_continue" />!')){
				$('#wait').show();
				$('#wait2').show();
				var i = 0;
				checkList.each(function(){
					for(var j=0;j< len ;j++){
						selected['listItemPlan['+i+'].item'] = $(this).val();
						selected['listItemPlan['+i+'].dat'] = $(this).parents('tr').find('td:eq('+ (4*j+5) +')').find('span').attr('title');
// 						var updtotal = $(this).parents('tr').find('td:eq('+ (2*j+6) +')').find('input').val();
						var cnt2 = $(this).parents('tr').find('td:eq('+ (4*j+6) +')').find('input').val();
						var cnt3 = $(this).parents('tr').find('td:eq('+ (4*j+8) +')').find('input').val();
// 						if(updtotal == null || updtotal == ''){
// 							updtotal = 0;
// 						}
						if(cnt2 == null || cnt2 == ''){
							cnt2 = 0;
						}
						if(cnt3 == null || cnt3 == ''){
							cnt3 = 0;
						}
						selected['listItemPlan['+i+'].cnt2'] = Number(cnt2);
						selected['listItemPlan['+i+'].cnt3'] = Number(cnt3);
// 						selected['listItemPlan['+i+'].updtotal'] = Number(updtotal);
						i++;
					}
				});
				$.post('<%=path%>/posItemPlanChoice3/updatePlan.do',selected,function(data){
					$('#wait').hide();
					$('#wait2').hide();
					if($("#isDeclare").val() == null || $("#isDeclare").val() !="1"){
						if(data ==1){
							alert('<fmt:message key="operation_successful" />!');
							$('#listForm').submit(); 
						}else{
							alert('<fmt:message key="operation_failed" />!');
						}
					}
				});
			// }
				return true;
			}else{
				alert('<fmt:message key="please_select_options_you_need_save" />！');
				return false;
			}
		}
		
		function calculate(){
			if (confirm('确认重新计算菜品销售计划吗？')) {
				if(DateDiff($("#edate").val(),$("#bdate").val())>7){
					alert('预估日期不能大于7天！');
					return;
				}
				var action = "<%=path%>/posItemPlanChoice3/calPlan.do";
				if($("#isDeclare").val() == 1){
					 var json_data = JSON.stringify(window.parent.declareGoods.salePlanList);
					 $("#salePlan").val(json_data);
				}
				$('#listForm').attr('action',action);
				$('#listForm').submit(); 
			}
		}
		
		// 检查最高最低库存是否有效数字
		function checkNum(inputObj){
			if(isNaN(inputObj.value)){
				alert("<fmt:message key='invalid_number'/>！");
				inputObj.focus();
				return false;
			}
		}
		//合计计算
		function getTotalSum(){//计算统计数据
			var sum_cnt2 = 0;
			var sum_cnt3 = 0; 
			$('.table-body').find('tr').each(function (){
				if($(this).find('td:eq(1)').text()!=''){//非空行
					var cnt2 = $(this).find('td:eq(6)').find('input').val();
					    sum_cnt2 += Number(cnt2);
					var cnt3 = $(this).find('td:eq(8)').find('input').val();
				        sum_cnt3 += Number(cnt3);
				}
			});
			$('#sum_cnt2').text(sum_cnt2);//午餐总数量
			$('#sum_cnt3').text(sum_cnt3);//晚餐总数量
		}
		//日期计算
		function DateDiff(sDate1, sDate2)
		{ 
		    var aDate, oDate1, oDate2, iDays;
		    aDate = sDate1.split("-");
		    oDate1 = new Date(aDate[1] + '/' + aDate[2] + '/' + aDate[0]); //转换为12-18-2002格式
		    aDate = sDate2.split("-");
		    oDate2 = new Date(aDate[1] + '/' + aDate[2] + '/' + aDate[0]);
		    iDays = parseInt((oDate1 - oDate2) / 1000 / 60 / 60 /24); //把相差的毫秒数转换为天数
		    return iDays;
		}
		</script>
	</body>
</html>