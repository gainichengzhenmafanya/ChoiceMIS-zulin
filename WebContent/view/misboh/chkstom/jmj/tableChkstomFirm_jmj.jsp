<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="audit_filled_reported_manifest"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			
		<style type="text/css">
		.page{
			margin-bottom: 25px;
		}
		.test{
			color:blue;
		}
		</style>
	</head>
	<body>
<!-- 		<div class="tool"> -->
<!-- 		</div> -->
		<%--存放一个状态 判断是何种操作类型 --%>
		<form id="listForm" action="<%=path%>/chkstomMisJMJ/toChkstom.do" method="post">
			<input type="hidden" name="idList" value="${idList}"/>
			<input type="hidden" id="firm" name="firm" value="${chkstom.firm}"/>
			<div class="bj_head">
				<div class="form-line">
					<div class="form-label"><fmt:message key="date"/>：</div>
					<div class="form-input">
						<input type="text" id="maded" name="maded" class="Wdate text" value="<fmt:formatDate value="${chkstom.maded}" pattern="yyyy-MM-dd"/>" onfocus="new WdatePicker({minDate:new Date()})"/>
					</div>
					<div class="form-label"><fmt:message key="orders_num"/>:</div>
					<div class="form-input">
						<c:if test="${chkstom.chkstoNo!=null}"><c:out value="${chkstom.chkstoNo }"></c:out></c:if>
						<input type="hidden" id="chkstoNo" name="chkstoNo" class="text" value="${chkstom.chkstoNo }"/> 					
					</div>
					
				</div>
				<div class="form-line">
					<div class="form-label" style="color:red;"><fmt:message key="arrival_date"/>:</div>
					<div class="form-input">
						<input type="text" name="hoped" id="hoped" class="Wdate text" value="<fmt:formatDate value="${chkstom.hoped}" pattern="yyyy-MM-dd"/>" onfocus="new WdatePicker({minDate:new Date()})"/>
					</div>
					<div class="form-label"><fmt:message key="make_orders"/>:</div>
					<div class="form-input">
						<c:if test="${chkstom.madeby!=null}"><c:out value="${chkstom.madebyName}"></c:out></c:if>					
						<input type="hidden" id="madeby" name="madeby" class="text" value="${chkstom.madeby }"/>
					</div>		
				</div>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 26px;"><span >&nbsp;</span></td>
								<td colspan="3"><fmt:message key="supplies"/></td>
								<td colspan="2">采购单位</td>
								<td colspan="4"><fmt:message key="standard_unit"/></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="remark"/></span></td>
							</tr>
							<tr>									
								<td class="num" style="width: 26px;"><span >&nbsp;</span></td>
								<td><span style="width:70px;"><fmt:message key="supplies_code"/></span></td>
								<td><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
								<td><span style="width:90px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:70px;"><fmt:message key="quantity"/></span></td>								
								<td><span style="width:55px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:50px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:50px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:70px;"><fmt:message key="unit_price"/></span></td>
								<td><span style="width:80px;"><fmt:message key="amount"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<c:set var="sum_num" value="${0}"/>  <!-- 物资条数 -->
				<c:set var="sum_amount" value="${0}"/>  <!-- 总数量 -->
				<div class="table-body" style="height: 100%">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>
						<c:forEach var="chkstod" items="${chkstodList}" varStatus="status">
							<tr>
								<td align="center" style="width:26px;"><span >${status.index+1}</span></td>
								<td><span style="width:70px;">${chkstod.supply.sp_code }</span></td>
								<td><span style="width:100px;">${chkstod.supply.sp_name }</span></td>
								<td><span style="width:90px;">${chkstod.supply.sp_desc }</span></td>
								<td><span style="width:70px;text-align: right;"><fmt:formatNumber value="${chkstod.amount1}" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:55px;">${chkstod.supply.unit3 }</span></td>
								<td><span style="width:50px;text-align: right;"><fmt:formatNumber value="${chkstod.amount}" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:50px;">${chkstod.supply.unit }</span></td>
								<td><span style="width:70px;text-align: right;"><fmt:formatNumber value="${chkstod.price}" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:80px;text-align: right;"><fmt:formatNumber value="${chkstod.price*chkstod.amount}" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:70px;">${chkstod.memo}</span></td>
							</tr>
							<c:set var="sum_num" value="${status.index+1}"/>  
							<c:set var="sum_amount" value="${sum_amount + chkstod.amount1}"/>   
							<c:set var="sum_price" value="${sum_price + chkstod.price*chkstod.amount}"/>   
						</c:forEach>
						</tbody>
					</table>					
				</div>
			</div>
			<div style="height:10px;">	
				<table border="0" cellspacing="0" cellpadding="0" style="margin-top:0;z-index:0;height:10px">
					<thead>
						<tr>
							<td style="width:26px;"></td>
							<td style="width:80px;background:#F1F1F1;"><fmt:message key="total"/><u>&nbsp;&nbsp;<label id="sum_num">${sum_num}</label>&nbsp;&nbsp;</u></td>
							<td style="width:210px;"></td>
							<td style="width:110px;background:#F1F1F1;"><fmt:message key="quantity"/><u>&nbsp;&nbsp;<label id="sum_amount"><fmt:formatNumber value="${sum_amount}" type="currency" pattern="0.00"/></label>&nbsp;&nbsp;</u></td>
							<td style="width:245px;"></td>
							<td style="width:150px;background:#F1F1F1;"><fmt:message key="total_amount"/><u>&nbsp;&nbsp;<label id="sum_price"><fmt:formatNumber value="${sum_price}" type="currency" pattern="0.00"/></label>&nbsp;&nbsp;</u></td>
							<td style="width:15px;"></td>
						</tr>
					</thead>
				</table>
		   </div>
		</form>	
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		var validate;
		//工具栏
		$(document).ready(function(){
			//按钮快捷键
			focus() ;//页面获得焦点
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		    $('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),80);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
		});
		
		//统一修改到货日期
		function updateHoped(){
			var html = '<div style="margin:50px 100px;"><span style="color:red;font-size:24px;">请正确选择到货日期！！！</span><br><input type="text" id="hoped1" class="Wdate text" value="<fmt:formatDate value="${null == chkstom.hoped ? '' : chkstom.hoped}" pattern="yyyy-MM-dd"/>" onfocus="new WdatePicker({minDate:new Date()})" style="width:150px;"/></div>';
			$('body').window({
				id: 'window_updateHoped',
				//title: '<fmt:message key="all_the_revised_delivery_date"/>',
				title: '单据到货日期确认',
				content: html,
				width: '400px',//'1000px'
				height: '300px',
				draggable: true,
				isModal: true,
				confirmClose: false,
				topBar: {
					items: [{
							text: '<fmt:message key="enter"/>',
							title: '<fmt:message key="enter"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								if(setHoped()){
									$('.close').click();
									saveChkstom();
								}
							}
						},{
							text: '<fmt:message key="cancel"/>',
							title: '<fmt:message key="cancel"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.close').click();
							}
						}]
				}
			});
		}
		
		//统一修改到货日期
		function setHoped(){
			$('#hoped').val($('#hoped1').val());
			if($('#hoped1').val() == ''){
				alert('请填写到货日期！');
				return false;
			}
			if(!confirm('是否保存到货日期为【'+$('#hoped1').val()+'】的报货单?')){
				return false;
			}
			return true;
		}
		
		//保存添加
		function saveChkstom(){
			if($('#hoped').val() == ''){
				alert('请填写到货日期！');
				return;
			}
			var numNull=0;//默认0代表成功
			var isNull=0;
			var sp_name = '';
			$('.table-body').find('tr').each(function(){
				if($(this).find('td:eq(1)').text()!=''){
					isNull=1;
					var amount=$(this).find('td:eq(4)').text()?$(this).find('td:eq(4)').text():$(this).find('td:eq(4) input').val();
					if(Number(amount)==0){
						numNull=1;//1数量为0
						sp_name = $(this).find('td:eq(2)').text()?$(this).find('td:eq(2)').text():$(this).find('td:eq(2) input').val();
						return false;
					}
					if(amount=="" || amount==null || isNaN(amount)){
						numNull=2;//2数量为空
						sp_name = $(this).find('td:eq(2)').text()?$(this).find('td:eq(2)').text():$(this).find('td:eq(2) input').val();
						return false;
					}
				}
			});
			if(Number(isNull)==0){
				alert('不能加入空单据！');
				return;
			}
			if(Number(numNull)==1){//数量不为0
				alert('物资：['+sp_name +']<fmt:message key="number_cannot_be_zero"/>！');
				return;
			}
			if(Number(numNull)==2){//数量为空或字母
				alert('物资：['+sp_name +']数量不是有效数字！');
				return;
			}
			var keys = ["supply.sp_code","supply.sp_name","supply.sp_desc","amount1","supply.unit3","amount","supply.unit",
			            "price","totalAmt","hoped","memo"];
			var data = {};
			var i = 0;
			$("#listForm *[name]").each(function(){
				var name = $(this).attr("name"); 
				if(name) data[name] = $(this).val();
			});
			var rows = $(".grid .table-body table tr");
			for(i=0;i<rows.length;i++){
				var status=$(rows[i]).find('td:eq(0)').text();
				data["chkstodList["+i+"]."+"price"] = $(rows[i]).data('price') ? $(rows[i]).data('price'):$('#price_'+status).val();
				cols = $(rows[i]).find("td");
				var j = 0;
				for(j=1;j <= keys.length;j++){
					var value = $.trim($(rows[i]).find("td:eq("+j+")").text());
					value = value ? value : $.trim($(rows[i]).find("td:eq("+j+") input").val());					
					if(value)
						data["chkstodList["+i+"]."+keys[j-1]] = value;
				}
				data["chkstodList["+i+"]."+"hoped"] = $('#hoped').val();
			}
			//提交并返回值，判断执行状态
			$.post("<%=path%>/chkstomMisJMJ/saveByAddOrUpdateDept.do",data,function(data){
				var rs = eval('('+data+')');
				switch(Number(rs.pr)){
				case -1:
					showMessage({
								type: 'error',
								msg: '<fmt:message key="save_fail"/>！',
								speed: 1000
								});
					parent.pageReload();
					break;
				case 1:
					alert('上传成功！');
					parent.pageReload();
				}
			});			 
		}
		
		//获取系统时间
		function getDate(){
			var myDate=new Date();  
			var yy=myDate.getFullYear();
			var MM=myDate.getMonth()+1;
			var dd=myDate.getDate();
			if(MM<10)
				MM="0"+MM;
			if(dd<10)
				dd="0"+dd;
			return fullDate=yy+"-"+MM+"-"+dd;
		}
		</script>			
	</body>
</html>