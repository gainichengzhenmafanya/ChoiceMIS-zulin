<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>供应商邮件查询</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
		.page{
			margin-bottom:25px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		.bgBlue{background-color:lightblue;}
		form .form-line .form-label:first-child{
			margin-left: 0px;
			width: 40px;
		}
		form .form-line .form-label{
			width: 60px;
			margin-left: 20px;
		}
		form .form-line .form-input{
			width: 100px;
			display: inline-block;
		}
		form .form-line .form-input , form .form-line .form-input input[type=text]{
			width: 100px;
		}
		</style>				
	</head>
	<body>
		<div class="tool"></div>	
			<form id="SearchForm" action="<%=path%>/chkstomMisJMJ/listDeliverMail.do" method="post">
				<input type="hidden" id="firm" name="firm" value="${pd.firm}"/>
				<div class="form-line">
					<div class="form-label" style="width:60px;"><fmt:message key="startdate"/></div>
					<div class="form-input"><input style="width:85px;" type="text" id="bdate" name="bdate" class="Wdate text" value="${pd.bdate}" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edate\')}'})"/></div>			
					<div class="form-label">JDE<fmt:message key="orders_num"/></div>
					<div class="form-input"><input type="text" id="jdeno" name="jdeno" class="text" value="${pd.jdeno}" onkeyup="this.value=this.value.replace(/\D/g,'')"/></div>
					<div class="form-label"><fmt:message key="arrival_date"/></div>
					<div class="form-input">
						<input type="text" name="ind" id="ind" class="Wdate text" value="${pd.ind}" onclick="new WdatePicker()"/>
					</div>
					<div class="form-label"><fmt:message key="supplies_code"/></div>
					<div class="form-input"><input type="text" id="sp_code" name="sp_code" class="text" value="${sp_code }"/>
					<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label" style="width:60px;"><fmt:message key="enddate"/></div>
					<div class="form-input"><input style="width:85px;" type="text" id="edate" name="edate" class="Wdate text" value="${pd.edate}" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdate\')}'})"/></div>
					<div class="form-label">凭证号</div>
					<div class="form-input"><input type="text" id="vouno" name="vouno" class="text" value="${pd.vouno }"/></div>
					<div class="form-label"><fmt:message key="suppliers"/></div>
					<div class="form-input">
						<input type="text"  id="deliverDes" name="deliverDes" readonly="readonly" value="${pd.deliverDes}"/>
						<input type="hidden" id="deliver" name="deliver" value="${pd.deliver}"/>
						<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key='query_suppliers'/>' />
					</div>
				</div>
				<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width:25px;">&nbsp;</span></td>
								<td><span style="width:80px;">JDE单号</span></td>
								<td><span style="width:150px;"><fmt:message key="document_number"/></span></td>
								<td><span style="width:60px;">供应商编码</span></td>
								<td><span style="width:100px;">供应商名称</span></td>
								<td><span style="width:80px;">物资编码</span></td>
								<td><span style="width:100px;">物资名称</span></td>
								<td><span style="width:100px;">物资规格</span></td>
								<td><span style="width:40px;">单位</span></td>
								<td><span style="width:60px;">数量</span></td>
								<td><span style="width:80px;">报货<fmt:message key="date"/></span></td>
								<td><span style="width:80px;"><fmt:message key="arrival_date"/></span></td>
								<td><span style="width:80px;">更新<fmt:message key="date"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="p" items="${pdList }" varStatus="status">
								<tr>
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td><span style="width:80px;">${p.jdeno }</span></td>
									<td><span style="width:150px;">${p.vouno }</span></td>
									<td><span style="width:60px;">${p.deliver }</span></td>
									<td><span style="width:100px;">${p.deliverDes }</span></td>
									<td><span style="width:80px;">${p.sp_code }</span></td>
									<td><span style="width:100px;">${p.sp_name }</span></td>
									<td><span style="width:100px;">${p.sp_desc }</span></td>
									<td><span style="width:40px;">${p.unit }</span></td>
									<td><span style="width:60px;text-align:right"><fmt:formatNumber type="currency" value="${p.amount }" pattern="#0.00"/></span></td>
									<td><span style="width:80px;">${p.dat }</span></td>
									<td><span style="width:80px;">${p.ind }</span></td>
									<td><span style="width:80px;">${p.madat }</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>				
			</div>
			<page:page form="SearchForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		var validate;
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		$(document).ready(function(){
			//按钮快捷键
			focus() ;//页面获得焦点
		 	$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			parent.$('.close').click();
		 		}
			}); 
			var tool = $('.tool').toolbar({
				items: [{
					text: '<fmt:message key="select" />',
					title: '<fmt:message key="query_messages_manifest"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						$("#SearchForm").submit();
					}
				},{
					text: '<fmt:message key="quit"/>',
					title: '<fmt:message key="quit"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
					}
				}]
			});					
		 	//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     }
			     else {
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			     }
			});
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		    $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),100);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法		
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/misbohcommon/selectSupplyLeft.do?positn=1&defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
			
			$('#seachDeliver').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#deliver').val();
					var defaultName = $('#deliverDes').val();
					var offset = getOffset('bdate');
					top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/misbohcommon/selectOneDeliver.do?gysqx=2&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deliverDes'),$('#deliver'),'900','500','isNull');
				}
			});
		});
		function pageReload(){
	    	$('#SearchForm').submit();
		}
		</script>				
	</body>
</html>