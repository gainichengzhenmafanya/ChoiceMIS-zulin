<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="purchase_the_template" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<style type="text/css">
		.blueBGColor{background-color:	#F1F1F1;}
		.redBGColor{background-color:	#F1F1F1;}
		.onEdit{
			background:lightblue;
			border:1px solid;
			border-bottom-color: blue;
			border-top-color: blue;
			border-left-color: blue;
			border-right-color: blue;
		}
		.input{
			background:transparent;
			border:1px solid;
		}
		</style>		
</head>
<body>
	<div class="tool"></div>
	<input type="hidden" id="subSta" name="subSta"/>
	<form id="listForm" action="<%=path%>/chkstomMis/addChkstoDemo.do" method="post">
		<div class="form-line">	
			<div class="form-label"><fmt:message key="title" /></div>
			<div class="form-input">
				<input type="hidden" id="firm" name="firm" value="${firm}"/><!-- 适用分店wjf -->
				<select class="select" id="chkstodemono" name="chkstodemono" onchange="findByTitle(this);">
<%-- 					<option value=""><fmt:message key="all" /></option>				 --%>
					<c:forEach var="chkstodemo" items="${listTitle}" varStatus="status">
						<option
							<c:if test="${chkstodemo.chkstodemono==chkstodemono}"> selected="selected" </c:if> value="${chkstodemo.chkstodemono}">${chkstodemo.title}
						</option>
					</c:forEach>				
				</select>
			</div>
		</div>
	
		<div class="grid">		
			<div class="table-head" >
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<td class="num"><span style="width: 25px;"></span></td>
							<td><span style="width:80px;"><fmt:message key="supplies_code" /></span></td>
							<td><span style="width:120px;"><fmt:message key="supplies_name" /></span></td>
							<td><span style="width:80px;"><fmt:message key="specification" /></span></td>
							<td><span style="width:80px;"><fmt:message key="purchase_quantity" /></span></td>
							<td><span style="width:60px;">
								<c:choose>
									<c:when test="${isDistributionUnit == 'Y' }"><fmt:message key="Distribution_Unit"/></c:when>
									<c:otherwise><fmt:message key="procurement_unit" /></c:otherwise>
								</c:choose>
							</span></td>
							<td><span style="width:60px;"><fmt:message key="standard_unit" /></span></td>
							<td><span style="width:120px;"><fmt:message key="remark" /></span></td>
						</tr>
					</thead>
				</table>
			</div>				
			<div class="table-body">
				<table cellspacing="0" cellpadding="0">
					<tbody>
						<c:forEach var="chkstodemo" items="${chkstodemoList}" varStatus="status">
							<tr <c:choose>
									<c:when test="${isDistributionUnit == 'Y' }">data-mincnt="${chkstodemo.dismincnt}" data-unitper="${chkstodemo.disunitper }"</c:when>
									<c:otherwise>data-mincnt="${chkstodemo.supply.mincnt}" data-unitper="${chkstodemo.supply.unitper3 }"</c:otherwise>
								</c:choose>
								 data-price="<fmt:formatNumber value="${chkstodemo.price}" type="currency" pattern="0.00"/>">
								<td class="num"><span style="width: 25px;">${status.index+1}</span></td>
								<td title="${chkstodemo.supply.sp_code}"><span  style="width:80px;">${chkstodemo.supply.sp_code}</span></td>
								<td title="${chkstodemo.supply.sp_name}"><span  style="width:120px;">${chkstodemo.supply.sp_name}</span></td>
								<td title="${chkstodemo.supply.sp_desc}"><span  style="width:80px;">${chkstodemo.supply.sp_desc}</span></td>
								<td><span style="width:80px;text-align:right;"><fmt:formatNumber value="${chkstodemo.amount}" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:60px;">
									<c:choose>
										<c:when test="${isDistributionUnit == 'Y' }">${chkstodemo.disunit }</c:when>
										<c:otherwise>${chkstodemo.supply.unit3 }</c:otherwise>
									</c:choose>
								</span></td>
								<td><span style="width:60px;">${chkstodemo.supply.unit}</span></td>
								<td><span style="width:120px;">${chkstodemo.memo}</span></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
<%-- 		<page:page form="listForm" page="${pageobj}"></page:page> --%>
<%-- 		<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" /> --%>
<%-- 		<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />		 --%>
<!-- 	</form>				 -->
	</form>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
	<script type="text/javascript">
	var validate;
	//工具栏
	$(document).ready(function(){
		//按钮快捷键
		focus() ;//页面获得焦点
		
		//屏蔽鼠标右键
	 	$(document).bind('keyup',function(e){
	 		if(e.keyCode==27){
	 			parent.$('.close').click();
	 		}
	 		//表格数量一变，校验一下，价格就接着变
	 		if($(e.srcElement).is("input")){//对表格内的输入框进行判读，延迟600毫秒
	 			var index=$(e.srcElement).closest('td').index();
	    		if(index=="4"){
		 			$(e.srcElement).unbind('blur').blur(function(e){
		 				validateByMincnt($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
		 			});
		    		validator($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
	    		}
	    	}
		});
	 	
	   $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
	   $('.grid').find('.table-body').find('tr').hover(
			function(){
				$(this).addClass('tr-over');
			},
			function(){
				$(this).removeClass('tr-over');
			}
		);
		
		//自动实现滚动条
		setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
		setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
		loadGrid();//  自动计算滚动条的js方法	
		editCells();
		$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),4);
	});
	
	//动态下拉列表框
	function findByTitle(){
		$('#listForm').submit();
	}
	//确认修改
	function enterUpdate() {
		var checkboxList = $('.grid').find('.table-body').find('tr');
		var data = {};
		var i = 0;
		var totalAmt = 0;
		var totalPrice = 0;
		checkboxList.each(function(){
			//1.如果数量是0的过滤掉
			var amount = $(this).find('td:eq(4)').text()?$(this).find('td:eq(4)').text():$(this).find('td:eq(4) input').val();
			if(Number(amount) == 0  || amount == null || amount == ''){
				return true;
			}
			//2.如果已经存在的物资过滤掉
			var sp_code = $(this).find('td:eq(1) span').text();
			var flag = true;
			parent.$('.table-body').find('tr').each(function (){
				if($(this).find("td:eq(1)").text() == sp_code){
					flag = false;
					return false;
				}
			});
			if(!flag){
				return true;
			}
			if(parent.$(".table-body").find("tr:last").find("td:eq(1)").find('span').text() != ''){
				i++;
				parent.$(".table-body").autoGrid.addRow();
			}
			var unitper = $(this).data('unitper');
			var mincnt = $(this).data('mincnt');
			var price = $(this).data('price');
			var sp_name = $(this).find('td:eq(2) span').text();
			parent.$(".table-body").find("tr:last").find("td:eq(1)").find('span').text(sp_code).data('sp_name',sp_name);
			parent.$(".table-body").find("tr:last").find("td:eq(2)").find('span').text(sp_name);
			parent.$(".table-body").find("tr:last").find("td:eq(3)").find('span').text($(this).find('td:eq(3) span').text());
			parent.$(".table-body").find("tr:last").find("td:eq(4)").find('span').text(Number(amount).toFixed(2)).attr('title',amount).css("text-align","right");
			parent.$(".table-body").find("tr:last").find("td:eq(5)").find('span').text($(this).find('td:eq(5)').text());
			var cnt = 0;
			if(Number(unitper) != 0){
				cnt = Number(amount)/Number(unitper);
			}
			parent.$(".table-body").find("tr:last").find("td:eq(6)").find('span').text(cnt.toFixed(2)).attr('title',cnt).css("text-align","right");
			parent.$(".table-body").find("tr:last").find("td:eq(7)").find('span').text($(this).find('td:eq(6)').text());
			parent.$(".table-body").find("tr:last").find("td:eq(8)").find('span').text(Number(price).toFixed(2)).attr('title',price).css("text-align","right");
			parent.$(".table-body").find("tr:last").find("td:eq(9)").find('span').text((Number(amount)*Number(price)).toFixed(2)).css("text-align","right");
			parent.$(".table-body").find("tr:last").find("td:eq(10)").find('span').text(getInd());
			parent.$(".table-body").find("tr:last").find("td:eq(11)").find('span').text($(this).find('td:eq(7)').text()?$(this).find('td:eq(7)').text():$(this).find('td:eq(7) input').val());
			parent.$(".table-body").find("tr:last").data("unitper",unitper);
			parent.$(".table-body").find("tr:last").data("mincnt",mincnt);
			totalAmt = Number(totalAmt) + Number(amount);
			totalPrice = Number(totalPrice) + (Number(amount)*Number(price));
		});
		parent.$('#sum_num').text(Number(parent.$('#sum_num').text()) + i);
		parent.$('#sum_amount').text((Number(parent.$('#sum_amount').text()) + totalAmt).toFixed(2));
		parent.$('#sum_price').text((Number(parent.$('#sum_price').text()) + totalPrice).toFixed(2));
	    //关闭弹出窗口
		parent.$('.close').click();
	}
	//编辑表格
	function editCells(){
		if($(".table-body").find('tr').length == 0){
			return;
		}
		$(".table-body").autoGrid({
			initRow:1,
			colPerRow:8,
			widths:[26,80,120,80,80,60,60,120],
			colStyle:['','','','',{background:"#F1F1F1"},'','',{background:"#F1F1F1"}],
			VerifyEdit:{verify:true,enable:function(cell,row){
				return row.find('td').index(cell) == 2 || (row.find("td:eq(2)").text() == null || $.trim(row.find("td:eq(2)").text()) != '' ) ? true : false;
			}},
			onEdit:$.noop,
			editable:[4,7],//能输入的位置
			onLastClick:$.noop,
			onEnter:function(data){
				$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.value) ;
			},
			cellAction:[{
				index:4,
				action:function(row,data2){
					if(Number(data2.value) < 0){
						alert('数量不能为负数！');
						row.find("td:eq(4)").find('span').text(data2.ovalue);
						$.fn.autoGrid.setCellEditable(row,4);
					}else if(isNaN(data2.value)){
						alert('<fmt:message key="number_be_not_number"/>！');
						row.find("td:eq(4)").find('span').text(data2.ovalue);
						$.fn.autoGrid.setCellEditable(row,4);
					}else if(Number(row.data("mincnt"))!=0 && !(/(^[1-9]\d*$)/.test(Number(data2.value)/Number(row.data("mincnt"))))){
						alert('该物质最低申购数量为'+row.data("mincnt")+"，申购量必须是最小申购量的整数倍!");
						row.find("td:eq(4)").find('span').text(data2.ovalue);
						$.fn.autoGrid.setCellEditable(row,4);
					} else {
						$.fn.autoGrid.setCellEditable(row.next(),4);
					}
				}
			}]
		});
	}
	
	function validateByMincnt(index,row,data2){//最小申购量判断
		if(index=="4"){
			if(isNaN(data2.value)||Number(data2.value) < 0){
				alert('<fmt:message key="please_enter_positive_integer"/>！');
				row.find("input").focus().select();
			}else if(Number(row.data("mincnt"))!=0 && !(/(^[1-9]\d*$)/.test(Number(data2.value)/Number(row.data("mincnt"))))){
				alert('该物质最低申购数量为'+row.data("mincnt")+"，申购量必须是最小申购量的整数倍!");
				row.find("input").val(data2.value).focus().select();
			}else{
				row.find("td:eq(4)").find('span').text(Number(data2.value).toFixed(2));
			}
		}
	}
	function validator(index,row,data2){//输入框验证
		if(index=="4"){
			if(isNaN(data2.value)||Number(data2.value) < 0){
				data2.value=0;
			}
		}
	}
	
	//获取系统时间
	function getInd(){
		var myDate=new Date(new Date().getTime() + 24*60*60*1000);  
		var yy=myDate.getFullYear();
		var MM=myDate.getMonth()+1;
		var dd=myDate.getDate();
		if(MM<10)
			MM="0"+MM;
		if(dd<10)
			dd="0"+dd;
		return fullDate=yy+"-"+MM+"-"+dd;
	}
	</script>
</body>
</html>