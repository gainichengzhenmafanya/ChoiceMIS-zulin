<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>MIS-查询已审核报货单</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.page{margin-bottom: 25px;}
			.search{
				margin-top:3px;
				cursor: pointer;
			}
			form .form-line:first-child{
				margin-left: 0px;
			}
			form .form-line .form-label{
				width: 70px;
			}
			form .form-line .form-input , form .form-line .form-input input[type=text]{
				width: 100px;
			}	
			.table-head td span{
				white-space: normal;
			}
		</style>					
	</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/chkstomMis/listCheckedChkstom.do" method="post">
			<input type="hidden" id="firm" name="firm" value="${chkstom.firm }"/>
			<div class="form-line">
				<div class="form-label"><fmt:message key="startdate"/>：</div>
				<div class="form-input"><input autocomplete="off" type="text" id="bdate" name="bMaded" class="Wdate text" value="<fmt:formatDate value="${chkstom.bMaded}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edate\')}'})"/></div>						
				<div class="form-label"><fmt:message key="orders_num"/>：</div>
				<div class="form-input"><input type="text" id="chkstoNo2" name="chkstoNo" class="text" value="${chkstom.chkstoNo}" onkeyup="this.value=this.value.replace(/\D/g,'')"/></div>
				<div class="form-label"><fmt:message key="document_number"/>：</div>
				<div class="form-input"><input type="text" id="vouno" name="vouno" class="text" value="${chkstom.vouno}" /></div>
<%-- 				<div class="form-label"><fmt:message key="supplies_code"/>：</div> --%>
<!-- 				<div class="form-input" style="width:125px;"> -->
<%-- 					<input type="text" id="sp_code" name="sp_code" class="text" onkeyup="ajaxSearch('sp_code')" value="${sp_code }"/> --%>
<%-- 					<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' /> --%>
<!-- 				</div> -->
<%--  				<div class="form-label"><fmt:message key="purchase_positions"/>：</div> --%>
<!-- 				<div class="form-input"> -->
<%-- 					<input type="text" class="text" id="firm" name="positn.des" readonly="readonly" value="${chkstom.positn.des}"/> --%>
<%-- 					<input type="hidden" id="firmDes" name="firm" value="${chkstom.positn.code}"/> --%>
<!-- 				</div> -->
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="enddate"/>：</div>
				<div class="form-input"><input autocomplete="off" type="text" id="edate" name="eMaded" class="Wdate text" value="<fmt:formatDate value="${chkstom.eMaded}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdate\')}'})"/></div>
				<div class="form-label"><fmt:message key="orders_maker"/>：</div>
				<div class="form-input"><input type="text" id="madeby" name="madeby" class="text" value="${chkstom.madeby }"/></div>
				<div class="form-label"><fmt:message key="orders_author"/>：</div>
				<div class="form-input"><input type="text" id="checby" name="checby" class="text" value="${chkstom.checby }"/></div>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width:25px;"></span></td>
								<td><span style="width:20px;"><!-- <input type="checkbox" id="chkAll"/> --></span></td>
								<td><span style="width:70px;"><fmt:message key="reported_num"/></span></td>
								<td style="width:150px;"><fmt:message key="document_number"/></td>
								<td><span style="width:130px;"><fmt:message key="reported_date"/></span></td>
								<td><span style="width:130px;"><fmt:message key="fill_time"/></span></td>
<%-- 								<td><span style="width:100px;"><fmt:message key="purchase_branche"/></span></td> --%>
								<td <c:if test="${isNotShowSp_price == 'Y' }">style="display:none;"</c:if>><span style="width:80px;"><fmt:message key="total_amount1"/></span></td>
								<td><span style="width:100px;"><fmt:message key="orders_maker"/></span></td>
								<td><span style="width:80px;"><fmt:message key="orders_author"/></span></td>
								<td><span style="width:130px;"><fmt:message key="submit"/><fmt:message key="time"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="chkstom" items="${chkstomList }" varStatus="status">
								<tr>
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td><span style="width:20px; text-align: center;"><input type="checkbox" name="idList" id="chk_${chkstom.chkstoNo }" value="${chkstom.chkstoNo }"/></span></td>									
									<td><span style="width:70px;">${chkstom.chkstoNo }</span></td>
									<td><span style="width:140px;">${chkstom.vouno }</span></td>
									<td><span style="width:130px;"><fmt:formatDate value="${chkstom.maded}" pattern="yyyy-MM-dd" type="date"/></span></td>
									<td><span style="width:130px;">${chkstom.madet}</span></td>
<%-- 									<td><span style="width:100px;">${chkstom.positn.des }</span></td> --%>
									<td <c:if test="${isNotShowSp_price == 'Y' }">style="display:none;"</c:if>><span style="width:80px;text-align: right;"><fmt:formatNumber value="${chkstom.totalAmt }" pattern="##.#" minFractionDigits="2" ></fmt:formatNumber></span></td>
									<td><span style="width:100px;">${chkstom.madebyName }</span></td>
									<td><span style="width:80px;">${chkstom.checbyName }</span></td>
									<td><span style="width:130px;"><fmt:formatDate value="${chkstom.checd}" pattern="yyyy-MM-dd HH:mm:ss " type="date"/></span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>				
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		var validate;
		$(document).ready(function(){
			if($('#chkstoNo2').val()==0)$('#chkstoNo2').val('');
			//双击
			$('.grid .table-body tr').live('dblclick',function(){
				$(":checkbox").attr("checked", false);
				$(this).find(":checkbox").attr("checked", true);
				searchCheckedChkstom();
			});
			//双击
			$('.grid .table-body tr').live('click',function(){
				$(":checkbox").attr("checked", false);
				$(this).find(":checkbox").attr("checked", true);
			});
			//按钮快捷键
			focus() ;//页面获得焦点
		 	$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode){
	                case 70: $('.<fmt:message key="select"/>').click(); break;
	                case 80: $('.<fmt:message key="print"/>').click(); break;
	            }
			});  
		 	$('.tool').toolbar({
				items: [{
					text: '<fmt:message key="select" />',
					title: '<fmt:message key="select"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						if(validate._submitValidate()){
							$("#listForm").submit();
						}
					}
				},{
					text: '<fmt:message key="view" />',
					title: '<fmt:message key="view"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-140px','-100px']
					},
					handler: function(){
						searchCheckedChkstom();
					}
				},{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
					}
				}]
			});
		 	/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'chkstoNo2',
					validateType:['canNull','intege'],
					param:['T','T'],
					error:['','<fmt:message key="single_number_is_digital_or_empty"/>！']
				}]
			});
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		    $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),100);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法		
			changeTh();
		});
		//查看已审核单据的详细信息
		var detailWin;
		function searchCheckedChkstom(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() ==1){
					var chkValue = checkboxList.filter(':checked').val();
					detailWin = $('body').window({
						id: 'checkedChkstomDetail',
						title: '<fmt:message key="chkstom_detail" />',
						content: '<iframe id="checkedChkstomDetailFrame" frameborder="0" src="<%=path%>/chkstomMis/findDetail.do?chkstoNo='+chkValue+'"></iframe>',
						width: '85%',
						height: '460px',
						draggable: true,
						isModal: true,
						confirmClose:false
					});
			}else{
				alert('<fmt:message key="please_select_data_to_view"/>！');
				return ;
			}
		}
		//打印单条
		function printChkstom(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() ==1){
				var chkValue = [];
				checkboxList.filter(':checked').each(function(){
					chkValue.push($(this).val());
				});
				window.open ("<%=path%>/chkstomMis/printChkstom.do?chkstoNo="+chkValue.join(","),'newwindow','height='+window.screen.height+',width='+window.screen.width+',top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');
			}else{
				alert('<fmt:message key="please_select_single_message_to_print"/>！');
				return ;
			}
		}
		</script>				
	</body>
</html>