<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>MIS-报货单填制</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			
		<style type="text/css">
		.page{
			margin-bottom: 25px;
		}
		.test{
			color:blue;
		}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<%--存放一个状态 判断是何种操作类型 --%>
		<input type="hidden" id="sta" name="sta" value="${sta }"/>
		<form id="listForm" action="<%=path%>/chkstomMis/saveByAdd.do" method="post">
			<input type="hidden" id="firm" name="firm" value="${chkstom.firm}"/>
			<input type="hidden" id="createTyp" name="createTyp" value="${chkstom.createTyp }"/><!-- 报货来源 -->
			<input type="hidden" id="manifsttyp" name="manifsttyp" value="${chkstom.manifsttyp }"/><!-- 报货类别 -->
			<div class="bj_head">
				<div class="form-line">
					<div class="form-label"><fmt:message key="date"/>:</div>
					<div class="form-input">
						<!-- <input type="text" id="maded" name="maded" class="Wdate text" value="<fmt:formatDate value="${chkstom.maded}" pattern="yyyy-MM-dd"/>" readonly="readonly" onfocus="new WdatePicker({minDate:new Date()})"/> -->
						<input type="text" id="maded" name="maded" value="<fmt:formatDate value="${chkstom.maded}" pattern="yyyy-MM-dd"/>" readonly="readonly" />
					</div>
					<div class="form-label"><fmt:message key="scm_document_no"/>:</div>
					<div class="form-input" style="width:80px;">
						<c:if test="${chkstom.chkstoNo!=null}"><c:out value="${chkstom.chkstoNo }"></c:out></c:if>
						<input type="hidden" id="chkstoNo" name="chkstoNo" class="text" value="${chkstom.chkstoNo }"/> 					
					</div>
					<div class="form-label"><fmt:message key="document_number"/>:</div>
					<div class="form-input">
						<c:if test="${chkstom.vouno!=null}"><c:out value="${chkstom.vouno }"></c:out></c:if>
						<input type="hidden" id="vouno" name="vouno" class="text" value="${chkstom.vouno }"/>
					</div>
<%-- 					<div class="form-label"><fmt:message key="all_the_revised_delivery_date"/>:</div> --%>
<!-- 					<div class="form-input" style="width:100px;"> -->
<%-- 						<input type="text" id="hoped" class="Wdate text" value="<fmt:formatDate value="${chkstom.maded}" pattern="yyyy-MM-dd"/>" onfocus="new WdatePicker({minDate:new Date()})" style="width:90px;"/> --%>
<!-- 					</div> -->
<%-- 					<input type="button" value="<fmt:message key="update"/>" onclick="setHoped()"/> --%>
				</div>
				<div class="form-line">
					<div class="form-label" style="color:red;"><fmt:message key="arrival_date"/>:</div>
					<div class="form-input">
						<input type="text" name="hoped" id="hoped" class="Wdate text" value="<fmt:formatDate value="${chkstom.hoped}" pattern="yyyy-MM-dd"/>" 
							<c:choose>
								<c:when test="${chkstom.createTyp == 2 }">readonly="readonly"</c:when>
								<c:otherwise> onfocus="new WdatePicker({minDate:new Date()})"</c:otherwise>
							</c:choose>
						/>
					</div>
					<div class="form-label"><fmt:message key="orders_maker"/>:</div>
					<div class="form-input" style="width:80px;">
						<c:if test="${chkstom.madeby!=null}"><c:out value="${chkstom.madebyName}"></c:out></c:if>					
						<input type="hidden" id="madeby" name="madeby" class="text" value="${chkstom.madeby }"/>
					</div>
					<div class="form-label"><fmt:message key="orders_author"/>:</div>
					<div id="showChecby" class="form-input" style="width:80px;">
						<c:if test="${chkstom.checby!=null}"><c:out value="${chkstom.checby}"></c:out></c:if>
						<input type="hidden" id="checby" name="checby" class="text" value="${chkstom.checby }"/>
					</div>	
					<div class="form-label">
						<fmt:message key="please_select_materials"/>:<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
					</div>
				</div>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 26px;"><span >&nbsp;</span></td>
								<td colspan="3"><fmt:message key="supplies"/></td>
								<td colspan="2">
									<c:choose>
										<c:when test="${isDistributionUnit == 'Y' }"><fmt:message key="Distribution_Unit"/></c:when>
										<c:otherwise><fmt:message key="procurement_unit" /></c:otherwise>
									</c:choose>
								</td>
								<td colspan="4"><fmt:message key="standard_unit"/></td>
								<td rowspan="2" style="display:none;"><span style="width:90px;"><fmt:message key="arrival_date"/></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="remark"/></span></td>
							</tr>
							<tr>									
								<td class="num" style="width: 26px;"><span >&nbsp;</span></td>
								<td><span style="width:70px;"><fmt:message key="supplies_code"/></span></td>
								<td><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
								<td><span style="width:90px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:70px;"><fmt:message key="quantity"/></span></td>								
								<td><span style="width:35px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:50px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:35px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:40px;"><fmt:message key="unit_price"/></span></td>
								<td><span style="width:80px;"><fmt:message key="amount"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<c:set var="sum_num" value="${0}"/>  <!-- 物资条数 -->
				<c:set var="sum_amount" value="${0}"/>  <!-- 总数量 -->
				<c:set var="sum_price" value="${0}"/>  <!-- 总数量 -->
				<div class="table-body">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>
						<c:forEach var="chkstod" items="${chkstodList}" varStatus="status">
							<tr <c:choose>
									<c:when test="${isDistributionUnit == 'Y' }">data-mincnt="${chkstod.dismincnt}" data-unitper="${chkstod.disunitper }"</c:when>
									<c:otherwise>data-mincnt="${chkstod.supply.mincnt}" data-unitper="${chkstod.supply.unitper3 }"</c:otherwise>
								</c:choose>>
								<td align="center" style="width:26px;"><span >${status.index+1}</span></td>
								<td><span style="width:70px;" data-sp_name="${chkstod.supply.sp_name }">${chkstod.supply.sp_code }</span></td>
								<td><span style="width:100px;">${chkstod.supply.sp_name }</span></td>
								<td><span style="width:90px;">${chkstod.supply.sp_desc }</span></td>
								<c:choose>
									<c:when test="${isDistributionUnit == 'Y' }">
										<td><span style="width:70px;text-align: right;" title="${chkstod.amountdis}"><fmt:formatNumber value="${chkstod.amountdis}" type="currency" pattern="0.00"/></span></td>
										<td><span style="width:35px;">${chkstod.disunit }</span></td>
									</c:when>
									<c:otherwise>
										<td><span style="width:70px;text-align: right;" title="${chkstod.amount1}"><fmt:formatNumber value="${chkstod.amount1}" type="currency" pattern="0.00"/></span></td>
										<td><span style="width:35px;">${chkstod.supply.unit3 }</span></td>
									</c:otherwise>
								</c:choose>
								<td><span style="width:50px;text-align: right;" title="${chkstod.amount}"><fmt:formatNumber value="${chkstod.amount}" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:35px;">${chkstod.supply.unit }</span></td>
								<td><span style="width:40px;text-align: right;" title="${chkstod.supply.sp_price}"><fmt:formatNumber value="${chkstod.supply.sp_price}" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:80px;text-align: right;"><fmt:formatNumber value="${chkstod.supply.sp_price*chkstod.amount}" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:90px;display:none;">${chkstod.hoped}</span></td>
								<td><span style="width:70px;">${chkstod.memo}</span></td>
								<td style="display:none;"><span><input type="hidden" id="price_${status.index+1}" value="${chkstod.price }"/></span></td>								
							</tr>
							<c:set var="sum_num" value="${status.index+1}"/>  
							<c:set var="sum_amount" value="${sum_amount + chkstod.amount1}"/>   
							<c:set var="sum_price" value="${sum_price + chkstod.supply.sp_price*chkstod.amount}"/>   
						</c:forEach>
						</tbody>
					</table>					
				</div>
			</div>
			<div style="height:10px;">	
				<table border="0" cellspacing="0" cellpadding="0" style="margin-top:0;z-index:0;height:10px">
					<thead>
						<tr>
							<td style="width:26px;"></td>
							<td style="width:80px;background:#F1F1F1;"><fmt:message key="total"/><u>&nbsp;&nbsp;<label id="sum_num">${sum_num}</label>&nbsp;&nbsp;</u></td>
							<td style="width:210px;"></td>
							<td style="width:110px;background:#F1F1F1;"><fmt:message key="quantity"/><u>&nbsp;&nbsp;<label id="sum_amount"><fmt:formatNumber value="${sum_amount}" type="currency" pattern="0.00"/></label>&nbsp;&nbsp;</u></td>
							<td style="width:125px;"></td>
							<td style="width:150px;background:#F1F1F1;"><fmt:message key="total_amount"/><u>&nbsp;&nbsp;<label id="sum_price"><fmt:formatNumber value="${sum_price}" type="currency" pattern="0.00"/></label>&nbsp;&nbsp;</u></td>
							<td style="width:15px;"></td>
<%-- 							<td><font color="blue"><fmt:message key="operation_tips"/></font></td> --%>
						</tr>
					</thead>
				</table>
		   </div>
		</form>	
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		//如果导入报货单报错了。。。wjf
		var importError = '<c:out value="${importError}"/>';
		if(importError != null && importError != '' && importError != 'undefined'){
			alert("<fmt:message key='import'/><fmt:message key='failure'/>！<fmt:message key='reason'/>：\n"+importError);
		}
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		var validate;
		//工具栏
		$(document).ready(function(){
			focus() ;//页面获得焦点
			//键盘事件绑定
		 	$(document).bind('keyup',function(e){//mouseup
		 		if($(e.srcElement).is("input")){//对表格内的输入框进行判读，延迟500毫秒
		 			var index=$(e.srcElement).closest('td').index();
		    		if(index=="4"||index=="8"){
		    			$(e.srcElement).unbind('blur').blur(function(e){
			 				validateByMincnt($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
		    			});
		    			validator(index,$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
		    		}
		    	}
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
		 		if($("#sta").val() == "edit" || $("#sta").val() == "add"){
			 		if(window.event && window.event.keyCode == 120) { 
			 			window.event.keyCode = 505; 
			 			getChkstoDemo();
			 		} 
			 		if(window.event && window.event.keyCode == 505)window.event.returnValue=false;
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode)
	            {
	                case 85: $('#autoId-button-101').click(); break;//查看上传
	                case 70: $('#autoId-button-102').click(); break;//查询
	                case 65: $('#autoId-button-103').click(); break;//新建
	                case 69: $('#autoId-button-104').click(); break;//编辑
	                case 83: $('#autoId-button-105').click(); break;//保存
					case 68: $('#autoId-button-106').click(); break;//删除
	                case 80: $('#autoId-button-107').click(); break;//打印
	            }
			}); 
			//查找物资
		 	$('#seachSupply').bind('click.custom',function(e){
		 		if($("#sta").val() != "edit" && $("#sta").val() != "add"){//必须是新增或者编辑的情况下才能选择物资  2015.1.2wjf
		 			alert('<fmt:message key="must_add_or_edit_can_select"/>！');
		 			return;
		 		}
		 		if(!!!top.customWindow){
					top.customSupply('<fmt:message key="please_select_materials" />',encodeURI('<%=path%>/misbohcommon/selectSupplyLeft.do?ynsto=Y&single=false&positn='+$('#firm').val()+'&typ_eas='+$('#manifsttyp').val()),$('#sp_code'),null,$('#unit1'),$('.unit'),$('.unit1'),null,handler2);
				}
			});
		 	function handler2(sp_codes){
		 		var codes = '';//存放页面上的编码，用来判断是否存在
		 		$('.grid').find('.table-body').find('tr').each(function (){
		 			codes += $(this).find('td:eq(1)').text()+",";
				});
		 		var price;
		 		var sp_code;
				var sp_code_arry = new Array();
		 		sp_code_arry = sp_codes.split(",");
				
				for(var i=0; i<sp_code_arry.length; i++){
					sp_code = sp_code_arry[i];
					if(codes.indexOf(sp_code) != -1 ){//如果已存在，则继续下次循环
						continue;
					}
					$.ajax({
						type: "POST",
						url: "<%=path%>/misbohcommon/findSupplyBySpcode.do",
						data: "sp_code="+sp_code,
						dataType: "json",
						success:function(supply){
							if(!$(".table-body").find("tr:last").find("td:eq(1)").find('span').text()==''){
								$.fn.autoGrid.addRow();
							}
							$(".table-body").find("tr:last").find("td:eq(1)").find('span').text(supply.sp_code).data('sp_name',supply.sp_name);
							$(".table-body").find("tr:last").find("td:eq(2)").find('span').text(supply.sp_name);
							$(".table-body").find("tr:last").find("td:eq(3)").find('span').text(supply.sp_desc);
							if('${isDistributionUnit}' == 'Y'){
								$(".table-body").find("tr:last").find("td:eq(5)").find('span').text(supply.disunit);
								$(".table-body").find("tr:last").data("unitper",supply.disunitper);
								$(".table-body").find("tr:last").data("mincnt",supply.dismincnt);
							}else{
								$(".table-body").find("tr:last").find("td:eq(5)").find('span').text(supply.unit3);
								$(".table-body").find("tr:last").data("unitper",supply.unitper3);
								$(".table-body").find("tr:last").data("mincnt",supply.mincnt);
							}
							$(".table-body").find("tr:last").find("td:eq(4)").find('span').text(0).css("text-align","right");
							$(".table-body").find("tr:last").find("td:eq(6)").find('span').text(0).css("text-align","right");
							$(".table-body").find("tr:last").find("td:eq(8)").find('span').attr('title',supply.sp_price).text(supply.sp_price.toFixed(2)).css("text-align","right");
							$(".table-body").find("tr:last").find("td:eq(7)").find('span').text(supply.unit);
							$(".table-body").find("tr:last").find("td:eq(9)").find('span').text(0).css("text-align","right");
							$(".table-body").find("tr:last").find("td:eq(10)").find('span').text(getInd());
						}
					});
				}
		 	}
			//判断按钮的显示与隐藏
			if($("#sta").val() == "add"){
				loadToolBar([true,true,true,false,true,false,false,false,true]);
				editCells();
				$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),2);
				setTimeout('keepSessionAjax()',300000);
			}else if($("#sta").val() == "show"){
				loadToolBar([true,true,true,true,false,true,true,true,false]);
			}else{
				loadToolBar([true,true,true,false,false,false,false,false,false]);
			}
			/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'maded',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']
				},{
					type:'text',
					validateObj:'chkstoNo',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key="orders_num"/><fmt:message key="cannot_be_empty"/>！']
				}]
			});
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		   
		    $('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),110);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
		});
		
		//控制按钮显示
		function loadToolBar(use){
			$('.tool').html('');
			$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[1],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							if($("#sta").val()=="add"||$("#sta").val()=="edit"){
								if(confirm('<fmt:message key="data_unsaved_whether_to_exit"/>？')){
									searchChkstom();
								}
							}else{
								searchChkstom();
							}
						}
					},'-',{
						text: '<fmt:message key="insert" />',
						title: '<fmt:message key="insert"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')}&&use[2],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','0px']
						},
						handler: function(){
							if($("#sta").val()=="add"||$("#sta").val()=="edit"){
								if(confirm('<fmt:message key="data_unsaved_whether_to_exit"/>？')){
									addChkstom();
								}
							}else{
								addChkstom();
							}
						}
					},{
						text: '<fmt:message key="template" />',
						title: '<fmt:message key="template" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')}&&use[8],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','0px']
						},
						handler: function(){
							if($("#firm").val()==null||$("#firm").val()==''){
								if(confirm('请选择申购仓位！')){
									return;
								}
							}else{
								getChkstoDemo();
							}
						}
					},{
						text: '<fmt:message key="edit" />',
						title: '<fmt:message key="edit"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[3],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-20px','0px']
						},
						handler: function(){
							if($("#checby").val()!=null && $("#checby").val()!=""){
								alert('<fmt:message key="orders_audited_cannot_edited"/>！');
								/* showMessage({
										type: 'error',
										msg: '<fmt:message key="orders_audited_cannot_edited"/>！',
										speed: 1000
										}); */
								return false;
							}else if($("#chkstoNo").val()==null || $("#chkstoNo").val()==""){
								return false;
							}else if($("#sta").val()=="add"){
								return false;
							}else{
								$("#sta").val("edit");
								if($('#createTyp').val() == 2){//如果是预估来的， 模板不可选
									loadToolBar([true,true,true,false,true,true,true,false,false]);
								}else{
									loadToolBar([true,true,true,false,true,true,true,false,true]);
								}
								editCells();
								setTimeout('keepSessionAjax()',300000);
							}
						}
					},{
						text: '<fmt:message key="save" />',
						title: '<fmt:message key="save"/>',
						useable: use[4],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							if($("#sta").val()=="add" || $("#sta").val()=="edit"){
								if(validate._submitValidate()){
									if($('#createTyp').val() == 2){
										saveChkstom();
										return;
									}
									updateHoped();
								}
							}
						}
					},{
						text: '<fmt:message key="submit" />',
						title: '<fmt:message key="submit" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')}&&use[7],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-58px','-240px']
						},
						handler: function(){
							checkChkstom();
						}
					},{
						text: '<fmt:message key="delete" />',
						title: '<fmt:message key="delete"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')}&&use[5],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							deleteChkstom();
						}
					},'-',{
						text: '<fmt:message key="import" />',
						title: '<fmt:message key="import" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'import')},
						icon: {
							url: '<%=path%>/image/Button/excel.bmp',
							position: ['2px','2px']
						},
						handler: function(){
							importChkstom();
						}
					},{
						text: '<fmt:message key="export" />',
						title: '<fmt:message key="export"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[6],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							exportChkstom();
						}
					},{
						text: '<fmt:message key="print" />',
						title: '<fmt:message key="print"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[6],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							printChkstom();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							if($("#sta").val()=="add"||$("#sta").val()=="edit"){
								if(confirm('<fmt:message key="data_unsaved_whether_to_exit"/>？'))
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}else{
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
							}
						}
					}
				]
			});
		}
		
		//编辑表格
		function editCells()
		{
			if($("#sta").val() == "add"){
				//如果导入成功，则不加1 wjf
				var importFlag = $('#importFlag').val();
				if(importFlag != 'OK'){
					$('#sum_num').text(Number($('#sum_num').text())+1);//总行数
				}
			}
			$(".table-body").autoGrid({
				initRow:1,
				colPerRow:12,
				widths:[26,80,110,100,80,45,60,45,50,90,100,80],
				colStyle:['','','','',{background:"#F1F1F1"},'','','','','',{display:"none"},''],
				VerifyEdit:{verify:true,enable:function(cell,row){
					return row.find('td').index(cell) == 2 || (row.find("td:eq(2)").text() == null || $.trim(row.find("td:eq(2)").text()) != '' ) ? true : false;
				}},
				onEdit:$.noop,
				editable:[2,4,11],//能输入的位置(8暂去)
				onLastClick:function(row){
					$('#sum_num').text(Number($('#sum_num').text())-1);//总行数
					$('#sum_amount').text(Number($('#sum_amount').text())-row.find('td:eq(4)').text());//总数量
					$('#sum_price').text(Number(Number($('#sum_price').text())-Number(row.find('td:eq(9)').text())).toFixed(2));//总金额
// 					$('#sum_amount').text(Math.round((Number($('#sum_amount').text())-Number(row.find('td:eq(5)').text()))*100)/100);//总数量
				},
				onEnter:function(data){
					var pos = data.curobj.closest('tr').find('td').index(data.curobj.closest('td'));
					if(pos == 2){
						if($.trim(data.curobj.closest('td').prev().text())){
							data.curobj.find('span').html(data.curobj.closest('td').prev().find('span').data('sp_name'));
							return;
						}else if(!data.actionobj){
							$.fn.autoGrid.setCellEditable(data.curobj.find('span').closest('tr'),2);
							return;
						} 
					}
					$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.value) ;
				},
				cellAction:[{
					index:2,
					action:function(row){
						$.fn.autoGrid.setCellEditable(row,4);
					},
					onCellEdit:function(event,data,row){
						data['url'] = '<%=path%>/misbohcommon/findSupplyTop10.do?ynsto=Y&typ_eas='+$("#manifsttyp").val();
						/*if (data.value.split(".").length>2) {
							data['key'] = 'sp_code';
						}else if(!isNaN(data.value)){
							data['key'] = 'sp_code';
						}else{
							data['key'] = 'sp_init';
						}*/
						data['key'] = 'sp_code';
						data.sp_position = $('#firm').val();
						$.fn.autoGrid.ajaxEdit(data,row);
					},
					resultFormat:function(data){
						var desc ='';
						if(data.sp_desc !='' && data.sp_desc !=null){
							desc = '-'+data.sp_desc ;
						}
						return data.sp_init+'-'+data.sp_code+'-'+data.sp_name + desc;
					},
					afterEnter:function(data2,row){
						var num=0;
						$('.grid').find('.table-body').find('tr').each(function (){
							if($(this).find("td:eq(1)").text()==data2.sp_code){
								num=1;
							}
						});
						if(num==1){
							showMessage({
 								type: 'error',
 								msg: '<fmt:message key="added_supplies_remind"/>！',
 								speed: 1000
 							});
							$.fn.autoGrid.setCellEditable(row,2);
							return;
						}
						row.find("td:eq(1) span").text(data2.sp_code).data('sp_name',data2.sp_name);
						row.find("td:eq(2) span input").val(data2.sp_name).focus();
						row.find("td:eq(3) span").text(data2.sp_desc==null ? "":data2.sp_desc);//修改规格问题
						row.find("td:eq(4) span").text(0).css("text-align","right");//采购数量默认0
						if('${isDistributionUnit}' == 'Y'){
							row.find("td:eq(5) span").text(data2.disunit);//配送单位
							row.data("unitper",data2.disunitper);
							row.data("mincnt",data2.dismincnt);
						}else{
							row.find("td:eq(5) span").text(data2.unit3);//采购单位
							row.data("unitper",data2.unitper3);//unitper3 是采购单位和标准单位的转换率
							row.data("mincnt",data2.mincnt);
						}
						row.find("td:eq(6) span").text(0).css("text-align","right");//标准数量
						row.find("td:eq(7) span").text(data2.unit);//标准单位
						row.find("td:eq(8) span").text((data2.sp_price).toFixed(2)).attr("title",data2.sp_price).css("text-align","right");
						row.find("td:eq(9) span").text(0).css("text-align","right");
						row.find("td:eq(10) span").text(data2.receiveDate);
						row.find("td:eq(11) span").text();
					}
				},{
					index:4,
					action:function(row,data2){
						if(Number(data2.value) == 0){
							alert('<fmt:message key="number_cannot_be_zero"/>！');
							row.find("td:eq(4)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,4);
						}else if(Number(data2.value) < 0){
							alert('数量不能为负数！');
							row.find("td:eq(4)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,4);
						}else if(isNaN(data2.value)){
							alert('<fmt:message key="number_be_not_number"/>！');
							row.find("td:eq(4)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,4);
						}else if(data2.value.length > 6){
							alert('<fmt:message key="the_maximum_length_not"/>6<fmt:message key="digit"/>！');
							row.find("td:eq(4)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,4);
						}else if(Number(row.data("mincnt"))!=0 && !(/(^[1-9]\d*$)/.test(Number(data2.value)/Number(row.data("mincnt"))))){
							alert('该物资最低申购数量为'+row.data("mincnt")+"，申购量必须是最小申购量的整数倍!");
							row.find("td:eq(4)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,4);
						}else {
							//$('#sum_amount').text(Number($('#sum_amount').text())+Number(row.find("td:eq(4)").text())-data2.ovalue);//总数量
							//采购数量变，标准数量6也变     如果转换率不为0，则直接除以转换率，为0  就不算了
							row.find("td:eq(4)").find('span').attr('title',data2.value);
							if(row.data("unitper") != 0){
								var amount = Number(row.find("td:eq(4) span").text())/Number(row.data("unitper"));
								row.find("td:eq(6) span").text(amount.toFixed(2)).attr('title',amount);//标准数量
								row.find("td:eq(9) span").text((amount*Number(row.find("td:eq(8) span").attr('title'))).toFixed(2));//总金额
								getTotalSum();
							}
							$.fn.autoGrid.setCellEditable(row,11);
						}
					}
				},{
					index:10,
					action:function(row,data){
						$.fn.autoGrid.setCellEditable(row,11);
					},CustomAction:function(event,data){
						var input = data.curobj.find('span').find('input');
						input.addClass("Wdate text");
						input.bind('click',function(){
							input.unbind('keyup');
							new WdatePicker({
								startDate:'%y-%M-{%d+1}',
								el:'input',
								onpicked:function(dp){
									data.curobj.find('span').html(dp.cal.getNewDateStr());
									$.fn.autoGrid.setCellEditable(data.row,11);
								}
							});
						});
						input.trigger('click');
					}
				},{
					index:11,
					action:function(row,data){
						if(!row.next().html())$.fn.autoGrid.addRow();
						$.fn.autoGrid.setCellEditable(row.next(),2);
						$('#sum_num').text(Number($('#sum_num').text())+1);//总行数
					}
				}]
			});
		}
		function getTotalSum(){//计算统计数据
			var sum_amount = 0; 
			var sum_totalamt = 0;
			$('.table-body').find('tr').each(function (){
				if($(this).find('td:eq(1)').text()!=''){//非空行
					var amount = $(this).find('td:eq(4)').text();
					if(!amount){
						amount = $(this).find('td:eq(4)').find("input:eq(0)").val();
					}
					sum_amount += Number(amount);
					var price  = $(this).find('td:eq(9)').text();
					if(!price){//正在编辑该数据
						price = $(this).find('td:eq(9)').find("input:eq(0)").val();
					}
					sum_totalamt = parseFloat(sum_totalamt) + parseFloat(price);
				}
			});
			$('#sum_num').text($(".table-body").find('tr').length);//总行数
			$('#sum_amount').text(Number(sum_amount).toFixed(2));//总数量
			$('#sum_price').text(Number(sum_totalamt).toFixed(2));//总金额
		}
		
		//查找报货单
		function searchChkstom(){
			var curwindow = $('body').window({
				id: 'window_searchChkstom',
				title: '<fmt:message key="query_messages_manifest"/>',
				content: '<iframe id="searchChkstomFrame" frameborder="0" src="<%=path%>/chkstomMis/searchByKey.do?init=init"></iframe>',
				width: 800,
				height: 430,
				draggable: true,
				isModal: true
			});
			curwindow.max();
		}
		
		//查找报货单
		function searchUncheck(bdate,edate){
			var action = "<%=path%>/chkstomMis/searchByKey.do?bMaded="+bdate+"&eMaded="+edate;
			var curwindow = $('body').window({
				id: 'window_searchChkstom',
				title: '<fmt:message key="query_messages_manifest"/>',
				content: '<iframe id="searchChkstomFrame" frameborder="0" src='+action+'></iframe>',
				width: 800,
				height: 430,
				draggable: true,
				isModal: true
			});
			curwindow.max();
		}
		
		//新增报货单
		function addChkstom(){			
			window.location.replace("<%=path%>/chkstomMis/addChkstom.do");
		}
		
		//保存添加
		function saveChkstom(){
			if($('#hoped').val() == ''){
				alert('请填写到货日期！');
				return;
			}
			/*if(!confirm('是否保存到货日期为【'+$('#hoped').val()+'】的报货单?')){
				return;
			}*/
			var indNull=0;//默认0代表成功
			var numNull=0;//默认0代表成功
			var isNull=0;
			var sp_name = '';
			$('.table-body').find('tr').each(function(){
				if($(this).find('td:eq(1)').text()!=''){
					isNull=1;
					var amount1 = $(this).find('td:eq(4) span').attr('title')?$(this).find('td:eq(4) span').attr('title'):$(this).find('td:eq(4) span').text();
					if(Number(amount1)==0){
						numNull=1;//1数量为0
						sp_name = $(this).find('td:eq(2)').text()?$(this).find('td:eq(2)').text():$(this).find('td:eq(2) input').val();
						return false;
					}
					if(amount1=="" || amount1==null || isNaN(amount1)){
						numNull=2;//2数量为空
						sp_name = $(this).find('td:eq(2)').text()?$(this).find('td:eq(2)').text():$(this).find('td:eq(2) input').val();
						return false;
					}
			//		if(ind==''||ind==null){
			//			indNull=1;//1日期为空
			//			sp_name = $(this).find('td:eq(2)').text()?$(this).find('td:eq(2)').text():$(this).find('td:eq(2) input').val();
			//			return false;
			//		}
				}
			});
			if(Number(isNull)==0){
				alert('不能加入空单据！');
				return;
			}
			if(Number(numNull)==1){//数量不为0
				alert('<fmt:message key="supplies"/>：['+sp_name +']<fmt:message key="number_cannot_be_zero"/>！');
				return;
			}
			if(Number(numNull)==2){//数量为空或字母
				alert('<fmt:message key="supplies"/>：['+sp_name +']<fmt:message key="quantity"/><fmt:message key="not_valid_number"/>！');
				return;
			}
// 			if(Number(indNull)==1){//日期为空
// 				alert('<fmt:message key="supplies"/>：['+sp_name +']<fmt:message key="arrival_date"/><fmt:message key="cannot_be_empty"/>！');
// 				return;
// 			}
			$('#wait').show();
			$('#wait2').show();
			var keys = ["supply.sp_code","supply.sp_name","supply.sp_desc","amount1","supply.unit3","amount","supply.unit",
			            "price","totalAmt","hoped","memo"];
			var data = {};
			var i = 0;
			$("#listForm *[name]").each(function(){
				var name = $(this).attr("name"); 
				if(name) data[name] = $(this).val();
			});
			var rows = $(".grid .table-body table tr");
			for(i=0;i<rows.length;i++){
// 				var status=$(rows[i]).find('td:eq(0)').text();
// 				data["chkstodList["+i+"]."+"price"] = $(rows[i]).find('td:eq(8) span').attr('title') ? $(rows[i]).find('td:eq(8) span').attr('title'):$('#price_'+status).val();
				cols = $(rows[i]).find("td");
				var j = 0;
				for(j=1;j <= keys.length;j++){
					var value = $.trim($(rows[i]).find("td:eq("+j+") span").attr('title'))?$.trim($(rows[i]).find("td:eq("+j+") span").attr('title')):$.trim($(rows[i]).find("td:eq("+j+")").text());
					value = value ? value : $.trim($(rows[i]).find("td:eq("+j+") input").val());
					if(value)
						data["chkstodList["+i+"]."+keys[j-1]] = value;
				}
				data["chkstodList["+i+"]."+"totalAmt"] = 0;
				data["chkstodList["+i+"]."+"hoped"] = $('#hoped').val();
			}
			//提交并返回值，判断执行状态
			$.post("<%=path%>/chkstomMis/saveByAddOrUpdate.do?sta="+$("#sta").val(),data,function(data){
				$('#wait').hide();
				$('#wait2').hide();
				var rs = eval('('+data+')');
				switch(Number(rs)){
				case -1:
					showMessage({
								type: 'error',
								msg: '<fmt:message key="save_fail"/>！',
								speed: 1000
								});
					break;
				case 1:
					alert('<fmt:message key="save_successful"/>！');
					openChkstom($('#chkstoNo').val());
// 					loadToolBar([true,true,true,true,false,true,true,true]);
// 					$("#sta").val("show");
// 					break;
				}
			});			 
		}
		
		
		// 检测报货时间
		function checkOrderTime(){
			var param = {};
			var bool = false;
			param['maded'] = $("#maded").val();
			param['firm'] = $('#firm').val();
			$('#wait').show();
			$('#wait2').show();
			$.post('<%=path%>/chkstomMis/eqOrderTime.do',param,function(data){
				$('#wait').hide();
				$('#wait2').hide();
				var errorLog = data;
				if(errorLog!=null && errorLog!='1'){
					alert('<fmt:message key="misboh_DeclareGoodsGuide_orderTimeError"/>【'+errorLog+'】<fmt:message key="misboh_DeclareGoodsGuide_orderTimeError1"/>');
					bool = true;
				}
			});
			return bool;
		}
		
		//审核
		function checkChkstom(){
			var firm = $('#firm').val();
	
			if(checkOrderTime()){
				return;
			}
			var chkstoNo = $("#chkstoNo").val();
			if(chkstoNo!=null && chkstoNo!=""){
				//提交并返回值，判断执行状态
				$('#wait').show();
				$('#wait2').show();
				$.post("<%=path%>/chkstomMis/checkChkstom.do?chkstoNo="+chkstoNo+"&firm="+firm,function(data){
					$('#wait').hide();
					$('#wait2').hide();
					var rs = eval('('+data+')');
					switch(Number(rs.pr)){
					case -1:
						alert('<fmt:message key="data_unsaved"/>！');
						break;
					case 1:
						$('#showChecby').text(rs.checbyName);
						showMessage({
							type: 'success',
							msg: "提交成功！",
							speed: 3000
						});
						loadToolBar([true,true,true,false,false,false,true,false,false]);
						break;
					case 0:
						alert('<fmt:message key="cannot_repeat_audit"/>！');
						break;
					}
				});
			}
		}
		
		//删除
		function deleteChkstom(){
			var chkstoNo=$("#chkstoNo").val();
			if($("#chkstoNo").val()!=null && $("#chkstoNo").val()!="" && $("#chkstoNo").val()!=0){
				if($("#checby").val()!=null && $("#checby").val()!=""){
					alert('<fmt:message key="orders_audited_cannot_deleted"/>');
					return false;
				}
				if($("#sta").val()!="show"){
					alert('<fmt:message key="orders_unsaved_cannot_deleted"/>！');
				}else{
					if(confirm('<fmt:message key="whether_delete_orders"/>？')){
						//提交并返回值，判断执行状态
						$('#wait').show();
						$('#wait2').show();
						$.post("<%=path%>/chkstomMis/deleteChkstom.do?chkstoNo="+chkstoNo,function(data){
							$('#wait').hide();
							$('#wait2').hide();
							var rs = eval('('+data+')');
							switch(Number(rs.pr)){
							case 0:
								alert('<fmt:message key="orders_unsaved_cannot_audited"/>！');
								break;
							case 1:
								alert('<fmt:message key="successful_deleted"/>！');
								window.location.replace("<%=path%>/chkstomMis/chkstomTable.do");
								break;
							}
						});
					}						
				}
			}
		}
		
		// 查找添加盘点模板
		function getChkstoDemo(){
	    	var action = "<%=path%>/chkstomMis/addChkstoDemo.do?firm="+$('#firm').val();//调用模板的时候传申购门店
			$('body').window({
				id: 'window_addChkstoDemo',
				title: '报货单模板',
				content: '<iframe id="addChkstoDemoFrame" name="addChkstoDemoFrame" frameborder="0" src='+action+'></iframe>',
				width: '80%',//'1000px'
				height: '420px',
				draggable: true,
				isModal: true,
				confirmClose: false,
				topBar: {
					items: [{
							text: '确定',
							title: '确定',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								if(getFrame('addChkstoDemoFrame')){
									window.frames["addChkstoDemoFrame"].enterUpdate();
								}
							}
						},{
							text: '取消',
							title: '取消',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.close').click();
							}
						}]
				}
			});
		}
		//导出报货单
		function exportChkstom(){
			if(null!=$("#chkstoNo").val() && ""!=$("#chkstoNo").val()){
				window.location.href="<%=path%>/chkstomMis/exportChkstom.do?chkstoNo="+$("#chkstoNo").val();
			}
		}
		//打印单据
		function printChkstom(){ 
			if(null!=$("#chkstoNo").val() && ""!=$("#chkstoNo").val()){
				window.open ("<%=path%>/chkstomMis/printChkstom.do?chkstoNo="+$("#chkstoNo").val(),'newwindow','height='+window.screen.height+',width='+window.screen.width+',top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');
			}
		}
		//双击打开
		function openChkstom(chkstoNo){
			window.location.replace("<%=path%>/chkstomMis/findChk.do?chkstoNo="+chkstoNo);
		}
		function validateByMincnt(index,row,data2){//最小申购量判断
			if(index=="4"){
				if(Number(data2.value) == 0){
					alert('<fmt:message key="number_cannot_be_zero"/>！');
					row.find("td:eq(4)").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,4);
				}else if(Number(row.data("mincnt"))!=0 && !(/(^[1-9]\d*$)/.test(Number(data2.value)/Number(row.data("mincnt"))))){
					alert('该物资最低申购数量为'+row.data("mincnt")+"，申购量必须是最小申购量的整数倍!");
					row.find("td:eq(4)").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,4);
				}
			}
		}
		function validator(index,row,data2){//输入框验证
			if(index=="4"){
				if(Number(data2.value) < 0){
					alert('数量不能为负数！');
					row.find("td:eq(4)").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,4);
				}else if(isNaN(data2.value)){
					alert('<fmt:message key="number_be_not_number"/>！');
					row.find("td:eq(4)").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,4);
				}else if(data2.value.length > 6){
					alert('<fmt:message key="the_maximum_length_not"/>6<fmt:message key="digit"/>！');
					row.find("td:eq(4)").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,4);
				}else {
					if(isNaN(Number(row.data("unitper")))){
						row.data("unitper",Number(data2.value)/data2.ovalue);
					}
					$('#sum_amount').text(Number($('#sum_amount').text())+Number(data2.value)-Number(data2.ovalue));//总数量
					row.find("input").data("ovalue",data2.value);
					row.find("td:eq(4)").find('span').attr('title',data2.value);
					if(row.data("unitper") != 0){
						var amount = Number(data2.value)/Number(row.data("unitper"));
						row.find("td:eq(6) span").text(amount.toFixed(2)).attr('title',amount);//标准数量
						row.find("td:eq(9) span").text((amount*Number(row.find("td:eq(8) span").attr('title'))).toFixed(2));//总金额
						getTotalSum();
					}
				}
			}
		}
		//统一修改到货日期
		function setHoped(){
			$('#hoped').val($('#hoped1').val());
			if($('#hoped1').val() == ''){
				alert('请填写到货日期！');
				return false;
			}
			if(!confirm('是否保存到货日期为【'+$('#hoped1').val()+'】的报货单?')){
				return false;
			}
			return true;
			
		//	if($('#hoped').val()<$('#maded').val()){
		//		alert('到货日期不能小于制单日期！');
		//		return false;
		//	}
// 			$('.table-body').find('tr').each(function(){
// 				if ($(this).find('td:eq(1)').text()!=null&&$(this).find('td:eq(1)').text()!='') {
// 					$(this).find('td:eq(10)').find('span').text($('#hoped').val());
// 				}
// 			});
		}
		
		//统一修改到货日期
		function updateHoped(){
			var html = '<div style="margin:50px 100px;"><span style="color:red;font-size:24px;">请正确选择到货日期！！！</span><br><input type="text" id="hoped1" class="Wdate text" value="<fmt:formatDate value="${null == chkstom.hoped ? '' : chkstom.hoped}" pattern="yyyy-MM-dd"/>" onfocus="new WdatePicker({minDate:new Date()})" style="width:150px;"/></div>';
			$('body').window({
				id: 'window_updateHoped',
				//title: '<fmt:message key="all_the_revised_delivery_date"/>',
				title: '单据到货日期确认',
				content: html,
				width: '400px',//'1000px'
				height: '300px',
				draggable: true,
				isModal: true,
				confirmClose: false,
				topBar: {
					items: [{
							text: '<fmt:message key="enter"/>',
							title: '<fmt:message key="enter"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								if(setHoped()){
									$('.close').click();
									saveChkstom();
								}
							}
						},{
							text: '<fmt:message key="cancel"/>',
							title: '<fmt:message key="cancel"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.close').click();
							}
						}]
				}
			});
		}
		
		//导入excel报货单
		function importChkstom(){
	    	$('body').window({
				id: 'window_importSupply',
				title: '<fmt:message key="import" />报货单',
				content: '<iframe id="importChkstom" frameborder="0" src="<%=path%>/chkstomMis/importChkstom.do"></iframe>',
				width: '400px',
				height: '200px',
				draggable: true,
				isModal: true
			});
	    }
		
		
		//获取系统时间
		function getInd(){
			var myDate=new Date(new Date().getTime() + 24*60*60*1000);  
			var yy=myDate.getFullYear();
			var MM=myDate.getMonth()+1;
			var dd=myDate.getDate();
			if(MM<10)
				MM="0"+MM;
			if(dd<10)
				dd="0"+dd;
			return fullDate=yy+"-"+MM+"-"+dd;
		}
		
		//新增和编辑状态下 保持session
		function keepSessionAjax(){
			$.ajax({
				url:'<%=path%>/misbohcommon/keepSessionAjax.do',
				type:"post",
				success:function(data){}
			});
			setTimeout('keepSessionAjax()',300000);
		}
		</script>			
	</body>
</html>