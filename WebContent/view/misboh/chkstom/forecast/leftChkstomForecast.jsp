<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			$.ajaxSetup({
				async: false
			});
			//刷新左侧  点下一步时调用
			function setNextLeft(index){
				var url = '';
				$.ajax({
					url:'<%=path%>/chkstomForecast/getLeftList.do',
					type:'post',
					success:function(msg){
						$('.table-body').find('tbody').empty();
						for(var i = 0; i < msg.length; i++){
							var html = '<tr><td><span style="width:25px;"><input type="radio" name="idList" id="chk_'+i+'" value="'+msg[i].url+'" disabled="disabled"/></span></td>';
							html += '<td><span title="'+msg[i].name+'" style="width:108px;">'+msg[i].name+'&nbsp;</span></td></tr>';
							$('.table-body').find('tbody').append(html);
						}
						$('#chk_'+index).attr('checked',true);
						$('#chk_'+index).parents('tr').addClass('tr-over');
						url = $('#chk_'+index).val();
					}
				});
				return url;
			}
			
			//点上一步时调用
			function setPrevLeft(index){
				$('#chk_'+index).attr('checked',true);
				$('.table-body').find('tr').removeClass('tr-over');
				$('#chk_'+index).parents('tr').addClass('tr-over');
				var url = $('#chk_'+index).val();
				return url;
			}
		</script>
	</head>
	<body>
	<form id="leftForm" method="post" action="<%=path%>/chkstomForecast/listLeftCf.do">
		<div class="grid" style="width: 100%;">
			<div class="table-body">
				<table cellspacing="0" cellpadding="0">
					<tbody>
						<c:forEach var="list" items="${leftList}" varStatus="sta">
							<tr <c:if test="${sta.index == 0 }">class="tr-over" </c:if>>
								<td><span style="width:25px;">
										<input type="radio" <c:if test="${sta.index == 0 }">checked="checked" </c:if>
										 name="idList" id="chk_${sta.index}" value="${list.url}" disabled="disabled"/></span> 
								</td>
								<td><span title="${list.name}" style="width:108px;">${list.name}&nbsp;</span></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</form>
	</body>
</html>