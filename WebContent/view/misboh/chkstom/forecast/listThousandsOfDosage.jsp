<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		$(document).ready(function(){
			parent.stopVisibility();
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),85);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			
			$('#save').click(function(){
				$('#mainForm').submit();
			});
		});
		
		//计算周期用量
		function gclick(obj){
			var forecastMoney = Number($(obj).parents('tr').data('forecastmoney'));
			var val = Number($(obj).val());
			$(obj).parents('td').next('td').find('span').text(Number(val*forecastMoney/1000).toFixed(2));
		}
		
		//回车下一个
		function eqEnter(obj){
			if(event.keyCode == 13){
				if($(obj).parents('tr').next('tr').length != 0)
					$(obj).parents('tr').next('tr').find('input').focus();
				else
					$('.table-body').find('tr:first').find('input').focus();
			}
		}
		
		function save(){
			var data = {};
			$('.table-body').find('tr').each(function(i){
				data['qyylList['+i+'].sp_code'] = $(this).attr('data-sp_code');
				data['qyylList['+i+'].estimatedParameters'] = $(this).find('td:eq(5)').find('input').val();
				data['qyylList['+i+'].periodUse'] = $(this).find('td:eq(6)').find('span').text();
			});
			var bool = false;
			$.ajax({
				url:'<%=path%>/chkstomForecast/saveThousands.do',
				data:data,
				type:'post',
				success:function(msg){
					bool = true;
				}
			});
			return bool;
		}
			
		</script>
	</head>
	<body>
		<form id="mainForm" method="post" action="<%=path%>/chkstomForecast/listThousandsOfDosage.do">
			<div class="bj_head">
				<div class="form-line">
					<div class="form-label"><fmt:message key="startdate"/>:</div>
					<div class="form-input">
						<input type="text" id="bdate" name="bdate" class="Wdate text" value="${cf.bdate }" readonly="readonly" onfocus="new WdatePicker()"/>
					</div>
					<div class="form-label"><fmt:message key="enddate"/>:</div>
					<div class="form-input">
						<input type="text" id="edate" name="edate" class="Wdate text" value="${cf.edate }" readonly="readonly" onfocus="new WdatePicker()"/>
					</div>
					<div class="form-label">
						<input type="button" id="save" class="text" style="width: 50px;cursor:pointer;height: 20px" value="<fmt:message key="calculate"/>" />
					</div>
				</div>
			</div>
			<div class="grid">
				<div class="table-head">
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td colspan="4"><fmt:message key="supplies"/></td>
								<c:if test="${cf.vplantyp == 'PAX'}">
									<td rowspan="2"><span style="width:80px;"><fmt:message key="misboh_DeclareGoodsGuide_ThousandsPeopleOfDosage"/></span></td>
									<td rowspan="2"><span style="width:80px;"><fmt:message key="misboh_DeclareGoodsGuide_ThousandsPeopleOfDosage_edit"/></span></td>
									<td rowspan="2"><span style="width:80px;"><fmt:message key="misboh_DeclareGoodsGuide_Periodic_quantity"/></span></td>
								</c:if>
								<c:if test="${cf.vplantyp == 'AMT'}">
									<td rowspan="2"><span style="width:80px;"><fmt:message key="misboh_DeclareGoodsGuide_ThousandsYuanOfDosage"/></span></td>
									<td rowspan="2"><span style="width:80px;"><fmt:message key="misboh_DeclareGoodsGuide_ThousandsYuanOfDosage_edit"/></span></td>
									<td rowspan="2"><span style="width:80px;"><fmt:message key="misboh_DeclareGoodsGuide_Periodic_quantity"/></span></td>
								</c:if>
								<c:if test="${cf.vplantyp == 'TC'}">
									<td rowspan="2"><span style="width:80px;"><fmt:message key="misboh_DeclareGoodsGuide_ThousandsTimeOfDosage"/></span></td>
									<td rowspan="2"><span style="width:80px;"><fmt:message key="misboh_DeclareGoodsGuide_ThousandsTimeOfDosage"/></span></td>
									<td rowspan="2"><span style="width:80px;"><fmt:message key="misboh_DeclareGoodsGuide_Periodic_quantity"/></span></td>
								</c:if>
							</tr>
							<tr>
								<td><span style="width:70px;"><fmt:message key="supplies_code"/></span></td>
								<td><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
								<td><span style="width:90px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:50px;"><fmt:message key="unit"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="declare" items="${cf.qyylList}" varStatus="status">
								<tr data-forecastmoney="${declare.forecastMoney }" data-sp_code="${declare.sp_code }">
								<td><span style="width:70px;">${declare.sp_code }</span></td>
								<td><span style="width:100px;">${declare.sp_name }</span></td>
								<td><span style="width:90px;">${declare.sp_desc }</span></td>
								<td><span style="width:50px;">${declare.sp_unit }</span></td>
								<td><span style="width:80px;text-align: right;"><fmt:formatNumber value="${declare.estimatedParameters }" type="currency" pattern="0.00"/></span></td>
								<td>
									<span style="width:80px;text-align: right;">
										<input style="width:80px;text-align:right;" onkeyup="eqEnter(this)" onblur="gclick(this)" type="text" value="<fmt:formatNumber value="${declare.estimatedParameters }" type="currency" pattern="0.00"/>"/>';
									</span>
								</td>
								<td><span style="width:80px;text-align: right;"><fmt:formatNumber value="${declare.periodUse }" type="currency" pattern="0.00"/></span></td>
							 </tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
	</body>
</html>