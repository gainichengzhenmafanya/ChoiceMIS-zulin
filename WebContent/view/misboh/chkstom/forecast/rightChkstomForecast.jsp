<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		$(document).ready(function(){
			parent.stopVisibility();
			/*弹出树*/
			$('#seachDept').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#dept').val();
					var defaultName = $('#deptName').val();
					//alert(defaultCode+"==="+defaultName);
					var offset = getOffset('maded');
					if($('#sta').val() == 1 || $('#sta').val() == 2){//估清和千人用量
						top.cust('<fmt:message key="please_select_positions" />',encodeURI('<%=path%>/misbohcommon/findPositnSuper.do?typn=7&iffirm=0&mold=oneTmany&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deptName'),$('#dept'),'760','520','isNull',hander);
					}else{
						top.cust('<fmt:message key="please_select_positions" />',encodeURI('<%=path%>/misbohcommon/findPositnSuper.do?typn=7&iffirm=0&mold=one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deptName'),$('#dept'),'760','520','isNull',hander);
					}
				}
			});
			
			
			setElementHeight('.grid',['.tool'],$(document.body),85);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
		});
		
		function hander(){
			parent.$('#dept').val($('#dept').val());
		}
		//保存报货类别
		function save(){
			var bool = false;
			//1.校验订货单报货次数
			var flag = $('#flag').val();
			if(flag != '-1'){
				alert("以下报货类别已存在报货单"+flag+",请在报货单填制中查询！");
				return bool;
			}
			var dat = $('#maded').val();
			//2.验证订货时间
			if(checkOrderTime(dat)){
				return bool;
			}
			//3.是否有配送班表或者是否选择报货类别
			var code = $('#CategoryTable').find(':checkbox').filter(':checked');
			if(code.size() == 0){
				alert('<fmt:message key="misboh_DeclareGoodsGuide_The_current_order" />【'+dat+'】<fmt:message key="misboh_DeclareGoodsGuide_The_current_order1" />');
				return bool;
			}
			//4.选中报货类别是否设置了到货日期和下次到货日期 
			var data = {};
			var flag = true;
			code.each(function(i){
				var des = $(this).parents('tr').find('td:eq(1)').find('span').attr('title');
				var receiveDate = $(this).parents('tr').find('td:eq(2)').find('input').val();
				var nextReceiveDate = $(this).parents('tr').find('td:eq(3)').find('input').val();
				if(receiveDate == '' || nextReceiveDate == ''){
					alert(des+":<fmt:message key='misboh_DeclareGoodsGuide_error3' />");
					flag = false;
					return false;
				}
				if(receiveDate < dat){
					alert(des+":到货日不能小于订货日！");
					flag = false;
					return false;
				}
				if(nextReceiveDate <= receiveDate){
					alert(des+":下次到货日不能小于等于到货日！");
					flag = false;
					return false;
				}
				var category_Code = $(this).val();
				var codetyp = $(this).data('codetyp');
				if($('#sta').val() == 1){//千元用量
					codetyp = 1;
				}else if($('#sta').val() == 2){//估清
					codetyp = 6;
				}else if($('#sta').val() == 3){//安全库存
					codetyp = 4;
				}else if($('#sta').val() == 4){//周期平均
					codetyp = 8;
				}
				var days = $(this).data('days');
				var des = $(this).data('des');
				data['sdList['+i+'].category_Code'] = category_Code;
				data['sdList['+i+'].codetyp'] = codetyp;
				data['sdList['+i+'].des'] = des;
				data['sdList['+i+'].days'] = days;
				data['sdList['+i+'].receiveDate'] = receiveDate;
				data['sdList['+i+'].nextReceiveDate'] = nextReceiveDate;
			});
			if(!flag){
				return bool;
			}
			$("#mainForm *[name]").each(function(){
				var name = $(this).attr("name"); 
				if(name) data[name] = $(this).val();
			});
			//这里改为启用档口的就必须选档口进行预估报货20160720wjf
			if($('#isdept').val() == 'Y'){
				if($('#dept').val() == ''){
					alert('请选择要报货的部门！');
					return bool;
				}
			}
			$.ajax({
				url:'<%=path%>/chkstomForecast/saveCodetyp.do',
				data:data,
				type:'post',
				success:function(msg){
					if(msg != "0"){
						bool = false;
						alert('<fmt:message key="misboh_DeclareGoodsGuide_salePlanError"/>\n'+msg);
					}else{
						bool = true;
					}
				}
			});
			if(!bool)
				return bool;
			return bool;
		}
		
		// 检测报货时间
		function checkOrderTime(dat){
			var param = {};
			var bool = false;
			param['dat'] = dat;
			$.post('<%=path%>/chkstomForecast/checkOrderTime.do',param,function(msg){
				if(msg!=null && msg!='1'){
					alert('<fmt:message key="misboh_DeclareGoodsGuide_orderTimeError"/>【'+msg+'】<fmt:message key="misboh_DeclareGoodsGuide_orderTimeError1"/>');
					bool = true;
				}
			});
			return bool;
		}
		
		// 获取系统当前时间
		Date.prototype.format = function(){	
			var yy = String(this.getFullYear());
			var mm = String(this.getMonth() + 1);
			var dd = String(this.getDate());
			if(mm<10){
				mm = ''+0+mm;
			}
			if(dd<10){
				dd = ''+0+dd;
			}
			var str = yy+"-"+mm+"-"+dd;
			return str;
		};
		
		// 设置下次到货日
		function setNEXTRECEIVEDATE(_RECEIVEDATE,_index){
			var _value=$('#'+_RECEIVEDATE).val();
			new WdatePicker({minDate:_value});
			$("#"+_index).attr("checked",true);
		}
		// 设置到货日
		function setRECEIVEDATE(_index){
	 			new WdatePicker({minDate:$('#bdat').val()});
	 			$("#"+_index).attr("checked",true);
		}
		// 查看配送班表
		function getScheduleMis(){
	    	$('body').window({
				id: 'window_addScheduleMis',
				title: '<fmt:message key="The_shipping_schedule" />',
				content: '<iframe id="addscheduleMisFrame" name="addscheduleMisFrame" frameborder="0" src="<%=path%>/scheduleMis/list.do?sta=1"></iframe>',
				width: '99%',//'1000px'
				height: '95%',
				draggable: true,
				isModal: true,
				confirmClose: false
			});
		}
		
		//启用配送班表的选了日期后刷新页面
		function updatDate(){
			$("#mainForm").submit();
		}
		
		//西贝之前用的 14:00报货
		function updatNextDate(){
			var maded = $('#maded').val();
			var timenow = new Date();
			var time14 = new Date();
			time14.setHours(14,0,0);
			var hoped = getInd(Date.parse(maded));
			if(timenow.getTime() > time14.getTime()){
				hoped = getInd(Date.parse(hoped));
			}
			$('.nextDate').val(hoped);
		}
		
		//西贝现在用的
		function updatNextDate2(){
			var maded = $('#maded').val();
			var hoped = getInd(Date.parse(maded));
			$('.nextDate').val(hoped);
			$('.nextDate2').val(getInd(Date.parse(hoped)));
		}
		
		//获取系统时间
		function getInd(maded){
			var myDate=new Date(maded + 24*60*60*1000);  
			var yy=myDate.getFullYear();
			var MM=myDate.getMonth()+1;
			var dd=myDate.getDate();
			if(MM<10)
				MM="0"+MM;
			if(dd<10)
				dd="0"+dd;
			return fullDate=yy+"-"+MM+"-"+dd;
		}
		</script>
	</head>
	<body>
		<form id="mainForm" method="post" action="<%=path%>/chkstomForecast/listRightCf.do">
		<input type="hidden" id="sta" value="${cf.sta}"/> 
		<input type="hidden" id="isdept" name ="isdept" value="${cf.isdept}"/> 
		<input type="hidden" name ="flag" id="flag" value="${flag}"/> 
		<input type="hidden" name ="isuseschedule" value="${cf.isuseschedule}"/> 
		<input type="hidden" name="firm" id="firm" value="${cf.firm}"/>
		 	<div class=bj_head>
				<div class="form-line">
					<div class="form-label"><fmt:message key="misboh_DeclareGoodsGuide_date"/>：</div>
					<div class="form-input" style="width:100px;text-align: center;">
						<c:if test="${cf.orderDayControl == 1 }">
							<fmt:formatDate value="${cf.maded}" pattern="yyyy-MM-dd"/>
							<input type="hidden" id="maded" name="maded"  value="<fmt:formatDate value="${cf.maded}" pattern="yyyy-MM-dd"/>" />
						</c:if>
						<c:if test="${cf.orderDayControl != 1 }">
							<input style="width:100px;"  type="text" id="maded" name="maded" class="Wdate text" value="<fmt:formatDate value="${cf.maded}" pattern="yyyy-MM-dd"/>" 
								<c:if test ="${cf.isuseschedule == 'Y'}"> onchange="updatDate();" </c:if>
<%-- 								<c:if test ="${isNotUpdateHoped == 'Y'}"> onchange="updatNextDate();" </c:if> --%>
								<c:if test ="${isNotUpdateHoped == 'Y'}"> onchange="updatNextDate2();" </c:if>
							  onfocus="new WdatePicker({minDate:new Date()})"/>
						</c:if>
						<!-- 判断是否启用配送班表   Y 启用  N 禁用 -->
					</div>
					<div class="form-input" style="width:80px;bottom: -3px;">
						<c:if test ="${cf.isuseschedule == 'Y'}">
							<input type="button" value="<fmt:message key="misboh_DeclareGoodsGuide_ckpsbb"/>" onclick="getScheduleMis()"/>
						</c:if>
					</div>
					<c:if test ="${cf.isdept == 'Y'}">
						<div class="form-label"><fmt:message key="sector"/>：</div>
						<div class="form-input" style="width:205px;">
							<input style="width:95px;" type="hidden" id="dept" name="dept" value="${cf.dept }"/>
							<input style="width:95px;" type="text" id="deptName" name="deptName" value="${cf.deptName }"/>
							<img id="seachDept" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="misboh_DeclareGoodsGuide_dept" />' />
						</div>
					</c:if>
				</div>
			</div>
			<div class="grid">
				<div class="table-head">
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:15px;"></span></td>
								<td><span style="width:240px;"><fmt:message key="Daily_goods_category"/></span></td>
								<td><span style="width:150px;"><fmt:message key="arrival_date1"/></span></td>
								<td><span style="width:150px;"><fmt:message key="misboh_DeclareGoodsGuide_nextArrival_date"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" id="CategoryTable">
						<tbody>
							<!-- 是否启用配送班表   N 禁用 Y 启用 -->
							<c:if  test ="${cf.isuseschedule =='Y' }">
								<c:forEach var="scheduleD" items="${codetypList}" varStatus="sta">
									<tr>
										<td><span style="width:15px;"> 
											<input type="checkbox" id="${sta.index}" value="${scheduleD.category_Code}" 
												<c:if test="${scheduleD.nextReceiveDate != null }">
													checked="checked" 
												</c:if> disabled="disabled" data-codetyp="${scheduleD.codetyp}" data-days="${scheduleD.days}" data-des="${scheduleD.des}"/></span> 
										</td>
										<td><span title="${scheduleD.des}" id="chk_DES${sta.index}"  style="width:240px;">${scheduleD.des}&nbsp;
										</span></td>
										<td><span title="${scheduleD.receiveDate}" style="width:150px;">${scheduleD.receiveDate}
											<input  type="hidden" id="chk_RECEIVEDATE${sta.index}" value="${scheduleD.receiveDate}" />
										</span></td>
										<td><span title="${scheduleD.nextReceiveDate}" style="width:150px;">${scheduleD.nextReceiveDate}
										<input  type="hidden" id="chk_NEXTRECEIVEDATE${sta.index}" value="${scheduleD.nextReceiveDate}" />
										</span></td>
									</tr>
								</c:forEach>
							</c:if>
							<c:if  test ="${cf.isuseschedule !='Y' }">
								<c:forEach var="scheduleD" items="${codetypList}" varStatus="sta">
									<tr>
										<td><span  style="width:15px;"> 
											<input type="checkbox" id="${sta.index}" value="${scheduleD.category_Code}" <c:if test="${!empty scheduleD.receiveDate}">checked="checked"</c:if>
												data-codetyp="${scheduleD.codetyp}" data-days="${scheduleD.days}" data-des="${scheduleD.des}"/></span> 
										</td>
										<td><span title="${scheduleD.des}" style="width:240px;" id="chk_DES${sta.index}">${scheduleD.des}&nbsp;
										</span></td>
										<td><span title="${scheduleD.receiveDate}" style="width:150px;">
											<input  type="text" id="chk_RECEIVEDATE${sta.index}" style="width:120px;" class="Wdate text nextDate" value="${scheduleD.receiveDate}" 
											<c:choose>
												<c:when test="${isNotUpdateHoped == 'Y' }"> value="${scheduleD.receiveDate}" readonly="readonly"</c:when>
												<c:otherwise> onclick="setRECEIVEDATE('${sta.index}')" onfocus="setRECEIVEDATE('${sta.index}')" </c:otherwise>
											</c:choose>/>
										</span></td>
										<td><span title="${scheduleD.nextReceiveDate}" style="width:150px;">
											<input  type="text" id="chk_NEXTRECEIVEDATE${sta.index}" style="width:120px;" class="Wdate text nextDate2" value="${scheduleD.nextReceiveDate}" 
											<c:choose>
												<c:when test="${isNotUpdateHoped == 'Y' }"> value="${scheduleD.nextReceiveDate}" readonly="readonly"</c:when>
												<c:otherwise> onclick="setNEXTRECEIVEDATE('chk_RECEIVEDATE${sta.index}','${sta.index}')" onfocus="setNEXTRECEIVEDATE('chk_RECEIVEDATE${sta.index}','${sta.index}')" </c:otherwise>
											</c:choose>/>
										</span></td>
									</tr>
								</c:forEach>
							</c:if>
						</tbody>
					</table>
				</div>
			</div>
			</form>
	</body>
</html>