<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript">
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		
		$(document).ready(function(){
			parent.stopVisibility();
			$(document).bind('keyup',function(e){
		 		//表格数量一变，校验一下，价格就接着变
		 		if($(e.srcElement).is("input")){//对表格内的输入框进行判读，延迟600毫秒
		 			var index=$(e.srcElement).closest('td').index();
		    		if(index=="9"){
			 			$(e.srcElement).unbind('blur').blur(function(e){
			 				validateByMincnt($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
			 			});
			    		validator($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
		    		}
		    	}
			});
		 	
		   $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		   $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
			//自动实现滚动条
			setElementHeight('.grid',['.bj_head'],$(document.body),90);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法	
			editCells();
			$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),9);
		});
		
		//编辑表格
		function editCells(){
			if($(".table-body").find('tr').length == 0){
				return;
			}
			$(".table-body").autoGrid({
				initRow:1,
				colPerRow:12,
				widths:[46,80,110,100,60,80,80,80,80,60,80,90],
				colStyle:['','','','','','','','','','',{background:"red"},{background:"#F1F1F1"}],
				VerifyEdit:{verify:true,enable:function(cell,row){
					return row.find('td').index(cell) == 2 || (row.find("td:eq(2)").text() == null || $.trim(row.find("td:eq(2)").text()) != '' ) ? true : false;
				}},
				onEdit:$.noop,
				editable:[9,11],
				onLastClick:function(row){
				},
				onEnter:function(data){
					$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.value) ;
				},
				cellAction:[{
					index:9,
					action:function(row,data2){
						if(Number(data2.value) < 0){
							alert('<fmt:message key="number_cannot_be_negative"/>！');
							row.find("td:eq(9)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,9);
						}else if(isNaN(data2.value)){
							alert('<fmt:message key="number_be_not_number"/>！');
							row.find("td:eq(9)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,9);
						}else if(data2.value.length > 6){
							alert('<fmt:message key="the_maximum_length_not"/>6<fmt:message key="digit"/>！');
							row.find("td:eq(9)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,9);
						}else if(Number(data2.value) != 0 && Number(row.data("stomin"))!=0 && (Number(data2.value) < Number(row.data('stomin')))){
							alert('<fmt:message key="the_minimum_purchase_quantity_of_material"/>'+row.data("stomin")+'.');
							row.find("td:eq(9)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,9);
						}else if(Number(data2.value) != 0 && Number(row.data("stocnt"))!=0 && !(/(^[1-9]\d*$)/.test(Number(data2.value)/Number(row.data("stocnt"))))){
							alert('申购量必须是申购倍数'+row.data("stocnt")+'的整数倍！');
							row.find("td:eq(9)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,9);
						}else if(Number(data2.value) != 0 && Number(row.data("stocnt"))==0 && Number(row.data("mincnt"))!=0 && !(/(^[1-9]\d*$)/.test(Number(data2.value)/Number(row.data("mincnt"))))){
							alert('申购量必须是申购倍数'+row.data("mincnt")+'的整数倍！');
							row.find("td:eq(9)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,9);
						}else{
							$.fn.autoGrid.setCellEditable(row.next(),9);
						}
					}
				},{
					index:11,
					action:function(row,data){
						if(data.value.length > 30){
							alert('<fmt:message key="the_maximum_length_not"/>30<fmt:message key="digit"/>');
							row.find("td:eq(11)").find('span').text(data.value.substring(0,30));
							$.fn.autoGrid.setCellEditable(row,11);
						}
						$.fn.autoGrid.setCellEditable(row.next(),9);
					}
				}]
			});
		}
		
		function validateByMincnt(index,row,data2){//最小申购量判断
			if(index=="9"){
				if(Number(data2.value) != 0 && Number(row.data("stomin"))!=0 && (Number(data2.value) < Number(row.data('stomin')))){
					alert('<fmt:message key="the_minimum_purchase_quantity_of_material"/>'+row.data("stomin")+'.');
					row.find("td:eq(9)").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,9);
				}else if(Number(data2.value) != 0 && Number(row.data("stocnt"))!=0 && !(/(^[1-9]\d*$)/.test(Number(data2.value)/Number(row.data("stocnt"))))){
					alert('申购量必须是申购倍数'+row.data("stocnt")+'的整数倍！');
					row.find("td:eq(9)").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,9);
				}else if(Number(data2.value) != 0 && Number(row.data("stocnt"))==0 && Number(row.data("mincnt"))!=0 && !(/(^[1-9]\d*$)/.test(Number(data2.value)/Number(row.data("mincnt"))))){
					alert('申购量必须是申购倍数'+row.data("mincnt")+'的整数倍！');
					row.find("td:eq(9)").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,9);
				}
			}
		}
		function validator(index,row,data2){//输入框验证
			if(index=="9"){
				if(Number(data2.value) < 0){
					alert('<fmt:message key="number_cannot_be_negative"/>！');
					row.find("td:eq(9)").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,9);
				}else if(isNaN(data2.value)){
					alert('<fmt:message key="number_be_not_number"/>！');
					row.find("td:eq(9)").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,9);
				}else if(data2.value.length > 6){
					alert('<fmt:message key="the_maximum_length_not"/>6<fmt:message key="digit"/>！');
					row.find("td:eq(9)").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,9);
				}else{
					row.find("input").data("ovalue",data2.value);
				}
			}
		}
		
		//生成报货单
		function saveChkstom(){
			var grid = $('.easyui-tabs').find('.grid');
			grid.each(function(){
// 				var numNull=0;//默认0代表成功
// 				var isNull=0;
// 				var sp_name = '';
// 				rows.each(function(){
// 					isNull=1;
// 					var amount = $(this).find('td:eq(10)').text()?$(this).find('td:eq(10)').text():$(this).find('td:eq(10) input').val();
// 					if(Number(amount)==0){
// 						numNull=1;//1数量为0
// 						sp_name = $(this).find('td:eq(2)').text()?$(this).find('td:eq(2)').text():$(this).find('td:eq(2) input').val();
// 						return false;
// 					}
// 					if(amount=="" || amount==null || isNaN(amount)){
// 						numNull=2;//2数量为空
// 						sp_name = $(this).find('td:eq(2)').text()?$(this).find('td:eq(2)').text():$(this).find('td:eq(2) input').val();
// 						return false;
// 					}
// 				});
// 				if(Number(isNull)==0){
// 					return false;
// 				}
// 				if(Number(numNull)==1){//数量不为0
// 					alert('<fmt:message key="supplies"/>：['+sp_name +']<fmt:message key="number_cannot_be_zero"/>！');
// 					flag = false;
// 					return false;
// 				}
// 				if(Number(numNull)==2){//数量为空或字母
// 					alert('<fmt:message key="supplies"/>：['+sp_name +']<fmt:message key="quantity"/><fmt:message key="not_valid_number"/>！');
// 					return false;
// 				}
				var data = {};
				data['maded'] = $('#maded').val();
				var chkstoNo = $(this).find('input[name="chkstoNo"]').val();
				data['chkstoNo'] =  chkstoNo;
				data['manifsttyp'] =  $(this).find('input[name="manifsttyp"]').val();
				var hoped = $(this).find('input[name="hoped"]').val();
				var i = 0;
				var rows = $(this).find(".table-body").find("table").find("tr");
				rows.each(function(){
					var sp_code = $(this).find('td:eq(1)').text();
					var amount1 = $(this).find('td:eq(9)').text()?$(this).find('td:eq(9)').text():$(this).find('td:eq(9) input').val();
					if(Number(amount1) == 0 || amount1=="" || amount1==null || isNaN(amount1)){//如果数量是0,不是 个数等等 则过滤掉
						return true;
					}
					var unitper3 = Number($(this).data('unitper3'));
					var amount = 0;
					if(unitper3 != 0){
						amount = amount1/unitper3;
					}
					var memo = $(this).find('td:eq(11)').text()?$(this).find('td:eq(11)').text():$(this).find('td:eq(11) input').val();
					data["chkstodList["+i+"].supply.sp_code"] = sp_code;
					data["chkstodList["+i+"].amount1"] = amount1;
					data["chkstodList["+i+"].amountdis"] = amount1;
					data["chkstodList["+i+"].amount"] = amount;
					data["chkstodList["+i+"].hoped"] = hoped;
					data["chkstodList["+i+"].memo"] = memo;
					if(('${cf.sta}' == 1 || '${cf.sta}' == 2) && '${cf.dept}' != ''){
						var positn = $(this).find('td:eq(12) span').attr('data-positn');
						data["chkstodList["+i+"].positn"] = positn;
					}
					i++;
				});
				if(i == 0){//没有明细，直接跳过
					alert('<fmt:message key="scm_document_no"/>:'+chkstoNo+'没有明细！已跳过！');
					return true;
				}
				//提交并返回值，判断执行状态
				parent.showVisibility();
				$.post("<%=path%>/chkstomForecast/saveChkstom.do",data,function(data){
					parent.stopVisibility();
					if(data == 1){
						alert('<fmt:message key="scm_document_no"/>:'+chkstoNo+'<fmt:message key="save_successful"/>！');
					}else{
						alert('<fmt:message key="save_fail"/>！');
					}
				});			 
			});
		}
		
		</script>
	</head>
	<body>
		<form id="mainForm" method="post" action="">
		<div class="easyui-tabs"  fit="false" plain="true" style="z-index:88;">
			<input type="hidden" id="maded" name="maded" value="<fmt:formatDate value="${cf.maded}" pattern="yyyy-MM-dd"/>" />
			<c:forEach var="sd" items="${cf.sdList}" varStatus="sta">
				<div title='${sd.des}' style="width:100%;">
					<div class="bj_head">
						<div class="form-line">
							<div class="form-label"><fmt:message key="scm_document_no"/>:</div>
							<div class="form-input" style="width:80px;">
								<c:if test="${sd.chkstono!=null}"><c:out value="${sd.chkstono }"></c:out></c:if>
							</div>
						</div>
					</div>
			 	<div class="grid">
			 		<input type="hidden" name="chkstoNo" value="${sd.chkstono }"/>
					<input type="hidden" name="manifsttyp" value="${sd.category_Code }"/><!-- 报货类别 -->
					<input type="hidden" name="hoped" value="${sd.receiveDate }"/><!-- 到货日期 -->
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num" style="width: 46px;"><span >&nbsp;</span></td>
									<td colspan="4"><fmt:message key="supplies"/></td>
									<td rowspan="2"><span style="width:70px;"><fmt:message key="minimum_inventory" /></span></td>
									<td rowspan="2"><span style="width:70px;"><fmt:message key="current_inventory" /></span></td>
									<td rowspan="2"><span style="width:70px;"><fmt:message key ="ontheway" /></span></td>
									<td rowspan="2"><span style="width:70px;"><fmt:message key="computation" /></span></td>
									<td colspan="2">
										<c:choose>
											<c:when test="${cf.isDistributionUnit == 'Y' }"><fmt:message key="Distribution_Unit"/></c:when>
											<c:otherwise><fmt:message key="procurement_unit" /></c:otherwise>
										</c:choose>
									</td>
									<td rowspan="2"><span style="width:80px;"><fmt:message key="remark" /></span></td>
									<c:if test="${(cf.sta == 1 or cf.sta == 2) and !empty cf.dept }">
										<td rowspan="2"><span style="width:80px;"><fmt:message key="positions" /></span></td>
									</c:if>
								</tr>
								<tr>									
									<td class="num" style="width: 26px;"><span >&nbsp;</span></td>
									<td><span style="width:70px;"><fmt:message key="supplies_code"/></span></td>
									<td><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
									<td><span style="width:90px;"><fmt:message key="specification"/></span></td>
									<td><span style="width:50px;"><fmt:message key="unit"/></span></td>
									<td><span style="width:70px;"><fmt:message key="adjustment_amount" /></span></td>
									<td><span style="width:50px;"><fmt:message key="unit"/></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0">
							<c:forEach var="declare" items="${sd.declareList}" varStatus="status">
								<tr <c:choose>
										<c:when test="${cf.isDistributionUnit == 'Y' }">data-stomin="0" data-stocnt="0" data-mincnt="${declare.dismincnt}" data-unitper3="${declare.disunitper }"</c:when>
										<c:otherwise>data-stomin="${declare.stomin}" data-stocnt="${declare.stocnt}" data-mincnt="${declare.mincnt}" data-unitper3="${declare.unitper3 }"</c:otherwise>
									</c:choose>>
									<td align="center" style="width:46px;"><span >${status.index+1}</span></td>
									<td><span style="width:70px;">${declare.sp_code }</span></td>
									<td><span style="width:100px;">${declare.sp_name }</span></td>
									<td><span style="width:90px;">${declare.sp_desc }</span></td>
									<td><span style="width:50px;">${declare.unit }</span></td>
									<td><span style="width:70px;text-align: right;"><fmt:formatNumber value="${declare.spmin}" type="currency" pattern="0.00"/></span></td>
									<td><span style="width:70px;text-align: right;"><fmt:formatNumber value="${declare.spnow}" type="currency" pattern="0.00"/></span></td>
									<td><span style="width:70px;text-align: right;"><fmt:formatNumber value="${declare.spway}" type="currency" pattern="0.00"/></span></td>
									<td><span style="width:70px;text-align: right;"><fmt:formatNumber value="${declare.spcal}" type="currency" pattern="0.00"/></span></td>
									<c:choose>
										<c:when test="${cf.isDistributionUnit == 'Y' }">
											<td><span style="width:70px;text-align: right;"><fmt:formatNumber value="${declare.amountdis}" type="currency" pattern="0.00"/></span></td>
											<td><span style="width:50px;">${declare.disunit }</span></td>
										</c:when>
										<c:otherwise>
											<td><span style="width:70px;text-align: right;"><fmt:formatNumber value="${declare.spuse}" type="currency" pattern="0.00"/></span></td>
											<td><span style="width:50px;">${declare.unit3 }</span></td>
										</c:otherwise>
									</c:choose>
									<td><span style="width:80px;">${declare.memo}</span></td>
									<c:if test="${(cf.sta == 1 or cf.sta == 2) and !empty cf.dept }">
										<td><span style="width:80px;" data-positn="${declare.positn}" title="${declare.positndes}">${declare.positndes}</span></td>
									</c:if>
								</tr>
							</c:forEach>
						</table>					
					</div>
				</div>
				</div>
			</c:forEach>		 	
		</div>
		</form>
	</body>
</html>