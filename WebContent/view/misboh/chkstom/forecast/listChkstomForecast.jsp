<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="misboh_DeclareGoodsGuide"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<style type="text/css">
			.moduleInfo {
				//background-color: #E1E1E1;
			}
			.leftFrame{
				width:12%;
			}
			.mainFrame{
				width:88%;
			}
		</style>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				loadBtn(false,true,false);
			});
			//ajax同步设置
			$.ajaxSetup({
				async: false
			});
			var declareGoods;
			var _index = 0;
			function loadBtn(last,next,_ok){
				$('.tool').text("");
				$('.tool').toolbar({
					items: [{
							text: '<fmt:message key="misboh_DeclareGoodsGuide_last" />',
							title: '<fmt:message key="misboh_DeclareGoodsGuide_last" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&last,
							handler: function(){
								preview();
							}
						},{
							text: '<fmt:message key="misboh_DeclareGoodsGuide_next" />',
							title: '<fmt:message key="misboh_DeclareGoodsGuide_next" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&next,
							handler: function(){
								// 执行点击下一步参数
								generateReport();
							}
						},{
							text: '<fmt:message key="confirm" />',
							title: '<fmt:message key="confirm" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&_ok,
							handler: function(){	
								confirmation();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							useable:true,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								closeChkstomForecast();
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}
					]
				});
			}
			
			//上一步
			function preview(){
				_index --;
				showVisibility();
				var url = window.frames["leftFrame"].setPrevLeft(_index);
				if(url.indexOf('?') != -1){//加这是上一步的标志
					url += '&step=-1';
				}else{
					url += '?step=-1';
				}
				window.frames["mainFrame"].location.href = '<%=path%>'+url;
				//按钮控制
				if(_index==0){
					loadBtn(false,true,false);
				}else{
					loadBtn(true,true,false);
				}
// 				stopVisibility();
			}
			
			//下一步
			function generateReport(){
				showVisibility();
				//1.右侧界面先保存
				if(!window.frames["mainFrame"].save()){//每一个页面都有个save方法
					stopVisibility();
					return;
				}
				_index ++;
				//2.左侧界面刷新
				var url = window.frames["leftFrame"].setNextLeft(_index);
				//3.根据左侧页面得到右侧页面
				if(url != ''){
					window.frames["mainFrame"].location.href = '<%=path%>'+url;
				}
				if(url.indexOf('/chkstomForecast/toSaveChkstom.do') != -1){
					$('#wait').css('left','30%').find('span').text('系统正在进行大量计算，过程可能需要几分钟，请您耐心等待！LOADING...');
					loadBtn(true,false,true);
				}else{
					loadBtn(true,true,false);
				}
// 				stopVisibility();
			}
			
			//确认生成订货单
			function confirmation(){
				showVisibility();
				window.frames["mainFrame"].saveChkstom();
				if($('#dept').val() != '')
					openChkstomDept();
				else
					openChkstom();
				$('#mainForm').submit();
			}
			//打开报货单填制提交
			function openChkstom(){
				showInfo('04948F998FC54E15B049488A6DC8037D','<fmt:message key="Report_order"/><fmt:message key="Fill_in"/>','/chkstomMis/chkstomTable.do');
				searchUncheck('iframe_04948F998FC54E15B049488A6DC8037D');
			}
			function openChkstomDept(){
				showInfo('21B89378A5024E8498B071D12963E008','<fmt:message key="scm_dept"/><fmt:message key="Report_order"/><fmt:message key="Fill_in"/>','/chkstomDeptMis/table.do?upd=0');
				searchUncheck('iframe_21B89378A5024E8498B071D12963E008');
			}
			function showInfo(moduleId,moduleName,moduleUrl){
	  	 		window.parent.tabMain.addItem([{
		  				id: 'tab_'+moduleId,
		  				text: moduleName,
		  				title: moduleName,
		  				closable: true,
		  				content: '<iframe id="iframe_'+moduleId+'" name="iframe_'+moduleId+'" frameborder="0" src="<%=path%>'+moduleUrl+'"></iframe>'
		  			}
		  		]);
	  	 		window.parent.tabMain.show('tab_'+moduleId);
	  	 	}
			function searchUncheck(id){
	  	 		var bdate = window.frames["mainFrame"].document.getElementById('maded').value;
				var edate = window.frames["mainFrame"].document.getElementById('maded').value;
	  	 		window.setTimeout(function(){
					var frame = parent.document.getElementById(id);
					if(frame && frame.contentWindow)
						frame.contentWindow.searchUncheck(bdate,edate);
				},1000);
	  	 	}
			
			//清除后台session
			function closeChkstomForecast(){
				$.ajax({
					url:'<%=path%>/chkstomForecast/closeChkstomForecast.do',
					type:"post",
					success:function(data){}
				});
			}
			
			function showVisibility(){
				$("#wait2").show();
				$("#wait").show();
			}
			
			function stopVisibility(){
				$('#wait').css('left','50%').find('span').text('LOADING...');
				$("#wait2").hide();
				$("#wait").hide();
			}
			
		</script>
	</head>
	<body>
		<input type="hidden" id="dept" value=""/>
		<form id="mainForm" action="<%=path%>/chkstomForecast/toChkstomForecast.do?sta=${cf.sta}" method="post"></form>
		<div class="tool"></div>
		<div class="leftFrame">
			<iframe src="<%=path%>/chkstomForecast/listLeftCf.do?sta=${cf.sta}" frameborder="0" name="leftFrame" id="leftFrame"></iframe>
    	</div>
    	<div class="mainFrame">
			<iframe src="<%=path%>/chkstomForecast/listRightCf.do?sta=${cf.sta}"  frameborder="0" name="mainFrame" id="mainFrame"></iframe>
    	</div>
	</body>
</html>