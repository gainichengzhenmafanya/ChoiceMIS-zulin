<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		$(document).ready(function(){
			parent.stopVisibility();
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),95);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			
// 			loadToolBar();
			
			$('#calculate').click(function(){
				if (!confirm('确认重新计算千人用量吗？')) {
					return;
				}
				$('#listForm').submit();
				parent.showVisibility();
			});
			
		});
		
		function loadToolBar(){
			$('.tool').html('');
			$('.tool').toolbar({
				items: [{
					text: '<fmt:message key="calculate" />',
					title: '<fmt:message key="calculate" />',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-80px','-0px']
					},
					handler: function(){
						$('#listForm').submit();
						parent.showVisibility();
					}
				}]
			});
		}
		
		//计算周期用量
		function gclick(obj){
			var forecastMoney = Number($(obj).parents('tr').data('forecastmoney'));
			var val = Number($(obj).val());
			$(obj).parents('td').next('td').find('span').text(Number(val*forecastMoney).toFixed(2));
		}
		
		//回车下一个
		function eqEnter(obj){
			if(event.keyCode == 13){
				if($(obj).parents('tr').next('tr').length != 0)
					$(obj).parents('tr').next('tr').find('input').focus();
				else
					$('.table-body').find('tr:first').find('input').focus();
			}
		}
		
		function save(){
			if($('.table-body').find('tr').size() == 0){
				alert('没有数据，不能进行下一步，请重新计算。');
				return false;
			}
			var data = {};
			if(confirm('当前报货方式:千元用量，预估的报货单最终调整量是否考虑安全库存、当前库存、在途数量？\n点击确定，则最终调整量=周期用量+安全库存-当前库存-在途数量，最后向上取整\n点击取消，则最终调整量=周期用量，最后向上取整')){
				data['qyylCalWay'] = 'Y';
			} else {
				data['qyylCalWay'] = 'N';
			}
			$('.table-body').find('tr').each(function(i){
				data['qyylList['+i+'].forecastMoney'] = $(this).attr('data-forecastmoney');
				data['qyylList['+i+'].positn'] = $(this).attr('data-positn');
				data['qyylList['+i+'].positndes'] = $(this).attr('data-positndes');
				data['qyylList['+i+'].sp_code'] = $(this).attr('data-sp_code');
				data['qyylList['+i+'].sp_name'] = $(this).attr('data-sp_name');
				data['qyylList['+i+'].sp_desc'] = $(this).attr('data-sp_desc');
				data['qyylList['+i+'].unit'] = $(this).attr('data-unit');
				data['qyylList['+i+'].spcal'] = $(this).attr('data-spcal');
				data['qyylList['+i+'].spuse'] = $(this).find('td:eq(5)').find('input').val();
				data['qyylList['+i+'].periodUse'] = $(this).find('td:eq(6)').find('span').text();
			});
			var bool = false;
			$.ajax({
				url:'<%=path%>/chkstomForecast/saveSpcodeUse.do',
				data:data,
				type:'post',
				success:function(msg){
					bool = true;
				}
			});
			return bool;
		}
			
		</script>
	</head>
	<body>
		<div class="tool"></div>
		<form id="listForm" method="post" action="<%=path%>/chkstomForecast/listSpcodeUse.do">
			<input type="hidden" name="sta" value="1"/>
			<div class="bj_head">
				<div class="form-line">
					<div class="form-label"><fmt:message key="the_reference_date"/>:</div>
					<div class="form-input" style="width:250px;">
						<font style="color:blue;">从:</font>
						<input type="text" style="width:90px;margin-top: -3px;" id="bdate" name="bdate" value="${cf.bdate }" class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edate\')}'});"/>
						<font style="color:blue;"><fmt:message key="to"/>:</font>
						<input type="text" style="width:90px;margin-top: -3px;" id="edate" name="edate" value="${cf.edate }" class="Wdate text" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdate\')}'});"/>
					</div>
					<div class="form-input">
						<input type="button" style="width:80px" id="calculate" name="calculate" value="重新计算"/>
					</div>
				</div>
			</div>
			<div class="grid">
				<div class="table-head">
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td colspan="4"><fmt:message key="supplies"/></td>
								<c:if test="${cf.vplantyp == 'PAX'}">
									<td rowspan="2"><span style="width:80px;"><fmt:message key="misboh_DeclareGoodsGuide_ThousandsPeopleOfDosage"/></span></td>
									<td rowspan="2"><span style="width:80px;"><fmt:message key="misboh_DeclareGoodsGuide_ThousandsPeopleOfDosage_edit"/></span></td>
									<td rowspan="2"><span style="width:80px;"><fmt:message key="misboh_DeclareGoodsGuide_Periodic_quantity"/></span></td>
								</c:if>
								<c:if test="${cf.vplantyp == 'AMT'}">
									<td rowspan="2"><span style="width:80px;">万元用量</span></td>
									<td rowspan="2"><span style="width:80px;">万元用量调整</span></td>
									<td rowspan="2"><span style="width:80px;"><fmt:message key="misboh_DeclareGoodsGuide_Periodic_quantity"/></span></td>
								</c:if>
								<c:if test="${cf.vplantyp == 'TC'}">
									<td rowspan="2"><span style="width:80px;"><fmt:message key="misboh_DeclareGoodsGuide_ThousandsTimeOfDosage"/></span></td>
									<td rowspan="2"><span style="width:80px;"><fmt:message key="misboh_DeclareGoodsGuide_ThousandsTimeOfDosage"/></span></td>
									<td rowspan="2"><span style="width:80px;"><fmt:message key="misboh_DeclareGoodsGuide_Periodic_quantity"/></span></td>
								</c:if>
								<td rowspan="2"><span style="width:100px;"><fmt:message key="sector"/></span></td>
							</tr>
							<tr>
								<td><span style="width:70px;"><fmt:message key="supplies_code"/></span></td>
								<td><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
								<td><span style="width:90px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:50px;"><fmt:message key="unit"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="declare" items="${cf.qyylList}" varStatus="status">
								<tr data-forecastmoney="${declare.forecastMoney }" data-spcal=${declare.spcal } data-positn="${declare.positn}" data-positndes="${declare.positndes}" 
									data-sp_code="${declare.sp_code }" data-sp_name="${declare.sp_name }" data-sp_desc="${declare.sp_desc }" data-unit="${declare.unit }">
								<td><span style="width:70px;">${declare.sp_code }</span></td>
								<td><span style="width:100px;">${declare.sp_name }</span></td>
								<td><span style="width:90px;">${declare.sp_desc }</span></td>
								<td><span style="width:50px;">${declare.unit }</span></td>
								<td><span style="width:80px;text-align: right;"><fmt:formatNumber value="${declare.spcal }" type="currency" pattern="0.00"/></span></td>
								<td>
									<span style="width:80px;text-align: right;">
										<input style="width:80px;text-align:right;" onkeyup="eqEnter(this)" onblur="gclick(this)" type="text" value="<fmt:formatNumber value="${declare.spuse }" type="currency" pattern="0.00"/>"/>';
									</span>
								</td>
								<td><span style="width:80px;text-align: right;"><fmt:formatNumber value="${declare.periodUse }" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:100px;" title="${declare.positndes}">${declare.positndes}</span></td>
							 </tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
	</body>
</html>