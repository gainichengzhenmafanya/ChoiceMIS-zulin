<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="purchase_the_template" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<style type="text/css">
		.blueBGColor{background-color:	#F1F1F1;}
		.redBGColor{background-color:	#F1F1F1;}
		.onEdit{
			background:lightblue;
			border:1px solid;
			border-bottom-color: blue;
			border-top-color: blue;
			border-left-color: blue;
			border-right-color: blue;
		}
		.input{
			background:transparent;
			border:1px solid;
		}
		</style>		
</head>
<body>
	<form id="listForm" action="<%=path%>/chkstomForecast/addChkstoDemo.do" method="post">
		<div class="bj_head">
		<div class="form-line">	
			<div class="form-label">报货类别:</div>
			<div class="form-input">
				<select class="select" id="typ_eas" name="typ_eas" onchange="findByTitle();">
					<c:forEach var="sd" items="${sdList}" varStatus="status">
						<option
							<c:if test="${sd.category_Code==typ_eas}"> selected="selected" </c:if> value="${sd.category_Code}">${sd.des}
						</option>
					</c:forEach>				
				</select>
			</div>
			<div class="form-label">选择模板:</div>
			<div class="form-input">
				<select class="select" id="chkstodemono" name="chkstodemono" onchange="findByTitle();">
					<c:forEach var="chkstodemo" items="${listTitle}" varStatus="status">
						<option
							<c:if test="${chkstodemo.chkstodemono==chkstodemono}"> selected="selected" </c:if> value="${chkstodemo.chkstodemono}">${chkstodemo.title}
						</option>
					</c:forEach>				
				</select>
			</div>
			<div class="form-label">
				<input type="button" id="save" class="text" style="width: 50px;cursor:pointer;height: 20px" 
					value="<fmt:message key="save"/>" title="如果多个报货类别，不同类别请手动保存！如果一个报货类别，直接下一步即可。" />
			</div>
		</div>
		</div>
		<div class="grid">		
			<div class="table-head" >
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<td class="num"><span style="width: 25px;"></span></td>
							<td><span style="width:80px;"><fmt:message key="supplies_code" /></span></td>
							<td><span style="width:120px;"><fmt:message key="supplies_name" /></span></td>
							<td><span style="width:80px;"><fmt:message key="specification" /></span></td>
							<td><span style="width:80px;">
								<c:choose>
									<c:when test="${isDistributionUnit == 'Y' }"><fmt:message key="Distribution_Unit"/></c:when>
									<c:otherwise><fmt:message key="procurement_unit" /></c:otherwise>
								</c:choose>
							</span></td>
							<td><span style="width:80px;"><fmt:message key="quantity" /></span></td>
							<td><span style="width:80px;"><fmt:message key="standard_unit" /></span></td>
							<td <c:if test="${isNotShowSp_price == 'Y' }">style="display:none;"</c:if>><span style="width:80px;"><fmt:message key="scm_standard_sale" /></span></td>
							<td><span style="width:120px;"><fmt:message key="remark" /></span></td>
						</tr>
					</thead>
				</table>
			</div>				
			<div class="table-body">
				<table cellspacing="0" cellpadding="0">
					<tbody>
						<c:forEach var="chkstodemo" items="${chkstodemoList}" varStatus="status">
							<tr <c:choose>
									<c:when test="${isDistributionUnit == 'Y' }">data-stomin="0" data-stocnt="0" data-mincnt="${chkstodemo.dismincnt}" data-unitper="${chkstodemo.disunitper }"</c:when>
									<c:otherwise>data-stomin="${chkstodemo.stomin}" data-stocnt="${chkstodemo.stocnt}" data-mincnt="${chkstodemo.supply.mincnt}" data-unitper="${chkstodemo.supply.unitper3 }"</c:otherwise>
								</c:choose>>
								<td class="num"><span style="width: 25px;">${status.index+1}</span></td>
								<td title="${chkstodemo.supply.sp_code}"><span  style="width:80px;">${chkstodemo.supply.sp_code}</span></td>
								<td title="${chkstodemo.supply.sp_name}"><span  style="width:120px;">${chkstodemo.supply.sp_name}</span></td>
								<td title="${chkstodemo.supply.sp_desc}"><span  style="width:80px;">${chkstodemo.supply.sp_desc}</span></td>
								<td><span style="width:80px;">
									<c:choose>
										<c:when test="${isDistributionUnit == 'Y' }">${chkstodemo.disunit }</c:when>
										<c:otherwise>${chkstodemo.supply.unit3 }</c:otherwise>
									</c:choose>
								</span></td>
								<td><span style="width:80px;text-align:right;"><fmt:formatNumber value="${chkstodemo.amount}" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:80px;">${chkstodemo.supply.unit}</span></td>
								<td <c:if test="${isNotShowSp_price == 'Y' }">style="display:none;"</c:if>><span style="width:80px;text-align:right;"><fmt:formatNumber value="${chkstodemo.price}" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:120px;">${chkstodemo.memo}</span></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label">
				<span style="color:red;">
					(提醒:如果多个报货类别，不同类别请手动保存！如果一个报货类别，直接下一步即可。)
				</span>
			</div>
		</div>
	</form>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
	<script type="text/javascript">
	//ajax同步设置
	$.ajaxSetup({
		async: false
	});
	//工具栏
	$(document).ready(function(){
		parent.stopVisibility();
		//按钮快捷键
		focus() ;//页面获得焦点
		
		//屏蔽鼠标右键
	 	$(document).bind('keyup',function(e){
	 		//表格数量一变，校验一下，价格就接着变
	 		if($(e.srcElement).is("input")){//对表格内的输入框进行判读，延迟600毫秒
	 			var index=$(e.srcElement).closest('td').index();
	    		if(index=="5"){
		 			$(e.srcElement).unbind('blur').blur(function(e){
		 				validateByMincnt($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
		 			});
		    		validator($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
	    		}
	    	}
		});
	 	
	   $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
	   $('.grid').find('.table-body').find('tr').hover(
			function(){
				$(this).addClass('tr-over');
			},
			function(){
				$(this).removeClass('tr-over');
			}
		);
	    $('#save').click(function(){
	    	save();
	    })
		
		//自动实现滚动条
		setElementHeight('.grid',['.tool'],$(document.body),110);	//计算.grid的高度
		setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
		loadGrid();//  自动计算滚动条的js方法	
		editCells();
		if($(".table-body").find('tr').length > 0){
			$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),5);
		}
	});
	
	//动态下拉列表框
	function findByTitle(){
		$('#listForm').submit();
	}
	//确认修改
	function save(){
		var bool = false;
		var checkboxList = $('.grid').find('.table-body').find('tr');
		var data = {};
		var i = 0;
		data['category_Code'] = $('#typ_eas').val();
		checkboxList.each(function(){
			//1.如果数量是0的过滤掉
			var amount = $(this).find('td:eq(5)').text()?$(this).find('td:eq(5)').text():$(this).find('td:eq(5) input').val();
			if(Number(amount) == 0  || amount == null || amount == ''){
				return true;
			}
			data['declareList['+i+'].sp_code'] = $(this).find('td:eq(1) span').text();
			data['declareList['+i+'].spuse'] = amount;
			data['declareList['+i+'].amountdis'] = amount;
			data['declareList['+i+'].memo'] = $(this).find('td:eq(8)').text()?$(this).find('td:eq(8)').text():$(this).find('td:eq(8) input').val();
			i++;
		});
		if(i == 0){
			bool = true;
			return bool;
		}
		parent.showVisibility();
		$.ajax({
			url:'<%=path%>/chkstomForecast/saveChkstoDemo.do',
			data:data,
			type:'post',
			success:function(msg){
				showMessage({
					type: 'success',
					msg: '报货类别:'+$('#typ_eas').find('option:checked').text()+'保存成功！',
					speed: 2000
				});
				parent.stopVisibility();
				bool = true;
			}
		});
		return bool;
	}
	//编辑表格
	function editCells(){
		if($(".table-body").find('tr').length == 0){
			return;
		}
		var colstyle = ['','','','',{background:"#F1F1F1"},'','',{background:"#F1F1F1"}];
		if('${isNotShowSp_price}' == 'Y'){
			colstyle = ['','','','',{background:"#F1F1F1"},'',{display:"none"},{background:"#F1F1F1"}];
		}
		$(".table-body").autoGrid({
			initRow:1,
			colPerRow:8,
			widths:[26,80,120,80,80,80,80,120],
			colStyle:colstyle,
			VerifyEdit:{verify:true,enable:function(cell,row){
				return row.find('td').index(cell) == 2 || (row.find("td:eq(2)").text() == null || $.trim(row.find("td:eq(2)").text()) != '' ) ? true : false;
			}},
			onEdit:$.noop,
			editable:[5,8],//能输入的位置
			onLastClick:$.noop,
			onEnter:function(data){
				$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.value) ;
			},
			cellAction:[{
				index:5,
				action:function(row,data2){
					if(Number(data2.value) < 0){
						alert('<fmt:message key="number_cannot_be_negative"/>！');
						row.find("td:eq(5)").find('span').text(data2.ovalue);
						$.fn.autoGrid.setCellEditable(row,5);
					}else if(isNaN(data2.value)){
						alert('<fmt:message key="number_be_not_number"/>！');
						row.find("td:eq(5)").find('span').text(data2.ovalue);
						$.fn.autoGrid.setCellEditable(row,5);
					}else if(Number(data2.value) != 0 && Number(row.data("stomin"))!=0 && (Number(data2.value) < Number(row.data('stomin')))){
						alert('<fmt:message key="the_minimum_purchase_quantity_of_material"/>'+row.data("stomin")+'.');
						row.find("td:eq(5)").find('span').text(data2.ovalue);
						$.fn.autoGrid.setCellEditable(row,5);
					}else if(Number(data2.value) != 0 && Number(row.data("stocnt"))!=0 && !(/(^[1-9]\d*$)/.test(Number(data2.value)/Number(row.data("stocnt"))))){
						alert('申购量必须是申购倍数'+row.data("stocnt")+'的整数倍！');
						row.find("td:eq(5)").find('span').text(data2.ovalue);
						$.fn.autoGrid.setCellEditable(row,5);
					}else if(Number(data2.value) != 0 && Number(row.data("stocnt"))==0 && Number(row.data("mincnt"))!=0 && !(/(^[1-9]\d*$)/.test(Number(data2.value)/Number(row.data("mincnt"))))){
						alert('申购量必须是申购倍数'+row.data("mincnt")+'的整数倍！');
						row.find("td:eq(5)").find('span').text(data2.ovalue);
						$.fn.autoGrid.setCellEditable(row,5);
					} else {
						$.fn.autoGrid.setCellEditable(row.next(),5);
					}
				}
			}]
		});
	}
	
	function validateByMincnt(index,row,data2){//最小申购量判断
		if(index=="5"){
			if(isNaN(data2.value)||Number(data2.value) < 0){
				alert('<fmt:message key="please_enter_positive_integer"/>！');
				row.find("td:eq(5)").find('span').text(data2.ovalue);
				$.fn.autoGrid.setCellEditable(row,5);
			}else if(Number(data2.value) != 0 && Number(row.data("stomin"))!=0 && (Number(data2.value) < Number(row.data('stomin')))){
				alert('<fmt:message key="the_minimum_purchase_quantity_of_material"/>'+row.data("stomin")+'.');
				row.find("td:eq(5)").find('span').text(data2.ovalue);
				$.fn.autoGrid.setCellEditable(row,5);
			}else if(Number(data2.value) != 0 && Number(row.data("stocnt"))!=0 && !(/(^[1-9]\d*$)/.test(Number(data2.value)/Number(row.data("stocnt"))))){
				alert('申购量必须是申购倍数'+row.data("stocnt")+'的整数倍！');
				row.find("td:eq(5)").find('span').text(data2.ovalue);
				$.fn.autoGrid.setCellEditable(row,5);
			}else if(Number(data2.value) != 0 && Number(row.data("stocnt"))==0 && Number(row.data("mincnt"))!=0 && !(/(^[1-9]\d*$)/.test(Number(data2.value)/Number(row.data("mincnt"))))){
				alert('申购量必须是申购倍数'+row.data("mincnt")+'的整数倍！');
				row.find("td:eq(5)").find('span').text(data2.ovalue);
				$.fn.autoGrid.setCellEditable(row,5);
			}else{
				row.find("td:eq(5)").find('span').text(Number(data2.value).toFixed(2));
			}
		}
	}
	function validator(index,row,data2){//输入框验证
		if(index=="5"){
			if(Number(data2.value) < 0){
				alert('<fmt:message key="number_cannot_be_negative"/>！');
				row.find("td:eq(5)").find('span').text(data2.ovalue);
				$.fn.autoGrid.setCellEditable(row,5);
			}else if(isNaN(data2.value)){
				alert('<fmt:message key="number_be_not_number"/>！');
				row.find("td:eq(5)").find('span').text(data2.ovalue);
				$.fn.autoGrid.setCellEditable(row,5);
			}else if(data2.value.length > 6){
				alert('<fmt:message key="the_maximum_length_not"/>6<fmt:message key="digit"/>！');
				row.find("td:eq(5)").find('span').text(data2.ovalue);
				$.fn.autoGrid.setCellEditable(row,5);
			}else{
				row.find("input").data("ovalue",data2.value);
			}
		}
	}
	</script>
</body>
</html>