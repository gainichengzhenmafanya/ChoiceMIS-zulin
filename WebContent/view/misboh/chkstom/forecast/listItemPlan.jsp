<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="reported_acceptance"/>--菜品销售计划</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>	
			<style type="text/css">	
			.page{margin-bottom: 25px;}		
			.onEdit{
				border:1px solid;
				border-bottom-color: blue;
				border-top-color: blue;
				border-left-color: blue;
				border-right-color: blue;
			}
			.input{
				background:transparent;
				border:0px solid;
			}
			a.l-btn-plain{
				border:1px solid #7eabcd; 
				height:22px;
			}
			.search{
				margin-top:-2px;
				cursor: pointer;
			}
			.textInput span {
				padding:0px;
			}
			.textInput input {
				border:0px;
				width:60px;
			}
			</style>
	</head>
	<body> 
		<div class="tool"></div>
		<%--当前登录用户 --%>	
		<form id="listForm" action="<%=path%>/forecastMis/planList.do" method="post">	
		<input type="hidden" id="msg" value="${msg }"/>
		<input type="hidden" id="listStrSize" name="listStrSize" value="${listStrSize}"/>
		<input type="hidden" id="firm" name="firm" value="${itemPlan.firm}"/>
		<input type="hidden" id="dept1" name="dept1" value="${itemPlan.dept1}"/>
			<div class="form-line">	
				<div class="form-label"><fmt:message key="scm_estimated_date"/><fmt:message key="date"/>:</div>
				<div class="form-input" style="width:190px;">
					<input type="text" style="width:90px;margin-top: -3px;" id="startdate" name="startdate" value="<fmt:formatDate value="${itemPlan.startdate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" readonly="readonly"/>
					<font style="color:blue;"><fmt:message key="to"/></font>
					<input type="text" style="width:90px;margin-top: -3px;" id="enddate" name="enddate" value="<fmt:formatDate value="${itemPlan.enddate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" readonly="readonly"/>
				</div>
				<div class="form-label" style="width:80px;"></div>
				<div class="form-input">
					<input type="button" style="width:80px" id="calculate" name="calculate" value="重新计算"/>
				</div>
			</div>	
		   	<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num"><span style="width:30px;">&nbsp;</span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="sector"/></span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="coding"/></span></td>
								<td rowspan="2"><span style="width:100px;"><fmt:message key="name"/></span></td>
								<td rowspan="2"><span style="width:25px;"><fmt:message key="unit"/></span></td>
								<c:forEach var="dat" items="${dateList}" varStatus="status">
									<td colspan="2"><span>${dat}</span></td>
								</c:forEach>
							</tr>
							<tr>
								<c:forEach var="dat" items="${dateList}" varStatus="status">
									<td><span style="width:60px;"><fmt:message key="misboh_forecastreference"/></span></td>
	 								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td>
								</c:forEach>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>	
							<c:forEach var="posItemPlan" items="${posItemPlanList}" varStatus="status">
								<tr>
									<td class="num"><span style="width:30px;">${status.index+1}</span></td>
									<td>
										<span title="${posItemPlan.dept}" style="width:80px;">${posItemPlan.deptdes}
										<input type="hidden" name="show_id" value="${posItemPlan.item}"/>
									</span>
									</td>
									<td><span title="${posItemPlan.itcode}" style="width:80px;">${posItemPlan.itcode}</span></td>
									<td><span title="${posItemPlan.itdes}" style="width:100px;">${posItemPlan.itdes}</span></td>
									<td><span title="${posItemPlan.itunit}" style="width:25px;">${posItemPlan.itunit}</span></td>
									<c:forEach var="itemplan" items="${posItemPlan.itemplanlist}" varStatus="status">
										<td><span title="${itemplan['dat']}" style="width:60px;text-align: right;">${itemplan['cal']}</span></td>
										<td class="textInput"><span title="${itemplan['upd']}" style="width:60px;">
											<input type="text" value="${itemplan['upd']}" style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
									</c:forEach>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){	
			parent.stopVisibility();
			focus() ;//页面获得焦点
// 			loadToolBarDeclare();
			if (""!=$("#msg").val()) {
				alert($("#msg").val());
			}
		 	$(document).bind('keydown',function(e){//按钮快捷键
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
			});

			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
		 	//控制按钮显示
			function loadToolBarDeclare(){
				$('.tool').html('');
				$('.tool').toolbar({
					items: [{
						text: '<fmt:message key="calculate" />',
						title: '<fmt:message key="calculate" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							if (confirm('确认重新计算菜品销售计划吗？')) {
								var action = "<%=path%>/chkstomForecast/calItemPlan.do";
								$('#listForm').attr('action',action);
								$('#listForm').submit(); 
								parent.showVisibility();
							}
						}
					} ]
				});
		 	}
			//自动实现滚动条 				
			setElementHeight('.grid',['.tool'],$(document.body),90);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();	
			
			$('#calculate').click(function(){
				if (confirm('确认重新计算菜品销售计划吗？')) {
					var action = "<%=path%>/chkstomForecast/calItemPlan.do";
					$('#listForm').attr('action',action);
					$('#listForm').submit(); 
					parent.showVisibility();
				}
			});
		});	
		
		//报货向导用
		function save(){
			if(saveUpdate()){
				var data = {};
				if(confirm('当前报货方式:菜品点击率，预估的报货单最终调整量是否考虑安全库存、当前库存、在途数量？\n点击确定，则最终调整量=周期用量+安全库存-当前库存-在途数量，并且向上取整\n点击取消，则最终调整量=周期用量，并且向上取整')){
					data['xsjhCalWay'] = 'Y';
				} else {
					data['xsjhCalWay'] = 'N';
				}
				var bool = false;
				$.ajax({
					url:'<%=path%>/chkstomForecast/saveXsjhCalWay.do',
					data:data,
					type:'post',
					async:false,
					success:function(msg){
						bool = true;
					}
				});
				return bool;
			} else {
				return false;
			}
		}
		
		//保存登记
		function saveUpdate(){
			var len = $('#listStrSize').val();
			var selected = {};
			var checkList = $("input[name='show_id']");
			if(checkList && checkList.size() > 0){
				var i = 0;
				checkList.each(function(){
					for(var j=0;j < len ;j++){
						selected['listItemPlan['+i+'].item'] = $(this).val();
						selected['listItemPlan['+i+'].dat'] = $(this).parents('tr').find('td:eq('+ (2*j+5) +')').find('span').attr('title');
						var updtotal = $(this).parents('tr').find('td:eq('+ (2*j+6) +')').find('input').val();
						if(updtotal == null || updtotal == ''){
							updtotal = 0;
						}
						selected['listItemPlan['+i+'].updtotal'] = Number(updtotal);
						i++;
					}
				});
				$('#wait').show();
				$('#wait2').show();
				$.ajax({
					url:'<%=path%>/forecastMis/updatePlan.do',
					type:"post",
					data:selected,
					async: false,
					success:function(data){
						$('#wait').hide();
						$('#wait2').hide();
						flag = true;
						showMessage({
							type: 'success',
							msg: '<fmt:message key="save_successful"/>！',
							speed: 1000
						});
					}
				});
				return flag;
			}else{
				alert('<fmt:message key="please_select_options_you_need_save" />！');
				return false;
			}
		}
		
		// 检查最高最低库存是否有效数字
		function checkNum(inputObj){
			if(isNaN(inputObj.value)){
				alert("无效数字！");
				inputObj.focus();
				return false;
			}
		}
		function DateDiff(sDate1, sDate2){ 
		    var aDate, oDate1, oDate2, iDays;
		    aDate = sDate1.split("-");
		    oDate1 = new Date(aDate[1] + '/' + aDate[2] + '/' + aDate[0]); //转换为12-18-2002格式
		    aDate = sDate2.split("-");
		    oDate2 = new Date(aDate[1] + '/' + aDate[2] + '/' + aDate[0]);
		    iDays = parseInt((oDate1 - oDate2) / 1000 / 60 / 60 /24); //把相差的毫秒数转换为天数
		    return iDays;
		}
		
		</script>
	</body>
</html>