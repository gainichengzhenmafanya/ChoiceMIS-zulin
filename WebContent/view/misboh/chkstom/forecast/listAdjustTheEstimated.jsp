<%@page import="com.choice.misboh.commonutil.DateJudge"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		$(document).ready(function(){
			parent.stopVisibility();
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),85);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
		});
		
		function save(){
// 			if(!checkSalePlan()){
// 				return false;
// 			}
			var param = {};
			var bool = false;
			$('.table-body').find('tr').each(function(i){
				param['salePlanList['+i+'].dat'] = $(this).data('dat');
				param['salePlanList['+i+'].week'] = $(this).data('week');
				param['salePlanList['+i+'].people_t'] = $(this).data('people_t');
				param['salePlanList['+i+'].money_t'] = $(this).data('money_t');
				param['salePlanList['+i+'].bills_t'] = $(this).data('bills_t');
				param['salePlanList['+i+'].money'] = $(this).find('td:eq(3)').find('input').val();
			});
			$.ajax({
				url:'<%=path%>/chkstomForecast/saveSalePlan.do',
				data:param,
				type:'post',
				success:function(msg){
					parent.declareGoods = msg;//菜品销售计划用
					bool = true;
				}
			});
			return bool;
		}
		
		//回车下一个
		function eqEnter(obj){
			if(event.keyCode == 13){
				if($(obj).parents('tr').next('tr').length != 0)
					$(obj).parents('tr').next('tr').find('input').focus();
				else
					$('.table-body').find('tr:first').find('input').focus();
			}
		}
		
		</script>
		
	</head>
	<body>
		<form id="mainForm" method="post" action="">
			<input type="hidden" id="vplantyp" value="${cf.vplantyp}" />
			<div class="bj_head">
				<div class="form-line">
					<div class="form-input">
						根据总部配置，当前预估方式：
						<span style="color:red">
						<c:if test="${cf.vplantyp=='PAX'}">
							客流
						</c:if>
						<c:if test="${cf.vplantyp=='AMT'}">
							营业额
						</c:if>
						<c:if test="${cf.vplantyp=='TC'}">
							单次
						</c:if>
						</span>
					</div>
				</div>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:100px;"><fmt:message key="business_day"/></span></td>
								<td><span style="width:50px;"><fmt:message key="week"/></span></td>
								<td><span style="width:100px;"><fmt:message key="misboh_DeclareGoodsGuide_yg1" /></span></td>
								<td><span style="width:100px;"><fmt:message key="misboh_DeclareGoodsGuide_yg2" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="salePlan" items="${cf.salePlanList}" varStatus="status">
							<c:set var="value" value="${0 }"></c:set>
							<c:if test="${cf.vplantyp=='PAX'}">
								<c:set var="value" value="${salePlan.people_t }"></c:set>
							</c:if>
							<c:if test="${cf.vplantyp=='AMT'}">
								<c:set var="value" value="${salePlan.money_t }"></c:set>
							</c:if>
							<c:if test="${cf.vplantyp=='TC'}">
								<c:set var="value" value="${salePlan.bills_t }"></c:set>
							</c:if>
							<c:set var="value1" value="${value }"></c:set>
							<c:if test="${salePlan.money != 0}"><!-- 上一步记录原来的值 -->
								<c:set var="value1" value="${salePlan.money }"></c:set>
							</c:if>
							
							<tr data-dat="<fmt:formatDate value="${salePlan.dat }" pattern="yyyy-MM-dd"/>" data-week="${salePlan.week }"
								data-people_t="${salePlan.people_t }" data-money_t="${salePlan.money_t }" data-bills_t="${salePlan.bills_t }">
								<td><span style="width:100px;"><fmt:formatDate value="${salePlan.dat }" pattern="yyyy-MM-dd"/></span></td>
								<td><span style="width:50px;">${salePlan.week }</span></td>
								<td><span style="width:100px;text-align: right;"><fmt:formatNumber value="${value}" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:100px;">
									<input style="width:100px;text-align:right;" pattern="[0-9]"  onkeyup="eqEnter(this)" type="text" value="<fmt:formatNumber value="${value1}" type="currency" pattern="0.00"/>"/>
								</span></td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
	</body>
</html>