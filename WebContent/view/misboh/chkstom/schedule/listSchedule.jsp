<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><fmt:message key="The_shipping_schedule"/></title>
		<link rel="stylesheet" type="text/css" href="<%=path%>/css/Calendar.css" />
		<link rel="stylesheet" type="text/css" href="<%=path%>/css/popupUtil.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			setElementHeight('#body',['.tool','.bj_head'],$(document.body),60);	//计算.grid的高度
			var tool = $('.tool').toolbar({
				items: [{
// 						text: '<fmt:message key="select" />',
// 						title: '<fmt:message key="select"/>',
// 						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
// 						icon: {
<%-- 							url: '<%=path%>/image/Button/op_owner.gif', --%>
// 							position: ['0px','-40px']
// 						},
// 						handler: function(){
// 							$('#listForm').submit();
// 						}
// 					},{
						text: '<fmt:message key="scm_pre_month"/>',
						title: '<fmt:message key="scm_pre_month"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','0px']
						},
						handler: function(){
							goUp();
						}
					},{
						text: '<fmt:message key="scm_next_motn"/>',
						title: '<fmt:message key="scm_next_motn"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','0px']
						},
						handler: function(){
							goDown();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							if($("#sta").val()!=1){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}else{
								parent.$('.close').click();
							}
						}
					}
				]
			});
			createCalendar();
		});
		var date;
		var month='<c:out value="${month }"/>';
		if (month==''||month==null) {
			date=new Date();
		}else{
			date=new Date(month.substring(0,4),month.split("-")[1]-1,'01');
		}
		
		function pageReload(){ 
			$('#listForm').submit();
		}
		function createCalendar(){ 
			clearTable();
			//var date = new Date(year,month,day);
			//判断1号星期几 
		 	var weekIndex = new Date(date.getFullYear(),date.getMonth(),1).getDay();//星期
			//获取该月天数
			var maxDay = GetMonthDayCount(date);
			var tb = document.getElementById("tb_calendar");
			var rowIndex = 1;
			var colIndex = 0;
			for(var i=0;i<weekIndex;i++){
				tb.rows[rowIndex].cells[colIndex].innerText = " ";
				colIndex++;
			} 
			
			for(var m=0;m<maxDay;m++){
				var omonth = date.getMonth()+1;
				var oday = m+1;
				if (omonth < 10){
					omonth = "0" + omonth;
				}
				if (oday < 10){
					oday = "0" + oday;
				}
				var odate = date.getFullYear() + "" + omonth + "" + oday;
				var txxodate = date.getFullYear() + "-" + omonth + "-" + oday;
				tb.rows[rowIndex].cells[colIndex].innerHTML = "<div id='div_"+oday+"' style='height: 80px;width:98%' >"
							+"<div style='height: 20px;'>"
							+"	<div style='float: left;'><span>"+oday+"</span></div>"
							+"</div>"
							+"<div style='width:100%;height:60px;overflow-y: auto;text-align:center;' >"
							+"	<table style='width:100%;height:auto;' id='table_"+oday+"'>"
							+"	</table>"
							+"</div>"
						+"</div>";
				colIndex++;
				if(colIndex == 7){
					rowIndex++;
					colIndex = 0;
				}
			}

			var scheduleMonth = date.getFullYear()+"-"+(date.getMonth()+1);
			refresh(scheduleMonth);
			if(date.getMonth()<9){
				document.getElementById("divDate").innerText=date.getFullYear()+"-0"+(date.getMonth()+1);
				$('#month').val(document.getElementById("divDate").innerText);
			}else{
				document.getElementById("divDate").innerText=date.getFullYear()+"-"+(date.getMonth()+1);
				$('#month').val(document.getElementById("divDate").innerText);
			}
		}
		
		function clearTable(){
			var tb = document.getElementById("tb_calendar");
			for(var m=1;m<tb.rows.length;m++){ 
				for(var i=0;i<tb.rows[0].cells.length;i++){
					tb.rows[m].cells[i].innerHTML="";
				}
			}
		}
		
		//上一月
		function goUp(){
			date.setMonth(date.getMonth()-1);  
			createCalendar();
		}
		//下一月
		function goDown(){
			date.setMonth(date.getMonth()+1);
			createCalendar();
		}
		function GetMonthDayCount(newDate){  
			switch(newDate.getMonth()+1)  
			{  
				case   1:case   3:case   5:case   7:case   8:case   10:case   12:  
				return   31;  
				case   4:case   6:case   9:case   11:  
				return   30;  
			}  
			//feb:  
			newDate = new Date(newDate);
			var   lastd=28;  
			newDate.setDate(29);  
			while(newDate.getMonth()==1)  
			{  
				lastd++;  
				newDate.setDate(newDate.getDate()+1);   
			}  
			return   lastd;  
		}
		function refresh(scheduleMonth) {
			if (scheduleMonth != null || typeof(scheduleMonth) !="undefined"){
				var oday = "";
				if (scheduleMonth.split("-")[1] < 10){
					oday = "0" + scheduleMonth.split("-")[1];
				}else{
					oday=scheduleMonth.split("-")[1];
				}
				scheduleMonth = scheduleMonth.split("-")[0]+"-"+oday;
			} else {
				scheduleMonth=document.getElementById("divDate").innerText;
				
				if(scheduleMonth =='' || scheduleMonth==null){
					var date1 = new Date();
					scheduleMonth = date1.getFullYear()+"-"+(date1.getMonth()+1);
				}
				
				var maxDay = GetMonthDayCount(new Date(scheduleMonth.split("-")[0],scheduleMonth.split("-")[1]-1,1));
				for(var m=1;m<=maxDay;m++){ 
					var oday = "";
					if (m < 10){
						oday = "0" + m;
					}else{
						oday=m;
					}
					var tmptable = document.getElementById("table_"+oday);
					for (var trow=tmptable.rows.length - 1;trow>=0;trow--){
						tmptable.deleteRow(trow);
					}
				}
				var omonth = "";
				if (scheduleMonth.split("-")[1] < 10){
					omonth = scheduleMonth.split("-")[1];
				}else{
					omonth=scheduleMonth.split("-")[1];
				}
				scheduleMonth = scheduleMonth.split("-")[0]+"-"+omonth;
			}
			//遍历list 
			$.ajax({
		        url:"<%=path%>/scheduleMis/findScheduleByMonth.do",
		        cache:false,
				type:'post',
				async:false,
				data:'scheduleMonth='+scheduleMonth+"&positnCode="+document.getElementById("positnCode").value+"&category_Code="+document.getElementById("code").value,
				dataType:'json',
				ifModified:true,
				timeout: 30000,
				error: function(XMLHttpRequest, textStatus, errorThrown){
					alert('Error loading json document:'+XMLHttpRequest.readyState);
				},
				success: function(msg){	
					var scheduleMonth = date.getFullYear()+"-"+(date.getMonth()+1);
					var omonth = "";
					if (scheduleMonth.split("-")[1] < 10){
						omonth = "0" + scheduleMonth.split("-")[1];
					}else{
						omonth=scheduleMonth.split("-")[1];
					}
					scheduleMonth = scheduleMonth.split("-")[0]+"-"+omonth;
					if(msg.data!=null && msg.data.length>0){
						for (var i = 0;i<msg.data.length;i++){
							if (msg.data[i].orderDate.substring(0,7)==scheduleMonth){
								var table = document.getElementById("table_"+msg.data[i].orderDate.substring(8));
								var newRow = table.insertRow(-1);
							 	newCell = newRow.insertCell(0);
							 	newCell.noWrap = "nowrap";
							 	newCell.align = "left";
							 	newCell.style.cursor = "hand";
							 	newCell.style.background = msg.data[i].orderColor;
							 	newCell.title = "<fmt:message key='order_goods'/><fmt:message key='date'/>:"+msg.data[i].orderDate+"<fmt:message key='arrival_date'/>:"+msg.data[i].receiveDate;
							 	newCell.innerHTML = "<span style='color: black'><fmt:message key='order_goods'/>："+msg.data[i].category_Code+"&nbsp;&nbsp;&nbsp;&nbsp;"+msg.data[i].orderTime+"</span>"; 
							}
						}
					}
					if(msg.data2 != null && msg.data2.length>0){
						for (var i = 0;i<msg.data2.length;i++){
							if (msg.data2[i].receiveDate.substring(0,7)==scheduleMonth){
								var table = document.getElementById("table_"+msg.data2[i].receiveDate.substring(8));
								var newRow = table.insertRow(-1);
							 	newCell = newRow.insertCell(0);
							 	newCell.noWrap = "nowrap";
							 	newCell.align = "left";
							 	newCell.style.cursor = "hand";
							 	newCell.style.background = msg.data2[i].receiveColor;
							 	newCell.title = "<fmt:message key='order_goods'/><fmt:message key='date'/>:"+msg.data2[i].orderDate+"<fmt:message key='arrival_date'/>:"+msg.data2[i].receiveDate;
							 	newCell.innerHTML = "<span style='color: black'><fmt:message key='receive_goods'/>："+msg.data2[i].category_Code+"&nbsp;&nbsp;&nbsp;&nbsp;"+msg.data2[i].receiveTime+"</span>"; 
							}
						}
					}
				}
			});
		}
	</script>
	</head>
	<body>
		<div class="tool"></div>
		<form id="listForm" action="<%=path%>/scheduleMis/list.do" method="post">
		<input type="hidden" name="positnCode" id="positnCode" value="${scheduleD.positnCode}" />
		<input type="hidden" id="month" name="month" value="${month}"/>
		<input type="hidden" id="sta" name="sta" value="${sta}"/>
		<div class="bj_head">
			<div class="form-line">
			<div class="form-label"></div>
			<div class="form-input" id="divDate">
			</div>
			<div class="form-input" style="float:right;width:220px;">
				<select name="category_Code" onchange="refresh()" style="width: 200px" id="code">
					<option value="-1"><fmt:message key="all" /></option>
					<c:forEach items="${grptypList}" var="grptyp">
						<option
							<c:if test="${category_Code == grptyp.code}"> selected="selected" </c:if>
							value="${grptyp.code }">
							${grptyp.des }
						</option>
					</c:forEach>
				</select>
			</div>
			<div class="form-label" style="float:right;"><fmt:message key="supplyclassification"/>:</div>
			</div>
		</div>
			<div id="body" style="overflow-y:auto;">
				<table width="100%" cellspacing="0" cellpadding="0"
					class="monthGrid" id="tb_calendar" onclick="">
					<thead>
						<tr class="weekdayRow">
							<td class="weekday firstCol" style="width: 14.28%;">
								<fmt:message key="week7" />
							</td>
							<td class="weekday" style="width: 14.28%;">
								<fmt:message key="week1" />
							</td>
							<td class="weekday" style="width: 14.28%;">
								<fmt:message key="week2" />
							</td>
							<td class="weekday" style="width: 14.28%;">
								<fmt:message key="week3" />
							</td>
							<td class="weekday" style="width: 14.28%;">
								<fmt:message key="week4" />
							</td>
							<td class="weekday" style="width: 14.28%;">
								<fmt:message key="week5" />
							</td>
							<td class="weekday" style="width: 14.28%;">
								<fmt:message key="week6" />
							</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="contentdayweek firstCol"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentdayweek"></td>
						</tr>
						<tr>
							<td class="contentdayweek firstCol"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentdayweek"></td>
						</tr>
						<tr>
							<td class="contentdayweek firstCol"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentdayweek"></td>
						</tr>
						<tr>
							<td class="contentdayweek firstCol"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentdayweek"></td>
						</tr>
						<tr>
							<td class="contentdayweek firstCol"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentdayweek"></td>
						</tr>
						<tr>
							<td class="contentdayweek firstCol"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentdayweek"></td>
						</tr>
					</tbody>
				</table>
			</div>
		</form>
	</body>
</html>
