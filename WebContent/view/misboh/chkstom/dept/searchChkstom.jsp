<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="query_messages_manifest"/>_查询未上传</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">		
		.page{
			margin-bottom: 25px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label:first-child{
			margin-left: 0px;
			width: 40px;
		}
		form .form-line .form-label{
			width: 60px;
			margin-left: 20px;
		}
		form .form-line .form-input{
			width: 100px;
			display: inline-block;
		}
		form .form-line .form-input , form .form-line .form-input input[type=text]{
			width: 100px;
		}
		</style>
	</head>
	<body>
		<div class="tool"></div>	
			<form id="SearchForm" action="<%=path%>/chkstomDeptMis/searchByKey.do" method="post">
				<input type="hidden" id="upd" name="upd" value="${upd}"/>
				<input type="hidden" id="firm" name="firm" value="${chkstom.firm}"/>
				<div class="form-line">
					<div class="form-label" style="width:60px;"><fmt:message key="startdate"/></div>
					<div class="form-input"><input autocomplete="off" style="width:85px;" type="text" id="bdate" name="bMaded" class="Wdate text" value="<fmt:formatDate value="${chkstom.bMaded}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edate\')}'})"/></div>			
					<div class="form-label"><fmt:message key="orders_num"/></div>
					<div class="form-input"><input type="text" id="chkstoNo" name="chkstoNo" class="text" value="${chkstom.chkstoNo}" onkeyup="this.value=this.value.replace(/\D/g,'')"/></div>
					<div class="form-label"><fmt:message key="supplies_code"/></div>
					<div class="form-input"><input type="text" id="sp_code" name="sp_code" class="text" value="${sp_code }" style="width:198px"/>
					<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' /></div>
				</div>
				<div class="form-line">
					<div class="form-label" style="width:60px;"><fmt:message key="enddate"/></div>
					<div class="form-input"><input autocomplete="off" style="width:85px;" type="text" id="edate" name="eMaded" class="Wdate text" value="<fmt:formatDate value="${chkstom.eMaded}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdate\')}'})"/></div>
					<div class="form-label"><fmt:message key="orders_maker"/></div>
					<div class="form-input"><input type="text" id="madeby" name="madeby" class="text" value="${chkstom.madeby }"/></div>
					<div class="form-label"><fmt:message key="Report_goods_department"/></div>
					<div class="form-input">
						<select class="select" id="dept" name="dept" style="width:200px">
							<option value=""></option>							
							<c:forEach var="positn" items="${positnList}" varStatus="status">
								<option
								<c:if test="${positn.code==chkstom.dept}"> 
									   	selected="selected"
								</c:if>
								value="${positn.code}" title="${positn.des}">${positn.code}-${positn.init}-${positn.des}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width:25px;">&nbsp;</span></td>
								<td style="width:30px;"><input type="checkbox" id="chkAll"/></td>
								<td style="width:80px;"><fmt:message key="reported_num"/></td>
								<td style="width:100px;"><fmt:message key="date"/></td>
								<td style="width:130px;"><fmt:message key="time"/></td>
								<td style="width:80px;"><fmt:message key="department_code"/></td>
								<td style="width:100px;"><fmt:message key="department_name"/></td>
								<td style="width:80px;<c:if test="${isNotShowSp_price == 'Y' }">display:none;</c:if>"><fmt:message key="total_amount1"/></td>
								<td style="width:100px;"><fmt:message key="orders_maker"/></td>
<!-- 								<td style="width:80px;">是否上传</td> -->
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="chkstom" items="${chkstomList }" varStatus="status">
								<tr>
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td><span style="width:20px; text-align: center;"><input type="checkbox" name="idList" id="chk_${chkstom.chkstoNo }" value="${chkstom.chkstoNo }"/></span></td>									
									<td><span style="width:70px;" title="${chkstom.chkstoNo }">${chkstom.chkstoNo }</span></td>
									<td><span style="width:90px;" title="<fmt:formatDate value="${chkstom.maded}" pattern="yyyy-MM-dd" type="date"/>"><fmt:formatDate value="${chkstom.maded}" pattern="yyyy-MM-dd" type="date"/></span></td>
									<td><span style="width:120px;" title="${chkstom.madet}">${chkstom.madet}</span></td>
									<td><span style="width:70px;" title="${chkstom.dept}">${chkstom.dept }</span></td>
									<td><span style="width:90px;" title="${chkstom.positn.des}">${chkstom.positn.des }</span></td>
									<td <c:if test="${isNotShowSp_price == 'Y' }">style="display:none;"</c:if>><span style="width:70px;text-align:right;" title="${chkstom.totalAmt }"><fmt:formatNumber value="${chkstom.totalAmt }" type="currency" pattern="0.00"/></span></td>
									<td><span style="width:90px;" title="${chkstom.madebyName}">${chkstom.madebyName }</span></td>
<%-- 									<td><span style="width:70px;" title="${chkstom.bak1}">${chkstom.bak1 }</span></td> --%>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>				
			</div>
			<page:page form="SearchForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		var validate;
		$(document).ready(function(){
			//双击事件
			$('.grid .table-body tr').live('dblclick',function(){
				var status = $(window.parent.document).find("#sta").val();
				if(status == 'add' || status == 'edit'){
					if(!confirm('<fmt:message key="reported_manifest_unsaved_remind"/>')){
						return;
					}
				}
				var chkstoNo = $(this).find("input:checkbox").val();
				chkstoNo ? window.parent.openChkstom(chkstoNo) && window.close() : alert("error!");
			});
			//按钮快捷键
			focus() ;//页面获得焦点
		 	$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			parent.$('.close').click();
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode){
	                case 70: parent.$('.<fmt:message key="query_messages_manifest"/>').click(); break;
	                case 67: parent.$('.<fmt:message key="check_messages_manifest"/>').click(); break;
	                case 68: parent.$('.<fmt:message key="delete_messages_manifest"/>').click(); break;
	            }
			}); 
		 	//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     }else{
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			     }
			});
		 	/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'chkstoNo',
					validateType:['canNull','intege'],
					param:['T','T'],
					error:['','<fmt:message key="single_number_is_digital_or_empty"/>！']
				}]
			});
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		    $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),77);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法		
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/misbohcommon/selectSupplyLeft.do?positn=1&defaultCode='+defaultCode,$('#sp_code'));
				}
			});
		});
		var tool = $('.tool').toolbar({
			items: [{
				text: '<fmt:message key="select" />',
				title: '<fmt:message key="query_messages_manifest"/>',
				useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
				icon: {
					url: '<%=path%>/image/Button/op_owner.gif',
					position: ['0px','-40px']
				},
				handler: function(){
					if(validate._submitValidate()){
						$("#SearchForm").submit();
					}
				}
			},{
				text: '<fmt:message key="enter" />',
				title: '<fmt:message key="enter"/>',
				useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
				icon: {
					url: '<%=path%>/image/Button/op_owner.gif',
					position: ['-38px','0px']
				},
				handler: function(){
					enterChkstom();
				}
			},{
				text: '<fmt:message key="delete" />',
				title: '<fmt:message key="delete_messages_manifest"/>',
				useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
				icon: {
					url: '<%=path%>/image/Button/op_owner.gif',
					position: ['-38px','0px']
				},
				handler: function(){
					deleteChkstom();
				}
			},{
				text: '<fmt:message key="quit"/>',
				title: '<fmt:message key="quit"/>',
				icon: {
					url: '<%=path%>/image/Button/op_owner.gif',
					position: ['-160px','-100px']
				},
				handler: function(){
					parent.$('.close').click();
				}
			}]
		});
		
		//进入报货单
		function enterChkstom(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() == 1){
					var status = $(window.parent.document).find("#sta").val();
					if(status == 'add' || status == 'edit'){
						if(!confirm('<fmt:message key="reported_manifest_unsaved_remind"/>')){
							return;
						}
					}
					var chkstoNo1 = checkboxList.filter(':checked').val();
					chkstoNo1 ? window.parent.openChkstom(chkstoNo1) && window.close() : alert("error!");
			}else{
				alert('<fmt:message key="please_select_single_message"/>！');
				return ;
			}
		}

		//删除报货单主表
		function deleteChkstom(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() > 0){
				if(confirm('<fmt:message key="delete_data_confirm"/>？')){
					var chkValue = [];
					checkboxList.filter(':checked').each(function(){
						chkValue.push($(this).val());
					});
					$.post("<%=path%>/chkstomDeptMis/deleteChkstom.do?chkstoNoIds="+chkValue.join(","),function(data){
						var rs = eval('('+data+')');
						switch(Number(rs.pr)){
						case 0:
							alert('<fmt:message key="orders_unsaved_cannot_audited"/>！');
							break;
						case 1:
							alert('<fmt:message key="successful_deleted"/>！');
							$('#SearchForm').submit();
							break;
						}
					});
				}
			}else{
				alert('<fmt:message key="please_select_information_you_need_to_delete"/>！');
				return ;
			}
		}			
		function pageReload(){
	    	$('#SearchForm').submit();
		}
		</script>
	</body>
</html>