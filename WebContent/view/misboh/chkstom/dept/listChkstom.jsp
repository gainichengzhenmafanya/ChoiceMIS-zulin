<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>查询档口报货单上传</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<style type="text/css">
			.page{margin-bottom: 25px;}
			.bgBlue{
				background: #D2E9FF;
			}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<form id="listForm" action="<%=path%>/chkstomDeptMis/listCheckedChkstom.do" method="post">
				<input type="hidden" id="firm" name="firm" value="${chkstom.firm}"/>
				<div class="form-line">
					<div class="form-label" style="width:60px;"><fmt:message key="startdate"/></div>
					<div class="form-input"><input autocomplete="off" type="text" id="bdate" name="bMaded" class="Wdate text" value="<fmt:formatDate value="${chkstom.bMaded}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edate\')}'})"/></div>
					<div class="form-label" style="width:60px;"><fmt:message key="enddate"/></div>
					<div class="form-input"><input autocomplete="off" type="text" id="edate" name="eMaded" class="Wdate text" value="<fmt:formatDate value="${chkstom.eMaded}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdate\')}'})"/></div>
					<div class="form-label"><fmt:message key="orders_num"/></div>
					<div class="form-input"><input type="text" id="chkstoNo" name="chkstoNo" class="text" value="${chkstom.chkstoNo}" onkeyup="this.value=this.value.replace(/\D/g,'')"/></div>
					<div class="form-label">[
						<input type="radio" name="bak1" value="Y" <c:if test="${chkstom.bak1=='Y' }"> checked="checked"</c:if>/><fmt:message key="uploaded"/>
						<input type="radio" name="bak1" value="N" <c:if test="${chkstom.bak1=='N' }"> checked="checked"</c:if>/><fmt:message key="Not_upload"/>
					]</div>
				</div>
				<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width:25px;">&nbsp;</span></td>
								<td style="width:30px;"><input type="checkbox" id="chkAll"/></td>
								<td style="width:80px;"><fmt:message key="reported_num"/></td>
								<td style="width:100px;"><fmt:message key="date"/></td>
								<td style="width:130px;"><fmt:message key="time"/></td>
								<td style="width:80px;"><fmt:message key="scm_dept"/><fmt:message key="coding"/></td>
								<td style="width:100px;"><fmt:message key="scm_dept"/><fmt:message key="name"/></td>
								<td style="width:80px;<c:if test="${isNotShowSp_price == 'Y' }">display:none;</c:if>"><fmt:message key="total_amount1"/></td>
								<td style="width:100px;"><fmt:message key="orders_maker"/></td>
								<td style="width:80px;"><fmt:message key="whether_upload"/></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="chkstom" items="${chkstomList }" varStatus="status">
								<tr>
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td><span style="width:20px; text-align: center;"><input type="checkbox" name="idList" id="chk_${chkstom.chkstoNo }" value="${chkstom.chkstoNo }"/></span></td>									
									<td><span style="width:70px;" title="${chkstom.chkstoNo }">${chkstom.chkstoNo }</span></td>
									<td><span style="width:90px;" title="<fmt:formatDate value="${chkstom.maded}" pattern="yyyy-MM-dd" type="date"/>"><fmt:formatDate value="${chkstom.maded}" pattern="yyyy-MM-dd" type="date"/></span></td>
									<td><span style="width:120px;" title="${chkstom.madet}">${chkstom.madet}</span></td>
									<td><span style="width:70px;" title="${chkstom.dept}">${chkstom.dept }</span></td>
									<td><span style="width:90px;" title="${chkstom.positn.des}">${chkstom.positn.des }</span></td>
									<td <c:if test="${isNotShowSp_price == 'Y' }">style="display:none;"</c:if>><span style="width:70px;text-align:right;" title="${chkstom.totalAmt }"><fmt:formatNumber value="${chkstom.totalAmt }" type="currency" pattern="0.00"/></span></td>
									<td><span style="width:90px;" title="${chkstom.madebyName}">${chkstom.madebyName }</span></td>
									<td><span style="width:70px;" title="${chkstom.bak1}"><img src="<%=path%>/image/kucun/${chkstom.bak1 }.png"/></span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		var validate;
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		$(document).ready(function(){
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),78);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法			
			changeTh();
			$('.grid').find('.table-body').find('tr').live("click", function () {
				if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     } else {
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			     }
			 });
			
			//双击事件
			$('.grid .table-body tr').live('dblclick',function(){
				$(":checkbox").attr("checked", false);
				$(this).find(":checkbox").attr("checked", true);
				toSelectDetailed();
			});
			
			/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'chkstoNo',
					validateType:['canNull','intege'],
					param:['T','T'],
					error:['','<fmt:message key="single_number_is_digital_or_empty"/>！']
				}]
			});

			var detailWin;
			function toSelectDetailed(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList&& checkboxList.filter(':checked').size() ==1){
					var chkValue = checkboxList.filter(':checked').val();
					detailWin = $('body').window({
						id: 'checkedChkoutmDetail',
						title: '<fmt:message key="select"/><fmt:message key="scm_dept"/><fmt:message key="reports"/><fmt:message key="the_detail"/>',//查询档口报货单明细
						content: '<iframe id="checkedChksoutDetailFrame" frameborder="0" src="<%=path%>/chkstomDeptMis/findDetail.do?chkstoNo='+chkValue+'"></iframe>',
						width: '85%',
						height: '500px',
						draggable: true,
						isModal: true,
						confirmClose:false,
						topBar: {
							items: [{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel"/>',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-60px','0px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else{
					alert('<fmt:message key="please_select_data_to_view"/>！');
					return ;
				}
			}
		});
		
		var tool = $('.tool').toolbar({
			items: [{
					text: '<fmt:message key="select" />',
					title: '<fmt:message key="select" />',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						if(validate._submitValidate()){
							$("#listForm").submit();
						}
					}
				},{
					text: '<fmt:message key="Daily_order_generation_stores" />',
					title: '<fmt:message key="Daily_order_generation_stores" />',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-18px','0px']
					},
					handler: function(){
						if ($('.grid').find('.table-body').find(':checkbox').filter(':checked').size()<1) {
							alert('<fmt:message key="please_select_data_need_to_be_operated" />!');//please_select_data_need_to_be_operated
						}else{
							toChkstom();
						}
					}
				},{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit" />',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
					}
				}
			]
		});
		
		// 检测报货时间
		function checkOrderTime(){
			var param = {};
			var bool = false;
			param['maded'] = $("#bdate").val();
			param['firm'] = $('#firm').val();
			$.post('<%=path%>/chkstomMis/eqOrderTime.do',param,function(data){
				var errorLog = data;
				if(errorLog!=null && errorLog!='1'){
					alert('<fmt:message key="misboh_DeclareGoodsGuide_orderTimeError"/>【'+errorLog+'】<fmt:message key="misboh_DeclareGoodsGuide_orderTimeError1"/>');
					bool = true;
				}
			});
			return bool;
		}
		
		//生成门店报货单
		function toChkstom(){
			
			if(checkOrderTime()){
				return;
			}
			
			var idChkValue = [];
			var flag = true;
			var chkstono = 0;
			var checkboxList = $('.grid').find('.table-body').find('tr').find(':checkbox');
			checkboxList.filter(':checked').each(function(i){
				idChkValue.push($(this).val());
				if($(this).parents('tr').find('td:eq(9) span').attr('title') == 'Y'){
					flag = false;
					chkstono = $(this).val();
					return false;
				}
			});
			if(!flag){
				alert('<fmt:message key="reported_num"/>:['+chkstono+']<fmt:message key="uploaded"/>!');
				return;
			}
			var ids=idChkValue.join(",");
			$('body').window({
				id: 'window_chkstomexplan',
				title: '<fmt:message key="Daily_order_generation_stores" />',
				content: '<iframe id="chkstomFrame" name="chkstomFrame" frameborder="0" src="<%=path%>/chkstomDeptMis/toChkstom.do?idList='+ids+'"></iframe>',
				width: '85%',
				height: '500px',
				draggable: true,
				isModal: true
			});
		}
		function pageReload(){
	        window.location.href = '<%=path%>/chkstomDeptMis/listCheckedChkstom.do';
	    }
		</script>
	</body>
</html>