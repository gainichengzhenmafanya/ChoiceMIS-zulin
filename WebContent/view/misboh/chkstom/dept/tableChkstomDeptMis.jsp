<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>MIS-档口报货单填制</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			
		<style type="text/css">
		.page{
			margin-bottom: 25px;
		}
		.test{
			color:blue;
		}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<%--存放一个状态 判断是何种操作类型 --%>
		<input type="hidden" id="sta" name="sta" value="${sta }"/>
		<input type="hidden" id="isNotShowSp_price" value="${isNotShowSp_price }"/><!-- 是否显示单价和金额 -->
		<input type="hidden" id="isNotUpdateHoped" value="${isNotUpdateHoped }"/><!-- 是否不能修改报货日期和到货日期 -->
		<form id="listForm" action="<%=path%>/chkstomDeptMis/saveByAdd.do" method="post">
			<input type="hidden" id="upd" name="upd" value="${upd}"/>
			<input type="hidden" id="firm" name="firm" value="${chkstom.firm}"/>
			<div class="bj_head">
				<div class="form-line">
					<div class="form-label"><fmt:message key="date"/>：</div>
					<div class="form-input" style="width:255px;">
						<input type="text" style="width:253px;" id="maded" name="maded" class="Wdate text" value="<fmt:formatDate value="${chkstom.maded}" pattern="yyyy-MM-dd"/>" 
						<c:if test="${isNotUpdateHoped != 'Y' }"> onfocus="new WdatePicker({minDate:new Date()})" </c:if> readonly="readonly"/>
					</div>
					<div class="form-label"><fmt:message key="orders_num"/>:</div>
					<div class="form-input" style="width:100px;">
						<c:if test="${chkstom.chkstoNo!=null}"><c:out value="${chkstom.chkstoNo }"></c:out></c:if>
						<input type="hidden" id="chkstoNo" name="chkstoNo" class="text" value="${chkstom.chkstoNo }"/> 					
					</div>
<%-- 					<div class="form-label"><fmt:message key="all_the_revised_delivery_date"/>:</div> --%>
<!-- 					<div class="form-input" style="width:100px;"> -->
<%-- 						<input type="text" id="hoped" class="Wdate text" value="<fmt:formatDate value="${chkstom.maded}" pattern="yyyy-MM-dd"/>" onfocus="new WdatePicker({minDate:new Date()})" style="width:90px;"/> --%>
<!-- 					</div> -->
<%-- 					<input type="button" value="<fmt:message key="update"/>" onclick="setHoped()"/> --%>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="scm_export_dept"/>：</div>
					<div class="form-input" style="width:255px;">
						<input type="text" class="text" id="deptdes" onfocus="this.select()" onblur="selectFirstFirm(this)" name="deptdes" style="width:50px;margin-top:4px;vertical-align:top;" value="${chkstom.dept}"/>
						<select class="select" id="dept" name="dept" style="width:200px" onchange="findByDes(this);">							
							<c:forEach var="positn" items="${positnList}" varStatus="status">
								<option
								<c:if test="${positn.code==chkstom.dept}"> 
									   	selected="selected"
								</c:if>
								id="des" value="${positn.code}" title="${positn.des}">${positn.code}-${positn.init}-${positn.des}</option>
							</c:forEach>
						</select>
					</div>
					<div class="form-label" style="width:80px;margin-left:30px;"><fmt:message key="orders_maker"/>:</div>
					<div class="form-input" style="width:100px;">
						<c:if test="${chkstom.madeby!=null}"><c:out value="${chkstom.madebyName}"></c:out></c:if>					
						<input type="hidden" id="madeby" name="madeby" class="text" value="${chkstom.madeby }"/>
					</div>
					<div class="form-label">
						<fmt:message key="please_select_materials"/>:
						<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
					</div>
				</div>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 26px;"><span >&nbsp;</span></td>
								<td colspan="3"><fmt:message key="supplies"/></td>
								<td colspan="2"><fmt:message key="procurement_unit"/></td>
								<td colspan="4"><fmt:message key="standard_unit"/></td>
								<td rowspan="2"><span style="width:90px;"><fmt:message key="arrival_date"/></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="remark"/></span></td>
							</tr>
							<tr>									
								<td class="num" style="width: 26px;"><span >&nbsp;</span></td>
								<td><span style="width:70px;"><fmt:message key="supplies_code"/></span></td>
								<td><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
								<td><span style="width:90px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:70px;"><fmt:message key="quantity"/></span></td>								
								<td><span style="width:55px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:50px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:50px;"><fmt:message key="unit"/></span></td>
								<td <c:if test="${isNotShowSp_price == 'Y' }">style="display:none;"</c:if>><span style="width:70px;"><fmt:message key="unit_price"/></span></td>
								<td <c:if test="${isNotShowSp_price == 'Y' }">style="display:none;"</c:if>><span style="width:80px;"><fmt:message key="amount"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<c:set var="sum_num" value="${0}"/>  <!-- 物资条数 -->
				<c:set var="sum_amount" value="${0}"/>  <!-- 总数量 -->
				<c:set var="sum_price" value="${0}"/>  <!-- 总数量 -->
				<div class="table-body">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>
						<c:forEach var="chkstod" items="${chkstodList}" varStatus="status">
							<tr data-stomin="${chkstod.stomin}" data-stocnt="${chkstod.stocnt}" data-mincnt="${chkstod.supply.mincnt}" data-unitper="${chkstod.supply.unitper3 }">
								<td align="center" style="width:26px;"><span >${status.index+1}</span></td>
								<td><span style="width:70px;" data-sp_name="${chkstod.supply.sp_name }">${chkstod.supply.sp_code }</span></td>
								<td><span style="width:100px;">${chkstod.supply.sp_name }</span></td>
								<td><span style="width:90px;">${chkstod.supply.sp_desc }</span></td>
								<td><span style="width:70px;text-align: right;" title="${chkstod.amount1}"><fmt:formatNumber value="${chkstod.amount1}" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:55px;">${chkstod.supply.unit3 }</span></td>
								<td><span style="width:50px;text-align: right;" title="${chkstod.amount}"><fmt:formatNumber value="${chkstod.amount}" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:50px;">${chkstod.supply.unit }</span></td>
								<td <c:if test="${isNotShowSp_price == 'Y' }">style="display:none;"</c:if>><span style="width:70px;text-align: right;" title="${chkstod.supply.sp_price}"><fmt:formatNumber value="${chkstod.supply.sp_price}" type="currency" pattern="0.00"/></span></td>
								<td <c:if test="${isNotShowSp_price == 'Y' }">style="display:none;"</c:if>><span style="width:80px;text-align: right;"><fmt:formatNumber value="${chkstod.supply.sp_price*chkstod.amount}" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:90px;">${chkstod.hoped}</span></td>
								<td><span style="width:70px;">${chkstod.memo}</span></td>
								<td style="display:none;"><span><input type="hidden" id="price_${status.index+1}" value="${chkstod.price }"/></span></td>								
							</tr>
							<c:set var="sum_num" value="${status.index+1}"/>  
							<c:set var="sum_amount" value="${sum_amount + chkstod.amount1}"/>   
							<c:set var="sum_price" value="${sum_price + chkstod.supply.sp_price*chkstod.amount}"/>   
						</c:forEach>
						</tbody>
					</table>					
				</div>
			</div>
			<div style="height:10px;">	
				<table border="0" cellspacing="0" cellpadding="0" style="margin-top:0;z-index:0;height:10px">
					<thead>
						<tr>
							<td style="width:26px;"></td>
							<td style="width:80px;background:#F1F1F1;"><fmt:message key="total"/><u>&nbsp;&nbsp;<label id="sum_num">${sum_num}</label>&nbsp;&nbsp;</u></td>
							<td style="width:210px;"></td>
							<td style="width:110px;background:#F1F1F1;"><fmt:message key="quantity"/><u>&nbsp;&nbsp;<label id="sum_amount"><fmt:formatNumber value="${sum_amount}" type="currency" pattern="0.00"/></label>&nbsp;&nbsp;</u></td>
							<td style="width:245px;"></td>
							<td style="width:150px;background:#F1F1F1; <c:if test="${isNotShowSp_price == 'Y' }">display:none;</c:if>"><fmt:message key="total_amount"/><u>&nbsp;&nbsp;<label id="sum_price"><fmt:formatNumber value="${sum_price}" type="currency" pattern="0.00"/></label>&nbsp;&nbsp;</u></td>
							<td style="width:15px;"></td>
<%-- 							<td><font color="blue"><fmt:message key="operation_tips"/></font></td> --%>
						</tr>
					</thead>
				</table>
		   </div>
		</form>	
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		//ajax同步设置
// 		$.ajaxSetup({
// 			async: false
// 		});
		var validate;
		//工具栏
		$(document).ready(function(){
			$("#deptdes").val($("#dept").val());
			/*过滤*/
			$('#deptdes').bind('keyup',function(){
		          $("#dept").find('option')
		                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
		                    .attr('selected','selected');
		    });
			//按钮快捷键
			focus() ;//页面获得焦点
			//键盘事件绑定
		 	$(document).bind('keyup',function(e){//mouseup
		 		if($(e.srcElement).is("input")){//对表格内的输入框进行判读，延迟500毫秒
		 			var index=$(e.srcElement).closest('td').index();
		    		if(index=="4"||index=="8"){
		    			$(e.srcElement).unbind('blur').blur(function(e){
			 				validateByMincnt($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
		    			});
		    			validator(index,$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
		    		}
		    	}
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
		 		if($("#sta").val() == "edit" || $("#sta").val() == "add"){
			 		if(window.event && window.event.keyCode == 120) { 
			 			window.event.keyCode = 505; 
			 			getChkstoDemo();
			 		} 
			 		if(window.event && window.event.keyCode == 505)window.event.returnValue=false;
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode)
	            {
	                case 85: $('#autoId-button-101').click(); break;//查看上传
	                case 70: $('#autoId-button-102').click(); break;//查询
	                case 65: $('#autoId-button-103').click(); break;//新建
	                case 69: $('#autoId-button-104').click(); break;//编辑
	                case 83: $('#autoId-button-105').click(); break;//保存
					case 68: $('#autoId-button-106').click(); break;//删除
	                case 80: $('#autoId-button-107').click(); break;//打印
	            }
			}); 
		 	function handler(a){//修复赋值bug
		 		$('#deptdes').val(a);
		 		$('#dept').val(a);
		 	}
		 	document.getElementById("dept").onfocus=function(){
	 			this.size = this.length;
	 			$('#dept').css('height','100px');
	 			$('#dept').parent('.form-input').css('z-index','2');
	 		};
 			document.getElementById("dept").onblur = function(){
	 			this.size = 1;
	 			$('#dept').css('height','20px');
 			};
	 		$('#dept').bind('dblclick', function() {
					$('#deptdes').val($(this).val());
					$('#dept').blur();
			});
			//查找物资
		 	$('#seachSupply').bind('click.custom',function(e){
		 		if($("#sta").val() != "edit" && $("#sta").val() != "add"){//必须是新增或者编辑的情况下才能选择物资  2015.1.2wjf
		 			alert('<fmt:message key="must_add_or_edit_can_select"/>！');
		 			return;
		 		}
		 		if($('#deptDes').val()==''){
					showMessage({
							type: 'error',
							msg: '请先选择申购仓位！',
							speed: 1000
						});
				}else{
			 		if(!!!top.customWindow){
						top.customSupply('<fmt:message key="please_select_materials" />',encodeURI('<%=path%>/misbohcommon/selectSupplyLeft.do?ynsto=Y&single=false&positn='+$('#firm').val()),$('#sp_code'),null,$('#unit1'),$('.unit'),$('.unit1'),null,handler2);
					}
				}
			});
		 	function handler2(sp_codes){
		 		var codes = '';//存放页面上的编码，用来判断是否存在
		 		$('.grid').find('.table-body').find('tr').each(function (){
		 			codes += $(this).find('td:eq(1)').text()+",";
				});
		 		var price;
		 		var sp_code;
				var sp_code_arry = new Array();
		 		sp_code_arry = sp_codes.split(",");
				
				for(var i=0; i<sp_code_arry.length; i++){
					sp_code = sp_code_arry[i];
					if(codes.indexOf(sp_code) != -1 ){//如果已存在，则继续下次循环
						continue;
					}
					$.ajax({
						type:"POST",
						url:"<%=path%>/chkstomMis/findTypByPositn.do?code="+$('#firm').val()+"&sp_code="+sp_code,
						dataType: "json",
						async: false,
						success:function(data){//默认入库取报价
							var url ="<%=path%>/supply/findBprice.do";
							var data1 = "sp_code="+sp_code+"&area="+$('#firm').val()+"&madet="+$('#maded').val();
							if(data=='CK'){//出库取售价
								 url ="<%=path%>/supply/findSprice.do";
								 data1 = "supply.sp_code="+sp_code+"&area.code="+$('#firm').val()+"&madet="+$('#maded').val();
							}
							$.ajax({
								type: "POST",
								url: url,
								data: data1,
								dataType: "json",
								async: false,
								success:function(spprice){
									price = spprice.price;
									$.ajax({
										type: "POST",
										url: "<%=path%>/misbohcommon/findSupplyBySpcode.do",
										data: "sp_code="+sp_code,
										dataType: "json",
										success:function(supply){
											if(!$(".table-body").find("tr:last").find("td:eq(1)").find('span').text()==''){
												$.fn.autoGrid.addRow();
											}
											$(".table-body").find("tr:last").find("td:eq(1)").find('span').text(supply.sp_code).data('sp_name',supply.sp_name);
											$(".table-body").find("tr:last").find("td:eq(2)").find('span').text(supply.sp_name);
											$(".table-body").find("tr:last").find("td:eq(3)").find('span').text(supply.sp_desc);
											$(".table-body").find("tr:last").find("td:eq(5)").find('span').text(supply.unit3);
											$(".table-body").find("tr:last").find("td:eq(4)").find('span').text(0).css("text-align","right");
											$(".table-body").find("tr:last").find("td:eq(6)").find('span').text(0).css("text-align","right");
											$(".table-body").find("tr:last").find("td:eq(8)").find('span').attr('title',price).text(price.toFixed(2)).css("text-align","right");
											$(".table-body").find("tr:last").find("td:eq(7)").find('span').text(supply.unit);
											$(".table-body").find("tr:last").find("td:eq(9)").find('span').text(0).css("text-align","right");
											$(".table-body").find("tr:last").find("td:eq(10)").find('span').text($(".table-body").find("tr:last").prev().find("td:eq(10) span").text()?$(".table-body").find("tr:last").prev().find("td:eq(10) span").text():getInd());
											$(".table-body").find("tr:last").data("unitper",supply.unitper3);
											$(".table-body").find("tr:last").data("mincnt",supply.mincnt);
											$(".table-body").find("tr:last").data("stomin",supply.stomin);
											$(".table-body").find("tr:last").data("stocnt",supply.stocnt);
										}
									});
								}
							});
						}
		 			});
				}
		 	}
			//判断按钮的显示与隐藏
			if($("#sta").val() == "add"){
				loadToolBar([true,true,true,false,true,false,false,false]);
				editCells();
				$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),2);
				setTimeout('keepSessionAjax()',300000);
			}else if($("#sta").val() == "show"){
				loadToolBar([true,true,true,true,false,true,true,true]);
			}else{
				loadToolBar([true,true,true,false,false,false,false,false]);
				$('#deptdes').attr("disabled","disabled");
				$('#dept').attr("disabled","disabled");
			}
			/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'maded',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']
				},{
					type:'text',
					validateObj:'chkstoNo',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key="orders_num"/><fmt:message key="cannot_be_empty"/>！']
				}]
			});
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		   
		    $('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),110);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
		});
		//控制按钮显示
		function loadToolBar(use){
			$('.tool').html('');
			$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[1],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							if($("#sta").val()=="add"||$("#sta").val()=="edit"){
								if(confirm('<fmt:message key="data_unsaved_whether_to_exit"/>？')){
									searchChkstom();
								}
							}else{
								searchChkstom();
							}
						}
					},'-',{
						text: '<fmt:message key="insert" />',
						title: '<fmt:message key="insert"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')}&&use[2],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','0px']
						},
						handler: function(){
							if($("#sta").val()=="add"||$("#sta").val()=="edit"){
								if(confirm('<fmt:message key="data_unsaved_whether_to_exit"/>？')){
									addChkstom();
								}
							}else{
								addChkstom();
							}
						}
					},{
						text: '<fmt:message key="template" />',
						title: '<fmt:message key="template" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')}&&use[4],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','0px']
						},
						handler: function(){
							if($("#dept").val()==null||$("#dept").val()==''){
								if(confirm('请选择报货档口！')){
									return;
								}
							}else{
								getChkstoDemo();
							}
						}
					},{
						text: '<fmt:message key="edit" />',
						title: '<fmt:message key="edit"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[3],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-20px','0px']
						},
						handler: function(){
							if($("#chkstoNo").val()==null || $("#chkstoNo").val()==""){
								return false;
							}else if($("#sta").val()=="add"){
								return false;
							}else{
								$("#sta").val("edit");
								loadToolBar([true,true,true,false,true,true,true,false]);
								editCells();
								setTimeout('keepSessionAjax()',300000);
							}
						}
					},{
						text: '<fmt:message key="save" />',
						title: '<fmt:message key="save"/>',
						useable: use[4],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							if($("#sta").val()=="add" || $("#sta").val()=="edit"){
								if(validate._submitValidate()){
									if(null!=$('#dept').val() && ""!=$('#dept').val()){
										saveChkstom();
									}else{
										alert('报货档口不能为空！');
										/* showMessage({
	 										type: 'error',
	 										msg: '申购仓位不能为空！',
	 										speed: 1000
	 									}); */
									}
								}
							}
						}
					},{
						text: '<fmt:message key="delete" />',
						title: '<fmt:message key="delete"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')}&&use[5],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							deleteChkstom();
						}
					}
					<c:if test="${isNotUpdateHoped != 'Y'}">
					,{
						text: '<fmt:message key="all_the_revised_delivery_date"/>',
						title: '<fmt:message key="all_the_revised_delivery_date"/>',
						useable: use[4],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-58px','-240px']
						},
						handler: function(){
							updateHoped();
						}
					}
					</c:if>
					,'-',{
						text: '<fmt:message key="print" />',
						title: '<fmt:message key="print"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[6],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							printChkstom();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							if($("#sta").val()=="add"||$("#sta").val()=="edit"){
								if(confirm('<fmt:message key="data_unsaved_whether_to_exit"/>？'))
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}else{
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
							}
						}
					}
				]
			});
		}
		
		//编辑表格
		function editCells(){
			if($("#sta").val() == "add"){
				$('#sum_num').text(Number($('#sum_num').text())+1);//总行数
			}
			var colstyle = ['','',{background:"#F1F1F1"},'',{background:"#F1F1F1"},'','','','','',{background:"#F1F1F1"},{background:"#F1F1F1"}];
			if($('#isNotShowSp_price').val() == 'Y'){
				colstyle = ['','',{background:"#F1F1F1"},'',{background:"#F1F1F1"},'','','',{display:"none"},{display:"none"},{background:"#F1F1F1"},{background:"#F1F1F1"}];
			}
			var editable = [2,4,10,11];
			if($('#isNotUpdateHoped').val() == 'Y'){
				colstyle = ['','',{background:"#F1F1F1"},'',{background:"#F1F1F1"},'','','','','','',{background:"#F1F1F1"}];
				editable = [2,4,11];
			}
			$(".table-body").autoGrid({
				initRow:1,
				colPerRow:12,
				widths:[26,80,110,100,80,65,60,60,80,90,100,80],
				colStyle:colstyle,
				VerifyEdit:{verify:true,enable:function(cell,row){
					return row.find('td').index(cell) == 2 || (row.find("td:eq(2)").text() == null || $.trim(row.find("td:eq(2)").text()) != '' ) ? true : false;
				}},
				onEdit:$.noop,
				editable:editable,
				onLastClick:function(row){
					$('#sum_num').text(Number($('#sum_num').text())-1);//总行数
					$('#sum_amount').text(Number($('#sum_amount').text())-row.find('td:eq(4)').text());//总数量
					$('#sum_price').text(Number(Number($('#sum_price').text())-Number(row.find('td:eq(9)').text())).toFixed(2));//总金额
				},
				onEnter:function(data){
					var pos = data.curobj.closest('tr').find('td').index(data.curobj.closest('td'));
					if(pos == 2){
						if($.trim(data.curobj.closest('td').prev().text())){
							data.curobj.find('span').html(data.curobj.closest('td').prev().find('span').data('sp_name'));
							return;
						}else if(!data.actionobj){
							$.fn.autoGrid.setCellEditable(data.curobj.find('span').closest('tr'),2);
							return;
						} 
					}
					$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.value) ;
				},
				cellAction:[{
					index:2,
					action:function(row){
						$.fn.autoGrid.setCellEditable(row,4);
					},
					onCellEdit:function(event,data,row){
						data['url'] = '<%=path%>/misbohcommon/findSupplyTop10.do?ynsto=Y';
						/*if (data.value.split(".").length>2) {
							data['key'] = 'sp_code';
						}else if(!isNaN(data.value)){
							data['key'] = 'sp_code';
						}else{
							data['key'] = 'sp_init';
						}*/
						data['key'] = 'sp_code';
						data.sp_position = $('#firm').val();
						$.fn.autoGrid.ajaxEdit(data,row);
					},
					resultFormat:function(data){
						var desc ='';
						if(data.sp_desc !='' && data.sp_desc !=null){
							desc = '-'+data.sp_desc ;
						}
						return data.sp_init+'-'+data.sp_code+'-'+data.sp_name + desc;
					},
					afterEnter:function(data2,row){
						var num=0;
						$('.grid').find('.table-body').find('tr').each(function (){
							if($(this).find("td:eq(1)").text()==data2.sp_code){
								num=1;
							}
						});
						if(num==1){
							showMessage({
 								type: 'error',
 								msg: '<fmt:message key="added_supplies_remind"/>！',
 								speed: 1000
 							});
							$.fn.autoGrid.setCellEditable(row,2);
							return;
						}
						//查询价格
						if($('#firm').val()==''){
							showMessage({
 								type: 'error',
 								msg: '请先选择报货档口！',
 								speed: 1000
 							});
						}else{
							//判断是取报价还是售价 出库售价 入库报价
							$.ajax({
								type:"POST",
								url:"<%=path%>/chkstomMis/findTypByPositn.do?code="+$('#firm').val()+"&sp_code="+data2.sp_code,//判断是什么类型CK 还是RK
								dataType: "json",
								success:function(data){//默认入库取报价
									var url ="<%=path%>/supply/findBprice.do";
									var data1 = "sp_code="+data2.sp_code+"&area="+$('#firm').val()+"&madet="+$('#maded').val();
									if(data=='CK'){//出库取售价
										 url ="<%=path%>/supply/findSprice.do";
										 data1 = "supply.sp_code="+data2.sp_code+"&area.code="+$('#firm').val()+"&madet="+$('#maded').val();
									}
									$.ajax({
										type: "POST",
										url: url,
										data: data1,
										dataType: "json",
										success:function(spprice){
											row.find("td:eq(1) span").text(data2.sp_code).data('sp_name',data2.sp_name);
											row.find("td:eq(2) span input").val(data2.sp_name).focus();
											row.find("td:eq(3) span").text(data2.sp_desc==null ? "":data2.sp_desc);//修改规格问题
											row.find("td:eq(4) span").text(0).css("text-align","right");//采购数量默认0
											row.find("td:eq(5) span").text(data2.unit3);//采购单位
											row.data("unitper",data2.unitper3);//unitper3 是采购单位和标准单位的转换率
											row.find("td:eq(6) span").text(0).css("text-align","right");//标准数量
											row.find("td:eq(7) span").text(data2.unit);//标准单位
											row.find("td:eq(8) span").text((spprice.price).toFixed(2)).attr("title",spprice.price).css("text-align","right");
											row.find("td:eq(9) span").text(0).css("text-align","right");
											row.find("td:eq(10) span").text(row.prev().find("td:eq(10) span").text()?row.prev().find("td:eq(10) span").text():getInd());
											row.find("td:eq(11) span").text();
											row.data("unitper",data2.unitper3);//unitper3 是采购单位和标准单位的转换率
											row.data("mincnt",data2.mincnt);
											row.data("stomin",data2.stomin);
											row.data("stocnt",data2.stocnt);
										}
									});
								}
							});
						}
					}
				},{
					index:4,
					action:function(row,data2){
						if(Number(data2.value) == 0){
							alert('<fmt:message key="number_cannot_be_zero"/>！');
							row.find("td:eq(4)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,4);
						}else if(Number(data2.value) < 0){
							alert('<fmt:message key="number_cannot_be_negative"/>！');
							row.find("td:eq(4)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,4);
						}else if(isNaN(data2.value)){
							alert('<fmt:message key="number_be_not_number"/>！');
							row.find("td:eq(4)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,4);
						}else if(data2.value.length > 6){
							alert('<fmt:message key="the_maximum_length_not"/>6<fmt:message key="digit"/>！');
							row.find("td:eq(4)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,4);
						}else if(Number(row.data("stomin"))!=0 && (Number(data2.value) < Number(row.data('stomin')))){
							alert('<fmt:message key="the_minimum_purchase_quantity_of_material"/>'+row.data("stomin")+'.');
							row.find("td:eq(4)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,4);
						}else if(Number(row.data("stocnt"))!=0 && !(/(^[1-9]\d*$)/.test(Number((Number(data2.value)/Number(row.data("stocnt"))).toFixed(8))))){
							alert('申购量必须是申购倍数'+row.data("stocnt")+'的整数倍！');
							row.find("td:eq(4)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,4);
						}else if(Number(row.data("stocnt"))==0 && Number(row.data("mincnt"))!=0 && !(/(^[1-9]\d*$)/.test(Number((Number(data2.value)/Number(row.data("mincnt"))).toFixed(8))))){
							alert('申购量必须是申购倍数'+row.data("mincnt")+'的整数倍！');
							row.find("td:eq(4)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,4);
						}else {
							//采购数量变，标准数量6也变     如果转换率不为0，则直接除以转换率，为0  就不算了
							row.find("td:eq(4)").find('span').attr('title',data2.value);
							if(row.data("unitper") != 0){
								var amount = Number(row.find("td:eq(4) span").text())/Number(row.data("unitper"));
								row.find("td:eq(6) span").text(amount.toFixed(2)).attr('title',amount);//标准数量
								row.find("td:eq(9) span").text((amount*Number(row.find("td:eq(8) span").attr('title'))).toFixed(2));//总金额
								getTotalSum();
							}
							if($('#isNotUpdateHoped').val() == 'Y'){
								$.fn.autoGrid.setCellEditable(row,11);
							}else{
								$.fn.autoGrid.setCellEditable(row,10);
							}
						}
					}
				},{
					index:10,
					action:function(row,data){
						$.fn.autoGrid.setCellEditable(row,11);
					},CustomAction:function(event,data){
						var input = data.curobj.find('span').find('input');
						input.addClass("Wdate text");
						input.bind('click',function(){
							input.unbind('keyup');
							new WdatePicker({
								startDate:'%y-%M-{%d+1}',
								el:'input',
								onpicked:function(dp){
									data.curobj.find('span').html(dp.cal.getNewDateStr());
									$.fn.autoGrid.setCellEditable(data.row,11);
								}
							});
						});
						input.trigger('click');
					}
				},{
					index:11,
					action:function(row,data){
						if(!row.next().html())$.fn.autoGrid.addRow();
						$.fn.autoGrid.setCellEditable(row.next(),2);
						$('#sum_num').text(Number($('#sum_num').text())+1);//总行数
					}
				}]
			});
		}
		function getTotalSum(){//计算统计数据
			var sum_amount = 0; 
			var sum_totalamt = 0;
			$('.table-body').find('tr').each(function (){
				if($(this).find('td:eq(1)').text()!=''){//非空行
					var amount = $(this).find('td:eq(4)').text();
					if(!amount){
						amount = $(this).find('td:eq(4)').find("input:eq(0)").val();
					}
					sum_amount += Number(amount);
					var price  = $(this).find('td:eq(9)').text();
					if(!price){//正在编辑该数据
						price = $(this).find('td:eq(9)').find("input:eq(0)").val();
					}
					sum_totalamt = parseFloat(sum_totalamt) + parseFloat(price);
				}
			});
			$('#sum_num').text($(".table-body").find('tr').length);//总行数
			$('#sum_amount').text(sum_amount);//总数量
			$('#sum_price').text(Number(sum_totalamt).toFixed(2));//总金额
		}
		
		//查找报货单
		function searchChkstom(){
			var curwindow = $('body').window({
				id: 'window_searchChkstom',
				title: '<fmt:message key="query_messages_manifest"/>',
				content: '<iframe id="searchChkstomFrame" frameborder="0" src="<%=path%>/chkstomDeptMis/searchByKey.do?init=init&upd='+$('#upd').val()+'"></iframe>',
				width: 800,
				height: 430,
				draggable: true,
				isModal: true
			});
			curwindow.max();
		}
		
		//查找报货单
		function searchUncheck(bdate,edate){
			var action = "<%=path%>/chkstomDeptMis/searchByKey.do?upd=1&bMaded="+bdate+"&eMaded="+edate;
			var curwindow = $('body').window({
				id: 'window_searchChkstom',
				title: '<fmt:message key="query_messages_manifest"/>',
				content: '<iframe id="searchChkstomFrame" frameborder="0" src='+action+'></iframe>',
				width: 800,
				height: 430,
				draggable: true,
				isModal: true
			});
			curwindow.max();
		}
		
		//新增报货单
		function addChkstom(){			
			window.location.replace("<%=path%>/chkstomDeptMis/addChkstom.do?upd="+$('#upd').val());
		}
		
		//保存添加
		function saveChkstom(){
			//先判断有没有上传
			var dataS = {};
			var flag = true;
			dataS['chkstoNo'] = $("#chkstoNo").val();
			if($("#sta").val()=='edit')
				$.ajax({url:'<%=path%>/chkstomDeptMis/chkChect.do',type:'POST',
					data:dataS,async:false,success:function(data){
					if ("ok"!=data) {
						flag = false;
					}
				}});
			if(!flag){
				alert("此单据已经上传，不能进行编辑！");
				return;
			}
			var indNull=0;//默认0代表成功
			var numNull=0;//默认0代表成功
			var isNull=0;
			var sp_name = '';
			$('.table-body').find('tr').each(function(){
				if($(this).find('td:eq(1)').text()!=''){
					isNull=1;
					var amount=$(this).find('td:eq(4)').text()?$(this).find('td:eq(4)').text():$(this).find('td:eq(4) input').val();
					var ind=$(this).find('td:eq(10)').text()?$(this).find('td:eq(10)').text():$(this).find('td:eq(10) input').val();
					if(Number(amount)==0){
						numNull=1;//1数量为0
						sp_name = $(this).find('td:eq(2)').text()?$(this).find('td:eq(2)').text():$(this).find('td:eq(2) input').val();
						return false;
					}
					if(amount=="" || amount==null || isNaN(amount)){
						numNull=2;//2数量不是数字
						sp_name = $(this).find('td:eq(2)').text()?$(this).find('td:eq(2)').text():$(this).find('td:eq(2) input').val();
						return false;
					}
					//申购倍数最小申购量判断
					if(Number($(this).data("stomin"))!=0 && (Number(amount) < Number($(this).data('stomin')))){
						numNull = 3;
						indNull = Number($(this).data("stomin"));
						sp_name = $(this).find('td:eq(2)').text()?$(this).find('td:eq(2)').text():$(this).find('td:eq(2) input').val();
						return false;
					}
					if(Number($(this).data("stocnt"))!=0 && !(/(^[1-9]\d*$)/.test(Number((Number(amount)/Number($(this).data("stocnt"))).toFixed(8))))){
						numNull = 4;
						indNull = Number($(this).data("stocnt"));
						sp_name = $(this).find('td:eq(2)').text()?$(this).find('td:eq(2)').text():$(this).find('td:eq(2) input').val();
						return false;
					}
					if(Number($(this).data("stocnt"))==0 && Number($(this).data("mincnt"))!=0 && !(/(^[1-9]\d*$)/.test(Number((Number(amount)/Number($(this).data("mincnt"))).toFixed(8))))){
						numNull = 4;
						indNull = Number($(this).data("mincnt"));
						sp_name = $(this).find('td:eq(2)').text()?$(this).find('td:eq(2)').text():$(this).find('td:eq(2) input').val();
						return false;
					}
					if(ind==''||ind==null){
						indNull=1;//1日期为空
						sp_name = $(this).find('td:eq(2)').text()?$(this).find('td:eq(2)').text():$(this).find('td:eq(2) input').val();
						return false;
					}
				}
			});
			if(Number(isNull)==0){
				alert('<fmt:message key="not_add_empty_document"/>！');
				return;
			}
			if(Number(numNull)==1){//数量不为0
				alert('<fmt:message key="supplies"/>：['+sp_name +']<fmt:message key="number_cannot_be_zero"/>！');
				return;
			}
			if(Number(numNull)==2){//数量为空或字母
				alert('<fmt:message key="supplies"/>：['+sp_name +']<fmt:message key="quantity"/><fmt:message key="not_valid_number"/>！');
				return;
			}
			if(Number(numNull)==3){//最小申购量
				alert('<fmt:message key="supplies"/>：['+sp_name +']<fmt:message key="the_minimum_purchase_quantity_of_material"/>'+indNull+'.');
				return;
			}
			if(Number(numNull)==4){//最小申购倍数
				alert('<fmt:message key="supplies"/>：['+sp_name +']申购量必须是申购倍数'+indNull+'的整数倍！');
				return;
			}
			if(Number(indNull)==1){//日期为空
				alert('<fmt:message key="supplies"/>：['+sp_name +']<fmt:message key="arrival_date"/><fmt:message key="cannot_be_empty"/>！');
				return;
			}
			$('#wait').show();
			$('#wait2').show();
			var keys = ["supply.sp_code","supply.sp_name","supply.sp_desc","amount1","supply.unit3","amount","supply.unit",
			            "price","totalAmt","hoped","memo"];
			var data = {};
			var i = 0;
			$("#listForm *[name]").each(function(){
				var name = $(this).attr("name"); 
				if(name) data[name] = $(this).val();
			});
			var rows = $(".grid .table-body table tr");
			for(i=0;i<rows.length;i++){
// 				var status=$(rows[i]).find('td:eq(0)').text();
// 				data["chkstodList["+i+"]."+"price"] = $(rows[i]).find('td:eq(8) span').attr('title') ? $(rows[i]).find('td:eq(8) span').attr('title'):$('#price_'+status).val();
				if(!$(rows[i]).find('td:eq(1) span').text())break;
				cols = $(rows[i]).find("td");
				var j = 0;
				for(j=1;j <= keys.length;j++){
					var value = $.trim($(rows[i]).find("td:eq("+j+") span").attr('title'))?$.trim($(rows[i]).find("td:eq("+j+") span").attr('title')):$.trim($(rows[i]).find("td:eq("+j+")").text());
					value = value ? value : $.trim($(rows[i]).find("td:eq("+j+") input").val());
					if(value)
						data["chkstodList["+i+"]."+keys[j-1]] = value;
				}
			}
			//提交并返回值，判断执行状态
			$.post("<%=path%>/chkstomDeptMis/saveByAddOrUpdate.do?sta="+$("#sta").val(),data,function(data){
				$('#wait').hide();
				$('#wait2').hide();
				if(data == 1){
					alert('<fmt:message key="save_successful"/>！');
					openChkstom($('#chkstoNo').val());
				}else{
					alert(data);
				}
			});			 
		}
		
		//删除
		function deleteChkstom(){
			var chkstoNo=$("#chkstoNo").val();
			if($("#chkstoNo").val()!=null && $("#chkstoNo").val()!="" && $("#chkstoNo").val()!=0){
				//先判断有没有上传
				var dataS = {};
				var flag = true;
				dataS['chkstoNo'] = chkstoNo;
				$.ajax({url:'<%=path%>/chkstomDeptMis/chkChect.do',type:'POST',
					data:dataS,async:false,success:function(data){
					if ("ok"!=data) {
						flag = false;
					}
				}});
				if(!flag){
					alert("此单据已经上传，不能删除！");
					return;
				}
				if($("#sta").val()!="show"){
					alert('<fmt:message key="orders_unsaved_cannot_deleted"/>！');
				}else{
					if(confirm('<fmt:message key="whether_delete_orders"/>？')){
						//提交并返回值，判断执行状态
						$('#wait').show();
						$('#wait2').show();
						$.post("<%=path%>/chkstomDeptMis/deleteChkstom.do?chkstoNo="+chkstoNo,function(data){
							$('#wait').hide();
							$('#wait2').hide();
							var rs = eval('('+data+')');
							switch(Number(rs.pr)){
							case 0:
								alert('<fmt:message key="orders_unsaved_cannot_audited"/>！');
								break;
							case 1:
								alert('<fmt:message key="successful_deleted"/>！');
								window.location.replace("<%=path%>/chkstomDeptMis/table.do?upd="+$('#upd').val());
								break;
							}
						});
					}						
				}
			}
		}
		
		// 查找添加盘点模板
		function getChkstoDemo(){
	    	var action = "<%=path%>/chkstomMis/addChkstoDemo.do?firm="+$('#firm').val();//调用模板的时候传申购门店
			$('body').window({
				id: 'window_addChkstoDemo',
				title: '<fmt:message key="report_form_template"/>',
				content: '<iframe id="addChkstoDemoFrame" name="addChkstoDemoFrame" frameborder="0" src='+action+'></iframe>',
				width: $(document.body).width()-20,//'1000px'
				height: '80%',
				draggable: true,
				isModal: true,
				confirmClose: false,
				topBar: {
					items: [{
							text: '<fmt:message key="enter"/>',
							title: '<fmt:message key="enter"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								if(getFrame('addChkstoDemoFrame')){
									window.frames["addChkstoDemoFrame"].enterUpdate();
								}
							}
						},{
							text: '<fmt:message key="cancel"/>',
							title: '<fmt:message key="cancel"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.close').click();
							}
						}]
				}
			});
		}
		
		//获取系统时间
		function getInd(){
			var myDate=new Date(Date.parse($('#maded').val()) + 24*60*60*1000);  
			var yy=myDate.getFullYear();
			var MM=myDate.getMonth()+1;
			var dd=myDate.getDate();
			if(MM<10)
				MM="0"+MM;
			if(dd<10)
				dd="0"+dd;
			return fullDate=yy+"-"+MM+"-"+dd;
		}
		//打印单据
		function printChkstom(){ 
			if(null!=$("#chkstoNo").val() && ""!=$("#chkstoNo").val()){
				window.open ("<%=path%>/chkstomDeptMis/printChkstom.do?chkstoNo="+$("#chkstoNo").val(),'newwindow','height='+window.screen.height+',width='+window.screen.width+',top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');
			}
		}
		//双击打开
		function openChkstom(chkstoNo){
			window.location.replace("<%=path%>/chkstomDeptMis/findChk.do?chkstoNo="+chkstoNo+"&upd="+$('#upd').val());
		}
		function validateByMincnt(index,row,data2){//最小申购量判断
			if(index=="4"){
				if(Number(data2.value) == 0){
					alert('<fmt:message key="number_cannot_be_zero"/>！');
					row.find("td:eq(4)").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,4);
				}else if(Number(row.data("stomin"))!=0 && (Number(data2.value) < Number(row.data('stomin')))){
					alert('<fmt:message key="the_minimum_purchase_quantity_of_material"/>'+row.data("stomin")+'.');
					row.find("td:eq(4)").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,4);
				}else if(Number(row.data("stocnt"))!=0 && !(/(^[1-9]\d*$)/.test(Number((Number(data2.value)/Number(row.data("stocnt"))).toFixed(8))))){
					alert('申购量必须是申购倍数'+row.data("stocnt")+'的整数倍！');
					row.find("td:eq(4)").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,4);
				}else if(Number(row.data("stocnt"))==0 && Number(row.data("mincnt"))!=0 && !(/(^[1-9]\d*$)/.test(Number((Number(data2.value)/Number(row.data("mincnt"))).toFixed(8))))){
					alert('申购量必须是申购倍数'+row.data("mincnt")+'的整数倍！');
					row.find("td:eq(4)").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,4);
				}
			}
		}
		function validator(index,row,data2){//输入框验证
			if(index=="4"){
				if(Number(data2.value) < 0){
					alert('<fmt:message key="number_cannot_be_negative"/>！');
					row.find("td:eq(4)").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,4);
				}else if(isNaN(data2.value)){
					alert('<fmt:message key="number_be_not_number"/>！');
					row.find("td:eq(4)").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,4);
				}else if(data2.value.length > 6){
					alert('<fmt:message key="the_maximum_length_not"/>6<fmt:message key="digit"/>！');
					row.find("td:eq(4)").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,4);
				}else {
					if(isNaN(Number(row.data("unitper")))){
						row.data("unitper",Number(data2.value)/data2.ovalue);
					}
					$('#sum_amount').text(Number($('#sum_amount').text())+Number(data2.value)-Number(data2.ovalue));//总数量
					row.find("input").data("ovalue",data2.value);
					row.find("td:eq(4)").find('span').attr('title',data2.value);
					if(row.data("unitper") != 0){
						var amount = Number(data2.value)/Number(row.data("unitper"));
						row.find("td:eq(6) span").text(amount.toFixed(2)).attr('title',amount);//标准数量
						row.find("td:eq(9) span").text((amount*Number(row.find("td:eq(8) span").attr('title'))).toFixed(2));//总金额
						getTotalSum();
					}
				}
			}
		}
		//统一修改到货日期
		function setHoped(){
			if($('#hoped').val()<$('#maded').val()){
				alert('<fmt:message key="The_arrival_date_cannot_be_less_than_made_date"/>！');
				return;
			}
			$('.table-body').find('tr').each(function(){
				if ($(this).find('td:eq(1)').text()!=null&&$(this).find('td:eq(1)').text()!='') {
					$(this).find('td:eq(10)').find('span').text($('#hoped').val());
				}
			});
		}
		
		//统一修改到货日期
		function updateHoped(){
			var html = '<div style="margin:10px 40px;"><input type="text" id="hoped" class="Wdate text" value="<fmt:formatDate value="${chkstom.maded}" pattern="yyyy-MM-dd"/>" onfocus="new WdatePicker({minDate:new Date()})" style="width:90px;"/></div>';
			$('body').window({
				id: 'window_updateHoped',
				title: '<fmt:message key="all_the_revised_delivery_date"/>',
				content: html,
				width: '200px',//'1000px'
				height: '100px',
				draggable: true,
				isModal: true,
				confirmClose: false,
				topBar: {
					items: [{
							text: '<fmt:message key="enter"/>',
							title: '<fmt:message key="enter"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								setHoped();
								$('.close').click();
							}
						},{
							text: '<fmt:message key="cancel"/>',
							title: '<fmt:message key="cancel"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.close').click();
							}
						}]
				}
			});
		}
		
		//新增和编辑状态下 保持session
		function keepSessionAjax(){
			$.ajax({
				url:'<%=path%>/misbohcommon/keepSessionAjax.do',
				type:"post",
				success:function(data){}
			});
			setTimeout('keepSessionAjax()',300000);
		}
		</script>			
	</body>
</html>