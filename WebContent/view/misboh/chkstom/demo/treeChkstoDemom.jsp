<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>报货单模板主表</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.bgBlue{
				background: #D2E9FF;
			}
		</style>
	</head>
	<body>
		<input type="hidden" name="sta" id="sta" value="select"/>
		<div class="tool"></div>
			<form id="listForm" action="<%=path%>/chkstodemomMis/listChkstoDemom.do" method="post">
				<input type="hidden" name="tempTyp" id="tempTyp" value="${chkstoDemom.tempTyp }"/><!-- 模板类型 用来区分报货单模板，出库单模板2015.9.17wjf -->
				<div class="form-line">
					<div class="form-label" style="width:80px;"><fmt:message key="title"/>：</div>
					<div class="form-input">
						<input type="text" id="title" name="title" style="width:120px;" class="text" value="${chkstoDemom.title }" />
					</div>
					<div class="form-label" style="width:80px;"><fmt:message key="scm_year"/>：</div>
					<div class="form-input">
						<input type="text" id="yearr" name="yearr" class="text" value="${chkstoDemom.yearr }" />
					</div>
					<div class="form-label"><fmt:message key="fill_time"/>：</div>
					<div class="form-input">
						<input type="text" id="maded" name="maded" class="Wdate text" onfocus="WdatePicker();" value="<fmt:formatDate value='${chkstoDemom.maded}' pattern='yyyy-MM-dd'/>"/>
					</div>
				</div>
				<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 25px;"></span></td>
									<td style="width:30px; text-align: center;">
										<span>
<!-- 											<input type="checkbox" id="chkAll"/> -->
										</span>
									</td>
									<td><span style="width:150px;"><fmt:message key="title"/></span></td>
									<td><span style="width:60px;"><fmt:message key="scm_year"/></span></td>
									<td><span style="width:80px;"><fmt:message key="fill_time"/></span></td>
									<td><span style="width:100px;"><fmt:message key="time"/></span></td>
									<td><span style="width:80px;"><fmt:message key="orders_maker"/></span></td>
<!-- 									<td><span style="width:100px;">凭证号</span></td> -->
<!-- 									<td><span style="width:100px;">合计金额</span></td> -->
<!-- 									<td><span style="width:100px;">状态</span></td> -->
									<td><span style="width:120px;"><fmt:message key="remark"/></span></td>
									<td><span style="width:80px;"><fmt:message key="Definition_mode"/></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0" class="datagrid">
							<tbody>
								<c:forEach var="chkstodemom" items="${listChkstoDemom}" varStatus="status">
									<tr>
										<td class="num" ><span style="width: 25px;">${status.index+1}</span></td>
										<td style="width:30px; text-align: center;">
											<input type="checkbox" name="idList" id="chk_<c:out value='${chkstodemom.chkstodemono}' />" value="<c:out value='${chkstodemom.chkstodemono}' />"/>
										</td>
										<td><span style="width:150px;">${chkstodemom.title}</span></td>
										<td><span style="width:60px;">${chkstodemom.yearr}</span></td>
										<td><span style="width:80px;"><fmt:formatDate value="${chkstodemom.maded}" pattern="yyyy-MM-dd"/></span></td>
										<td><span style="width:100px;">${chkstodemom.madet}</span></td>
										<td><span style="width:80px;">${chkstodemom.madeby}</span></td>
<%-- 										<td><span style="width:100px;">${chkstodemom.vouno}</span></td> --%>
<%-- 										<td><span style="width:100px;"><fmt:formatNumber type="number" value="${chkstodemom.totalamt} " pattern="#.##"/></span></td> --%>
<%-- 										<td><span style="width:100px;">${chkstodemom.status}</span></td> --%>
										<td><span style="width:120px;">${chkstodemom.memo}</span>
										</td>
										<td><span style="width:80px;">
											<c:choose> 
													 <c:when test="${chkstodemom.typ  eq 'Y'}"><fmt:message key="Store_definition"/></c:when> 
													  <c:otherwise><fmt:message key="Central_release"/></c:otherwise> 
											</c:choose>
											<input type="hidden" name="typ" id="typ${chkstodemom.chkstodemono}" value="${chkstodemom.typ}"/>
											</span>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<page:page form="listForm" page="${pageobj}"></page:page>
					<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
					<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
				</div>
			</form>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
// 			if($.browser.msie){ 
// 				if ($.browser.version == 8.0) {
// 					setElementHeight('.grid',['.tool'],$(document.body),390);	//计算.grid的高度--适用IE8
// 				} else {
// 					setElementHeight('.grid',['.tool'],$(document.body),495);	//计算.grid的高度 -- 适用IE8以上版本
// 				}
// 			};
			setElementHeight('.grid',['.tool'],$(document.body),80);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid',20);	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			var tool = $('.tool').toolbar({
				items: [{
					text: '<fmt:message key="select"/>',
					title: '<fmt:message key="select"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						$('#sta').val("select");
						$("#listForm").submit();
// 						$('.search-div').slideToggle(100);
					}
				},{
					text: '<fmt:message key="view" />',
					title: '<fmt:message key="view"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-140px','-100px']
					},
					handler: function(){
						viewList();
					}
				},"-",{
						text: '<fmt:message key="insert"/>',
						title: '<fmt:message key="insert"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','0px']
						},
						handler: function(){
							$('#sta').val("add");
							saveChkstoDemod();
						}
					},{
						text: '<fmt:message key="update"/>',
						title: '<fmt:message key="update"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-18px','0px']
						},
						handler: function(){
							$('#sta').val("update");
							updateChkstoDemod();
						}
					},{
						text: '<fmt:message key="delete"/>',
						title: '<fmt:message key="delete"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							$('#sta').val("delete");
							deleteChkstoDemod();
						}
					},"-",{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
						}
					}
				]
			});
			
			//双击
			$('.grid .table-body tr').live('dblclick',function(){
				$(":checkbox").attr("checked", false);
				$(this).find(":checkbox").attr("checked", true);
				viewList();
			});
			
			//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     } else {
			    	 $('.grid').find(":checkbox").attr("checked", false);
			         $('.bgBlue').removeClass("bgBlue");
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			     }
			});
		});
		
		//查看详情
		function viewList(){
            var checkboxList = $('.grid').find('.table-body').find(':checkbox');
            if(checkboxList&& checkboxList.filter(':checked').size() == 1){
                var chkstodemono = checkboxList.filter(':checked').eq(0).val();
                var title = $('.grid').find('.table-body').find('tr').find(':checkbox').filter(':checked').parents('tr').find('td:eq(2) span').text();
                var  url="<%=path%>/chkstodemomMis/listChkstoDemod.do?chkstodemono="+chkstodemono;
                $('body').window({
                    id: 'window_view',
                    title: '<fmt:message key="title" />：'+ title,
                    content: '<iframe id="viewListFrame" frameborder="0" src="'+url+'"></iframe>',
                    width: '800px',
                    height: '480px',
                    draggable: true,
                    isModal: true,
                    topBar: {
                        items: [{
                            text: '<fmt:message key="close" />',
                            title: '<fmt:message key="close" />',
                            icon: {
                                url: '<%=path%>/image/Button/op_owner.gif',
                                position: ['-160px','-100px']
                            },
                            handler: function(){
                                $('.close').click();
                            }
                        }
                        ]
                    }
                });
            }else if(checkboxList&& checkboxList.filter(':checked').size()>1){
                alert('<fmt:message key="can_nly_choose_a_piece_of_information"/>！');
            }else{
                alert('<fmt:message key="select_a_need_to_view_the_information"/>！');
            }
		}
		
		var win;
		function saveChkstoDemod(){
			win=$('body').window({
				id: 'window_saverole',
				title: '<fmt:message key="insert"/><fmt:message key="template"/>',
				content: '<iframe id="saveChkstoDemodFrame" frameborder="0" src="<%=path%>/chkstodemomMis/addChkstoDemod.do?tempTyp='+$("#tempTyp").val()+'"></iframe>',
				width: $(document.body).width()-20/* '1100px' */,
				height: $(document.body).height()-100,
				draggable: true,
				isModal: true
				
			});
			win.max();
		}
		
		function updateChkstoDemod(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			
			if(checkboxList 
					&& checkboxList.filter(':checked').size() == 1){
				var chkValue = checkboxList.filter(':checked').eq(0).val();
				if($("#typ"+chkValue).val() =='N'){
					alert("当前模版是总部定义，不可编辑!");
					return;
				}
				var curwin = $('body').window({
					title: '<fmt:message key="update"/><fmt:message key="template"/>',
					content: '<iframe id="updateChkstoDemodFrame" frameborder="0" src="<%=path%>/chkstodemomMis/updChkstoDemom.do?chkstodemono='+chkValue+'"></iframe>',
					width: $(document.body).width()-20/* '1000px' */,
					height: $(document.body).height()-100,
					draggable: true,
					isModal: true
				});
				curwin.max();
			}else{
				alert('<fmt:message key="select_a_need_to_update_the_information"/>！');
				return ;
			}
			
		}
		
	
		function deleteChkstoDemod(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			
			if(checkboxList 
					&& checkboxList.filter(':checked').size() > 0){
				if(confirm("<fmt:message key='Automatic_filtering_the_Central_definition_templete'/><fmt:message key='Are_you_sure_you_want_to_delete_the_template'/>?")){
					var chkValue = [];
					
					checkboxList.filter(':checked').each(function(){
						chkValue.push($(this).val());
					});
					
					$('body').window({
						title: '<fmt:message key="delete"/><fmt:message key="template"/>',
						content: '<iframe id="deleteChkstoDemodFrame" frameborder="0" src="<%=path%>/chkstodemomMis/deleteChkstoDemom.do?ids='+chkValue.join(",")+'"></iframe>',
						width: '300px',
						height: '200px',
						draggable: true,
						isModal: true
					});
				}
			}else{
				alert('<fmt:message key="please_select"/><fmt:message key="delete"/><fmt:message key="data"/>!');
				return ;
			}
		}
		//所属分店
		function toAccountFirm() {
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() < 1){
				alert('<fmt:message key="please_select"/><fmt:message key="report_form_template"/>！');
				return;
			}
			var chkValue = [];
			checkboxList.filter(':checked').each(function(){
				chkValue.push($(this).val());
			});
			path="<%=path%>/chkstodemomMis/listChkstoDemoFirm.do?chkstodemo="+chkValue.join(",")+"&single=false";
			customWindow = $('body').window({
				id: 'window_selectFirm',
				title: '<fmt:message key="scm_association_branch"/>',
				content: '<iframe id="accountFirmFrame" frameborder="0" src="'+path+'"></iframe>',
				width: 800,
				height: '500px',
				draggable: true,
				isModal: true,
				topBar: {
					items: [{
							text: '<fmt:message key="enter" />',
							title: '<fmt:message key="modify_supplier" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								if(getFrame('accountFirmFrame')&&window.document.getElementById("accountFirmFrame").contentWindow.select_Positn()){
// 									submitFrameForm('accountFirmFrame','listForm');
									window.document.getElementById("accountFirmFrame").contentWindow.$("#listForm").submit();
								}
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.close').click();
							}
						}
					]
				}
			});
		}
		function pageReload(){
			$('#sta').val("select");
			$("#listForm").submit();
	    }
		</script>
	</body>
</html>