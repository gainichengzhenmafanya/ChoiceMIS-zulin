<%@ page import="com.choice.framework.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="lo" uri="/WEB-INF/tld/local.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="module_operation_information"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<style type="text/css">
				#toolbar {
					position: relative;
					height: 27px;
				}
			</style>
		</head>
	<body>
	  <div class="grid">
		<div class="table-head" >
			<table cellspacing="0" cellpadding="0">
				<thead>
					<tr>
						<td class="num" style="width: 25px;">&nbsp;</td> 
						<td style="width:30px;">
							<input type="checkbox" id="chkAll"/>
						</td>
						<td style="width:160px;"><fmt:message key="module_name"/></td>
					</tr>
				</thead>
			</table>
		</div>
		<div class="table-body">
			<table cellspacing="0" cellpadding="0">
				<tbody>
				<c:forEach var="roleOperate" items="${roleOperateList}" varStatus="num">
				<tr id="col_<c:out value="${roleOperate.id}" />">
					<td class="num" style="width: 25px;"><c:out value="${num.index+1}" /></td>
					<td style="width:30px; text-align: center;">
						<input type="checkbox" id="chk_${roleOperate.id}" value="${roleOperate.id}"/>
					</td>
					<td style="width:160px;">
						<c:out value="${lo:show(roleOperate.name)}" />&nbsp;
					</td>
				</tr>
				</c:forEach>
				</tbody>
			</table>
		</div>
	  </div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			
			setElementHeight('.grid',['.tool'],$(document.body),0);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid',0);	//计算.table-body的高度
		 	loadGrid();//  自动计算滚动条的js方法
		 	//将用户已经选择的记录选中
			var tagIdList = '${tagIdList}';
			if(tagIdList!=''){
				var tagIds = tagIdList.split(',');
				for(var i in tagIds){
					$("#chk_"+tagIds[i]).attr("checked","checked");
				}	
			}
		});

		//全选多选框事件
		$('#chkAll').bind('change',function(){
			$(':checkbox').attr('checked',$(this).attr('checked'));
		});
	 	$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
	   	$('.grid').find('.table-body').find('tr').hover(
			function(){
				$(this).addClass('tr-over');
			},
			function(){
				$(this).removeClass('tr-over');
			}
		);
		//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
		$('.grid').find('.table-body').find('tr').live("click", function () {
		     if ($(this).hasClass("bgBlue")) {
		         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
		     }
		     else
		     {
		         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
		     }
		 });
		function saveUsedModel(){
			var checkboxList=$(':checkbox',$('.table-body'));
			if(checkboxList && checkboxList.filter(':checked').size() > 0 && checkboxList.filter(':checked').size() < 11){
				if(confirm('<fmt:message key="do_you_want_to_submit_data"/>？')){
					var chkValue = [];
					var selected = {};
					checkboxList.filter(':checked').each(function(){
						chkValue.push($(this).val());
					});
					selected['roleOperateId'] = chkValue.join(",");
					$.ajax({url: '<%=path%>/myDesk/saveUsedModel.do',
						type: "POST",
						async: false,
						data: selected,
						success: function(data){
							if(data == "yes"){
							 	//弹出提示信息
							 	alert('<fmt:message key="save_the_settings_successfully_relogin_system_effect"/>！');
							 	parent.$('.close').click();
							}else{
								alert('<fmt:message key="save_fail"/>！');
							}
						}
					});
				}
			}else if(checkboxList.filter(':checked').size() > 10){
				alert('最多只能选择十个模块！');
				return ;
			}else{
				alert('<fmt:message key="please_select_options_you_need_save"/>！');
				return ;
			}
		}
		</script>

	</body>
</html>