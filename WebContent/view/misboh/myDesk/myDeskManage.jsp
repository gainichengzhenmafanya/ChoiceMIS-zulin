<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>student Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/view/tableMain.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/easyui.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/icon.css" />
			<style type="text/css">
				.moduleHeader .title1 {
				    float: left;
				    font-weight: bold;
				    line-height: 16px;
				    min-width: 8%;
				    cursor: pointer;
				    margin-left:15px;
				}
				.grid {
					position : static;
				}
			</style>
		</head>
	<body >
		<div class="tool"></div>
		<div class="moduleHeader">
		<span class="title1" style="display: none;color:#666666" id="span_m_33_3" onclick="showDiv('m_33_3')"><fmt:message key="Daily_guide"/></span><!-- misboh用2015.6.8wqq -->
		<span class="title1" style="display: none;color:#666666" id="span_m_33_1" onclick="showDiv('m_33_1')"><fmt:message key="The_current_store_needs_to_report_today"/></span><!-- misboh用2015.6.3wjf -->
		<span class="title1" style="display: none;color:#666666" id="span_m_33_2" onclick="showDiv('m_33_2')"><fmt:message key="The_current_store_needs_to_inspect_today"/></span><!-- misboh用2015.6.3wjf -->
		<span class="title1" style="display: none;color:#666666" id="span_m_33_4" onclick="showDiv('m_33_4')"><fmt:message key="my_remind"/></span>
		<span class="title1" style="display: none;color:#666666" id="span_m_1_2" onclick="showDiv('m_1_2')"><fmt:message key="announcement_notification"/></span>
		</div>
		<!-- BS-BOH 门店BOH -->
		<div id="m_33_3" class="module" style="width:97%;display: none;height: 135px;">
		<h4 class="moduleHeader">
		    <span class="title" ><fmt:message key="Daily_guide"/></span>
		    <span class="close"><img src="<%=path%>/image/close.png" style="margin-right: 0px;" onclick="hideDiv('m_33_3')"></img></span>
		  </h4>
		</div>
		<div id="m_33_1" class="module" style="display: none;height: 135px;">
		<h4 class="moduleHeader">
		    <span class="title" ><fmt:message key="The_current_store_needs_to_report_today"/></span>
		    <span class="close"><img src="<%=path%>/image/close.png" style="margin-right: 0px;" onclick="hideDiv('m_33_1')"></img></span>
		  </h4>
		</div>
		<div id="m_33_2" class="module" style="display: none;height: 135px;">
		<h4 class="moduleHeader">
		    <span class="title" ><fmt:message key="The_current_store_needs_to_inspect_today"/></span>
		    <span class="close"><img src="<%=path%>/image/close.png" style="margin-right: 0px;" onclick="hideDiv('m_33_2')"></img></span>
		  </h4>
		</div>
		<div id="m_33_4" class="module" style="display: none;height: 135px;">
		<h4 class="moduleHeader">
		    <span class="title" ><fmt:message key="my_remind"/></span>
		    <span class="close"><img src="<%=path%>/image/close.png" style="margin-right: 0px;" onclick="hideDiv('m_33_4')"></img></span>
		  </h4>
		</div>
		<div id="m_1_2" class="module" style="display: none;">
		<h4 class="moduleHeader">
		    <span class="title" ><fmt:message key="announcement_notification"/></span>
		    <span class="close"><img src="<%=path%>/image/close.png" style="margin-right: 0px;" onclick="hideDiv('m_1_2')"></img></span>
		  </h4>
			<ul>
				<marquee  direction=up height=100px id=m onmouseout=m.start() onMouseOver=m.stop() scrollamount=2 >
					<c:forEach var="announce" items="${announcementList}" varStatus="status">
						<li> <a href="#" onclick="showAnnounce(<c:out value="${announce.id}" />)"><c:out value="${announce.title}" /></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<c:out value="${announce.create_time}" /></li>
					</c:forEach>
				</marquee>
			</ul>
		</div>
<!-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->			
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="save_the_settings"/>',
							title: '<fmt:message key="save_my_settings"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','0px']
							},
							handler: function(){
								saveConfig();
							}
						},{
							text: '<fmt:message key="save_the_commonly"/>',
							title: '<fmt:message key="save_my_commonly"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','0px']
							},
							handler: function(){
								chooseModels();
							}
						},{
							text: '<fmt:message key="quit"/>',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '/Choice/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}
					]
				});
  				$(".tool div").css("position","static");
  				$(".tool ul,li").css("position","static");
				$('.grid').find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
				);
				$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
			 	});
				//显示用户当前角色可以显示的菜单
				var roleMenu = '${roleMenu}';
				var roleArray = roleMenu.split(',');
				for(var i in roleArray){
					$("#span_"+roleArray[i]).show();	
				}
				//显示用户配置的菜单信息
				var menu = '${menu}';
				if(menu!=''){
					var menuArray = menu.split(',');
					for(var i in menuArray){
						$("#"+menuArray[i]).show();
						$("#span_"+menuArray[i])[0].style.color='blue';
					}	
				}
			});
			
			//保存设置
			function saveConfig(){
				var id='${id}';
				var s = $(".module");
				var divIds = "";
				for(var i=0;i<s.length;i++){
					if(s[i].style.display!="none"){
						divIds = divIds + s[i].id+","	;
					}
				}
				divIds = divIds.substring(0,divIds.length-1);
				var data = {};
				data['id']=id;
				data['menu']=divIds;
				$.post("<%=path%>/myDesk/saveWorkbench.do",data,function(data){
					alert('<fmt:message key="save_the_settings_successfully_relogin_system_effect"/>！');
				});	
			}
			//div隐藏
			function hideDiv(id){
				$("#"+id).slideUp();
				$("#span_"+id)[0].style.color='#666666';
			}
			//div显示
			function showDiv(id){
				var count = 0;
				$(".moduleHeader").find("span").each(function(){
					if($(this)[0].style.color=='blue'){
						count++;
					}
				});
				if($("#span_"+id)[0].style.color=='blue'){
					hideDiv(id);
					return;
				}
				if(count==10){
					alert("最多设置10个常用操作！");
					return;
				}
				$("#"+id).slideDown();
				$("#span_"+id)[0].style.color='blue';
			}	  
			function setCommonly(){
				
			}
			//选择常用操作(最多10个)
			function chooseModels(){
				$('body').window({
					id: 'window_saveDeliver',
					title: '<fmt:message key="save_the_commonly" />',
					content: '<iframe id="saveUsedFrame" frameborder="0" src="<%=path%>/myDesk/addUsedTag.do"></iframe>',
					width: '700px',
					height: '540px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								title: '<fmt:message key="save_my_commonly" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('saveUsedFrame')){
// 										window.frames["saveUsedFrame"].saveUsedModel();
										window.document.getElementById("saveUsedFrame").contentWindow.saveUsedModel();
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}]
					}
				});
			}
		</script>
	</body>
</html>