<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="Dishes_set" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<style type="text/css">
			.separator{
				display:none !important;
			}
 			.page{ 
				margin-bottom: 25px; 
 			} 
			.separator ,div , .pgSearchInfo{
				display: none;
			}
			div[class]{
				display:block;
			}
			.tr-select{
				background-color: #D2E9FF;
			}
			
			#wait4 { 
					filter:alpha(opacity=80);
					-moz-opacity:0.8;
					-khtml-opacity: 0.8;
					opacity: 0.8;
					background-color:black;
					width:100%;
					height:100%;
					position:absolute;
					z-index:998;
					top:0px;
					left:0px;
				}			
				#wait3 { 
					position:absolute; 
					top:40%; 
					left:40%; 
					margin-left:-80px; 
					margin-top:-30px; 
					z-index:999;
				}
		</style>
	</head>
	<body onload="addDate()">
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/costReduction/listCostReduction.do" method="post">
		<input type="hidden" id="sta" name="sta" value="${sta}" />
			<div class="search" style="margin-left: 0px;margin-bottom: 10px;">
				<div class="search-condition">
					<table class="search-table" cellspacing="0" cellpadding="0" >
						<tr>
							<td class="c-left"><fmt:message key="startdate" />：</td>
							<td><input type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${costItem.bdat }" pattern="yyyy-MM-dd"/>" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'})"/></td>
							<td style="width: 20px;"></td>
							<td class="c-left"><fmt:message key="enddate" />：</td>
							<td><input type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${costItem.edat }" pattern="yyyy-MM-dd"/>" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'})"/></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="grid" style="overflow:auto">
				<div class="table-head" style="overflow:visible;">
					<table cellspacing="0" cellpadding="0" id="headTable">
						<thead>
							<tr>
								<td class="num"><span style="width: 30px;"></span></td>
								<td><span style="width:100px;"><fmt:message key="Subtract" /><fmt:message key="date" /></span></td>
								<td><span style="width:80px;"><fmt:message key="pubitem_code" /></span></td>
								<td><span style="width:120px;"><fmt:message key="pubitem_name" /></span></td>
								<td><span style="width:60px;"><fmt:message key="unit" /></span></td>
								<td><span style="width:100px;"><fmt:message key="quantity" /></span></td>
								<td><span style="width:100px;"><fmt:message key="amount" /></span></td>
								<td><span style="width:120px;"><fmt:message key="sector" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body" style="overflow:visible;">
					<table cellspacing="0" cellpadding="0" class="datagrid">
						<tbody>
							<c:forEach var="firm" items="${listCostReduction}" varStatus="status">
								<tr>
									<td class="num"><span style="width: 30px;">${status.index+1}</span></td>
									<td><span style="width:100px;">${firm.dat}</span></td>
									<td><span style="width:80px;" title="${firm.item}">${firm.item}</span></td>
									<td><span style="width:120px;" title="${firm.des}">${firm.des}</span></td>
									<td><span style="width:60px;">${firm.unit}</span></td>
									<td><span style="width:100px;text-align: right;"><fmt:formatNumber value="${firm.cnt}" pattern="##.##" /></span></td>
									<td><span style="width:100px;text-align: right;"><fmt:formatNumber value="${firm.amt}" pattern="##.##" /></span></td>
									<td><span style="width:120px;">${firm.deptdes}</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>			
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<div id="wait4" style="display:none;"></div> 
		<div id="wait3" style="display:none;color:white;font-size:15px;"><img src="../image/loading_detail.gif" />&nbsp;
		<span id="costTime">系统正在进行计算，不要关闭核减界面！LOADING...</span><br/>
		<div id="p" class="easyui-progressbar" style="margin:0 auto;width: 400px;"></div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script><!-- 兼容浏览器ie 用1.7报错参数无效 -->
		 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
		 <script type="text/javascript" src="<%=path%>/js/boh/common/teleFunc-${sessionScope.locale}.js"></script>
		 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
		 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
		 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/boh/BoxSelect.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				var sta = $("#sta").val();
				if(sta !=null && sta == '0'){
					alert('<fmt:message key="Non_food_sale_data" />!');
				}else if(sta != null && sta == '1'){
					alert('<fmt:message key="Subtract" /><fmt:message key="successful" />！');
				}else if(sta != null && sta == '-1'){
					alert('<fmt:message key="The_current_cost_free_card_data_stores" />!');
				}else if(sta != null && sta == '-3'){
					alert('<fmt:message key="Anti_reduction_failed" />!');
				}else if(sta != null && sta == '2'){
					alert('<fmt:message key="Anti_reduction" /><fmt:message key="successful" />！');
				}
	 			$("#bdat").click(function(){
		  			new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});
		  		});
		  		$("#edat").click(function(){
		  			new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});
		  		});
		  		
				var tool = $('.tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							search();
						}
					},{
						text: '<fmt:message key="Subtract" />',
						title: '<fmt:message key="Subtract" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							subtract();
						}
					},{
						text: '<fmt:message key="Anti_reduction" />',
						title: '<fmt:message key="Anti_reduction" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							antiReduction();
						}
					},"-",{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}
					]
				});
					
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),67);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
			});

			//一进入便加载日期，如果日期为空则加载当前系统日期
			function addDate(){
				var bdat = $("#bdat").val();
				var edat = $("#edat").val();
				
				if (bdat == '') {
					$("#bdat").htmlUtils("setDate","now");
				}
				if (edat == '') {
					$("#edat").htmlUtils("setDate","now");
				}
			}
			
			//查询
			function search(){
				var bdat = $("#bdat").val();
				var edat = $("#edat").val();
				if ('' == bdat) {
					alert('<fmt:message key="startdate" /><fmt:message key="cannot_be_empty" />');
					return;
				}else if('' == edat){
					alert('<fmt:message key="enddate" /><fmt:message key="cannot_be_empty" />');
					return;
				}
				var a =  new Date(edat.replace(/-/g,"/")).getTime()- new Date(bdat.replace(/-/g,"/")).getTime();
				if(a/(24*60*60*1000)>30) {
					alert("<fmt:message key='select'/><fmt:message key='time'/><fmt:message key='can_not_be_greater_than'/>30<fmt:message key='day'/>!");
					return;
				}
				$("#listForm").submit();
			}
			var timerId;
			//核减
			function subtract(){
				var bdat = $("#bdat").val();
				var edat = $("#edat").val();
				if ('' == bdat) {
					alert('<fmt:message key="startdate" /><fmt:message key="cannot_be_empty" />');
					return;
				}else if('' == edat){
					alert('<fmt:message key="enddate" /><fmt:message key="cannot_be_empty" />');
					return;
				}
				var a =  new Date(edat.replace(/-/g,"/")).getTime()- new Date(bdat.replace(/-/g,"/")).getTime();
				if(a/(24*60*60*1000)>=7) {
					alert("<fmt:message key='time'/><fmt:message key='can_not_be_greater_than'/>7<fmt:message key='day'/>!");
					return;
				}
				if(confirm('<fmt:message key="whether" /><fmt:message key="Subtract" />?')){
					$('#wait1').show();
					$('#wait2').show();
					$.ajax({
						url:"<%=path%>/costReduction/checkCostReduction.do?bdat="+bdat+"&edat="+edat,
						type:"post",
						success:function(msg){
							$('#wait').hide();
							$('#wait2').hide();
							if(msg == '0'){
								$('#wait3').show();
								$('#wait4').show();
								timerId = window.setInterval(getForm,500);
								$.ajax({
									url:"<%=path%>/costReduction/subtract.do?bdat="+bdat+"&edat="+edat,
									type:"post",
									success:function(sta){
									window.clearInterval(timerId);
									$('#p').progressbar('setValue', 100);
									window.setTimeout(function(){									
										$('#wait3').hide();
										$('#wait4').hide();
										if(sta !=null && sta == '0'){
											alert('<fmt:message key="Non_food_sale_data" />!');
										}else if(sta != null && sta == '1'){
											alert('<fmt:message key="Subtract" /><fmt:message key="successful" />！');
											$("#listForm").submit();
										}else if(sta != null && sta == '-1'){
											alert('<fmt:message key="The_current_cost_free_card_data_stores" />!');
										}
									},200);
									}
								});
							}else{
								alert('当前门店在'+msg+'已做核减！请先进行反核减！');
							}
						}
					});
				}
			}
			
			function getForm(){
			//使用JQuery从后台获取JSON格式的数据
				$.ajax({
					type : "post",//请求方式
					url : "<%=path%>/costReduction/subtractTime.do",//发送请求地址
					timeout : 30000,//超时时间：30秒
					//请求成功后的回调函数 data为json格式
					success : function(data) {
						var msg = eval('('+data+')');
						if (msg.time >= 100) {
							window.clearInterval(timerId);
						}
						$('#p').progressbar('setValue', msg.time);
						$('#costTime').text(msg.des);
					},
					//请求出错的处理
					error : function() {
						window.clearInterval(timerId);
						alert("请求出错");
					}
				});
			}

			
			//反核减
			function antiReduction(){
				var bdat = $("#bdat").val();
				var edat = $("#edat").val();
				if ('' == bdat) {
					alert('<fmt:message key="startdate" /><fmt:message key="cannot_be_empty" />');
					return;
				}else if('' == edat){
					alert('<fmt:message key="enddate" /><fmt:message key="cannot_be_empty" />');
					return;
				}
				var a =  new Date(edat.replace(/-/g,"/")).getTime()- new Date(bdat.replace(/-/g,"/")).getTime();
				if(a/(24*60*60*1000)>=7) {
					alert("<fmt:message key='time'/><fmt:message key='can_not_be_greater_than'/>7<fmt:message key='day'/>!");
					return;
				}
				if(confirm('<fmt:message key="whether" /><fmt:message key="Anti_reduction" />?')){
					location.href="<%=path%>/costReduction/antiReduction.do?bdat="+bdat+"&edat="+edat;
					$('#wait').show();
					$('#wait2').show();
				}
			}
		</script>
	</body>
</html>