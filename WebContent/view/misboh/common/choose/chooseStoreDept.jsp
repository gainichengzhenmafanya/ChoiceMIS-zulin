<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
				.grid td span{
					padding:0px;
				}
				.form-line .form-input{
					width: 17%;
					margin-right: 0px;
					padding-left: 0px;
				}
				.form-line .form-input input,.form-line .form-input select{
					width:90%;
				}
			</style>
		</head>
	<body>
		<div id='toolbar'></div>
		<form id="queryForm" name="queryForm" method="post">
				<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }"/>
				<input type="hidden" id="pk_id" name="pk_id" value="${pk_id }"/>
				<div class="form-line">
					<div class="form-label" style="width:40px"><fmt:message key="coding" />：</div>
					<div class="form-input">
						<input id="vcode" name="vcode" class="text"/>
					</div>
					<div class="form-label" style="width:40px"><fmt:message key="name" />：</div>
					<div class="form-input">
						<input id="vname" name="vname" class="text"/>
					</div>
					<input type="button" id="filterquery" value="<fmt:message key="query" />"/>
				</div>
				
		</form>
		<div class="grid">
			<div class="table-head">
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<td class="num"><span style="width: 20px;"></span></td>
							<td style="width:30px;"><input type="checkbox" id="chkAll" />
							</td>
							<td><span style="width:80px;"><fmt:message key="department_number" /></span></td>
							<td><span style="width:150px;"><fmt:message key="department_name" /></span></td>
						</tr>
					</thead>
				</table>
			</div>
			<div class="table-body">
				<table cellspacing="0" cellpadding="0" class="datagrid">
					<tbody>
						<c:forEach var="storeDept" items="${storeDeptList}"
							varStatus="status">
							<tr>
								<td class="num"><span style="width: 20px;">${status.index+1}</span>
								</td>
								<td style="width:30px; text-align: center;"><span>
								<input type="checkbox" name="idList" id="<c:out value='${storeDept.pk_storedept}' />"
									value="<c:out value='${storeDept.pk_storedept}' />" /></span>
								</td>
								<td><span style="width:80px;">${storeDept.vcode}</span></td>
								<td><span style="width:150px;">${storeDept.vname}</span></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				$(document).keydown(function(e){
					if(e.keyCode == 13) $("#filterquery").trigger('click');
				});
				$("#toolbar").toolbar({
						items: [{
							text: '<fmt:message key="enter" />',
							useable:true,
							handler: function(){
								var checkboxList = $('.grid').find('.table-body').find(':checkbox');
								var data = {show:[],code:[],mod:[],entity:[]};
								checkboxList.filter(':checked').each(function(){
									//if($(this).attr('disabled'))return;
									var entity = {};
									var row = $(this).closest('tr');
									data.code.push($(this).val());
									entity.vcode = $.trim(row.children('td:eq(2)').text());
									entity.vname = $.trim(row.children('td:eq(3)').text());
									entity.pk_role = $.trim($(this).val());
									data.show.push(entity.vname);
									data.entity.push(entity);
								});
								if("${domId}" == "selected"){
									parent.selected = data.code;
								}
								parent['${callBack}'](data);
								$(".close",parent.document).click();
							}
						},{
							text: '<fmt:message key="cancel" />',
							useable: true,
							handler: function(){
								$(".close",parent.document).click();
							}
						}
					]
				});
				
				$("#filterquery").click(function(){
					var trlist=$(".table-body").find('tbody').find('tr');
					var bcode=$.trim($("#vcode").val());
					var bname=$.trim($("#vname").val());
					if(trlist.length>0){
						trlist.each(function(){
							var vcode=$(this).find('td span').eq(2).text();
							var vname=$(this).find('td span').eq(3).text();
							var bol=false;
							if(bcode!=""){
								if($.trim(vcode).indexOf(bcode)){
									bol=true;
								}
							}
							if(bname!=""){
								if($.trim(vname).indexOf(bname)){
									bol=true;
								}
							}
							if(bol){
								//一旦有不满足<fmt:message key="condition" />，即把这行隐藏,否则现在
								$(this).attr("style","display:none");
								$(this).find('input').attr("checked", false );
							}else{
								$(this).attr("style","display: table-row;");
							}
							
						});
					}
				});
				setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').live('mouseover',function(){
					$(this).addClass('tr-over');
				});
				$('.grid').find('.table-body').find('tr').live('mouseout',function(){
					$(this).removeClass('tr-over');
				});
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				var mod = '<c:out value="${single}"/>';
				if(mod){
					$('#chkAll').unbind('click');
					$('#chkAll').css('display','none');
				}
				$('.grid').find('.table-body').find('tr').live("click", function () {
					$(this).find(':checkbox').trigger('click');
				 });
				$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
					var mod = '<c:out value="${single}"/>';
					if(mod){
						$(this).closest('.table-body').find(':checkbox').not($(this)).removeAttr("checked");
					}
					event.stopPropagation();
				});
				if(typeof(parent.editTable) == 'function')
					parent.editTable($('.grid'));
				
				var depts=$('#pk_id').val().split(','); //获得已选的部门
				$(".table-body").find('tr td input[name="idList"]').each(function(){
					if($.inArray($(this).val(),depts) >= 0){
						$(this).attr('checked','checked');
					}
				});
			});
		</script>
	</body>
</html>