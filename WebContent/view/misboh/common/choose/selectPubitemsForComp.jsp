<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="pubitem" /><fmt:message key="select1" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
  		<link type="text/css" rel="stylesheet" href="<%=path%>/js/mis/chosen/chosen.css" />
		<style type="text/css">
				.userInfo,.accountInfo {
					position: relative;
					top: 1px;
					background-color: #E1E1E1;
				}
				
				.userInfo {
					height: 91px;
					line-height: 91px;
				}
				
				.accountInfo {
					height: 91px;
					line-height: 91px;
				}
				
				.accountInfo .form-label{
					width: 40%;
				}
				
			</style>
	</head>
	<body>
		<div id='toolbar'></div>
		<div class="form" style="text-align: center;width: 98%;height: 85%">
			<form id="pubitemForm" method="post" action="" style="height: 100%;">
				<input type="hidden" id="pk_pubitems" name="pk_pubitem"/>
				<input type="hidden" id="domId" name="domId" value="${domId }"/>
				<input type="hidden" id="isTc" name="isTc" value="${isTc }"/>
				<div class="form-line">
					<div class="form-label" style="width:80px;">编码/名称</div>
					<div class="form-input">
						<select id="vpcode" name="vpcode"  data-placeholder="<fmt:message key="select1" /><fmt:message key="pubitem" />..." class="chosen-select" style="width:190px;" tabindex="2" onchange="setchangeValue(this)">
							<option value=""></option>
							<c:forEach var="pubitem" items="${listPubitemInfo}" varStatus="status">
								<option value="${pubitem['PK_PUBITEM']}">${pubitem['VCODE']} -- ${pubitem['VNAME']} -- ${pubitem['VUNITNAME']}</option>
							</c:forEach>
						</select>
					</div>
				</div>
			
				<table cellspacing="0" cellpadding="0" style="height: 100%;">
					<tr>
						<td align="left" height="45%">
							<fieldset style="width: 155px;border-color: #B1C3D9; height: 170px;float: left ">
								<legend><fmt:message key="category" /><fmt:message key="list" /></legend>
								
								<div class="treePanel" style="width: 97%; height: 90%; overflow: auto;">
							        <script src="<%=path%>/js/tree/MzTreeView10.js" type="text/javascript"></script>
						        	<script type="text/javascript">
						          		var tree = new MzTreeView("tree");
						          
						          		tree.nodes['0_00000000000000000000000000000000'] = 'text:<fmt:message key="all" />;method:setSelectPubitemList("0","00000000000000000000000000000000")';
						          		<c:forEach var="pubitem" items="${listPubitemTree}" varStatus="status">
						          			tree.nodes["${pubitem['PARENTID']}_${pubitem['CHILDRENID']}"] 
							          		= "text:${pubitem['VNAME']}; method:setSelectPubitemList('${pubitem['ORDERID']}','${pubitem['CHILDRENID']}')";
						          		</c:forEach>
						          		 tree.setIconPath("<%=path%>/image/tree/none/");
						          		document.write(tree.toString());
						        	</script>
						    	</div>
						    	
							</fieldset>
							<fieldset style="width: 110px;border-color: #B1C3D9; height: 170px; ">
								<legend><fmt:message key="pubitem" /><fmt:message key="group_zu" /><fmt:message key="list" /></legend>
								
								<div class="treePanel" style="width: 97%; height: 90%; overflow: auto;">
						        	<script type="text/javascript">
						          		var treeTeam = new MzTreeView("treeTeam");
						          		treeTeam.nodes['0_1'] = 'text:<fmt:message key="all" />;method:setSelectPubitemTeamList("0","1")';
						          		<c:forEach var="pubitem1" items="${listPubitemTeam}" varStatus="status">
							          		treeTeam.nodes['1_${pubitem1.PK_PUBITEMTEAM}'] = "text:${pubitem1.VNAME}; method:setSelectPubitemTeamList('1','${pubitem1.PK_PUBITEMTEAM}')";
						          		</c:forEach>
						          		 treeTeam.setIconPath("<%=path%>/image/tree/none/");
						          		document.write(treeTeam.toString());
						        	</script>
						    	</div>
						    	
							</fieldset>
						</td>
						<td rowspan="2" align="center" valign="middle" style="width: 35px; height: 90%; " >
							<input type="button" value=">>" onclick="_rightAll()" />
							<br />
							<br />
							<br />
							<input type="button" value="&nbsp;>&nbsp;" onclick="_right()" />
							<br />
							<br />
							<br />
							<input type="button" value="&nbsp;&lt;&nbsp;" onclick="_left()" />
							<br />
							<br />
							<br />
							<input type="button" value="&lt;&lt;" onclick="_leftAll()" />
						</td>
						<td rowspan="2"  align="left" height="95%">
							<fieldset style="width: 275px;border-color: #B1C3D9; height: 100%; ">
								<legend><fmt:message key="selected" /><fmt:message key="pubitem" /></legend>
								<div style="width: 99%; height: 95.5%; overflow: auto;">
									<select class="yixuan"  style="width: 100%; height: 100%; overflow: auto;" multiple="multiple" id="selectedpubitem"  >
										<c:forEach var="pubitem" items="${listSelectedPubitem}" varStatus="status">
											<option value="${pubitem.pk_pubitem}">${pubitem.vcode} -- ${pubitem.vname}</option>
										</c:forEach>
									</select>
								</div>
							</fieldset>
						</td>
					</tr>
					<tr>
						<td  align="left" height="50%">
							<fieldset style="width: 275px;border-color: #B1C3D9; height:100%; ">
								<legend><fmt:message key="select1" /><fmt:message key="pubitem" /></legend>
								<div style="width: 97%; height: 90%; overflow: auto;">
									<select class="weixuan" style="width: 100%; height: 100%; overflow: auto;" multiple="multiple" id="selectpubitem" >
									</select>
								</div>
							</fieldset>
						</td>
					</tr>
				</table>
			</form>
		</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/chosen/chosen.jquery.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/chosen/prism.js" charset="utf-8"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				$("#toolbar").toolbar({
					items: [{
						text: '<fmt:message key="enter" />',
						useable:true,
						handler: function(){
							var data = {show:[],code:[],mod:[],entity:[]};
							$(".yixuan option").each(function(){
								var entity = {};
// 								if(jQuery.inArray($(this).val(),$("#domId").val().split(","))=='-1'){
									data.code.push($(this).val());
									data.show.push($.trim($(this).html().substring($(this).html().lastIndexOf(" -- ")+4)));
									data.mod.push($.trim($(this).html().substring(0,$(this).html().indexOf(" -- "))));
									entity.pk_store = $.trim($(this).val());
									var arrItem = $(this).html().split(" -- ");
									entity.vcode = $.trim(arrItem[0]);
									entity.vname = $.trim(arrItem[1]);
									entity.vunitname = $.trim(arrItem[2]);
									data.entity.push(entity);
// 								}
							});
			 				parent['${callBack}'](data);
			 				$(".close",parent.document).click();
						}
					},{
						text: '<fmt:message key="cancel" />',
						useable: true,
						handler: function(){
							$(".close",parent.document).click();
						}
					}
				]
			});
				
				//<fmt:message key="control" /><fmt:message key="store" /><fmt:message key="select1" />框
				var config = {
			      '.chosen-select'           : {},
			      '.chosen-select-deselect'  : {allow_single_deselect:true},
			      '.chosen-select-no-single' : {disable_search_threshold:10},
			      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
			      '.chosen-select-width'     : {width:"95%"}
			    };
			    for (var selector in config) {
			      $(selector).chosen(config[selector]);
			    }
			});
			
			var _nodetypes="";
			var _nodeid="";
			function setSelectPubitemList(nodetypes,nodeid) {
				_nodetypes=nodetypes;
				_nodeid=nodeid;
				$("#selectpubitem").empty();
				<c:forEach var="pubitem" items="${listPubitemInfo}" varStatus="status">;
					if(nodetypes == "0"){
						$("#selectpubitem").append("<option value='${pubitem['PK_PUBITEM']}'>${pubitem['VCODE']} -- ${pubitem['VNAME']} -- ${pubitem['VUNITNAME']}</option>");
					} else if (nodetypes == "1") {
						if (nodeid == "${pubitem['VGRPTYP']}") {
							$("#selectpubitem").append("<option value='${pubitem['PK_PUBITEM']}'>${pubitem['VCODE']} -- ${pubitem['VNAME']} -- ${pubitem['VUNITNAME']}</option>");
						}
					} else if (nodetypes == "2") {
						if (nodeid == "${pubitem['VGRP']}") {
							$("#selectpubitem").append("<option value='${pubitem['PK_PUBITEM']}'>${pubitem['VCODE']} -- ${pubitem['VNAME']} -- ${pubitem['VUNITNAME']}</option>");
						}
					} else if (nodetypes == "3") {
						if (nodeid == "${pubitem['VTYP']}") {
							$("#selectpubitem").append("<option value='${pubitem['PK_PUBITEM']}'>${pubitem['VCODE']} -- ${pubitem['VNAME']} -- ${pubitem['VUNITNAME']}</option>");
						}
					}
				</c:forEach>;
			}
			function setSelectPubitemTeamList(nodetypes,nodeid) {
 				$.ajaxSetup({async:false});
				$.post("<%=path %>/misbohcommon/listTeamDetail.do?pk_pubitemTeam="+nodeid+"&vmemo="+$("#isTc").val(),function(data){
					//1、取出原来选择门店中含有的数据
					var array = new Array(); //定义数组
					
					//1、从此处进行清空，重新加载  选中区域列表中对应的数据
					$("#selectpubitem").empty();
					setSelectPubitemList(_nodetypes,_nodeid);
					//2、再获取选择门店中的数据
				   $("#selectpubitem option").each(function(){ //遍历全部option
				        var txt = $(this).val(); //获取option的内容
				        array.push(txt); //添加到数组中
				    });
				   $("#selectpubitem").empty();
					if(array.length==0){
						for(var i in data){
							$("#selectpubitem").append("<option value='"+data[i]['pk_pubitem']+"'>"+data[i]['pubitemCode']+" -- "+data[i]['pubitemName']+" -- "+data[i]['vunitName']+"</option>");
						}
					}else{
						//2、对新添加的数据进行比较，假如含有，则不往里面添加，没有  则添加
						for(var i in data){
							for(var j=0;j<array.length;j++){
								if(data[i]['pk_pubitem']==array[j]){
									$("#selectpubitem").append("<option value='"+data[i]['pk_pubitem']+"'>"+data[i]['pubitemCode']+" -- "+data[i]['pubitemName']+" -- "+data[i]['vunitName']+"</option>");
									break;
								}
							}
							
						}
						
					}
				});
			}
			$('#selectpubitem').bind('dblclick',function(){
				_right();
			});
			function _rightAll() {
				var v='';
				$(".weixuan option").each(function(){
					var b=true;
					var r=$(this);
					$(".yixuan option").each(function(){
						if(r.val()==$(this).val()){
							b=false;
							return false;
						}
					});
					if(b){
						v+= '<option value="'+$(this).val()+'" >'+$(this).html()+'</option>';
					}
				});
				$('.yixuan').append(v);
			}
			
			function _right() {
				var v='';
				$(".weixuan option:selected").each(function(){
					var b=true;
					var r=$(this);
					$(".yixuan option").each(function(){
						if(r.val()==$(this).val()){
							b=false;
							return false;
						}
					});
					if(b){
						v+= '<option value="'+$(this).val()+'" >'+$(this).html()+'</option>';
					}
				});
				$('.yixuan').append(v);		
			}
						
			function _left() {
				var v='';
				$(".yixuan option:selected").each(function(){
					var b=true;
					var r=$(this);
					$(".weixuan option").each(function(){
						if(r.val()==$(this).val()){
							b=false;
							return false;
						}
					});
					if(b){
						v+= '<option value="'+$(this).val()+'" >'+$(this).html()+'</option>';
					}
				});
				$(".yixuan option:selected").each(function(){
					$(this).remove();
				});
			}
			
			function _leftAll() {
				$(".yixuan").empty();
			}
			
			//返回<fmt:message key="selected" /><fmt:message key="list" />长度
			function reutrnValue() {
				var chkValue = [];
				$(".yixuan option").each(function(){
					var r=$(this);
					chkValue.push($(this).val());
				});
				$("#pk_stores").val(chkValue);
				return chkValue.length;
			}
			
			//返回<fmt:message key="selected" /><fmt:message key="list" /><fmt:message key="store" /><fmt:message key="coding" />--<fmt:message key="name" />
			function reutrnChkValue() {
				var chkValue = [];
				$(".yixuan option").each(function(){
					var r=$(this);
					chkValue.push($(this).html());
				});
// 				$("#pk_stores").val(chkValue);
				return chkValue;
			}
			
			function setchangeValue() {
				var v='';
				$("#vpcode option:selected").each(function(){
					var b=true;
					var r=$(this);
					$(".yixuan option").each(function(){
						if(r.val()==$(this).val()){
							b=false;
							return false;
						}
					});
					if(b){
						v+= '<option value="'+$(this).val()+'" >'+$(this).html()+'</option>';
					}
				});
				$('.yixuan').append(v);
			}
			//返回<fmt:message key="store" /><fmt:message key="main" />键、<fmt:message key="coding" />、<fmt:message key="name" /><fmt:message key="information" />
			function returnPubitemList(){
				var data = {show:[],code:[],mod:[],entity:[]};
				$(".yixuan option").each(function(){
					var entity = {};
					entity.pk_pubitem = $.trim($(this).val());
					entity.vcode = $.trim($(this).html().substring(0,$(this).html().indexOf(" ")));
					alerterror($.trim($(this).html().substring(0,$(this).html().indexOf(" "))));
					entity.vname = $.trim($(this).html().substring($(this).html().lastIndexOf(" ")+1));
					alerterror($.trim($(this).html().substring($(this).html().lastIndexOf(" ")+1)));
					data.entity.push(entity);
					return ;
				});
			}
		</script>
	</body>
</html>