<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
				.grid td span{
					padding:0px;
				}
				.form-line .form-input{
					width: 17%;
					margin-right: 0px;
					padding-left: 0px;
				}
				.form-line .form-input input,.form-line .form-input select{
					width:90%;
				}
			</style>
		</head>
	<body>
		<div id='toolbar'></div>
		<input type="hidden" id="parentName"/>
		<input type="hidden" id="parentId"/>
		<form id="listForm">
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width: 25px;">&nbsp;</span></td>
								<td>
									<span style="width:30px;"><input type="checkbox" id="chkAll"/></span>
								</td>
								<td><span style="width:80px;"><fmt:message key="coding" /></span></td>
								<td><span style="width:110px;"><fmt:message key="name" /> </span></td>
								<td><span style="width:190px;"><fmt:message key="abbreviation" /> </span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="storeOperator" items="${listStoreOperator}" varStatus="status">
								<tr>
									<td class="num"><span style="width: 25px;">${status.index+1}</span></td>
									<td>
										<span style="width:30px; text-align: center;">
										<input type="checkbox"  name="idList" 
										<c:if test="${domId==storeOperator.vcode}"> checked="checked"</c:if>
										id="chk_<c:out value='${storeOperator.pk_operator}' />" value="<c:out value='${storeOperator.pk_operator}' />"/></span>
									</td>
									<td><span style="width:80px;"><c:out value='${storeOperator.vcode}' />&nbsp;</span></td>
									<td><span style="width:110px;"><c:out value='${storeOperator.vname}' />&nbsp;</span></td>
									<td><span style="width:90px;"><c:out value='${storeOperator.vinit}' />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }" />
			<input type="hidden" id="callBack" name="callBack" value="${callBack }" />
			<input type="hidden" id="domId" name="domId" value="${domId }" />
			<input type="hidden" id="single" name="single" value="${single }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/tele/teleUtil.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		
		<script type="text/javascript">
			var selected;
			$(document).ready(function(){
				$(document).keydown(function(e){
					if(e.keyCode == 13) return false;
				});
// 				selected = '${domId}' == 'selected' ? parent.selected : (typeof(parent['${domId}']) == 'function' ? parent['${domId}']() : $('#${domId}',parent.document).val().split(","));
				$("#toolbar").toolbar({
					items: [{
							text: '<fmt:message key="enter" />',
							useable:true,
							handler: function(){
								var checkboxList = $('.grid').find('.table-body').find(':checkbox');
								var data = {pk:[],code:[],name:[],mod:[],entity:[],init:[]};
								checkboxList.filter(':checked').each(function(){
									var row = $(this).closest('tr');
									data.pk.push($(this).val());
									data.code.push($.trim(row.children('td:eq(2)').text()));
									data.name.push($.trim(row.children('td:eq(3)').text()));
									data.init.push($.trim(row.children('td:eq(4)').text()));
								});
								if("${domId}" == "selected"){
									parent.selected = data.code;
								}
								parent['${callBack}'](data);
								$(".close",parent.document).click();
							}
						},{
							text: '<fmt:message key="cancel" />',
							useable: true,
							handler: function(){
								$(".close",parent.document).click();
							}
						}
					]
				});
				$("#area,#modid,#region").change(function(){
					delay.apply(this,[1,fillData]);
				});
				$('#init').keyup(function(e){
					delay.apply(this,[1,fillData]);
				});
				if(selected){
					$(".table-body").find('tr td input').each(function(){
						if($.inArray($(this).val(),selected) >= 0){
							$(this).attr('checked','checked');
						}
					});
					$(".table-body").find('tr').find('td:eq(2)').find('span').each(function(){
						var a = $(".table-body").find('tr').find('td:eq(2)').find('span').length;
						if($.inArray($.trim($(this).text()),selected) >= 0){
							$(this).closest('tr').find(':checkbox').attr('checked','checked');
						}
					});
				}
				setElementHeight('.grid',['.tool'],$(document.body),30);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').live('mouseover',function(){
					$(this).addClass('tr-over');
				});
				$('.grid').find('.table-body').find('tr').live('mouseout',function(){
					$(this).removeClass('tr-over');
				});
				//当点击tr行的时候,tr行头的checkbox也能被<fmt:message key="selected" />,不用非得点击checkbox才能<fmt:message key="selected" />行
 				var mod = '<c:out value="${single}"/>';
				if(mod){
					$('#chkAll').unbind('click');
					$('#chkAll').css('display','none');
				}else{
					$('#chkAll').click(function(){
					if($(this).get(0).checked){
						$(this).closest('.grid').find('.table-body').find('tr').find(':checkbox').attr('checked','checked');
					}else{
						$(this).closest('.grid').find('.table-body').find('tr').find(':checkbox').removeAttr('checked');
					}
					});
				}
				$('.grid').find('.table-body').find('tr').live("click", function () {
					$(this).find(':checkbox').trigger('click');
				 });
				$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
					var mod = '<c:out value="${single}"/>';
					if(mod){
						$(this).closest('.table-body').find(':checkbox').not($(this)).removeAttr("checked");
						//$(this).attr('checked','checked');
					}
					event.stopPropagation();
				});
				if(typeof(parent.editTable) == 'function')
					parent.editTable($('.grid'));
			});
			
			function fillData(){
				$(".table-body").teleUtil('autoFill',{
					url:'<%=path%>/store/findFirm.do',
					param:getParam($('#queryForm')),
					cols:['FIRMID','FIRMDES','AREA','MODNAM'],
					id:'FIRMID',
					selected:selected
				});
			}
		</script>
	</body>
</html>