<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="vendor_defined" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<style type="text/css">
			.page{margin-bottom: 0px;}
			.table-head td span{
				white-space: normal;
			}
		</style>
	</head>
	<body>
		<div class="tool"></div>		
		<form id="listForm" action="<%=path%>/misbohcommon/deliverList2.do?typCode=${typCode}" method="post">
		<input type="hidden" id="gysqx" name="gysqx" value="${deliver.gysqx }"/>
		<table class="searchDiv" cellspacing="0" cellpadding="0" style="background-color:#EEE;" >
			<tr>
				<td>&nbsp;
		        	<font style="font-size:2.2ex;"><b><fmt:message key="coding" />:</b></font>
		            <input type="text" id="code" name="code" style="width:80px;" class="text" onkeydown="javascript: if(event.keyCode==13){$('#search').click();}" value="<c:out value="${deliver.code}" />"/>
		        </td>
		        <td>&nbsp;
			        	<font style="font-size:2.2ex;"><b><fmt:message key="name" />:</b></font>
			            <input type="text" id="des" name="des" class="text" style="width:80px;" onkeydown="javascript: if(event.keyCode==13){$('#search').click();}" value="<c:out value="${deliver.des}" />"/>
			        </td>
		        <td>&nbsp;
		        	<font style="font-size:2.2ex;"><b><fmt:message key="abbreviation" />:</b></font>
		            <input type="text" id="init" name="init" class="text" style="width:80px;" onkeydown="javascript: if(event.keyCode==13){$('#search').click();}" value="<c:out value="${deliver.init}" />"/>
		        </td>
		        <td>&nbsp;
		        	<input type="button" style="width:60px" id="search" name="search" value='<fmt:message key="select" />'/>
		        </td>
			</tr>
		</table>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width:25px;">&nbsp;</span></td>
<!-- 								<td><span style="width:20px;"><input type="checkbox" id="chkAll"/></span></td> -->
								<td><span style="width:20px;"></span></td>
								<!--  <td><span style="width:60px;"><fmt:message key="suppliers_typ" /></span></td>-->
								<td><span style="width:50px;"><fmt:message key="coding" /></span></td>
								<td><span style="width:180px;"><fmt:message key="name" /></span></td>
								<td><span style="width:120px;"><fmt:message key="abbreviation_code" /></span></td>
								<td><span style="width:50px;"><fmt:message key="contact" /></span></td>
								<td><span style="width:90px;"><fmt:message key="tel" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="deliver" items="${deliverList}" varStatus="status">
								<tr>
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td><span style="width:20px;text-align: center;">
										<input type="checkbox" name="idList" id="chk_${deliver.code}" value="${deliver.code}"/></span>
									</td>
									<!-- <td><span title="${deliver.typ}" style="width:60px;">${deliver.typ}</span></td>-->
									<td><span title="${deliver.code}" style="width:50px;text-align: center;">${deliver.code}</span></td>
									<td><span title="${deliver.des}"  style="width:180px;">${deliver.des}</span></td>
									<td><span title="${deliver.init}" style="width:120px;">${deliver.init}</span></td>
									<td><span title="${deliver.person}" style="width:50px;">${deliver.person}</span></td>
									<td><span title="${deliver.tel}" style="width:90px;">${deliver.tel}</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>			
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />			
			
		</form>	
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			function ajaxSearch(){
				if (event.keyCode == 13){	
					$('.search-div').hide();
					$('#listForm').submit();
				} 
			}
			$(document).ready(function(){
				//typ是否有值，有，默认选中
				f_typ();
				//按钮快捷键
				focus() ;//页面获得焦点
				$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
				});
		
				//自动实现滚动条
				setElementHeight('.grid',['.tool','.searchDiv'],$(document.body),10);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				setElementHeight('.table-body',['.table-head'],'.grid');
	
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$('.grid .table-body tr').live('dblclick',function(){
					$(this).find(':checkbox').attr("checked", true);
					select_Deliver();
				});
				 $('.tool').toolbar({
						items: [{
								text: '<fmt:message key="enter" />',
								title: '<fmt:message key="enter" />',
								//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-120px','0px']
								},
								handler: function(){
									select_Deliver();
									//alert($('#parentId').val());
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-38px','0px']
								},
								handler: function(){
// 									parent.$('.close').click();
									top.closeCustom();
								}
							}]
					});
				
				//单击每行选中前面的checkbox
				$('.grid').find('.table-body').find('tr').live("click", function () {
					if($(this).find(':checkbox')[0].checked){
						$(":checkbox").attr("checked", false);
					}else{
						$(":checkbox").attr("checked", false);
						$(this).find(':checkbox').attr("checked", true);
					}
				 });
				
				
				//禁用checkbox本身的事件
				$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
					event.stopPropagation();
					if(this.checked){
						$(this).attr("checked",false);	
					}else{
						$(this).attr("checked",true);
					}
					$(this).closest('tr').click();
				});
				//---------------------------
				
				var t=$("#init").val();
				$("#init").val("").focus().val(t);
				
				//让之前选中的默认选中
				var defaultCode = parent.$('#defaultCode').val();
				var defaultName = parent.$('#defaultName').val();
				if(defaultCode!=''){
					$('.grid').find('.table-body').find(':checkbox').each(function(){
						if(this.id.substr(4,this.id.length)==defaultCode){
							$(this).attr("checked", true);
							$('#parentId').val(defaultCode);
							$('#parentName').val(defaultName);
						}
					});	
				}
			});	
			
			function select_Deliver(){
				if($('.grid').find('.table-body').find(':checkbox:checked').size()>0){
					$('.grid').find('.table-body').find(':checkbox:checked').each(function(){
						parent.$('#parentId').val($(this).closest('tr').find('td:eq(2)').find('span').attr('title'));
						parent.$('#parentName').val($(this).closest('tr').find('td:eq(3)').find('span').attr('title'));
					});
				}else{
					parent.$('#parentId').val('');
					parent.$('#parentName').val('');
					//alert("请选择一条数据");
					//return;
				}
				top.customWindow.afterCloseHandler('Y');
				top.closeCustom();
			}
			
			//判断让typ有值的select的option项选中
		   function f_typ(){
			   var typ=$("#typ1").val();
			   $("select[@name=typ] option").each(function(){
					if($(this).val() == typ){
						$(this).attr("selected", true);
					}
			   });
		   }
			//按条件查询供应商
		   $('#search').bind("click",function search(){
			 	$('#listForm').submit();
			});
		</script>
	</body>
</html>