<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>统配按单据验收</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>	
		<style type="text/css">
			.page{margin-bottom: 25px;}
			.ico-clear {
			    background-position: 0px 0px;
			    background-image: url("<%=path%>/image/Window/operator.gif");
	    	    background-repeat: no-repeat;
	    	    cursor: pointer;
	    	    width: 15px;
	    	    height: 15px;
			}
			.textInput span{
				padding:0px;
			}
			.textInput input{
				border:0px;
				background: #F1F1F1;
			}
			a.l-btn-plain{
				border:1px solid #7eabcd; 
				height:22px;
			}
			.search{
				margin-top:-2px;
				cursor: pointer;
			}
			.redspan{
				color:red;
			}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<input type="hidden" id="maded1" class="maded1" value="<fmt:formatDate value="${supplyAcct.dat }" pattern="yyyy-MM-dd" type="date"/>"/>
		<input type="hidden" id="ynUseDept" value="${ynUseDept }"/>
		<input type="hidden" id="chkyh" value="${supplyAcct.chkyh }"/>
		<input type="hidden" id="pr" value="${supplyAcct.deal }"/>
		<input type="hidden" id="ynTh"/><!-- 是否退货 -->
		<form id="listForm" action="<%=path%>/inspectionOut/tableSupplyAcctTP.do" method="post">
			<input type="hidden" id="chkno" name="chkno" value="${supplyAcct.chkno }"/>
			<input type="hidden" id="firmcode" name="firmcode" value="${supplyAcct.firm }"/>
			<input type="hidden" id="dat" name="dat" value="<fmt:formatDate value="${supplyAcct.dat }" pattern="yyyy-MM-dd" type="date"/>"/>
			<div class="form-line">	
				<div class="form-label">单号:</div>
				<div class="form-input">
					${supplyAcct.chkno }
				</div>
				<div class="form-label">凭证号:</div>
				<div class="form-input">
					${supplyAcct.vouno }
				</div>
				<div class="form-label"><span style="color:red;">入库日期:</span></div>
				<div class="form-input">
					<input type="text" id="maded" class="Wdate text" value="<fmt:formatDate value="${supplyAcct.maded }" pattern="yyyy-MM-dd" type="date"/>" onfocus="new WdatePicker()"/>
				</div>
			</div>
			<div class="form-line">	
				<div class="form-label">单据日期:</div>
				<div class="form-input">
					<fmt:formatDate value="${supplyAcct.dat }" pattern="yyyy-MM-dd" type="date"/>
				</div>
				<c:choose>
					<c:when test="${supplyAcct.chkinno > 0}">
						<div class="form-label"></div>
						<div class="form-input"></div>
						<div class="form-label"><span style="color:red;">入库单号:</span></div>
						<div class="form-input">
							${supplyAcct.chkinno}
						</div>
					</c:when>
					<c:otherwise>
						<div class="form-label"></div>
						<div class="form-input"><span style="font-weight: bold;">您有    <span id="changeNum" style="color: red;">0</span>条数据未保存</span></div>
					</c:otherwise>
				</c:choose>
			</div>
			<div class="grid">		
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num" ><span style="width: 25px;">&nbsp;</span></td>
								<td colspan="3"><fmt:message key="supplies"/></td>
								<td rowspan="2"><span style="width:80px;">到货<fmt:message key="quantity"/></span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="inspection"/><fmt:message key="quantity"/></span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="difference"/><fmt:message key="quantity"/></span></td>
								<td rowspan="2"><span style="width:40px;"><fmt:message key="standard_unit_br"/></span></td>
								<td rowspan="2"><span style="width:40px;"><fmt:message key="unit_price"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="amount"/></span></td>
								<td rowspan="2"><span style="width:100px;"><fmt:message key="remark"/></span></td>
							</tr>
							<tr>
								<td><span style="width:65px;"><fmt:message key="supplies_code"/></span></td>
								<td><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
								<td><span style="width:80px;"><fmt:message key="specification"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>		
							<c:forEach var="dis" items="${disList }" varStatus="status">
								<tr data-unitper="${dis.supply.unitper }" data-id="${dis.id }" data-chkstono="${dis.chkstoNo }" data-isdept="${dis.isDept }" <c:if test="${dis.cntfirm == 0}">class="nochange"</c:if>>
									<td class="num"><span style="width: 25px;">${status.index+1}</span></td>
									<td><span title="${dis.sp_code }" style="width:65px;">${dis.sp_code }</span></td>
									<td><span title="${dis.sp_name }" style="width:100px;">${dis.sp_name }</span></td>
									<td><span title="${dis.spdesc }" style="width:80px;">${dis.spdesc }</span></td>
									<td><span title="<fmt:formatNumber value="${dis.cntout}" type="currency" pattern="0.00"/>" style="width:80px;text-align:right"><fmt:formatNumber value="${dis.cntout}" type="currency" pattern="0.00"/></span></td>
									<td class="textInput">
										<span style="width:80px;">
											<input title="<fmt:formatNumber value="${dis.cntfirm}" type="currency" pattern="0.00"/>" type="text" class="nextclass" style="text-align: right;width: 80px;" 
												value="<fmt:formatNumber value="${dis.cntfirm}" type="currency" pattern="0.00"/>" id="amountin_${dis.id }" onfocus="this.select()" onblur="saveAmountin(this,'${dis.id}','${dis.supply.unitper }')" onkeyup="validate(this)"/>
										</span>
									</td>
									<td><span title="<fmt:formatNumber value="${dis.cntfirm - dis.cntout}" type="currency" pattern="0.00"/>" style="width:80px;text-align:right;" <c:if test="${(dis.cntfirm - dis.cntout) < 0}">class="redspan"</c:if>><fmt:formatNumber value="${dis.cntfirm - dis.cntout}" type="currency" pattern="0.00"/></span></td>
									<td><span title="${dis.unit }" style="width:40px;">${dis.unit }</span></td>
									<td><span style="width:40px;text-align:right;" title="<fmt:formatNumber value="${dis.pricesale}" type="currency" pattern="0.00"/>"><fmt:formatNumber value="${dis.pricesale}" type="currency" pattern="0.00"/></span></td>
									<td><span style="width:60px;text-align:right" title="<fmt:formatNumber value="${dis.amount}" type="currency" pattern="0.00"/>"><fmt:formatNumber value="${dis.amount}" type="currency" pattern="0.00"/></span></td>
									<td><span title="${dis.des }" style="width:100px;">${dis.des}</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript">
		//生成入库单时的状态
	   	chkMsg();
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		$(document).ready(function(){
			//自动实现滚动条 				
			setElementHeight('.grid',['.tool'],$(document.body),130);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			
			//判断是否有小于0的，则所有都是退货
			$('.grid').find('.table-body').find('tr').each(function(){
				if(Number($(this).find('td:eq(4)').text()) < 0){
					$('#ynTh').val('Y');
					return false;
				}
			});
			
			$('.textInput').find('input').live('click',function(event){
				var self = this;
				setTimeout(function(){
					self.select();
				},10);
			});
			$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
			});
			//编辑到货数量和单价时，按回车可以跳到下一行的同一列
			$('tbody .nextclass').live('keydown',function(e){
				if(parent.bhysEditState=="edit"){//判断如果是编辑状态
			 		if(e.keyCode==13){
			 			var lie = $(this).parent().parent().prevAll().length;
						var hang= $(this).parent().parent().parent().prevAll().length + 1;
						$('tbody').find('tr:eq('+hang+')').find('td:eq('+lie+')').find('span').find('input').focus();
						if(hang == $('.table-body').find('tr').length){
							$('tbody').find('tr:eq(0)').find('td:eq('+lie+')').find('span').find('input').focus();
						}
			 		}
				}
			});
			
			//判断如果不是编辑状态
			if(parent.bhysEditState!='edit'){
				$('tbody input[type="text"]').attr('disabled',true);//不可编辑
				if($('#chkyh').val() == 'N'){
					loadToolBar([true,true,false,true]);
				}else{
					loadToolBar2();
				}
			}else{
				$('tbody input[type="text"]').attr('disabled',false);
				new tabTableInput("table-body","text"); //input  上下左右移动
				loadToolBar([true,false,true,true]);
				if($('tbody tr:first .nextclass').length!=0){
					$('tbody tr:first .nextclass')[0].focus();//如果是编辑状态下查询，将焦点定位到第一行的采购数量列
				}
			}
		});
		//控制按钮显示
		function loadToolBar(use){
			$('.tool').html('');						
			$('.tool').toolbar({
				items: [{
					text: '<fmt:message key="select" />',
					title: '<fmt:message key="select"/>',
					useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						$("#listForm").submit();
					}
				},{
					text: '<fmt:message key="edit" />',
					title: '<fmt:message key="edit"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[1],
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-20px','0px']
					},
					handler: function(){
						if($('.grid').find('.table-body').find("tr").size()<1){
							alert('<fmt:message key="data_empty_edit_invalid"/>！！');
							return;
						}else{
							parent.bhysEditState='edit';
							loadToolBar([true,false,true,false]);
							$('tbody input[type="text"]').attr('disabled',false);
							new tabTableInput("table-body","text"); //input  上下左右移动	
							if($('tbody tr:first .nextclass').length!=0){
								$('tbody tr:first .nextclass')[0].focus();//如果是编辑状态下查询，将焦点定位到第一行的采购数量列
							}
						}									
					}
				},{
					text: '<fmt:message key="save" />',
					title: '<fmt:message key="save"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[2],
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-80px','-0px']
					},
					handler: function(){
						updateDis();
					}
				},{
					text: '<fmt:message key="acceptance_to_storage"/>',
					title: '<fmt:message key="acceptance_to_storage"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')}&&use[3],
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-60px','-20px']
					},
					handler: function(){
						updateMaded();
					}
				},{
					text: '<fmt:message key="scm_ps_yh" />',
					title: '<fmt:message key="scm_ps_yh"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[2],
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-80px','-0px']
					},
					handler: function(){
						peiSongToyanHuo();
					}
				},{
// 					text: '<fmt:message key="print_acceptance_single"/>',
// 					title: '<fmt:message key="print_acceptance_single"/>',
// 					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
// 					icon: {
<%-- 						url: '<%=path%>/image/Button/op_owner.gif', --%>
// 						position: ['-140px','-100px']
// 					},
// 					handler: function(){
// 						printDis();
// 					}
// 				},{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						if(parent.bhysEditState=="edit"){
							if(confirm('<fmt:message key="data_unsaved_whether_to_exit"/>?')){
								parent.pageReload();
								parent.$('.close').click();
							}
						}else{
							parent.pageReload();
							parent.$('.close').click();
						}
					}
				}]
			});
		}
		
		//控制按钮显示
		function loadToolBar2(){
			$('.tool').html('');						
			$('.tool').toolbar({
				items: [{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						if(parent.bhysEditState=="edit"){
							if(confirm('<fmt:message key="data_unsaved_whether_to_exit"/>?')){
								parent.pageReload();
								parent.$('.close').click();
							}
						}else{
							parent.pageReload();
							parent.$('.close').click();
						}
					}
				}]
			});
		}
		//修改报货
		function updateDis(){
			//如果修改过数据就保存 
			if(parent.ysTrList){
				var predata = parent.ysTrList;
				var disList = {};
				var num=0;
				var chkValue = [];
				for(var i in predata){
					disList['SupplyAcctList['+num+'].id']=i;
					chkValue.push(i);
					undefined==predata[i].cntfirm?'':disList['SupplyAcctList['+num+'].cntfirm']=predata[i].cntfirm;
					undefined==predata[i].cntfirm1?'':disList['SupplyAcctList['+num+'].cntfirm1']=predata[i].cntfirm1;
					num++;
				}
				var ids = chkValue.join(',');
				$.ajax({
					url : '<%=path%>/inspectionOut/updateAcct.do?ids='+ids,
					type : 'post',
					data : disList,
					success : function(msg){
						if(msg == '-1'){
							alert('选择的物资部分或者全部已经验收，请查询后再修改。');
						} else {
							alert('<fmt:message key="save"/><fmt:message key="successful"/>！');
							var action="<%=path%>/inspectionOut/tableSupplyAcctTP.do";
							$('#listForm').attr('action',action);
							$('#listForm').submit();
						}
					}
				});
			}else{
				alert('<fmt:message key="do_not_modify_any_data"/>！');
			}
			parent.bhysEditState = '';
			parent.ysChangeNum = 0;
			parent.ysTrList=undefined;
			loadToolBar([true,true,false,true]);
			$('tbody input[type="text"]').attr('disabled',true);//不可编辑
			$('#listForm').submit();
		}
		//打印单据
		function printDis(){
			$("#wait2").val("NO");
			$('#listForm').attr('target','report');
			window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
			var action = '<%=path%>/inspectionOut/printReceipt.do';	
			var action1="<%=path%>/inspectionOut/tableCheck.do";
			$('#listForm').attr('action',action);
			$('#listForm').submit();
			$('#listForm').attr('action',action1);
			$('#listForm').attr('target','');
			$("#wait2").val("");
		}		
		//焦点离开检查输入是否合法
		function validate(e){
			if(null==e.value || ""==e.value){
				e.value=e.defaultValue;
				showMessage({
							type: 'error',
							msg: '<fmt:message key="cannot_be_empty"/>！',
							speed: 1000
							});
				$(e).focus();
				return;
			}
			if(isNaN(e.value) && $.trim(e.value) != '-'){
				e.value=e.defaultValue;
				showMessage({
							type: 'error',
							msg: '<fmt:message key="not_a_valid_number"/>！',
							speed: 1000
							});
				$(e).focus();
				return;
			}
			if(Number(e.value) < 0 && $('#ynTh').val() != 'Y'){
				e.value=e.defaultValue;
				showMessage({
							type: 'error',
							msg: '<fmt:message key="cannot_be_negative"/>！',
							speed: 1000
							});
				$(e).focus();
				return;
			}
			if(Number(e.value) > 0 && $('#ynTh').val() == 'Y'){
				e.value=e.defaultValue;
				showMessage({
							type: 'error',
							msg: '<fmt:message key="hr_Not_positive"/>！',
							speed: 1000
							});
				$(e).focus();
				return;
			}
			var diff = (Number(e.value)-Number($(e).parents('tr').find('td:eq(4)').find('span').attr('title'))).toFixed(2);
			if(diff < 0)
				$(e).parents('tr').find('td:eq(6)').find('span').text(diff).addClass("redspan");
			else
				$(e).parents('tr').find('td:eq(6)').find('span').text(diff).removeClass("redspan");
		}
		//保存到货数量
		function saveAmountin(e,f,u){
			if(isNaN(e.value)){
				e.value = e.defaultValue;
			}
			if(e.value == ''){
				e.value = "0.00";
			}
			if(e.defaultValue!=e.value){
				if(parent.ysTrList){
					if(parent.ysTrList[f]){
						parent.ysTrList[f]['cntfirm']=e.value;
						parent.ysTrList[f]['cntfirm1'] = Number(e.value)*Number(u);//参考数量
					}else{
						parent.ysTrList[f]={};
						parent.ysChangeNum = parent.ysChangeNum+1;
						$("#changeNum").text(parent.ysChangeNum);
						parent.ysTrList[f]['cntfirm']=e.value;
						parent.ysTrList[f]['cntfirm1'] = Number(e.value)*Number(u);//参考数量
					}
				}else{
					parent.ysTrList = {};
					parent.ysTrList[f]={};
					parent.ysChangeNum = 1;
					$("#changeNum").text(1);
					parent.ysTrList[f]['cntfirm']=e.value;
					parent.ysTrList[f]['cntfirm1'] = Number(e.value)*Number(u);//参考数量
				}
				var diff = (Number(e.value)-Number($(e).parents('tr').find('td:eq(4)').find('span').attr('title'))).toFixed(2);
				if(diff < 0)
					$(e).parents('tr').find('td:eq(6)').find('span').text(diff).addClass("redspan");
				else
					$(e).parents('tr').find('td:eq(6)').find('span').text(diff).removeClass("redspan");
			}
		}
		
		function pageReload(){
			$('#listForm').submit();
		}
		
		//修改入库日期
		function updateMaded(){
			var html = '<div style="margin:50px 100px;"><span style="color:red;font-size:24px;">请正确选择入库日期！！！</span><br><input type="text" id="maded2" class="Wdate text" value="<fmt:formatDate value="${null == supplyAcct.dat ? '' : supplyAcct.dat}" pattern="yyyy-MM-dd"/>" onfocus="new WdatePicker()" style="width:150px;"/></div>';
			$('body').window({
				id: 'window_updateHoped',
				title: '单据入库日期确认',
				content: html,
				width: '400px',//'1000px'
				height: '300px',
				draggable: true,
				isModal: true,
				confirmClose: false,
				topBar: {
					items: [{
							text: '<fmt:message key="enter"/>',
							title: '<fmt:message key="enter"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								$('#maded').val($('#maded2').val());
								$('#maded1').val($('#maded2').val());
								if($('#maded2').val() == ''){
									alert('请填写入库日期！');
									return false;
								}
								if(!confirm('是否确认入库日期为【'+$('#maded2').val()+'】的验收入库单?')){
									return false;
								}
								$('.close').click();
								checkIn();
							}
						},{
							text: '<fmt:message key="cancel"/>',
							title: '<fmt:message key="cancel"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.close').click();
							}
						}]
				}
			});
		}
		
		//验收入库方法
		function checkIn(){
			//1.先判断验货数量有几个为0的
			var nochange = $(".nochange").size();
			if(nochange != 0){
				if(!confirm('注意！！！您有'+nochange+'条验货数量是0的物资，是否继续？')){
					return;
				}
			}
			//先判断仓库有没有期初
			$.ajax({
				url:"<%=path%>/misbohcommon/checkQC.do?code="+$('#firmcode').val(),
				type:"post",
				success:function(data){
					if(data){
						//判断是否盘点
						var msg = 0;
						var data = {};
						var maded = $('#maded').val();
						data['maded'] = maded;
						data['positn'] = $('#firmcode').val();
						$.ajax({url:'<%=path%>/misbohcommon/chkYnInout.do',type:'POST',
								data:data,async:false,success:function(data){
							msg = data;
						}});
						if(1 == msg){//提示已做盘点，是否继续
							if(!confirm('当前门店在'+maded+'已做盘点,是否继续进行出入库操作?')){return};
						}else if(2 == msg){
							alert('当前门店在'+maded+'已做盘点,不能进行出入库操作！');
							return;
						}
						ChkstomYsrk(maded);
					}else{
						alert('当前门店尚未期初！<fmt:message key="can_not"/><fmt:message key="storage"/>！');
						return;
					}
				}
			});
		}
		
		//验收入库
		function ChkstomYsrk(maded){
			//改为先判断有没有选择物资  wjf
			var checkboxList = $('.grid').find('.table-body').find('tr');
			if(checkboxList && checkboxList.size() == 0){
				alert('<fmt:message key="empty_document_unallowed_please_select_supplies"/>！');
				return;
			}
			var flag1 = false;
			var chkValue = [];
			var chkValue1 = [];
			var chkstonoValue = [];
			checkboxList.each(function(i){
				if('Y' == $(this).data('isdept')){//是档口报货来的
					flag1 = true;
					chkValue1.push($(this).data('id'));
					chkstonoValue.push($(this).data('chkstono'));//申购单号
				}else{
					chkValue.push($(this).data('id'));
				}
			});
			if(flag1){
				//chkstonoValue去重
				var chkstonoValue2 = [];
				var json = {};
				for(var i = 0; i < chkstonoValue.length; i++){
					if(!json[chkstonoValue[i]]){
						chkstonoValue2.push(chkstonoValue[i]);
				   		json[chkstonoValue[i]] = 1;
				  	}
				}
				//判断物资在不在档口报货单里存在
				var ids=chkValue1.join(",");
				var chkstono=chkstonoValue2.join(",");
				$('#wait').show();
				$('#wait2').show();
				window.setTimeout(function(){
					$.ajax({
						url:'<%=path%>/inspectionOut/checkChkstomDeptSpcode.do?ids='+ids+'&chkstoNos='+chkstono,
						type:"post",
						success:function(data){
							$('#wait').hide();
							$('#wait2').hide();
							if(data == 0){
								checkSaveGroup(ids,maded);
							}else{
								checkDeptSave(ids,chkstono,maded);
							}
						}
					});
				},200);
			}else if(chkValue.length > 0){
				if("Y" == $('#ynUseDept').val()){//分多档口的,弹出多档口的验货界面
					checkSaveGroup(chkValue.join(','),maded);
				}else{
					var action="<%=path%>/inspectionOut/checkSupplyAcctTP.do?maded="+maded;
					$('#listForm').attr('action',action);
					$('#listForm').submit();
				}
			}
		}
		
		//门店验货  无档口的情况
		function checkSave(chkValue,maded){
			//先判断仓库有没有期初
			$.ajax({
				url:"<%=path%>/misbohcommon/checkQC.do?code="+$('#firmCode').val(),
				type:"post",
				success:function(data){
					if(data){
						var action = '<%=path%>/inspectionOut/check.do?ids='+chkValue.join(",")+'&maded='+maded;
						$('body').window({
							title: '<fmt:message key="audit_arrival_information"/>',
							content: '<iframe frameborder="0" src='+action+'></iframe>',
							width: 500,
							height: '245px',
							draggable: true,
							isModal: true
						});
					}else{
// 						alert($('#firmCode').val()+'该门店还未做期初！不能入库！');
						alert($('#firmCode').val()+'<fmt:message key="the_storage_positions_do_not_at_the_beginning_of_the_period"/>！<fmt:message key="can_not"/><fmt:message key="storage"/>！');
						return;
					}
				}
			});
		}
		//门店分组验货 有档口的情况下
		function checkSaveGroup(ids,maded){
			$('body').window({
				id: 'window_chkstomexplan',
				title: '<fmt:message key="branche_acceptance_ratio" />',
				content: '<iframe id="chkinmGroupFrame" name="chkinmGroupFrame" frameborder="0" src="<%=path%>/inspectionOut/toChkinmGroup.do?ids='+ids+'"></iframe>',
				width: '99%',
				height: '95%',
				draggable: true,
				isModal: true
			});
		}
		//档口验货
		function checkDeptSave(ids,chkstono,maded){
			$('body').window({
				id: 'window_chkstomexplan',
				title: '<fmt:message key="dire_dept" />',
				content: '<iframe id="chkinmZBFrame" name="chkinmZBFrame" frameborder="0" src="<%=path%>/inspectionOut/toChkinmDept.do?id='+ids+'&chkstoNos='+chkstono+'"></iframe>',
				width: '99%',
				height: '95%',
				draggable: true,
				isModal: true
			});
		}
		
		//生成入库单的状态
		function chkMsg(){
			var pr = $("#pr").val();
			if(pr == "0"){
				alert('<fmt:message key="empty_data_cannot_to_store"/>！');
			}else if(pr == "-1"){
				alert("生成入库单失败！");
			}else if(pr == "1"){
				alert('<fmt:message key="storage_completed_to_query_documents"/>！');
				parent.pageReload();
				parent.$('.close').click();
			}
		}
		
		//配送数量等于验货数量
		function peiSongToyanHuo(){
			var checkboxList = $('.grid').find('.table-body').find('tr');
			if(checkboxList && checkboxList.size() > 0){
				checkboxList.each(function(i){
					var amount = Number($(this).find('td:eq(4)').find('span').attr('title'));
					$(this).find('td:eq(5)').find('span').find('input').val(amount.toFixed(2)).attr('title',amount);//验货数量
					$(this).find('td:eq(6)').find('span').text('0.00').removeClass("redspan");
					var f = $(this).data('id');
					if(parent.ysTrList){
						if(parent.ysTrList[f]){
							parent.ysTrList[f]['cntfirm']= amount;
							parent.ysTrList[f]['cntfirm1'] = amount*Number($(this).data('unitper'));//参考数量
						}else{
							parent.ysTrList[f]={};
							parent.ysChangeNum = parent.ysChangeNum+1;
							$("#changeNum").text(parent.ysChangeNum);
							parent.ysTrList[f]['cntfirm'] = amount;
							parent.ysTrList[f]['cntfirm1'] = amount*Number($(this).data('unitper'));//参考数量
						}
					}else{
						parent.ysTrList = {};
						parent.ysTrList[f]={};
						parent.ysChangeNum = 1;
						$("#changeNum").text(1);
						parent.ysTrList[f]['cntfirm'] = amount;
						parent.ysTrList[f]['cntfirm1'] = amount*Number($(this).data('unitper'));//参考数量
					}
				});
			}
		}
		</script>
	</body>
</html>