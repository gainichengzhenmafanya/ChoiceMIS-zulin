<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>直配按单据验收</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>	
		<style type="text/css">
			.page{margin-bottom: 25px;}
			.ico-clear {
				    background-position: 0px 0px;
				    background-image: url("<%=path%>/image/Window/operator.gif");
			    	    background-repeat: no-repeat;
			    	    cursor: pointer;
			    	    width: 15px;
			    	    height: 15px;
			}
			.textInput span{
				padding:0px;
			}
			.textInput input{
				border:0px;
				background: #F1F1F1;
			}
			a.l-btn-plain{
				border:1px solid #7eabcd; 
				height:22px;
			}
			.search{
				margin-top:-2px;
				cursor: pointer;
			}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<input type="hidden" id="pr" value="${arrivalm.pr }"/>
		<form id="listForm" action="<%=path%>/inspectionDire/findArrivald.do" method="post">
			<input type="hidden" id="pk_arrival" name="pk_arrival" value="${arrivalm.pk_arrival }"/>
			<input type="hidden" id="vbatchno" name="vbatchno" value="${arrivalm.vbatchno }"/>
			<input type="hidden" id="pk_org" name="pk_org" value="${arrivalm.pk_org }"/>
			<input type="hidden" id="istate" name="istate" value="${arrivalm.istate }"/>
			<div class="form-line">	
				<div class="form-label">到货单号:</div>
				<div class="form-input">
					${arrivalm.varrbillno }
					<input type="hidden" id="varrbillno" name="varrbillno" value="${arrivalm.varrbillno }"/>
				</div>
				<div class="form-label"><fmt:message key="supply_units"/>:</div>
				<div class="form-input">
					${arrivalm.deliverDes}
					<input type="hidden" id="deliverCode" name="deliverCode" value="${arrivalm.pk_supplier}"/>
				</div>
				<c:if test="${arrivalm.ichkinno > 0}">
					<div class="form-label"><span style="color:red;">入库日期:</span></div>
					<div class="form-input">
						<input type="text" id="maded" class="Wdate text" value="<fmt:formatDate value="${arrivalm.maded }" pattern="yyyy-MM-dd" type="date"/>" onfocus="new WdatePicker()"/>
					</div>
				</c:if>
			</div>
			<div class="form-line">
				<div class="form-label">单据日期:</div>
				<div class="form-input">
					${arrivalm.darrbilldate}
					<input type="hidden" id="darrbilldate" name="darrbilldate" value="${arrivalm.darrbilldate}"/>
				</div>
				<div class="form-label"><fmt:message key="summary"/>:</div>
				<div class="form-input">
					${arrivalm.vmemo}
					<input type="hidden" id="vmemo" name="vmemo" value="${arrivalm.vmemo }" />					
				</div>
				<c:choose>
					<c:when test="${arrivalm.ichkinno > 0}">
						<div class="form-label"><span style="color:red;">入库单号:</span></div>
						<div class="form-input">
							${arrivalm.ichkinno}
						</div>
					</c:when>
					<c:otherwise>
						<div class="form-input"><span style="font-weight: bold;">您有    <span id="changeNum" style="color: red;">0</span>条数据未保存</span></div>
					</c:otherwise>
				</c:choose>
			</div>
			<div class="grid">		
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num" ><span style="width: 25px;">&nbsp;</span></td>
								<td colspan="3"><fmt:message key="supplies"/></td>
								<td colspan="5"><fmt:message key="standard_unit"/></td>
								<td colspan="4">配送单位</td>
								<td rowspan="2"><span style="width:40px;"><fmt:message key="unit_price"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="amount"/></span></td>
								<td rowspan="2"><span style="width:100px;"><fmt:message key="remark"/></span></td>
							</tr>
							<tr>
								<td><span style="width:65px;"><fmt:message key="supplies_code"/></span></td>
								<td><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
								<td><span style="width:80px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:60px;"><fmt:message key="reports"/><fmt:message key="quantity"/></span></td>
								<td><span style="width:60px;">到货<fmt:message key="quantity"/></span></td>
								<td><span style="width:60px;"><fmt:message key="inspection"/><fmt:message key="quantity"/></span></td>
								<td><span style="width:60px;"><fmt:message key="difference"/><fmt:message key="quantity"/></span></td>
								<td><span style="width:40px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:60px;">到货<fmt:message key="quantity"/></span></td>
								<td><span style="width:60px;"><fmt:message key="inspection"/><fmt:message key="quantity"/></span></td>
								<td><span style="width:60px;"><fmt:message key="difference"/><fmt:message key="quantity"/></span></td>
								<td><span style="width:40px;"><fmt:message key="unit"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>		
							<c:forEach var="dis" items="${arrivaldList }" varStatus="status">
								<tr data-disunitper="${dis.disunitper }"
									<c:if test="${dis.narrnum != dis.ninsnum}">style="color:red;"</c:if> >
									<td class="num"><span style="width: 25px;">${status.index+1}</span></td>
									<td><span title="${dis.vcode }" style="width:65px;">${dis.vcode }</span></td>
									<td><span title="${dis.vname }" style="width:100px;">${dis.vname }</span></td>
									<td><span title="${dis.sp_desc }" style="width:80px;">${dis.sp_desc }</span></td>
									<td><span title="<fmt:formatNumber value="${dis.npurnum}" type="currency" pattern="0.00"/>" style="width:60px;text-align:right"><fmt:formatNumber value="${dis.npurnum}" type="currency" pattern="0.00"/></span></td>
									<td><span title="<fmt:formatNumber value="${dis.narrnum}" type="currency" pattern="0.00"/>" style="width:60px;text-align:right"><fmt:formatNumber value="${dis.narrnum}" type="currency" pattern="0.00"/></span></td>
									<td class="textInput">
										<span style="width:60px;">
											<input title="<fmt:formatNumber value="${dis.ninsnum}" type="currency" pattern="0.00"/>" type="text" class="nextclass" style="text-align: right;width: 60px;" 
												value="<fmt:formatNumber value="${dis.ninsnum}" type="currency" pattern="0.00"/>" id="amountin_${dis.pk_arrivald }" onfocus="this.select()" onblur="saveAmountin(this,'${dis.pk_arrivald}','${dis.disunitper }')" onkeyup="validate(this)"/>
										</span>
									</td>
									<td><span title="<fmt:formatNumber value="${dis.narrnum - dis.ninsnum}" type="currency" pattern="0.00"/>" style="width:60px;text-align:right"><fmt:formatNumber value="${dis.narrnum - dis.ninsnum}" type="currency" pattern="0.00"/></span></td>
									<td><span title="${dis.pk_unit }" style="width:40px;">${dis.pk_unit }</span></td>
									<td><span title="<fmt:formatNumber value="${dis.narrnum*dis.disunitper}" type="currency" pattern="0.00"/>" style="width:60px;text-align:right"><fmt:formatNumber value="${dis.narrnum*dis.disunitper}" type="currency" pattern="0.00"/></span></td>
									<td class="textInput">
										<span style="width:60px;">
											<input title="<fmt:formatNumber value="${dis.ninsnum*dis.disunitper}" type="currency" pattern="0.00"/>" type="text" class="nextclass" style="text-align: right;width: 60px;" 
												value="<fmt:formatNumber value="${dis.ninsnum*dis.disunitper}" type="currency" pattern="0.00"/>" id="amount1in_${dis.pk_arrivald }" onfocus="this.select()" onblur="saveAmount1in(this,'${dis.pk_arrivald}','${dis.disunitper }')" onkeyup="validate(this)"/>
										</span>
									</td>
									<td><span title="<fmt:formatNumber value="${(dis.narrnum - dis.ninsnum)*dis.disunitper}" type="currency" pattern="0.00"/>" style="width:60px;text-align:right"><fmt:formatNumber value="${(dis.narrnum - dis.ninsnum)*dis.disunitper }" type="currency" pattern="0.00"/></span></td>
									<td><span title="${dis.disunit }" style="width:40px;">${dis.disunit }</span></td>
									<td><span style="width:40px;text-align:right;" title="<fmt:formatNumber value="${dis.nprice}" type="currency" pattern="0.00"/>"><fmt:formatNumber value="${dis.nprice}" type="currency" pattern="0.00"/></span></td>
									<td><span style="width:60px;text-align:right" title="<fmt:formatNumber value="${dis.nmoney}" type="currency" pattern="0.00"/>"><fmt:formatNumber value="${dis.nmoney}" type="currency" pattern="0.00"/></span></td>
									<td><span title="${dis.vmemo }" style="width:100px;">${dis.vmemo}</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript">
		//生成入库单时的状态
// 	   	chkMsg();
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		$(document).ready(function(){
			//自动实现滚动条 				
			setElementHeight('.grid',['.tool'],$(document.body),110);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法		
			$('.textInput').find('input').live('click',function(event){
				var self = this;
				setTimeout(function(){
					self.select();
				},10);
			});
			$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
			});
			//编辑到货数量和单价时，按回车可以跳到下一行的同一列
			$('tbody .nextclass').live('keydown',function(e){
				if(parent.bhysEditState=="edit"){//判断如果是编辑状态
			 		if(e.keyCode==13){
			 			var lie = $(this).parent().parent().prevAll().length;
						var hang= $(this).parent().parent().parent().prevAll().length + 1;
						$('tbody').find('tr:eq('+hang+')').find('td:eq('+lie+')').find('span').find('input').focus();
						if(hang == $('.table-body').find('tr').length){
							$('tbody').find('tr:eq(0)').find('td:eq('+lie+')').find('span').find('input').focus();
						}
			 		}
				}
			});
			
			//判断如果不是编辑状态
			if(parent.bhysEditState!='edit'){
				$('tbody input[type="text"]').attr('disabled',true);//不可编辑
				if($('#istate').val() == 0 || $('#istate').val() == 1){//未确认
					loadToolBar([true,true,false,true]);
				}else if($('#istate').val() == 2){//已确认
					alert('提示：此单据供应商已经确认，不能继续修改。如需修改，清联系供应商进行反确认！');
					loadToolBar([true,false,false,true]);
				}else{
					loadToolBar2();
				}
			}else{
				$('tbody input[type="text"]').attr('disabled',false);
				new tabTableInput("table-body","text"); //input  上下左右移动
				loadToolBar([true,false,true,true]);
				if($('tbody tr:first .nextclass').length!=0){
					$('tbody tr:first .nextclass')[0].focus();//如果是编辑状态下查询，将焦点定位到第一行的采购数量列
				}
			}
		});
		//控制按钮显示
		function loadToolBar(use){
			$('.tool').html('');						
			$('.tool').toolbar({
				items: [{
					text: '<fmt:message key="select" />',
					title: '<fmt:message key="select"/>',
					useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						$("#listForm").submit();
					}
				},{
					text: '<fmt:message key="edit" />',
					title: '<fmt:message key="edit"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[1],
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-20px','0px']
					},
					handler: function(){
						//首先判断现在的状态是否能修改
						if(!checkState()){
							alert('供应商已经确认，不能修改！如果需要修改，请联系供应商进行发确认！');
							return;
						}
						if($('.grid').find('.table-body').find("tr").size()<1){
							alert('<fmt:message key="data_empty_edit_invalid"/>！！');
							return;
						}else{
							parent.bhysEditState='edit';
							loadToolBar([true,false,true,false]);
							$('tbody input[type="text"]').attr('disabled',false);
							new tabTableInput("table-body","text"); //input  上下左右移动	
							if($('tbody tr:first .nextclass').length!=0){
								$('tbody tr:first .nextclass')[0].focus();//如果是编辑状态下查询，将焦点定位到第一行的采购数量列
							}
						}									
					}
				},{
					text: '<fmt:message key="save" />',
					title: '<fmt:message key="save"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[2],
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-80px','-0px']
					},
					handler: function(){
						//首先判断现在的状态是否能修改
						if(!checkState()){
							alert('供应商已经确认，不能修改！如果需要修改，请联系供应商进行发确认！');
							return;
						}
						updateDis();
					}
				},{
// 					text: '<fmt:message key="acceptance_to_storage"/>',
// 					title: '<fmt:message key="acceptance_to_storage"/>',
// 					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')}&&use[3],
// 					icon: {
<%-- 						url: '<%=path%>/image/Button/op_owner.gif', --%>
// 						position: ['-60px','-20px']
// 					},
// 					handler: function(){
// 						updateMaded();
// 					}
// 				},{
// 					text: '<fmt:message key="print_acceptance_single"/>',
// 					title: '<fmt:message key="print_acceptance_single"/>',
// 					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
// 					icon: {
<%-- 						url: '<%=path%>/image/Button/op_owner.gif', --%>
// 						position: ['-140px','-100px']
// 					},
// 					handler: function(){
// 						printDis();
// 					}
// 				},{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						if(parent.bhysEditState=="edit"){
							if(confirm('<fmt:message key="data_unsaved_whether_to_exit"/>?')){
								parent.pageReload();
								parent.$('.close').click();
							}
						}else{
							parent.pageReload();
							parent.$('.close').click();
						}
					}
				}]
			});
		}
		
		function checkState(){
			var bool = true;
			$.ajax({
				url:"<%=path%>/inspectionDire/checkState.do?pk_arrival="+$('#pk_arrival').val(),
				type:"post",
				async: false,
				success:function(data){
					if(data == 0 || data == 1){
						bool = true;
					}else{
						bool = false;
					}
				}
			});
			return bool;
		}
		
		//控制按钮显示
		function loadToolBar2(){
			$('.tool').html('');						
			$('.tool').toolbar({
				items: [{
					text: '发送邮件',
					title: '发送邮件',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-60px','-20px']
					},
					handler: function(){
						toSendEmail(2);
					}
				},{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						if(parent.bhysEditState=="edit"){
							if(confirm('<fmt:message key="data_unsaved_whether_to_exit"/>?')){
								parent.pageReload();
								parent.$('.close').click();
							}
						}else{
							parent.pageReload();
							parent.$('.close').click();
						}
					}
				}]
			});
		}
		//修改报货
		function updateDis(){
			//改为先判断有没有选择物资  wjf
			var checkboxList = $('.grid').find('.table-body').find('tr');
			if(checkboxList && checkboxList.size() == 0){
				alert('<fmt:message key="empty_document_unallowed_please_select_supplies"/>！');
				return;
			}
			//循环提示是否有数量为0的物资
			var hint = '';
			checkboxList.each(function(){
				var amount = $(this).find('td:eq(6)').find('input').val();
				if(Number(amount) == 0){
					var i = $(this).find('td:eq(0)').text();
					var sp_name = $(this).find('td:eq(2)').find('span').attr('title');
					hint += '第'+i+'行:物资['+sp_name+'],\n';
				}
			});
			if('' != hint){
				if(!confirm(hint+'验货数量为0,是否继续进行保存?'))return;
			}
			//如果修改过数据就保存 
			if(parent.ysTrList){
				var predata = parent.ysTrList;
				var disList = {};
				var num=0;
				disList['pk_arrival'] = $('#pk_arrival').val();
				for(var i in predata){
					disList['arrivaldList['+num+'].pk_arrivald']=i;
					undefined==predata[i].ninsnum?'':disList['arrivaldList['+num+'].ninsnum']=predata[i].ninsnum;
					num++;
				}
				$.post("<%=path%>/inspectionDire/updateArrivald.do",disList,function(data){
					var rs = eval('('+data+')');
					if(rs.pr=="succ"){
						$('#istate').val(1);
						alert('<fmt:message key="successful"/><fmt:message key="update"/>'+rs.updateNum+'<fmt:message key="article"/><fmt:message key="data"/>！');
					}else{
						alert('<fmt:message key="update"/><fmt:message key="failure"/>！\n<fmt:message key="the_documents_of_inbound_or_outbound"/>！');
					}
					var action="<%=path%>/inspectionDire/findArrivald.do";
					$('#listForm').attr('action',action);
					$('#listForm').submit();
				});
			}else{
				alert('<fmt:message key="do_not_modify_any_data"/>！');
			}
			parent.bhysEditState = '';
			parent.ysChangeNum = 0;
			parent.ysTrList=undefined;
			loadToolBar([true,true,false,true]);
			$('tbody input[type="text"]').attr('disabled',true);//不可编辑
			$('#listForm').submit();
		}
		//打印单据
		function printDis(){
			$("#wait2").val("NO");
			$('#listForm').attr('target','report');
			window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
			var action = '<%=path%>/inspectionDire/printReceipt.do';	
			var action1="<%=path%>/inspectionDire/tableCheck.do";
			$('#listForm').attr('action',action);
			$('#listForm').submit();
			$('#listForm').attr('action',action1);
			$('#listForm').attr('target','');
			$("#wait2").val("");
		}		
		//焦点离开检查输入是否合法
		function validate(e){
			if(null==e.value || ""==e.value){
				e.value=e.defaultValue;
				showMessage({
							type: 'error',
							msg: '<fmt:message key="cannot_be_empty"/>！',
							speed: 1000
							});
				$(e).focus();
				return;
			}
			if(Number(e.value)<0 || isNaN(e.value)){
				e.value=e.defaultValue;
				showMessage({
							type: 'error',
							msg: '<fmt:message key="not_a_valid_number"/>！',
							speed: 1000
							});
				$(e).focus();
				return;
			}
		}
		//保存到货数量
		function saveAmountin(e,f,u){
			var value = $(e).val();
			var ovalue = $(e).parents('tr').find('td:eq(5) span').attr('title');
			if(value/ovalue >= 2 || value/ovalue <= 0.5){
				alert('验货数量比例超过50%,请注意！');
			}
			if(e.defaultValue!=e.value){
				if(parent.ysTrList){
					if(parent.ysTrList[f]){
						parent.ysTrList[f]['ninsnum']=e.value;
					}else{
						parent.ysTrList[f]={};
						parent.ysChangeNum = parent.ysChangeNum+1;
						$("#changeNum").text(parent.ysChangeNum);
						parent.ysTrList[f]['ninsnum']=e.value;
					}
				}else{
					parent.ysTrList = {};
					parent.ysTrList[f]={};
					parent.ysChangeNum = 1;
					$("#changeNum").text(1);
					parent.ysTrList[f]['ninsnum']=e.value;
				}
			}
		}
		//保存到货数量
		function saveAmount1in(e,f,u){
			var value = $(e).val();
			var ovalue = $(e).parents('tr').find('td:eq(9) span').attr('title');
			if(value/ovalue >= 2 || value/ovalue <= 0.5){
				alert('验货数量比例超过50%,请注意！');
			}
			if(e.defaultValue!=e.value){
				if(parent.ysTrList){
					if(parent.ysTrList[f]){
						parent.ysTrList[f]['ninsnum']=e.value/u;
					}else{
						parent.ysTrList[f]={};
						parent.ysChangeNum = parent.ysChangeNum+1;
						$("#changeNum").text(parent.ysChangeNum);
						parent.ysTrList[f]['ninsnum']=e.value/u;
					}
				}else{
					parent.ysTrList = {};
					parent.ysTrList[f]={};
					parent.ysChangeNum = 1;
					$("#changeNum").text(1);
					parent.ysTrList[f]['ninsnum']=e.value/u;
				}
				$(e).parents('tr').find('td:eq(6) input').attr('title',e.value/u).val(e.value/u);
			}
		}
		
		function pageReload(){
			$('#listForm').submit();
		}
		
		//修改入库日期
		function updateMaded(){
			var html = '<div style="margin:50px 100px;"><span style="color:red;font-size:24px;">请正确选择入库日期！！！</span><br><input type="text" id="maded1" class="Wdate text" value="${null == arrivalm.darrbilldate ? '' : arrivalm.darrbilldate}" onfocus="new WdatePicker()" style="width:150px;"/></div>';
			$('body').window({
				id: 'window_updateHoped',
				//title: '<fmt:message key="all_the_revised_delivery_date"/>',
				title: '单据入库日期确认',
				content: html,
				width: '400px',//'1000px'
				height: '300px',
				draggable: true,
				isModal: true,
				confirmClose: false,
				topBar: {
					items: [{
							text: '<fmt:message key="enter"/>',
							title: '<fmt:message key="enter"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								$('#maded').val($('#maded1').val());
								if($('#maded1').val() == ''){
									alert('请填写入库日期！');
									return false;
								}
								if(!confirm('是否确认入库日期为【'+$('#maded1').val()+'】的验收入库单?')){
									return false;
								}
								$('.close').click();
								checkIn();
							}
						},{
							text: '<fmt:message key="cancel"/>',
							title: '<fmt:message key="cancel"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.close').click();
							}
						}]
				}
			});
		}
		
		//验收入库方法
		function checkIn(){
			//先判断仓库有没有期初
			$.ajax({
				url:"<%=path%>/misbohcommon/checkQC.do?code="+$('#pk_org').val(),
				type:"post",
				success:function(data){
					if(data){
						//判断是否盘点
						var msg = 0;
						var data = {};
						var maded = $('#maded').val();
						data['maded'] = maded;
						data['positn'] = $('#pk_org').val();
						$.ajax({url:'<%=path%>/misbohcommon/chkYnInout.do',type:'POST',
								data:data,async:false,success:function(data){
							msg = data;
						}});
						if(1 == msg){//提示已做盘点，是否继续
							if(!confirm('当前门店在'+maded+'已做盘点,是否继续进行出入库操作?')){return};
						}else if(2 == msg){
							alert('当前门店在'+maded+'已做盘点,不能进行出入库操作！');
							return;
						}
						ChkstomYsrk(maded);
					}else{
						alert('当前门店尚未期初！<fmt:message key="can_not"/><fmt:message key="storage"/>！');
						return;
					}
				}
			});
		}
		
		//验收入库
		function ChkstomYsrk(maded){
			//改为先判断有没有选择物资  wjf
			var checkboxList = $('.grid').find('.table-body').find('tr');
			if(checkboxList && checkboxList.size() == 0){
				alert('<fmt:message key="empty_document_unallowed_please_select_supplies"/>！');
				return;
			}
			//循环提示是否有数量为0的物资
			var hint = '';
			checkboxList.each(function(){
				var amount = $(this).find('td:eq(6)').find('input').val();
				if(Number(amount) == 0){
					var i = $(this).find('td:eq(0)').text();
					var sp_name = $(this).find('td:eq(2)').find('span').attr('title');
					hint += '第'+i+'行:物资['+sp_name+'],\n';
				}
			});
			if('' != hint){
				if(!confirm(hint+'验货数量为0,是否继续进行入库操作?'))return;
			}
			var action="<%=path%>/inspectionDire/checkArrivalm.do?maded="+maded;
			$('#listForm').attr('action',action);
			$('#listForm').submit();
		}
		
		//生成入库单的状态
		function chkMsg(){
			var pr = $("#pr").val();
			if(pr == "0"){
				alert('<fmt:message key="empty_data_cannot_to_store"/>！');
			}else if(pr == "-1"){
				alert("生成入库单失败！");
			}else if(pr == "1"){
				alert('<fmt:message key="storage_completed_to_query_documents"/>！');
				parent.pageReload();
				parent.$('.close').click();
// 				toSendEmail(1);
			}else{
				var checkboxList = $('.grid').find('.table-body').find('tr');
				if(checkboxList && checkboxList.size() == 0){
					parent.pageReload();
	 				parent.$('.close').click();
				}
			}
		}
		//发送邮件
		function toSendEmail(flag){
			var pk_arrival = $('#pk_arrival').val();
			$('body').window({
				id: 'sendEmail',
				title: '发送邮件',
				content: '<iframe id="sendEmailFrame" name="sendEmailFrame" frameborder="0" src="<%=path%>/inspectionDire/toSendEmail.do?pk_arrival='+pk_arrival+'&flag='+flag+'"></iframe>',
				width: '85%',
				height: '460px',
				draggable: true,
				isModal: true,
				topBar: {
					items: [{
							text: '<fmt:message key="enter" />',
							title: '<fmt:message key="enter" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								if(getFrame('sendEmailFrame')&&window.document.getElementById("sendEmailFrame").contentWindow.validate._submitValidate()){
									$('#wait').show();
									$('#wait2').show();
									submitFrameForm('sendEmailFrame','listForm');
								}
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.close').click();
								if(flag == 1){
									parent.pageReload();
					 				parent.$('.close').click();
								}
							}
						}
					]
				}
			});
		}
		</script>
	</body>
</html>