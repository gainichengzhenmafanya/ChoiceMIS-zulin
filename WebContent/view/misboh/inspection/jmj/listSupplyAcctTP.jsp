<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>MIS-查询统配验货按单据</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.page{margin-bottom: 25px;}
		</style>					
	</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/inspectionOut/listSupplyAcctTP.do" method="post">
			<input type="hidden" id="firmcode" name="firmcode" value="${supplyAcct.firmcode }"/>
			<div class="form-line">
				<div class="form-label"><fmt:message key="startdate"/>:</div>
				<div class="form-input"><input type="text" id="bdate" name="bdate" class="Wdate text" value="${supplyAcct.bdate }" onclick="new WdatePicker()"/></div>
				<div class="form-label"><fmt:message key="orders_num"/>(<fmt:message key="document_number"/>):</div>
				<div class="form-input"><input type="text" id="vouno" name="vouno" class="text" value="${supplyAcct.vouno}"/></div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="enddate"/>:</div>
				<div class="form-input">
					<input type="text" id="edate" name="edate" class="Wdate text" value="${supplyAcct.edate }" onclick="new WdatePicker()"/>
				</div>
				<div class="form-label">单据<fmt:message key="status"/>:</div>
				<div class="form-input">
					<select name="chkyh">
						<option value="" <c:if test="${empty supplyAcct.chkyh }">selected="selected"</c:if>>全部</option>
						<option value="no" <c:if test="${supplyAcct.chkyh == 'no' }">selected="selected"</c:if>>未验货</option>
						<option value="yes" <c:if test="${supplyAcct.chkyh == 'yes' }">selected="selected"</c:if>>已验货</option>
					</select>
				</div>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width:25px;"></span></td>
								<td><span style="width:20px;"><!-- <input type="checkbox" id="chkAll"/> --></span></td>
								<td><span style="width:80px;"><fmt:message key="orders_num"/></span></td>
								<td><span style="width:150px;"><fmt:message key="document_number"/></span></td>
								<td><span style="width:100px;"><fmt:message key="document_date"/></span></td>
								<td><span style="width:80px;"><fmt:message key="status"/></span></td>
<!-- 								<td><span style="width:80px;">报货日期</span></td> -->
								<td><span style="width:80px;">入库日期</span></td>
								<td><span style="width:80px;">入库单号</span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="dis" items="${disList }" varStatus="status">
								<tr data-chkyh="${dis.chkyh }">
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td><span style="width:20px; text-align: center;"><input type="checkbox" name="idList" id="chk_${dis.chkno }" value="${dis.chkno }" data-dat="<fmt:formatDate value="${dis.dat }" pattern="yyyy-MM-dd" type="date"/>"/></span></td>
									<td><span style="width:80px;">${dis.chkno }</span></td>									
									<td><span style="width:150px;">${dis.vouno }</span></td>
									<td><span style="width:100px;"><fmt:formatDate value="${dis.dat }" pattern="yyyy-MM-dd" type="date"/></span></td>
									<td><span style="width:80px;">
										<c:choose>
											<c:when test="${dis.chkyh == 'N' }">未验货</c:when>
											<c:otherwise>已验货</c:otherwise>
										</c:choose>
									</span></td>
<%-- 									<td><span style="width:80px;"><fmt:formatDate value="${dis.bmaded }" pattern="yyyy-MM-dd" type="date"/></span></td> --%>
									<td><span style="width:80px;"><fmt:formatDate value="${dis.maded }" pattern="yyyy-MM-dd" type="date"/></span></td>
									<td><span style="width:80px;">${dis.chkinno }</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>				
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		var validate;
		$(document).ready(function(){
			//双击
			$('.grid .table-body tr').live('dblclick',function(){
				$(":checkbox").attr("checked", false);
				$(this).find(":checkbox").attr("checked", true);
				searchCheckedChkstom();
			});
			//双击
			$('.grid .table-body tr').live('click',function(){
				$(":checkbox").attr("checked", false);
				$(this).find(":checkbox").attr("checked", true);
			});
			//按钮快捷键
			focus() ;//页面获得焦点
		 	$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode){
	                case 70: $('.<fmt:message key="select"/>').click(); break;
	                case 80: $('.<fmt:message key="print"/>').click(); break;
	            }
			});  
		 	$('.tool').toolbar({
				items: [{
					text: '<fmt:message key="select" />',
					title: '<fmt:message key="select"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						$("#listForm").submit();
					}
				},{
					text: '验收',
					title: '验收',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-140px','-100px']
					},
					handler: function(){
						searchCheckedChkstom();
					}
				},{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
					}
				}]
			});
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		    $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),100);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法		
			changeTh();
		});
		//查看已审核单据的详细信息
		var detailWin;
		function searchCheckedChkstom(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() ==1){
// 					if(checkboxList.filter(':checked').parents('tr').data('chkyh') == 'Y'){
// 						alert('该单据已经验货,不能继续验收!');
// 						return;
// 					}
					var chkValue = checkboxList.filter(':checked').val();
					var dat = checkboxList.filter(':checked').data('dat');
					detailWin = $('body').window({
						id: 'checkedChkstomDetail',
						title: '统配验货详情',
						content: '<iframe id="checkedChkstomDetailFrame" frameborder="0" src="<%=path%>/inspectionOut/tableSupplyAcctTP.do?chkno='+chkValue+'&dat='+dat+'"></iframe>',
						width: '85%',
						height: '460px',
						draggable: true,
						isModal: true,
						confirmClose:false
					});
					detailWin.max();
			}else{
				alert('<fmt:message key="please_select_data_to_view"/>！');
				return ;
			}
		}
		//打印单条
		function printChkstom(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() ==1){
				var chkValue = [];
				checkboxList.filter(':checked').each(function(){
					chkValue.push($(this).val());
				});
				window.open ("<%=path%>/chkstomMis/printChkstom.do?chkstoNo="+chkValue.join(","),'newwindow','height='+window.screen.height+',width='+window.screen.width+',top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');
			}else{
				alert('<fmt:message key="please_select_single_message_to_print"/>！');
				return ;
			}
		}
		
		function pageReload(){
			$('#listForm').submit();
		}
		</script>				
	</body>
</html>