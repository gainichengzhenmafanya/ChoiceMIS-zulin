<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>MIS-查询已审核报货单</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.page{margin-bottom: 25px;}
		</style>					
	</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/inspectionDire/listArrivalm.do" method="post">
			<input type="hidden" id="pk_org" name="pk_org" value="${arrivalm.pk_org }"/>
			<div class="form-line">
				<div class="form-label"><fmt:message key="startdate"/>:</div>
				<div class="form-input"><input type="text" id="bdate" name="bdate" class="Wdate text" value="${arrivalm.bdate }" onclick="new WdatePicker()"/></div>						
				<div class="form-label">到货单单号:</div>
				<div class="form-input"><input type="text" id="varrbillno" name="varrbillno" class="text" value="${arrivalm.varrbillno}"/></div>
				<div class="form-label"><fmt:message key="supply_units"/>:</div>
				<div class="form-input">
					<input type="text"  id="deliverDes" name="deliverDes" readonly="readonly" value="${arrivalm.deliverDes}" class="text" style="width:165px;"/>
					<input type="hidden" id="pk_supplier" name="pk_supplier" value="${arrivalm.pk_supplier}"/>
					<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' /> 
				</div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="enddate"/>:</div>
				<div class="form-input">
					<input type="text" id="edate" name="edate" class="Wdate text" value="${arrivalm.edate }" onclick="new WdatePicker()"/>
				</div>
				<div class="form-label">报货单单号:</div>
				<div class="form-input"><input type="text" id="vbatchno" name="vbatchno" class="text" value="${arrivalm.vbatchno}"/></div>
				<div class="form-label">单据<fmt:message key="status"/>:</div>
				<div class="form-input">
					<select name="istate" style="margin-top:3px;"><!-- 0:未操作1已保存 2已确认 3已验货 -->
						<option value="" <c:if test="${empty arrivalm.istate }">selected="selected"</c:if>>全部</option>
						<option value="0" <c:if test="${arrivalm.istate == 0 }">selected="selected"</c:if>>门店未编辑</option>
						<option value="1" <c:if test="${arrivalm.istate == 1 }">selected="selected"</c:if>>供应商未确认(门店已编辑)</option>
						<option value="2" <c:if test="${arrivalm.istate == 2 }">selected="selected"</c:if>>供应商已确认</option>
						<option value="3" <c:if test="${arrivalm.istate == 3 }">selected="selected"</c:if>>已审核</option>
					</select>
				</div>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width:25px;"></span></td>
								<td><span style="width:20px;"><!-- <input type="checkbox" id="chkAll"/> --></span></td>
								<td><span style="width:70px;"><fmt:message key="orders_num"/></span></td>
								<td><span style="width:60px;"><fmt:message key="suppliers"/></span></td>
								<td><span style="width:150px;"><fmt:message key="suppliers_name"/></span></td>
								<td><span style="width:80px;"><fmt:message key="document_date"/></span></td>
								<td><span style="width:130px;"><fmt:message key="time"/></span></td>
								<td><span style="width:150px;"><fmt:message key="status"/></span></td>
								<td><span style="width:100px;"><fmt:message key="remark"/></span></td>
								<td><span style="width:150px;">报货单号</span></td>
								<td><span style="width:80px;">报货日期</span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="am" items="${arrivalmList }" varStatus="status">
								<tr data-istate=${am.istate }
									<c:if test="${am.istate == 3}">style="color:#B7B7B7;"</c:if>
									<c:if test="${am.istate == 0}">style="color:red;"</c:if>>
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td><span style="width:20px; text-align: center;"><input type="checkbox" name="idList" id="chk_${am.pk_arrival }" value="${am.pk_arrival }"/></span></td>									
									<td><span style="width:70px;" title="${am.varrbillno }">${am.varrbillno }</span></td>
									<td><span style="width:60px;" title="${am.pk_supplier }">${am.pk_supplier }</span></td>
									<td><span style="width:150px;" title="${am.deliverDes }">${am.deliverDes }</span></td>
									<td><span style="width:80px;" title="${am.darrbilldate }">${am.darrbilldate }</span></td>
									<td><span style="width:130px;" title="${am.darrbilltime }">${am.darrbilltime }</span></td>
									<td><span style="width:150px;" title="${am.istate }">
										<c:choose>
											<c:when test="${am.istate == 0 }">门店未编辑</c:when>
											<c:when test="${am.istate == 1 }">供应商未确认(门店已编辑)</c:when>
											<c:when test="${am.istate == 2 }">供应商已确认</c:when>
											<c:otherwise>已审核</c:otherwise>
										</c:choose>
									</span></td>
									<td><span style="width:100px;" title="${am.vmemo }">${am.vmemo }</span></td>
									<td><span style="width:150px;" title="${am.vbatchno }">${am.vbatchno }</span></td>
									<td><span style="width:80px;" title="${am.bdate }">${am.bdate }</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>				
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		var validate;
		$(document).ready(function(){
			//双击
			$('.grid .table-body tr').live('dblclick',function(){
				$(":checkbox").attr("checked", false);
				$(this).find(":checkbox").attr("checked", true);
				searchCheckedChkstom();
			});
			//双击
			$('.grid .table-body tr').live('click',function(){
				$(":checkbox").attr("checked", false);
				$(this).find(":checkbox").attr("checked", true);
			});
			//按钮快捷键
			focus() ;//页面获得焦点
		 	$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode){
	                case 70: $('.<fmt:message key="select"/>').click(); break;
	                case 80: $('.<fmt:message key="print"/>').click(); break;
	            }
			});  
		 	$('.tool').toolbar({
				items: [{
					text: '<fmt:message key="select" />',
					title: '<fmt:message key="select"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						$("#listForm").submit();
					}
				},{
					text: '查看详情',
					title: '查看详情',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-140px','-100px']
					},
					handler: function(){
						searchCheckedChkstom();
					}
				},{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
					}
				}]
			});
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		    $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),100);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法		
			changeTh();
			$('#seachDeliver').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#pk_supplier').val();
					var defaultName = $('#deliverDes').val();
					var offset = getOffset('bdate');
					top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/misbohcommon/selectOneDeliver.do?gysqx=2&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deliverDes'),$('#pk_supplier'),'900','500','isNull');
				}
			});
		});
		//查看已审核单据的详细信息
		var detailWin;
		function searchCheckedChkstom(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() ==1){
// 					if(checkboxList.filter(':checked').parents('tr').data('istate') == 1){
// 						alert('该单据已经验货,不能继续验收!');
// 						return;
// 					}
					if('' == checkboxList.filter(':checked').parents('tr').find('td:eq(4)').find('span').attr('title')){
						alert('系统中没有编码为['+checkboxList.filter(':checked').parents('tr').find('td:eq(3)').find('span').attr('title')+']的供应商,不能继续验收!请联系技术人员进行同步!');
						return;
					}
					var chkValue = checkboxList.filter(':checked').val();
					detailWin = $('body').window({
						id: 'checkedChkstomDetail',
						title: '到货单详情',
						content: '<iframe id="checkedChkstomDetailFrame" frameborder="0" src="<%=path%>/inspectionDire/findArrivald.do?pk_arrival='+chkValue+'"></iframe>',
						width: '85%',
						height: '460px',
						draggable: true,
						isModal: true,
						confirmClose:false
					});
					detailWin.max();
			}else{
				alert('<fmt:message key="please_select_data_to_view"/>！');
				return ;
			}
		}
		
		function pageReload(){
			$('#listForm').submit();
		}
		</script>				
	</body>
</html>