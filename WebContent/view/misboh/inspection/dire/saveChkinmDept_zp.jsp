<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="generated_newspaper_manifest"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<style type="text/css"> 	
				.textInput span{
					padding:0px;
				}
				.textInput input{
					border:0px;
					background: #F1F1F1;
				}
		</style>
	</head>
	<body>  
	<div class="tool"></div>
		<form id="listForm" method="post" action="<%=path %>/inspectionDire/saveChkstom.do">
<%-- 			<input type="hidden" class="text" id="ids" name="ids" value="${ids}" /> --%>
<%-- 		<div class="form-label"><fmt:message key="reported_that_trucks_between"/>：${positn.des}</div> --%>
		<div class="form-input">
			<input type="hidden" class="text" id="positn" name="positn" value="${positn.code}" />
		</div>		
 			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num" style="width: 25px;">&nbsp;</td>
								<td rowspan="2" style="width:30px;">
									<input type="checkbox" id="chkAll" checked="checked" />
								</td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="supplies_code"/></span></td>
								<td rowspan="2"><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="specification"/></span></td>
								<td rowspan="2"><span style="width:100px;"><fmt:message key="storage"/><fmt:message key="scm_dept"/></span></td>
								<td rowspan="2" <c:if test="${isNotShowSp_price == 'Y' }">style="display:none;"</c:if>><span style="width:60px;"><fmt:message key="storage"/><fmt:message key="price"/></span></td>
								<td colspan="4"><fmt:message key="reports"/></td>
								<td colspan="4"><span><fmt:message key="receive_goods"/></span></td>
								<td colspan="4"><span><fmt:message key="storage"/></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="remark"/></span></td>
							</tr>
							<tr>
								<td><span  style="width:40px;"><fmt:message key="reports"/><br/><fmt:message key="unit"/></span></td>
								<td><span  style="width:50px;"><fmt:message key="reports"/><br/><fmt:message key="quantity"/></span></td>
								<td><span  style="width:40px;"><fmt:message key="standard_unit_br"/></span></td>
								<td><span  style="width:50px;"><fmt:message key="scm_standard_quantity"/></span></td>
								
								<td><span  style="width:40px;"><fmt:message key="standard_unit_br"/></span></td>
								<td><span  style="width:50px;"><fmt:message key="quantity"/></span></td>
								<td><span  style="width:40px;"><fmt:message key="the_reference_unit_br"/></span></td>
								<td><span  style="width:50px;"><fmt:message key="quantity"/></span></td>
								
								<td><span  style="width:40px;"><fmt:message key="standard_unit_br"/></span></td>
								<td><span  style="width:50px;"><fmt:message key="quantity"/></span></td>
								<td><span  style="width:40px;"><fmt:message key="the_reference_unit_br"/></span></td>
								<td><span  style="width:50px;"><fmt:message key="quantity"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" id="table-body">
						<tbody>
							<c:forEach var="product" items="${supplyacctList}" varStatus="status">
								<tr data-deliver="${product.deliverCode }" data-unitper="${product.unitper}" data-id="${product.id }" data-sp_id="${product.sp_id }"
									data-dued="${product.dued}" data-pcno="${product.pcno }" data-chkstono="${product.chkstoNo }"
									<c:if test="${product.amount!=product.accprate }">style="color:red;"</c:if>>
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" checked="checked" name="idList" id="chk_${product.sp_code}" value="${product.sp_code}"/>
									</td>
									<td><span title="${product.sp_code}" style="width:80px;text-align: left;"><c:if test="${product.sta != 'noShow'}">${product.sp_code}</c:if>&nbsp;</span></td>
									<td><span title="${product.sp_name}" style="width:100px;text-align: left;"><c:if test="${product.sta != 'noShow'}">${product.sp_name}</c:if>&nbsp;</span></td>
									<td><span title="${product.sp_desc}" style="width:60px;text-align: right;"><c:if test="${product.sta != 'noShow'}">${product.sp_desc}</c:if>&nbsp;</span></td>
									<td><span title="${product.positnCode}" style="width:100px;text-align: left;">${product.positnDes}&nbsp;</span></td>
									<td <c:if test="${isNotShowSp_price == 'Y' }">style="display:none;"</c:if>><span title="${product.price}" style="width:60px;text-align: right;"><c:if test="${product.sta != 'noShow'}"><fmt:formatNumber value="${product.price}" pattern="##.##" minFractionDigits="2" /></c:if>&nbsp;</span></td>
									<td><span title="${product.unit3}" style="width:40px;text-align: left;">${product.unit3}&nbsp;</span></td>
									<td><span title="${product.amount1}" style="width:50px;text-align: right;"><fmt:formatNumber value="${product.amount1}" pattern="##.##" minFractionDigits="2" />&nbsp;</span></td>
									<td><span title="${product.unit}" style="width:40px;text-align: left;">${product.unit}&nbsp;</span></td>
									<td><span title="${product.amount}" style="width:50px;text-align: right;"><fmt:formatNumber value="${product.amount}" pattern="##.##" minFractionDigits="2" />&nbsp;</span></td>
									
									<td><span title="${product.unit}" style="width:40px;text-align: left;"><c:if test="${product.sta != 'noShow'}">${product.unit}</c:if>&nbsp;</span></td>
									<td><span title="${product.amountin}" style="width:50px;text-align: right;"><c:if test="${product.sta != 'noShow'}"><fmt:formatNumber value="${product.amountin}" pattern="##.##" minFractionDigits="2" /></c:if>&nbsp;</span></td>
									<td><span title="${product.unit1}" style="width:40px;text-align: left;"><c:if test="${product.sta != 'noShow'}">${product.unit1}</c:if>&nbsp;</span></td>
									<td><span title="${product.amount1in}" style="width:50px;text-align: right;"><c:if test="${product.sta != 'noShow'}"><fmt:formatNumber value="${product.amount1in}" pattern="##.##" minFractionDigits="2" /></c:if>&nbsp;</span></td>
									
									<td><span title="${product.unit}" style="width:40px;text-align: left;">${product.unit}&nbsp;</span></td>
									<td class="textInput">
										<span title="${product.accprate}" style="width:60px;padding: 0px;text-align:center;">
											<input type="text" class="nextclass" style="text-align: right;width: 55px;" 
												value="<fmt:formatNumber value="${product.accprate}" type="currency" pattern="0.00"/>" onfocus="this.select()" onkeyup="validate(this);"/>
										</span>
									</td>
									<td><span title="${product.unit1}" style="width:40px;text-align: left;">${product.unit1}&nbsp;</span></td>
									<td class="textInput">
										<span title="${product.accpratemin}" style="width:60px;padding: 0px;text-align:center;">
											<input type="text" style="width:55px;text-align:right;"
												value="<fmt:formatNumber value="${product.accpratemin}" type="currency" pattern="0.00"/>" onfocus="this.select()" onkeyup="validate(this);"/>
										</span>
									</td>
									<td class="textInput">
										<span title="" style="width:80px;padding: 0px;">
											<input type="text" value="" onfocus="this.select()"/>
										</span>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript">
		//ajax同步设置
// 		$.ajaxSetup({
// 			async: false
// 		});
		$(document).ready(function(){
			setElementHeight('.grid',['.tool'],$(document.body),27);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			new tabTableInput("table-body","text"); //input  上下左右移动
			loadTool(true);
			setTimeout('keepSessionAjax()',300000);//保持session不失效
		});
		
		function loadTool(use){
			$('.tool').html('');
			$('.tool').toolbar({
				items: [{
					text: '<fmt:message key="dire_dept" />',
					title: '<fmt:message key="dire_dept" />',
					useable: use,
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-40px','-40px']
					},
					handler: function(){
						saveChkinmDept(parent.$('#maded1').val());
					}
				},{
					text: '<fmt:message key="cancel" />',
					title: '<fmt:message key="cancel"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-60px','0px']
					},
					handler: function(){
						parent.$('.close').click();
					}
				}]
			});
		}
		
		function validate(e){
	    	if(isNaN(e.value) || $.trim(e.value) == ''){
				alert('<fmt:message key="number_be_not_valid_number"/>！');
				e.value=e.defaultValue;
				e.focus();
				return;
			};
			$(e).parents("td").find('span').attr('title',e.value);
			if($(e).parents("td").index()==18){
				if(Number($(e).parents('tr').data("unitper")) != 0){//转换率不为0的才能这么转换wjf
					$(e).parents('tr').find('td:eq(16)').find('input').val((Number($(e).parents('tr').find('td:eq(18)').find('input').val())/Number($(e).parents('tr').data("unitper"))).toFixed(2));
					$(e).parents('tr').find('td:eq(16)').find('span').attr('title',Number($(e).parents('tr').find('td:eq(18)').find('input').val())/Number($(e).parents('tr').data("unitper")));
				}
			}else if($(e).parents("td").index()==16){
				 $(e).parents('tr').find('td:eq(18)').find('input').val((Number($(e).parents('tr').find('td:eq(16)').find('input').val())*Number($(e).parents('tr').data("unitper"))).toFixed(2));
				 $(e).parents('tr').find('td:eq(18)').find('span').attr('title',Number($(e).parents('tr').find('td:eq(16)').find('input').val())*Number($(e).parents('tr').data("unitper")));
			}
		}
		
		//确定修改并生成报货单
		function saveChkinmDept(maded){ 
			loadTool(false);
			$('#wait').css('left','25%').find('span').text('系统正在保存验货数据转成入库单，过程可能需要几分钟，请您耐心等待，不要关闭此界面！LOADING...');
			$("#wait2").show();
			$("#wait").show();
			window.setTimeout(function(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() > 0){
				//1.判断同一个物资，所有档口都得选中
				var flag = true;
				var first_id = 0;//第一个物资编码
				var first_ischeck = true;//是否选中
				checkboxList.each(function(i){
					var id = $(this).parents('tr').data('id');//物资单号
					var ischeck = $(this).attr('checked');//是否选中
					if(first_id == id){
						if(first_ischeck != ischeck){
							flag = false;
							return false;
						}
					}else{
						first_id = id;
						first_ischeck = ischeck;
					}
				});
				if(!flag){
					alert('<fmt:message key="the_same_material_must_take_all_the_stalls_are_chosen"/>！');
					loadTool(true);
					$('#wait').hide();
					$('#wait2').hide();
					return;
				}
				//0.判断档口是不是做了期初
				var flag1 = true;
				var deptList = [];
				//2.判断验货数和到货数是不是相符
				first_id = 0;//第一个物资编码
				var first_dh = 0;//用来验证验货数和到货数是不是相符
				var first_dh1 = 0;//用来验证到货参考数量和验货参考数量是不是相符
				var first_yh = 0;
				var first_yh1 = 0;
				var flag2 = true;
				var flag3 = true;
				var sp_name = '';
				//3.判断标准数量和惨数量是不是都有值 (转换率为0的情况下)
				var flag4 = true;
				var deptDes = '';
				checkboxList.filter(':checked').each(function(i){
					var id = $(this).parents('tr').data('id');//物资单号
					var dh = $(this).parents('tr').find('td:eq(12)').find('span').attr('title');//到货
					var dh1 = $(this).parents('tr').find('td:eq(14)').find('span').attr('title');//参考数量到货
					var yh = $(this).parents('tr').find('td:eq(16)').find('span').attr('title');//验货
					var yh1 = $(this).parents('tr').find('td:eq(18)').find('span').attr('title');//参考数量验货
					deptDes = $(this).parents('tr').find('td:eq(5)').find('span').text();
					if(first_id == id){
						first_yh = Number(first_yh) + Number(yh);
						first_yh1 = Number(first_yh1) + Number(yh1);
					}
					//判断标准数量和参考数量是不是一样，不一样返回
					if((Number(yh) == 0 && Number(yh1) != 0) || (Number(yh1) == 0 && Number(yh) != 0) ){
						if(sp_name == '' || $(this).parents('tr').find('td:eq(3)').find('span').attr('title') != ''){
							sp_name = $(this).parents('tr').find('td:eq(3)').find('span').attr('title');
						}
						flag4 = false;
						return false;
					}
					if(first_id != id || i == checkboxList.filter(':checked').length-1){//如果同一个物资循环完了 或者最后一个了
						if(Number(first_dh).toFixed(2) != Number(first_yh).toFixed(2)){
							flag2 = false;
							return false;
						}
						if(Number(first_dh1).toFixed(2) != Number(first_yh1).toFixed(2)){//到货参考数量和验货数量不一样
							flag3 = false;
							return false;
						}
						first_id = id;
						first_dh = dh;
						first_yh = yh;
						first_dh1 = dh1;
						first_yh1 = yh1;
						sp_name = $(this).parents('tr').find('td:eq(3)').find('span').attr('title');
					}
					//判断有没有做期初
					var dept = $(this).parents('tr').find('td:eq(5)').find('span').attr('title');
					if($.inArray(dept,deptList)<0 && Number(yh) != 0){//查了的就不查了 没查的并且入库数量不是0的才去查期初
						deptList.push(dept);
						$.ajax({
							url:"<%=path%>/misbohcommon/checkQC.do?code="+dept,
							type:"post",
							async: false,
							success:function(data){
								if(!data){
									flag1 = false;
									alert(deptDes+'<fmt:message key="the_storage_positions_do_not_at_the_beginning_of_the_period"/>!<fmt:message key="can_not"/><fmt:message key="storage"/>!');
								}
							}
						});
					}
				});
				if(!flag1){
					//alert(dept+'<fmt:message key="the_storage_positions_do_not_at_the_beginning_of_the_period"/>!<fmt:message key="can_not"/><fmt:message key="storage"/>!');
					loadTool(true);
					$('#wait').hide();
					$('#wait2').hide();
					return;
				}
				if(!flag4){
					alert('<fmt:message key="supplies"/>：['+sp_name+']<fmt:message key="at"/>'+deptDes+'<fmt:message key="standard_number_or_reference_cannot_have_a"/>0！');
					loadTool(true);
					$('#wait').hide();
					$('#wait2').hide();
					return;
				}
				if(!flag2){
					alert('<fmt:message key="supplies"/>：['+sp_name+']<fmt:message key="receive_goods"/><fmt:message key="quantity"/>和<fmt:message key="storage"/><fmt:message key="quantity"/>不相符！\n<fmt:message key="receive_goods"/>：'+Number(first_dh).toFixed(2)+',<fmt:message key="storage"/>：'+Number(first_yh).toFixed(2));
					loadTool(true);
					$('#wait').hide();
					$('#wait2').hide();
					return;
				}
				if(!flag3){
					alert('<fmt:message key="supplies"/>：['+sp_name+']<fmt:message key="receive_goods"/><fmt:message key="reference_unit"/><fmt:message key="quantity"/>和<fmt:message key="storage"/><fmt:message key="reference_unit"/><fmt:message key="quantity"/>不相符！\n<fmt:message key="receive_goods"/>：'+Number(first_dh1).toFixed(2)+',<fmt:message key="storage"/>：'+Number(first_yh1).toFixed(2));
					loadTool(true);
					$('#wait').hide();
					$('#wait2').hide();
					return;
				}
				var selected = {};
				var chkValue = [];
				var chkValue2 = [];//存chkstod_dept 的id
				checkboxList.filter(':checked').each(function(i){
					chkValue.push($(this).parents('tr').attr('data-id'));
					chkValue2.push($(this).parents('tr').attr('data-sp_id'));
					selected['chkindList['+i+'].supply.sp_code'] = $(this).val();
					selected['chkindList['+i+'].indept'] = $(this).parents('tr').find('td:eq(5)').find('span').attr('title');
					selected['chkindList['+i+'].price'] = $(this).parents('tr').find('td:eq(6)').find('span').attr('title');
					selected['chkindList['+i+'].amount'] = $(this).parents('tr').find('td:eq(16)').find('span').attr('title');
					selected['chkindList['+i+'].memo'] = $(this).parents('tr').find('td:eq(19)').find('input').val();//备注
					selected['chkindList['+i+'].amount1'] = $(this).parents('tr').find('td:eq(18)').find('span').attr('title');
					selected['chkindList['+i+'].dued'] = $(this).parents('tr').data('dued');
					selected['chkindList['+i+'].pcno'] = $(this).parents('tr').attr('data-pcno');
					selected['chkindList['+i+'].deliver'] = $(this).parents('tr').attr('data-deliver');
					selected['chkindList['+i+'].chkstono'] = $(this).parents('tr').attr('data-chkstono');//报货单号用来存到入库单表
					selected['chkindList['+i+'].id'] = $(this).parents('tr').attr('data-id');//报货单从表主键用来更新入库单
				});
				var result='parent';
				selected['ids'] = chkValue.join(',');
				selected['sp_ids'] = chkValue2.join(',');
				selected['maded'] = maded;
				$.post('<%=path%>/inspectionDire/saveChkinmDept_zp.do',selected,function(data){
					$('#wait').hide();
					$('#wait2').hide();
				 	//弹出提示信息
				 	if ('1'!=data) {
				 		alert(data);
				 	}else{
			    		alert('<fmt:message key="operation_successful"/>！');
			    	}
					parent.pageReload(result);
				});
			}else{
				alert('<fmt:message key="please_select_materials"/>！');
				loadTool(true);
				$('#wait').hide();
				$('#wait2').hide();
				return ;
			}
			},200);
		}
		
		//新增和编辑状态下 保持session
		function keepSessionAjax(){
			$.ajax({
				url:'<%=path%>/misbohcommon/keepSessionAjax.do',
				type:"post",
				success:function(data){}
			});
			setTimeout('keepSessionAjax()',300000);
		}
		</script>
	</body>
</html>