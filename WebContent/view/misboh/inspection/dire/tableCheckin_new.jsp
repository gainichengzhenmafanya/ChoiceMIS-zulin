<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="acceptance_to_storage"/>验收入库</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<style type="text/css">
		.page{margin-bottom: 25px;}
		.search{
			margin-top:-2px;
			cursor: pointer;
		}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<input type="hidden" id="ynUseDept" value="${ynUseDept }"/>
		<input type="hidden" id="isChkinPriceNot0" value="${isChkinPriceNot0 }"/>
		<!-- 验收入库返回的信息 -->
		<form id="listForm" action="<%=path%>/inspectionDire/tableCheckin.do" method="post" target="">
			<div class="bj_head">
			<input type="hidden" id="chkMsg" name="chkMsg" value="${chkMsg }"/>
			<input type="hidden" id="isDept" name="isDept" value="${dis.isDept }"/><!-- 是否是档口报货单 -->
			<div class="form-line">	
				<div class="form-label"><font style="vertical-align:middle;" color="red" size="3"><b>*</b></font><fmt:message key="position_select"/></div>
				<div class="form-input" style="width:170px;">
					<input type="text" style="vertical-align:text-bottom;" class="text" id="firmDes"  name="firmDes" readonly="readonly" value="${dis.firmDes}"/>
					<input type="hidden" id="firmCode" name="firmCode" value="${dis.firmCode }"/>
				</div>
				<div class="form-label"><fmt:message key="startdate"/></div>
				<div class="form-input"><input type="text" id="bdat" name="bdat" value="<fmt:formatDate value="${dis.bdat}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/></div>		
				<div class="form-input">
					<input type="checkbox" <c:if test="${ind1=='ind' }"> checked="checked" </c:if> id="ind1" name="ind1" value="ind"/>
			        <font style="color:blue;"><fmt:message key="unknown_arrival_date"/></font>
				</div>	
			</div>
			<div class="form-line">	
				<div class="form-label"><fmt:message key="supply_unit"/></div>
				<div class="form-input" style="width:170px;">
					<input type="text" style="vertical-align:text-bottom;" class="text" id="deliverDes" name="deliverDes" readonly="readonly" value="${dis.deliverDes}"/>
					<input type="hidden" id="deliverCode" name="deliverCode" value="${dis.deliverCode}"/>
					<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
				</div>
				<div class="form-label"><fmt:message key="enddate"/></div>
				<div class="form-input"><input type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${dis.edat}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/></div>	
			</div>
			<div class="form-line">	
				<div class="form-label"><fmt:message key="category_selection"/></div>
				<div class="form-input" style="width:170px;">
					<input type="text" style="vertical-align:text-bottom;" class="text" id="typDes" name="typDes" readonly="readonly" value="${dis.typDes}"/>
					<input type="hidden" id="typCode" name="typCode" value="${dis.typCode}"/>
					<img id="seachTyp" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_category"/>' />
				</div>
				<div class="form-label"><span style="color:red;"><fmt:message key="storage"/><fmt:message key="date"/></span></div>
				<div class="form-input">
					 <input type="text" id="maded1" name="maded1" class="Wdate text" value="<fmt:formatDate value="${maded1}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker();"/>
				</div>
<%-- 				<div class="form-label"><fmt:message key="note_type"/></div> --%>
<!-- 				<div class="form-input"> -->
<!-- 				     <select style="width:133px;" class="select" id="memo1" name="memo1"> -->
<!-- 		        		 <option value=""></option> -->
<!-- 		                 <option -->
<%-- 		                 <c:if test="${dis.memo1=='不合格' }"> --%>
<!-- 		                 selected="selected" -->
<%-- 		                 </c:if> --%>
<%-- 		                  value="不合格"><fmt:message key="unqualified"/></option> --%>
<!-- 		        	 </select> -->
<!-- 				</div>-->
			</div>
		</div>
		    <div class="grid">		
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num" ><span style="width: 25px;">&nbsp;</span></td>
								<td rowspan="2"><span style="width: 30px;"><input type="checkbox" id="chkAll"/></span></td>
								<td rowspan="2" ><span style="width:45px;"><fmt:message key="serial_number"/></span></td>
								<td rowspan="2" ><span style="width:65px;"><fmt:message key="supplies_code"/></span></td>
								<td rowspan="2" ><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
								<td rowspan="2" ><span style="width:50px;"><fmt:message key="specification"/></span></td>
								<td rowspan="2" ><span style="width:40px;"><fmt:message key="suppliers"/></span></td>
								<td rowspan="2" ><span style="width:100px;"><fmt:message key="suppliers_name"/></span></td>
								<td colspan="3" ><fmt:message key="reports"/></td>
								<td colspan="6" ><fmt:message key="deliver_goods"/></td>
<%-- 								<td rowspan="2" ><span style="width:30px;"><fmt:message key="direction"/></span></td> --%>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="remark"/></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="additional_items"/></span></td>
								<td rowspan="2" ><span style="width:50px;"><fmt:message key="whether_storage"/></span></td>
								<td rowspan="2" ><span style="width:70px;"><fmt:message key="production_date"/></span></td>
								<td rowspan="2" ><span style="width:50px;"><fmt:message key="pc_no"/></span></td>
							</tr>
							<tr>
								<td ><span style="width:30px;"><fmt:message key="procurement_unit_br"/></span></td>
								<td ><span style="width:50px;"><fmt:message key="reports"/><br/><fmt:message key="quantity"/></span></td>
								<td ><span style="width:50px;"><fmt:message key="adjustment"/><br/><fmt:message key="quantity"/></span></td>
								
								<td ><span style="width:30px;"><fmt:message key="standard_unit_br"/></span></td>
								<td ><span style="width:50px;"><fmt:message key="quantity"/></span></td>
								<td <c:if test="${isNotShowSp_price == 'Y' }">style="display:none;"</c:if>><span style="width:40px;"><fmt:message key="unit_price"/></span></td>
								<td <c:if test="${isNotShowSp_price == 'Y' }">style="display:none;"</c:if>><span style="width:50px;"><fmt:message key="amount"/></span></td>
								<td ><span style="width:30px;"><fmt:message key="the_reference_unit_br"/></span></td>
								<td ><span style="width:50px;"><fmt:message key="quantity"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>		
							<c:forEach var="dis" items="${disList }" varStatus="status">
								<tr data-chkyh="${dis.chkyh }" data-unitper="${dis.unitper }"
									data-chkstono="${dis.chkstoNo }">
									<td class="num" >
										<span style="width: 25px;">${status.index+1}</span>
									</td>
									<td>
										<span style="width:30px; text-align: center;">
											<input type="checkbox"  name="idList" id="chk_${dis.id}" value="${dis.id}" sp_per1 = "${dis.sp_per1 }" ynbatch="${dis.ynbatch }"/>
										</span>
									</td>
									<td title="${dis.id }"><span style="width:45px;">${dis.id }</span></td>
									<td title="${dis.sp_code }"><span style="width:65px;">${dis.sp_code }</span></td>
									<td title="${dis.sp_name }"><span style="width:100px;">${dis.sp_name }</span></td>
									<td title="${dis.sp_desc }"><span style="width:50px;">${dis.sp_desc }</span></td>
									<td title="${dis.deliverCode }"><span style="width:40px;">${dis.deliverCode }</span></td>
									<td title="${dis.deliverDes }"><span style="width:100px;">${dis.deliverDes }</span></td>
									<td title="${dis.unit3 }"><span style="width:30px;">${dis.unit3 }</span></td>
									<td title="<fmt:formatNumber value="${dis.amount1}" type="currency" pattern="0.00"/>"><span style="width:50px;text-align:right"><fmt:formatNumber value="${dis.amount1}" type="currency" pattern="0.00"/></span></td>
									<td title="<fmt:formatNumber value="${dis.amount1sto}" type="currency" pattern="0.00"/>"><span style="width:50px;text-align:right"><fmt:formatNumber value="${dis.amount1sto}" type="currency" pattern="0.00"/></span></td>
									<td title="${dis.unit }"><span style="width:30px;">${dis.unit }</span></td>
									<td title="<fmt:formatNumber value="${dis.amountin}" type="currency" pattern="0.00"/>"><span style="width:50px;text-align:right"><fmt:formatNumber value="${dis.amountin}" type="currency" pattern="0.00"/></span></td>
									<td title="<fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/>" <c:if test="${isNotShowSp_price == 'Y' }">style="display:none;"</c:if>><span style="width:40px;text-align:right"><fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/></span></td>
									<td title="<fmt:formatNumber value="${dis.amountin*dis.pricein}" type="currency" pattern="0.00"/>" <c:if test="${isNotShowSp_price == 'Y' }">style="display:none;"</c:if>><span style="width:50px;text-align:right"><fmt:formatNumber value="${dis.pricein*dis.amountin}" pattern="##.##" minFractionDigits="2" /></span></td>
									<td title="${dis.unit1 }"><span style="width:30px;">${dis.unit1 }</span></td>
									<td title="<fmt:formatNumber value="${dis.amount1in}" type="currency" pattern="0.00"/>"><span style="width:50px;text-align:right"><fmt:formatNumber value="${dis.amount1in}" type="currency" pattern="0.00"/></span></td>
<%-- 									<td title="${dis.inout }"><span style="width:30px;"> --%>
<%-- 										<c:if test="${dis.inout == 'dire'}"><fmt:message key="direct"/></c:if> --%>
<!-- 									</span></td> -->
									<c:choose>
										<c:when test="${fn:contains(dis.memo, '##')}">
											<c:set var="memo" value="${fn:split(dis.memo, '##')}"></c:set>
											<td><span style="width:70px;" title="${empty memo[1] ? '':memo[0]}">${empty memo[1] ? '':memo[0]}</span></td>
											<td><span style="width:70px;" title="${empty memo[1] ? memo[0]:memo[1]}">${empty memo[1] ? memo[0]:memo[1]}</span></td>
										</c:when>
										<c:otherwise>
											<td><span style="width:70px;" title="${dis.memo}">${dis.memo}</span></td>
											<td><span style="width:70px;"></span></td>
										</c:otherwise>
									</c:choose>
									<td>
										<span style="width:50px;text-align:center">
											<c:choose>
												<c:when test="${dis.chkin=='Y' }">
													<img src="<%=path%>/image/themes/icons/ok.png"/>
												</c:when>
												<c:otherwise>
													<img src="<%=path%>/image/themes/icons/no.png"/>
												</c:otherwise>
											</c:choose>
										</span>
									</td>
									<td><span style="width:70px;" title="<fmt:formatDate value="${dis.dued}" pattern="yyyy-MM-dd" type="date"/>"><fmt:formatDate value="${dis.dued}" pattern="yyyy-MM-dd" type="date"/></span></td>
									<td><span style="width:50px;" title="${dis.pcno}">${dis.pcno}</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
		   	//生成入库单时的状态
		   	chkMsg();
			//按钮快捷键
			focus() ;//页面获得焦点
		 	$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode){
	                case 70: $('#autoId-button-101').click(); break;
	                case 65: $('#autoId-button-102').click(); break;
	                case 80: $('#autoId-button-103').click(); break;
	            }
			}); 
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		    $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
		   //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     }else{
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			     }
			 });
			var action = '${action}';
			if(action=='init'){
				loadToolBar([false]);
			}else{
				loadToolBar([true]);
			}
			function loadToolBar(use){
			  $('.tool').toolbar({
				items: [{
						text: '<fmt:message key="query_storage"/>',
						title: '<fmt:message key="query_storage"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-20px','-40px']
						},
						handler: function(){
							if(!$("#firmDes").val()){
								alert('<fmt:message key="please_select_positions_to_query_and_acceptance_storage"/>！');
								/* showMessage({
									type: 'error',
									msg: '<fmt:message key="please_select_positions_to_query_and_acceptance_storage"/>！',
									speed: 1000
								}); */
							}else{
								$('#listForm').submit();									
							}
						}
					},{
						text: '<fmt:message key="acceptance_to_storage"/>',
						title: '<fmt:message key="acceptance_to_storage"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')}&& use[0],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-60px','-20px']
						},
						handler: function(){
// 							if($('#maded1').val() == ''){
// 								alert('<fmt:message key="Please_select_a_warehouse_date"/>！');//请选择入库日期
// 								return;
// 							}
							//0.改为先判断有没有选择物资  wjf
							var checkboxList = $('.grid').find('.table-body').find(':checkbox');
							if(checkboxList && checkboxList.filter(':checked').size() == 0){
								alert('<fmt:message key="empty_document_unallowed_please_select_supplies"/>！');
								return;
							}
							//1.如果价格必须不能是0
							if($('#isChkinPriceNot0').val() == 'Y'){
								var checkboxList = $('.grid').find('.table-body').find(':checkbox');
								if(checkboxList && checkboxList.filter(':checked').size() > 0){
									var flag = true;
									var sp_code = '';
									checkboxList.filter(':checked').each(function(i){
										var price = Number($(this).parents('tr').find('td:eq(13)').find('span').text());//价格
										if(Number(price) == 0){
											flag = false;
											sp_code = $(this).parents('tr').find('td:eq(4)').attr('title');
											return false;
										}
									});
									if(!flag){
										alert('<fmt:message key="supplies"/>:['+sp_code+']单价不能为0！');
										return;
									}
								}
							}
							//2.提示正确填写入库日期
							updateMaded();
						}
					},{
						text: '<fmt:message key="print" />',
						title: '<fmt:message key="print"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							printYsrkChkstom();	
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
// 							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							parent.pageReload();
							parent.$('.close').click();
						}
					}]
				});
			}
			//自动实现滚动条 				
			setElementHeight('.grid',['.tool'],$(document.body),130);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			$('#seachDeliver').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#deliverCode').val();
					var defaultName = $('#deliverDes').val();
					var offset = {top:0,left:0};
					top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/misbohcommon/selectOneDeliver.do?gysqx=2&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deliverDes'),$('#deliverCode'),'900','500','isNull');
				}
			});
			$('#seachTyp').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#typCode').val();
					//var defaultName = $('#typDes').val();
					var offset = getOffset('bdat');
					top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/misbohcommon/selectMoreGrpTyp.do?defaultCode='+defaultCode),offset,$('#typDes'),$('#typCode'),'320','460','isNull');
				}
			});
		});
		//生成入库单的状态
		function chkMsg(){
			var chkMsg=$("#chkMsg").val();
			if(Number(chkMsg==2)){
				alert('<fmt:message key="empty_data_cannot_to_store"/>！');
<%-- 				var action="<%=path%>/inspectionDire/listCheckin.do"; --%>
// 				$('#listForm').attr('action',action);
// 				$('#listForm').submit();
			}
			if(Number(chkMsg==1)){
				alert('<fmt:message key="storage_completed_to_query_documents"/>！');
				var action="<%=path%>/inspectionDire/tableCheckin.do";
				$('#listForm').attr('action',action);
				$('#listForm').submit();
			}
		}
		
		//修改入库日期
		function updateMaded(){
			var html = '<div style="margin:50px 100px;"><span style="color:red;font-size:24px;">请正确选择入库日期！！！</span><br>';
			html += '<input type="text" id="maded2" class="Wdate text" value="<fmt:formatDate value="${null == maded1 ? '' : maded1}" pattern="yyyy-MM-dd"/>" onfocus="new WdatePicker()" style="width:150px;"/></div>';
			$('body').window({
				id: 'window_updateHoped',
				title: '入库日期确认',
				content: html,
				width: '400px',//'1000px'
				height: '300px',
				draggable: true,
				isModal: true,
				confirmClose: false,
				topBar: {
					items: [{
							text: '<fmt:message key="enter"/>',
							title: '<fmt:message key="enter"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								$('#maded1').val($('#maded2').val());
								if($('#maded2').val() == ''){
									alert('请填写入库日期！');
									return false;
								}
								if(!confirm('是否确认入库日期为【'+$('#maded2').val()+'】的验收入库单?')){
									return false;
								}
								$('.close').click();
								checkIn();
							}
						},{
							text: '<fmt:message key="cancel"/>',
							title: '<fmt:message key="cancel"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.close').click();
							}
						}]
				}
			});
		}
		
		//验收入库
		function checkIn(){
			//先判断仓库有没有期初
			$.ajax({
				url:"<%=path%>/misbohcommon/checkQC.do?code="+$('#firmCode').val(),
				type:"post",
				success:function(data){
					if(data){
						if(!$("#firmDes").val()){
							alert('<fmt:message key="please_select_positions_to_query_and_acceptance_storage"/>！');
							/* showMessage({
								type: 'error',
								msg: '<fmt:message key="please_select_positions_to_query_and_acceptance_storage"/>！',
								speed: 1000
							}); */
						}else{
							//判断是否盘点2015.5.27wjf
							var msg = 0;
							var data = {};
							data['maded'] = $('#maded1').val();
							data['positn'] = $('#firmCode').val();
							$.ajax({url:'<%=path%>/misbohcommon/chkYnInout.do',type:'POST',
									data:data,async:false,success:function(data){
								msg = data;
							}});
							if(1 == msg){//提示已做盘点，是否继续
								if(!confirm('<fmt:message key="positions"/>:['+$('#firmDes').val()+']<fmt:message key="at"/>'+$('#maded1').val()+'<fmt:message key="have_inventoried_Whether_to_continue_for_the_in-out_warehouse_operation"/>?')){return;};
							}else if(2 == msg){//已经盘点不能继续
								alert('<fmt:message key="positions"/>:['+$('#firmDes').val()+']<fmt:message key="at"/>'+$('#maded1').val()+'<fmt:message key="have_inventoried_Whether_to_cannot_for_the_in-out_warehouse_operation"/>!');
								return;
							}
							//判断是不是有档口报货的物资
							if($('#isDept').val() == '1'){
								chkinmDept();
							}else{
								ChkstomYsrk();
							}
						}
					}else{
						alert($('#firmDes').val()+'<fmt:message key="the_storage_positions_do_not_at_the_beginning_of_the_period"/>！<fmt:message key="can_not"/><fmt:message key="storage"/>！');
						return;
					}
				}
			});
		}
		
		//验收入库  非档口报货物资入库方法
		function ChkstomYsrk(){
			var isNull=0;
			var codes = [];
// 			var flag = true;//判断该有生产日期但没有的报货单
// 			var flag1 = true;//用来判断有没有批次号
			var sp_code = "";
			if(confirm('<fmt:message key="Acceptance_on_this_page_will_only_have_selected_material_Whether_or_not_to_continue"/>?')){//只会验收本页已选中物资,是否继续
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() > 0){
					checkboxList.filter(':checked').each(function(i){
						isNull=1;
						var amount = Number($(this).parents('tr').find('td:eq(12)').find('span').text());//标准数量
						var amount1 = Number($(this).parents('tr').find('td:eq(16)').find('span').text());//参考数量
						if((Number(amount) == 0 && Number(amount1) != 0) || (Number(amount1) == 0 && Number(amount) != 0) ){
							isNull=2;
							sp_code = $(this).parents('tr').find('td:eq(4)').attr('title');
							return false;
						}
	// 					var sp_per1 = $(this).attr('sp_per1');
	// 					var dued = $(this).parents('tr').find('td:eq(21) span').attr('title');
	// 					if(sp_per1 != 0 && dued == ''){//如果没有生产日期
	// 						flag = false;
	// 						sp_code = $(this).parents('tr').find('td:eq(4)').attr('title');
	// 						return false; 
	// 					}
	// 					var ynbatch = $(this).attr('ynbatch');
	// 					var pcno = $(this).parents('tr').find('td:eq(22) span').attr('title');
	// 					if(ynbatch == 'Y' && pcno == ''){//如果没有批次号
	// 						flag1 = false;
	// 						sp_code = $(this).parents('tr').find('td:eq(4)').attr('title');
	// 						return false; 
	// 					}
						codes.push($(this).val());
					});
				}
				if(isNull==0){
					alert('<fmt:message key="empty_document_unallowed_please_select_supplies"/>！');
					return;
				}
				if(isNull==2){
					alert('<fmt:message key="supplies"/>:['+sp_code+']<fmt:message key="standard_number_or_reference_cannot_have_a"/>0！<fmt:message key="please_return_the_modified"/>！');
					return;
				}
	// 			if(!flag){
	// 				alert('物资:['+sp_code+']没有生产日期,请返回报货分拨进行添加！');
	// 				return;
	// 			}
	// 			if(!flag1){
	// 				alert('物资:['+sp_code+']没有批次号,请返回报货分拨进行添加！');
	// 				return;
	// 			}
				if("Y" == $('#ynUseDept').val()){//分多档口的,弹出多档口的验货界面
					checkSaveGroup(codes);
				}else{
					var action="<%=path%>/inspectionDire/saveYsrkChkinm.do?inCondition="+codes.join(',');
					$('#listForm').attr('action',action);
					$('#listForm').submit();
				}
			}
		}
		
		//门店分组验货 有档口的情况下
		function checkSaveGroup(chkValue){
			var ids=chkValue.join(",");
			$('body').window({
				id: 'window_chkstomexplan',
				title: '<fmt:message key="branche_acceptance_ratio"/>',
				content: '<iframe id="chkinmGroupFrame" name="chkinmGroupFrame" frameborder="0" src="<%=path%>/inspectionDire/toChkinmGroupZP.do?inCondition='+ids+'"></iframe>',
				width: '99%',
				height: '95%',
				draggable: true,
				isModal: true
			});
		}
		
		//验收入库 档口报货物资入库方法
		function chkinmDept(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			var count=0;
			if(checkboxList && checkboxList.filter(':checked').size() > 0){
				var flag = true;//用来判断如果转换率是0的物资，如果改了标准数量，参考数量不能为0;或者反过来 wjf
				var sp_code = "";
				var chkValue = [];
				var chkstonoValue = [];
				checkboxList.filter(':checked').each(function(i){
					if(Number($(this).parents('tr').data('unitper')) == 0){
						var cnttrival = $(this).parents('tr').find('td:eq(12)').find('input').val();
						var cntutrival = $(this).parents('tr').find('td:eq(16)').find('input').val();
						if((Number(cnttrival) == 0 && Number(cntutrival) != 0) || (Number(cntutrival) == 0 && Number(cnttrival) != 0) ){
							flag = false;
							sp_code = $(this).parents('tr').find('td:eq(4)').find('span').text();
							return;
						}
					}
					chkValue.push($(this).val());
					chkstonoValue.push($(this).parents('tr').data('chkstono'));//申购单号
					if(Number($(this).parents('tr').find('td:eq(12)').find('span').find('input').val())==0){
						count++;
					}
				});
				if(!flag){
					alert('<fmt:message key="supplies"/>：['+sp_code+']<fmt:message key="storage"/><fmt:message key="standard_number_or_reference_cannot_have_a"/>0。');
					return;
				}
				if(count>0){
					if(confirm("<fmt:message key='the_number_of_inspection'/><fmt:message key='have'/>"+count+"<fmt:message key='article'/>0<fmt:message key='data'/>，<fmt:message key='whether'/><fmt:message key='continue'/>？")){
						checkDeptSave(chkValue,chkstonoValue);
					}
				}else{
					checkDeptSave(chkValue,chkstonoValue);
				}
				
			}else{
				alert('<fmt:message key="please_select_a_straight_to_file_export_goods"/>！');
				return ;
			}
		}
		//直发档口
		function checkDeptSave(chkValue,chkstonoVaue){
			var ids=chkValue.join(",");
			var chkstono=chkstonoVaue.join(",");
			$('body').window({
				id: 'window_chkstomexplan',
				title: '<fmt:message key="dire_dept" />',
				content: '<iframe id="chkinmZBFrame" name="chkinmZBFrame" frameborder="0" src="<%=path%>/inspectionDire/toChkinmDept.do?iszp=Y&id='+ids+'&chkstoNos='+chkstono+'"></iframe>',
				width: '99%',
				height: '95%',
				draggable: true,
				isModal: true
			});
		}
		
		function pageReload(){
			$('#listForm').submit();
		}
		
		//打印
		function printYsrkChkstom(){
			$("#wait2").val('NO');//不用等待加载
			$('#listForm').attr('target','report');
			window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
			var action = '<%=path%>/inspectionDire/viewYsrkChkstom.do';	
			var action1="<%=path%>/inspectionDire/tableCheckin.do";
			$('#listForm').attr('action',action);
			$('#listForm').submit();
			$('#listForm').attr('action',action1);
			$('#listForm').attr('target','');
			$("#wait2").val('');//等待加载还原
     	}
		</script>
	</body>
</html>