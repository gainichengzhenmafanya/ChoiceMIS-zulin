<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>MIS-供应商到货验收</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>	
		<style type="text/css">
			.page{margin-bottom: 25px;}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<%--当前登录用户 --%>	
		<input type="hidden" id="selectDelCodeId" name="selectDelCodeId"/>	
		<input type="hidden" id="is_dept" value="${is_dept }"/><!-- 是否多档口 -->
		<input type="hidden" id="isNotShowSp_price" value="${isNotShowSp_price }"/><!-- 是否显示单价和金额 -->
		<form id="listForm" action="<%=path%>/inspectionDire/tableCheck.do" method="post">
			<div class="form-line">	
				<div class="form-label"><fmt:message key="startdate"/></div>
				<div class="form-input">
					<input type="text" id="bdat" name="bdat" value="<fmt:formatDate value="${dis.bdat}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/>
				</div>
				<div class="form-label"><fmt:message key="supply_units"/> </div>
				<div class="form-input">
					<input type="text"  id="deliverDes" name="deliverDes" readonly="readonly" value="${dis.deliverDes}" class="text" style="width:120px;"/>
					<input type="hidden" id="deliverCode" name="deliverCode" value="${dis.deliverCode}"/>
					<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' /> 
				</div>
				<div class="form-label"><fmt:message key="category_selection"/></div>
				<div class="form-input">
					<input type="hidden" id="typCode" name="typCode" value="${dis.typCode}"/>
					<input type="text"  id="typDes" name="typDes" readonly="readonly" value="${dis.typDes}" class="text" />
					<img id="seachTyp" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_category"/>' />
				</div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="enddate"/></div>
				<div class="form-input">
					<input type="text" id="edat" name="edat" value="<fmt:formatDate value="${dis.edat}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/>
				</div>
				<div class="form-label"><fmt:message key="abbreviation_code"/></div>
				<div class="form-input">
					<input type="text" id="sp_init" name="sp_init" value="${dis.sp_init }" style="text-transform:uppercase;width:120px;" class="text"/>					
				</div>
				<div class="form-label"><fmt:message key="supplies_code"/></div>
				<div class="form-input">
					<input type="text" id="sp_code" name="sp_code" value="${dis.sp_code }" class="text" />			
					<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
				</div>
			</div>
			<div class="form-line">
				<div class="form-label"></div>
				<div class="form-input">[
					<input type="radio" <c:if test="${dis.yndo=='YES' }"> checked="checked"</c:if> name="yndo" value="YES"/><fmt:message key="inspected"/>
					<input type="radio" <c:if test="${dis.yndo=='NO' }"> checked="checked"</c:if> name="yndo" value="NO"/><fmt:message key="uninspect"/>]
				</div>
				<div class="form-label"></div>
				<div class="form-input" style="font-weight: bold;">
					<fmt:message key ="do_you_have" /><span id="changeNum" style="color: red;">0</span><fmt:message key ="the_data_is_not_saved_there_are_a_total_of" /> 
						<span style="color: green;">${totalCount}</span> <fmt:message key ="article_data" />
<%-- 					<fmt:message key="You_have"/><span id="changeNum" style="color: red;">0</span><fmt:message key="unsaved_data"/></span> --%>
				</div>
				<div class="form-label" style="margin-left: 50px;margin-top:5px;padding-left:0px; width: 200px;height:15px;background-color: #F0F0F0;" >
					<div id="currState" style="background-color: #28FF28;height:15px;width:0px;" ></div>
				</div>
				<div id="per" style="margin-top: 5px;">0</div>	
			</div>
			<div class="grid">		
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num" ><span style="width: 25px;">&nbsp;</span></td>
								<td rowspan="2" style="display:none;"><span style="width: 30px;"><input type="checkbox" id="chkAll"/></span></td>
								<td rowspan="2" ><span style="width:45px;"><fmt:message key="serial_number"/></span></td>
								<td rowspan="2" ><span style="width:65px;"><fmt:message key="supplies_code"/></span></td>
								<td rowspan="2" ><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
								<td rowspan="2" ><span style="width:50px;"><fmt:message key="specification"/></span></td>
								<td rowspan="2" ><span style="width:40px;"><fmt:message key="suppliers"/></span></td>
								<td rowspan="2" ><span style="width:100px;"><fmt:message key="suppliers_name"/></span></td>
								<td colspan="3" ><fmt:message key="reports"/></td>
								<td colspan="<c:if test="${isNotShowSp_price == 'Y' }">6</c:if><c:if test="${isNotShowSp_price == 'N' }">8</c:if>" ><fmt:message key="deliver_goods"/></td>
								<td colspan="2"><fmt:message key="inspection_ratio"/></td>
<%-- 								<td rowspan="2" ><span style="width:30px;"><fmt:message key="direction"/></span></td> --%>
								<td rowspan="2" ><span style="width:70px;"><fmt:message key="remark"/></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="additional_items"/></span></td>
								<td rowspan="2" ><span style="width:50px;"><fmt:message key="scm_if_check"/></span></td>
<!-- 								<td rowspan="2" ><span style="width:70px;">生产日期</span></td> -->
<!-- 								<td rowspan="2" ><span style="width:50px;">批次号</span></td> -->
							</tr>
							<tr>
								<td ><span style="width:30px;"><fmt:message key="procurement_unit_br"/></span></td>
								<td ><span style="width:50px;"><fmt:message key="reports"/><br/><fmt:message key="quantity"/></span></td>
								<td ><span style="width:50px;"><fmt:message key="adjustment"/><br/><fmt:message key="quantity"/></span></td>
								
								<td ><span style="width:30px;"><fmt:message key="standard_unit_br"/></span></td>
								<td ><span style="width:50px;"><fmt:message key="deliver_goods"/><fmt:message key="quantity"/></span></td>
								<td ><span style="width:50px;"><fmt:message key="inspection"/><fmt:message key="quantity"/></span></td>
								<td <c:if test="${isNotShowSp_price == 'Y' }">style="display:none;"</c:if>><span style="width:40px;"><fmt:message key="unit_price"/></span></td>
								<td <c:if test="${isNotShowSp_price == 'Y' }">style="display:none;"</c:if>><span style="width:60px;"><fmt:message key="amount"/></span></td>
								<td ><span style="width:30px;"><fmt:message key="the_reference_unit_br"/></span></td>
								<td ><span style="width:50px;"><fmt:message key="deliver_goods"/><fmt:message key="quantity"/></span></td>
								<td ><span style="width:50px;"><fmt:message key="inspection"/><fmt:message key="quantity"/></span></td>
								<td><span style="width:50px;"><fmt:message key="upper_limit"/></span></td>
 								<td><span style="width:50px;"><fmt:message key="lower_limit"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>		
							<c:forEach var="dis" items="${disList }" varStatus="status">
								<tr  <c:if test="${dis.amount != dis.amountin }">style="color:red;"</c:if> data-chkyh="${dis.chkyh }" data-unitper="${dis.unitper }"
									data-chkstono="${dis.chkstoNo }">
									<td class="num"><span style="width: 25px;">${status.index+1}</span></td>
									<td style="display:none;">
										<span style="width:30px; text-align: center;">
											<input type="checkbox"  name="idList" id="chk_${dis.id}" value="${dis.id}"/>
										</span>
									</td>
									<td><span title="${dis.id }" style="width:45px;">${dis.id }</span></td>
									<td><span title="${dis.sp_code }" style="width:65px;">${dis.sp_code }</span></td>
									<td><span title="${dis.sp_name }" style="width:100px;">${dis.sp_name }</span></td>
									<td><span title="${dis.sp_desc }" style="width:50px;">${dis.sp_desc }</span></td>
									<td><span title="${dis.deliverCode }" style="width:40px;">${dis.deliverCode }</span></td>
									<td><span title="${dis.deliverDes }" style="width:100px;">${dis.deliverDes }</span></td>
									<td><span title="${dis.unit3 }" style="width:30px;">${dis.unit3 }</span></td>
									<td><span title="${dis.amount1}" style="width:50px;text-align:right"><fmt:formatNumber value="${dis.amount1}" type="currency" pattern="0.00"/></span></td>
									<td><span title="${dis.amount1sto}" style="width:50px;text-align:right"><fmt:formatNumber value="${dis.amount1sto}" type="currency" pattern="0.00"/></span></td>
									<td><span title="${dis.unit }" style="width:30px;">${dis.unit }</span></td>
									<td><span title="${dis.amount}" style="width:50px;text-align:right"><fmt:formatNumber value="${dis.amount}" type="currency" pattern="0.00"/></span></td>
									<td class="textInput">
										<span style="width:50px;">
											<input title="${dis.amountin}" type="text" class="nextclass" style="text-align: right;width: 50px;" 
												value="<fmt:formatNumber value="${dis.amountin}" type="currency" pattern="0.00"/>" id="amountin_${dis.id }" onfocus="this.select()" onblur="saveAmountin(this,'${dis.id}','${dis.unitper }')" onkeyup="validate(this);sumTotalAmt(this,'${dis.id }')"/>
										</span>
									</td>
									<c:choose>
										<c:when test="${dis.ynprice == 'N'}"><!-- 如果没有取到报价，价格可以修改 -->
											<td class="textInput" <c:if test="${isNotShowSp_price == 'Y' }">style="display:none;"</c:if>>
												<span style="width:40px;">
													<input id="pricein_${dis.id }" class="nextclass" title="${dis.pricein}" type="text" style="width: 35px;text-align: right;" 
														value="<fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/>" onfocus="this.select()" onblur="savePricein(this,'${dis.id}')" onkeyup="validate(this);sumTotalAmt(this,'${dis.id }')"/>
												</span>
											</td>
										</c:when>
										<c:otherwise>
											<td <c:if test="${isNotShowSp_price == 'Y' }">style="display:none;"</c:if>>
												<span id="pricein_${dis.id }" style="width:40px;text-align:right;" title="${dis.pricein}">
													<fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/>
												</span>
											</td>
										</c:otherwise>
									</c:choose>
									<td <c:if test="${isNotShowSp_price == 'Y' }">style="display:none;"</c:if>>
										<span id="totalAmt_${dis.id }" style="width:60px;text-align:right">
											<fmt:formatNumber value="${dis.amountin*dis.pricein}" type="currency" pattern="0.00"/>
										</span>
									</td>
									<td><span title="${dis.unit1 }" style="width:30px;">${dis.unit1 }</span></td>
									<td><span title="${dis.amount1in}" style="width:50px;text-align:right"><fmt:formatNumber value="${dis.amount1in}" type="currency" pattern="0.00"/></span></td>
									<td class="textInput">
										<span style="width:50px;">
											<input class="nextclass"  title="${dis.amount1in}" type="text" style="width: 50px;text-align: right;" 
												value="<fmt:formatNumber value="${dis.amount1in}" type="currency" pattern="0.00"/>" onfocus="this.select()" onblur="saveAmount1in(this,'${dis.id}','${dis.unitper }')" onkeyup="validate(this);sumTotalAmt(this,'${dis.id }')"/>
										</span>
									</td>
									<td>
										<span title="${dis.accprate}" style="width:50px;text-align:right;">
											<fmt:formatNumber value="${dis.accprate}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber>&nbsp;
										</span>
									</td>
									<td>
										<span title="${dis.accpratemin}" style="width:50px;text-align:right;">
											<fmt:formatNumber value="${dis.accpratemin}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber>&nbsp;
										</span>
									</td>
<%-- 									<td><span title="${dis.inout }" style="width:30px;"> --%>
<%-- 										<c:if test="${dis.inout == 'dire'}"><fmt:message key="direct"/></c:if> --%>
<!-- 									</span></td> -->
<%-- 									<td><span title="${dis.memo1 }" style="width:40px;">${dis.memo1}</span></td> --%>
									<c:choose>
										<c:when test="${fn:contains(dis.memo, '##')}">
											<c:set var="memo" value="${fn:split(dis.memo, '##')}"></c:set>
											<td><span style="width:70px;" title="${empty memo[1] ? '':memo[0]}">${empty memo[1] ? '':memo[0]}</span></td>
											<td><span style="width:70px;" title="${empty memo[1] ? memo[0]:memo[1]}">${empty memo[1] ? memo[0]:memo[1]}</span></td>
										</c:when>
										<c:otherwise>
											<td><span style="width:70px;" title="${dis.memo}">${dis.memo}</span></td>
											<td><span style="width:70px;"></span></td>
										</c:otherwise>
									</c:choose>
									<td>
										<span style="width:50px;text-align:center">
											<c:choose>
												<c:when test="${dis.chkyh=='Y' }">
													<img src="<%=path%>/image/themes/icons/ok.png"/>
												</c:when>
												<c:otherwise>
													<img src="<%=path%>/image/themes/icons/no.png"/>
												</c:otherwise>
											</c:choose>
										</span>
									</td>
<%-- 									<td><span style="width:70px;" title="<fmt:formatDate value="${dis.dued}" pattern="yyyy-MM-dd" type="date"/>"><fmt:formatDate value="${dis.dued}" pattern="yyyy-MM-dd" type="date"/></span></td> --%>
<%-- 									<td><span style="width:50px;" title="${dis.pcno}">${dis.pcno}</span></td> --%>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
<%-- 		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script> --%>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript">
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		var nScrollHeight=0;
		var nScrollTop=0;
		var returnInfo = true;
		var pageSize = 0;
		$(document).ready(function(){
			//自动实现滚动条 				
			setElementHeight('.grid',['.tool'],$(document.body),135);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法		
			$('.textInput').find('input').live('click',function(event){
				var self = this;
				setTimeout(function(){
					self.select();
				},10);
			});
			$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
			});
			//编辑到货数量和单价时，按回车可以跳到下一行的同一列
			$('tbody .nextclass').live('keydown',function(e){
				if(parent.bhysEditState=="edit"){//判断如果是编辑状态
			 		if(e.keyCode==13){
			 			var lie = $(this).parent().parent().prevAll().length;
						var hang= $(this).parent().parent().parent().prevAll().length + 1;
						$('tbody').find('tr:eq('+hang+')').find('td:eq('+lie+')').find('span').find('input').focus();
						if(hang == $('.table-body').find('tr').length){
							$('tbody').find('tr:eq(0)').find('td:eq('+lie+')').find('span').find('input').focus();
						}
			 		}
				}
			});
			
			$('#seachDeliver').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#deliverCode').val();
					var defaultName = $('#deliverDes').val();
					var offset = getOffset('bdat');
					top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/misbohcommon/selectOneDeliver.do?gysqx=2&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deliverDes'),$('#deliverCode'),'900','500','isNull');
				}
			});
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/misbohcommon/selectSupplyLeft.do?positn=1&defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
			$('#seachTyp').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#typCode').val();
					//var defaultName = $('#typDes').val();
					var offset = getOffset('bdat');
					top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/misbohcommon/selectMoreGrpTyp.do?defaultCode='+defaultCode),offset,$('#typDes'),$('#typCode'),'320','460','isNull');
				}
			});
			if('${action}'!='init' && '${currState}'!='1'){
				addTr('first');	
			}else if('${action}'!='init' && '${currState==1}'){
				$("#per").text('100%');
				$("#currState").width(205);
			}
			pageSize = '${pageSize}';
			var ndivHeight = $(".table-body").height();
			$(".table-body").scroll(function(){
		          	nScrollHeight = $(this)[0].scrollHeight;
		          	nScrollTop = $(this)[0].scrollTop;
		          	if((ndivHeight+nScrollTop)/nScrollHeight>0.66 && $("#per").text()!='100%' && returnInfo){
		          			returnInfo = false;
		          			addTr();	
		          	}
		          });
			if('${action}'=='init'){
				parent.bhysEditState = '';//页面初始化的时候讲编辑状态改为非
				parent.ysTrList=undefined;//将trlist清空
				parent.ysChangeNum=0;//将已经修改的条数改为0
				$("#sp_init").focus();
			}else{
				$("#changeNum").text(parent.ysChangeNum);
			}
			//判断如果不是编辑状态
			if(parent.bhysEditState!='edit'){
				$('tbody input[type="text"]').attr('disabled',true);//不可编辑
				if($('input:radio[name="yndo"]:checked').val()=='NO'){
					loadToolBar([true,true,false,true]);
				}else{//查询已审核或者全部的时候 才能使用直发档口按钮
					loadToolBar([true,false,false,false]);
				}
			}else{
				$('tbody input[type="text"]').attr('disabled',false);
				new tabTableInput("table-body","text"); //input  上下左右移动
				loadToolBar([true,false,true,true]);
				if($('tbody tr:first .nextclass').length!=0){
					$('tbody tr:first .nextclass')[0].focus();//如果是编辑状态下查询，将焦点定位到第一行的采购数量列
				}
			}
		});
		//控制按钮显示
		function loadToolBar(use){
			$('.tool').html('');						
			$('.tool').toolbar({
				items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select"/>',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								$("#listForm").submit();
							}
						},'-',{
							text: '<fmt:message key="edit" />',
							title: '<fmt:message key="edit"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[1],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-20px','0px']
							},
							handler: function(){
								if($('.grid').find('.table-body').find("tr").size()<1){
									alert('<fmt:message key="data_empty_edit_invalid"/>！！');
									return;
								}else{
									parent.bhysEditState='edit';
									loadToolBar([true,false,true,false]);
									$('tbody input[type="text"]').attr('disabled',false);
									$('#resetVal').attr('disabled',false);
									new tabTableInput("table-body","text"); //input  上下左右移动	
									if($('tbody tr:first .nextclass').length!=0){
										$('tbody tr:first .nextclass')[0].focus();//如果是编辑状态下查询，将焦点定位到第一行的采购数量列
									}
								}									
							}
						},{
							text: '<fmt:message key="save" />',
							title: '<fmt:message key="save"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[2],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								updateDis();
							}
						},{
							text: '<fmt:message key="inspect" />',
							title: '<fmt:message key="inspect" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')}&&use[3],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','-20px']
							},
							handler: function(){
								//判断是不是有档口报货的物资
								if($('#is_dept').val() == 'Y'){
									//提示先验档口的还是直接验门店的
									if(confirm('<fmt:message key="Have_deparment_goods_Click_OK_to_the_department_inspection_cancel_to_the_store_inspection" />'))//存在档口报货的物资,点确定先进行档口验货,取消进行门店验货
										openCheckin(1);
									else
										openCheckin(0);
								}else{
									openCheckin(0);
								}
							}
						},'-',{
							text: 'Excel',
							title: 'Excel',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								exportDis();
							}
						},{
							text: '<fmt:message key="print_acceptance_single"/>',
							title: '<fmt:message key="print_acceptance_single"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								printDis();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								if(parent.bhysEditState=="edit"){
									if(confirm('<fmt:message key="data_unsaved_whether_to_exit"/>？'))
									invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
								}else{
									invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
								}
							}
						}]
			});
		}		
		//修改报货
		function updateDis(){
			$("#sp_init").focus();
			//如果修改过数据就保存 
			if(parent.ysTrList){
				var predata = parent.ysTrList;
				var disList = {};
				var num=0;
				var flag = true;
				var id = 0;
				for(var i in predata){
					disList['listDis['+num+'].id']=i;
					if((predata[i].amountin == 0 && predata[i].amount1in != 0) || (predata[i].amountin != 0 && predata[i].amount1in == 0)){
						flag = false;
						id = i;
						break;
					}
					undefined==predata[i].amountin?'':disList['listDis['+num+'].amountin']=predata[i].amountin;
					undefined==predata[i].amount1in?'':disList['listDis['+num+'].amount1in']=predata[i].amount1in;
					undefined==predata[i].pricein?'':disList['listDis['+num+'].pricein']=predata[i].pricein;
					undefined==predata[i].totalAmt?'':disList['listDis['+num+'].totalAmt']=predata[i].totalAmt;
					undefined==predata[i].bak2?'':disList['listDis['+num+'].bak2']=predata[i].bak2;
					num++;
				}
				if(!flag){
					alert('<fmt:message key="serial_number"/>:['+id+']<fmt:message key="standard_number_or_reference_cannot_have_a"/>0！<fmt:message key="please_return_the_modified"/>！');
					return;
				}
				$.post("<%=path%>/inspectionDire/updateDis.do",disList,function(data){
					var rs = eval('('+data+')');
					if(rs.pr=="succ"){
						alert('<fmt:message key="successful"/><fmt:message key="update"/>'+rs.updateNum+'<fmt:message key="article"/><fmt:message key="data"/>！');
						var action="<%=path%>/inspectionDire/tableCheck.do";
						$('#listForm').attr('action',action);
						$('#listForm').submit();
					}else{
						alert('<fmt:message key="update"/><fmt:message key="failure"/>！\n<fmt:message key="the_documents_of_inbound_or_outbound"/>！');
						var action="<%=path%>/inspectionDire/tableCheck.do";
						$('#listForm').attr('action',action);
						$('#listForm').submit();
					}
				});
				
			}else{
				alert('<fmt:message key="do_not_modify_any_data"/>！');
			}
			parent.bhysEditState = '';
			parent.ysChangeNum = 0;
			parent.ysTrList=undefined;
			loadToolBar([true,true,false,true]);
			$('tbody input[type="text"]').attr('disabled',true);//不可编辑
			$('#listForm').submit();
		}
		//导出excel
		function exportDis(){
			$('#listForm').attr('action',"<%=path%>/inspectionDire/exportDis.do");
			$('#listForm').submit();
		}
		//打印单据
		function printDis(){
			$("#wait2").val("NO");
			$('#listForm').attr('target','report');
			window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
			var action = '<%=path%>/inspectionDire/printReceipt.do';	
			var action1="<%=path%>/inspectionDire/tableCheck.do";
			$('#listForm').attr('action',action);
			$('#listForm').submit();
			$('#listForm').attr('action',action1);
			$('#listForm').attr('target','');
			$("#wait2").val("");
		}		
		//获取系统时间
		function getDate(){
			var myDate=new Date();  
			var yy=myDate.getFullYear();
			var MM=myDate.getMonth()+1;
			var dd=myDate.getDate();
			var hh=myDate.getHours();
			var mm=myDate.getMinutes();
			var ss=myDate.getSeconds();
			if(MM<10)
				MM="0"+MM;
			if(dd<10)
				dd="0"+dd;
			if(hh<10)
				hh="0"+hh;
			if(mm<10)
				mm="0"+mm;
			if(ss<10)
				ss="0"+ss;
			return fullDate=yy+"-"+MM+"-"+dd+" "+hh+":"+mm+":"+ss;
		}
		//焦点离开检查输入是否合法
		function validate(e){
			if(null==e.value || ""==e.value){
				e.value=e.defaultValue;
				showMessage({
							type: 'error',
							msg: '<fmt:message key="cannot_be_empty"/>！',
							speed: 1000
							});
				$(e).focus();
				return;
			}
			if(Number(e.value)<0 || isNaN(e.value) || $.trim(e.value) == ''){
				e.value=e.defaultValue;
				showMessage({
							type: 'error',
							msg: '<fmt:message key="not_a_valid_number"/>！',
							speed: 1000
							});
				$(e).focus();
				return;
			}
		}
		//保存到货数量
		function saveAmountin(e,f,u){
			if(e.defaultValue!=e.value){
				if(!checkYhbl(e)){
					alert('<fmt:message key="does_not_conform_to_the_inspection_rate"/>！');
					e.value=e.defaultValue;
					e.focus();
					return;
				}
				if(parent.ysTrList){
					if(parent.ysTrList[f]){
						parent.ysTrList[f]['amountin']=e.value;
						parent.ysTrList[f]['amount1in'] = Number(e.value)*Number(u);//参考数量
					}else{
						parent.ysTrList[f]={};
						parent.ysChangeNum = parent.ysChangeNum+1;
						$("#changeNum").text(parent.ysChangeNum);
						parent.ysTrList[f]['amountin']=e.value;
						parent.ysTrList[f]['amount1in'] = Number(e.value)*Number(u);//参考数量
					}
				}else{
					parent.ysTrList = {};
					parent.ysTrList[f]={};
					parent.ysChangeNum = 1;
					$("#changeNum").text(1);
					parent.ysTrList[f]['amountin']=e.value;
					parent.ysTrList[f]['amount1in'] = Number(e.value)*Number(u);//参考数量
				}
				$(e).parents('tr').find('td:eq(18) span').find('input').val((Number(e.value)*Number(u)).toFixed(2));//显示参考数量
			}
		}
		
		//保存到货数量1
		function saveAmount1in(e,f,u){
			if(e.defaultValue!=e.value){
				var flag = true;
				if(Number(u) != 0){
					var val = $(e).closest('tr').find('td:eq(13)').find('span').find('input').val();
					$(e).closest('tr').find('td:eq(13)').find('span').find('input').val(Number(e.value/u).toFixed(2));
					if(!checkYhbl($(e).closest('tr').find('td:eq(13)').find('span').find('input').get(0))){
						alert('<fmt:message key="does_not_conform_to_the_inspection_rate"/>！');
						e.value=e.defaultValue;
						$(e).closest('tr').find('td:eq(13)').find('span').find('input').val(val);
						e.focus();
						flag = false;
					}
				}
				if(!flag){
					return;
				}
				if(parent.ysTrList){
					if(parent.ysTrList[f]){
						parent.ysTrList[f]['amount1in']=e.value;
						if(Number(u) != 0){
							parent.ysTrList[f]['amountin'] = Number(e.value)/Number(u);//标准数量
							$(e).parents('tr').find('td:eq(13) span').find('input').val((Number(e.value)/Number(u)).toFixed(2));
						}
					}else{
						parent.ysTrList[f]={};
						parent.ysChangeNum = parent.ysChangeNum+1;
						$("#changeNum").text(parent.ysChangeNum);
						parent.ysTrList[f]['amount1in']=e.value;
						if(Number(u) != 0){
							parent.ysTrList[f]['amountin'] = Number(e.value)/Number(u);//标准数量
							$(e).parents('tr').find('td:eq(13) span').find('input').val((Number(e.value)/Number(u)).toFixed(2));
						}
					}
				}else{
					parent.ysTrList = {};
					parent.ysTrList[f]={};
					parent.ysChangeNum = 1;
					$("#changeNum").text(1);
					parent.ysTrList[f]['amount1in']=e.value;
					if(Number(u) != 0){
						parent.ysTrList[f]['amountin'] = Number(e.value)/Number(u);//标准数量
						$(e).parents('tr').find('td:eq(13) span').find('input').val((Number(e.value)/Number(u)).toFixed(2));
					}
				}
			}
		}
		
		//计算总额
		function sumTotalAmt(e,f){
			if(e.value==e.defaultValue){
				return;
			}
// 			$("#h_pricein_"+f).val($("#pricein_"+f).val());
			var price=$("#pricein_"+f).val()?$("#pricein_"+f).val():$("#pricein_"+f).text();
			var amti=$("#amountin_"+f).val();
			$("#totalAmt_"+f).text((price*amti).toFixed(2)).css('text-align','right');
			if(parent.ysTrList){
				if(parent.ysTrList[f]){
					parent.ysTrList[f]['totalAmt']=(price*amti).toFixed(2);
				}else{
					parent.ysTrList[f]={};
					parent.ysChangeNum = parent.ysChangeNum+1;
					$("#changeNum").text(parent.ysChangeNum);
					parent.ysTrList[f]['totalAmt']=(price*amti).toFixed(2);
				}
			}else{
				parent.ysTrList = {};
				parent.ysTrList[f]={};
				parent.ysChangeNum = 1;
				$("#changeNum").text(1);
				parent.ysTrList[f]['totalAmt']=(price*amti).toFixed(2);
			}
		}

		//保存单价
		function savePricein(e,f){
			if(e.defaultValue!=e.value){
				if(parent.ysTrList){
					if(parent.ysTrList[f]){
						parent.ysTrList[f]['pricein']=e.value;
					}else{
						parent.ysTrList[f]={};
						parent.ysChangeNum = parent.ysChangeNum+1;
						$("#changeNum").text(parent.ysChangeNum);
						parent.ysTrList[f]['pricein']=e.value;
					}
				}else{
					parent.ysTrList = {};
					parent.ysTrList[f]={};
					parent.ysChangeNum = 1;
					$("#changeNum").text(1);
					parent.ysTrList[f]['pricein']=e.value;
				}
			}
		}
		
		//计算验货比率
		function checkYhbl(e){
			var value = Number(e.value);
			var cnt = Number($(e).closest('tr').find('td:eq(12)').find('span').attr('title'));//原值
			var blsx = Number($(e).closest('tr').find('td:eq(19)').find('span').attr('title'));//上限
			var blxx = Number($(e).closest('tr').find('td:eq(20)').find('span').attr('title'));//下限
			if(blsx == 0 && blxx == 0){//都为0 随便验
				return true;
			}else if(blsx == 1 && blxx == 1){// 都为1的情况下，按发货数量验
				if(cnt == value){
					return true;
				}else{
					return false;
				}
			}else{
				if(value <= (cnt*blsx) && value >= (cnt*blxx)){//如果在验货比率内
					return true;
				}else{
					return false;
				}
			}
		}
		
		//打开验收入库界面
		function openCheckin(is_dept){
			var bdat=$('#bdat').val();
			var edat=$('#edat').val();
			if($('tbody tr:first .nextclass').length==0){
				alert("<fmt:message key='do_not_modify_any_data'/>！");
			}else{
				$('body').window({
					id: 'window_checkIn',
					title: '<fmt:message key="acceptance_to_storage"/>',
					content: '<iframe id="checkInFrame" frameborder="0" src="<%=path%>/inspectionDire/tableCheckin.do?bdat='+bdat+'&edat='+edat+'&isDept='+is_dept+'"></iframe>',
					width: '99%',
					height: '98%',
					draggable: true,
					isModal: true
				});
			}
		}
		
		var totalCount;
		var condition;
		var currPage;
		function addTr(check){
			if(check=='first'){
				totalCount = '${totalCount}';
				condition = ${disJson};
				currPage= 1;
				condition.maded = "";
				condition.bdat = new Date(condition.bdat.time).format("yyyy-mm-dd");
				condition.edat = new Date(condition.edat.time).format("yyyy-mm-dd");
				condition.ind = "";
				condition.dued = "";
				condition['totalCount'] = totalCount;
				condition['currPage']=1;
				$("#per").text((Number('${currState}')*100).toFixed(0)+'%');
				$("#currState").width(Number('${currState}')*200);
				return;
			}
			$.post("<%=path%>/inspectionDire/listAjax.do",condition,function(data){
				var isShow = $('#isNotShowSp_price').val() == 'Y'?'style="display:none;"':'';
				var rs = eval('('+data+')');
				//var rs = data;
				$("#per").text((rs.currState*100).toFixed(0)+'%');
				$("#currState").width(""+rs.currState*200+"px");
				//不是最后一页
				var num = rs.currPage*pageSize;
				var disesList1 = rs.disesList1;
				for(var i in disesList1){
					var dis = disesList1[i];
					var tr = '<tr ';
					if(dis.amount!=dis.amountin){
						tr = tr+'style="color:red;"';
					}
					tr = tr + '>';
					tr = tr + '<td class="num"><span style="width:25px;">'+ ++num +'</span></td>';
					tr = tr + '<td style="display:none;"><span style="width:30px; text-align: center;"></span></td>';
					tr = tr + '<td><span title="'+dis.id+'" style="width:45px;">'+dis.id+'</span></td>';
					tr = tr + '<td><span title="'+dis.sp_code+'" style="width:65px;">'+dis.sp_code+'</span></td>';
					tr = tr + '<td><span title="'+dis.sp_name+'" style="width:100px;">'+dis.sp_name+'</span></td>';
					tr = tr + '<td><span title="'+dis.sp_desc+'" style="width:50px;">'+dis.sp_desc+'</span></td>';
					tr = tr + '<td><span title="'+dis.deliverCode+'" style="width:40px;">'+dis.deliverCode+'</span></td>';
					tr = tr + '<td><span title="'+dis.deliverDes+'" style="width:100px;">'+dis.deliverDes+'</span></td>';
					//采购单位
					tr = tr + '<td><span title="'+dis.unit3+'" style="width:30px;">'+dis.unit3+'</span></td>';//采购单位数量
					tr = tr + '<td><span title="'+dis.amount1+'" style="width:50px;text-align: right;">'+dis.amount1.toFixed(2)+'</span></td>';
					tr = tr + '<td><span title="'+dis.amount1sto+'" style="width:50px;text-align: right;">'+dis.amount1sto.toFixed(2)+'</span></td>';
					//标准单位
					tr = tr + '<td><span title="'+dis.unit+'" style="width:30px;">'+dis.unit+'</span></td>';
					tr = tr + '<td><span title="'+dis.amount+'" style="width:50px;text-align: right;">'+dis.amount.toFixed(2)+'</span></td>';
					tr = tr + '<td class="textInput"><span style="width:50px;"><input class="nextclass" title="'+((undefined==dis.amountin)?"":dis.amountin.toFixed(2))+'" type="text" style="width: 50px;text-align: right;" value="'+((undefined==dis.amountin)?"":dis.amountin.toFixed(2))+'"  id="amountin_'+dis.id+'"  onfocus="this.select()" onblur="saveAmountin(this,'+dis.id+','+dis.unitper+')" onkeyup="validate(this);sumTotalAmt(this,'+dis.id +')"/></span></td>';
					if(dis.ynprice == 'N'){//有报价的不能修改
						tr = tr + '<td class="textInput" '+isShow+'><span style="width:40px;"><input class="nextclass" title="'+((undefined==dis.pricein)?"":dis.pricein.toFixed(2))+'" type="text" style="width: 35px;text-align: right;" value="'+((undefined==dis.pricein)?"":dis.pricein.toFixed(2))+'""  id="pricein_'+dis.id+'"  onfocus="this.select()" onblur="savePricein(this,'+dis.id+')" onkeyup="validate(this);sumTotalAmt(this,'+dis.id +')"/></span></td>';
					}else{
						tr = tr + '<td '+isShow+'><span  id="pricein_'+dis.id+'"  style="width:40px;text-align:right;" title="'+dis.pricein.toFixed(2)+'">'+dis.pricein.toFixed(2)+'</span></td>';
					}
					tr = tr + '<td '+isShow+'><span  id="totalAmt_'+dis.id+'"  style="width:60px;text-align: right;">'+ Number(dis.pricein * dis.amountin).toFixed(2)+'</span></td>';
					//参考单位
					tr = tr + '<td><span title="'+dis.unit1+'" style="width:30px;">'+dis.unit1+'</span></td>';
					tr = tr + '<td><span title="'+dis.amount1in+'" style="width:50px;text-align: right;">'+dis.amount1in.toFixed(2)+'</span></td>';
					tr = tr + '<td class="textInput"><span style="width:50px;"><input class="nextclass" title="'+((undefined==dis.amount1in)?"":dis.amount1in.toFixed(2))+'" type="text" style="width: 50px;text-align: right;" value="'+((undefined==dis.amount1in)?"":dis.amount1in.toFixed(2))+'" onfocus="this.select()" onblur="saveAmount1in(this,'+dis.id+','+dis.unitper+')" onkeyup="validate(this);sumTotalAmt(this,'+dis.id +')"/></span></td>';
					tr = tr + '<td><span title="'+dis.accprate.toFixed(2)+'" style="width:50px;text-align:right;">'+dis.accprate.toFixed(2)+'</span></td>';
					tr = tr + '<td><span title="'+dis.accpratemin.toFixed(2)+'" style="width:50px;text-align:right;">'+dis.accpratemin.toFixed(2)+'</span></td>';
// 					tr = tr + '<td><span title="'+dis.inout+'" style="width:30px;">'+dis.inout+'</span></td>';
					var memo = dis.memo;
					var memo1 = '';
					if(memo && memo.indexOf('##')!=-1){
						var memos = dis.memo.split('##');
						memo = memos[0];
						memo1 = memos[1];
					}
					tr = tr + '<td><span title="'+memo+'" style="width:70px;">'+memo+'</span></td>';
					tr = tr + '<td><span title="'+memo1+'" style="width:70px;">'+memo1+'</span></td>';//附加项
					if(dis.chkyh == 'Y'){
						tr = tr + '<td><span style="width:50px;text-align:center"><img src="<%=path%>/image/themes/icons/ok.png"/></span></td>';
					}else{
						tr = tr + '<td><span style="width:50px;text-align:center"><img src="<%=path%>/image/themes/icons/no.png"/></span></td>';
					}
					tr = tr + '</tr>';	
					$(".grid .table-body tbody").append($(tr));
				}
				if(rs.over!='over'){
					condition['currPage']=++currPage;
					returnInfo = true;
				}
				if(parent.bhysEditState!='edit'){
					$('tbody input[type="text"]').attr('disabled',true);//不可编辑
				}else{
					new tabTableInput("table-body","text"); //input  上下左右移动	
				}
			});
		}
		
		Date.prototype.format = function(){	
			var yy = String(this.getFullYear());
			var mm = String(this.getMonth() + 1);
			var dd = String(this.getDate());
			if(mm<10){
				mm = ''+0+mm;
			}
			if(dd<10){
				dd = ''+0+dd;
			}
			var str = yy+"-"+mm+"-"+dd;
			return str;
		};
		
		function pageReload(){
			$('#listForm').submit();
		}
		
		</script>
	</body>
</html>