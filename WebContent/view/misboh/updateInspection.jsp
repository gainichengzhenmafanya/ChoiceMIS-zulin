<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="materials_list" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
	</head>
	<body>
		<div class="easyui-tabs" fit="false" plain="true" id="tabs">
				<div title="<fmt:message key="Out_inspection"/>">
					<div class="grid" style="width:99% !important;">
						<div class="table-head" >
							<table cellspacing="0" cellpadding="0">
								<thead>
									<tr>
										<td><span style="width:80px;text-align:right;"><fmt:message key="Inspection_count"/></span></td>
										<td><span style="width:100px;text-align:right;"><fmt:message key="Material_count"/></span></td>
									</tr>
								</thead>
							</table>
						</div>
						<div class="table-body" style="height:170px;">
							<table cellspacing="0" cellpadding="0">
								<tbody>
									<c:forEach var="dire" items="${outList}" varStatus="status">
										<tr ondblclick="ps(this)">
											<td><span style="width:80px;text-align:right;">${dire.id }</span></td>
											<td><span style="width:100px;text-align:right;"><fmt:formatNumber value="${dire.cntout }" type="currency" pattern="0.00"/></span></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div title="<fmt:message key="Dire_inspection"/>">
					<div class="grid" style="width:99% !important;">
						<div class="table-head" >
							<table cellspacing="0" cellpadding="0">
								<thead>
									<tr>
										<td><span style="width:120px;"><fmt:message key="suppliers"/></span></td>
										<td><span style="width:80px;"><fmt:message key="Inspection_count"/></span></td>
										<td><span style="width:100px;"><fmt:message key="Material_count"/></span></td>
									</tr>
								</thead>
							</table>
						</div>
						<div class="table-body" style="height:170px;">
							<table cellspacing="0" cellpadding="0">
								<tbody>
									<c:forEach var="dire" items="${direList}" varStatus="status">
										<tr ondblclick="zp(this)">
											<td><span style="width:120px;" title="${dire.deliverCode }">${dire.deliverDes }</span></td>
											<td><span style="width:80px;text-align:right;">${dire.id }</span></td>
											<td><span style="width:100px;text-align:right;"><fmt:formatNumber value="${dire.amountin }" type="currency" pattern="0.00"/></span></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div title="<fmt:message key="Transfer_inspection"/>">
					<div class="grid" style="width:99% !important;">
						<div class="table-head" >
							<table cellspacing="0" cellpadding="0">
								<thead>
									<tr>
										<td><span style="width:120px;"><fmt:message key="Transferout"/><fmt:message key="positions"/></span></td>
										<td><span style="width:80px;"><fmt:message key="Inspection_count"/></span></td>
										<td><span style="width:100px;"><fmt:message key="Material_count"/></span></td>
									</tr>
								</thead>
							</table>
						</div>
						<div class="table-body" style="height:170px;">
							<table cellspacing="0" cellpadding="0">
								<tbody>
									<c:forEach var="dire" items="${dbList}" varStatus="status">
										<tr ondblclick="db(this)">
											<td><span style="width:120px;" title="${dire.positn }">${dire.positndes }</span></td>
											<td><span style="width:80px;text-align:right;">${dire.id }</span></td>
											<td><span style="width:100px;text-align:right;"><fmt:formatNumber value="${dire.cntout }" type="currency" pattern="0.00"/></span></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>	
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			//配送验货
			function ps(obj){
				var bdate = getNow();
				var edate = getNow();
				parent.showInfo('83F8F27E01F742308A0538342AD0CE9F','<fmt:message key="Out_inspection"/>','/inspection/dispatch.do?mold=select&check=2&bdate='
						+bdate+'&edate='+edate);
			}
			
			//直配验货
			function zp(obj){
				var bdat = getNow();
				var edat = getNow();
				var deliverCode = $(obj).find('td:eq(0)').find('span').attr('title');
				var deliverDes = encodeURI($(obj).find('td:eq(0)').find('span').text());
				parent.showInfo('84CAE7674FB841A3ABBB734B336A21A0','<fmt:message key="Dire_inspection"/>','/inspection/tableCheck.do?yndo=NO&bdat='
						+bdat+'&edat='+edat+'&deliverCode='+deliverCode+'&deliverDes='+encodeURI(deliverDes));
			}
			
			//调拨验货 
			function db(obj){
				var bdate = getNow();
				var edate = getNow();
				var positn = $(obj).find('td:eq(0)').find('span').attr('title');
				var positndes = encodeURI($(obj).find('td:eq(0)').find('span').text());
				parent.showInfo('8F1421D5867E45B099C31CF580BD0CD2','<fmt:message key="Transfer_inspection"/>','/inspection/dispatchDb.do?mold=select&check=2&bdate='
						+bdate+'&edate='+edate+'&positn='+positn+'&positndes='+encodeURI(positndes));
			}
			
			//获取系统时间
			function getNow(){
				var myDate=new Date(new Date().getTime());  
				var yy=myDate.getFullYear();
				var MM=myDate.getMonth()+1;
				var dd=myDate.getDate();
				if(MM<10)
					MM="0"+MM;
				if(dd<10)
					dd="0"+dd;
				return fullDate=yy+"-"+MM+"-"+dd;
			}
		</script>
	</body>
</html>