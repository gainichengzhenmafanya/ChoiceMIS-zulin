<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>项目销售统计</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<%@ include file="../reportMis/share/permission.jsp"%>
  </head>
  <body>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label" style="width: 60px;"><fmt:message key="date" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" />
			</div>
			<div class="form-label" style="width: 40px;">&nbsp;</div>
			<div class="form-input">
				<input type="radio" id="all" value="" checked="checked" name="sft" style="margin-top:2px;"/><label for="all"><fmt:message key="all_day" /></label>
				<input type="radio" id="normal" value="1" name="sft" style="margin-top:2px;"/><label for="normal"><fmt:message key="lunch" /></label>
				<input type="radio" id="pkg" value="2" name="sft" style="margin-top:3px;"/><label for="pkg"><fmt:message key="dinner" /></label>
			</div>
			<div class="form-label" style="width: 40px;">&nbsp;</div>
			<div class="form-input">
				<input type="checkbox" id="status" name="status" /><fmt:message key="The_core_food" />
			</div>
		</div>
		<input type="hidden" id="pk_store" name="pk_store" value="${store.pk_store }"/>
		<input type="hidden" id="vscode" name="vscode" value="${store.vcode }"/>
	</form>
	
	<input type="hidden" id="reportName" value="itemsalegq"/>
	<input type="hidden" id="excelUrl" value="<%=path%>/proReport/exportReport.do"/>
	<input type="hidden" id="printUrl" value="<%=path%>/proReport/printReport.do"/>
	<input type="hidden" id="dataUrl" value="<%=path%>/proReport/listItemSaleGQ.do"/>
	<input type="hidden" id="title" value="菜品销售沽清<fmt:message key="statistical" />"/>
	<div id="datagrid"></div>
	
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
		 	//默认<fmt:message key="time" />
		 	$("#bdat").htmlUtils("setDate","yes");
		 	$("#bdat").click(function(){
	  			new WdatePicker();
  	  		});
		 	//初始化<fmt:message key="selected" />标签<fmt:message key="content" />
			initTab(false);
  	 	});
  		//初始化选中标签内容
  	 	function initTab(rebuild){
  			var excelUrl = $("#excelUrl").val();
  	 		var printUrl = $("#printUrl").val();
  	 		var reportName = $("#reportName").val();
  	 		var dataUrl = $("#dataUrl").val();
  	 		var title = $("#title").val();
  	 		var grid = $("#datagrid");

  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp:true,
  	 			excelUrl:excelUrl+'?reportName='+reportName,
  	 			printUrl:printUrl+'?reportName='+reportName,
  	 			colsChooseUrl:'<%=path%>/proReport/toColumnsChoose.do?reportName='+reportName,
  	 			toolbar:['search','excel','exit'],
  	 			searchFun:function(){
  	 				$('#datagrid').datagrid('load',getParam($("#queryForm")));
  	 			}
  	 		});
  	 		if(rebuild || grid.css('display') != 'none'){
  	  				firstLoad = true;//重置第一<fmt:message key="secondary" />加载标志
		  	 		builtTable({
		  	 			headUrl:"<%=path%>/proReport/findHeaders.do?reportName="+reportName,
		  	 			dataUrl:dataUrl,
		  	 			title:title,
		  	 			grid:grid,
		  	 			alignCols:['ncount','updtotal','ndifcount'],
		  	 			numCols:['ncount','updtotal','ndifcount']
		  	 		});
  	  		}
  	 	}

  		//列选择后页面重新加载
  		function pageReload(){
  			closeColChooseWin();
  			initTab(true);
  			//重新<fmt:message key="select" />
  			$("#datagrid").datagrid('reload');
  		}
  	 </script>
  </body>
</html>