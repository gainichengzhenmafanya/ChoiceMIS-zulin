<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="reported_acceptance"/>--营业预估</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>	
		<style type="text/css">
			.page{margin-bottom: 25px;}
			.onEdit{
				border:1px solid;
				border-bottom-color: blue;
				border-top-color: blue;
				border-left-color: blue;
				border-right-color: blue;
			}
			.input{
				background:transparent;
				border:0px solid;
			}
			a.l-btn-plain{
				border:1px solid #7eabcd; 
				height:22px;
			}
			.search{
				margin-top:-2px;
				cursor: pointer;
			}
			.textInput span {
				padding:0px;
			}
			.textInput input {
				border:0px;
				width:55px;
			}
		</style>
		<script type="text/javascript">
			var path="<%=path%>";
		</script>					
	</head>
	<body>
		<div class="tool"></div>
		<%--当前登录用户 --%>	
		<form id="listForm" action="<%=path%>/forecastMis/castList.do" method="post">	
		<input type="hidden" id="firm" name="firm" value="${salePlan.firm}"/>
		<input type="hidden" id="firmNm" name="firmNm" readonly="readonly" value="${salePlan.firmNm}"/>
			<div class="form-line">	
				<div class="form-label"><fmt:message key="scm_estimated_date"/><fmt:message key="date"/>:</div>
				<div class="form-input" style="width:300px;">
					<font style="color:blue;"><fmt:message key="startdate"/>:</font>
					<input type="text" style="width:90px;" id="startdate" name="startdate" value="<fmt:formatDate value="${salePlan.startdate}" pattern="yyyy-MM-dd" type="date"/>" 
						class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'enddate\')}'});"/>
					<font style="color:blue;"><fmt:message key="enddate"/>:</font>
					<input type="text" style="width:90px;" id="enddate" name="enddate" value="<fmt:formatDate value="${salePlan.enddate}" pattern="yyyy-MM-dd" type="date"/>" 
						class="Wdate text" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'startdate\')}'});"/>
				</div>
			</div>	
			<div class="form-line">	
				<div class="form-label"><fmt:message key="the_reference_date"/>:</div>
				<div class="form-input" style="width:300px;">
					<font style="color:blue;"><fmt:message key="startdate"/>:</font>
					<input type="text" style="width:90px;" id="bdate" name="bdate" value="<fmt:formatDate value="${salePlan.bdate}" pattern="yyyy-MM-dd" type="date"/>" 
						class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edate\')}'});"/>
					<font style="color:blue;"><fmt:message key="enddate"/>:</font>
					<input type="text" style="width:90px;" id="edate" name="edate" value="<fmt:formatDate value="${salePlan.edate}" pattern="yyyy-MM-dd" type="date"/>" 
						class="Wdate text" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdate\')}'});"/>
				</div>
				<div class="form-label"></div>
				<div class="form-input" style="width:200px;">
					营业额*系数：<input type="text" value="1" id="xishu" style="text-align:right;width:30px;"/>&nbsp;<input type="button" value="计算" onclick="calAll()"/>
				</div>
			</div>	
		   	<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num"><span style="width:15px;">&nbsp;</span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="date"/></span></td>
								<td rowspan="2"><span style="width:25px;"><fmt:message key="scm_weeks"/></span></td>
								<td rowspan="2"><span style="width:25px;"><fmt:message key="scm_holidays"/></span></td>
								<td rowspan="2"><span style="width:50px;"><fmt:message key="special_events"/></span></td>
								
								<td colspan="3"><span>营业额</span></td>
 								<td colspan="3"><span>人均</span></td>
 								<td colspan="3"><span>人数</span></td>
 								<td colspan="3"><span>单均</span></td>
 								<td colspan="3"><span>单数</span></td>
							</tr>
							<tr>
 								<td><span style="width:60px;"><fmt:message key="last_year"/></span></td>
								<td><span style="width:60px;">系统预估</span></td>
 								<td><span style="width:60px;">最终预估</span></td>
 								<td><span style="width:60px;"><fmt:message key="last_year"/></span></td>
 								<td><span style="width:60px;">系统预估</span></td>
 								<!-- <fmt:message key="misboh_forecastreference"/>  <fmt:message key="adjustment"/>-->
 								<td><span style="width:60px;">最终预估</span></td>
 								<td><span style="width:60px;"><fmt:message key="last_year"/></span></td>
 								<td><span style="width:60px;">系统预估</span></td>
 								<td><span style="width:60px;">最终预估</span></td>
 								<td><span style="width:60px;"><fmt:message key="last_year"/></span></td>
 								<td><span style="width:60px;">系统预估</span></td>
 								<td><span style="width:60px;">最终预估</span></td>
 								<td><span style="width:60px;"><fmt:message key="last_year"/></span></td>
 								<td><span style="width:60px;">系统预估</span></td>
 								<td><span style="width:60px;">最终预估</span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>	
							<c:forEach var="posSalePlan" items="${posSalePlanList}" varStatus="status">
								<tr <c:if test="${posSalePlan.trueorfalse == 1}">style="color:#B7B7B7;"</c:if>>
									<td class="num"><span style="width:15px;">${status.index+1}
									</span></td>
									<td>
										<c:choose>
											<c:when test="${!empty posSalePlan.dat}">
												<span title="<fmt:formatDate value="${posSalePlan.dat}" pattern="yyyy-MM-dd"/>" style="width:80px;"><fmt:formatDate value="${posSalePlan.dat}" pattern="yyyy-MM-dd"/></span>
												<input type="hidden" name="show_id" id="trueorfalse${status.index+1}" value="${posSalePlan.trueorfalse}"/>
											</c:when>
											<c:otherwise>
												<span><fmt:message key="total"/></span>
											</c:otherwise>
										</c:choose>
									</td>
									<td><span title="${posSalePlan.week}" style="width:26px;">${posSalePlan.week}</span></td>
									<td><span title="${posSalePlan.isHoliday}" style="width:25px;">${posSalePlan.isHoliday}</span></td>
									<td><span title="${posSalePlan.memo}" style="width:50px;text-align: right;" class="form-input">
										<c:choose>
											<c:when test="${posSalePlan.trueorfalse == 1}">
												 ${posSalePlan.memo}
											</c:when>
											<c:otherwise>
												<input type="text" style="width:50px;border: 0px;" value="${posSalePlan.memo}"/>
											</c:otherwise>
										</c:choose>
									</span></td>
									<td><span title="${posSalePlan.last_money}" style="width:60px;text-align: right;"><fmt:formatNumber value="${posSalePlan.last_money}" pattern="##.##" minFractionDigits="2"/></span></td>
									<td><span title="${posSalePlan.money_y}" style="width:60px;text-align: right;"><fmt:formatNumber value="${posSalePlan.money_y}" pattern="##.##" minFractionDigits="2"/></span></td>
									<td class="textInput"><span title="${posSalePlan.money_t}" style="width:60px;text-align: right;">
									<c:choose>
										<c:when test="${posSalePlan.trueorfalse == 1}">
											 <fmt:formatNumber value="${posSalePlan.money_t}" pattern="##.##" minFractionDigits="2"/>
										</c:when>
										<c:otherwise>
												<input tiaozheng="tiaozheng" type="text" value="<fmt:formatNumber value="${posSalePlan.money_t}" pattern="##.##" minFractionDigits="2"/>" onchange="changeNum(7,${status.index})"; style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()"  onkeyup="this.value=this.value.replace(/[^\d\.\d{0,3}]/g,'')" onblur="checkNum(this);"/>
										</c:otherwise>
									</c:choose>
									</span></td>
<%-- 										<input tiaozheng=tiaozheng type="text" readonly="readonly" value="<fmt:formatNumber value="${posSalePlan.money_t}" pattern="##.##" minFractionDigits="2"/>" onchange="changeNum(8,${status.index})"; style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td> --%>
									
									<td><span title="${posSalePlan.last_percapita}" style="width:60px;text-align: right;"><fmt:formatNumber value="${posSalePlan.last_percapita}" pattern="##.##" minFractionDigits="2"/></span></td>
									<td><span title="${posSalePlan.percapita_y}" style="width:60px;text-align: right;"><fmt:formatNumber value="${posSalePlan.percapita_y}" pattern="##.##" minFractionDigits="2"/></span></td>
									<td class="textInput"><span title="${posSalePlan.percapita_t}" style="width:60px;text-align: right;">
<%-- 										<input tiaozheng=tiaozheng type="text" value="${posSalePlan.percapita_t}" onchange="changeNum(11,${status.index})"; style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/> --%>
										<fmt:formatNumber value="${posSalePlan.percapita_t}" pattern="##.##" minFractionDigits="2"/>
									</span></td>
									
									<td><span title="${posSalePlan.last_people}" style="width:60px;text-align: right;">${posSalePlan.last_people}</span></td>
									<td><span title="${posSalePlan.people_y}" style="width:60px;text-align: right;">${posSalePlan.people_y}</span></td>
									<td class="textInput"><span title="${posSalePlan.people_t}" style="width:60px;text-align: right;">
									<c:choose>
										<c:when test="${posSalePlan.trueorfalse == 1}">
											<input type="hidden" style="width:50px;border: 0px;"  value="${posSalePlan.people_t}"/>${posSalePlan.people_t}
										</c:when>
										<c:otherwise>
												<input tiaozheng="tiaozheng" onkeyup="value=value.replace(/[^\d]/g,'') " type="text" value="${posSalePlan.people_t}" onchange="changeNum(13,${status.index})"; style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/>
										</c:otherwise>
									</c:choose>
										<!--  <input tiaozheng=tiaozheng type="text" <c:if test="${posSalePlan.trueorfalse == 1}">readonly="readonly"</c:if> value="${posSalePlan.people_t}" onchange="changeNum(14,${status.index})"; style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/>-->
									</span></td>
									
									<td><span title="${posSalePlan.last_ac}" style="width:60px;text-align: right;"><fmt:formatNumber value="${posSalePlan.last_ac}" pattern="##.##" minFractionDigits="2"/></span></td>
									<td><span title="${posSalePlan.ac_y}" style="width:60px;text-align: right;"><fmt:formatNumber value="${posSalePlan.ac_y}" pattern="##.##" minFractionDigits="2"/></span></td>
									<td class="textInput"><span title="${posSalePlan.ac_t}" style="width:60px;text-align: right;">
<%-- 										<input tiaozheng=tiaozheng type="text" value="${posSalePlan.ac_t}" onchange="changeNum(17,${status.index})"; style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/> --%>
										<fmt:formatNumber value="${posSalePlan.ac_t}" pattern="##.##" minFractionDigits="2"/>
									</span></td>
									
									<td><span title="${posSalePlan.last_bills}" style="width:60px;text-align: right;">${posSalePlan.last_bills}</span></td>
									<td><span title="${posSalePlan.bills_y}" style="width:60px;text-align: right;">${posSalePlan.bills_y}</span></td>
									<td class="textInput"><span title="${posSalePlan.bills_t}" style="width:60px;text-align: right;">
									<c:choose>
										<c:when test="${posSalePlan.trueorfalse == 1}">
											<input type="hidden" style="width:50px;border: 0px;"  value="${posSalePlan.bills_t}"/>${posSalePlan.bills_t}
										</c:when>
										<c:otherwise>
												<input tiaozheng="tiaozheng" onkeyup="value=value.replace(/[^\d]/g,'') " type="text" value="${posSalePlan.bills_t}" onchange="changeNum(19,${status.index})"; style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/>
										</c:otherwise>
									</c:choose>
									
									<!-- 	<input tiaozheng=tiaozheng type="text" <c:if test="${posSalePlan.trueorfalse == 1}">readonly="readonly"</c:if> value="${posSalePlan.bills_t}" onchange="changeNum(20,${status.index})"; style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/> -->
									</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>		
		<div class="form-line">
			<div class="form-input" style="color:red;">
				【计算逻辑：营业额预估值(系统预估)=参考日期内同一周次(注：非节假日)营业额的平均值&nbsp;(人数、单数预估同理)】
			</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			focus() ;//页面获得焦点
			
			if($('.grid').find('.table-body').find('.num').size()>0){
				loadToolBar([true,true,true,true]);
			}else {
				loadToolBar([true,false,false,false]);
			}
			
// 		 	loadKey();
		 	// 修改界面周次显示  自动计算合计
// 		 	loadWeekAndTotal();

			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				});
			//自动实现滚动条 				
			setElementHeight('.grid',['.tool'],$(document.body),100);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();	
		});	
		// 加载按钮
		function loadToolBar(use){
			$('.tool').html('');
			$('.tool').toolbar({
				items: [{
					text: '<fmt:message key="select" />',
					title: '<fmt:message key="select" />',
					useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						$("#listForm").submit();
					}
				},'-',{
					text: '<fmt:message key="calculate" />',
					title: '<fmt:message key="calculate" />',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'calculate')}&&use[0],
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-80px','-0px']
					},
					handler: function(){
						calculate();
					}
				},{
					text: '<fmt:message key="save" />',
					title: '<fmt:message key="save" />',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[1],
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-80px','-0px']
					},
					handler: function(){
						saveUpdate();
					}
				},'-',{
					text: 'Excel',
					title: '导出Excel',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')}&&use[3],
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-40px','-20px']
					},
					handler: function(){
						$("#wait2").val('NO');//不用等待加载
						$('#listForm').attr('action','<%=path%>/forecastMis/exportCast.do');
						$('#listForm').submit();
						$('#listForm').attr('target','');
						$('#listForm').attr('action','<%=path%>/forecastMis/castList.do');
						$("#wait2").val('');//等待加载还原
					}
				},{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
					}
				}]
			});
		}
		
		
		
		//保存登记
		function saveUpdate(){
			var selected = {};
			var checkList = $("input[name='show_id']");
				if(confirm('数据将被更新，是否继续保存？')){
					checkList.each(function(i){
						var par = $(this).val();
						if(par != 1){
							selected['SalePlanList['+i+'].memo'] = $.trim($(this).parents('tr').find('td:eq(4)').find('input').val());
							selected['SalePlanList['+i+'].money_t'] = parseFloat($(this).parents('tr').find('td:eq(7)').find('input').val());
							selected['SalePlanList['+i+'].bills_t'] = parseInt($(this).parents('tr').find('td:eq(19)').find('input').val());
							selected['SalePlanList['+i+'].people_t'] = parseInt($(this).parents('tr').find('td:eq(13)').find('input').val());
						}else{
							selected['SalePlanList['+i+'].memo'] = $.trim($(this).parents('tr').find('td:eq(4)').find('span').text());
							selected['SalePlanList['+i+'].money_t'] = parseFloat($.trim($(this).parents('tr').find('td:eq(7)').find('span').text()));
							selected['SalePlanList['+i+'].bills_t'] = parseInt($.trim($(this).parents('tr').find('td:eq(19)').find('span').text()));
							selected['SalePlanList['+i+'].people_t'] = parseInt($.trim($(this).parents('tr').find('td:eq(13)').find('span').text()));
						}
						selected['SalePlanList['+i+'].firm'] = $('#firmId').val();
						selected['SalePlanList['+i+'].dat'] = $.trim($(this).parents('tr').find('td:eq(1)').find('span').attr('title'));
						selected['SalePlanList['+i+'].ac_t'] = parseFloat($.trim($(this).parents('tr').find('td:eq(16)').find('span').text()));
						selected['SalePlanList['+i+'].percapita_t'] = parseFloat($.trim($(this).parents('tr').find('td:eq(10)').find('span').text()));
						selected['SalePlanList['+i+'].isHoliday'] = $.trim($(this).parents('tr').find('td:eq(3)').find('span').text());
					});
					$.post('<%=path%>/forecastMis/updateSalePlan.do',selected,function(data){
						if(data ==1){
							alert('<fmt:message key="operation_successful" />!');
							$('#listForm').submit();
						}else{
							alert('<fmt:message key="operation_failed" />!');
						}
					});
				}
		}
		function DateDiff(sDate1, sDate2)
		{ 
		    var aDate, oDate1, oDate2, iDays;
		    aDate = sDate1.split("-");
		    oDate1 = new Date(aDate[1] + '/' + aDate[2] + '/' + aDate[0]); //转换为12-18-2002格式
		    aDate = sDate2.split("-");
		    oDate2 = new Date(aDate[1] + '/' + aDate[2] + '/' + aDate[0]);
		    iDays = parseInt((oDate1 - oDate2) / 1000 / 60 / 60 /24); //把相差的毫秒数转换为天数
		    return iDays;
		}
		function calculate(){
			if (confirm('确认按照参考日期对预估时间段内的营业预估值进行重新计算吗？')) {
				if(!$('#bdate').val()) {
					alert('<fmt:message key="please_enter"/><fmt:message key="scm_reference"/><fmt:message key="startdate"/>！');
					return;
				}
				if(!$('#edate').val()) {
					alert('<fmt:message key="please_enter"/><fmt:message key="scm_reference"/><fmt:message key="enddate"/>！');
					return;
				}
				if(!$('#startdate').val()) {
					alert('<fmt:message key="please_enter"/><fmt:message key="scm_estimated_date"/><fmt:message key="startdate"/>！');
					return;
				}
				if(!$('#enddate').val()) {
					alert('<fmt:message key="please_enter"/><fmt:message key="scm_estimated_date"/><fmt:message key="enddate"/>！');
					return;
				}
				var startdate = $('#startdate').val();
				var enddate = $('#enddate').val();
				//预估日期不能大于31天
				if(DateDiff(enddate,startdate)>30){
					alert('预估日期不能大于30天！');
					return;
				}
				var bdate = $('#bdate').val();
				var edate = $('#edate').val();
				//参考日期必须大于30天
				if(DateDiff(edate,bdate) < 6){
					alert('参考日期不能小于一周！');
					return;
				}
				var action = "<%=path%>/forecastMis/calPosSalePlan.do";
				$('#listForm').attr('action',action);
				$('#listForm').submit(); 
			}
		}
		// 检查最高最低库存是否有效数字
		function checkNum(inputObj){
			if(isNaN(inputObj.value)){
				alert("无效数字！");
				inputObj.focus();
				return false;
			}
			if(inputObj.value == ''){
				inputObj.value ="0.00";
			}
			
			var amt = $(inputObj).parents('tr').find('td:eq(7)').find('input').val()*1;
			var people = $(inputObj).parents('tr').find('td:eq(13)').find('input').val()*1;
			var bills = $(inputObj).parents('tr').find('td:eq(19)').find('input').val()*1;
			if(people != 0){
				var percapita_t = amt/people;
				$(inputObj).parents('tr').find('td:eq(10)').find('span').text(addNum(percapita_t));
			}
			if(bills != 0){
				var ac_t = amt/bills;
				$(inputObj).parents('tr').find('td:eq(16)').find('span').text(addNum(ac_t));
			}
		}
		//计算列与行合计
		function changeNum(n,b){
				var numSum = 0;
				var tousNum = 0;
				var checkList = $("input[name='show_id']");
			//列合计
			checkList.each(function (){
				var num1 =  0;
				if($(this).val()!=1){
					num1 = $(this).parents('tr').find("td:eq("+n+")").find('input').val();
				}else{
					num1 = $(this).parents('tr').find("td:eq("+n+")").find('span').text();
				}
				if(num1 == '' ){
					num1 = '0.00';
				}
				if(n == 7){
					numSum += parseFloat(num1);
				}else{
					numSum += parseInt(num1);
				}
			})
			if(n == 7){
				$("#tblGrid").find('tfoot').find('tr').find('td:eq('+(n-2)+')').find('span').text(addNum(numSum));
			}else{
				$("#tblGrid").find('tfoot').find('tr').find('td:eq('+(n-2)+')').find('span').text(numSum);
			}
			//行合计
			/* $("#tblGrid").find("tbody").find("tr:eq("+b+")").find('td').find('[tiaozheng=tiaozheng]').each(function (){
				tousNum+=parseInt($(this).val());
			$("#tblGrid").find("tbody").find("tr:eq("+b+")").find('[span=span]').html(tousNum);
			$("#tblGrid").find("tbody").find("tr:eq("+b+")").find('[span=span]').attr('title',tousNum);
			})
			var tousSum = 0;
			$("#tblGrid").find('tfoot').find('tr').find('[tous=tous]').each(function(){
				tousSum+=parseInt($(this).val());
			})
			$("#tblGrid").find('tfoot').find('tr').find('td:eq(12)').text(tousSum);*/
		}
		
		function calAll(){
			var xishu = $('#xishu').val();
			if(isNaN(xishu)){
				alert("系数无效！");
				$('#xishu').val(1);
				return false;
			}
			var total = 0;
			$('.table-body').find('tbody').find('tr').each(function(i){
				var showid = $(this).find("input[name='show_id']").val();
				if(showid != 1){
					var input = $(this).find('td:eq(7) input');
					var oldvalue = Number(input.val());
					var value = (oldvalue*xishu).toFixed(2);
					input.val(value);
					total += value - oldvalue;
					var people = $(this).find('td:eq(13)').find('input').val();
					var bills = $(this).find('td:eq(19)').find('input').val();
					if(people != 0){
						var percapita_t = value/people;
						$(this).find('td:eq(10)').find('span').text(addNum(percapita_t));
					}
					if(bills != 0){
						var ac_t = value/bills;
						$(this).find('td:eq(16)').find('span').text(addNum(ac_t));
					}
				}
			});
			var totalamt = $("#tblGrid").find('tfoot').find('tr').find('td:eq(5)').find('span');
			totalamt.text((Number(totalamt.text()) + total).toFixed(2)).attr('title',totalamt+total);
			$('#xishu').val(1);
		}
		
		// 修改界面日期显示 周次
		function loadWeekAndTotal(){
			var a = [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0];//存放合计值
			var checkList = $("input[name='show_id']");
		 	checkList.each(function(){
				// 自动计算是否假期
// 				var week=$(this).parents('tr').find('td:eq(2)').find('span').attr('title');
// 				switch (week) {
// 					case '一' :
// 						$(this).parents('tr').find('td:eq(3)').find('span').text('否');
// 						break;
// 					case '二' :
// 						$(this).parents('tr').find('td:eq(3)').find('span').text('否');
// 						break;
// 					case '三' :
// 						$(this).parents('tr').find('td:eq(3)').find('span').text('否');
// 						break;
// 					case '四' :
// 						$(this).parents('tr').find('td:eq(3)').find('span').text('否');
// 						break;
// 					case '五' :
// 						$(this).parents('tr').find('td:eq(3)').find('span').text('否');
// 						break;
// 					case '六' :
// 						$(this).parents('tr').find('td:eq(3)').find('span').text('是');
// 						break;
// 					case '日' :
// 						$(this).parents('tr').find('td:eq(3)').find('span').text('是');
// 						break;
// 				}
				// 自动计算合计
				if($(this).val()!=1){
					a[0]+=$(this).parents('tr').find('td:eq(5)').find('span').attr('title')*1;
					a[1]+=$(this).parents('tr').find('td:eq(6)').find('span').attr('title')*1;
					a[2]+=$(this).parents('tr').find('td:eq(7)').find('input').val()*1;
					a[3]+=$(this).parents('tr').find('td:eq(8)').find('span').attr('title')*1;
					a[4]+=$(this).parents('tr').find('td:eq(9)').find('span').attr('title')*1;
					a[5]+=$(this).parents('tr').find('td:eq(10)').find('span').text()*1;
					a[6]+=$(this).parents('tr').find('td:eq(11)').find('span').attr('title')*1;
					a[7]+=$(this).parents('tr').find('td:eq(12)').find('span').attr('title')*1;
					a[8]+=$(this).parents('tr').find('td:eq(13)').find('input').val()*1;
					a[9]+=$(this).parents('tr').find('td:eq(14)').find('span').attr('title')*1;
					a[10]+=$(this).parents('tr').find('td:eq(15)').find('span').attr('title')*1;
					a[11]+=$(this).parents('tr').find('td:eq(16)').find('span').text()*1;
					a[12]+=$(this).parents('tr').find('td:eq(17)').find('span').attr('title')*1;
					a[13]+=$(this).parents('tr').find('td:eq(18)').find('span').attr('title')*1;
					a[14]+=$(this).parents('tr').find('td:eq(19)').find('input').val()*1;
				}else{
					a[0]+=$(this).parents('tr').find('td:eq(5)').find('span').attr('title')*1;
					a[1]+=$(this).parents('tr').find('td:eq(6)').find('span').attr('title')*1;
					a[2]+=$(this).parents('tr').find('td:eq(7)').find('span').text()*1;
					a[3]+=$(this).parents('tr').find('td:eq(8)').find('span').attr('title')*1;
					a[4]+=$(this).parents('tr').find('td:eq(9)').find('span').attr('title')*1;
					a[5]+=$(this).parents('tr').find('td:eq(10)').find('span').text()*1;
					a[6]+=$(this).parents('tr').find('td:eq(11)').find('span').attr('title')*1;
					a[7]+=$(this).parents('tr').find('td:eq(12)').find('span').attr('title')*1;
					a[8]+=$(this).parents('tr').find('td:eq(13)').find('span').text()*1;
					a[9]+=$(this).parents('tr').find('td:eq(14)').find('span').attr('title')*1;
					a[10]+=$(this).parents('tr').find('td:eq(15)').find('span').attr('title')*1;
					a[11]+=$(this).parents('tr').find('td:eq(16)').find('span').text()*1;
					a[12]+=$(this).parents('tr').find('td:eq(17)').find('span').attr('title')*1;
					a[13]+=$(this).parents('tr').find('td:eq(18)').find('span').attr('title')*1;
					a[14]+=$(this).parents('tr').find('td:eq(19)').find('span').text()*1;
				}
			});
			if(0==checkList.size() || !checkList.size()) {
				$('tfoot').hide();
			} 
			$('tfoot').find('tr').find('td:eq(3)').find('span').text(addNum(a[0]));
			$('tfoot').find('tr').find('td:eq(4)').find('span').text(addNum(a[1]));
			$('tfoot').find('tr').find('td:eq(5)').find('span').text(addNum(a[2]));
		/*  $('tfoot').find('tr').find('td:eq(6)').find('span').text(a[3]);
			$('tfoot').find('tr').find('td:eq(7)').find('span').text(a[4]);
			$('tfoot').find('tr').find('td:eq(8)').find('span').text(a[5]);*/
			$('tfoot').find('tr').find('td:eq(9)').find('span').text(a[6]);
			$('tfoot').find('tr').find('td:eq(10)').find('span').text(a[7]);
			$('tfoot').find('tr').find('td:eq(11)').find('span').text(a[8]);
			/*	$('tfoot').find('tr').find('td:eq(12)').find('span').text(a[9]);
			$('tfoot').find('tr').find('td:eq(13)').find('span').text(a[10]);
			$('tfoot').find('tr').find('td:eq(14)').find('span').text(a[11]);*/
			$('tfoot').find('tr').find('td:eq(15)').find('span').text(a[12]);
			$('tfoot').find('tr').find('td:eq(16)').find('span').text(a[13]);
			$('tfoot').find('tr').find('td:eq(17)').find('span').text(a[14]);
		}
		
		//按钮快捷键
		function loadKey(){
			$(document).bind('keydown',function(e){//按钮快捷键
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
		 		if(window.event && window.event.keyCode == 118) { 
			 		window.event.keyCode = 505; 
		 		} 
		 		if(window.event && window.event.keyCode == 505){ 
		 			window.event.returnValue=false; 
		 		}; 
		 		if(e.altKey ==false){
		 			return;
		 		}
		 		switch (e.keyCode) {
	                case 70: $('#autoId-button-101').click(); break;
	                case 69: $('#autoId-button-102').click(); break;
	                case 83: $('#autoId-button-103').click(); break;
	                case 67: $('#autoId-button-104').click(); break;
	                case 68: $('#autoId-button-105').click(); break;
					case 80: $('#autoId-button-106').click(); break;
	            }
			});
		}
		
		function addNum(_val){
			return Number(_val).toFixed(2);
		}
		</script>
	</body>
</html>