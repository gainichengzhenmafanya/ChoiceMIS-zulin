<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="reported_acceptance"/>--菜品预估实际对比</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>	
		<style type="text/css">
			.page{margin-bottom: 25px;}
			.onEdit{
				border:1px solid;
				border-bottom-color: blue;
				border-top-color: blue;
				border-left-color: blue;
				border-right-color: blue;
			}
			.input{
				background:transparent;
				border:0px solid;
			}
			a.l-btn-plain{
				border:1px solid #7eabcd; 
				height:22px;
			}
			.search{
				margin-top:-2px;
				cursor: pointer;
			}
			.textInput span {
				padding:0px;
			}
			.textInput input {
				border:0px;
				width:55px;
			}
		</style>
		<script type="text/javascript">
			var path="<%=path%>";
		</script>	
	</head>
	<body> 	
		<div class="tool"></div>
		<%--当前登录用户 --%>	
		<form id="listForm" action="<%=path%>/forecast/costCmpList.do" method="post">
		<input type="hidden" id="mis" name="mis" value="${mis}"/>	
		<input type="hidden" id="msg" name="msg" value="${msg}"/>	
			<div class="form-line">	
				<div class="form-label"><fmt:message key="scm_estimated_date"/><fmt:message key="date"/>:</div>
				<div class="form-input" style="width:190px;">
					<font style="color:blue;">从:</font>
					<input type="text" style="width:90px;" id="startdate" name="startdate" value="<fmt:formatDate value="${startdate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'enddate\')}'});"/>
					<font style="color:blue;"><fmt:message key="to"/>:</font>
					<input type="text" style="width:90px;" id="enddate" name="enddate" value="<fmt:formatDate value="${enddate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'startdate\')}'});"/>
				</div>
				<div class="form-label">仓位</div>
				<div class="form-input">
					<input type="text" name="deptdes" id="deptdes" style="width: 136px;margin-top: -3px;" autocomplete="off" class="text" value="<c:out value="${posItemPlan.deptdes}" />"/>
					<img id="seachDept" class="search" src="<%=path%>/image/themes/icons/search.png" alt='查询仓位' />
					<input type="hidden" id="dept" name="dept" value=""/>
				</div>
			</div>	
		   		<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num"><span style="width:22px;">&nbsp;</span></td>
								<td rowspan="2"><span style="width:50px;"><fmt:message key="coding"/></span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="name"/></span></td>
								<td rowspan="2"><span style="width:25px;"><fmt:message key="unit"/></span></td>
								
								<td colspan="5"><span>一班</span></td>
 								<td colspan="5"><span>二班</span></td>
 								<td colspan="5"><span>三班</span></td>
 								<td colspan="5"><span>四班</span></td>
 								<td colspan="5"><span><fmt:message key="total"/></span></td>
							</tr>
							<tr>
								<td><span style="width:60px;"><fmt:message key="scm_estimated_date"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="actual"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="scm_ygcyl"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="scm_tzcyl"/></span></td>
 								
 								<td><span style="width:60px;"><fmt:message key="scm_estimated_date"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="actual"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="scm_ygcyl"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="scm_tzcyl"/></span></td>
 								
 								<td><span style="width:60px;"><fmt:message key="scm_estimated_date"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="actual"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="scm_ygcyl"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="scm_tzcyl"/></span></td>
 								
 								<td><span style="width:60px;"><fmt:message key="scm_estimated_date"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="actual"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="scm_ygcyl"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="scm_tzcyl"/></span></td>
 								
 								<td><span style="width:60px;"><fmt:message key="scm_estimated_date"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="actual"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="scm_ygcyl"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="scm_tzcyl"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>	
							<c:forEach var="posItemPlan" items="${posItemPlanList}" varStatus="status">
								<tr>
									<td class="num"><span style="width:22px;">${status.index+1}</span></td>
									<td><span title="${posItemPlan.itcode}" style="width:50px;">${posItemPlan.itcode}</span></td>
									<td><span title="${posItemPlan.itdes }" style="width:80px;">${posItemPlan.itdes }</span></td>
									<td><span title="${posItemPlan.itunit }" style="width:25px;">${posItemPlan.itunit }</span></td>
									
									<td><span title="${posItemPlan.cnt1old }" style="width:60px;text-align: right;">${posItemPlan.cnt1old }</span></td>
									<td><span title="${posItemPlan.cnt1 }" style="width:60px;text-align: right;">${posItemPlan.cnt1 }</span></td>
									<td><span title="${posItemPlan.cnt1act }" style="width:60px;text-align: right;">${posItemPlan.cnt1act }</span></td>
									<td><span title="${posItemPlan.lv1 }" style="width:60px;text-align: right;">${posItemPlan.lv1 }</span></td>
									<td><span title="${posItemPlan.lv1old }" style="width:60px;text-align: right;">${posItemPlan.lv1old }</span></td>

									<td><span title="${posItemPlan.cnt2old }" style="width:60px;text-align: right;">${posItemPlan.cnt2old }</span></td>
									<td><span title="${posItemPlan.cnt2 }" style="width:60px;text-align: right;">${posItemPlan.cnt2 }</span></td>
									<td><span title="${posItemPlan.cnt2act }" style="width:60px;text-align: right;">${posItemPlan.cnt2act }</span></td>
									<td><span title="${posItemPlan.lv2 }" style="width:60px;text-align: right;">${posItemPlan.lv2 }</span></td>
									<td><span title="${posItemPlan.lv2old }" style="width:60px;text-align: right;">${posItemPlan.lv2old }</span></td>
									
									<td><span title="${posItemPlan.cnt3old }" style="width:60px;text-align: right;">${posItemPlan.cnt3old }</span></td>
									<td><span title="${posItemPlan.cnt3 }" style="width:60px;text-align: right;">${posItemPlan.cnt3 }</span></td>
									<td><span title="${posItemPlan.cnt3act }" style="width:60px;text-align: right;">${posItemPlan.cnt3act }</span></td>
									<td><span title="${posItemPlan.lv3 }" style="width:60px;text-align: right;">${posItemPlan.lv3 }</span></td>
									<td><span title="${posItemPlan.lv3old }" style="width:60px;text-align: right;">${posItemPlan.lv3old }</span></td>
									
									<td><span title="${posItemPlan.cnt4old }" style="width:60px;text-align: right;">${posItemPlan.cnt4old }</span></td>
									<td><span title="${posItemPlan.cnt4 }" style="width:60px;text-align: right;">${posItemPlan.cnt4 }</span></td>
									<td><span title="${posItemPlan.cnt4act }" style="width:60px;text-align: right;">${posItemPlan.cnt4act }</span></td>
									<td><span title="${posItemPlan.lv4 }" style="width:60px;text-align: right;">${posItemPlan.lv4 }</span></td>
									<td><span title="${posItemPlan.lv4old }" style="width:60px;text-align: right;">${posItemPlan.lv4old }</span></td>
									
									<td><span title="${posItemPlan.totalold }" style="width:60px;text-align: right;">${posItemPlan.totalold }</span></td>
									<td><span title="${posItemPlan.total }" style="width:60px;text-align: right;">${posItemPlan.total }</span></td>
									<td><span title="${posItemPlan.totalact }" style="width:60px;text-align: right;">${posItemPlan.totalact }</span></td>
									<td><span title="${posItemPlan.totallv }" style="width:60px;text-align: right;">${posItemPlan.totallv }</span></td>
									<td><span title="${posItemPlan.totallvold }" style="width:60px;text-align: right;">${posItemPlan.totallvold }</span></td>
									
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript">
		
		$(document).ready(function(){	
			focus() ;//页面获得焦点
			if ("a"!=$("#msg").val()) {
				alert($("#msg").val());
			}
			if($('.grid').find('.table-body').find('.num').size()>0){
				loadToolBar([true,true,true]);
			}else {
				loadToolBar([true,false,false]);
			}
		 	$(document).bind('keydown',function(e){//按钮快捷键
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
		 		if(window.event && window.event.keyCode == 118) { 
			 		window.event.keyCode = 505; 
		 		} 
		 		if(window.event && window.event.keyCode == 505){ 
		 			window.event.returnValue=false; 
		 		}; 
		 		if(e.altKey ==false){
		 			return;
		 		}
		 		switch (e.keyCode) {
	                case 70: $('#autoId-button-101').click(); break;
	                case 69: $('#autoId-button-102').click(); break;
	                case 83: $('#autoId-button-103').click(); break;
	                case 67: $('#autoId-button-104').click(); break;
	                case 68: $('#autoId-button-105').click(); break;
					case 80: $('#autoId-button-106').click(); break;
	            }
			});
		 	//排序结束
			$('#bdate').bind('click',function(){
				new WdatePicker();
			});
			$('#edate').bind('click',function(){
				new WdatePicker();
			});

			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
			function loadToolBar(use){
				$('.tool').html('');
				$('.tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />(<u>F</u>)',
						title: '查询',
						useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							$("#listForm").submit();
						}
					},{
						text: '<fmt:message key="print" />(<u>P</u>)',
						title: '打印',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[1],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							$("#wait2").val('NO');//不用等待加载
							$('#listForm').attr('target','report');
							window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
							var action="<%=path%>/forecast/printCostCmp.do";
							$('#listForm').attr('action',action);
							$('#listForm').submit();
							$('#listForm').attr('target','');
							$('#listForm').attr('action','<%=path%>/forecast/costCmpList.do');
							$("#wait2").val('');//等待加载还原
						}
					},{
						text: 'Excel',
						title: '导出Excel',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')}&&use[2],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$("#wait2").val('NO');//不用等待加载
							$('#listForm').attr('action','<%=path%>/forecast/exportCostCmp.do');
							$('#listForm').submit();
							$('#listForm').attr('target','');
							$('#listForm').attr('action','<%=path%>/forecast/costCmpList.do');
							$("#wait2").val('');//等待加载还原
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}]
				});
			}
			
			//自动实现滚动条 				
			setElementHeight('.grid',['.tool'],$(document.body),55);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();		
			
  	 		/*弹出树*/
			$('#seachDept').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#dept').val();
					var defaultName = $('#deptdes').val();
					//alert(defaultCode+"==="+defaultName);
					var offset = getOffset('dept');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/findPositnSuper.do?typn='+'7&iffirm=1&mold='+'oneTone&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deptdes'),$('#dept'),'760','520','isNull');
				}
			});
			
		});	
		</script>
	</body>
</html>