<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<title>Insert title here</title>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<style type="text/css">
				.tr-select{
					background-color: #D2E9FF;
				}
				.separator ,div , .pgSearchInfo{
					display: none;
				}
				div[class]{
					display:block;
				}
				.tr-select{
					background-color: #D2E9FF;
				}
				.grid td span{
					padding:0px;
				}
				.page{
					margin-bottom: 25px;
				}
			</style>
	</head>
	<body>
		<div class="form">
			<form id="addItemFrame" action="<%=path%>/deliver/addSupplyBatch.do" method="post">
			<div class="grid" style="overflow: auto;">
				<div class="table-head">
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span class="num" style="width: 25px;">&nbsp;</span></td>
								<td><span style="width:20px;">
									<input type="checkbox" id="chkAll"/></span>
								</td>
								<td><span style="width:100px;"><fmt:message key="scm_pubitem_code" /></span></td>
								<td><span style="width:140px;"><fmt:message key="scm_pubitem_name" /></span></td>
								<td><span style="width:60px;"><fmt:message key="abbreviation" /></span></td>
								<td><span style="width:60px;"><fmt:message key="unit" /></span></td>
								<td><span style="width:120px;"><fmt:message key="sector" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body" style="height: 100%;overflow: auto;">
					<table cellspacing="0" cellpadding="0" id="tblGrid">
						<tbody>
							<c:forEach var="pubitem" items="${itemList}" varStatus="status">
								<tr id="${pubitem.vcode}" data-deptcode="${pubitem.deptcode }">
									<td><span class="num" name="num_id" style="width: 25px;">${status.index+1}</span></td>
									<td><span style="width:20px; text-align: center;">
										<input type="checkbox" name="idList" name="code" id="chk_${pubitem.vcode}" value="${pubitem.vcode}"/>
										</span>
									</td>
									<td><span title="${pubitem.vcode}" style="width:100px;">${pubitem.vcode}</span></td>
									<td><span title="${pubitem.vname}" style="width:140px;">${pubitem.vname}</span></td>
									<td><span title="${pubitem.vinit}" style="width:60px;">${pubitem.vinit}</span></td>
									<td><span title="${pubitem.unitname}" style="width:60px;">${pubitem.unitname}</span></td>
									<td><span title="${pubitem.deptname}" style="width:120px;">${pubitem.deptname}</span></td>
<%-- 									<td><span title="${pubitem.deptcode}" style="width:120px;">${pubitem.deptcode}&nbsp;</span></td> --%>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		</div>  
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),30);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }
				     else
				     {
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				 });
				var checkboxList = $('.grid').find('.table-body').find('tr').find('input');
				checkboxList.each(function(){
					// alert($(this).val());
					var code = $(this).val();
					var _this = $(this);
					var select = parent.window.getItem();
					for(var k=0;k<select.length;k++){
						if(code == select[k]){
							// $('.grid').find('.table-body').find('tr.eq('+code+')').remove('td');
							$("#"+code).remove();
							break;
						}
					}
				});
				var numList = $("span[name='num_id']");
				var index = 1;
				numList.each(function(){
					$(this).text(index++);
				});
				
				
			});
			function addItem(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() == 1){
					var data = {entity:[]};
					checkboxList.filter(':checked').each(function(){
						var entity = {};
						var row = $(this).closest('tr');
						entity.vcode = $.trim(row.children('td:eq(2)').text());
						entity.vname = $.trim(row.children('td:eq(3)').text());
						entity.vinit = $.trim(row.children('td:eq(4)').text());
						entity.vunit = $.trim(row.children('td:eq(5)').text());
						entity.deptcode = $.trim(row.attr('data-deptcode'));
						data.entity.push(entity);
					});
					parent.window.addItem(data);
					parent.$(".close").click();
				} else if(checkboxList && checkboxList.filter(':checked').size() > 1) {
					alert('请选择一条菜品数据！');
					return ;
				} else{
					alert('请选择需要添加的菜品！');
					return ;
				}
			}
		</script>
</body>
</html>