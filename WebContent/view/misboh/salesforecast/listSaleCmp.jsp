<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>	
			<style type="text/css">			
			.onEdit{
				border:1px solid;
				border-bottom-color: blue;
				border-top-color: blue;
				border-left-color: blue;
				border-right-color: blue;
			}
			.input{
				background:transparent;
				border:0px solid;
			}
			a.l-btn-plain{
				border:1px solid #7eabcd; 
				height:22px;
			}
			.search{
				margin-top:-2px;
				cursor: pointer;
			}
			.textInput span {
				padding:0px;
			}
			.textInput input {
				border:0px;
				width:60px;
			}
		</style>
		<script type="text/javascript">
			var path="<%=path%>";
		</script>	
	</head>
	<body>	
		<div class="tool"></div>
		<%--当前登录用户 --%>	
		<form id="listForm" action="<%=path%>/forecast/saleCmpList.do" method="post">	
		<input type="hidden" id="mis" name="mis" value="${mis}"/>	
		<input type="hidden" id="msg" name="msg" value="${msg}"/>
			<div class="form-line">	
				<div class="form-label"><fmt:message key="scm_estimated_date"/><fmt:message key="date"/>:</div>
				<div class="form-input" style="width:190px;">
					<font style="color:blue;">从:</font>
					<input type="text" style="width:90px;" id="startdate" name="startdate" value="<fmt:formatDate value="${startdate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'enddate\')}'});"/>
					<font style="color:blue;"><fmt:message key="to"/>:</font>
					<input type="text" style="width:90px;" id="enddate" name="enddate" value="<fmt:formatDate value="${enddate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'startdate\')}'});"/>
				</div>
				<div class="form-label">仓位</div>
				<div class="form-input">
					<input type="text" name="deptdes" id="deptdes" style="width: 136px;margin-top: -3px;" autocomplete="off" class="text" value="<c:out value="${posSalePlan.deptdes}" />"/>
					<img id="seachDept" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
					<input type="hidden" id="dept" name="dept" value=""/>
				</div>
			</div>	
		   		<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num"><span style="width:22px;">&nbsp;</span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="date"/></span></td>
								<td rowspan="2"><span style="width:40px;"><fmt:message key="scm_weeks"/></span></td>
								<td rowspan="2"><span style="width:40px;"><fmt:message key="scm_holidays"/></span></td>
								<td rowspan="2"><span style="width:50px;"><fmt:message key="special_events"/></span></td>
								
								<td colspan="5"><span>一班</span></td>
 								<td colspan="5"><span>二班</span></td>
 								<td colspan="5"><span>三班</span></td>
 								<td colspan="5"><span>四班</span></td>
 								<td colspan="5"><span><fmt:message key="total"/></span></td>
							</tr>
							<tr>
								<td><span style="width:60px;"><fmt:message key="scm_estimated_date"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="actual"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="scm_ygcyl"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="scm_tzcyl"/></span></td>
 								
 								<td><span style="width:60px;"><fmt:message key="scm_estimated_date"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="actual"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="scm_ygcyl"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="scm_tzcyl"/></span></td>
 								
 								<td><span style="width:60px;"><fmt:message key="scm_estimated_date"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="actual"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="scm_ygcyl"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="scm_tzcyl"/></span></td>
 								
 								<td><span style="width:60px;"><fmt:message key="scm_estimated_date"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="actual"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="scm_ygcyl"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="scm_tzcyl"/></span></td>
 								
 								<td><span style="width:60px;"><fmt:message key="scm_estimated_date"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="actual"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="scm_ygcyl"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="scm_tzcyl"/></span></td>
 								
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>	
							<c:forEach var="posSalePlan" items="${posSalePlanList}" varStatus="status">
								<tr>
									<td class="num"><span style="width:22px;">${status.index+1}</span></td>
									<td><span title="<fmt:formatDate value="${posSalePlan.dat}" pattern="yyyy-MM-dd"/>" style="width:80px;"><fmt:formatDate value="${posSalePlan.dat}" pattern="yyyy-MM-dd"/></span></td>
									<td><span title="${posSalePlan.week }" style="width:40px;">${posSalePlan.week }</span></td>
									<td class="textInput"><span title="" style="width:40px;">
										<input type="text" onfocus="this.blur()"  readonly="readonly" style="width:40px;text-align: middle;padding: 0px;" /></span></td>
									<td><span title="${posSalePlan.memo }" style="width:50px;">${posSalePlan.memo }</span></td>
									<td><span title="${posSalePlan.pax1old }" style="width:60px;text-align: right;">${posSalePlan.pax1old }</span></td>
									<td><span title="${posSalePlan.pax1 }" style="width:60px;text-align: right;">${posSalePlan.pax1 }</span></td>
									<td><span title="${posSalePlan.pax1act }" style="width:60px;text-align: right;">${posSalePlan.pax1act }</span></td>
									<td><span title="${posSalePlan.lv1 }" style="width:60px;text-align: right;">${posSalePlan.lv1 }</span></td>
									<td><span title="${posSalePlan.lv1old }" style="width:60px;text-align: right;">${posSalePlan.lv1old }</span></td>

									<td><span title="${posSalePlan.pax2old }" style="width:60px;text-align: right;">${posSalePlan.pax2old }</span></td>
									<td><span title="${posSalePlan.pax2 }" style="width:60px;text-align: right;">${posSalePlan.pax2 }</span></td>
									<td><span title="${posSalePlan.pax2act }" style="width:60px;text-align: right;">${posSalePlan.pax2act }</span></td>
									<td><span title="${posSalePlan.lv2 }" style="width:60px;text-align: right;">${posSalePlan.lv2 }</span></td>
									<td><span title="${posSalePlan.lv2old }" style="width:60px;text-align: right;">${posSalePlan.lv2old }</span></td>
									
									<td><span title="${posSalePlan.pax3old }" style="width:60px;text-align: right;">${posSalePlan.pax3old }</span></td>
									<td><span title="${posSalePlan.pax3 }" style="width:60px;text-align: right;">${posSalePlan.pax3 }</span></td>
									<td><span title="${posSalePlan.pax3act }" style="width:60px;text-align: right;">${posSalePlan.pax3act }</span></td>
									<td><span title="${posSalePlan.lv3 }" style="width:60px;text-align: right;">${posSalePlan.lv3 }</span></td>
									<td><span title="${posSalePlan.lv3old }" style="width:60px;text-align: right;">${posSalePlan.lv3old }</span></td>
									
									<td><span title="${posSalePlan.pax4old }" style="width:60px;text-align: right;">${posSalePlan.pax4old }</span></td>
									<td><span title="${posSalePlan.pax4 }" style="width:60px;text-align: right;">${posSalePlan.pax4 }</span></td>
									<td><span title="${posSalePlan.pax4act }" style="width:60px;text-align: right;">${posSalePlan.pax4act }</span></td>
									<td><span title="${posSalePlan.lv4 }" style="width:60px;text-align: right;">${posSalePlan.lv4 }</span></td>
									<td><span title="${posSalePlan.lv4old }" style="width:60px;text-align: right;">${posSalePlan.lv4old }</span></td>
									
									<td><span title="${posSalePlan.totalold }" style="width:60px;text-align: right;">${posSalePlan.totalold }</span></td>
									<td><span title="${posSalePlan.total }" style="width:60px;text-align: right;">${posSalePlan.total }</span></td>
									<td><span title="${posSalePlan.totalact }" style="width:60px;text-align: right;">${posSalePlan.totalact }</span></td>
									<td><span title="${posSalePlan.totallv }" style="width:60px;text-align: right;">${posSalePlan.totallv }</span></td>
									<td><span title="${posSalePlan.totallvold }" style="width:60px;text-align: right;">${posSalePlan.totallvold }</span></td>
									
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){	
			focus() ;//页面获得焦点
			if ("a"!=$("#msg").val()) {
				alert($("#msg").val());
			}
			
			if($('.grid').find('.table-body').find('.num').size()>0){
				loadToolBar([true,true,true]);
			}else {
				loadToolBar([true,false,false]);
			}
			
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			checkboxList.each(function(){
				// 自动计算是否假期
				var week=$(this).parents('tr').find('td:eq(3)').find('span').attr('title');
				switch (week) {
					case '星期一' :
						$(this).parents('tr').find('td:eq(4)').find('span').find('input').val('<fmt:message key="no1" />');
						break;
					case '星期二' :
						$(this).parents('tr').find('td:eq(4)').find('span').find('input').val('<fmt:message key="no1" />');
						break;
					case '星期三' :
						$(this).parents('tr').find('td:eq(4)').find('span').find('input').val('<fmt:message key="no1" />');
						break;
					case '星期四' :
						$(this).parents('tr').find('td:eq(4)').find('span').find('input').val('<fmt:message key="no1" />');
						break;
					case '星期五' :
						$(this).parents('tr').find('td:eq(4)').find('span').find('input').val('<fmt:message key="no1" />');
						break;
					case '星期六' :
						$(this).parents('tr').find('td:eq(4)').find('span').find('input').val('<fmt:message key="be" />');
						break;
					case '星期日' :
						$(this).parents('tr').find('td:eq(4)').find('span').find('input').val('<fmt:message key="be" />');
						break;
				}
			});
		 	$(document).bind('keydown',function(e){//按钮快捷键
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
		 		if(window.event && window.event.keyCode == 118) { 
			 		window.event.keyCode = 505; 
		 		} 
		 		if(window.event && window.event.keyCode == 505){ 
		 			window.event.returnValue=false; 
		 		}; 
		 		if(e.altKey ==false){
		 			return;
		 		}
		 		switch (e.keyCode) {
	                case 70: $('#autoId-button-101').click(); break;
	                case 69: $('#autoId-button-102').click(); break;
	                case 83: $('#autoId-button-103').click(); break;
	                case 67: $('#autoId-button-104').click(); break;
	                case 68: $('#autoId-button-105').click(); break;
					case 80: $('#autoId-button-106').click(); break;
	            }
			});
		 	//排序结束
			$('#bdate').bind('click',function(){
			new WdatePicker();
			});
			$('#edate').bind('click',function(){
				new WdatePicker();
			});

			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
		 	//控制按钮显示
			function loadToolBar(use){
				$('.tool').html('');
				$('.tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />(<u>F</u>)',
						title: '<fmt:message key="select" />',
						useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							$("#listForm").submit();
						}
					},{
						text: '<fmt:message key="print" />(<u>P</u>)',
						title: '<fmt:message key="print" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[1],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							$("#wait2").val('NO');//不用等待加载
							$('#listForm').attr('target','report');
							window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
							var action="<%=path%>/forecast/printSaleCmp.do";
							$('#listForm').attr('action',action);
							$('#listForm').submit();
							$('#listForm').attr('target','');
							$('#listForm').attr('action','<%=path%>/forecast/saleCmpList.do');
							$("#wait2").val('');//等待加载还原
						}
					},{
						text: 'Excel',
						title: 'Excel',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')}&&use[2],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$("#wait2").val('NO');//不用等待加载
							$('#listForm').attr('action','<%=path%>/forecast/exportSaleCmp.do');
							$('#listForm').submit();
							$('#listForm').attr('target','');
							$('#listForm').attr('action','<%=path%>/forecast/saleCmpList.do');
							$("#wait2").val('');//等待加载还原
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}]
				});
		 	}
			
			//自动实现滚动条 				
			setElementHeight('.grid',['.tool'],$(document.body),55);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();		
			
  	 		/*弹出树*/
			$('#seachDept').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#dept').val();
					var defaultName = $('#deptdes').val();
					//alert(defaultCode+"==="+defaultName);
					var offset = getOffset('dept');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/findPositnSuper.do?typn='+'7&iffirm=1&mold='+'oneTone&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deptdes'),$('#dept'),'760','520','isNull');
				}
			});
		});	
		</script>
	</body>
</html>