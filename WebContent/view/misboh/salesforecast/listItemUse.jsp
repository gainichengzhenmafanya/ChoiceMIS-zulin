<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>菜品点击率</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>	
		<style type="text/css">
			.page{margin-bottom: 25px;}
			.onEdit{
				border:1px solid;
				border-bottom-color: blue;
				border-top-color: blue;
				border-left-color: blue;
				border-right-color: blue;
			}
			.input{
				background:transparent;
				border:0px solid;
			}
			a.l-btn-plain{
				border:1px solid #7eabcd; 
				height:22px;
			}
			.search{
				margin-top:-2px;
				cursor: pointer;
			}
			.textInput span {
				padding:0px;
			}
			.textInput input {
				border:0px;
				width:55px;
			}
		</style>
		<script type="text/javascript">
			var path="<%=path%>";
		</script>					
	</head>
	<body>
		<div class="tool"></div>
		<%--当前登录用户 --%>	
		<form id="listForm" action="<%=path%>/itemUse/list.do" method="post">
			<input type="hidden" id="msg" name="msg" value="${msg}"/>
			<input type="hidden" id="isDeclare" name="isDeclare" value="${isDeclare}"/>	
			<input type="hidden" id="firm" name="firm" value="${itemUse.firm}"/>
			<input type="hidden" id="vplantyp" name="vplantyp" value="${itemUse.vplantyp}"/>
			<div class="form-line">
				<div class="form-label"><fmt:message key="the_reference_date"/>:</div>
				<div class="form-input" style="width:190px;">
					<input type="text" style="width:90px;margin-top: -3px;" id="bdate" name="bdate" value="<fmt:formatDate value="${itemUse.bdate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edate\')}'});"/>
					<font style="color:blue;"><fmt:message key="to"/></font>
					<input type="text" style="width:90px;margin-top: -3px;" id="edate" name="edate" value="<fmt:formatDate value="${itemUse.edate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdate\')}'});"/>
				</div>
<%-- 			 	<div class="form-label" style="margin-left:50px;"><fmt:message key="store"/>:</div> --%>
			</div>	
		   	<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num"><span style="width:30px;">&nbsp;</span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="scm_pubitem_code"/></span></td>
								<td rowspan="2"><span style="width:100px;"><fmt:message key="scm_pubitem_name"/></span></td>
								<td rowspan="2"><span style="width:45px;"><fmt:message key="unit"/></span></td>
								
								<td colspan="2"><span>星期一</span></td>
 								<td colspan="2"><span>星期二</span></td>
 								<td colspan="2"><span>星期三</span></td>
 								<td colspan="2"><span>星期四</span></td>
 								
 								<td colspan="2"><span>星期五</span></td>
 								<td colspan="2"><span>星期六</span></td>
 								<td colspan="2"><span>星期七</span></td>
 								<td colspan="2"><span>节假日</span></td>
							</tr>
							<tr>
								<td><span style="width:60px;"><fmt:message key="misboh_forecastreference"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="misboh_forecastreference"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="misboh_forecastreference"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="misboh_forecastreference"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td>
 								
 								<td><span style="width:60px;"><fmt:message key="misboh_forecastreference"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="misboh_forecastreference"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="misboh_forecastreference"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="misboh_forecastreference"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td>
 								
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>	
							<c:forEach var="itemUse" items="${itemUseList}" varStatus="status">
								<tr data-dept="${itemUse.dept }">
									<td class="num"><span style="width:30px;">${status.index+1}</span></td>
									<td><span title="${itemUse.itcode}" style="width:80px;">${itemUse.itcode}
									<input type="hidden" value="${itemUse.itcode}" name="show_id"/>
									</span></td>
									<td><span title="${itemUse.itdes}" style="width:100px;">${itemUse.itdes}</span></td>
									<td><span title="${itemUse.itunit}" style="width:45px;">${itemUse.itunit}</span></td>
									<td><span title="${itemUse.prev11}" style="width:60px;text-align: right;"><fmt:formatNumber value="${itemUse.prev11}" pattern="##.##" minFractionDigits="2"/></span></td>
									<td class="textInput"><span title="${itemUse.modv11}" style="width:60px;">
										<input type="text" value="<fmt:formatNumber value="${itemUse.modv11}" pattern="##.##" minFractionDigits="2"/>" style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
									
									<td><span title="${itemUse.prev21}" style="width:60px;text-align: right;"><fmt:formatNumber value="${itemUse.prev21}" pattern="##.##" minFractionDigits="2"/></span></td>
									<td class="textInput"><span title="${itemUse.modv21}" style="width:60px;">
										<input type="text" value="<fmt:formatNumber value="${itemUse.modv21}" pattern="##.##" minFractionDigits="2"/>" style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
									
									<td><span title="${itemUse.prev31}" style="width:60px;text-align: right;"><fmt:formatNumber value="${itemUse.prev31}" pattern="##.##" minFractionDigits="2"/></span></td>
									<td class="textInput"><span title="${itemUse.modv31}" style="width:60px;">
										<input type="text" value="<fmt:formatNumber value="${itemUse.modv31}" pattern="##.##" minFractionDigits="2"/>" style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
									
									<td><span title="${itemUse.prev41}" style="width:60px;text-align: right;"><fmt:formatNumber value="${itemUse.prev41}" pattern="##.##" minFractionDigits="2"/></span></td>
									<td class="textInput"><span title="${itemUse.modv41}" style="width:60px;">
										<input type="text" value="<fmt:formatNumber value="${itemUse.modv41}" pattern="##.##" minFractionDigits="2"/>" style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
									
									<td><span title="${itemUse.prev51}" style="width:60px;text-align: right;"><fmt:formatNumber value="${itemUse.prev51}" pattern="##.##" minFractionDigits="2"/></span></td>
									<td class="textInput"><span title="${itemUse.modv51}" style="width:60px;">
										<input type="text" value="<fmt:formatNumber value="${itemUse.modv51}" pattern="##.##" minFractionDigits="2"/>" style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
									
									<td><span title="${itemUse.prev61}" style="width:60px;text-align: right;"><fmt:formatNumber value="${itemUse.prev61}" pattern="##.##" minFractionDigits="2"/></span></td>
									<td class="textInput"><span title="${itemUse.modv61}" style="width:60px;">
										<input type="text" value="<fmt:formatNumber value="${itemUse.modv61}" pattern="##.##" minFractionDigits="2"/>" style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>

									<td><span title="${itemUse.prev71}" style="width:60px;text-align: right;"><fmt:formatNumber value="${itemUse.prev71}" pattern="##.##" minFractionDigits="2"/></span></td>
									<td class="textInput"><span title="${itemUse.modv71}" style="width:60px;">
										<input type="text" value="<fmt:formatNumber value="${itemUse.modv71}" pattern="##.##" minFractionDigits="2"/>" style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
									
									<td><span title="${itemUse.prev81}" style="width:60px;text-align: right;"><fmt:formatNumber value="${itemUse.prev81}" pattern="##.##" minFractionDigits="2"/></span></td>
									<td class="textInput"><span title="${itemUse.modv81}" style="width:60px;">
										<input type="text" value="<fmt:formatNumber value="${itemUse.modv81}" pattern="##.##" minFractionDigits="2"/>" style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
<%-- 			<page:page form="listForm" page="${pageobj}"></page:page> --%>
<%-- 			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" /> --%>
<%-- 			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" /> --%>
		</form>		
		<div class="form-line">
			<div class="form-input" style="color:red;">
				【根据配置,当前菜品点击率为：
				<c:choose>
					<c:when test="${itemUse.vplantyp == 'AMT' }">
						千元菜品点击率&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						计算逻辑：菜品点击率(参考)=同一周次菜品销售数量合计/同一周次营业额合计*1000&nbsp;】
					</c:when>
					<c:when test="${itemUse.vplantyp == 'TC' }">
						千单菜品点击率&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						计算逻辑：菜品点击率(参考)=同一周次菜品销售数量合计/同一周次单数合计*1000&nbsp;】
					</c:when>
					<c:otherwise>
						千人菜品点击率&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						计算逻辑：菜品点击率(参考)=同一周次菜品销售数量合计/同一周次客流合计*1000&nbsp;】
					</c:otherwise>
				</c:choose>
				 
			</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){	
			if($("#isDeclare").val() =="1"){
				parent.stopVisibility();
			}
			focus() ;//页面获得焦点
			loadTool();
			if ("calculated"==$("#msg").val()) {
				$("#wait3").css("visibility","visible");
			} else if ("dataNull"==$("#msg").val()) {
				alert("缺少菜品销售数据，点击率计算失败!");
			}

			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			if($("#isDeclare").val() == null || $("#isDeclare").val() !="1"){
				//自动实现滚动条 				
				setElementHeight('.grid',['.tool'],$(document.body),80);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();			
			}else{
				//自动实现滚动条 				
				setElementHeight('.grid',['.tool'],$(document.body),102);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();			
			}
			loadTotal();
		});
		
		function loadTool(){
			if($("#isDeclare").val() == null || $("#isDeclare").val() !="1"){
				if($('.grid').find('.table-body').find('.num').size()>0){
					loadToolBar([true,true,true,true,true]);
				}else {
					loadToolBar([true,false,false,false,false]);
				}
			}else{
				loadToolBarDeclare();
			}
		}
		
		// 报货向导 --- 引用点击率界面
		function loadToolBarDeclare(){
			$('.tool').html('');
			$('.tool').toolbar({
				items: [{
					text: '<fmt:message key="calculate" />',
					title: '计算',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-80px','-0px']
					},
					handler: function(){
						calculate();
					}
				}]
			});
		}
		
		// 独立节点引用点击率界面
		function loadToolBar(use){
			$('.tool').html('');
			$('.tool').toolbar({
				items: [{
					text: '<fmt:message key="select" />',
					title: '<fmt:message key="select" />',
					useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						$("#listForm").submit();
					}
				},'-',{
					text: '<fmt:message key="calculate" />',
					title: '<fmt:message key="calculate" />',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'calculate')}&&use[0],
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-80px','-0px']
					},
					handler: function(){
						calculate();
					}
				},{
					text: '添加菜品',
					title: '添加菜品',
					useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-80px','-0px']
					},
					handler: function(){
						addNewItem();
					}
				},{
					text: '<fmt:message key="save" />',
					title: '<fmt:message key="save" />',
					useable: use[1],
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-80px','-0px']
					},
					handler: function(){
						saveUpdate();
					}
				},'-',{
					text: 'Excel',
					title: '导出Excel',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')}&&use[4],
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-40px','-20px']
					},
					handler: function(){
						$("#wait2").val('NO');//不用等待加载
						$('#listForm').attr('action','<%=path%>/itemUse/export.do');
						$('#listForm').submit();
						$('#listForm').attr('target','');
						$('#listForm').attr('action','<%=path%>/itemUse/list.do');
						$("#wait2").val('');//等待加载还原
					}
				},{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
					}
				}
				]
			});
		}
		
		
		// 加载合计行
		function loadTotal(){
			var chkckList = $("input[name='show_id']");
			var total=[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0];
			chkckList.each(function(){
				total[0] += parseFloat($(this).parents('tr').find('td:eq(4)').find('span').text());
				total[1] += parseFloat($(this).parents('tr').find('td:eq(5)').find('input').val());
				total[2] += parseFloat($(this).parents('tr').find('td:eq(6)').find('span').text());
				total[3] += parseFloat($(this).parents('tr').find('td:eq(7)').find('input').val());
				total[4] += parseFloat($(this).parents('tr').find('td:eq(8)').find('span').text());
				total[5] += parseFloat($(this).parents('tr').find('td:eq(9)').find('input').val());
				total[6] += parseFloat($(this).parents('tr').find('td:eq(10)').find('span').text());
				total[7] += parseFloat($(this).parents('tr').find('td:eq(11)').find('input').val());
				total[8] += parseFloat($(this).parents('tr').find('td:eq(12)').find('span').text());
				total[9] += parseFloat($(this).parents('tr').find('td:eq(13)').find('input').val());
				total[10] += parseFloat($(this).parents('tr').find('td:eq(14)').find('span').text());
				total[11] += parseFloat($(this).parents('tr').find('td:eq(15)').find('input').val());
				total[12] += parseFloat($(this).parents('tr').find('td:eq(16)').find('span').text());
				total[13] += parseFloat($(this).parents('tr').find('td:eq(17)').find('input').val());
				total[14] += parseFloat($(this).parents('tr').find('td:eq(18)').find('span').text());
				total[15] += parseFloat($(this).parents('tr').find('td:eq(19)').find('input').val());
			});
			var trtd = '<tr id="total">' + 
			'<td class="num"><span style="width:30px;">'+(chkckList.length+1)+'</span></td>' + 
			'<td><span style="width:80px;">合计</span></td>' + 
			'<td><span style="width:100px;"></span></td>' + 
			'<td><span style="width:45px;"></span></td>' +
			'<td><span style="width:60px;text-align: right;">'+addNum(total[0])+'</span></td>' + 
			'<td><span style="width:60px;text-align: right;">'+addNum(total[1])+'</span></td>' + 
			'<td><span style="width:60px;text-align: right;">'+addNum(total[2])+'</span></td>' + 
			'<td><span style="width:60px;text-align: right;">'+addNum(total[3])+'</span></td>' + 
			'<td><span style="width:60px;text-align: right;">'+addNum(total[4])+'</span></td>' + 
			'<td><span style="width:60px;text-align: right;">'+addNum(total[5])+'</span></td>' + 
			'<td><span style="width:60px;text-align: right;">'+addNum(total[6])+'</span></td>' + 
			'<td><span style="width:60px;text-align: right;">'+addNum(total[7])+'</span></td>' + 
			'<td><span style="width:60px;text-align: right;">'+addNum(total[8])+'</span></td>' + 
			'<td><span style="width:60px;text-align: right;">'+addNum(total[9])+'</span></td>' + 
			'<td><span style="width:60px;text-align: right;">'+addNum(total[10])+'</span></td>' + 
			'<td><span style="width:60px;text-align: right;">'+addNum(total[11])+'</span></td>' + 
			'<td><span style="width:60px;text-align: right;">'+addNum(total[12])+'</span></td>' + 
			'<td><span style="width:60px;text-align: right;">'+addNum(total[13])+'</span></td>' + 
			'<td><span style="width:60px;text-align: right;">'+addNum(total[14])+'</span></td>' + 
			'<td><span style="width:60px;text-align: right;">'+addNum(total[15])+'</span></td>' + 
			'</tr>';
			$("#total").remove();
			$('#tblGrid').append(trtd);
		}
		
		function addNum(_val){
			return Number(_val).toFixed(2);
		}
		
		//报货向导用
		function save(){
			return saveUpdate();
		}
		
		//保存登记
		function saveUpdate(){
			var selected = {};
			var chkckList = $("input[name='show_id']");
			if(chkckList&& chkckList.size() > 0){
					chkckList.each(function(i){
						if($(this).val()!='total'){
							selected['ItemUseList['+i+'].item'] = $(this).val();
							//预估值
							selected['ItemUseList['+i+'].itcode'] =$(this).val();
							selected['ItemUseList['+i+'].itdes'] =$(this).parents('tr').find('td:eq(2)').find('span').text();
							selected['ItemUseList['+i+'].itunit'] =$(this).parents('tr').find('td:eq(3)').find('span').text();
							selected['ItemUseList['+i+'].dept'] = $(this).parents('tr').attr('data-dept');
							selected['ItemUseList['+i+'].prev11'] =parseFloat($(this).parents('tr').find('td:eq(4)').find('span').text());
							selected['ItemUseList['+i+'].prev21'] = parseFloat($(this).parents('tr').find('td:eq(6)').find('span').text());
							selected['ItemUseList['+i+'].prev31'] = parseFloat($(this).parents('tr').find('td:eq(8)').find('span').text());
							selected['ItemUseList['+i+'].prev41'] = parseFloat($(this).parents('tr').find('td:eq(10)').find('span').text());
							selected['ItemUseList['+i+'].prev51'] = parseFloat($(this).parents('tr').find('td:eq(12)').find('span').text());
							selected['ItemUseList['+i+'].prev61'] = parseFloat($(this).parents('tr').find('td:eq(14)').find('span').text());
							selected['ItemUseList['+i+'].prev71'] = parseFloat($(this).parents('tr').find('td:eq(16)').find('span').text());
							selected['ItemUseList['+i+'].prev81'] = parseFloat($(this).parents('tr').find('td:eq(18)').find('span').text());
							//调整值
							selected['ItemUseList['+i+'].modv11'] = parseFloat($(this).parents('tr').find('td:eq(5)').find('input').val());
							selected['ItemUseList['+i+'].modv21'] = parseFloat($(this).parents('tr').find('td:eq(7)').find('input').val());
							selected['ItemUseList['+i+'].modv31'] = parseFloat($(this).parents('tr').find('td:eq(9)').find('input').val());
							selected['ItemUseList['+i+'].modv41'] = parseFloat($(this).parents('tr').find('td:eq(11)').find('input').val());
							selected['ItemUseList['+i+'].modv51'] = parseFloat($(this).parents('tr').find('td:eq(13)').find('input').val());
							selected['ItemUseList['+i+'].modv61'] = parseFloat($(this).parents('tr').find('td:eq(15)').find('input').val());
							selected['ItemUseList['+i+'].modv71'] = parseFloat($(this).parents('tr').find('td:eq(17)').find('input').val());
							selected['ItemUseList['+i+'].modv81'] = parseFloat($(this).parents('tr').find('td:eq(19)').find('input').val());
						}
					});
					$.post('<%=path%>/itemUse/update.do',selected,function(data){
						if($("#isDeclare").val() == null || $("#isDeclare").val() !="1"){
							if(data ==1){
								alert('<fmt:message key="operation_successful" />!');
								$('#listForm').submit(); 
							}else{
								alert('<fmt:message key="operation_failed" />!');
							}
						}
					});
				return true;
			}else{
				alert('<fmt:message key="please_select_options_you_need_save" />！');
				return false;
			}
		}
		function DateDiff(sDate1, sDate2)
		{ 
		    var aDate, oDate1, oDate2, iDays;
		    aDate = sDate1.split("-");
		    oDate1 = new Date(aDate[1] + '/' + aDate[2] + '/' + aDate[0]); //转换为12-18-2002格式
		    aDate = sDate2.split("-");
		    oDate2 = new Date(aDate[1] + '/' + aDate[2] + '/' + aDate[0]);
		    iDays = parseInt((oDate1 - oDate2) / 1000 / 60 / 60 /24); //把相差的毫秒数转换为天数
		    return iDays;
		}
		function calculate(){
			if (confirm('是否确认重新计算，将清除历史点击率数据？')) {
				if(!$('#bdate').val()) {
					alert('请输入起始日期！');
					return;
				}
				if(!$('#edate').val()) {
					alert('请输入结束日期！');
					return;
				}
				if($("#bdate").val() > $("#edate").val()) {
					alert('起始日期不能大于结束日期！');
					return;
				}
				if(DateDiff($("#edate").val(),$("#bdate").val())>31){
					alert('参考日期不要大于一个月！');
					return;
				}
				$("#wait3").css("visibility","visible");
				$("#msg").val("calculated");
				$(".button").attr("disabled",true);
				var action = "<%=path%>/itemUse/calculate.do";
				$('#listForm').attr('action',action);
				$('#listForm').submit(); 
			}
		}
		// 检查最高最低库存是否有效数字
		function checkNum(inputObj){
			if(isNaN(inputObj.value)){
				alert("无效数字！");
				inputObj.focus();
				return false;
			}
			if(inputObj.value == ''){
				inputObj.value = '0.00';
			}
			loadTotal();
		}
		//添加新菜品
		function addNewItem(){
			$('body').window({
				id: 'window_supply',
				title: '<fmt:message key="insert" />菜品',
				content: '<iframe id="addItemFrame" name="addItemFrame" frameborder="0" src="<%=path%>/itemUse/addNewItem.do"></iframe>',
				width: '750px',
				height: '550px',
				draggable: true,
				isModal: true,
				topBar: {
					items: [{
							text: '<fmt:message key="save" />',
							title: '<fmt:message key="save" /><fmt:message key="supplies_code" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								if(getFrame('addItemFrame')){
									window.document.getElementById("addItemFrame").contentWindow.addItem();
								}
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.close').click();
							}
						}
					]
				}
			});
		}
		
		function getItem(){
			var selected = new Array;
			var chkckList = $("input[name='show_id']");
			chkckList.each(function(i){
				selected.push($(this).val());
			});
			return selected;
		}
		
		//添加菜品
		function addItem(data){
			var flag = true;
			var chkckList = $("input[name='show_id']");
			chkckList.each(function(i){
				var itcode = $.trim($(this).val());
				if(data.entity[0].vcode == itcode){
					flag = false;
				}
			});
			if(!flag){
				alert('菜品已添加，请勿重复添加！');
				return;
			}
			$("#total").remove();
			var trtd = '<tr data-dept='+ data.entity[0].deptcode +'>' + 
						'<td class="num"><span style="width:30px;">'+(chkckList.length+1)+'</span></td>' + 
						'<td><span title="'+ data.entity[0].vcode +'" style="width:80px;">'+
						data.entity[0].vcode +'<input type="hidden" value="'+data.entity[0].vcode+'" name="show_id"/></span></td>' + 
						'<td><span title="'+ data.entity[0].vname +'" style="width:100px;">'+ data.entity[0].vname +'</span></td>' + 
						'<td><span title="'+ data.entity[0].vunit +'" style="width:45px;">'+ data.entity[0].vunit +'</span></td>' + 
						'<td><span title="0.00" style="width:60px;text-align: right;"><fmt:formatNumber value="0.00" pattern="##.##" minFractionDigits="2"/></span></td>' + 
						'<td class="textInput"><span title="0.00" style="width:60px;">' + 
							'<input type="text" value="<fmt:formatNumber value="0.00" pattern="##.##" minFractionDigits="2"/>" style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>' + 
						
						'<td><span title="0.00" style="width:60px;text-align: right;"><fmt:formatNumber value="0.00" pattern="##.##" minFractionDigits="2"/></span></td>' + 
						'<td class="textInput"><span title="0.00" style="width:60px;">' + 
							'<input type="text" value="<fmt:formatNumber value="0.00" pattern="##.##" minFractionDigits="2"/>" style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>' + 
						
						'<td><span title="0.00" style="width:60px;text-align: right;"><fmt:formatNumber value="0.00" pattern="##.##" minFractionDigits="2"/></span></td>' + 
						'<td class="textInput"><span title="0.00" style="width:60px;">' + 
							'<input type="text" value="<fmt:formatNumber value="0.00" pattern="##.##" minFractionDigits="2"/>" style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>' + 
						
						'<td><span title="0.00" style="width:60px;text-align: right;"><fmt:formatNumber value="0.00" pattern="##.##" minFractionDigits="2"/></span></td>' + 
						'<td class="textInput"><span title="0.00" style="width:60px;">' + 
							'<input type="text" value="<fmt:formatNumber value="0.00" pattern="##.##" minFractionDigits="2"/>" style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>' + 
						
						'<td><span title="0.00" style="width:60px;text-align: right;"><fmt:formatNumber value="0.00" pattern="##.##" minFractionDigits="2"/></span></td>' + 
						'<td class="textInput"><span title="0.00" style="width:60px;">' + 
							'<input type="text" value="<fmt:formatNumber value="0.00" pattern="##.##" minFractionDigits="2"/>" style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>' + 
						
						'<td><span title="0.00" style="width:60px;text-align: right;"><fmt:formatNumber value="0.00" pattern="##.##" minFractionDigits="2"/></span></td>' + 
						'<td class="textInput"><span title="0.00" style="width:60px;">' + 
							'<input type="text" value="<fmt:formatNumber value="0.00" pattern="##.##" minFractionDigits="2"/>" style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>' + 
			
						'<td><span title="0.00" style="width:60px;text-align: right;"><fmt:formatNumber value="0.00" pattern="##.##" minFractionDigits="2"/></span></td>' + 
						'<td class="textInput"><span title="0.00" style="width:60px;">' + 
							'<input type="text" value="<fmt:formatNumber value="0.00" pattern="##.##" minFractionDigits="2"/>" style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>' + 
						
						'<td><span title="0.00" style="width:60px;text-align: right;"><fmt:formatNumber value="0.00" pattern="##.##" minFractionDigits="2"/></span></td>' + 
						'<td class="textInput"><span title="${itemUse.modv81}" style="width:60px;">' + 
							'<input type="text" value="<fmt:formatNumber value="0.00" pattern="##.##" minFractionDigits="2"/>" style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>' + 
					'</tr>';
			$('#tblGrid').append(trtd);
			loadTotal();
            loadToolBar([true,true,true,true,true]);
		}
		</script>
	</body>
</html>