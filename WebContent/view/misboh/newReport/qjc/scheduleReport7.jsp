<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>排班报表</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
		.leftDiv{
			float: left;
			height: 100%;
			width: 120px;
			padding-left: 20px;
			line-height: 30px;
			padding-top: 30px;
		}
		.chartdiv{
			height: 100%;
		}
		.form-label2{
			width: 40px;
			height: 25px;
			position: relative;
			margin: 0;
			float: left;
			text-align: right;
			vertical-align: middle;
			line-height: 25px;
			margin-right: 5px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 6.5%;
		}
		form .form-line .form-input{
			width: 13%;
		}
		form .form-line .form-input input[type=text]{
			width: 85%;
		}
	</style>
  </head>	
  <body>
  	<%@ include file="../share/permission.jsp"%>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label"><fmt:message key="startdate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" />
			</div>
			<div class="form-label"><fmt:message key="enddate" /></div>
			<div class="form-input">	
				<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" />
			</div>
			<div class="form-label" style="width:60px;">时段</div>
			<div class="form-input">
				<select id="interval" name="interval" class="select">
					<option value="1">30</option>
					<option value="2">60</option>
				</select>分钟
			</div>
		</div>
		<input type="hidden" id="pk_store" name="pk_store" value="${store.pk_store }" />
	</form>
	<input type="hidden" id="reportName" value="scheduleReport7"/>
	<input type="hidden" id="excelUrl" value="<%=path%>/MISBOHNewreport/exportReport.do"/>
	<input type="hidden" id="dataUrl" value="<%=path%>/MISBOHNewreport/queryScheduleReport7.do"/>
	<input type="hidden" id="title" value="排班报表"/>
	<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		//默认<fmt:message key="time" />
  	 		$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
  	 		$("#bdat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
		    });
		    $("#edat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
		    });
  	 		
			//初始化<fmt:message key="selected" />标签<fmt:message key="content" />
			initTab(false);
  	 		initone();
  	 	});
  	 	
  		//初始化<fmt:message key="selected" />标签<fmt:message key="content" />
  	 	function initTab(rebuild){
  	 		var excelUrl = $("#excelUrl").val();
  	 		var reportName = $("#reportName").val();
  	 		var dataUrl = $("#dataUrl").val();
  	 		var title = $("#title").val();
  	 		var grid = $("#datagrid");

  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp:true,
  	 			excelUrl:excelUrl+'?reportName='+reportName,
  	 			toolbar:['search','excel','exit'],
  	 			searchFun:function(){
  	 				$('#datagrid').datagrid('load',getParam($("#queryForm")));
  	 			}
  	 		});
  	 		if(rebuild || grid.css('display') != 'none'){
  	  				firstLoad = true;//重置第一<fmt:message key="secondary" />加载标志
		  	 		builtTable({
		  	 			dataUrl:dataUrl,
		  	 			title:title,
		  	 			grid:grid,
		  	 			createHeader:function(data,head,frozenHead){
		  	 				var col1 = [];
		  	 				col1.push({field:"VINTERVAL",width:100,title:'时段',align:'left'});
							col1.push({field:"KTCOUNT",width:80,title:'开台数',align:'right'});
							col1.push({field:"NMONEY",width:80,title:'销售金额',align:'right'});
							col1.push({field:"JZCOUNT",width:80,title:'结账台数',align:'right'});
							col1.push({field:"NYMONEY",width:80,title:'结账金额',align:'right'});
							col1.push({field:"ZTCOUNT",width:80,title:'在台数',align:'right'});
		  	 				
		  	 				head.push(col1);
		  	 			}
		  	 		});
  	  			}
  	 	}
  		//列<fmt:message key="select1" />后页面重新加载
  		function pageReload(){
  			closeColChooseWin();
  			initTab(true);
  		}
  	 </script>
  </body>
</html>