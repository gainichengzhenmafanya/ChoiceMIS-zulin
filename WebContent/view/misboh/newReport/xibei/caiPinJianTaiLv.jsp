<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>集团菜品见台率</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<style type="text/css">
		.radio{
			line-height: 20px;
			width:80px;
			display: inline-block;
		}
		.leftFrame{
			width:290px;
		}
	</style>
	<%@ include file="../share/permission.jsp"%>
  </head>	
  <body>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
  		<div class="form-line">
			<div class="form-label" style="width:60px;"><fmt:message key="startdate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" />
			</div>
			<div class="form-label" style="width:60px;"><fmt:message key="bigClass" /></div>
			<div class="form-input">
				<select name="pubgrptyp" id="pubgrptyp" style="width: 133px;" class="select" key="VCNCODEONE" data="NAME" select="true"></select>
			</div>
			<div class="form-label" style="width:60px;"><fmt:message key="Holiday_type" /></div>
			<div class="inputdiv form-input" id="jiarileixing2">
				<select id="HolidayType" name="HolidayType" style="width: 133px;" class="select">
					<option value="1"><fmt:message key="all" /></option>
					<option value="2"><fmt:message key="Working_day" /></option>
					<option value="3"><fmt:message key="holidays" /></option>
				</select>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label" style="width:60px;"><fmt:message key="enddate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" />
			</div>
			<div class="form-label" style="width:60px;"><fmt:message key="smallClass" /></div>
			<div class="form-input">
				<select name="pubgrp" id="pubgrp" style="width: 133px;" class="select" key="VCNCODETWO" data="NAME" select="true"></select>
			</div>
			<div class="form-label"style="width:60px;"><fmt:message key="scm_flight" /></div>
			<div class="form-input">
				<select id="sft" name="sft" style="width: 133px;" class="select">
					<option value="0"><fmt:message key="all_day" /></option>
					<c:forEach var="sft" items="${sftList}">
						<option value="${sft.vcode}" <c:if test="${sft.vcode==condition.sft}"> selected="selected" </c:if> >${sft.vname}</option>
					</c:forEach>				
				</select>
			</div>			
		</div>
		<div class="form-line">
			<div class="form-label" style="width:120px;margin-left:90px;">
				<fmt:message key="by_pubitem_vdese" />&nbsp;&nbsp;<input type="checkbox" name="iflag" id="iflag" value="1" />
			</div>
			<div class="form-label" ><fmt:message key="pubitem" /><fmt:message key="type" />:</div>
			<div class="form-input" style="width:400px;">
				<input type="radio" name="costCategory" id="all" value="0" checked="checked" /><label for="all"><fmt:message key="all" /><fmt:message key="pubitem" /></label>
				<input type="radio" name="costCategory" id="H" value="1" /><label for="C"><fmt:message key="The_core_food" /></label>
				<input type="radio" name="costCategory" id="M" value="2" /><label for="A"><fmt:message key="Star_food" /></label>
				<input type="radio" name="costCategory" id="Z" value="3" /><label for="D"><fmt:message key="Optional_dish" /></label>
			</div>
		</div>
		<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }"/>
	</form>
   	<div id="datagrid"></div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		
		 	//默认<fmt:message key="time" />
		 	$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
  	 		$("#bdat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
		    });
		    $("#edat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
		    });
  	  		
			//初始化选中标签内容
			initTab();
			
			$('#pubgrptyp').htmlUtils("select");
	 		//页面加载时默认查询小类信息
	 		$('#pubgrptyp').attr("url","<%=path%>/chineseBusinessReport/findAllMarsaleclassOne.do");
	 		$('#pubgrptyp').siblings().remove();//把已经存在的select去掉
	 		$('#pubgrptyp').htmlUtils("select");
	 		
	 		$('#pubgrp').htmlUtils("select");
	 		//页面加载时默认查询小类信息
	 		$('#pubgrp').attr("url","<%=path%>/chineseBusinessReport/findAllMarsaleclassTwo.do");
	 		$('#pubgrp').siblings().remove();//把已经存在的select去掉
	 		$('#pubgrp').htmlUtils("select");
  	 	});
  		
  		//初始化选中标签内容
  	 	function initTab(){
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:$('#datagrid'),
  	 			exportTyp:true,
  	 			excelUrl:"<%=path%>/MISBOHNewreport/exportReport.do?reportName=caiPinJianTaiLv",
  	 			colsChooseUrl:'<%=path%>/MISBOHNewreport/toColumnsChoose.do?reportName=caiPinJianTaiLv',
  	 			toolbar:['search','excel','exit']
  	 		});
  	 		builtTable({
  	 			headUrl:"<%=path%>/MISBOHNewreport/findHeaders.do?reportName=caiPinJianTaiLv",
  	 			dataUrl:"<%=path%>/MISBOHNewreport/findCaiPinJianTaiLv.do",
  	 			title:'<fmt:message key="See_the_statistical_analysis_of_the_rate_of_dishes" />',
  	 			grid:$('#datagrid'),
  	 			width:'100%',
  	 			height:tableHeight,
  	 			alignCols:['itcode','tblsrate','amtrate'],
  	 			numCols:['amt','cnt','totalamt','num','jamt']
  	 		});
  	 	}
  	 </script>
  </body>
</html>