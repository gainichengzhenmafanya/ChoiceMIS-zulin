<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>营业时段解析表</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<style type="text/css">
	</style>
	<%@ include file="../share/permission.jsp"%>
  </head>	
  <body>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
  		<div class="form-line">
			<div class="form-label"><fmt:message key="startdate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" />
			</div>
			<div class="form-label"><fmt:message key="enddate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" />
			</div>
			<div class="form-label"><fmt:message key="period_of_time" /></div>
			<div class="form-input">
				<input type="text" id="interval" name="interval" style="margin-top: 3px;" class="text" onkeyup="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}" onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}" style="margin-top: 0px;" value="30"/>
			</div>
			<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }"/>
		</div>
	</form>
   	<div id="datagrid"></div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		
		 	//默认<fmt:message key="time" />
		 	$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
  	 		$("#bdat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
		    });
		    $("#edat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
		    });
  	  		
			//初始化选中标签内容
			initTab();
  	 	});
  		
  		//初始化选中标签内容
  	 	function initTab(){
  	 		queryParams = getParam($("#queryForm"));
  			
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:$('#datagrid'),
  	 			exportTyp:true,
  	 			excelUrl:"<%=path%>/MISBOHNewreport/exportReport.do?reportName=saledatabytime",
  	 			toolbar:['search','excel','exit'],
  	 			searchFun:function(){
  	 				initTab();
  	 			}
  	 		});
  	 		builtTable({
  	 			dataUrl:"<%=path%>/MISBOHNewreport/findSaleDataByTime.do",
  	 			title:'<fmt:message key="saledatabytime" />',
  	 			grid:$('#datagrid'),
  	 			createHeader:function(data,head,frozenHead){
  	 				var col1 = [];
  	 				
 	 				col1.push({field:'DAT',title:'<fmt:message key="time" />',width:80,align:'center'});
 	 				col1.push({field:'PAX',title:'<fmt:message key="billing_pax_avg" />',width:80,align:'right'});
 	 				col1.push({field:'PAX1',title:'<fmt:message key="settling_pax_avg" />',width:80,align:'right'});
 	 				col1.push({field:'AMT',title:'<fmt:message key="business_income" />',width:80,align:'right'});
 	 				col1.push({field:'JAMT',title:'<fmt:message key="Net_income" />',width:80,align:'right'});
 	 				col1.push({field:'YMAMT',title:'<fmt:message key="Optimal_from_the_forehead" />',width:80,align:'right'});
 	 				col1.push({field:'CNT',title:'<fmt:message key="billing_tbls_avg" />',width:80,align:'right'});
 	 				col1.push({field:'CNT1',title:'<fmt:message key="settling_tbls_avg" />',width:80,align:'right'});
  	 				
 	  	 			head.push(col1);
	  	 		}
  	 		});
  	 	}
  	 </script>
  </body>
</html>