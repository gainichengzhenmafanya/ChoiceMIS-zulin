<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>营收客流分段统计明细表</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<style type="text/css">
	</style>
	<%@ include file="../share/permission.jsp"%>
  </head>	
  <body>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
  		<div class="form-line">
			<div class="form-label"><fmt:message key="startdate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" />
			</div>
			<div class="form-label"><fmt:message key="enddate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" />
			</div>
			<div class="form-label"><fmt:message key="analysis_dimension" /></div>
			<div class="form-input">
				<select id="queryMod" name="queryMod" style="width: 133px;" class="select">
					<option value="1"><fmt:message key="monthly" /></option>
					<option value="2"><fmt:message key="weekly" /></option>
				</select>
			</div>
			<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }"/>
		</div>
	</form>
   	<div id="datagrid"></div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		
		 	//默认<fmt:message key="time" />
		 	$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
  	 		$("#bdat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
		    });
		    $("#edat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
		    });
  	  		
			//初始化选中标签内容
			initTab();
  	 	});
  		
  		//初始化选中标签内容
  	 	function initTab(){
  	 		queryParams = getParam($("#queryForm"));
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:$('#datagrid'),
  	 			exportTyp:true,
  	 			excelUrl:"<%=path%>/MISBOHNewreport/exportReport.do?reportName=businessburst",
  	 			toolbar:['search','excel','exit'],
  	 			searchFun:function(){
  	 				initTab();
  	 			}
  	 		});
  	 		builtTable({
  	 			headUrl:"<%=path%>/MISBOHNewreport/findItemTypeSaleHeaders.do?bdat="+$("#bdat").val()+"&edat="+$("#edat").val()+"&queryMod="+$("#queryMod").val(),
  	 			dataUrl:"<%=path%>/MISBOHNewreport/findBusinessBurst.do",
  	 			title:'<fmt:message key="businessburst" />',
  	 			grid:$('#datagrid'),
  	 			createHeader:function(data,head,frozenHead){
  	 				var start = data.start;
  	 				var end = data.end;
  	 				var col1 = [];
  	 				var col2 = [];
  	 				
  	 				col1.push({field:'STORENAME',title:'<fmt:message key="store" />',width:120,rowspan:2});
 	 				col1.push({field:'VTYPE',title:'<fmt:message key="type" />',width:70,rowspan:2});
 	 				col1.push({field:'VNAME',title:'<fmt:message key="project" />',width:80,rowspan:2});
 	 				var des = '<fmt:message key="month" />';
 	 				if ($('#queryMod').val() == 2) {
 	 					des = '<fmt:message key="week" />';
 	 				}
 	 				for (var i = start; i <= end; i++) {
 	 					var a = i;
 	 					if ($('#queryMod').val() == 2) {
 	 						if (i > 52) {
 	 							a = i - 52;
 	 						}
 	 					} else {
 	 						if (i > 12) {
 	 							a = i - 12;
 	 						}
 	 					}
 	 					col1.push({title:a + des,colspan:2});
 		  	 			col2.push({field:"NYMONEY" + a,width:70,title:'<fmt:message key="amount" />',align:'right'});
 		  	 			col2.push({field:"NYMONEYAVG" + a,width:70,title:'<fmt:message key="nymoney_avg" />',align:'right'});
 	 				}
  	 				
 	  	 			head.push(col1);
  	 				head.push(col2);
	  	 		}
  	 		});
  	 	}
  	 </script>
  </body>
</html>