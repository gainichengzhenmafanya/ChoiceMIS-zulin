<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><fmt:message key="checkout_type_report" /></title>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	</head>
	<body>
		<%@ include file="../share/permission.jsp"%>
		<div id="tool"></div>
	  	<form id="queryForm" name="queryForm" method="post">
  			<input type="hidden" name="startdate" id="startdate"/>
  			<input type="hidden" name="enddate" id="enddate"/>
			<input type="hidden" id="pk_store" name="pk_store" value="${store.pk_store }"/>
			<input type="hidden" id="ivalue" name="ivalue" value=""/>
	  			
			<div class="form-line">
				<div class="form-label" style="width:60px;"><fmt:message key="startdate" /></div>
				<div class="form-input">
					<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" />
				</div>
				<div class="form-label" style="width:60px;"><fmt:message key="enddate" /></div>
				<div class="form-input">
					<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" />
				</div>
				<div class="form-label" style="width:60px;" ><fmt:message key="store" />POS</div>
				<div class="form-input">
					<input type="text" id="vposname" name="vposname"  readonly="readonly" class="text" style="width: 120px;margin-top: 0px;" value="" />
					<input type="hidden" id="vcode" name="vcode" value="" />
					<img id="selectPos" class="search" src="<%=path%>/image/themes/icons/search.png"  style="margin-top: 0px;"  alt="<fmt:message key="select1" />POS<fmt:message key="device" />" />
				</div>
			</div>
		</form>
		
		<div class="easyui-tabs" fit="false" plain="true" id="tabs">
			<div title='<fmt:message key="checkout_type_report" />' style="position:absolute;">
				<input type="hidden" id="reportName" value="jsfstjbb"/>
				<input type="hidden" id="excelUrl" value="<%=path%>/proReport/exportReport.do"/>
				<input type="hidden" id="printUrl" value="<%=path%>/proReport/printReport.do"/>
				<input type="hidden" id="dataUrl" value="<%=path%>/proReport/queryStatementAccounts.do"/>
				<input type="hidden" id="title" value="<fmt:message key="checkout_type_report" />"/>
				<div id="datagrid"></div>
			</div>
			<div title='<fmt:message key="checkout_type_detail" />' style="position:absolute;">
				<input type="hidden" id="reportName" value="jsfsmx"/>
				<input type="hidden" id="excelUrl" value="<%=path%>/proReport/exportReport.do"/>
				<input type="hidden" id="printUrl" value="<%=path%>/proReport/printReport.do"/>
				<input type="hidden" id="dataUrl" value="<%=path%>/proReport/queryStatementAccountsDetail.do"/>
				<input type="hidden" id="title" value="<fmt:message key="checkout_type_detail" />"/>
				<div id="datagrid"></div>
			</div>
		</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
		<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				//默认<fmt:message key="time" />
	  	 		$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
	  	 		$("#bdat").click(function(){
			        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
			    });
			    $("#edat").click(function(){
			        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
			    });
		  		
		  		$("#tabs").tabs({
			  		onSelect:function(){
  	 				//初始化<fmt:message key="selected" />标签<fmt:message key="content" />
  	 					initTab(false);
  	 				//获取<fmt:message key="selected" />的面板
//   	 	  	 		var content = $('#tabs').tabs('getSelected').panel("body");
//   	 	  	 		var reportNameS = content.find("#reportName").val();
//   	 	  	 		if('jsfsmx'==reportNameS){
//   	 	  	 			content.find("#datagrid").datagrid('reload',getParam($("#queryForm")));
//   	 	  	 		}
  	 				}
  	 			});
	  	 		
	  	 		initTab(false);
			});
			
			//初始化<fmt:message key="selected" />标签<fmt:message key="content" />
	  	 	function initTab(rebuild){
	  	 		tab = $('#tabs').tabs('getSelected');//获取<fmt:message key="selected" />的面板
	  	 		var content = tab.panel("body");
	  	 		var excelUrl = content.find("#excelUrl").val();
	  	 		var printUrl = content.find("#printUrl").val();
	  	 		var reportName = content.find("#reportName").val();
	  	 		var dataUrl = content.find("#dataUrl").val();
	  	 		var title = content.find("#title").val();
	  	 		var grid = content.find("#datagrid");

	  	 		builtToolBar({
	  	 			basePath:"<%=path%>",
	  	 			toolbarId:'tool',
	  	 			formId:'queryForm',
	  	 			grid:grid,
	 				exportTyp:true,
	  	 			excelUrl:excelUrl+'?reportName='+reportName,
	  	 			printUrl:printUrl+'?reportName='+reportName,
	  	 			colsChooseUrl:'<%=path%>/proReport/toColumnsChoose.do?reportName='+reportName,
	  	 			toolbar:['search','excel','exit'],
	  	 			searchFun:function(){
	  	 				if(reportName=='jsfsmx') {
	  	 					alert('<fmt:message key="please_select_data" />!');
	  	 					return;
	  	 				}
	  	 				$('#datagrid').datagrid('load',getParam($("#queryForm")));
	  	 			}
	  	 		});
	  	 		if(rebuild || grid.css('display') != 'none'){
	  	  				firstLoad = true;//重置第一<fmt:message key="secondary" />加载标志
			  	 		builtTable({
			  	 			headUrl:"<%=path%>/proReport/findHeaders.do?reportName="+reportName,
			  	 			dataUrl:dataUrl,
			  	 			title:title,
			  	 			grid:grid,
			  	 			alignCols:['num','cnt','nmoney'],
			  	 			numCols:['sumamt','nmoney'],
// 			  	 			decimalDigitF:['nmoney','sumamt'],
			  	 			hiddenCols:[{field:'IVALUE'}],
			  	 			onDblClickRow:function(index,data){
			  	 				if(reportName!="jsfstjbb") return;
			  	 				var serials = {};
			  	 				$('#tabs').tabs("select",'<fmt:message key="checkout_type_detail" />');
			  	 				serials['ivalue'] = data['IVALUE'];
								serials['startdate'] = data['DWORKDATE'];
								serials['enddate'] = data['DWORKDATE'];
			  	 				serials['pk_store'] = $("#pk_store").val();
			  	 				serials['vcode'] = $("#vcode").val();
			  	 				//双击时候，给ivalue 和vscode属性赋值，用于结算方式明细导出。
			  	 				$('#ivalue').val(data['IVALUE']);
			  	 				$('#vcode').val($("#vcode").val());
								$('#startdate').val(data['DWORKDATE']);
								$('#enddate').val(data['DWORKDATE']);
			  	 				
			  	 				tab = $('#tabs').tabs('getSelected');//获取<fmt:message key="selected" />的面板
			  	 	  	 		var content = tab.panel("body");
			  	 	  	 		var reportNameS = content.find("#reportName").val();
			  	 	  	 		content.find("#datagrid").datagrid('reload',serials);
			  	 			}
			  	 		});
	  	  			}
	  	 	}
	  	 	
	  	  $("#selectPos").click(function(){
	             var pk_store=$("#pk_store").val();
	             if(pk_store=='' || pk_store.indexOf(',')!=-1){
	            	 alert('<fmt:message key="Please_select_a_store"/>');return;
	             }
				 var vposcode = $("#vcode").val();
	             selectStorePos({
	                 basePath:"<%=path%>",
	                 domId:$('#pk_storearear').val()==''?'selected':'vposcode',     //记住已<fmt:message key="select1" />的记录
	                 single:false,     //<fmt:message key="whether" />单选
	                 pk_store:pk_store,
	                 height:300,
	                 width:550
	             },pk_store);
	         });
	  		 
			 function setStorePos(data){
	            if(data==null){
	                return;
	            };
	            $("#vposname").val(data.name);
	            $("#vcode").val(data.show);
	        }
		</script>
	</body>
</html>