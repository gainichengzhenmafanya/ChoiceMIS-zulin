<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.misboh.domain.reportMis.PublicEntity" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>折扣分析</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
		.leftDiv{
			float: left;
			height: 100%;
			width: 120px;
			padding-left: 20px;
			line-height: 30px;
			padding-top: 30px;
		}
		.chartdiv{
			height: 100%;
		}
		.form-label2{
			width: 40px;
			height: 25px;
			position: relative;
			margin: 0;
			float: left;
			text-align: right;
			vertical-align: middle;
			line-height: 25px;
			margin-right: 5px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 6.5%;
		}
		form .form-line .form-input{
			width: 13%;
		}
		form .form-line .form-input input[type=text]{
			width: 85%;
		}
	</style>
  </head>	
  <body>
  	<%@ include file="../share/permission.jsp"%>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
				<div class="form-label"><fmt:message key="startdate" /></div>
				<div class="form-input">
					<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" />
				</div>
				<div class="form-label"><fmt:message key="enddate" /></div>
				<div class="form-input">
					<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" />
				</div>
		</div>
	</form>
	<input type="hidden" id="reportName" value="zkfxb"/>
	<input type="hidden" id="excelUrl" value="<%=path%>/proReport/exportReport.do"/>
	<input type="hidden" id="printUrl" value="<%=path%>/proReport/printReport.do"/>
	<input type="hidden" id="dataUrl" value="<%=path%>/proReport/queryDiscountAnalysis.do"/>
	<input type="hidden" id="title" value='<fmt:message key="discount_analysis" />'/>
	<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		//默认<fmt:message key="time" />
  	 		$("#bdat,#edat").htmlUtils("setDate","yes",'${newdate}');
  	 		$("#bdat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:-'+ROLE_TIME_beforeDay+'})}',maxDate:'#F{$dp.$D(\'edat\')}'});
		    });
		    $("#edat").click(function(){
		        new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',maxDate:'#F{$dp.$DV(\''+'${servertime}'+'\',{d:+'+ROLE_TIME_afterDay+'})}'});
		    });
  	 		
			//初始化<fmt:message key="selected" />标签<fmt:message key="content" />
			initTab(false);
			initone();
  	 	});
  	 	
  		//初始化<fmt:message key="selected" />标签<fmt:message key="content" />
  	 	function initTab(rebuild){
  	 		var excelUrl = $("#excelUrl").val();
  	 		var printUrl = $("#printUrl").val();
  	 		var reportName = $("#reportName").val();
  	 		var dataUrl = $("#dataUrl").val();
  	 		var title = $("#title").val();
  	 		var grid = $("#datagrid");

  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp:true,
  	 			excelUrl:excelUrl+'?reportName='+reportName,
  	 			printUrl:printUrl+'?reportName='+reportName,
  	 			colsChooseUrl:'<%=path%>/proReport/toColumnsChoose.do?reportName='+reportName,
  	 			toolbar:['search','excel','exit'],
  	 			searchFun:function(){
  	 				initTab(true);
  	 				$('#datagrid').datagrid('load',getParam($("#queryForm")));
  	 			}
  	 		});
  	 		if(rebuild || grid.css('display') != 'none'){
  	  				firstLoad = true;//重置第一<fmt:message key="secondary" />加载标志
		  	 		builtTable({
		  	 			headUrl:"<%=path%>/proReport/findHeaderForDiscfx.do?pk_store="+$('#pk_store').val()+"&bdat="+$('#bdat').val()+"&edat="+$('#edat').val(),
		  	 			dataUrl:dataUrl,
		  	 			title:title,
		  	 			grid:grid,
		  	 			alignCols:['vdictname','nshallamt','namt','ndictrate','nrate','ndimemo'],
// 		  	 			numCols:['nprice','ncount','nmoney','nzmoney','nymoney'],
		  	 			createHeader:function(data,head,frozenHead){
		  	 				var actTyp = data.actTyp;
		  	 				var actmtyp = data.actmtyp;
		  	 				var payment = data.payment;
		  	 				var actm = data.actm;
		  	 				var paymode= data.paymode;
		  	 				var actmtypmin = data.actmtypmin;
		  	 				var col1 = [];
		  	 				var col2 = [];
		  	 				var col3 = [];
		  	 				var yhname = '<fmt:message key="discount_analysis" />';
		  	 				var curCol1 = undefined;
		  	 				var curCol2 = undefined;
		  	 				var mod = undefined;
		  	 				frozenHead.push([{field:'DWORKDATE',width:130,title:'<fmt:message key="business_day" />',align:'left',rowspan:3}]);
		  	 				col1.push({field:'TC',width:60,title:'<fmt:message key="open_the_numbers" />',align:'right',rowspan:3});
		  	 				col1.push({field:'NMONEY',width:100,title:'<fmt:message key="should_total" />',align:'right',rowspan:3});
	 						col1.push({field:'NYMONEY',width:100,title:'<fmt:message key="real_total" />',align:'right',rowspan:3});
		  	 				col1.push({field:'NZMONEY',width:100,title:'<fmt:message key="discount_total" />',align:'right',rowspan:3});
		  	 				col1.push({field:'ZSMONEY',width:100,title:'<fmt:message key="gift_amt" />',align:'right',rowspan:3});
		  	 				col1.push({field:'NBZERO',width:80,title:'<fmt:message key="maLing" />',align:'right',rowspan:3});
		  	 				col1.push({field:'ZBYS',width:80,title:'<fmt:message key="ratio_for_should" />',align:'right',rowspan:3});
		  	 				//<fmt:message key="payment" />
		  	 				for(var pay in payment){
		  	 					if(payment[pay].ivalue!=1000){
	  	 							col1.push({field:'PAYMENT'+payment[pay].vcode,width:100,title:payment[pay].vname,align:'right',rowspan:3});
		  	 					}
		  	 				}
		  	 				var zkfx='<fmt:message key="discount_analysis" />';
		  	 				var tempzkfx;
		  	 				var curColActtyp;
		  	 				for(var cur in actTyp){
		  	 					if(tempzkfx == zkfx){
		  	 						curColActtyp.colspan += 2;
		  	 					}else{
		  	 						curColActtyp = {title:zkfx,colspan:2};
		  							col1.push(curColActtyp);
		  							tempzkfx = zkfx;
		  	 					}
		  	 					col2.push({width:100,title:actTyp[cur].vname,align:'center',colspan:2});
		  	 					col3.push({field:"AMT"+actTyp[cur].vcode,width:60,title:'<fmt:message key="amount" />',align:'right'});
		  	 					col3.push({field:"TYPZB"+actTyp[cur].vcode,width:70,title:'<fmt:message key="ratio_for_should" />',align:'right'});
		  	 				}
		  	 				
		  	 				
		  	 				var yhzkmx = '<fmt:message key="preferential" /><fmt:message key="discount_detail" />';
		  	 				var tempyhzkmx;
		  	 				var curColzkmx;
		  	 				for(var cur in actmtyp){
		  	 					if(tempyhzkmx == yhzkmx){
		  	 					}else{
		  	 						curColzkmx = {title:yhname,colspan:0};
		  							col1.push(curColzkmx);
		  							tempyhzkmx = yhzkmx;
		  	 					}
		  	 					var curColzkmx2 = {width:100,title:actmtyp[cur].vname,align:'center',colspan:0};
		  	 					col2.push(curColzkmx2);
		  	 					for(var actmm in actm){
		  	 						if(actm[actmm].pk_acttyp == actmtyp[cur].pk_ActTyp ){
		  	 							curColzkmx.colspan += 1;
		  	 							curColzkmx2.colspan += 1;
				  	 					col3.push({field:"ACTM"+actm[actmm].vcode,width:80,title:actm[actmm].vname,align:'right'});
				  	 				}
		  	 					}
		  	 				}
		  	 				
		  	 				var curColpaymode;
		  	 				var zffs='<fmt:message key="payment" />';
		  	 				var tempzffs;
		  	 				for(var pm in paymode){
		  	 					if(tempzffs == zffs){
		  	 					}else{
		  	 						curColpaymode = {title:zffs,colspan:0,rowspan:2};
		  							col1.push(curColpaymode);
		  							tempzffs = zffs;
		  	 					}
	  	 						col3.push({field:"PAYMODE"+paymode[pm].vcode,width:80,title:paymode[pm].vname,align:'right'});
	  	 						curColpaymode.colspan += 1;
		  	 				}


		  	 				//活动小类
		  	 				var hdxl='<fmt:message key="activity" /><fmt:message key="smallClass" />';
		  	 				var temphdxl;
		  	 				var curColhdxl;
		  	 				var curColhdxl2;
		  	 				for(var cur in actmtyp){
		  	 					if(temphdxl == hdxl){
		  	 					}else{
		  	 						curColhdxl = {title:hdxl,colspan:0};
		  							col1.push(curColhdxl);
		  							temphdxl = hdxl;
		  	 					}
		  	 					curColhdxl2 = {width:100,title:actmtyp[cur].vname,align:'center',colspan:0};
		  	 					col2.push(curColhdxl2);
		  	 					for(var actmm in actmtypmin){
		  	 						if(actmtypmin[actmm].pk_acttyp == actmtyp[cur].pk_ActTyp ){
		  	 							curColhdxl.colspan += 1;
		  	 							curColhdxl2.colspan += 1;
				  	 					col3.push({field:"ACTTYPMIN"+actmtypmin[actmm].vcode,width:80,title:actmtypmin[actmm].vname,align:'right'});
				  	 				}
		  	 					}
		  	 				}
		  	 				head.push(col1);
		  	 				head.push(col2);
		  	 				head.push(col3);
		  	 			}
		  	 		});
  	  			}
  	 	
  	 	}
  		//列<fmt:message key="select1" />后页面重新加载
  		function pageReload(){
  			closeColChooseWin();
  			initTab(true);
  			if($('#firmdes').val() && $('#firmid').val())	
  	  			$('#tabs').tabs('getSelected').panel("body").find("#datagrid").datagrid('reload');
			else {
				//alerterror('<fmt:message key="please_select_branche"/>');
				/* showMessage({
					type: 'error',
					msg: '<fmt:message key="please_select_branche"/>',
					speed: 1500
				}); */
			}
  		}
  	 </script>
  </body>
</html>