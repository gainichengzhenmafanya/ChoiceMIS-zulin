<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
response.setHeader("Pragma","No-cache"); 
response.setHeader("Cache-Control","no-cache"); 
response.setDateHeader("Expires", 0); 
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>		
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
  	<style type="text/css">
  		body{
/*   			background: url('<%=path%>/image/login/bj_2.png') center center; */
  		}
  		.login_Div{
  			position: absolute;
  			width:100%;
  			height:618px;
  			top:67px;
  			/* background: url('<%=path%>/image22/login/login_fd.png.png') no-repeat center center; */
  		}
	  	form {
	  	/* 	border :solid;
	  		border-width:5px;
	  		position: absolute;
	  		height: 300px;
	  		width: 500px;
	  		padding-right: 0px; */
	  		
		 	position: absolute;
			top:50%;
			left:50%;
			margin:-190px  0  0 -512px;
			width:1024px;
			height:380px;
	  		
	  	}
	  	* html .logo { 
			filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true, sizingMethod=scale, src="<%=path%>/image/login/logo.png"); 
			background:none; 
		}
	  	.logo {
	  		width:410px;
	  		height:70px;
	  		background: url('<%=path%>/image/login/logo.png') center center;
  			position: absolute;
	  		padding: 0px ;
	  		top: 100px;
	  		left: 40px;
	  	}
	  	.groupid {
  			position: absolute;
	  		padding: 0px ;
	  		top: 35px;
	  		left: 600px;
	  	}
	  	.nameDiv {
  			position: absolute;
	  		padding: 0px ;
	  		top: 85px;
	  		left: 600px;
	  	}
	  	.pwdDiv {
	  		position: absolute;
	  		padding: 0px;
	  		top: 135px;
	  		left: 600px;
	  	}
	  	.operateDiv{
	  		position: absolute;
	  		padding: 0px;
	  		top: 185px;
	  		left: 600px;
	  	}
	  	.autoLogin{
	  		position: absolute;
	  		padding: 0px;
	  		top: 10000000000px;
	  		left: 600px;
	  		display: none;
	  	}
	  	.suggest{
	  		position: absolute;
	  		padding: 0px;
	  		bottom: 25px;
	  		left: 360px;
	  		color:#FFF;
	  	}
	  	.reg{
	  		position: relative;
	  		float: left;
	  		color:white;
	    		font-size:14px;
		          font-family:微软雅黑;
		          cursor: pointer;
		          width: 80px;
		          margin-top: 5px;
	  	}
	  	* html .login { 
			filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true, sizingMethod=scale, src="<%=path%>/image/login/dl_1.png"); 
			background:none; 
		}
	  	.login{
	  		position: absolute;
	  		bottom: 89px;
	  		left: 599px;
	  		width: 90px;
	  		height:35px;
	  		background: url('<%=path%>/image/login/dl_1.png') center center;
	  		cursor: pointer;
	  	}
	  	* html .login_ { 
			filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true, sizingMethod=scale, src="<%=path%>/image/login/dl_.png"); 
			background:none; 
		}
	  	.login_{
	  		position: absolute;
	  		bottom: 89px;
	  		left: 599px;
	  		width: 90px;
	  		height:35px;
	  		background: url('<%=path%>/image/login/dl_.png') center center;
	  		cursor: pointer;
	  	}
	  	input {
		          color:#4E4E4E;
		          font-size:18px;
		       /*   font-family:verdana,simsun,sans-serif; */
		        /*   font-weight:700; */
		          width:150px;
		          height:25px;
		          line-height:25px;
		          border:none;
		    }
	    .label{
	    	color:white;
	    	font-size:14px;
	      font-family:verdana,simsun,sans-serif;
	    }  
	    #locale{
	    	position:absolute;
	    	top:30px;
	    	right:20px;
	    }
	    .mess_{
	    	margin-left:10px;
	    	margin-top:10px;
	    	color: #FFF;
	    }
	    .mess_1{
	    	margin-left:210px;
	    	margin-top:150px;
	    	color: #FFF;
	    }
  	</style>
  	<link rel="shortcut icon" href="<%=path %>/image/boh/logo.ico" />
	<link rel="icon" href="<%=path %>/image/boh/logo.ico" />
  </head>	
  
  <body> 
  	<div id="wait2" style="display:none;"></div>
	<div id="wait" style="display:none;">
		<img src="<%=path%>/image/loading_detail.gif" />
		&nbsp;
		<span style="color:white;font-size:15px;">登录中...</span>
	</div>  
  
  	<div class="login_Div">
  	
  	</div>
 	 	<form method="post" id="loginForm" name="loginForm" action="<%=path%>/login/loginIn.do" style="background:url(<%=path%>/image/login/form_bj.png) no-repeat center;">
 	 		<div class="logo"></div>
 	 		<div class="mess_">欢迎使用辰森餐饮信息平台，请登录！</div>
 	 		<div class="mess_1">北京辰森世纪科技股份有限公司</div>
 	 		<div class="logo"></div>
 	 		<label style="position: absolute;top:30px;right: 135px;font-size: 15px;color: #fff">语言Language</label>
 	 		<select id="locale" name="locale">
 	 			<option value="zh_CN">简体中文-zh_CN</option>
 	 			<option value="zh_TW">繁体中文-zh_TW</option>
 	 			<option value="en">英文-en</option>
 	 		</select>
 	 		<input id="remember" name="remember" type="hidden" value="${cookie.remember.value}" />
 	 		<div class="groupid">
 	 			<label class="label">企业号Enterprise NO.</label>
 	 			<input type="text" style="width: 140px;position: absolute;top: 16px;left:0px" id="pk_group" name="pk_group" value="${cookie.pk_group.value}" onfocus="this.select()"/>
 	 		</div>
 	 		<div class="nameDiv">
 	 			<label class="label">用户名USERNAME</label>
 	 			<input type="text" style="width: 140px;position: absolute;top: 16px;left:0px" id="accountName" name="name" value="${cookie.accountName.value}" onfocus="this.select()"/>
 	 		</div>
 	 		<div class="pwdDiv">
 	 			<label class="label">密&nbsp;&nbsp;码PASSWORD</label>
 	 			<input type="password" style="width: 140px;position: absolute;top: 16px;left:0px" " id="accountPwd" name="password" value="${cookie.password.value}" onfocus="this.select()"/>

 	 		</div>
 	 		<div class="operateDiv">
 	 			<label class="label">验证码VERIFICATION CODE</label>
 	 			<input type="text" id="inputCode" style="width: 65px;position: absolute;top: 17px;left:0px" font-size:15px;" onfocus="this.select()"></input>
 	 			<span id="code"  style="width: 60px;padding-left:7px;position: absolute;top: 17px;left:75px;font-style:oblique; font-size:18px;cursor: pointer;background: #c0c0c0;color:#FFF;-moz-inline-box;display:inline-block;" onclick="getCode()"></span>
 	 		</div>
 	 		<div class="autoLogin"> 			
 	 		 	<input type="checkbox" id="autoLogin" style="width: 15px;position: absolute;top:70px" <c:if test="${cookie.remember.value=='T'}">checked="checked"</c:if> /><label style="position: absolute;top:25px;left:15px;color: #FFF">记住密码</label>
 	 		</div>	
 	 		<div class="login" id="login" ></div>
 	 		<div class="suggest" style="width:300px;  border-bottom:solid;border-width:1px; border-bottom-color: white;">
 	 			建议使用谷歌Chrome浏览器,屏幕分辨率不低于1024*768
 	 		</div>
	 	 	</form>
  	 
  	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.min.js"></script>
  	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
  	 <script type="text/javascript">
  	 	var code='';//存放验证码
  	 	var defaultName="";
  	 	var defaultLanguage="";
  	 	$(document).ready(function(){
  	 		getCode();
  	 		$('#pk_group').focus();
  	 		
  	 		defaultpk_group = '${cookie.pk_group.value}';
  	 		defaultName = '${cookie.accountName.value}';
  	 		defaultLanguage = '${cookie.locale.value}'; 
  	 		for(var i=0;i<3;i++){
  	 			if($("#locale")[0].options[i].value==defaultLanguage){
  	 				$("#locale")[0].options[i].selected=true;
  	 			}
  	 		}
  	 		
  	 		var $remember = $('#remember'),
  	 		$body = $(document.body),
 			$loginForm = $('#loginForm'),
 			top = ($body.height() - $loginForm.outerHeight())*0.5,
 			left = ($body.width() - $loginForm.outerWidth())*0.5;
  	 		
  	 		if($remember.val() === 'T'){
  	 			<c:if test="${loginOut!='loginOut'}">
  	 				login();
  	 			</c:if>
  	 			
  	 		}
  	 			
  	 		
  	 		$('#login').click(function(){
  	 			login();
  	 		});
  	 		
  	 		//添加“回车”事件
  	 		$(document).keyup(function(e){
  	 			if(e.keyCode === 13){
  	 				login();
  	 			}
  	 		});
  	 		
  	 		<c:if test="${not empty info}">
	  	 		showMessage({
	  	 			type: 'error', 
	  	 			msg:'<c:out value="${info}"/>',
	  	 			speed:2500
	  	 		});
  	 		</c:if>
  	 		
  	 		$('#login').bind('mousedown.login',function(){
  	 			$('#login').removeClass("login");
  	 			$('#login').addClass("login_");
  	 		});
  	 		$('#login').bind('mouseup.login',function(){
  	 			$('#login').removeClass("login_");
  	 			$('#login').addClass("login");
  	 		});
  	 		//$('.reg').bind('click.reg',function(){
  	 		//	reg();
  	 		//});
  	 		
  	 		<c:if test="${loginOut=='loginOut'}">
//   	 			$("#accountName").val("");
  	 			$("#accountPwd").val("");
  	 			$("#autoLogin")[0].checked=false;
			</c:if>
  	 		
 			
  	 			
  	 	});
  	 	
  	 	function reg(){
  	 		alert("注册暂未开放！");return;
  	 		$('body').window({
						id: 'window_reg',
						title: '用户注册',
						content: '<iframe id="register" frameborder="0" src="<%=path%>/register/initRegister.do"></iframe>',
						width: 497,
						height: 180,
						isModal: true
					});
  	 	}
  	 	function login(){
  	 		var pk_group = $("#pk_group").val();
  	 		var accountName = $("#accountName").val();
  	 		var accountPwd = $("#accountPwd").val();
  	 		if(pk_group ==''){
  	 			alert('Enterprise NO. cannot be empty！');
  	 			$("#wait2").css('display','none');
  	 			$("#wait").css('display','none');
  	 			return;
  	 		}
  	 		if(accountName==''){
  	 			alert('USERNAME cannot be empty！');
  	 			$("#wait2").css('display','none');
  	 			$("#wait").css('display','none');
  	 			return;
  	 		}
  	 		if(accountPwd==''){
  	 			alert('PASSWORD cannot be empty！');
  	 			$("#wait2").css('display','none');
  	 			$("#wait").css('display','none');
  	 			return;
  	 		}
  	 		if(checkCode()){
  	 			$("#autoLogin")[0].checked?$('#remember').val('T'):$('#remember').val('F');
	  	 		$('#loginForm').submit();
  	 		}  	 	   
		}
  	 	//产生验证码
  	 	function getCode(){
  	 		var codeString=[2,3,4,5,6,7,8,9,'a','b','c','d','e','f','g','h','i','j','k','m','n','o','p','q','r','s','t','u','v','w','x','y'
 			                ,'z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
 			var a=Math.floor(Math.random()*58);
 			var b=Math.floor(Math.random()*58);
 			var c=Math.floor(Math.random()*58);
 			var d=Math.floor(Math.random()*58);
 			var htmlCode =""+codeString[a]+codeString[b]+codeString[c]+codeString[d];
 			$("#code").html(htmlCode);
 			code=""+codeString[a]+codeString[b]+codeString[c]+codeString[d];
  	 	}
  	 	//判断验证码是否正确
  	 	function checkCode(){
  	 		if($('#remember').val()==='T' && defaultName==$('#accountName').val()){
  	 			return true;
  	 		}
  	 		var inputCode = $("#inputCode").val();
  	 		if(inputCode.toUpperCase() != code.toUpperCase()){
  	 			loadError();
				showMessage({
					type: 'error',
					msg: '验证码错误！',
					speed: 3000
				});	
				$('#inputCode').focus();
				
  	 		//	alert("验证码错误！");
  	 			return false;
  	 		}
  	 		return true;
  	 	}
		$("#login").bind("click", function(){
			$("#wait2").css('display','block');
			$("#wait").css('display','block');
		});
		function loadError(){
			$("#wait2").css('display','none');
			$("#wait").css('display','none');
		}
  	 </script>
  </body>
</html>
