<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head lang="en">
    <meta name="viewport" content="width=device-width,height=device-height, initial-scale=1.0,user-scalable=no"/>
    <title></title>
    <style >
        body{
            background: url("/image/unauthorized.jpg");
            width: auto;
            height: auto;
            background-size: cover;
            background-repeat: no-repeat;
            background-origin: content-box;
            background-position: center;
            min-width: 800px;
            padding: 0px;
            margin: 0px;
        }
        .error{
            font-size: 20px;
            font-weight: bolder;
            padding-left: 48%;
            padding-top: 16%;
            color: #fff;
        }
    </style>
</head>
<body>
<div class="main">
    <div class="error">
        <span>对不起您没有访问权限，请返回 ...... </span>
    </div>

</div>
</body>
</html>
