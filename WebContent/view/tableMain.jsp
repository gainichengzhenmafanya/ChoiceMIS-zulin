<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>我的工作台</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/view/tableMain.css" />
			<style type="text/css">
				.table-body{
					height:190px;
    					overflow-x: hidden;
    					overflow-y: auto;
				}
				.grid {
					position : static;
				}
				.refresh{
/* 					visibility: hidden; */
					color: blue;cursor: pointer;
					margin:5px auto -3px 20px;
				}
				body{
					height: 95%;
				}
				
				#myRemind li{
					font-size:15px;
				}
			</style>
		</head>
		
	<body style="overflow:auto;">
		<div class="refresh" style="width:50%;float:left;"><fmt:message key="refresh" /><fmt:message key="my-desktop" />>>></div>
		<div id="message" style="width:40%;float:right;text-align:right;padding-right:30px;">
		<span style="cursor: pointer;text-decoration: underline;" onmousemove="$(this).css('color','blue')" onmouseout="$(this).css('color','black')" 
			onclick="showInfo('62d1a702b589499abd370d98a9643caa','<fmt:message key="my_desktop"/>','/myDesk/open.do')"><fmt:message key="scm_set_up_the" /><fmt:message key="my-desktop" />>>></span></div>
		
		<!-- m_33_3   m_33_1   m_33_2  m_33_4  bs门店boh用 wjf -->
		<div id="m_33_3" style="display: none;height:180px;background: none repeat scroll 0 0 white;border: 1px solid #DEDCD8; margin: 10px 5px;padding: 1px;width:98%;float:left;" >
			<h4 class="moduleHeader">
			    <span style="float: left;font-size: 90%;font-weight: bold;"><fmt:message key="Daily_guide"/></span>
			</h4>
			<jsp:include page="misboh/storeGuideList.jsp" />
		</div>
		<div id="m_33_1" class="module" style="display: none;height: 235px;" >
		<h4 class="moduleHeader">
		    <span style="float: left;font-size: 90%;font-weight: bold;"><fmt:message key="The_current_store_needs_to_report_today"/></span>
		  </h4>
			<div id="" class="grid" style="height: 220px;" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:100px;"><fmt:message key="report_Category"/></span></td>
								<td><span style="width:70px;"><fmt:message key="reported_date"/></span></td>
								<td><span style="width:60px;"><fmt:message key="reported_time"/></span></td>
								<td><span style="width:70px;"><fmt:message key="Delivery_date"/></span></td>
								<td><span style="width:60px;"><fmt:message key="Delivery_time"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="schedule" items="${scheduleList}" varStatus="status">
								<tr>
									<td><span style="width:100px;">${schedule.category_Code }</span></td>
									<td><span style="width:70px;">${schedule.orderDate }</span></td>
									<td><span style="width:60px;">${schedule.orderTime }</span></td>
									<td><span style="width:70px;">${schedule.receiveDate }</span></td>
									<td><span style="width:60px;">${schedule.receiveTime }</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div> 
		<div id="m_33_2" class="module" style="display: none;height: 235px;" >
		<h4 class="moduleHeader">
		    <span style="float: left;font-size: 90%;font-weight: bold;"><fmt:message key="The_current_store_needs_to_inspect_today"/></span>
		  </h4>
		  <jsp:include page="misboh/updateInspection.jsp" />
		</div> 
		<div id="m_33_4" class="module" style="height: 150px;display: none;">
			<h4 class="moduleHeader">
			    <span class="title" ><fmt:message key="my_remind"/></span>
			</h4>
			<div>
				<ul id="myRemind" style="height:100px;">
					<c:choose>
						<c:when test="${chkstomCount == 0 && chkinmCount == 0 && chkinmzfCount == 0 && chkoutmCount == 0 && chkoutmdbCount == 0}">
							<li><fmt:message key="Nearly_three_days_you_donot_have_uncheck_orders"/></li>
						</c:when>
						<c:otherwise>
							<c:if test="${chkstomCount != 0 }">
								<li>&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="nearly_three_days_you_have"/>
									<span style="cursor: pointer;color: red;" onclick="openUnChecked('chkstom')">${chkstomCount }</span>
									<fmt:message key="article"/><fmt:message key="Report_order"/><fmt:message key="unchecked"/>
									&nbsp;&nbsp;&nbsp;&nbsp;<span style="cursor: pointer;color: red;" onclick="openUnChecked('chkstom')"><fmt:message key="Clicktoview"/>>></span>
								</li>
							</c:if>
							<c:if test="${chkinmCount != 0 }">
								<li>&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="nearly_three_days_you_have"/>
									<span style="cursor: pointer;color: red;" onclick="openUnChecked('chkinm')">${chkinmCount }</span>
									<fmt:message key="article"/><fmt:message key="In_orders"/><fmt:message key="unchecked"/>
									&nbsp;&nbsp;&nbsp;&nbsp;<span style="cursor: pointer;color: red;" onclick="openUnChecked('chkinm')"><fmt:message key="Clicktoview"/>>></span>
								</li>
							</c:if>
							<c:if test="${chkinmzfCount != 0 }">
								<li>&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="nearly_three_days_you_have"/>
									<span style="cursor: pointer;color: red;" onclick="openUnChecked('chkinmzf')">${chkinmzfCount }</span>
									<fmt:message key="article"/><fmt:message key="Inout_order"/><fmt:message key="unchecked"/>
									&nbsp;&nbsp;&nbsp;&nbsp;<span style="cursor: pointer;color: red;" onclick="openUnChecked('chkinmzf')"><fmt:message key="Clicktoview"/>>></span>
								</li>
							</c:if>
							<c:if test="${chkoutmCount != 0 }">
								<li>&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="nearly_three_days_you_have"/>
									<span style="cursor: pointer;color: red;" onclick="openUnChecked('chkoutm')">${chkoutmCount }</span>
									<fmt:message key="article"/><fmt:message key="Out_orders"/><fmt:message key="unchecked"/>
									&nbsp;&nbsp;&nbsp;&nbsp;<span style="cursor: pointer;color: red;" onclick="openUnChecked('chkoutm')"><fmt:message key="Clicktoview"/>>></span>
								</li>
							</c:if>
							<c:if test="${chkoutmdbCount != 0 }">
								<li>&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="nearly_three_days_you_have"/>
									<span style="cursor: pointer;color: red;" onclick="openUnChecked('chkoutmdb')">${chkoutmdbCount }</span>
									<fmt:message key="article"/><fmt:message key="Transfer_order"/><fmt:message key="unchecked"/>
									&nbsp;&nbsp;&nbsp;&nbsp;<span style="cursor: pointer;color: red;" onclick="openUnChecked('chkoutmdb')"><fmt:message key="Clicktoview"/>>></span>
								</li>
							</c:if>
						</c:otherwise>
					</c:choose>
				</ul>
			</div>
		</div>
		<div id="m_1_2" class="module" style="height: 150px;display: none;">
		<h4 class="moduleHeader">
		    <span class="title" ><fmt:message key="announcement_notification"/></span>
		  </h4>
			<ul>
				<marquee  direction=up height=120px id=m onmouseout=m.start() onMouseOver=m.stop() scrollamount=2 >
					<c:forEach var="announce" items="${announcementList}" varStatus="status">
						<li onclick="showAnnounce(<c:out value="${announce.id}" />)" style="cursor: pointer;"><c:out value="${announce.title}" />
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<c:out value="${announce.create_time}" /></li>
					</c:forEach>
				</marquee>
			</ul>
		</div>
<!-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->			
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script language="JavaScript" src="<%=path%>/Charts/FusionCharts.js"></script>
		
		<script type="text/javascript">
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
			$(document).ready(function(){
				
				//显示菜单
				var menu = '${menu}';
				if(menu==''){
					$("#message").find("span").html('<fmt:message key="you_have_not_set_my_desktop_remind"/>！');
				}else{
					var menuArray = menu.split(',');
					var chartSwf = "Column3D.swf";
					for(var i in menuArray){
						$("#"+menuArray[i]).show();
						if(menuArray[i]!="m_1_1" && menuArray[i]!="m_1_2" && menuArray[i]!="m_2_5"){
							var w = $("#"+menuArray[i]).width();
							var h = $("#"+menuArray[i]).height()-20;
							var num = 3;
							if(menuArray[i]=="m_2_1" ||menuArray[i]=="m_2_2"){
								w=w-10;
							}
							if(menuArray[i]=="m_2_3" ||menuArray[i]=="m_2_4"){
								num = $("#num_"+menuArray[i]).val();
								chartSwf = "ScrollColumn2D.swf";
							}
							if(menuArray[i]=="m_2_6"){
								chartSwf = "Area2D.swf";
							}
							if(menuArray[i]=="m_2_8"){
								chartSwf = "Pie3D.swf";
							}
							if(menuArray[i]=="m_2_9"){
								chartSwf = "Line.swf";
							}
							if(menuArray[i]=="m_2_10"){
								chartSwf = "Column3D.swf";
							}
							
							//图表
							var chart = new FusionCharts("<%=path%>/Charts/"+chartSwf+"?ChartNoDataText=无数据显示", "myChart"+menuArray[i], w, h);
							chart.setDataURL(escape("<%=path%>/mainInfo/getXml.do?id="+menuArray[i]+"&days="+num));
							chart.render(menuArray[i]+"_div");
						}
					}
				}
				//table行加斑马色
				$('.grid').find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
				);
				$('.refresh').bind('click',function refresh(){//刷新
					location.href = location.href;
				});
				
			});
			
			function show(id){
				$("#"+id)[0].style.visibility='visible';
			}
			
			function hide(id){
				$("#"+id)[0].style.visibility='hidden';
			}
			
			//查看公告
			function showAnnounce(announcementId){
				 $('body').window({
					id: 'window_addAnnouncement',
					//title: '查看公告',
					content: '<iframe id="showAnnouncementFrame" frameborder="0" src="<%=path%>/announcement/show.do?id='+announcementId+'"></iframe>',
					width: 520,
					height: '380px',
					confirmClose:false,
					draggable: true,
					isModal: true
				});
				
			};
			//打开提醒里边的信息
			function openUnChecked(type){
				if(type=='chkstom'){
					showInfo('04948F998FC54E15B049488A6DC8037D','<fmt:message key="Report_order"/><fmt:message key="Fill_in"/>','/chkstomMis/chkstomTable.do');
					searchUncheck('iframe_04948F998FC54E15B049488A6DC8037D');
				}else if(type=='chkinm'){
					showInfo('D0EDA324850B4CC9BDED565B8D902FFF','<fmt:message key="In_orders"/><fmt:message key="Fill_in"/>','/chkinmMis/add.do?action=init');
					searchUncheck('iframe_D0EDA324850B4CC9BDED565B8D902FFF');
				}else if(type=='chkinmzf'){
					showInfo('96CE49A2774145A3B742EE394A5AF762','<fmt:message key="Inout_order"/><fmt:message key="Fill_in"/>','/chkinmZbMis/addzb.do?action=init');
					searchUncheck('iframe_96CE49A2774145A3B742EE394A5AF762');
				}else if(type=='chkoutm'){
					showInfo('AD216F61A31B4E85803C15A37857465A','<fmt:message key="Out_orders"/><fmt:message key="Fill_in"/>','/chkoutMis/addChkout.do?action=init');
					searchUncheck('iframe_AD216F61A31B4E85803C15A37857465A');
				}else if(type=='chkoutmdb'){
					showInfo('C5B73F18F2A54057BA47DDFB0B3B082E','<fmt:message key="Transfer_order"/><fmt:message key="Fill_in"/>','/chkoutDbMis/addChkout.do?action=init');
					searchUncheck('iframe_C5B73F18F2A54057BA47DDFB0B3B082E');
				}
			};
	  	 	function showInfo(moduleId,moduleName,moduleUrl){
	  	 		window.parent.tabMain.addItem([{
		  				id: 'tab_'+moduleId,
		  				text: moduleName,
		  				title: moduleName,
		  				closable: true,
		  				content: '<iframe id="iframe_'+moduleId+'" name="iframe_'+moduleId+'" frameborder="0" src="<%=path%>'+moduleUrl+'"></iframe>'
		  			}
		  		]);
	  	 		window.parent.tabMain.show('tab_'+moduleId);
	  	 	}
	  	 	
	  	 	function searchUncheck(id){
	  	 		var bdate = '${unCheckBdate}';
				var edate = '${unCheckEdate}';
	  	 		window.setTimeout(function(){
					var frame = parent.document.getElementById(id);
					if(frame && frame.contentWindow)
						frame.contentWindow.searchUncheck(bdate,edate);
				},1000);
	  	 	}
		</script>
	</body>
</html>