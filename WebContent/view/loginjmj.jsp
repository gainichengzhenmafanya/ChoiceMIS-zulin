<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%
String path = request.getContextPath();
response.setHeader("Pragma","No-cache"); 
response.setHeader("Cache-Control","no-cache"); 
response.setDateHeader("Expires", 0); 
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>		
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
  	<style type="text/css">
	  	 * html .login { 
			filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true, sizingMethod=scale, src="<%=path%>/image/login-jmj/dl_1.png"); 
			background:none; 
		}
	  	.login{
	  		position: absolute;
	  		left: 795px;
	  		width: 90px;
	  		height:110px;
	  		background: url('<%=path%>/image/login-jmj/dl_1.png') center center;
	  		cursor: pointer;
	  	}
	  	* html .login_ { 
			filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true, sizingMethod=scale, src="<%=path%>/image/login-jmj/dl_.png"); 
			background:none; 
		}
	  	.login_{
	  		position: absolute;
	  		left: 795px;
	  		width: 90px;
	  		height:110px;
	  		background: url('<%=path%>/image/login-jmj/dl_.png') center center;
	  		cursor: pointer;
	  	}
	  	.suggest{
	  		position: absolute;
	  		padding: 0px;
	  		text-align:center;
	  		bottom: 25px;
	  		color:#FFF;
	  		width: 100%;
	  	}
	  	.reg{
	  		position: relative;
	  		float: left;
	    		font-size:14px;
		          font-family:微软雅黑;
		          cursor: pointer;
		          width: 80px;
		          margin-top: 5px;
	  	}
	  	.nameDiv {
  			position: absolute;
	  		padding: 0px ;
	  		left: 600px;
	  	}
	  	.nameLabel {
  			position: absolute;
	  		padding: 0px ;
	  		left: 539px;
	  	}
	  	.pwdLabel {
	  		position: absolute;
	  		padding: 0px;
	  		top: 46.5%;
	  		left: 539px;
	  	}
	  	.pwdDiv {
	  		position: absolute;
	  		padding: 0px;
	  		top: 46%;
	  		left: 600px;
	  	}
	  	.operateDiv{
	  		position: absolute;
	  		padding: 0px;
	  		left: 600px;
	  	}
	  	.operatelabel{
	  		position: absolute;
	  		padding: 0px;
	  		left: 539px;
	  	}
	  	input {
		          color:#CABA84;
		          font-size:18px;
		          line-height:25px;
		          border:1,solid red;
		          border:solid  #CABA84;
		          border-width: 1px;
		          border-radius:4px;
		    }
	    .label{
	    	font-size:14px;
	    	color:#836D20;
	     	font-family:verdana,simsun,sans-serif;
	    }  
	    #locale{
	    	position:absolute;
	    	color:#836D20;
	    	width:143px;
	    	height:25px;
	   	 	border:1,solid red;
	        border:solid  #CABA84;
		    border-width: 1px;
	    }
	   	.login_title{
	  		position: absolute;
	  		width: 300px;
	  		height:50px;
	  		cursor: pointer;
	  	}
	   
  	</style>
  </head>	
  
  <body> 
  	<div id="wait2" style="display:none;"></div>
	<div id="wait" style="display:none;">
		<img src="<%=path%>/image/loading_detail.gif" />
		&nbsp;
		<span style="color:white;font-size:15px;">登录中...</span>
	</div>  
 	 	<form method="post" id="loginForm" name="loginForm" action="<%=path%>/login/loginIn.do" >
 	 		<img src="<%=path%>/image/login-jmj/form_bj.png" class="logimg" />
 	 		<select id="locale" name="locale">
 	 			<option value="zh_CN">简体中文-zh_CN</option>
 	 			<option value="zh_TW">繁体中文-zh_TW</option>
 	 			<option value="en">英文-en</option>
 	 		</select>
 	 		<img src="<%=path%>/image/login-jmj/logo_title.png" class="login_title" id="login_title"/>
 	 		<input id="remember" name="remember" type="hidden" value="${cookie.remember.value}" />
 	 		<div class="nameLabel"><label class="label" style="text-align: center;">用户名:</label></div>
 	 		<div class="nameDiv">
	 	 			<input type="text" style="width: 140px;position: absolute;left:0px;width: 180px;height: 25px;" id="accountName" name="name" value="${cookie.accountName.value}" onfocus="this.select()"/>
 	 		</div>
 	 		<div class="pwdLabel">
 	 			<label class="label">密&nbsp;&nbsp;&nbsp;码:</label>
 	 		</div>
 	 		<div class="pwdDiv">
 	 			<input type="password" style="width: 140px;position: absolute;left:0px;width: 180px;height: 25px;"  id="accountPwd" name="password" value="${cookie.password.value}" onfocus="this.select()"/>
 	 		</div>
 	 		<div class="operatelabel">
 	 			<label class="label">验证码:</label>
 	 		</div>
 	 		<div class="operateDiv">
 	 			<input type="text" id="inputCode" style="width: 95px;position: absolute;left:0px;font-size:15px;" onfocus="this.select()"></input>
 	 			<span id="code"  style="width: 60px;padding-left:7px;position: absolute;left:105px;font-style:oblique; font-size:18px;cursor: pointer;background: #EEEEEE;-moz-inline-box;display:inline-block;" onclick="getCode()"></span>
 	 		</div>
 	 		<div class="login" id="login" ></div>
 	 		<div class="suggest" style="color:#ECECE4; ">
 	 			麦点九毛九餐饮连锁有限公司 版权所有<br/>
 	 			建议使用IE7.0及其以上版本,屏幕分辨率不低于1024*768
 	 		</div>
	 	 	</form>
  	 
  	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.min.js"></script>
  	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
  	 <script type="text/javascript">
  	 
  	$(window).resize(function() { 
  		setImg();
  	 });
  	 
  	function setImg(){
  		$body = $(document.body),
  		$(".logimg").css("height",$body.height()+"px");
  		$(".logimg").css("width",$body.width()+"px");
  		var lheight = $body.height()*0.055;
		$("#locale").css("top",lheight+"px");
		var height = $body.height()*0.425;
		$(".login_title").css("top",(height-55)+"px");
		$(".nameLabel").css("top",height+"px");
		$(".nameDiv").css("top",height+"px");
		$(".login_").css("top",height+"px");
		$(".login").css("top",height+"px");
		height = height+40;
		$(".pwdLabel").css("top",height+"px");
		$(".pwdDiv").css("top",height+"px");
		height = height+40;
		$(".operatelabel").css("top",height+"px");
		$(".operateDiv").css("top",height+"px");
		
		var width = $body.width()*0.50;
		$(".nameLabel").css("left",width+"px");
		$(".pwdLabel").css("left",width+"px");
		$(".operatelabel").css("left",width+"px");
		$(".login_title").css("left",(width-10)+"px");
		width = width+50;
		$(".nameDiv").css("left",width+"px");
		$(".pwdDiv").css("left",width+"px");
		$(".operateDiv").css("left",width+"px");
		width = width+200;
		$(".login_").css("left",width+"px");
		$(".login").css("left",width+"px");
		
		width = $body.width()*0.02;		
		$("#locale").css("right",width+"px");
  	}
  	 	var code='';//存放验证码
  	 	var defaultName="";
  	 	var defaultLanguage="";
  	 	$(document).ready(function(){
  	 		getCode();
  	 		$('#accountName').focus();
  	 		
  	 		defaultName = '${cookie.accountName.value}';
  	 		defaultLanguage = '${cookie.locale.value}'; 
  	 		for(var i=0;i<3;i++){
  	 			if($("#locale")[0].options[i].value==defaultLanguage){
  	 				$("#locale")[0].options[i].selected=true;
  	 			}
  	 		}
  	 		
  	 		var $remember = $('#remember'),
  	 		$body = $(document.body),
 			$loginForm = $('#loginForm'),
 			top = ($body.height() - $loginForm.outerHeight())*0.5,
 			left = ($body.width() - $loginForm.outerWidth())*0.5;
  	 		// 设置文本框 按钮位置
  	 		setImg();
  	 		
  	 		if($remember.val() === 'T'){
  	 			<c:if test="${loginOut!='loginOut'}">
  	 				login();
  	 			</c:if>
  	 			
  	 		}
  	 		$('#login').click(function(){
  	 			login();
  	 		});
  	 		
//   	 		添加“回车”事件
//   	 		$(document).keyup(function(e){
//   	 			if(e.keyCode === 13){
//   	 				login();
//   	 			}
//   	 		});
  	 		$('#accountName').bind('keypress',function(even){
  	            if(event.keyCode == "13")    
  	             {
  	            	$('#accountPwd').focus();
  	             }
  	 		});
  	 		$('#accountPwd').bind('keypress',function(even){
  	            if(event.keyCode == "13")    
  	             {
  	            	$('#inputCode').focus();            	
  	             }
  	 		});
  	 		$('#inputCode').bind('keypress',function(even){
  	            if(event.keyCode == "13")    
  	             {
  	          		login();
  	             }
  	 		});
  	 		
  	 		
  	 		<c:if test="${not empty info}">
	  	 		showMessage({
	  	 			type: 'error', 
	  	 			msg:'<c:out value="${info}"/>',
	  	 			speed:2500
	  	 		});
  	 		</c:if>
  	 		
  	 		$('#login').bind('mousedown.login',function(){
  	 			$('#login').removeClass("login");
  	 			$('#login').addClass("login_");
  	 		});
  	 		$('#login').bind('mouseup.login',function(){
  	 			$('#login').removeClass("login_");
  	 			$('#login').addClass("login");
  	 		});
  	 		//$('.reg').bind('click.reg',function(){
  	 		//	reg();
  	 		//});
  	 		
  	 		<c:if test="${loginOut=='loginOut'}">
//   	 			$("#accountName").val("");
  	 			$("#accountPwd").val("");
  	 			$("#autoLogin")[0].checked=false;
			</c:if>
  	 		
 			
  	 			
  	 	});
  	 	
  	 	function reg(){
  	 		alert("注册暂未开放！");return;
  	 		$('body').window({
						id: 'window_reg',
						title: '用户注册',
						content: '<iframe id="register" frameborder="0" src="<%=path%>/register/initRegister.do"></iframe>',
						width: 497,
						height: 180,
						isModal: true
					});
  	 	}
  	 	function login(){
  	 		var accountName = $("#accountName").val();
  	 		var accountPwd = $("#accountPwd").val();
  	 		if(accountName=='' || accountPwd==''){
  	 			alert("用户名或密码不能为空！");
  	 			$("#wait2").css('display','none');
  	 			$("#wait").css('display','none');
  	 			return;
  	 		}
  	 		if(checkCode()){
				 <c:choose>
		  	 	 <c:when test="${loginOut=='loginOut'}">    
	  	 			$('#loginForm').submit();
		  	 	 </c:when>
		  	 	 <c:otherwise>  
					$('#loginForm').attr('target','login');
					var showwin= window.open("about:blank","login",'status=no,toolbar=no,menubar=no,location=no,z-look=no,scrollbars=no,resizable=yes,width='+(screen.availWidth-10)+',height='+(screen.availHeight-30)+',top=0,left=0'); 
					$('#loginForm').submit();
					window.opener=null;window.open('','_self');
			        if(showwin)
			        {
			            showwin.focus();
			            window.close();
			        }
			        else
			        {
			            alert("检测到弹出窗口阻止程序。您的 Web 浏览器必须允许该站点弹出窗口");
			        }
					
		  	 	 </c:otherwise>
		  	 	 </c:choose> 
  	 		}  	 	   
		}
  	 	//产生验证码
  	 	function getCode(){
  	 		var codeString=[2,3,4,5,6,7,8,9,'a','b','c','d','e','f','g','h','i','j','k','m','n','o','p','q','r','s','t','u','v','w','x','y'
 			                ,'z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
 			var a=Math.floor(Math.random()*58);
 			var b=Math.floor(Math.random()*58);
 			var c=Math.floor(Math.random()*58);
 			var d=Math.floor(Math.random()*58);
 			var htmlCode =""+codeString[a]+codeString[b]+codeString[c]+codeString[d];
 			$("#code").html(htmlCode);
 			code=""+codeString[a]+codeString[b]+codeString[c]+codeString[d];
  	 	}
  	 	//判断验证码是否正确
  	 	function checkCode(){
  	 		if($('#remember').val()==='T' && defaultName==$('#accountName').val()){
  	 			return true;
  	 		}
  	 		var inputCode = $("#inputCode").val();
  	 		if(inputCode.toUpperCase() != code.toUpperCase()){
  	 			loadError();
				showMessage({
					type: 'error',
					msg: '验证码错误！',
					speed: 3000
				});	
				$('#inputCode').focus();
				
  	 		//	alert("验证码错误！");
  	 			return false;
  	 		}
  	 		return true;
  	 	}
		$("#login").bind("click", function(){
			$("#wait2").css('display','block');
			$("#wait").css('display','block');
		});
		function loadError(){
			$("#wait2").css('display','none');
			$("#wait").css('display','none');
		}
  	 </script>
  </body>
</html>
