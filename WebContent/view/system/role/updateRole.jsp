<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>role Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	</head>
	<body>
		<div class="form">
			<form id="roleForm" method="post" action="<%=path %>/role/saveByUpdate.do">
				<div  style="height:500px; width:500px;left:50%;top:50%;margin:0px auto;margin-left:160px;margin-top:160px;">
					<input type="hidden" id="id" name="id" value="<c:out value="${role.id}" />"/>
					<input type="hidden" id="menu" name="menu" class="text"/>
					<div class="form-line">
						<div class="form-label"><fmt:message key="name"/></div>
						<div class="form-input"><input type="text" id="name" name="name" class="text" value="<c:out value="${role.name}" />"/></div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="whether_to_enable"/></div>
						<div class="form-input">
							<select id="deleteFlag" name="deleteFlag" style="height: 22px;margin-top: 3px; border: 1px solid #999999;">
							<option value="T"
							<c:if test="${role.deleteFlag=='T'}">
							selected="selected"
							</c:if>
							><fmt:message key="enable"/></option>
							<option value="F" 
							<c:if test="${role.deleteFlag=='F'}">
							selected="selected"
							</c:if>
							><fmt:message key="disable1"/></option>
							</select>
						</div>
					</div>
				</div>
			</form>
		</div>
	</body>
</html>