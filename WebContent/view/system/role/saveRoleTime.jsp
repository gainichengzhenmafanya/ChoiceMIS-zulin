<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>role Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
	</head>
	<body>
		<div class="form">
			<form id="roleTimeForm" method="post" action="<%=path %>/roleTime/saveOrUpdate.do">
			<div  style="height:500px; width:500px;left:50%;top:50%;margin:0px auto;margin-left:160px;margin-top:160px;">
			<input type="hidden" id="status" name="status" class="text" value='${status}'/>
			<input type="hidden" id="roleTimeId" name="roleTimeId" class="text" value='<c:out value="${roleTime.roleTimeId}" />'/>
			<input type="hidden" id="roleId" name="roleId" class="text" value='<c:out value="${roleTime.roleId}" />'/>
				<div class="form-line">
					<div class="form-label">多少天前：</div>
					<div class="form-input">
						<input type="text" id="beforeDay" name="beforeDay" value='<c:out value="${roleTime.beforeDay}"/>' class="text"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label">多少天后：</div>
					<div class="form-input">
						<input type="text" id="afterDay" name="afterDay" value='<c:out value="${roleTime.afterDay}"/>' class="text"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label">提示：</div>
					<div class="form-input">
						参考日期为服务器当前日期！
					</div>
				</div>
			</div>
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>	
		<script type="text/javascript">
		var validate;
		$(document).ready(function(){
			/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'beforeDay',
					validateType:['intege'],
					param:['F'],
					error:['请输入正整数！']
				},{
					type:'text',
					validateObj:'afterDay',
					validateType:['intege'],
					param:['F'],
					error:['请输入正整数！']
				}]
			});
		});
		</script>
	</body>
</html>