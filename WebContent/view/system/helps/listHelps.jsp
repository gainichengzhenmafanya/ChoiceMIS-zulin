<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>帮助列表</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<style type="text/css">
			.tr-select{
				background-color: #D2E9FF;
			}
			.leftFrame{
				width : 23%;
			}
			.mainFrame{
				width : 77%
			}
			.condition{
				border: 1px solid #5A7581;
				margin: 0px 0px 0px 0px;
			 	padding: 1px;
			 	padding-left: 0px;
			 	background-color:#CDDCEE;
			}
		</style>
			<script type="text/javascript">
				var path = "<%=path%>";
			</script>
	</head>
	<body>
	<div class="leftFrame">
		<div class="condition">
			<span>&nbsp;&nbsp;当前系统：</span>
			<span>
				<select id="sysName" onchange="findSys(this)" style="width:80px;margin-left:0px;">
					<option value="SCM">物流</option>
					<option value="TELE">决策</option>
					<option value="CRM">会员</option>
				</select>
			</span>
			&nbsp;&nbsp; <button onclick="addHelp()">新建帮助</button>
		</div>
	    <div class="treePanel">
	    <script src="<%=path%>/js/tree/MzTreeView10.js" type="text/javascript"></script>
        <script type="text/javascript">
	        var tree = new MzTreeView("tree");
	        <c:forEach var="helps" items="${listHelps}" varStatus="status">
	        	tree.nodes['0_${helps.projectId}'] = 'text:${helps.projectName};';
	         	tree.nodes['${helps.projectId}_${helps.parentName}'] = 'text:${helps.parentName};';
	         	tree.nodes['${helps.parentName}_${helps.id}'] = 'text:${helps.childName}-${helps.id}; method:loadCols("${helps.projectId}","${helps.id}")';
	        </c:forEach>
	        tree.setIconPath("<%=path%>/image/tree/none/");
	        document.write(tree.toString());
        </script>
	    </div>
	</div>
	<div style="display: inline-block;float: left;" class="mainFrame">
    		<iframe src="" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
    </div> 
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/ueditor/editor_config.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/ueditor/editor_all_min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/ueditor/dialogs/image/image.js"></script>
	 <script type="text/javascript">
			$(document).ready(function(){
				$("option[value=${project}]").attr("selected","selected"); 
			});
			function addHelp(){
				$('body').window({
					id: 'window_help',
					title: '帮助',
					content: '<iframe id="HelpsFrame" frameborder="0" src="<%=path %>/helps/addHelp.do"></iframe>',
					width: '350px',
					height: '300px',
					draggable: true,
					isModal: true})
			}
			function findSys(dat){
				location.href='<%=path %>/helps/list.do?projectId='+dat.value
			}
			function ClassClick(dat){
				$('.tr-select').removeClass("tr-select");
				$(dat).addClass("tr-select");
				loadCols($.trim($(dat).attr("helpId")));
			};
			function loadCols(projectId,helpId){
				window.mainFrame.location = "<%=path%>/helps/list.do?id="+helpId+"&projectId="+projectId+"&typ=typ";
			}
		</script>
	</body>
</html>