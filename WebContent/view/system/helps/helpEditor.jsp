<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>帮助信息编辑框</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<script type="text/javascript">
			var path = "<%=path%>";
		</script>
	</head>
	<body>
      <div class="tool"></div>
      <form id="someForm" action="<%=path %>/helps/updateHelps.do" method="post">
      	<div class="form-label" align="center" style="font-size: 20px;color: #0072E3;">【${helps.childName }】的帮助信息</div>
		  <div class="form-input" align="center">
		  	  <input type="hidden" name="id" id="childId" value="${helps.id }"/>
			  <textarea wrap="physical" id="content"  name="des" style="width: 100%;height: 100%;text-align: left;">${helps.des }</textarea>
		  </div>
	  </form>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/ueditor/editor_config.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/ueditor/editor_all_min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/ueditor/dialogs/image/image.js"></script>
	 <script type="text/javascript">
			var editor;
			var popWindow;
			$(document).ready(function(){
				loadToolBar();
				editor = UE.getEditor('content');
				//限制文本框的字符长度（汉字算两个字符）
				$("#content").bind('blur keyup',function(){
		 	 		$(this).limitLength(2000);
		 	 		var startStr = $("#content").val().replace(/[^\x00-\xff]/g, "**"); 
					var curLength = startStr.length; 
					$("#count").html(curLength+'/2000');
		 		});
			});
			//将编辑框内容同步内容到content容器
			function getText(){
				var result = true;
				editor.sync(); 
				if(editor.getContent().replace(/[^\x00-\xff]/g, "**").length>3000){
					result = false;
					alert("<fmt:message key="content"/><fmt:message key="length_too_long"/>！");
				}
				return result;
			}
			//加载工具条按钮
			function loadToolBar(){
				$('.tool').toolbar({
					items: [{
							text: '保存',
							title: '保存修改',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								if(getText()){ //此处以非空为例
								    editor.sync();       //同步内容
								    $("#someForm").submit();   //提交Form
								}
							}
						},{
							text: '删除',
							title: '删除修改',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								window.parent.$("body").window({
									title: '<fmt:message key="delete" />帮助',
									content: '<iframe id="deleteHelpFrame" frameborder="0" src="<%=path %>/helps/deleteHelp.do?id='+$('#childId').val()+'"></iframe>',
									width: 238,
									height: '215px',
									draggable: true,
									isModal: true
								});
							}
						},{
							text: '退出',
							title: '退出',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.tab-item .button-click',top.document).find('.button-arrow').click();									
							}
						}
					]
				});
			}
		</script>
	</body>
</html>