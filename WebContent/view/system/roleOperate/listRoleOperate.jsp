<%@ page import="com.choice.framework.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="lo" uri="/WEB-INF/tld/local.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="module_operation_information"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<style type="text/css">
				#toolbar {
					position: relative;
					height: 27px;
				}
			</style>
		</head>
	<body>
	<div class="leftFrame">
	<div id="treeToolbar"></div>
	    <div class="treePanel">
	        <script src="<%=path%>/js/tree/MzTreeView10.js" type="text/javascript"></script>
	        <script type="text/javascript">
	          var tree = new MzTreeView("tree");
	          tree.nodes['0_00000000000000000000000000000000'] = 'text:<fmt:message key="all_modules_directory"/>; dbmethod:selectItem(\'000\',\'00000000000000000000000000000000\')';
	          <c:forEach var="module" items="${tree}" varStatus="status">
		          		tree.nodes['${module.parentModule.id}_${module.id}'] = 'text:${lo:show(module.name)};dbmethod:selectItem(\'${module.code}\',\'${module.id}\')';
	          </c:forEach>
	          tree.setIconPath("<%=path%>/js/tree/");
	          document.write(tree.toString());
	          tree.expandAll();
	        </script>
	    </div>
	    </div>
	    <div class="mainFrame">
	    <div id="toolbar"></div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
						   <tr><td colspan="4"><fmt:message key="role_name"/>：<c:out value="${role.name}" />&nbsp;&nbsp;
						   <font color="red">【注】：双击左侧模块，可在下方显示模块权限名称，选中相关权限保存即可</font></td></tr>
							<tr>
								<td class="num" style="width: 25px;">&nbsp;</td> 
								<td style="width:30px;">
									<input type="checkbox" id="chkAll" checked="checked"/>
								</td>
								<td style="width:196px;"><fmt:message key="module_name"/></td>
								<td id="typeList" style="width:470px;text-align:left;"><fmt:message key="list_of_operations"/>:
								<br />
								<c:forEach var="type" items="${operateType}" varStatus="items">
								<c:if test="${items.index%7==0}"><br /></c:if>
								&nbsp;&nbsp;<input type="checkbox" checked="checked" class="chk_<c:out value="${type.id}" />" /><c:out value="${type.name}" />
								</c:forEach>
								</td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
						<c:forEach var="roleOperate" items="${roleOperateList}" varStatus="num">
						<tr id="col_<c:out value="${roleOperate.id}" />">
							<td class="num" style="width: 25px;"><c:out value="${num.index+1}" /></td>
							<td style="width:30px; text-align: center;">
								<input type="checkbox" id="chk_${roleOperate.id}" value="" checked="checked" />
							</td>
							<td style="width:160px;">
								<c:out value="${lo:show(roleOperate.name)}" />&nbsp;
							</td>
							<td style="width:470px;">
								<c:forEach var="operate" items="${roleOperate.childOperate}" varStatus="operateNum">
									<c:if test="${operateNum.index !=0 && operateNum.index%7==0}">
									<br />
									</c:if>
									&nbsp;&nbsp;<input class="chk_<c:out value="${operate.type}" />" type="checkbox" 
									<c:if test="${operate.useable == '1'}">
									checked="checked"  ck="ck"
									</c:if>
									value="<c:out value="${operate.id}" />" /><c:out value="${operate.name}" />
								</c:forEach>
							</td>
						</tr>
						</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
    	</div>
    	<form id="roleOperateForm" action="<%=path%>/roleOperate/list.do">
    		<input type="hidden" id="id" name="id" value="${role.id}"/>
    	</form>
    	<form id="roleOperateRangeForm" action="<%=path%>/roleOperateRange/list.do">
			<input type="hidden" id="id" name="id" value="<c:out value="${role.id}" />"/>
    	</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript">
		var colNum=1;
		$(document).ready(function(){
			(function(){
			    // remove layerX and layerY
			    var all = $.event.props,
		        len = all.length,
		        res = [];
			    while (len--) {
			    var el = all[len];
			    if (el != 'layerX' && el != 'layerY') res.push(el);
			    }
			    $.event.props = res;
		    }());

			var tooltree = $('#treeToolbar').toolbar({
				items: [{
						text: '<fmt:message key="expandAll" />',
						title: '<fmt:message key="expandAll"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-80px']
						},
						handler: function(){
							tree.expandAll();
						}
					}
				]
			});
			var tool = $('#toolbar').toolbar({
				items: [{
						text: '<fmt:message key="save" />',
						title: '<fmt:message key="save_permissions_information"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','0px']
						},
						handler: function(){
							saveRoleOperateByAdd();
						}
					},{
						text: '<fmt:message key="delete" />',
						title: '<fmt:message key="delete"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							removeLine();
						}
					},"-",
					
					//wangjie 2014年10月27日 16:15:49
// 					{
// 						text: '<fmt:message key="to_role_assignment_purview" />',
// 						title: '<fmt:message key="to_role_assignment_purview"/>',
// 						icon: {
<%-- 							url: '<%=path%>/image/Button/op_owner.gif', --%>
// 							position: ['-100px','-40px']
// 						},
// 						handler: function(){
// 							toNowRoleOperateRange();
// 						}
// 					},"-",
					{
						text: '<fmt:message key="back_roles_page" />',
						title: '<fmt:message key="back_roles_page"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-20px']
						},
						handler: function(){
							 window.location.href="<%=path%>/role/list.do";
						}
					}
				 ]
			});
			
			//全选多选框事件
			$('#chkAll').bind('change',function(){
				$(':checkbox').attr('checked',$(this).attr('checked'));
			});
			
			//行全选多选框事件
			$(':checkbox[id^="chk_"]').live('change',function(){
				//选择右侧所有的checkbox
				$(':checkbox[class^="chk_"]',$(this).closest('tr')).attr('checked',$(this).attr('checked'));
				checkType();
				checkAll();
			});
			
			//选项多选框事件
			$(':checkbox[class^="chk_"]',$('.table-body')).live('change',function(){
				checkLine();
				checkType();
				checkAll();
			});
			
			//种类多选框事件
			$(':checkbox','#typeList').bind('change',function(){
				$('.'+$(this).attr('class'),$('.table-body')).attr('checked',$(this).attr('checked'));
				checkLine();
				checkAll();
			});
			//解决谷歌浏览器出现的bug
 			$(':checkbox[class^="chk_"]',$('.table-body')).each(function (){
				if($(this).attr("ck")!="ck"){
					$(this).removeAttr('checked');
				}
			}); 
			
			setElementHeight('.treePanel',['#toolbar'],$(document.body),30);
						//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),56);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid',0);	//计算.table-body的高度
		 	 //loadGrid();//  自动计算滚动条的js方法
			var $grid = $('.grid'), 
			$thead = $('.table-head',$grid),
			$tbody = $('.table-body',$grid),
			docWidth = $(document.body).width()-180,
			headWidth = $thead.find('table').outerWidth()-180,
			bodyWidth = $tbody.find('table').outerWidth()-180,
			headWidth = (headWidth > docWidth || headWidth + 17 > docWidth)
				? headWidth + 17 
				: docWidth,
			bodyWidth = (bodyWidth > docWidth || bodyWidth + 17 > docWidth)
				? bodyWidth + 17 
				: docWidth;

			$thead.width(headWidth);
			$tbody.width(bodyWidth);
			
			if(headWidth > docWidth){
				$grid.width(docWidth).css('overflow-x','auto');
				$tbody.height($tbody.height() - 17);
			}else{
				$grid.width(docWidth).css('overflow-x','hidden');		
			}
			checkLine();
			removeLine();
		});
		
		function toNowRoleOperateRange(){
			$('#roleOperateRangeForm').submit();
		}
		
		//按照类别检查操作列表多选框是否需要选中
		function checkType(){
			$(':checkbox','#typeList').attr('checked','checked');
			$(':checkbox','#typeList').each(function(){
				$('.'+$(this).attr('class'),$('.table-body')).each(function (){
					if(!$(this).is(':checked')){
						$('.'+$(this).attr('class'),'#typeList').removeAttr('checked');
						return false;
					}else{
						$('.'+$(this).attr('class'),'#typeList').attr('checked','checked');
					}
				});
			});
		}
		
		//按照行计算每行的多选框是否选中
		function checkLine(){
			$('tr[id^="col_"]',$('.table-body')).each(function (){
				$(this).find(':checkbox[class^="chk_"]').each(function (){
					if(!$(this).is(':checked')){
						$(this).closest('tr').find(':checkbox[id^="chk_"]').removeAttr('checked');
						return false;
					}else{
						$(this).closest('tr').find(':checkbox[id^="chk_"]').attr('checked','checked');
					}
				});
				
			});
		}
		
		//按照所有选项计算全选多选框是否选中
		function checkAll(){
			$(':checkbox[id^="chk_"]',$('.table-body')).each(function(){
				if(!$(this).is(':checked')){
					$('#chkAll').removeAttr('checked');
					return false;
				}
				else
					$('#chkAll').attr('checked','checked');
			});
		}
		
		function removeLine(){
			$(':checkbox[id^="chk_"]',$('.table-body')).closest('tr').each(function(){
				var temp=true;
				$(this).find(':checkbox[class^="chk_"]').each(function (){
					if($(this).is(':checked')){
						temp=false;
					}
				});
				if(temp){
					$(this).remove();
				}
			});
			reMakeNum();
			checkType();
			checkAll();
		}
		
		function reMakeNum(){
			$('td[class="num"]',$('.table-body')).each(function (i){
				$(this).text(i+1);
			});
		}
		
		//点击模块树通过AJAX方式在后台获取权限列表并加载到页面上
		function selectItem(modulecode,moduleId){
			jQuery.ajax({
				type : 'POST',
				contentType : 'application/json',
				url : '<%=path%>/roleOperate/add.do',
				data : '{"code":"'+modulecode+'","id":"'+moduleId+'"}',
				dataType : 'json',
				success : function(data) {
					
					for(var i=0;i<data.length;i++){
					if(!!!$('#col_'+data[i].id).html()){
						var colnum='<tr id="col_'+data[i].id+'"><td class="num" style="width: 25px;">'+colNum+'</td>';
						colNum =colNum+1;
						var colcheckbox='<td style="width:30px; text-align: center;"><input type="checkbox" id="chk_'+data[i].id+'" value="" checked="checked" /></td>';
						var colmoduleName='<td style="width:180px;">'+data[i].name+'&nbsp;</td>';
						var operateChk='';
						for(var j=0;j<data[i].childOperate.length;j++){
							if(j!=0 && j%8==0){
								operateChk +='<br />';
							}
							operateChk +='&nbsp;&nbsp;<input type="checkbox"  class="chk_'+data[i].childOperate[j].type+'" value="'+data[i].childOperate[j].id+'" checked="checked" />'+data[i].childOperate[j].name;
						}
						var colthree='<td style="width:450px;">'+operateChk+'&nbsp;</td></tr>';
						$('.table-body tbody').append(colnum+colcheckbox+colmoduleName+colthree);
						}
					}
					reMakeNum();
   				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					alert(XMLHttpRequest+"|"+textStatus+"|"+errorThrown);
				}
			});
		}
		
		function saveRoleOperateByAdd(){
			var checkboxList=$(':checkbox[class^="chk_"]',$('.table-body'));
			if(checkboxList 
					&& checkboxList.filter(':checked').size() > 0){
				if(confirm('<fmt:message key="do_you_want_to_submit_data"/>？')){
					var chkValue = [];
					
					checkboxList.filter(':checked').each(function(){
						chkValue.push($(this).val());
					});
					var selected = {};
					selected['roleId'] = '${role.id}';
					selected['roleOperateId'] = chkValue.join(",");
					$.post('<%=path%>/roleOperate/saveByAdd.do',selected,function(data){
					 	//弹出提示信息
					 	alert('<fmt:message key="operation_successful"/>！');
						$('#roleOperateForm').submit();
					});
	<%-- 				$('body').window({
						title: '<fmt:message key="tips"/>',
						content: '<iframe id="updateRoleOperateFrame" frameborder="0" src="<%=path%>/roleOperate/saveByAdd.do?roleOperateId='+chkValue.join(",")+'&roleId=${role.id}"></iframe>',
						width: 538,
						height: '185px',
						draggable: true,
						isModal: true
					}); --%>
				}
			}else{
				alert('<fmt:message key="please_select_permissions_need_to_be_saved"/>！');
				return ;
			}
		}
		
		function pageReload(){
			$('#roleOperateForm').submit();
		}
		</script>

	</body>
</html>