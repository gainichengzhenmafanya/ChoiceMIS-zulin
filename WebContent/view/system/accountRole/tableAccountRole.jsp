<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="associated_with_the_role_of_information"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<style type="text/css">
				.relate_0 {
					background:url(<%=path %>/image/unrelated.gif) no-repeat 22px center;
				}
				
				.relate_1 {
					background:url(<%=path %>/image/related.gif) no-repeat 22px center;
				}
				
			</style>
		</head>
	<body>
		<input type="hidden" id="accountId" name="accountId" value="${accountId}"/>
		<div id="toolbar"></div>
		<div class="grid">
			<div class="table-head">
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<td class="num"></td>
							<td class="chk">
								<input type="checkbox" id="chkAll"/>
							</td>
							<td style="width:180px;"><fmt:message key="role_name"/></td>
							<td style="width:120px;"><fmt:message key="associated_state"/></td>
						</tr>
					</thead>
				</table>
			</div>
			<div class="table-body">
				<table cellspacing="0" cellpadding="0">
					<tbody>
						<c:forEach var="role" items="${roleList}" varStatus="status">
							<tr>
								<td class="num">${status.index+1}</td>
								<td class="chk">
									<input type="checkbox" name="idList" id="chk_<c:out value='${role.id}' />" 
										value="<c:out value='${role.id}' />"/>
								</td>
								<td>
									<span style="width:170px;"><c:out value="${role.name}" />&nbsp;</span>
								</td>
								<td class="center" id="state_${role.id}">
									<span style="width:110px;" class="relate_${role.deleteFlag}">${ACCOUNT_ROLE_STATE[role.deleteFlag]}&nbsp;</span>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/system/accountRole.js"></script>

		<script type="text/javascript">
		$(document).ready(function(){
			
			$('#toolbar').toolbar({
				items: [{
						text: '<fmt:message key="related_roles" />',
						title: '<fmt:message key="related_roles"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-59px','-60px']
						},
						handler: function(){
							if(getAllID()){
								addAccountRole("<%=path%>",getAllID(),$('#accountId').val(),"<c:out value='${ACCOUNT_ROLE_STATE["1"]}' />");
							}else{
								alert('<fmt:message key="please_select_the_role_information_needs_associated"/>！');
								return ;
							}
						}
					},{
						text: '<fmt:message key="disassociate" />',
						title: '<fmt:message key="disassociate"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-79px','-60px']
						},
						handler: function(){
							if(getAllID()){
								deleteAccountRole("<%=path%>",getAllID(),$('#accountId').val(),"<c:out value='${ACCOUNT_ROLE_STATE["0"]}' />");
							}else{
								alert('<fmt:message key="please_choose_the_role_of_information_need_to_disassociate"/>！');
								return ;
							}
						}
					},{
						text: '取消',
						title: '取消',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							$('.close',parent.document).click();
						}
					}
				]
			});//end toolbar
			
			//设置表格的整体高度
			setElementHeight('.grid',['#toolbar']);
			
			//设置表格数据展示部分的高度
			setElementHeight('.table-body',['.table-head'],'.grid');
			
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
			/* $("#chkAll").click(function() {
		                if (!!$("#chkAll").attr("checked")) {
		                	$('.grid').find('.table-body').find('tr').addClass("bgBlue");
		                }else{
		                	$('.grid').find('.table-body').find('tr').removeClass("bgBlue");
		            	      }
		            }); */
		});	//end $(document).ready();

		</script>
	</body>
</html>