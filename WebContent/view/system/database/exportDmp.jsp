<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@page import="javax.swing.JFileChooser" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Basic Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<style type="text/css">
				.tool {
					position: relative;
					height: 27px;
				}
			</style>
		</head>
	<body>
		<div class="tool">
		</div>
	 	<div id="result"></div>
		<form action="<%=path%>/database/downloadTemplate.do" id="export" method="post" enctype="multipart/form-data">
			<div align="center" id="divExport">
			<br>
			</div> 
		</form>
		<br>							
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/browseFolder.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript">
			var tool;
			$(document).ready(function(){
				tool	= _tool();

				//$("#export").submit();
				
				});
			
			var _tool=function(){
				return $('.tool').toolbar({
					items: [
					 {
						text: '下载',
						title: '下载数据库',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							$("#export").attr('action','<%=path%>/database/downloadTemplate.do?').submit();
							 //handle();
						}
					}
				]
				});
				}
			function handle(){
					$.ajax({
						type:"POST",
						url:'<%=path%>/dataBase/downloadTemplate.do?',
						dataType : "json",
						success : function(listError) {
							$("#result").html("导入成功");
						window.setTimeout(function(){
							window.close();
						}, 1500);	
					}
				});
			}
			
		</script>
	</body>
</html>