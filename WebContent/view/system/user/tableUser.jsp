<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="personnel_information"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			
			<style type="text/css">
				#tool {
					position: relative;
					height: 27px;
				}
				.show-firm-row{
					color:red;
				}
			</style>
		</head>
	<body>
		<div id="tool"></div>
		<div class="leftFrame" style="width: 670px;">
		<div class="grid">
			<form id="listForm" action="<%=path%>/user/list.do" method="post">
				<div class="table-head">
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"></td>
								<td class="chk">
									<input type="checkbox" id="chkAll"/>
								</td>
								<td style="width:120px;">组编号</td>
								<td style="width:150px;">组名称</td>
<%-- 								<td style="width:100px;"><fmt:message key="birthday"/></td> --%>
<%-- 								<td style="width:50px;"><fmt:message key="sex"/></td> --%>
								<td style="width:120px;"><fmt:message key="their_departments"/></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="user" items="${userList}" varStatus="status">
								<tr>
									<td class="num">${status.index+1}</td>
									<td class="chk">
										<input type="checkbox" name="idList" id="chk_<c:out value='${user.id}' />" value="<c:out value='${user.id}' />"/>
									</td>
									<td>
										<span style="width:110px;"><c:out value="${user.code}" />&nbsp;</span>
									</td>
									<td>
										<span style="width:140px;"><c:out value="${user.name}" />&nbsp;</span>
									</td>
<!-- 									<td class="center"> -->
<%-- 										<span style="width:90px;"><fmt:formatDate value="${user.birthday}" type="date" />&nbsp;</span> --%>
<!-- 									</td> -->
<!-- 									<td class="center"> -->
<%-- 										<span style="width:40px;"><c:out value='${SEX_RADIO[user.sex]}' />&nbsp;</span> --%>
<!-- 									</td> -->
									<td>
										<span style="width:110px;"><c:out value='${user.department.name}' />&nbsp;</span>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</form>
		</div>
		</div>
		<div class="mainFrame" style="width:215px;">
		    <iframe src="" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>

		<script type="text/javascript">
		$(document).ready(function(){
			
			var tool = $('#tool').toolbar({
				items: [{
					text: '<fmt:message key="insert" />',
					title: '<fmt:message key="new_personnel_information"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','0px']
					},
					handler: function(){
						if($('#departmentId',$(parent.document)).val()
								&& $('#departmentName',$(parent.document)).val()){
							
							parent.addUser('<%=path%>');
						}else{
							alert('<fmt:message key="please_select_departments"/>！');
						}
					}
				},{
					text: '<fmt:message key="update" />',
					title: '<fmt:message key="modify_personnel_information"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-18px','0px']
					},
					handler: function(){
						if(getFirstID()){
							parent.updateUser('<%=path%>',getFirstID());
						}else{
							alert('<fmt:message key="please_select_information_you_need_to_modify"/>！');
							return ;
						}
					}
				},{
					text: '<fmt:message key="delete" />',
					title: '<fmt:message key="delete_personnel_information"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-38px','0px']
					},
					handler: function(){
						if(getAllID()){
							if(!confirm('<fmt:message key="delete_data_confirm"/>？'))
								 return;
							parent.deleteUser('<%=path%>',getAllID());
						}else{
							alert('<fmt:message key="please_select_information_you_need_to_delete"/>！');
							return ;
						}
					}
				},{
					text: '<fmt:message key="account_information" />',
					title: '<fmt:message key="account_information"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-158px','-40px']
					},
					handler: function(){
						if(getFirstID()){
							parent.showAccount('<%=path%>',getFirstID());
						}else{
							alert('<fmt:message key="please_select_the_staff_need_to_create_a_single_account"/>！');
							return ;
						}
					}
				},{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit" />',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
					}
				}]
			
			});	//end $('#tool').toolbar()

			//设置表格的总体高度
			setElementHeight('.grid',['#tool']);
			
			//设置表格数据展示部分的高度
			setElementHeight('.table-body',['.table-head'],'.grid',28);
			loadGrid();//  自动计算滚动条的js方法
			
			//显示效果 wjf
			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
			//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行 wjf
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("show-firm-row")) {
			         $(this).removeClass("show-firm-row").find(":checkbox").attr("checked", false);
			     }else{
// 			    	 $(":checkbox").attr("checked", false);
//			    	 $('.grid').find('.table-body').find('tr').removeClass("show-firm-row");
			         $(this).addClass("show-firm-row").find(":checkbox").attr("checked", true);
// 			         var id=$(this).find(':checkbox').val();
<%-- 						window.mainFrame.location = "<%=path%>/user/listFirm.do?id="+id; --%>
			     }
			 });
			
		});	//end $(document).ready();

		</script>
	</body>
</html>