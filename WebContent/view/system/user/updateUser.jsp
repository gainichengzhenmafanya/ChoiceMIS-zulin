<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="personnel_information"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
	</head>
	<body>
		<div class="form">
			<form id="userForm" method="post" action="<%=path %>/user/saveByUpdate.do">
				<input type="hidden" id="id" name="id" value="<c:out value='${user.id}' />" />
				<input type="hidden" id="state" name="state" value="<c:out value='${user.state}' />" />
				<input type="hidden" id="deleteFlag" name="deleteFlag" value="<c:out value='${user.deleteFlag}' />" />
				<div class="form-line">
					<div class="form-label"><fmt:message key="number"/></div>
					<div class="form-input">
						<input type="text" id="code" name="code" class="text" 
							value="<c:out value='${user.code}' />"/>
					</div>
					<div class="form-label"><fmt:message key="name"/></div>
					<div class="form-input">
						<input type="text" id="name" name="name" class="text" 
							value="<c:out value='${user.name}' />" />
					</div>
				</div>
				<div class="form-line">
<%-- 					<div class="form-label"><fmt:message key="sex"/></div> --%>
<!-- 					<div class="form-input"> -->
						<input name="sex" value="0" type="hidden"/>
<%-- 						<fmt:message key="male"/> --%>
<%-- 						<input type="radio" id="sex_1" name="sex" value="1"/><fmt:message key="female"/> --%>
<!-- 					</div> -->
					<div class="form-label"><fmt:message key="higher_authorities"/></div>
					<div class="form-input">
						<input type="text" id="departmentName" name="department.name" class="selectDepartment text" 
							value="<c:out value='${user.department.name}' default='部门' />" />
						<input type="hidden" id="departmentId" name="department.id" class="text" 
							value="<c:out value='${user.department.id}' default='00000000000000000000000000000000' />" />
					</div>
				</div>
				<div class="form-line">
<%-- 					<div class="form-label"><fmt:message key="birthday"/></div> --%>
<!-- 					<div class="form-input"> -->
						<input type="hidden" id="birthday" name="birthday" class="Wdate text" type="hidden"
							value="<fmt:formatDate value='${user.birthday}' pattern="yyyy-MM-dd" type='date' />" />
					</div>
					<div class="form-label"></div>
					<div class="form-input"></div>
				</div>
			</form>
		</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
    	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
    	<script type="text/javascript" src="<%=path%>/js/validate.js"></script>		
		<script type="text/javascript">
		var validate;
			$(document).ready(function(){
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'code',
						validateType:['canNull','maxLength'],
						param:['F','100'],
						error:['<fmt:message key="number"/><fmt:message key="cannot_be_empty"/>！','<fmt:message key="number_input_extended"/>']
					},{
						type:'text',
						validateObj:'name',
						validateType:['canNull','maxLength'],
						param:['F','50'],
						error:['<fmt:message key="name"/><fmt:message key="cannot_be_empty"/>！','<fmt:message key="name_enter_the_ultra_long"/>']
					},{
						type:'text',
						validateObj:'birthday',
						validateType:['canNull','date'],
						param:['F'],
						error:['<fmt:message key="birthday"/><fmt:message key="cannot_be_empty"/>！','<fmt:message key="birthday"/><fmt:message key="incorrect_format_input"/>']
					}]
				});
				
				$('#departmentName').bind('focus.selectDepartment',function(e){
					if(!!!top.departmentWindow){
						var offset = getOffset('departmentName');
						top.selectDepartment('<%=path%>',offset,$(this),$('#departmentId'));
					}
				});
				
				$(':input[name="sex"]').each(function(){
					if($(this).val() === "${user.sex}")
						$(this).attr("checked","checked");
				});
				
				$('#birthday').bind('click',function(){
					new WdatePicker();
				});
				
			});
		</script>
	</body>
</html>