<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>操作类型</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
				.grid td span{
					padding:0px;
				}
				.form-input{
					width: auto;
					margin-right: 0px;
					padding-left: 0px;
				}
			</style>
		</head>
	<body>
		<div id='toolbar'></div>
		<form id="queryForm" name="queryForm" method="post">
		</form>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 25px;"><span>&nbsp;</span></td>
								<td>
									<span style="width:30px;"><input type="checkbox" id="chkAll"/></span>
								</td>
								<td><span style="width:180px;">事件名称</span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="event" items="${eventsList}" varStatus="status">
								<tr>
									<td class="num"><span style="width: 25px;">${status.index+1}</span></td>
									<td>
										<span style="width:30px; text-align: center;"><input type="checkbox"  name="idList" id="chk_${event['ID']}" value="${event['NAME']}"/></span>
									</td>
									<td><span style="width:180px;"><c:out value="${event['NAME']}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/tele/teleUtil.js"></script>
		<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
		<script type="text/javascript" src="<%=path%>/js/tele/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		
		<script type="text/javascript">
		var selected;
		$(document).ready(function(){
			selected = '${domId}' == 'selected' ? parent.selected : (typeof(parent['${domId}']) == 'function' ? parent['${domId}']() : $('#${domId}',parent.document).val().split(","));
				$("#toolbar").toolbar({
					items: [{
						text: '确定',
						useable:true,
						handler: function(){
							var checkboxList = $('.grid').find('.table-body').find(':checkbox');
							var data = {show:[],code:[],entity:[]};
							checkboxList.filter(':checked').each(function(){
								if($(this).attr('disabled'))return;
								var entity = {};
								var row = $(this).closest('tr');
								data.code.push($(this).val());
								data.show.push($.trim(row.children('td:eq(2)').text()));
								entity.name = $.trim(row.children('td:eq(2)').text());
								data.entity.push(entity);
							});
							if("${domId}" == "selected"){
								parent.selected = data.code;
							}
							parent['${callBack}'](data);
							$(".close",parent.document).click();
						}
					},{
						text: '取消',
						useable: true,
						handler: function(){
							$(".close",parent.document).click();
						}
					}
				]
			});
			if(selected){
				$(".table-body").find('tr td input').each(function(){
					if($.inArray($(this).val(),selected) >= 0)$(this).attr('checked','checked');
				});
			}
			setElementHeight('.grid',['.tool'],$(document.body),25);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			$('.grid').find('.table-body').find('tr').live('mouseover',function(){
				$(this).addClass('tr-over');
			});
			$('.grid').find('.table-body').find('tr').live('mouseout',function(){
				$(this).removeClass('tr-over');
			});
			//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			var mod = '<c:out value="${single}"/>';
			if(mod){
				$('#chkAll').unbind('click');
				$('#chkAll').css('display','none');
			}
			$('.grid').find('.table-body').find('tr').live("click.diy", function () {
				if(mod){
					$(this).closest('.table-body').find(':checkbox').not($(this)).removeAttr("checked");
				}
				$(this).find(':checkbox').trigger('click');
			 });
			$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
				var mod = '<c:out value="${single}"/>';
				if(mod){
					$(this).closest('.table-body').find(':checkbox').not($(this)).removeAttr("checked");
					$(this).attr('checked','checked');
				}
				event.stopPropagation();
			});
			if(typeof(parent.editTable) == 'function')
				parent.editTable($('.grid'));
		});
		</script>
	</body>
</html>