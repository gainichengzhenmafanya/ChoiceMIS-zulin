<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="lo" uri="/WEB-INF/tld/local.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
		<title>operate Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			
			<style type="text/css">
				#tool{
					position: relative;
					height: 27px;
				}
				
				.condition {
					position: relative;
					top: 1px;
					line-height: 81px;
				}
				.operateInfo {
					position: relative;
					top: 1px;
					height: 61px;
					line-height: 61px;
					background-color: #E1E1E1;
				}
				.form-line .form-label{
					width: 90px;
				}
				
				.form-line .form-input{
					width: 160px;
				}
			</style>
		</head>
	<body>
		<!-- <div class="form-group">操作信息</div> -->
		<div id="tool"></div>
		<div class="condition">
		</div>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span class="num" style="width: 25px;">&nbsp;</span></td>
								<td><span style="width:30px;">
									<input type="checkbox" id="chkAll"/></span>
								</td>
								<td><span style="width:120px;"><fmt:message key="operation_name"/></span></td>
								<td><span style="width:180px;"><fmt:message key="belongs_module"/></span></td>
								<td><span style="width:120px;"><fmt:message key="whether_to_enable"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="operate" items="${operateList}" varStatus="status">
								<tr>
									<td><span class="num" style="width: 25px;">${status.index+1}</span></td>
									<td><span style="width:30px; text-align: center;">
										<input type="checkbox" name="idList" id="chk_<c:out value='${operate.id}' />" value="<c:out value='${operate.id}' />"/></span>
									</td>
									<td><span style="width:120px;">
										<c:out value="${operate.name}" />&nbsp;</span>
									</td>
									<td><span style="width:180px;">
										<c:out value="${lo:show(operate.module.name)}" />&nbsp;</span>
									</td>
									<td><span style="width:120px;">
										<c:if test="${operate.useable=='T'}">
											<fmt:message key="enable"/>
										</c:if>
										<c:if test="${operate.useable=='F'}">
											<fmt:message key="disable1"/>
										</c:if>&nbsp;</span>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){	
				//加载toolbar（子模块操作）
				var tool = $('#tool').toolbar({
					items: [{
							text: '<fmt:message key="insert" />',
							title: '<fmt:message key="new_module_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								saveoperate('${moduleId}','${addable}');
							}
						},{
							text: '<fmt:message key="update" />',
							title: '<fmt:message key="modify_the_module_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								updateoperate();
							}
						},{
							text: '<fmt:message key="delete" />',
							title: '<fmt:message key="remove_the_module_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								deleteoperate();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}

					]
				});

				//grid隔行变色
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),57);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				
				//打开新增模块信息页面
				function saveoperate(moduleId,addable){
					if(addable=='T' && moduleId !=''){
						window.parent.saveoperate('${moduleId}');
					}else{
						alert('<fmt:message key="only_increase_to_the_lowest_level_module"/>!!');
					}
				}
				//打开修改模块信息页面
				function updateoperate(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					
					if(checkboxList 
							&& checkboxList.filter(':checked').size() == 1){
						var chkValue = checkboxList.filter(':checked').eq(0).val();
						
						window.parent.updateoperate(chkValue);
					}else{
						alert('<fmt:message key="please_select_information_you_need_to_modify"/>！');
						return ;
					}
					
				}
				
				//删除模块信息
				function deleteoperate(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					
					if(checkboxList 
							&& checkboxList.filter(':checked').size() > 0){
						if(confirm('<fmt:message key="delete_data_confirm"/>？')){
							var chkValue = [];
							
							checkboxList.filter(':checked').each(function(){
								chkValue.push($(this).val());
							});
							
							var action = '<%=path%>/operate/delete.do';
							window.parent.deleteoperate(action,chkValue.join(","));
						}
					}else{
						alert('<fmt:message key="please_select_information_you_need_to_delete"/>！');
						return ;
					}
				}
			});
		</script>
</body>
</html>