<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="lo" uri="/WEB-INF/tld/local.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Module Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		
	</head>
	<body>
		<div class="form">
			<form id="operateForm" method="post" action="<%=path %>/operate/saveByAdd.do">
				<div class="form-line">
					<div class="form-label"><fmt:message key="name"/></div>
					<div class="form-input"><input type="text" id="name" name="name" class="text" value="查询"/></div>
					<div class="form-label"><fmt:message key="whether_to_enable"/></div>
					<div class="form-input">
						<select id="useable" name="useable" style="height: 22px;margin-top: 3px; border: 1px solid #999999;">
						<option value="T" ><fmt:message key="enable"/></option>
						<option value="F" ><fmt:message key="disable1"/></option>
						</select>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="belongs_module"/></div>
					<div class="form-input">
						<c:out value="${lo:show(module.name)}" />
						<input type="hidden" id="module.id" name="module.id" value="<c:out value="${module.id}" />" />
					</div>
					<div class="form-label"><fmt:message key="kinds_of_operations"/></div>
					<div class="form-input">
						<select id="type" name="type" style="height: 22px;margin-top: 3px; border: 1px solid #999999;">
							<c:forEach var="type" items="${operateType}">
								<option value="${type.id}" >${type.name}</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</form>
		</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>	
		<script type="text/javascript">
		var validate;
			$(document).ready(function(){
				$('#type').bind('change', function() {
					$('#name').val($(this).find("option:selected").text());
				});

				$('#type').bind
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'name',
						validateType:['canNull','maxLength'],
						param:['F','50'],
						error:['<fmt:message key="name"/><fmt:message key="cannot_be_empty"/>！','<fmt:message key="number_input_extended"/>']
					}]
				});

			});
			
		</script>
	</body>
</html>