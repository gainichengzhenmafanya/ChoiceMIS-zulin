<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="sector_information"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
	</head>
	<body>
		<div class="form">
			<form id="departmentForm" method="post" action="<%=path %>/department/saveByAdd.do">
			<div  style="height:500px; width:500px;left:50%;top:50%;margin:0px auto;margin-left:160px;margin-top:90px;">
				<div class="form-line">
					<div class="form-label"><fmt:message key="number"/></div>
					<div class="form-input">
						<input type="text" id="viewCode" name="viewCode" class="text"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="name"/></div>
					<div class="form-input">
						<input type="text" id="name" name="name" class="text"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="no"/></div>
					<div class="form-input">
						<input type="text" id="sequence" name="sequence" class="text"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="higher_authorities"/></div>
					<div class="form-input">
						<input type="text" id="parentDepartmentName" name="parentDepartment.name" class="selectDepartment text" 
							value="<c:out value='${department.parentDepartment.name}' default='部门' />" />
						<input type="hidden" id="parentDepartmentId" name="parentDepartment.id" class="text" 
							value="<c:out value='${department.parentDepartment.id}' default='00000000000000000000000000000000' />" />
					</div>
				</div>
			</div>
			</form>
		</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>	
		<script type="text/javascript">
		var validate;
			$(document).ready(function(){
				$('#viewCode').bind('focusout',function(){
					$.ajax({
						type: "POST",
						url: '<%=path%>/department/viewCode.do',
						data: "viewcode="+$('#viewCode').val()+"&parentDepartmentId="+$('#parentDepartmentId').val(),
						dataType: 'text',
						success:function(date){
							if(date=='T'){
								alert('<fmt:message key="this_number_already_exists"/>！');
								$('#viewCode').focus();
							}
						},
						error: function(){
							alert('<fmt:message key="server_busy_please_try_again"/>');
						}
					});
				});

				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'viewCode',
						validateType:['canNull','maxLength','num1'],
						param:['F','100','F'],
						error:['<fmt:message key="number"/><fmt:message key="cannot_be_empty"/>！','<fmt:message key="number_input_extended"/>','<fmt:message key="serial_input_format_is_incorrect"/>']
					},{
						type:'text',
						validateObj:'name',
						validateType:['canNull','maxLength'],
						param:['F'],
						error:['<fmt:message key="name"/><fmt:message key="cannot_be_empty"/>！','<fmt:message key="name_enter_the_ultra_long"/>']
					},{
						type:'text',
						validateObj:'sequence',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="no"/><fmt:message key="cannot_be_empty"/>！']
					},{
						type:'text',
						validateObj:'sequence',
						validateType:['num1'],
						param:['F'],
						error:['<fmt:message key="serial_input_format_is_incorrect"/>']
					}]
				});
				

				var $parentDepartmentId = $('#departmentId',$(parent.document)),
					$parentDepartmentName = $('#departmentName',$(parent.document));
				
				if($parentDepartmentId && $parentDepartmentId.val())
					$('#parentDepartmentId').val($parentDepartmentId.val());
				
				if($parentDepartmentName && $parentDepartmentName.val())
					$('#parentDepartmentName').val($parentDepartmentName.val());
				
				$('#parentDepartmentName').bind('focus.selectDepartment',function(e){
					if(!!!top.departmentWindow){
						var offset = getOffset('parentDepartmentName');
						top.selectDepartment('<%=path%>',offset,$(this),$('#parentDepartmentId'),$('#parentDepartmentId').val());
					}
				});
			});
		</script>
	</body>
</html>