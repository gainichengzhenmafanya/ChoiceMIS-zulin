<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<title>Insert title here</title>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<style type="text/css">
				.tr-select{
					background-color: #D2E9FF;
				}
				.separator ,div , .pgSearchInfo{
					display: none;
				}
				div[class]{
					display:block;
				}
				.tr-select{
					background-color: #D2E9FF;
				}
				.grid td span{
					padding:0px;
				}
				.page{
					margin-bottom: 25px;
				}
			</style>
	</head>
	<body>
			<form id="copyToAccountFrame" action="<%=path%>/account/copyToAccount.do" method="post">
			<div class="grid" style="overflow: auto;">
				<div class="table-head">
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num">&nbsp;</td>
								<td class="chk">
									<%-- <input type="checkbox" id="chkAll_${user.id}" class="${user.id}"/> --%>
								</td>
								<td style="width:150px;"><fmt:message key="account_name"/></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body" style="height: 100%;overflow: auto;">
					<table cellspacing="0" cellpadding="0" id="tblGrid">
						<tbody>
							<c:forEach var="account" items="${accountList}" varStatus="status">
											<tr>
											<%-- 	<td class="num">${status.index+1}</td>
												<td class="chk">
													<input type="checkbox" class="${user.id}"  id="chk_<c:out value='${account.id}' />" value="<c:out value='${account.id}' />"/>
												</td>
												<td>
													<span style="width:140px;"><c:out value="${account.name}" />&nbsp;</span>
	</td>
												<td>
													<span style="width:110px;"><c:out value="${account.names}" />&nbsp;</span>
												</td>
												<td><span class="num" style="width: 25px;">${status.index+1}</span></td> --%>
												<td class="num">${status.index+1}</td>
												<td class="chk">
													<input type="checkbox" name="idList" name="code" id="chk_${account.id}" value="${account.id}"/>
												</td>
												<td><span title="${account.name}" style="width:150px;">${account.name}&nbsp;</span></td>
											</tr>
										</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>  
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		function accountName(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			var id = [];
			if(checkboxList 
					&& checkboxList.filter(':checked').size() > 0){
				var aim = checkboxList.filter(':checked'); 
				/* var level = $(aim.eq(0)).next().val() ; */
				aim.each(function(){
					id.push($(this).val());
				});
			}
			return id;
		}
			setElementHeight('.grid',['.tool'],$(document.body),0);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
		</script>
</body>
</html>