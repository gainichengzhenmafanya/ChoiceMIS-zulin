<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>账号list</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<style type="text/css">
			.separator{
				display:none !important;
			}
			.page{
				margin-bottom: 28px;
			}
			.separator ,div , .pgSearchInfo{
				display: none;
			}
			div[class]{
				display:block;
			}
			.tr-select{
				background-color: #D2E9FF;
			}
		</style>
	</head>
	<body>
		<form id="listForm" action="<%=path%>/accountPositn/listPositnAccount.do" method="post">
				<div class="form-line">
					<div class="form-label" style="width: 45px;"><fmt:message key="query_information" /></div>
					<div class="form-input" style="width: 80px;">
						<input type="text" id="key" value="${key}" style="width: 80px;"/>
					</div>
					<div class="tool"></div>
				</div>
				<div class="grid">
				<a href="javascript:refresh()"><img src="<%=path%>/image/tree/department/root.gif" style="margin-top:-2px" alt="<fmt:message key="branches_positions" />" /><span style="vertical-align:middle;font-size: 14px;"><fmt:message key="branches_positions" /></span></a>
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 20px;"></span></td>
									<td><span style="width:30px;"><fmt:message key="coding" /></span></td>
									<td><span style="width:150px;"><fmt:message key="name" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0" class="datagrid">
							<tbody>
								<c:forEach var="positn" items="${listPositn}" varStatus="status">
									<tr>
										<td class="num"><span style="width: 20px;">${status.index+1}</span></td>
										<td><span style="width:30px;">${positn.code}</span></td>
										<td><span style="width:150px;">${positn.des}</span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>			
				</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$(".pgPanel").children("div:first").css('display','block');
				$(".changePageSize , .pgSearchInfo").css("display","none");
				var offset = $(".tool").prev("div").offset();
				offset.left = $(".tool").prev("div").width() + offset.left + 10;
				$(".tool").button({
					text:'<fmt:message key="select" />',
					container:$(".tool"),
					position: {
						type: 'absolute',
						top: offset.top,	
						left: offset.left
					},
					handler:function(){
						if(isNaN($("#key").val())){
							$("#key").attr("name","init");
							$("#key").val($("#key").val().toUpperCase());
						}else{
							$("#key").attr("name","code");
						}
						$("#listForm").submit();
					}
				});
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),80);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').click(function(){
					$(this).addClass('tr-select');
					$('.grid').find('.table-body').find('tr').not(this).removeClass('tr-select');
				});
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
			});		
			function refresh(){
				parent.$("#mainFrame").attr("src","<%=path%>/accountPositn/tableFromPositn.do");
			}
		</script>
	</body>
</html>