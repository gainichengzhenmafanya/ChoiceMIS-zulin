<%@ page import="com.choice.orientationSys.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
String rootId = StringConstant.ROOT_ID;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>账号--仓位</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<style type="text/css">
				.leftFrame{
				width:22%;
				}
				.mainFrame{
				width:78%;
				}
			</style>
		</head>
	<body>
		<input type="hidden" name="positnDes" id="positnDes"/>
		<div class="leftFrame"> 
		    <iframe src="<%=path%>/accountPositn/listPositnAccount.do" id="PostinFrame"></iframe>
    	</div>
	    <div class="mainFrame" id="accountFrame">
	 	  <iframe src="<%=path%>/accountPositn/tableFromPositn.do" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
	    </div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/system/account.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript">
		var curWindow;
		$("#PostinFrame").load(function(){
			$(this).contents().find(".table-body tr").click(function(){
				var positn = $(this).find("td:eq(1)").text();
				var positnDes = $(this).find("td:eq(2)").text();
				$('#positnDes').val(positnDes);
				$("#mainFrame").contents().find("#positn").val(positn);
				$("#mainFrame").attr("src","<%=path%>/accountPositn/tableFromPositn.do?positn.code="+positn);
			});
		});
		
		function pageReload(){
			if(curWindow)
				curWindow.close();
			$("#mainFrame").attr("src","<%=path%>/accountPostin/tableFromPositn.do?positn="+$("#mainFrame").contents().find("#positn").val());
		}
		</script>
	</body>
</html>