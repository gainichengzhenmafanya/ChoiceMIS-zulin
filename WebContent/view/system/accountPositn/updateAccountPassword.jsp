<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>修改账号密码</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>		
		<style type="text/css">
/* 				.accountInfo { */
/* 					position: relative; */
/* 					top: 1px; */
/* 				} */
/* 				.hr{ */
/* 					border-bottom: 2px solid #677FB2; */
/* 				} */
/* 				.form-line .form-input{ */
/* 	 				width: 20%;  */
/* 	 			} */
/* 	 			.form-line .form-label{ */
/* 	 				width: 20%;  */
/* 	 			} */
			</style>
	</head>
	<body>
		<div id="toolbar"></div>
	<!-- 	<div class="form-group">账号信息</div> -->
		<div class="accountInfo" style="width:350px;margin:10px auto;">
			<form id="accountForm" name="accountForm" method="post" action="<%=path %>/account/saveByAdd.do">
				<input type="hidden" id="id" name="id" value="${accountPositn.id}" />
				<div class="form-line">
					<div class="form-label"><fmt:message key="name"/></div>
					<div class="form-input">
						<input type="text" id="name" name="name" class="text" readonly="readonly"  value="${accountPositn.name}"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="original_password"/></div>
					<div class="form-input">
						<input type="password" id="oldPassword" name="oldPassword" class="text" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="new_password"/></div>
					<div class="form-input">
						<input type="password" id="password" name="password" class="text" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="confirm_password"/></div>
					<div class="form-input">
						<input type="password" id="confimPassword" name="confimPassword" class="text" />
					</div>
				</div>
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				var toolbar = $('#toolbar').toolbar({
					items: [{
							text: '<fmt:message key="save" />',
							title: '<fmt:message key="save_account_information"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								validateOldPassword();
							}
						},{
							text: '取消',
							title: '取消',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.close',parent.document).click();
							}
						}
					]
				});//end toolbar
				
				setElementHeight('.accountInfo',['#toolbar','.form-group'],$(document.body),10);
				
			});//end $(document).ready()
			
			function validateOldPassword(){
				var id = $('#id').val(),
					name = $('#name').val(),
					password = $('#oldPassword').val();
					
					confimPassword0=$('#password').val();
					confimPassword1=$('#confimPassword').val();
					if(confimPassword0!=confimPassword1){
						alert('<fmt:message key="enter_two_passwords_dont_match"/>');
						return;
					}
				$.ajax({
					type: 'POST',
					url: '<%=path %>/account/ajaxValidate.do',
					contentType: 'application/json',
					data: '{"id":"'+id+'","name":"'+name+'","password":"'+password+'"}',
					dataType: 'html',
					success: function(info){
						
						//如果原密码输入正确，则保存新密码；否则，需要重新输入原密码
						if(info && info === 'T'){
							saveAccount();
						}else{
							//弹出提示信息
							showMessage({
								type: 'error',
								msg: '<fmt:message key="original_password_input_errors_please_re_enter"/>！',
								speed: 1000
							});	
							
							$('#oldPassword').val('').focus();
						}
					}
				});
				
			}
			
			function saveAccount(){
				var id = $('#id').val(),
					name = $('#name').val(),
					password = $('#password').val();
				
				$.ajax({
					type: 'POST',
					url: '<%=path %>/account/ajaxSaveByUpdate.do',
					contentType: 'application/json',
					data: '{"id":"'+id+'","name":"'+name+'","password":"'+password+'"}',
					dataType: 'html',
					success: function(info){
						if(info && info === 'T'){
							
							//弹出提示信息
							showMessage({
								handler: function(){
									var accountTab = parent.window.accountTab,
										updateAccountWin = parent.window.updateAccountWin;
									if(accountTab){
							    	var $tableFrame = accountTab.getItem('tab_tableAccount').div.find('#tableAccountFrame');
							    	$tableFrame.attr('src',$tableFrame.attr('src'));
							    	
							    	accountTab.close('tab_updateAccount');
							    	accountTab.show('tab_tableAccount');
									}else if(updateAccountWin){
										updateAccountWin.close();
									}
								}
							});					
						}else{
							//弹出提示信息
							showMessage({
								type: 'error',
								msg: info,
								speed: 1000
							});	
						}
					}
				});
			}
		</script>
	</body>
</html>