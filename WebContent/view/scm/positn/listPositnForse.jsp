<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>positn Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
				.tool {
					position: relative;
					height: 27px;
				}
				.page{
					margin-bottom: 0px;
				}
				.condition {
					position: relative;
					top: 1px;
					height: 31px;
					line-height: 31px;
				}
				.grid td span{
					padding:0px;
				}
				.table-head td span{
					white-space: normal;
				}
				.search-div .form-line .form-label{
					width: 10%;
				}
			</style>
		</head>
	<body>
		<div class="tool">
		</div>
		<form action="<%=path%>/positn/listDetailForse.do?descode=${descode}" id="queryForm" name="queryForm" method="post">
<!-- 			<div class="search-div" style="position: absolute;z-index: 99"> -->
<!-- 				<div class="form-line"> -->
<!-- 					<div class="form-label">缩写</div> -->
<!-- 					<div class="form-input"> -->
<%-- 						<input type="text" id="init" name="init" style="text-transform:uppercase;" value="${queryPositn.init}" onkeyup="ajaxSearch()" class="text"/> --%>
<!-- 					</div> -->
<!-- 					<div class="form-label">类别</div> -->
<!-- 					<div class="form-input"> -->
<!-- 						<select name="typ" class="select" onkeyup="ajaxSearch()"> -->
<!-- 							<option value=""/>全部</option> -->
<%-- 							<c:forEach items="${listPositnTyp }" var="positnTyp"> --%>
<%-- 								<option value="${positnTyp.typ }"/>${positnTyp.typ }</option> --%>
<%-- 							</c:forEach> --%>
<!-- 						</select> -->
<!-- 					</div> -->
<%-- 					<div class="form-label"><fmt:message key="business_group" /></div> --%>
<!-- 					<div class="form-input"> -->
<!-- 						<select name="mod2" class="select" onkeyup="ajaxSearch()"> -->
<%-- 							<option value=""><fmt:message key="please_select" /><fmt:message key="business_group" /></option> --%>
<%-- 							<c:forEach items="${listMod }" var="curMod"> --%>
<%-- 								<option value="<c:out value="${curMod.des }"/>"  --%>
<%-- 									<c:if test="${curMod.des == queryPositn.mod2 }"> --%>
<!-- 										selected="selected" -->
<%-- 									</c:if> --%>
<%-- 									><c:out value="${curMod.des }"/></option> --%>
<%-- 							</c:forEach> --%>
<!-- 						</select> -->
<!-- 					</div> -->
<!-- 					<div class="form-label"> -->
<%-- 						<fmt:message key="area" /></div> --%>
<!-- 						<div class="form-input"> -->
<!-- 							<select name="area" class="select" onkeyup="ajaxSearch()"> -->
<%-- 								<option value=""><fmt:message key="please_select" /><fmt:message key="area" /></option> --%>
<%-- 								<c:forEach items="${listArea }" var="curArea"> --%>
<%-- 										<option value="<c:out value="${curArea.des }"/>"  --%>
<%-- 										<c:if test="${curArea.des == queryPositn.area }"> --%>
<!-- 											selected="selected" -->
<%-- 										</c:if> --%>
<%-- 										><c:out value="${curArea.des }"/></option> --%>
<%-- 								</c:forEach> --%>
<!-- 							</select> -->
<!-- 						</div> -->
					
<!-- 				</div> -->
<!-- 				<div class="search-commit"> -->
<%-- 		       		<input type="button" class="search-button" id="search" value='<fmt:message key="select" />'/> --%>
<%-- 		       		<input type="button" class="search-button" id="resetSearch" value="<fmt:message key="empty" />"/> --%>
<!-- 				</div> -->
<!-- 			</div> -->
			
			<table  cellspacing="0" cellpadding="0" style="background-color:#EEE;" >
				<tr>
					<td>&nbsp;
			        	<font style="font-size:2.2ex;"><b><fmt:message key="coding"/>:</b></font>
			            <input type="text" id="code" name="code" style="width: 100px;" class="text" value="<c:out value="${positn.code}" />" onkeydown="javascript: if(event.keyCode==13){$('#queryForm').submit();} "/>
			        </td>
			        <td>&nbsp;
			        	<font style="font-size:2.2ex;"><b><fmt:message key="name"/>:</b></font>
			            <input type="text" id="des" name="des" style="width: 100px;" class="text" value="<c:out value="${positn.des}" />" onkeydown="javascript: if(event.keyCode==13){$('#queryForm').submit();} "/>
			        </td>
			        <td>&nbsp;
			        	<input type="button" id="search" style="width: 60px;" name="search" value='<fmt:message key="select"/>'/>
			        </td>
				</tr>
			</table>
			
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span class="num" style="width: 25px;padding-left:8px">&nbsp;</span></td>
								<td><span style="width:30px;">
									<!-- <input type="checkbox" id="chkAll"/></span> -->
								</td>
								<td><span style="width:50px;"><fmt:message key="coding" /></span></td>
								<td><span style="width:180px;"><fmt:message key="name" /></span></td>
								<td><span style="width:80px;"><fmt:message key="type" /></span></td>
								<td><span style="width:80px;"><fmt:message key="abbreviation" /></span></td>
								<td><span style="width:100px;"><fmt:message key="for_short" /></span></td>
<%-- 								<td><span style="width:30px;"><fmt:message key="used" /></span></td> --%>
								<td><span style="width:80px;"><fmt:message key="area" /></span></td>
<%-- 								<td><span style="width:80px;"><fmt:message key="business_group" /></span></td> --%>
								<td><span style="width:80px;"><fmt:message key="distribution_area" /></span></td>
<!-- 								<td><span style="width:180px;">IP</span></td> -->
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="positn" items="${listPositn}" varStatus="status">
								<tr>
									<td><span class="num" style="width: 25px;padding-left:8px;">${status.index+1}</span></td>
									<td><span style="width:30px; text-align: center;">
										<input type="checkbox"  name="idList" id="chk_<c:out value='${positn.code}' />" value="<c:out value='${positn.acct}' />"/></span>
									</td>

									<td><span style="width:50px;" title="${positn.code}"><c:out value="${positn.code}" />&nbsp;</span></td>
									<td><span style="width:180px;" title="${positn.des}"><c:out value="${positn.des}" />&nbsp;</span></td>
									<td><span style="width:80px;" title="${positn.typ}"><c:out value="${positn.typ}" />&nbsp;</span></td>
									<td><span style="width:80px;" title="${positn.init}"><c:out value="${positn.init}" />&nbsp;</span></td>
									<td><span style="width:100px;" title="${positn.des1}"><c:out value="${positn.des1}" />&nbsp;</span></td>
<%-- 									<td><span style="width:30px;" title="${positn.locked}"><c:out value="${positn.locked}" />&nbsp;</span></td> --%>
									<td><span style="width:80px;" title="${positn.area}"><c:out value="${positn.area}" />&nbsp;</span></td>
<%-- 									<td><span style="width:80px;" title="${positn.mod2}"><c:out value="${positn.mod2}" />&nbsp;</span></td> --%>
									<td><span style="width:80px;" title="${positn.psarea}"><c:out value="${positn.psarea}" />&nbsp;</span></td>
<%-- 									<td><span style="width:180px;" title="${positn.ip}"><c:out value="${positn.ip}" />&nbsp;</span></td> --%>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="queryForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		function ajaxSearch(){
			if (event.keyCode == 13){	
				$('.search-div').hide();
				$('#listForm').submit();
			} 
		}
		$('#search').bind('click',function(e){
			$('#queryForm').submit();
		});
		$(document).ready(function(){
			//typ是否有值，有，默认选中
			f_typ();
			//按钮快捷键
			focus() ;//页面获得焦点
			$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
			});
	
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			changeTh();
			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			setElementHeight('.table-body',['.table-head'],'.grid');

			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
			$('.grid .table-body tr').live('dblclick',function(){
				$(this).find(':checkbox').attr("checked", true);
				select_Deliver();
			});		
			
			 $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="enter" />',
							title: '<fmt:message key="enter" />',
							//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-120px','0px']
							},
							handler: function(){
								select_Deliver();
								//alert($('#parentId').val());
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel" />',
							//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
//									parent.$('.close').click();
								top.closeCustom();
							}
						}]
				});
			
			//单击每行选中前面的checkbox
			$('.grid').find('.table-body').find('tr').live("click", function () {
				if($(this).find(':checkbox')[0].checked){
					$(":checkbox").attr("checked", false);
				}else{
					$(":checkbox").attr("checked", false);
					$(this).find(':checkbox').attr("checked", true);
				}
			 });
			
			
			//禁用checkbox本身的事件
			$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
				event.stopPropagation();
				if(this.checked){
					$(this).attr("checked",false);	
				}else{
					$(this).attr("checked",true);
				}
				$(this).closest('tr').click();
			});
			//---------------------------
			
			var t=$("#init").val();
			$("#init").val("").focus().val(t);
			
			//让之前选中的默认选中
			var defaultCode = parent.$('#defaultCode').val();
			var defaultName = parent.$('#defaultName').val();
			if(defaultCode!=''){
				$('.grid').find('.table-body').find(':checkbox').each(function(){
					if(this.id.substr(4,this.id.length)==defaultCode){
						$(this).attr("checked", true);
						$('#parentId').val(defaultCode);
						$('#parentName').val(defaultName);
					}
				});	
			}
		});	
		
		function select_Deliver(){
			if($('.grid').find('.table-body').find(':checkbox:checked').size()>0){
				$('.grid').find('.table-body').find(':checkbox:checked').each(function(){
					parent.$('#parentId').val($(this).closest('tr').find('td:eq(2)').find('span').attr('title'));
					parent.$('#parentName').val($(this).closest('tr').find('td:eq(3)').find('span').attr('title'));
				});
			}else{
				parent.$('#parentId').val('');
				parent.$('#parentName').val('');
			}
			top.customWindow.afterCloseHandler('Y');
			top.closeCustom();
		}
		
		//判断让typ有值的select的option项选中
	   function f_typ(){
		   var typ=$("#typ1").val();
		   $("select[@name=typ] option").each(function(){
				if($(this).val() == typ){
					$(this).attr("selected", true);
				}
		   });
	   }
		</script>
	</body>
</html>