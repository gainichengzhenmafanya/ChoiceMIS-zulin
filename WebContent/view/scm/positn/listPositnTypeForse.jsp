<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="suppliers_find" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    	<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<style type="text/css">
			.tr-select{
				background-color: #D2E9FF;
			}
			.page{
				margin-bottom: 25px;
			}
			.changePageSize{
				display:none;
			}
			.pgSearchInfo{
				display:none;
			}
			.separator{
				display:none !important;
			}
			.separator {
				margin:0 0 0 0;
				background:none;
			}
			.leftFrame{
				width : 22%;
			}
			.mainFrame{
				width : 78%
			}
		</style>
	</head>
	<body>
	<div class="leftFrame">
		<form id="listForm" action="" method="post">
			<input type="hidden" id="cwqx" name="cwqx" class="text" readonly="readonly" value="${cwqx}"/>
			<input type="hidden" id="parentId" name="parentId" class="text" readonly="readonly" value=""/>
			<input type="hidden" id="parentName" name="parentName" class="text" readonly="readonly" value=""/>
			<input type="hidden" id="defaultCode" name="defaultCode" class="text" readonly="readonly" value="${defaultCode}"/>
			<input type="hidden" id="defaultName" name="defaultName" class="text" readonly="readonly" value="${defaultName}"/>
				<div class="form-line" style="margin-top: 2px;">
<!-- 					<div class="form-label" style="width: 45px;">类型名称：</div> -->
<!-- 					  <div class="form-input" style="width: 80px;"> -->
<%-- 						<input type="text" name="des" id="des" style="width:70px;" value="${queryPositn.des}" style="width: 80px;" onkeydown="javascript: if(event.keyCode==13){$('#listForm').submit();} "/> --%>
<!-- 					</div> -->
<!-- 					<div class="tool"></div> -->
				</div>
				<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td><span style="width:25px;"><fmt:message key="coding" /></span></td>
									<td><span style="width:125px;"><fmt:message key="name" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0" class="datagrid">
							<tbody>
								<c:forEach var="listPositnType" items="${listPositnType}" varStatus="status">
									<tr>
										<td class="num"><span style="width:25px;">${status.index+1}</span></td>
										<td><span style="width:125px;">${listPositnType.des}</span></td>
										<td><input type="hidden" id="deliverCode" value="${listPositnType.code}"></input></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>			
				</div>
		</form>
	</div>
	<div class="mainFrame">
	      <iframe src="<%=path%>/positn/listDetailForse.do" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
   	</div> 
		<script type="text/javascript" src="<%=path%>/js/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				var offset = $(".tool").prev("div").offset();
				offset.left = $(".tool").prev("div").width() + offset.left + 10;
				$(".tool").button({
					text:'<fmt:message key="select" />',
					container:$(".tool"),
					position: {
						type: 'absolute',
						top: offset.top,	
						left: offset.left
					},
					handler:function(){
						$("#listForm").submit();
					}
				});
				//自动实现滚动条
			//	setElementHeight('.grid',['.tool'],$(document.body),40);	//计算.grid的高度
				//setElementHeight('.table-body',['.table-head'],'.grid',20);	//计算.table-body的高度
			//	loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').live("click", function () {
					$(this).addClass('tr-select');
					$('.grid').find('.table-body').find('tr').not(this).removeClass('tr-select');
					// var positnType=$(this).find('td:eq(1)').find('span').text();
					 var descode = $(this).find('td:eq(2)').find('input').val();
					$("#mainFrame").attr("src","<%=path%>/positn/listDetailForse.do?descode="+descode);
				});
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$('.grid').find('.table-body').find('tr:eq(0)').click();
			});			
		</script>
	</body>
</html>