<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>supply Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<style type="text/css"> 
	 	.page{
			margin-bottom: 0px;
		}
		</style>
	</head>
	<body>
		<div class="tool">
		</div>
		<input type="hidden" id="defaultCode" value="${defaultCode}"/>
		<form id="listForm" action="<%=path%>/deliver/searchAllDeliver.do" method="post">
			<input type="hidden" id="parentId" name="parentId" value=""/>
			<input type="hidden" id="parentName" name="parentName" value=""/>
			<table  cellspacing="0" cellpadding="0">
				<tr >
					<td class="c-left"><fmt:message key="coding" />：</td>
					<td><input type="text" id="code" name="code" class="text" value="${queryDeliver.code}" autocomplete="off" onkeydown="javascript: if(event.keyCode==13){$('#search').click();} "/></td>
					<td class="c-left"><fmt:message key="name" />：</td>
					<td><input type="text" id="des" name="des" class="text" value="${queryDeliver.des}" onkeydown="javascript: if(event.keyCode==13){$('#search').click();} "/></td>
					<td class="c-left"><fmt:message key="abbreviation" />：</td>
					<td><input type="text" id="init" name="init" style="text-transform:uppercase;" class="text" value="${queryDeliver.init}" onkeydown="javascript: if(event.keyCode==13){$('#search').click();} "/></td>
			        <td width="200">&nbsp;
			        	<input type="button" style="width:60px" id="search" name="search" value='<fmt:message key="select" />'/>
			        </td>
			    </tr>
			</table>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td style="width:31px;">&nbsp;</td>
								<td style="width:30px;">
									<input type="checkbox" id="chkAll"/>
								</td>
								<td style="width:50px;"><fmt:message key="coding" /></td>
								<td style="width:230px;"><fmt:message key="name" /></td>
								<td style="width:90px;"><fmt:message key="abbreviation" /></td>
								<td style="width:50px;"><fmt:message key="contact" /></td>
								<td style="width:50px;"><fmt:message key="contact" />2</td>
								<td style="width:100px;"><fmt:message key="tel" /></td>
								<td style="width:100px;"><fmt:message key="fax" /></td>
								<td style="width:140px;"><fmt:message key="address" /></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" id="tblGrid">
						<tbody>
							<c:forEach var="deliver" varStatus="step" items="${deliverList}">
								<tr>
									<td class="num" style="width:31px;">${step.count}</td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" name="idList" id="chk_${deliver.code}" value="${deliver.code}"/>
									</td>
									<td><span title="${deliver.code}" style="width:40px;text-align: left;">${deliver.code}&nbsp;</span></td>
									<td><span title="${deliver.des}" style="width:220px;text-align: left;">${deliver.des}&nbsp;</span></td>
									<td><span title="${deliver.init}" style="width:80px;text-align: left;">${deliver.init}&nbsp;</span></td>
									<td><span title="${deliver.person}" style="width:40px;text-align: left;">${deliver.person}&nbsp;</span></td>
									<td><span title="${deliver.person1}" style="width:40px;text-align: left;">${deliver.person1}&nbsp;</span></td>
									<td><span title="${deliver.tel}" style="width:90px;text-align: left;">${deliver.tel}&nbsp;</span></td>
									<td><span title="${deliver.tax}" style="width:90px;text-align: left;">${deliver.tax}&nbsp;</span></td>
									<td><span title="${deliver.addr}" style="width:130px;text-align: left;">${deliver.addr}&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				setElementHeight('.grid',['.tool'],$(document.body),80);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$('#search').bind("click",function search(){
				 	$('#listForm').submit();
				});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="enter"/>',
							title: '<fmt:message key="enter"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-20px']
							},
							handler: function(){
								/* if ($('.grid').find('.table-body').find(':checkbox').filter(':checked').size()==0) {
									alert("请选择供应商(可多选)！");
									return;
								} */
								var deliver = '';
								var deliverName = '';
								var first=true;
								$('.grid').find('.table-body').find(':checkbox').filter(':checked').each(function(){
									if (!first) {
										deliver = deliver+','+$(this).val();
										deliverName = deliverName +','+$(this).parent().parent().find('td:eq(3)').find('span').attr('title');
									}else {
										deliver = deliver+$(this).val();
										deliverName = deliverName+$(this).parent().parent().find('td:eq(3)').find('span').attr('title');
										first=false;
									}
								});
								 $('#parentId').val(deliver);
								 $('#parentName').val(deliverName);
								 top.customWindow.afterCloseHandler('Y');
							      top.closeCustom();
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								parent.$('.close').click();
							}
						}
					]
				});
				//------------------------------
				//点击checkbox改变
				$('.grid').find('.table-body').find('tr').find(':checkbox').bind("change", function () {
				     if ($(this)[0].checked) {
				    	 $(this).attr("checked", false);
				     }else{
				    	 $(this).attr("checked", true);
				     }
				 });
				// 当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').bind("click", function () {
				     if ($(this).find(':checkbox')[0].checked) {
				    	 $(this).find(':checkbox').attr("checked", false);
				     }else{
				    	 $(this).find(':checkbox').attr("checked", true);
				     }
				 });
				//---------------------------
				var t=$("#init").val();
				$("#init").val("").focus().val(t);
				
				//让之前选中的默认选中
				var defaultCode = $('#defaultCode').val();
				var codes = defaultCode.split(',');
				if(defaultCode!=''){
					$('.grid').find('.table-body').find(':checkbox').each(function(){
						for(var i in codes){
							if(this.id.substr(4,this.id.length)==codes[i]){
								$(this).attr("checked", true);
							}
						}
					})	
				}
			});
		</script>
	</body>
</html>