<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="branches_and_positions_information" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
	</head>
	<body>
		<div class="form">
			<form id="updateForm" method="post" action="<%=path %>/dataDictionary/saveData.do?flag=update">
				<input type="hidden" value="${dataTyp.pk_datatyp }" id="pk_datatyp" name="pk_datatyp"/>
				<div class="form-line" style="margin-top:30px;">
					<div class="form-label" ><span class="red">*</span><fmt:message key="coding" />：</div>
					<div class="form-input" >
						<input type="text" id="vcode" name="vcode" class="text" value="${dataTyp.vcode }" readonly="readonly" />
					</div>
					<div class="form-label"><span class="red">*</span><fmt:message key="name" />：</div>
					<div class="form-input">
						<input type="text" id="vname" name="vname" class="text" value="${dataTyp.vname }" onkeyup="getSpInit(this,'vinit');" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="abbreviation" />：</div>
					<div class="form-input">
						<input type="text" id="vinit" name="vinit" class="text" value="${dataTyp.vinit }"  />
					</div>
					<div class="form-label"><span class="red">*</span><fmt:message key="no" />：</div>
					<div class="form-input">
						<input type="text" maxlength="7" id="isortno" name="isortno" readonly="readonly" class="text" value="${dataTyp.isortno }"  />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="remark" />：</div>
					<div class="form-input">
						<textarea rows="7" style="white-space:wrap;margin-top:10px;width:400px;height:100px;" id="vmemo" name="vmemo" >${dataTyp.vmemo }</textarea>
					</div>
				</div>
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'vname',
						validateType:['canNull','maxLength','withOutSpecialChar'],
						param:['F',50,'F'],
						error:['<fmt:message key="name" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="the_maximum_length" />50','<fmt:message key="no_contain_special_char" />！']
					},{
						type:'text',
						validateObj:'vinit',
						validateType:['canNull','maxLength'],
						param:['F',50],
						error:['<fmt:message key="abbreviation" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="the_maximum_length" />50']
					},{
						type:'text',
						validateObj:'vmemo',
						validateType:['maxLength'],
						param:[150],
						error:['<fmt:message key="the_maximum_length" />150']
					}]
				});
			});
		</script>
	</body>
</html>