<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="income_or_sit_details" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/dataDictionary/listData.do" method="post">
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 25px;">&nbsp;</td>
								<td style="width:30px;">
									<input type="checkbox" id="chkAll"/>
								</td>
								<td><span style="width:60px;"><fmt:message key="coding" /></span></td>
								<td><span style="width:140px;"><fmt:message key="name" /></span></td>
								<td><span style="width:80px;"><fmt:message key="abbreviation" /></span></td>
								<td><span style="width:60px;"><fmt:message key="no" /></span></td>
								<td><span style="width:200px;"><fmt:message key="remark" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="dataTyp" items="${listDataTyp}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" name="idList" id="chk_<c:out value='${dataTyp.pk_datatyp}' />" value="<c:out value='${dataTyp.pk_datatyp}' />"/>
									</td>
									<td ><span style="width:60px;"><c:out value="${dataTyp.vcode}" /></span></td>
									<td ><span style="width:140px;"><c:out value="${dataTyp.vname}" /></span></td>
									<td ><span style="width:80px;"><c:out value="${dataTyp.vinit}" /></span></td>
									<td style="width:70px;text-align: right;"><c:out value="${dataTyp.isortno}" /></td>
									<td style="width:210px;" title='${dataTyp.vmemo}'><c:out value="${dataTyp.vmemo}" /></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<input type="hidden" id="vtyp" name="vtyp" value="${vtyp }" />
			<div class="search-div" style="margin-left: 0px;">
				<div class="search-condition">
					<table class="search-table" cellspacing="0" cellpadding="0" >
						<tr>
							<td class="c-left"><fmt:message key="coding" />：</td>
							<td><input type="text" id="vcode" name="vcode" class="text" onkeypress="IsNum(event)" /></td>
							<td class="c-left"><fmt:message key="name" />：</td>
							<td><input type="text" id="vname" name="vname"  class="text" /></td>
							<td class="c-left"><fmt:message key="abbreviation" />：</td>
							<td><input type="text" id="vinit" name="vinit" class="text" /></td>
						</tr>
					</table>
				</div>
				<div class="search-commit">
		       		<input type="button" class="search-button" id="search" value='<fmt:message key="enter" />'/>
		       		<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-40px']
							},
							handler: function(){
								$('.search-div').slideToggle(100);
							}
						},'-',{
							text: '<fmt:message key="insert" />',
							title: '<fmt:message key="new_role_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								saveDataTyp();
							}
						},{
							text: '<fmt:message key="update" />',
							title: '<fmt:message key="modify_the_role_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								updateDataTyp();
							}
						},{
							text: '<fmt:message key="delete" />',
							title: '<fmt:message key="delete"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								deleteDataTyp();
							}
						},"-",{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}
					]
				});
				
				$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0).click();
			 		}
			 	});
				
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#listForm').submit();
				});
				/* 模糊<fmt:message key="select" />清空 */
				$("#resetSearch").bind('click', function() {
					$('.search-condition input').val('');
				});
				// 自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),20);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				//双击弹出<fmt:message key="update" />
				$('.grid').find('.table-body').find('tr').live("dblclick", function () {
					$(":checkbox").attr("checked", false);
					$(this).find(':checkbox').attr("checked", true);
					updateDataTyp();
					if($(this).hasClass("show-firm-row"))return;
					$('.grid').find('.table-body').find('tr').removeClass("show-firm-row");
					$(this).addClass("show-firm-row");
				 });
				
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }
				     else
				     {
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				 });
				
				//新增
				function saveDataTyp(){
					$('body').window({
						id: 'window_saveOutlay',
						title: '<fmt:message key="insert" /><fmt:message key="data" />',
						content: '<iframe id="SaveForm" frameborder="0" src="<%=path%>/dataDictionary/addData.do?vtyp=${vtyp }"></iframe>',
						width: '650px',
						height: '300px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save_role_of_information"/>',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('SaveForm')&&window.document.getElementById("SaveForm").contentWindow.validate._submitValidate()){
											if (window.document.getElementById("SaveForm").contentWindow.checkVcode()){
												if (window.document.getElementById("SaveForm").contentWindow.checkIsortno()){
													submitFrameForm('SaveForm','saveForm');
												}
											}
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}
				
				//修改
				function updateDataTyp(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					if(checkboxList && checkboxList.filter(':checked').size() ==1){
						var action='<%=path%>/dataDictionary/updateData.do?pk_datatyp='+checkboxList.filter(':checked').val();
						$('body').window({
							id: 'window_updateOutlay',
							title: '<fmt:message key="update" /><fmt:message key="data" />',
							content: '<iframe id="UpdateForm" frameborder="0" src='+action+'></iframe>',
							width: '650px',
							height: '300px',
							draggable: true,
							isModal: true,
							topBar: {
								items: [{
										text: '<fmt:message key="save" />',
										title: '<fmt:message key="data" /><fmt:message key="update" />',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-80px','-0px']
										},
										handler: function(){
											if(getFrame('UpdateForm')&&window.document.getElementById("UpdateForm").contentWindow.validate._submitValidate()){
												submitFrameForm('UpdateForm','updateForm');
											}
										}
									},{
										text: '<fmt:message key="cancel" />',
										title: '<fmt:message key="cancel" />',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-160px','-100px']
										},
										handler: function(){
											$('.close').click();
										}
									}
								]
							}
						});
					}else if(checkboxList && checkboxList.filter(':checked').size() > 1){
						alert('<fmt:message key="you_can_only_modify_a_data" />！');
						return;
					}else {
						alert('<fmt:message key="please_select_information_you_need_to_modify"/>！');
						return ;
					}
				}
			});
			
			//删除
			function deleteDataTyp(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					if(checkboxList && checkboxList.filter(':checked').size() > 0){
						if(confirm('<fmt:message key="delete_data_confirm" />？')){
							var chkValue = [];
							checkboxList.filter(':checked').each(function(){
								chkValue.push($(this).val());
							});
							var action = '<%=path%>/dataDictionary/saveData.do?flag=delete&pk_datatyp='+chkValue.join(",");
							$('body').window({
								title: '<fmt:message key="delete" /><fmt:message key="data" />',
								content: '<iframe frameborder="0" src='+action+'></iframe>',
								width: '340px',
								height: '255px',
								draggable: true,
								isModal: true
							});
						}
					}else{
						alert('<fmt:message key="please_select_information_you_need_to_delete" />！');
						return ;
					}
				}
		</script>
	</body>
</html>