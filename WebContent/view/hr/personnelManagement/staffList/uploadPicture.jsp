<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>上传<fmt:message key="the_picture" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		
		<style type="text/css">@import url(<%=path%>/js/mis/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css);</style>
			
	</head>
	<body>
		<div class="form">
			<form id="screenimageForm" method="post" enctype="multipart/form-data">
				<table border="1">
					<tr>
						<td colspan="2">
							<div style="width: 750px; margin: 0px auto">
								<div id="uploader" style="">
									<input type="hidden" id="pk_employee" name="pk_employee" value="${pk_employee }" />
									<p><fmt:message key="please_check_if_your_machine_is_installed" />
									   Flash, Silverlight, Gears, BrowserPlus,HTML5 .</p>
								</div>
							</div>
						</td>
					</tr>
				</table>
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>		
		<script type="text/javascript" src="<%=path%>/js/mis/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/colorpicker/jscolor.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 	
		<script type="text/javascript" src="<%=path%>/js/mis/plupload/js/plupload.full.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/plupload/js/jquery.plupload.queue/jquery.plupload.queue.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/plupload/js/i18n/cn.js"></script>
				
		<script type="text/javascript">
		
		$(function() {
			var pk_employee = $("#pk_employee").val();
			$("#uploader").pluploadQueue({
				// General settings
				runtimes : 'gears,flash,silverlight,browserplus,html5,html4',
				url : '<%=path%>/staffList/saveImage.do?pk_employee='+pk_employee,
				max_file_size : '45kb',
				unique_names : false,
				chunk_size: '45kb',
				// Specify what files to browse for
				filters : [
					{title : "image(*.png)", extensions : "png"}
				],
				
				// Flash settings
				flash_swf_url : '<%=path%>/js/mis/plupload/js/plupload.flash.swf',
				// Silverlight settings
				silverlight_xap_url : '<%=path%>/js/mis/plupload/js/plupload.silverlight.xap'
			});
			$('form').submit(function(e) {
		        var uploader = $('#uploader').pluploadQueue();
		        if (uploader.total.uploaded == 0) {
			        if (uploader.files.length > 0) {
			            // When all files are uploaded submit form
			            uploader.bind('StateChanged', function() {
			                if (uploader.files.length === (uploader.total.uploaded + uploader.total.failed)) {
			                    $('form')[0].submit();
			                }
			            });
			            uploader.start();
			        } else {
						alert('<fmt:message key="Please_upload_the_data_file" />.');
					}
// 			        return false;
			        e.preventDefault();
		        }
	    	});
		});
		
		</script>
	</body>
</html>