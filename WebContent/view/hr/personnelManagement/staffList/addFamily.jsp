<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="branches_and_positions_information" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<style type="text/css">
			.form {
				position: relative;
				width: 100%;
			}
		</style>
	</head>
	<body>
		<div class="form">
			<form id="familySaveForm" method="post" action="<%=path %>/staffList/saveFamily.do">
				<div class="form-line">
					<div class="form-label" ><span class="red">*</span><fmt:message key="hrMembers" /></div>
					<div class="form-input" >
						<input type="text" id="vmembers" name="vmembers" class="text" value="${family.vmembers }"/>
					</div>
					<div class="form-label" ><span class="red">*</span><fmt:message key="Relationship_between" /></div>
					<div class="form-input" >
						<input type="text" class="text" id="vrelationship" name="vrelationship" value="${family.vrelationship }" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="sex" /></div>
					<div class="form-input">
						<select id="vsex" name="vsex" class="select" >
							<option value="0" <c:if test="${family.vsex == '0' }">selected="selected"</c:if>><fmt:message key="male" /></option>
							<option value="1" <c:if test="${family.vsex == '1' }">selected="selected"</c:if>><fmt:message key="female" /></option>
						</select>
					</div>
					<div class="form-label"><fmt:message key="birthday" /></div>
					<div class="form-input">
						<input autocomplete="off" type="text" id="vbirthday" name="vbirthday" readonly="readonly" class="Wdate text" value="${family.vbirthday }"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="hrpost" /></div>
					<div class="form-input">
						<select id="vpost" name="vpost" class="text" style="width:133px;height:20px;">
							<option value=""></option>
							<c:forEach var="station" items="${vstation }" >
								<option value="${station.vname }" <c:if test="${station.vcode == family.vpost }">selected="selected"</c:if> >${station.vname }</option>
							</c:forEach>
						</select>
					</div>
					<div class="form-label"><fmt:message key="political_landscape" /></div>
					<div class="form-input">
						<select id="vpubliclist" name="vpubliclist" class="text" style="width:133px;height:20px;">
							<option value=""></option>
							<c:forEach var="publiclist" items="${vpubliclist }" >
								<option value="${publiclist.vname }" <c:if test="${publiclist.vcode == family.vpubliclist }">selected="selected"</c:if>>${publiclist.vname }</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="hrworkunit" /></div>
					<div class="form-input">
						<input type="text" class="text" id="vworkunit" name="vworkunit" value="${family.vworkunit }" />
					</div>
					<div class="form-label"><span style="color: red">*</span><fmt:message key="phone" /></div>
					<div class="form-input">
						<input type="text" class="text" id="vphone" name="vphone" value="${family.vphone }" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="link_address" /></div>
					<div class="form-input">
						<input type="text" class="text" id="vaddress" name="vaddress" value="${family.vaddress }" />
					</div>
				</div>
				<input type="hidden" id="flag" name="flag" value="${flag}"/>
				<input type="hidden" id="pk_family" name="pk_family" value="${family.pk_family}"/>
				<input type="hidden" id="pk_employee" name="pk_employee" value="${pk_employee}"/>
			</form>
		</div>
		<input type="hidden" id="subflag" value="0" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#vbirthday").htmlUtils("setDate","now");
				$("#vbirthday").click(function(){
		  			new WdatePicker();
		  		});
				
				//验证
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'vmembers',
						validateType:['canNull','maxLength','withOutSpecialChar'],
						param:['F',20,'F'],
						error:['<fmt:message key="hrMembers" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="the_maximum_length" />20','<fmt:message key="no_contain_special_char" />！']
					},{
						type:'text',
						validateObj:'vrelationship',
						validateType:['canNull','maxLength','withOutSpecialChar'],
						param:['F',10,'F'],
						error:['<fmt:message key="Relationship_between" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="the_maximum_length" />20','<fmt:message key="no_contain_special_char" />！']
					},{
						type:'text',
						validateObj:'vphone',
						validateType:['canNull','telephone'],
						param:['F','F'],
						error:['<fmt:message key="tel" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="telephone_format_is_not_correct" />！']
					}]
				});
			});
		</script>
	</body>
</html>