<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="hr_EmployeeRecordsMaintenance" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>			
	</head>
	<body >
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/employeeEntry/saveEmployeeEntry.do" method="post">
			<input type="hidden" id="vdutyid" name="vdutyid"/>
			<input type="hidden" id="vstation" name="vstation"/>
			<input type="hidden" id="vdutylvl" name="vdutylvl"/>
			<input type="hidden" id="pk_operator" name="pk_operator"/>
			
			<!-- 标题  -->
			<div class="form-line">
				<div class="form-label">
					<span style="color: blue;font-size: 18px;">【<fmt:message key="employees" /><fmt:message key="entry" />】</span>
				</div>
				<div class="form-label" style="margin-left:43px;">
					<input type="hidden" id="vempno" name="vempno" value="${employee.vempno }" />
					<b><fmt:message key="employees" /><fmt:message key="The_work_number" /></b>:<span id="empno" style="color:red;">${employee.vempno }</span>
				</div>
			</div>	
			<fieldset style="height: 345px;padding-left:43px;">
					<!-- 姓名缩写 考勤号 检测黑名单 -->
				<div class="form-line">
					<div class="form-label" style="width:50px;text-align: right;">
						<span style="color: red">*</span><fmt:message key="employees" /><fmt:message key="Full_name" />:
					</div>
					<div class="form-input" style="width:100px;">
						<input type="text" id="vname" name="vname" class="text" style="margin-bottom:3px;width:80px;" value="${employee.vname }" onkeyup="getSpInit_x(this,'vinit');"/>
						<img id="searchStoreOperator" class="search" src="/ChoiceMIS/image/themes/icons/search.png" />
					</div>
					<div class="form-label" style="width:65px;text-align: right;">
						<fmt:message key="employees" /><fmt:message key="abbreviation" />:
					</div>
					<div class="form-input" style="width:100px;margin-left:-8px;">
						<input type="text" id="vinit" name="vinit" class="text" style="margin-bottom:3px;width:100px;" value="${employee.vinit }"/>
					</div>
					<div class="form-label" style="width:65px;text-align: right;">
						<span style="color:red;">*</span><fmt:message key="Attendance_No" />:</div>
					<div class="form-input" style="width:100px;margin-left:-8px;">
						<input type="text" id="vkqcardno" name="vkqcardno" class="text" style="margin-bottom:3px;width: 100px;" value="${employee.vkqcardno }" />
					</div>
				</div>
				<!-- 身份证  -->
				<div class="form-line">
					<div class="form-label" style="width:50px;text-align: right;">
						<span style="color:red;">*</span><fmt:message key="ID_number" />:
					</div>
					<div class="form-input" style="width:280px;">
						<input type="text" id="vidcard" name="vidcard" class="text" onkeyup="value=value.replace(/[^\d]/g,'') " style="margin-bottom:3px;width:269px;" value="${employee.vidcard }"/>
					</div>
					<div class="form-label" style="width:50px;text-align: right;">
						<span style="color: blue;cursor: pointer;" id="checkBlack" onclick="checkBlack()"><u><fmt:message key="hr_check" /><fmt:message key="Black_list" /></u></span>
					</div>
				</div>
				<!-- 部门 -->
				<div class="form-line">
					<div class="form-label" style="width:50px;text-align: right;">
						<span style="color:red;">*</span><fmt:message key="entry" /><fmt:message key="sector" />:
						<input type="hidden" id="pk_hrdept" name="pk_hrdept" value="${employee.pk_hrdept }" />
						<input type="hidden" id="pk_store" name="pk_store" value="${employee.pk_store }" />
					</div>
					<div class="form-input" style="width:265px;">
						<input type="text" id="vdeptdes" name="vdeptdes" readonly="readonly" class="text" style="margin-bottom:3px;width: 269px;" value="${employee.vdeptdes }"/>
					</div>
					<div class="form-input" style="width:80px;margin-left:10px;">
						<input type="text" id="vdeptcode" name="vdeptcode" class="text" readonly="readonly" style="margin-bottom:3px;width:80px;" value="${employee.vdeptcode }"/>
					</div>
					<div class="form-input" style="width:10px;" align="left">
						<img id="searchHrDept" class="search" src="<%=path%>/image/themes/icons/search.png" />
					</div>
				</div>
				<!-- 年龄  任职岗位  岗位级别 -->
				<div class="form-line">
					<div class="form-label" style="width:50px;text-align: right;margin-left:6px;">
						<span style="color:red;">*</span><fmt:message key="age" />:
					</div>
					<div class="form-input" style="width:100px;margin-left:-6px;">
						<input type="text" id="vage" name="vage" class="text" style="margin-bottom:3px;width:100px;" onkeyup="value=value.replace(/[^\d]/g,'') "  value="${employee.vage }"/>
					</div>
					<div class="form-label" style="width:65px;text-align: right;">
						<span style="color:red;">*</span><fmt:message key="office" /><fmt:message key="Post" />:
					</div>
					<div class="form-input" style="width:100px;margin-left:-8px;">
						<select id="pk_station" name="pk_station" class="text" style="margin-top: 3px;width:101px;height: 20px;">
							<c:forEach var="school" items="${pk_station }" >
									<option value="${school.pk_datatyp }" <c:if test="${employee.pk_station == school.pk_datatyp }">selected="selected"</c:if>>${school.vname }</option>
							</c:forEach>
						</select>
					</div>
					<div class="form-label" style="width:65px;text-align: right;">
						<span style="color:red;">*</span><fmt:message key="Post" /><fmt:message key="hrLevel" />:
					</div>
					<div class="form-input" style="width:100px;margin-left:-8px;">
						<select id="pk_dutylvl" name="pk_dutylvl" class="text" style="margin-top: 3px;width:101px;height: 20px;">
							<c:forEach var="school" items="${pk_dutylvl }" >
								<option value="${school.pk_datatyp }" <c:if test="${employee.pk_dutylvl == school.pk_datatyp }">selected="selected"</c:if>>${school.vname }</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<!-- 入职时间 试用期 转正日期 -->
				<div class="form-line">
					<div class="form-label" style="width:50px;text-align: right;margin-left:5px;">
						<fmt:message key="entry" /><fmt:message key="date" />:
					</div>
					<div class="form-input" style="width:100px;margin-left:-5px;">
						<input autocomplete="off" type="text" id="vjrdat" name="vjrdat" style="width:100px;" class="Wdate text" value="${employee.vjrdat }"/>
					</div>
					<div class="form-input" style="width:65px;text-align: right;margin-left:-5px;">
						<fmt:message key="hr_The_probation_period" />:
					</div>
					<div class="form-input" style="width:100px;margin-left:-3px;">
						<select id="vtrial" name="vtrial" class="text" style="margin-top: 3px;width:101px;height: 20px;">
							<c:forEach var="school" items="${vtrial }" >
									<option value="${school.vname }" <c:if test="${employee.vtrial == school.vname }">selected="selected"</c:if>>${school.vname }</option>
							</c:forEach>
						</select>
					</div>
					<div class="form-label" style="width:65px;text-align: right;">
						<fmt:message key="positive" /><fmt:message key="date" />:
					</div>
					<div class="form-input" style="width:100px;margin-left:-8px;">
						<input autocomplete="off" type="text" id="vwkdat" name="vwkdat" style="width:100px;" class="Wdate text" value="${employee.vwkdat }"/>
					</div>
				</div>
				<!-- 工资级别 工资标准  基本工资 -->
				<div class="form-line">
					<div class="form-label" style="width:50px;text-align: right;">
						<span style="color:red;">*</span><fmt:message key="Wage" /><fmt:message key="hrLevel" />:
					</div>
					<div class="form-input" style="width:100px;">
						<select id="pk_dutyid" name="pk_dutyid" class="text" style="margin-top: 3px;width:102px;height: 20px;">
							<c:forEach var="school" items="${pk_dutyid }" >
									<option value="${school.pk_datatyp }" <c:if test="${employee.pk_dutyid == school.pk_datatyp }">selected="selected"</c:if>>${school.vname }</option>
							</c:forEach>
						</select>
					</div>
					<div class="form-label" style="width:65px;text-align: right;">
						<span style="color:red;">*</span><fmt:message key="Wage" /><fmt:message key="smc_standard" />:
					</div>
					<div class="form-input" style="width:100px;margin-left:-8px;">
						<input type="text" id="vwage" name="vwage" style="width:100px;" onkeyup="this.value=this.value.replace(/[^\d\.\d{0,3}]/g,'')"  class="text" value="${employee.vwage }"/>
					</div>
					<div class="form-label" style="width:65px;text-align:right;">
						<span style="color:red;">*</span><fmt:message key="baseic" /><fmt:message key="Wage" />:
					</div>
					<div class="form-input" style="width:100px;margin-left:-8px;">
						<input type="text" id="ngzwage" name="ngzwage" style="width:100px;" onkeyup="this.value=this.value.replace(/[^\d\.\d{0,3}]/g,'')"   class="text" value="${employee.ngzwage }"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label" style="width:56px;text-align: right;">
						<fmt:message key="remark"/>:</div>
					<div class="form-input"style="text-align: right;margin-left:-6px;margin-top:3px;">
						<textarea rows="10" style="width:435px;" cols="32" id = "vmemo" name ="vmemo">${employee.vmemo}</textarea>
					</div>
				</div>
			</fieldset>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
		<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>
		<script type="text/javascript">
			//ajax同步设置
			$.ajaxSetup({
				async: false
			});
			$(document).ready(function(){
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),32);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadValidate();
				// 按钮加载
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="save" />',
							title: '<fmt:message key="save" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','0px']
							},
							handler: function(){
								save();
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
							   parent.$(".close").click();						
							}
						}
					]
				});
				
				// 加载时间选择
				$("#vjrdat").focus(function(){
		 			new WdatePicker();
		 		});
				$("#vwkdat").focus(function(){
		 			new WdatePicker({minDate:$("#vjrdat").val()});
		 		});

				$("#searchStoreOperator").click(function(){
					selectStoreOperator({
		 				basePath:'<%=path%>',
		 				width:600,
		 				domId:$("#pk_operator").val(),
		 				pk_store:$("#pk_store").val(),
		 				width: 400,
						height: 350
		 			});
				});
			});

			//门店操作员
			function setStoreOperator(data){
	  	 		$("#pk_operator").val(data.pk);
	  	 		$("#vempno").val(data.code);
	  	 		$("#empno").text(data.code);
	  	 		$("#vname").val(data.name);
	  	 		$("#vinit").val(data.init);
	  	 	}
			
			// 加载验证
			function loadValidate(){
				validate = new Validate({
					validateItem:[
					 {
					// 工资级别
						type:'select',
						validateObj:'pk_dutyid',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="Wage" /><fmt:message key="hrLevel" /><fmt:message key="cannot_be_empty" />!']
					},
					{// 岗位级别
						type:'select',
						validateObj:'pk_dutylvl',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="Post" /><fmt:message key="hrLevel" /><fmt:message key="cannot_be_empty" />!']
					},
					{// 任职岗位
						type:'select',
						validateObj:'pk_station',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="office" /><fmt:message key="Post" /><fmt:message key="cannot_be_empty" />!']
					},
					{// 姓名
						type:'text',
						validateObj:'vname',
						validateType:['canNull','maxLength','withOutSpecialChar'],
						param:['F',50,'T'],
						error:['<fmt:message key="name" /><fmt:message key="cannot_be_empty" />!','<fmt:message key="the_maximum_length" />50','<fmt:message key="no_contain_special_char" />!']
					},{// 身份证
						type:'text',
						validateObj:'vidcard',
						validateType:['canNull','idCard'],
						param:['F','F'],
						error:['<fmt:message key="ID_number" /><fmt:message key="cannot_be_empty" />','<fmt:message key="ID_number" /><fmt:message key="incorrect_format" />！']
					},{
						// 年龄
						type:'text',
						validateObj:'vage',
						validateType:['canNull','maxValue'],
						param:['F','200'],
						error:['<fmt:message key="age" /><fmt:message key="cannot_be_empty" />','<fmt:message key="most_can_more_than" />200']
					},
					{
						// 部门
						type:'text',
						validateObj:'vdeptdes',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="entry" /><fmt:message key="sector" /><fmt:message key="cannot_be_empty" />']
					},
					{
						//考勤机
						type:'text',
						validateObj:'vkqcardno',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="Attendance_No" /><fmt:message key="cannot_be_empty" />']
					},{
						//基本工资
						type:'text',
						validateObj:'ngzwage',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="baseic" /><fmt:message key="Wage" /><fmt:message key="cannot_be_empty" />']
					},{
						//工资标准
						type:'text',
						validateObj:'vwage',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="Wage" /><fmt:message key="smc_standard" /><fmt:message key="cannot_be_empty" />']
					}]
				});
			}
			
			function save(){
				if(checkBlack_list()){
					return;					
				}
				if(validate._submitValidate()){
					$("#vdutyid").val($("#pk_dutylvl option:selected").text());
					$("#vstation").val($("#pk_station option:selected").text());
					$("#vdutylvl").val($("#pk_dutyid option:selected").text());
					$("#listForm").submit();
				}
			}
			
			// 黑名单验证
			function checkBlack_list(){
				if($("#vidcard").val()=='' || $("#vidcard").val()== null){
					alert('<fmt:message key="hr_checkidCardNull" />');
					return true;
				}
				var bool  = false;
				var param = {};
				param['vidcard'] = $("#vidcard").val();
				$.post('<%=path%>/employeeEntry/checkBlackList.do',param,function(data){
					if(data==1){
						alert('<fmt:message key="hr_checkblackError" />');
						bool = true;					
					}
				});
				return bool;
			}
			
			// 部门选择
			$("#searchHrDept").click(function(){
				var pk_store = $("#pk_store").val();
				var url = "<%=path%>/organizationStructure/toChooseHrDept.do";
					url += "?callBack=setHrDept&domId=selected&single=true&pk_store="+pk_store;
				return $('body').window({
					id: 'window_chooseSiteType',
					title: '<fmt:message key="select1" /><fmt:message key="sector" />',
					content: '<iframe id="listSiteTypeFrame" frameborder="0" src='+url+'></iframe>',
					width: 400,
					height: 350,
					confirmClose: false,
					draggable: true,
					isModal: true
				});
			});
			
			//接收部门 选中返回值
			function setHrDept(data){
	  	 		$("#pk_hrdept").val(data.code);
	  	 		$("#vdeptcode").val(data.show);
	  	 		$("#vdeptdes").val(data.name);
	  	 	}
	
			function checkBlack(){
				if(checkBlack_list()){
					return;					
				}else{
					alert('<fmt:message key="hr_checkblackOk" />');
				}
			}
		</script>
	</body>
</html>