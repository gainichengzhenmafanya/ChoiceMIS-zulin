<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="branches_and_positions_information" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<style type="text/css">
			.form {
				position: relative;
				width: 100%;
			}
		</style>
	</head>
	<body>
		<div class="form">
			<form id="employeeSaveForm" method="post" action="<%=path %>/staffList/saveEmployee.do">
				<div class="form-line">
					<div class="form-label">
						<span style="color: blue;font-size: 18px;">【<fmt:message key="archives" /><fmt:message key="maintenance" />】</span>
					</div>
					<div class="form-label">
						<input type="hidden" id="pk_store" name="pk_store" value="${employee.pk_store }" />
						<input type="hidden" id="pk_employee" name="pk_employee" value="${employee.pk_employee }" />
						<input type="hidden" id="vempno" name="vempno" value="${employee.vempno }" />
						<input type="hidden" id="vstatus" name="vstatus" value="${employee.vstatus }" />
						<b><fmt:message key="The_work_number" /></b>:${employee.vempno }
					</div>
					<div class="form-label">
						<b><fmt:message key="Full_name" /></b>:<span style="color: blue;">${employee.vname }</span>
					</div>
					<div class="form-label">
						<c:if test="${'' != employee.pk_employee }"><span style="color: red">【${employee.vstatus }<fmt:message key="status" />】</span></c:if>
					</div>
				</div>
				<div class="grid">
					<div class="easyui-tabs" fit="false" plain="true" id="tabs">
						<div title="<fmt:message key="baseic" />">
							<div style="border:solid #416AA3 2px;margin-top:3px;height:200px;margin-left:2px;margin-right:6px;">
								<input type="hidden" id="flag" value="baseic" />
								<div id="imgDiv" style="position: absolute;margin-left:518px;margin-top:3px;width:100px;height:150px;border: 1px #000000 solid;">
									<c:if test="${'' != image}">
										<img src="<%=path%>/scrimg/${image}.PNG" />
									</c:if>
								</div>
								<div style="position: absolute;margin-left:518px;margin-top:160px;width:100px;height:50px;">
									<span style="color: red"><b><fmt:message key="smc_standard" />：100 * 150</b></span><br />
									<span style="color: red"><b><fmt:message key="most_can_more_than" />45KB</b></span>
								</div>
								<div class="form-line">
									<div class="form-label" style="width:50px;">
										<span style="color: red">*</span><fmt:message key="employee_name" />:
									</div>
									<div class="form-input" style="width:10px;">
										<input type="text" id="vname" name="vname" readonly="readonly" class="text" style="margin-bottom:3px;width: 60px;" value="${employee.vname }" onkeyup="getSpInit(this,'vinit');"/>
									</div>
									<div class="form-label" style="width:25px;text-align: left;margin-left:60px;">
										<fmt:message key="sex" />:
									</div>
									<div class="form-input" style="width:20px;">
										<select id="pk_sex" name="pk_sex" class="text" style="margin-top: 3px;height:20px;">
											<c:forEach var="sex" items="${vsex }" >
												<option value="${sex.pk_datatyp }" <c:if test="${employee.pk_sex == sex.pk_datatyp }">selected="selected"</c:if>>${sex.vname }</option>
											</c:forEach>
										</select>
									</div>
									<div class="form-label" style="width:50px;margin-left:18px;">
										<span style="color:red;">*</span><fmt:message key="employees" /><fmt:message key="age" />:
									</div>
									<div class="form-input" style="width:10px;">
										<input type="text" id="vage" name="vage" class="text" style="margin-bottom:3px;width:85px;" value="${employee.vage }"/>
									</div>
									<div class="form-label" style="width:30px;text-align: right;margin-left:103px;">
										<span style="color:red;">*</span><fmt:message key="abbreviation" />:
									</div>
									<div class="form-input" style="width:10px;">
										<input type="text" id="vinit" name="vinit" class="text" style="width:85px;" value="${employee.vinit }"/>
									</div>
								</div>
								<div class="form-line">
									<div class="form-label" style="width:50px;">
										<span style="color: red">*</span><fmt:message key="ID_number" />:
									</div>
									<div class="form-input" style="width:10px;">
										<input type="text" id="vidcard" name="vidcard" readonly="readonly" class="text" style="margin-bottom:3px;width: 144px;" value="${employee.vidcard }"/>
									</div>
									<div class="form-label" style="width:50px;text-align: left;margin-left:135px;">
										<span style="color: red">*</span><fmt:message key="Check_work_attendance" /><fmt:message key="Card_number" />:
									</div>
									<div class="form-input" style="width:10px;">
										<input type="text" id="vkqcardno" name="vkqcardno" class="text" style="margin-bottom:3px;width: 85px;" value="${employee.vkqcardno }" />
									</div>
									<div class="form-label" style="width:25px;text-align: right;margin-left:103px;">
										<span style="color: red">*</span><fmt:message key="the_national" />:
									</div>
									<div class="form-input" style="width:30px;text-align: right;margin-left:6px;">
										<input type="hidden" id="pk_nation" name="pk_nation" value="${employee.pk_nation }"/>
										<input type="text" id="vnation" name="vnation" class="text" readonly="readonly" style="width:65px;" value="${employee.vnation }"/>
										<img id="searchNation" class="search" src="<%=path%>/image/themes/icons/search.png" />
									</div>
								</div>
								<div class="form-line">
									<div class="form-label" style="width:45px;margin-left:5px;">
										<fmt:message key="Social_security" /><fmt:message key="account" />:
									</div>
									<div class="form-input" style="width:10px;">
										<input type="text" id="vwagetyp" name="vwagetyp" class="text" style="margin-bottom:3px;width: 144px;" value="${employee.vwagetyp }"/>
									</div>
									<div class="form-label" style="width:26px;text-align: left;margin-left:135px;">
										<span style="color:red;">*</span><fmt:message key="The_form_of_employment" />:
									</div>
									<div class="form-input" style="width:25px;margin-left:24px;">
										<select id="pk_worktyp" name="pk_worktyp" class="text" style="margin-top: 3px;width: 87px;height: 20px;">
											<c:forEach var="worktyp" items="${vworktyp }" >
												<option value="${worktyp.pk_datatyp }" <c:if test="${employee.pk_worktyp == worktyp.pk_datatyp }">selected="selected"</c:if>>${worktyp.vname }</option>
											</c:forEach>
										</select>
									</div>
									<div class="form-label" style="width:25px;text-align: right;margin-left:88px;">
										<span style="color: red">*</span><fmt:message key="record_of_formal_schooling" />:
									</div>
									<div class="form-input" style="width:55px;margin-left:6px;">
										<select id="pk_schoolage" name="pk_schoolage" class="text" style="margin-top: 3px;width: 87px;height: 20px;">
											<c:forEach var="school" items="${vschoolage }" >
												<option value="${school.pk_datatyp }" <c:if test="${employee.pk_schoolage == school.pk_datatyp }">selected="selected"</c:if>>${school.vname }</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="form-line">
									<div class="form-label" style="width:45px;margin-left:5px;">
										<fmt:message key="Health_insurance" /><fmt:message key="account" />:
									</div>
									<div class="form-input" style="width:10px;">
										<input type="text" id="vmedno" name="vmedno" class="text" style="margin-bottom:3px;width: 144px;" value="${employee.vmedno }"/>
									</div>
									<div class="form-label" style="width:20px;text-align: right;margin-left:136px;">
										<span style="color:red;">*</span><fmt:message key="political_landscape" />:
									</div>
									<div class="form-input" style="width:55px;margin-left:29px;">
										<select id="pk_publiclist" name="pk_publiclist" class="text" style="margin-top: 3px;width: 87px;height: 20px;">
											<c:forEach var="publiclist" items="${vpubliclist }" >
												<option value="${publiclist.pk_datatyp }" <c:if test="${employee.pk_publiclist == publiclist.pk_datatyp }">selected="selected"</c:if>>${publiclist.vname }</option>
											</c:forEach>
										</select>
									</div>
									<div class="form-label" style="width:25px;text-align: right;margin-left:63px;">
										<fmt:message key="Academic_degree" />:
									</div>
									<div class="form-input" style="width:43px;text-align: right;margin-left:1px;">
										<select id="pk_degree" name="pk_degree" class="text" style="margin-top: 3px;width: 87px;height: 20px;">
											<option value=""></option>
											<c:forEach var="degree" items="${vdegree }" >
												<option value="${degree.pk_datatyp }" <c:if test="${employee.pk_degree == degree.pk_datatyp }">selected="selected"</c:if>>${degree.vname }</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="form-line">
									<div class="form-label" style="width:45px;margin-left:5px;">
										<fmt:message key="bank" /><fmt:message key="Card_number" />:
									</div>
									<div class="form-input" style="width:10px;">
										<input type="text" id="vbankno" name="vbankno" class="text" style="margin-bottom:3px;width: 144px;" value="${employee.vbankno }"/>
									</div>
									<div class="form-label" style="width:20px;text-align:right;margin-left:136px;">
										<span style="color:red;">*</span><fmt:message key="Registered_residence" /><fmt:message key="type" />:
									</div>
									<div class="form-input" style="width:55px;margin-left:29px;">
										<select id="pk_house" name="pk_house" class="text" style="margin-top: 3px;width: 87px;height: 20px;">
											<c:forEach var="house" items="${vhouse }" >
												<option value="${house.pk_datatyp }" <c:if test="${employee.pk_house == house.pk_datatyp }">selected="selected"</c:if>>${house.vname }</option>
											</c:forEach>
										</select>
									</div>
									<div class="form-label" style="width:44px;text-align: left;margin-left:40px;">
										<fmt:message key="Foreign_language_level" />:
									</div>
									<div class="form-input" style="width:12px;">
										<select id="pk_technic" name="pk_technic" class="text" style="margin-left:5px;margin-top: 4px;width: 87px;height: 20px;">
											<c:forEach var="school" items="${vtechnic }" >
												<option value="${school.pk_datatyp }" <c:if test="${employee.pk_technic == school.pk_datatyp }">selected="selected"</c:if>>${school.vname }</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="form-line">
									<div class="form-label" style="width:45px;margin-left:5px;">
										<fmt:message key="University_one_is_graduated_from" />:
									</div>
									<div class="form-input" style="width:10px;">
										<input type="text" id="vschool" name="vschool" class="text" style="margin-bottom:3px;width: 144px;" value="${employee.vschool }"/>
									</div>
									<div class="form-label" style="width:15px;text-align:right;margin-left:141px;">
										<fmt:message key="Insurance_status" />:
									</div>
									<div class="form-input" style="width:55px;margin-left:29px;">
										<select id="pk_bxsta" name="pk_bxsta" class="text" style="margin-top: 3px;width: 87px;height: 20px;">
											<c:forEach var="school" items="${vbxsta }" >
												<option value="${school.pk_datatyp }" <c:if test="${employee.pk_bxsta == school.pk_datatyp }">selected="selected"</c:if>>${school.vname }</option>
											</c:forEach>
										</select>
									</div>
									<div class="form-label" style="width:20px;text-align:right;margin-left:40px;">
										<fmt:message key="employees_source" />:
									</div>
									<div class="form-input" style="width:40px;margin-left:29px;">
										<select id="pk_comfrom" name="pk_comfrom" class="text" style="margin-top: 3px;width: 87px;height: 20px;">
											<c:forEach var="school" items="${vcomfrom }" >
												<option value="${school.pk_datatyp }" <c:if test="${employee.pk_comfrom == school.pk_datatyp }">selected="selected"</c:if>>${school.vname }</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="form-line">
									<div class="form-label" style="width:25px;text-align: right;margin-left:5px;">
										<fmt:message key="Professional" />:
									</div>
									<div class="form-input" style="width:10px;margin-left:19px;">
										<input type="text" id="vplace" name="vplace" class="text" style="margin-left:1px;width:144px;" value="${employee.vplace }"/>
									</div>
									<div class="form-label" style="width:15px;text-align: right;margin-left:142px;">
										<fmt:message key="marital_status" />:
									</div>
									<div class="form-input" style="width:55px;margin-left:29px;">
										<select id="pk_marriage" name="pk_marriage" class="text" style="margin-top: 3px;width: 87px;height: 20px;">
											<c:forEach var="school" items="${vmarriage }" >
												<option value="${school.pk_datatyp }" <c:if test="${employee.pk_marriage == school.pk_datatyp }">selected="selected"</c:if>>${school.vname }</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="form-line">
									<div class="form-label" style="width:44px;text-align: left;margin-left:5px;">
										<fmt:message key="birthday" />:
									</div>
									<div class="form-input" style="width:10px;margin-left:1px;">
										<input autocomplete="off" type="text" id="vbirthdat" name="vbirthdat" style="width:144px;" class="Wdate text" value="${birthday }"/>
									</div>
									<div class="form-label" style="width:45px;text-align: left;margin-left:141px;">
										<fmt:message key="the_period_of_validity" /><fmt:message key="to2" />:
									</div>
									<div class="form-input" style="width:10px;">
										<input autocomplete="off" type="text" id="vidvaldat" name="vidvaldat" style="width:85px;" class="Wdate text" value="${employee.vidvaldat }"/>
									</div>
								</div>
							</div>
							<div style="border:solid #416AA3 2px;margin-top:3px;height:52px;margin-left:2px;margin-right:6px;">
								<div class="form-line">
									<div class="form-label" style="width:50px;">
										<span style="color:red;">*</span><fmt:message key="link_address" />:
									</div>
									<div class="form-input" style="width:10px;">
										<input type="text" id="vaddress" name="vaddress" class="text" style="margin-bottom:3px;width: 244px;" value="${employee.vaddress }"/>
									</div>
									<div class="form-label" style="width:22px;text-align:right;margin-left:244px;">
										<span style="color:red;">*</span><fmt:message key="Place_of_origin" />:
									</div>
									<div class="form-input" style="width:45px;margin-left:8px;">
										<select id="pk_home" name="pk_home" style="margin-top: 3px;width: 102px;height: 20px;" class="text">
											<c:forEach var="school" items="${vhome }" >
												<option value="${school.pk_datatyp }" <c:if test="${employee.pk_home == school.pk_datatyp }">selected="selected"</c:if>>${school.vname }</option>
											</c:forEach>
										</select>
									</div>
									<div class="form-label" style="width:30px;margin-left:55px;">
										<fmt:message key="zip_code" />:
									</div>
									<div class="form-input" style="width:25px;margin-left:2px;">
										<input type="text" id="vpostcode" name="vpostcode" class="text" style="margin-bottom:3px;width: 100px;" value="${employee.vpostcode }"/>
									</div>
								</div>
								<div class="form-line">
									<div class="form-label" style="width:24px;margin-left:23px;">
										<fmt:message key="tel" />1:
									</div>
									<div class="form-input" style="width:15px;">
										<input type="text" id="vtele1" name="vtele1" class="text" style="margin-bottom:3px;width: 90px;margin-left:3px;" value="${employee.vtele1 }"/>
									</div>
									<div class="form-label" style="width:30px;margin-left:90px;">
										<fmt:message key="tel" />2:
									</div>
									<div class="form-input" style="width:30px;">
										<input type="text" id="vtele2" name="vtele2" class="text" style="margin-bottom:3px;width: 100px;" value="${employee.vtele2 }"/>
									</div>
									<div class="form-label" style="width:30px;margin-left:80px;">
										<span style="color:red;">*</span><fmt:message key="tele" />:
									</div>
									<div class="form-input" style="width:25px;">
										<input type="text" id="vmobil" name="vmobil" class="text" style="margin-bottom:3px;width: 100px;" value="${employee.vmobil }"/>
									</div>
								</div>
							</div>
							<div style="border:solid #416AA3 2px;margin-top:3px;margin-bottom:3px;height:175px;margin-left:2px;margin-right:6px;">
								<div class="form-line">
									<div class="form-label" style="width:44px;margin-left:6px;">
										<fmt:message key="their_departments" />:
									</div>
									<div class="form-input" style="width:45px;">
										<input type="hidden" id="pk_hrdept" name="pk_hrdept" value="${employee.pk_hrdept }" />
										<input type="text" id="vdeptdes" name="vdeptdes" readonly="readonly" class="text" style="margin-bottom:3px;width: 75px;" value="${employee.vdeptdes }"/>
										<input type="text" id="vdeptcode" name="vdeptcode" class="text" readonly="readonly" style="margin-bottom:3px;width:45px;" value="${employee.vdeptcode }"/>
										<img id="selectHrDept" class="search" src="<%=path%>/image/themes/icons/search.png" />
									</div>
									<div class="form-label" style="width:50px;text-align: left;margin-left:140px;">
										<span style="color:red;">*</span><fmt:message key="entry" /><fmt:message key="date" />:
									</div>
									<div class="form-input" style="width:10px;margin-left:2px;">
										<input autocomplete="off" type="text" id="vjrdat" name="vjrdat" style="width:85px;" class="Wdate text" value="${employee.vjrdat }"/>
									</div>
									<div class="form-label" style="width:50px;text-align: left;margin-left:115px;">
										<span><fmt:message key="positive" /><fmt:message key="date" /></span>:
									</div>
									<div class="form-input" style="width:10px;margin-left:4px;">
										<input autocomplete="off" type="text" id="vwkdat" name="vwkdat" style="width:99px;" class="Wdate text" value="${employee.vwkdat }"/>
									</div>
								</div>
								<div class="form-line">
									<div class="form-label" style="width:44px;margin-left:6px;">
										<fmt:message key="Post" /><fmt:message key="hrLevel" />:
									</div>
									<div class="form-input" style="width:45px;">
										<select id="pk_dutylvl" name="pk_dutylvl" class="text" style="margin-top: 3px;width:128px;height: 20px;">
											<c:forEach var="school" items="${vdutylvl }" >
												<option value="${school.pk_datatyp }" <c:if test="${employee.pk_dutylvl == school.pk_datatyp }">selected="selected"</c:if>>${school.vname }</option>
											</c:forEach>
										</select>
									</div>
									<div class="form-label" style="width:45px;text-align: left;margin-left:145px;">
										<fmt:message key="Since_the_contract_period" />:
									</div>
									<div class="form-input" style="width:10px;margin-left:2px;">
										<input autocomplete="off" type="text" id="vpackfdat" name="vpackfdat" style="width:85px;" class="Wdate text" value="${employee.vpackfdat }"/>
									</div>
									<div class="form-label" style="width:50px;text-align: left;margin-left:115px;">
										<span><fmt:message key="The_contract_period_to" /></span>:
									</div>
									<div class="form-input" style="width:10px;margin-left:4px;">
										<input autocomplete="off" type="text" id="vpacktdat" name="vpacktdat" style="width:99px;" class="Wdate text" value="${employee.vpacktdat }"/>
									</div>
								</div>
								<div class="form-line">
									<div class="form-label" style="width:44px;margin-left:6px;">
										<fmt:message key="office" /><fmt:message key="Post" />:
									</div>
									<div class="form-input" style="width:45px;">
										<select id="pk_station" name="pk_station" class="text" style="margin-top: 3px;width:128px;height: 20px;">
											<c:forEach var="school" items="${vstation }" >
												<option value="${school.pk_datatyp }" <c:if test="${employee.pk_station == school.pk_datatyp }">selected="selected"</c:if>>${school.vname }</option>
											</c:forEach>
										</select>
									</div>
									<div class="form-label" style="width:45px;text-align: left;margin-left:145px;">
										<fmt:message key="Contract" /><fmt:message key="The_first_sign" />:
									</div>
									<div class="form-input" style="width:10px;margin-left:2px;">
										<input autocomplete="off" type="text" id="vfirstpackdat" name="vfirstpackdat" style="width:85px;" class="Wdate text" value="${employee.vfirstpackdat }"/>
									</div>
								</div>
								<div class="form-line">
									<div class="form-label" style="width:50px;">
										<span style="color:red;">*</span><fmt:message key="Wage" /><fmt:message key="hrLevel" />:
									</div>
									<div class="form-input" style="width:45px;">
										<select id="pk_dutyid" name="pk_dutyid" class="text" style="margin-top: 3px;width:128px;height: 20px;">
											<c:forEach var="school" items="${vdutyid }" >
												<option value="${school.pk_datatyp }" <c:if test="${employee.pk_dutyid == school.pk_datatyp }">selected="selected"</c:if>>${school.vname }</option>
											</c:forEach>
										</select>
									</div>
									<div class="form-label" style="width:50px;text-align: left;margin-left:140px;">
										<span style="color:red;">*</span><fmt:message key="Wage" /><fmt:message key="smc_standard" />:
									</div>
									<div class="form-input" style="width:10px;margin-left:2px;">
										<input type="text" id="vwage" name="vwage" style="width:85px;" onkeyup="value=value.replace(/[^\d]/g,'') " class="text" value="${employee.vwage }"/>
									</div>
									<div class="form-label" style="width:50px;text-align: left;margin-left:110px;">
										<span style="color:red;">*</span><fmt:message key="baseic" /><fmt:message key="Wage" />:
									</div>
									<div class="form-input" style="width:10px;margin-left:9px;">
										<input type="text" id="ngzwage" name="ngzwage" style="width:99px;" onkeyup="value=value.replace(/[^\d]/g,'') " class="text" value="${employee.ngzwage }"/>
									</div>
								</div>
								<div class="form-line">
									<div class="form-label" style="width:50px;">
										<span style="color:red;">*</span><fmt:message key="tax_rate" /><fmt:message key="category" />:
									</div>
									<div class="form-input" style="width:45px;">
										<select id="pk_taxno" name="pk_taxno" class="text" style="margin-top: 3px;width:128px;height: 20px;">
											<c:forEach var="school" items="${vtaxno }" >
												<option value="${school.pk_datatyp }" <c:if test="${employee.pk_taxno == school.pk_datatyp }">selected="selected"</c:if>>${school.vname }</option>
											</c:forEach>
										</select>
									</div>
									<div class="form-label" style="width:65px;text-align: right;margin-left:180px;">
										<input type="checkbox" id="vytax" name="vytax" style="width:20px;" class="text" value="${employee.vytax }"/>
										<fmt:message key="Tax" /><fmt:message key="scm_lock" />
									</div>
									<div class="form-label" style="width:50px;text-align: left;margin-left:55px;">
										<fmt:message key="Tax" /><fmt:message key="scm_lock" /><fmt:message key="amount" />:
									</div>
									<div class="form-input" style="width:10px;margin-left:27px;">
										<input type="text" id="ntaxamt" name="ntaxamt" style="width:99px;" class="text" value="${employee.ntaxamt }"/>
									</div>
								</div>
								<div class="form-line">
									<div class="form-label" style="width:50px;margin-left:35px;">
										<input type="checkbox" id="vhtjsgl" name="vhtjsgl" style="width:20px;" class="text" <c:if test="${employee.vhtjsgl == 'on' }">checked="checked"</c:if> />
										<fmt:message key="whether" /><fmt:message key="Contract" /><fmt:message key="calculate" /><fmt:message key="Length_of_service" />
									</div>
									<div class="form-label" style="width:20px;text-align: right;margin-left:185px;">
										<fmt:message key="Length_of_service" />:
									</div>
									<div class="form-input" style="width:10px;margin-left:4px;">
										<input type="text" id="vgl" name="vgl" style="width:85px;" class="text" value="${employee.vgl }"/>
									</div>
									<div class="form-label" style="width:45px;text-align: left;margin-left:115px;">
										<fmt:message key="Seniority_allowance" />:
									</div>
									<div class="form-input" style="width:10px;">
										<input type="text" id="vglbt" name="vglbt" style="width:99px;margin-left:8px;" class="text" value="${employee.vglbt }"/>
									</div>
								</div>
								<div class="form-line">
									<div class="form-label" style="width:30px;margin-left:35px;">
										<input type="checkbox" id="vstopwage" name="vstopwage" style="width:20px;margin-top:2px;" class="text" <c:if test="${employee.vstopwage == 'on' }">checked="checked"</c:if> />
										<fmt:message key="Stop" /><fmt:message key="Wage" />
									</div>
									<div class="form-label" style="width:50px;margin-left:213px;">
										<input type="checkbox" id="vyblack" name="vyblack" style="width:20px;" class="text" <c:if test="${employee.vyblack == 'on' }">checked="checked"</c:if>/>
										<fmt:message key="Black_list" />
									</div>
								</div>
							</div>
						</div>
<%-- 						<div title='<fmt:message key="Clothing" />' > --%>
<!-- 							<input type="hidden" id="flag" value="clothing" />			 -->
<!-- 						</div> -->
						<div title="<fmt:message key="Contract" />" >
							<input type="hidden" id="flag" value="contract" />	
							<div class="grid2">
								<div class="table-head1" style="width:800px;background: url('../image/Button_new/table_head2.png') repeat-x center center;">
										<table cellspacing="0" cellpadding="0">
											<thead>
												<tr>
													<td><span style="width:30px;text-align: center;"><fmt:message key="the_serial_number" /></span></td>
													<td><span style="width:70px;text-align: center;"><fmt:message key="hr_Contract_begins" /></span></td>
													<td><span style="width:70px;text-align: center;"><fmt:message key="hr_The_contract_ended_in" /></span></td>
													<td><span style="width:70px;text-align: center;"><fmt:message key="Contract" /><fmt:message key="status" /></span></td>
													<td><span style="width:40px;text-align: center;"><fmt:message key="effective" /></span></td>
													<td><span style="width:50px;text-align: center;"><fmt:message key="hr_Time_limit" />(<fmt:message key="month" />)</span></td>
													<td><span style="width:90px;text-align: center;"><fmt:message key="The_contract_type" /></span></td>
													<td><span style="width:50px;text-align: center;"><fmt:message key="hr_Liquidated_damages" /></span></td>
													<td><span style="width:90px;text-align: center;"><fmt:message key="hr_Contract_execution_date" /></span></td>
													<td><span style="width:125px;text-align: center;"><fmt:message key="remark" /></span></td>
												</tr>
											</thead>
										</table>
									</div>
									<div class="table-body1" style="width:800px;">
										<table cellspacing="0" cellpadding="0">
											<tbody>
												<c:forEach var="cr" items="${listContract}" varStatus="status">
													<tr>
														<td><span style="width:30px;text-align: right;">${status.index+1}</span></td>
														<td><span style="width:70px;"><c:out value="${cr.VBEINDATE}" /></span></td>
														<td><span style="width:70px;"><c:out value="${cr.VENDDATE}" /></span></td>
														<td><span style="width:70px;"><c:out value="${cr.STATUS}" /></span></td>
														<td><span style="width:40px;">
															<c:if test="${cr.VEFFECTIVE==1}">执行</c:if>
															<c:if test="${cr.VEFFECTIVE==0}">过期</c:if>
															<c:if test="${cr.VEFFECTIVE!=1 && cr.VEFFECTIVE!=0}">未执行</c:if>
															</span>
														</td>
														<td><span style="width:50px;text-align: right;">
															<c:forEach var="vtrial" items="${vtrial}" varStatus="status">
																<c:if test="${vtrial.pk_datatyp == cr.VTIMELIMIT}">${vtrial.vname}</c:if>
															</c:forEach>
															</span>
														</td>
														<td><span style="width:90px;">${cr.VTYPDESC}</span></td>
														<td><span style="width:50px;text-align: right;"><c:out value="${cr.DBREACHCONTRACT}" /></span></td>
														<td><span style="width:90px;"><c:out value="${cr.VEXECUTIVEDATE}" /></span></td>
														<td><span style="width:125px;" title='${cr.VMEMO}'><c:out value="${cr.VMEMO}" /></span></td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>			
						</div>
<%-- 						<div title='<fmt:message key="License" />' > --%>
<!-- 							<input type="hidden" id="flag" value="license" />			 -->
<!-- 						</div> -->
<%-- 						<div title='<fmt:message key="Extended" />' > --%>
<!-- 							<input type="hidden" id="flag" value="extended" />			 -->
<!-- 						</div> -->
<%-- 						<div title='<fmt:message key="Custom" />' > --%>
<!-- 							<input type="hidden" id="flag" value="custom" />			 -->
<!-- 						</div> -->
						<div title='<fmt:message key="family" />'>
							<input type="hidden" id="flag" value="family" />
							<div class="grid1" style="width: 850px;">	
								<div class="table-head" style="width: 840px;">
									<table cellspacing="0" cellpadding="0" style="width: 840px;">
										<thead>
											<tr>
												<td class="num" style="width:22px;"></td>
												<td style="width:30px;">
													<input type="checkbox" id="chkAll"/>
												</td>
												<td><span style="width:50px;"><fmt:message key="hrMembers" /></span></td>
												<td><span style="width:40px;"><fmt:message key="Relationship_between" /></span></td>
												<td><span style="width:30px;"><fmt:message key="sex" /></span></td>
												<td><span style="width:70px;"><fmt:message key="birthday" /></span></td>
												<td><span style="width:50px;"><fmt:message key="hrpost" /></span></td>
												<td><span style="width:50px;"><fmt:message key="political_landscape" /></span></td>
												<td><span style="width:100px;"><fmt:message key="hrworkunit" /></span></td>
												<td><span style="width:90px;"><fmt:message key="phone" /></span></td>
												<td><span style="width:200px;"><fmt:message key="link_address" /></span></td>
											</tr>
										</thead>
									</table>
								</div>
								<div class="table-body" style="width: 840px;">
									<table cellspacing="0" cellpadding="0" style="width: 840px;">
										<tbody>
											<c:forEach var="family" items="${listFamily}" varStatus="status">
												<tr>
													<td class="num" style="width:22px;">${status.index+1}</td>
													<td style="width:30px; text-align: center;">
														<input type="checkbox" name="idList" id="chk_<c:out value='${family.pk_family}' />" value="<c:out value='${family.pk_family}' />"/>
													</td>
													<td><span style="width:50px;text-align: left;"><c:out value="${family.vmembers}" /></span></td>
													<td><span style="width:40px;text-align: left;"><c:out value="${family.vrelationship}" /></span></td>
													<td><span style="width:30px;">${family.vsex}</span></td>
													<td><span style="width:70px;text-align: left;">${family.vbirthday}</span></td>
													<td><span style="width:50px;"><c:out value="${family.vpost}" /></span></td>
													<td><span style="width:50px;"><c:out value="${family.vpubliclist}" /></span></td>
													<td title='${family.vworkunit}'><span style="width:100px;"><c:out value="${family.vworkunit}" /></span></td>
													<td><span style="width:90px;text-align: left;"><c:out value="${family.vphone}" /></span></td>
													<td title='${family.vaddress}'><span style="width:200px;"><c:out value="${family.vaddress}" /></span></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>		
						</div>
<%-- 						<div title='<fmt:message key="Experience" />' > --%>
<!-- 							<input type="hidden" id="flag" value="experience" />			 -->
<!-- 						</div> -->
<%-- 						<div title='<fmt:message key="Transaction" />' > --%>
<!-- 							<input type="hidden" id="flag" value="transaction" />			 -->
<!-- 						</div> -->
<%-- 						<div title='<fmt:message key="Reward_and_punishment" />' > --%>
<!-- 							<input type="hidden" id="flag" value="reward" />			 -->
<!-- 						</div> -->
<%-- 						<div title='<fmt:message key="Vacation" />'> --%>
<!-- 							<input type="hidden" id="flag" value="vacation" />			 -->
<!-- 						</div> -->
<%-- 						<div title='<fmt:message key="insurance" />' > --%>
<!-- 							<input type="hidden" id="flag" value="insurance" />			 -->
<!-- 						</div> -->
<%-- 						<div title='<fmt:message key="bank" /><fmt:message key="Card_number" />' > --%>
<!-- 							<input type="hidden" id="flag" value="bank" />			 -->
<!-- 						</div> -->
						<div title='<fmt:message key="Keep_on_record" />'>
							<input type="hidden" id="flag" value="record" />
							<div class="form-line" style="height: 400px;">
								<div class="form-label" style="width:50px;margin-left:40px;float: inherit;margin-top:10px;">
									<input type="checkbox" id="vbmxy" name="vbmxy" style="width:15px;" class="text" <c:if test="${employee.vbmxy == 'on' }">checked="checked"</c:if>/>
									<fmt:message key="Confidentiality_agreement" />
									<input type="checkbox" id="vypdjb" name="vypdjb" style="width:15px;margin-left:20px;" class="text" <c:if test="${employee.vypdjb == 'on' }">checked="checked"</c:if>/>
									<fmt:message key="Apply_for" />
									<input type="checkbox" id="vcphoto" name="vcphoto" style="width:15px;margin-top:2px;margin-left:20px;" class="text" <c:if test="${employee.vcphoto == 'on' }">checked="checked"</c:if> />
									<fmt:message key="Inch_photo" />
									<input type="checkbox" id="vcardfj" name="vcardfj" style="width:15px;margin-left:20px;" class="text" <c:if test="${employee.vcardfj == 'on' }">checked="checked"</c:if>/>
									<fmt:message key="hrCopy" />
									<input type="checkbox" id="vdbs" name="vdbs" style="width:15px;margin-left:20px;" class="text" <c:if test="${employee.vdbs == 'on' }">checked="checked"</c:if>/>
									<fmt:message key="Guarantee" />
								</div>
								<div class="form-label" style="width:50px;margin-left:5px;margin-top:10px;">
									<fmt:message key="remark" />:
								</div>
								<div class="form-input" align="center">
									<textarea wrap="physical" id="vmemo" name="vmemo" style="width:520px;text-align: left;height:300px;margin-top:15px;">${employee.vmemo}</textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				if('${tabs_flag}'=='family'){ //如果是家庭就返回家庭标签
					$('#tabs').tabs('select', '<fmt:message key="family" />');
				}
				<%
					session.removeAttribute("tabs_flag");
				%>
				
				
				// 自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),51);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				
				// 自动实现滚动条
				setElementHeight('.grid1',['.tool'],$(document.body),101);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid1');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				
				// 自动实现滚动条
				setElementHeight('.grid2',['.tool'],$(document.body),101);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid2');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				
				
				//为出生日期、身份证有效期至赋值
				$("#vidvaldat,#vpackfdat,#vpacktdat,#vfirstpackdat,#vpacknote").htmlUtils("setDate","now");
				$("#vidvaldat").click(function(){
		  			new WdatePicker();
		  		});
				$("#vbirthdat").click(function(){
		  			new WdatePicker();
		  		});
				$("#vjrdat").click(function(){
		  			new WdatePicker();
		  		});
				$("#vwkdat").click(function(){
		  			new WdatePicker();
		  		});
				
				//合同期自
				$("#vpackfdat").click(function(){
		  			new WdatePicker();
		  		});
				
				//合同期至
				$("#vpacktdat").click(function(){
		  			new WdatePicker();
		  		});
				
				//合同首签
				$("#vfirstpackdat").click(function(){
		  			new WdatePicker();
		  		});
				
				//合同续签通知日
				$("#vpacknote").click(function(){
		  			new WdatePicker();
		  		});
				
				//民族
				$("#searchNation").click(function(){
					var pk_store = $("#pk_store").val();
					var url = "<%=path%>/staffList/toChooseNation.do";
						url += "?callBack=setNation&domId=selected&single=true&pk_store="+pk_store;
					return $('body').window({
						id: 'window_chooseSiteType',
						title: '<fmt:message key="select1" /><fmt:message key="sector" />',
						content: '<iframe id="listSiteTypeFrame" frameborder="0" src='+url+'></iframe>',
						width: 400,
						height: 350,
						confirmClose: false,
						draggable: true,
						isModal: true
					});
				});
				
				//部门
				$("#selectHrDept").click(function(){
					var pk_store = $("#pk_store").val();
					var url = "<%=path%>/organizationStructure/toChooseHrDept.do";
						url += "?callBack=setHrDept&domId=selected&single=true&pk_store="+pk_store;
					return $('body').window({
						id: 'window_chooseSiteType',
						title: '<fmt:message key="select1" /><fmt:message key="sector" />',
						content: '<iframe id="listSiteTypeFrame" frameborder="0" src='+url+'></iframe>',
						width: 400,
						height: 350,
						confirmClose: false,
						draggable: true,
						isModal: true
					});
				});
				
				
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'vname',
						validateType:['canNull','maxLength','withOutSpecialChar'],
						param:['F',50,'F'],
						error:['<fmt:message key="name" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="the_maximum_length" />50','<fmt:message key="no_contain_special_char" />！']
					},{
						type:'text',
						validateObj:'vinit',
						validateType:['canNull','maxLength'],
						param:['F',50],
						error:['<fmt:message key="abbreviation" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="the_maximum_length" />50']
					},{
						type:'text',
						validateObj:'vage',
						validateType:['canNull','intege2','maxValue'],
						param:['F','F','999999'],
						error:['<fmt:message key="age" /><fmt:message key="cannot_be_empty" />','<fmt:message key="" />！','<fmt:message key="most_can_more_than" />999999']
					},{
						type:'text',
						validateObj:'vidcard',
						validateType:['canNull','idCard'],
						param:['F','F'],
						error:['<fmt:message key="ID_number" /><fmt:message key="cannot_be_empty" />','<fmt:message key="ID_number" /><fmt:message key="incorrect_format" />！']
					},{
						type:'text',
						validateObj:'vkqcardno',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="Attendance_No" /><fmt:message key="cannot_be_empty" />']
					},{
						type:'text',
						validateObj:'vworktyp',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="The_form_of_employment" /><fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'vpubliclist',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="political_landscape" /><fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'vnation',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="the_national" /><fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'vhouse',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="Registered_residence" /><fmt:message key="type" /><fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'vschoolage',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="record_of_formal_schooling" /><fmt:message key="type" /><fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'vmobil',
						validateType:['canNull','mobile'],
						param:['F','F'],
						error:['<fmt:message key="tele" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="tele" /><fmt:message key="incorrect_format" />！']
					},{
						type:'text',
						validateObj:'vtele1',
						validateType:['telephone'],
						param:['T'],
						error:['<fmt:message key="telephone_format_is_not_correct" />！']
					},{
						type:'text',
						validateObj:'vtele2',
						validateType:['telephone'],
						param:['T'],
						error:['<fmt:message key="telephone_format_is_not_correct" />！']
					},{
						type:'text',
						validateObj:'vaddress',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="link_address" /><fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'vjrdat',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="entry" /><fmt:message key="date" /><fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'vdutyid',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="Wage" /><fmt:message key="hrLevel" /><fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'vtaxno',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="tax_rate" /><fmt:message key="hrLevel" /><fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'ngzwage',
						validateType:['canNull','decmal'],
						param:['F','F'],
						error:['<fmt:message key="baseic" /><fmt:message key="Wage" /><fmt:message key="cannot_be_empty" />','<fmt:message key="" />！']
					},{
						type:'text',
						validateObj:'vwage',
						validateType:['canNull','decmal'],
						param:['F','F'],
						error:['<fmt:message key="Wage" /><fmt:message key="smc_standard" /><fmt:message key="cannot_be_empty" />','<fmt:message key="" />！']
					},{
						type:'text',
						validateObj:'vmemo',
						validateType:['maxLength'],
						param:[1000],
						error:['<fmt:message key="the_maximum_length" />1000']
					},{
						type:'text',
						validateObj:'vwkdat',
						validateType:['handler'],
						handler:function(){
							var vjrdat = $("#vjrdat").val();
							var vwkdat = $("#vwkdat").val();
							if (vjrdat > vwkdat) {
								alert('<fmt:message key="entry" /><fmt:message key="date" /><fmt:message key="can_not_be_greater_than" /><fmt:message key="positive" /><fmt:message key="date" />！');
							}
							return true;
						}
					},{
						type:'text',
						validateObj:'vpackfdat',
						validateType:['handler'],
						handler:function(){
							var vpackfdat = $("#vpackfdat").val();
							var vpacktdat = $("#vpacktdat").val();
							if (vpackfdat > vpacktdat) {
								alert('<fmt:message key="Since_the_contract_period" /><fmt:message key="can_not_be_greater_than" /><fmt:message key="The_contract_period_to" />！');
							}
							return true;
						}
					}]
				});
				
				//双击弹出<fmt:message key="update" />
				$('.grid1').find('.table-body').find('tr').live("dblclick", function () {
					$(":checkbox").attr("checked", false);
					$(this).find(':checkbox').attr("checked", true);
					update();
					if($(this).hasClass("show-firm-row"))return;
					$('.grid1').find('.table-body').find('tr').removeClass("show-firm-row");
					$(this).addClass("show-firm-row");
				 });
			});
			
			function setHrDept(data){
	  	 		$("#pk_hrdept").val(data.code);
	  	 		$("#vdeptcode").val(data.show);
	  	 		$("#vdeptdes").val(data.name);
	  	 	}
			
			function setNation(data){
	  	 		$("#pk_nation").val(data.code);
	  	 		$("#vnation").val(data.name);
	  	 	}
			
			//新增数据
			function add(){
				var tab = $("#tabs").tabs("getSelected");
				var flag = tab.find("#flag").val();
				var pk_employee = $("#pk_employee").val();
				switch(flag){
				case "family": 
					$('body').window({
						id: 'window_saveInceom',
						title: '<fmt:message key="insert" /><fmt:message key="family" /><fmt:message key="hrMembers" />',
						content: '<iframe id="SaveForm" frameborder="0" src="<%=path%>/staffList/addData.do?type=family&flag=add&pk_employee='+pk_employee+'"></iframe>',
						width: '600px',
						height: '400px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save_role_of_information"/>',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('SaveForm')&&window.document.getElementById("SaveForm").contentWindow.validate._submitValidate()){
											document.getElementById("SaveForm").contentWindow.document.forms["familySaveForm"].submit();
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
					break;
				}
			}
			
			//修改
			function update(){
				var tab = $("#tabs").tabs("getSelected");
				var flag = tab.find("#flag").val();
				switch(flag){
				case "family":
					var checkboxList = $('.grid1').find('.table-body').find(':checkbox');
					if(checkboxList && checkboxList.filter(':checked').size() ==1){
						var action='<%=path%>/staffList/addData.do?type=family&flag=update&pk_employee='+checkboxList.filter(':checked').val();
						$('body').window({
							id: 'window_updateemployee',
							title: '<fmt:message key="update" /><fmt:message key="family" /><fmt:message key="hrMembers" />',
							content: '<iframe id="UpdateForm" frameborder="0" src='+action+'></iframe>',
							width: '600px',
							height: '400px',
							draggable: true,
							isModal: true,
							topBar: {
								items: [{
										text: '<fmt:message key="save" />',
										title: '<fmt:message key="data" /><fmt:message key="update" />',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-80px','-0px']
										},
										handler: function(){
											if(getFrame('UpdateForm')&&window.document.getElementById("UpdateForm").contentWindow.validate._submitValidate()){
												document.getElementById("UpdateForm").contentWindow.document.forms["familySaveForm"].submit();
											}
										}
									},{
										text: '<fmt:message key="cancel" />',
										title: '<fmt:message key="cancel" />',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-160px','-100px']
										},
										handler: function(){
											$('.close').click();
										}
									}
								]
							}
						});
					}else if(checkboxList && checkboxList.filter(':checked').size() > 1){
						alert('<fmt:message key="you_can_only_modify_a_data" />！');
						return;
					}else {
						alert('<fmt:message key="please_select_information_you_need_to_modify"/>！');
						return ;
					}
					break;
				}
			}
			
			//上传图片
			function uploadPicture(){
				var tab = $("#tabs").tabs("getSelected");
				var flag = tab.find("#flag").val();
				var pk_employee = $("#pk_employee").val();
				switch(flag){
				case "baseic": 
					$('body').window({
						id: 'window_saveInceom',
						title: '<fmt:message key="upload" /><fmt:message key="the_picture" />',
						content: '<iframe id="SaveForm" frameborder="0" src="<%=path%>/staffList/uploadPicture.do?pk_employee='+pk_employee+'"></iframe>',
						width: '600px',
						height: '400px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save_role_of_information"/>',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										$('.close').click();
										window.location.href="<%=path%>/staffList/findEmployeeByPk.do?pk_employee="+pk_employee;
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
					break;
				}
			}
			
			//重新计算图片高度，解决图片失真问题
			function reCalcPicSize(){
				var allPic = $('#imgDiv').find('img');
				$.each(allPic,function(i){
					//获取原图宽高
					var pic = new Image();
					pic.src = $(this).attr('src');
					var picWidth = pic.width;
					var picHeight = pic.height;
					//重新计算宽高
					var newPwidth = 80;
					var newPheight = 80;
					if(picWidth>picHeight){
						newPheight = picHeight*80/picWidth;
					}else{
						newPwidth = picWidth*80/picHeight;
					}
					//设置img宽高为计算出的宽高
					$(this).width(newPwidth);
					$(this).height(newPheight);
				});
			}
			
			//保存数据
			function save(){
				$("#employeeSaveForm").submit();
			}
		</script>
	</body>
</html>