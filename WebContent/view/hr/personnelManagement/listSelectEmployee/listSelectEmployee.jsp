<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="operating_employee" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/staffList/listEmployee.do" method="post">
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 33px;">&nbsp;</td>
								<td style="width:30px;"><input type="checkbox" id="chkAll"/></td>
								<td><span style="width:70px;"><fmt:message key="The_work_number" /></span></td>
								<td><span style="width:70px;"><fmt:message key="name" /></span></td>
								<td><span style="width:30px;"><fmt:message key="sex" /></span></td>
								<td><span style="width:30px;"><fmt:message key="age" /></span></td>
								<c:forEach var="columns" items="${listColumns }" varStatus="status">
									<c:if test="${sessionScope.locale == 'zh_CN'}">
										<c:if test="${columns.columnName == 'vdeptdes'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.zhColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vposition'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.zhColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vstatus'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.zhColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vbirthdat'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.zhColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vjrdat'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.zhColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vwkdat'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.zhColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vpacktdat'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.zhColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vschoolage'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.zhColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vidcard'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.zhColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vworktyp'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.zhColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vaddress'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.zhColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vinit'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.zhColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vmemo'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.zhColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vyfirm'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.zhColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vjkcardno'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.zhColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vinsurance'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.zhColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vinvalid'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.zhColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vdbs'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.zhColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vfzgw'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.zhColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vbxsta'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.zhColumnName}" /></span></td>
										</c:if>
									</c:if>
									<c:if test="${sessionScope.locale == 'zh_TW'}">
										<c:if test="${columns.columnName == 'vdeptdes'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.twColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vposition'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.twColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vstatus'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.twColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vbirthdat'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.twColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vjrdat'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.twColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vwkdat'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.twColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vpacktdat'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.twColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vschoolage'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.twColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vidcard'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.twColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vworktyp'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.twColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vaddress'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.twColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vinit'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.twColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vmemo'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.twColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vyfirm'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.twColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vjkcardno'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.twColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vinsurance'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.twColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vinvalid'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.twColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vdbs'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.twColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vfzgw'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.twColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vbxsta'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.twColumnName}" /></span></td>
										</c:if>
									</c:if>
									<c:if test="${sessionScope.locale == 'en'}">
										<c:if test="${columns.columnName == 'vdeptdes'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.enColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vposition'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.enColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vstatus'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.enColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vbirthdat'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.enColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vjrdat'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.enColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vwkdat'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.enColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vpacktdat'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.enColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vschoolage'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.enColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vidcard'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.enColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vworktyp'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.enColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vaddress'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.enColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vinit'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.enColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vmemo'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.enColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vyfirm'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.enColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vjkcardno'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.enColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vinsurance'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.enColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vinvalid'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.enColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vdbs'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.enColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vfzgw'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.enColumnName}" /></span></td>
										</c:if>
										<c:if test="${columns.columnName == 'vbxsta'}">
											<td><span style="width:${columns.columnWidth}px;"><c:out value="${columns.enColumnName}" /></span></td>
										</c:if>
									</c:if>
								</c:forEach>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="employee" items="${listEmployee}" varStatus="status">
								<tr>
									<td class="num" style="width:36px;">${status.index+1}</td>
									<td style="width:28px; text-align: center;">
										<input type="hidden" id="${employee.pk_employee}" name="vstatus" value="${employee.vstatus }" />
										<input type="checkbox" name="idList" id="chk_<c:out value='${employee.pk_employee}' />" 
											<c:if test="${'true' == employee.flag }">checked="checked"</c:if>
											   value="<c:out value='${employee.pk_employee}' />"/>
									</td>
									<td><span style="width:70px;text-align: left;"><c:out value="${employee.vempno}" /></span></td>
									<td><span style="width:70px;text-align: left;"><c:out value="${employee.vname}" /></span></td>
									<td><span style="width:30px;">${employee.vsex}</span></td>
									<td><span style="width:30px;text-align: right;">${employee.vage}</span></td>
								<c:forEach var="columns" items="${listColumns }" varStatus="status">
									<c:if test="${columns.columnName == 'vdeptdes'}">
										<td><span style="width:${columns.columnWidth}px;"><c:out value="${employee.vdeptdes}" /></span></td>
									</c:if>
									<c:if test="${columns.columnName == 'vposition'}">
										<td><span style="width:${columns.columnWidth}px;"><c:out value="${employee.vposition}" /></span></td>
									</c:if>
									<c:if test="${columns.columnName == 'vstatus'}">
										<td><span style="width:${columns.columnWidth}px;"><c:out value="${employee.vstatus}" /></span></td>
									</c:if>
									<c:if test="${columns.columnName == 'vbirthdat'}">
										<td><span style="width:${columns.columnWidth}px;"><c:out value="${employee.vbirthdat}" /></span></td>
									</c:if>
									<c:if test="${columns.columnName == 'vjrdat'}">
										<td><span style="width:${columns.columnWidth}px;"><c:out value="${employee.vjrdat}" /></span></td>
									</c:if>
									<c:if test="${columns.columnName == 'vwkdat'}">
										<td><span style="width:${columns.columnWidth}px;"><c:out value="${employee.vwkdat}" /></span></td>
									</c:if>
									<c:if test="${columns.columnName == 'vpacktdat'}">
										<td><span style="width:${columns.columnWidth}px;"><c:out value="${employee.vpacktdat}" /></span></td>
									</c:if>
									<c:if test="${columns.columnName == 'vschoolage'}">
										<td><span style="width:${columns.columnWidth}px;"><c:out value="${employee.vschoolage}" /></span></td>
									</c:if>
									<c:if test="${columns.columnName == 'vidcard'}">
										<td><span style="width:${columns.columnWidth}px;"><c:out value="${employee.vidcard}" /></span></td>
									</c:if>
									<c:if test="${columns.columnName == 'vworktyp'}">
										<td><span style="width:${columns.columnWidth}px;"><c:out value="${employee.vworktyp}" /></span></td>
									</c:if>
									<c:if test="${columns.columnName == 'vaddress'}">
										<td><span style="width:${columns.columnWidth}px;"><c:out value="${employee.vaddress}" /></span></td>
									</c:if>
									<c:if test="${columns.columnName == 'vinit'}">
										<td><span style="width:${columns.columnWidth}px;"><c:out value="${employee.vinit}" /></span></td>
									</c:if>
									<c:if test="${columns.columnName == 'vmemo'}">
										<td><span style="width:${columns.columnWidth}px;"><c:out value="${employee.vmemo}" /></span></td>
									</c:if>
									<c:if test="${columns.columnName == 'vyfirm'}">
										<td><span style="width:${columns.columnWidth}px;"><c:out value="${employee.vyfirm}" /></span></td>
									</c:if>
									<c:if test="${columns.columnName == 'vjkcardno'}">
										<td><span style="width:${columns.columnWidth}px;"><c:out value="${employee.vjkcardno}" /></span></td>
									</c:if>
									<c:if test="${columns.columnName == 'vinsurance'}">
										<td><span style="width:${columns.columnWidth}px;"><c:out value="${employee.vinsurance}" /></span></td>
									</c:if>
									<c:if test="${columns.columnName == 'vinvalid'}">
										<td><span style="width:${columns.columnWidth}px;"><c:out value="${employee.vinvalid}" /></span></td>
									</c:if>
									<c:if test="${columns.columnName == 'vdbs'}">
										<td><span style="width:${columns.columnWidth}px;"><c:out value="${employee.vdbs}" /></span></td>
									</c:if>
									<c:if test="${columns.columnName == 'vfzgw'}">
										<td><span style="width:${columns.columnWidth}px;"><c:out value="${employee.vfzgw}" /></span></td>
									</c:if>
									<c:if test="${columns.columnName == 'vbxsta'}">
										<td><span style="width:${columns.columnWidth}px;"><c:out value="${employee.vbxsta}" /></span></td>
									</c:if>
								</c:forEach>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<div class="search-div" style="margin-left: 0px;">
				<div class="search-condition">
					<table class="search-table" cellspacing="0" cellpadding="0" >
						<tr>
							<td class="c-left"><fmt:message key="coding" />：</td>
							<td><input type="text" id="vempno" name="vempno" class="text" style="width:100px;" onkeypress="IsNum(event)" /></td>
							<td class="c-left"><fmt:message key="name" />：</td>
							<td><input type="text" id="vname" name="vname"  class="text" style="width:100px;"/></td>
							<td class="c-left"><fmt:message key="abbreviation" />：</td>
							<td><input type="text" id="vinit" name="vinit" class="text" style="width:100px;"/></td>
							<td class="c-left"><fmt:message key="employees" /><fmt:message key="status" />：</td>
							<td>
								<select id="vstatus" name="vstatus" style="width:100px;height: 22px;margin-top: 3px; border: 1px solid #999999;">
									<option value="1"><fmt:message key="all" /></option>
									<option value="2"><fmt:message key="Applicable" /></option>
									<option value="3"><fmt:message key="work" /></option>
									<option value="4"><fmt:message key="departure" /></option>
									<option value="5"><fmt:message key="delete" /></option>
								</select>
							</td>
						</tr>
					</table>
				</div>
				<div class="search-commit">
		       		<input type="button" class="search-button" id="search" value='<fmt:message key="enter" />'/>
		       		<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				var selected;
				$(document).keydown(function(e){
					if(e.keyCode == 13) return false;
				});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-40px']
							},
							handler: function(){
								$('.search-div').slideToggle(100);
							}
						},{
							text: '<fmt:message key="column_selection" />',
							title: '<fmt:message key="column_selection" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-40px']
							},
							handler: function(){
								toColsChoose();
							}
						},"-",{
							text: '<fmt:message key="enter" />',
							useable:true,
							handler: function(){
								var checkboxList = $('.grid').find('.table-body').find(':checkbox');
								var data = {pk:[],code:[],name:[],mod:[],entity:[]};
								checkboxList.filter(':checked').each(function(){
									var row = $(this).closest('tr');
									data.pk.push($(this).val());
									data.code.push($.trim(row.children('td:eq(2)').text()));
									data.name.push($.trim(row.children('td:eq(3)').text()));
								});
								if("${commonMethod.domId}" == "selected"){
									parent.selected = data.code;
								}
								if (data.pk == '') {
									alert('<fmt:message key="please_select" /><fmt:message key="employees" />！');
									return true;
								}
								parent.parent[parent.$("#callBack").val()](data);
								$(".close",parent.parent.document).click();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$(".close",parent.parent.document).click();
// 								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}
					]
				});
				
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#listForm').submit();
				});
				/* 模糊<fmt:message key="select" />清空 */
				$("#resetSearch").bind('click', function() {
					$('.search-condition input').val('');
				});
				// 自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),1);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
			});
			
			//实现复选框单选
			function CheckBoxCheck(chk){
			    var obj = $('.grid').find('.table-body').find('input[name=idList]');
			    for(var i = 0; i < obj.length; i++){
			        if(obj[i].type == "checkbox"){
			            obj[i].checked = false;
			        }
			    }
			    chk.checked=true;
			}
			
			// 跳转到列选择页面
			function toColsChoose() {
				colChooseWindow = $('body').window({
					title : '<fmt:message key="column_selection" />',
					content : '<iframe frameborder="0" src="<%=path%>/staffList/toColumnsChoose.do?reportName=employee"></iframe>',
					width : '460px',
					height : '430px',
					draggable : true,
					isModal : true,
					confirmClose : false
				});
			}
		</script>
	</body>
</html>