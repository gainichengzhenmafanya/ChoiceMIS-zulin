<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="operating_hrDept" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>			
	</head>
	<body onload="buttonCancel()">
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/organizationStructure/hrDeptDetail.do" method="post">
			<div class="grid" >
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="coding" />:</div>
					<div  class="form-input">
						<input type="hidden" id="pk_hrdept" name="pk_hrdept" value="${listHrDept.pk_hrdept}"/>
						<input type="hidden" id="pk_store" name="pk_store" value="${pk_store}"/>
						<input type="hidden" id="iproportion" name="iproportion" value="0"/>
						<input type="hidden" id="istatistic" name="istatistic" value="1"/>
						<input type="text" id="vcode" name="vcode" class="text clearinput" readonly="readonly" value="${listHrDept.vcode}"/>
					</div>
					<div  class="form-label"><span class="red">*</span><fmt:message key="name" />:</div>
					<div class="form-input">
						<input type="text" id="vname" name="vname" class="text clearinput" readonly="readonly" value="${listHrDept.vname}" onkeyup="getSpInit(this,'vinit');"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="abbreviation" />:</div>
					<div class="form-input">
						<input type="text" id="vinit" name="vinit" class="text clearinput" readonly="readonly" value="${listHrDept.vinit}" />
					</div>
					<div  class="form-label"><fmt:message key="enable_state" />:</div>
					<div class="form-input">
						<select id="enablestate" name="enablestate" class="select" disabled="disabled" style="width:133px;">
							<option value="2" <c:if test="${listHrDept.enablestate==2}">selected="selected"</c:if>><fmt:message key="have_enabled" /></option>
							<option value="1" <c:if test="${listHrDept.enablestate==1}">selected="selected"</c:if>><fmt:message key="not_enabled" /></option>
							<option value="3" <c:if test="${listHrDept.enablestate==3}">selected="selected"</c:if>><fmt:message key="stop_enabled" /></option>
						</select>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="no" />:</div>
					<div class="form-input">
						<input type="text" id="isortno" name="isortno" readonly="readonly" class="text clearinput" value="${listHrDept.isortno}" />
					</div>
					<div  class="form-label"><fmt:message key="remark" />:</div>
					<div  class="form-input" >
						<input type="text" id="vmemo" name="vmemo" class="text clearinput" readonly="readonly" value="${listHrDept.vmemo}"/>
					</div>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
		<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),32);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();													//自动计算滚动条的js方法
				$('.<fmt:message key="save" />,.<fmt:message key="cancel" />').closest("li").hide();
				
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'vcode',
						validateType:['canNull','intege2','maxLength','withOutSpecialChar'],
						param:['F','F',10,'T'],
						error:['<fmt:message key="coding" /><fmt:message key="cannot_be_empty" />!','<fmt:message key="coding" /><fmt:message key="only_start_with_nozero" />!','<fmt:message key="the_maximum_length" />10','<fmt:message key="no_contain_special_char" />!']
					},{
						type:'text',
						validateObj:'vname',
						validateType:['canNull','maxLength','withOutSpecialChar'],
						param:['F',50,'T'],
						error:['<fmt:message key="name" /><fmt:message key="cannot_be_empty" />!','<fmt:message key="the_maximum_length" />50','<fmt:message key="no_contain_special_char" />!']
					},{
						type:'text',
						validateObj:'vinit',
						validateType:['canNull',,'maxLength','withOutSpecialChar'],
						param:['F',50,'T'],
						error:['<fmt:message key="abbreviation" /><fmt:message key="cannot_be_empty" />!','<fmt:message key="the_maximum_length" />50','<fmt:message key="no_contain_special_char" />!']
					},{
						type:'text',
						validateObj:'isortno',
						validateType:['canNull','intege2','maxValue'],
						param:['F','F','999999'],
						error:['<fmt:message key="no" /><fmt:message key="cannot_be_empty" />','<fmt:message key="enter_the_number_of_non_zero" /><fmt:message key="coding" />！','<fmt:message key="sort_column_is_too_large" />999999']
					},{
						type:'text',
						validateObj:'vmemo',
						validateType:['maxLength'],
						param:[150],
						error:['<fmt:message key="the_maximum_length" />200']
					}]
				});

				validate1 = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'vcode',
						validateType:['handler'],
						handler:function(){
							var result = true;
							var commonMethod = new CommonMethod();
							commonMethod.vcode = $("#vcode").val();
							commonMethod.vcodename = "vcode";
							commonMethod.vtablename = "CHR_HRDEPT_3CH";
							var returnvalue = validateVcode("<%=path %>",commonMethod);
							if (returnvalue == "1") {
								result = false;
							}
							return result;
						},
						param:['F'],
						error:['<fmt:message key="coding" /><fmt:message key="already_exists" />!']
					}]
				});
					
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="insert" />',
							title: '<fmt:message key="insert"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								inserthrDept();
							}
						},{
							text: '<fmt:message key="update" />',
							title: '<fmt:message key="update"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								updateHrDept();
							}
						},{
							text: '<fmt:message key="save" />',
							title: '<fmt:message key="save" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','0px']
							},
							handler: function(){
								saveHrDept();
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','0px']
							},
							handler: function(){
								cancleHrDept();
							}
						},{
							text: '<fmt:message key="enable" />',
							title: '<fmt:message key="enable" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','-40px']
							},
							handler: function(){
								enable();
							}
						},{
							text: '<fmt:message key="disable1" />',
							title: '<fmt:message key="disable1" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-40px']
							},
							handler: function(){
								disable();
							}
						},"-",{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}
					]
				});
			});
			
			//新增
			function inserthrDept(){
				var pk_store = $("#pk_store").val();
				var pk_hrdept = $("#pk_hrdept").val();
				if ('' == pk_store || '' != pk_hrdept) {
					alert("<fmt:message key="please_select"/><fmt:message key="store"/>！");
					return true;
				}
				$('.text').removeAttr("readonly");	//将输入框置为可输入状态
				$('.select').removeAttr("disabled");
				$('#listForm input[type=text]').val('');
				getDefaultVcode("<%=path%>","CHR_HRDEPT_3CH","vcode","vcode","1");
				getDefaultVcode("<%=path%>","CHR_HRDEPT_3CH","isortno","isortno","1");
				$('.<fmt:message key="insert" />,.<fmt:message key="update" />,.<fmt:message key="enable" />,.<fmt:message key="disable1" />').closest("li").hide();
				$('.<fmt:message key="save" />,.<fmt:message key="cancel" />').closest("li").show();
			}
			
			//修改数据
			function updateHrDept(){
				var pk_hrdept = $('#pk_hrdept').val();
				var vcode = $("#vcode").val();
				if("" != pk_hrdept & "" != vcode){
					$('.<fmt:message key="insert" />,.<fmt:message key="enable" />,.<fmt:message key="disable1" />,.<fmt:message key="update" />').closest("li").hide();
					$('.<fmt:message key="save" />,.<fmt:message key="cancel" />').closest("li").show();
					$('.text').removeAttr("readonly");	//将输入框置为可输入状态
					$('.select').removeAttr("disabled");
					$("#vcode").attr("readonly","readonly");
				}else {
					alert('<fmt:message key="please_select_information_you_need_to_modify"/>！');
					return ;
				}
			}
				
			//保存新增的数据
			function saveHrDept(){
				var pk_hrdept = $('#pk_hrdept').val();
				var flag;
				if("" == pk_hrdept){
					flag = validate._submitValidate()&&validate1._submitValidate();
				}else{
					flag = validate._submitValidate();
				}
				if(flag){
					$.ajax({
				    	type: "POST",
				        url: "<%=path%>/organizationStructure/saveHrDept.do",
				        data: getParam($('#listForm')),
				        success: function (data) {
				        	if(data=="0"){
					        	showMessage({
								type: 'error',
								msg: '<fmt:message key="data" /><fmt:message key="save" /><fmt:message key="failure" />!',
								speed: 1500
								});
				            }else if(data.length=20){
					            parent.showMessage({
									type: 'success',
									msg: '<fmt:message key="data" /><fmt:message key="save" /><fmt:message key="successful" />!',
									speed: 1500,
									handler:function(){
										parent.refreshTree();
									}
								});
								$('.<fmt:message key="insert" />,.<fmt:message key="update" />,.<fmt:message key="enable" />,.<fmt:message key="disable1" />').closest("li").show();
								$('.<fmt:message key="save" />,.<fmt:message key="cancel" />').closest("li").hide();
								cancleHrDept();
				            }
				        },
				        error: function (msg) {
				        	showMessage({
								type: 'error',
								msg: '<fmt:message key="data" /><fmt:message key="save" /><fmt:message key="failure" />!',
								speed: 1500
							});
				        }
					});
				}
			}
			
			function buttonCancel(){
				$('.<fmt:message key="insert" />,.<fmt:message key="update" />,.<fmt:message key="enable" />,.<fmt:message key="disable1" />').closest("li").show();
				$('.<fmt:message key="save" />,.<fmt:message key="cancel" />').closest("li").hide();
				$('.text').attr("readonly","readonly");	//将输入框置为不可输入状态
				$('#listForm select').attr("disabled","disabled");
			}
			
			//取消按钮
			function cancleHrDept(){
				$('.<fmt:message key="insert" />,.<fmt:message key="update" />,.<fmt:message key="enable" />,.<fmt:message key="disable1" />').closest("li").show();
				$('.<fmt:message key="save" />,.<fmt:message key="cancel" />').closest("li").hide();
				$('.text').attr("readonly","readonly");	//将输入框置为不可输入状态
				$('#listForm select').attr("disabled","disabled");
				$('#listForm input[type=text]').val('');
			}
			
			//启用
			function enable(){
				var pk_hrdept = $('#pk_hrdept').val();
				if ('' != pk_hrdept) {
					parent.$('body').window({
						title: '<fmt:message key="enable" />',
						content: '<iframe id="updateButtontypeFrame" frameborder="0" src="<%=path%>/organizationStructure/enableOrDisable.do?enablestate=2&pk_hrdept='+$("#pk_hrdept").val()+'"></iframe>',
						width: '300px',
						height: '200px',
						draggable: true,
						isModal: true
					});
				} else {
					alert('<fmt:message key="no_data" /><fmt:message key="can_not" /><fmt:message key="enable" />！');
				}
			}
			
			//停用
			function disable(){
				var pk_hrdept = $('#pk_hrdept').val();
				if ('' != pk_hrdept) {
					parent.$('body').window({
						title: '<fmt:message key="disable1" />',
						content: '<iframe id="updateButtontypeFrame" frameborder="0" src="<%=path%>/organizationStructure/enableOrDisable.do?enablestate=3&pk_hrdept='+$("#pk_hrdept").val()+'"></iframe>',
						width: '300px',
						height: '200px',
						draggable: true,
						isModal: true
					});
				} else {
					alert('<fmt:message key="no_data" /><fmt:message key="can_not" /><fmt:message key="disable1" />！');
				}
			}
		</script>
	</body>
</html>