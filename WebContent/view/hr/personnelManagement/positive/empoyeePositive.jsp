<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="branches_and_positions_information" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<style type="text/css">
			.form {
				position: relative;
				width: 100%;
			}
		</style>
	</head>
	<body>
		<div class="form">
			<form id="listForm" method="post" action="<%=path %>/staffList/employeePositive.do">
				<div class="form-line">
					<div class="form-label">
						<span style="color: blue;font-size: 18px;margin-left:50px;">【<fmt:message key="archives" /><fmt:message key="maintenance" />】</span>
					</div>
					<div class="form-label" style="margin-left:50px;">
						<input type="hidden" id="pk_employee" name="pk_employee" value="${employee.pk_employee }" />
						<b><fmt:message key="The_work_number" /></b>：${employee.vempno }
					</div>
					<div class="form-label">
						<b><fmt:message key="Full_name" /></b>：<span style="color: blue;">${employee.vname }</span>
					</div>
					<div class="form-label">
						<b><fmt:message key="status" /></b>：<span style="color: red">${employee.vstatus }</span>
					</div>
				</div>
				<fieldset style="height:110px;width:635px;border:solid #416AA3 2px;padding-top:10px;">
					<legend><fmt:message key="hr_Regular_information" /></legend>
					<div class="form-line">
						<div class="form-label">
							<fmt:message key="entry" /><fmt:message key="date" />:
						</div>
						<div class="form-input">
							<input autocomplete="off" type="text" id="vjrdat" name="vjrdat" class="Wdate text" disabled="disabled" value="${employee.vjrdat }"/>
						</div>
						<div class="form-label">
							<span><fmt:message key="positive" /><fmt:message key="date" /></span>:
						</div>
						<div class="form-input">
							<input autocomplete="off" type="text" id="vwkdat1" name="vwkdat1" class="Wdate text" disabled="disabled" value="${positiveDate }"/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="hr_The_probation_period" />:</div>
						<div class="form-input">
							<select id="vsex" name="vsex" class="select" disabled="disabled" >
								<option value=""></option>
								<c:forEach var="trial" items="${vtrial }" >
									<option value="${trial.vname }" <c:if test="${employee.vtrial == trial.vname }">selected="selected"</c:if> >${trial.vname }</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-label">
							<fmt:message key="Wage" /><fmt:message key="smc_standard" />:
						</div>
						<div class="form-input">
							<input type="text" id="vwage" name="vwage" class="text" disabled="disabled" value="${employee.vwage }"/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label">
							<fmt:message key="Wage" /><fmt:message key="hrLevel" />:
						</div>
						<div class="form-input">
							<select id="pk_dutyid" name="pk_dutyid" class="select" disabled="disabled" >
								<c:forEach var="school" items="${vdutyid }" >
									<option value="${school.pk_datatyp }" <c:if test="${employee.pk_dutyid == school.pk_datatyp }">selected="selected"</c:if>>${school.vname }</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-label">
							<fmt:message key="baseic" /><fmt:message key="Wage" />:
						</div>
						<div class="form-input">
							<input type="text" id="ngzwage" name="ngzwage" class="text" disabled="disabled" value="${employee.ngzwage }"/>
						</div>
					</div>	
				</fieldset>
				<fieldset style="height: 220px;width:635px;border:solid #416AA3 2px;padding-top:10px;">
					<legend><fmt:message key="hr_After_the_positive_information" /></legend>
					<div class="form-line">
						<div class="form-label">
							<span><fmt:message key="positive" /><fmt:message key="date" /></span>:
						</div>
						<div class="form-input">
							<input autocomplete="off" type="text" id="vwkdat" name="vwkdat" class="Wdate text" value="${positiveDate }"/>
						</div>
						<div class="form-label"><fmt:message key="hr_Attn" />:</div>
						<div class="form-input">
							<input type="text" class="text" id="vwkemp" name="vwkemp" value="" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="positive" /><fmt:message key="hr_reason" />:</div>
						<div class="form-input">
							<select id="pk_positivereason" name="pk_positivereason" class="select" >
								<c:forEach var="reason" items="${positiveReason }" >
									<option value="${reason.pk_datatyp }" <c:if test="${employee.pk_positivereason == reason.pk_datatyp }">selected="selected"</c:if>>${reason.vname }</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-label">
							<fmt:message key="Wage" /><fmt:message key="hrLevel" />:
						</div>
						<div class="form-input" style="width:45px;">
							<select id="pk_dutyid" name="pk_dutyid" class="select" >
								<c:forEach var="school" items="${vdutyid }" >
									<option value="${school.pk_datatyp }" <c:if test="${employee.pk_dutyid == school.pk_datatyp }">selected="selected"</c:if>>${school.vname }</option>
								</c:forEach>
							</select>
						</div>
					</div>	
					<div class="form-line">
						<div class="form-label">
							<fmt:message key="Wage" /><fmt:message key="smc_standard" />:
						</div>
						<div class="form-input">
							<input type="text" id="vwage" name="vwage" onkeyup="value=value.replace(/[^\d]/g,'') " class="text" value="${employee.vwage }"/>
						</div>
						<div class="form-label">
							<fmt:message key="baseic" /><fmt:message key="Wage" />:
						</div>
						<div class="form-input">
							<input type="text" id="ngzwage" name="ngzwage" onkeyup="value=value.replace(/[^\d]/g,'') " class="text" value="${employee.ngzwage }"/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label">
							<fmt:message key="remark" />:
						</div>
						<div class="form-input">
							<textarea wrap="physical" id="vmemo" name="vmemo" style="width:400px;text-align: left;height:100px;margin-top:8px;"></textarea>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
		<input type="hidden" id="subflag" value="0" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-${sessionScope.locale}.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#vwkdat").click(function(){
		  			new WdatePicker({minDate:'#F{$dp.$D(\'${positiveDate}\')}'});
		  		});
			});
			
			function save(){
				$("#listForm").submit();
			}
		</script>
	</body>
</html>