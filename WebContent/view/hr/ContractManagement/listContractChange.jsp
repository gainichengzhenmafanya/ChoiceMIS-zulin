<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>合同变更记录</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>		
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
				
	</head>
	<body >
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/changeController/listContractChange.do" method="post">
		<!-- 标题  -->
		<div class="form-line">
			<div class="form-label">
				<span style="color: blue;font-size: 18px;">【合同变更记录查询】</span>
			</div>
		</div>
		<!-- 合同基本信息 -->
		<fieldset >
			<legend>合同基本信息</legend>
			<div class="form-line">
				<div class="form-label" style="width:80px;text-align: right;">
					员工状态：
				</div>
				<div class="form-label" style="width:100px;text-align: left;">
					<select id="esta" name="esta" class="text" style="margin-top: 3px;width:85px;height: 20px;">
						<option value="-1"><fmt:message key="please_select"/></option>
						<c:forEach var="school" items="${esta }" >
							<option value="${school.pk_datatyp }" <c:if test="${contract.esta == school.pk_datatyp }">selected="selected"</c:if>>${school.vname }</option>
						</c:forEach>
					</select>
				</div>
				<div class="form-label" style="width:60px;text-align: right;">
					部门：
				</div>
				<div class="form-label" style="width:85px;text-align: left;margin-left:-5px;">
					<div class="form-input" style="width:85px;">
						<input type="hidden" id="pk_hrdept" name="pk_hrdept" value="${contract.pk_hrdept }" />
						<input type="text" id="vdeptdes" name="vdeptdes" class="text" readonly="readonly" style="margin-bottom:3px;width:85px;" value="${contract.vdeptdes }"/>
						<img id="searchHrDept" class="search" src="<%=path%>/image/themes/icons/search.png" />
					</div>
				</div>
				<div class="form-label" style="width:80px;text-align: right;">
					合同类型：
				</div>
				<div class="form-label" style="width:85px;text-align: left;">
					<select id="vtyp" name="vtyp" class="text" style="margin-top: 3px;width:85px;height: 20px;">
						<option value="-1"><fmt:message key="please_select"/></option>
						<c:forEach var="school" items="${VTYP }" >
							<option value="${school.pk_datatyp }" <c:if test="${contract.vtyp == school.pk_datatyp }">selected="selected"</c:if>>${school.vname }</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="form-line">
				<div class="form-label" style="width:80px;text-align: right;">
					工号：
				</div>
				<div class="form-label" style="width:100px;text-align: left;">
					<input type="text" id="empno" name="empno" class="text" style="margin-bottom:3px;width:85px;" value="${contract.empno}" />
				</div>
				<div class="form-label" style="width:60px;text-align: right;">
					姓名：
				</div>
				<div class="form-label" style="width:85px;text-align: left;">
					<input type="text" id="ename" name="ename" class="text" style="margin-bottom:3px;width:85px;" value="${contract.ename }" />
				</div>
				<div class="form-label" style="width:75px;text-align: right;">
					有效：
				</div>
				<div class="form-label"  style="width:170px;text-align: right;">
					<input type="radio" id="isSta" name="isSta" class="text" value="-1"
					<c:if test="${contract.isSta != 2 && contract.isSta != 1 && contract.isSta != 0 }">checked="checked"</c:if>
					/>全部
					<input type="radio" id="isSta" name="isSta" class="text" value="2" 
					<c:if test="${contract.isSta == 2 }">checked="checked"</c:if>
					/>未执行
					<input type="radio" id="isSta" name="isSta" class="text" value="1" 
					<c:if test="${contract.isSta == 1 }">checked="checked"</c:if> /> 执行
					<input type="radio" id="isSta" name="isSta" class="text" value="0" 
					<c:if test="${contract.isSta == 0 }">checked="checked"</c:if>  />过期
				</div>
			</div>
			<div class="form-line">
				<div class="form-label" style="width:30px;text-align: right;">
				</div>
				<div class="form-label" style="width:150px;text-align: left;">
					<input type="radio" id="isDat" name="isDat" class="text" value="0" 
					<c:if test="${contract.isDat == 0 }">checked="checked"</c:if>/>到期日期
					<input type="radio" id="isDat1" name="isDat" class="text"  value="1" 
					<c:if test="${contract.isDat == 2 }">checked="checked"</c:if>/>签定日期
				</div>
				<div class="form-label" style="width:60px;text-align: right;">
					始自：
				</div>
				<div class="form-label" style="width:85px;text-align: left;">
					<input autocomplete="off" type="text" id="bfirst" name="bfirst" style="width:85px;" class="Wdate text" value="${contract.bfirst }"/>
				</div>
				<div class="form-label" style="width:75px;text-align: right;">
					终至：
				</div>
				<div class="form-label"  style="width:85px;text-align: left;">
					<input autocomplete="off" type="text" id="efirst" name="efirst" style="width:85px;" class="Wdate text" value="${contract.efirst }"/>
				</div>
			</div>
		</fieldset>
		<!-- 变更记录显示 -->
			<div class="grid" style="width: 100%;">
			<div class="table-head">
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:10px;"></span></td>
								<td><span style="width:30px;">序号</span></td>
								<td><span style="width:80px;">工号</span></td>
								<td><span style="width:100px;">姓名</span></td>
								<c:forEach var="columns" items="${listColumns }" varStatus="status">
									<c:if test="${sessionScope.locale == 'zh_CN'}">
										<td><span style="width:${columns.columnWidth}px;">${columns.zhColumnName }</span></td>
									</c:if>
									<c:if test="${sessionScope.locale == 'zh_TW'}">
										<td><span style="width:${columns.columnWidth}px;">${columns.twColumnName }</span></td>
									</c:if>
									<c:if test="${sessionScope.locale == 'en'}">
										<td><span style="width:${columns.columnWidth}px;">${columns.enColumnName }</span></td>
									</c:if>
								</c:forEach>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="cr" items="${contract.listContractRegistration}" varStatus="status">
								<tr>
									<td><span style="width:10px;">
									<input type="checkbox" name="idList" id="chk_${status.index}" value="${cr.PK_EMPLOYEE}"/></span></td>
									<td><span style="width:30px;">${status.index+1}</span></td>
									<td><span style="width:80px;text-align: left;"><c:out value="${cr.VEMPNO}" /></span></td>
									<td><span style="width:100px;text-align: left;"><c:out value="${cr.VENAME}" /></span></td>
									<c:forEach var="columns" items="${listColumns }" varStatus="sta">
										<c:if test="${columns.columnName != 'DBREACHCONTRACT' && columns.columnName != 'VEFFECTIVE'}">
											<td><span style="width:${columns.columnWidth}px;text-align: left;">${elf:append(cr,columns.properties,null)}</span></td>
										</c:if>
										<c:if test="${columns.columnName == 'DBREACHCONTRACT'}">
											<td><span style="width:${columns.columnWidth}px;;text-align: right;">
											<fmt:formatNumber value="${elf:append(cr,columns.properties,0)}" pattern="${contract.moneypattern}"/> 
											</span></td>
										</c:if>
										<c:if test="${columns.columnName == 'VEFFECTIVE'}">
											<td><span style="width:${columns.columnWidth}px;;text-align: left;">
												<c:if test="${elf:append(cr,columns.properties,null)==1}">执行</c:if>
												<c:if test="${elf:append(cr,columns.properties,null)==0}">过期</c:if>
												<c:if test="${elf:append(cr,columns.properties,null)!=1 && elf:append(cr,columns.properties,null)!=0}">未执行</c:if>
											</span></td>
										</c:if>
									</c:forEach>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>	
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
		<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>
		<script type="text/javascript">
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		$(document).ready(function(){
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),175);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				// 按钮加载
				var tool = loadTool();
				$("#bfirst,#efirst").focus(function(){
		 			new WdatePicker();
		 		});
				
				// 复选框  控制只能单选
					$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
						$(this).closest('.table-body').find(':checkbox').not($(this)).removeAttr("checked");
						event.stopPropagation();
					});
			});
		
		function chooseEmployee(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() ==1){
				var action='<%=path%>/staffList/findEmployeeByPk.do?buttontyp=archives&pk_employee='+checkboxList.filter(':checked').val();
				$('body').window({
					id: 'window_updateemployee',
					title: '<fmt:message key="employees" /><fmt:message key="archives" /><fmt:message key="maintenance" />',
					content: '<iframe id="archivesForm" frameborder="0" src='+action+'></iframe>',
					width:'650px',
					height:'500px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="insert" />',
								title: '<fmt:message key="insert" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									window.document.getElementById("archivesForm").contentWindow.add();
								}
							},{
								text: '<fmt:message key="update" />',
								title: '<fmt:message key="update" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									window.document.getElementById("archivesForm").contentWindow.update();
								}
							},"-",{
								text: '<fmt:message key="upload" /><fmt:message key="the_picture" />',
								title: '<fmt:message key="upload" /><fmt:message key="the_picture" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									window.document.getElementById("archivesForm").contentWindow.uploadPicture();
								}
							},"-",{
								text: '<fmt:message key="save" />',
								title: '<fmt:message key="save" /><fmt:message key="update" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('archivesForm')&&window.document.getElementById("archivesForm").contentWindow.validate._submitValidate()){
										window.document.getElementById("archivesForm").contentWindow.save();
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
		}
		
		
		// 跳转到列选择页面
		function toColsChoose() {
			colChooseWindow = $('body').window({
				title : '<fmt:message key="column_selection" />',
				content : '<iframe frameborder="0" src="<%=path%>/changeController/toColumnsChoose.do"></iframe>',
				width : '460px',
				height : '430px',
				draggable : true,
				isModal : true,
				confirmClose : false
			});
		}
		
		function loadTool(){
			var tool = $('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','0px']
						},
						handler: function(){
							$("#listForm").submit();
						}
					},{
						text: '查看档案',
						title: '查看档案',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','0px']
						},
						handler: function(){
							chooseEmployee();
						}
					}
					/*,{
						text: '<fmt:message key="print" />',
						title: '<fmt:message key="print" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-100px','-40px']
						},
						handler: function(){
						}
					}*/,{
						text: '<fmt:message key="column_selection" />',
						title: '<fmt:message key="column_selection" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-100px','-40px']
						},
						handler: function(){
							toColsChoose();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
						   $(".close").click();						
						}
					}
				]
			});
			return tool;
		}
		</script>
	</body>
</html>