<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>合同登记管理</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>			
	</head>
	<body >
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/contractRegistration/saveContractRegistration.do" method="post">
				<!-- 标题  -->
				<div class="form-line">
					<div class="form-label">
						<span style="color: blue;font-size: 18px;">【合同新签】</span>
					</div>
					<div class="form-label">
					<input type="hidden" id="VEMPNO" name="VEMPNO" value="${Contract.empno }" />
					<input type="hidden" id="VTYPDESC" name="VTYPDESC" />
					<input type="hidden" id="PK_EMPLOYEE" name="PK_EMPLOYEE" value="${Contract.pk_employee }" />
					<input type="hidden" id="PK_CONTRACTREG" name="PK_CONTRACTREG" value="${reg.PK_CONTRACTREG }" />
						<b><fmt:message key="employees" /><fmt:message key="The_work_number" /></b>:<span style="color:red;">${Contract.empno}</span>
					</div>
					<div class="form-label">
						<b>姓名</b>:<span style="color:blue;">${Contract.ename }</span>
					</div>
					<div class="form-label">
						<b>状态</b>:<span style="color:red;">${Contract.esta }</span>
					</div>
				</div>
				<fieldset>
					<legend>员工信息</legend>
					<!-- 单位  部门 -->
						<!-- 岗位  入职日期 -->
					<div class="form-line">
						<div class="form-label" style="width:5px;text-align: left;">
						</div>
						<div class="form-label" style="width:150px;text-align: left;">
							部门:${Contract.vdeptdes }
						</div>
						<div class="form-label" style="width:150px;text-align: left;">
							<fmt:message key="Post" />:${Contract.vstation }
						</div>
						<div class="form-label" style="width:150px;text-align: left;">
							<fmt:message key="entry" /><fmt:message key="date" />:${Contract.vjrdat }
						</div>
					</div>
				</fieldset>
				<fieldset style="height:295px">
					<legend>合同信息</legend>
						<!-- 合同 始于  合同止于-->
						<div class="form-line">
							<div class="form-label" style="width:50px;text-align: right;">
								<span style="color:red;">*</span>合同始于:
							</div>
							<div class="form-input" style="width:280px;">
								<input autocomplete="off" type="text" id="VBEINDATE" name="VBEINDATE" style="width:100px;" class="Wdate text" value="${reg.VBEINDATE }"/>
							</div>
							<div class="form-label" style="width:50px;text-align: right;">
								<span style="color:red;">*</span>合同止于:
							</div>
							<div class="form-input" style="width:100px;">
								<input autocomplete="off" type="text" id="VENDDATE" name="VENDDATE" style="width:100px;" class="Wdate text" value="${reg.VENDDATE }"/>
							</div>
						</div>
						<!-- 执行日期   期限-->
						<div class="form-line">
							<div class="form-label" style="width:50px;text-align: right;">
								<span style="color:red;">*</span>执行日期:
							</div>
							<div class="form-input" style="width:280px;">
								<input autocomplete="off" type="text" id="VEXECUTIVEDATE" name="VEXECUTIVEDATE" style="width:100px;" class="Wdate text" value="${reg.VEXECUTIVEDATE }"/>
							</div>
							<div class="form-label" style="width:56px;text-align: right;">
								<span style="color:red;">*</span>期限:
							</div>
							<div class="form-input" style="width:100px;">
								<select id="VTIMELIMIT" name="VTIMELIMIT" class="text" style="margin-top: 3px;width:102px;height: 20px;margin-left:-6px;">
									<c:forEach var="school" items="${VTIMELIMIT }" >
											<option value="${school.vname}" <c:if test="${reg.VTIMELIMIT == school.vname }">selected="selected"</c:if>>${school.vname}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<!-- 合同类型  违约金-->
						<div class="form-line">
							<div class="form-label" style="width:50px;text-align: right;">
								<span style="color:red;">*</span>合同类型:
							</div>
							<div class="form-input" style="width:280px;">
								<select id="VTYP" name="VTYP" class="text" style="margin-top: 3px;width:100px;height: 20px;">
									<c:forEach var="school" items="${VTYP }" >
											<option value="${school.pk_datatyp }" <c:if test="${reg.VTYP == school.pk_datatyp }">selected="selected"</c:if>>${school.vname }</option>
									</c:forEach>
								</select>
							</div>
							<div class="form-label" style="width:56px;text-align: right;">
								<span style="color:red;">*</span>违约金:
							</div>
							<div class="form-input" style="width:100px;">
								<input type="text" id="DBREACHCONTRACT" name="DBREACHCONTRACT" style="width:100px;margin-left:-6px;"  onkeyup="this.value=this.value.replace(/[^\d\.\d{0,3}]/g,'')" class="text" value="${reg.DBREACHCONTRACT }"/>
							</div>
						</div>
						<!-- 备注 -->
						<div class="form-line">
							<div class="form-label" style="width:56px;text-align: right;">
								<fmt:message key="remark"/>:
							</div>
							<div class="form-input" style="margin-left:-5px;">
								<textarea rows="10" cols="66" id = "VMEMO" name ="VMEMO">${reg.VMEMO}</textarea>
							</div>
						</div>
				</fieldset>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
		<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>
		<script type="text/javascript">
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
			$(document).ready(function(){
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),32);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadValidate();
				// 按钮加载
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="save" />',
							title: '<fmt:message key="save" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','0px']
							},
							handler: function(){
								save();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
							   parent.$(".close").click();						
							}
						}
					]
				});
				// 加载时间选择
				$("#VBEINDATE").focus(function(){
		 			new WdatePicker();
		 		});
				$("#VENDDATE").focus(function(){
		 			new WdatePicker({minDate:$("#VBEINDATE").val()});
		 		});
				$("#VEXECUTIVEDATE").focus(function(){
		 			new WdatePicker({minDate:$("#VBEINDATE").val(),maxDate:$("#VENDDATE").val()});
		 		});
			});
			// 保存
			function save(){
				if(validate._submitValidate()){
					$("#VTYPDESC").val($("#VTYP option:selected").text());
					$("#listForm").submit();
				}
			}
			
			// 加载验证
			function loadValidate(){
				validate = new Validate({
					validateItem:[
					 {
					// 期限
						type:'select',
						validateObj:'VTIMELIMIT',
						validateType:['canNull'],
						param:['F'],
						error:['期限<fmt:message key="cannot_be_empty" />!']
					},{
					// 合同类型
						type:'select',
						validateObj:'VTYP',
						validateType:['canNull'],
						param:['F'],
						error:['合同类型<fmt:message key="cannot_be_empty" />!']
					},{// 合同始于
						type:'text',
						validateObj:'VBEINDATE',
						validateType:['canNull'],
						param:['F'],
						error:['合同始于<fmt:message key="cannot_be_empty" />!']
					},{// 合同终止
						type:'text',
						validateObj:'VENDDATE',
						validateType:['canNull'],
						param:['F'],
						error:['合同止于<fmt:message key="cannot_be_empty" />!']
					},{// 合同执行
						type:'text',
						validateObj:'VEXECUTIVEDATE',
						validateType:['canNull'],
						param:['F'],
						error:['执行日期<fmt:message key="cannot_be_empty" />!']
					},{// 违约金
						type:'text',
						validateObj:'DBREACHCONTRACT',
						validateType:['canNull'],
						param:['F'],
						error:['违约金<fmt:message key="cannot_be_empty" />!']
					}]
				});
			}
			
		</script>
	</body>
</html>