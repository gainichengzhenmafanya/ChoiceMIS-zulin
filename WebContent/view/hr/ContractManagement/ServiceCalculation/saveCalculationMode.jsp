<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>工龄计算公式调整</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>			
	</head>
	<body >
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/serviceCalculation/saveServiceCalculation.do" method="post">
						<!-- 合同 始于  合同止于-->
						<div class="form-line">
							<div class="form-label" style="width:300px;text-align:center;">
								<span style="color:red;">当前工龄计算方式:合同工龄模式</span>
									<input type="hidden" id="PK_SERVICEFORMULA" name="PK_SERVICEFORMULA" value="${serviceFormula.PK_SERVICEFORMULA}" />
									<input type="hidden" id="PK_MODE" name="PK_MODE" value="${serviceFormula.PK_MODE}" />
									<input type="hidden" id="BFIELDDES" name="BFIELDDES" value="${serviceFormula.BFIELDDES}" />
							</div>
						</div>
						<!-- 执行日期   期限-->
						<div class="form-line">
							<div class="form-label" style="width:300px;text-align:center;margin-left:12px;">
								<span style="color:red;">*</span>合同类型:
								<select id="PK_CONTENTTYP" name="PK_CONTENTTYP" class="text" style="margin-top: 3px;width:152px;height: 20px;">
									<c:forEach var="school" items="${PK_CONTENTTYP }" >
											<option value="${school.pk_datatyp }" <c:if test="${serviceFormula.PK_CONTENTTYP == school.pk_datatyp }">selected="selected"</c:if>>${school.vname }</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<!-- 执行日期   期限-->
						<div class="form-line">
							<div class="form-label" style="width:300px;text-align:center;">
								<span style="color:red;">*</span>计算起始日期:
								<input autocomplete="off" type="text" id="BCDAT" name="BCDAT" style="width:150px;" class="Wdate text" value="${serviceFormula.BCDAT }"/>
							</div>
						</div>
						<!-- 执行日期   期限-->
						<div class="form-line">
							<div class="form-label" style="width:300px;text-align:center;">
								<span style="color:red;">*</span>起始日期字段:
								<select id="BFIELD" name="BFIELD" class="text" style="margin-top: 3px;width:152px;height: 20px;">
											<option value="VJRDAT"<c:if test="${serviceFormula.BFIELD == 'VJRDAT' }">selected="selected"</c:if>>入职日期</option>
											<option value="VWKDAT"<c:if test="${serviceFormula.BFIELD == 'VWKDAT' }">selected="selected"</c:if>>转正日期</option>
											<option value="VBEINDATE"<c:if test="${serviceFormula.BFIELD == 'VBEINDATE' }">selected="selected"</c:if>>首签起始日期</option>
											<option value="VENDDATE"<c:if test="${serviceFormula.BFIELD == 'VENDDATE' }">selected="selected"</c:if>>首签结束日期</option>
											<option value="VEXECUTIVEDATE"<c:if test="${serviceFormula.BFIELD == 'VEXECUTIVEDATE' }">selected="selected"</c:if>>首签执行日期</option>
								</select>
							</div>
						</div>
						<!-- 执行日期   期限-->
						<div class="form-line">
							<div class="form-label" style="width:300px;text-align:center;">
								<span style="color:red;">*</span>工龄增减天数:
								<select id="SERVICEDAY" name="SERVICEDAY" class="text" style="margin-top: 3px;width:152px;height: 20px;">
									<option value=""></option>
									<option value="0"<c:if test="${serviceFormula.SERVICEDAY == '0' }">selected="selected"</c:if>>0</option>
									<option value="1"<c:if test="${serviceFormula.SERVICEDAY == '1' }">selected="selected"</c:if>>1</option>
									<option value="2"<c:if test="${serviceFormula.SERVICEDAY == '2' }">selected="selected"</c:if>>2</option>
									<option value="3"<c:if test="${serviceFormula.SERVICEDAY == '3' }">selected="selected"</c:if>>3</option>
									<option value="4"<c:if test="${serviceFormula.SERVICEDAY == '4' }">selected="selected"</c:if>>4</option>
									<option value="5"<c:if test="${serviceFormula.SERVICEDAY == '5' }">selected="selected"</c:if>>5</option>
									<option value="6"<c:if test="${serviceFormula.SERVICEDAY == '6' }">selected="selected"</c:if>>6</option>
									<option value="7"<c:if test="${serviceFormula.SERVICEDAY == '7' }">selected="selected"</c:if>>7</option>
									<option value="8"<c:if test="${serviceFormula.SERVICEDAY == '8' }">selected="selected"</c:if>>8</option>
									<option value="9"<c:if test="${serviceFormula.SERVICEDAY == '9' }">selected="selected"</c:if>>9</option>
									<option value="10"<c:if test="${serviceFormula.SERVICEDAY == '10' }">selected="selected"</c:if>>10</option>
								</select>
							</div>
						</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
		<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>
		<script type="text/javascript">
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
			$(document).ready(function(){
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),32);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadValidate();
				// 按钮加载
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="save" />',
							title: '<fmt:message key="save" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','0px']
							},
							handler: function(){
								save();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
							   parent.$(".close").click();						
							}
						}
					]
				});
				// 加载时间选择
				$("#BCDAT").focus(function(){
		 			new WdatePicker();
		 		});
			});
			// 保存
			function save(){
				if(validate._submitValidate()){
					$("#BFIELDDES").val($("#BFIELD option:selected").text());
					$("#listForm").submit();
				}
			}
			
			// 加载验证
			function loadValidate(){
				validate = new Validate({
					validateItem:[
					 {
					// 增减天数
						type:'select',
						validateObj:'SERVICEDAY',
						validateType:['canNull'],
						param:['F'],
						error:['工龄增减天数<fmt:message key="cannot_be_empty" />!']
					},{
					// 合同类型
						type:'select',
						validateObj:'PK_CONTENTTYP',
						validateType:['canNull'],
						param:['F'],
						error:['合同类型<fmt:message key="cannot_be_empty" />!']
					},{
					// 起始日期字段
						type:'select',
						validateObj:'BFIELD',
						validateType:['canNull'],
						param:['F'],
						error:['起始日期字段<fmt:message key="cannot_be_empty" />!']
					},{//  计算起始日期
						type:'text',
						validateObj:'BCDAT',
						validateType:['canNull'],
						param:['F'],
						error:['计算起始日期<fmt:message key="cannot_be_empty" />!']
					}]
				});
			}
			
		</script>
	</body>
</html>