<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>合同工龄</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<style type="text/css">
			.moduleInfo {
				//background-color: #E1E1E1;
			}
			.leftFrame{
				width:20%;
			}
			.mainFrame{
				width:80%;
			}
		</style>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		
		$(document).ready(function(){
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),32);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			// 按钮加载
			var tool = loadBtn();
		});
	
		
		// 工具栏加载
		function loadBtn(){
			$('.tool').text("");
			$('.tool').toolbar({
				// 工龄计算  模式设置  查看档案  列选择 打印  关闭
				items: [{
						text: '工龄计算',
						title: '工龄计算',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						handler: function(){
							window.frames["mainFrame"].calCalculationMode();
						}
					},
					{
						text: '模式设置',
						title: '模式设置',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						handler: function(){
							showCalculationMode();
						}
					},{
						text: '查看档案',
						title: '查看档案',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						handler: function(){
							chooseEmployee();
						}
					},'-',{
						text: '打印',
						title: '打印',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}
				]
			});
		}
		
		// 模式设置
		function showCalculationMode(_val){
			var _url = '<%=path%>/serviceCalculation/listCalculationMode.do';
			colChooseWindow = $('body').window({
				title : '合同工龄计算模式',
				content : '<iframe frameborder="0" src="'+_url+'"></iframe>',
				width : '600px',
				height : '500px',
				draggable : true,
				isModal : true,
				confirmClose : false
			});
		}
		
		// 查看员工档案
		function chooseEmployee(){
			var checkboxList = window.frames["mainFrame"].$('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() ==1){
				var action='<%=path%>/staffList/findEmployeeByPk.do?buttontyp=archives&pk_employee='+checkboxList.filter(':checked').val();
				$('body').window({
					id: 'window_updateemployee',
					title: '<fmt:message key="employees" /><fmt:message key="archives" /><fmt:message key="maintenance" />',
					content: '<iframe id="archivesForm" frameborder="0" src='+action+'></iframe>',
					width: '650px',
					height: '500px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="insert" />',
								title: '<fmt:message key="insert" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									window.document.getElementById("archivesForm").contentWindow.add();
								}
							},{
								text: '<fmt:message key="update" />',
								title: '<fmt:message key="update" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									window.document.getElementById("archivesForm").contentWindow.update();
								}
							},"-",{
								text: '<fmt:message key="upload" /><fmt:message key="the_picture" />',
								title: '<fmt:message key="upload" /><fmt:message key="the_picture" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									window.document.getElementById("archivesForm").contentWindow.uploadPicture();
								}
							},"-",{
								text: '<fmt:message key="save" />',
								title: '<fmt:message key="save" /><fmt:message key="update" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('archivesForm')&&window.document.getElementById("archivesForm").contentWindow.validate._submitValidate()){
										window.document.getElementById("archivesForm").contentWindow.save();
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}else{
				alert("请选择查看一个员工档案");
			}
		}
		</script>
		
	</head>
	<body>
		<div class="tool">
		</div>
		<div class="leftFrame">
			<iframe src="<%=path %>/serviceCalculation/treeServiceCalculation.do" frameborder="0" name="leftFrame" id="leftFrame"></iframe>
    	</div>
    	<div class="mainFrame">
			<iframe src="<%=path %>/serviceCalculation/listServiceCalculation.do" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
    	</div>
	</body>
</html>