<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>工龄模式设置</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>		
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
				
	</head>
	<body >
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/serviceCalculation/listCalculationMode.do" method="post">
		<!-- 标题  -->
		<div class="form-line">
			<div class="form-label">
				<span style="color: blue;font-size: 18px;">【工龄计算模式】</span>
			</div>
		</div>
		<!-- 合同基本信息 -->
			<div class="form-line">
				<div class="form-label" style="width:80px;text-align: right;">
					模式：
				</div>
				<div class="form-label" style="width:130px;text-align: left;">
					<select id="esta" name="esta" class="text" style="margin-top: 3px;width:100px;height: 20px;">
						<option value="-1"><fmt:message key="please_select"/></option>
						<c:forEach var="school" items="${esta }" >
							<option value="${school.pk_datatyp }" <c:if test="${contract.esta == school.pk_datatyp }">selected="selected"</c:if>>${school.vname }</option>
						</c:forEach>
					</select>
				</div>
			</div>
		<!-- 变更记录显示 -->
			<div class="grid" style="width: 100%;">
			<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:10px;"></span></td>
								<td><span style="width:30px;">编号</span></td>
								<td><span style="width:80px;">合同类别号</span></td>
								<td><span style="width:160px;">合同类别名称</span></td>
								<td><span style="width:80px;">计算起始日期</span></td>
								<td><span style="width:80px;">计算结束日期</span></td>
								<td><span style="width:60px;">增减天数</span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="cr" items="${listServiceFormula}" varStatus="status">
								<tr>
									<td><span style="width:10px;">
									<input type="checkbox" name="idList" id="chk_${status.index}" value="${cr.PK_SERVICEFORMULA}"/></span></td>
									<td><span style="width:30px;">${status.index+1}</span></td>
									<td><span style="width:80px;">${cr.CONTENTTYPCODE}</span></td>
									<td><span style="width:160px;">${cr.CONTENTTYPDESC}</span></td>
									<td><span style="width:80px;">${cr.BCDAT}</span></td>
									<td><span style="width:80px;">${cr.BFIELDDES}</span></td>
									<td><span style="width:60px;">${cr.SERVICEDAY}</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>	
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
		<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>
		<script type="text/javascript">
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		$(document).ready(function(){
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),32);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				// 按钮加载
				var tool = loadTool();
				
				// 复选框  控制只能单选
					$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
						$(this).closest('.table-body').find(':checkbox').not($(this)).removeAttr("checked");
						event.stopPropagation();
					});
			});
		
		// 调整新签界面选择方法
		function saveCalculationModeRet(_val){
			var _url = '<%=path%>/serviceCalculation/saveCalculationModeRet.do?PK_SERVICEFORMULA='+_val;
			colChooseWindow = $('body').window({
				title : '编辑工龄公式',
				content : '<iframe frameborder="0" src="'+_url+'"></iframe>',
				width : '300px',
				height : '230px',
				draggable : true,
				isModal : true,
				confirmClose : false
			});
		}
		// 删除
		function delCalculationModeRet(_id){
			if(confirm("是否删除选中工龄计算公式")){
				if(_id=='' || _id== null){
					alert('请选择需要删除记录');
					return;
				}
				var param = {};
				param['PK_SERVICEFORMULA'] = _id;
				$.post('<%=path%>/serviceCalculation/delCalculationModeRet.do',param,function(data){
					if(data==1){
						alert("删除成功！");
					}else{
						alert("删除失败！");
					}
					$("#listForm").submit();
				});
			}
		}
		
		function loadTool(){
			var tool = $('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','0px']
						},
						handler: function(){
							$("#listForm").submit();
						}
					},{
						text: '新增',
						title: '新增',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','0px']
						},
						handler: function(){
							saveCalculationModeRet();
						}
					},{
						text: '修改',
						title: '修改',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','0px']
						},
						handler: function(){
							var _id = $("input[name='idList']:checked").val();
							if(_id=='' || _id== null){
								alert('请选择需要修改记录');
								return;
							}
							saveCalculationModeRet(_id);
						}
					},{
						text: '删除',
						title: '删除',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','0px']
						},
						handler: function(){
							var _id = $("input[name='idList']:checked").val();
							delCalculationModeRet(_id);
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							 parent.$(".close").click();				
						}
					}
				]
			});
			return tool;
		}
		
		</script>
	</body>
</html>