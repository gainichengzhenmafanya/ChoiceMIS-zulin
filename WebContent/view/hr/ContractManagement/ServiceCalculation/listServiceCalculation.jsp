<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>合同变更记录</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>		
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
				
	</head>
	<body >
		<form id="listForm" action="<%=path%>/serviceCalculation/listServiceCalculation.do" method="post">
		<!-- 标题  -->
		<div class="form-line">
			<div class="form-label" style="width:200px;text-align:left;" >
				<span style="color: blue;font-size: 18px;">【合同工龄计算】</span>
			</div>
			<div class="form-label">
				<span style="color:red;">使用前首先设置好工龄计算模式，模式设定以合同类型为基础</span>
			</div>
		</div>
		<fieldset>
			<div class="form-line">
				<div class="form-label" style="width:65px;text-align: right;">
					工号：
				</div>
				<div class="form-label" style="width:130px;text-align: left;">
					<input type="text" id="empno" name="empno" class="text" style="margin-bottom:3px;width:100px;" value="${contract.empno}" />
					<input type="hidden" id="PK_EMPLOYEE" name="PK_EMPLOYEE"  />
					<input type="hidden" id="pk_hrdept" name="pk_hrdept"  />
				</div>
				<div class="form-label" style="width:65px;text-align: right;">
					姓名：
				</div>
				<div class="form-label" style="width:110px;text-align: left;">
					<input type="text" id="ename" name="ename" class="text" style="margin-bottom:3px;width:100px;" value="${contract.ename }" />
				</div>
				<div class="form-label" style="width:65px;text-align: right;">
					缩写：
				</div>
				<div class="form-label"  style="width:110px;text-align: left;">
					<input type="text" id="ename" name="ename" class="text" style="margin-bottom:3px;width:100px;" value="${contract.ename }" />
				</div>
			</div>
			<div class="form-line">
				<div class="form-label" style="width:65px;text-align: right;">
					单位：
				</div>
				<div class="form-label" style="width:130px;text-align: left;">
					<input type="radio" id="isutily" name="isutil" class="text" value="0"/>年数
					<input type="radio" id="isutilm" name="isutil" class="text" value="1"/>月数
					<input type="radio" id="isutild" name="isutil" class="text" value="2"/>日数
				</div>
				<div class="form-label" style="width:65px;text-align: right;">
					员工状态：
				</div>
				<div class="form-label" style="width:130px;text-align: left;">
					<select id="esta" name="esta" class="text" style="margin-top: 3px;width:100px;height: 20px;">
						<option value="-1"><fmt:message key="please_select"/></option>
						<c:forEach var="school" items="${esta }" >
							<option value="${school.pk_datatyp }" <c:if test="${contract.esta == school.pk_datatyp }">selected="selected"</c:if>>${school.vname }</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="form-line">
				<div class="form-label" style="width:65px;text-align: right;">
					计算模式：
				</div>
				<div class="form-label" style="width:130px;text-align: left;">
					<select id="esta" name="esta" class="text" style="margin-top: 3px;width:100px;height: 20px;">
						<option value="-1"><fmt:message key="please_select"/></option>
						<c:forEach var="school" items="${esta }" >
							<option value="${school.pk_datatyp }" <c:if test="${contract.esta == school.pk_datatyp }">selected="selected"</c:if>>${school.vname }</option>
						</c:forEach>
					</select>
				</div>
				<div class="form-label" style="width:65px;text-align: right;">
					截取日期：
				</div>
				<div class="form-label" style="width:110px;text-align: left;">
					<input autocomplete="off" type="text" id="execdat" name="execdat" style="width:100px;" class="Wdate text" value="${contract.execdat }"/>
				</div>
				<div class="form-label" style="width:65px;text-align: right;">
					工龄大于：
				</div>
				<div class="form-label"  style="width:110px;text-align: left;">
					<input type="text" id="vagl" name="vagl" class="text" style="margin-bottom:3px;width:100px;" value="${contract.vagl }" />
				</div>
			</div>
		</fieldset>
		<!-- 变更记录显示 -->
			<div class="grid" style="width: 100%;">
			<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:30px;">序号</span></td>
								<td><span style="width:10px;"></span></td>
								<td><span style="width:60px;">工号</span></td>
								<td><span style="width:100px;">姓名</span></td>
								<c:forEach var="columns" items="${listColumns }" varStatus="status">
									<c:if test="${sessionScope.locale == 'zh_CN'}">
										<td><span style="width:${columns.columnWidth}px;">${columns.zhColumnName }</span></td>
									</c:if>
									<c:if test="${sessionScope.locale == 'zh_TW'}">
										<td><span style="width:${columns.columnWidth}px;">${columns.twColumnName }</span></td>
									</c:if>
									<c:if test="${sessionScope.locale == 'en'}">
										<td><span style="width:${columns.columnWidth}px;">${columns.enColumnName }</span></td>
									</c:if>
								</c:forEach>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="cr" items="${listEmployee}" varStatus="status">
								<tr>
									<td><span style="width:30px;">${status.index+1}</span></td>
									<td><span style="width:10px;">
										<input type="checkbox" name="idList" id="chk_${status.index}" value="${cr.pk_employee}"/>
									</span></td>
									<td><span style="width:60px;text-align: left;"><c:out value="${cr.vempno}" /></span></td>
									<td><span style="width:100px;text-align: left;"><c:out value="${cr.vname}" /></span></td>
									<c:forEach var="columns" items="${listColumns }" varStatus="sta">
											<td><span style="width:${columns.columnWidth}px;text-align: left;">${elf:append(cr,columns.properties,null)}</span></td>
									</c:forEach>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>	
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
		<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>
		<script type="text/javascript">
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		$(document).ready(function(){
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),170);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				// 按钮加载
				$("#bfirst,#efirst").focus(function(){
		 			new WdatePicker();
		 		});
				
			});
		// 跳转到列选择页面
		function toColsChoose() {
			colChooseWindow = $('body').window({
				title : '<fmt:message key="column_selection" />',
				content : '<iframe frameborder="0" src="<%=path%>/changeController/toColumnsChoose.do"></iframe>',
				width : '460px',
				height : '430px',
				draggable : true,
				isModal : true,
				confirmClose : false
			});
		}
		
		// 合同工龄计算
		function calCalculationMode(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			var pk_employee="-1";
			if(checkboxList&&checkboxList.filter(':checked').size()>0){
				if(!confirm('是否重新计算选中员工工龄？')){
					return;
				}
				$("#wait2").css("visibility","visible");
				$("#wait").css("visibility","visible");
				for(var k=0;k<checkboxList.size();k++){
					pk_employee+=","+checkboxList.get(k).value;
				}
				alert(pk_employee);	
			}else{
				if(!confirm('是否重新计算所有员工工龄？')){
					return;
				}
				for(var k=0;k<checkboxList.size();k++){
					pk_employee+=","+checkboxList.get(k).value;
				}
				alert(pk_employee);	
			}
			$("#PK_EMPLOYEE").val(pk_employee);
			$("#listForm").attr("action","<%=path%>/serviceCalculation/calCalculationMode.do");
			$("#listForm").submit();
		}
		function changeUrl(orderid,pk_id,code,name,father){
			$("#pk_hrdept").val(pk_id);
			$("#listForm").attr("action","<%=path%>/serviceCalculation/listServiceCalculation.do");
			$("#listForm").submit();
		}
		</script>
	</body>
</html>