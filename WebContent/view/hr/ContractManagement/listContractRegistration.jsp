<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>合同登记</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>		
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
				
	</head>
	<body >
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/contractRegistration/listContractRegistration.do" method="post">
		<!-- 标题  -->
		<div class="form-line">
			<div class="form-label">
				<span style="color: blue;font-size: 18px;">【合同登记】</span>
			</div>
			<div class="form-label">
				<input type="hidden" id="VEMPNO" name="VEMPNO" value="${Contract.empno }" />
				<input type="hidden" id="PK_EMPLOYEE" name="PK_EMPLOYEE" value="${Contract.pk_employee }" />
				<input type="hidden" id="pk_store" name="pk_store" value="${pk_store}" />
				<b>工号</b>:<span style="color:red;">${Contract.empno }</span>
			</div>
			<div class="form-label">
				<b>姓名</b>:<span style="color:blue;">${Contract.ename }</span>
			</div>
			<div class="form-label">
				<b>员工状态</b>:<span style="color:red;">${Contract.esta }</span>
			</div>
		</div>
		<!-- 合同基本信息 -->
		<fieldset>
			<legend>合同基本信息</legend>
			<div class="form-line">
				<div class="form-label" style="width:80px;text-align: right;">
					<b>首签日期自：</b>
				</div>
				<div class="form-label" style="width:65px;text-align: left;margin-left:1px;">
					${Contract.bfirst }
				</div>
				<div class="form-label" style="width:80px;text-align: right;">
					<b>首签日期至：</b>
				</div>
				<div class="form-label" style="width:65px;text-align: left;margin-left:1px;">
					${Contract.efirst }
				</div>
				<div class="form-label" style="width:90px;text-align: right;">
					<b>合同累计期限：</b>
				</div>
				<div class="form-label" style="width:55px;text-align: left;margin-left:1px;">
					${Contract.timeLimit }个月
				</div>
			</div>
			<div class="form-line">
				<div class="form-label" style="width:80px;text-align: right;">
					<b>末钱日期自：</b>
				</div>
				<div class="form-label" style="width:65px;text-align: left;margin-left:1px;">
					${Contract.bsign }
				</div>
				<div class="form-label" style="width:80px;text-align: right;">
					<b>末钱日期至：</b>
				</div>
				<div class="form-label" style="width:65px;text-align: left;margin-left:1px;">
					${Contract.esign }
				</div>
				<div class="form-label" style="width:90px;text-align: right;">
					<b>累计签定次数：</b>
				</div>
				<div class="form-label"  style="width:55px;text-align: left;margin-left:1px;">
					${Contract.signCnt}次
				</div>
			</div>
		</fieldset>
		<!-- 合同签订记录 -->
		<fieldset>
			<legend>合同签订记录</legend>
			<div class="grid" style="width: 100%;">
			<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:10px;"></span></td>
								<td><span style="width:30px;">序号</span></td>
								<td><span style="width:70px;">合同始于</span></td>
								<td><span style="width:70px;">合同止于</span></td>
								<c:forEach var="columns" items="${listColumns }" varStatus="status">
									<c:if test="${sessionScope.locale == 'zh_CN'}">
										<td><span style="width:${columns.columnWidth}px;">${columns.zhColumnName }</span></td>
									</c:if>
									<c:if test="${sessionScope.locale == 'zh_TW'}">
										<td><span style="width:${columns.columnWidth}px;">${columns.twColumnName }</span></td>
									</c:if>
									<c:if test="${sessionScope.locale == 'en'}">
										<td><span style="width:${columns.columnWidth}px;">${columns.enColumnName }</span></td>
									</c:if>
								</c:forEach>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="cr" items="${listContract}" varStatus="status">
								<tr>
									<td><span style="width:10px;">
									<input type="checkbox" name="idList" id="chk_${status.index}" value="${cr.PK_CONTRACTREG}"/></span></td>
									<td><span style="width:30px;">${status.index+1}</span></td>
									<td><span style="width:70px;text-align: left;"><c:out value="${cr.VBEINDATE}" /></span></td>
									<td><span style="width:70px;text-align: left;"><c:out value="${cr.VENDDATE}" /></span></td>
									<c:forEach var="columns" items="${listColumns }" varStatus="sta">
										<c:if test="${columns.columnName == 'VTIMELIMIT'}">
											<td><span style="width:${columns.columnWidth}px;text-align: right;">${elf:append(cr,columns.properties,0)}</span></td>
										</c:if>
										<c:if test="${columns.columnName == 'DBREACHCONTRACT'}">
											<td><span style="width:${columns.columnWidth}px;text-align: right;">
												<fmt:formatNumber value="${elf:append(cr,columns.properties,0)}" pattern="${Contract.moneypattern}"/></span></td>
										</c:if>
										
										<c:if test="${columns.columnName == 'VEFFECTIVE'}">
											<td><span style="width:${columns.columnWidth}px;text-align: left;">
												<c:if test="${elf:append(cr,columns.properties,null)==1}">执行</c:if>
												<c:if test="${elf:append(cr,columns.properties,null)==0}">过期</c:if>
												<c:if test="${elf:append(cr,columns.properties,null)!=1 && elf:append(cr,columns.properties,null)!=0}">未执行</c:if>
											</span></td>
										</c:if>
										<c:if test="${columns.columnName != 'VTIMELIMIT' && columns.columnName != 'VEFFECTIVE' && columns.columnName != 'DBREACHCONTRACT'}">
											<td><span style="width:${columns.columnWidth}px;text-align: left;">${elf:append(cr,columns.properties,null)}</span></td>
										</c:if>
										
									</c:forEach>
									<!-- <td><span style="width:70px;text-align: left;"><c:out value="${cr.VEFFECTIVE}" /></span></td>
									<td><span style="width:70px;text-align: right;"><c:out value="${cr.VTIMELIMIT}" /></span></td>
									<td><span style="width:70px;text-align: left;"><c:out value="${cr.VTYPDESC}" /></span></td>
									<td><span style="width:70px;text-align: left;"><c:out value="${cr.DBREACHCONTRACT}" /></span></td>
									<td><span style="width:180px;text-align: left;"><c:out value="${cr.VMEMO}" /></span></td> -->
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>	
		</fieldset>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
		<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>
		<script type="text/javascript">
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		$(document).ready(function(){
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),175);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				// 按钮加载
				var tool = loadTool();
				loadCheck();
				checkEmployee();
			});
		// 判断是否选中员工
		function checkEmployee(){
			var _pk = $("#PK_EMPLOYEE").val();
			if(_pk == null || _pk == ''){
				chooseEmployee();
			}
		}
		// 复选框  控制只能单选
		function loadCheck(){
			$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
				$(this).closest('.table-body').find(':checkbox').not($(this)).removeAttr("checked");
				event.stopPropagation();
			});
		}
		
		// 员工列表选择
		 function chooseEmployee(){
			var pk_store = $("#pk_store").val();
			var url = "<%=path%>/staffList/toChooseEmployee.do";
				url += "?callBack=setEmployee&domId=selected&single=true&type=listEmp&pk_store="+pk_store;
			return $('body').window({
				id: 'window_chooseSiteType',
				title: '<fmt:message key="select1" /><fmt:message key="sector" />',
				content: '<iframe id="listSiteTypeFrame" frameborder="0" src='+url+'></iframe>',
				width: 600,
				height: 430,
				confirmClose: false,
				draggable: true,
				isModal: true
			});
		}
		  function setEmployee(data){
			  $("#PK_EMPLOYEE").val(data.pk);
			  $("#listForm").submit();
		  }
		  
		// 调整新签界面选择方法
		function saveContractReg(_val){
			var _pk = $("#PK_EMPLOYEE").val();
			if(_pk == null || _pk == ''){
				alert("请先选择员工，在进行操作！");
				return;
			}
			var _url = '<%=path%>/contractRegistration/saveContractRegistrationRet.do?PK_EMPLOYEE='+$("#PK_EMPLOYEE").val()+'&PK_CONTRACTREG='+_val;
			colChooseWindow = $('body').window({
				title : '合同登记管理',
				content : '<iframe frameborder="0" src="'+_url+'"></iframe>',
				width : '600px',
				height : '430px',
				draggable : true,
				isModal : true,
				confirmClose : false
			});
		}
		
		// 跳转到列选择页面
		function toColsChoose() {
			colChooseWindow = $('body').window({
				title : '<fmt:message key="column_selection" />',
				content : '<iframe frameborder="0" src="<%=path%>/contractRegistration/toColumnsChoose.do"></iframe>',
				width : '460px',
				height : '430px',
				draggable : true,
				isModal : true,
				confirmClose : false
			});
		}
		
		function delContractReg(_id){
			var _pk = $("#PK_EMPLOYEE").val();
			if(_pk == null || _pk == ''){
				alert("请先选择员工，在进行操作！");
				return;
			}
			if(confirm("是否删除选中合同登记记录")){
				if(_id=='' || _id== null){
					alert('请选择需要删除记录');
					return;
				}
				var param = {};
				param['PK_CONTRACTREG'] = _id;
				$.post('<%=path%>/contractRegistration/delContractReg.do',param,function(data){
					if(data==1){
						alert("删除成功！");
					}else{
						alert("删除失败！");
					}
					$("#listForm").submit();
				});
			}
		}
		function pageReload(pk_employee){
			 $("#PK_EMPLOYEE").val(pk_employee);
			  $("#listForm").submit();
		}
		
		function loadTool(){
			var tool = $('.tool').toolbar({
				items: [{
						text: '选择员工',
						title: '选择员工',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','0px']
						},
						handler: function(){
							chooseEmployee();
						}
					},{
						text: '新签',
						title: '新签',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','0px']
						},
						handler: function(){
							saveContractReg('');
						}
					},{
						text: '<fmt:message key="update" />',
						title: '<fmt:message key="update" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','0px']
						},
						handler: function(){
							var _id = $("input[name='idList']:checked").val();
							saveContractReg(_id);
						}
					},{
						text: '<fmt:message key="delete" />',
						title: '<fmt:message key="delete" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','0px']
						},
						handler: function(){
							var _id = $("input[name='idList']:checked").val();
							delContractReg(_id);
						}
					}
					/*,{
						text: '<fmt:message key="print" />',
						title: '<fmt:message key="print" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-100px','-40px']
						},
						handler: function(){
						}
					}*/,'-',{
						text: '<fmt:message key="column_selection" />',
						title: '<fmt:message key="column_selection" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-100px','-40px']
						},
						handler: function(){
							toColsChoose();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));	
						}
					}
				]
			});
			return tool;
		}
		</script>
	</body>
</html>