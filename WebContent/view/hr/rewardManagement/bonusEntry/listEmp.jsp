<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="income_or_sit_details" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/scheduleAttendance/listTeamManagement.do" method="post">
			<div class="form-line" style="margin-top:5px;">
				<div class="form-lable" style="padding-left:30px;">
					<fmt:message key="add" /><fmt:message key="date" />:
					<input autocomplete="off" type="text" id="dworkdate" name="dworkdate" class="Wdate text" value="${dworkdate }"/>
					<span style="color: red;">【<fmt:message key="hr_pao" />！】</span>
				</div>
			</div>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 25px;">&nbsp;</td>
								<td><span style="width:60px;"><fmt:message key="number" /></span></td>
								<td><span style="width:100px;"><fmt:message key="Full_name" /></span></td>
								<td><span style="width:80px;"><fmt:message key="hrpost" /></span></td>
								<td><span style="width:60px;"><fmt:message key="hr_Perfect_attendance" /></span></td>
								<td><span style="width:60px;"><fmt:message key="hr_Post_prize" /></span></td>
								<td><span style="width:140px;"><fmt:message key="hr_rejected" /><fmt:message key="reason" /></span></td>
								<td style="visibility: hidden;"></td>
								<td style="visibility: hidden;"></td>
								<td style="visibility: hidden;"></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" id="tableBody">
						<tbody>
							<c:forEach var="bonusEntry" items="${getEmpBonus}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;">
										${status.index+1}
									</td>
									<td ><span style="width:60px;"><c:out value="${bonusEntry.vcode}" /></span></td>
									<td ><span style="width:100px;"><c:out value="${bonusEntry.vname}" /></span></td>
									<td ><span style="width:80px;"><c:out value="${bonusEntry.vstation}" /></span></td>
									<td>
										<input type="text" id="nfullaward" name="nfullaward" style="width:70px;text-align: right;border:0px;" value="0.00" />
									</td>
									<td>
										<input type="text" id="nthisaward" name="nthisaward" style="width:70px;text-align: right;border:0px;" value="0.00" />
									</td>
									<td>
										<input type="text" id="vdismissreasons" name="vdismissreasons" style="width:150px;border:0px;" value="" />
									</td>
									<td style="visibility: hidden;">
										<input type="hidden" id="pk_store" name="pk_store" value="${bonusEntry.pk_store }" />
									</td>
									<td style="visibility: hidden;">
										<input type="hidden" id="pk_hrdept" name="pk_hrdept" value="${bonusEntry.pk_hrdept }" />
									</td>
									<td style="visibility: hidden;">
										<input type="hidden" id="pk_employee" name="pk_employee" value="${bonusEntry.pk_employee }" />
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#dworkdate").click(function(){
		  			new WdatePicker();
		  		});
				var tool=$('.tool').toolbar({
					items:[{
							text: '<fmt:message key="save" />',
							title: '<fmt:message key="save" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-40px']
							},
							handler: function(){
								saveData();
							}
						},'-',{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
// 								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
								$(".close",parent.parent.document).click();
							}
						}
					]
				});

				// 自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),30);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
			});
			
			var saveData = function(){
				var data = {};
				$.ajaxSetup({async:false});
				$("#tableBody").find("tr").each(function(i){
                    data["list["+i+"].nfullaward"] = $.trim($(this).find("td:eq(4)").find("input").val());
                    data["list["+i+"].nthisaward"] = $.trim($(this).find("td:eq(5)").find("input").val());
                    data["list["+i+"].vdismissreasons"] = $.trim($(this).find("td:eq(6)").find("input").val());
					data["list["+i+"].pk_store"] = $.trim($(this).find("td:eq(7)").find("input").val());
					data["list["+i+"].pk_hrdept"] = $.trim($(this).find("td:eq(8)").find("input").val());
					data["list["+i+"].pk_employee"] = $.trim($(this).find("td:eq(9)").find("input").val());
                    data["list["+i+"].dworkdate"] = $("#dworkdate").val();
                    data["list["+i+"].nauditstatus"] = 1;
				});
				$.post("<%=path%>/bonusEntry/addBonusEntry.do",data,function(){
					$('body').window({
						id: 'window_saveStorePos',
						title: '<fmt:message key="save" /><fmt:message key="data" />',
						content: '<iframe id="SaveForm" frameborder="0" src="<%=path%>/operationManagement/toList.do?flag=income"></iframe>',
						width: '350px',
						height: '300px',
						draggable: true,
						isModal: true,
						position : {
							left : 260
						}
					});
				});
			};
		</script>
	</body>
</html>