<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="income_or_sit_details" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/bonusEntry/listBonusEntry.do" method="post">
			<div class="form-line" style="margin-top:5px;">
				<div class="form-lable" style="padding-left:30px;">
					<fmt:message key="business_day" />:
					<input autocomplete="off" type="text" id="dworkdate" name="dworkdate" class="Wdate text" value="${dworkdate }"/>
				</div>
			</div>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 25px;">&nbsp;</td>
								<td style="width:30px;">
									<input type="checkbox" id="chkAll"/>
								</td>
								<td><span style="width:60px;"><fmt:message key="number" /></span></td>
								<td><span style="width:100px;"><fmt:message key="employee_name" /></span></td>
								<td><span style="width:80px;"><fmt:message key="This_month" /><fmt:message key="hr_bonus" /></span></td>
								<td><span style="width:80px;"><fmt:message key="hr_The_examination_and_approval" /></span></td>
								<td><span style="width:80px;"><fmt:message key="hr_The_approver" /></span></td>
								<td><span style="width:80px;"><fmt:message key="hr_The_examination_and_approval" /><fmt:message key="date" /></span></td>
								<td><span style="width:200px;"><fmt:message key="hr_rejected" /><fmt:message key="reason" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="bonusEntry" items="${listBonusEntry}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td style="width:30px; text-align: center;">
										<input type="hidden" id="pk_hrdept" name="pk_hrdept" value="${bonusEntry.pk_hrdept }" />
										<input type="hidden" id="pk_employee" name="pk_employee" value="${bonusEntry.pk_employee }" />
										<input type="checkbox" name="idList" id="chk_<c:out value='${bonusEntry.pk_bonusentry}' />" value="<c:out value='${bonusEntry.pk_bonusentry}' />"/>
									</td>
									<td ><span style="width:60px;"><c:out value="${bonusEntry.vcode}" /></span></td>
									<td ><span style="width:100px;"><c:out value="${bonusEntry.vname}" /></span></td>
									<td ><span style="width:80px;text-align: right;"><c:out value="${bonusEntry.nfullaward + bonusEntry.nthisaward}" /></span></td>
									<td style="width:90px;">
										<c:if test="${0 == bonusEntry.nauditstatus }"><fmt:message key="checked" /></c:if>
										<c:if test="${1 == bonusEntry.nauditstatus }"><fmt:message key="unchecked" /></c:if>
									</td>
									<td><span style="width:80px;">${bonusEntry.vapprovalperson}</span></td>
									<td><span style="width:80px;">${bonusEntry.dworkdate}</span></td>
									<td><span style="width:200px;">${bonusEntry.vdismissreasons}</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#dworkdate").click(function(){
		  			new WdatePicker();
		  		});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-40px']
							},
							handler: function(){
								$('.search-div').slideToggle(100);
							}
						},'-',{
							text: '<fmt:message key="insert" />',
							title: '<fmt:message key="new_role_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								saveTeam();
							}
						},{
							text: '<fmt:message key="update" />',
							title: '<fmt:message key="modify_the_role_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								updateTeam();
							}
						},"-",{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}
					]
				});
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#listForm').submit();
				});
				/* 模糊<fmt:message key="select" />清空 */
				$("#resetSearch").bind('click', function() {
					$('.search-condition input').val('');
				});
				// 自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),20);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
			});
			
			//新增
			function saveTeam(){
				var action='<%=path%>/bonusEntry/treeBonusEntry.do';
				$('body').window({
					id: 'window_saveOutlay',
					title: '<fmt:message key="insert" /><fmt:message key="data" />',
					content: '<iframe id="SaveForm" frameborder="0" src='+action+'></iframe>',
					width: '800px',
					height: '500px',
					draggable: true,
					isModal: true
				});
			}
			
			//修改
			function updateTeam(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() ==1){
					var action='<%=path%>/bonusEntry/updateData.do?pk_team='+checkboxList.filter(':checked').val();
					$('body').window({
						id: 'window_updateOutlay',
						title: '<fmt:message key="update" /><fmt:message key="data" />',
						content: '<iframe id="UpdateForm" frameborder="0" src='+action+'></iframe>',
						width: '650px',
						height: '300px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="data" /><fmt:message key="update" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('UpdateForm')&&window.document.getElementById("UpdateForm").contentWindow.validate._submitValidate()){
											submitFrameForm('UpdateForm','updateForm');
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList && checkboxList.filter(':checked').size() > 1){
					alert('<fmt:message key="you_can_only_modify_a_data" />！');
					return;
				}else {
					alert('<fmt:message key="please_select_information_you_need_to_modify"/>！');
					return ;
				}
			}	
		</script>
	</body>
</html>