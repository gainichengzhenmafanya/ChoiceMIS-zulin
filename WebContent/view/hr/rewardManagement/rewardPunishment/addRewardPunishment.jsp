<%@page import="com.choice.misboh.commonutil.DateJudge"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="branches_and_positions_information" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
	</head>
	<body>
		<div class="form">
			<form id="saveForm" method="post" action="<%=path %>/rewardPunishment/saveData.do">
				<div class="form-line" style="margin-top:10px;margin-left:15px;">
					<div class="form-label">
						<span style="color: blue;font-size: 18px;margin-left:30px;">
						<c:if test="${'' == leave.pk_rewardpunishment or null == leave.pk_rewardpunishment }">
							【<fmt:message key="hr_Newly_build" /><fmt:message key="Reward_and_punishment" />】
						</c:if>
						<c:if test="${'' != leave.pk_rewardpunishment and null != leave.pk_rewardpunishment }">
							【<fmt:message key="update" /><fmt:message key="Reward_and_punishment" />】
						</c:if>
						</span>
					</div>
					<div class="form-label" style="margin-left:50px;">
						<input type="hidden" id="pk_rewardpunishment" name="pk_rewardpunishment" value="${leave.pk_rewardpunishment }" />
						<input type="hidden" id="pk_employee" name="pk_employee" value="${employee.pk_employee }" />
						<input type="hidden" id="pk_hrdept" name="pk_hrdept" value="${employee.pk_hrdept }" />
						<input type="hidden" id="vempno" name="vempno" value="${employee.vempno }" />
						<b><fmt:message key="The_work_number" /></b>：<span id="vempno">${employee.vempno }</span>
					</div>
					<div class="form-label">
						<input type="hidden" id="vname" name="vname" value="${employee.vname }" />
						<b><fmt:message key="Full_name" /></b>：<span id="vname" style="color: blue;">${employee.vname }</span>
					</div>
					<div class="form-label">
						<input type="hidden" id="pk_status" name="pk_status" value="${employee.pk_status }" />
						<b><fmt:message key="status" /></b>：<span id="vstatus" style="color: red">${employee.vstatus }</span>
					</div>
				</div>
				<fieldset style="height:200px;width:560px;border:solid #416AA3 2px;padding-top:20px;margin-top:10px;margin-left:10px;padding-right:15px;">
					<legend style="margin-left:20px;"><fmt:message key="Reward_and_punishment" /><fmt:message key="edit" /></legend>
					<div class="form-line">
						<div class="form-label"><fmt:message key="Reward_and_punishment" /><fmt:message key="type" />：</div>
						<div class="form-input">
							<select id="pk_repunityp" name="pk_repunityp" class="select">
								<c:forEach var="typ" items="${leaveType }" >
									<option value="${typ.vcode }" <c:if test="${leave.pk_repunityp == typ.pk_datatyp }">selected="selected"</c:if> >${typ.vname }</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-label"><fmt:message key="Reward_and_punishment" /><fmt:message key="date" />：</div>
						<div class="form-input">
							<input autocomplete="off" type="text" id="dworkdate" name="dworkdate" class="Wdate text" value="${dworkdate }"/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="Reward_and_punishment" /><fmt:message key="amount" />：</div>
						<div class="form-input">
							<input type="text" id="namount" name="namount" class="text" value="${leave.namount }"/>
						</div>
						<div class="form-label"><fmt:message key="Reward_and_punishment" /><fmt:message key="summary" />：</div>
						<div class="form-input">
							<input type="text" id="vsummary" name="vsummary" class="text" value="${leave.vsummary }"/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="remark" />：</div>
						<div class="form-input">
							<textarea rows="7" style="white-space:wrap;margin-top:5px;width:400px;height:100px;" id="vmemo" name="vmemo" >${leave.vmemo }</textarea>
						</div>
					</div>
				</fieldset>
				<input type="hidden" id="flag" name="flag" class="text" value="${flag }"/>
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
		  		$("#dworkdate").click(function(){
		  			new WdatePicker();
		  		});
				
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'vmemo',
						validateType:['maxLength'],
						param:[150],
						error:['<fmt:message key="the_maximum_length" />350']
					}]
				});
			});
			
			//根据奖惩类型判断金额正负
			function checkAmount(){
				var repunityp = $("#pk_repunityp").val();
				var namount = $("#namount").val();
				
				if (namount.match("^\\d+(\\.\\d+)?$") || namount.match("^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$")) {
					if (0 == repunityp || 1 == repunityp || 2 == repunityp || 4 == repunityp || 5 == repunityp || 11 == repunityp) {
						if (0 > namount) {
							alert('<fmt:message key="hr_Reward" /><fmt:message key="amount" />，<fmt:message key="cannot_be_negative" />!');
							$("#namount").focus();
							return true;
						} else {
							$("#saveForm").submit();
						}
					} else {
						if (namount > 0) {
							alert('<fmt:message key="hr_Punishment" /><fmt:message key="amount" />，<fmt:message key="hr_Not_positive" />!');
							$("#namount").focus();
							return true;
						} else {
							$("#saveForm").submit();
						}
					}
				} else {
					alert('<fmt:message key="amount" /><fmt:message key="only_digits" />!');
					$("#namount").val('0.00');
					$("#namount").focus();
					return true;
				}
			}
		</script>
	</body>
</html>