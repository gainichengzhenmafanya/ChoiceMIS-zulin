<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="income_or_sit_details" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/queryScheduling/listQueryScheduling.do" method="post">
				<div class="form-line">
					<input type="hidden" id="pk_hrdept" name="pk_hrdept"  value="${pk_hrdept }"/>
					<input type="hidden" id="pk_team" name="pk_team"  value="${pk_team }"/>
					<input type="hidden" id="pk_store" name="pk_store"  value="${pk_store }"/>
					<div class="form-label"><fmt:message key="startdate" /></div>
					<div class="form-input">
						<input autocomplete="off" type="text" id="vbdat" name="vbdat" class="Wdate text" value="${vbdat }" />
					</div>
					<div class="form-label"><fmt:message key="enddate" /></div>
					<div class="form-input">	
						<input autocomplete="off" type="text" id="vedat" name="vedat" class="Wdate text" value="${vedat }" />
					</div>
				</div>
			<div class="grid" >
				<div class="table-head" style="width:${count * 200}px;">
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width:25px;">&nbsp;</td>
								<td style="width:80px;"><fmt:message key="Post" /></td>
								<td style="width:100px;"><fmt:message key="Check_work_attendance" /><fmt:message key="Card_number" /></td>
								<td style="width:80px;"><fmt:message key="Full_name" /></td>
								<c:forEach var="list" items="${listDate }">
									<td style="width:120px;">${list }</td>
								</c:forEach>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body" style="width:${count * 200}px;">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="crew" items="${listShift}" varStatus="status">
								<tr>
									<td class="num" style="width:25px;">${status.index+1}</td>
									<td style="width:80px;"><c:out value="${crew['VSTATION']}" /></td>
									<td style="width:100px;"><c:out value="${crew['VKQCARDNO']}" /></td>
									<td style="width:80px;"><c:out value="${crew['VEMPNAME']}" /></td>
									<c:forEach var="list" items="${listDate }">
										<td style="width:120px;">${crew[list]}</td>
									</c:forEach>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				if ('' == '${vbdat}') {
					$("#vbdat").htmlUtils("setDate","now");
				}
				if ('' == '${vedat}') {
					$("#vedat").htmlUtils("setDate","now");
				}
				$("#vbdat").click(function(){
		  			new WdatePicker({maxDate:'#F{$dp.$D(\'vedat\')}'});
		  		});
		  		$("#vedat").click(function(){
		  			new WdatePicker({minDate:'#F{$dp.$D(\'vbdat\')}'});
		  		});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-40px']
							},
							handler: function(){
								var vbdat = $("#vbdat").val();
								var vedat = $("#vedat").val();
								if ('' == vbdat) {
									alert('<fmt:message key="startdate" /><fmt:message key="cannot_be_empty" />!');
									return true;
								} else if ('' == vedat) {
									alert('<fmt:message key="enddate" /><fmt:message key="cannot_be_empty" />!');
									return true;
								} else {
									$("#listForm").submit();
								}
							}
						},'-',{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}
					]
				});
				// 自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),20);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
			});
		</script>
	</body>
</html>