<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="income_or_sit_details" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/crewScheduling/listCrewScheduling.do" method="post">
				<div class="form-line">
					<input type="hidden" id="pk_hrdept" name="pk_hrdept"  value="${pk_hrdept }"/>
					<input type="hidden" id="pk_team" name="pk_team"  value="${pk_team }"/>
					<input type="hidden" id="pk_store" name="pk_store"  value="${pk_store }"/>
					<div class="form-label"><fmt:message key="startdate" /></div>
					<div class="form-input">
						<input autocomplete="off" type="text" id="vbdat" name="vbdat" class="Wdate text" value="${vbdat }" />
					</div>
					<div class="form-label"><fmt:message key="enddate" /></div>
					<div class="form-input">	
						<input autocomplete="off" type="text" id="vedat" name="vedat" class="Wdate text" value="${vedat }" />
					</div>
<%-- 					<div class="form-label"><fmt:message key="scm_flight" /><fmt:message key="coding" /></div> --%>
<!-- 					<div class="form-input">	 -->
<%-- 						<input type="text" id="vcode" name="vcode" class="text" value="${vcode }"/> --%>
<!-- 					</div> -->
				</div>
			<div class="grid" >
				<div style="position: absolute;margin-left:500px;margin-top:20px;">
					<c:if test="${0 != count }"><span style="color: red;">【<fmt:message key="ht_note" />！】</span></c:if>
				</div>
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 25px;">&nbsp;</td>
								<td style="width:30px;"><input type="checkbox" id="chkAll"/></td>
								<td><span style="width:120px;"><fmt:message key="date" /></span></td>
								<td><span style="width:121px;"><fmt:message key="coding" /></span></td>
								<td><span style="width:121px;"><fmt:message key="name" /></span></td>
								<td style="visibility: hidden;"></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" id="tableBody">
						<tbody>
							<c:forEach var="crew" items="${listShift}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" name="idList" id="${status.index+1}" value="${status.index+1}"/>
									</td>
									<td id="date${status.index+1}"><span style="width:120px;"><c:out value="${crew.dworkdate}" /></span></td>
									<td>
										<input type="text" id="vcode${status.index+1}" name="vcode${status.index+1}" style="border: 0px;" 
											   readonly="readonly" value="${crew.vcode}"/>
									</td>
									<td>
										<input type="text" id="vname${status.index+1}" name="vname${status.index+1}" style="border: 0px;" 
											   readonly="readonly" value="${crew.vname}"/>
									</td>
									<td style="visibility: hidden;">
										<input type="hidden" id="pk_shift${status.index+1}" name="pk_shift${status.index+1}" value="${crew.pk_shift }"/>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				if ('' == '${vbdat}') {
					$("#vbdat").htmlUtils("setDate","now");
				}
				if ('' == '${vedat}') {
					$("#vedat").htmlUtils("setDate","now");
				}
				$("#vbdat").click(function(){
		  			new WdatePicker({maxDate:'#F{$dp.$D(\'vedat\')}'});
		  		});
		  		$("#vedat").click(function(){
		  			new WdatePicker({minDate:'#F{$dp.$D(\'vbdat\')}'});
		  		});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-40px']
							},
							handler: function(){
								var pk_team = $("#pk_team").val();
								var vbdat = $("#vbdat").val();
								var vedat = $("#vedat").val();
								if ('' == pk_team) {
									alert('<fmt:message key="please_select" /><fmt:message key="hr_team" />!');
									return true;
								} else if ('' == vbdat) {
									alert('<fmt:message key="startdate" /><fmt:message key="cannot_be_empty" />!');
									return true;
								} else if ('' == vedat) {
									alert('<fmt:message key="enddate" /><fmt:message key="cannot_be_empty" />!');
									return true;
								} else {
									$("#listForm").submit();
								}
							}
						},'-',{
							text: '<fmt:message key="add" /><fmt:message key="scm_flight" />',
							title: '<fmt:message key="add"/><fmt:message key="data"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								addShift();
							}
						},{
							text: '<fmt:message key="save" />',
							title: '<fmt:message key="save"/><fmt:message key="data"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								saveData();
							}
						},{
							text: '<fmt:message key="delete" />',
							title: '<fmt:message key="delete"/><fmt:message key="data"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								deleteData();
							}
						},"-",{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}
					]
				});
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#listForm').submit();
				});
				/* 模糊<fmt:message key="select" />清空 */
				$("#resetSearch").bind('click', function() {
					$('.search-condition input').val('');
				});
				// 自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),20);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				//双击弹出<fmt:message key="update" />
				$('.grid').find('.table-body').find('tr').live("dblclick", function () {
					$(":checkbox").attr("checked", false);
					$(this).find(':checkbox').attr("checked", true);
					addShift();
					if($(this).hasClass("show-firm-row"))return;
					$('.grid').find('.table-body').find('tr').removeClass("show-firm-row");
					$(this).addClass("show-firm-row");
				 });
				
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }
				     else
				     {
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				 });
				
// 				 loadCheck();
			});
			
			//实现复选框单选
			function loadCheck(){
				$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
					$(this).closest('.table-body').find(':checkbox').not($(this)).removeAttr("checked");
					event.stopPropagation();
				});
			}
			
			function setShift(data){
				var index = $('.grid').find('.table-body').find(':checkbox').filter(':checked').val();
	  	 		$("#pk_shift"+index).val(data.code);
	  	 		$("#vcode"+index).val(data.show);
	  	 		$("#vname"+index).val(data.name);
	  	 	}
			
			//添加班次
			function addShift(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				var index = checkboxList.filter(':checked').val();
				var pk_team = $("#pk_team").val();
				if ('' == pk_team) {
					alert('<fmt:message key="please_select" /><fmt:message key="hr_team" />！');
					return true;
				}
				
				if(checkboxList && checkboxList.filter(':checked').size() ==1){
					var pk_store = $("#pk_store").val();
					var pk_shift = $("#pk_shift"+index).val();
					var url = "<%=path%>/shiftSetting/toChooseShift.do";
						url += "?callBack=setShift&domId=selected&single=true&pk_store="+pk_store+"&pk_shift="+pk_shift;
					return $('body').window({
						id: 'window_chooseSiteType',
						title: '<fmt:message key="select1" /><fmt:message key="scm_flight" />',
						content: '<iframe id="listSiteTypeFrame" frameborder="0" src='+url+'></iframe>',
						width: 400,
						height: 350,
						confirmClose: false,
						draggable: true,
						isModal: true
					});
				}else if(checkboxList && checkboxList.filter(':checked').size() > 1){
					alert('<fmt:message key="you_can_only_modify_a_data" />！');
					return;
				}else {
					alert('<fmt:message key="please_select"/><fmt:message key="date"/>！');
					return ;
				}
			}
			
			//保存
			var saveData = function(){
				$.ajaxSetup({async:false});
				var data = {};
				var vbdat = $('#vbdat').val();
				var vedat = $('#vedat').val();
				var pk_team = $("#pk_team").val();
				var pk_hrdept = $("#pk_hrdept").val();
				if ('' == pk_team) {
					alert('<fmt:message key="please_select" /><fmt:message key="hr_team" />！');
					return true;
				}
				$("#tableBody").find("tr").each(function(i){
					data["listCrew["+i+"].dworkdate"]=$.trim($(this).find("td:eq(2)").find("span").text());
					data["listCrew["+i+"].vcode"]=$("#vcode"+(i+1)).val();
					data["listCrew["+i+"].vname"]=$("#vname"+(i+1)).val();
                    data["listCrew["+i+"].pk_shift"]=$("#pk_shift"+(i+1)).val();
                    data["listCrew["+i+"].pk_team"]=pk_team;
                    data["listCrew["+i+"].pk_store"]=$("#pk_store").val();
                    data["listCrew["+i+"].pk_hrdept"]=pk_hrdept;
				});
				
				//表格行数
				var tab = document.getElementById("tableBody") ;
			    var rows = tab.rows.length;
				
			    if (0 == rows) {
			    	alert('<fmt:message key="data_for_the_air_to_operate" />!');
			    	return true;
			    }
				
				$.post("<%=path%>/crewScheduling/saveData.do?vbdat="+vbdat+"&vedat="+vedat+"&pk_team="+pk_team,data,function(){
					$('body').window({
						id: 'window_saveStorePos',
						title: '<fmt:message key="save" /><fmt:message key="data" />',
						content: '<iframe id="SaveForm" frameborder="0" src="<%=path%>/misbohcommon/toList.do"></iframe>',
						width: '350px',
						height: '300px',
						draggable: true,
						isModal: true
					});
				});
			};
			
			//删除
			function deleteData(){
				var pk_team = $("#pk_team").val();
				var checkboxList = [];
				var str=document.getElementsByName("idList"); 
				for (var i=0;i<str.length;i++ ){
					if(str[i].checked){
						checkboxList.push($("#date"+(i+1)).text());
					}
				}
				
				if ('' == pk_team) {
					alert('<fmt:message key="please_select" /><fmt:message key="hr_team" />!');
					return true;
				}
				
				if(checkboxList.length > 0){
					if(confirm('<fmt:message key="delete_data_confirm" />？')){
						var action = '<%=path%>/crewScheduling/deleteData.do?pk_team='+pk_team+'&dworkdate='+checkboxList.join(",");
						$('body').window({
							title: '<fmt:message key="delete" /><fmt:message key="data" />',
							content: '<iframe frameborder="0" src='+action+'></iframe>',
							width: '340px',
							height: '255px',
							draggable: true,
							isModal: true
						});
					}
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				}
			}	
		</script>
	</body>
</html>