<%@ page import="com.choice.orientationSys.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
	String rootId = StringConstant.ROOT_ID;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>人力资源组织结构</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		</head>
	<body>
		<div class="leftFrame" style="width: 20%;float:left;">
        	<div id="toolbar"></div>
	    	<div class="treePanel" style=" overflow-y: scroll;">
		        <script src="<%=path%>/js/tree/MzTreeView10.js" type="text/javascript"></script>
	        	<script type="text/javascript">
	          		var tree = new MzTreeView("tree");
	          
	          		tree.nodes['0_~'] = 'text:<fmt:message key="the_current_store" />;method:changeUrl("0","~","<fmt:message key="the_current_store" />")';
	          		<c:forEach var="marsale" items="${treeCrewScheduling}" varStatus="status">
	          			tree.nodes["${marsale['PARENTID']}_${marsale['CHILDRENID']}"] 
		          		= "text:${marsale['VCODE']}-${marsale['VNAME']}; method:changeUrl('${marsale['ORDERID']}','${marsale['CHILDRENID']}','${marsale['VCODE']}','${marsale['VNAME']}','${marsale['PARENTID']}')";
	          		</c:forEach>
	          		 tree.setIconPath("<%=path%>/image/tree/none/");
	          		document.write(tree.toString());
	        	</script>
	    	</div>
    	</div>
    	<div  class="mainFrame" style="width:80%;" id="dataList">
    		<iframe src="" id="listFrame" frameborder="0" scrolling="no"></iframe>
    	</div>
	    <input type="hidden" id="pk_father" name="pk_father" />
	    <input type="hidden" id="pk_id" name="pk_id" value="${pk_id }" />
	    <form id="listForm" action="<%=path%>/crewScheduling/treeCrewScheduling.do" method="post">
			<div class="search-div" style="margin-left: 0px;">
				<div class="search-condition">
					<table class="search-table" cellspacing="0" cellpadding="0" >
						<tr>
							<td class="c-left"><fmt:message key="coding" />：</td>
							<td><input type="text" id="vcode" name="vcode" class="text" /></td>
							<td class="c-left"><fmt:message key="name" />：</td>
							<td><input type="text" id="vname" name="vname" class="text" onkeyup="getSpInit(this,'vinit');" /></td>
							<td class="c-left"><fmt:message key="abbreviation" />：</td>
							<td><input type="text" id="vinit" name="vinit" class="text" /></td>
							<td class="c-left"><fmt:message key="enable_state" />：</td>
							<td>
								<select id="enablestate" name="enablestate" class="select" style="height: 22px;margin-top: 3px; border: 1px solid #999999;">
									<option value="1" ><fmt:message key="not_enabled" /></option>
									<option value="2" selected="selected"><fmt:message key="have_enabled" /></option>
									<option value="3" ><fmt:message key="stop_enabled" /></option>
								</select>
							</td>
						</tr>
					</table>
				</div>
				<div class="search-commit">
			       	<input type="button" class="search-button" id="search" value='<fmt:message key="enter" />'/>
			       	<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/common/teleFunc-${sessionScope.locale}.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
		<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>	
		<script type="text/javascript">
			function changeUrl(orderid,pk_id,code,name,father){
				$('#pk_father').val(father);
				tree.focus(pk_id);
				loadDataList(orderid,pk_id,father,code,name);
				$('#pk_id').val(pk_id);
				$('#vcode').val(code);
				$('#vname').val(name);
			}
			//加载右侧数据
			function loadDataList(orderid,pk_id,father,code,name){
				var action = "<%=path%>/crewScheduling/listCrewScheduling.do";
				if(orderid=='1'){
					action+= "?pk_store="+pk_id;
				}else if(orderid=='2'){
					action+= "?pk_hrdept="+pk_id+"&pk_store="+father;
				}else if(orderid=='3'){
					action+= "?pk_team="+pk_id+"&pk_hrdept="+father;
				}
				$('#listFrame').attr('src',action);
			}
			
			$(document).ready(function(){
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'vcode',
						validateType:['maxLength','withOutSpecialChar'],
						param:[10,'T'],
						error:['<fmt:message key="the_maximum_length" />10','<fmt:message key="no_contain_special_char" />!']
					},{
						type:'text',
						validateObj:'vname',
						validateType:['maxLength','withOutSpecialChar'],
						param:[50,'T'],
						error:['<fmt:message key="the_maximum_length" />50','<fmt:message key="no_contain_special_char" />!']
					},{
						type:'text',
						validateObj:'vinit',
						validateType:['maxLength','withOutSpecialChar'],
						param:[25,'T'],
						error:['<fmt:message key="the_maximum_length" />25','<fmt:message key="no_contain_special_char" />!']
					}]
				});
				var expanFlag = true;
				var toolbar = $('#toolbar').toolbar({
					items: [{
							text: '<fmt:message key="expandAll" />',
							title: '<fmt:message key="expandAll"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-80px']
							},
							handler: function(){
								tree.expandAll();
							}
						},{
							text: '<fmt:message key="refresh" />',
							title: '<fmt:message key="refresh"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','0px']
							},
							handler: function(){
								refreshTree();
							}
						},{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','0px']
							},
							handler: function(){
								$('.search-div').slideToggle(100);
							}
						}
					]
				});
				
				setElementHeight('.treePanel',['#toolbar'],$(document.body),29);//计算.treePanel的高度
				setElementHeight('#dataList',['#toolbar'],$(document.body),0);//计算右侧的高度
				changeUrl('0','${pk_id}','${vcode}','${vname}'); //初次加载全部
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#listForm').submit();
				});
				/* 模糊<fmt:message key="select" />清空 */
				$("#resetSearch").bind('click', function() {
					$('.search-condition input').val('');
				});
			});
			
			//刷新
			function refreshTree(){
				window.location.href = '<%=path%>/crewScheduling/treeCrewScheduling.do';
	    	}
	    
		    //查询
		    $("#search").bind('click', function() {
				$('.search-div').hide();
// 				window.location.href = '<%=path%>/crewScheduling/treeCrewScheduling.do'; 
				if(validate._submitValidate()){
					$("#listForm").submit();
				}
				expandAll();
			});
		</script>
	</body>
</html>