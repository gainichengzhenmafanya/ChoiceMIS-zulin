<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="branches_and_positions_information" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
	</head>
	<body>
		<div class="form">
			<form id="saveForm" method="post" action="<%=path %>/scheduleAttendance/saveData.do?flag=add">
				<div class="form-line" style="margin-top:50px;">
					<div class="form-label"><span class="red">*</span><fmt:message key="add" /><fmt:message key="number_of_people" />：</div>
					<div class="form-input">
						<input type="text" id="number" name="number" class="text" />
					</div>
				</div>
				<input type="hidden" id="listTd" name="listTd" class="text" value="${td }" />
				<input type="hidden" id="totalhour" name="totalhour" class="text" value="${totalhour }" />
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#number").focus();
				
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'number',
						validateType:['canNull','intege','maxLength'],
						param:['F','F',8],
						error:['<fmt:message key="number_of_people" /><fmt:message key="cannot_be_empty" />',
						       '<fmt:message key="number_of_people" /><fmt:message key="the_only_positive_integer" />！',
						       '<fmt:message key="the_maximum_length" />8']
					}]
				});
			});
			
			function add(){
				var number = $("#number").val();
				var listTd = $("#listTd").val();
				var totalhour = $("#totalhour").val();
// 				var listTd = td.split(",");
				
				//组合单元格id和人数，作为返回值
				return listTd + ":" + number + ":" + totalhour;
			}
		</script>
	</body>
</html>