<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="income_or_sit_details" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/classTableSetting/listClassTableSetting.do" method="post">
			<div class="form-line">
				<input type="hidden" id="pk_store" name="pk_store"  value="${pk_store }"/>
				<div class="form-label" style="width:30px;margin-left:10px;"><span style="color: red">*</span><fmt:message key="date" />：</div>
				<div class="form-input">
					<input autocomplete="off" type="text" id="dworkdate" name="dworkdate" class="Wdate text" value="${dworkdate }" />
				</div>
			</div>	
			<div class="grid" >
				<div class="gridtable" style="width:1280px;">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width:25px;">&nbsp;</span></td>
									<td><span style="width:80px;text-align: center;"><fmt:message key="the_workstation" />\<fmt:message key="time" /></span></td>
									<c:forEach var="time" items="${listTime }">
										<td colspan="2"><span style="width:35px;text-align: center;">${time }:00</span></td>
									</c:forEach>
									<td><span style="width:50px;"><fmt:message key="total" /></span></td>
								</tr>
							</thead>
						</table>	
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0" id="tableBody">
							<tbody>
								<c:forEach var="dataTyp" items="${listDataTyp }" varStatus="status">
									<tr id="tr${status.index+1}">
										<td><span style="width:25px;">${status.index+1}</span></td>
										<td>
											<span style="width:80px;text-align: center;">${dataTyp.vname }</span>
											<input type="hidden" id="work${status.index+1}" name="work${status.index+1}" value="${dataTyp.pk_datatyp }" />
										</td>
										<c:forEach var="list" items="${listInt }" varStatus="index">
											<td <c:if test="${'true' == flag }">
													onmouseup="up(this,'${status.index+1}_${index.index+1}')" 
													onmousedown="down('${status.index+1}_${index.index+1}')" 
													onclick="addColor(this,'${status.index+1}_${index.index+1}')" 
												</c:if>
												id="${status.index+1}_${index.index+1}" 
												style="width:22px;color: red;text-align: center;">
											</td>
											<input type="hidden" id="time${status.index+1}_${index.index+1}"
												   name="time${status.index+1}_${index.index+1}" value="${list }" />
										</c:forEach>
										<td id="td${status.index+1}" align="right"><span style="width:50px;"></span></td>
									</tr>
								</c:forEach>
								<tr id="trlast" style="visibility: hidden;">
									<td><span style="width:25px;"></span></td>
									<td>
										<span style="width:80px;text-align: center;"></span>
									</td>
									<c:forEach var="list" items="${listInt }" varStatus="index">
										<td style="width:22px;color: red;text-align: center;"></td>
									</c:forEach>
									<td id="tdlast" align="right"><span style="width:50px;"></span></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<input type="hidden" id="mouseDown" name="mouseDown" />
			<input type="hidden" id="selectTd" name="selectTd" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#dworkdate").click(function(){
		  			new WdatePicker();
		  		});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-40px']
							},
							handler: function(){
								$("#listForm").submit();
							}
						},'-',{
							text: '<fmt:message key="scm_set_up_the" /><fmt:message key="number_of_people" />',
							title: '<fmt:message key="scm_set_up_the" /><fmt:message key="number_of_people" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								var selectTd = $("#selectTd").val();
								var totalhour = $("#td"+selectTd.split("_")[0]).text();
								if ('false' == '${flag}') {
									alert('<fmt:message key="hr_available" /><fmt:message key="scm_set_up_the" /><fmt:message key="number_of_people" />!');
									return true;
								} else {
									if ("3px solid rgb(255, 0, 0)" != $("#"+selectTd).css("border")) {
										alert('<fmt:message key="hr_Select_the" />!');
										return true;
									} else {
										addPeopleNumber(selectTd,totalhour);
									}
								}
							}
						},{
							text: '<fmt:message key="copy" />',
							title: '<fmt:message key="copy"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								copy();
							}
						},{
							text: 'Excel',
							title: 'Excel<fmt:message key="export"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								var dworkdate = $("#dworkdate").val();
								window.location.href = "<%=path%>/classTableSetting/createExcel.do?reportName=classTableSetting&dworkdate="+dworkdate;
							}
						},{
							text: '<fmt:message key="save" />',
							title: '<fmt:message key="save"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								if ('false' == '${flag}') {
									alert('<fmt:message key="hr_hdcnbs" />!');
									return true;
								} else {
									save();
								}
							}
						},{
							text: '<fmt:message key="delete" />',
							title: '<fmt:message key="delete"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								var selectTd = $("#selectTd").val();
								if ('false' == '${flag}') {
									alert('<fmt:message key="hr_historical" />!');
									return true;
								} else {
									if ("3px solid rgb(255, 0, 0)" != $("#"+selectTd).css("border")) {
										alert('<fmt:message key="hr_Select_the" />!');
										return true;
									} else {
										deleteData(selectTd);
									}
								}
							}
						},'-',{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}
					]
				});
				// 自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),55);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				
				var dworkdate = '${dworkdate}';
				var pk_store = '${pk_store}';
				$.ajax({
					url:'<%=path%>/classTableSetting/getTableSetting.do?dworkdate='+dworkdate+'&pk_store'+pk_store,
					type:'post',
					async:false,
					timeout: 30000,
					success: function (msg){
						var table = document.getElementById("tableBody");
						var row = table.rows.length;
						var cell = table.rows[0].cells.length;
						for(var i = 0; i < msg.length; i++){
							for (var rowI = 0; rowI <= row ; rowI++) {
								if ($("#work"+rowI).val() == msg[i].workstation) {
									for (var cellI = 1; cellI < cell; cellI++) {
										if ($("#time"+rowI+"_"+cellI).val() == msg[i].vbegintime) {
											$("#"+rowI+"_"+cellI).attr("bgcolor", "green");
											
											//合并列
											$("#"+rowI+"_"+cellI).attr("colspan",Number(msg[i].vendtime).toFixed(0));
 											
											//隐藏列
											for (var k = 1; k < Number(msg[i].vendtime).toFixed(0); k++) {
												$("#"+rowI+"_"+(cellI + k)).css("display","none");
											}
											$("#"+rowI+"_"+cellI).text(msg[i].npeople);
											$("#td"+rowI).text(msg[i].totalhour);
											$("#td"+rowI).attr("width","60px");
										}
									}
								}
							}
						}
					}
				});
			});
			
			function addColor(td,id){
				var totalhour = $("#td"+id.split("_")[0]).text();//获取点击单元格所在行的总工时
				
				var hiddenId = $("#selectTd").val();	//获取隐藏域的id
				
				//如果早已有了变红的单元格，则将红色去掉
				if ("3px solid rgb(255, 0, 0)" == $("#"+id).css("border")) {
					$("#"+id).removeAttr("style");
					$("#"+id).css("width","22px");
					$("#"+id).css("color","red");
					$("#"+id).css("text-align","center");
				} else {
					//若点击的单元格背景色已是绿色，则将此单元格边框变为红色
					if ("green" == $("#"+id).attr("bgcolor")) {
						$("#"+hiddenId).removeAttr("style");
						$("#"+hiddenId).css("width","22px");
						$("#"+hiddenId).css("color","red");
						$("#"+hiddenId).css("text-align","center");
						$("#"+id).css("border","solid red");
					} else {
						$("#"+hiddenId).removeAttr("style");
						$("#"+hiddenId).css("width","22px");
						$("#"+hiddenId).css("color","red");
						$("#"+hiddenId).css("text-align","center");
					}
				}
				
				//将点击的单元格的背景色填充为绿色，并设置人数
				if(undefined == $("#"+id).attr("colspan") || 1 == $("#"+id).attr("colspan")){
					$("#"+id).attr("bgcolor","green");
					$("#"+id).attr("colspan",1);
					
					if (12 == $("#"+id).text().length) {
						addPeopleNumber(id,totalhour);
					}
				}
				
				//将点击的单元格id放入隐藏域
				$("#selectTd").val(id);
			}
			
			////鼠标左键弹起后获取单元格id，并进行判断
			function up(td,a){
				var mouseDown = $("#mouseDown").val();					//获取隐藏域中鼠标按下后得到的单元格id
				var totalhour = $("#td"+mouseDown.split("_")[0]).text();//所选行的总工时	
				var rowCha = a.split("_")[0] - mouseDown.split("_")[0];	//获取行差(单元格id格式："行_列")
				var celCha = a.split("_")[1] - mouseDown.split("_")[1];	//获取列差(单元格id格式："行_列")
				var td = [];
				
				//行差等于0，则点击、弹起的单元格位于同一行；大于0，则鼠标从上往下移动；小于0，则鼠标从下往上移动
				if (rowCha == 0){
					
					//列差大于0，则从左往右移动；小于0，则从右往左
					if (celCha > 0) {
						for (var i = mouseDown.split("_")[1]; a.split("_")[1] >= i; i++) {
							td.push(a.split("_")[0]+"_"+i);
						}

						if ('' != td.join(",")) {
							//为第一个单元格(点下的单元格)填充背景色
							$("#"+mouseDown).attr("bgcolor","green");
							
							//合并列
							$("#"+mouseDown).attr("colspan",celCha+1);
							
							//隐藏列
							for (var i = 1; i <= celCha; i++) {
								$("#"+mouseDown.split("_")[0]+"_"+((Number(mouseDown.split("_")[1])+Number(i)))).remove();
							}
							addPeopleNumber(td.join(","),totalhour);
						}
					}
				}
			}
			
			//鼠标左键点击时获取单元格id，放入隐藏域中
			function down(i){
				$("#mouseDown").val(i);
			}
			
			//添加人数:listTd单元格id，totalhour单元格所在行的工时
			function addPeopleNumber(listTd,totalhour){
				var action='<%=path%>/classTableSetting/addPeopleNumber.do?td='+listTd+'&totalhour='+totalhour;
				$('body').window({
					title: '<fmt:message key="scm_set_up_the" /><fmt:message key="number_of_people" />',
					content: '<iframe id="SaveForm" frameborder="0" src='+action+'></iframe>',
					width: '340px',
					height: '255px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="enter" />',
								title: '<fmt:message key="enter"/>',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('SaveForm')&&window.document.getElementById("SaveForm").contentWindow.validate._submitValidate()){
										//获取人数和单元格id组合
										var numberTd = window.document.getElementById("SaveForm").contentWindow.add();
										$('.close').click();
										
										//获取所选单元格id数组
										var tdId = numberTd.split(":")[0].split(",");
										
										//获取人数
										var number = numberTd.split(":")[1];
										
										//获取工时
										var totalhour = numberTd.split(":")[2] == '' ? 0 : numberTd.split(":")[2];
										
										var value = $("#"+tdId[0]).text();
										
										//获取行号
										var row = numberTd.split(":")[0].split(",")[0].split("_")[0];
										$("#td"+row).text(Number(tdId.length * 0.5) + Number(totalhour));
										
										//当只选中一个单元格，且单元格中没有数据时，合计中只放入原来的数据
										if (tdId.length == 1 && 12 != value.length) {
											$("#td"+row).text(totalhour);
										} else {
											$("#td"+row).text(Number(tdId.length * 0.5) + Number(totalhour));
										}
										
										//将人数放入第一个单元格中
										$("#"+tdId[0]).text(number);
									}
								}
							}
						]
					}
				});
			}
			
			//保存数据
			function save(){
				var tab = document.getElementById("tableBody");
				var rows = tab.rows.length;
				var data = {};
				$.ajaxSetup({async:false});
				var a = 0;
				
				for (var i = 0; i < rows; i++) {
					if ('' != $("#td"+(i+1)).text()) {
						for (var k = 1; k <= 48; k++) {
							if ($("#"+(i+1)+"_"+k).attr("bgcolor") == "green") {
								var endTd = Number(k)+Number($("#"+(i+1)+"_"+k).attr("colspan"));
								data["listClassTableSetting["+a+"].vbegintime"] = $("#time"+(i+1)+"_"+k).val();
								data["listClassTableSetting["+a+"].vendtime"] = $("#time"+(i+1)+"_"+endTd).val();
								data["listClassTableSetting["+a+"].totalhour"] = $("#td"+(i+1)).text();
								data["listClassTableSetting["+a+"].npeople"] = $("#"+(i+1)+"_"+k).text();
								data["listClassTableSetting["+a+"].pk_store"] = $("#pk_store").val();
				                data["listClassTableSetting["+a+"].dworkdate"] = $("#dworkdate").val();
				                data["listClassTableSetting["+a+"].workstation"] = $("#work"+(i+1)).val();
				                a++;
							}
						}
					}
				}
				data["pk_store"] = $("#pk_store").val();
				data["dworkdate"] = $("#dworkdate").val();
				$.post("<%=path%>/classTableSetting/saveData.do",data,function(){
					$('body').window({
						id: 'window_saveStorePos',
						title: '<fmt:message key="save" /><fmt:message key="data" />',
						content: '<iframe id="SaveForm" frameborder="0" src="<%=path%>/operationManagement/toList.do?flag=income"></iframe>',
						width: '350px',
						height: '300px',
						draggable: true,
						isModal: true
					});
				});
			}
			
			//复制
			function copy(){
				var action='<%=path%>/classTableSetting/selectDate.do';
				$('body').window({
					title: '<fmt:message key="select1" /><fmt:message key="date" />',
					content: '<iframe id="SaveForm" frameborder="0" src='+action+'></iframe>',
					width: '340px',
					height: '255px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="enter" />',
								title: '<fmt:message key="enter"/>',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('SaveForm')&&window.document.getElementById("SaveForm").contentWindow.validate._submitValidate()){
										//获取日期
										var dworkdate = window.document.getElementById("SaveForm").contentWindow.add();
										var param = {};
// 										$("#dworkdate").val(dworkdate);
										$('.close').click();
										param['dworkdate'] = dworkdate;
										param['pk_store'] = $("#pk_store").val();
										$.post("<%=path%>/classTableSetting/getTableSetting.do",param,function(data){
											if (0 != data.length) {
												if(confirm('<fmt:message key="hr_overwrite" />？')){
													save();
												}
											} else {
												var data = {};
												$.ajaxSetup({async:false});
												data["pk_store"] = $("#pk_store").val();
												data["dworkdate"] = $("#dworkdate").val();
												data["dworkdate1"] = dworkdate;
												$.post("<%=path%>/classTableSetting/copy.do",data,function(){
													$('body').window({
														id: 'window_saveStorePos',
														title: '<fmt:message key="save" /><fmt:message key="data" />',
														content: '<iframe id="SaveForm" frameborder="0" src="<%=path%>/operationManagement/toList.do?flag=income"></iframe>',
														width: '350px',
														height: '300px',
														draggable: true,
														isModal: true
													});
												});
											}
										});
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			
			//删除
			function deleteData(selectTd){
				if(confirm('<fmt:message key="delete_data_confirm" />？')){
					var vbegintime = $("#time"+selectTd).val();
					var dworkdate = $("#dworkdate").val();
					var pk_store = $("#pk_store").val();
					var workstation = $("#work"+selectTd.split("_")[0]).val();
					var action = '<%=path%>/classTableSetting/deleteData.do?vbegintime='+vbegintime+
							'&dworkdate='+dworkdate+'&pk_store='+pk_store+'&workstation='+workstation;
					$('body').window({
						title: '<fmt:message key="delete" /><fmt:message key="data" />',
						content: '<iframe frameborder="0" src='+action+'></iframe>',
						width: '340px',
						height: '255px',
						draggable: true,
						isModal: true
					});
				}
			}
		</script>
	</body>
</html>