<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="branches_and_positions_information" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
	</head>
	<body>
		<div class="form">
			<form id="saveForm" method="post" action="<%=path %>/shiftSetting/saveData.do?flag=add">
				<div class="form-line" style="margin-top:20px;">
					<div class="form-label"><span class="red">*</span><fmt:message key="coding" />：</div>
					<div class="form-input">
						<input type="text" id="vcode" name="vcode" class="text" value="${shift.vcode }" />
					</div>
					<div class="form-label"><span class="red">*</span><fmt:message key="name" />：</div>
					<div class="form-input">
						<input type="text" id="vname" name="vname" class="text" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="starttime" />1：</div>
					<div class="form-input">
						<input type="text" id="vbegintime1" name="vbegintime1" class="text" onblur="isTime(this)"/>
					</div>
					<div class="form-label"><span class="red">*</span><fmt:message key="endtime" />1：</div>
					<div class="form-input">
						<input type="text" id="vendtime1" name="vendtime1" class="text" onblur="isTime(this)"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="starttime" />2：</div>
					<div class="form-input">
						<input type="text" id="vbegintime2" name="vbegintime2" class="text" onblur="isTime(this)"/>
					</div>
					<div class="form-label"><fmt:message key="endtime" />2：</div>
					<div class="form-input">
						<input type="text" id="vendtime2" name="vendtime2" class="text" onblur="isTime(this)"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="remark" />：</div>
					<div class="form-input">
						<textarea rows="7" style="white-space:wrap;margin-top:10px;width:400px;height:100px;" id="vmemo" name="vmemo" ></textarea>
					</div>
				</div>
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
			//ajax同步设置
			$.ajaxSetup({
				async: false
			});
			$(document).ready(function(){
				$("#vname").focus();
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'vcode',
						validateType:['canNull','intege2','maxLength'],
						param:['F','F',8],
						error:['<fmt:message key="coding" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="enter_the_number_of_non_zero" /><fmt:message key="coding" />！','<fmt:message key="the_maximum_length" />8']
					},{
						type:'text',
						validateObj:'vname',
						validateType:['canNull','maxLength','withOutSpecialChar'],
						param:['F',50,'F'],
						error:['<fmt:message key="name" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="the_maximum_length" />50','<fmt:message key="no_contain_special_char" />！']
					},{
						type:'text',
						validateObj:'vbegintime1',
						validateType:['canNull'],
						param:['F',50],
						error:['<fmt:message key="starttime" />1<fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'vendtime1',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="endtime" />1<fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'vbegintime1',
						validateType:['handler'],
						handler:function(){
							return isTime(document.getElementById("vbegintime1"));
						},
						error:['<fmt:message key="incorrect_format" />!']
					},/*{
						type:'text',
						validateObj:'vbegintime1',
						validateType:['handler'],
						handler:function(){
							var result = true;
							var starttime = parseInt($('#vbegintime1').val().replace(':',''),10);
							var endtime = parseInt($('#vendtime1').val().replace(':',''),10);
							if(starttime>endtime){  //如果开始时间大于结束时间不允许保存
								result = false;
							}
							return result;
						},
						error:['<fmt:message key="starttime" /><fmt:message key="can_not_be_greater_than" /><fmt:message key="endtime" />！']
					},*/{
						type:'text',
						validateObj:'vmemo',
						validateType:['maxLength'],
						param:[150],
						error:['<fmt:message key="the_maximum_length" />150']
					}]
				});
			});
			
			//格式化时间
			function isTime(vals){
				if(vals.value.match('^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])$')){
					return true;
				}else if(vals.value.match('^([0-1][0-9]|[2][0-4])$')){
					vals.value=vals.value+":00";
					return true;
				}else if(vals.value.match('^([0-9]|[1][0-9]|[2][0-4])$')){
					vals.value="0"+vals.value+":00";
					return true;
				}
				vals.value="";
				return false;
			}
			
			//检验当前类型中的vcode是否已存在
			function checkVcode(){
				var bool  = true;
				var param = {};
				param['vcode'] = $("#vcode").val();
				$.post('<%=path%>/shiftSetting/checkVcode.do',param,function(data){
					if(data==1){
						alert('<fmt:message key="coding" /><fmt:message key="already_exists" />！');
						bool  = false;					
					}
				});
				
				return bool;
			}
		</script>
	</body>
</html>