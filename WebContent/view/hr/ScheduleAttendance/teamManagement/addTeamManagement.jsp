<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="branches_and_positions_information" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
	</head>
	<body>
		<div class="form">
			<form id="saveForm" method="post" action="<%=path %>/scheduleAttendance/saveData.do?flag=add">
				<div class="form-line" style="margin-top:30px;">
					<div class="form-label"><span class="red">*</span><fmt:message key="coding" />：</div>
					<div class="form-input">
						<input type="text" id="vcode" name="vcode" class="text" value="${team.vcode }" />
					</div>
					<div class="form-label"><span class="red">*</span><fmt:message key="name" />：</div>
					<div class="form-input">
						<input type="text" id="vname" name="vname" class="text" onkeyup="getSpInit(this,'vinit');" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="abbreviation" />：</div>
					<div class="form-input">
						<input type="text" id="vinit" name="vinit" class="text" />
					</div>
					<div class="form-label"><span class="red">*</span><fmt:message key="no" />：</div>
					<div class="form-input">
						<input type="text" maxlength="7" id="isortno" name="isortno" class="text" value="${team.isortno }" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="remark" />：</div>
					<div class="form-input">
						<textarea rows="7" style="white-space:wrap;margin-top:10px;width:400px;height:100px;" id="vmemo" name="vmemo" ></textarea>
					</div>
				</div>
				<input type="hidden" id="pk_hrdept" name="pk_hrdept" class="pk_hrdept" value="${pk_hrdept }" />
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>
		<script type="text/javascript">
			//ajax同步设置
			$.ajaxSetup({
				async: false
			});
			$(document).ready(function(){
				$("#vname").focus();
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'vcode',
						validateType:['canNull','intege2','maxLength'],
						param:['F','F',8],
						error:['<fmt:message key="coding" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="enter_the_number_of_non_zero" /><fmt:message key="coding" />！','<fmt:message key="the_maximum_length" />8']
					},{
						type:'text',
						validateObj:'vname',
						validateType:['canNull','maxLength','withOutSpecialChar'],
						param:['F',50,'F'],
						error:['<fmt:message key="name" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="the_maximum_length" />50','<fmt:message key="no_contain_special_char" />！']
					},{
						type:'text',
						validateObj:'vinit',
						validateType:['canNull','maxLength'],
						param:['F',50],
						error:['<fmt:message key="abbreviation" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="the_maximum_length" />50']
					},{
						type:'text',
						validateObj:'isortno',
						validateType:['canNull','intege2'],
						param:['F','F'],
						error:['<fmt:message key="no" /><fmt:message key="cannot_be_empty" />','<fmt:message key="enter_the_number_of_non_zero" /><fmt:message key="coding" />！']
					},{
						type:'text',
						validateObj:'vmemo',
						validateType:['maxLength'],
						param:[150],
						error:['<fmt:message key="the_maximum_length" />150']
					}]
				});
			});
			
			//检验当前类型中的vcode是否已存在
			function checkVcode(){
				var bool  = true;
				var param = {};
				param['vcode'] = $("#vcode").val();
				$.post('<%=path%>/scheduleAttendance/checkVcode.do',param,function(data){
					if(data==1){
						alert('<fmt:message key="coding" /><fmt:message key="already_exists" />！');
						bool  = false;					
					}
				});
				
				return bool;
			}
			
			//检验当前类型中的isortno是否已存在
			function checkIsortno(){
				var bool  = true;
				var param = {};
				param['isortno'] = $("#isortno").val();
				$.post('<%=path%>/scheduleAttendance/checkIsortno.do',param,function(data){
					if(data==1){
						alert('<fmt:message key="no" /><fmt:message key="already_exists" />！');
						bool = false;					
					}
				});
				
				return bool;
			}
		</script>
	</body>
</html>