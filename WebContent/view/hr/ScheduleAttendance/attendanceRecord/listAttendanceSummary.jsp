<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="income_or_sit_details" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/attendanceSummary/listAttendanceSummary.do" method="post">
			<div class="search-condition" style="padding-left:30px;">
				<table cellspacing="0" cellpadding="0" >
					<tr>
						<td><fmt:message key="startdate" />：</td>
						<td><input autocomplete="off" type="text" id="vbdat" name="vbdat" class="Wdate text" value="${vbdat }"/></td>
						<td style="width:20px;"></td>
						<td><fmt:message key="enddate" />：</td>
						<td><input autocomplete="off" type="text" id="vedat" name="vedat" class="Wdate text" value="${vedat }"/></td>
					</tr>
				</table>
			</div>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 25px;">&nbsp;</td>
								<td style="width:30px;"></td>
								<td><span style="width:80px;"><fmt:message key="date" /></span></td>
								<td><span style="width:100px;"><fmt:message key="actual" /><fmt:message key="scm_process_time" /></span></td>
								<td><span style="width:100px;"><fmt:message key="hr_Theory" /><fmt:message key="scm_process_time" /></span></td>
								<td><span style="width:100px;"><fmt:message key="scm_process_time" /><fmt:message key="difference" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="attendanceSummary" items="${listAttendanceSummary}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" name="idList" id="chk_<c:out value='${attendanceSummary.dworkdate}' />" 
											   value="<c:out value='${attendanceSummary.dworkdate}' />"/>
									</td>
									<td><span style="width:80px;"><c:out value="${attendanceSummary.dworkdate}" /></span></td>
									<td><span style="width:100px;text-align: right;"><c:out value="${attendanceSummary.attendance}" /></span></td>
									<td><span style="width:100px;text-align: right;"><c:out value="${attendanceSummary.totalhour}" /></span></td>
									<td><span style="width:100px;text-align: right;"><c:out value="${attendanceSummary.cha}" /></span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<input type="hidden" id="pk_store" name="pk_store" value="${pk_store }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
	  	 		//<fmt:message key="date" /><fmt:message key="select1" />
	 			$("#vbdat").click(function(){
		  			new WdatePicker({maxDate:'#F{$dp.$D(\'vedat\')}'});
		  		});
		  		$("#vedat").click(function(){
		  			new WdatePicker({minDate:'#F{$dp.$D(\'vbdat\')}'});
		  		});
		  		
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-40px']
							},
							handler: function(){
								$("#listForm").submit();
							}
						},"-",{
							text: '<fmt:message key="view" /><fmt:message key="actual" /><fmt:message key="scm_process_time" />',
							title: '<fmt:message key="view" /><fmt:message key="actual" /><fmt:message key="scm_process_time" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-40px']
							},
							handler: function(){
								actual();
							}
						},{
							text: '<fmt:message key="view" /><fmt:message key="hr_Theory" /><fmt:message key="scm_process_time" />',
							title: '<fmt:message key="view" /><fmt:message key="hr_Theory" /><fmt:message key="scm_process_time" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-40px']
							},
							handler: function(){
								theory();
							}
						},"-",{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}
					]
				});
				
				// 自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),1);		//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				
				//只能单选
				$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
					$(this).closest('.table-body').find(':checkbox').not($(this)).removeAttr("checked");
					event.stopPropagation();
				});
			});
			
			//查看实际工时
			function actual(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox').filter(':checked');
				
				if (checkboxList && checkboxList.filter(':checked').size() ==1) {
					var action = '<%=path%>/attendanceRecord/listWorkAttendance.do?flag=summary&DAT='+checkboxList.filter(':checked').val();
					$('body').window({
						id: 'window_saveStorePos',
						title: '<fmt:message key="hr_Theory" /><fmt:message key="scm_process_time" />',
						content: '<iframe id="SaveForm" frameborder="0" src="' + action + '"></iframe>',
						width: '800px',
						height: '500px',
						draggable: true,
						isModal: true,
						position : {
							top : 10
						}
					});
				}else if(checkboxList && checkboxList.filter(':checked').size() > 1){
					alert('<fmt:message key="you_can_only_modify_a_data" />！');
					return;
				}else {
					alert('<fmt:message key="please_select_data_to_view"/>！');
					return ;
				}
			}
			
			//查看理论工时
			function theory(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox').filter(':checked');
				
				if (checkboxList && checkboxList.filter(':checked').size() ==1) {
					var action = '<%=path%>/attendanceSummary/listClassTableSetting.do?dworkdate='+checkboxList.filter(':checked').val();
					$('body').window({
						id: 'window_saveStorePos',
						title: '<fmt:message key="hr_Theory" /><fmt:message key="scm_process_time" />',
						content: '<iframe id="SaveForm" frameborder="0" src="' + action + '"></iframe>',
						width: '800px',
						height: '500px',
						draggable: true,
						isModal: true,
						position : {
							top : 10
						}
					});
				}else if(checkboxList && checkboxList.filter(':checked').size() > 1){
					alert('<fmt:message key="you_can_only_modify_a_data" />！');
					return;
				}else {
					alert('<fmt:message key="please_select_data_to_view"/>！');
					return ;
				}
			}
		</script>
	</body>
</html>