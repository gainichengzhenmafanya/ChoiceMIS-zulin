<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="income_or_sit_details" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/attendanceRecord/listAttendanceRecord.do?flag=${flag}" method="post">
			<div class="search-condition" style="padding-left:30px;">
				<table cellspacing="0" cellpadding="0" >
					<tr>
						<td><fmt:message key="hr_open" /><fmt:message key="date" />：</td>
						<td><input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" value="${bdat }"/> ~ 
							<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" value="${edat }"/></td>
						<td style="width:20px;"></td>
						<td><fmt:message key="Full_name" />：</td>
						<td><input type="text" id="vename" name="vename" class="text" /></td>
						<td style="width:20px;"></td>
						<td><fmt:message key="The_work_number" />：</td>
						<td><input type="text" id="vempno" name="vempno" class="text" /></td>
					</tr>
				</table>
			</div>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 25px;">&nbsp;</td>
								<td><span style="width:80px;"><fmt:message key="The_work_number" /></span></td>
								<td><span style="width:80px;"><fmt:message key="Full_name" /></span></td>
								<td><span style="width:60px;"><fmt:message key="hr_Go_to_work" /><fmt:message key="sign_in" /></span></td>
								<td><span style="width:60px;"><fmt:message key="hr_Go_off_work" /><fmt:message key="sign_back" /></span></td>
								<td><span style="width:60px;"><fmt:message key="hr_Go_out" /></span></td>
								<td><span style="width:60px;"><fmt:message key="hr_Go_out" /><fmt:message key="back" /></span></td>
								<td><span style="width:60px;"><fmt:message key="hr_Work_overtime" /><fmt:message key="sign_in" /></span></td>
								<td><span style="width:60px;"><fmt:message key="hr_Work_overtime" /><fmt:message key="sign_back" /></span></td>
								<td><span style="width:80px;"><fmt:message key="Post" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="attendanceRecord" items="${listAttendance}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td><span style="width:80px;"><c:out value="${attendanceRecord.vempno}" /></span></td>
									<td><span style="width:80px;"><c:out value="${attendanceRecord.vename}" /></span></td>
									<td><span style="width:60px;text-align: right;"><c:out value="${attendanceRecord.sbqd}" /></span></td>
									<td><span style="width:60px;text-align: right;"><c:out value="${attendanceRecord.xbqt}" /></span></td>
									<td><span style="width:60px;text-align: right;"><c:out value="${attendanceRecord.wc}" /></span></td>
									<td><span style="width:60px;text-align: right;"><c:out value="${attendanceRecord.wcfh}" /></span></td>
									<td><span style="width:60px;text-align: right;"><c:out value="${attendanceRecord.jbqd}" /></span></td>
									<td><span style="width:60px;text-align: right;"><c:out value="${attendanceRecord.jbqt}" /></span></td>
									<td><span style="width:80px;"><c:out value="${attendanceRecord.vstation}" /></span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
	  	 		//<fmt:message key="date" /><fmt:message key="select1" />
	 			$("#bdat").click(function(){
		  			new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});
		  		});
		  		$("#edat").click(function(){
		  			new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});
		  		});
		  		
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-40px']
							},
							handler: function(){
// 								$('.search-div').slideToggle(100);
								$("#listForm").submit();
							}
						},"-",{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}
					]
				});
				
				// 自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),1);		//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
			});
		</script>
	</body>
</html>