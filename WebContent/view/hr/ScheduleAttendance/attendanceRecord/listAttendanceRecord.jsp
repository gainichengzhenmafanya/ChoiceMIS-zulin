<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="income_or_sit_details" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/attendanceRecord/listAttendanceRecord.do?flag=${flag}" method="post">
			<input type="hidden" id="pk_hrdept" name="pk_hrdept" value="${pk_hrdept }" />
			<div class="search-condition" style="padding-left:30px;">
				<table cellspacing="0" cellpadding="0" >
					<tr>
						<td><fmt:message key="hr_open" /><fmt:message key="date" />：</td>
						<td><input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" style="width:90px;" value="${bdat }"/> ~ 
							<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" style="width:90px;" value="${edat }"/></td>
						<td style="width:20px;"></td>
						<td><fmt:message key="Full_name" />：</td>
						<td><input type="text" id="vename" name="vename" class="text" style="width:90px;" /></td>
						<td style="width:20px;"></td>
						<td><fmt:message key="Attendance_No" />：</td>
						<td><input type="text" id="vkqcardno" name="vkqcardno" class="text" style="width:90px;" /></td>
					</tr>
				</table>
			</div>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 25px;">&nbsp;</td>
								<td><span style="width:60px;"><fmt:message key="Full_name" /></span></td>
								<td><span style="width:80px;"><fmt:message key="Attendance_No" /></span></td>
								<td><span style="width:100px;"><fmt:message key="hr_open" /><fmt:message key="date" /></span></td>
								<td><span style="width:100px;"><fmt:message key="hr_open" /><fmt:message key="time" /></span></td>
								<td><span style="width:80px;"><fmt:message key="hr_open" /><fmt:message key="positn_state" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="attendanceRecord" items="${listAttendance}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td ><span style="width:60px;"><c:out value="${attendanceRecord.vename}" /></span></td>
									<td ><span style="width:80px;"><c:out value="${attendanceRecord.vkqcardno}" /></span></td>
									<td ><span style="width:100px;"><c:out value="${attendanceRecord.dworkdate}" /></span></td>
									<td style="width:110px;"><c:out value="${attendanceRecord.vtime}" /></td>
									<td style="width:90px;" ><c:out value="${attendanceRecord.vpunchstate}" /></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
	  	 		//<fmt:message key="date" /><fmt:message key="select1" />
	 			$("#bdat").click(function(){
		  			new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});
		  		});
		  		$("#edat").click(function(){
		  			new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});
		  		});
		  		
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-40px']
							},
							handler: function(){
// 								$('.search-div').slideToggle(100);
								$("#listForm").submit();
							}
						},"-",{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}
					]
				});
				
				// 自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),47);		//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
			});
		</script>
	</body>
</html>