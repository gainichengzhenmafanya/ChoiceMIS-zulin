<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="income_or_sit_details" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/dailySchedule/listDailySchedule.do" method="post">
			<div class="form-line">
				<input type="hidden" id="pk_store" name="pk_store"  value="${pk_store }"/>
				<div class="form-label" style="width:30px;margin-left:10px;"><span style="color: red">*</span><fmt:message key="date" />：</div>
				<div class="form-input">
					<input autocomplete="off" type="text" id="dworkdate" name="dworkdate" class="Wdate text" value="${dworkdate }" />
				</div>
				<div class="form-label" style="width:50px;"><fmt:message key="employees" /><fmt:message key="coding" />：</div>
				<div class="form-input" style="width:120px;">
					<input type="text" id="vempno" name="vempno" class="text" style="width:100px;" />
				</div>
				<div class="form-label" style="width:50px;"><fmt:message key="employee_name" />：</div>
				<div class="form-input" style="width:120px;">
					<input type="text" id="vname" name="vname" class="text" style="width:100px;" />
				</div>
				<div class="form-label" style="width:30px;"><fmt:message key="Post" />：</div>
				<div class="form-input" style="width:100px;">
					<input type="text" id="vstation" name="vstation" class="text" style="width:100px;" />
				</div>
			</div>	
			<div class="grid" >
				<div class="gridtable" style="width:${count * 200}px;">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td rowspan="2" class="num"><span style="width:25px;text-align: center;">&nbsp;</span></td>
									<td rowspan="2"><span style="width:80px;text-align: center;"><fmt:message key="coding" /></span></td>
									<td rowspan="2"><span style="width:80px;text-align: center;"><fmt:message key="name" /></span></td>
									<td rowspan="2"><span style="width:80px;text-align: center;"><fmt:message key="Post" /></span></td>
									<td rowspan="2"><span style="width:80px;text-align: center;"><fmt:message key="Attendance_No" /></span></td>
									<c:forEach var="list" items="${getTeamByDept }">
										<td colspan="${list.size }" style="text-align: center;">${list.vname }</td>
									</c:forEach>
								</tr>
								<tr>
									<c:forEach var="list" items="${getTeamByDept }">
										<c:forEach var="shift" items="${list.listShift }">
											<td><span style="width:140px;text-align: center;">${shift.vname }</span></td>
										</c:forEach>
									</c:forEach>
								</tr>
							</thead>
						</table>	
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0" id="tableBody">
							<tbody>
								<c:forEach var="emp" items="${getEmpShift}" varStatus="status">
									<tr>
										<td class="num" ><span style="width:25px;text-align: center;">${status.index+1}</span></td>
										<td><span style="width:80px;"><c:out value="${emp.vempno}" /></span></td>
										<td><span style="width:80px;"><c:out value="${emp.vname}" /></span></td>
										<td><span style="width:80px;"><c:out value="${emp.vstation}" /></span></td>
										<td><span style="width:80px;text-align: right;"><c:out value="${emp.vkqcardno}" /></span></td>
										<c:forEach var="list" items="${getTeamByDept }">
											<c:forEach var="shift" items="${list.listShift }">
												<td <c:if test="${shift.pk_shift == emp.pk_shift and list.pk_hrdept == emp.pk_hrdept }">bgcolor="green"</c:if> >
													<span style="width:140px;text-align: center;"></span>
												</td>
											</c:forEach>
										</c:forEach>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#dworkdate").click(function(){
		  			new WdatePicker();
		  		});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-40px']
							},
							handler: function(){
								var dworkdate = $("#dworkdate").val();
								if ('' == dworkdate) {
									alert('<fmt:message key="date" /><fmt:message key="cannot_be_empty" />!');
									return true;
								} else {
									$("#listForm").submit();
								}
							}
						},'-',{
							text: 'Excel',
							title: 'Excel<fmt:message key="export"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								var dworkdate = $("#dworkdate").val();
								window.location.href = "<%=path%>/dailySchedule/exportReport.do?reportName=dailySchedule&dworkdate="+dworkdate;
							}
						},/*{
							text: '<fmt:message key="export" />',
							title: '<fmt:message key="exporting_reports"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								exportReport();
							}
						},*/'-',{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}
					]
				});
				// 自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),20);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
			});
		</script>
	</body>
</html>