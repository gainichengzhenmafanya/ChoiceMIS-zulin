<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>考勤记录</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<style type="text/css">
				table.gridtable {
					font-family: verdana,arial,sans-serif;
					font-size:11px;
					color:#333333;
					border-width: 1px;
					border-color: #666666;
					border-collapse: collapse;
				}
				table.gridtable th {
					border-width: 1px;
					padding: 2px;
					border-style: solid;
					border-color: #666666;
					background-color: #dedede;
				}
				table.gridtable td {
					border-width: 1px;
					padding: 2px;
					border-style: solid;
					border-color: black;
					background-color: #ffffff;
				}
			</style>
		</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/attendanceRecord/listWorkAttendance.do" method="post">
		<input type="hidden" id="edit" value="${edit}"/>
			<div class="search-condition" style="padding-left:30px;">
				<table cellspacing="0" cellpadding="0" >
					<tr>
						<td><fmt:message key="hr_open" /><fmt:message key="date" />：</td>
						<td><input autocomplete="off" type="text" id="DAT" name="DAT" class="Wdate text" style="width:90px;" value="${workAttendance.DAT}"/> </td>
						<td style="width:20px;"></td>
						<td><fmt:message key="Full_name" />：</td>
						<td><input type="text" id="VNAME" name="VNAME" class="text" style="width:90px;" value="${workAttendance.VNAME}"/></td>
						<td style="width:20px;"></td>
						<td><fmt:message key="Attendance_No" />：</td>
						<td style="width:100px;" ><input type="text" id="VKQCARDNO" name="VKQCARDNO" class="text" style="width:90px;" value="${workAttendance.VKQCARDNO}"/></td>
						<td>状态</td>
						<td>
						<input type="radio" id="STA0" name="STA" value="2" <c:if test="${workAttendance.STA  != 1 && workAttendance.STA  != 0}">checked="checked"</c:if>/>全部
						<input type="radio" id="STA1" name="STA" value="0" <c:if test="${workAttendance.STA  == 0}">checked="checked"</c:if>/>正常
						<input type="radio" id="STA2" name="STA" value="1" <c:if test="${workAttendance.STA  == 1}">checked="checked"</c:if>/>异常
						</td>
					</tr>
				</table>
			</div>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td style="width: 20px;"></td>
								<td class="num" style="width: 25px;">序号</td>
								<td><span style="width:80px;">考勤号</span></td>
								<td><span style="width:100px;">姓名</span></td>
								<td><span style="width:80px;">考勤工时</span></td>
								<td><span style="width:30px;">状态</span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body" style="height:445px;">
					<table cellspacing="0" cellpadding="0">
						<tbody>
						 	<c:forEach var="workAttendance" items="${list}" varStatus="status">
								<tr>
									<td style="width: 20px;">
										<input type="checkbox" value="${workAttendance.PK_EMPLOYEE}"/>
									</td>
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td ><span style="width:80px;" id="VKQCARDNO${status.index}" ><c:out value="${workAttendance.VKQCARDNO}" /></span></td>
									<td ><span style="width:100px;" id="VNAME${status.index}" >
									<input type="hidden" id="VECODE${status.index}" value="${workAttendance.VECODE}"/>
									<c:out value="${workAttendance.VNAME}" /></span></td>
									<td ><span style="width:80px;"><input type="text" name ="ATTENDANCE" style="text-align: right;width:78px;" disabled="disabled" id="ATTENDANCE${status.index}"
									onkeyup="this.value=this.value.replace(/[^\d\.\d{0,3}]/g,'')"
									 value='<fmt:formatNumber value="${workAttendance.ATTENDANCE}" pattern="${spattern}"/>'/></span></td>
									<td ><span style="width:30px;">
										<c:if test="${workAttendance.STA  == 1}">
											异常
										</c:if>
										<c:if test="${workAttendance.STA  != 1}">
											正常
										</c:if>
									</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div class="table-total">
					<table class="gridtable">
							<tr>
								<td><span style="width:90px;color: red;">实际工时合计：</span></td>
								<td><span style="width:100px;text-align: right;">${getWorkHour.ATTENDANCE }</span></td>
								<td><span style="width:90px;color: red;">理论工时合计：</span></td>
								<td><span style="width:100px;text-align: right;">${getWorkHour.TOTALHOUR }</span></td>
							</tr>
					</table>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
	 			$("#DAT").click(function(){
		  			new WdatePicker();
		  		});
	 			
	 			var edit = $("#edit").val();
	 			if(edit=='0'){
	 				loadTool(false,false);
	 			}else{
	 				loadTool(true,false);
	 			}
				// 自动实现滚动条
// 				setElementHeight('.grid',['.tool'],$(document.body),55);		//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
			});
			function loadTool(edit,save){
				$('.tool').text("");
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-40px']
							},
							handler: function(){
								$("#listForm").submit();
							}
						},{
							text: '<fmt:message key="edit" />',
							title: '<fmt:message key="edit" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&edit,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-40px']
							},
							handler: function(){
								loadTool(false,true);
								$("input[name='ATTENDANCE']").attr("disabled",false);
							}
						},{
							text: '<fmt:message key="save" />',
							title: '<fmt:message key="save" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&save,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-40px']
							},
							handler: function(){
								saveData();
							}
						},{
							text: '重新计算工时',
							title: '重新计算工时',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&save,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-40px']
							},
							handler: function(){
								calData();
							}
						},"-",{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								if(save){
									if(confirm('当前已编辑数据未保存，是否继续？')){
										invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
									}
								}else{
									if ("summary" == '${flag}') {
										$(".close",parent.document).click();
									} else {
										invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
									}
								}
							}
						}
					]
				});
				return tool;
			}
			function saveData(){
				var sta = 1;
				if(confirm('是否确认已修复所有异常记录,如未修复所有记录，请勾选已修复记录！')){
					sta = 0;
				}
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				var data = {};
				$.ajaxSetup({async:false});
				for(var k=0;k<checkboxList.size();k++){
					data["workAttendanceList["+k+"].PK_EMPLOYEE"] = checkboxList.get(k).value;
					data["workAttendanceList["+k+"].ATTENDANCE"] = $("#ATTENDANCE"+k).val();
					data["workAttendanceList["+k+"].VECODE"] = $("#VECODE"+k).val();
					data["workAttendanceList["+k+"].VNAME"] =  $.trim($("#VNAME"+k).attr("text"));
					data["workAttendanceList["+k+"].VKQCARDNO"] =  $.trim($("#VKQCARDNO"+k).attr("text"));
					if(sta == 0){
						data["workAttendanceList["+k+"].STA"] = sta;
					}else{
						if(checkboxList.get(k).checked){
							data["workAttendanceList["+k+"].STA"] = 0;
						}else{
							data["workAttendanceList["+k+"].STA"] = 1;
						}
					}
				}
				data["DAT"]=$("#DAT").val();
				$.post("<%=path%>/attendanceRecord/saveWorkAttendance.do",data,function(){
					$('body').window({
						id: 'window_saveStorePos',
						title: '考勤工时保存',
						content: '<iframe id="SaveForm" frameborder="0" src="<%=path%>/operationManagement/toList.do?flag=income"></iframe>',
						width: '350px',
						height: '300px',
						draggable: true,
						isModal: true
					});
				});
			}
			function calData(){
				if(confirm('是否重新计算当前选中日期工时？')){
					var data = {};
					$.ajaxSetup({async:false});
					data["DAT"]=$("#DAT").val();
					$.post("<%=path%>/attendanceRecord/calWorkAttendance.do",data,function(){
						$('body').window({
							id: 'calWorkAttendance',
							title: '考勤工时计算',
							content: '<iframe id="SaveForm" frameborder="0" src="<%=path%>/operationManagement/toList.do?flag=income"></iframe>',
							width: '350px',
							height: '300px',
							draggable: true,
							isModal: true
						});
					});
				}
			}
			
		</script>
	</body>
</html>