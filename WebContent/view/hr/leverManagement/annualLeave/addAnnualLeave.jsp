<%@page import="com.choice.misboh.commonutil.DateJudge"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="branches_and_positions_information" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
	</head>
	<body>
		<div class="form">
			<form id="saveForm" method="post" action="<%=path %>/annualLeave/saveData.do">
				<div class="form-line" style="margin-top:10px;margin-left:15px;">
					<div class="form-label">
						<span style="color: blue;font-size: 18px;margin-left:30px;">
							<c:if test="${'' == leave.pk_annualleave or null == leave.pk_annualleave }">
								【<fmt:message key="hr_Newly_build" /><fmt:message key="year" /><fmt:message key="Vacation" />】
							</c:if>
							<c:if test="${'' != leave.pk_annualleave and null != leave.pk_annualleave }">
								【<fmt:message key="update" /><fmt:message key="year" /><fmt:message key="Vacation" />】
							</c:if>
						</span>
					</div>
					<div class="form-label" style="margin-left:50px;">
						<input type="hidden" id="pk_annualleave" name="pk_annualleave" value="${leave.pk_annualleave }" />
						<input type="hidden" id="pk_employee" name="pk_employee" value="${employee.pk_employee }" />
						<input type="hidden" id="pk_hrdept" name="pk_hrdept" value="${employee.pk_hrdept }" />
						<input type="hidden" id="vempno" name="vempno" value="${employee.vempno }" />
						<b><fmt:message key="The_work_number" /></b>：<span id="vempno">${employee.vempno }</span>
					</div>
					<div class="form-label">
						<input type="hidden" id="vname" name="vname" value="${employee.vname }" />
						<b><fmt:message key="Full_name" /></b>：<span id="vname" style="color: blue;">${employee.vname }</span>
					</div>
					<div class="form-label">
						<input type="hidden" id="pk_status" name="pk_status" value="${employee.pk_status }" />
						<b><fmt:message key="status" /></b>：<span id="vstatus" style="color: red">${employee.vstatus }</span>
					</div>
				</div>
				<fieldset style="height:230px;width:560px;border:solid #416AA3 2px;padding-top:10px;margin-top:10px;margin-left:10px;padding-right:15px;">
					<legend style="margin-left:20px;"><fmt:message key="Vacation" /><fmt:message key="record" /></legend>
					<div class="form-line">
						<div class="form-label"><fmt:message key="Vacation" /><fmt:message key="type" />：</div>
						<div class="form-input">
							<input type="hidden" id="pk_leavetyp" name="pk_leavetyp" class="text" value="${leaveType[0].pk_datatyp }"/>
							<input type="text" id="leavetyp" name="leavetyp" class="text" value="${leaveType[0].vname }" disabled="disabled"/>
						</div>
						<div class="form-label"><fmt:message key="Vacation" /><fmt:message key="reason" />：</div>
						<div class="form-input">
							<input type="text" id="vleavereason" name="vleavereason" class="text" value="${leave.vleavereason }"/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><span class="red">*</span><fmt:message key="hr_Holiday_period" />：</div>
						<div class="form-input">
							<input autocomplete="off" type="text" id="vbdat" name="vbdat" class="Wdate text" value="${vbdat }" onblur="dateDifference()"/>
						</div>
						<div class="form-label"><span class="red">*</span><fmt:message key="hr_Take_holidays"/>：</div>
						<div class="form-input">
							<input autocomplete="off" type="text" id="vedat" name="vedat" class="Wdate text" value="${vedat }" onblur="dateDifference()"/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="hr_But_Hugh" /><fmt:message key="days" />：</div>
						<div class="form-input">
							<input type="text" id="ncandays" name="ncandays" class="text" value="${leave.ncandays }"/>(<fmt:message key="day" />)
						</div>
						<div class="form-label"><fmt:message key="hr_Hugh_has" /><fmt:message key="days" />：</div>
						<div class="form-input">
							<input type="text" id="nrepairdays" name="nrepairdays" class="text" value="${leave.nrepairdays }"/>(<fmt:message key="day" />)
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><span class="red">*</span><fmt:message key="Vacation" /><fmt:message key="days" />：</div>
						<div class="form-input">
							<input type="text" id="vnum" name="vnum" class="text" value="${leave.vnum }"/>(<fmt:message key="day" />)
						</div>
						<div class="form-label"><fmt:message key="hr_Sick_leave" /><fmt:message key="date" />：</div>
						<div class="form-input">
							<input autocomplete="off" type="text" id="vsickdate" name="vsickdate" class="Wdate text" value="${vsickdate }"/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="Vacation" /><fmt:message key="scm_year" />：</div>
						<div class="form-input">
							<input autocomplete="off" type="text" id="vyears" name="vyears" class="Wdate text" value="${vyears}"/>(<fmt:message key="year" />)
						</div>
						<div class="form-label"><fmt:message key="hr_Working_years" />：</div>
						<div class="form-input">
							<input type="text" id="nworktime" name="nworktime" class="text" value="${leave.nworktime }"/>(<fmt:message key="year" />)
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="remark" />：</div>
						<div class="form-input">
							<textarea rows="7" style="white-space:wrap;margin-top:5px;width:400px;height:70px;" id="vmemo" name="vmemo" >${leave.vmemo }</textarea>
						</div>
					</div>
				</fieldset>
				<input type="hidden" id="flag" name="flag" class="text" value="${flag }"/>
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/validate/bohvalidate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
	  	 		//<fmt:message key="date" /><fmt:message key="select1" />
	 			$("#vbdat").click(function(){
		  			new WdatePicker({maxDate:'#F{$dp.$D(\'vedat\')}'});
		  		});
		  		$("#vedat").click(function(){
		  			new WdatePicker({minDate:'#F{$dp.$D(\'vbdat\')}'});
		  		});
		  		$("#vsickdate").click(function(){
		  			new WdatePicker();
		  		});
		  		$("#vyears").click(function(){ 
		  			new WdatePicker({dateFmt:'yyyy'});
		  		});
		  		
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'vnum',
						validateType:['canNull','num2'],
						param:['F','F'],
						error:['<fmt:message key="Vacation" /><fmt:message key="days" /><fmt:message key="cannot_be_empty" />！',
						       '<fmt:message key="Vacation" /><fmt:message key="days" /><fmt:message key="only_digits" />！']
					},{
						type:'text',
						validateObj:'vbdat',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="hr_Holiday_period" /><fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'vedat',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="hr_Take_holidays" /><fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'nworktime',
						validateType:['num2'],
						param:['F'],
						error:['<fmt:message key="hr_Working_years" /><fmt:message key="only_digits" />！']
					},{
						type:'text',
						validateObj:'ncandays',
						validateType:['num2'],
						param:['F'],
						error:['<fmt:message key="hr_But_Hugh" /><fmt:message key="days" /><fmt:message key="only_digits" />！']
					},{
						type:'text',
						validateObj:'nrepairdays',
						validateType:['num2'],
						param:['F'],
						error:['<fmt:message key="hr_Hugh_has" /><fmt:message key="days" /><fmt:message key="only_digits" />！']
					},{
						type:'text',
						validateObj:'vmemo',
						validateType:['maxLength'],
						param:[150],
						error:['<fmt:message key="the_maximum_length" />350']
					}]
				});

				//若工作年限为空，则自动计算工作年限并赋值
				if ('' == '${leave.nworktime }') {
					var entryDate = '${employee.vjrdat}';	//入职日期
					var nowDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();//当前日期
					$("#nworktime").val((DateDiff(entryDate,nowDate) / 365).toFixed(2));
				}
			});
			
			//自动计算休假天数
			function dateDifference(){  
				var vbdat = $("#vbdat").val();
				var vedat = $("#vedat").val();
				$("#vnum").val(DateDiff(vbdat,vedat));
			} 
			 
			//计算两个日期间的天数
			function DateDiff(vbdat, vedat){    //vbdat和vedat是2006-12-18格式  
				var date, date1, date2, days;  
			    date = vbdat.split("-");  
			    date1 = new Date(date[1] + '-' + date[2] + '-' + date[0]);    	//转换为12-18-2006格式  
			       
			    date = vedat.split("-");  
			    date2 = new Date(date[1] + '-' + date[2] + '-' + date[0]);  
			       
			    days = parseInt(Math.abs(date1 - date2) / 1000 / 60 / 60 /24);	//把相差的毫秒数转换为天数  
			    return days;        
			}   
		</script>
	</body>
</html>