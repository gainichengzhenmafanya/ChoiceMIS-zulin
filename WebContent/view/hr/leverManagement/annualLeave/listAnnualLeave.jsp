<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="income_or_sit_details" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/annualLeave/listAnnualLeave.do" method="post">
			<div class="form-line">
				<div class="form-label">
					<span style="color: blue;font-size: 18px;margin-left:50px;">【<fmt:message key="Vacation" /><fmt:message key="maintenance" />】</span>
				</div>
				<div class="form-label" style="margin-left:50px;">
					<input type="hidden" id="pk_employee" name="pk_employee" value="${employee.pk_employee }" />
					<input type="hidden" id="pk_hrdept" name="pk_hrdept" value="${employee.pk_hrdept }" />
					<b><fmt:message key="The_work_number" /></b>：<span id="vempno">${employee.vempno }</span>
				</div>
				<div class="form-label">
					<b><fmt:message key="Full_name" /></b>：<span id="vname" style="color: blue;">${employee.vname }</span>
				</div>
				<div class="form-label">
					<b><fmt:message key="status" /></b>：<span id="vstatus" style="color: red">${employee.vstatus }</span>
				</div>
			</div>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 25px;">&nbsp;</td>
								<td style="width:30px;">
									<input type="checkbox" id="chkAll"/>
								</td>
								<td><span style="width:100px;"><fmt:message key="hr_Holiday_period" /></span></td>
								<td><span style="width:100px;"><fmt:message key="hr_Take_holidays" /></span></td>
								<td><span style="width:100px;"><fmt:message key="Vacation" /><fmt:message key="type" /></span></td>
								<td><span style="width:100px;"><fmt:message key="Vacation" /><fmt:message key="days" /></span></td>
								<td><span style="width:100px;"><fmt:message key="Vacation" /><fmt:message key="scm_year" />(<fmt:message key="year" />)</span></td>
								<td><span style="width:100px;"><fmt:message key="hr_Working_years" /></span></td>
								<td><span style="width:100px;"><fmt:message key="hr_Sick_leave" /><fmt:message key="date" /></span></td>
								<td><span style="width:100px;"><fmt:message key="Vacation" /><fmt:message key="reason" /></span></td>
								<td><span style="width:200px;"><fmt:message key="remark" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="leave" items="${listLeave}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" name="idList" id="chk_<c:out value='${leave.pk_annualleave}' />" value="<c:out value='${leave.pk_annualleave}' />"/>
									</td>
									<td><span style="width:100px;"><c:out value="${leave.vbdat}" /></span></td>
									<td><span style="width:100px;"><c:out value="${leave.vedat}" /></span></td>
									<td><span style="width:100px;"><c:out value="${leave.vname}" /></span></td>
									<td><span style="width:100px;text-align: right;"><c:out value="${leave.vnum}" /></span></td>
									<td><span style="width:100px;"><c:out value="${leave.vyears}" /></span></td>
									<td><span style="width:100px;text-align: right;"><fmt:formatNumber value="${leave.nworktime}" pattern="###0.00"/></span></td>
									<td><span style="width:100px;"><c:out value="${leave.vsickdate}" /></span></td>
									<td><span style="width:100px;"><c:out value="${leave.vleavereason}" /></span></td>
									<td><span style="width:200px;"><c:out value="${leave.vmemo}" /></span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<div class="search-div" style="margin-left: 0px;">
				<div class="search-condition">
					<table class="search-table" cellspacing="0" cellpadding="0" >
						<tr>
							<td><fmt:message key="Vacation" /><fmt:message key="date" />：</td>
							<td><input autocomplete="off" type="text" id="vbdat" name="vbdat" class="Wdate text" value="${vbdat }"/> ~ 
								<input autocomplete="off" type="text" id="vedat" name="vedat" class="Wdate text" value="${vedat }"/></td>
						</tr>
					</table>
				</div>
				<div class="search-commit">
		       		<input type="button" class="search-button" id="search" value='<fmt:message key="enter" />'/>
		       		<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				//员工列表弹出框
				if ('' == '${employee.pk_employee }') {
					var url = "<%=path%>/staffList/toChooseEmployee.do?callBack=setEmployee&domId=selected&single=true&type=listEmp&pk_store=${pk_store}";
					return $('body').window({
						id: 'window_chooseSiteType',
						title: '<fmt:message key="select1" /><fmt:message key="employees" />',
						content: '<iframe id="listSiteTypeFrame" frameborder="0" src='+url+'></iframe>',
						width: 600,
						height: 430,
						confirmClose: false,
						draggable: true,
						isModal: true
					});
					
					<%
						session.removeAttribute("employee.pk_employee");
					%>
				}
				
	  	 		//<fmt:message key="date" /><fmt:message key="select1" />
	 			$("#vbdat").click(function(){
		  			new WdatePicker({maxDate:'#F{$dp.$D(\'vedat\')}'});
		  		});
		  		$("#vedat").click(function(){
		  			new WdatePicker({minDate:'#F{$dp.$D(\'vbdat\')}'});
		  		});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-40px']
							},
							handler: function(){
								$('.search-div').slideToggle(100);
							}
						},'-',{
							text: '<fmt:message key="insert" />',
							title: '<fmt:message key="new_role_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								saveleave();
							}
						},{
							text: '<fmt:message key="update" />',
							title: '<fmt:message key="modify_the_role_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								updateleave();
							}
						},{
							text: '<fmt:message key="delete" />',
							title: '<fmt:message key="delete"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								deleteleave();
							}
						},"-",{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}
					]
				});
				$("#search").bind('click', function() {
					var vbdat = $("#vbdat").val();
					var vedat = $("#vedat").val();
					
					if ('' == vbdat && '' != vedat) {
						alert('<fmt:message key="please_enter" /><fmt:message key="hr_Holiday_period" />！');
						return true;
					}
					
					if ('' != vbdat && '' == vedat) {
						alert('<fmt:message key="please_enter" /><fmt:message key="hr_Take_holidays" />！');
						return true;
					}
					$('.search-div').hide();
					$('#listForm').submit();
				});
				/* 模糊<fmt:message key="select" />清空 */
				$("#resetSearch").bind('click', function() {
					$('.search-condition input').val('');
				});
				// 自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),20);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				
				//双击弹出<fmt:message key="update" />
				$('.grid').find('.table-body').find('tr').live("dblclick", function () {
					$(":checkbox").attr("checked", false);
					$(this).find(':checkbox').attr("checked", true);
					updateleave();
					if($(this).hasClass("show-firm-row"))return;
					$('.grid').find('.table-body').find('tr').removeClass("show-firm-row");
					$(this).addClass("show-firm-row");
				 });
				
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }
				     else
				     {
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				 });
			});
			
			function setEmployee(data){
				$("#pk_employee").val(data.pk);
				if ('' == data.pk) {
					alert('<fmt:message key="please_select" /><fmt:message key="employees" />！');
					return true;
				} else {
					$("#listForm").submit();
				}
			}
			
			//新增
			function saveleave(){
				var pk_employee = $("#pk_employee").val();
				var action = "<%=path%>/annualLeave/addData.do?flag=add&pk_employee="+pk_employee;
				$('body').window({
					id: 'window_saveOutlay',
					title: '<fmt:message key="hr_Newly_build" /><fmt:message key="data" />',
					content: '<iframe id="SaveForm" frameborder="0" src='+action+'></iframe>',
					width: '610px',
					height: '380px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								title: '<fmt:message key="save_role_of_information"/>',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('SaveForm')&&window.document.getElementById("SaveForm").contentWindow.validate._submitValidate()){
										submitFrameForm('SaveForm','saveForm');
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});	
			}
			
			//修改
			function updateleave(){
				var pk_employee = $("#pk_employee").val();
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				var pk_annualleave = checkboxList.filter(':checked').val();
				if(checkboxList && checkboxList.filter(':checked').size() ==1){
					var action='<%=path%>/annualLeave/addData.do?flag=update&pk_employee='+pk_employee+'&pk_annualleave='+pk_annualleave;
					$('body').window({
						id: 'window_updateOutlay',
						title: '<fmt:message key="update" /><fmt:message key="data" />',
						content: '<iframe id="UpdateForm" frameborder="0" src='+action+'></iframe>',
						width: '610px',
						height: '380px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="data" /><fmt:message key="update" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('UpdateForm')&&window.document.getElementById("UpdateForm").contentWindow.validate._submitValidate()){
											submitFrameForm('UpdateForm','saveForm');
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList && checkboxList.filter(':checked').size() > 1){
					alert('<fmt:message key="you_can_only_modify_a_data" />！');
					return;
				}else {
					alert('<fmt:message key="please_select_information_you_need_to_modify"/>！');
					return ;
				}
			}
			
			//删除
			function deleteleave(){
				var pk_employee = $("#pk_employee").val();
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() > 0){
					if(confirm('<fmt:message key="delete_data_confirm" />？')){
						var chkValue = [];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
						});
						var action = '<%=path%>/annualLeave/saveData.do?flag=delete&pk_employee='+pk_employee+'&pk_annualleave='+chkValue.join(",");
						$('body').window({
							title: '<fmt:message key="delete" /><fmt:message key="data" />',
							content: '<iframe frameborder="0" src='+action+'></iframe>',
							width: '340px',
							height: '255px',
							draggable: true,
							isModal: true
						});
					}
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				}
			}	
		</script>
	</body>
</html>