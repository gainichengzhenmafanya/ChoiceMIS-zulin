<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="income_or_sit_details" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/leaveInquiry/listLeaveInquiry.do" method="post">
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 25px;">&nbsp;</td>
								<td><span style="width:100px;"><fmt:message key="employees" /><fmt:message key="number" /></span></td>
								<td><span style="width:80px;"><fmt:message key="Full_name" /></span></td>
								<td><span style="width:80px;"><fmt:message key="Vacation" /><fmt:message key="type" /></span></td>
								<td><span style="width:80px;"><fmt:message key="hr_Holiday_period" /></span></td>
								<td><span style="width:80px;"><fmt:message key="hr_Take_holidays" /></span></td>
								<td><span style="width:60px;"><fmt:message key="Vacation" /><fmt:message key="days" /></span></td>
								<td><span style="width:80px;"><fmt:message key="hr_Sick_leave" /><fmt:message key="date" /></span></td>
								<td><span style="width:60px;"><fmt:message key="office" /><fmt:message key="status" /></span></td>
								<td><span style="width:60px;"><fmt:message key="Post" /></span></td>
								<td><span style="width:150px;"><fmt:message key="Vacation" /><fmt:message key="reason" /></span></td>
								<td><span style="width:200px;"><fmt:message key="remark" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="leave" items="${listLeaveInquiry}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td><span style="width:100px;"><c:out value="${leave.vempno}" /></span></td>
									<td><span style="width:80px;"><c:out value="${leave.vname}" /></span></td>
									<td><span style="width:80px;"><c:out value="${leave.vleavetyp}" /></span></td>
									<td><span style="width:80px;"><c:out value="${leave.vbdat}" /></span></td>
									<td><span style="width:80px;"><c:out value="${leave.vedat}" /></span></td>
									<td><span style="width:60px;text-align: right;">${leave.vnum}</span></td>
									<td><span style="width:80px;"><c:out value="${leave.vsickdate}" /></span></td>
									<td><span style="width:60px;"><c:out value="${leave.vstatus}" /></span></td>
									<td><span style="width:60px;"><c:out value="${leave.vstation}" /></span></td>
									<td><span style="width:150px;"><c:out value="${leave.vleavereason}" /></span></td>
									<td><span style="width:200px;"><c:out value="${leave.vmemo}" /></span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<input type="hidden" id="pk_hrdept" name="pk_hrdept" value="${pk_hrdept }" />
			<div class="search-div" style="margin-left: 0px;">
				<div class="search-condition">
					<table class="search-table" cellspacing="0" cellpadding="0" >
						<tr>
							<td><fmt:message key="Vacation" /><fmt:message key="date" />：</td>
							<td><input autocomplete="off" type="text" id="vbdat" name="vbdat" class="Wdate text" value="${vbdat }"/> ~ 
								<input autocomplete="off" type="text" id="vedat" name="vedat" class="Wdate text" value="${vedat }"/></td>
							<td><fmt:message key="employee_name" />：</td>
							<td><input type="text" id="vname" name="vname" class="text" /></td>
							<td><fmt:message key="Vacation" /><fmt:message key="type" />：</td>
							<td><select id="pk_leavetyp" name="pk_leavetyp" class="select">
									<option></option>
									<c:forEach var="typ" items="${leaveType }" >
										<option value="${typ.pk_datatyp }" >${typ.vname }</option>
									</c:forEach>
								</select>
							</td>
						</tr>
					</table>
				</div>
				<div class="search-commit">
		       		<input type="button" class="search-button" id="search" value='<fmt:message key="enter" />'/>
		       		<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/mis/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				//<fmt:message key="date" /><fmt:message key="select1" />
	 			$("#vbdat").click(function(){
		  			new WdatePicker({maxDate:'#F{$dp.$D(\'vedat\')}'});
		  		});
		  		$("#vedat").click(function(){
		  			new WdatePicker({minDate:'#F{$dp.$D(\'vbdat\')}'});
		  		});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-40px']
							},
							handler: function(){
								$('.search-div').slideToggle(100);
							}
						},'-',{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}
					]
				});
				$("#search").bind('click', function() {
					var vbdat = $("#vbdat").val();
					var vedat = $("#vedat").val();
					
					if ('' == vbdat && '' != vedat) {
						alert('<fmt:message key="please_enter" /><fmt:message key="hr_Holiday_period" />！');
						return true;
					}
					
					if ('' != vbdat && '' == vedat) {
						alert('<fmt:message key="please_enter" /><fmt:message key="hr_Take_holidays" />！');
						return true;
					}
					$('.search-div').hide();
					$('#listForm').submit();
				});
				/* 模糊<fmt:message key="select" />清空 */
				$("#resetSearch").bind('click', function() {
					$('.search-condition input').val('');
				});
				// 自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),5);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
			});
		</script>
	</body>
</html>