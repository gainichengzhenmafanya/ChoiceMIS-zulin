<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
response.setHeader("Pragma","No-cache"); 
response.setHeader("Cache-Control","no-cache"); 
response.setDateHeader("Expires", 0); 
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
	  <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
	  <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
	  <script type="text/javascript">
          if(!(window==top))
              top.window.location.href="<%=path%>/login.do";
	  </script>
  	<style type="text/css">

		body {
	      background: url(<%=path%>/image/login/bg.jpg);
	      width: auto;
	      height: auto;
	      background-size: cover;
	      background-repeat: no-repeat;
	      background-origin: content-box;
	      background-position: center;
	      min-width: 800px;
	      padding: 0px;
	      margin: 0px;
	      z-index: -3;
	
	    }
	
	    .main {
	      background: rgba(0, 0, 0, 0.0);
	      width: 100%;
	      height: 100%;
	      z-index: -1;
	    }
	
	    .top {
	      width: 100%;
	      height: 50px;
	      background: rgba(0, 0, 0, 0.6);
	      overflow: hidden;
	    }
	
	    .foot {
	      width: 100%;
	      height: 30px;
	      position: absolute;
	      bottom: 0px;
	      background: rgba(0, 0, 0, 0.6);
	      text-align: center;
	    }
	
	    .foot span {
	      color: #aaa;
	      font-size: 14px;
	      height: 30px;
	      line-height: 30px;
	      font-family:"Microsoft YaHei";
	    }
	
	    .top_span {
	      color: #fff;
	      font-size: 24px;
	      padding-left: 100px;
	      height: 50px;
	      line-height: 50px;
	      font-family: "Microsoft YaHei";
	    }
	
	    .top_span::before {
	      content: '';
	      display: inline-block;
	      background: url(<%=path%>/image/login/logo.png) center center no-repeat;
	      background-size: contain;
	      height: 50px;
	      width: 70px;
	      margin: 0 10px -14px 0;
	    }
	
	    .content {
	      width: 100%;
	      height: auto;
	      margin-top: 140px;
	      overflow: hidden;
	      float: left;
	    }
	
	    .left {
	      width: 58%;
	      float: left;
	    }
	
	    .right {
	      width: 30%;
	      float: right;
	    }
	
	    .login {
	      width: 300px;
	      /*height: 280px;*/
	      background: rgba(0, 0, 0, 0.6);
	      border-radius: 10px;
	    }
	
	    .login_content {
	      width: 240px;
	      padding: 20px 30px;
	      text-align: center;
	    }
	
	    .login_content>span:first-child{
	      color: #fff;
	      font-size: 14px;
	      /*padding-top: 10px;*/
	      padding-bottom:20px;
	      text-align: left;
	      display: block;
	    }
	    .title_error{
	        margin-bottom: 10px;
	        font-size: 14px;
	        text-align: left;
	        color: red;
	        display: block;
	    }
	    .title_info_error{
	      margin-bottom: 10px;
	      font-size: 14px;
	      text-align: left;
	        color: red;
	        display: block;
	    }

	    button {
	      display: inline-block;
	      margin-bottom: 0;
	      font-weight: 500;
	      text-align: center;
	      -ms-touch-action: manipulation;
	      touch-action: manipulation;
	      cursor: pointer;
	      background-image: none;
	      border: 1px solid transparent;
	      white-space: nowrap;
	      line-height: 1.5;
	      padding: 0 15px;
	      font-size: 12px;
	      border-radius: 4px;
	      height: 28px;
	      -webkit-user-select: none;
	      -moz-user-select: none;
	      -ms-user-select: none;
	      user-select: none;
	      -webkit-transition: all .3s cubic-bezier(.645, .045, .355, 1);
	      transition: all .3s cubic-bezier(.645, .045, .355, 1);
	      position: relative;
	      color: rgba(0, 0, 0, .65);
	      background-color: #fff;
	      border-color: #d9d9d9;
	    }
	
	    #submit {
	      width: 100%;
	      height: 38px;
	      color: #fff;
	      background-color: #108ee9;
	      border-color: #108ee9;
	      margin-right: 8px;
	
	      margin-bottom: 10px;
	      font-size: 16px;
	    }
	
	     :-moz-placeholder {
	      /* Mozilla Firefox 4 to 18 */
	      color: #aaa;
	    }
	
	     ::-moz-placeholder {
	      /* Mozilla Firefox 19+ */
	      color: #aaa;
	    }
	
	    input:-ms-input-placeholder,
	    textarea:-ms-input-placeholder {
	      color: #aaa;
	    }
	
	    input::-webkit-input-placeholder,
	    textarea::-webkit-input-placeholder {
	      color: #aaa;
	    }
	
	    .left_top {
	      text-align: center;
	      font-family:"Microsoft YaHei";
	      /* float:left; */
	    }
	
	    .left_top p {
	      color: #fff;
	      font-size: 32px;
	      font-weight: bold;
	      padding-top: 40px;
	    }
	
	    .left_top div span {
	      color: #fff;
	      font-size: 16px;
	    }
	
	    .title,
	    .verify_title {
	      overflow: hidden;
	      background: #AAD1EE;
	      color: #000;
	      text-align: center;
	      width: 100%;
	      height: 38px;
	      line-height: 38px;
	      border-radius: 4px;
	      display: block;
	
	    }
	
	    .title span,
	    .verify_title span {
	      float: left;
	      width: 60px;
	        font-size: 14px;
	    }
	
	    .title input,
	    .verify_title input {
	      float: right;
	      padding-left: 10px;
	
	      outline: white;
	      border: 0px;
	      box-shadow: none;
	      background-color: #D0EBFF!important;
	      width: 170px;
	      height: 38px;
	      cursor: text;
	      line-height: 2.5;
	      margin-bottom: 10px;
	      font-size: 16px;
	      background-image: none;
	      border-radius: 0, 4px, 4px, 0;
	      -webkit-transition: all .3s;
	      transition: all .3s;
	    }
	
	    .verify_title input {
	      float: left;
	      width: 100px;
	    }
	
	    .title input:focus,
	    .verify_title input:focus {
	      outline: none; // 如何把蓝色去掉？
	      border: 0; // 如何改成别的颜色？
	      background-color: rgba(0, 0, 0, 0)!important;
	    }
	
	    .title input:-webkit-autofill,
	    .verify_title input:-webkit-autofill {
	      -webkit-box-shadow: 0 0 0px 1000px #D0EBFF inset;
	    }
	
	    *:focus {
	      outline: none;
	    }
	
	    .verify {
	      width: 70px;
	      border-radius: 0, 4px, 4px, 0;
	      background-color: #D0EBFF;
	      float: left;
	    }
    
	    #locale{
	    	position:absolute;
	    	top:-124px;
	    	right:11px;	    	
	        background: rgba(0,0,0, 0.8);
	        width: 100px;
	        height: 45px;
	        line-height: 58px;
	        cursor: text;
	        color: #fff;
	        border: 0;
	    }

/* ====================================新老分割线========================================== */
	  	form {
		 	position: absolute;
			top:50%;
			left:50%;
			margin:-190px  0  0 -512px;
			width:1024px;
			height:380px;
	  		
	  	}

  	</style>
  	<link rel="shortcut icon" href="<%=path %>/image/scm/logo.ico" />
	<link rel="icon" href="<%=path %>/image/scm/logo.ico" />
  </head>	
  
  <body> 
  	<div class="main">
    <div class="top">
       <span class="top_span">辰森餐饮供应链信息平台</span>
       <!-- <select id="locale" name="locale">
	       <option value="zh_CN">简体中文</option>
	       <option value="zh_TW">繁体中文</option>
	       <option value="en">英文</option>
       </select> -->
    </div>
    <div class="content">
      <div class="left">
        <div class="left_top">
          <p>选辰森，打造智慧生态连锁餐饮!</p>
       
          <div>
            <span>————————&nbsp;&nbsp;安全&nbsp;&nbsp;|&nbsp;&nbsp;稳健&nbsp;&nbsp;|&nbsp;&nbsp;创新&nbsp;&nbsp;————————</span>
          </div>
        </div>
      </div>
 	 	<form method="post" id="loginForm" name="loginForm" action="<%=path%>/login.do" >
	      <div class="right">
	 	 		<select id="locale" name="locale">
	 	 			<option value="zh_CN">简体中文</option>
	 	 			<option value="zh_TW">繁体中文</option>
	 	 			<option value="en">英&nbsp;&nbsp;&nbsp;&nbsp;文</option>
	 	 		</select> 
 	 		<div class="login">
	          <div class="login_content">
	            <span>用户登录</span>

				  <c:if test="${not empty msg}">
					  <span class="title_error">
						  <strong>
							  <i class="icon-remove"></i>
							  对不起!
						  </strong>
							  ${msg}
						  <c:if test="${not empty accLoginCount}">
							  <br>您还有${accLoginCount}次重试机会,或者进行找回密码！
						  </c:if>
						  <c:if test="${showActivation}">
							  ,<a href="javascript:showStep(5)">激活</a>！
						  </c:if>
					  </span>
				  </c:if>
	            <div class="title">
	            	<span>企业号</span>
	            	<input type="text" id="pk_group" name="pk_group"  onfocus="this.select()" value="${token.pkGroup}"/>
	            </div>
	            <span class="title_error" id="group_error"></span>
	            
	            <div class="title">
	            	<span>用户名</span>
	            	<input type="text" id="accountName" name="name" onfocus="this.select()" value="${token.username}"/>
	            </div>
	            <span class="title_error" id="name_error"></span>
	            
	            <div class="title">
	            	<span>密&nbsp;&nbsp;码</span>
	            	<input type="password" id="accountPwd" name="password" onfocus="this.select()" /></div>
	            <span class="title_error" id="password_error"></span>
	            
	            <div class="verify_title"><span>验证码</span><input type="text" name="validateCode" id="validateCode" />
					<img id="CreateCheckCode" src="<%=path %>/getImgRandom.do" alt="" onclick="getCodeNew()"/>
	            </div>	            
	            <span class="title_error" id="verify_error"></span>
	            
	            <span class="title_info_error" id="verify_error"></span>	            
	            <button name="button" id="submit">登录</button>
	          </div>
	        </div>
 	 	 </div>
 	 	</form>
	    <div class="foot">
	      <span>建议使用IE7.0及其以上版本，分辨率不低于1024*768</span>
	    </div>
	   </div>
  </div>
  	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.min.js"></script>
  	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
  	 <script type="text/javascript">
  	 	//获取验证码
  	 	function getCodeNew(){
			$("#CreateCheckCode").attr('src',"<%=path %>/getImgRandom.do?nocache=" + new Date().getTime());
		}
 	     //获取字节长度
	  	  function isNameLength(str) {
	 	     if(!str) {
	 	       return 0;
	 	     }
	 	     var sTmpStr,
	 	         sTmpChar;
	 	     var nOriginLen = 0;
	 	     var nStrLength = 0;
	 		     sTmpStr = new String(str);
	 		     nOriginLen = sTmpStr.length;
	 	     for(var i = 0; i < nOriginLen; i++) {
	 		       sTmpChar = sTmpStr.charAt(i);
	 		       if(escape(sTmpChar).length > 4) {
	 		         nStrLength += 2;
	 		       } else if(sTmpChar != '\r') {
	 		         nStrLength++;
	 		       }
	 	     }
	 	     return nStrLength;
	 	   }
  	 	$(document).ready(function(){  	 		
  	 		$('#pk_group').focus();
  	 		$('#submit').click(function(){
  	 			login();
  	 		});
  	 		
  	 		//添加“回车”事件
  	 		$(document).keyup(function(e){
  	 			if(e.keyCode === 13){
  	 				login();
  	 			}
  	 		});
  	 		
  	 		 /* <c:if test="${not empty info}">
	  	 		showMessage({
	  	 			type: 'error', 
	  	 			msg:'<c:out value="${info}"/>',
	  	 			speed:2500
	  	 		});
  	 		</c:if>  */ // 去掉原始提示消息
  	 		
  	 	});
  	 	
  	 	function reg(){
  	 		alert("注册暂未开放！");return;
  	 		$('body').window({
				id: 'window_reg',
				title: '用户注册',
				content: '<iframe id="register" frameborder="0" src="<%=path%>/register/initRegister.do"></iframe>',
				width: 497,
				height: 180,
				isModal: true
			});
  	 	}
  	 	
  	 	function login(){
			 <c:choose>
	  	 	 <c:when test="${loginOut=='loginOut'}">    
  	 			$('#loginForm').submit();
	  	 	 </c:when>
	  	 	 <c:otherwise>  
				$('#loginForm').submit();
	  	 	 </c:otherwise>
	  	 	 </c:choose> 
		}
        $('#loginForm').submit(function () {
            var pk_group = $("#pk_group").val();
            var accountName = $("#accountName").val();
            var accountPwd = $("#accountPwd").val();
            var validateCode = $("validateCode").val();
            var reg = /^\s*$/g;

            var group_error=""; name_error="",password_error="",verify_error="",title_info_error="";
            //  如果是空，或者""
            if(pk_group=="" || reg.test(pk_group)){
                group_error="企业号不能为空！";
                $('#group_error').html(group_error);
                return false;
            }
            if(accountName=="" || reg.test(accountName)){
                name_error="用户名不能为空！";
                $('#name_error').html(name_error);
                return false;
            }else if(isNameLength(accountName)>20){
                name_error="用户名长度不能超过10个！";
                $('#name_error').html(name_error);
                return false;
            }
            if(accountPwd=="" || reg.test(accountPwd)){
                password_error="密码不能为空！";
                $('#password_error').html(password_error);
                return false;
            }
            if(validateCode=="" || reg.test(validateCode)){
                verify_error="验证码不能为空！";
                $('#verify_error').html(verify_error);
                return false;
            }
			return true;
        });
		$("#login").bind("click", function(){
			$("#wait2").css('display','block');
			$("#wait").css('display','block');
		});
		function loadError(){
			$("#wait2").css('display','none');
			$("#wait").css('display','none');
		} 
  	 </script>
  </body>
</html>
