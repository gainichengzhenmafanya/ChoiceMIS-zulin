<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>
<%@page import="net.sf.jasperreports.engine.export.JRXlsExporterParameter"%>
<%@page import="net.sf.jasperreports.engine.export.JRXlsExporter"%>
<%@page import="net.sf.jasperreports.engine.JasperExportManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="net.sf.jasperreports.engine.data.JRBeanCollectionDataSource"%>
<%@page import="com.choice.orientationSys.util.IreportMapDataSource"%>
<%@page import="java.util.List"%>
<%@page import="net.sf.jasperreports.engine.JasperCompileManager"%>
<%@page import="net.sf.jasperreports.engine.util.JRLoader"%>
<%@page import="net.sf.jasperreports.engine.JasperReport"%>
<%@page import="net.sf.jasperreports.engine.JasperPrint"%>
<%@page import="net.sf.jasperreports.engine.JasperFillManager"%>
<%@page import="net.sf.jasperreports.engine.export.JRHtmlExporter"%>
<%@page import="net.sf.jasperreports.j2ee.servlets.ImageServlet"%>
<%@page import="net.sf.jasperreports.engine.JRExporterParameter"%>
<%@page import="net.sf.jasperreports.engine.export.JRHtmlExporterParameter"%>
<%@page import="net.sf.jasperreports.engine.JasperRunManager"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.io.File"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="message_to_print" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<style media="print">
			    .tool { display: none }
			</style>
			<style type="text/css">
				body{overflow: auto;}
			</style>
		</head>
	<body>
	<div class="tool">
	</div>
	<form action="<%=path%>${action}" id="listForm" name="listForm" target="report" method="post">
	<input type="hidden" name="type" id="type" />
      <%
	      Map map = (Map)request.getAttribute("actionMap");
     	  if(null!=map){
	     	  Iterator it = map.entrySet().iterator();
		      while (it.hasNext()) {
		    	
			      Map.Entry entry = (Map.Entry) it.next();
			      System.out.println(entry.getKey()+"::::::"+entry.getValue());
			      if(null!=entry.getValue()){
			      %>
			    	  <input type="hidden" name="<%=entry.getKey()%>" value="<%=entry.getValue()%>"/>
			      <%
			      }
			  }
	      } %>
	  </form>
	  <% 
          //接收传入参数
	      List  list=(List)request.getAttribute("List");
	      HashMap parameters =(HashMap)request.getAttribute("parameters");
	      String reportUrl=(String)request.getAttribute("reportUrl");
          // HTML Export：
	      File reportFile = new File(getServletContext().getRealPath(reportUrl));
	      JasperReport jasperReport = (JasperReport) JRLoader.loadObject(reportFile.getPath());
	      IreportMapDataSource jdt=new IreportMapDataSource(list);
	      //填充报表
	      JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,jdt);
	      //导出HTML格式：
          JRHtmlExporter exporter = new JRHtmlExporter();
          request.getSession().setAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE,jasperPrint);
          exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);//--使用报表(JasperPrint)
          exporter.setParameter(JRExporterParameter.OUTPUT_WRITER, out);//--输出到流
          exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI,"../image/px.");
          exporter.exportReport();
      %>
        <OBJECT id="WebBrowser" height="0" width="0" classid="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2" VIEWASTEXT></OBJECT>
 
  		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
   	    <script type="text/javascript">
			$(document).ready(function(){
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="export" />PDF',
							title: '<fmt:message key="export" />PDF',
							icon: {
								url: '<%=path%>/image/Button/pdf.png',
								position: ['5px','2px']
							},
							handler: function(){
								actionReport("pdf");
							}
						},{
							text: '<fmt:message key="export" />Excel',
							title: '<fmt:message key="export" />Excel',
							icon: {
								url: '<%=path%>/image/Button/excel.bmp',
								position: ['5px','2px']
							},
							handler: function(){
								actionReport("excel");
							}
						},{
							text: '<fmt:message key="direct_pdf_printing" />',
							title: '<fmt:message key="direct_pdf_printing" />',
							icon: {
								url: '<%=path%>/image/Button/pdf.png',
								position: ['5px','2px']
							},
							handler: function(){
								actionReport("printPdf");
							}
						}
						/* 屏蔽按钮 jinshuai
						,{
							text: '<fmt:message key="print_preview" />',
							title: '<fmt:message key="print_preview" />',
							icon: {
								url: '<%=path%>/image/Button/excel.bmp',
								position: ['5px','2px']
							},
							handler: function(){
								document.all.WebBrowser.ExecWB(7,1);
							//	actionReport("printExcel");
							}
						},{
							text: '<fmt:message key="page_setup" />',
							title: '<fmt:message key="page_setup" />',
							icon: {
								url: '<%=path%>/image/Button/word.bmp',
								position: ['5px','2px']
							},
							handler: function(){
								document.all.WebBrowser.ExecWB(8,1);
								//actionReport("printWord");
							}
						},{
							text: '<fmt:message key="print" />',
							title: '<fmt:message key="print" />',
							icon: {
								url: '<%=path%>/image/Button/word.bmp',
								position: ['5px','2px']
							},
							handler: function(){
								window.print();
								//document.all.WebBrowser.ExecWB(6,1);
								//actionReport("printWord");
							}
						},{
							text: '<fmt:message key="direct_printing" />',
							title: '<fmt:message key="direct_printing" />',
							icon: {
								url: '<%=path%>/image/Button/word.bmp',
								position: ['5px','2px']
							},
							handler: function(){
								document.all.WebBrowser.ExecWB(6,6);
								//actionReport("printWord");
							}
						}
						*/
					]
				}); 
			});	
			//回调方法
			function actionReport(typ){
				$('#type').val(typ);
				window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
				$('#listForm').submit();
			}
			function readOfficeVersion(){
				var version="";
				var word=null;
				try{
					word=new ActiveXObject("Word.application");
				}catch(e){
					alert('<fmt:message key="not_detected_installed_on_your_computer" />office!/n1、'
							+'<fmt:message key="please_check_whether_you_are_using_IE_browser" />；\n2、'
							+'<fmt:message key="please_check_if_your_machine_is_installed" />Microsoft Office 2003/2007/2010;\n3、'
							+'<fmt:message key="whether_you_check_your_browser_settings_to_enable_ActiveX_controls" />');
				}
				var result = word.Version;
				version = result == "11.0" ? "office2003" : (result == "12.0" ? "office2007" : (result == "14.0" ? "office2010" : "unknown version"));
				//及时关闭Word进程
				word.Application.Quit();
				return version;
			}
	</script>
	</body>
</html>