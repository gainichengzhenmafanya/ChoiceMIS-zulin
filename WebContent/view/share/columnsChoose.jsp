<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
String path = request.getContextPath();
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
<title><fmt:message key="column_selection" /></title>
</head>
<body>
	<div class="tool"></div>
	<form method="post" name="myform">
	  <table border="0" width="450" style="margin:6px 3px;">
	    <tr>
	      <td width="40%">
			<select id="list1" style="width:100%;" multiple="multiple" name="list1" size="20" ondblclick="moveOption(document.myform.list1, document.myform.list2)">
				<c:forEach var="dictColumns" items="${dictColumnsListByName}">
					<option value="${dictColumns.id}">${dictColumns.zhColumnName}</option>
				</c:forEach>
			</select>
		  </td>
	      <td width="20%" align="center">
	      	<input type="hidden" id="tableName" value="${tableName}">
	      	<input type="button" style="width: 70px;height: 27px;" title="向上" value='↑' onclick="upColumns()"><br><br>
			<input type="button" style="width: 70px;height: 27px;" title="添加" value='>' onclick="moveOption(document.myform.list1, document.myform.list2)"><br><br>
			<input type="button" style="width: 70px;height: 27px;" title="删除" value='< ' onclick="moveOption(document.myform.list2, document.myform.list1)"><br><br>
			<input type="button" style="width: 70px;height: 27px;" title="添加全部" value='>>' onclick="operateAll(document.myform.list1, document.myform.list2)"><br><br>
			<input type="button" style="width: 70px;height: 27px;" title="删除全部" value='<< ' onclick="operateAll(document.myform.list2, document.myform.list1)"><br><br>
			<input type="button" style="width: 70px;height: 27px;" title="向下" value='↓' onclick="downColumns()"><br><br>
		  </td>
	      <td width="40%">
			<select id="list2" style="width:100%;" multiple name="list2" size="20" ondblclick="moveOption(document.myform.list2, document.myform.list1)">
				<c:forEach var="dictColumns" items="${dictColumnsListByAccount}">
					<option value="${dictColumns.id}">${dictColumns.zhColumnName}</option>
				</c:forEach>
			</select>
		  </td>
	    </tr>
	  </table>
	</form>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script language="JavaScript">
		$(document).ready(function(){
			//移除list1以选中的字段
			$('#list2 option').each(function(){
				$('#list1 option').remove("option[value="+$(this).val()+"]");
			 });
			var tool = $('.tool').toolbar({
				items: [{
						id:	'saveButton',
						text: '<fmt:message key="save" />',
						title: '<fmt:message key="save" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','0px']
						},
						handler: function(){
							selectColumns();
							$('#saveButton').unbind();
						}
					},{
						text: '<fmt:message key="restore_the_default" />',
						title: '<fmt:message key="restore_the_default" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-18px','0px']
						},
						handler: function(){
							recoverDefaultColumns();
						}
					}
				]
			});
		});
		//左右移动
		function moveOption(e1, e2){
			try{
				for(var i=0;i<e1.options.length;i++){
					if(e1.options[i].selected){
						var e = e1.options[i];
						e2.options.add(new Option(e.text, e.value));
						e1.remove(i);
						i=i-1;
					}
				}
			}
			catch(e){}
		}
		//整体左右移动
		function operateAll(e1, e2){
			try{
				for(var i=0;i<e1.options.length;i++){
					var e = e1.options[i];
					e2.options.add(new Option(e.text, e.value));
					e1.remove(i);
					i=i-1;
				}
			}
			catch(e){}
		}
		//选择
		function selectColumns(){
			var columnIds = [];
			$("#list2 option").each(function(){
				columnIds.push($(this).val());
			 });
			window.location="<%=path%>/${objBean}/saveColumnsChoose.do?id="+
							columnIds.join(",")+
							"&tableName="+$('#tableName').val();
// 			window.parent.location.reload();
		}
		//恢复默认
		function recoverDefaultColumns(){
			if(confirm('<fmt:message key="are_you_sure_you_want_to_restore_the_default_display_field" />？')){
				window.location="<%=path%>/${objBean}/recoverDefaultColumns.do?tableName="+$('#tableName').val();
			}
		}
		//上移
		function upColumns(){
			var prvObj = $('#list2 option[selected!=""]').prev();
			prvObj.before($('#list2 option[selected!=""]'));
		}
		//下移
		function downColumns(){
			var prvObj = $('#list2 option[selected!=""]').next();
			prvObj.after($('#list2 option[selected!=""]'));
		}
	</script>
</body>
</html>