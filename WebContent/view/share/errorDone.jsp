<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Done Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
	</head>
	<body>
		<div class="bgDiv"></div>
		<div class="message error">
			<c:out value="${msg}"></c:out>
		</div>
		<input type="hidden" id="idCard" value="${studentInfo.identityId}"/>
		
  	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript">
			
		
			$(document).ready(function(){
				
				var top = ($('.bgDiv').height() - $('.message').outerHeight()) * 0.5;
				var left = ($('.bgDiv').width() - $('.message').outerWidth()) * 0.5;
				
				$('.bgDiv').fadeTo('fast',0.33,function(){
					$('.message').css({
						'margin-top': top+'px',
						'margin-left': left+'px'
					}).show();
				});
				
				window.setTimeout(function(){
					if(typeof(eval(parent.pageReload))=='function')
						parent.pageReload();
					else
						parent.location.href = parent.location.href;
						//parent.location.reload();
				}, 1500);
			});
			
		</script>
	</body>
</html>