<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="lo" uri="/WEB-INF/tld/local.tld"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>模块信息</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<style type="text/css">
			
			</style>
		</head>
	<body>
	  <div class="treePanel">
	    <script src="<%=path%>/js/tree/MzTreeView10.js" type="text/javascript"></script>
	     <script type="text/javascript">
	       var tree = new MzTreeView("tree");
	       
	       tree.nodes['0_00000000000000000000000000000000'] = 'text:模块; dbmethod:changeUrl(\'00000000000000000000000000000000\',\'模块\')';
	       <c:forEach var="module" items="${moduleList}" varStatus="status">
	        	tree.nodes['${module.parentModule.id}_${module.id}'] = 'text:${lo:show(module.name)}; dbmethod:changeUrl(\'${module.id}\',\'${lo:show(module.name)}\')';
	       </c:forEach>
	       tree.setIconPath("<%=path%>/js/tree/");
	       document.write(tree.toString());
	     </script>
	  </div>
    <input type="hidden" id="moduleId" name="moduleId" />
    <input type="hidden" id="moduleName" name="moduleName" />
    
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript">
			function changeUrl(moduleId,moduleName)
	    {
	      $('#moduleId').val(moduleId);
	      $('#moduleName').val(moduleName);
	      top.closeSelect();
	    }
			
			$(document).ready(function(){
				$('.treePanel').height($(document.body).height());
			});// end $(document).ready();
		</script>

	</body>
</html>