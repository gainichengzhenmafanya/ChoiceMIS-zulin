

function addUser(path){
	$('body').window({
		id: 'window_addUser',
		title: '新增组信息',
		content: '<iframe id="addUserFrame" frameborder="0" src="'+path+'/user/add.do"></iframe>',
		width: '650px',
		height: '500px',
		isModal: true,
		draggable: true,
		topBar: {
			items: [{
				text: '保存',
				title: '保存组信息',
				icon: {
					url: path+'/image/Button/op_owner.gif',
					position: ['-80px','-0px']
				},
				handler: function(){
					if(getFrame('addUserFrame')&&window.document.getElementById("addUserFrame").contentWindow.validate._submitValidate()){
						submitFrameForm('addUserFrame','userForm');
					}
				}
			},{
				text: '取消',
				title: '取消',
				icon: {
					url: path+'/image/Button/op_owner.gif',
					position: ['-160px','-100px']
				},
				handler: function(){
					$('.close').click();
				}
			}]
		}
	});
}

function updateUser(path,chkValue){
		
	$('body').window({
		id: 'window_updateUser',
		title: '修改组信息',
		content: '<iframe id="updateUserFrame" frameborder="0" src="'+path+'/user/update.do?id='+chkValue+'"></iframe>',
		width: '650px',
		height: '500px',
		isModal: true,
		draggable: true,
		topBar: {
			items: [{
				text: '保存',
				title: '保存组信息',
				icon: {
					url: path+'/image/Button/op_owner.gif',
					position: ['-80px','-0px']
				},
				handler: function(){
					if(getFrame('updateUserFrame')&&window.document.getElementById("updateUserFrame").contentWindow.validate._submitValidate()){
						submitFrameForm('updateUserFrame','userForm');
					}
				}
			},{
				text: '取消',
				title: '取消',
				icon: {
					url: path+'/image/Button/op_owner.gif',
					position: ['-160px','-100px']
				},
				handler: function(){
					$('.close').click();
				}
			}]
		}
	});
	
}

function deleteUser(path,param){
	
	$('body').window({
		id: 'window_deleteUser',
		title: '提示',
		content: '<iframe id="deleteUserFrame" frameborder="0" src="'+path+'/user/delete.do?ids='+param+'"></iframe>',
		width: '650px',
		height: '500px',
		isModal: true
	});
}


