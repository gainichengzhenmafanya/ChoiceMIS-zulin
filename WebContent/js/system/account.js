
function showAccount(path,userId){
	var showWin = $('body').window({
		id: 'window_showAccount',
		title: '显示账号信息',
		width: '650px',
		height: '500px',
		isModal: true,
		draggable: true,
		confirmClose: false
	});
	
	$content = showWin._win_content;
	accountTab = $content.tab({
		width: $content.width()-2,
		height: $content.height()-2,
		items: [{
				id: 'tab_tableAccount',
				text: '账号列表',
				title: '账号列表',
				icon: {
					url: path+'/image/Button/set.png'
				},
				content: '<iframe id="tableAccountFrame" frameborder="0" src="'+path+'/account/tableFromUser.do?userId='+userId+'"></iframe>'
			}
		]
	});
	
}

function changeState(path,ids,state,show,context){
	context = context || $(document.body);	
	$.ajax({
		url: path+'/account/ajaxChangeState.do',
		type: 'POST',
		data: 'state='+state+'&ids='+ids,
		dataType: 'html',
		success: function(info){
			if(info && info === 'T'){
				
				//弹出提示信息
				showMessage();					
				
				//修改选中账号的标识
				var idArray = ids.split(',');
				$.each(idArray,function(i){
					var id = idArray[i];
					$('#state_'+id,context)
						.children('span')
						.removeClass()
						.addClass('accountState_'+state)
						.html(show+"&nbsp;");
					$('#chk_'+id,context).removeAttr('checked');
				});
				
			}
		}
	});
}

function addAccountFromUser(path,userId){
	var accountTab = accountTab || parent.window.accountTab;
	accountTab.addItem([{
			id: 'tab_addAccount',
			text: '新增账号',
			title: '新增账号信息',
			closable: true,
			icon: {
				url: path+'/image/Button/set.png'
			},
			content: '<iframe id="addAccountFrame_FromUser" frameborder="0" src="'+path+'/account/addFromUser.do?userId='+userId+'"></iframe>'
		}
	]);
	accountTab.show('tab_addAccount');
}

function updateAccountFromUser(path,accountId){
	var accountTab = accountTab || parent.window.accountTab;
	accountTab.addItem([{
			id: 'tab_updateAccount',
			text: '修改密码',
			title: '修改账号密码',
			closable: true,
			icon: {
				url: path+'/image/Button/set.png'
			},
			content: '<iframe id="updateAccountFrame_FromUser" frameborder="0" src="'+path+'/account/updateFromUser.do?id='+accountId+'"></iframe>'
		}
	]);
	accountTab.show('tab_updateAccount');
}

function updateAccount(path,accountId){
	updateAccountWin = $('body').window({
		id: 'window_updateAccount',
		title: '修改账号密码',
		content: '<iframe id="updateAccountFrame" frameborder="0" src="'+path+'/account/update.do?id='+accountId+'"></iframe>',
		width: '650px',
		height: '500px',
		isModal: true,
		draggable: true,
		confirmClose: false
	});
}

function addAccount(path,userId){
	addAccountWin = $('body').window({
		id: 'window_addAccount',
		title: '新增账号信息',
		content: '<iframe id="addAccountFrame" frameborder="0" src="'+path+'/account/add.do?userId='+userId+'"></iframe>',
		width: '650px',
		height: '500px',
		isModal: true,
		draggable: true,
		confirmClose: false
	});
}

function deleteAccount(path,param){
	$.ajax({
		type: 'POST',
		url: path+'/account/ajaxDelete.do',
		data: 'ids='+param,
		dataType: 'html',
		success: function(info){
			if(info && info === 'T'){
				
				//弹出提示信息
				showMessage({
					handler: function(){
						var accountTab = parent.window.accountTab;
						if(accountTab){
							var $tableFrame = accountTab.getItem('tab_tableAccount').div.find('#tableAccountFrame');
					    	$tableFrame.attr('src',$tableFrame.attr('src'));
						}else{
							window.pageReload();
						}
					}
				});					
			}
		}
	});
}

function relateRole(path,accountId){
	relateRoleWin = $('body').window({
		id: 'window_relateRole',
		title: '关联角色',
		content: '<iframe id="relateRoleFrame" frameborder="0" src="'+path+'/accountRole/table.do?accountId='+accountId+'"></iframe>',
		width: '650px',
		height: '500px',
		isModal: true,
		draggable: true,
		confirmClose: false
	});
}

