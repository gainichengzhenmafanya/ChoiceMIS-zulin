

function addDepartment(path){
	$('body').window({
		id: 'window_addDepartment',
		title: '新增部门信息',
		content: '<iframe id="addDepartmentFrame" frameborder="0" src="'+path+'/department/add.do?"></iframe>',
		width: '650px',
		height: '500px',
		isModal: true,
		draggable: true,
		topBar: {
			items: [{
				text: '保存',
				title: '保存部门信息',
				icon: {
					url: path+'/image/Button/op_owner.gif',
					position: ['-80px','-0px']
				},
				handler: function(){
					if(getFrame('addDepartmentFrame')&&window.document.getElementById("addDepartmentFrame").contentWindow.validate._submitValidate()){
						submitFrameForm('addDepartmentFrame','departmentForm');
					}
				}
			},{
				text: '取消',
				title: '取消',
				icon: {
					url: path+'/image/Button/op_owner.gif',
					position: ['-160px','-100px']
				},
				handler: function(){
					$('.close').click();
				}
			}]
		}
	});
}

function updateDepartment(path,chkValue){
		
	$('body').window({
		id: 'window_updateDepartment',
		title: '修改部门信息',
		content: '<iframe id="updateDepartmentFrame" frameborder="0" src="'+path+'/department/update.do?id='+chkValue+'"></iframe>',
		width: '650px',
		height: '500px',
		isModal: true,
		draggable: true,
		topBar: {
			items: [{
				text: '保存',
				title: '保存部门信息',
				icon: {
					url: path+'/image/Button/op_owner.gif',
					position: ['-80px','-0px']
				},
				handler: function(){
					if(getFrame('updateDepartmentFrame')&&window.document.getElementById("updateDepartmentFrame").contentWindow.validate._submitValidate()){
						submitFrameForm('updateDepartmentFrame','departmentForm');
					}
				}
			},{
				text: '取消',
				title: '取消',
				icon: {
					url: path+'/image/Button/op_owner.gif',
					position: ['-160px','-100px']
				},
				handler: function(){
					$('.close').click();
				}
			}]
		}
	});
	
}

function saveDepartment(path,param){
	
	$('body').window({
		id: 'window_updateDepartment',
		title: '提示',
		content: '<iframe id="updateDepartmentFrame" frameborder="0" src="'+path+'/department/saveByUpdate.do?'+param+'"></iframe>',
		width: '650px',
		height: '500px',
		isModal: true
	});
}

function deleteDepartment(path,param){
	var flag = true;
	var par = {};
	par['ids'] = param;
	$.post(path+"/department/checkDepartmentCode.do",par,function(data){
		flag = data;
	});
	if(!flag){
		alert('所选部门下还有人员，无法删除！');
		return;
	}
	$('body').window({
		id: 'window_deleteDepartment',
		title: '提示',
		content: '<iframe id="deleteDepartmentFrame" frameborder="0" src="'+path+'/department/delete.do?id='+param+'"></iframe>',
		width: 538,
		height: '215px',
		isModal: true
	});
}



