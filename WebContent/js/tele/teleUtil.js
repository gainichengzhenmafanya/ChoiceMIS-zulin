(function($){
	$.fn.extend({
		teleUtil:function(options,args){
			if(options == 'autoFill'){
				var self = $(this);
				var table = self.find("table");
				var tr = self.data('tr') ? self.data('tr'): table.find("tr:first").clone();
				var url = args.url;//表格填充数据 json
				var param = args.param;//提交url参数
				var content = [];
				var selected = args.selected;
				var id = args.id;//表格id
				var cols = args.cols ? (args.cols.length ? args.cols : []) : [];//填充数据key
				table.html('');
				tr.removeClass('tr-over');
				tr.find('td > span').contents().filter(function(){
					return this.nodeType == 3;
				}).remove();
				self.data('tr',tr);
				$.ajax({
					url:url,
					data:param,
					type:'POST',
					async:false,
					success:function(data){
						content = data;
					}
				});
				for(var i in content){
					var cur = content[i];
					var curRow = tr.clone();
					if(selected && $.inArray(String(cur[id]),selected) >= 0){
						curRow.children("td:eq("+1+")").children("input").attr('checked','checked');
					}
					curRow.children("td:eq("+0+")").text(Number(i)+1);
					curRow.children("td:eq("+1+")").find("input")
					.val(cur[id])
					.attr('id','chk_'+cur[id]);
					for(var j = 0 ; j < cols.length ; j ++){
						curRow.children("td:eq("+(j+2)+")").children("span").text(cur[cols[j]]);
					}
					table.append(curRow);
				}
				
				
			
			}else if(options == "getWeekOfYear"){//获取日期为该年的第几周，周一为一周的开始
				var dat = args;
				var typ = Object.prototype.toString.call(dat);
				switch(typ){
					case "[object String]":
						var cur = dat.match(/\d+/g);
						dat = new Date(cur[0],Number(cur[1]) - 1,cur[2]);
						break;
					case "[object Date]":
						break;
					default:
						alert("请输入合法的日期格式！");
				}
				var firstDay = new Date(dat.getFullYear(),0,1);
				var days = Math.round((dat - firstDay)/86400000);
				return Math.ceil((days + firstDay.getDay())/7);
			}
			
			
			
		}
	});
})(jQuery);