(function($){
	$.fn.extend({
		autoGrid:function(options,args){
			var settings = $.extend({},{
					initRow:1,//初始行数
					colPerRow:12,//每行单元格数
					showRowNum:false,
					rowNumWidth:25,
					RowCheckBox:false,
					rowChkBoxWidth:25,
					addtion:{0:{maxLength:3}},
					RowDeletButton:true,
					idColumn:0,
					onBeforeEdit:function(curRow,span,addtion){
						//为单元格添加input
						if(span.children("input").length > 0){
							span.text(span.children("input").val());
						}
						var input = $("<input id='input' name='input' style='border:0;' />");
						if(addtion[span.data('cellIndex')]){
							var p = addtion[span.data('cellIndex')];
							if(p.maxLength){
								//input.attr('maxlength',p.maxLength);
								input.bind('blur keyup',function(event){
									$(this).limitLength(p.maxLength);
									event.stopPropagation();
								});
							}
						}
						var text = $.trim(span.text());
						input.width(span.width()-4);
						input.val(text);
						span.html('');
						input.click(function(){
							return false;
						});
						input.appendTo(span).focus().select();
						return input;
					},
					onAfterEdit:function(curRow,span,obj,addtion){
						obj.remove();
						span.text(obj.val());
					},
					onCancelEdit:function(curRow,span,obj,addtion){
						$(obj).remove();
						span.html(span.data("prevVal") ? span.data("prevVal") : '');
					},
					verify:function(curRow,span,obj){
						return true;
					},
					VerifyEdit:{verify:false,enable:$.noop},//表格编辑条件
					//editable:[1,4,6,9],//可编辑表格index
					widths:[26,100,180,80,80,80,50,80,80,80],//每格宽度
					colStyle:{},
					afterDelet:$.noop,
					cellAction:{
						2:{
							onBeforeEdit:function(curRow,span){
								var select = $('<select><option value="1">1</option><option value="2">2</option></select>');
								select.appendTo(span);
								return select;
							},
							verify:function(curRow,span,obj){
								return true;
							},
							onAfterEdit:function(curRow,span,obj){
								obj.remove();
								span.text(obj.val());
							},
							onCancelEdit:function(curRow,span,obj){
								$(obj).remove();
								span.html(span.data("prevVal") ? span.data("prevVal") : '');
							}
						}
					}
			},options);
			
			var self = $(this);
			var div = $("<div></div>");
			var table = $("<table cellspacing='0' cellpadding='0'></table>");
			var row = $("<tr></tr>");
			var cell = $("<td><span style='display:block;'>&nbsp;</span></td>");
			var editList = settings.editable;
			var cellAction = settings.cellAction;
			var revise = 0;
			var rowDelCell = $('<td name="deleCell" style="width:10px;border:0;cursor: pointer;"><img src="../image/scm/move.gif"/></td>');
			var rowNumCell = $('<td name="rowNum" style="text-align:center;"><span style="display:block;padding:1px;"></span></td>');
			var rowChkCell = $('<td name="rowChk" style="text-align:center;"><span style="display:block;padding:1px;"><input type="checkbox"/></span></td>');
			//var rowDelCell = $('<td name="deleCell" style="width:10px;border:0;cursor: pointer;">del</td>');
			
			if(!settings.editable){
				settings.editable = [];
				editList = settings.editable;
				for(var i = 0 ; i < settings.colPerRow ; i ++)editList.push(i);
			}
			rowNumCell.find("span").width(settings.rowNumWidth);
			rowChkCell.find("span").width(settings.rowChkBoxWidth);
			//判断当前div内有无表格，有则追加行并追加删除按钮，没有则新建表格
			if(self.find("tr:first").size()){//div内有表格
				if(settings.showRowNum){//添加行号
					revise ++;
				}
				if(settings.RowCheckBox){//添加选择框
					revise ++;
				}
				table = self.find("table");
				if(!table.data('ready')){
					table.data('revise',revise);
					table.data('showRowNum',settings.showRowNum);
					table.data('RowCheckBox',settings.RowCheckBox);
					table.data('RowDeletButton',settings.RowDeletButton);
					table.data('idColumn',settings.idColumn);
					table.data('afterDelet',settings.afterDelet);
					table.find("tr > td > span").each(function(){
						if($(this).data('val_dataGrid') == undefined)
							$(this).data("val_dataGrid",$.trim($(this).text()));
					});
				}
				if(settings.showRowNum && (!table.data('ready'))){//修改序号列名字
					var td = $(this).find('td:eq('+ (revise - 1) +')');
					if(td.attr('name') != 'rowNum')
						td.attr('name','rowNum');
				}
				if(settings.RowCheckBox && (!table.data('ready'))){//修改checkBox列名字
					var td = $(this).find('td:first');
					if(td.find(':checkbox').size() && td.attr('name') != 'rowChk')
						td.attr('name','rowChk');
				}
				row = table.find("tr:first").clone();
				if(settings.RowDeletButton){
					if(row.find('td[name="deleCell"]').size()) row.find('td[name="deleCell"]').remove();
					row.append(rowDelCell);
					if(!table.data('ready')){
						var trs = table.find('tr');
						trs.each(function(){//添加删除按钮,设置checkbox名字
							$(this).data('rowIndex',trs.index($(this)));
//							var lastCell = $(this).find('td:last');
//							if(!lastCell.attr('name') || lastCell.attr('name') != 'deleCell'){
//								var cur = rowDelCell.clone();
//								$(this).append(cur);
//								cur.click(function(){
//									delRow($(this).closest('tr'));
//								});
//							}
						});
						//table.find('tr:first').find('td[name="deleCell"]').replaceWith('<td name="deleCell" style="width:20px;border:0"></td>');
					}
				}
				row.find('td[name="deleCell"]').replaceWith('<td name="deleCell" style="width:10px;border:0;cursor: pointer;"><img src="../image/scm/move.gif"/></td>');
				row.find('td > span').contents().filter(function(){
					return this.nodeType == 3;
				}).remove();
				table.data('ready',true);
			}else{//div内无表格
				self.html('');
				for(var i = 0 ; i < settings.colPerRow ; i ++){
					var curCell = cell.clone();
					if(settings.widths[i])
						curCell.find('span').width(settings.widths[i]-10);
					if(settings.colStyle[i]){
						curCell.attr('style',settings.colStyle[i]);
					}
					curCell.appendTo(row);
				}
				if(settings.showRowNum){//添加行号
					revise ++;
					row.prepend(rowNumCell.clone());
				}
				if(settings.RowCheckBox){//添加选择框
					revise ++;
					row.prepend(rowChkCell.clone());
				}
				if(settings.RowDeletButton){//添加删除按钮
					row.append(rowDelCell.clone());
				}
				table.appendTo(self);
				for(var i = 0 ; i < settings.initRow ; i ++)addRow();
				table.find('tr:first').find('td[name="deleCell"]').replaceWith('<td name="deleCell" style="width:20px;border:0"></td>');
				if(settings.showRowNum || settings.RowCheckBox || settings.RowDeletButton){
					table.find('tr').each(function(){//为tr添加行号
						if(settings.showRowNum){
							$(this).find("td[name='rowNum']").find("span").text(table.find('tr').index(this)+1);
						}
					});
				}
				table.data('revise',revise);
				table.data('showRowNum',settings.showRowNum);
				table.data('RowCheckBox',settings.RowCheckBox);
				table.data('RowDeletButton',settings.RowDeletButton);
				table.data('idColumn',settings.idColumn);
				table.data('afterDelet',settings.afterDelet);
				table.data('ready',true);
			}
			revise = table.data('revise') ? table.data('revise') : revise;
			var showRowNum = table.data('showRowNum');
			var RowCheckBox = table.data('RowCheckBox');
			var RowDeletButton = table.data('RowDeletButton');
			var idColumn = table.data('idColumn');
			//添加单击事件（判断单元格是否可编辑并进行相应操作）
			if(typeof(options) != 'string'){
				table.click(function(event){
					if(table.data('prevEdit')){
						var prevEdit = $(table.data('prevEdit'));
						prevEdit.find('span').html(prevEdit.find('span').data('prevVal'));
					}
					if($(event.target).closest('td').get(0) && $(event.target).closest('td').get(0).tagName){
						var cell = $($(event.target).closest('td'));
						var curRow = $(cell.closest("tr"));
						var posX = curRow.find('td').index(cell);
						if(editList){
							if($.inArray(posX - revise, editList) >= 0)setEditable(cell);
						}else{
							editList = [];
							for(var i = 0 ; i < settings.widths.length ; i ++)editList.push(i);
							setEditable(cell);
						}
					}
					event.stopPropagation();
				});
			}else if(options == 'setEditable'){
				setEditable(args);
			}else if(options == 'addRow'){
				addRow(args);
			}else if(options == 'delRowBatch'){
				delRowBatch(args);
			}else if(options == 'getSelected'){
				return getSelected();
			}else if(options == 'getRow'){
				return getRow(args);
			}else if(options == 'getModified'){
				return getModified();
			}else if(options == 'getDeleted'){
				return getDeleted();
			}else if(options == 'getAddedRow'){
				return getAddedRow();
			}else if(options == 'resetRowStatus'){
				resetRowStatus(args);
			}else if(options == 'delRow'){
				delRow(args);
			}
			
			
			//设置表格可编辑
			function setEditable(cell){
				if(settings.VerifyEdit.verify){
					if(!settings.VerifyEdit.enable(cell))return;
				}
				table.data('prevEdit',cell);
				var span = $(cell).find("span");
				span.data('prevVal',span.html());
				var curRow = $(cell).closest("tr");
				var index = $.inArray(curRow.find('td').index($(cell)) - revise,editList);
				var addtion = settings.addtion;
				var sellIndex = curRow.find('td').index($(cell)) - revise;
				var beforeEdit = cellAction && cellAction[sellIndex] && cellAction[sellIndex].onBeforeEdit ? cellAction[sellIndex].onBeforeEdit : settings.onBeforeEdit;
				var afterEdit = cellAction && cellAction[sellIndex] && cellAction[sellIndex].onAfterEdit ? cellAction[sellIndex].onAfterEdit : settings.onAfterEdit;
				var cancelEdit = cellAction && cellAction[sellIndex] && cellAction[sellIndex].onCancelEdit ? cellAction[sellIndex].onCancelEdit : settings.onCancelEdit;
				var verify = cellAction && cellAction[sellIndex] && cellAction[sellIndex].verify ? cellAction[sellIndex].verify : settings.verify;
				span.data('cellIndex',sellIndex);
				span.unbind(".autoGrid");
				var obj = beforeEdit(curRow,span,addtion);
				obj.click(function(event){
					event.stopPropagation();
				});
				//ie9,ie10兼容
				setTimeout(function(){
					obj.blur(function(event){
						cancelEdit(curRow,span,obj,addtion);
						event.stopPropagation();
					});
				},0);
				span.bind("keydown.autoGrid",function(event){
					switch(event.keyCode){
						case 37://Left
							if(!verify(curRow,span,obj)){
								obj = beforeEdit(curRow,span,addtion);
								return;
							}
							afterEdit(curRow,span,obj,addtion);
							if(index > 0){
								setEditable(curRow.children('td')[editList[index - 1] + revise]);
								$(curRow.children('td')[editList[index - 1] + revise]).trigger("click");
							}else{
								var cell = curRow.prev('tr').size() ? curRow.prev('tr').find("td")[editList[editList.length - 1] + revise] : curRow.find("td")[editList[0] + revise];
								setEditable(cell);
							}
							return false;
							break;
						case 40://Done
						case 38://Up
							break;
						case 39://Right
						case 13://Enter
						case 108:
							if(!verify(curRow,span,obj)){
								obj = beforeEdit(curRow,span,addtion);
								return;
							}
							afterEdit(curRow,span,obj,addtion);
							if(index < editList.length - 1){
								setEditable(curRow.children('td')[editList[index + 1] + revise]);
							}else{
								if(!curRow.next('tr').size()){
									addRow();
								}
								var nextRow = curRow.next('tr');
								setEditable(nextRow.find("td")[editList[0] + revise]);
							}
							if(!span.closest('tr').data("newRow") && span.data('val_dataGrid') != $.trim(obj.val())){
								span.closest('tr').data('changed',true);
							}
							return false;
							break;
						case 27://Esc
							cancelEdit(curRow,span,obj,addtion);
							return false;
							break;
					}
				});
			}
			
			
			//添加行
			function addRow(content){
				var newRow = row.clone();
				newRow.data('rowIndex',table.find('tr').size());
				newRow.data("newRow",true);
				newRow.find("td span").each(function(){
					$(this).data('val_dataGrid','');
				});
				if(content){
					var spans = newRow.find('td > span');
					for(var i = revise ; i < content.length + revise ; i ++){
						$(spans[i]).html(content[i - revise]);
					}
				}
				newRow.appendTo(table);
				if(showRowNum)
					newRow.children('td[name="rowNum"]').find("span")
						.text(table.find('tr').length);
				table.closest('div').css('height',table.closest('div').height());
				if(RowDeletButton)
					newRow.children('td[name="deleCell"]').click(function(){
						var curTr = $(this).closest('tr');
						delRow(curTr);
						table.data('afterDelet')(curTr);
					});
			}
			
			//批量删除行
			function delRowBatch(rowIndex){
				var trs = table.find('tr').filter(function(){
					return ($.inArray($(this).data('rowIndex'),rowIndex) >= 0);
				});
				trs.each(function(){
					delRow($(this));
				});
			}
			
			//删除行
			function delRow(tr){
				var rowH = tr.height();
				var tbH = table.height();
				tr.nextAll("tr").each(function(){
					var curIndex = Number($(this).data('rowIndex'));
					$(this).data('rowIndex',-- curIndex);
					var curNum = Number($.trim($(this).children("td[name='rowNum']").text()));
					$(this).children("td[name='rowNum']").find('span').html(Number(curNum-1));
				});
				var id = $(tr.find('td')[idColumn + revise]).find('span').data('val_dataGrid');
				if(id){
					var deleted = table.data('deleted') ? table.data('deleted') : [];
					deleted.push(id);
					table.data('deleted',deleted);
				}
				if(!table.find('tr').index(tr)){
					table.find('tr:eq(1)').children('td[name="deleCell"]').replaceWith('<td name="deleCell" style="width:20px;border:0"></td>');
				}
				tr.remove();
				table.height(tbH-rowH);
				//table.closest('div').height(tbH);
			}
			//获取选中行的行号
			function getSelected(){
				var selected = [];
				var trs = table.find('tr');
				trs.each(function(){
						if($(this).find(':checkbox:first').attr('checked')){
						selected.push(trs.index($(this)));
					}
				});
				return selected;
			}
			//获取指定行
			function getRow(rowIndex){
				return $(table.find('tr')[rowIndex]);
			}
			
			//获取内容改变的行的内容
			function getModified(){
				var rows = [];
				table.find('tr').each(function(){
					var td = [];
					if($(this).data('changed')){
						$(this).find('td').each(function(){
							td.push($(this).find('span'));
						});
						rows.push(td);
					}
				});
				return rows;
			}
			//获取删除行的idColumn内容
			function getDeleted(){
				return table.data('deleted');
			}
			//获取新增的行
			function getAddedRow(){
				var rows = [];
				table.find('tr').each(function(){
					var td = [];
					if($(this).data('newRow')){
						$(this).find('td').each(function(){
							td.push($(this).find('span'));
						});
						rows.push(td);
					}
				});
				return rows;
			}
			//重置行状态
			function resetRowStatus(args){//['newRow','changed']
				args = args ? args : ['newRow','changed'];
				table.find('tr').each(function(){
					for(var cell in args)
						$(this).data(args[cell],'');
				});
			}
			$.fn.autoGrid.self = $(this);
			$.fn.autoGrid.editable = editList;
			$.fn.autoGrid.revise = revise;
		}
	});
})(jQuery);