(function($){
	$.fn.extend({
		autoGrid:function(options){
			var settings = $.extend({},{
					isAdd:true,
					initRow:1,//初始行数
					delSta:-1,// 删除判断条件
					colPerRow:12,//每行单元格数
					VerifyEdit:{verify:false,enable:$.noop},//表格编辑条件
					editable:[1,4,6,9],//可编辑表格index
					widths:[26,100,180,80,80,80,50,80,80,80],//每格宽度
					colStyle:['','','','','','','','','',''],
					onEdit:$.noop,
					onLastClick:$.noop,
					onEnter:function(data){//默认回车后动作    data中curobj表示当前td对象，value保存输入值，ovalue保存输入之前的值
						var self = curobj.find('span');
						$.trim(data.value) ? self.html(data.value) : self.html(data.ovalue) ;
					},
					cellAction:[]
			},options);
			
			var div = $("<div></div>");
			var table = $("<table cellspacing='0' cellpadding='0'></table>");
			var row = $("<tr></tr>");
			var cell = $("<td><span></span></td>");
			var edit = 0;
			var lastCell = $('<td name="deleCell" style="width:10px;border:0;cursor: pointer;"  onclick="$.fn.autoGrid.deleteRow(this,'+settings.delSta+')"><img src="../image/scm/move.gif"/></td>');
			var addCell = $('<td name="addCell" style="width:10px;border:0;cursor: pointer;"  onclick="$.fn.autoGrid.addRow(2)"><img src="../image/scm/add.png"/></td>');
			//判断当前div内有无表格，有则追加行并追加删除按钮，没有则新建表格
			if($(this).find("table").length > 0 && $(this).find("table:last").find('tr').length > 0 && $(this).find("table:last").find('tr:first').find('td').length > 0){
				table = $(this).find("table:last");
				if($(this).get(0) == table.parent().get(0))
					table.wrap('<div></div>');
				row = table.find("tr:first").clone();
				row.find('td').find('span').html('');
				table.find('tr').not(table.find('tr:first')).each(function(){
					if(!$(this).find('td:last').attr('name') || $(this).find('td:last').attr('name') != 'deleCell'){
						curLastCell = lastCell.clone();
						if(settings.isAdd)$(this).append(curLastCell);
					}
				});
				if(settings.isAdd)table.find('tr:last').append(addCell.clone());
				if(settings.isAdd && table.find('tr').length>1)table.find('tr:first').append(lastCell.clone());
			}else{
				table =  $(this).find("table:last") || table;
				div.html('');
				for(var i = 0 ; i < settings.colPerRow ; i ++){
					if(settings.widths[i])
						cell.find('span').width(settings.widths[i]-10);
					if(settings.colStyle[i])
						cell.css(settings.colStyle[i]);
					cell.clone().appendTo(row);
					cell = $("<td><span></span></td>");
				}
				$(this).html('');
				table.appendTo(div);
				div.appendTo($(this));
				for(var i = 0 ; i < settings.initRow ; i ++)row.clone().appendTo(table);
				if(settings.isAdd)table.find('tr').not(table.find('tr:first')).append(lastCell);
				if(settings.isAdd)table.find('tr:last').append(addCell.clone());
				table.find('tr').each(function(){
					$(this).children('td:first')
					.html('<span style="width:24px;padding:1px;">'+(table.find('tr').index(this)+1)+'</span>')
					.css("text-align","center");
				});
				if(settings.isAdd && table.find('tr').length>1)table.find('tr:first').append(lastCell.clone());
			}
			//获取可编辑单元格index
			for(s in settings.editable)
				edit += (1 << settings.editable[s]);
			//添加单击事件（判断单元格是否可编辑并进行想应操作）
			table.closest('.grid').click(function(event,aim,n){
				if($(event.target)[0].tagName=='IMG'){
					return null;
				}
				if($(event.target)[0].tagName=='DIV'){
					table.find('td').find('input').each(function(){
						settings.onEnter({curobj:$(this).closest('td'),value:$(this).val(),ovalue:$(this).data("ovalue")});
					});
					return null;
				}
				if($(event.target).closest('table').parent('div').attr('class')=='table-head'){
					return null;
				}
				if(($(event.target).closest('td').get(0).tagName).toLowerCase() == 'td' || (event.target.tagName).toLowerCase() == 'td' || aim){
					var curRow = aim ? $(aim).closest('tr').get(0) : $(event.target).closest('tr').get(0);
					var posX = aim ? n : $(event.target).closest('tr').find('td').index($(event.target).closest('td'));
					var self = $(curRow).find("td:eq("+posX+")");
					table.find('td').not(self).find('input').each(function(){
						settings.onEnter({curobj:$(this).closest('td'),value:$(this).val(),ovalue:$(this).data("ovalue"),actionobj:self});
					});
					if((1 << posX)& edit)setEditable(self,$(curRow));
				}
			});
			//设置表格可编辑
			function setEditable(cur,curRow){
				if(cur.attr('edit')=='false'){
					var posX = curRow.find('td').index(cur);
					var nextPosX = settings.editable[$.inArray(posX,settings.editable)+1];
					setEditable(curRow.find('td:eq('+nextPosX+')'),curRow);
					return;
				}
				$("#mMenu").remove();
				if(settings.VerifyEdit.verify){
					if(!settings.VerifyEdit.enable(cur,curRow))return;
				}
//				table.find('td').not(cur).find('input').each(function(){
//					settings.onEnter({curobj:$(this).closest('td'),value:$(this).val(),ovalue:$(this).data("ovalue")});
//				});
				var cellAction ;
				var cellEdit ;
				var customAction;
				var self = cur.find('span');
				//获取单元格自定义方法
				for(a in settings.cellAction){
					if(settings.cellAction[a].index == curRow.find('td').index(cur.get(0))){
						cellAction = settings.cellAction[a].action;
						cellEdit = settings.cellAction[a].onCellEdit;
						customAction = settings.cellAction[a].CustomAction;
						$.fn.autoGrid.resultFmt = settings.cellAction[a].resultFormat;
						$.fn.autoGrid.afterEnter = settings.cellAction[a].afterEnter;
					}
				}
				//为单元格添加input
				if(self.children("input").length > 0){
					self.text(self.children("input").val());
				}
				var input = $("<input id='input' name='input' style='border:0;' />");
				var text = self.text();
				input.data('ovalue',text);
				input.width(self.width()-4);
				input.val(text);
				self.html('');
				input.appendTo(self).focus().select();
				input.click(function(){
					return false;
				});
				//onEdit事件执行单元格自定义方法，没有则执行默认方法
				input.bind('onEdit',function(event){
					typeof(cellEdit) == 'function' ? cellEdit(event,{curobj:cur,value:$(this).val(),valBefore:text},curRow) : settings.onEdit(event,{curobj:cur,value:$(this).val(),valBefore:text});
				});
				//onEnter事件，点击enter后执行方法
				input.bind('onEnter',function(event){
					if(top.$("#messagewin").length==1){
						top.$("#okbtn").trigger('click');
						return;
					}
					
					if($(this).next("ul").length == 1){
						var e = $.Event('keydown',{keyCode:13});
						$(document).trigger(e,0);
						return;
					};
					settings.onEnter({curobj:cur,value:input.val(),ovalue:text});
					if(typeof(cellAction) == 'function')cellAction(curRow,{value:$(this).val(),ovalue:text});
					return false;
				});
				//单元格自定义事件判断，添加
				typeof(customAction) == 'function' ? input.bind('CustomAction',customAction(event,{curobj:cur,row:curRow,input:input})) : 
				input.bind('keydown',fc = function(event){
					switch(event.keyCode){
					case 37:
						var currentData = {value:$(this).val(),ovalue:text};
						var c = $(settings.editable).index(curRow.find('td').index($(this).closest('td')));
						if(settings.editable[c-1]){
							if(c < settings.editable.length - 1){
								if(typeof(cellAction) == 'function'){
									var args = ['curRow','currentData'];
									var pat = /function\s*\(.*,.*\)\{/;
									var act = String(cellAction).replace(pat, "").replace(/}$/, "");
									var params = String(cellAction).replace(/function\s*\(/g, "").replace(/\)\{[\d\D]*\}$/g, "").split(",");
									act = act.replace(eval("/(\\W+)"+params[0]+"(\\W+)/g"),"$1"+args[0]+"$2");
									act = act.replace(eval("/(\\W+)"+params[1]+"(\\W+)/g"),"$1"+args[1]+"$2");
									act = act.replace(eval("/(\\$\\.fn\\.autoGrid\\.setCellEditable\\(.*,)"+ settings.editable[c+1]+"+(\\);)/g"),"$1"+settings.editable[c-1]+"$2");
									input.parent('span').html(currentData.value);
									eval(act);
								}
							}else{
								$(this).parent('span').html(currentData.value);
								setCellEditable(curRow,settings.editable[c-1]);
							}
						}
						break;
					case 39:
						var c = $(settings.editable).index(curRow.find('td').index($(this).closest('td')[0]));
						if(c != settings.editable.length - 1)input.trigger('onEnter');
						break;
					case 40:
					case 38:
						if($(this).next("ul").length == 1)
							input.get(0).blur();
						else
							return false;
						break;
					case 13:
					case 108:
						input.trigger('onEnter');
						return false;
						break;
					case 27:
						self.text(text);
						break;
					}
				});
				input.bind('keyup',function(event){
					var code = event.keyCode;
					if(code != 13 && code != 37 && code != 39 && code != 38 && code != 40)
						input.trigger('onEdit');
				});
				input.trigger('CustomAction');
			}
			//设置表格可编辑
			function setCellEditable(row,n){
				setEditable($(row.get(0).cells[n]),row);
			}
			//删除行
			function deleteRow(obj,delSta){
				
				// 根据显示状态确认是否要删除 --- 损耗管理使用
				if(delSta != -1){
					var v = $(obj).parent("tr").data('state');
					if(v!=null && v !=''){
						if(v == delSta){
							alert("【已确认】不能进行删除!");
							return;
						}
					}
				}
				var tb = $(obj).closest('table');
				var rowH = $(obj).parent("tr").height();
				var tbH = tb.height();
				$(obj).parent("tr").nextAll("tr").each(function(){
					var curNum = Number($.trim($(this).children("td:first").text()));
					$(this).children("td:first").html('<span style="width:26px;padding:0px;">'+Number(curNum-1)+'</span>');
				});
				
				if($(obj).next().length!=0){
					var addCell = $('<td name="addCell" style="width:10px;border:0;cursor: pointer;"  onclick="$.fn.autoGrid.addRow(2)"><img src="../image/scm/add.png"/></td>');
					tb.find('tr:last').prev().append(addCell);
				}
				
				var tr = $(obj).closest('tr');
				if(tr.prev().length==0 ){//删除第一行
					if(tr.next().find('td:last').attr('name')=='addCell'){//第二行最后是个+号
						tr.next().find('td[name="deleCell"]').remove();
					}
				}else if(tr.prev().prev().length==0){//点击第二行的删除
					if(tr.find('td:last').attr('name')=='addCell'){
						tr.prev().find('td[name="deleCell"]').remove();
					}
				}
				
				$(obj).parent("tr").remove();
			};

			//添加行
			function addRow(){
				table.find('tr:last').find('td:last').remove();
				row.clone().appendTo(table);
				table.find('tr:last').children('td:first').html('<span style="width:25px;padding:0px;">'+table.find('tr').length+'</span>').css("text-align","center");
				table.find('tr:last').find('td:eq(2)').find('span').attr('vname','');
				table.find('tr:last').find('td:eq(5)').find('span').attr('vname','');
				if(settings.isAdd)table.find('tr:last').append(lastCell.clone());
				if(settings.isAdd)table.find('tr:last').append(addCell.clone());
				
				if(table.find('tr:first').find('td:last').attr('name')!='deleCell'){
					table.find('tr:first').append(lastCell.clone());
				}
				
				var tb = table.closest('table');
				var tbH = tb.height();
				table.closest('div').height(tb.height());
			}
			//公开的，设置表格可编辑，添加行，ajax填充的方法
			$.fn.autoGrid.setCellEditable = function(row,n){setCellEditable(row,n);};
			$.fn.autoGrid.addRow = function(){addRow();};
			$.fn.autoGrid.deleteRow = function(obj,eqDelRow){deleteRow(obj,eqDelRow);};
			$.fn.autoGrid.resultFmt = $.noop;
			$.fn.autoGrid.afterEnter = $.noop;
			$.fn.autoGrid.ajaxEdit = function(data,row,td){
				var param = {};
				param[data.key] = data.value.toUpperCase();
				if(data.sp_position)param.sp_position = data.sp_position;
				$.ajax({
					type: "POST",
					url: data.url,
					data: param,
					dataType: "json",
					success:function(supplyList){
						if(supplyList.length!=0){
							$(document).unbind('.ajax');
							$("#mMenu").remove();
							var menu = $("<ul id='mMenu'style='position:absolute;'></ul>");
//							var menu = $("<ul id='mMenu'style='position:absolute;display:block;'></ul>");
							var input = $(data.curobj.find("input"));
							var tab = input.closest('table');
							var gridHeight = tab.closest('.grid').height();
							var tabContent = tab.closest('div');
							var size = 10;
							for(var i in supplyList){
								var list = $('<li></li>');
								list.text($.fn.autoGrid.resultFmt(supplyList[i]));
								list.appendTo(menu);
							}
//							tab.closest('.div').height(tab.closest('.div').height()+20*supplyList.length)
//							window.scrollTo(0,document.body.scrollHeight);
//							tabContent.scrollTop(0,document.body.scrollHeight)
							size = size - supplyList.length;
							var mtheight = $(data.curobj.find("input")).offset().top+(20*supplyList.length);
							//alert(document.body.clientHeight+":"+gridHeight+":"+tab.clientHeight);
//							if(mtheight>=document.body.clientHeight){
//								$(data.curobj.find("input")).after(menu);
//								menu.css("top",$(data.curobj.find("input")).offset().top
//										-(20*supplyList.length)-((20*supplyList.length)/2)
//										+$(data.curobj.find("input")).height()-20*size+10*size);
//							}else{
								$(data.curobj.find("input")).after(menu);
//							}
							var tableHeight = tab.height();
							var menuHeight = menu.height();
//							if(tableHeight>gridHeight){
								tabContent.height(menu.offset().top - tabContent.parent().closest('div').offset().top + menuHeight);
								tabContent.parent().closest('div').scrollTop(tabContent.height() - tabContent.parent().closest('div').height());
								tab.height(tableHeight);
								tabContent.parent().closest('div').bind("scroll.ajax",function(event){
									$(this).scrollTop(tabContent.height() - tabContent.parent().closest('div').height());
								});
//							}
							
							var cur = menu.children("li").first().addClass("mMenuSelected");
							$(document).bind('keydown.ajax',function (event,custom) {
								switch(event.keyCode){
								case 40:
									cur = cur.next().text() ? cur.next() : menu.children("li").first();
									break;
								case 38:
								//	return false;
									cur = cur.prev().text() ? cur.prev() : menu.children("li").last();
									break;
								case 13:
									var index = custom >= 0 ? custom : menu.children("li").index(cur);
										$(document).unbind('.ajax');
										tabContent.parent().closest('div').unbind(".ajax");
										$("#mMenu").remove();
										if(td)td.find('input').focus();
										//tabContent.height(tab.height());
										$.fn.autoGrid.afterEnter(supplyList[index],row);
										return;
								}
								$('#mMenu').children("li").removeClass("mMenuSelected");
								cur.addClass("mMenuSelected");
							}); 
							tabContent.parent().closest('div').scroll(function(event){
								var inOffset = input.offset();
								var offset = menu.offset();
								offset.top=inOffset.top+15;
								menu.offset(offset);
							});
							menu.click(function(event){
								if(event.target.tagName.toLowerCase() == 'li'){
									var e = $.Event('keydown.ajax',{keyCode:13});
									$(document).trigger(e,menu.children("li").index(event.target));
								}
								return false;
							});
//							tabContent.parent().closest('div').trigger('scroll');
						}
					},
					error: function(){
						alert("服务器没有返回数据，可能服务器忙，请重试");
					}
				});
			};
		}
	});
})(jQuery);