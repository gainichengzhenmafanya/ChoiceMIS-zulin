﻿(function($){
	$.fn.ButtonObj = function(){
		return this.each(function() {
			var button = new ButtonObj();
		});
	};
	
	ButtonObj= function(options,container){
		//初始化按钮
		this._init();
	};
	
	ButtonObj.prototype = {
		_init:function(){
			this.pk_button,
			this.pk_pubitem,
			this.vcode,
			this.vname,
			this.nnumbers,
			this.pk_buttontype,
			this.vtypename,
			this.vtitle,
			this.vcolors,
			this.vkeys,
			this.vbitmaps,
			this.pk_event,
			this.nwidth,
			this.nheight,
			this.nxpix,
			this.nypix;
		}
	};
})(jQuery);

//标记button颜色，代表按钮为选中状态
function signButtonColor(buttonDiv,event){
	var e = event || window.event || arguments.callee.caller.arguments[0];
	if(!e.ctrlKey){  //如果没有按下ctrl键，就是单选
//		$('.screenDiv').find('div').css("border-color","");
//		buttonDiv.css("border-color", "red");
//		if(buttonDiv.attr('id')!='movediv'){ //movediv不允许删除
//			deleteObj = buttonDiv;
//		}
		$('.screenDiv').find('div').removeClass('deletediv');
		buttonDiv.addClass('deletediv');
		if(buttonDiv.attr('id')!='movediv'){ //movediv不允许删除
//			deleteObj = buttonDiv;
			listDeleteDiv = $('.deletediv');
		}
	}else{  //如果按下ctrl键，可以多选
		if(buttonDiv.hasClass('deletediv')){
			buttonDiv.removeClass('deletediv');
		}else{
			buttonDiv.addClass('deletediv');
		}
		listDeleteDiv = $('.deletediv');
	}
}

var currentInputObj = null ;
var currentDiv  = null;
/**弹出图片选择
**/
function openSelectImg(inputObj,selectObj){
	currentInputObj = inputObj;
	currentDiv = selectObj;
	var ileftV = jQuery("#"+inputObj).offset().left; //获取目标文本框距左边框距离
	var itopV = jQuery("#"+inputObj).offset().top; //获取目标文本框距上边框距离
	var iinputH = jQuery("#"+inputObj).outerHeight(); //获取目标文本框高度
	
	jQuery("#"+selectObj).css("left",ileftV);// 设置弹出层距左边框距离
	jQuery("#"+selectObj).css("top", itopV + iinputH + 2);//设置弹出层距上边框距离
	jQuery("#"+selectObj).css("display","block");//设置弹出层显示
	
}

//关闭父级页面弹出层
function closeSelectImg(selectObj){
	jQuery("#"+selectObj,parent.document).css("display","none");
}

//选定后提交修改父级界面值
function submitSelectImg(inputObj,selectObj,vpath,imgname,type){
	jQuery("#showimg",parent.document).css("display","block");
	jQuery("#showspan",parent.document).css("display","none");
	jQuery("#"+inputObj,parent.document).val(imgname);
	jQuery("#"+selectObj,parent.document).css("display","none");
	jQuery("#imageShow",parent.document).attr("src",vpath+imgname+".PNG");
}

var movecount = 1;  //定义方向键一次移动的像素数
/**************键盘事件******************/
document.onkeydown=function(event){
	var e = event || window.event || arguments.callee.caller.arguments[0];
	if(e && e.keyCode==46){ // delete 键
		deleteButton();
	}
	if(e.ctrlKey){  //如果按下ctrl键
		$('#datagrid').css('overflow',"hidden");
		if(e && e.keyCode==37){  //左移
			leftmove();
		}
		if(e && e.keyCode==38){   //上移
			upmove();
		}
		if(e && e.keyCode==39){   //右移
			rightmove();
		}
		if(e && e.keyCode==40){   //下移
			downmove();
		}
	}
}; 

document.onkeyup=function(event){
	var e = event || window.event || arguments.callee.caller.arguments[0];
	if(e && e.keyCode==17){ // 如果ctrl键弹起
		$('#datagrid').css('overflow',"auto");
	}
};

//判断是否选中了控件
function chargeSelect(){
	if(listDeleteDiv.length <= 0){
		alert("没有要移动的控件！");
		return false;
	}else{
		return true;
	}
}
//左移
function leftmove(){
	if(chargeSelect()){  
		//如果选中了控件
		for(var i=0;i<listDeleteDiv.length;i++){
			var left = $(listDeleteDiv[i]).position().left;
			var x = (left-movecount)>0?left-movecount:0;
			$(listDeleteDiv[i]).css('left',x);
		}
		saveflag=1;
	}
}
//上移
function upmove(){
	if(chargeSelect()){  
		//如果选中了控件
		for(var i=0;i<listDeleteDiv.length;i++){
			var top = $(listDeleteDiv[i]).position().top;
			var x = (top-movecount)>0?top-movecount:0;
			$(listDeleteDiv[i]).css('top',x);
		}
		saveflag=1;
	}
}
//右移
function rightmove(){
	if(chargeSelect()){  
		//如果选中了控件
		for(var i=0;i<listDeleteDiv.length;i++){
			var left = $(listDeleteDiv[i]).position().left;
			var x = (left*1+movecount)>0? left*1+movecount :0;
			$(listDeleteDiv[i]).css('left',x);
		}
		saveflag=1;
	}
}
//下移
function downmove(){
	if(chargeSelect()){  
		//如果选中了控件
		for(var i=0;i<listDeleteDiv.length;i++){
			var top = $(listDeleteDiv[i]).position().top;
			var x = (top*1+movecount)>0? top*1+movecount :0;
			$(listDeleteDiv[i]).css('top',x);
		}
		saveflag=1;
	}
}
//左对齐
function leftalign(){
	if(chargeSelect()){
		var standleft = $(listDeleteDiv[0]).position().left;//定义左对齐的位置，默认取第一个div的值
		//如果选中了控件，遍历控件，选y坐标最小的
		for(var i=0;i<listDeleteDiv.length-1;i++){
			var left = $(listDeleteDiv[i+1]).position().left; //取下一个按钮的x轴坐标
			if(standleft-left>0){  //如果标准值大于这个值，就取这个值
				standleft = left;
			}
		}
		//遍历控件，设置左对齐
		for(var i=0;i<listDeleteDiv.length;i++){
			$(listDeleteDiv[i]).css('left',standleft);
		}
		saveflag=1;  //表示已经修改
	}
}

//顶对齐
function topalign(){
	if(chargeSelect()){
		var standtop = $(listDeleteDiv[0]).position().top;//定义顶对齐的位置，默认取第一个div的值
		//如果选中了控件，遍历控件，选y坐标最小的
		for(var i=0;i<listDeleteDiv.length-1;i++){
			var top = $(listDeleteDiv[i+1]).position().top; //取下一个按钮的x轴坐标
			if(standtop-top>0){  //如果标准值大于这个值，就取这个值
				standtop = top;
			}
		}
		//从第二个开始遍历控件，设置左对齐
		for(var i=1;i<listDeleteDiv.length;i++){
			$(listDeleteDiv[i]).css('top',standtop);
		}
		saveflag=1;//表示已经修改
	}
}


/** 
*删除数组指定下标或指定对象 
*/
Array.prototype.remove=function(obj){ 
	for(var i =0;i <this.length;i++){ 
		var temp = this[i]; 
		if(!isNaN(obj)){ 
			temp=i; 
		} 
		if(temp == obj){ 
			for(var j = i;j <this.length;j++){ 
				this[j]=this[j+1]; 
			} 
			this.length = this.length-1; 
		} 
	} 
};
//--------------------------------------台位设置--------------------------------------
(function($){
	$.fn.SiteObj = function(){
		return this.each(function() {
			var site = new SiteObj();
		});
	};
	
	SiteObj= function(options,container){
		//初始化按钮
		this._init();
	};
	
	SiteObj.prototype = {
		_init:function(){
			this.divcnt,	//批量添加时的div数量
			this.pk_sited,
			this.vcode,
			this.pk_storeid,
			this.pk_storearearid,
			this.vname,
			this.enablestate,
			this.vtablefcode,
			this.ishow,
			this.ipersons,
			this.iusestatus,
			this.itablesrotno,
			this.itableposid,
			this.vonclick,
			this.vintx,
			this.vinty,
			this.vwidth,
			this.vheight,
			this.vallwidth,
			this.vallheight,
			this.vallintx,
			this.vallinty,
			this.vmemo,
			this.creator,
			this.creationtime,
			this.modifier,
			this.modifiedtime,
			this.vinit,
			this.ifloor,
			this.iscrooms,
			this.ityp,
			this.nmincost,
			this.nroomcost,
			this.ncostrate,
			this.nagio,
			this.pk_prn,
			this.vprnip,
			this.isarear,
			this.pk_arearid,
			this.areardes,
			this.varearpix;
		}
	};
})(jQuery);