$("#movediv").draggable({ 
	grid: [ 1,1 ],
	containment: "parent",
	drag:function(event,ui){
		show_coords(event);
	}
});
$("#movediv").resizable({
	grid: [1,1],  // 拖拽距离
	minHeight: 60,  // 最小高度
	minWidth: 160,	// 最小宽度
	handles: 'e,s,se',  //拖拽上左边框定位不准
	helper: "ui-resizable-helper",
	start:function(event,ui){
		show_coords(event);
	},
	resize:function(event,ui){
		show_coords(event);
	},
	stop:function(event,ui){
		show_coords(event);
	}
});
function show_coords(event){ 
	var t = $('#movediv').css('top').replace('px','');
	var f = $('#movediv').css('left').replace('px','');
	var w = $('#movediv').css('width').replace('px','');
	var h = $('#movediv').css('height').replace('px','');
	var x = $('#movediv').width()/16; 
	var y = $('#movediv').height()/15;  
	var xx = f*1 + w/2 -10; 
	var yy = t*1 + h/2 -5;
	$('#coords').css('left',xx*1);
	$('#coords').css('top',yy);
	$('#coords').text(x +" X "+ y);
//	$('#coords').show();
}
$('#movediv').bind('click',function(){
	$('#coords').hide();
});
$('#movediv').bind('dblclick',function(){
	$('#coords').hide();
	batchAddButton();  //批量添加按钮
});
$('#movediv').mousedown(function(e){
	if(e.which==3){//右键隐藏
		$(this).hide();
	}
});