(function($){
	$.fn.CommonMethod = function(){
		return this.each(function() {
			var commonMethod = new CommonMethod();
		});
	};
	
	CommonMethod= function(options,container){
		//初始化按钮
		this._init();
	};
	
	CommonMethod.prototype = {
		_init:function(){
			this.vcode,
			this.vcodename,
			this.vtablename,
			this.isortno;
			this.enablestate;
			this.ids;
			this.pk_id;
			this.pk_store;
			this.modulename;
		}
	};
})(jQuery);


//编码重复验证
function validateVcode(vpath,common){
	$.ajaxSetup({ 
		  async: false 
  	});
	
	var returnvalue = 0;
	
	var data = {};
	data["vcode"] = common.vcode;
	data["vcodename"] = common.vcodename;
	data["vtablename"] = common.vtablename;
	data["pk_store"] = common.pk_store;
	
	$.post(vpath + "/misbohcommon/validateVcode.do",data,function(returndata){
		if (returndata != null && returndata > 0) {
			returnvalue = 1;
		} else if (returndata != null && returndata == "error") {
			returnvalue = -1;
		} else {
			returnvalue = 0;
		}
	});
	return returnvalue;
}

//获取默认排序
function getSortNo(vpath,common){
	$.ajaxSetup({ 
		  async: false 
  	});
	
	var returnvalue = 1;
	
	var data = {};
	data["isortno"] = common.isortno;
	data["vtablename"] = common.vtablename;
	$.post(vpath + "/misbohcommon/getSortNo.do",data,function(returndata){
		if (returndata!=null && returndata!="error" && returndata!="") {
			returnvalue = returndata;
		}
	});
	return returnvalue;
}


//排序号重复验证
function validateSortNo(vpath,common){
	$.ajaxSetup({ 
		  async: false 
	});
	 
	var returnvalue ;
	var data = {};
	data["isortno"] = common.isortno;
	data["vtablename"] = common.vtablename;
	data["visortnoName"] = common.visortnoName;
	data["pk_id"]=common.pk_id;
	data["pk_id_name"]=common.pk_id_name;
	$.post(vpath + "/misbohcommon/validateSortNo.do",data,function(returndata){
		returnvalue=returndata;
	});
	return returnvalue;
}

//设置是否启用
function IsEnableFiled(vpath,common){
	$.ajaxSetup({ 
		  async: false 
  	});
	
	var returnvalue = 0;
	
	var data = {};
	data["enablestate"] = common.enablestate;
	data["vtablename"] = common.vtablename;
	data["pk_id"]=common.pk_id;
	data["ids"]=common.ids;
	data['modulename']=common.modulename;
	data['vcode']=common.vcode;
	
	$.post(vpath + "/commonMethod/IsEnable.do",data,function(returndata){
		if (returndata!=null && returndata!="error") {
			returnvalue = returndata;
		}
	});
	return returnvalue;
}

//特殊字符验证
function checkSpecilWord(objvalue){
	var regArray=new Array("`","~","!","@","#","$","^","&","*","=","|","{","}","'","%",":","；",
			"'",",","\\","\\\\","[","\\","\\\\","]",".","<",">","?","！","@","#","￥","…","…",
			"&","*","&",";","【","】","‘","’","；","：","”","“","。","，","、","？");
	var len = regArray.length;
	for(var iu=0;iu<len;iu++){
        if (objvalue.indexOf(regArray[iu])!=-1){
			return false;
        }
	}
	return true;
}

//ajax获取选项按钮的默认第一个值,tablename表名，namefield显示字段，idfield实际值字段,wheresql查询语句，ordfield排序字段，tg_name页面显示的控件id，
//tg_id页面实际值的控件id,isall是否查询全部数据(0或1，默认为0，只查询有用数据)
function getDefaultValue(vpath,tablename,namefield,idfield,wheresql,ordfield,tg_name,tg_id,isall){
	$.ajaxSetup({async:false});
	$.post(vpath +"/commonMethod/getDefaultValue.do",{"tablename":tablename,"namefield":namefield,"idfield":idfield,"wheresql":wheresql,
		"ordfield":ordfield,"tg_name":tg_name,"tg_id":tg_id,"isall":isall},function(data){
		if(data){
			$('#'+tg_name).val(data['NAME']);//赋显示值
			$('#'+tg_id).val(data['PKID']);//赋实际值
		}
	});
}

//获取功能新增时默认的编码    ,tablename表名，namefield查询的编码字段，tg_name赋值的页面id，wheresql其他的查询条件,defaultvalue如果查询为空默认返回的第一个值
function getDefaultVcode(vpath,tablename,namefield,tg_name,defaultvalue,wheresql){
	$.ajaxSetup({async:false});
	$.post(vpath +"/misbohcommon/getDefaultVcode.do",{"tablename":tablename,"namefield":namefield,"wheresql":wheresql,
		"tg_name":tg_name,"defaultvalue":defaultvalue},function(data){
		if(data){
			$('#'+tg_name).val(data);//赋值
		}
	});
}

//获取功能新增时默认的排序    ,tablename表名，namefield查询的编码字段，tg_name赋值的页面id，wheresql其他的查询条件,defaultvalue如果查询为空默认返回的第一个值
function getDefaultSortNo(vpath,tablename,namefield,tg_name,defaultvalue,wheresql){
	$.ajaxSetup({async:false});
	$.post(vpath +"/misbohcommon/getDefaultSortNo.do",{"tablename":tablename,"namefield":namefield,"wheresql":wheresql,
		"tg_name":tg_name,"defaultvalue":defaultvalue},function(data){
		if(data){
			$('#'+tg_name).val(data);//赋值
		}
	});
}

//验证门店数量和注册数量
function validateCount(vpath){
	 var result = true;
	 $.ajaxSetup({ 
		  async: false 
	  	});
	 $.post(vpath +"/store/validateCount.do",{},function(data){
		 var rs = data;
		if(rs == "true"){
			result = true;
		}else{
			result = false;
		}
	});	
	 return result;
}

//展开树的所有节点
function expandAll(){
  if(this.totalNode>500) if(
    confirm("您是否要停止展开全部节点？\r\n\r\n节点过多！展开很耗时")) return;
  if(this.node["0"].childNodes.length==0) return;
  var e = this.node["0"].childNodes[0];
  var isdo = t = false;
  while(e.id != "0")
  {
    var p = this.node[e.parentId].childNodes, pn = p.length;
    if(p[pn-1].id==e.id && (isdo || !e.hasChild)){e=this.node[e.parentId]; isdo = true;}
    else
    {
      if(e.hasChild && !isdo)
      {
        this.expand(e.id, true), t = false;
        for(var i=0; i<e.childNodes.length; i++)
        {
          if(e.childNodes[i].hasChild){e = e.childNodes[i]; t = true; break;}
        }
        if(!t) isdo = true;
      }
      else
      {
        isdo = false;
        for(var i=0; i<pn; i++)
        {
          if(p[i].id==e.id) {e = p[i+1]; break;}
        }
      }
    }
  }
}