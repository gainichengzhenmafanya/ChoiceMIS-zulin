function ajaxSupply(key,path){
	var keys=$('input[name='+key+']').val();
	$.ajax({
		type: "POST",
		url: path+"/supply/findTop.do",
		data: ""+key+"="+keys,
		dataType: "json",
		success:function(supplyList){
			if(supplyList.length!=0){
				$("#mMenu").remove();
				$('input[name='+key+']').after("<ul id='mMenu'></ul>");
					$('#mMenu').append('<li class="mMenuSelected">'+supplyList[0].sp_code+'-'+supplyList[0].sp_init+'-'+supplyList[0].sp_name+'</li>');
				for(var i = 1 ; i < supplyList.length ; i++){
					$('#mMenu').append('<li>'+supplyList[i].sp_code+'-'+supplyList[i].sp_init+'-'+supplyList[i].sp_name+'</li>');
		  		};
				$("#mMenu li").each(function(i) {
					$(this).bind('click',function(){
						$('#sp_init').val(supplyList[i].sp_init);
						$('#sp_code').val(supplyList[i].sp_code);
						$('#sp_name').val(supplyList[i].sp_name);
						$('#sp_desc').val(supplyList[i].sp_desc);
						$('#unit').val(supplyList[i].unit);
						$('#price').val(supplyList[i].sp_price);
						$('#mMenu').hide();
					})
				});
				var j=0;
				$(document).keydown(function (event) { 
					if (event.keyCode == 40) { //下
						j++;
					}
					if (event.keyCode == 38) { //上
						j--;
					}
					if(j>=supplyList.length){
						j=0;
					}
					if(j<=-1){
						j=supplyList.length-1;
					}
					if (event.keyCode == 13) { //回车
						$('#sp_init').val(supplyList[j].sp_init);
						$('#sp_code').val(supplyList[j].sp_code);
						$('#sp_name').val(supplyList[j].sp_name);
						$('#sp_desc').val(supplyList[j].sp_desc);
						$('#unit').val(supplyList[j].unit);
						$('#price').val(supplyList[j].sp_price);
						$('#mMenu').remove();
					}
					$('#mMenu li').removeClass("mMenuSelected");
					$('#mMenu li:eq('+j+')').addClass("mMenuSelected");
				}); 
			}
		},
		error: function(){
			alert("服务器没有返回数据，可能服务器忙，请重试");
		}
	});
}