
function addMul(settings){
	if(top.addMulWindow){
		closeAddMul();
		addMulWindow = null;
	}
	addMulWindow = $('body').window({
		id: 'addMulWindow',
		title: settings.title,
		content: '<iframe id="selectFrame" name="selectFrame" frameborder="0" src="'+settings.url+'"></iframe>',
		width: 600,
		height: 500,
		draggable: false,
		isModal: true,
		confirmClose: false
//		position: {
//			type: 'absolute',
//			top: settings.offset.top,	
//			left: settings.offset.left
//		}
	});
	$('#selectFrame',top.window.document).load(function(){
		var self = $('#selectFrame',top.window.document).contents();
		self.find("#selectDone").click(function(){
			var show = settings.show;
			var value = settings.value;
			var checkboxList = self.find('.grid .table-body :checkbox');
			var showValue = [];
			var formValue = [];
			if(checkboxList 
					&& checkboxList.filter(':checked').size() > 0){
					checkboxList.filter(':checked').each(function(){
						var id = $.trim($(this).val());
						showValue.push($.trim($(this).closest("tr").find("#"+show+"_"+id).text()));
						formValue.push($.trim($(this).closest("tr").find("#"+value+"_"+id).text()));
					});
					settings.showBox.val(showValue.join(","));
					settings.valueBox.val(formValue.join(","));
			}
			closeAddMul();
			addMulWindow = null;
		});
	});
	return addMulWindow;
}

function closeAddMul(){
	if(addMulWindow){
		addMulWindow.close();
	}
}