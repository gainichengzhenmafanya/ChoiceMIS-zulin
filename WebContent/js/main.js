
function selectDepartment(path,offset,nameInput,idInput,code){
	departmentWindow = $('body').window({
		id: 'window_selectDepartment',
		title: '选择部门',
		content: '<iframe id="selectDepartmentFrame" name="selectDepartmentFrame" frameborder="0" src="'+path+'/department/select.do?code='+code+'"></iframe>',
		width: 200,
		height: 300,
		confirmClose: false,
		position: {
			type: 'absolute',
			top: offset.top,	
			left: offset.left
		},
		afterCloseHandler: function(){
			var deptName = $('#selectDepartmentFrame',top.window.document)
				.contents()
				.find('#departmentName')
				.val();
			var	deptId = $('#selectDepartmentFrame',top.window.document)
				.contents()
				.find('#departmentId')
				.val();
			
			nameInput.val(deptName ? deptName : nameInput.val());
			idInput.val(deptId ? deptId : idInput.val());
			
			departmentWindow = null;
		}
	});
	
	return departmentWindow;
}

function closeDepartment(){
	if(departmentWindow){
		departmentWindow.close();
	}
}

/**
 * 
 * @param option
 * 	option{
 * 		src: "",	//请求的路径
 * 		title: "",	//标题栏显示的信息
 * 		width: 200,	//窗口的宽度
 * 		height: 280,//窗口的高度
 * 		offset: {	//窗口的定位
 * 			top: 0,	
 * 			left: 0
 * 		},
 * 		items:[{	//选择之后，需要展示信息的对象数组
 * 			input: null	//显示信息的文本框（必须是jQuery对象）
 * 			selectInput: null	//显示选中信息的文本框（必须是jQuery选择器）
 * 		}]
 *  }
 *  
 * @returns lib.ui.window
 */
function selectWin(option){
	
	var config = {
		width: 200,
		height: 280
	};
	
	$.extend(config,option);
	
	selectWindow = $('body').window({
		id: 'window_select',
		title: config.title,
		content: '<iframe id="selectFrame" name="selectFrame" frameborder="0" src="'+config.src+'"></iframe>',
		width: config.width,
		height: config.height,
		confirmClose: false,
		position: {
			type: 'absolute',
			top: config.offset.top,	
			left: config.offset.left
		},
		afterCloseHandler: function(){
			if(config.items && $.isArray(config.items) && config.items.length > 0){
				$.each(config.items,function(i){
					var input = config.items[i].input,
					selectval = $('#selectFrame',top.window.document)
							.contents()
							.find(config.items[i].selectInput)
							.val();
					
					input.val(selectval ? selectval : input.val());
				});
			}
			
			selectWindow = null;
		}
	});
	
	return selectWindow;
}

function closeSelect(){
	if(selectWindow){
		selectWindow.close_enter();
	}
}