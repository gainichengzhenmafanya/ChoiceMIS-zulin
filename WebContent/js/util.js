(function($){
	$.fn.extend({
		htmlUtils:function(options,args,num){
			if(options == 'select'){
				var selected = [];
				var selectStyle = {display:'inline-block','white-space':'nowrap','font-size':'12px',padding:'0',border:'1px solid #A4BED4',background:'#fff',"margin-top":'2px'};
				var listStyle = {position:'absolute', "z-index": '10', display:'none', width: '200px' };
				var textStyle = {position:'relative', "z-index": '9',"font-size":'12px',border:'0px',"line-height":'18px',height:'18px',padding:'0px','*height':'18px',"*line-height":'18px',_height:'18px',"_line-height":'18px',width: '180px',cursor:'pointer'};
				var arrowStyle = {background:"#E0ECF9  url(../image/select_arrow.gif) no-repeat 3px 4px",width:'18px',height:'20px',overflow:'hidden',display:'inline-block',"vertical-align":'top',cursor:'pointer',opacity:'0.6',filter:'alpha(opacity=60)','white-space':'nowrap'};
				var panel = {background:'#fff',overflow:'auto',width: '198px', 'max-height': '198px',overflow:'auto',border:'1px solid #99BBE8','border-top-width':'0px' };
				var select = Boolean($(this).attr("select")) || false;
				var self = $(this);
				var single = $(this).attr("single");
				var id = $(this).attr("id");
				var defCode = $(this).attr("code");
				var defDes = $(this).attr("des");
				var offset = self.offset();//当前位置
				var loadOnce = String($(this).attr("loadOnce")) == 'false' ? false : true;
				var inputAction = args ? (typeof(args.inputClick) == 'function' ? args.inputClick : $.noop):$.noop;
				var beforLoad = args ? (typeof(args.beforLoad) == 'function' ? args.beforLoad : $.noop):$.noop;
				var onSelected = args ? (typeof(args.onSelected) == 'function' ? args.onSelected : $.noop):$.noop;
				var show_input = $('<span class="diy_select"><input autocomplete="off" readonly="readonly"><span><span></span></span></span>').css(selectStyle);
				var list = $("<div class='diy_select_list' name='sele_list_panel'><ul style='list-style:none inside none;padding:2px;margin:0px'></ul></div>")
					.css(listStyle)
					.css(panel)
					.width($(this).width());
				show_input.children('input')
					.css(textStyle)
					.width($(this).width()-Number(arrowStyle.width.replace("px","")))
					.click(function(){inputAction();});
				//默认显示值
				if($.trim(defDes))show_input.children('input').val($.trim(defDes));
				show_input.attr('id',id);
				show_input.width($(this).width());
				show_input.closest('span').children('input').next('span').children('span').css(arrowStyle)
					.hover(function(){
						$(this).css("opacity","1.0");
						$(this).css("filter","alpha(opacity=100)");
					},function(){
						$(this).css("opacity","0.6");
						$(this).css("filter","alpha(opacity=60)");
					})
					.bind('click.show',function(){showList(self);});
				defCode?self.data('checkedVal',defCode):self.data('checkedVal',"");
				defDes?self.data('checkedName',defDes):self.data('checkedName',"");
				if(!loadOnce){
					show_input.bind('click.load',function(event){
						beforLoad();
						loadContent();
					});
				}else{
					show_input.one('click.load',function(event){
						beforLoad();
						loadContent();
					});
				}
				var submitInput = $('<input type="hidden" value=""/>');
				//默认提交值
				if($.trim(defCode)){
					submitInput.val($.trim(defCode));
					selected = selected.concat(defCode.split(','));
				}
				show_input.append(submitInput.attr("name",$(this).attr("name")));
				list.hover($.noop,function(){hideList(show_input);});
				
				$(this).after(show_input);
				$(this).next('span').after(list);
				$(this).hide();
				return show_input;
				
				function setValue(){
					var checkedKey = [];
					var checkedVal = [];
					list.find("input").filter(":checkbox").filter(":checked").each(function(){
						if($(this).val() != 'selectAll'){
							checkedKey.push($(this).val());
							checkedVal.push($(this).parent('span').text());
						}
					});
					selected = checkedKey;
					show_input.find("input:hidden").val(checkedKey.join(","));
					show_input.find("input").first().val(checkedVal.join(","));
					self.data('checkedVal',checkedKey.join(","));
					self.data('checkedName',checkedVal.join(","));
				}
				function showList(cur){
					list.hide();
					list.slideDown("fast");
					$(cur).unbind(".show");
					$(cur).bind("click.hidden",function(){hideList(this);});
				}
				function hideList(cur){
					onSelected();
					list.slideUp("fast");
					$(cur).unbind(".hidden");
					$(cur).bind("click.show",function(){showList(this);});
				}
				function checkAllSelect(selectAll){
					var checkbox = list.find("input").filter(":checkbox").not(selectAll.find(":checkbox"));
					if(checkbox.size() == checkbox.filter(":checked").size())
						return true;
					else
						return false;
				}
				function loadContent(){
					self.siblings('.diy_select_list').find('ul').html('');
					var url = self.attr("url");
					var key = self.attr("key") || "code";
					var value = self.attr("data") || "des";
					var content = typeof(args) == 'object' ? (Object.prototype.toString.call(args) == '[object Array]' ? args : []):(args && args.content ? args.content : []);
					var selectAll = $('<li id="util_selectAll" style="line-height:18px;cursor:pointer;"><span><input style="margin:4px 1px 3px 1px;" type="checkbox" value="selectAll"/>&nbsp;全选</span></li>');
					if(url){
						$.ajaxSetup({ 
							  async: false 
							  });
						$.get(url,function(data){
							content = [];
							for(var i in data){
								if(data[i])
									content.push({key:data[i][key],value:data[i][value]});
							}
						});
					}
					if(select){
						selectAll.appendTo(list.children("ul"));
						selectAll.hover(function(){$(this).css('background','#D2E9FF');},function(){$(this).css('background','white');});
						selectAll.click(function(){
							$(this).find(":checkbox").triggerHandler("diy_click");
						});
						selectAll.find(":checkbox").bind("diy_click",function(){
							$(this).attr("checked") ? $(this).removeAttr("checked") : $(this).attr("checked","checked");
							if($(this).attr("checked")){
								list.children("ul").find(":checkbox").each(function(){
									$(this).attr("checked","checked");
								});
							}else{
								list.children("ul").find(":checkbox").each(function(){
									$(this).removeAttr("checked");
								});
							};
							setValue($(this));
						});
						selectAll.find("input").filter(":checkbox").bind("click",function(event){
							if($(this).attr("checked")){
								list.children("ul").find(":checkbox").each(function(){
									$(this).attr("checked","checked");
								});
							}else{
								list.children("ul").find(":checkbox").each(function(){
									$(this).removeAttr("checked");
								});
							};
							setValue();
							event.stopPropagation();
						});
					}
					for(var i in content){
						var element = $('<li style="line-height:18px;cursor:pointer;"><span><input style="margin:4px 1px 3px 1px;" type="checkbox"/>&nbsp;</span></li>');
						element.children("span").children("input").val(content[i].key);
						element.children("span").append(content[i].value);
						//if($.inArray(content[i].key,selected) >= 0)element.find('input').attr('checked','checked');
						element.appendTo(list.children("ul"));
						element.hover(function(){$(this).css('background','#D2E9FF');},function(){$(this).css('background','white');});
						element.click(function(){
							$(this).find(":checkbox").triggerHandler("diy_click");
						});
					}
					if(checkAllSelect(selectAll)){
						selectAll.find(":checkbox").attr("checked","checked");
					}
					list.find("input").filter(":checkbox").not(selectAll.find(":checkbox")).bind("diy_click",function(event){
						if(single){
							$(this).attr("checked","checked");
							list.find("input").filter(":checkbox").not($(this)).removeAttr("checked");
						}else
							$(this).attr("checked") ? $(this).removeAttr("checked") : $(this).attr("checked","checked");
						setValue();
						if(checkAllSelect(selectAll)){
							selectAll.find(":checkbox").attr("checked","checked");
						}else{
							selectAll.find(":checkbox").removeAttr("checked");
						};
					});
					list.find("input").filter(":checkbox").not(selectAll.find(":checkbox")).bind("click",function(event){
						if(single){
							$(this).attr("checked","checked");
							list.find("input").filter(":checkbox").not($(this)).removeAttr("checked");
						}else
							$(this).attr("checked") ? $(this).removeAttr("checked") : $(this).attr("checked","checked");
						setValue();
						if(checkAllSelect(selectAll)){
							selectAll.find(":checkbox").attr("checked","checked");
						}else{
							selectAll.find(":checkbox").removeAttr("checked");
						};
						event.stopPropagation();
					});
				}
			}
			
			
			else if (options == 'setDate'){
				var dat = new Date();
				var now="";
				if(!isNaN(args)){
					dat.setDate(dat.getDate()-args);
					now += dat.getFullYear()+"-";     
					now += ((dat.getMonth()+1)<10 ? "0"+(dat.getMonth()+1):(dat.getMonth()+1))+"-";     
					now += (dat.getDate()<10 ? "0"+dat.getDate():dat.getDate());
				}else if(args == "now"){
					now += dat.getFullYear()+"-";     
					now += ((dat.getMonth()+1)<10 ? "0"+(dat.getMonth()+1):(dat.getMonth()+1))+"-";     
					now += (dat.getDate()<10 ? "0"+dat.getDate():dat.getDate());
				}else if(args == "yes"){
					dat = new Date(dat.getTime() - 24*60*60*1000);
					now += dat.getFullYear()+"-";     
					now += ((dat.getMonth()+1)<10 ? "0"+(dat.getMonth()+1):(dat.getMonth()+1))+"-";     
					now += (dat.getDate()<10 ? "0"+dat.getDate():dat.getDate());
				}else if(args == "prevMonth"){
					var year = dat.getFullYear();
					var month = dat.getMonth();
					var day = dat.getDate();
					var total = getDaysOfMonth({year:year,month:month+1});
					var prevTotal = getDaysOfMonth({year:dat.getFullYear(),month:dat.getMonth() ? dat.getMonth() : 12});
					var prevDate = new Date(dat.getTime() - (prevTotal * (total-day) / total + day)*24*60*60*1000);
					var month = prevDate.getMonth() + 1;
					var year = prevDate.getFullYear();
					var day = prevDate.getDate();
					now = year + "-"; + month + "-";
					now += (month < 10 ? "0" + month : month) + "-";
					now += (day<10 ? "0"+day:day);
				}else if(args == "curYear"){
					now = dat.getFullYear();
				}else if(args == "curMonth"){
					now = ((dat.getMonth()+1)<10 ? "0"+(dat.getMonth()+1):(dat.getMonth()+1));
				}else if(args == "curYearMonth"){
					now = dat.getFullYear() + "-" + ((dat.getMonth()+1)<10 ? "0"+(dat.getMonth()+1):(dat.getMonth()+1));
				}else if(args == "year"){
					now += Number(Number(dat.getFullYear())+Number(num))+"-";     
					now += ((dat.getMonth()+1)<10 ? "0"+(dat.getMonth()+1):(dat.getMonth()+1))+"-";     
					now += (dat.getDate()<10 ? "0"+dat.getDate():dat.getDate());
				}else if(args == "month"){
					var preyear = dat.getFullYear();
					var premonth = dat.getMonth();
					var preday = dat.getDate();
					var month = premonth + Number(num);
					if(month>12){
						month = month-12;
						preyear = preyear + 1;
					}
					now = preyear + "-"; + month + "-";
					now += (month < 10 ? "0" + month : month) + "-";
					now += (preday<10 ? "0"+preday:preday);
				}else if(args=="lastDay"){
				     var currentMonth=dat.getMonth(); 
				     var nextMonth=++currentMonth; 
				     var nextMonthDayOne =new Date(dat.getFullYear(),nextMonth,1); 
				     var minusDate=1000*60*60*24; 
				     var last = new Date(nextMonthDayOne.getTime()-minusDate); 
				     now += last.getFullYear()+"-";     
					 now += ((last.getMonth()+1)<10 ? "0"+(last.getMonth()+1):(last.getMonth()+1))+"-";     
					 now += (last.getDate()<10 ? "0"+last.getDate():last.getDate());
				}
				
				$(this).val(now);
			}else if(options == 'getDaysOfMonth'){
				return getDaysOfMonth(args);
			};
			
			function getDaysOfMonth(args){
				var year = Number(args.year);
				var month = Number(args.month);
				var days = 0;
				var month1={1:31,3:31,5:31,7:31,8:31,10:31,12:31,4:30,6:30,9:30,11:30};
				if(month1[month]){
					days = month1[month];
				}else if(month==2){
						days =year%400 == 0 || (year%4==0 && year%100!=0)?29:28;
				}else{
					alert("月份不能为空！");
				}
				return days;
			}
		}
	});
})(jQuery);